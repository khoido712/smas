/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;

using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Threading.Tasks;
using System.Web.Configuration;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class CapacityStatisticBusiness
    {
        private int NumTask
        {
            get
            {
                if (WebConfigurationManager.AppSettings["NumTaskReport"] != null)
                {
                    return Convert.ToInt32(WebConfigurationManager.AppSettings["NumTaskReport"]);
                }

                return 50;
            }
        }

        #region Lay du lieu bao cao cu
        public void SendAll(List<CapacityStatisticsBO> ListData)
        {
            CapacityStatistic objCap = null;
            foreach (CapacityStatisticsBO cs in ListData)
            {
                objCap = new CapacityStatistic();
                objCap.ProcessedDate = DateTime.Now;
                objCap.ReportCode = cs.ReportCode;
                objCap.SchoolID = cs.SchoolID;
                objCap.AcademicYearID = cs.AcademicYearID;
                objCap.Year = cs.Year;
                objCap.Semester = cs.Semester;
                objCap.EducationLevelID = cs.EducationLevelID;
                objCap.SubCommitteeID = cs.SubCommitteeID;
                objCap.SentToSupervisor = true;
                objCap.SentDate = DateTime.Now;
                objCap.CapacityLevel01 = cs.CapacityLevel01;
                objCap.CapacityLevel02 = cs.CapacityLevel02;
                objCap.CapacityLevel03 = cs.CapacityLevel03;
                objCap.CapacityLevel04 = cs.CapacityLevel04;
                objCap.CapacityLevel05 = cs.CapacityLevel05;
                objCap.CapacityLevel06 = cs.CapacityLevel06;
                objCap.CapacityLevel07 = cs.CapacityLevel07;
                objCap.PupilTotal = cs.PupilTotal;
                objCap.BelowAverage = cs.BelowAverage;
                objCap.OnAverage = cs.OnAverage;
                objCap.IsCommenting = cs.IsCommenting;
                objCap.SubjectID = cs.SubjectID;
                objCap.AppliedLevel = cs.AppliedLevel;
                Insert(objCap);
            }
        }

        public string GetHashKeyForConductCapacityStatistics(CapacityConductReport CapacityConductReport)
        {

            //Dictionary[“Year”] = Year
            //Dictionary[“EducationLevelID”] = EducationLevelID
            //Dictionary[“Semester”] = Semester
            //Dictionary[“TrainingTypeID”] = TrainingTypeID
            //Dictionary[“SubcommitteeID”] = SubcommitteeID
            //Dictionary[“ProvinceID”] = ProvinceID
            //Dictionary[“DistrictID”] = DistrictID

            //- Dictionary[“Year”] = Year
            //- Dictionary[“EducationLevelID”] = EducationLevelID
            //- Dictionary[“Semester”] = Semester
            //- Dictionary[“TrainingTypeID”] = TrainingTypeID
            //- Dictionary[“SubcommitteeID”] = SubcommitteeID
            //- Dictionary[“ProvinceID”] = ProvinceID
            //- Dictionary[“DistrictID”] = DistrictID


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = CapacityConductReport.Year;
            dic["EducationLevelID"] = CapacityConductReport.EducationLevelID;
            dic["Semester"] = CapacityConductReport.Semester;
            dic["TrainingTypeID"] = CapacityConductReport.TrainingTypeID;
            dic["SubcommitteeID"] = CapacityConductReport.SubcommitteeID;
            dic["ProvinceID"] = CapacityConductReport.ProvinceID;
            dic["DistrictID"] = CapacityConductReport.DistrictID;
            dic["SubjectID"] = CapacityConductReport.SubjectID;
            return ReportUtils.GetHashKey(dic);
        }

        /// <summary>
        /// <author>minhh</author>
        /// <date>4/12/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="InputParameterHashkey"></param>
        /// <returns></returns>
        public List<CapacityStatisticsBO> CreateSGDCapacityStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            string reportCodeToSearch = SystemParamsInFile.REPORT_THONG_KE_HOC_LUC_SO;
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");

            FileID = 0;

            int subCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");

            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");


            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceSecondary = this.SearchByProvince23(SearchInfo);
            if (lstCapacityStatisticOfProvinceSecondary != null)
            {


                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCodeToSearch);

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet 
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet, "P10");
                sheet.Name = "THCS_TKHL";

                //fill dữ liệu vào sheet thông tin chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);

                string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string districtName = districtID > 0 ? DistrictBusiness.Find(districtID).DistrictName : "[Tất cả]";
                string provinceName = supervisingDept.Province.ProvinceName;
                string trainingName = trainningTypeID > 0 ? TrainingTypeBusiness.Find(trainningTypeID).Resolution : "[Tất cả]";
                string educationName = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "Khối [Tất cả]";
                string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                string provinceAndDate = provinceName + ", " + dateTime;
                string semesterName = semester > 0 ? ReportUtils.ConvertSemesterForReportName(semester) : string.Empty;
                string semesterAndAcademic = districtName + " - Hệ " + trainingName + " - " + educationName + " - " + ReportUtils.ConvertSemesterForDisplayTitle(semester) + " - Năm học " + year + " - " + (year + 1);

                sheet.SetCellValue("A2", supervisingDeptName);

                sheet.SetCellValue("I4", provinceAndDate);
                sheet.SetCellValue("B7", semesterAndAcademic);

                int startRow = 10;

                IVTRange range = tempSheet.GetRange("A10", "P10");
                IVTRange lastRange = tempSheet.GetRange("A198", "P198");
                IVTRange rangeTC = tempSheet.GetRange("A200", "P200");
                
                for (int i = 0; i < lstCapacityStatisticOfProvinceSecondary.Count; i++)
                {
                    CapacityStatisticsBO capBO = lstCapacityStatisticOfProvinceSecondary[i];
                    capBO.TotalPupil = capBO.TotalPupil.HasValue ? capBO.TotalPupil.Value : 0;
                    capBO.TotalExcellent = capBO.TotalExcellent.HasValue ? capBO.TotalExcellent.Value : 0;
                    capBO.TotalGood = capBO.TotalGood.HasValue ? capBO.TotalGood.Value : 0;
                    capBO.TotalNormal = capBO.TotalNormal.HasValue ? capBO.TotalNormal.Value : 0;
                    capBO.TotalWeak = capBO.TotalWeak.HasValue ? capBO.TotalWeak.Value : 0;
                    capBO.TotalPoor = capBO.TotalPoor.HasValue ? capBO.TotalPoor.Value : 0;
                    capBO.TotalAboveAverage = capBO.TotalAboveAverage.HasValue ? capBO.TotalAboveAverage.Value : 0;
                    
                    if (i == lstCapacityStatisticOfProvinceSecondary.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                    }
                    else
                    {
                        //Copy row style
                        if (startRow + i > 10)
                        {

                            if ((i + 1) % 5 == 0)
                            {
                                sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                            }
                            else
                                sheet.CopyPasteSameRowHeigh(range, startRow);
                        }
                    }

                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ten truong
                    sheet.SetCellValue(startRow, 2, capBO.SchoolName);

                    //Ngay gui
                    sheet.SetCellValue(startRow, 3, capBO.SentDate.HasValue ? capBO.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);

                    //Tong so HS
                    sheet.SetCellValue(startRow, 4, capBO.TotalPupil);

                    //Gioi
                    sheet.SetCellValue(startRow, 5, capBO.TotalExcellent);
                    //sheet.SetCellValue(startRow, 6, Math.Round(capBO.PercentExcellent.Value, 2, MidpointRounding.AwayFromZero));


                    //Kha
                    sheet.SetCellValue(startRow, 7, capBO.TotalGood);
                    //sheet.SetCellValue(startRow, 8, Math.Round(capBO.PercentGood.Value, 2, MidpointRounding.AwayFromZero));

                    //TB
                    sheet.SetCellValue(startRow, 9, capBO.TotalNormal);
                    //sheet.SetCellValue(startRow, 10, Math.Round(capBO.PercentNormal.Value, 2, MidpointRounding.AwayFromZero));
                    
                    //Yeu
                    sheet.SetCellValue(startRow, 11, capBO.TotalWeak);
                    //sheet.SetCellValue(startRow, 12, Math.Round(capBO.PercentWeak.Value, 2, MidpointRounding.AwayFromZero));

                    //Kem
                    sheet.SetCellValue(startRow, 13, capBO.TotalPoor);
                    //sheet.SetCellValue(startRow, 14, Math.Round(capBO.PercentPoor.Value, 2, MidpointRounding.AwayFromZero));

                    //TB tro len
                    sheet.SetCellValue(startRow, 15, capBO.TotalAboveAverage);
                    //sheet.SetCellValue(startRow, 16, Math.Round(capBO.PercentAboveAverage.Value, 2, MidpointRounding.AwayFromZero));

                    startRow++;
                }
                
                sheet.CopyPasteSameRowHeigh(rangeTC, startRow);
                sheet.SetCellValue(startRow, 1, "Tổng cộng");
                sheet.MergeRow(startRow, 1, 3);
                string alphabetTS = VTVector.ColumnIntToString(4);
                string fomularTS = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                sheet.SetFormulaValue(startRow, 4, fomularTS.Replace("#", alphabetTS));

                for (int i = 1; i <= 11; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 4);
                    string fomular = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                    sheet.SetFormulaValue(startRow, i + 4, fomular.Replace("#", alphabet));
                }
                tempSheet.Delete();

                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCodeToSearch;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashkey;
                pr.ReportData = ReportUtils.Compress(oBook.ToStream());
                sentToSupervisor = true;
                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;;
                string khoi = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "TatCa";

                outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + DistrictBusiness.Find(districtID).DistrictName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(trainningTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", semesterName);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", semester},
                {"Year", year},
                {"EducationLevelID", educationLevelID},
                {"TrainingTypeID", trainningTypeID},
                {"DistrictID ", districtID = 0 },
                {"ProvinceID", provinceID},
                 {"SupervisingDeptID", SupervisingDeptID},
                {"SentToSupervisor", true}
            };
                ProcessedReportParameterRepository.Insert(dic, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
            }
            return lstCapacityStatisticOfProvinceSecondary;
        }

        /// <summary>
        /// <author>minhh</author>
        /// <modifier>hieund - 05/12/2012</modifier>
        /// <date>4/12/2012</date>
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>

        public List<CapacityStatisticsBO> SearchByProvince23(IDictionary<string, object> SearchInfo)
        {
            int districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            int year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int trainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int subCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                SupervisingDeptID = supervisingDept.ParentID.Value;
            }
            int AppliedLevel = 0;
            if (educationLevelID < 10)
                AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            else
                AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            if (provinceID == 0) return null;

            IQueryable<CapacityStatistic> queryCapacityStatistic = this.All;

            if (reportCode.Trim().Length != 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.ReportCode.Equals(reportCode));

            if (year > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.Year == year);

            if (semester > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.Semester == semester);

            if (sentToSupervisor.HasValue)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SentToSupervisor == sentToSupervisor);

            if (educationLevelID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.EducationLevelID == educationLevelID);

            if (subCommitteeID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SubCommitteeID == subCommitteeID);

            if (subjectID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SubjectID == subjectID);

            int grade = Utils.GradeToBinary(AppliedLevel);
            // Lay them thong tin cua ban hoc
            // Neu cap 2 thi truong nay la null nen ban chat la van lay ngay gui gan nhat theo truong
            // Neu cap 3 thi mot so truong hop phai tong hop du lieu tu nhieu ban do vay phai lay them de SUM
            IQueryable<CapacityStatistic> queryCapacityStatisticMax = queryCapacityStatistic.Where(o => o.SentDate == queryCapacityStatistic
                .Where(u => u.SchoolID == o.SchoolID && (u.SubCommitteeID.HasValue ? u.SubCommitteeID : 0) == (o.SubCommitteeID.HasValue ? o.SubCommitteeID : 0))
                .Select(u => u.SentDate).Max());

            var query = (from s in SchoolProfileRepository.All
                         join p in SupervisingDeptBusiness.All on s.SupervisingDeptID equals p.SupervisingDeptID
                         join q in queryCapacityStatisticMax on s.SchoolProfileID equals q.SchoolID into g
                         from c in g.DefaultIfEmpty()
                         where s.IsActive == true && (p.SupervisingDeptID == SupervisingDeptID || p.ParentID == SupervisingDeptID)
                            && (provinceID > 0 ? s.ProvinceID == provinceID : true)
                            && (districtID > 0 ? s.DistrictID == districtID : true)
                            && (s.EducationGrade & grade) != 0
                            && (trainingTypeID > 0 ? s.TrainingTypeID == trainingTypeID : true)
                         group c by new
                         {
                             c.SchoolID,
                             s.SchoolName
                         } into gr
                         select new CapacityStatisticsBO
                         {
                             SchoolID = gr.Key.SchoolID,
                             SchoolName = gr.Key.SchoolName,
                             SentDate = gr.Max(o => o.SentDate),
                             TotalExcellent = gr.Sum(c => c.CapacityLevel01.HasValue ? c.CapacityLevel01.Value : 0),
                             TotalGood = gr.Sum(c => c.CapacityLevel02.HasValue ? c.CapacityLevel02.Value : 0),
                             TotalNormal = gr.Sum(c => c.CapacityLevel03.HasValue ? c.CapacityLevel03.Value : 0),
                             TotalWeak = gr.Sum(c => c.CapacityLevel04.HasValue ? c.CapacityLevel04.Value : 0),
                             TotalPoor = gr.Sum(c => c.CapacityLevel05.HasValue ? c.CapacityLevel05.Value : 0),
                             TotalPass = gr.Sum(c => c.CapacityLevel06.HasValue ? c.CapacityLevel06.Value : 0),
                             TotalFail = gr.Sum(c => c.CapacityLevel07.HasValue ? c.CapacityLevel07.Value : 0),
                             TotalPupil = gr.Sum(c => c.PupilTotal.HasValue ? c.PupilTotal.Value : 0),
                             TotalAboveAverage = gr.Sum(c => c.OnAverage.HasValue ? c.OnAverage.Value : 0)
                         });

            List<CapacityStatisticsBO> lstCapacityStatisticsOfProvince23 = query.ToList();
            lstCapacityStatisticsOfProvince23 = lstCapacityStatisticsOfProvince23.OrderBy(o => o.SchoolName).ToList();
            lstCapacityStatisticsOfProvince23.ForEach(u =>
            {
                u.PercentAboveAverage = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalAboveAverage.HasValue ? Math.Round((double)u.TotalAboveAverage * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentExcellent = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalExcellent.HasValue ? Math.Round((double)u.TotalExcellent * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentGood = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalGood.HasValue ? Math.Round((double)u.TotalGood * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentNormal = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalNormal.HasValue ? Math.Round((double)u.TotalNormal * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentWeak = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalWeak.HasValue ? Math.Round((double)u.TotalWeak * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentPoor = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalPoor.HasValue ? Math.Round((double)u.TotalPoor * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentPass = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalPass.HasValue ? Math.Round((double)u.TotalPass * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                u.PercentFail = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalFail.HasValue ? Math.Round((double)u.TotalFail * 100.0 / (double)u.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
            });
            lstCapacityStatisticsOfProvince23 = lstCapacityStatisticsOfProvince23.OrderBy(o => o.SchoolName).ToList();
            return lstCapacityStatisticsOfProvince23;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public List<CapacityStatisticsBO> SearchBySupervisingDept23(IDictionary<string, object> SearchInfo)
        {
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int? AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int grade = Utils.GradeToBinary(AppliedLevel.Value);
            if (SupervisingDeptID.HasValue && SupervisingDeptID > 0)
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
                if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
            }
            var query = CapacityStatisticRepository.AllNoTracking;
            query = query.Where(u => u.ReportCode == ReportCode && u.SentToSupervisor == SentToSupervisor);
            if (Year > 0)
            {
                query = query.Where(u => u.Year == Year);
            }
            if (Semester > 0)
            {
                query = query.Where(u => u.Semester == Semester);
            }
            if (EducationLevelID > 0)
            {
                query = query.Where(u => u.EducationLevelID == EducationLevelID);
            }
            if (SubCommitteeID > 0)
            {
                query = query.Where(u => u.SubCommitteeID == SubCommitteeID);
            }
            if (SubjectID > 0)
            {
                query = query.Where(u => u.SubjectID == SubjectID);
            }
            IQueryable<CapacityStatistic> queryMax = query.Where(o => o.SentDate == query.Where(u => u.SchoolID == o.SchoolID).Select(u => u.SentDate).Max());
            List<CapacityStatisticsBO> _lstCapacityStatisticsOfProvince23 =
                (from s in SchoolProfileRepository.All
                 join q in queryMax on s.SchoolProfileID equals q.SchoolID into g
                 from c in g.DefaultIfEmpty()
                 where s.IsActive == true
                                 && (ProvinceID > 0 ? s.ProvinceID == ProvinceID : true)
                                 && (SupervisingDeptID > 0 ? s.SupervisingDeptID == SupervisingDeptID : true)
                                 && (s.EducationGrade & grade) != 0
                                 && (TrainingTypeID > 0 ? s.TrainingTypeID == TrainingTypeID : true)                                 
                 select new CapacityStatisticsBO
                  {
                      SchoolName = s.SchoolName,
                      SchoolID = s.SchoolProfileID,
                      SentDate = c.SentDate,
                      TotalExcellent = c.CapacityLevel01.HasValue ? c.CapacityLevel01.Value : 0,
                      TotalGood = c.CapacityLevel02.HasValue ? c.CapacityLevel02.Value : 0,
                      TotalNormal = c.CapacityLevel03.HasValue ? c.CapacityLevel03.Value : 0,
                      TotalWeak = c.CapacityLevel04.HasValue ? c.CapacityLevel04.Value : 0,
                      TotalPoor = c.CapacityLevel05.HasValue ? c.CapacityLevel05.Value : 0,
                      TotalPass = c.CapacityLevel06.HasValue ? c.CapacityLevel06.Value : 0,
                      TotalFail = c.CapacityLevel07.HasValue ? c.CapacityLevel07.Value : 0,
                      TotalPupil = c.PupilTotal.HasValue ? c.PupilTotal.Value : 0,
                      TotalAboveAverage = c.OnAverage.HasValue ? c.OnAverage.Value : 0
                  }).OrderBy(o => o.SchoolName).ToList();
            if (_lstCapacityStatisticsOfProvince23 != null && _lstCapacityStatisticsOfProvince23.Count > 0)
            {
                foreach (var item in _lstCapacityStatisticsOfProvince23)
                {
                    if (item.TotalPupil.HasValue && item.TotalPupil.Value > 0)
                    {
                        if (item.TotalAboveAverage.HasValue && item.TotalAboveAverage.Value > 0)
                        {
                            item.PercentAboveAverage = Math.Round((double)item.TotalAboveAverage * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        }
                        item.PercentGood = Math.Round((double)item.TotalGood * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        item.PercentExcellent = Math.Round((double)item.TotalExcellent * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        item.PercentNormal = Math.Round((double)item.TotalNormal * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        item.PercentWeak = Math.Round((double)item.TotalWeak * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        item.PercentPoor = Math.Round((double)item.TotalPoor * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        item.PercentPass = Math.Round((double)item.TotalPass * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                        item.PercentFail = Math.Round((double)item.TotalFail * 100.0 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero);
                    }
                }
            }
            _lstCapacityStatisticsOfProvince23 = _lstCapacityStatisticsOfProvince23.OrderBy(o => o.SchoolName).ToList();
            return _lstCapacityStatisticsOfProvince23;
        }

        /// <summary>
        /// minhh
        /// 4/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="InputParameterHashkey"></param>
        /// <returns></returns>
        public List<CapacityStatisticsBO> CreatePGDCapacityStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            string reportCodeToSearch = SystemParamsInFile.REPORT_THONG_KE_HOC_LUC_PHONG;
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int? AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            FileID = 0;


            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");


            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceSecondary = this.SearchBySupervisingDept23(SearchInfo);
            if (lstCapacityStatisticOfProvinceSecondary != null)
            {


                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCodeToSearch);

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet 
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet, "P10");

                //fill dữ liệu vào sheet thông tin chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

                string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string districtName = districtID > 0 ? DistrictBusiness.Find(districtID).DistrictName : "[Tất cả]";
                string provinceName = supervisingDept.Province.ProvinceName;
                string trainingName = trainningTypeID > 0 ? TrainingTypeBusiness.Find(trainningTypeID).Resolution : "[Tất cả]";
                string educationName = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "Khối [Tất cả]";
                string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                string provinceAndDate = provinceName + ", " + dateTime;
                string semesterName = semester > 0 ? ReportUtils.ConvertSemesterForReportName(semester) : string.Empty;
                string semesterAndAcademic = districtName + " - Hệ " + trainingName + " - " + educationName + " - " + ReportUtils.ConvertSemesterForDisplayTitle(semester) + " - Năm học " + year + " - " + (year + 1);

                sheet.SetCellValue("A2", supervisingDeptName);

                sheet.SetCellValue("I4", provinceAndDate);
                sheet.SetCellValue("B7", semesterAndAcademic);

                int startRow = 10;

                IVTRange range = tempSheet.GetRange("A10", "P10");
                IVTRange lastRange = tempSheet.GetRange("A198", "P198");
                IVTRange rangeTC = tempSheet.GetRange("A200", "P200");
               
                for (int i = 0; i < lstCapacityStatisticOfProvinceSecondary.Count; i++)
                {
                    CapacityStatisticsBO capBO = lstCapacityStatisticOfProvinceSecondary[i];
                    capBO.TotalPupil = capBO.TotalPupil.HasValue ? capBO.TotalPupil.Value : 0;
                    capBO.TotalExcellent = capBO.TotalExcellent.HasValue ? capBO.TotalExcellent.Value : 0;
                    capBO.TotalGood = capBO.TotalGood.HasValue ? capBO.TotalGood.Value : 0;
                    capBO.TotalNormal = capBO.TotalNormal.HasValue ? capBO.TotalNormal.Value : 0;
                    capBO.TotalWeak = capBO.TotalWeak.HasValue ? capBO.TotalWeak.Value : 0;
                    capBO.TotalPoor = capBO.TotalPoor.HasValue ? capBO.TotalPoor.Value : 0;
                    capBO.TotalAboveAverage = capBO.TotalAboveAverage.HasValue ? capBO.TotalAboveAverage.Value : 0;

                    if (i == lstCapacityStatisticOfProvinceSecondary.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                    }
                    else
                    {
                        //Copy row style
                        if (startRow + i > 10)
                        {
                            if ((i + 1) % 5 == 0)
                            {
                                sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                            }
                            else
                                sheet.CopyPasteSameRowHeigh(range, startRow);
                        }
                    }

                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ten truong
                    sheet.SetCellValue(startRow, 2, capBO.SchoolName);

                    //Ngay gui
                    sheet.SetCellValue(startRow, 3, capBO.SentDate.HasValue ? capBO.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);

                    //Tong so HS
                    sheet.SetCellValue(startRow, 4, capBO.TotalPupil);

                    //Gioi
                    sheet.SetCellValue(startRow, 5, capBO.TotalExcellent);
                    //sheet.SetCellValue(startRow, 6, Math.Round(capBO.PercentExcellent.Value, 2, MidpointRounding.AwayFromZero));


                    //Kha
                    sheet.SetCellValue(startRow, 7, capBO.TotalGood);
                    //sheet.SetCellValue(startRow, 8, Math.Round(capBO.PercentGood.Value, 2, MidpointRounding.AwayFromZero));

                    //TB
                    sheet.SetCellValue(startRow, 9, capBO.TotalNormal);
                    //sheet.SetCellValue(startRow, 10, Math.Round(capBO.PercentNormal.Value, 2, MidpointRounding.AwayFromZero));

                    //Yeu
                    sheet.SetCellValue(startRow, 11, capBO.TotalWeak);
                    //sheet.SetCellValue(startRow, 12, Math.Round(capBO.PercentWeak.Value, 2, MidpointRounding.AwayFromZero));

                    //Kem
                    sheet.SetCellValue(startRow, 13, capBO.TotalPoor);
                    //sheet.SetCellValue(startRow, 14, Math.Round(capBO.PercentPoor.Value, 2, MidpointRounding.AwayFromZero));

                    //TB tro len
                    sheet.SetCellValue(startRow, 15, capBO.TotalAboveAverage);
                    //sheet.SetCellValue(startRow, 16, Math.Round(capBO.PercentAboveAverage.Value, 2, MidpointRounding.AwayFromZero));

                    startRow++;
                }
               
                sheet.CopyPasteSameRowHeigh(rangeTC, startRow);
                sheet.SetCellValue(startRow, 1, "Tổng cộng");
                sheet.MergeRow(startRow, 1, 3);
                string alphabetTS = VTVector.ColumnIntToString(4);
                string fomularTS = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                sheet.SetFormulaValue(startRow, 4, fomularTS.Replace("#", alphabetTS));

                for (int i = 1; i <= 11; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 4);
                    string fomular = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                    sheet.SetFormulaValue(startRow, i + 4, fomular.Replace("#", alphabet));
                }
                tempSheet.Delete();

                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCodeToSearch;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashkey;
                pr.ReportData = ReportUtils.Compress(oBook.ToStream());
                sentToSupervisor = true;
                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;   
                string khoi = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "TatCa";

                outputNamePattern = outputNamePattern.Replace("[Semester]", semesterName);
                outputNamePattern = outputNamePattern.Replace("[EducationLevel]", khoi);
                outputNamePattern = outputNamePattern.Replace("SGD", "PGD");
                outputNamePattern = outputNamePattern.Replace("[TrainingType]", trainningTypeID > 0 ? TrainingTypeBusiness.Find(trainningTypeID).Resolution : "TatCa");
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", semester},
                {"Year", year},
                {"EducationLevelID", educationLevelID},
                {"TrainingTypeID", trainningTypeID},
                {"DistrictID ", districtID },
                {"ProvinceID", provinceID},
                 {"SupervisingDeptID", SupervisingDeptID},
                {"SentToSupervisor", true}
            };
                ProcessedReportParameterRepository.Insert(dic, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
            }
            return lstCapacityStatisticOfProvinceSecondary;
        }


        #region SearchByProvinceGroupByLevel23

        /// <summary>
        /// Tìm kiếm thống kê học lực/học lực môn cấp 2 với account Sở.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public List<CapacityStatisticsBO> SearchByProvinceGroupByLevel23(IDictionary<string, object> SearchInfo)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            if (provinceID == 0) return null;
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int appliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            int grade = Utils.GradeToBinary(appliedLevel);
            if (supervisingDeptID.HasValue && supervisingDeptID > 0)
            {
                SupervisingDept sdept = SupervisingDeptBusiness.Find(supervisingDeptID);
                if (sdept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || sdept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = sdept.ParentID.Value;
                }
            }
            List<SchoolProfile> lstSchool = new List<SchoolProfile>();
            if (districtID == 0) lstSchool = SchoolProfileRepository.All.Where(p => p.ProvinceID == provinceID).ToList<SchoolProfile>();
            else
            {
                lstSchool = SchoolProfileRepository.All.Where(p => p.ProvinceID == provinceID).ToList<SchoolProfile>();
            }
            List<CapacityStatistic> lstCapacityStatistic = new List<CapacityStatistic>().ToList();
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_SECONDARY).ToList();
            IQueryable<CapacityStatistic> lstCapacityStatisticQuery = CapacityStatisticRepository.All.Where(p => p.ReportCode == reportCode && p.Year == year && p.Semester == semester && p.SentToSupervisor == true && p.AppliedLevel == appliedLevel);
            lstCapacityStatistic = (from c in lstCapacityStatisticQuery
                                    join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                    join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                    where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                                    && (c.ProcessedDate == (from c1 in lstCapacityStatisticQuery where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID select c1.ProcessedDate).Max())
                                    select c).ToList<CapacityStatistic>();
            List<CapacityStatisticsBO> lstCapacityStatisticsOfProvince23 = new List<CapacityStatisticsBO>();
            var query = from j in
                            (from s in lstEducationLevel
                             join c in lstCapacityStatistic on s.EducationLevelID equals c.EducationLevelID into g
                             from jn in g.DefaultIfEmpty()
                             select new
                             {
                                 EducationLevelID = s.EducationLevelID,
                                 EducationLevelName = s.Resolution,
                                 CapacityLevel01 = jn == null ? 0 : jn.CapacityLevel01,
                                 CapacityLevel02 = jn == null ? 0 : jn.CapacityLevel02,
                                 CapacityLevel03 = jn == null ? 0 : jn.CapacityLevel03,
                                 CapacityLevel04 = jn == null ? 0 : jn.CapacityLevel04,
                                 CapacityLevel05 = jn == null ? 0 : jn.CapacityLevel05,
                                 CapacityLevel06 = jn == null ? 0 : jn.CapacityLevel06,
                                 CapacityLevel07 = jn == null ? 0 : jn.CapacityLevel07,
                                 PupilTotal = jn == null ? 0 : jn.PupilTotal
                             })
                        group j by new { j.EducationLevelID, j.EducationLevelName } into g2
                        select new CapacityStatisticsBO
                        {
                            EducationLevelName = g2.Key.EducationLevelName,
                            TotalExcellent = g2.Sum(u => u.CapacityLevel01),
                            TotalGood = g2.Sum(u => u.CapacityLevel02),
                            TotalNormal = g2.Sum(u => u.CapacityLevel03),
                            TotalWeak = g2.Sum(u => u.CapacityLevel04),
                            TotalPoor = g2.Sum(u => u.CapacityLevel05),
                            TotalPass = g2.Sum(u => u.CapacityLevel06),
                            TotalFail = g2.Sum(u => u.CapacityLevel07),
                            TotalPupil = g2.Sum(u => u.PupilTotal),
                            PercentExcellent = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel01) / (double)g2.Sum(u => u.PupilTotal),
                            PercentGood = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel02) / (double)g2.Sum(u => u.PupilTotal),
                            PercentNormal = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel03) / (double)g2.Sum(u => u.PupilTotal),
                            PercentWeak = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel04) / (double)g2.Sum(u => u.PupilTotal),
                            PercentPoor = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel05) / (double)g2.Sum(u => u.PupilTotal),
                            PercentPass = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel06) / (double)g2.Sum(u => u.PupilTotal),
                            PercentFail = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel07) / (double)g2.Sum(u => u.PupilTotal),
                            TotalAboveAverage = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06),
                            PercentAboveAverage = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * (g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06)) / (double)g2.Sum(u => u.PupilTotal)
                        };
            lstCapacityStatisticsOfProvince23 = query.ToList();

            return lstCapacityStatisticsOfProvince23;

        }

        #endregion


        #region SearchBySupervisingDeptGroupByLevel23

        /// <summary>
        /// Tìm kiếm thống kê học lực/học lực  cấp 2, 3 - Phòng
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public List<CapacityStatisticsBO> SearchBySupervisingDeptGroupByLevel23(IDictionary<string, object> SearchInfo)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int appliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            int grade = Utils.GradeToBinary(appliedLevel);
            if (SupervisingDeptID.HasValue && SupervisingDeptID > 0)
            {
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = supervisingDept.ParentID.Value;
                }
            }
            //List<SchoolProfile> lstSchool = new List<SchoolProfile>();
            //if (districtID == 0) lstSchool = SchoolProfileRepository.All.Where(p => p.SupervisingDeptID == SupervisingDeptID).ToList<SchoolProfile>();
            if (provinceID == 0) return null;
            List<CapacityStatistic> lstCapacityStatistic = new List<CapacityStatistic>();
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_SECONDARY).ToList();
            IQueryable<CapacityStatistic> lstCapacityStatisticQuery = CapacityStatisticRepository.All.Where(p => p.ReportCode == reportCode && p.Year == year && p.Semester == semester && p.SentToSupervisor == true && p.AppliedLevel == appliedLevel);
            lstCapacityStatistic = (from c in lstCapacityStatisticQuery
                                    join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                    join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                    where s.ProvinceID == provinceID && (sd.SupervisingDeptID == SupervisingDeptID || sd.ParentID == SupervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                                    && (c.ProcessedDate == (from c1 in lstCapacityStatisticQuery where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID select c1.ProcessedDate).Max())
                                    select c).ToList<CapacityStatistic>();

            //lstCapacityStatistic = (from c in CapacityStatisticRepository.All.ToList()
            //                        join s in SchoolProfileRepository.All.ToList() on c.SchoolID equals s.SchoolProfileID
            //                        where (s.DistrictID == districtID || districtID == 0)
            //                        && c.ReportCode == reportCode
            //                        && c.Year == year
            //                        && c.Semester == semester
            //                        && c.SentToSupervisor == sentToSupervisor
            //                        && (c.SentDate == (from c1 in CapacityStatisticRepository.All.ToList() where c.SchoolID == c1.SchoolID select c1.SentDate).Max())
            //                        select c).ToList<CapacityStatistic>();
            List<CapacityStatisticsBO> lstCapacityStatisticsOfProvince23;
            var query = from j in
                            (from s in lstEducationLevel
                             join c in lstCapacityStatistic on s.EducationLevelID equals c.EducationLevelID into g
                             from jn in g.DefaultIfEmpty()
                             select new
                             {
                                 EducationLevelID = s.EducationLevelID,
                                 EducationLevelName = s.Resolution,
                                 CapacityLevel01 = jn == null ? 0 : jn.CapacityLevel01,
                                 CapacityLevel02 = jn == null ? 0 : jn.CapacityLevel02,
                                 CapacityLevel03 = jn == null ? 0 : jn.CapacityLevel03,
                                 CapacityLevel04 = jn == null ? 0 : jn.CapacityLevel04,
                                 CapacityLevel05 = jn == null ? 0 : jn.CapacityLevel05,
                                 CapacityLevel06 = jn == null ? 0 : jn.CapacityLevel06,
                                 CapacityLevel07 = jn == null ? 0 : jn.CapacityLevel07,
                                 PupilTotal = jn == null ? 0 : jn.PupilTotal

                             })
                        group j by new { j.EducationLevelID, j.EducationLevelName } into g2
                        select new CapacityStatisticsBO
                        {
                            EducationLevelName = g2.Key.EducationLevelName,
                            TotalExcellent = g2.Sum(u => u.CapacityLevel01),
                            TotalGood = g2.Sum(u => u.CapacityLevel02),
                            TotalNormal = g2.Sum(u => u.CapacityLevel03),
                            TotalWeak = g2.Sum(u => u.CapacityLevel04),
                            TotalPoor = g2.Sum(u => u.CapacityLevel05),
                            TotalPass = g2.Sum(u => u.CapacityLevel06),
                            TotalFail = g2.Sum(u => u.CapacityLevel07),
                            TotalPupil = g2.Sum(u => u.PupilTotal),
                            PercentExcellent = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel01) / (double)g2.Sum(u => u.PupilTotal),
                            PercentGood = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel02) / (double)g2.Sum(u => u.PupilTotal),
                            PercentNormal = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel03) / (double)g2.Sum(u => u.PupilTotal),
                            PercentWeak = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel04) / (double)g2.Sum(u => u.PupilTotal),
                            PercentPoor = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel05) / (double)g2.Sum(u => u.PupilTotal),
                            PercentPass = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel06) / (double)g2.Sum(u => u.PupilTotal),
                            PercentFail = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel07) / (double)g2.Sum(u => u.PupilTotal),
                            TotalAboveAverage = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06),
                            PercentAboveAverage = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * (g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06)) / (double)g2.Sum(u => u.PupilTotal)


                        };
            lstCapacityStatisticsOfProvince23 = query.ToList();
            return lstCapacityStatisticsOfProvince23;

        }
        #endregion


        #region SearchByProvinceGroupByDistrict23

        /// <summary>
        /// Tìm kiếm TK HL theo quận/huyện
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public List<CapacityStatisticsBO> SearchByProvinceGroupByDistrict23(IDictionary<string, object> SearchInfo)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");

            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSuperVisingDeptRole");
            int grade = Utils.GradeToBinary(appliedLevel);
            if (supervisingDeptID.HasValue && supervisingDeptID > 0)
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
                if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = su.ParentID.Value;
                }
            }
            string reportCode = "";
            if (appliedLevel == 2)
            {
                reportCode = SystemParamsInFile.THONGKEHOCLUCCAP2;
            }
            else if (appliedLevel == 3)
            {
                reportCode = SystemParamsInFile.THONGKEHOCLUCTHEOBANCAP3;
            }
            List<CapacityStatistic> lstCapacityStatistic = new List<CapacityStatistic>();
            //List<District> lstDistrict = (from p in DistrictBusiness.All
            //                                 join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
            //                                 join s  in SupervisingDeptBusiness.All on q.ProvinceID equals s.ProvinceID
            //                                 where p.IsActive == true && (s.SupervisingDeptID == supervisingDeptID || s.ParentID == supervisingDeptID)
            //                                 select p).ToList();
            var lstDistrict = (from p in DistrictBusiness.All
                               join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                               join s in SupervisingDeptBusiness.All on q.ProvinceID equals s.ProvinceID
                               where p.IsActive == true && (s.SupervisingDeptID == supervisingDeptID || s.ParentID == supervisingDeptID)
                               select new
                               {
                                   DistrictID = p.DistrictID,
                                   DistrictName = p.DistrictName,
                                   ProvinceID = q.ProvinceID
                               }).Distinct().ToList();
            lstDistrict = lstDistrict.OrderBy(o => o.DistrictName).ToList();

            IQueryable<CapacityStatistic> lstCapacityStatisticReposity = CapacityStatisticRepository.All.Where(p => p.ReportCode == reportCode && p.Year == year && p.Semester == semester && p.SentToSupervisor == true && p.AppliedLevel == appliedLevel);
            if (appliedLevel == 3)
            {
                lstCapacityStatistic = (from c in lstCapacityStatisticReposity
                                        join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                        join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                        where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                                        && (c.ProcessedDate == (from c1 in lstCapacityStatisticReposity where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID && c.SubCommitteeID == c1.SubCommitteeID select c1.ProcessedDate).Max())
                                        select c).ToList<CapacityStatistic>();
            }
            else
            {
                lstCapacityStatistic = (from c in lstCapacityStatisticReposity
                                        join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                        join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                        where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                                        && (c.ProcessedDate == (from c1 in lstCapacityStatisticReposity where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID select c1.ProcessedDate).Max())
                                        select c).ToList<CapacityStatistic>();
            }

            //if (lstCapacityStatistic.Count() == 0)
            //{
            //    return null;
            //}
            //else
            //{
            List<CapacityStatisticsBO> lstCapacityStatisticsOfProvince23;
            var query = from j in
                            (from s in lstDistrict
                             join c in lstCapacityStatistic on s.DistrictID equals c.SchoolProfile.DistrictID into g
                             from jn in g.DefaultIfEmpty()
                             select new
                             {
                                 DistrictID = s.DistrictID,
                                 DistrictName = s.DistrictName,
                                 CapacityLevel01 = jn == null ? 0 : jn.CapacityLevel01,
                                 CapacityLevel02 = jn == null ? 0 : jn.CapacityLevel02,
                                 CapacityLevel03 = jn == null ? 0 : jn.CapacityLevel03,
                                 CapacityLevel04 = jn == null ? 0 : jn.CapacityLevel04,
                                 CapacityLevel05 = jn == null ? 0 : jn.CapacityLevel05,
                                 CapacityLevel06 = jn == null ? 0 : jn.CapacityLevel06,
                                 CapacityLevel07 = jn == null ? 0 : jn.CapacityLevel07,
                                 PupilTotal = jn == null ? 0 : jn.PupilTotal

                             })
                        group j by new { j.DistrictID, j.DistrictName } into g2
                        select new CapacityStatisticsBO
                        {
                            DistrictName = g2.Key.DistrictName,
                            TotalExcellent = g2.Sum(u => u.CapacityLevel01),
                            TotalGood = g2.Sum(u => u.CapacityLevel02),
                            TotalNormal = g2.Sum(u => u.CapacityLevel03),
                            TotalWeak = g2.Sum(u => u.CapacityLevel04),
                            TotalPoor = g2.Sum(u => u.CapacityLevel05),
                            TotalPass = g2.Sum(u => u.CapacityLevel06),
                            TotalFail = g2.Sum(u => u.CapacityLevel07),

                            TotalPupil = g2.Sum(u => u.PupilTotal),
                            PercentExcellent = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel01) / (double)g2.Sum(u => u.PupilTotal),
                            PercentGood = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel02) / (double)g2.Sum(u => u.PupilTotal),
                            PercentNormal = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel03) / (double)g2.Sum(u => u.PupilTotal),
                            PercentWeak = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel04) / (double)g2.Sum(u => u.PupilTotal),
                            PercentPoor = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel05) / (double)g2.Sum(u => u.PupilTotal),
                            PercentPass = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel06) / (double)g2.Sum(u => u.PupilTotal),
                            PercentFail = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * g2.Sum(u => u.CapacityLevel07) / (double)g2.Sum(u => u.PupilTotal),
                            TotalAboveAverage = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06),
                            PercentAboveAverage = g2.Sum(u => u.PupilTotal) == 0 ? 0 : 100 * (g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06)) / (double)g2.Sum(u => u.PupilTotal)






                            //TotalPupil = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07),
                            //PercentExcellent = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel01) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //PercentGood = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel02) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //PercentNormal = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel03) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //PercentWeak = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel04) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //PercentPoor = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel05) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //PercentPass = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel06) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //PercentFail = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : g2.Sum(u => u.CapacityLevel07) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                  + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100,
                            //TotalAboveAverage = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06),

                            //PercentAboveAverage = g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //             + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07) == 0 ?
                            //             0 : (g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel06)) / (double)(g2.Sum(u => u.CapacityLevel01) + g2.Sum(u => u.CapacityLevel02) + g2.Sum(u => u.CapacityLevel03) + g2.Sum(u => u.CapacityLevel04)
                            //                   + g2.Sum(u => u.CapacityLevel05) + g2.Sum(u => u.CapacityLevel06) + g2.Sum(u => u.CapacityLevel07)) * 100

                        };
            lstCapacityStatisticsOfProvince23 = query.ToList();
            return lstCapacityStatisticsOfProvince23;
            //}

        }

        #endregion


        #region CreateSGDCapacityStatisticsByLevelSecondary
        /// <summary>
        /// Tạo thống kê học lực theo khối cấp 2 – Sở
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public List<CapacityStatisticsBO> CreateSGDCapacityStatisticsByLevelSecondary(string InputParameterHashKey, IDictionary<string, object> SearchInfo, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            string ReportCode = SystemParamsInFile.REPORT_SGD_THCS_THONGKEHOCLUCTHEOKHOI;
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");

            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceByLevelSecondary = this.SearchByProvinceGroupByLevel23(SearchInfo);
            FileID = 0;
            //if (lstCapacityStatisticOfProvinceByLevelSecondary == null) return null;
            //else
            //{

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string AcademicYear = this.AcademicYearBusiness.All.Where(o => o.Year == Year && o.IsActive == true).FirstOrDefault().DisplayTitle;
            string Suppervising = this.SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
            string Province = this.ProvinceBusiness.Find(ProvinceID).ProvinceName;
            string SemesterName = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange range = firstSheet.GetRange("A11", "N11");
            IVTRange rangeTotal = firstSheet.GetRange("A12", "N12");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            //Fill dữ liệu chung
            sheet.Name = "TKHLTHEOKHOI";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A7", SemesterName + " - NĂM HỌC " + AcademicYear);
            sheet.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            //int i = 0;
            int startrow = 11;
            int beginRow = 11;

            List<object> list = new List<object>();
            Dictionary<string, object> dicParam = new Dictionary<string, object>();
            foreach (var item in lstCapacityStatisticOfProvinceByLevelSecondary)
            {

                sheet.CopyPasteSameRowHeigh(range, startrow);
                list.Add(new Dictionary<string, object> 
                    {                      
                        {"EducationLevelName", item.EducationLevelName},
                        {"TotalPupil", item.TotalPupil},
                        {"TotalGood", item.TotalExcellent},
                        {"TotalFair", item.TotalGood},
                        {"TotalNormal", item.TotalNormal},
                        {"TotalWeak", item.TotalWeak},
                         {"TotalPoor", item.TotalPoor}                                        
                    });
                startrow++;
            }

            dicParam.Add("list", list);
            sheet.FillVariableValue(dicParam);

            int endRow = startrow;
            sheet.CopyPasteSameRowHeigh(rangeTotal, endRow);

            string alphabetTS = VTVector.ColumnIntToString(2);
            string fomularTS = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
            sheet.SetFormulaValue(endRow, 2, fomularTS.Replace("#", alphabetTS));

            for (int i = 0; i < 9; i += 2)
            {
                string alphabet = VTVector.ColumnIntToString(i + 3);
                string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
            }
            //Xoá sheet template
            firstSheet.Delete();

            Stream Data = oBook.ToStream();
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = ReportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;


            //Tạo tên file SGD_THCS_ThongKeHocLucTheoKhoi_[EducationLevel]_[Semester]
            string outputNamePattern = reportDef.OutputNamePattern;

            string semester = ReportUtils.ConvertSemesterForReportName(Semester);

            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", "");
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID", EducationLevelID},
                {"DistrictID", 0},
                {"ProvinceID", ProvinceID},
                {"SupervisingDeptID", SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            //}

            return lstCapacityStatisticOfProvinceByLevelSecondary;
        }

        #endregion

        #region CreatePGDCapacityStatisticsByLevelSecondary
        /// <summary>
        /// Tạo thống kê học lực cấp 2 - Phòng
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public List<CapacityStatisticsBO> CreatePGDCapacityStatisticsByLevelSecondary(string InputParameterHashKey, IDictionary<string, object> SearchInfo, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            string ReportCode = SystemParamsInFile.REPORT_PGD_THCS_THONGKEHOCLUCTHEOKHOI;
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");

            List<CapacityStatisticsBO> lstCapacityStatisticsOfSchoolByLevelSecondary = this.SearchBySupervisingDeptGroupByLevel23(SearchInfo);
            FileID = 0;
            if (lstCapacityStatisticsOfSchoolByLevelSecondary == null) return null;
            else
            {

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
                string AcademicYear = this.AcademicYearBusiness.All.Where(o => o.Year == Year && o.IsActive == true).FirstOrDefault().DisplayTitle;
                string Suppervising = this.SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string Province = this.ProvinceBusiness.Find(ProvinceID).ProvinceName;
                string SemesterName = "";
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    SemesterName = "HỌC KỲ I";

                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    SemesterName = "HỌC KỲ II";
                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    SemesterName = "CẢ NĂM";
                }

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTRange range = firstSheet.GetRange("A11", "N11");
                IVTRange rangeTotal = firstSheet.GetRange("A12", "N12");

                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                //Fill dữ liệu chung
                sheet.Name = "TKHLTHEOKHOI";
                sheet.SetCellValue("A2", Suppervising);
                sheet.SetCellValue("A7", SemesterName + " - NĂM HỌC " + AcademicYear);
                sheet.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

                //int i = 0;
                int startrow = 11;
                int beginRow = 11;

                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                List<object> list = new List<object>();
                foreach (var item in lstCapacityStatisticsOfSchoolByLevelSecondary)
                {

                    sheet.CopyPasteSameRowHeigh(range, startrow);
                    list.Add(new Dictionary<string, object> 
                    {                      
                        {"EducationLevelName", item.EducationLevelName},
                        {"TotalPupil", item.TotalPupil},
                        {"TotalGood", item.TotalExcellent},
                        {"TotalFair", item.TotalGood},
                        {"TotalNormal", item.TotalNormal},
                        {"TotalWeak", item.TotalWeak},
                         {"TotalPoor", item.TotalPoor}                                        
                    });
                    startrow++;
                }


                dicParam.Add("list", list);
                sheet.FillVariableValue(dicParam);

                int endRow = startrow;
                sheet.CopyPasteSameRowHeigh(rangeTotal, endRow);
                string alphabetTS = VTVector.ColumnIntToString(2);
                string fomularTS = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 2, fomularTS.Replace("#", alphabetTS));

                for (int i = 0; i < 7; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
                }
                //Xoá sheet template
                firstSheet.Delete();

                Stream Data = oBook.ToStream();
                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = ReportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashKey;
                pr.ReportData = ReportUtils.Compress(Data);
                pr.SentToSupervisor = true;


                //Tạo tên file SGD_THCS_ThongKeHocLucTheoKhoi_[EducationLevel]_[Semester]
                string outputNamePattern = reportDef.OutputNamePattern;
                string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                string Resolution = "";
                if (EducationLevelID != 0)
                {
                    Resolution = this.EducationLevelBusiness.Find(EducationLevelID).Resolution;
                }
                else
                {
                    Resolution = "Tất Cả";
                }

                outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(Resolution));
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID", EducationLevelID},
                {"DistrictID", 0},
                {"ProvinceID", ProvinceID},
                {"SupervisingDeptID", SupervisingDeptID}
            };
                ProcessedReportParameterRepository.Insert(dic, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
            }

            return lstCapacityStatisticsOfSchoolByLevelSecondary;
        }

        #endregion

        #region CreateSGDCapacityStatisticsByDistrictSecondary
        /// <summary>
        /// Tạo thống kê học lực theo Quận/Huyện cấp 2– Sở
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public List<CapacityStatisticsBO> CreateSGDCapacityStatisticsByDistrictSecondary(string InputParameterHashKey, IDictionary<string, object> SearchInfo, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            string ReportCode = SystemParamsInFile.REPORT_SGD_THCS_THONGKEHOCLUCTHEOQUANHUYEN;
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceByDistrictSecondary = this.SearchByProvinceGroupByDistrict23(SearchInfo);

            if (lstCapacityStatisticOfProvinceByDistrictSecondary == null)
            {
                FileID = 0;
                return null;

            }
            else
            {

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
                string AcademicYear = this.AcademicYearBusiness.All.Where(o => o.Year == Year && o.IsActive == true).FirstOrDefault().DisplayTitle;
                string Suppervising = this.SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string Province = this.ProvinceBusiness.Find(ProvinceID).ProvinceName;
                string SemesterName = "";
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    SemesterName = "HỌC KỲ I";

                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    SemesterName = "HỌC KỲ II";
                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    SemesterName = "CẢ NĂM";
                }

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTRange range = firstSheet.GetRange("A13", "O13");
                IVTRange rangeLast = firstSheet.GetRange("A14", "O14");
                IVTRange rangeSum = firstSheet.GetRange("A15", "O15");

                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                //Fill dữ liệu chung
                sheet.Name = "TKHLTHEOQUANHUYEN";
                sheet.SetCellValue("A2", Suppervising);
                sheet.SetCellValue("A8", SemesterName + " - NĂM HỌC " + AcademicYear);
                sheet.SetCellValue("J4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

                int i = 0;
                int startrow = 12;
                int beginRow = 12;

                List<object> list = new List<object>();
                Dictionary<string, object> dicParam = new Dictionary<string, object>();
                foreach (var item in lstCapacityStatisticOfProvinceByDistrictSecondary)
                {
                    i++;

                    if (i % 5 == 0 || lstCapacityStatisticOfProvinceByDistrictSecondary.Count() == i)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeLast, startrow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(range, startrow);
                    }

                    list.Add(new Dictionary<string, object> {
                    { "Order", i},
                    {  "DistrictName", item.DistrictName },
                    {"TotalPupil", item.TotalPupil},
                    {  "TotalGood", item.TotalExcellent},
                    {  "TotalFair", item.TotalGood },
                    {  "TotalNormal", item.TotalNormal },
                    {  "TotalWeak", item.TotalWeak },
                    {  "TotalPoor", item.TotalPoor }
                   
                });
                    startrow++;
                }

                dicParam.Add("list", list);
                sheet.FillVariableValue(dicParam);
                int endRow = startrow;
                sheet.CopyPasteSameRowHeigh(rangeSum, endRow);
                string alphabetTS = VTVector.ColumnIntToString(3);
                string fomularTS = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 3, fomularTS.Replace("#", alphabetTS));
                for (int j = 1; j < 12; j += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(j + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, j + 3, fomular.Replace("#", alphabet));
                }

                //Xoá sheet template
                firstSheet.Delete();

                Stream Data = oBook.ToStream();
                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = ReportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashKey;
                pr.ReportData = ReportUtils.Compress(Data);
                pr.SentToSupervisor = true;


                //Tạo tên file SGD_THCS_ThongKeHocLucTheoQuanHuyen_[EducationLevel]_[Semester]
                string outputNamePattern = reportDef.OutputNamePattern;
                string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                //string Resolution = "";
                //if (EducationLevelID != 0)
                //{
                //    Resolution = this.EducationLevelBusiness.Find(EducationLevelID).Resolution;
                //}
                //else
                //{
                //    Resolution = "Tất Cả";
                //}

                //outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                //outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(Resolution));
                //pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
                pr.ReportName = ReportCode + "_" + semester + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                    {"Year", Year},
                    {"Semester", Semester},
                    {"EducationLevelID", EducationLevelID},
                    {"DistrictID", 0},
                    {"ProvinceID", ProvinceID},
                    {"SupervisingDeptID", SupervisingDeptID}
                };
                ProcessedReportParameterRepository.Insert(dic, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
                return lstCapacityStatisticOfProvinceByDistrictSecondary;
            }


        }

        #endregion

        //public IQueryable<CapacityStatisticsBO> SearchByProvinceGroupBySubCommittee23(IDictionary<string, object> SearchInfo)
        //{
        //    int? Year = Utils.GetInt(SearchInfo, "Year");
        //    int? Semester = Utils.GetInt(SearchInfo, "Semester");
        //    int? EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
        //    int? TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
        //    int? SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
        //    string ReportCode = Utils.GetString(SearchInfo, "Year");
        //    bool? SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
        //    int? ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
        //    int? DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
        //    int? SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");

        //    IDictionary<string, object> dic = new Dictionary<string, object>();
        //    dic["DistrictID"] = DistrictID;
        //    dic["ProvinceID"] = ProvinceID;

        //    IEnumerable<SchoolProfile> listSchoolProfile = SchoolProfileBusiness.Search(SearchInfo);
        //    if (!DistrictID.HasValue && !ProvinceID.HasValue)
        //    {
        //        listSchoolProfile = null;
        //    }

        //    var querry = from Cs in this.All
        //                 join sp in listSchoolProfile on Cs.SchoolID equals sp.SchoolProfileID
        //                 select new { sp, Cs };

        //    if (ReportCode!=null)
        //        querry = querry.Where(o=>o.Cs.ReportCode == ReportCode);
        //    if (Year.HasValue)
        //        querry = querry.Where(o => o.Cs.Year == Year.Value);
        //    if (Semester.HasValue)
        //        querry = querry.Where(o => o.Cs.Semester == Semester.Value);
        //    if (SentToSupervisor.HasValue)
        //        querry = querry.Where(o=> o.Cs.SentToSupervisor == SentToSupervisor.Value);
        //    if (EducationLevelID.HasValue)
        //        querry = querry.Where(o => o.Cs.EducationLevelID == EducationLevelID.Value);
        //    if (SubCommitteeID.HasValue)
        //        querry = querry.Where(o => o.Cs.SubCommitteeID == SubCommitteeID.Value);
        //    if (SubjectID.HasValue)
        //        querry = querry.Where(o => o.Cs.SubjectID == SubjectID.Value);
        //    if (TrainingTypeID.HasValue)
        //        querry = querry.Where(o => o.sp.TrainingTypeID == TrainingTypeID.Value);


        //    List< CapacityStatisticsBO> 
        //}

        /// <summary>
        /// Thong ke hoc luc cac truong - SGD
        /// </summary>
        /// <author>hieund</author>
        /// <param name="dic"></param>
        /// <param name="InputParameterHashKey"></param>
        /// <param name="processedReportID"></param>
        /// <returns></returns>
        public List<CapacityStatisticsBO> CreateSGDCapacityStatisticsTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int processedReportID)
        {
            #region khoi tao
            processedReportID = 0;
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int SubcommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceTertiary = this.SearchByProvince23(dic).ToList();

            if (lstCapacityStatisticOfProvinceTertiary == null)
                return null;


            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUC);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet templateSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet, "P14");
            sheet.Name = "THPT_TKHL";
            #endregion

            #region tao khung
            IVTRange sumTempRow = templateSheet.GetRange("A40", "P40");
            int sumRowIndex = 0;
            int countlstCapicity = lstCapacityStatisticOfProvinceTertiary.Count;
            if (countlstCapicity > 5)
            {
                IVTRange topTempRow = templateSheet.GetRange("A16", "P16");
                IVTRange midTempRow = templateSheet.GetRange("A17", "P17");
                IVTRange botTempRow = templateSheet.GetRange("A18", "P18");
                for (int i = 0; i < countlstCapicity - 5; i++)
                {
                    if (i % 5 == 0)
                        sheet.CopyPaste(topTempRow, i + 15, 1);
                    else
                        if (i % 5 == 4)
                            sheet.CopyPaste(botTempRow, i + 15, 1);
                        else
                            sheet.CopyPaste(midTempRow, i + 15, 1);
                }
                sumRowIndex = countlstCapicity + 10;

            }
            else
            {
                sumRowIndex = 10 + countlstCapicity;
                // Xoa cac dong thua di
                for (int row = 14; row > 10 + countlstCapicity; row--)
                {
                    sheet.DeleteRow(row);
                }
            }
            sheet.CopyPaste(sumTempRow, sumRowIndex, 1);

            sheet.SetFormulaValue(string.Format("{0}{1}", "D", sumRowIndex), string.Format("=SUM({0}:{1})", "D10", string.Format("{0}{1}", "D", sumRowIndex - 1)));
            for (char colChar = 'E'; colChar <= 'P'; colChar = (char)(colChar + 2))
                sheet.SetFormulaValue(string.Format("{0}{1}", colChar, sumRowIndex), string.Format("=SUM({0}:{1})", string.Format("{0}{1}", colChar, "10"), string.Format("{0}{1}", colChar, sumRowIndex - 1)));
            #endregion

            #region truyen bien
            string educationLevel = EducationLevelID == 0 ? "" : EducationLevelBusiness.Find(EducationLevelID).Resolution;
            string trainingTypeName = TrainingTypeID > 0 ? TrainingTypeBusiness.Find(TrainingTypeID).Resolution : "[Tất cả]";
            string semester = ReportUtils.ConvertSemesterForDisplayTitle(Semester);

            Dictionary<string, object> variableDic = new Dictionary<string, object>();
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            variableDic["SupervisingDeptName"] = supervisingDept.SupervisingDeptName;
            variableDic["ProvinceVsDateTime"] = supervisingDept.Province.ProvinceName + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string subcommitteeName = SubcommitteeID == 0 ? "[Tất cả]" : SubCommitteeBusiness.Find(SubcommitteeID).Resolution;
            variableDic["SubCommittee"] = subcommitteeName.StartsWith("Ban ") ? subcommitteeName.Replace("Ban ","") : subcommitteeName;
            variableDic["EducationLevel"] = educationLevel.ToUpper().Replace("KHỐI ", "");
            variableDic["Semester"] = semester;
            variableDic["TrainingType"] = trainingTypeName;
            variableDic["AcademicYear"] = Year + " - " + (Year + 1);
            List<object> listRow = new List<object>();
            int index = 1;
            foreach (CapacityStatisticsBO capacity in lstCapacityStatisticOfProvinceTertiary)
            {
                listRow.Add(new Dictionary<string, object> {
                    { "Index",  index++},
                    {  "SchoolName", capacity.SchoolName },
                    {  "SentDate", capacity.SentDate.HasValue ? capacity.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty },
                    {  "TotalExcellent" , capacity.TotalExcellent},
                    {  "TotalGood", capacity.TotalGood },
                    {  "TotalNormal", capacity.TotalNormal },
                    {  "TotalWeak", capacity.TotalWeak },
                    {  "TotalPoor" , capacity.TotalPoor},
                    {  "TotalPass" , capacity.TotalPass},
                    {  "TotalFail" , capacity.TotalFail},
                    {  "PercentExcellent", capacity.PercentExcellent },
                    {  "PercentGood", capacity.PercentGood },
                    {  "PercentNormal", capacity.PercentNormal },
                    {  "PercentWeak", capacity.PercentWeak },
                    {  "PercentPoor", capacity.PercentPoor },
                    {  "PercentPass" , capacity.PercentPass},
                    {  "PercentFail" , capacity.PercentFail},
                    {  "TotalPupil" , capacity.TotalPupil},
                    {  "TotalAboveAverage", capacity.TotalAboveAverage },
                    {  "PercentAboveAverage" , capacity.PercentAboveAverage}
                });
            }
            variableDic["Rows"] = listRow;
            sheet.FillVariableValue(variableDic);
            #endregion

            //xoa sheet rong
            templateSheet.Delete();

            #region luu vao db
            System.IO.Stream data = oBook.ToStream();
            ProcessedReport processedReport = new ProcessedReport();
            processedReport.ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUC;
            processedReport.ProcessedDate = DateTime.Now;
            processedReport.InputParameterHashKey = InputParameterHashKey;
            processedReport.ReportData = ReportUtils.Compress(data);
            processedReport.SentToSupervisor = true;

            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("_[District]", DistrictID > 0 ? "_" + DistrictBusiness.Find(DistrictID).DistrictName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (TrainingTypeID > 0 ? "_" + trainingTypeName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("_[SubCommittee]", (SubcommitteeID > 0 ? "_" + subcommitteeName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.ConvertSemesterForReportName(Semester));
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", EducationLevelID > 0 ? "_" + educationLevel : string.Empty);
            processedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicParams = new Dictionary<string, object> {                
                {"Year", Year},                
                {"Semester", Semester},                
                {"EducationLevelID ", EducationLevelID},
                {"TrainingTypeID", TrainingTypeID},
                {"SubcommitteeID", SubcommitteeID},
                {"SubjectID", SubjectID},
                {"ProvinceID", ProvinceID},
                {"DistrictID", DistrictID},
                {"SupervisingDeptID", SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dicParams, processedReport);
            ProcessedReportBusiness.Insert(processedReport);
            ProcessedReportBusiness.Save();
            #endregion
            //lay id cua report da them vao
            processedReportID = processedReport.ProcessedReportID;
            return lstCapacityStatisticOfProvinceTertiary;
        }

        /// <summary>
        /// <author>PhuongH</author>
        /// <date>7/12/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<CapacityStatisticsBO> CreateSGDCapacityStatisticsByDistrictTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int SubcommitteeID = Utils.GetInt(dic, "SubcommitteeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(dic, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(dic, "IsSuperVisingDeptRole");
            List<CapacityStatisticsBO> lstCapacityStatistic = this.SearchByProvinceGroupByDistrict23(dic);

            if (lstCapacityStatistic == null)
            {
                FileID = 0;
                return null;
            }
            else
            {
                lstCapacityStatistic.ToList();
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOQUANHUYEN);
                string templatePath = ReportUtils.GetTemplatePath(reportDef);
                Stream data = null;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Tạo dữ liệu chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                string supervisingDeptName = supervisingDept.SupervisingDeptName.ToUpper();
                string convertSemester = SMASConvert.ConvertSemester(Semester);
                DateTime reportDate = DateTime.Now;
                string provinceName = supervisingDept.Province.ProvinceName;
                string academicYearTitle = Year + " - " + (Year + 1);

                int firstRow = 11;
                int beginRow = firstRow + 1;
                int endRow = 0;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "O" + firstRow);
                sheet.Name = "ThongKeHocLucTheoQuanHuyen";
                //Danh sách học sinh chuyển lớp

                //Template Dòng đầu tiên
                IVTRange topRow = firstSheet.GetRange("A12", "O12");
                //Template dòng giữa
                IVTRange middleRow = firstSheet.GetRange("A13", "O13");
                //Template dòng cuối
                IVTRange bottomRow = firstSheet.GetRange("A14", "O14");
                //Merge row
                IVTRange lastRow = firstSheet.GetRange("A15", "O15");
                //Template để mergeRow người lập báo cáo

                int currentRow = 0;

                List<object> list = new List<object>();
                int index = 1;
                Dictionary<string, object> dic2 = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName", supervisingDeptName}, 
                    {"Semester", convertSemester},
                    {"ProvinceName", provinceName},                  
                    {"ReportDate", reportDate},
                    {"AcademicYearTitle", academicYearTitle}
                };
                foreach (CapacityStatisticsBO capacityStatistic in lstCapacityStatistic)
                {
                    currentRow = firstRow + index;

                    if (index == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    else if (index % 5 == 0 || index == lstCapacityStatistic.Count())
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }

                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object> {
                    { "Index",  index++},
                    {  "DistrictName", capacityStatistic.DistrictName },
                    {  "TotalExcellent" , capacityStatistic.TotalExcellent},
                    {  "TotalGood", capacityStatistic.TotalGood },
                    {  "TotalNormal", capacityStatistic.TotalNormal },
                    {  "TotalWeak", capacityStatistic.TotalWeak },
                    {  "TotalPoor" , capacityStatistic.TotalPoor},
                    {  "TotalPass" , capacityStatistic.TotalPass},
                    {  "TotalFail" , capacityStatistic.TotalFail},
                    {  "TotalPupil" , capacityStatistic.TotalPupil},
                    {  "TotalAboveAverage", capacityStatistic.TotalAboveAverage },
                });
                }
                dic2.Add("list", list);
                sheet.FillVariableValue(dic2);
                endRow = currentRow + 1;
                sheet.CopyPasteSameRowHeigh(lastRow, endRow);
                for (int i = 1; i < 10; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
                }
                string fomularSum = "=SUM(C" + (beginRow) + ":" + "C" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 3, fomularSum);
                //Fill dữ liệu

                firstSheet.Delete();
                data = oBook.ToStream();
                string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                ProcessedReport processedReport = new ProcessedReport();
                processedReport.ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOQUANHUYEN;
                processedReport.ProcessedDate = DateTime.Now;
                string outputNamePattern = reportDef.OutputNamePattern;
                outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                processedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
                processedReport.InputParameterHashKey = InputParameterHashKey;
                processedReport.ReportData = ReportUtils.Compress(data);
                processedReport.SentToSupervisor = true;
                ProcessedReportParameterRepository.Insert(dic, processedReport);
                ProcessedReportRepository.Insert(processedReport);
                ProcessedReportRepository.Save();

                FileID = processedReport.ProcessedReportID;
                return lstCapacityStatistic;
            }

        }

        //trangdd
        public IQueryable<SubjectCatBO> SearchSubjectHasReport(IDictionary<string, object> dic)
        {
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int? Semester = Utils.GetInt(dic, "Semester");
            bool SentToSupervisor = Utils.GetBool(dic, "SentToSupervisor");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? SubCommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? ProvinceID = Utils.GetInt(dic, "ProvinceID");
            var query = from ms in CapacityStatisticRepository.All
                        join sc in SubjectCatRepository.All on ms.SubjectID equals sc.SubjectCatID
                        join sp in SchoolProfileRepository.All on ms.SchoolID equals sp.SchoolProfileID
                        where ms.ReportCode == ReportCode
                        && ms.Year == Year
                        && (ms.Semester == Semester || Semester == 0)
                        && ms.SentToSupervisor == SentToSupervisor
                        && (ms.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                        && (ms.SubCommitteeID == SubCommitteeID || SubCommitteeID == 0)
                        && (sp.TrainingTypeID == TrainingTypeID || TrainingTypeID == 0)
                        && (sp.SupervisingDeptID == SupervisingDeptID || SupervisingDeptID == 0)
                        && (sp.ProvinceID == ProvinceID || ProvinceID == 0)
                        select new SubjectCatBO
                        {
                            SubjectCatID = ms.SubjectID.Value,
                            SubjectName = sc.SubjectName,
                            DisplayName = sc.DisplayName,
                            Description = sc.Description,
                            OrderInSubject = sc.OrderInSubject,
                            IsCommenting = sc.IsCommenting
                        };
            return query.Distinct().OrderBy(o => o.OrderInSubject);
        }

        /// <summary>
        /// <author>trangdd</author>
        /// <date>4/12/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="InputParameterHashkey"></param>
        /// <returns></returns>        
        public List<CapacityStatisticsBO> CreateSGDCapacitySubjectStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int? AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int subCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            FileID = 0;
            // List<CapacityStatisticsBO> query = this.SearchByProvince23(SearchInfo);
            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceSecondary = this.SearchByProvince23(SearchInfo);
            if (lstCapacityStatisticOfProvinceSecondary != null)
            {

                string reportCode = SystemParamsInFile.REPORT_SGD_THCS_ThongKeHLM;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet 
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet, "S10");
                sheet.Name = "THCS_TKHLM";

                //fill dữ liệu vào sheet thông tin chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                //AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

                string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string educationName;
                if (educationLevelID == 0)
                { educationName = "Khối [Tất cả]"; }
                else
                { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }
                string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;

                string semesterName = "";

                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
                else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
                else semesterName = "Cả năm";

                string Mh = "";
                if (SubjectCatBusiness.Find(subjectID) != null)
                {
                    Mh = SubjectCatBusiness.Find(subjectID).DisplayName;
                }
                string TieuDeBaoCao = "THỐNG KÊ HỌC LỰC MÔN HỌC " + semesterName.ToUpper() + " NĂM HỌC " + year + " - " + (year + 1);
                sheet.SetCellValue("A2", supervisingDeptName);
                sheet.SetCellValue("A3", dateTime);
                sheet.SetCellValue("A5", TieuDeBaoCao);
                sheet.SetCellValue("A6", "Môn: " + Mh + " - " + educationName);

                int startRow = 10;

                IVTRange range = tempSheet.GetRange("A200", "S200");
                IVTRange lastRange = tempSheet.GetRange("A196", "S196");
                IVTRange range5School = tempSheet.GetRange("A198", "S198");

                for (int i = 0; i < lstCapacityStatisticOfProvinceSecondary.Count; i++)
                {
                    CapacityStatisticsBO capBO = lstCapacityStatisticOfProvinceSecondary[i];
                    capBO.TotalPupil = capBO.TotalPupil.HasValue ? capBO.TotalPupil.Value : 0;
                    capBO.TotalExcellent = capBO.TotalExcellent.HasValue ? capBO.TotalExcellent.Value : 0;
                    capBO.TotalGood = capBO.TotalGood.HasValue ? capBO.TotalGood.Value : 0;
                    capBO.TotalNormal = capBO.TotalNormal.HasValue ? capBO.TotalNormal.Value : 0;
                    capBO.TotalWeak = capBO.TotalWeak.HasValue ? capBO.TotalWeak.Value : 0;
                    capBO.TotalPoor = capBO.TotalPoor.HasValue ? capBO.TotalPoor.Value : 0;
                    capBO.TotalPass = capBO.TotalPass.HasValue ? capBO.TotalPass.Value : 0;
                    capBO.TotalFail = capBO.TotalFail.HasValue ? capBO.TotalFail.Value : 0;
                    capBO.TotalAboveAverage = capBO.TotalAboveAverage.HasValue ? capBO.TotalAboveAverage.Value : 0;

                    //Copy row style
                    if (startRow + i > 11)
                    {
                        if ((i + 1) % 5 == 0 || i == lstCapacityStatisticOfProvinceSecondary.Count - 1)
                        {
                            sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                        }
                        else
                            sheet.CopyPasteSameRowHeigh(range, startRow);
                    }

                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ten truong
                    sheet.SetCellValue(startRow, 2, capBO.SchoolName);

                    //Tong so HS
                    sheet.SetCellValue(startRow, 3, capBO.TotalPupil);

                    //Gioi
                    sheet.SetCellValue(startRow, 4, capBO.TotalExcellent);
                    //sheet.SetCellValue(startRow, 5, Math.Round(capBO.PercentExcellent.HasValue ? (decimal)capBO.PercentExcellent : 0, 2, MidpointRounding.AwayFromZero));

                    //Kha
                    sheet.SetCellValue(startRow, 6, capBO.TotalGood);
                    //sheet.SetCellValue(startRow, 7, Math.Round(capBO.PercentGood.HasValue ? (decimal)capBO.PercentGood : 0, 2, MidpointRounding.AwayFromZero));

                    //TB
                    sheet.SetCellValue(startRow, 8, capBO.TotalNormal);
                    //sheet.SetCellValue(startRow, 9, Math.Round(capBO.PercentNormal.HasValue ? (decimal)capBO.PercentNormal : 0, 2, MidpointRounding.AwayFromZero));

                    //Yeu
                    sheet.SetCellValue(startRow, 10, capBO.TotalWeak);
                    //sheet.SetCellValue(startRow, 11, Math.Round(capBO.PercentWeak.HasValue ? (decimal)capBO.PercentWeak : 0, 2, MidpointRounding.AwayFromZero));

                    //Kem
                    sheet.SetCellValue(startRow, 12, capBO.TotalPoor);
                    //sheet.SetCellValue(startRow, 13, Math.Round(capBO.PercentPoor.HasValue ? (decimal)capBO.PercentPoor : 0, 2, MidpointRounding.AwayFromZero));

                    //Đ
                    sheet.SetCellValue(startRow, 14, capBO.TotalPass);
                    //sheet.SetCellValue(startRow, 15, Math.Round(capBO.PercentPass.HasValue ? (decimal)capBO.PercentPass : 0, 2, MidpointRounding.AwayFromZero));
                    //CĐ
                    sheet.SetCellValue(startRow, 16, capBO.TotalFail);
                    //sheet.SetCellValue(startRow, 17, Math.Round(capBO.PercentFail.HasValue ? (decimal)capBO.PercentFail : 0, 2, MidpointRounding.AwayFromZero));

                    //TB tro len
                    sheet.SetCellValue(startRow, 18, capBO.TotalAboveAverage);
                    //sheet.SetCellValue(startRow, 19, Math.Round(capBO.PercentAboveAverage.HasValue ? (decimal)capBO.PercentAboveAverage : 0, 2, MidpointRounding.AwayFromZero));

                    startRow++;
                }

                sheet.CopyPasteSameRowHeigh(range5School, startRow);
                sheet.SetCellValue(startRow, 1, "Tổng cộng");
                sheet.MergeRow(startRow, 1, 2);

                // Thiet lap cong thuc
                string alphabetTS = VTVector.ColumnIntToString(3);
                string fomularTS = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                sheet.SetFormulaValue(startRow, 3, fomularTS.Replace("#", alphabetTS));

                for (int i = 1; i <= 15; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                    sheet.SetFormulaValue(startRow, i + 3, fomular.Replace("#", alphabet));
                }

                tempSheet.Delete();
                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashkey;
                pr.ReportData = ReportUtils.Compress(oBook.ToStream());
                sentToSupervisor = true;
                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;
                string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);

                outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + DistrictBusiness.Find(districtID).DistrictName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(trainningTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);

                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                ProcessedReportParameterRepository.Insert(SearchInfo, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
            }
            return lstCapacityStatisticOfProvinceSecondary;
        }

        /// <summary>
        /// <author>trangdd</author>
        /// <date>4/12/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="InputParameterHashkey"></param>
        /// <returns></returns>        
        public List<CapacityStatisticsBO> CreatePGDCapacitySubjectStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int? AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int subCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            FileID = 0;

            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceSecondary = this.SearchBySupervisingDept23(SearchInfo);
            if (lstCapacityStatisticOfProvinceSecondary != null)
            {
                string reportCode = SystemParamsInFile.REPORT_PGD_THCS_ThongKeHLM;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet 
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet, "S10");
                sheet.Name = "THCS_TKHLM";

                //fill dữ liệu vào sheet thông tin chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                //AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

                string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string educationName;
                if (educationLevelID == 0)
                { educationName = "Khối [Tất cả]"; }
                else
                { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }
                string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;

                string semesterName = "";

                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
                else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
                else semesterName = "Cả năm";
                string Mh = "";
                if (SubjectCatBusiness.Find(subjectID) != null)
                {
                    Mh = SubjectCatBusiness.Find(subjectID).DisplayName;
                }
                string TieuDeBaoCao = "THỐNG KÊ HỌC LỰC MÔN HỌC " + semesterName.ToUpper() + " NĂM HỌC " + year + " - " + (year + 1);
                sheet.SetCellValue("A2", supervisingDeptName);
                sheet.SetCellValue("A3", dateTime);
                sheet.SetCellValue("A5", TieuDeBaoCao);
                sheet.SetCellValue("A6", "Môn: " + Mh + " - " + educationName);

                int startRow = 10;

                IVTRange range = tempSheet.GetRange("A200", "S200");
                IVTRange lastRange = tempSheet.GetRange("A196", "S196");
                IVTRange range5School = tempSheet.GetRange("A198", "S198");

                for (int i = 0; i < lstCapacityStatisticOfProvinceSecondary.Count; i++)
                {
                    CapacityStatisticsBO capBO = lstCapacityStatisticOfProvinceSecondary[i];
                    capBO.TotalPupil = capBO.TotalPupil.HasValue ? capBO.TotalPupil.Value : 0;
                    capBO.TotalExcellent = capBO.TotalExcellent.HasValue ? capBO.TotalExcellent.Value : 0;
                    capBO.TotalGood = capBO.TotalGood.HasValue ? capBO.TotalGood.Value : 0;
                    capBO.TotalNormal = capBO.TotalNormal.HasValue ? capBO.TotalNormal.Value : 0;
                    capBO.TotalWeak = capBO.TotalWeak.HasValue ? capBO.TotalWeak.Value : 0;
                    capBO.TotalPoor = capBO.TotalPoor.HasValue ? capBO.TotalPoor.Value : 0;
                    capBO.TotalPass = capBO.TotalPass.HasValue ? capBO.TotalPass.Value : 0;
                    capBO.TotalFail = capBO.TotalFail.HasValue ? capBO.TotalFail.Value : 0;
                    capBO.TotalAboveAverage = capBO.TotalAboveAverage.HasValue ? capBO.TotalAboveAverage.Value : 0;

                    //Copy row style
                    if (startRow + i > 11)
                    {
                        if ((i + 1) % 5 == 0 || i == lstCapacityStatisticOfProvinceSecondary.Count - 1)
                        {
                            sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                        }
                        else
                            sheet.CopyPasteSameRowHeigh(range, startRow);
                    }

                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ten truong
                    sheet.SetCellValue(startRow, 2, capBO.SchoolName);

                    //Tong so HS
                    sheet.SetCellValue(startRow, 3, capBO.TotalPupil);

                    //Gioi
                    sheet.SetCellValue(startRow, 4, capBO.TotalExcellent);
                    //sheet.SetCellValue(startRow, 5, Math.Round((decimal)capBO.PercentExcellent, 2, MidpointRounding.AwayFromZero));

                    //Kha
                    sheet.SetCellValue(startRow, 6, capBO.TotalGood);
                    //sheet.SetCellValue(startRow, 7, Math.Round((decimal)capBO.PercentGood, 2, MidpointRounding.AwayFromZero));

                    //TB
                    sheet.SetCellValue(startRow, 8, capBO.TotalNormal);
                    //sheet.SetCellValue(startRow, 9, Math.Round((decimal)capBO.PercentNormal, 2, MidpointRounding.AwayFromZero));

                    //Yeu
                    sheet.SetCellValue(startRow, 10, capBO.TotalWeak);
                    //sheet.SetCellValue(startRow, 11, Math.Round((decimal)capBO.PercentWeak, 2, MidpointRounding.AwayFromZero));

                    //Kem
                    sheet.SetCellValue(startRow, 12, capBO.TotalPoor);
                    //sheet.SetCellValue(startRow, 13, Math.Round((decimal)capBO.PercentPoor, 2, MidpointRounding.AwayFromZero));

                    //Đ
                    sheet.SetCellValue(startRow, 14, capBO.TotalPass);
                    //sheet.SetCellValue(startRow, 15, Math.Round((decimal)capBO.PercentPass, 2, MidpointRounding.AwayFromZero));
                    //CĐ
                    sheet.SetCellValue(startRow, 16, capBO.TotalFail);
                    //sheet.SetCellValue(startRow, 17, Math.Round((decimal)capBO.PercentFail, 2, MidpointRounding.AwayFromZero));

                    //TB tro len
                    sheet.SetCellValue(startRow, 18, capBO.TotalAboveAverage);
                    //sheet.SetCellValue(startRow, 19, Math.Round((decimal)capBO.PercentAboveAverage, 2, MidpointRounding.AwayFromZero));

                    startRow++;
                }
                
                sheet.CopyPasteSameRowHeigh(range5School, startRow);
                sheet.SetCellValue(startRow, 1, "Tổng cộng");
                sheet.MergeRow(startRow, 1, 2);
                string alphabetTS = VTVector.ColumnIntToString(3);
                string fomularTS = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                sheet.SetFormulaValue(startRow, 3, fomularTS.Replace("#", alphabetTS));

                for (int i = 1; i <= 15; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                    sheet.SetFormulaValue(startRow, i + 3, fomular.Replace("#", alphabet));
                }

                tempSheet.Delete();

                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashkey;
                pr.ReportData = ReportUtils.Compress(oBook.ToStream());
                sentToSupervisor = true;
                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;
                string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);
                
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(trainningTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);

                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                ProcessedReportParameterRepository.Insert(SearchInfo, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
            }
            return lstCapacityStatisticOfProvinceSecondary;
        }

        /// <summary>
        /// CreateSGDCapacityStatisticsBySubCommitteeTertiary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns>
        /// List{CapacityStatisticsBO}
        /// </returns>
        ///<author>Nam ta </author>
        ///<Date>12/5/2012</Date>
        public List<CapacityStatisticsBO> CreateSGDCapacityStatisticsBySubCommitteeTertiary(IDictionary<string, object> SearchInfo, String InputParameterHashKey, out int ProcessedReportID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int? Year = Utils.GetInt(SearchInfo, "Year");
            int? Semester = Utils.GetInt(SearchInfo, "Semester");
            int? EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int? SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSuperVisingDeptRole");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            var lstCapacityStatisticOfProvinceBySubCommitteeTertiary = this.SearchByProvinceGroupBySubCommittee23(SearchInfo);
            ProcessedReportID = 0;

            if (lstCapacityStatisticOfProvinceBySubCommitteeTertiary != null && lstCapacityStatisticOfProvinceBySubCommitteeTertiary.Count() > 0)
            {

                string reportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHOCLUCTHEOBAN;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //L?y sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);

                SupervisingDept SupervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                //tieu de ten so giao duc

                sheet.SetCellValue("A2", SupervisingDept.SupervisingDeptName.ToUpper());
                string semeterString = "";
                //tieu de nam hoc
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    semeterString = "Học Kỳ I";
                }
                else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    semeterString = "Học Kỳ II";
                }
                else
                {
                    semeterString = "Cả năm";
                }
                sheet.SetCellValue("A7", semeterString + " Năm học " + Year.ToString() + " - " + (Year + 1).ToString());

                //Tieu de th?i gian
                if (ProvinceID.HasValue)
                {
                    Province Province = ProvinceBusiness.Find(ProvinceID);
                    sheet.SetCellValue("J4", Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                }

                //IEnumerable<int> GroupEducationLevel = lstCapacityStatisticOfProvinceBySubCommitteeTertiary.Select(u => u.EducationLevel.Value);

                int rowStart = 11;
                int EducationLevelOld = lstCapacityStatisticOfProvinceBySubCommitteeTertiary != null && lstCapacityStatisticOfProvinceBySubCommitteeTertiary.Count() > 0 ? lstCapacityStatisticOfProvinceBySubCommitteeTertiary.FirstOrDefault().EducationLevelID.Value : 0;

                IVTRange RowCopy = firstSheet.GetRange("A12", "O12");
                IVTRange RowSumEducationLevel = firstSheet.GetRange("A13", "O13");
                IVTRange RowSumProvince = firstSheet.GetRange("A18", "O18");
                //xoa 3 dong template
                sheet.DeleteRow(11);
                sheet.DeleteRow(12);
                sheet.DeleteRow(13);
                sheet.DeleteRow(14);
                int rowMerger = rowStart;
                int firstEdu = 11;
                int beginRow = 11;

                List<EducationLevel> listEducationLevel = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && o.IsActive == true).ToList();
                foreach (var edu in listEducationLevel)
                {
                    IQueryable<CapacityStatisticsBO> lst = lstCapacityStatisticOfProvinceBySubCommitteeTertiary.Where(o => o.EducationLevel.EducationLevelID == edu.EducationLevelID);
                    foreach (var item in lst)
                    {
                        sheet.CopyPasteSameSize(RowCopy, rowStart, 1);
                        sheet.SetCellValue(rowStart, 2, item.SubCommitteeTitle);
                        sheet.SetCellValue(rowStart, 3, item.TotalPupil);
                        sheet.SetCellValue(rowStart, 4, item.TotalExcellent);
                        sheet.SetCellValue(rowStart, 6, item.TotalGood);
                        sheet.SetCellValue(rowStart, 8, item.TotalNormal);
                        sheet.SetCellValue(rowStart, 10, item.TotalWeak);
                        sheet.SetCellValue(rowStart, 12, item.TotalPoor);
                        rowStart++;
                    }
                    int count = lst.Count();
                    sheet.CopyPasteSameSize(RowSumEducationLevel, rowStart, 1);


                    IVTRange RowMerger = sheet.GetRange(rowMerger, 1, rowStart, 1);
                    RowMerger.Merge();
                    sheet.SetCellValue(rowMerger, 1, edu.Resolution);
                    rowMerger = rowStart + 1;
                    int endrow = rowMerger - 2;
                    sheet.SetCellValue(rowStart, 2, "Toàn khối " + edu.Resolution);

                    string alphabetTS = VTVector.ColumnIntToString(3);
                    string fomularTS = "=SUM(#" + (firstEdu) + ":" + "#" + (endrow) + ")";
                    sheet.SetFormulaValue(rowStart, 3, fomularTS.Replace("#", alphabetTS));

                    for (int i = 1; i <= 11; i += 2)
                    {
                        string alphabet = VTVector.ColumnIntToString(i + 3);
                        string fomular = "=SUM(#" + (firstEdu) + ":" + "#" + (endrow) + ")";
                        sheet.SetFormulaValue(rowStart, i + 3, fomular.Replace("#", alphabet));
                    }
                    firstEdu = firstEdu + count + 1;
                    rowStart++;
                }

                //Tinh tong 
                //copy dong row tong theo tinh
                sheet.CopyPasteSameSize(RowSumProvince, rowStart, 1);
                //Cột tổng số toàn bộ các khối
                string alphabetTST = VTVector.ColumnIntToString(3);
                string fomularTST = "=SUM(#" + (beginRow) + ":" + "#" + (rowStart - 1) + ")/2";
                sheet.SetFormulaValue(rowStart, 3, fomularTST.Replace("#", alphabetTST));
                for (int i = 1; i <= 11; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (rowStart - 1) + ")/2";
                    sheet.SetFormulaValue(rowStart, i + 3, fomular.Replace("#", alphabet));
                }
                sheet.Name = "TKHLTheoBan";
                //ket thuc file chuyen sang luu vao csdl
                //-	ReportCode = IDictionary[“ReportCode”]
                //-	ProcessedDate = Ngày hi?n t?i
                //-	InputParameterHashKey = InputParameterHashKey
                //-	ReportData : ReportUtils.Compress(Data)
                //-	SentToSupervisor = 1
                //Map tuong ?ng vào d?i tu?ng ProcessedReport r?i g?i hàm ProcessedReportBusiness.Insert(ProcessedReport)
                //Xoa sheet dau
                firstSheet.Delete();
                CapacityConductReport CapacityConductReport = new CapacityConductReport();
                CapacityConductReport.DistrictID = DistrictID.Value;
                CapacityConductReport.EducationLevelID = EducationLevelID.Value;
                CapacityConductReport.ProvinceID = ProvinceID.Value;
                CapacityConductReport.Semester = Semester.Value;
                CapacityConductReport.SubcommitteeID = SubCommitteeID.Value;
                CapacityConductReport.SupervisingDeptID = SupervisingDeptID.Value;
                CapacityConductReport.TrainingTypeID = TrainingTypeID.Value;
                CapacityConductReport.Year = Year.Value;
                ProcessedReport ProcessedReport = new ProcessedReport();
                ProcessedReport.ReportCode = reportCode;
                ProcessedReport.ProcessedDate = DateTime.Now;
                ProcessedReport.InputParameterHashKey = this.GetHashKeyForConductCapacityStatistics(CapacityConductReport);
                ProcessedReport.ReportData = ReportUtils.Compress(oBook.ToStream());
                ProcessedReport.SentToSupervisor = true;
                //T?o tên file HS_[SchoolLevel]_BangDiem_[Class]_[Semester]_[Subject]
                string outputNamePattern = reportDef.OutputNamePattern;
                string semester = ReportUtils.ConvertSemesterForReportName(Semester.Value);
                outputNamePattern = outputNamePattern.Replace("_[District]", DistrictID.HasValue && DistrictID.Value > 0 ? "_" + DistrictBusiness.Find(DistrictID).DistrictName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (TrainingTypeID.HasValue && TrainingTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(TrainingTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", EducationLevelID.HasValue && EducationLevelID.Value > 0 ? "_" + EducationLevelBusiness.Find(EducationLevelID).Resolution : string.Empty);

                ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID ", EducationLevelID},
                {"TrainingTypeID", TrainingTypeID},
                {"SubcommitteeID", SubCommitteeID},
                {"SubjectID", SubjectID},
                {"ProvinceID", ProvinceID},
                {"DistrictID", DistrictID},
                {"SupervisingDeptID", SupervisingDeptID}
            };
                ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
                ProcessedReportRepository.Insert(ProcessedReport);
                ProcessedReportRepository.Save();
                //luu report vao csdl
                ProcessedReportID = ProcessedReport.ProcessedReportID;
                //luu vao bang ProcessedReportParameter 
                return lstCapacityStatisticOfProvinceBySubCommitteeTertiary.ToList();
            }
            else return null;


        }

        /// <summary>
        /// SearchByProvinceGroupBySubCommittee23
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{CapacityStatisticsBO}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/5/2012</Date>
        public IQueryable<CapacityStatisticsBO> SearchByProvinceGroupBySubCommittee23(IDictionary<string, object> SearchInfo)
        {
            int? Year = Utils.GetInt(SearchInfo, "Year");
            int? Semester = Utils.GetInt(SearchInfo, "Semester");
            int? SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = SystemParamsInFile.THONGKEHOCLUCTHEOBANCAP3;
            bool? SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSuperVisingDeptRole");
            SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                supervisingDeptID = su.ParentID.Value;
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            // Tim kiem cac truong thuoc quyen quan ly cua phong so
            dic["SupervisingDeptID"] = supervisingDeptID;
            dic["IsActive"] = true;
            dic["Year"] = Year;
            dic["Semester"] = Semester;
            dic["AppliedLevel"] = AppliedLevel;
            IQueryable<SchoolProfile> listSchoolProfile = SchoolProfileBusiness.Search(dic); // Danh sach cac truong thuoc quyen quan ly cua phong so dang xet

            int grade = Utils.GradeToBinary(AppliedLevel);
            listSchoolProfile = listSchoolProfile.Where(o => (o.EducationGrade & grade) != 0);

            var lstCapacityStatistics = from Cs in this.All
                                        join sp in listSchoolProfile on Cs.SchoolID.Value equals sp.SchoolProfileID
                                        where Cs.AppliedLevel == AppliedLevel
                                        select Cs;

            if (ReportCode != null)
                lstCapacityStatistics = lstCapacityStatistics.Where(o => o.ReportCode == ReportCode);
            if (Year.HasValue && Year != 0)
                lstCapacityStatistics = lstCapacityStatistics.Where(o => o.Year == Year.Value);
            if (Semester.HasValue && Semester != 0)
                lstCapacityStatistics = lstCapacityStatistics.Where(o => o.Semester == Semester.Value);
            if (SentToSupervisor.HasValue)
                lstCapacityStatistics = lstCapacityStatistics.Where(o => o.SentToSupervisor == SentToSupervisor.Value);
            CapacityStatisticsBO item = new CapacityStatisticsBO();

            List<CapacityStatisticsBO> lstCapacityStatisticOfProvince23 = new List<CapacityStatisticsBO>();

            var query = from lcss in lstCapacityStatistics
                        join lsch in listSchoolProfile on lcss.SchoolID equals lsch.SchoolProfileID
                        where lcss.SentDate == (from p in lstCapacityStatistics where lcss.SchoolID == p.SchoolID && lcss.EducationLevelID == p.EducationLevelID && lcss.SubCommitteeID == p.SubCommitteeID select p.SentDate).Max()
                        select lcss;
            var lstCapacityStatisticOfSchool23 = query.GroupBy(u => new { u.EducationLevelID, u.SubCommitteeID, SubCommitteeTitle = u.SubCommittee.Resolution, EducationLevelName = u.EducationLevel.Resolution }).Select(u => new CapacityStatisticsBO
            {
                EducationLevelID = u.Key.EducationLevelID.Value,
                SubCommitteeID = u.Key.SubCommitteeID.Value,
                SubCommitteeTitle = u.Key.SubCommitteeTitle,
                EducationLevelName = u.Key.EducationLevelName,
                TotalExcellent = u.Sum(v => v.CapacityLevel01),
                TotalGood = u.Sum(v => v.CapacityLevel02),
                TotalNormal = u.Sum(v => v.CapacityLevel03),
                TotalWeak = u.Sum(v => v.CapacityLevel04),
                TotalPoor = u.Sum(v => v.CapacityLevel05),
                TotalPupil = u.Sum(v => v.PupilTotal),
            }).OrderBy(u => u.EducationLevel).ThenBy(u => u.SubCommitteeTitle).ToList();

            if (lstCapacityStatisticOfSchool23 != null && lstCapacityStatisticOfSchool23.Count > 0)
            {
                foreach (var obj in lstCapacityStatisticOfSchool23)
                {
                    obj.TotalPupil = obj.TotalPupil == 0 ? 0 : obj.TotalPupil;
                    obj.PercentExcellent = Math.Round((((double)obj.TotalExcellent / (double)obj.TotalPupil) * 100), 2);
                    obj.PercentGood = Math.Round(((double)obj.TotalGood / (double)obj.TotalPupil) * 100, 2);
                    obj.PercentNormal = Math.Round(((double)obj.TotalNormal / (double)obj.TotalPupil) * 100, 2);
                    obj.PercentWeak = Math.Round(((double)obj.TotalWeak / (double)obj.TotalPupil) * 100, 2);
                    obj.PercentPoor = Math.Round(((double)obj.TotalPoor / (double)obj.TotalPupil) * 100, 2);
                    obj.TotalAboveAverage = obj.TotalExcellent + obj.TotalGood + obj.TotalNormal;
                    obj.PercentAboveAverage = Math.Round(((double)obj.TotalAboveAverage / (double)obj.TotalPupil) * 100, 2);

                }
            }

            return lstCapacityStatisticOfSchool23.AsQueryable();
        }


        /// <summary>
        /// Thống kê học lực môn
        /// Author: Tamhm1
        /// Date: 10/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="inputParameterHashKey"></param>
        /// <returns></returns>
        public List<CapacityStatisticsBO> CreateSGDCapacitySubjectStatisticsTertiary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID)
        {
            //Nếu kết quả khác null thì thực hiện các bước tiếp theo. Nếu = null thì return luôn
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? subcommitteeID = Utils.GetInt(SearchInfo, "SubcommitteeID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            string ReportCode = "SGD_THPT_ThongKeHLM";
            List<CapacityStatisticsBO> lstCapacityStatisticOfProvinceTertiary;
            lstCapacityStatisticOfProvinceTertiary = this.SearchByProvince23(SearchInfo);
            FileID = 0;
            if (lstCapacityStatisticOfProvinceTertiary.Count() == 0) return null;
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
                Stream data = null;
                //Lấy các thông tin: Tên Sở (SupervisingDeptName dựa vào SupervisingDeptID từ session); Môn học (SubjectName dựa vào SubjectID); Học kỳ, năm học, loại hình đào tạo, khối học (trong IDictionary); Ngày tháng (lấy ngày hiện tại) để điền vào các vị trí tương ứng trong file excel
                //Lấy kết quả lstCapacityStatisticOfProvinceTertiary để điền giá trị vào file excel
                //	Chuyển file excel thu được sang Stream: Data
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Tạo dữ liệu chung

                string supervisingDeptName = SupervisingDeptRepository.Find(supervisingDeptID).SupervisingDeptName.ToUpper();
                string convertSemester = SMASConvert.ConvertSemester(semester).ToUpper();
                var Subject = this.SubjectCatRepository.Find(subjectID);
                string subjectName = Subject.DisplayName.ToUpper();
                string academicYearTitle = year + " - " + (year + 1);
                string educationLevelName = "";
                var EducationLevel = this.EducationLevelRepository.Find(educationLevelID);
                if (educationLevelID.Value != 0)
                    educationLevelName = "-  " + EducationLevel.Resolution.ToUpper();


                int firstRow = 9;
                int beginRow = firstRow + 1;
                int endRow = 0;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "S" + firstRow);
                sheet.Name = "ThongKeHLM";
                //Thong ke hoc luc mon

                //Template Dòng đầu tiên
                IVTRange topRow = firstSheet.GetRange("A10", "S10");
                //Template dòng giữa
                IVTRange middleRow = firstSheet.GetRange("A11", "S11");
                //Template dòng cuối
                IVTRange lastRow = firstSheet.GetRange("A12", "S12");
                //Templata dòng tổng
                IVTRange sumRow = firstSheet.GetRange("A13", "S13");
                int currentRow = 0;

                List<object> list = new List<object>();

                Dictionary<string, object> dic = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName", supervisingDeptName}, 
                    {"Semester", convertSemester},
                    {"SubjectName", subjectName},
                    {"EducationLevelName", educationLevelName},                  
                    {"AcademicYearTitle", academicYearTitle}
                };
                int order = 1;
                int count = lstCapacityStatisticOfProvinceTertiary.Count;
                foreach (CapacityStatisticsBO capacityStatistic in lstCapacityStatisticOfProvinceTertiary)
                {
                    currentRow = firstRow + order;

                    if (order == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    else if (order % 5 == 0 || order == count)
                    {
                        sheet.CopyPasteSameRowHeigh(lastRow, currentRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object> 
                    {      
                        {"Order", order++},
                        {"SchoolName", capacityStatistic.SchoolName},
                        {"TotalExcellent",capacityStatistic.TotalExcellent},
                        {"TotalGood", capacityStatistic.TotalGood},                      
                        {"TotalNormal", capacityStatistic.TotalNormal},
                        {"TotalWeak", capacityStatistic.TotalWeak},
                        {"TotalPoor",capacityStatistic.TotalPoor}, 
                        {"TotalPass",capacityStatistic.TotalPass}, 
                        {"TotalFail",capacityStatistic.TotalFail}, 
                        {"TotalPupil",capacityStatistic.TotalPupil}            
                    });

                }
                dic.Add("list", list);
                sheet.FillVariableValue(dic);
                endRow = currentRow + 1;
                sheet.CopyPasteSameRowHeigh(sumRow, endRow);
                for (int i = 1; i < 16; i += 2)
                {   
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
                }
                string fomularSumTotal = "=SUM(C" + (beginRow) + ":" + "C" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 3, fomularSumTotal);
                //Fill dữ liệu

                firstSheet.Delete();
                data = oBook.ToStream();


                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = SystemParamsInFile.REPORT_SGD_THPT_ThongKeHLM;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = inputParameterHashKey;
                pr.ReportData = ReportUtils.Compress(data);
                pr.SentToSupervisor = true;

                //Tao ten file
                string outputNamePattern = reportDef.OutputNamePattern;
                string hk = ReportUtils.ConvertSemesterForReportName(semester);
                string khoi = "";
                if (educationLevelID.Value != 0)
                {
                    khoi = EducationLevelBusiness.Find(educationLevelID).Resolution;
                }

                outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + DistrictBusiness.Find(districtID).DistrictName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainingTypeID.HasValue && trainingTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(trainingTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", EducationLevel != null ? "_" + EducationLevel.Resolution : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[Subject]", Subject == null ? string.Empty : "_" + Subject.DisplayName);
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic1 = new Dictionary<string, object> {
                {"Year", year},              
                {"Semester", semester},
                {"EducationLevelID", educationLevelID},
                {"TrainingTypeID", trainingTypeID},
                {"SubcommitteeID", subcommitteeID},
                {"SubjectID", subjectID},
                {"ProvinceID",provinceID},
                {"DistrictID", districtID},
                {"SupervisingDeptID", supervisingDeptID}
            };
                ProcessedReportRepository.Insert(pr);
                ProcessedReportParameterRepository.Insert(dic1, pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
                //FileID = pr.ProcessedReportID;
                return lstCapacityStatisticOfProvinceTertiary;
            }
        }


        #region dungnt
        /// <summary>
        /// CreateSGDCapacitySubjectStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashkey">The input parameter hashkey.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/12/2012   2:04 PM
        /// </remarks>
        public List<CapacityStatisticsBO> CreateSGDCapacitySubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");

            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");

            List<CapacityStatisticsBO> lsMSBDBO = SearchByProvince(SearchInfo);
            lsMSBDBO = lsMSBDBO.OrderBy(o => o.DistrictName).ToList();
            if (lsMSBDBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("SGD_TH_TongHopHocLucMonTinhDiem");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A11", "M11");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "M11");
            sheet.Name = "TH_HLM";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP HỌC LỰC MÔN TÍNH ĐIỂM - " + thisSubject.SubjectName.ToUpper();
            if (Semester == 0)
            {
                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            }
            string semesterAndYear = ReportUtils.ConvertSemesterForReportName(Semester) + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSBDBO.Count(); j++)
            {
                // Tao style
                sheet.CopyPasteSameSize(templateRange, "A" + (j + 11).ToString());
                CapacityStatisticsBO item = lsMSBDBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["DistrictName"] = item.DistrictName;
                innerDic["TotalSchool"] = item.TotalSchool;
                innerDic["STT"] = j + 1;
                innerDic["TotalPupil"] = item.TotalPupil;

                innerDic["TotalExcellent"] = item.TotalExcellent;
                innerDic["PercentExcellent"] = Math.Round(100.0 * item.PercentExcellent.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalGood"] = item.TotalGood;
                innerDic["PercentGood"] = Math.Round(100.0 * item.PercentGood.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalNormal"] = item.TotalNormal;
                innerDic["PercentNormal"] = Math.Round(100.0 * item.PercentNormal.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalWeak"] = item.TotalWeak;
                innerDic["PercentWeak"] = Math.Round(100.0 * item.PercentWeak.Value, 2, MidpointRounding.AwayFromZero);
                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "SGD_TH_TongHopHocLucMonTinhDiem";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //SGD_TH_TongHopHLM _[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }

            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID = 0
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",0},
                {"SupervisingDeptID",SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSBDBO;
        }

        /// <summary>
        /// CreatePGDCapacitySubjectStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashkey">The input parameter hashkey.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/12/2012   2:04 PM
        /// </remarks>
        public List<CapacityStatisticsBO> CreatePGDCapacitySubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");

            List<CapacityStatisticsBO> lsMSOSBO = SearchBySupervisingDept(SearchInfo);
            if (lsMSOSBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("PGD_TH_TongHopHocLucMonTinhDiem");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;


            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A11", "L11");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "L11");
            sheet.Name = "TH_HLM";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP HỌC LỰC MÔN TÍNH ĐIỂM - " + thisSubject.SubjectName.ToUpper();
            string strSemester = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "Học kỳ " + Semester;
            }
            else
            {
                strSemester = "Cả năm";
            }
            string semesterAndYear = strSemester + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSOSBO.Count(); j++)
            {
                // Tao style
                sheet.CopyPasteSameSize(templateRange, "A" + (j + 11).ToString());
                CapacityStatisticsBO item = lsMSOSBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["School"] = item.SchoolName;
                innerDic["TotalPupil"] = item.TotalPupil;

                innerDic["TotalExcellent"] = item.TotalExcellent;
                innerDic["PercentExcellent"] = Math.Round(100.0 * item.PercentExcellent.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalGood"] = item.TotalGood;
                innerDic["PercentGood"] = Math.Round(100.0 * item.PercentGood.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalNormal"] = item.TotalNormal;
                innerDic["PercentNormal"] = Math.Round(100.0 * item.PercentNormal.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalWeak"] = item.TotalWeak;
                innerDic["PercentWeak"] = Math.Round(100.0 * item.PercentWeak.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["STT"] = j + 1;

                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "PGD_TH_TongHopHocLucMonTinhDiem";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //PGD_TH_TongHopHLM _[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }
            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",DistrictID},
                 {"SupervisingDeptID",SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSOSBO;
        }

        /// <summary>
        /// SearchByProvince
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/12/2012   2:04 PM
        /// </remarks>
        public List<CapacityStatisticsBO> SearchByProvince(IDictionary<string, object> SearchInfo)
        {
            int districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            int year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int trainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int subCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(supervisingDeptID);
            if(supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                supervisingDeptID = supervisingDept.ParentID.Value;
            }
            int grade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);
            if (provinceID == 0) return null;

            IQueryable<CapacityStatistic> queryCapacityStatistic = this.All.Where(o => o.SchoolProfile.IsActive && (o.SchoolProfile.EducationGrade & grade) != 0);

            if (provinceID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SchoolProfile.ProvinceID == provinceID);

            if (districtID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SchoolProfile.DistrictID == districtID);

            if (reportCode.Trim().Length != 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.ReportCode.Equals(reportCode));

            if (year > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.Year == year);

            if (semester > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.Semester == semester);

            if (sentToSupervisor.HasValue)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SentToSupervisor == sentToSupervisor);

            if (educationLevelID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.EducationLevelID == educationLevelID);

            if (subCommitteeID == 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => !o.SubCommitteeID.HasValue);
            else
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SubCommitteeID == subCommitteeID);

            if (subjectID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SubjectID == subjectID);

            if (trainingTypeID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SchoolProfile.TrainingTypeID == trainingTypeID);
            if (supervisingDeptID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => (o.SchoolProfile.SupervisingDeptID == supervisingDeptID || o.SchoolProfile.SupervisingDept.ParentID == supervisingDeptID));

            var queryCapacityStatisticMax = queryCapacityStatistic.Where(o => o.SentDate == queryCapacityStatistic
                .Where(u => u.SchoolID == o.SchoolID).Select(u => u.SentDate).Max());
            var query = from d in DistrictBusiness.All
                        join c in queryCapacityStatisticMax on d.DistrictID equals c.SchoolProfile.DistrictID into g
                        from jn in g.DefaultIfEmpty()
                        where (d.IsActive.HasValue && d.IsActive.Value && (provinceID > 0 ? d.ProvinceID == provinceID : true))
                        group jn by new { d.DistrictID, d.DistrictName } into g2
                        orderby g2.Key.DistrictName
                        select new CapacityStatisticsBO
                        {
                            DistrictID = g2.Key.DistrictID,
                            DistrictName = g2.Key.DistrictName,
                            TotalSchool = g2.Count(o => o.SchoolID.HasValue),
                            TotalExcellent = g2.Sum(u => u.CapacityLevel01),
                            TotalGood = g2.Sum(u => u.CapacityLevel02),
                            TotalNormal = g2.Sum(u => u.CapacityLevel03),
                            TotalWeak = g2.Sum(u => u.CapacityLevel04),
                            TotalPoor = g2.Sum(u => u.CapacityLevel05),
                            TotalAboveAverage = g2.Sum(u => u.OnAverage),
                            TotalPupil = g2.Sum(u => u.PupilTotal)
                        };

            List<CapacityStatisticsBO> lstCapacityStatisticsOfProvince = query.ToList();
            lstCapacityStatisticsOfProvince.ForEach(u =>
            {
                u.PercentExcellent = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalExcellent.HasValue ? (double)u.TotalExcellent / (double)u.TotalPupil : 0.0;
                u.PercentGood = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalGood.HasValue ? (double)u.TotalGood / (double)u.TotalPupil : 0;
                u.PercentNormal = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalNormal.HasValue ? (double)u.TotalNormal / (double)u.TotalPupil : 0;
                u.PercentWeak = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalWeak.HasValue ? (double)u.TotalWeak / (double)u.TotalPupil : 0;
                u.PercentPoor = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalPoor.HasValue ? (double)u.TotalPoor / (double)u.TotalPupil : 0;
            });
            return lstCapacityStatisticsOfProvince;
        }

        /// <summary>
        /// SearchBySupervisingDept23
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/12/2012   2:04 PM
        /// </remarks>
        public List<CapacityStatisticsBO> SearchBySupervisingDept(IDictionary<string, object> SearchInfo)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");

            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            int grade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);
            if (provinceID == 0) return null;

            IQueryable<CapacityStatistic> queryCapacityStatistic = this.All;

            if (reportCode.Trim().Length != 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.ReportCode.Equals(reportCode));

            if (year > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.Year == year);

            if (semester > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.Semester == semester);

            queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SentToSupervisor == sentToSupervisor);

            if (educationLevelID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.EducationLevelID == educationLevelID);

            if (subjectID > 0)
                queryCapacityStatistic = queryCapacityStatistic.Where(o => o.SubjectID == subjectID);

            var queryCapacityStatisticMax = queryCapacityStatistic.Where(o => o.SentDate == queryCapacityStatistic
                .Where(u => u.SchoolID == o.SchoolID).Select(u => u.SentDate).Max());

            var query = from s in SchoolProfileRepository.All
                        join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                        join c in queryCapacityStatisticMax on s.SchoolProfileID equals c.SchoolID into g
                        from jn in g.DefaultIfEmpty()
                        where s.IsActive == true
                                   && (provinceID > 0 ? s.ProvinceID == provinceID : true)
                                   && (SupervisingDeptID > 0 ? (sd.SupervisingDeptID == SupervisingDeptID || sd.ParentID == SupervisingDeptID) : true)
                                   && (s.EducationGrade & grade) != 0
                                   && (trainningTypeID > 0 ? s.TrainingTypeID == trainningTypeID : true)
                        orderby s.SchoolName
                        select new CapacityStatisticsBO
                        {
                            SchoolID = s.SchoolProfileID,
                            SchoolName = s.SchoolName,
                            TotalExcellent = jn.CapacityLevel01,
                            TotalGood = jn.CapacityLevel02,
                            TotalNormal = jn.CapacityLevel03,
                            TotalWeak = jn.CapacityLevel04,
                            TotalPoor = jn.CapacityLevel05,
                            TotalAboveAverage = jn.OnAverage,
                            TotalPupil = jn.PupilTotal
                        };
            List<CapacityStatisticsBO> lstCapacityStatisticsOfProvince = query.ToList();
            lstCapacityStatisticsOfProvince.ForEach(u =>
            {
                u.PercentExcellent = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalExcellent.HasValue ? (double)u.TotalExcellent / (double)u.TotalPupil : 0;
                u.PercentGood = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalGood.HasValue ? (double)u.TotalGood / (double)u.TotalPupil : 0;
                u.PercentNormal = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalNormal.HasValue ? (double)u.TotalNormal / (double)u.TotalPupil : 0;
                u.PercentWeak = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalWeak.HasValue ? (double)u.TotalWeak / (double)u.TotalPupil : 0;
                u.PercentPoor = u.TotalPupil.HasValue && u.TotalPupil.Value > 0 && u.TotalPoor.HasValue ? (double)u.TotalPoor / (double)u.TotalPupil : 0;
            });
            return lstCapacityStatisticsOfProvince;
        }

        /// <summary>
        /// CreateSGDCapacityJudgeSubjectStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashkey">The input parameter hashkey.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 10/12/2012   2:36 PM
        /// </remarks>
        public List<CapacityStatisticsBO> CreateSGDCapacityJudgeSubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");

            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");

            List<CapacityStatisticsBO> lsMSBDBO = SearchByProvince(SearchInfo);
            lsMSBDBO = lsMSBDBO.OrderBy(o => o.DistrictName).ToList();
            if (lsMSBDBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("SGD_TH_TongHopHocLucMonNhanXet");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A11", "K11");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "K11");
            sheet.Name = "TH_HLM";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP HỌC LỰC MÔN NHẬN XÉT - " + thisSubject.SubjectName.ToUpper();
            if (Semester == 0)
            {
                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            }
            string semesterAndYear = ReportUtils.ConvertSemesterForReportName(Semester) + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSBDBO.Count(); j++)
            {
                // Set style
                sheet.CopyPasteSameSize(templateRange, "A" + (j + 11).ToString());
                CapacityStatisticsBO item = lsMSBDBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["DistrictName"] = item.DistrictName;
                innerDic["TotalSchool"] = item.TotalSchool;
                innerDic["STT"] = j + 1;
                innerDic["TotalPupil"] = item.TotalPupil;

                innerDic["TotalAPlus"] = item.TotalExcellent;
                innerDic["PercentAPlus"] = Math.Round(100.0 * item.PercentExcellent.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalA"] = item.TotalNormal;
                innerDic["PercentA"] = Math.Round(100.0 * item.PercentNormal.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalB"] = item.TotalWeak;
                innerDic["PercentB"] = Math.Round(100.0 * item.PercentWeak.Value, 2, MidpointRounding.AwayFromZero);
                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "SGD_TH_TongHopHocLucMonNhanXet";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //SGD_TH_TongHopHocLucMonNhanXet_[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }

            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID = 0
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",0},
                {"SupervisingDeptID",SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSBDBO;
        }
        /// <summary>
        /// CreatePGDCapacityJudgeSubjectStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashkey">The input parameter hashkey.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 10/12/2012   2:36 PM
        /// </remarks>
        public List<CapacityStatisticsBO> CreatePGDCapacityJudgeSubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");

            List<CapacityStatisticsBO> lsMSOSBO = SearchBySupervisingDept(SearchInfo);
            if (lsMSOSBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("PGD_TH_TongHopHocLucMonNhanXet");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;


            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A11", "K11");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "K11");
            sheet.Name = "TH_HLM";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP HỌC LỰC MÔN NHẬN XÉT - " + thisSubject.SubjectName.ToUpper();
            string strSemester = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "Học kỳ " + Semester;
            }
            else
            {
                strSemester = "Cả năm";
            }
            string semesterAndYear = strSemester + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSOSBO.Count(); j++)
            {
                // set style
                sheet.CopyPasteSameSize(templateRange, "A" + (j + 11).ToString());
                CapacityStatisticsBO item = lsMSOSBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["School"] = item.SchoolName;
                innerDic["TotalPupil"] = item.TotalPupil;

                innerDic["TotalAPlus"] = item.TotalExcellent;
                innerDic["PercentAPlus"] = Math.Round(100.0 * item.PercentExcellent.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalA"] = item.TotalNormal;
                innerDic["PercentA"] = Math.Round(100.0 * item.PercentNormal.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["TotalB"] = item.TotalWeak;
                innerDic["PercentB"] = Math.Round(100.0 * item.PercentWeak.Value, 2, MidpointRounding.AwayFromZero);

                innerDic["STT"] = j + 1;

                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "PGD_TH_TongHopHocLucMonNhanXet";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //PGD_TH_TongHopHocLucMonNhanXet _[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }
            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",DistrictID},
                 {"SupervisingDeptID",SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSOSBO;
        }

        #endregion


        public IQueryable<CapacityStatistic> Search(IDictionary<string, object> SearchInfo)
        {

            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");

            IQueryable<CapacityStatistic> lstCapacityStatistic = CapacityStatisticBusiness.All;

            if (SchoolID != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (Year != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.Year == Year);
            }
            if (ReportCode.Length != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.ReportCode.ToLower().Contains(ReportCode.ToLower()));
            }
            if (Semester != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.Semester == Semester);
            }
            if (SubjectID != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.SubjectID == SubjectID);
            }
            if (EducationLevelID != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SubCommitteeID != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.SubCommitteeID == SubCommitteeID);
            }
            if (AppliedLevel != 0)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.AppliedLevel == AppliedLevel);
            }
            if (SentToSupervisor != false)
            {
                lstCapacityStatistic = lstCapacityStatistic.Where(o => o.SentToSupervisor == SentToSupervisor);
            }

            IQueryable<CapacityStatistic> lstCapacityStatistic1 = (from o in lstCapacityStatistic
                                                                   where o.SentDate == (from c in lstCapacityStatistic
                                                                                        where o.SchoolID == c.SchoolID && (o.Year == c.Year || Year == 0) && o.EducationLevelID == c.EducationLevelID && (o.SubCommitteeID == c.SubCommitteeID || SubCommitteeID == 0)
                                                                                        && o.SubjectID == c.SubjectID && o.Semester == c.Semester && o.ReportCode == c.ReportCode && o.AppliedLevel == c.AppliedLevel && o.IsCommenting == c.IsCommenting
                                                                                        select c.SentDate).Max()
                                                                   select o);

            return lstCapacityStatistic1;

        }

        #endregion

        #region lay du lieu bao cao moi
        public List<CapacityStatistic> GetListDataCapacity()
        {
            List<CapacityStatistic> listData = new List<CapacityStatistic>();           
            return listData;
        }

        public void GeneralData(CapacityStatisticRequestBO capacityObj)
        {
            try
            {
                #region Thong ke hoc luc (theo truong,theo cap)
                if (capacityObj.reportType == SystemParamsInFile.CAPACITY_STATISTIC_ID || capacityObj.reportType == SystemParamsInFile.CONDUCT_STATISTIC_ID)
                {
                    if (capacityObj.reportType == SystemParamsInFile.CAPACITY_STATISTIC_ID)
                    {
                        this.context.SP_CAPACITY_REPORT_ALL_SCHOOL(capacityObj.SupervisingDeptID, capacityObj.ProvinceID, capacityObj.DistrictID, capacityObj.YearID
                            , capacityObj.SemesterID, capacityObj.AppliedLevelID, capacityObj.EducationLevelID, capacityObj.IsSupervisingDept,
                            capacityObj.IsFemale, capacityObj.IsEthnic,capacityObj.IsEthnicFemale);
                    }
                    else if (capacityObj.reportType == SystemParamsInFile.CONDUCT_STATISTIC_ID)
                    {
                        this.context.SP_CONDUCT_REPORT_ALL_SCHOOL(capacityObj.SupervisingDeptID, capacityObj.ProvinceID, capacityObj.DistrictID, capacityObj.YearID
                                                   , capacityObj.SemesterID, capacityObj.AppliedLevelID, capacityObj.EducationLevelID, capacityObj.IsSupervisingDept,
                                                   capacityObj.IsFemale, capacityObj.IsEthnic, capacityObj.IsEthnicFemale);
                    }
                }
                else if (capacityObj.reportType == SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_ID
                    || capacityObj.reportType == SystemParamsInFile.CAPACITY_STATISTIC_DISTRICT_ID
                    || capacityObj.reportType == SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_ID
                    || capacityObj.reportType == SystemParamsInFile.CONDUCT_STATISTIC_DISTRICT_ID)
                {
                    List<SchoolProfileBO> listSchool = this.GetListSchool(capacityObj.IsSupervisingDept, capacityObj.ProvinceID, capacityObj.DistrictID, capacityObj.AppliedLevelID).ToList();
                    // Chay Async de Thong ke so lieu
                    Task[] arrTask = new Task[NumTask];
                    for (int z = 0; z < NumTask; z++)
                    {
                        int currentIndex = z;
                        arrTask[z] = Task.Factory.StartNew(() =>
                        {
                            SMASEntities smasContext = new SMASEntities();
                            List<SchoolProfileBO> lstSchoolIdRunTask = listSchool.Where(p => p.SchoolProfileID % NumTask == currentIndex).ToList();
                            if (lstSchoolIdRunTask != null && lstSchoolIdRunTask.Count > 0)
                            {
                                if (capacityObj.reportType == SystemParamsInFile.CAPACITY_STATISTIC_EDUCATION_LEVEL_ID)
                                {
                                    for (int i = lstSchoolIdRunTask.Count - 1; i >= 0; i--)
                                    {
                                        smasContext.SP_CAPACITY_REPORT_ALL_EDU(capacityObj.SupervisingDeptID, capacityObj.ProvinceID, lstSchoolIdRunTask[i].DistrictID.HasValue ? lstSchoolIdRunTask[i].DistrictID.Value : 0,
                                            capacityObj.YearID, capacityObj.SemesterID, capacityObj.AppliedLevelID, capacityObj.EducationLevelID, capacityObj.IsSupervisingDept,
                                            lstSchoolIdRunTask[i].SchoolProfileID, capacityObj.IsFemale, capacityObj.IsEthnic, capacityObj.IsEthnicFemale);
                                    }
                                }
                                else if (capacityObj.reportType == SystemParamsInFile.CAPACITY_STATISTIC_DISTRICT_ID)
                                {
                                    for (int i = lstSchoolIdRunTask.Count - 1; i >= 0; i--)
                                    {
                                        smasContext.SP_CAPACITY_REPORT_DISTRICT(capacityObj.SupervisingDeptID, capacityObj.ProvinceID, lstSchoolIdRunTask[i].DistrictID.HasValue ? lstSchoolIdRunTask[i].DistrictID.Value : 0,
                                            lstSchoolIdRunTask[i].SchoolProfileID, capacityObj.YearID, capacityObj.SemesterID, capacityObj.IsSupervisingDept, capacityObj.AppliedLevelID,
                                            capacityObj.IsFemale, capacityObj.IsEthnic, capacityObj.IsEthnicFemale);
                                    }
                                }
                                else if (capacityObj.reportType == SystemParamsInFile.CONDUCT_STATISTIC_EDUCATION_LEVEL_ID)
                                {
                                    for (int i = lstSchoolIdRunTask.Count - 1; i >= 0; i--)
                                    {
                                        smasContext.SP_CONDUCT_REPORT_ALL_EDU(capacityObj.SupervisingDeptID, capacityObj.ProvinceID, lstSchoolIdRunTask[i].DistrictID.HasValue ? lstSchoolIdRunTask[i].DistrictID.Value : 0,
                                            capacityObj.YearID, capacityObj.SemesterID, capacityObj.AppliedLevelID, capacityObj.EducationLevelID, capacityObj.IsSupervisingDept,
                                            lstSchoolIdRunTask[i].SchoolProfileID, capacityObj.IsFemale, capacityObj.IsEthnic, capacityObj.IsEthnicFemale);
                                    }

                                }
                                else if (capacityObj.reportType == SystemParamsInFile.CONDUCT_STATISTIC_DISTRICT_ID)
                                {
                                    for (int i = lstSchoolIdRunTask.Count - 1; i >= 0; i--)
                                    {
                                        smasContext.SP_CONDUCT_REPORT_DISTRICT(capacityObj.SupervisingDeptID, capacityObj.ProvinceID, lstSchoolIdRunTask[i].DistrictID.HasValue ? lstSchoolIdRunTask[i].DistrictID.Value : 0,
                                            lstSchoolIdRunTask[i].SchoolProfileID, capacityObj.YearID, capacityObj.SemesterID, capacityObj.IsSupervisingDept, capacityObj.AppliedLevelID,
                                            capacityObj.IsFemale, capacityObj.IsEthnic, capacityObj.IsEthnicFemale);
                                    }
                                }
                            }
                        });
                    }
                    Task.WaitAll(arrTask);
                }
                #endregion
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "GeneralData", "null", ex);
            }
        }

        public IQueryable<SchoolProfileBO> GetListSchool(bool IsSuperVisingDeptRole, int provinceId, int districtId, int appliedLevelId)
        {
            IQueryable<SchoolProfileBO> listSchool = null;
            List<int> listGradeTeriary = new List<int>{4,6,7,31};
            List<int> listGradeSecondary = new List<int>{2,3,6,7,31};
            if (appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                listSchool = (from a in SchoolProfileBusiness.All.AsNoTracking().Where(p => p.IsActive && p.IsActiveSMAS == true && listGradeTeriary.Contains(p.EducationGrade))
                              group a by new { a.SchoolProfileID, a.DistrictID, a.ProvinceID, a.SchoolName, a.District.DistrictName } into g
                              where g.Key.ProvinceID == provinceId
                              select new SchoolProfileBO
                              {
                                  SchoolProfileID = g.Key.SchoolProfileID,
                                  DistrictID = g.Key.DistrictID,
                                  ProvinceID = g.Key.ProvinceID,
                                  SchoolName = g.Key.SchoolName,
                                  DistrictName = g.Key.DistrictName
                              });
            }
            else if (appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                listSchool = (from a in SchoolProfileBusiness.All.AsNoTracking().Where(p => p.IsActive && p.IsActiveSMAS == true && listGradeSecondary.Contains(p.EducationGrade))
                              group a by new { a.SchoolProfileID, a.DistrictID, a.ProvinceID, a.SchoolName, a.District.DistrictName } into g
                              where ((IsSuperVisingDeptRole && g.Key.ProvinceID == provinceId) || (!IsSuperVisingDeptRole && g.Key.ProvinceID == provinceId && g.Key.DistrictID == districtId))
                              select new SchoolProfileBO
                              {
                                  SchoolProfileID = g.Key.SchoolProfileID,
                                  DistrictID = g.Key.DistrictID,
                                  ProvinceID = g.Key.ProvinceID,
                                  SchoolName = g.Key.SchoolName,
                                  DistrictName = g.Key.DistrictName
                              });
            }
            return listSchool;
        }

        public List<CapacityStatisticsBO> GetListCapacityAllSchool(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelID, bool IsSuperVisingDeptRole, int districtId)
        {
            IQueryable<CapacityStatisticsBO> listCapacity = (from a in CapacityStatisticBusiness.All.AsNoTracking()
                                                          where a.SupervisingDeptID == supervisingDeptId
                                                          && a.ReportCode == reportCode
                                                          && a.Year == yearId
                                                          && a.Semester == semester
                                                          && a.AppliedLevel == appliedLevelID
                                                          && ((educationLevelId > 0 && a.EducationLevelID == educationLevelId) || educationLevelId == 0)
                                                          select new CapacityStatisticsBO
                                                           {
                                                               PupilTotal = a.PupilTotal,
                                                               CapacityLevel01 = a.CapacityLevel01,
                                                               CapacityLevel02 = a.CapacityLevel02,
                                                               CapacityLevel03 = a.CapacityLevel03,
                                                               CapacityLevel04 = a.CapacityLevel04,
                                                               CapacityLevel05 = a.CapacityLevel05,
                                                               OnAverage = a.OnAverage,
                                                               SchoolName = a.SchoolProfile.SchoolName,
                                                               SchoolID = a.SchoolID,
                                                               DistrictName = a.SchoolProfile.District.DistrictName,
                                                               DistrictID = a.DistrictID,
                                                               Semester = a.Semester,
                                                               EducationLevelID = a.EducationLevelID,
                                                               IsEthnic = a.IsEthnic,
                                                               IsFemale = a.IsFemale
                                                           }).OrderBy(p => p.DistrictName).ThenBy(p => p.SchoolName); ;

            return listCapacity.ToList();
        }

        public List<CapacityStatisticsBO> GetListCapacityAllEdu(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId)
        {
            List<CapacityStatisticsBO> listResult = new List<CapacityStatisticsBO>();
            List<CapacityStatisticsBO> listDataAllSchool = this.GetListCapacityAllSchool(reportCode, yearId, semester, 0, provinceID, supervisingDeptId, appliedLevelId, IsSuperVisingDeptRole, districtId);
            if (listDataAllSchool != null && listDataAllSchool.Count > 0)
            {
                listResult = listDataAllSchool.GroupBy(n => new { n.EducationLevelID, n.IsFemale, n.IsEthnic }).
                                     Select(group =>
                                         new CapacityStatisticsBO
                                         {
                                             EducationLevelID = group.Key.EducationLevelID,
                                             IsFemale = group.Key.IsFemale,
                                             IsEthnic = group.Key.IsEthnic,
                                             PupilTotal = group.Sum(p => p.PupilTotal),
                                             CapacityLevel01 = group.Sum(p => p.CapacityLevel01),
                                             CapacityLevel02 = group.Sum(p => p.CapacityLevel02),
                                             CapacityLevel03 = group.Sum(p => p.CapacityLevel03),
                                             CapacityLevel04 = group.Sum(p => p.CapacityLevel04),
                                             CapacityLevel05 = group.Sum(p => p.CapacityLevel05),
                                             OnAverage = group.Sum(p => p.OnAverage)
                                         }).ToList();


            }
            return listResult;
        }

        public List<CapacityStatisticsBO> GetListCapacityDistrict(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId)
        {
            List<CapacityStatisticsBO> listResult = new List<CapacityStatisticsBO>();
            List<CapacityStatisticsBO> listDataAllSchool = this.GetListCapacityAllSchool(reportCode, yearId, semester, 0, provinceID, supervisingDeptId, appliedLevelId, IsSuperVisingDeptRole, districtId);
            if (listDataAllSchool != null && listDataAllSchool.Count > 0)
            {
                listResult = listDataAllSchool.GroupBy(p => new { p.DistrictID, p.DistrictName, p.IsFemale, p.IsEthnic }).
                    Select(group => new CapacityStatisticsBO
                    {
                        DistrictID = group.Key.DistrictID,
                        DistrictName = group.Key.DistrictName,
                        IsFemale = group.Key.IsFemale,
                        IsEthnic = group.Key.IsEthnic,
                        PupilTotal = group.Sum(p => p.PupilTotal),
                        CapacityLevel01 = group.Sum(p => p.CapacityLevel01),
                        CapacityLevel02 = group.Sum(p => p.CapacityLevel02),
                        CapacityLevel03 = group.Sum(p => p.CapacityLevel03),
                        CapacityLevel04 = group.Sum(p => p.CapacityLevel04),
                        CapacityLevel05 = group.Sum(p => p.CapacityLevel05),
                        OnAverage = group.Sum(p => p.OnAverage)
                    }).OrderBy(p => p.DistrictName).ToList();
            }

            return listResult;
        }

       
        
        
        #endregion
    }
}
