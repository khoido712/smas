/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    /// <summary>
    /// Author Quanglm
    /// </summary>

    public partial class TeachingTargetRegistrationBusiness
    {
        #region
        private void ValidateEntity(TeachingTargetRegistration TeachingTargetRegistration)
        {
            //// Kiem tra cac thong tin chung
            //ValidationMetadata.ValidateObject(TeachingTargetRegistration);
            //// Kiem tra TeacherID co ton tai trong he thong
            //EmployeeBusiness.CheckAvailable(TeachingTargetRegistration.TeacherID, "Employee_Label_EmployeeID", false);
            //AcademicYearBusiness.CheckAvailable(TeachingTargetRegistration.AcademicYearID, "AcademicYear_Label_AcademicYearID", false);
            //// Kiem tra mon hoc phai ton tai trong bang TeachingAssignment
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic["ClassID"] = TeachingTargetRegistration.ClassID;
            //IQueryable<TeachingAssignmentBO> ListTeachingAssignment = TeachingAssignmentBusiness.Search(dic);
            //if (ListTeachingAssignment.Count() == 0){
            //    throw new BusinessException("TeachingTargetRegistration_Label_ClassID_Err");
            //}
            //dic["SubjectID"] = TeachingTargetRegistration.SubjectID;
            //ListTeachingAssignment = TeachingAssignmentBusiness.Search(dic);
            //if (ListTeachingAssignment.Count() == 0)
            //{
            //    throw new BusinessException("TeachingTargetRegistration_Label_SubjectID_Err");
            //}

            //dic["TeacherID"] = TeachingTargetRegistration.TeacherID;
            //ListTeachingAssignment = TeachingAssignmentBusiness.Search(dic);
            //if (ListTeachingAssignment.Count() == 0)
            //{
            //    throw new BusinessException("TeachingTargetRegistration_Label_TeacherID_Err");
            //}
        }
        #endregion

        #region Them moi
        public override TeachingTargetRegistration Insert(TeachingTargetRegistration TeachingTargetRegistration)
        {
            this.ValidateEntity(TeachingTargetRegistration);
            return base.Insert(TeachingTargetRegistration);
        }
        #endregion

        #region Cap nhat
        public override TeachingTargetRegistration Update(TeachingTargetRegistration TeachingTargetRegistration)
        {
            this.ValidateEntity(TeachingTargetRegistration);
            return base.Update(TeachingTargetRegistration);
        }
        #endregion

        #region Xoa
        public void Delete(int TeachingTargetRegistrationID)
        {
            this.CheckAvailable(TeachingTargetRegistrationID, "TeachingTargetRegistration_Label_TeachingTargetRegistrationID");
            base.Delete(TeachingTargetRegistrationID);
        }
        #endregion

        #region Tim kiem
        public IQueryable<TeachingTargetRegistration> Search(IDictionary<string, object> dic)
        {
            IQueryable<TeachingTargetRegistration> ListTeachingTargetRegistration = TeachingTargetRegistrationRepository.All;
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            string FullName = Utils.GetString(dic, "FullName");
            if (ClassID != 0)
            {
                ListTeachingTargetRegistration = from ttr in ListTeachingTargetRegistration
                                                 join cp in ClassProfileBusiness.All on ttr.ClassID equals cp.ClassProfileID
                                                 where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                 && ttr.ClassID == ClassID
                                                 select ttr;
                    //ListTeachingTargetRegistration.Where(x => x.ClassID == ClassID);
            }
            if (SubjectID != 0)
            {
                ListTeachingTargetRegistration = ListTeachingTargetRegistration.Where(x => x.SubjectID == SubjectID);
            }
            if (TeacherID != 0)
            {
                ListTeachingTargetRegistration = ListTeachingTargetRegistration.Where(x => x.TeachingTargetRegistrationID == TeacherID);
            }
            if (AcademicYearID != 0)
            {
                ListTeachingTargetRegistration = ListTeachingTargetRegistration.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (FullName != null && !FullName.Equals(string.Empty))
            {
                ListTeachingTargetRegistration = ListTeachingTargetRegistration.Where(x => x.Employee.FullName.ToLower().Contains(FullName.Trim().ToLower()));
            }
            return ListTeachingTargetRegistration;

        }
        #endregion

    }
}