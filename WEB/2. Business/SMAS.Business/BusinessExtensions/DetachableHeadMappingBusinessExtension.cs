/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class DetachableHeadMappingBusiness
    {
        private IQueryable<DetachableHeadMapping> Search(IDictionary<string, object> SearchInfo)
        {
            int DetachableHeadMappingID = Utils.GetInt(SearchInfo, "DetachableHeadMappingID");
            int DetachableHeadBagID = Utils.GetInt(SearchInfo, "DetachableHeadBagID");
            int OrderNumber = Utils.GetShort(SearchInfo, "OrderNumber");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedLevel = (int)Utils.GetByte(SearchInfo, "AppliedLevel");
            string NamedListCode = Utils.GetString(SearchInfo, "NamedListCode");
            string DetachableHeadNumber = Utils.GetString(SearchInfo, "DetachableHeadNumber");
            string Description = Utils.GetString(SearchInfo, "Description");
            int? FromNamedListNumber = Utils.GetNullableInt(SearchInfo, "FromNamedListNumber");
            int? ToNamedListNumber = Utils.GetNullableInt(SearchInfo, "ToNamedListNumber");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int ExaminationID = Utils.GetInt(SearchInfo, "ExaminationID");
            int ExaminationSubjectID = Utils.GetInt(SearchInfo, "ExaminationSubjectID");
            string BagTitle = Utils.GetString(SearchInfo, "DetachableHeadNumber");
            string Prefix = Utils.GetString(SearchInfo, "DetachableHeadNumber");

            IQueryable<DetachableHeadMapping> query = this.All;
            if (DetachableHeadMappingID != 0)
            {
                query = query.Where(o => o.DetachableHeadMappingID == DetachableHeadMappingID);
            }

            if (DetachableHeadBagID != 0)
            {
                query = query.Where(o => o.DetachableHeadBagID == DetachableHeadBagID);
            }

            if (OrderNumber != 0)
            {
                query = query.Where(o => o.OrderNumber == OrderNumber);
            }

            if (EducationLevelID != 0)
            {
                query = query.Where(o => o.DetachableHeadBag.ExaminationSubject.EducationLevelID == EducationLevelID);
            }

            if (AppliedLevel != 0)
            {
                query = query.Where(o => o.DetachableHeadBag.ExaminationSubject.EducationLevel.Grade == AppliedLevel);
            }

            if (NamedListCode != "")
            {
                query = query.Where(o => o.Candidate.NamedListCode.Contains(NamedListCode));
            }

            if (DetachableHeadNumber != "")
            {
                query = query.Where(o => o.DetachableHeadNumber == DetachableHeadNumber);
            }

            if (Description != "")
            {
                query = query.Where(o => o.Description.Contains(Description));
            }

            if (FromNamedListNumber != null)
            {
                query = query.Where(o => o.Candidate.NamedListNumber >= FromNamedListNumber);
            }

            if (ToNamedListNumber != null)
            {
                query = query.Where(o => o.Candidate.NamedListNumber <= ToNamedListNumber);
            }

            if (SchoolID != 0)
            {
                query = query.Where(o => o.DetachableHeadBag.Examination.SchoolID == SchoolID);
            }

            if (ExaminationID > 0)
            {
                query = query.Where(o => o.Candidate.ExaminationID==ExaminationID);
            }

            if (ExaminationSubjectID > 0)
            {
                query = query.Where(o => o.Candidate.ExaminationSubject.ExaminationSubjectID == ExaminationSubjectID);
            }

            if (!string.IsNullOrEmpty(BagTitle))
            {
                query = query.Where(o => o.DetachableHeadBag.BagTitle==BagTitle);
            }

            if (!string.IsNullOrEmpty(Prefix))
            {
                query = query.Where(o => o.DetachableHeadBag.Prefix==Prefix);
            }

            return query;
        }
        public DetachableHeadMapping Update(int SchoolID, int AppliedLevel, int DetachableHeadMappingID, int DetachableHeadBagID, string DetachableHeadNumber)
        {
            this.CheckAvailable(DetachableHeadMappingID, "DetachableHeadMapping_Label", false);
            this.DetachableHeadBagBusiness.CheckAvailable(DetachableHeadBagID, "DetachableHeadMapping_Label_DetachableHeadBagID", false);

            DetachableHeadMapping Entity = this.Find(DetachableHeadMappingID);
            Entity.DetachableHeadBagID = DetachableHeadBagID;
            Entity.DetachableHeadBag = this.DetachableHeadBagBusiness.Find(DetachableHeadBagID);
            Entity.DetachableHeadNumber = DetachableHeadNumber;
            ValidationMetadata.ValidateObject(Entity);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = Entity.DetachableHeadBag.ExaminationID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = Entity.DetachableHeadBag.ExaminationID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            compatible = Entity.DetachableHeadBag.Examination.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED;
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Entity.DetachableHeadBag.Examination.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            base.Update(Entity);
            return Entity;
        }
        public IQueryable<DetachableHeadMapping> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
        public Stream CreateDetachableHeadMappingReport(IDictionary<string, object> dic, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.QLT_DANHPHACH);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet templateSheet = oBook.GetSheet(1);

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? educationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            int? examinationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");

            //Lay danh sach cac tui phach
            IDictionary<string, object> dicDetachableHeadBag = new Dictionary<string, object>();
            dicDetachableHeadBag["AcademicYearID"] = dic["AcademicYearID"];
            dicDetachableHeadBag["AppliedLevel"] = dic["AppliedLevel"];
            dicDetachableHeadBag["ExaminationID"] = dic["ExaminationID"];
            dicDetachableHeadBag["ExaminationSubjectID"] = dic["ExaminationSubjectID"];
            dicDetachableHeadBag["EducationLevelID"] = dic["EducationLevelID"];
            List<DetachableHeadBag> lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(schoolID, dicDetachableHeadBag).ToList();

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examination exam = ExaminationBusiness.Find(examinationID);
            AcademicYear acaYear = AcademicYearBusiness.Find(academicYearID);
            ExaminationSubject examSubject = ExaminationSubjectBusiness.Find(examinationSubjectID);
            EducationLevel eduLevel = educationLevelID.HasValue && educationLevelID.Value > 0 ? EducationLevelBusiness.Find(educationLevelID) : null;

            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["ExaminationName"] = examinationID > 0 && exam != null ? exam.Title : "Tất cả";
            dicVarable["AcademicYear"] = acaYear != null ? (acaYear.Year + " - " + (acaYear.Year + 1)) : "Tất cả";
            dicVarable["SubjectName"] = examSubject != null ? examSubject.SubjectCat.DisplayName : "Tất cả";
            dicVarable["EducationLevel"] = eduLevel != null ? eduLevel.Resolution : "Tất cả";
            
            foreach (DetachableHeadBag headBag in lstDetachableHeadBag)
            {
                IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet);
                sheet.Name = ReportUtils.StripVNSign(headBag.BagTitle);

                dicVarable["BagTitle"] = headBag.BagTitle;

                dic["DetachableHeadBagID"] = headBag.DetachableHeadBagID;
                List<DetachableHeadMapping> listDetach = Search(dic).OrderBy(u=> u.Candidate.NamedListNumber).ToList();

                //tao khung
                if (listDetach.Count > 0)
                {
                    for (int i = 0; i < listDetach.Count; i++)
                    {
                        sheet.CopyAndInsertARow(12, 13);
                    }
                }
                else
                {
                    sheet.DeleteRow(11);
                }

                #region do du lieu vao report
                int index = 1;
                List<object> listData = new List<object>();
                listDetach.ForEach(u =>
                {
                    listData.Add(new
                    {
                        Index = index++,
                        u.Candidate.NamedListCode,
                        u.DetachableHeadNumber,
                        u.Description
                    });
                });
                dicVarable["Rows"] = listData;
                sheet.FillVariableValue(dicVarable);
                #endregion
            }

            templateSheet.Delete();
            #region reportName

            string examTitle = exam != null ? ReportUtils.StripVNSign(exam.Title) : "TatCa";
            string educationLevel = educationLevelID.HasValue && educationLevelID.Value > 0 ? ReportUtils.StripVNSign(EducationLevelBusiness.Find(educationLevelID.Value).Resolution) : "TatCa";
            string subjectName = examinationSubjectID.HasValue && examinationSubjectID.Value > 0 ? ReportUtils.StripVNSign(ExaminationSubjectBusiness.Find(examinationSubjectID.Value).SubjectCat.DisplayName) : "TatCa";

            reportName = reportName.Replace("Examination", examTitle)
                                    .Replace("EducationLevel", educationLevel)
                                    .Replace("Subject", subjectName);

            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }
    }
}
