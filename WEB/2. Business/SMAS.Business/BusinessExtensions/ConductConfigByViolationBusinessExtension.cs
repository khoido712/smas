/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ConductConfigByViolationBusiness
    {

        #region Search


        /// <summary>
        /// Tìm kiếm xếp loại hạnh kiểm theo vi phạm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<ConductConfigByViolation></returns>
        public IQueryable<ConductConfigByViolation> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            int? AppliedLevel = Utils.GetNullableByte(dic, "AppliedLevel");

            IQueryable<ConductConfigByViolation> lsConductConfigByViolation = ConductConfigByViolationRepository.All;
            if (IsActive.HasValue)
            {
                lsConductConfigByViolation = lsConductConfigByViolation.Where(con => con.IsActive == IsActive);
            }
            if (SchoolID != 0)
                lsConductConfigByViolation = lsConductConfigByViolation.Where(con => con.SchoolID == SchoolID);

            if (AppliedLevel.HasValue && AppliedLevel.Value > 0)
                lsConductConfigByViolation = lsConductConfigByViolation.Where(con => con.AppliedLevel == AppliedLevel.Value);

            return lsConductConfigByViolation;
        }
        #endregion


        #region Insert


        /// <summary>
        /// Thêm thông tin xếp loại hạnh kiểm theo vi phạm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="lsConductConfigByViolation">List conductconfigbyviolation.</param>
        public void Insert(List<ConductConfigByViolation> lsConductConfigByViolation)
        {
            //Chi thuc hien voi danh sach co phan tu
            if (lsConductConfigByViolation == null || lsConductConfigByViolation.Count == 0) return;

            List<ConductLevel> lstConductLevel = ConductLevelBusiness.All.Where(u => u.IsActive.Value).ToList();

            lsConductConfigByViolation = lsConductConfigByViolation.OrderBy(u => u.ConductLevelID).ToList();

            //Neu truyen vao cac truong khac nhau thi bao loi
            if (lsConductConfigByViolation.Select(u => u.SchoolID).Distinct().Count() > 1)
                throw new BusinessException("ConductConfigByViolation_Label_MoreThanOneSchool");

            //Trường học không tồn tại hoặc đã bị xóa – Kiểm tra SchoolID với IsActive = TRUE
            SchoolProfileBusiness.CheckAvailable(lsConductConfigByViolation[0].SchoolID, "ConductConfigByViolation_Label_SchoolID", true);

            for (int i = 0; i < lsConductConfigByViolation.Count; i++)
            {
                if (i > 0)
                    Utils.ValidateCompareNumberDecimal(lsConductConfigByViolation[i].MaxValue.Value, lsConductConfigByViolation[i - 1].MinValue.Value, "ConductConfigByViolation_Label_MaxValue", "ConductConfigByViolation_Label_MinValue");

                //Trong một loại hạnh kiểm thì điểm đến phải lớn hơn điểm từ. – Kiểm tra MaxValue, MinValue.
                if (lsConductConfigByViolation[i].MaxValue.Value < lsConductConfigByViolation[i].MinValue.Value)
                    throw new BusinessException("ConductConfigByViolation_Label_Compare");

                //Số điểm nhập vào phải là số nguyên dương – Kiểm tra MaxValue, MinValue
                Utils.ValidatePositiveInteger((decimal)lsConductConfigByViolation[i].MinValue, "ConductConfigByViolation_Label_MinValue");

                //Số điểm nhập vào phải là số nguyên dương – Kiểm tra MaxValue, MinValue
                Utils.ValidatePositiveInteger((decimal)lsConductConfigByViolation[i].MaxValue, "ConductConfigByViolation_Label_MaxValue");

                if (!lstConductLevel.Any(u => u.ConductLevelID == lsConductConfigByViolation[i].ConductLevelID))
                    throw new BusinessException("ConductConfigByViolation_Label_ConductLevelID", lsConductConfigByViolation[i].ConductLevelID);

                base.Insert(lsConductConfigByViolation[i]);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa thông tin xếp loại hạnh kiểm theo vi phạm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="lsConductConfigByViolation">List conductconfigbyviolation.</param>
        public void Update(List<ConductConfigByViolation> lsConductConfigByViolation)
        {
            //Chi thuc hien voi danh sach co phan tu
            if (lsConductConfigByViolation == null || lsConductConfigByViolation.Count == 0) return;

            List<ConductLevel> lstConductLevel = ConductLevelBusiness.All.Where(u => u.IsActive.Value).ToList();

            lsConductConfigByViolation = lsConductConfigByViolation.OrderBy(u => u.ConductLevelID).ToList();

            //Neu truyen vao cac truong khac nhau thi bao loi
            if (lsConductConfigByViolation.Select(u => u.SchoolID).Distinct().Count() > 1)
                throw new BusinessException("ConductConfigByViolation_Label_MoreThanOneSchool");

            //Trường học không tồn tại hoặc đã bị xóa – Kiểm tra SchoolID với IsActive = TRUE
            SchoolProfileBusiness.CheckAvailable(lsConductConfigByViolation[0].SchoolID, "ConductConfigByViolation_Label_SchoolID", true);

            for (int i = 0; i < lsConductConfigByViolation.Count; i++)
            {
                if (i > 0)
                    Utils.ValidateCompareNumberDecimal(lsConductConfigByViolation[i].MaxValue.Value, lsConductConfigByViolation[i - 1].MinValue.Value, "ConductConfigByViolation_Label_MaxValue", "ConductConfigByViolation_Label_MinValue");

                //Trong một loại hạnh kiểm thì điểm đến phải lớn hơn điểm từ. – Kiểm tra MaxValue, MinValue.
                if (lsConductConfigByViolation[i].MaxValue.Value < lsConductConfigByViolation[i].MinValue.Value) 
                    throw new BusinessException("ConductConfigByViolation_Label_Compare");

                //Số điểm nhập vào phải là số nguyên dương – Kiểm tra MaxValue, MinValue
                Utils.ValidatePositiveInteger((decimal)lsConductConfigByViolation[i].MinValue, "ConductConfigByViolation_Label_MinValue");

                //Số điểm nhập vào phải là số nguyên dương – Kiểm tra MaxValue, MinValue
                Utils.ValidatePositiveInteger((decimal)lsConductConfigByViolation[i].MaxValue, "ConductConfigByViolation_Label_MaxValue");

                if (!lstConductLevel.Any(u => u.ConductLevelID == lsConductConfigByViolation[i].ConductLevelID))
                    throw new BusinessException("ConductConfigByViolation_Label_ConductLevelID", lsConductConfigByViolation[i].ConductLevelID);

                base.Update(lsConductConfigByViolation[i]);
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa thông tin xếp loại hạnh kiểm theo vi phạm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="lsConductConfigByViolation">List conductconfigbyviolation.</param>
        public void Delete(List<ConductConfigByViolation> lsConductConfigByViolation, int SchoolID)
        {
            foreach (ConductConfigByViolation violation in lsConductConfigByViolation)
            {
                // Kiem tra xem loai xep loai co thuoc truong
                if (violation.SchoolID != SchoolID)
                    new BusinessException("ConductConfigByViolation_Label_School_Err");
                violation.IsActive = false;
                base.Update(violation);
            }
        }
        #endregion
    }
}
