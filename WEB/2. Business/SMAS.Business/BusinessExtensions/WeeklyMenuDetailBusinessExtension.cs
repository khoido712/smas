﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class WeeklyMenuDetailBusiness
    {
        /// <summary>
        /// Tìm kiếm thực đơn tuần
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<WeeklyMenuDetail> Search(IDictionary<string, object> dic)
        {
            int? SchoolID = Utils.GetInt(dic, "SchoolID");
            int? WeeklyMenuID = Utils.GetInt(dic, "WeeklyMenuID");
            IQueryable<WeeklyMenuDetail> lstWeeklyMenuDetail = WeeklyMenuDetailRepository.All;
                                                               
            //- SchoolID: default = 0, 0 -> all ~ tìm kiếm theo WeeklyMenu(WeeklyMenuID).SchoolID
            if (SchoolID != 0)
            {        
                lstWeeklyMenuDetail = lstWeeklyMenuDetail.Where(s => (s.WeeklyMenu.SchoolID == SchoolID));
            }
            if(WeeklyMenuID != 0)
            {
                lstWeeklyMenuDetail = lstWeeklyMenuDetail.Where(s => (s.WeeklyMenuID == WeeklyMenuID));
            }
            return lstWeeklyMenuDetail;
        }
        /// <summary>
        /// Tìm kiếm thông tin chi tiết thực đơn tuần
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="SchoolID">ID trường học</param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<WeeklyMenuDetail> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }
        
    }
}