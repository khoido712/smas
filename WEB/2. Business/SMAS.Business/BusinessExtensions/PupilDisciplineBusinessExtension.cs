/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author minhh 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class PupilDisciplineBusiness
    {
        #region validate
        private void Validate(PupilDiscipline pupilDiscipline)
        {
            ValidationMetadata.ValidateObject(pupilDiscipline);

            AcademicYear academicYear = AcademicYearRepository.Find(pupilDiscipline.AcademicYearID);
            //AcademicYear(AcademicYearID). FirstSemesterStartDate  < DisciplinedDate < AcademicYear(AcademicYearID). SecondSemesterEndDate
            if (academicYear.FirstSemesterStartDate > pupilDiscipline.DisciplinedDate || academicYear.SecondSemesterEndDate < pupilDiscipline.DisciplinedDate)
            {
                throw new BusinessException("PupilDiscipline_Validate_NotAfterDate");
            }

            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                  new Dictionary<string, object>()
                {
                    {"ClassProfileID",pupilDiscipline.ClassID},
                    {"AcademicYearID",pupilDiscipline.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, ClassID: not compatible(PupilProfile)
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                  new Dictionary<string, object>()
                {
                    {"PupilProfileID",pupilDiscipline.PupilID},
                    {"CurrentClassID",pupilDiscipline.ClassID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying1");
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = pupilDiscipline.PupilID;
            SearchInfo["AcademicYearID"] = pupilDiscipline.AcademicYearID;
            SearchInfo["ClassID"] = pupilDiscipline.ClassID;
            SearchInfo["Check"] = "Check";
            PupilOfClass pupilOfClass = PupilOfClassBusiness.SearchBySchool(pupilDiscipline.SchoolID, SearchInfo).FirstOrDefault();
            if (pupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying1");
            }
            //ClassID, EducationLevelID: not compatible(ClassProfile)
            bool ClassProfileEducationLevelCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                  new Dictionary<string, object>()
                {
                    {"EducationLevelID",pupilDiscipline.EducationLevelID},
                    {"ClassProfileID",pupilDiscipline.ClassID}
                }, null);
            if (!ClassProfileEducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",pupilDiscipline.SchoolID},
                    {"AcademicYearID",pupilDiscipline.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }          
            //Kiểm tra trùng dữ liệu PupilID, DisciplineTypeID, DisciplinedDate
            bool Disduplicate = new PupilDisciplineRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilDiscipline",
                new Dictionary<string, object>()
                {
                    {"PupilID",pupilDiscipline.PupilID},
                    {"DisciplineTypeID",pupilDiscipline.DisciplineTypeID},
                    {"DisciplinedDate",pupilDiscipline.DisciplinedDate}
                },
                  new Dictionary<string, object>()
                 {
                    {"PupilDisciplineID",pupilDiscipline.PupilDisciplineID},                    
                });
            if (Disduplicate)
            {
                throw new BusinessException("Common_Validate_NotDuplicate_PupilDiscipline");
            }
            //LeavingDate <= DateTime.Now            
            Utils.ValidatePastDate(pupilDiscipline.DisciplinedDate, "PupilDiscipline_Label_DisciplinedDate");

            //AcademicYear(AcademicYearID). FirstSemesterStartDate  < DisciplinedDate < AcademicYear(AcademicYearID). SecondSemesterEndDate
            if (academicYear.FirstSemesterStartDate > pupilDiscipline.DisciplinedDate || academicYear.SecondSemesterEndDate < pupilDiscipline.DisciplinedDate)
            {                
                List<object> Params = new List<object>();
                Params.Add("PupilDiscipiline_Label_DiscipilineDate");
                Params.Add(academicYear.FirstSemesterStartDate);
                Params.Add(academicYear.SecondSemesterEndDate);
                throw new BusinessException("PupilDiscipline_DisciplineDate_Not_InAcademicYear_Time", Params);
            }

        }
        #endregion

        #region SearchPupilDiscipline
        /// <summary>
        /// Tìm kiếm thông tin học sinh bị kỉ luật
        /// </summary>
        /// <author>minhh</author>
        /// <date>10/10/2012</date>
        /// <param></param>
        private IQueryable<PupilDiscipline> Search(IDictionary<string, object> dic)
        {
            int PupilDisciplineID = Utils.GetInt(dic, "PupilDisciplineID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetShort(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int DisciplineTypeID = Utils.GetShort(dic, "DisciplineTypeID");
            bool IsRecordedInSchoolReport = Utils.GetBool(dic, "IsRecordedInSchoolReport");
            int DisciplineLevel = (int)Utils.GetByte(dic, "DisciplineLevel");
            DateTime? DisciplinedDate = Utils.GetDateTime(dic, "DisciplinedDate");
            DateTime? FromDisciplinedDate = Utils.GetDateTime(dic, "FromDisciplinedDate");
            DateTime? ToDisciplinedDate = Utils.GetDateTime(dic, "ToDisciplinedDate");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            List<int> lstClassID = Utils.GetIntList(dic, "ListClassID");

            IQueryable<PupilDiscipline> Query = from p in this.PupilDisciplineRepository.All
                                                join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                where q.IsActive == true
                                                select p;
            if (ClassID != 0)
            {
                Query = from pd in Query
                        join cp in ClassProfileBusiness.All on pd.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && pd.ClassID == ClassID
                        select pd;
            }
            if (PupilDisciplineID != 0)
            {
                Query = Query.Where(o => o.PupilDisciplineID == PupilDisciplineID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }

            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (DisciplineTypeID != 0)
            {
                Query = Query.Where(o => o.DisciplineTypeID == DisciplineTypeID);
            }
            if (DisciplineLevel != 0)
            {
                Query = Query.Where(o => o.DisciplineLevel == DisciplineLevel);
            }
            //if (IsRecordedInSchoolReport != null)
            //{
            //    Query = Query.Where(o => o.IsRecordedInSchoolReport == IsRecordedInSchoolReport);
            //}
            if (DisciplinedDate.HasValue)
            {
                Query = Query.Where(o => o.DisciplinedDate == DisciplinedDate);
            }
            if (FromDisciplinedDate.HasValue)
            {
                Query = Query.Where(o => o.DisciplinedDate >= FromDisciplinedDate);
            }
            if (ToDisciplinedDate.HasValue)
            {
                Query = Query.Where(o => o.DisciplinedDate <= ToDisciplinedDate);
            }
            if (PupilCode.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            }
            if (FullName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }    
            if (lstClassID.Count != 0) {
                Query = Query.Where(o => lstClassID.Contains(o.ClassID) == true);
            }
            Query = Query.OrderBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);
            return Query;
        }
        #endregion

        #region SearchBySchool
        /// <summary>
        /// Tìm kiếm thông tin kỉ luật học sinh theo trường
        /// <author>minhh</author>
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<PupilDiscipline> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0)
                dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
        #endregion

        #region delete
        /// <summary>
        /// Xóa thông tin học sinh bị kỉ luật
        /// </summary>
        /// <author>minhh</author>
        /// <date>10/10/2012</date>
        /// <param>id kỉ luật cần xóa, id người dùng, id trường</param>
        public void DeletePupilDiscipline(int UserID, int PupilDisciplineID, int SchoolID)
        {

            PupilDiscipline pupilDiscipline = PupilDisciplineRepository.Find(PupilDisciplineID);
            if (!AcademicYearBusiness.IsCurrentYear(pupilDiscipline.AcademicYearID))
            {
                throw new BusinessException("PupilDiscipline_Validate_IsNotCurrentYear");
            }

            // Kiểm tra trạng thái học sinh vi phạm kỉ luật
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
            if (pupilDiscipline.PupilID != 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = pupilDiscipline.AcademicYearID;
                SearchInfo["ClassID"] = pupilDiscipline.ClassID;
                SearchInfo["Check"] = "Check";
                SearchInfo["PupilID"] = pupilDiscipline.PupilID;
                List<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(pupilDiscipline.SchoolID, SearchInfo).ToList();
                //List<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All.Where(o => (o.PupilID == pupilDiscipline.PupilID && o.AcademicYearID == pupilDiscipline.AcademicYearID && o.SchoolID == pupilDiscipline.SchoolID && o.ClassID == pupilDiscipline.ClassID)).ToList();
                foreach (PupilOfClass item in lstPupilOfClass)
                {
                    if (item.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        throw new BusinessException("PupilDiscipline_Validate_PupilNotWorking1");
                    }
                }
            }

            //PupilID, SchoolID: not compatible(PupilProfile)
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                  new Dictionary<string, object>()
                {
                    {"PupilProfileID",pupilDiscipline.PupilID},
                    {"CurrentSchoolID",SchoolID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //PupilDisciplineID: PK(PupilDiscipline)
            new PupilDisciplineBusiness(null).CheckAvailable(PupilDisciplineID, "PupilDiscipline_Label_PupilDiscipline");

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilDiscipline.ClassID))
            {
                base.Delete(PupilDisciplineID);
            }
        }
        #endregion

        #region insert
        /// <summary>
        /// Thêm thông tin học sinh bị kỉ luật
        /// </summary>
        /// <author>minhh</author>
        /// <date>10/10/2012</date>
        /// <param></param>
        /// 
        public void InsertPupilDiscipline(int UserID, PupilDiscipline pupilDiscipline)
        {
            this.Validate(pupilDiscipline);
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilDiscipline.ClassID))
            {
                base.Insert(pupilDiscipline);
            }
        }
        #endregion

        #region update
        /// <summary>
        /// Sửa thông tin học sinh bị kỉ luật
        /// </summary>
        /// <author>minhh</author>
        /// <date>10/10/2012</date>
        /// <param></param>
        /// 
        public void UpdatePupilDiscipline(int UserID, PupilDiscipline pupilDiscipline)
        {
            this.Validate(pupilDiscipline);
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilDiscipline.ClassID))
            {
                base.Update(pupilDiscipline);
            }
        }
        #endregion
    }
}
