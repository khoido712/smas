﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class MovementAcceptanceBusiness
    {
        #region validate
        void Validate(MovementAcceptance Entity)
        {
            ValidationMetadata.ValidateObject(Entity);
            // If(MovedFromClassID != null): FK(ClassProfile)
            if (Entity.MovedFromClassID.HasValue)
            {
                bool MovedFromClassCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"ClassProfileID", Entity.MovedFromClassID.Value}
                });
                if (!MovedFromClassCompatible)
                {
                    List<object> Params = new List<object>();
                    Params.Add("MovementAcceptance_Label_MovedFromClassID");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);
                }
            }

            // If(MovedFromSchoolID != null): FK(SchoolProfile)
            if (Entity.MovedFromSchoolID.HasValue)
            {
                bool MovedFromClassCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                new Dictionary<string, object>()
                {
                    {"SchoolProfileID", Entity.MovedFromSchoolID.Value}
                });
                if (!MovedFromClassCompatible)
                {
                    List<object> Params = new List<object>();
                    Params.Add("MovementAcceptance_Label_MovedFromSchoolID");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);
                }
            }
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
              new Dictionary<string, object>()
                {
                    {"SchoolID", Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"ClassProfileID", Entity.ClassID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, ClassID: Not compatible(PupilProfile)
            //bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
            //   new Dictionary<string, object>()
            //    {
            //        {"PupilProfileID", Entity.PupilID},
            //        {"CurrentClassID", Entity.MovedFromClassID}
            //    }, null);
            //if (!PupilProfileCompatible)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}
            //ClassID, EducationLevelID: not compatible (ClassProfile)
            bool EducationLevelCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"EducationLevelID", Entity.EducationLevelID},
                    {"ClassProfileID",Entity.ClassID}
                }, null);
            if (!EducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm m?i thông tin ti?p nh?n h?c sinh chuy?n d?n
        /// <author>minhh</author>
        /// <date>16/10/2012</date>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="entity"></param>
        public void InsertMovementAcceptance(int UserID, MovementAcceptance entity, bool IsInHistory)
        {

            AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
            int partitionId = UtilsBusiness.GetPartionId(ay.SchoolID);

            Dictionary<string, object> searchInfo = new Dictionary<string, object>();
            searchInfo["ClassID"] = entity.ClassID;
            searchInfo["AcademicYearID"] = entity.AcademicYearID;
            searchInfo["PupilID"] = entity.PupilID;
            //this.Validate(entity);
            //If( UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID) = False): Không cho phép thêm m?i
            //Else Th?c hi?n bu?c 2 => 6
            //Th?c hi?n insert vào b?ng MovementAcceptance
            //C?p nh?t EnrolmentDate, CurrentSchoolID và CurrentClassID trong PupilProfile
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID))
            {
                return;
            }
                base.Insert(entity);

                PupilProfile pupilProfile = PupilProfileBusiness.Find(entity.PupilID);
                pupilProfile.CurrentSchoolID = entity.SchoolID;
                pupilProfile.CurrentClassID = entity.ClassID;
                pupilProfile.CurrentAcademicYearID = entity.AcademicYearID;
                pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                pupilProfile.EnrolmentDate = DateTime.Now;
                PupilProfileBusiness.Update(pupilProfile);

                List<PupilOfSchool> lstpoc = PupilOfSchoolBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "PupilID", entity.PupilID } }).ToList();
                PupilOfSchoolBusiness.DeleteAll(lstpoc);

                PupilOfSchool newPupilOfSchool = new PupilOfSchool();
                newPupilOfSchool.PupilID = entity.PupilID;
                newPupilOfSchool.SchoolID = entity.SchoolID;
                newPupilOfSchool.EnrolmentDate = DateTime.Now;
                newPupilOfSchool.EnrolmentType = SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL;

                PupilOfSchoolBusiness.Insert(newPupilOfSchool);

                PupilOfClass newPupilOfClass = new PupilOfClass();
                newPupilOfClass.PupilID = entity.PupilID;
                newPupilOfClass.AcademicYearID = entity.AcademicYearID;
                newPupilOfClass.SchoolID = entity.SchoolID;
                newPupilOfClass.ClassID = entity.ClassID;
                newPupilOfClass.Year = ay.Year;
                newPupilOfClass.AssignedDate = DateTime.Now;
                newPupilOfClass.Description = entity.Description;
                newPupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                PupilOfClass lastPoc = PupilOfClassBusiness.All.Where(o => o.ClassID == entity.ClassID
                    && o.PupilProfile.IsActive && o.OrderInClass.HasValue).OrderByDescending(o => o.OrderInClass).FirstOrDefault();
                int maxOrderInClass = 0;
                if (lastPoc != null)
                {
                    maxOrderInClass = lastPoc.OrderInClass.Value;
                }
                //Gán entity.OrderInClass = m + 1,Th?c hi?n insert vào b?ng PupilOfClass
                newPupilOfClass.OrderInClass = (int)(maxOrderInClass + 1);
                PupilOfClassBusiness.Insert(newPupilOfClass);

            //Xoá di?m ? tru?ng m?i n?u có
            #region xoa diem o truong moi
            if (IsInHistory)
            {
                List<MarkRecordHistory> lstMarkRecordDelete = MarkRecordHistoryBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                                                                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                                                                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
                if (lstMarkRecordDelete.Count > 0)
                {
                    MarkRecordHistoryBusiness.DeleteAll(lstMarkRecordDelete);
                }
                List<JudgeRecordHistory> lstJudgeRecordDelete = JudgeRecordHistoryBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                                                                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                                                                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
                if (lstJudgeRecordDelete.Count > 0)
                {
                    JudgeRecordHistoryBusiness.DeleteAll(lstJudgeRecordDelete);
                }
                List<SummedUpRecordHistory> lstSummedUpRecordDelete = SummedUpRecordHistoryBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                                                                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                                                                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
                if (lstSummedUpRecordDelete.Count > 0)
                {
                    SummedUpRecordHistoryBusiness.DeleteAll(lstSummedUpRecordDelete);
                }
            }
            else
            {
                List<MarkRecord> lstMarkRecordDelete = MarkRecordBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                                                                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                                                                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
                if (lstMarkRecordDelete.Count > 0)
                {
                    MarkRecordBusiness.DeleteAll(lstMarkRecordDelete);
                }
                List<JudgeRecord> lstJudgeRecordDelete = JudgeRecordBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                                                                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                                                                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
                if (lstJudgeRecordDelete.Count > 0)
                {
                    JudgeRecordBusiness.DeleteAll(lstJudgeRecordDelete);
                }
                List<SummedUpRecord> lstSummedUpRecordDelete = SummedUpRecordBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                                                                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                                                                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
                if (lstSummedUpRecordDelete.Count > 0)
                {
                    SummedUpRecordBusiness.DeleteAll(lstSummedUpRecordDelete);
                }
            }



            List<VPupilRanking> lstPupilRankingDelete = VPupilRankingBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionId).ToList();
            if (lstPupilRankingDelete.Count > 0)
            {
                VPupilRankingBusiness.DeleteAll(lstPupilRankingDelete);
            }
            /*List<PupilEmulation> lstPupilEmulationDelete = PupilEmulationBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                && o.PupilID == entity.PupilID).ToList();*/
            //Chiendd1: 30/07/2015, thay doi thanh ham SearchBySchool
            List<PupilEmulation> lstPupilEmulationDelete = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, searchInfo).ToList();
            if (lstPupilEmulationDelete.Count > 0)
            {
                PupilEmulationBusiness.DeleteAll(lstPupilEmulationDelete);
            }
            List<ConductEvaluationDetail> lstConductEvaluationDetailDelete = ConductEvaluationDetailBusiness.All.Where(o => o.SchoolID == entity.SchoolID
                && o.AcademicYearID == entity.AcademicYearID && o.ClassID == entity.ClassID
                && o.PupilID == entity.PupilID).ToList();
            if (lstConductEvaluationDetailDelete.Count > 0)
            {
                ConductEvaluationDetailBusiness.DeleteAll(lstConductEvaluationDetailDelete);
            }
            #endregion

            //T?o Dictionary(string, object) theo: PupilID = entity.PupilID, ClassID = entity.MovedFromClassID, SchoolID = entity.MovedFromSchoolID;
            //r?i g?i hàm MarkRecordBusiness.Search(Dictionary) và JudgeRecordBusiness.Search(Dictionary)
            //    Danh sách di?m c?a h?c sinh ? tru?ng cu.
            //C?p nh?t b?n ghi l?i danh sách di?m này theo ClassID = entity.ClassID, SchoolID = entity.SchoolID, AcademicYearID = entity.AcademicYearID

            #region markRecord
            int partitionMovedId = UtilsBusiness.GetPartionId(entity.MovedFromSchoolID.Value);
            if (IsInHistory)
            {
                List<MarkRecordHistory> lstMarkRecord = MarkRecordHistoryBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                                                                                   && o.ClassID == entity.MovedFromClassID
                                                                                   && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).ToList();
                if (lstMarkRecord.Count != 0)
                {
                    MarkRecordHistory newMarkRecord = new MarkRecordHistory();
                    foreach (MarkRecordHistory markRecord in lstMarkRecord)
                    {

                        //Cap nhat IsOldData = true
                        markRecord.IsOldData = true;
                        MarkRecordHistoryBusiness.Update(markRecord);
                        //Them diem vao lop moi
                        newMarkRecord = new MarkRecordHistory();
                        newMarkRecord.PupilID = markRecord.PupilID;
                        newMarkRecord.ClassID = entity.ClassID;
                        newMarkRecord.SchoolID = entity.SchoolID;
                        newMarkRecord.AcademicYearID = entity.AcademicYearID;
                        newMarkRecord.SubjectID = markRecord.SubjectID;
                        newMarkRecord.MarkTypeID = markRecord.MarkTypeID;
                        newMarkRecord.CreatedAcademicYear = markRecord.CreatedAcademicYear;
                        newMarkRecord.Semester = markRecord.Semester;
                        newMarkRecord.Title = markRecord.Title;
                        newMarkRecord.Mark = markRecord.Mark;
                        newMarkRecord.OrderNumber = markRecord.OrderNumber;
                        newMarkRecord.MarkedDate = markRecord.MarkedDate;
                        newMarkRecord.CreatedDate = markRecord.CreatedDate;
                        newMarkRecord.ModifiedDate = markRecord.ModifiedDate;
                        newMarkRecord.IsOldData = false;
                        newMarkRecord.OldMark = markRecord.OldMark;
                        newMarkRecord.IsSMS = markRecord.IsSMS;

                        MarkRecordHistoryBusiness.Insert(newMarkRecord);
                    }
                }
            }
            else
            {
                List<MarkRecord> lstMarkRecord = MarkRecordBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                                                                                   && o.ClassID == entity.MovedFromClassID
                                                                                   && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).ToList();
                if (lstMarkRecord.Count != 0)
                {
                    MarkRecord newMarkRecord = new MarkRecord();
                    foreach (MarkRecord markRecord in lstMarkRecord)
                    {

                        //Cap nhat IsOldData = true
                        markRecord.IsOldData = true;
                        MarkRecordBusiness.Update(markRecord);
                        //Them diem vao lop moi
                        newMarkRecord = new MarkRecord();
                        newMarkRecord.PupilID = markRecord.PupilID;
                        newMarkRecord.ClassID = entity.ClassID;
                        newMarkRecord.SchoolID = entity.SchoolID;
                        newMarkRecord.AcademicYearID = entity.AcademicYearID;
                        newMarkRecord.SubjectID = markRecord.SubjectID;
                        newMarkRecord.MarkTypeID = markRecord.MarkTypeID;
                        newMarkRecord.CreatedAcademicYear = markRecord.CreatedAcademicYear;
                        newMarkRecord.Semester = markRecord.Semester;
                        newMarkRecord.Title = markRecord.Title;
                        newMarkRecord.Mark = markRecord.Mark;
                        newMarkRecord.OrderNumber = markRecord.OrderNumber;
                        newMarkRecord.MarkedDate = markRecord.MarkedDate;
                        newMarkRecord.CreatedDate = markRecord.CreatedDate;
                        newMarkRecord.ModifiedDate = markRecord.ModifiedDate;
                        newMarkRecord.IsOldData = false;
                        newMarkRecord.OldMark = markRecord.OldMark;
                        newMarkRecord.IsSMS = markRecord.IsSMS;

                        MarkRecordBusiness.Insert(newMarkRecord);
                    }
                }
            }
            #endregion

            #region judgerecord
            if (IsInHistory)
            {
                List<JudgeRecordHistory> lstJudgeRecord = JudgeRecordHistoryBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                && o.ClassID == entity.MovedFromClassID
                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).ToList();
                if (lstJudgeRecord.Count != 0)
                {
                    JudgeRecordHistory newJudgeRecord = new JudgeRecordHistory();
                    foreach (JudgeRecordHistory judgeRecord in lstJudgeRecord)
                    {

                        //Cap nhat IsOldData = true
                        judgeRecord.IsOldData = true;
                        JudgeRecordHistoryBusiness.Update(judgeRecord);

                        //Them diem vao lop moi
                        newJudgeRecord = new JudgeRecordHistory();
                        newJudgeRecord.PupilID = judgeRecord.PupilID;
                        newJudgeRecord.ClassID = entity.ClassID;
                        newJudgeRecord.SchoolID = entity.SchoolID;
                        newJudgeRecord.AcademicYearID = entity.AcademicYearID;
                        newJudgeRecord.SubjectID = judgeRecord.SubjectID;
                        newJudgeRecord.MarkTypeID = judgeRecord.MarkTypeID;
                        newJudgeRecord.CreatedAcademicYear = judgeRecord.CreatedAcademicYear;
                        newJudgeRecord.Semester = judgeRecord.Semester;
                        newJudgeRecord.Title = judgeRecord.Title;
                        newJudgeRecord.Judgement = judgeRecord.Judgement;
                        newJudgeRecord.OrderNumber = judgeRecord.OrderNumber;
                        newJudgeRecord.MarkedDate = judgeRecord.MarkedDate;
                        newJudgeRecord.CreatedDate = judgeRecord.CreatedDate;
                        newJudgeRecord.ModifiedDate = judgeRecord.ModifiedDate;
                        newJudgeRecord.IsOldData = false;
                        newJudgeRecord.OldJudgement = judgeRecord.OldJudgement;
                        newJudgeRecord.IsSMS = judgeRecord.IsSMS;

                        JudgeRecordHistoryBusiness.Insert(newJudgeRecord);
                    }
                }
            }
            else
            {
                List<JudgeRecord> lstJudgeRecord = JudgeRecordBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                && o.ClassID == entity.MovedFromClassID
                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).ToList();
                if (lstJudgeRecord.Count != 0)
                {
                    JudgeRecord newJudgeRecord = new JudgeRecord();
                    foreach (JudgeRecord judgeRecord in lstJudgeRecord)
                    {

                        //Cap nhat IsOldData = true
                        judgeRecord.IsOldData = true;
                        JudgeRecordBusiness.Update(judgeRecord);

                        //Them diem vao lop moi
                        newJudgeRecord = new JudgeRecord();
                        newJudgeRecord.PupilID = judgeRecord.PupilID;
                        newJudgeRecord.ClassID = entity.ClassID;
                        newJudgeRecord.SchoolID = entity.SchoolID;
                        newJudgeRecord.AcademicYearID = entity.AcademicYearID;
                        newJudgeRecord.SubjectID = judgeRecord.SubjectID;
                        newJudgeRecord.MarkTypeID = judgeRecord.MarkTypeID;
                        newJudgeRecord.CreatedAcademicYear = judgeRecord.CreatedAcademicYear;
                        newJudgeRecord.Semester = judgeRecord.Semester;
                        newJudgeRecord.Title = judgeRecord.Title;
                        newJudgeRecord.Judgement = judgeRecord.Judgement;
                        newJudgeRecord.OrderNumber = judgeRecord.OrderNumber;
                        newJudgeRecord.MarkedDate = judgeRecord.MarkedDate;
                        newJudgeRecord.CreatedDate = judgeRecord.CreatedDate;
                        newJudgeRecord.ModifiedDate = judgeRecord.ModifiedDate;
                        newJudgeRecord.IsOldData = false;
                        newJudgeRecord.OldJudgement = judgeRecord.OldJudgement;
                        newJudgeRecord.IsSMS = judgeRecord.IsSMS;

                        JudgeRecordBusiness.Insert(newJudgeRecord);
                    }
                }
            }
            #endregion

            #region summeduprecord
            if (IsInHistory)
            {
                List<SummedUpRecordHistory> lstSummedUpRecord = SummedUpRecordHistoryBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                                                                                                               && o.ClassID == entity.MovedFromClassID
                                                                                                               && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).Where(o => o.PeriodID == null).ToList();
                if (lstSummedUpRecord.Count != 0)
                {
                    SummedUpRecordHistory newSummedUpRecord = new SummedUpRecordHistory();
                    foreach (SummedUpRecordHistory summedUpRecord in lstSummedUpRecord)
                    {
                        //Cap nhat IsOldData = true
                        summedUpRecord.IsOldData = true;
                        SummedUpRecordHistoryBusiness.Update(summedUpRecord);
                        //Them diem vao lop moi
                        newSummedUpRecord = new SummedUpRecordHistory();
                        newSummedUpRecord.PupilID = summedUpRecord.PupilID;
                        newSummedUpRecord.ClassID = entity.ClassID;
                        newSummedUpRecord.SchoolID = entity.SchoolID;
                        newSummedUpRecord.AcademicYearID = entity.AcademicYearID;
                        newSummedUpRecord.SubjectID = summedUpRecord.SubjectID;
                        newSummedUpRecord.IsCommenting = summedUpRecord.IsCommenting;
                        newSummedUpRecord.CreatedAcademicYear = summedUpRecord.CreatedAcademicYear;
                        newSummedUpRecord.Semester = summedUpRecord.Semester;
                        newSummedUpRecord.PeriodID = null;
                        newSummedUpRecord.SummedUpMark = summedUpRecord.SummedUpMark;
                        newSummedUpRecord.JudgementResult = summedUpRecord.JudgementResult;
                        newSummedUpRecord.ReTestMark = summedUpRecord.ReTestMark;
                        newSummedUpRecord.ReTestJudgement = summedUpRecord.ReTestJudgement;
                        newSummedUpRecord.Comment = summedUpRecord.Comment;
                        newSummedUpRecord.SummedUpDate = summedUpRecord.SummedUpDate;
                        newSummedUpRecord.IsOldData = false;

                        SummedUpRecordHistoryBusiness.Insert(newSummedUpRecord);
                    }
                }
            }
            else
            {
                List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                   && o.ClassID == entity.MovedFromClassID
                   && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).Where(o => o.PeriodID == null).ToList();
                if (lstSummedUpRecord.Count != 0)
                {
                    SummedUpRecord newSummedUpRecord = new SummedUpRecord();
                    foreach (SummedUpRecord summedUpRecord in lstSummedUpRecord)
                    {
                        //Cap nhat IsOldData = true
                        summedUpRecord.IsOldData = true;
                        SummedUpRecordBusiness.Update(summedUpRecord);
                        //Them diem vao lop moi
                        newSummedUpRecord = new SummedUpRecord();
                        newSummedUpRecord.PupilID = summedUpRecord.PupilID;
                        newSummedUpRecord.ClassID = entity.ClassID;
                        newSummedUpRecord.SchoolID = entity.SchoolID;
                        newSummedUpRecord.AcademicYearID = entity.AcademicYearID;
                        newSummedUpRecord.SubjectID = summedUpRecord.SubjectID;
                        newSummedUpRecord.IsCommenting = summedUpRecord.IsCommenting;
                        newSummedUpRecord.CreatedAcademicYear = summedUpRecord.CreatedAcademicYear;
                        newSummedUpRecord.Semester = summedUpRecord.Semester;
                        newSummedUpRecord.PeriodID = null;
                        newSummedUpRecord.SummedUpMark = summedUpRecord.SummedUpMark;
                        newSummedUpRecord.JudgementResult = summedUpRecord.JudgementResult;
                        newSummedUpRecord.ReTestMark = summedUpRecord.ReTestMark;
                        newSummedUpRecord.ReTestJudgement = summedUpRecord.ReTestJudgement;
                        newSummedUpRecord.Comment = summedUpRecord.Comment;
                        newSummedUpRecord.SummedUpDate = summedUpRecord.SummedUpDate;
                        newSummedUpRecord.IsOldData = false;

                        SummedUpRecordBusiness.Insert(newSummedUpRecord);
                    }
                }
            }
            #endregion

            #region pupilRanking
            List<PupilRanking> lstPupilRanking = PupilRankingBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                && o.ClassID == entity.MovedFromClassID
                && o.PupilID == entity.PupilID && o.Last2digitNumberSchool == partitionMovedId).Where(o => o.PeriodID == null).ToList();
            if (lstPupilRanking.Count != 0)
            {
                PupilRanking newPupilRanking = new PupilRanking();
                foreach (PupilRanking pupilRanking in lstPupilRanking)
                {

                    //Cap nhat IsOldData = true
                    pupilRanking.IsOldData = true;
                    PupilRankingBusiness.Update(pupilRanking);
                    //Them diem vao lop moi
                    newPupilRanking = new PupilRanking();
                    newPupilRanking.PupilID = pupilRanking.PupilID;
                    newPupilRanking.ClassID = entity.ClassID;
                    newPupilRanking.SchoolID = entity.SchoolID;
                    newPupilRanking.AcademicYearID = entity.AcademicYearID;
                    newPupilRanking.EducationLevelID = pupilRanking.EducationLevelID;
                    newPupilRanking.CreatedAcademicYear = pupilRanking.CreatedAcademicYear;
                    newPupilRanking.Semester = pupilRanking.Semester;
                    newPupilRanking.PeriodID = null;
                    newPupilRanking.AverageMark = pupilRanking.AverageMark;
                    newPupilRanking.TotalAbsentDaysWithoutPermission = pupilRanking.TotalAbsentDaysWithoutPermission;
                    newPupilRanking.TotalAbsentDaysWithPermission = pupilRanking.TotalAbsentDaysWithPermission;
                    newPupilRanking.CapacityLevelID = pupilRanking.CapacityLevelID;
                    newPupilRanking.ConductLevelID = pupilRanking.ConductLevelID;
                    newPupilRanking.Rank = pupilRanking.Rank;
                    newPupilRanking.RankingDate = pupilRanking.RankingDate;
                    newPupilRanking.StudyingJudgementID = pupilRanking.StudyingJudgementID;
                    newPupilRanking.IsCategory = pupilRanking.IsCategory;
                    newPupilRanking.IsOldData = false;

                    PupilRankingBusiness.Insert(newPupilRanking);
                }
            }
            #endregion

            #region pupilEmulation
            Dictionary<string, object> searchEmulationMovedFromSchoolID = new Dictionary<string, object>();
            searchInfo["ClassID"] = entity.MovedFromClassID;
            searchInfo["PupilID"] = entity.PupilID;

            List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.AllNoTracking.Where(o => o.SchoolID == entity.MovedFromSchoolID
                && o.ClassID == entity.MovedFromClassID
                && o.PupilID == entity.PupilID
                && o.Last2digitNumberSchool == partitionId).ToList();
            //List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.MovedFromSchoolID.Value, searchEmulationMovedFromSchoolID).ToList();
            if (lstPupilEmulation.Count != 0)
            {
                PupilEmulation newPupilEmulation = new PupilEmulation();
                foreach (PupilEmulation pupilEmulation in lstPupilEmulation)
                {
                    //Them diem vao lop moi
                    newPupilEmulation = new PupilEmulation();
                    newPupilEmulation.PupilID = pupilEmulation.PupilID;
                    newPupilEmulation.ClassID = entity.ClassID;
                    newPupilEmulation.SchoolID = entity.SchoolID;
                    newPupilEmulation.AcademicYearID = entity.AcademicYearID;
                    newPupilEmulation.Year = pupilEmulation.Year;
                    newPupilEmulation.Semester = pupilEmulation.Semester;
                    newPupilEmulation.HonourAchivementTypeID = pupilEmulation.HonourAchivementTypeID;
                    newPupilEmulation.CreatedDate = pupilEmulation.CreatedDate;
                    newPupilEmulation.Last2digitNumberSchool = partitionId;
                    PupilEmulationBusiness.Insert(newPupilEmulation);
                }
            }
            #endregion


            #region conductEvaluationDetail
            List<ConductEvaluationDetail> lstConductEvaluationDetail = ConductEvaluationDetailBusiness.All.Where(o => o.SchoolID == entity.MovedFromSchoolID
                && o.ClassID == entity.MovedFromClassID
                && o.PupilID == entity.PupilID).ToList();
            if (lstConductEvaluationDetail.Count != 0)
            {
                ConductEvaluationDetail newConductEvaluationDetail = new ConductEvaluationDetail();
                foreach (ConductEvaluationDetail conductEvaluationDetail in lstConductEvaluationDetail)
                {
                    //Them diem vao lop moi
                    newConductEvaluationDetail = new ConductEvaluationDetail();
                    newConductEvaluationDetail.PupilID = conductEvaluationDetail.PupilID;
                    newConductEvaluationDetail.ClassID = entity.ClassID;
                    newConductEvaluationDetail.SchoolID = entity.SchoolID;
                    newConductEvaluationDetail.AcademicYearID = entity.AcademicYearID;

                    newConductEvaluationDetail.Semester = conductEvaluationDetail.Semester;
                    newConductEvaluationDetail.Task1Evaluation = conductEvaluationDetail.Task1Evaluation;
                    newConductEvaluationDetail.Task2Evaluation = conductEvaluationDetail.Task2Evaluation;
                    newConductEvaluationDetail.Task3Evaluation = conductEvaluationDetail.Task3Evaluation;
                    newConductEvaluationDetail.Task4Evaluation = conductEvaluationDetail.Task4Evaluation;
                    newConductEvaluationDetail.Task5Evaluation = conductEvaluationDetail.Task5Evaluation;
                    newConductEvaluationDetail.EvaluatedDate = conductEvaluationDetail.EvaluatedDate;
                    newConductEvaluationDetail.UpdatedDate = conductEvaluationDetail.UpdatedDate;

                    ConductEvaluationDetailBusiness.Insert(newConductEvaluationDetail);
                }
            }
            #endregion


        }
        #endregion

        #region Search
        /// <summary>
        /// T́m ki?m thông tin ti?p nh?n h?c sinh chuy?n d?n
        /// <author>minhh</author>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<MovementAcceptance> Search(IDictionary<string, object> dic = null)
        {
            int MovementAcceptanceID = Utils.GetInt(dic, "MovementAcceptanceID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int Semester = Utils.GetInt(dic, "Semester");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetShort(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            DateTime? MovedDate = Utils.GetDateTime(dic, "MovedDate");
            int? MovedFromClassID = Utils.GetNullableInt(dic, "MovedFromClassID");
            int? MovedFromSchoolID = Utils.GetNullableInt(dic, "MovedFromSchoolID");
            string MovedFromClassName = Utils.GetString(dic, "MovedFromClassName");
            string MovedFromSchoolName = Utils.GetString(dic, "MovedFromSchoolName");
            IQueryable<MovementAcceptance> Query = this.MovementAcceptanceRepository.All;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (ClassID != 0)
            {
                Query = from ma in Query
                        join cp in ClassProfileBusiness.All on ma.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && ma.ClassID == ClassID
                        select ma;
            }
            if (MovementAcceptanceID != 0)
            {
                Query = Query.Where(o => o.MovementAcceptanceID == MovementAcceptanceID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }

            if (AppliedLevel != 0)
            {
                Query = Query.Where(o => o.EducationLevel.Grade == AppliedLevel);
            }


            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (Semester != 0 && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && aca != null)
                {
                    Query = Query.Where(o => o.MovedDate >= aca.FirstSemesterStartDate && o.MovedDate <= aca.FirstSemesterEndDate);
                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && aca != null)
                {
                    Query = Query.Where(o => o.MovedDate >= aca.SecondSemesterStartDate && o.MovedDate <= aca.SecondSemesterEndDate);
                }
            }
            if (MovedDate != null)
            {
                Query = Query.Where(o => o.MovedDate == MovedDate);
            }
            if (MovedFromClassID.HasValue)
            {
                Query = Query.Where(o => o.MovedFromClassID == MovedFromClassID);
            }
            if (MovedFromSchoolID.HasValue)
            {
                Query = Query.Where(o => o.MovedFromSchoolID == MovedFromSchoolID);
            }
            if (MovedFromClassName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.MovedFromClassName.ToLower().Contains(MovedFromClassName.ToLower()));
            }
            if (MovedFromSchoolName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.MovedFromSchoolName.ToLower().Contains(MovedFromSchoolName.ToLower()));
            }
            return Query;
        }
        #endregion

        #region SearchBySchool
        /// <summary>
        /// T́m ki?m thông tin ti?p nh?n h?c sinh chuy?n d?n theo tru?ng
        /// <author>minhh</author>
        /// <date>16/10/2012</date>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MovementAcceptance> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0) dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
        #endregion
        #region
        /// <summary>
        /// Tamhm1
        /// Date: 21/11/2012
        /// L?y ra danh sách h?c sinh chuy?n d?n 
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        public List<MovementAcceptance> GetListMovementAcceptance(int AcademicYearID, int SchoolID, int Semester)
        {
            if (AcademicYearID == 0) return null;
            if (Semester == 0) return null;
            //var lstMovementAcceptance = from p in MovementAcceptanceRepository.All
            //                            join pp in PupilProfileRepository.All on p.PupilID equals pp.PupilProfileID
            //                            join pos in PupilOfSchoolRepository.All on p.PupilID equals pos.PupilID
            //                            where p.AcademicYearID == AcademicYearID
            //                            && p.SchoolID == SchoolID
            //                            && p.Semester == Semester
            //                                && pp.CurrentAcademicYearID == AcademicYearID
            //                                && pp.CurrentSchoolID == SchoolID
            //                                && pos.AcademicYearID == AcademicYearID
            //                                && pos.SchoolID == SchoolID
            //                            && pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
            //                            && pos.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL
            //                            select p;

            var lsPoS = PupilOfSchoolRepository.All.Where(o => o.SchoolID == SchoolID)
                .Where(o => o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).Select(o => o.PupilID);
            var lstMovementAcceptance = MovementAcceptanceRepository.All
                .Where(o => o.AcademicYearID == AcademicYearID)
                //.Where(o => o.Semester == Semester)
                .Where(o => o.SchoolID == SchoolID)
                .Where(o => o.PupilProfile.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                .Where(o => lsPoS.Contains(o.PupilID));
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstMovementAcceptance = lstMovementAcceptance.Where(o => o.Semester == Semester);
            }
            if (lstMovementAcceptance.ToList().Count() == 0)
                return null;
            return lstMovementAcceptance.ToList();
        }
        #endregion
    }
}
