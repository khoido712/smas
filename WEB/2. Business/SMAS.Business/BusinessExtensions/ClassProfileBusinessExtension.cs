/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author quanglm
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public enum RoleOfClass
    {
        /// <summary>
        /// Chu nhiem
        /// </summary>
        HeadTeacher,
        /// <summary>
        /// Lam thay chu nhiem
        /// </summary>
        HeadTeacherSub,
        /// <summary>
        /// Giam thi
        /// </summary>
        Overseeing,
        /// <summary>
        /// Giao vien bo mon
        /// </summary>
        SubjectTeacher,
        /// <summary>
        /// Khong có Role nào trong cac Role tren
        /// </summary>
        None
    }

    public partial class ClassProfileBusiness
    {
        #region Trả về mã nhóm lớp theo mã lớp
        public int GetEducationLevelIDByClassID(int ClassID)
        {
            int educationLevelId = 0;
            ClassProfile obj = new ClassProfile();
            obj = this.ClassProfileBusiness.All.FirstOrDefault(x => x.ClassProfileID == ClassID);
            if (obj != null)
            {
                educationLevelId = obj.EducationLevelID;
                return educationLevelId;
            }

            return 0;
        }
        #endregion

        #region Kiem tra du lieu dau vao
        private void ValidateEntity(ClassProfile ClassProfile)
        {
            // Kiem tra cac thong tin chung trong doi tuong
            Validate(ClassProfile);
            if (ClassProfile.IsCombinedClass)
            {
                string combinedCode = ClassProfile.CombinedClassCode == null ? string.Empty : ClassProfile.CombinedClassCode.Trim();
                Utils.ValidateRequire(combinedCode, "ClassProfile_Control_CombileCode");
                Utils.ValidateMaxLength(combinedCode, 100, "ClassProfile_Control_CombileCode");
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            IDictionary<string, object> dicEx = new Dictionary<string, object>();
            dic["DisplayName"] = ClassProfile.DisplayName;
            dic["SchoolID"] = ClassProfile.SchoolID;
            dic["AcademicYearID"] = ClassProfile.AcademicYearID;
            dic["IsActive"] = true;
            if (ClassProfile.ClassProfileID != 0)
            {
                dicEx["ClassProfileID"] = ClassProfile.ClassProfileID;
            }
            if (ClassProfile.ClassProfileID == 0)
            {
                bool ClassExist = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", dic, dicEx);

                if (ClassExist)
                {
                    List<object> listParam = new List<object>();
                    listParam.Add("ClassProfile_Control_DisplayNameDuplicated");
                    throw new BusinessException("ClassProfile_Validate_Duplicate", listParam);
                }
            }

            // Kiem tra bat buoc phai chon mon nghe pho thong, doi skynet update lai DB
            // Kiem tra mon nghe pho thong phai ton tai trong he thong, doi skynet update lai DB
            if (ClassProfile.ApprenticeshipGroupID.HasValue)
            {
                ApprenticeshipGroup ApprenticeshipGroup = ApprenticeshipGroupBusiness.Find(ClassProfile.ApprenticeshipGroupID.Value);
                if (ApprenticeshipGroup == null || !ApprenticeshipGroup.IsActive.HasValue || !ApprenticeshipGroup.IsActive.Value)
                {
                    List<object> listParam = new List<object>();
                    listParam.Add("SubjectCat_Label_IsApprenticeshipSubject");
                    throw new BusinessException("Common_Validate_NotExist", listParam);
                }
            }
            // Kiem tra phai chon ngoai ngu 1 truoc khi chon ngoai ngu 2    
            if (ClassProfile.ClassSubjects != null)
            {
                List<ClassSubject> ListClassSubject = ClassProfile.ClassSubjects.ToList();
                var countFl1 = ListClassSubject.Where(a => a.SubjectCat.IsForeignLanguage == true &&
                    (!a.IsSecondForeignLanguage.HasValue || a.IsSecondForeignLanguage == false)).Count();
                if (countFl1 == 0)
                {
                    var countFl2 = ListClassSubject.Where(a => a.SubjectCat.IsForeignLanguage == true &&
                    (a.IsSecondForeignLanguage.HasValue & a.IsSecondForeignLanguage == true)).Count();
                    if (countFl2 > 0)
                    {
                        List<object> Params = new List<object>();
                        throw new BusinessException("ClassProfile_Err_FL1NextFL2", Params);
                    }
                }
            }
            // Kiem tra khong duoc chon ngoai ngu 1 va ngoai ngu 2 trung nhau
            if (ClassProfile.FirstForeignLanguageID.HasValue && ClassProfile.SecondForeignLanguageID.HasValue)
            {
                if (ClassProfile.FirstForeignLanguageID.Value.Equals(ClassProfile.SecondForeignLanguageID.Value) && ClassProfile.FirstForeignLanguageID != 0)
                {
                    throw new BusinessException("ClassProfile_Label_DuplicateForeignLang");
                }
            }

        }
        #endregion

        #region Lay ra danh sach cac lop co quyen phu trach
        /// <summary>
        /// Lay ra danh sach cac lop co quyen phu trach
        /// </summary>
        /// <modifier>HaiVT 31/05/2013</modifier>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<ClassOfTeacherBO> SearchByTeacherPermission(IDictionary<string, object> dic)
        {
            int employeeID = Utils.GetInt(dic, "EmployeeID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            //gvcn
            IQueryable<ClassProfile> lstGVCN = ClassProfileBusiness.GetListClassByHeadTeacher(employeeID, academicYearID);
            //gvbm
            IQueryable<ClassProfile> lstGVBM = ClassProfileBusiness.GetListClassBySubjectTeacher(employeeID, academicYearID);
            //giao vu
            IQueryable<ClassProfile> lstGVGV = ClassProfileBusiness.GetListClassByTeacher(employeeID, academicYearID);
            // giam thi
            IQueryable<ClassProfile> lstGT = ClassProfileBusiness.GetListClassByOverSeeing(employeeID, academicYearID);

            IQueryable<ClassProfile> lstClassProfile = lstGVCN.Union(lstGVBM).Union(lstGVGV).Union(lstGT).Where(p => p.EducationLevel.Grade == appliedLevel);

            if (lstClassProfile != null && lstClassProfile.Count() > 0)
            {
                List<ClassOfTeacherBO> lstClassOfTeacherBO = new List<ClassOfTeacherBO>();
                if (lstGT != null && lstGT.Count() > 0)
                {
                    lstClassOfTeacherBO = (from lst in lstClassProfile
                                           join el in EducationLevelBusiness.All on lst.EducationLevelID equals el.EducationLevelID
                                           //lay ID + ten gvcn
                                           join eh in EmployeeBusiness.All on lst.HeadTeacherID equals eh.EmployeeID into eh_join
                                           from eh_left in eh_join.DefaultIfEmpty()
                                               //lay ID + ten gvcn lam thay
                                           join hs in HeadTeacherSubstitutionBusiness.All on lst.ClassProfileID equals hs.ClassID into hs_join
                                           from hs_left in hs_join.DefaultIfEmpty()
                                           join ehs in EmployeeBusiness.All on hs_left.SubstituedHeadTeacher equals ehs.EmployeeID into ehs_join
                                           from ehs_left in ehs_join.DefaultIfEmpty()
                                               //kiem tra co phai giam thi khong
                                           join csa in lstGT on lst.ClassProfileID equals csa.ClassProfileID into csa_join
                                           from csa_left in csa_join.DefaultIfEmpty()
                                           select new ClassOfTeacherBO
                                           {
                                               SchoolID = lst.SchoolID,
                                               ClassID = lst.ClassProfileID,
                                               AppliedLevel = el.Grade,
                                               EducationLevelID = lst.EducationLevelID,
                                               Resolution = el.Resolution,
                                               DisplayName = lst.DisplayName,
                                               HeadTeacherID = eh_left.EmployeeID,
                                               SubstituedHeadTeacher = ehs_left.EmployeeID,
                                               HeadTeacherName = eh_left.FullName,
                                               SubstituedHeadTeacherName = ehs_left.FullName,
                                               ClassIDOfSupervisior = csa_left.ClassProfileID,
                                               IsVNEN = lst.IsVnenClass,
                                               SectionID = lst.Section
                                           }).Distinct().ToList();
                }
                else
                {
                    lstClassOfTeacherBO = (from lst in lstClassProfile
                                           join el in EducationLevelBusiness.All on lst.EducationLevelID equals el.EducationLevelID
                                           //lay ID + ten gvcn
                                           join eh in EmployeeBusiness.All on lst.HeadTeacherID equals eh.EmployeeID into eh_join
                                           from eh_left in eh_join.DefaultIfEmpty()
                                               //lay ID + ten gvcn lam thay
                                           join hs in HeadTeacherSubstitutionBusiness.All on lst.ClassProfileID equals hs.ClassID into hs_join
                                           from hs_left in hs_join.DefaultIfEmpty()
                                           join ehs in EmployeeBusiness.All on hs_left.SubstituedHeadTeacher equals ehs.EmployeeID into ehs_join
                                           from ehs_left in ehs_join.DefaultIfEmpty()
                                           select new ClassOfTeacherBO
                                           {
                                               SchoolID = lst.SchoolID,
                                               ClassID = lst.ClassProfileID,
                                               AppliedLevel = el.Grade,
                                               EducationLevelID = lst.EducationLevelID,
                                               Resolution = el.Resolution,
                                               DisplayName = lst.DisplayName,
                                               HeadTeacherID = eh_left.EmployeeID,
                                               SubstituedHeadTeacher = ehs_left.EmployeeID,
                                               HeadTeacherName = eh_left.FullName,
                                               SubstituedHeadTeacherName = ehs_left.FullName,
                                               ClassIDOfSupervisior = null,
                                               IsVNEN = lst.IsVnenClass,
                                               SectionID = lst.Section
                                           }).Distinct().ToList();
                }

                ClassOfTeacherBO objClassOfTeacher = null;
                List<int> listClassId = lstClassOfTeacherBO.Select(o => o.ClassID).Distinct().ToList();
                if (lstClassOfTeacherBO.Count > 0)
                {
                    IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(lstClassOfTeacherBO.First().SchoolID,
                        new Dictionary<string, object>() { { "AcademicYearID", academicYearID }, { "Status", SystemParamsInFile.PUPIL_STATUS_STUDYING } })
                        .Where(o => listClassId.Contains(o.ClassID));

                    var countPupilOfClass = listPupilOfClass.GroupBy(o => new { o.ClassID, o.PupilProfile.Genre })
                        .Select(o => new { o.Key.ClassID, o.Key.Genre, countPupil = o.Count() }).ToList();

                    for (int i = lstClassOfTeacherBO.Count - 1; i >= 0; i--)
                    {
                        objClassOfTeacher = lstClassOfTeacherBO[i];
                        var pupilOfClass = countPupilOfClass.Where(o => o.ClassID == objClassOfTeacher.ClassID);
                        objClassOfTeacher.CountOfPupil = pupilOfClass.Sum(o => o.countPupil);
                        objClassOfTeacher.CountOfMale = pupilOfClass.Where(o => o.Genre == SystemParamsInFile.GENRE_MALE).Sum(o => o.countPupil);
                        objClassOfTeacher.CountOfFemale = pupilOfClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).Sum(o => o.countPupil);
                    }
                }

                return lstClassOfTeacherBO.OrderBy(p => p.DisplayName).ToList();
            }
            else
            {
                return new List<ClassOfTeacherBO>();
            }

        }

        public List<ClassOfTeacherBO> SearchByTeacherPermissionMobile(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int employeeID = Utils.GetInt(dic, "EmployeeID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            bool isAdmid = Utils.GetBool(dic, "IsAdmin");
            bool isBGH = Utils.GetBool(dic, "IsBGH");
            bool viewAll = Utils.GetBool(dic, "ViewAll");

            IQueryable<ClassProfile> lstClassProfile, lstGVCN, lstGVBM, lstGVGV, lstGT;

            if (!isAdmid && !isBGH && !viewAll)
            {
                //gvcn
                lstGVCN = ClassProfileBusiness.GetListClassByHeadTeacher(employeeID, academicYearID);
                //gvbm
                lstGVBM = ClassProfileBusiness.GetListClassBySubjectTeacher(employeeID, academicYearID);
                //giao vu
                lstGVGV = ClassProfileBusiness.GetListClassByTeacher(employeeID, academicYearID);
                // giam thi
                lstGT = ClassProfileBusiness.GetListClassByOverSeeing(employeeID, academicYearID);

                lstClassProfile = lstGVCN.Union(lstGVBM).Union(lstGVGV).Union(lstGT).Where(p => p.EducationLevel.Grade == appliedLevel);
            }
            else
            {
                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearID;
                dic["AppliedLevel"] = appliedLevel;

                lstClassProfile = ClassProfileBusiness.SearchBySchool(schoolID, dic);
            }

            if (lstClassProfile != null && lstClassProfile.Count() > 0)
            {
                List<ClassOfTeacherBO> lstClassOfTeacherBO = new List<ClassOfTeacherBO>();
              
                lstClassOfTeacherBO = (from lst in lstClassProfile
                                        join el in EducationLevelBusiness.All on lst.EducationLevelID equals el.EducationLevelID
                                        //lay ID + ten gvcn
                                        join eh in EmployeeBusiness.All on lst.HeadTeacherID equals eh.EmployeeID into eh_join
                                        from eh_left in eh_join.DefaultIfEmpty()
                                        //lay ID + ten gvcn lam thay
                                        join hs in HeadTeacherSubstitutionBusiness.All on lst.ClassProfileID equals hs.ClassID into hs_join
                                        from hs_left in hs_join.DefaultIfEmpty()
                                        join ehs in EmployeeBusiness.All on hs_left.SubstituedHeadTeacher equals ehs.EmployeeID into ehs_join
                                        from ehs_left in ehs_join.DefaultIfEmpty()
                                        select new ClassOfTeacherBO
                                        {
                                            SchoolID = lst.SchoolID,
                                            ClassID = lst.ClassProfileID,
                                            AppliedLevel = el.Grade,
                                            EducationLevelID = lst.EducationLevelID,
                                            Resolution = el.Resolution,
                                            DisplayName = lst.DisplayName,
                                            HeadTeacherID = eh_left.EmployeeID,
                                            SubstituedHeadTeacher = ehs_left.EmployeeID,
                                            HeadTeacherName = eh_left.FullName,
                                            SubstituedHeadTeacherName = ehs_left.FullName,
                                            ClassIDOfSupervisior = null,
                                            IsVNEN = lst.IsVnenClass,
                                            SectionID = lst.Section
                                        }).Distinct().ToList();
                

                ClassOfTeacherBO objClassOfTeacher = null;
                List<int> listClassId = lstClassOfTeacherBO.Select(o => o.ClassID).Distinct().ToList();
                if (lstClassOfTeacherBO.Count > 0)
                {
                    IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(lstClassOfTeacherBO.First().SchoolID,
                        new Dictionary<string, object>() { { "AcademicYearID", academicYearID }, { "Status", SystemParamsInFile.PUPIL_STATUS_STUDYING } })
                        .Where(o => listClassId.Contains(o.ClassID));

                    var countPupilOfClass = listPupilOfClass.GroupBy(o => new { o.ClassID, o.PupilProfile.Genre })
                        .Select(o => new { o.Key.ClassID, o.Key.Genre, countPupil = o.Count() }).ToList();

                    for (int i = lstClassOfTeacherBO.Count - 1; i >= 0; i--)
                    {
                        objClassOfTeacher = lstClassOfTeacherBO[i];
                        var pupilOfClass = countPupilOfClass.Where(o => o.ClassID == objClassOfTeacher.ClassID);
                        objClassOfTeacher.CountOfPupil = pupilOfClass.Sum(o => o.countPupil);
                        objClassOfTeacher.CountOfMale = pupilOfClass.Where(o => o.Genre == SystemParamsInFile.GENRE_MALE).Sum(o => o.countPupil);
                        objClassOfTeacher.CountOfFemale = pupilOfClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).Sum(o => o.countPupil);
                    }
                }

                return lstClassOfTeacherBO.OrderBy(p => p.DisplayName).ToList();
            }
            else
            {
                return new List<ClassOfTeacherBO>();
            }

        }
        #endregion

        #region Lay ra danh sach cac lop co quyen phu trach giang day
        /// <summary>
        /// Lay ra danh sach cac lop co quyen phu trach gang day
        /// </summary>
        /// <modifier>HaiVT 31/05/2013</modifier>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<ClassOfTeacherBO> GetListTeachingClass(int academicYearID, int appliedLevel, int employeeID)
        {

            //gvbm
            IQueryable<ClassProfile> lstGVBM = ClassProfileBusiness.GetListClassBySubjectTeacher(employeeID, academicYearID);

            IQueryable<ClassProfile> lstClassProfile = lstGVBM.Where(p => p.EducationLevel.Grade == appliedLevel);

            if (lstClassProfile != null && lstClassProfile.Count() > 0)
            {
                List<ClassOfTeacherBO> lstClassOfTeacherBO = new List<ClassOfTeacherBO>();

                lstClassOfTeacherBO = (from lst in lstClassProfile
                                       join el in EducationLevelBusiness.All on lst.EducationLevelID equals el.EducationLevelID
                                       //lay ID + ten gvcn
                                       join eh in EmployeeBusiness.All on lst.HeadTeacherID equals eh.EmployeeID into eh_join
                                       from eh_left in eh_join.DefaultIfEmpty()
                                           //lay ID + ten gvcn lam thay
                                       join hs in HeadTeacherSubstitutionBusiness.All on lst.ClassProfileID equals hs.ClassID into hs_join
                                       from hs_left in hs_join.DefaultIfEmpty()
                                       join ehs in EmployeeBusiness.All on hs_left.SubstituedHeadTeacher equals ehs.EmployeeID into ehs_join
                                       from ehs_left in ehs_join.DefaultIfEmpty()
                                       select new ClassOfTeacherBO
                                       {
                                           SchoolID = lst.SchoolID,
                                           ClassID = lst.ClassProfileID,
                                           AppliedLevel = el.Grade,
                                           EducationLevelID = lst.EducationLevelID,
                                           Resolution = el.Resolution,
                                           DisplayName = lst.DisplayName,
                                           HeadTeacherID = eh_left.EmployeeID,
                                           SubstituedHeadTeacher = ehs_left.EmployeeID,
                                           HeadTeacherName = eh_left.FullName,
                                           SubstituedHeadTeacherName = ehs_left.FullName,
                                           ClassIDOfSupervisior = null,
                                           IsVNEN = lst.IsVnenClass
                                       }).ToList();

                ClassOfTeacherBO objClassOfTeacher = null;
                List<int> listClassId = lstClassOfTeacherBO.Select(o => o.ClassID).Distinct().ToList();
                if (lstClassOfTeacherBO.Count > 0)
                {
                    IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(lstClassOfTeacherBO.First().SchoolID,
                        new Dictionary<string, object>() { { "AcademicYearID", academicYearID }, { "Status", SystemParamsInFile.PUPIL_STATUS_STUDYING } })
                        .Where(o => listClassId.Contains(o.ClassID));

                    var countPupilOfClass = listPupilOfClass.GroupBy(o => new { o.ClassID, o.PupilProfile.Genre })
                        .Select(o => new { o.Key.ClassID, o.Key.Genre, countPupil = o.Count() }).ToList();

                    for (int i = lstClassOfTeacherBO.Count - 1; i >= 0; i--)
                    {
                        objClassOfTeacher = lstClassOfTeacherBO[i];
                        var pupilOfClass = countPupilOfClass.Where(o => o.ClassID == objClassOfTeacher.ClassID);
                        objClassOfTeacher.CountOfPupil = pupilOfClass.Sum(o => o.countPupil);
                        objClassOfTeacher.CountOfMale = pupilOfClass.Where(o => o.Genre == SystemParamsInFile.GENRE_MALE).Sum(o => o.countPupil);
                        objClassOfTeacher.CountOfFemale = pupilOfClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).Sum(o => o.countPupil);
                    }
                }

                return lstClassOfTeacherBO.OrderBy(p => p.DisplayName).ToList();
            }
            else
            {
                return new List<ClassOfTeacherBO>();
            }

        }
        #endregion

        #region Tim kiem
        /// <summary>
        /// Tim kiem lop hoc
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassProfile> Search(IDictionary<string, object> dic)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelId = Utils.GetInt(dic, "EducationLevelID");
            int ClassProfileID = Utils.GetInt(dic, "ClassProfileID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int HeadTeacherID = Utils.GetInt(dic, "HeadTeacherID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SchoolSubsidiaryID = Utils.GetInt(dic, "SchoolSubsidiaryID");
            int SubCommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            string DisplayName = Utils.GetString(dic, "DisplayName");
            int? UserAccountID = Utils.GetNullInt(dic, "UserAccountID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            // QuangLM bo sung de lay danh sach lop theo phan quyen cua giao vien trong truong hop ko truyen UserAccountID
            int? TeacherByRoleID = Utils.GetNullInt(dic, "TeacherByRoleID");
            int? Semester = Utils.GetNullInt(dic, "Semester");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN", false);
            IQueryable<ClassProfile> lsClassProfile = this.ClassProfileBusiness.All.Where(p => !p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value));
            List<int> listClassID = null;
            int? TeacherID = null;
            if (UserAccountID.HasValue && UserAccountID.Value != 0)
            {
                UserAccount objUserAccount = UserAccountBusiness.Find(UserAccountID);
                TeacherID = (objUserAccount == null) ? null : objUserAccount.EmployeeID;
            }
            else if (TeacherByRoleID.HasValue && TeacherByRoleID.Value > 0)
            {
                TeacherID = TeacherByRoleID.Value;
            }

            #region Lay danh sach lop hoc theo phan quyen giao vien
            if (TeacherID.HasValue && TeacherID.Value != 0)
            {
                listClassID = new List<int>();
                int Type = Utils.GetInt(dic, "Type");
                if (Type > 0)
                {
                    List<int> listClassIDBySupervisingTeacher = new List<int>();
                    // Giao vien chu nhiem + GVBM
                    List<int> listClassIDByHeadTeacher = ClassProfileBusiness.All.Where(o => o.HeadTeacherID == TeacherID && o.IsActive == true)
                       .Where(o => (SchoolID == 0 || o.SchoolID == SchoolID))
                       .Where(o => (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID)).Select(o => o.ClassProfileID).ToList();
                    IQueryable<ClassSupervisorAssignment> IQueryClassIDBySupervisingTeacher = ClassSupervisorAssignmentBusiness.All
                        .Where(o => o.TeacherID == TeacherID)
                        .Where(o => (SchoolID == 0 || o.SchoolID == SchoolID))
                        .Where(o => (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID));

                    DateTime dt = DateTime.Now.Date;
                    List<int> listClassIDByHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All
                        .Where(o => o.SubstituedHeadTeacher == TeacherID)
                        .Where(o => (SchoolID == 0 || o.SchoolID == SchoolID))
                        .Where(o => (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID))
                        .Where(o => o.EndDate >= dt)
                        .Where(o => o.AssignedDate <= dt)
                        .Select(o => o.ClassID).ToList();

                    List<ClassAssigment> lstClassAssigment = ClassAssigmentBusiness.All
                        .Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID).ToList();



                    /*List<int> listClassIDByTeachingAssignment = TeachingAssignmentBusiness.All
                        .Where(o => o.IsActive == true)
                        .Where(o => o.TeacherID == TeacherID)
                        .Where(o => (SchoolID == 0 || ((o.SchoolID == SchoolID) && o.Last2digitNumberSchool==UtilsBusiness.GetPartionId(SchoolID)))
                        .Where(o => (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID))
                        .Where(o => o.ClassID.HasValue)
                        .Where(o => (Semester == 0 || o.Semester == Semester))
                        .Select(o => o.ClassID.Value).ToList();*/
                    //Chiendd1, 07/07/2015 Bo sung dieu kien patitionId cho bang TeachingAssignment
                    if (SchoolID == 0)
                    {
                        Employee objEmployee = EmployeeBusiness.Find(TeacherID);
                        SchoolID = objEmployee.SchoolID.Value;
                    }
                    Dictionary<string, object> dicTa = new Dictionary<string, object>();
                    dicTa["AcademicYearID"] = AcademicYearID;
                    dicTa["IsActive"] = true;
                    dicTa["TeacherID"] = TeacherID;
                    dicTa["Semester"] = Semester;

                    List<int> listClassIDByTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dicTa)
                                                                .Where(o => o.ClassID.HasValue)
                                                                .Select(o => o.ClassID.Value).Distinct().ToList();

                    switch (Type)
                    {
                        case SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER:
                            // Giao vien chu nhiem
                            listClassIDByTeachingAssignment.Clear();
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER)
                                .Select(o => o.ClassID).ToList();
                            break;
                        case SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER:
                            // GVBM
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                                        .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER)
                                                        .Select(o => o.ClassID).ToList();
                            listClassIDByHeadTeacher.Clear();
                            listClassIDByHeadTeacherSubstitution.Clear();
                            break;
                        case SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING:
                            // GT
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                                        .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING)
                                                        .Select(o => o.ClassID).ToList();
                            listClassIDByHeadTeacher.Clear();
                            listClassIDByHeadTeacherSubstitution.Clear();
                            listClassIDByTeachingAssignment.Clear();
                            break;
                        case SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER:

                            // Giao vien chu nhiem + GVBM
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER
                                || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER)
                                .Select(o => o.ClassID).ToList();
                            break;
                        case SystemParamsInFile.TEACHER_ROLE_HEAD_OVERSEEING:
                            // GVCN + GT
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                                        .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                        || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING)
                                                        .Select(o => o.ClassID).ToList();
                            listClassIDByTeachingAssignment.Clear();
                            break;
                        case SystemParamsInFile.TEACHER_ROLE_SUBJECT_OVERSEEING:
                            // GVBM + GT
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                                        .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER
                                                        || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING)
                                                        .Select(o => o.ClassID).ToList();
                            listClassIDByHeadTeacher.Clear();
                            listClassIDByHeadTeacherSubstitution.Clear();
                            break;
                        case SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING:
                            // GVCN + GVBM + GT
                            listClassIDBySupervisingTeacher = IQueryClassIDBySupervisingTeacher
                                                        .Where(o => o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                            || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER
                                                            || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING)
                                                            .Select(o => o.ClassID).ToList();
                            break;
                        default:
                            listClassIDBySupervisingTeacher.Clear();
                            listClassIDByHeadTeacher.Clear();
                            listClassIDByHeadTeacherSubstitution.Clear();
                            listClassIDByTeachingAssignment.Clear();
                            break;
                    }
                    listClassID = listClassIDByHeadTeacher.Union(listClassIDByHeadTeacherSubstitution)
                        .Union(listClassIDBySupervisingTeacher).Union(listClassIDByTeachingAssignment).ToList();
                }
                else
                {
                    listClassID = ClassProfileBusiness.All.Where(o => o.IsActive == true)
                        .Where(o => (SchoolID == 0 || o.SchoolID == SchoolID))
                        .Where(o => (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID)).Select(o => o.ClassProfileID).ToList();
                }
            }
            #endregion Lay danh sach lop hoc theo phan quyen giao vien

            #region loc bo nhung lop dang ky hoc VNEN
            if (isVNEN)
            {
                lsClassProfile = lsClassProfile.Where(p => (!p.IsVnenClass.HasValue || (p.IsVnenClass.HasValue && p.IsVnenClass.Value == false)));
            }
            #endregion

            if (listClassID != null)
            {
                lsClassProfile = lsClassProfile.Where(x => listClassID.Contains(x.ClassProfileID));
            }

            if (lstClassID.Count > 0)
            {
                lsClassProfile = lsClassProfile.Where(x => lstClassID.Contains(x.ClassProfileID));
            }

            if (AppliedLevel != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.EducationLevel.Grade == AppliedLevel);
            }
            if (ClassProfileID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.ClassProfileID == ClassProfileID);
            }
            if (EducationLevelId != 0 && EducationLevelId != SystemParamsInFile.Combine_Class_ID)
            {
                lsClassProfile = lsClassProfile.Where(x => x.EducationLevelID == EducationLevelId);
            }
            if (EducationLevelId != 0 && EducationLevelId == SystemParamsInFile.Combine_Class_ID)
            {
                lsClassProfile = lsClassProfile.Where(x => x.IsCombinedClass == true);
            }

            if (AcademicYearID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (HeadTeacherID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.HeadTeacherID == HeadTeacherID);
            }
            if (SchoolID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.SchoolID == SchoolID);
            }
            if (SchoolSubsidiaryID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.SchoolSubsidiaryID == SchoolSubsidiaryID);
            }
            if (SubCommitteeID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.SubCommitteeID == SubCommitteeID);
            }
            if (!DisplayName.Equals(string.Empty))
            {
                lsClassProfile = lsClassProfile.Where(x => x.DisplayName.ToLower().Contains(DisplayName.ToLower()));
            }
            return lsClassProfile;
        }

        public IQueryable<ClassProfile> SearchTeacherTeaching(IDictionary<string, object> dic)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelId = Utils.GetInt(dic, "EducationLevelID");
            int ClassProfileID = Utils.GetInt(dic, "ClassProfileID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int HeadTeacherID = Utils.GetInt(dic, "HeadTeacherID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SchoolSubsidiaryID = Utils.GetInt(dic, "SchoolSubsidiaryID");
            int SubCommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            string DisplayName = Utils.GetString(dic, "DisplayName");
            int? UserAccountID = Utils.GetNullInt(dic, "UserAccountID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            int? TeacherByRoleID = Utils.GetNullInt(dic, "TeacherByRoleID");

            IQueryable<ClassProfile> lsClassProfile = this.ClassProfileBusiness.All.Where(p => !p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value));
            List<int> listClassID = null;
            int? TeacherID = null;
            if (UserAccountID.HasValue && UserAccountID.Value != 0)
            {
                UserAccount objUserAccount = UserAccountBusiness.Find(UserAccountID);
                TeacherID = (objUserAccount == null) ? null : objUserAccount.EmployeeID;
            }
            else if (TeacherByRoleID.HasValue && TeacherByRoleID.Value > 0)
            {
                TeacherID = TeacherByRoleID.Value;
            }

            #region Lay danh sach lop hoc theo phan quyen giao vien
            if (TeacherID.HasValue && TeacherID.Value != 0)
            {
                listClassID = new List<int>();
                int Type = Utils.GetInt(dic, "Type");
                if (Type > 0)
                {
                    // 13/04/2018
                    // Thuyen Fix: Bo sug them Giao vien giang day
                    List<int> lstTeacherTeaching = ClassAssigmentBusiness.All
                       .Where(x => x.TeacherID == TeacherID)
                       .Where(x => x.IsTeacher.HasValue)
                       .Where(x => x.IsTeacher == true)
                       .Where(x => (SchoolID == 0 || x.SchoolID == SchoolID))
                       .Where(x => x.AcademicYearID == AcademicYearID).Select(x => x.ClassID).ToList();

                    // GVCN
                    listClassID = (from cp in ClassProfileBusiness.All
                                   join cs in ClassAssigmentBusiness.All on cp.ClassProfileID equals cs.ClassID
                                   where cp.IsActive == true && cp.HeadTeacherID == TeacherID
                                   && cp.SchoolID == SchoolID && cp.AcademicYearID == AcademicYearID
                                   && cs.TeacherID == TeacherID
                                   && cs.IsHeadTeacher == true
                                   && cs.SchoolID == SchoolID
                                   && cs.AcademicYearID == AcademicYearID
                                   select cp).ToList().Select(x => x.ClassProfileID).ToList();

                    if (lstTeacherTeaching.Count() > 0)
                    {
                        listClassID.AddRange(lstTeacherTeaching);
                    }
                                 
                    if (Type == SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY) // Giáo viên phụ trách - chủ nhiệm - giảng dạy
                    {
                        // Tim kiem GVPT
                        List<int> lstClassAssigment = ClassAssigmentBusiness.All
                            .Where(x => x.TeacherID == TeacherID)
                            .Where(x => x.IsHeadTeacher == false)
                            .Where(x => (!x.IsTeacher.HasValue || (x.IsTeacher.HasValue && x.IsTeacher == false)))
                            .Where(x => (SchoolID == 0 || x.SchoolID == SchoolID))
                            .Where(x => x.AcademicYearID == AcademicYearID).Select(x => x.ClassID).ToList();

                        if (lstClassAssigment.Count() > 0)
                        {
                            listClassID.AddRange(lstClassAssigment);
                        }
                    }
                }
                else
                {
                    // admin
                    listClassID = ClassProfileBusiness.All.Where(o => o.IsActive == true)
                        .Where(o => (SchoolID == 0 || o.SchoolID == SchoolID))
                        .Where(o => (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID))
                        .Select(o => o.ClassProfileID).ToList();
                }
            }
            #endregion Lay danh sach lop hoc theo phan quyen giao vien

            if (listClassID != null)
            {
                lsClassProfile = lsClassProfile.Where(x => listClassID.Contains(x.ClassProfileID));
            }

            if (lstClassID.Count > 0)
            {
                lsClassProfile = lsClassProfile.Where(x => lstClassID.Contains(x.ClassProfileID));
            }
            if (AppliedLevel != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.EducationLevel.Grade == AppliedLevel);
            }
            if (ClassProfileID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.ClassProfileID == ClassProfileID);              
            }
            //var temp = lsClassProfile.ToList();
            if (EducationLevelId != 0 && EducationLevelId != SystemParamsInFile.Combine_Class_ID)
            {
                lsClassProfile = lsClassProfile.Where(x => x.EducationLevelID == EducationLevelId);
            }
            if (EducationLevelId != 0 && EducationLevelId == SystemParamsInFile.Combine_Class_ID)
            {
                lsClassProfile = lsClassProfile.Where(x => x.IsCombinedClass == true);
            }
            if (AcademicYearID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (HeadTeacherID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.HeadTeacherID == HeadTeacherID);
            }
            if (SchoolID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.SchoolID == SchoolID);
            }
            if (SchoolSubsidiaryID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.SchoolSubsidiaryID == SchoolSubsidiaryID);
            }
            if (SubCommitteeID != 0)
            {
                lsClassProfile = lsClassProfile.Where(x => x.SubCommitteeID == SubCommitteeID);
            }
            if (!DisplayName.Equals(string.Empty))
            {
                lsClassProfile = lsClassProfile.Where(x => x.DisplayName.ToLower().Contains(DisplayName.ToLower()));
            }
            return lsClassProfile;
        }

        public IQueryable<ClassProfile> SearchClassBySupervisingDept(Dictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int Year = Utils.GetInt(dic, "Year");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            bool isSub = Utils.GetBool(dic, "IsSub");
            //Nếu HierachyLevel = 3, ProvinceID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE) && (ProvinceID == 0))
            {
                return null;
            }

            //Nếu HierachyLevel = 5, SupervisingDeptID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE) && (SupervisingDeptID == 0))
            {
                return null;
            }
            //Neu HierachyLevel = 3 thi tim kiem theo ProvinceID, ko tim theo SupervisingDeptID
            if (HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                SupervisingDeptID = 0;
            }
            var query = from cp in ClassProfileRepository.All.Where(o => o.IsActive == true && o.EducationLevel.Grade == AppliedLevel || AppliedLevel == 0)
                        join sp in SchoolProfileRepository.All.Where(o => o.ProvinceID == ProvinceID) on cp.SchoolID equals sp.SchoolProfileID
                        select cp;

                        
            //if (ProvinceID != 0)
            //{
            //    query = query.Where(o => (o.ProvinceID == ProvinceID));
            //}
            if (DistrictID != 0 && isSub == true)
            {
                query = query.Where(o => (o.SchoolProfile.DistrictID == DistrictID));
            }
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            if (SupervisingDeptID != 0)
            {
                query = query.Where(o => (o.SchoolProfile.SupervisingDeptID == SupervisingDeptID || o.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID));
            }
            if (SchoolID != 0)
            {
                query = query.Where(o => (o.SchoolID == SchoolID));
            }
            //if (Year != 0)
            //{
            //    query = query.Where(o => (o.Year == Year));
            //}
            if (EducationLevelID != 0)
            {
                query = query.Where(o => (o.EducationLevelID == EducationLevelID));
            }
            
            query = query.Where(o => o.IsActive == true);

            return query;
        }

        #endregion

        #region Them moi
        /// <summary>
        /// Them moi vao CSDL thong tin va tinh chat cua lop
        /// </summary>
        /// <param name="ClassProfile"></param>
        /// <returns></returns>
        public override ClassProfile Insert(ClassProfile ClassProfile)
        {
            if (ClassProfile.SubCommitteeID == 0)
            {
                ClassProfile.SubCommitteeID = null;
            }
            if (ClassProfile.ClassProfileID != 0)
            {
                this.CheckConstraints(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", ClassProfile.ClassProfileID, "ClassProfile_Label_ClassProfile");
            }
            this.ValidateEntity(ClassProfile);
            ClassProfile = base.Insert(ClassProfile);
            if (ClassProfile.EducationLevelID > 12)
            {
                // Them moi ClassAssigment neu lop co giao vien chu nhiem voi cap mam non
                if (ClassProfile.HeadTeacherID.HasValue && ClassProfile.HeadTeacherID > 0)
                {
                    ClassAssigment ca = new ClassAssigment();
                    ca.SchoolID = ClassProfile.SchoolID;
                    ca.AcademicYearID = ClassProfile.AcademicYearID;
                    ca.ClassProfile = ClassProfile;
                    ca.TeacherID = ClassProfile.HeadTeacherID.Value;
                    ca.IsHeadTeacher = true;
                    ClassAssigmentBusiness.Insert(ca);
                }
            }
            //int ClassID = ClassProfile.ClassProfileID;
            // Them moi vao bang PropertyOfClass
            /*if (ClassProfile.PropertyOfClasses != null)
            {
                List<PropertyOfClass> ListPropertyOfClass = ClassProfile.PropertyOfClasses.ToList();
                foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
                {
                    PropertyOfClass.ClassID = ClassID;
                    new PropertyOfClassBusiness(null).Insert(PropertyOfClass);
                }
            }
            // Insert vao bang ClassSubject
            if (ClassProfile.ClassSubjects != null)
            {
                List<ClassSubject> ListClassSubject = ClassProfile.ClassSubjects.ToList();
                foreach (ClassSubject ClassSubject in ListClassSubject)
                {
                    ClassSubject.ClassID = ClassID;
                    new ClassSubjectBusiness(null).Insert(ClassSubject);
                }
            }*/
            return ClassProfile;
        }
        #endregion

        #region Cap nhat
        /// <summary>
        /// Cap nhat vao CSDL thong tin va tinh chat cua lop
        /// </summary>
        /// <param name="ClassProfile"></param>
        /// <returns></returns>
        ///        
        public void UpdateClassAss(ClassProfile ClassProfile)
        {
            if (ClassProfile.SubCommitteeID == 0)
            {
                ClassProfile.SubCommitteeID = null;
            }
            int ClassID = ClassProfile.ClassProfileID;
            this.ValidateEntity(ClassProfile);
            // Them moi vao bang PropertyOfClass
            //if (ClassProfile.PropertyOfClasses != null)
            //{
            //    List<PropertyOfClass> ListPropertyOfClass = ClassProfile.PropertyOfClasses.ToList();
            //    foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
            //    {
            //        PropertyOfClass.ClassID = ClassID;
            //        new PropertyOfClassBusiness(null).Insert(PropertyOfClass);
            //    }
            //}
            // Insert vao bang ClassSubject
            //if (ClassProfile.ClassSubjects != null)
            //{
            //    List<ClassSubject> ListClassSubject = ClassProfile.ClassSubjects.ToList();
            //    foreach (ClassSubject ClassSubject in ListClassSubject)
            //    {
            //        ClassSubject.SubjectCat = null;
            //        ClassSubject.ClassID = ClassID;
            //        new ClassSubjectBusiness(null).Insert(ClassSubject);
            //    }
            //}

            // Cap nhat vao CSDL           
            // Loai bo thong tin lop con
            //ClassProfile.PropertyOfClasses = null;
            //ClassProfile.ClassSubjects = null;            
            // Xoa thong tin giang day cu\
            base.Update(ClassProfile);
        }
        #endregion

        #region Dùng cho nghiệp vụ phân công phụ trách ở mầm non
        public override ClassProfile Update(ClassProfile ClassProfile)
        {
            if (ClassProfile.SubCommitteeID == 0)
            {
                ClassProfile.SubCommitteeID = null;
            }
            int ClassID = ClassProfile.ClassProfileID;
            this.ValidateEntity(ClassProfile);
            if (ClassProfile.EducationLevelID > 12) // Neu la cap mam non thi them thong tin giang day
            {
                if (!ClassProfile.HeadTeacherID.HasValue || ClassProfile.HeadTeacherID <= 0) // if head teacherID does not has value, delete all class assignment record
                {
                    var temp = this.ClassAssigmentBusiness.All.Where(ca => ca.ClassID == ClassProfile.ClassProfileID && ca.IsHeadTeacher).ToList();
                    this.ClassAssigmentBusiness.DeleteAll(temp);
                }
                else
                {
                    var classAssigment = this.ClassAssigmentBusiness.All.Where(ca => ca.ClassID == ClassProfile.ClassProfileID && ca.IsHeadTeacher).ToList();
                    if (classAssigment == null || classAssigment.Count == 0)
                    {
                        var item = new ClassAssigment()
                        {
                            SchoolID = ClassProfile.SchoolID,
                            AcademicYearID = ClassProfile.AcademicYearID,
                            ClassProfile = ClassProfile,
                            TeacherID = ClassProfile.HeadTeacherID.Value,
                            IsHeadTeacher = true,
                        };
                        this.ClassAssigmentBusiness.Insert(item);
                    }
                    else // update first class assignment and delete other class assignment record because each class should have only one class assignment for head teacher.
                    {
                        var item = classAssigment.First();
                        item.SchoolID = ClassProfile.SchoolID;
                        item.AcademicYearID = ClassProfile.AcademicYearID;
                        item.ClassProfile = ClassProfile;
                        item.TeacherID = ClassProfile.HeadTeacherID.Value;
                        item.IsHeadTeacher = true;
                        // delete class assignment record which has index greater than 0
                        this.ClassAssigmentBusiness.DeleteAll(classAssigment.Skip(1).Take(classAssigment.Count - 1).ToList());
                    }
                }
            }
            return base.Update(ClassProfile);
        }
        #endregion

        public void UpdateCustom(ClassProfile ClassProfile, IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            if (ClassProfile.SubCommitteeID == 0)
            {
                ClassProfile.SubCommitteeID = null;
            }
            int ClassID = ClassProfile.ClassProfileID;
            this.ValidateEntity(ClassProfile);

            if (ClassProfile.IsCombinedClass)
            {
                List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.All.Where(cp => cp.AcademicYearID == AcademicYearID
                                                           && cp.SchoolID == SchoolID
                                                           && cp.IsActive == true
                                                           && cp.EducationLevel.Grade == AppliedLevel
                                                           && cp.IsCombinedClass == true
                                                           && cp.ClassProfileID != ClassID
                                                           && cp.CombinedClassCode.ToUpper().Equals(ClassProfile.CombinedClassCode.ToUpper())).ToList();

                if (lstClassProfile.Count > 0)
                {                   
                    if (ClassProfile.HeadTeacherID.HasValue)
                    {
                        ClassProfile objPC = null;
                        for (int i = 0; i < lstClassProfile.Count; i++)
                        {
                            objPC = lstClassProfile[i];
                            objPC.HeadTeacherID = ClassProfile.HeadTeacherID;
                            this.ClassProfileRepository.Update(objPC);
                        }
                    }
                    else {
                        var checkIsHeadTeacher = lstClassProfile.FirstOrDefault();
                        ClassProfile.HeadTeacherID = checkIsHeadTeacher.HeadTeacherID;
                    }
                        
                }
            }

            if (ClassProfile.EducationLevelID > 12) // Neu la cap mam non thi them thong tin giang day
            {
                if (!ClassProfile.HeadTeacherID.HasValue || ClassProfile.HeadTeacherID <= 0) // if head teacherID does not has value, delete all class assignment record
                {
                    var temp = this.ClassAssigmentBusiness.All.Where(ca => ca.ClassID == ClassProfile.ClassProfileID && ca.IsHeadTeacher).ToList();
                    this.ClassAssigmentBusiness.DeleteAll(temp);
                }
                else
                {
                    var classAssigment = this.ClassAssigmentBusiness.All.Where(ca => ca.ClassID == ClassProfile.ClassProfileID && ca.IsHeadTeacher).ToList();
                    if (classAssigment == null || classAssigment.Count == 0)
                    {
                        var item = new ClassAssigment()
                        {
                            SchoolID = ClassProfile.SchoolID,
                            AcademicYearID = ClassProfile.AcademicYearID,
                            ClassProfile = ClassProfile,
                            TeacherID = ClassProfile.HeadTeacherID.Value,
                            IsHeadTeacher = true,
                        };
                        this.ClassAssigmentBusiness.Insert(item);
                    }
                    else // update first class assignment and delete other class assignment record because each class should have only one class assignment for head teacher.
                    {
                        var item = classAssigment.First();
                        item.SchoolID = ClassProfile.SchoolID;
                        item.AcademicYearID = ClassProfile.AcademicYearID;
                        item.ClassProfile = ClassProfile;
                        item.TeacherID = ClassProfile.HeadTeacherID.Value;
                        item.IsHeadTeacher = true;
                        // delete class assignment record which has index greater than 0
                        this.ClassAssigmentBusiness.DeleteAll(classAssigment.Skip(1).Take(classAssigment.Count - 1).ToList());
                    }
                }
            }
            this.ClassProfileRepository.Update(ClassProfile);
            this.ClassProfileRepository.Save();
            this.ClassAssigmentBusiness.Save();
            //base.Update(ClassProfile);
        }
        

        #region Xoa
        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="ClassProfileID"></param>
        public void Delete(int ClassProfileID, int AcademicYearID, int AppliedLevel, RESTORE_DATA objRestore, int? UserID = 0)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                ClassProfile ClassProfile = this.Find(ClassProfileID);
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["CurrentClassID"] = ClassProfileID;
                dic["ClassID"] = ClassProfileID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["SchoolID"] = ClassProfile.SchoolID;
                dic["Year"] = academicYear.Year;
                // Kiem tra dieu kien nam hoc
                if (ClassProfile.AcademicYearID != AcademicYearID)
                {
                    throw new BusinessException("ClassProfile_Label_AcademicYear_Err");
                }
                // Kiem tra cap hoc
                if (ClassProfile.EducationLevel == null || ClassProfile.EducationLevel.Grade != (int)AppliedLevel)
                {
                    throw new BusinessException("ClassProfile_Label_AppliedLevel_Err");
                }

                //Xoa hoc sinh
                List<int> lstPupilID = PupilOfClassBusiness.All.Where(p => p.ClassID == ClassProfileID).Select(p => p.PupilID).Distinct().ToList();
                List<RESTORE_DATA_DETAIL> lstRestoreDetail = PupilProfileBusiness.DeleteListPupilProfile(UserID.Value, lstPupilID, ClassProfile.SchoolID, AcademicYearID, objRestore);
                //Xoa mon hoc cho lop
                //List<ClassSubject> ListID2 = ClassSubjectBusiness.SearchBySchool(ClassProfile.SchoolID, dic).ToList();
                //ClassSubjectBusiness.DeleteAll(ListID2);
                this.CheckAvailable(ClassProfileID, "ClassProfile_Label_ClassProfile");
                // cap nhat truong isActive = false
                ClassProfile.IsActive = false;
                this.ClassProfileBusiness.Update(ClassProfile);

                //Truong hop chua co HS
                if (lstRestoreDetail == null)
                {
                    lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();
                }
                int partitionId = UtilsBusiness.GetPartionId(objRestore.SCHOOL_ID);

                RestoreClassProfileBO objClassRes = new RestoreClassProfileBO
                {
                    ClassId = ClassProfile.ClassProfileID,
                    ClassName = ClassProfile.DisplayName,
                    ReplaceClassName = "",
                    SqlUndo = RestoreDataConstant.SQL_UNDO_CLASS
                };
                RESTORE_DATA_DETAIL objRestoreDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = objRestore.ACADEMIC_YEAR_ID,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    IS_VALIDATE = 1,
                    LAST_2DIGIT_NUMBER_SCHOOL = partitionId,
                    ORDER_ID = 2,
                    RESTORE_DATA_ID = objRestore.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRestore.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = objRestore.SCHOOL_ID,
                    SQL_DELETE = "",
                    SQL_UNDO = JsonConvert.SerializeObject(objClassRes),
                    TABLE_NAME = RestoreDataConstant.TABLE_CLASS_PROFILE
                };
                lstRestoreDetail.Add(objRestoreDetail);
                //Luu hanh dong xoa
                if (lstRestoreDetail != null)
                {
                    RestoreDataBusiness.Insert(objRestore);
                    RestoreDataBusiness.Save();
                    RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
                }

            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("AcademicYearID={0}, ClassProfileID={1}, AppliedLevel={2}", AcademicYearID, ClassProfileID, AppliedLevel);
                LogExtensions.ErrorExt(logger, DateTime.Now, "Delete", paramList, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        #endregion

        #region Tim kiem lop hoc theo truong
        /// <summary>
        /// Tim kiem lop hoc theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassProfile> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        /// <summary>
        /// Tìm kiếm giáo viên giảng dạy và phụ trách
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassProfile> SearchTeacherTeachingBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.SearchTeacherTeaching(SearchInfo);
        }
        #endregion

        #region Tim kiem lop hoc theo nam hoc
        /// <summary>
        /// Tim kiem lop hoc theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassProfile> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null)
        {
            if (AcademicYearID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["AcademicYearID"] = AcademicYearID;
            return this.Search(SearchInfo);
        }
        #endregion

        #region ham kiem tra co phai la gvcn ko
        public bool IsHeadTeacher(int TeacherID, int AcademicYearID, int ClassID)
        {
            IQueryable<ClassProfile> lsClassProfile = Search(new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"HeadTeacherID",TeacherID},
                {"ClassProfileID",ClassID}
            });
            if (lsClassProfile.Count() > 0)
            {
                return true;
            }

            //IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID},
            //    {"TeacherID",TeacherID},
            //    {"ClassID",ClassID },
            //    {"PermissionLevel",GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER}
            //});
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.TeacherID == TeacherID)).Where(o => (o.ClassID == ClassID))
                .Where(o => (o.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER));
            if (lsClassSupervisorAssignment.Count() > 0)
            {
                return true;
            }

            //IQueryable<HeadTeacherSubstitution> lsHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID  },
            //    {"HeadTeacherID",TeacherID},
            //    {"ClassID",ClassID }
            //});
            IQueryable<HeadTeacherSubstitution> lsHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All
                .Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.HeadTeacherID == TeacherID))
                .Where(o => (o.ClassID == ClassID));

            if (lsHeadTeacherSubstitution.Count() > 0)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region danh sach giao vien co quyen cn
        /// <summary>
        /// Lấy danh sách lớp mà giáo viên có quyền chủ nhiệm
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <param name="AcademicYearID"></param>
        /// <returns></returns>
        public IQueryable<ClassProfile> GetListClassByHeadTeacher(int TeacherID, int AcademicYearID)
        {
            IQueryable<ClassProfile> lsClassProfile = Search(new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"HeadTeacherID",TeacherID}
            });

            List<int> lsClassIDinClassProfile = lsClassProfile.Select(o => o.ClassProfileID).ToList();
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All
                        .Where(o => o.AcademicYearID == AcademicYearID && o.TeacherID == TeacherID && o.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER);

            List<int> lsClassIDinClassSupervisorAssignment = lsClassSupervisorAssignment.Select(o => o.ClassID).ToList();

            IQueryable<HeadTeacherSubstitution> lsHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All
                                                .Where(o => o.AcademicYearID == AcademicYearID && o.HeadTeacherID == TeacherID);

            List<int> lsClassIDinHeadTeacherSubstitution = lsHeadTeacherSubstitution.Select(o => o.ClassID).ToList();

            List<int> tempList = lsClassIDinClassProfile.Union(lsClassIDinClassSupervisorAssignment).ToList();

            List<int> listAll = tempList.Union(lsClassIDinHeadTeacherSubstitution).ToList();

            return ClassProfileBusiness.All.Where(o => listAll.Contains(o.ClassProfileID) && o.IsActive == true);
        }
        #endregion

        #region Kiểm tra có phải GVBM không.
        public bool IsSubjectTeacher(int TeacherID, int AcademicYearID, int ClassID)
        {
            //IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID},
            //    {"TeacherID",TeacherID},
            //    {"ClassID",ClassID },
            //    {"PermissionLevel",GlobalConstants.SUPERVISING_PERMISSION_SUBJECT_TEACHER}
            //});
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.TeacherID == TeacherID)).Where(o => (o.ClassID == ClassID))
                .Where(o => (o.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_SUBJECT_TEACHER));

            if (lsClassSupervisorAssignment.Count() > 0)
            {
                return true;
            }

            //IQueryable<TeachingAssignmentBO> lsTeachingAssignment = TeachingAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID  },
            //    {"HeadTeacherID",TeacherID},
            //    {"ClassID",ClassID }
            //});
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            Dictionary<string, object> dicTa = new Dictionary<string, object>();
            dicTa["AcademicYearID"] = AcademicYearID;
            dicTa["IsActive"] = true;
            dicTa["TeacherID"] = TeacherID;
            dicTa["ClassID"] = ClassID;

            IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(objAcademicYear.SchoolID, dicTa);
            /*IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentRepository.All
                .Where(o => (o.AcademicYearID == AcademicYearID))
                 .Where(o => (o.TeacherID == TeacherID))
                  .Where(o => (o.ClassID == ClassID));*/

            if (lsTeachingAssignment.Count() > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Lấy danh sách các lớp mà giáo viên có quyền GVBM
        public IQueryable<ClassProfile> GetListClassBySubjectTeacher(int TeacherID, int AcademicYearID)
        {
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All
                .Where(o => o.AcademicYearID == AcademicYearID && o.TeacherID == TeacherID && o.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_SUBJECT_TEACHER);

            List<int> lsClassIDinClassSupervisorAssignment = lsClassSupervisorAssignment.Select(o => (o.ClassID)).ToList();

            IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All
               .Where(o => o.AcademicYearID == AcademicYearID && o.TeacherID == TeacherID && o.IsActive == true);

            List<int> lsClassIDinTeachingAssignment = lsTeachingAssignment.Select(o => (o.ClassID.Value)).ToList();
            List<int> listAll = lsClassIDinClassSupervisorAssignment.Union(lsClassIDinTeachingAssignment).ToList();
            IQueryable<ClassProfile> lsClassProfileResult = ClassProfileBusiness.All.Where(o => listAll.Contains(o.ClassProfileID));

            return lsClassProfileResult;
        }
        #endregion


        #region Kiểm tra có phải Giám thị không.
        public bool IsOverseeingTeacher(int TeacherID, int AcademicYearID, int ClassID)
        {
            //IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID},
            //    {"TeacherID",TeacherID},
            //    {"ClassID",ClassID },
            //    {"PermissionLevel",GlobalConstants.SUPERVISING_PERMISSION_OVERSEEING}
            //});
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.TeacherID == TeacherID)).Where(o => (o.ClassID == ClassID))
                .Where(o => (o.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_OVERSEEING));

            if (lsClassSupervisorAssignment.Count() > 0)
            {
                return true;
            }
            return false;
        }
        #endregion


        #region Lấy danh sách các lớp mà giáo viên có quyền giám thị
        public IQueryable<ClassProfile> GetListClassByOverSeeing(int TeacherID, int AcademicYearID)
        {
            //IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID},
            //    {"TeacherID",TeacherID},
            //    {"PermissionLevel",GlobalConstants.SUPERVISING_PERMISSION_OVERSEEING}
            //});
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.TeacherID == TeacherID))
                .Where(o => (o.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_OVERSEEING));

            List<int> lsClassIDinClassSupervisorAssignment = lsClassSupervisorAssignment.Select(o => (o.ClassID)).ToList();
            IQueryable<ClassProfile> lsClassProfileResult = ClassProfileBusiness.All.Where(o => lsClassIDinClassSupervisorAssignment.Contains(o.ClassProfileID) && o.IsActive == true);

            return lsClassProfileResult;
        }
        #endregion

        #region Lấy danh sách các lớp mà giáo viên có quyền giáo vụ
        public IQueryable<ClassProfile> GetListClassByTeacher(int TeacherID, int AcademicYearID)
        {
            IQueryable<ClassProfile> lsClassProfile = Search(new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"HeadTeacherID",TeacherID}
            });

            List<int> lsClassIDinClassProfile = lsClassProfile.Select(o => (o.ClassProfileID)).ToList();

            //IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID},
            //    {"TeacherID",TeacherID}
            //});
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentBusiness.All.Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.TeacherID == TeacherID));

            List<int> lsClassIDinClassSupervisorAssignment = lsClassSupervisorAssignment.Select(o => (o.ClassID)).ToList();

            //IQueryable<HeadTeacherSubstitution> lsHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID  },
            //    {"HeadTeacherID",TeacherID}
            //});
            IQueryable<HeadTeacherSubstitution> lsHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All
                .Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.HeadTeacherID == TeacherID))
               ;

            List<int> lsClassIDinHeadTeacherSubstitution = lsHeadTeacherSubstitution.Select(o => (o.ClassID)).ToList();

            //IQueryable<TeachingAssignmentBO> lsTeachingAssignment = TeachingAssignmentBusiness.Search(new Dictionary<string, object>()
            //{
            //    {"AcademicYearID",AcademicYearID  },
            //    {"HeadTeacherID",TeacherID}
            //});
            IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All
               .Where(o => (o.AcademicYearID == AcademicYearID))
                .Where(o => (o.TeacherID == TeacherID))
                .Where(o => o.IsActive)
                ;

            List<int> lsClassIDinTeachingAssignment = lsTeachingAssignment.Select(o => (o.ClassID.Value)).ToList();


            List<int> tempList1 = lsClassIDinClassProfile.Union(lsClassIDinClassSupervisorAssignment).ToList();
            List<int> tempList2 = lsClassIDinTeachingAssignment.Union(lsClassIDinHeadTeacherSubstitution).ToList();
            List<int> listAll = tempList1.Union(tempList2).ToList();

            IQueryable<ClassProfile> lsClassProfileResult = ClassProfileBusiness.All.Where(o => listAll.Contains(o.ClassProfileID) && o.IsActive == true);

            return lsClassProfileResult;
        }
        #endregion

        #region Validate
        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="cp">cp</param>
        /// <author>
        /// vtit_dungnt77 - 06/05/2013
        /// </author>
        private void Validate(ClassProfile cp)
        {

            Utils.ValidateMaxLength(cp.CombinedClassCode, 40, "ClassProfile_Label_CombinedClassCode");
            Utils.ValidateMaxLength(cp.DisplayName, 200, "ClassProfile_Label_DisplayName");
            Utils.ValidateMaxLength(cp.Description, 200, "ClassProfile_Label_Description");
        }
        #endregion

        #region Kiem tra mot tai khoan co phai la giao vien CN, GV, ...
        public bool CheckRoleOfClass(int teacherID, int academicYearID, int classID, RoleOfClass roleCheck)
        {
            bool result = false;
            if (roleCheck == RoleOfClass.HeadTeacherSub)
            {
                //Lam thay chu nhiem
                var query = (from e in EmployeeBusiness.All
                             join c in HeadTeacherSubstitutionBusiness.All on e.EmployeeID equals c.HeadTeacherSubstitutionID
                             where c.ClassID == classID && e.EmployeeID == teacherID && c.AcademicYearID == academicYearID
                             select c).FirstOrDefault();
                if (query != null)
                {
                    result = true;
                }
            }
            else if (roleCheck == RoleOfClass.Overseeing)
            {
                //Giam thi
                var query = GetListClassByOverSeeing(teacherID, academicYearID).FirstOrDefault();
                if (query != null)
                {
                    result = true;
                }
            }
            else if (roleCheck == RoleOfClass.SubjectTeacher)
            {
                //Giao vien bo mon
                //Kiem tra trong phan cong giao vu voi lop hien tai
                var query = GetListClassBySubjectTeacher(teacherID, academicYearID).FirstOrDefault();
                if (query != null)
                {
                    result = true;
                }
            }
            else if (roleCheck == RoleOfClass.HeadTeacher)
            {
                //Chu nhiem lop
                var query = GetListClassByHeadTeacher(teacherID, academicYearID).FirstOrDefault();
                if (query != null)
                {
                    result = true;
                }
            }
            return result;
        }
        #endregion

        public void UpdateClassOrder(int SchoolID = 0, int AcademicYearID = 0)
        {
            //ID truong
            var listSchool = SchoolProfileBusiness.AllNoTracking.Where(o => o.IsActive && (SchoolID == 0 || o.SchoolProfileID == SchoolID)).Select(o => new { o.SchoolProfileID, o.EducationGrade }).ToList();
            this.context.Configuration.AutoDetectChangesEnabled = false;
            foreach (var School in listSchool)
            {
                // Nam hoc
                List<int> listAcaID = AcademicYearBusiness.AllNoTracking.Where(o => o.SchoolID == School.SchoolProfileID
                    && (AcademicYearID == 0 || o.AcademicYearID == AcademicYearID && o.IsActive == true)
                    ).Select(o => o.AcademicYearID).ToList();
                // Khoi
                List<int> listEduID = EducationLevelBusiness.AllNoTracking.OrderBy(o => o.EducationLevelID).Select(o => o.EducationLevelID).ToList();
                // Lop
                List<ClassProfile> listClass = this.AllNoTracking.Where(o => o.SchoolID == School.SchoolProfileID).ToList();
                bool isSave = false;
                foreach (int AcaID in listAcaID)
                {
                    // Lop 
                    List<ClassProfile> listClassByAca = listClass.Where(o => o.AcademicYearID == AcaID).ToList();
                    foreach (int EduID in listEduID)
                    {
                        List<ClassProfile> listClassByEdu = listClassByAca.Where(o => o.EducationLevelID == EduID).ToList();
                        List<ClassOrderNumber> listClassOrderNumber = new List<ClassOrderNumber>();
                        foreach (ClassProfile c in listClassByEdu)
                        {
                            string classNameCut = string.Empty;
                            int orderNunber = this.GetSubfixNumber(c.DisplayName, out classNameCut);
                            listClassOrderNumber.Add(new ClassOrderNumber
                            {
                                ClassID = c.ClassProfileID,
                                ClassNameCut = classNameCut,
                                OrderNumber = orderNunber
                            });
                        }
                        // Sap xep lai
                        if (listClassOrderNumber.Count > 0)
                        {
                            listClassOrderNumber = listClassOrderNumber.OrderBy(o => o.ClassNameCut).ThenBy(o => o.OrderNumber).ToList();
                            for (int i = 0; i < listClassOrderNumber.Count; i++)
                            {
                                ClassProfile classProfile = listClassByEdu.Find(o => o.ClassProfileID == listClassOrderNumber[i].ClassID);
                                classProfile.OrderNumber = i + 1;
                                base.Update(classProfile);
                            }
                            isSave = true;
                        }
                    }
                }
                if (isSave)
                {
                    this.Save();
                }
            }
        }

        private int GetSubfixNumber(string ClassName, out string s)
        {
            s = string.Empty;
            if (string.IsNullOrWhiteSpace(ClassName))
            {
                return 0;
            }
            int number = 0;
            ClassName = ClassName.Trim();
            int len = ClassName.Length;
            int posCut = 0;
            for (int i = len - 1; i >= 0; i--)
            {
                int testNum = 0;
                posCut = i;
                if (int.TryParse(ClassName[i].ToString(), out testNum))
                {
                    number += testNum * (int)(Math.Pow(10, (len - 1 - i)));
                }
                else
                {
                    break;
                }
            }
            s = ClassName.Substring(0, posCut + 1);
            return number;
        }

        /// <summary>
        /// Quanglm1
        /// 16/04/2014
        /// Lay danh sach lop chua nhap diem day du
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <param name="AppliedLevel"></param>
        /// <returns></returns>
        public List<ClassProfileRefBO> GetListClassNotSummedUp(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel)
        {
            return this.context.F_GET_CLASS_NOT_SUMMED_UP(SchoolID, AcademicYearID, Semester, AppliedLevel);
        }

        /// <summary>
        /// Xóa bỏ giáo viên Chủ nhiệm lớp khi đã xóa GV 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="TeacherID"></param>
        public void RemoveHeadTeacherInClass(Dictionary<string, object> dic)
        {
            var LstClassHasRole = this.Search(dic).ToList();
            int Count = 0;
            if (LstClassHasRole != null && (Count = LstClassHasRole.Count) > 0)
            {
                ClassProfile objUpdate;
                for (int i = 0; i < Count; i++)
                {
                    objUpdate = LstClassHasRole[i];
                    objUpdate.HeadTeacherID = null;
                    this.ClassProfileBusiness.Update(objUpdate);
                }
                this.ClassProfileBusiness.Save();
            }
        }

        /// <summary>
        /// Viethd4: Ham lay danh sach lop dua theo phan quyen account dang nhap
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="userAccountID"></param>
        /// <param name="isAdminSchoolRole"></param>
        /// <returns></returns>
        public IQueryable<ClassProfile> getClassByAccountRole(int academicYearID, int schoolID, int? educationLevelID, int? classID, int userAccountID, bool isSuperViSingDept, bool isSubSuperVisingDept)
        {
            IQueryable<ClassProfile> listClass;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["ClassProfileID"] = classID;
            dic["IsVNEN"] = true;
            if ((isSubSuperVisingDept || isSuperViSingDept))
            {
                listClass = ClassProfileBusiness.SearchBySchool(schoolID, dic)
                .OrderBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName);
                return listClass;
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            //Quyen admin truong
            bool isAdminSchoolRole = UserAccountBusiness.IsSchoolAdmin(userAccountID);
            //Quyen BGH
            bool isRolePrincipal = false;
            Employee emp = UserAccountBusiness.Find(userAccountID).Employee;
            if (emp != null)
            {
                if (emp.WorkGroupTypeID.HasValue
                    && (emp.WorkGroupTypeID.Value == 3)
                    )
                {
                    isRolePrincipal = true;
                }
            }

            if (!isAdminSchoolRole && !isRolePrincipal && !academicYear.IsTeacherCanViewAll)
            {
                dic["UserAccountID"] = userAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            listClass = ClassProfileBusiness.SearchBySchool(schoolID, dic)
                .OrderBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName);

            return listClass;
        }

        public IQueryable<ClassProfile> getClassByRoleAppliedLevel(int academicYearID, int schoolID, int appliedLevel, int userAccountID)
        {
            IQueryable<ClassProfile> listClass;

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            //Quyen admin truong
            bool isAdminSchoolRole = UserAccountBusiness.IsSchoolAdmin(userAccountID);
            //Quyen BGH
            bool isRolePrincipal = false;
            Employee emp = UserAccountBusiness.Find(userAccountID).Employee;
            if (emp != null)
            {
                if (emp.WorkGroupTypeID.HasValue
                    && (emp.WorkGroupTypeID.Value == 3)
                    )
                {
                    isRolePrincipal = true;
                }
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["AppliedLevel"] = appliedLevel;

            if (!isAdminSchoolRole && !isRolePrincipal && !academicYear.IsTeacherCanViewAll)
            {
                dic["UserAccountID"] = userAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            listClass = ClassProfileBusiness.SearchBySchool(schoolID, dic).OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName);

            return listClass;
        }

        public IQueryable<ClassProfile> getClassByRoleAppliedLevelMobile(int academicYearID, int schoolID, int appliedLevel, int userAccountID, int role)
        {
            IQueryable<ClassProfile> listClass;

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            //Quyen admin truong
            bool isAdminSchoolRole = role == 1;
            //Quyen BGH
            bool isRolePrincipal = role == 3;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["AppliedLevel"] = appliedLevel;

            if (!isAdminSchoolRole && !isRolePrincipal && !academicYear.IsTeacherCanViewAll)
            {
                dic["UserAccountID"] = userAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            listClass = ClassProfileBusiness.SearchBySchool(schoolID, dic).OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName);

            return listClass;
        }

        public IQueryable<ClassProfile> getClassAdminOrHeadTeacher(int academicYearID, int schoolID, int appliedLevel, int userAccountID)
        {
            IQueryable<ClassProfile> listClass;

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            //Quyen admin truong
            bool isAdminSchoolRole = UserAccountBusiness.IsSchoolAdmin(userAccountID);
            //Quyen BGH
            bool isRolePrincipal = false;
            Employee emp = UserAccountBusiness.Find(userAccountID).Employee;
            if (emp != null)
            {
                if (emp.WorkGroupTypeID.HasValue
                    && (emp.WorkGroupTypeID.Value == 3)
                    )
                {
                    isRolePrincipal = true;
                }
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["AppliedLevel"] = appliedLevel;

            if (!isAdminSchoolRole && !isRolePrincipal && !academicYear.IsTeacherCanViewAll)
            {
                dic["UserAccountID"] = userAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            }
            listClass = ClassProfileBusiness.SearchBySchool(schoolID, dic).OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName);

            return listClass;
        }

        public List<ClassProfileBO> GetListClassBySubject(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            List<ClassProfileBO> lstClassProfileBO = (from cp in ClassProfileBusiness.All
                                                      join sc in ClassSubjectBusiness.All on cp.ClassProfileID equals sc.ClassID
                                                      where cp.SchoolID == SchoolID
                                                      && cp.AcademicYearID == AcademicYearID
                                                      && cp.EducationLevelID == EducationLevelID
                                                      && sc.SubjectID == SubjectID
                                                      && cp.IsActive == true
                                                      orderby cp.OrderNumber, cp.DisplayName
                                                      select new ClassProfileBO
                                                      {
                                                          classProfile = cp
                                                      }).ToList();
            return lstClassProfileBO;
        }

        public List<ClassProfileBO> GetListClassByEducationLevel(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic["SchoolID"]);
            int AcademicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int EducationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            List<ClassProfileBO> lstClassProfileBO = new List<ClassProfileBO>();
            ClassProfileBO objCPBO = null;
            List<ClassProfile> lstCP = ClassProfileBusiness.Search(dic).OrderBy(p => p.OrderNumber).ToList();
            ClassProfile objCP = null;
            List<int?> lstHeadTeacherID = lstCP.Where(p => p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID).Distinct().ToList();
            //lay ra danh sach giao vien theo listID
            List<EmployeeBO> lstE = (from e in EmployeeBusiness.All
                                     join f in SchoolFacultyBusiness.All on e.SchoolFacultyID equals f.SchoolFacultyID
                                     where e.SchoolID == SchoolID
                                     && e.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING
                                     && lstHeadTeacherID.Contains(e.EmployeeID)
                                     select new EmployeeBO
                                     {
                                         EmployeeID = e.EmployeeID,
                                         FullName = e.FullName,
                                         SchoolFacultyName = f.FacultyName
                                     }).ToList();
            EmployeeBO objEBO = null;

            for (int i = 0; i < lstCP.Count; i++)
            {
                objCP = lstCP[i];
                objCPBO = new ClassProfileBO();
                if (objCP.HeadTeacherID.HasValue)
                {
                    objEBO = lstE.Where(p => p.EmployeeID == objCP.HeadTeacherID.Value).FirstOrDefault();
                    if (objEBO != null)
                    {
                        objCPBO.strTitle = "GVCN: " + objEBO.FullName + "-" + objEBO.SchoolFacultyName;
                    }
                }
                objCPBO.classProfile = objCP;
                lstClassProfileBO.Add(objCPBO);
            }
            return lstClassProfileBO;
        }

        public bool CheckIsCombinedClass(int schoolId, int academicYear, int AppliedLevel)
        {
            var result = ClassProfileBusiness.All.Where(x => x.IsCombinedClass == true
                                                            && x.SchoolID == schoolId
                                                            && x.AcademicYearID == academicYear
                                                            && x.EducationLevel.Grade == AppliedLevel).FirstOrDefault();
            return result != null ? true : false;
        }

        public List<ClassProfile> GetListClassBySchool(int SchoolID, int AcademicYearID)
        {

            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID}
                }).ToList();

            return listClass;
        }

        public List<ClassProfile> GetListClass(int AcademicYearID, int EducationLevelID)
        {

            return ClassProfileBusiness.SearchByAcademicYear(AcademicYearID, new Dictionary<string, object>(){
                {"EducationLevelID", EducationLevelID}
                    }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();

        }

        public Stream CreateClassProfileReport(IVTWorkbook oBook, List<ClassProfile> listClassProfile, int SchoolID, int AcademicYearID, int AppliedLevel, int AppliedTraningType, List<int?> ListAppliedLevelByGrade)
        {
            IVTWorksheet oSheet1 = oBook.GetSheet(1);
            
            #region Export
            var listClassID = new List<int>();
            listClassID = listClassProfile.Select(x => x.ClassProfileID).ToList();
            var listClassSpecialCatID = new List<int?>();
            listClassSpecialCatID = listClassProfile.Select(x => x.ClassSpecialtyCatID).ToList();
            var listClassSpecialCat = ClassSpecialtyCatRepository.All.Where(x => listClassSpecialCatID.Contains(x.ClassSpecialtyCatID)).ToList();
            Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
            dicSeachParam.Add("IsActive", true);
            //dicSeachParam["AppliedLevel"] = AppliedLevel;
            dicSeachParam["SchoolID"] = SchoolID;
            //dicSeachParam.Add("IsForeignLanguage", true);
            var listSubjectAll = this.SubjectCatBusiness.Search(dicSeachParam).OrderBy(o => o.SubjectName);
            IQueryable<SubjectCat> listSubject = listSubjectAll.Where(x => x.AppliedLevel == AppliedLevel);

            Dictionary<string, object> dicSeachClassSubject = new Dictionary<string, object>();
            dicSeachParam["AcademicYearID"] = AcademicYearID;
            dicSeachParam["ListClassID"] = listClassID;
            IQueryable<ClassSubject> listClassSubject = this.ClassSubjectBusiness.Search(dicSeachParam);
            var listClassSubjectSpecialName = (from cs in listClassSubject
                                               join sa in listSubject on cs.SubjectID equals sa.SubjectCatID
                                               //where cs.IsSpecializedSubject == true
                                               select new ClassSubjectBO
                                               {
                                                   SubjectID = cs.SubjectID,
                                                   SubjectName = sa.SubjectName,
                                                   ClassID = cs.ClassID,
                                                   SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                                                   SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester,
                                                   Is_Specialize_Subject = cs.IsSpecializedSubject,
                                                   IsForeignLanguage = sa.IsForeignLanguage
                                               }).ToList();
            var listPropertyOfClassAll = GetDataPropertyOfClass(new List<int?>() { AppliedLevel }, listClassID);
            List<int> listAppliedSpecKindergarten = null;
            if (ListAppliedLevelByGrade.Contains(4) && ListAppliedLevelByGrade.Contains(5))
            {
                listAppliedSpecKindergarten = new List<int>() { 4, 5 };
            }
            this.SetDataForReportClassProfile(AppliedLevel, SchoolID, AppliedTraningType, listPropertyOfClassAll, oSheet1, listClassProfile, listClassSubjectSpecialName, listSubject, listClassSpecialCat, listAppliedSpecKindergarten);
            #endregion

            return oBook.ToStream();
        }
        public List<PropertyOfClassBO> GetDataPropertyOfClass(List<int?> listAppliedLevel, List<int> listClassProfile)
        {
            Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
            List<ClassPropertyCat> ListClassPropertyCatAll = this.ClassPropertyCatBusiness.All.Where(x => listAppliedLevel.Contains(x.AppliedLevel) && x.IsActive).ToList();
            List<PropertyOfClassBO> ListPropertyOfClass = (from cpc in ListClassPropertyCatAll
                                                           join pc in PropertyOfClassBusiness.All.Where(x => listClassProfile.Contains(x.ClassID) && x.IsActive) on cpc.ClassPropertyCatID equals pc.ClassPropertyCatID
                                                           select new PropertyOfClassBO
                                                           {
                                                               ClassID = pc.ClassID,
                                                               ClassPropertyCatID = cpc.ClassPropertyCatID,
                                                               PropertyOfClassID = pc.PropertyOfClassID,
                                                               ClassPropertyCatName = cpc.Resolution,
                                                               AppliedLevel = cpc.AppliedLevel,
                                                               ClassPropertyCatCode = Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(cpc.Resolution))
                                                           }).ToList();
            return ListPropertyOfClass;          
        }

        public ProcessedReport GetReportClassProfile(IDictionary<string, object> dic, bool isZip = false)
        {
            string reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN;
            if (isZip)
            {
                reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN_Zip;
            }
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        
        public void DownloadZipFileClassProfile(List<ClassProfile> listClassProfile, List<int?> ListAppliedLevelByGrade, int SchoolID, int AcademicYearID, int AppliedLevel,int AppliedTraningType, out string folderSaveFile)
        {
            string fileName = string.Empty;    
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            string reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN;
            // Lấy đường dẫn báo cáo
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            IVTWorkbook oBook = null;
            this.CreateZipFileClassProfile(oBook, listClassProfile, ListAppliedLevelByGrade, SchoolID,AcademicYearID, AppliedLevel,AppliedTraningType,folder);
            folderSaveFile = folder;
        }

        public void CreateZipFileClassProfile(IVTWorkbook oBook, List<ClassProfile> listClassProfile, List<int?> ListAppliedLevelByGrade, int SchoolID, int AcademicYearID, int AppliedLevel, int AppliedTraningType, string folder)
        {
            var listClassID = new List<int>();
            listClassID = listClassProfile.Select(x => x.ClassProfileID).ToList();
            Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
            dicSeachParam.Add("IsActive", true);
            //dicSeachParam["AppliedLevel"] = AppliedLevel;
            dicSeachParam["SchoolID"] = SchoolID;
            //dicSeachParam.Add("IsForeignLanguage", true);
            var listSubjectAll = this.SubjectCatBusiness.Search(dicSeachParam).OrderBy(o => o.SubjectName);
            IQueryable<SubjectCat> listSubject = listSubjectAll.Where(x => ListAppliedLevelByGrade.Contains(x.AppliedLevel));

            Dictionary<string, object> dicSeachClassSubject = new Dictionary<string, object>();
            dicSeachParam["AcademicYearID"] = AcademicYearID;
            dicSeachParam["ListClassID"] = listClassID;
            dicSeachParam["SchoolID"] = SchoolID;
            IQueryable<ClassSubject> listClassSubject = this.ClassSubjectBusiness.Search(dicSeachParam);
            var listClassSubjectSpecialName = (from cs in listClassSubject
                                               join sa in listSubject on cs.SubjectID equals sa.SubjectCatID
                                               //where cs.IsSpecializedSubject == true
                                               select new ClassSubjectBO
                                               {
                                                   SubjectID = cs.SubjectID,
                                                   SubjectName = sa.SubjectName,
                                                   ClassID = cs.ClassID,
                                                   SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                                                   SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester,
                                                   Is_Specialize_Subject = cs.IsSpecializedSubject,
                                                   IsForeignLanguage = sa.IsForeignLanguage
                                               }).ToList();
            var listClassSpecialCatID = new List<int?>();
            listClassSpecialCatID = listClassProfile.Select(x => x.ClassSpecialtyCatID).ToList();
            var listClassSpecialCat = ClassSpecialtyCatRepository.All.Where(x => listClassSpecialCatID.Contains(x.ClassSpecialtyCatID)).ToList();
            var fileName = string.Empty;
            var fileNameZip = string.Empty;
            int? grade = 0;
            string templatePath = string.Empty;
            IVTWorksheet oSheet1 = null;
            string reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN_Zip;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            List<ClassProfile> listClassProfileMN = null;
            List<int> listlevelMN = new List<int>() { 13, 14, 15, 16, 17, 18 };
            List<ClassProfile> listClassProfileLevel1 = null;
            List<int> listlevel1 = new List<int>() { 1, 2, 3, 4, 5 };
            List<ClassProfile> listClassProfileLevel2 = null;
            List<int> listlevel2 = new List<int>() { 6, 7, 8, 9 };
            List<ClassProfile> listClassProfileLevel3 = null;
            List<int> listlevel3 = new List<int>() { 10, 11, 12 };
            List<int> listAllLevel = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            List<int> listAppliedSpecKindergarten = null;
            Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            if(AppliedTraningType == 3)
            {
                ListAppliedLevelByGrade.Add(-1);
            }
            for (int i = 0; i < ListAppliedLevelByGrade.Count; i++)
            {
                //Trường hợp này xử lý cho gán dữ liệu nhóm lớp cấp Mầm non
                if (ListAppliedLevelByGrade.Contains(4) && ListAppliedLevelByGrade.Contains(5))
                {
                    listAppliedSpecKindergarten = new List<int>() { 4, 5 };
                }
                var listPropertyOfClassAll = GetDataPropertyOfClass(ListAppliedLevelByGrade, listClassID);
                grade = ListAppliedLevelByGrade[i];
                if (grade == 1 && AppliedTraningType != 3)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/DanhSach-C1-Lop.xls";
                    listClassProfileLevel1 = listClassProfile.Where(x => listlevel1.Contains(x.EducationLevelID)).ToList();
                    fileName = "DanhSach-C1-Lop.xls";
                    oBook = VTExport.OpenWorkbook(templatePath);
                    oSheet1 = oBook.GetSheet(1);
                    //Set data
                    this.SetDataForReportClassProfile(grade.Value, SchoolID, AppliedTraningType, listPropertyOfClassAll, oSheet1, listClassProfileLevel1, listClassSubjectSpecialName, listSubject, listClassSpecialCat);

                    oBook.CreateZipFile(fileName, folder);
                    
                }
                else if (grade == 2 && AppliedTraningType != 3)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/DanhSach-C2-Lop.xls";
                    listClassProfileLevel2 = listClassProfile.Where(x => listlevel2.Contains(x.EducationLevelID)).ToList();
                    fileName = "DanhSach-C2-Lop.xls";
                    oBook = VTExport.OpenWorkbook(templatePath);
                    oSheet1 = oBook.GetSheet(1);
                    //Set data
                    this.SetDataForReportClassProfile(grade.Value, SchoolID, AppliedTraningType, listPropertyOfClassAll, oSheet1, listClassProfileLevel2, listClassSubjectSpecialName, listSubject, listClassSpecialCat);

                    oBook.CreateZipFile(fileName, folder);
                    
                }
                else if (grade == 3 && AppliedTraningType != 3)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/DanhSach-C3-Lop.xls";
                    listClassProfileLevel3 = listClassProfile.Where(x => listlevel3.Contains(x.EducationLevelID)).ToList();
                    fileName = "DanhSach-C3-Lop.xls";
                    oBook = VTExport.OpenWorkbook(templatePath);
                    oSheet1 = oBook.GetSheet(1);
                    //Set data
                    this.SetDataForReportClassProfile(grade.Value, SchoolID, AppliedTraningType, listPropertyOfClassAll, oSheet1, listClassProfileLevel3, listClassSubjectSpecialName, listSubject, listClassSpecialCat);

                    oBook.CreateZipFile(fileName, folder);

                }
                else if ((grade == 4 || grade == 5) && AppliedTraningType != 3)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/DanhSach-MN-Lop.xls";
                    listClassProfileMN = listClassProfile.Where(x => listlevelMN.Contains(x.EducationLevelID)).ToList();
                    fileName = "DanhSach-MN-Lop.xls";
                    oBook = VTExport.OpenWorkbook(templatePath);
                    oSheet1 = oBook.GetSheet(1);
                    //Set data
                    this.SetDataForReportClassProfile(grade.Value, SchoolID, AppliedTraningType, listPropertyOfClassAll, oSheet1, listClassProfileMN, listClassSubjectSpecialName, listSubject, listClassSpecialCat, listAppliedSpecKindergarten);

                    oBook.CreateZipFile(fileName, folder);

                }
                else if (grade == -1)
                {
                    var listAppliedLevelTraningType = new List<int>();
                    var fileNameLevel = new List<string>(); 
                    if (ListAppliedLevelByGrade.Contains(1))
                    {
                        fileNameLevel.Add("C1");
                        listAppliedLevelTraningType.AddRange(new List<int>() { 1, 2, 3, 4, 5 });
                    }
                    if (ListAppliedLevelByGrade.Contains(2))
                    {
                        fileNameLevel.Add("C2");
                        listAppliedLevelTraningType.AddRange(new List<int>() { 6, 7, 8, 9 });
                    }
                    if (ListAppliedLevelByGrade.Contains(3))
                    {
                        fileNameLevel.Add("C3");
                        listAppliedLevelTraningType.AddRange(new List<int>() { 10, 11, 12 });
                    }
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/DanhSach-GDTX-Lop.xls";
                    var listClassProfileApprenticeship = listClassProfile.Where(x => listAppliedLevelTraningType.Contains(x.EducationLevelID)).ToList();

                    fileName = string.Format("DanhSach-{0}-GDTX-Lop.xls", string.Join("-",fileNameLevel));
                    oBook = VTExport.OpenWorkbook(templatePath);
                    oSheet1 = oBook.GetSheet(1);
                    //Set data
                    this.SetDataForReportClassProfile(grade.Value, SchoolID, AppliedTraningType, listPropertyOfClassAll, oSheet1, listClassProfileApprenticeship, listClassSubjectSpecialName, listSubject, listClassSpecialCat);

                    oBook.CreateZipFile(fileName, folder);

                }
            }
            
        }
        public ProcessedReport InsertZipFileClassProfileReport(Transcripts entity, Stream data, string fileNameByLevel, bool isZip = false)
        {
            string reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN;
            if(isZip)
                reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN_Zip;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyTranscriptsReport(entity);
            pr.ReportData = isZip ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", fileNameByLevel);
            outputNamePattern = outputNamePattern.Replace("--", "-");
            pr.ReportName = outputNamePattern + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"PupilID", entity.PupilID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public void SetDataForReportClassProfile(int AppliedLevel, int SchoolID, int AppliedTraningType, List<PropertyOfClassBO> listPropertyOfClassAll,
                                    IVTWorksheet oSheet1, List<ClassProfile> listClassProfile, List<ClassSubjectBO> listClassSubjectAll, IQueryable<SubjectCat> listSubjectCatAll,
                                    List<ClassSpecialtyCat> listClassSpecialCat, List<int> listAppliedSpecKindergarten = null) 
        {
            
            List<SubjectCat> listSubject = listSubjectCatAll.Where(x => x.AppliedLevel == AppliedLevel).ToList() ;
            decimal? SectionAllSubPerWeek = 0;
            decimal? SectionEnglishPerWeek = 0;
            var StrSectionAllSubPerWeek = string.Empty;
            var StrSectionEnglishPerWeek = string.Empty;
            SubjectCat objFirstForeinLanguage = null;
            SubjectCat objSecondForeinLanguage = null;
            ClassSpecialtyCat ClassSepcialCatItem = null;
            List<ClassSubjectBO> listENClassSubject = null;
            //Cấp 1
            if (AppliedLevel == 1 && AppliedTraningType != 3)
            {
                int startRow = 3;
                int stt = 1;                
                foreach (var item in listClassProfile)
                {
                    SectionAllSubPerWeek = (listClassSubjectAll.Where(x => x.ClassID == item.ClassProfileID).Sum(x => x.SectionPerWeekFirstSemester) + listClassSubjectAll.Where(x => x.ClassID == item.ClassProfileID).Sum(x => x.SectionPerWeekSecondSemester)) / 2;
                    objFirstForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.FirstForeignLanguageID).FirstOrDefault();
                    objSecondForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.SecondForeignLanguageID).FirstOrDefault();
                    listENClassSubject = listClassSubjectAll.Where(x => x.ClassID == item.ClassProfileID && x.IsForeignLanguage == true).ToList();
                    SectionEnglishPerWeek = (listENClassSubject.Sum(x => x.SectionPerWeekFirstSemester) + listENClassSubject.Sum(x => x.SectionPerWeekSecondSemester)) / 2;
                    //Xử lý format số tiết ngoại ngữ/tuần
                    if (SectionEnglishPerWeek != null && Math.Ceiling((decimal)SectionEnglishPerWeek) >= 2 && Math.Ceiling((decimal)SectionEnglishPerWeek) <= 4)
                    {
                        StrSectionEnglishPerWeek = string.Format("Học {0} tiết/tuần", Math.Ceiling((decimal)SectionEnglishPerWeek));
                    }
                    else if (SectionEnglishPerWeek != null && Math.Ceiling((decimal)SectionEnglishPerWeek) > 4)
                    {
                        StrSectionEnglishPerWeek = "Học trên 4 tiết/tuần";
                    }
                    else
                    {
                        StrSectionEnglishPerWeek = SectionEnglishPerWeek != null && Math.Ceiling((decimal)SectionEnglishPerWeek) == 1 ? "Học 1 tiết/tuần" : string.Empty;
                    }
                    //Xử lý tổng số tiết học/tuần
                    if (SectionAllSubPerWeek == null || SectionAllSubPerWeek < 30) 
                    {
                        StrSectionAllSubPerWeek = "Học dưới 30 tiết/tuần";
                    }
                    else if (SectionAllSubPerWeek != null && Math.Ceiling((decimal)SectionAllSubPerWeek) >= 30 && Math.Ceiling((decimal)SectionAllSubPerWeek) <= 35)
                    {
                        StrSectionAllSubPerWeek = string.Format("Học {0} tiết/tuần", Math.Ceiling((decimal)SectionAllSubPerWeek));
                    }
                    else if (SectionAllSubPerWeek > 35)
                    {
                        StrSectionAllSubPerWeek = "Học trên 35 tiết/tuần";
                    }

                    oSheet1.SetCellValue(startRow, 1, stt);
                    oSheet1.SetCellValue(startRow, 2, item.EducationLevel.Resolution);
                    oSheet1.SetCellValue(startRow, 3, item.DisplayName); //Mã lớp
                    oSheet1.SetCellValue(startRow, 4, item.DisplayName);
                    oSheet1.SetCellValue(startRow, 5, item.OrderNumber);
                    oSheet1.SetCellValue(startRow, 6, item.Employee != null ? item.Employee.EmployeeCode : string.Empty);
                    oSheet1.SetCellValue(startRow, 7, objFirstForeinLanguage != null ? objFirstForeinLanguage.SubjectName : string.Empty);
                    oSheet1.SetCellValue(startRow, 8, objSecondForeinLanguage != null ? objSecondForeinLanguage.SubjectName : string.Empty);
                    oSheet1.SetCellValue(startRow, 9, StrSectionEnglishPerWeek);//Số tiết ngoại ngữ/tuần
                    oSheet1.SetCellValue(startRow, 10, StrSectionAllSubPerWeek);//Số tiết học/tuần
                    oSheet1.SetCellValue(startRow, 11, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 2 buổi/ngày"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//Học 2 buổi/ngày
                    oSheet1.SetCellValue(startRow, 12, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 6-8 buổi/tuần"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//Học 6-8 buổi/tuần
                    oSheet1.SetCellValue(startRow, 13, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 9-10 buổi/tuần"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Học 9-10 buổi/tuần
                    oSheet1.SetCellValue(startRow, 14, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 5 buổi/tuần"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Học 5 buổi/tuần
                    oSheet1.SetCellValue(startRow, 15, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Đủ thiết bị dạy học Tiếng Việt"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Đủ TBDH - Tiếng Việt
                    oSheet1.SetCellValue(startRow, 16, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Đủ thiết bị dạy học Toán"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Đủ TBDH - Toán
                    oSheet1.SetCellValue(startRow, 17, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Có đại diện cha mẹ học sinh lớp"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Có đại diện cha mẹ hs lớp
                    oSheet1.SetCellValue(startRow, 18, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Có đại diện cha mẹ học sinh trường"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Có đại diện cha mẹ hs trường
                    oSheet1.SetCellValue(startRow, 19, item.IsCombinedClass ? "x" : string.Empty);//Lớp ghép
                    oSheet1.SetCellValue(startRow, 20, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp bán trú"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Bán trú
                    oSheet1.SetCellValue(startRow, 21, listPropertyOfClassAll.Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp chuyên biệt"))).FirstOrDefault() != null
                                                        ? "x" : string.Empty);//Chuyên biệt
                    oSheet1.SetCellValue(startRow, 22, item.IsVnenClass == true ? "x" : string.Empty);//Lớp VNEN
                    startRow++;
                    stt++;
                }

                oSheet1.GetRange(3, 1, startRow - 1, 22).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                //oSheet1.SetFontName("Times New Roman", 10);   
            }
            else if (AppliedLevel == 2 && AppliedTraningType != 3) // cấp 2
            {
                int startRow = 3;
                int stt = 1;
                foreach (var item in listClassProfile)
                {
                    objFirstForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.FirstForeignLanguageID).FirstOrDefault();
                    objSecondForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.SecondForeignLanguageID).FirstOrDefault();
                    oSheet1.SetCellValue(startRow, 1, stt);
                    oSheet1.SetCellValue(startRow, 2, item.EducationLevel.Resolution);
                    oSheet1.SetCellValue(startRow, 3, item.DisplayName); //Mã lớp
                    oSheet1.SetCellValue(startRow, 4, item.DisplayName);
                    oSheet1.SetCellValue(startRow, 5, item.OrderNumber);
                    oSheet1.SetCellValue(startRow, 6, item.Employee != null ? item.Employee.EmployeeCode : string.Empty);
                    oSheet1.SetCellValue(startRow, 7, objFirstForeinLanguage != null ? objFirstForeinLanguage.SubjectName : string.Empty);
                    oSheet1.SetCellValue(startRow, 8, objSecondForeinLanguage != null ? objSecondForeinLanguage.SubjectName : string.Empty);
                    oSheet1.SetCellValue(startRow, 9, item.IsCombinedClass ? "x" : string.Empty);//Lớp ghép
                    oSheet1.SetCellValue(startRow, 10, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp bán trú"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//Bán trú
                    oSheet1.SetCellValue(startRow, 11, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 2 buổi/ngày"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//Học 2 buổi/ngày
                    oSheet1.SetCellValue(startRow, 12, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 5 buổi/tuần"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//Học 5 buổi/tuần
                    oSheet1.SetCellValue(startRow, 13, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp có học sinh học nghề"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//HS học nghề
                    startRow++;
                    stt++;
                }

                oSheet1.GetRange(3, 1, startRow - 1, 13).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                //oSheet1.SetFontName("Times New Roman", 10);
            }
            else if (AppliedLevel == 3 && AppliedTraningType != 3) // cấp 3
            {
                int startRow = 3;
                int stt = 1;
                foreach (var item in listClassProfile)
                {
                    ClassSepcialCatItem = listClassSpecialCat.Where(x => x.ClassSpecialtyCatID == item.ClassSpecialtyCatID).FirstOrDefault();
                    objFirstForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.FirstForeignLanguageID).FirstOrDefault();
                    objSecondForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.SecondForeignLanguageID).FirstOrDefault();
                    oSheet1.SetCellValue(startRow, 1, stt);
                    oSheet1.SetCellValue(startRow, 2, item.EducationLevel.Resolution);
                    oSheet1.SetCellValue(startRow, 3, item.DisplayName);//Mã lớp
                    oSheet1.SetCellValue(startRow, 4, item.DisplayName);
                    oSheet1.SetCellValue(startRow, 5, item.OrderNumber);
                    oSheet1.SetCellValue(startRow, 6, item.Employee != null ? item.Employee.EmployeeCode : string.Empty);
                    oSheet1.SetCellValue(startRow, 7, item.SubCommittee != null ? item.SubCommittee.Resolution : string.Empty);
                    oSheet1.SetCellValue(startRow, 8, objFirstForeinLanguage != null ? objFirstForeinLanguage.SubjectName : string.Empty);
                    oSheet1.SetCellValue(startRow, 9, objSecondForeinLanguage != null ? objSecondForeinLanguage.SubjectName : string.Empty);
                    oSheet1.SetCellValue(startRow, 10, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 2 buổi/ngày"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);/////hoc 2 buoi/ngay
                    oSheet1.SetCellValue(startRow, 11, listPropertyOfClassAll.
                                                        Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode 
                                                        == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp có học sinh học nghề"))).FirstOrDefault() != null 
                                                        ? "x" : string.Empty);//Lop co hs hoc nghe
                    oSheet1.SetCellValue(startRow, 12, item.IsSpecializedClass == true && item.ClassSpecialtyCatID > 0 ? "x" : string.Empty);//Lop chuyen 
                    oSheet1.SetCellValue(startRow, 13, ClassSepcialCatItem != null ? ClassSepcialCatItem.Resolution : string.Empty);//He chuyen
                    startRow++;
                    stt++;
                }

                oSheet1.GetRange(3, 1, startRow - 1, 13).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                //oSheet1.SetFontName("Times New Roman", 10);
            }
            else if ((AppliedLevel == 4 || AppliedLevel == 5) && AppliedTraningType != 3) // cấp mầm non
            {
                int startRow = 3;
                int stt = 1;
                List<ClassProfile> ListClassProcess = new List<ClassProfile>();
                List<int> listlevelNT = new List<int>() { 13, 14, 15 };
                List<int> listlevelMG = new List<int>() { 16, 17, 18 };
                if (listAppliedSpecKindergarten == null || (listAppliedSpecKindergarten != null && listAppliedSpecKindergarten.Count == 0))
                {
                    listAppliedSpecKindergarten = new List<int>() { AppliedLevel };
                }
                foreach (var applied in listAppliedSpecKindergarten)
	            {
                    if(applied == 4)
                    {
                        ListClassProcess = listClassProfile.Where(x => listlevelNT.Contains(x.EducationLevelID)).ToList();
                    }
                    else
                    {
                        ListClassProcess = listClassProfile.Where(x => listlevelMG.Contains(x.EducationLevelID)).ToList();
                    }
                    foreach (var item in ListClassProcess)
                    {
                        objFirstForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.FirstForeignLanguageID).FirstOrDefault();
                        objSecondForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.SecondForeignLanguageID).FirstOrDefault();
                        oSheet1.SetCellValue(startRow, 1, stt);
                        oSheet1.SetCellValue(startRow, 2, applied == 4 ? "Nhóm trẻ" : "Lớp mẫu giáo");
                        oSheet1.SetCellValue(startRow, 3, item.EducationLevel.Resolution);
                        oSheet1.SetCellValue(startRow, 4, item.DisplayName);//Mã lớp
                        oSheet1.SetCellValue(startRow, 5, item.DisplayName);
                        oSheet1.SetCellValue(startRow, 6, item.OrderNumber);
                        oSheet1.SetCellValue(startRow, 7, item.Employee != null ? item.Employee.EmployeeCode : string.Empty);
                        oSheet1.SetCellValue(startRow, 8, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp học 2 buổi/ngày"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Học 2 buổi/ngày
                        oSheet1.SetCellValue(startRow, 9, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Ghép 2 độ tuổi"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Lớp ghép 2 tuổi
                        oSheet1.SetCellValue(startRow, 10, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Ghép 3 độ tuổi"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Lớp ghép 3 độ tuổi
                        oSheet1.SetCellValue(startRow, 11, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Ghép 4 độ tuổi"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Lớp ghép 4 độ tuổi
                        oSheet1.SetCellValue(startRow, 12, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp bán trú"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Bán trú 
                        oSheet1.SetCellValue(startRow, 13, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Lớp có HS khuyết tật học hòa nhập"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Có trẻ khuyết tật học hòa nhập
                        oSheet1.SetCellValue(startRow, 14, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Chương trình giáo dục mầm non mới"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Chương trình giáo dục mầm non mới
                        oSheet1.SetCellValue(startRow, 15, listPropertyOfClassAll.
                                                            Where(x => x.ClassID == item.ClassProfileID && x.ClassPropertyCatCode
                                                            == Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar("Chương trình chăm sóc giáo dục"))).FirstOrDefault() != null
                                                            ? "x" : string.Empty);//Chương trình chăm sóc giáo dục
                        startRow++;
                        stt++;
                    }
	            }
                
                oSheet1.GetRange(3, 1, startRow - 1, 16).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                //oSheet1.SetFontName("Times New Roman", 10);
            }
            else if(AppliedTraningType == 3) // GDTX
            {
                var dicClassType = this.DicApprenticeshipClassTypeList();
                KeyValuePair<string, string> ClassTypeItem = new KeyValuePair<string, string>();
                var dicTrainingType = this.DicApprenticeshipTrainingTypeList();
                KeyValuePair<string, string> TrainingTypeItem = new KeyValuePair<string, string>();
                //var dicClassType = this.DicApprenticeshipClassTypeList();
                //Delete row unusing
                for (int i = 3; i <= 16;i++)
                    oSheet1.DeleteRow(3);
                Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
                dicSeachParam.Add("IsActive", true);
                dicSeachParam.Add("IsApprenticeshipSubject", true);
                dicSeachParam["AppliedLevel"] = AppliedLevel;
                dicSeachParam["SchoolID"] = SchoolID;
                var listApprenticeshipGroup = ApprenticeshipGroupBusiness.Search(dicSeachParam).OrderBy(o => o.GroupName);
                ApprenticeshipGroup ApprenticeshipGroupByClass = null;
                var listTraningProByListClass = listClassProfile.Select(x => x.TrainingProgramID).ToList();
                var listTraningProgram = this.TrainingProgramRepository.All.Where(x => x.IsActive && listTraningProByListClass.Contains(x.TrainingProgramID));
                TrainingProgram TrainingProgramByClass = null;
                int startRow = 3;
                int stt = 1;
                foreach (var item in listClassProfile)
                {
                    TrainingProgramByClass = listTraningProgram.Where(x => x.TrainingProgramID == item.TrainingProgramID).FirstOrDefault();
                    ClassTypeItem = dicClassType.Where(x => x.Key == item.ClassType.ToString()).FirstOrDefault();
                    TrainingTypeItem = dicTrainingType.Where(x => x.Key == item.TrainingType.ToString()).FirstOrDefault();
                    ApprenticeshipGroupByClass = listApprenticeshipGroup.Where(x => x.ApprenticeshipGroupID == item.ApprenticeshipGroupID).FirstOrDefault();
                    objFirstForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.FirstForeignLanguageID).FirstOrDefault();
                    objSecondForeinLanguage = listSubject.Where(x => x.SubjectCatID == item.SecondForeignLanguageID).FirstOrDefault();
                    oSheet1.SetCellValue(startRow, 1, stt);
                    oSheet1.SetCellValue(startRow, 2, !ClassTypeItem.Equals(default(KeyValuePair<string, string>)) ? ClassTypeItem.Value : string.Empty);
                    oSheet1.SetCellValue(startRow, 3, item.EducationLevel.Resolution);
                    oSheet1.SetCellValue(startRow, 4, item.DisplayName);//Mã lớp
                    oSheet1.SetCellValue(startRow, 5, item.DisplayName);
                    oSheet1.SetCellValue(startRow, 6, item.OrderNumber);
                    oSheet1.SetCellValue(startRow, 7, item.Employee != null ? item.Employee.EmployeeCode : string.Empty);
                    oSheet1.SetCellValue(startRow, 8, !TrainingTypeItem.Equals(default(KeyValuePair<string, string>)) ? TrainingTypeItem.Value : string.Empty);
                    oSheet1.SetCellValue(startRow, 9, TrainingProgramByClass != null ? TrainingProgramByClass.Resolution : string.Empty);//CT đào tạo, bồi dưỡng
                    oSheet1.SetCellValue(startRow, 10, ApprenticeshipGroupByClass != null ? ApprenticeshipGroupByClass.GroupName : string.Empty);//Hướng nghiệp dạy nghề
                    startRow++;
                    stt++;
                }
                oSheet1.GetRange(3, 1, startRow - 1, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                //oSheet1.SetFontName("Times New Roman", 10);
            }
        }

        public Dictionary<string, string> DicApprenticeshipClassTypeList()
        {
            var dicResult = new Dictionary<string, string>();
            dicResult["1"] = "Phổ cập Giáo dục Tiểu học";
            dicResult["2"] = "Bổ túc THCS";
            dicResult["3"] = "Bổ túc THPT";
            return dicResult;
        }
        public Dictionary<string, string> DicApprenticeshipTrainingTypeList()
        {
            var dicResult = new Dictionary<string, string>();
            dicResult["1"] = "Học tập trung định kỳ";
            dicResult["2"] = "Vừa làm vừa học";
            dicResult["3"] = "Tự học có hướng dẫn";
            return dicResult;
        }

        public string GetHashKeyTranscriptsReport(Transcripts entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"PupilID",entity.PupilID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        private string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


    }
    public class ClassOrderNumber
    {
        public int ClassID { get; set; }
        public string ClassNameCut { get; set; }
        public int OrderNumber { get; set; }
    }
}
