﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{ 
    public partial class ExamPupilAbsenceBusiness
    {
        public IQueryable<ExamPupilAbsence> Search(IDictionary<string, object> search)
        {
            long examPupilAbsenceID = Utils.GetLong(search, "ExamPupilAbsenceID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            long examPupilID = Utils.GetLong(search, "ExamPupilID");
            bool reasonAbsence = Utils.GetBool(search, "ReasonAbsence",false);

            IQueryable<ExamPupilAbsence> query = this.All;

            if (examPupilAbsenceID != 0)
            {
                query = query.Where(o => o.ExamPupilAbsenceID == examPupilAbsenceID);
            }
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (examPupilID != 0)
            {
                query = query.Where(o => o.ExamPupilID == examPupilID);
            }
            if (reasonAbsence == true)
            {
                query = query.Where(o => o.ReasonAbsence == true);
            }

            return query;
        }

        public List<ExamPupilAbsenceBO> GetListExamPupil(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);

            IQueryable<ExamPupilAbsenceBO> query = (from ep in ExamPupilRepository.All.Where(o => o.SchoolID == schoolID && o.LastDigitSchoolID == lastDigitSchoolID)
                                                    join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                    join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                    where pp.IsActive == true && ep.SchoolID == schoolID && poc.AcademicYearID == academicYearID && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                    select new ExamPupilAbsenceBO
                                                    {
                                                        ExaminationsID = ep.ExaminationsID,
                                                        ExamPupilID = ep.ExamPupilID,
                                                        ExamGroupID = ep.ExamGroupID,
                                                        ExamRoomID = ep.ExamRoomID,
                                                        PupilCode = pp.PupilCode,
                                                        Name = pp.Name,
                                                        FullName = pp.FullName,
                                                        EthnicCode = pp.Ethnic.EthnicCode,
                                                        SBD = ep.ExamineeNumber,

                                                    });

            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }

            // Loại bỏ những thí sinh vắng thi
            List<ExamPupilAbsenceBO> listPupil = query.ToList();
            List<long> listPupilAbsence = this.Search(search).Select(o => o.ExamPupilID).ToList();
            for (int i = 0; i < listPupilAbsence.Count; i++)
            {
                listPupil = listPupil.Where(o => o.ExamPupilID != listPupilAbsence[i]).ToList();
            }

            return listPupil;
        }

        public List<ExamPupilAbsenceBO> GetListExamPupilAbsence(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            int educationLevelID = Utils.GetInt(search, "EducationLevelID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            string fullName = Utils.GetString(search, "FullName").ToLower();

            IQueryable<ExamPupilAbsenceBO> query = (from epv in ExamPupilAbsenceRepository.All
                                                    join ep in ExamPupilRepository.All.Where(o => o.SchoolID == schoolID && o.LastDigitSchoolID == lastDigitSchoolID) on epv.ExamPupilID equals ep.ExamPupilID
                                                    join er in ExamRoomRepository.All on ep.ExamRoomID equals er.ExamRoomID
                                                    join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                    join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                    join sc in SubjectCatRepository.All on epv.SubjectID equals sc.SubjectCatID
                                                    where poc.SchoolID == schoolID && pp.IsActive == true && poc.AcademicYearID == academicYearID
                                                    && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING && poc.AcademicYearID == academicYearID
                                                    select new ExamPupilAbsenceBO
                                                    {
                                                        ExamPupilAbsenceID = epv.ExamPupilAbsenceID,
                                                        ExaminationsID = epv.ExaminationsID,
                                                        ExamRoomID = epv.ExamRoomID,
                                                        ExamPupilID = ep.ExamPupilID,
                                                        ExamGroupID = epv.ExamGroupID,
                                                        SubjectID = epv.SubjectID,
                                                        ContentReasonAbsence = epv.ContentReasonAbsence,
                                                        ReasonAbsence = epv.ReasonAbsence,
                                                        ExamineeNumber=ep.ExamineeNumber,
                                                        Genre=pp.Genre,
                                                        BirtDate=pp.BirthDate,
                                                        PupilCode = pp.PupilCode,
                                                        Name = pp.Name,
                                                        FullName = pp.FullName,
                                                        EthnicCode = pp.Ethnic.EthnicCode,
                                                        SubjectName = sc.SubjectName,
                                                        ExamRoomCode = er.ExamRoomCode,
                                                        SBD = ep.ExamineeNumber,
                                                        ClassName = poc.ClassProfile.DisplayName

                                                    });

            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (fullName != string.Empty)
            {
                query = query.Where(o => o.FullName.ToLower().Contains(fullName));
            }

            return query.ToList();
        }

        public IQueryable<ExamPupilAbsenceBO> SearchForReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            long examinationsID = Utils.GetLong(dic, "ExaminationsID");
            long examGroupID = Utils.GetLong(dic, "ExamGroupID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            long examRoomID = Utils.GetLong(dic, "ExamRoomID");
            string fullName = Utils.GetString(dic, "FullName").ToLower();


            IQueryable<ExamPupilAbsenceBO> query = (from epa in ExamPupilAbsenceRepository.All
                                                    join ep in ExamPupilRepository.All on epa.ExamPupilID equals ep.ExamPupilID
                                                    join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                    join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                    join cp in ClassProfileRepository.All on poc.ClassID equals cp.ClassProfileID
                                                    join er in ExamRoomRepository.All on epa.ExamRoomID equals er.ExamRoomID
                                                    join sc in SubjectCatRepository.All on epa.SubjectID equals sc.SubjectCatID
                                                    where epa.ExaminationsID == examinationsID
                                                    && pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                    && poc.AcademicYearID == academicYearID
                                                    && cp.IsActive==true
                                                    select new ExamPupilAbsenceBO
                                                    {
                                                        ExamineeNumber = ep.ExamineeNumber,
                                                        FullName = pp.FullName,
                                                        BirtDate = pp.BirthDate,
                                                        Genre = pp.Genre,
                                                        ClassName = cp.DisplayName,
                                                        SubjectName = sc.SubjectName,
                                                        ExamRoomCode = er.ExamRoomCode,
                                                        ReasonAbsence = epa.ReasonAbsence,
                                                        ContentReasonAbsence = epa.ContentReasonAbsence,
                                                        ExamGroupID = epa.ExamGroupID,
                                                        SubjectID = epa.SubjectID,
                                                        ExamRoomID = epa.ExamRoomID
                                                    });


            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (fullName != string.Empty)
            {
                query = query.Where(o => o.FullName.ToLower().Contains(fullName));
            }

            return query;
        }

        public void InsertListExamPupilAbsence(IDictionary<string, object> insertInfo, long[] ExamPupilID,
            long[] CheckExamPupilID, string[] ContentReasonAbsence, long[] ReasonAbsence)
        {
            long examinationsID = Utils.GetLong(insertInfo, "ExaminationsID");
            long examGroupID = Utils.GetLong(insertInfo, "ExamGroupID");
            int subjectID = Utils.GetInt(insertInfo, "SubjectID");
            long examRoomID = Utils.GetLong(insertInfo, "ExamRoomID");

            IDictionary<string, object> columnMapping = ColumnMappings();
            /*List<ExamPupilViolate> listInsert = new List<ExamPupilViolate>();*/
            ExamPupilAbsence insert = null;
            int countPupil = ExamPupilID.Count();
            long tempID = 0;
            for (int i = 0; i < countPupil; i++)
            {
                tempID = ExamPupilID[i];
                if (CheckExamPupilID.Contains(tempID))
                {
                    insert = new ExamPupilAbsence();
                    insert.ExamPupilAbsenceID = this.GetNextSeq<long>();
                    insert.ExaminationsID = examinationsID;
                    insert.ExamGroupID = examGroupID;
                    insert.ExamRoomID = examRoomID;
                    insert.SubjectID = subjectID;
                    insert.ExamPupilID = tempID;
                    if (ReasonAbsence != null && ReasonAbsence.Contains(tempID))
                    {
                        insert.ReasonAbsence = true;
                        insert.ContentReasonAbsence = ContentReasonAbsence[i];
                    }
                    else
                    {
                        insert.ReasonAbsence = false;
                        insert.ContentReasonAbsence = ContentReasonAbsence[i];
                    }
                    insert.CreateTime = DateTime.Now;
                    /*listInsert.Add(insert);*/
                    this.Insert(insert);
                }
            }

            /*for (int i = 0; i < listInsert.Count; i++)
            {
                this.Insert(listInsert[i]);
            }*/

            this.Save();
        }

        public void DeleteExamPupilAbsence(List<long> listExamPupilAbsenceID)
        {
            if (listExamPupilAbsenceID == null)
            {
                return;
            }
            for (int i = 0; i < listExamPupilAbsenceID.Count; i++)
            {
                this.Delete(listExamPupilAbsenceID[i]);
            }
            this.Save();
        }

        public void UpdateExamPupilAbsence(long examPupilAbsenceID, string contentAbsenceReason, bool? reasonAbsence)
        {
            ExamPupilAbsence absence = this.Find(examPupilAbsenceID);
            if (absence != null)
            {
                absence.ReasonAbsence = reasonAbsence;
                if (reasonAbsence.HasValue == true)
                {
                    absence.ContentReasonAbsence = contentAbsenceReason;
                }
                absence.UpdateTime = DateTime.Now;
                this.Update(absence);
            }
            this.Save();
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamPupilAbsenceID", "EXAM_PUPIL_ABSENCE_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ContentReasonAbsence", "CONTENT_REASON_ABSENCE");
            columnMap.Add("ReasonAbsence", "REASON_ABSENCE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }

        #region Report
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamPupilAbsenceReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_VANG_THI;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamPupilAbsenceReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_VANG_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Điền các thông tin tiêu đề báo cáo
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = exam.ExaminationsName;
            string strAcademicYear = "Năm học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("A4", examinationsName);
            oSheet.SetCellValue("A5", strAcademicYear);
            oSheet.SetCellValue("H4", provinceAndDate);

            
            //Fill du lieu
            List<ExamPupilAbsenceBO> listResult = this.ExamPupilAbsenceBusiness.SearchForReport(dic)
                                                    .OrderBy(o => o.ExamineeNumber)
                                                    .ToList();

            int startRow = 9;
            int lastRow = startRow + listResult.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 10;
            int curColumn = startColumn;

            IVTWorksheet curSheet = oBook.CopySheetToLast(oSheet, "II" + (lastRow).ToString());

            for (int i = 0; i < listResult.Count; i++)
            {
                ExamPupilAbsenceBO obj = listResult[i];
                //STT
                curSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //SBD
                curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                curColumn++;
                //Ho va ten
                curSheet.SetCellValue(curRow, curColumn, obj.FullName);
                curColumn++;
                //Ngay sinh
                curSheet.SetCellValue(curRow, curColumn, obj.BirtDate);
                curColumn++;
                //Gioi tinh
                curSheet.SetCellValue(curRow, curColumn, obj.GenreName);
                curColumn++;
                //Lop
                curSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                curColumn++;
                //Mon thi
                curSheet.SetCellValue(curRow, curColumn, obj.SubjectName);
                curColumn++;
                //Phong thi
                curSheet.SetCellValue(curRow, curColumn, obj.ExamRoomCode);
                curColumn++;
                //Vang co ly do
                curSheet.SetCellValue(curRow, curColumn, obj.DisplayReasonAbsence);
                curColumn++;
                //Ly do vang
                curSheet.SetCellValue(curRow, curColumn, obj.ContentReasonAbsence);
                curColumn++;


                //Ve khung cho dong
                IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Dotted;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                curRow++;
                curColumn = startColumn;
            }

            IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

            curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
            curSheet.FitAllColumnsOnOnePage = true;

            oSheet.Delete();
            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamPupilAbsenceReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_VANG_THI;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion 

        //Lay hoc sinh vang thi boi khoa chinh
        public ExamPupilAbsenceBO GetExamPupilByID(long examPupilAbsenceID, int schoolID,int academicYearID)
        {
            ExamPupilAbsenceBO objResult = (from epv in ExamPupilAbsenceRepository.All
                                                    join ep in ExamPupilRepository.All on epv.ExamPupilID equals ep.ExamPupilID
                                                    join er in ExamRoomRepository.All on ep.ExamRoomID equals er.ExamRoomID
                                                    join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                    join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                    where poc.SchoolID == schoolID && pp.IsActive == true && poc.AcademicYearID == academicYearID
                                                    && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING && poc.AcademicYearID == academicYearID
                                                    && epv.ExamPupilAbsenceID == examPupilAbsenceID
                                                    select new ExamPupilAbsenceBO
                                                    {
                                                        ExamPupilAbsenceID = epv.ExamPupilAbsenceID,
                                                        ExaminationsID = epv.ExaminationsID,
                                                        ExamRoomID = epv.ExamRoomID,
                                                        ExamPupilID = ep.ExamPupilID,
                                                        ExamGroupID = epv.ExamGroupID,
                                                        SubjectID = epv.SubjectID,
                                                        ContentReasonAbsence = epv.ContentReasonAbsence,
                                                        ReasonAbsence = epv.ReasonAbsence,
                                                        ExamineeNumber = ep.ExamineeNumber,
                                                        Genre = pp.Genre,
                                                        BirtDate = pp.BirthDate,
                                                        PupilCode = pp.PupilCode,
                                                        Name = pp.Name,
                                                        FullName = pp.FullName,
                                                        EthnicCode = pp.Ethnic.EthnicCode,
                                                        ExamRoomCode = er.ExamRoomCode,
                                                        SBD = ep.ExamineeNumber
                                                    }).FirstOrDefault();
            return objResult;
               
        }
    }
}