/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Web.Security;

namespace SMAS.Business.Business
{
    public partial class AuthenticationTokenBusiness
    {
        public AuthenticationToken ValidateToken(int userAccountID, Guid token)
        {
            AuthenticationToken authToken = this.All.FirstOrDefault(o => o.UserAccountID == userAccountID && o.Token == token);

            if (authToken != null)
            {
                //Neu chua het han, cap nhat lai thoi gian het han
                if (DateTime.Compare(DateTime.Now, authToken.ExpiredTime) <= 0)
                {
                    authToken.ExpiredTime = DateTime.Now.AddSeconds(GlobalConstants.TOKEN_EXPIRED_TIME);
                }
                //Neu het han thi renew token khac
                else
                {
                    authToken.Token = Guid.NewGuid();
                    authToken.ExpiredTime = DateTime.Now.AddSeconds(GlobalConstants.TOKEN_EXPIRED_TIME);
                }

                this.Update(authToken);
                this.Save();
            }

            return authToken;
        }
    }
}
