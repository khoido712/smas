﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author  trangdd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using System.Drawing;
using SMAS.Models.Models.CustomModels;
using System.Transactions;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class JudgeRecordBusiness
    {
        public int startCol = 5;
        private const int rowHeaderStart = 6;
        private const int rowHeaderEnd = 13;

        #region Search

        /// <summary>
        /// Tìm kiếm thông tin chi tiết hồ sơ kết quả môn học đánh giá theo giai đoạn / học kỳ / năm học
        /// </summary>
        /// <author>trangdd</author>
        /// <date>02/10/2012</date>
        private IQueryable<JudgeRecord> SearchJudgeRecord(IDictionary<string, object> dic)
        {
            int JudgeRecordID = Utils.GetInt(dic, "JudgeRecordID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Semester = Utils.GetInt(dic, "Semester", -1);
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            string Judgement = Utils.GetString(dic, "Judgement");
            string ReTestJudgement = Utils.GetString(dic, "ReTestJudgement");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            int OrderNumber = Utils.GetInt(dic, "OrderNumber");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");

            //tien hanh tim
            IQueryable<JudgeRecord> ListJudgeRecord = JudgeRecordRepository.All.Where(o => o.PupilProfile.IsActive);
            //AnhVD 20131217 
            if (ClassID != 0)
            {
                ListJudgeRecord = from jr in ListJudgeRecord
                                  join cp in ClassProfileBusiness.All on jr.ClassID equals cp.ClassProfileID
                                  where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                  && jr.ClassID == ClassID
                                  select jr;
            }
            if (SchoolID > 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Last2digitNumberSchool == SchoolID % 100));
            }
            //End
            if (JudgeRecordID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.JudgeRecordID == JudgeRecordID));
            }
            if (PupilID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.PupilID == PupilID));
            }
            if (SchoolID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.AcademicYearID == AcademicYearID));
            }
            if (MarkTypeID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (SubjectID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (Semester != -1)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Semester == Semester));
            }
            // Quanglm Sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        ListJudgeRecord = ListJudgeRecord.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (Judgement != null && !Judgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Judgement.Contains(Judgement)));
            }
            if (ReTestJudgement != null && !ReTestJudgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.ReTestJudgement.Contains(ReTestJudgement)));
            }
            if (CreatedDate.HasValue)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                   o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (OrderNumber != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.OrderNumber == OrderNumber));
            }
            if (MarkTitle != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.MarkType.Title == MarkTitle));
            }
            if (Title != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Title == Title));
            }
            if (strMarkType != null && strMarkType != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.ClassProfile.EducationLevelID == EducationLevelID));
            }

            return ListJudgeRecord;
        }

        #endregion Search

        #region Validate

        public void Validate(JudgeRecord JudgeRecord)
        {
            ValidationMetadata.ValidateObject(JudgeRecord);

            //If(SchoolID != null): SchoolID: FK(SchoolProfile)
            if (JudgeRecord.SchoolID > 0)
            {
                SchoolProfileBusiness.CheckAvailable(JudgeRecord.SchoolID, "JudgeRecord_Label_SchoolID", true);
            }

            //SubjectID, ClassID: not compatible(ClassSubject)
            bool SubjectCompatible = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject",
                   new Dictionary<string, object>()
                {
                    {"ClassID",JudgeRecord.ClassID},
                    {"SubjectID",JudgeRecord.SubjectID}
                }, null);
            if (!SubjectCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",JudgeRecord.SchoolID},
                    {"AcademicYearID",JudgeRecord.AcademicYearID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //PupilID, AcademicYearID: not compatible(PupilProfile)
            bool PupilCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                     new Dictionary<string, object>()
                {
                    {"PupilProfileID",JudgeRecord.PupilID},
                    {"CurrentAcademicYearID",JudgeRecord.AcademicYearID}
                }, null);
            if (!PupilCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //PupilID, ClassID: not compatible(PupilProfile)
            bool ClassCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                     new Dictionary<string, object>()
                {
                    {"PupilProfileID",JudgeRecord.PupilID},
                    {"CurrentClassID",JudgeRecord.ClassID}
                }, null);
            if (!ClassCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(JudgeRecord.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }

            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
            PupilOfClass PupilOfClass = PupilOfClassBusiness.All.Where(o => o.PupilID == JudgeRecord.PupilID).Where(o => o.ClassID == JudgeRecord.ClassID).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }
            ////ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester) = true: Không cho phép nhập điểm (Đối với môn miễn giảm)
            ////Chưa có hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester)
            //if (ExemptedSubjectBusiness.IsExemptedSubject(JudgeRecord.PupilID, JudgeRecord.ClassID, JudgeRecord.AcademicYearID, JudgeRecord.SubjectID, JudgeRecord.Semester))
            //{
            //    throw new BusinessException("Common_Validate_IsExemptedSubject");
            //}

            //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = 1 (nếu kết quả trả về != null => là môn nhận xét)
            IDictionary<string, object> SearchClassSubject = new Dictionary<string, object>();
            SearchClassSubject["SubjectID"] = JudgeRecord.SubjectID;
            SearchClassSubject["ClassID"] = JudgeRecord.ClassID;
            IEnumerable<ClassSubject> ListClassSubjectFromDB = this.ClassSubjectBusiness.SearchBySchool(JudgeRecord.SchoolID, SearchClassSubject);
            ClassSubject classSubject = ListClassSubjectFromDB.FirstOrDefault();
            if (classSubject.IsCommenting != 1)
            {
                throw new BusinessException("Common_Validate_IsCommenting ");
            }
        }

        #endregion Validate

        #region Insert

        /// <summary>
        /// Thêm mới thông tin chi tiết hồ sơ kết quả môn học đánh giá theo giai đoạn / học kỳ / năm học
        /// </summary>
        /// <author>trangdd</author>
        /// <date>02/10/2012</date>
        /// update by namdv3
        public bool InsertJudgeRecord(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? Period, int? EmployeeID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, int SubjectID)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
                bool isDBChange = false;
                #region validate 1
                if (lstJudgeRecord == null || lstJudgeRecord.Count == 0) return isDBChange;
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

                //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");

                // Kiem tra quyen giao vien bo mon
                if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID, Semester))
                    throw new BusinessException("JudgeRecord_Label_NotAuthen_Add");

                //Academic not compatible
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //PeriodID(PeriodDeclarationID), Semester: not compatible(PeriodDeclaration)
                if (Period != null)
                {
                    bool PeriodDeclarationCompatible = new PeriodDeclarationRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                            new Dictionary<string, object>()
                    {
                    {"PeriodDeclarationID",Period},
                    {"Semester",Semester}
                    }, null);
                    if (!PeriodDeclarationCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion
                var listPupilID = lstJudgeRecord.Select(u => u.PupilID).Distinct().ToList();
                ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();
                ClassProfile classProfile = classSubject.ClassProfile;
                var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               where poc.ClassID == ClassID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                               && listPupilID.Contains(poc.PupilID) && pp.IsActive
                               && poc.AcademicYearID == academicYear.AcademicYearID
                               select new { poc.Status, poc.AcademicYearID, poc.ClassID }
                                       ).ToList();
                // Kiem tra trang thai hoc sinh dang hoc
                if (listPOC.Count() != listPupilID.Count)
                {
                    throw new BusinessException("Import_Validate_StudyingStatus");
                }
                // Kiem tra nam hoc va lop hoc hien tai
                if (listPOC.Any(o => o.AcademicYearID != AcademicYearID || o.ClassID != ClassID))
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                #region Lay thong tin mien giam
                List<ExemptedSubject> listExemptedByClass = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID).Where(u => u.SubjectID == SubjectID).ToList();

                // HKT
                List<ExemptedSubject> listExemptedHKI = listExemptedByClass.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                //HK2
                List<ExemptedSubject> listExemptedHKII = listExemptedByClass.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<ExemptedSubject> listExempted = new List<ExemptedSubject>();
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    listExempted.AddRange(listExemptedHKI);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listExempted.AddRange(listExemptedHKII);
                }
                #endregion

                #region validate 2
                if (classSubject == null)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = ISCOMMENTING_TYPE_JUDGE
                //(nếu kết quả trả về != null => là môn nhận xét)
                if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    throw new BusinessException("SummedUpRecord_Labal_ErrNXSubject");

                bool subjectLearnInHKI = classSubject.SectionPerWeekFirstSemester > 0;
                bool subjectLearnInHKII = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester > 0;

                foreach (var JudgeRecord in lstJudgeRecord)
                {
                    //if (JudgeRecord.Judgement != "-1")
                    //{
                    //    ValidationMetadata.ValidateObject(JudgeRecord);
                    //}
                    //Kiem tra diem thi hop le
                    if (JudgeRecord.Judgement != "-1")
                    {
                        UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, GlobalConstants.ISCOMMENTING_TYPE_JUDGE, JudgeRecord.Judgement);
                    }

                    //Không cho phép nhập điểm (Đối với môn miễn giảm)
                    //if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    //    throw new BusinessException("ExemptedSubject_Label_IsExemptedSubject");
                }
                #endregion

                string strMarkTitle = Period.HasValue ? MarkRecordBusiness.GetMarkTitle(Period.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<string> lstTitle = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID, Period ?? 0);
                List<string> ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();

                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                //neu khong phai tai khoan quan tri truong va tat ca con diem bi khoa thi bao loi
                if (lstLockedMarkDetail.Contains("LHK") && !isSchoolAdmin)
                {
                    throw new BusinessException("MarkRecord_Validate_MarkLock");
                }
                // Neu khong phai la tk quan tri truong thi bo qua cac con diem bi khoa
                if (!isSchoolAdmin)
                {
                    lstJudgeRecord = lstJudgeRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                }
                IDictionary<string, object> SearchDelete = new Dictionary<string, object>();
                SearchDelete["AcademicYearID"] = AcademicYearID;
                SearchDelete["SchoolID"] = SchoolID;
                SearchDelete["ClassID"] = ClassID;
                SearchDelete["SubjectID"] = SubjectID;
                SearchDelete["Semester"] = Semester;
                SearchDelete["checkWithClassMovement"] = "1";
                IQueryable<JudgeRecord> iqJudgeDb = this.SearchJudgeRecord(SearchDelete).Where(u => listPupilID.Contains(u.PupilID));
                // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                if (!isSchoolAdmin)
                {
                    iqJudgeDb = iqJudgeDb.Where(v => !ArrLockedMark.Contains(v.Title));
                    for (int i = 0; i < ArrLockedMark.Count; i++)
                    {
                        if (strtmp.Contains(ArrLockedMark[i]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[i], "");
                        }
                    }
                }

                if (Period.HasValue)
                    iqJudgeDb = iqJudgeDb.Where(u => lstTitle.Contains(u.Title));
                List<JudgeRecord> listJudgeDb = iqJudgeDb.ToList();



                //Insert du lieu moi vao DB
                List<JudgeRecord> lstJudgeInsert = new List<JudgeRecord>();
                if (lstJudgeRecord != null && lstJudgeRecord.Count > 0)
                {
                    lstJudgeInsert = lstJudgeRecord.Where(p => !string.IsNullOrEmpty(p.Judgement) && !"-1".Equals(p.Judgement)).ToList();
                    JudgeRecord mrOfPupilObj = null;
                    if (lstJudgeInsert != null && lstJudgeInsert.Count > 0)
                    {
                        int _pupilId = 0;
                        string _title = string.Empty;
                        string _mark = string.Empty;
                        foreach (JudgeRecord itemInsert in lstJudgeInsert)
                        {
                            _pupilId = itemInsert.PupilID;
                            _title = itemInsert.Title;
                            _mark = itemInsert.Judgement;
                            //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                            //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                            mrOfPupilObj = listJudgeDb.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                            if (mrOfPupilObj != null)
                            {
                                if (mrOfPupilObj.Judgement.Equals(_mark))
                                {
                                    itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                    itemInsert.LogChange = mrOfPupilObj.LogChange;
                                    itemInsert.SynchronizeID = mrOfPupilObj.SynchronizeID;
                                }
                                else
                                {
                                    itemInsert.ModifiedDate = DateTime.Now;
                                    itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                                }
                                itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                            }
                            else
                            {
                                itemInsert.CreatedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                            }

                            itemInsert.CreatedAcademicYear = academicYear.Year;
                        }

                        this.BulkInsert(lstJudgeInsert, JudgeColumnMappings(), "JudgeRecordID");
                        if (lstJudgeInsert.Count > 0)
                        {
                            isDBChange = true;
                        }
                    }
                }
                //Xoa tat ca du lieu trong DB
                List<long> lstJudgeIdDelete = lstJudgeRecord.Where(p => p.JudgeRecordID > 0).Select(m => m.JudgeRecordID).Distinct().ToList();
                if (lstJudgeIdDelete.Count > 0)
                {
                    this.DeleteJudgeRecordByListID(AcademicYearID, SchoolID, 0, lstJudgeIdDelete);
                    isDBChange = true;
                }
                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                //List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listJudgeDb.Select(o => o.PupilID)).ToList();
                List<int> pupilIDs = lstJudgeRecord.Select(p => p.PupilID).Distinct().ToList();
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = Period;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                //ThreadMarkBusiness.InsertActionList(info);
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion
                // Neu co hoc sinh khong thay doi diem moi phai lay lai danh sach TBM
                /*if (lstSummedUpRecord.Count != listPupilID.Count)
                {
                    lstSummedUpRecord = lstSummedUpRecord.Where(o => listPupilID.Contains(o.PupilID)).ToList();
                }*/
                if (lstSummedUpRecord.Count == 0)
                    return isDBChange;
                List<SummedUpRecord> listSummedUpInsert = new List<SummedUpRecord>();
                #region hoc ki 1
                //Tìm kiếm bản ghi trong bảng SummedUpRecord
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Period != null)
                {
                    // Xoa du lieu cu
                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["AcademicYearID"] = AcademicYearID;
                    SearchInfo["ClassID"] = ClassID;
                    SearchInfo["SubjectID"] = SubjectID;
                    SearchInfo["PeriodID"] = Period;
                    SearchInfo["Semester"] = Semester;
                    // Khong can join de tim isactive voi HS va truong
                    SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
                    List<SummedUpRecord> ListSummedUpRecordFromDB = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, SearchInfo)
                        .Where(o => listPupilID.Contains(o.PupilID))
                        .ToList();

                    List<int> listPupilIDEx = listPupilID.Where(u => listExemptedHKII.Select(a => a.PupilID).Contains(u)).ToList();

                    //neu mien giam HK2 hoac khong hoc hoc ky II va nhap diem theo ky
                    if (!Period.HasValue && (classSubject.SectionPerWeekSecondSemester == 0 || (listPupilIDEx != null && listPupilIDEx.Count() > 0)))
                    {
                        // Xoa diem ca nam va lay bang diem hoc ky I
                        SearchInfo["AcademicYearID"] = AcademicYearID;
                        SearchInfo["ClassID"] = ClassID;
                        SearchInfo["SubjectID"] = SubjectID;
                        SearchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                        // Khong can join de tim isactive voi HS va truong
                        SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";

                        // Lay danh sach hoac la mien giam hoac ca lop neu mon do khong hoc o ky 2
                        var ListSummedUpRecordFromDBALL = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, SearchInfo)
                            .Where(o => classSubject.SectionPerWeekSecondSemester == 0 || listPupilIDEx.Contains(o.PupilID))
                            .ToList();

                        if (ListSummedUpRecordFromDBALL != null & ListSummedUpRecordFromDBALL.Count > 0)
                        {
                            //SummedUpRecordBusiness.DeleteAll(ListSummedUpRecordFromDBALL);
                            //Xoa tat ca du lieu trong DB
                            List<int> listValSummed = ListSummedUpRecordFromDBALL.Select(p => p.PupilID).ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, GlobalConstants.SEMESTER_OF_YEAR_ALL, SchoolID, AcademicYearID);

                        }
                    }

                    //Thực hiện xóa bản ghi nếu thấy trong bảng SummedUpRecord
                    if (ListSummedUpRecordFromDB != null & ListSummedUpRecordFromDB.Count > 0)
                    {
                        //SummedUpRecordBusiness.DeleteAll(ListSummedUpRecordFromDB);
                        //Xoa tat ca du lieu trong DB                    
                        List<int> listValSummed = ListSummedUpRecordFromDB.Select(p => p.PupilID).ToList();
                        MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                    }

                    List<SummedUpRecord> lstSummedUpRecordTmp = lstSummedUpRecord.Where(p => !p.JudgementResult.Equals(string.Empty) && p.JudgementResult != "-1").ToList();
                    foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecordTmp)
                    {
                        SummedUpRecord.CreatedAcademicYear = academicYear.Year;
                        listSummedUpInsert.Add(SummedUpRecord);
                        //truong hop chi o hoc ky I hoac mien giam ky 2 va dang nhap diem theo ky
                        if (!Period.HasValue && (classSubject.SectionPerWeekSecondSemester == 0 || (listPupilIDEx != null && listPupilIDEx.Any(u => u == SummedUpRecord.PupilID))))
                        {
                            SummedUpRecord summedUpSemester = new SummedUpRecord()
                            {
                                AcademicYearID = SummedUpRecord.AcademicYearID,
                                ClassID = SummedUpRecord.ClassID,
                                Comment = SummedUpRecord.Comment,
                                IsCommenting = SummedUpRecord.IsCommenting,
                                IsOldData = SummedUpRecord.IsOldData,
                                JudgementResult = SummedUpRecord.JudgementResult,
                                PeriodID = SummedUpRecord.PeriodID,
                                PupilID = SummedUpRecord.PupilID,
                                ReTestJudgement = SummedUpRecord.ReTestJudgement,
                                ReTestMark = SummedUpRecord.ReTestMark,
                                SchoolID = SummedUpRecord.SchoolID,
                                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL,
                                SubjectID = SummedUpRecord.SubjectID,
                                SummedUpDate = SummedUpRecord.SummedUpDate,
                                SummedUpMark = SummedUpRecord.SummedUpMark,
                                CreatedAcademicYear = academicYear.Year
                            };
                            //this.SummedUpRecordBusiness.Insert(summedUpSemester);
                            listSummedUpInsert.Add(summedUpSemester);
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                }
                #endregion hoc ki 1
                #region hoc ky 2
                else
                {
                    #region insert ky II
                    //    Bước 1: với mỗi phần tử trong lstPupilExempted
                    /* Tìm kiếm trong bảng SummedUpRecord theo
                          PupilID = lstPupilExempted [i].PupilID
                          AcademicYearID= AcademicYearID
                          ClassID = ClassID
                          SubjectID = SubjectID
                          Semester = SEMESTER_OF_YEAR_ALL
                        Thực hiện xóa bản ghi nếu thấy */
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = AcademicYearID;
                    dic["ClassID"] = ClassID;
                    dic["SubjectID"] = SubjectID;
                    // Khong can join de tim isactive voi HS va truong
                    dic["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
                    //dic["PeriodID"] = Period;
                    // Diem TBM cua 3 HK de xu ly
                    List<SummedUpRecord> lstSummedUpAll = SummedUpRecordBusiness.SearchBySchool(SchoolID, dic)
                        .Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL))
                        .ToList();
                    // Diem TBM cua HK1
                    List<SummedUpRecord> lstSummedUpHKI = lstSummedUpAll.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                    if (lstPupilExempted != null && lstPupilExempted.Count > 0)
                    {
                        List<SummedUpRecord> listSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() {
                    {"AcademicYearID", AcademicYearID},
                    {"ClassID", ClassID},
                    {"SubjectID", SubjectID},
                    {"NoCheckIsActivePupilSchool", "NoCheckIsActivePupilSchool"},
                    {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_ALL},
                    }).Where(o => lstPupilExempted.Contains(o.PupilID)).ToList();
                        if (listSummedUpRecord.Count > 0)
                        {
                            //SummedUpRecordBusiness.DeleteAll(listSummedUpRecord);
                            List<int> listValSummed = listSummedUpRecord.Select(p => p.PupilID).ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        /*- Kiểm tra học kỳ 1 có Miễn giảm hay không: Gọi hàm
                         * ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                            + Nếu kết quả trả về True => return
                            + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                            + False: thì return
                            + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách gọi hàm
                         * SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST) kết quả trả về nếu:
                            + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                            + JudgeResult != null: Điểm TBM cả năm = Điểm TBM học kỳ 1 (JudgeResult3 = JudgeResult)
                         * Thực hiện insert vào bảng SummedUpRecord
                         */

                        foreach (var pupilid in lstPupilExempted)
                        {
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == pupilid);
                            if (!IsExemptedSubject && subjectLearnInHKI) //Duoc miem giam hoc ky 2, co hoc o HK I
                            {
                                SummedUpRecord surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == pupilid);
                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecord surIII = new SummedUpRecord();
                                    surIII.AcademicYearID = surI.AcademicYearID;
                                    surIII.ClassID = surI.ClassID;
                                    surIII.Comment = surI.Comment;
                                    surIII.IsCommenting = surI.IsCommenting;
                                    //surIII.IsLegalBot = surI.IsLegalBot;
                                    surIII.JudgementResult = surI.JudgementResult;
                                    surIII.PeriodID = surI.PeriodID;
                                    surIII.PupilID = surI.PupilID;
                                    surIII.ReTestJudgement = surI.ReTestJudgement;
                                    surIII.ReTestMark = surI.ReTestMark;
                                    surIII.SchoolID = surI.SchoolID;
                                    surIII.SubjectID = surI.SubjectID;
                                    surIII.SummedUpDate = surI.SummedUpDate;
                                    surIII.SummedUpMark = surI.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }
                    /*
                        Bước 2:
                        // Lưu điểm kỳ 2
                        Tìm kiếm trong bảng SummedUpRecord theo
                        -	PupilID = lstSummedUpRecord[i].PupilID
                        -	AcademicYearID= lstSummedUpRecord[i].AcademicYearID
                        -	ClassID = lstSummedUpRecord[i].ClassID
                        -	SubjectID = lstSummedUpRecord[i].SubjectID
                        -	Semester = lstSummedUpRecord[i].Semester
                        Thực hiện xóa bản ghi nếu thấy
                        Thực hiện insert lstSummedUpRecord vào SummedUpRecord
                     */
                    if (lstSummedUpRecord != null && lstSummedUpRecord.Count > 0)
                    {
                        // Xoa du lieu cu HK2 va ca nam
                        //lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                        //  || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && listPupilID.Contains(o.PupilID)).ToList();
                        // Xoa du lieu cu HK2 va ca nam
                        if (Period > 0)
                        {
                            lstSummedUpAll = lstSummedUpAll.Where(o => o.PeriodID == Period && listPupilID.Contains(o.PupilID)).ToList();
                        }
                        else
                        {
                            lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && listPupilID.Contains(o.PupilID)).ToList();
                        }

                        if (lstSummedUpAll.Count > 0)
                        {
                            //SummedUpRecordBusiness.DeleteAll(lstSummedUpAll);
                            //SummedUpRecordBusiness.Save();
                            List<int> listValSummed = lstSummedUpAll.Select(p => p.PupilID).ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        List<SummedUpRecord> lstSummedUpRecordTmp = lstSummedUpRecord.Where(p => !p.JudgementResult.Equals(string.Empty) && p.JudgementResult != "-1").ToList();
                        foreach (var summedUpRecord in lstSummedUpRecordTmp)
                        {
                            summedUpRecord.CreatedAcademicYear = academicYear.Year;
                            listSummedUpInsert.Add(summedUpRecord);
                        }
                        // SummedUpRecordBusiness.Save();

                        /*
                         // tính cả năm
                        - Kiểm tra học kỳ 1 có Miễn giảm hay không:
                         * Gọi hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                        + Nếu kết quả trả về True => Điểm TBM cả năm = TBM học kỳ 2
                        + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                        + False: thì return Điểm TBM cả năm = TBM học kỳ 2
                        + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách
                         * gọi hàm SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST)
                         * kết quả trả về nếu:
                        + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                        + JudgeResult != null:
                         * Nếu ((sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “Đ”)
                         * or (sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “Đ”)) thì JudgementResult3 = “Đ” – cả năm.
                         Nếu ((sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “CĐ”)
                         * or (sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “CĐ”)) thì JudgementResult3 = “CĐ” – cả năm.
                         */

                        foreach (var summedUpRecord in lstSummedUpRecord)
                        {
                            // lay diem != -1
                            if (summedUpRecord.JudgementResult.Equals("-1"))
                            {
                                continue;
                            }
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == summedUpRecord.PupilID);
                            if (IsExemptedSubject || !subjectLearnInHKI)
                            {
                                SummedUpRecord surIII = new SummedUpRecord();
                                surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                surIII.ClassID = summedUpRecord.ClassID;
                                surIII.Comment = summedUpRecord.Comment;
                                surIII.IsCommenting = summedUpRecord.IsCommenting;
                                //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                surIII.JudgementResult = summedUpRecord.JudgementResult;
                                surIII.PeriodID = summedUpRecord.PeriodID;
                                surIII.PupilID = summedUpRecord.PupilID;
                                surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                surIII.ReTestMark = summedUpRecord.ReTestMark;
                                surIII.SchoolID = summedUpRecord.SchoolID;
                                surIII.SubjectID = summedUpRecord.SubjectID;
                                surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                surIII.CreatedAcademicYear = academicYear.Year;
                                surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                //SummedUpRecordBusiness.Insert(surIII);
                                listSummedUpInsert.Add(surIII);
                            }
                            else
                            {
                                SummedUpRecord surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == summedUpRecord.PupilID);

                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecord surIII = new SummedUpRecord();
                                    surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                    surIII.ClassID = summedUpRecord.ClassID;
                                    surIII.Comment = summedUpRecord.Comment;
                                    surIII.IsCommenting = summedUpRecord.IsCommenting;
                                    //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                    surIII.JudgementResult = summedUpRecord.JudgementResult;
                                    surIII.PeriodID = summedUpRecord.PeriodID;
                                    surIII.PupilID = summedUpRecord.PupilID;
                                    surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                    surIII.ReTestMark = summedUpRecord.ReTestMark;
                                    surIII.SchoolID = summedUpRecord.SchoolID;
                                    surIII.SubjectID = summedUpRecord.SubjectID;
                                    surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                    surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;

                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }

                        if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                        {
                            SummedUpRecordBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                        }
                    }
                    #endregion insert ky II
                }
                #endregion hoc ky 2

                JudgeRecordBusiness.Save();
                //Tinh TBM cho dot hoac ky

                SummedJudgeMark(listPupilID, academicYear, Semester, Period, classSubject);
                return isDBChange;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public void InsertJudgeRecordByListSubject(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, List<int> listSubjectID, int? EmployeeID)
        {
            if (lstJudgeRecord == null || lstJudgeRecord.Count == 0) return;
            if (listSubjectID == null || listSubjectID.Count == 0)
            {
                return;
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");

            int SubjectID = 0;

            var listPupilIDAll = lstJudgeRecord.Select(u => u.PupilID).Distinct().ToList();
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SchoolID", SchoolID } }).ToList();
            var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                           join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                           where poc.ClassID == ClassID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                           && listPupilIDAll.Contains(poc.PupilID) && pp.IsActive
                           select new { poc.Status, poc.AcademicYearID, poc.ClassID }
                             ).ToList();
            //Thong tin mien giam cua truong
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID).ToList();

            //Dictionary<int, string> dicLockRecord = MarkRecordBusiness.GetLockMarkTitleBySubject(SchoolID, AcademicYearID, ClassID, Semester, 0);

            IDictionary<string, object> SearchDelete = new Dictionary<string, object>();
            SearchDelete["AcademicYearID"] = AcademicYearID;
            SearchDelete["SchoolID"] = SchoolID;
            SearchDelete["ClassID"] = ClassID;
            //SearchDelete["SubjectID"] = SubjectID;
            SearchDelete["Semester"] = Semester;
            SearchDelete["checkWithClassMovement"] = "1";
            List<JudgeRecord> iqJudgeDbAllSubject = this.SearchJudgeRecord(SearchDelete).Where(u => listPupilIDAll.Contains(u.PupilID)).ToList();

            // Tim kiem danh sach them, sua, xoa
            //List<JudgeRecord> listInsert = new List<JudgeRecord>();
            //List<JudgeRecord> listUpdate = new List<JudgeRecord>();

            // Xoa du lieu cu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            //SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["PeriodID"] = Period;
            //SearchInfo["Semester"] = Semester;
            // Khong can join de tim isactive voi HS va truong
            SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            List<SummedUpRecord> ListSummedUpRecordFromDBAllSubject = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, SearchInfo)
                .Where(o => listPupilIDAll.Contains(o.PupilID))
                .ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["ClassID"] = ClassID;
            dic["PeriodID"] = Period;
            dic["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            // Diem TBM cua 3 HK de xu ly
            List<SummedUpRecord> lstSummedUpAllSubject = SummedUpRecordBusiness.SearchBySchool(SchoolID, dic)
                .Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                    || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL))
                .ToList();

            List<JudgeRecord> lsttmp = new List<JudgeRecord>();

            for (int i = 0; i < listSubjectID.Count; i++)
            {
                SubjectID = listSubjectID[i];
                var listPupilID = lstJudgeRecord.Where(p => p.SubjectID == SubjectID).Select(u => u.PupilID).Distinct().ToList();
                // Kiem tra quyen giao vien bo mon
                if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID, Semester))
                    throw new BusinessException("JudgeRecord_Label_NotAuthen_Add");


                //Academic not compatible
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                if (Period != null)
                {
                    bool PeriodDeclarationCompatible = new PeriodDeclarationRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                            new Dictionary<string, object>()
                            {
                                    {"PeriodDeclarationID",Period},
                                    {"Semester",Semester}
                            }, null);
                    if (!PeriodDeclarationCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                ClassSubject classSubject = listClassSubject.Where(p => p.SubjectID == SubjectID).FirstOrDefault();
                ClassProfile classProfile = classSubject.ClassProfile;

                // Kiem tra nam hoc va lop hoc hien tai
                if (listPOC.Any(o => o.AcademicYearID != AcademicYearID || o.ClassID != ClassID))
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                #region Lay thong tin mien giam

                List<ExemptedSubject> listExemptedByClass = listExemptedSubject.Where(u => u.SubjectID == SubjectID).ToList();
                // HKT
                List<ExemptedSubject> listExemptedHKI = listExemptedByClass.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                //HK2
                List<ExemptedSubject> listExemptedHKII = listExemptedByClass.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<ExemptedSubject> listExempted = new List<ExemptedSubject>();
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    listExempted.AddRange(listExemptedHKI);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listExempted.AddRange(listExemptedHKII);
                }
                #endregion

                #region validate 2
                if (classSubject == null)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = ISCOMMENTING_TYPE_JUDGE
                //(nếu kết quả trả về != null => là môn nhận xét)
                if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    throw new BusinessException("SummedUpRecord_Labal_ErrNXSubject");

                bool subjectLearnInHKI = classSubject.SectionPerWeekFirstSemester > 0;
                bool subjectLearnInHKII = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester > 0;

                foreach (var JudgeRecord in lstJudgeRecord)
                {
                    //if (JudgeRecord.Judgement != "-1")
                    //{
                    //    ValidationMetadata.ValidateObject(JudgeRecord);
                    //}
                    //Kiem tra diem thi hop le
                    if (JudgeRecord.Judgement != "-1")
                    {
                        UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, GlobalConstants.ISCOMMENTING_TYPE_JUDGE, JudgeRecord.Judgement);
                    }

                    //Không cho phép nhập điểm (Đối với môn miễn giảm)
                    // if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    //    throw new BusinessException("ExemptedSubject_Label_IsExemptedSubject");
                }
                #endregion

                string strMarkTitle = Period.HasValue ? MarkRecordBusiness.GetMarkTitle(Period.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<string> lstTitle = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                List<int> lstPupilIDExemted = listExempted.Select(p => p.PupilID).ToList();

                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                //string lstLockedMarkDetail = dicLockRecord.Where(p => p.Key == SubjectID).Select(p => p.Value).FirstOrDefault();
                string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID,Period??0);
                //MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                List<string> ArrLockedMark = (lstLockedMarkDetail != null && lstLockedMarkDetail.Count() > 0) ? lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList() : new List<string>();
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                //neu khong phai tai khoan quan tri truong va tat ca con diem bi khoa thi bao loi
                if (ArrLockedMark.Contains("LHK") && !isSchoolAdmin)
                {
                    throw new BusinessException("MarkRecord_Validate_MarkLock");
                }
                // Neu khong phai la tk quan tri truong thi bo qua cac con diem bi khoa
                if (!isSchoolAdmin)
                {
                    lsttmp = lstJudgeRecord.Where(o => !ArrLockedMark.Contains(o.Title) && o.SubjectID == SubjectID).ToList();
                    for (int j = 0; j < ArrLockedMark.Count; j++)
                    {
                        if (strtmp.Contains(ArrLockedMark[j]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[j], "");
                        }
                    }
                }
                else
                {
                    lsttmp = lstJudgeRecord.Where(o => o.SubjectID == SubjectID).ToList();
                }

                List<JudgeRecord> iqJudgeDb = iqJudgeDbAllSubject.Where(p => p.SubjectID == SubjectID).ToList();
                // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                if (!isSchoolAdmin)
                {
                    iqJudgeDb = iqJudgeDb.Where(v => !ArrLockedMark.Contains(v.Title)).ToList();
                }
                if (Period.HasValue)
                {
                    iqJudgeDb = iqJudgeDb.Where(u => lstTitle.Contains(u.Title)).ToList();
                }

                ///Danh sach diem nhan xet theo mon hoc
                List<JudgeRecord> listJudgeDb = iqJudgeDb;
                List<int> lstPupilIDtmp = listPupilID.Where(p => !(lstPupilIDExemted.Contains(p))).ToList();
                //Insert du lieu moi vao DB
                JudgeRecord judgePupilIdObj = null;
                List<JudgeRecord> lstJudgeInsert = new List<JudgeRecord>();

                if (lsttmp != null && lsttmp.Count > 0)
                {

                    lstJudgeInsert = lsttmp.Where(p => !p.Judgement.Equals(string.Empty) && !"-1".Equals(p.Judgement) && lstPupilIDtmp.Contains(p.PupilID)).ToList();

                    int _pupilId = 0;
                    string _title = string.Empty;
                    string _mark = string.Empty;
                    foreach (JudgeRecord itemInsert in lstJudgeInsert)
                    {
                        _pupilId = itemInsert.PupilID;
                        _title = itemInsert.Title;
                        _mark = itemInsert.Judgement;
                        //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                        //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                        judgePupilIdObj = listJudgeDb.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                        if (judgePupilIdObj != null)
                        {
                            if (judgePupilIdObj.Judgement.Equals(_mark))
                            {
                                itemInsert.ModifiedDate = judgePupilIdObj.ModifiedDate;
                                itemInsert.LogChange = judgePupilIdObj.LogChange;
                                itemInsert.SynchronizeID = judgePupilIdObj.SynchronizeID;
                            }
                            else
                            {
                                itemInsert.ModifiedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                            }
                            itemInsert.CreatedDate = judgePupilIdObj.CreatedDate;
                        }
                        else
                        {
                            itemInsert.CreatedDate = DateTime.Now;
                            itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                        }

                        itemInsert.CreatedAcademicYear = academicYear.Year;
                    }

                    if (lstJudgeInsert != null && lstJudgeInsert.Count > 0)
                    {
                        this.BulkInsert(lstJudgeInsert, JudgeColumnMappings(), "JudgeRecordID");
                    }
                }

                //Xoa tat ca du lieu trong DB theo JudgeRecordID
                List<long> lstJudgeIdDelete = lsttmp.Where(p => p.JudgeRecordID > 0 && lstPupilIDtmp.Contains(p.PupilID)).Select(m => m.JudgeRecordID).Distinct().ToList();
                if (lstJudgeIdDelete.Count > 0)
                {
                    this.DeleteJudgeRecordByListID(AcademicYearID, SchoolID, 0, lstJudgeIdDelete);
                }

                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                //List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listJudgeDb.Select(o => o.PupilID)).ToList();
                List<int> pupilIDs = lstJudgeInsert.Select(p => p.PupilID).Distinct().ToList();
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = Period;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion
                if (lstSummedUpRecord.Count == 0)
                    return;

                List<SummedUpRecord> listSummedUpInsert = new List<SummedUpRecord>();
                #region hoc ki 1
                //Tìm kiếm bản ghi trong bảng SummedUpRecord
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    List<SummedUpRecord> ListSummedUpRecordFromDB = ListSummedUpRecordFromDBAllSubject.Where(o => lstPupilIDtmp.Contains(o.PupilID) && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).ToList();

                    //Thực hiện xóa bản ghi nếu thấy trong bảng SummedUpRecord
                    if (ListSummedUpRecordFromDB != null & ListSummedUpRecordFromDB.Count > 0)
                    {
                        List<int> listValSummed = ListSummedUpRecordFromDB.Select(p => p.PupilID).ToList();
                        MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                    }

                    foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
                    {

                        // Lay diem != - 1 de luu vao db
                        if (SummedUpRecord.JudgementResult != "-1")
                        {
                            SummedUpRecord.CreatedAcademicYear = academicYear.Year;
                        }
                    }

                    listSummedUpInsert = lstSummedUpRecord.Where(p => lstPupilIDtmp.Contains(p.PupilID) && p.JudgementResult != "-1").ToList();
                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                }
                #endregion hoc ki 1
                #region hoc ky 2
                else
                {
                    #region insert ky II

                    //    Bước 1: với mỗi phần tử trong lstPupilExempted
                    /* Tìm kiếm trong bảng SummedUpRecord theo
                          PupilID = lstPupilExempted [i].PupilID
                          AcademicYearID= AcademicYearID
                          ClassID = ClassID
                          SubjectID = SubjectID
                          Semester = SEMESTER_OF_YEAR_ALL
                        Thực hiện xóa bản ghi nếu thấy */

                    List<SummedUpRecord> lstSummedUpAll = lstSummedUpAllSubject.Where(p => p.SubjectID == SubjectID).ToList();
                    // Diem TBM cua HK1
                    List<SummedUpRecord> lstSummedUpHKI = lstSummedUpAll.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();

                    if (lstPupilExempted != null && lstPupilExempted.Count > 0)
                    {
                        List<SummedUpRecord> listSummedUpRecord = ListSummedUpRecordFromDBAllSubject.Where(o => lstPupilExempted.Contains(o.PupilID)
                            && o.SubjectID == SubjectID
                            && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();

                        if (listSummedUpRecord.Count > 0)
                        {
                            // SummedUpRecordBusiness.DeleteAll(listSummedUpRecord);
                            List<int> lstVal = listSummedUpRecord.Select(c => c.PupilID).Distinct().ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, lstVal, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        /*- Kiểm tra học kỳ 1 có Miễn giảm hay không: Gọi hàm
                         * ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                            + Nếu kết quả trả về True => return
                            + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                            + False: thì return
                            + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách gọi hàm
                         * SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST) kết quả trả về nếu:
                            + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                            + JudgeResult != null: Điểm TBM cả năm = Điểm TBM học kỳ 1 (JudgeResult3 = JudgeResult)
                         * Thực hiện insert vào bảng SummedUpRecord
                         */

                        foreach (var pupilid in lstPupilExempted)
                        {
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == pupilid);
                            if (!IsExemptedSubject && subjectLearnInHKI) //Duoc miem giam hoc ky 2, co hoc o HK I
                            {
                                SummedUpRecord surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == pupilid);
                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecord surIII = new SummedUpRecord();
                                    surIII.AcademicYearID = surI.AcademicYearID;
                                    surIII.ClassID = surI.ClassID;
                                    surIII.Comment = surI.Comment;
                                    surIII.IsCommenting = surI.IsCommenting;
                                    //surIII.IsLegalBot = surI.IsLegalBot;
                                    surIII.JudgementResult = surI.JudgementResult;
                                    surIII.PeriodID = surI.PeriodID;
                                    surIII.PupilID = surI.PupilID;
                                    surIII.ReTestJudgement = surI.ReTestJudgement;
                                    surIII.ReTestMark = surI.ReTestMark;
                                    surIII.SchoolID = surI.SchoolID;
                                    surIII.SubjectID = surI.SubjectID;
                                    surIII.SummedUpDate = surI.SummedUpDate;
                                    surIII.SummedUpMark = surI.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }
                    /*
                        Bước 2:
                        // Lưu điểm kỳ 2
                        Tìm kiếm trong bảng SummedUpRecord theo
                        -	PupilID = lstSummedUpRecord[i].PupilID
                        -	AcademicYearID= lstSummedUpRecord[i].AcademicYearID
                        -	ClassID = lstSummedUpRecord[i].ClassID
                        -	SubjectID = lstSummedUpRecord[i].SubjectID
                        -	Semester = lstSummedUpRecord[i].Semester
                        Thực hiện xóa bản ghi nếu thấy
                        Thực hiện insert lstSummedUpRecord vào SummedUpRecord
                     */
                    if (lstSummedUpRecord != null && lstSummedUpRecord.Count > 0)
                    {
                        // Xoa du lieu cu HK2 va ca nam
                        lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && lstPupilIDtmp.Contains(o.PupilID)).ToList();

                        if (lstSummedUpAll != null && lstSummedUpAll.Count > 0)
                        {
                            List<int> lstVal = lstSummedUpAll.Select(c => c.PupilID).Distinct().ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, lstVal, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        foreach (SummedUpRecord summedUpRecord in lstSummedUpRecord)
                        {
                            if (summedUpRecord.JudgementResult != "-1")
                            {
                                summedUpRecord.CreatedAcademicYear = academicYear.Year;
                                //SummedUpRecordBusiness.Insert(summedUpRecord);
                            }
                        }
                        List<SummedUpRecord> listSummedUpInsertTmp = lstSummedUpRecord.Where(p => lstPupilIDtmp.Contains(p.PupilID) && p.JudgementResult != "-1").ToList();
                        for (int k = 0; k < listSummedUpInsertTmp.Count; k++)
                        {
                            listSummedUpInsert.Add(listSummedUpInsertTmp[k]);
                        }

                        // SummedUpRecordBusiness.Save();
                        //var detachContext1 = ((IObjectContextAdapter)context).ObjectContext;
                        //detachContext1.SaveChanges(SaveOptions.DetectChangesBeforeSave);

                        /*
                         // tính cả năm
                        - Kiểm tra học kỳ 1 có Miễn giảm hay không:
                         * Gọi hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                        + Nếu kết quả trả về True => Điểm TBM cả năm = TBM học kỳ 2
                        + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                        + False: thì return Điểm TBM cả năm = TBM học kỳ 2
                        + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách
                         * gọi hàm SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST)
                         * kết quả trả về nếu:
                        + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                        + JudgeResult != null:
                         * Nếu ((sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “Đ”)
                         * or (sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “Đ”)) thì JudgementResult3 = “Đ” – cả năm.
                         Nếu ((sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “CĐ”)
                         * or (sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “CĐ”)) thì JudgementResult3 = “CĐ” – cả năm.
                         */
                        foreach (var summedUpRecord in lstSummedUpRecord)
                        {
                            // lay diem != -1
                            if (summedUpRecord.JudgementResult.Equals("-1"))
                            {
                                continue;
                            }
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == summedUpRecord.PupilID);
                            if (IsExemptedSubject || !subjectLearnInHKI)
                            {
                                SummedUpRecord surIII = new SummedUpRecord();
                                surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                surIII.ClassID = summedUpRecord.ClassID;
                                surIII.Comment = summedUpRecord.Comment;
                                surIII.IsCommenting = summedUpRecord.IsCommenting;
                                //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                surIII.JudgementResult = summedUpRecord.JudgementResult;
                                surIII.PeriodID = summedUpRecord.PeriodID;
                                surIII.PupilID = summedUpRecord.PupilID;
                                surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                surIII.ReTestMark = summedUpRecord.ReTestMark;
                                surIII.SchoolID = summedUpRecord.SchoolID;
                                surIII.SubjectID = summedUpRecord.SubjectID;
                                surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                surIII.CreatedAcademicYear = academicYear.Year;
                                surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                //SummedUpRecordBusiness.Insert(surIII);
                                listSummedUpInsert.Add(surIII);
                            }
                            else
                            {
                                SummedUpRecord surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == summedUpRecord.PupilID);

                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecord surIII = new SummedUpRecord();
                                    surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                    surIII.ClassID = summedUpRecord.ClassID;
                                    surIII.Comment = summedUpRecord.Comment;
                                    surIII.IsCommenting = summedUpRecord.IsCommenting;
                                    //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                    surIII.JudgementResult = summedUpRecord.JudgementResult;
                                    surIII.PeriodID = summedUpRecord.PeriodID;
                                    surIII.PupilID = summedUpRecord.PupilID;
                                    surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                    surIII.ReTestMark = summedUpRecord.ReTestMark;
                                    surIII.SchoolID = summedUpRecord.SchoolID;
                                    surIII.SubjectID = summedUpRecord.SubjectID;
                                    surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                    surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                    #endregion insert ky II
                }
                #endregion hoc ky 2

                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, Semester, Period, classSubject);
            }

        }

        public void InsertJudgeRecordByListClass(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, List<int> listClassID, int SubjectID, int? EmployeeID)
        {

            if (lstJudgeRecord == null || lstJudgeRecord.Count == 0) return;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");
            if (listClassID == null || listClassID.Count == 0)
            {
                return;
            }

            int ClassID = 0;
            var listPupilIDAll = lstJudgeRecord.Select(u => u.PupilID).Distinct().ToList();
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).ToList();
            var listPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                    join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                                    where poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                    && listPupilIDAll.Contains(poc.PupilID) && pp.IsActive
                                    && poc.AcademicYearID == academicYear.AcademicYearID && poc.SchoolID == academicYear.SchoolID
                                    select new { poc.Status, poc.AcademicYearID, poc.ClassID }
                              ).ToList();

            List<ExemptedSubject> listExemptedBySchoolID = ExemptedSubjectBusiness.GetListExemptedSubjectBySchoolID(SchoolID, AcademicYearID, SubjectID).ToList();


            IDictionary<string, object> SearchDelete = new Dictionary<string, object>();
            SearchDelete["AcademicYearID"] = AcademicYearID;
            SearchDelete["SchoolID"] = SchoolID;
            //SearchDelete["ClassID"] = ClassID;
            SearchDelete["SubjectID"] = SubjectID;
            SearchDelete["Semester"] = Semester;
            SearchDelete["checkWithClassMovement"] = "1";
            List<JudgeRecord> iqJudgeDbAllClassID = this.SearchJudgeRecord(SearchDelete).Where(u => listPupilIDAll.Contains(u.PupilID)).ToList();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            //SearchInfo["ClassID"] = ClassID;
            SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["PeriodID"] = Period;
            //SearchInfo["Semester"] = Semester;
            // Khong can join de tim isactive voi HS va truong
            SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            List<SummedUpRecord> ListSummedUpRecordFromDBAllClass = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            //dic["ClassID"] = ClassID;
            dic["SubjectID"] = SubjectID;
            // Khong can join de tim isactive voi HS va truong
            dic["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            // Diem TBM cua 3 HK de xu ly
            List<SummedUpRecord> lstSummedUpAllClass = SummedUpRecordBusiness.SearchBySchool(SchoolID, dic).ToList();

            List<Task> lstTask = new List<Task>();

            for (int k = 0; k < listClassID.Count; k++)
            {
                //string dicLockRecord = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                ClassID = listClassID[k];
                var listPupilID = lstJudgeRecord.Where(p => p.ClassID == ClassID).Select(u => u.PupilID).Distinct().ToList();
                // Kiem tra quyen giao vien bo mon
                if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID, Semester))
                    throw new BusinessException("JudgeRecord_Label_NotAuthen_Add");
                //Academic not compatible
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //PeriodID(PeriodDeclarationID), Semester: not compatible(PeriodDeclaration)
                if (Period != null)
                {
                    bool PeriodDeclarationCompatible = new PeriodDeclarationRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                            new Dictionary<string, object>()
                            {
                                    {"PeriodDeclarationID",Period},
                                    {"Semester",Semester}
                            }, null);
                    if (!PeriodDeclarationCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }

                ClassSubject classSubject = listClassSubject.Where(p => p.ClassID == ClassID).FirstOrDefault();
                ClassProfile classProfile = classSubject.ClassProfile;
                var listPOC = listPupilOfClass.Where(p => p.ClassID == ClassID).ToList();

                // Kiem tra trang thai hoc sinh dang hoc
                if (listPOC.Count != listPupilID.Count)
                {
                    throw new BusinessException("Import_Validate_StudyingStatus");
                }

                // Kiem tra nam hoc va lop hoc hien tai
                if (listPOC.Any(o => o.AcademicYearID != AcademicYearID || o.ClassID != ClassID))
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                #region Lay thong tin mien giam
                List<ExemptedSubject> listExemptedByClass = listExemptedBySchoolID.Where(p => p.ClassID == ClassID).ToList();

                // HKT
                List<ExemptedSubject> listExemptedHKI = listExemptedByClass.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                //HK2
                List<ExemptedSubject> listExemptedHKII = listExemptedByClass.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<ExemptedSubject> listExempted = new List<ExemptedSubject>();
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    listExempted.AddRange(listExemptedHKI);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listExempted.AddRange(listExemptedHKII);
                }
                #endregion

                #region validate 2
                if (classSubject == null)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = ISCOMMENTING_TYPE_JUDGE
                //(nếu kết quả trả về != null => là môn nhận xét)
                if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    throw new BusinessException("SummedUpRecord_Labal_ErrNXSubject");

                bool subjectLearnInHKI = classSubject.SectionPerWeekFirstSemester > 0;
                bool subjectLearnInHKII = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester > 0;

                foreach (var JudgeRecord in lstJudgeRecord)
                {
                    //if (JudgeRecord.Judgement != "-1")
                    //{
                    //    ValidationMetadata.ValidateObject(JudgeRecord);
                    //}
                    //Kiem tra diem thi hop le
                    if (JudgeRecord.Judgement != "-1")
                    {
                        UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, GlobalConstants.ISCOMMENTING_TYPE_JUDGE, JudgeRecord.Judgement);
                    }

                    //Không cho phép nhập điểm (Đối với môn miễn giảm)
                    // if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    //    throw new BusinessException("ExemptedSubject_Label_IsExemptedSubject");
                }
                #endregion

                string strMarkTitle = Period.HasValue ? MarkRecordBusiness.GetMarkTitle(Period.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<string> lstTitle = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();


                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                //string lstLockedMarkDetail = dicLockRecord.Where(p => p.Key == SubjectID).Select(p => p.Value).FirstOrDefault();
                string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID, Period ?? 0);
                List<string> ArrLockedMark = !String.IsNullOrEmpty(lstLockedMarkDetail) ? lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList() : new List<string>();
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                //neu khong phai tai khoan quan tri truong va tat ca con diem bi khoa thi bao loi
                if (!String.IsNullOrEmpty(lstLockedMarkDetail) && lstLockedMarkDetail.Contains("LHK") && !isSchoolAdmin)
                {
                    throw new BusinessException("MarkRecord_Validate_MarkLock");
                }
                // Neu khong phai la tk quan tri truong thi bo qua cac con diem bi khoa
                List<JudgeRecord> lsttmp = new List<JudgeRecord>();
                if (!isSchoolAdmin)
                {
                    lsttmp = lstJudgeRecord.Where(o => !ArrLockedMark.Contains(o.Title) && o.ClassID == ClassID).ToList();
                    for (int i = 0; i < ArrLockedMark.Count; i++)
                    {
                        if (strtmp.Contains(ArrLockedMark[i]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[i], "");
                        }
                    }
                }
                else
                {
                    lsttmp = lstJudgeRecord.Where(o => o.ClassID == ClassID).ToList();
                }

                ///Danh sach diem nhan xet theo mon hoc
                List<JudgeRecord> listJudgeDbByClassID = iqJudgeDbAllClassID.Where(p => p.ClassID == ClassID).ToList();
                // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                if (!isSchoolAdmin)
                {
                    listJudgeDbByClassID = listJudgeDbByClassID.Where(v => !ArrLockedMark.Contains(v.Title)).ToList();
                }
                if (Period.HasValue)
                {
                    listJudgeDbByClassID = listJudgeDbByClassID.Where(u => lstTitle.Contains(u.Title)).ToList();
                }

                List<int> lstPupilExemtedID = listExempted.Select(p => p.PupilID).Distinct().ToList();
                List<int> lstPupilIDtmp = listPupilID.Where(p => !lstPupilExemtedID.Contains(p)).Distinct().ToList();

                //Insert du lieu moi vao DB
                if (lsttmp != null && lsttmp.Count > 0)
                {
                    List<JudgeRecord> lstJudgeInsert = lsttmp.Where(p => lstPupilIDtmp.Contains(p.PupilID)
                                                        && !string.IsNullOrEmpty(p.Judgement) && !"-1".Equals(p.Judgement)).ToList();
                    JudgeRecord judgePupilIdObj = null;
                    int _pupilId = 0;
                    string _title = string.Empty;
                    string _mark = string.Empty;
                    foreach (JudgeRecord itemInsert in lstJudgeInsert)
                    {
                        _pupilId = itemInsert.PupilID;
                        _title = itemInsert.Title;
                        _mark = itemInsert.Judgement;
                        //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                        //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                        judgePupilIdObj = listJudgeDbByClassID.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                        if (judgePupilIdObj != null)
                        {
                            if (judgePupilIdObj.Judgement.Equals(_mark))
                            {
                                itemInsert.ModifiedDate = judgePupilIdObj.ModifiedDate;
                                itemInsert.LogChange = judgePupilIdObj.LogChange;
                                itemInsert.SynchronizeID = judgePupilIdObj.SynchronizeID;
                            }
                            else
                            {
                                itemInsert.ModifiedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                            }
                            itemInsert.CreatedDate = judgePupilIdObj.CreatedDate;
                        }
                        else
                        {
                            itemInsert.CreatedDate = DateTime.Now;
                            itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                        }

                        itemInsert.CreatedAcademicYear = academicYear.Year;
                    }

                    if (lstJudgeInsert != null && lstJudgeInsert.Count > 0)
                    {
                        this.BulkInsert(lstJudgeInsert, JudgeColumnMappings(), "JudgeRecordID");
                    }
                }

                //Xoa tat ca du lieu trong DB theo JudgeRecordID
                List<long> lstJudgeIdDelete = lsttmp.Where(p => p.JudgeRecordID > 0 && lstPupilIDtmp.Contains(p.PupilID)).Select(m => m.JudgeRecordID).Distinct().ToList();
                if (lstJudgeIdDelete.Count > 0)
                {
                    this.DeleteJudgeRecordByListID(AcademicYearID, SchoolID, 0, lstJudgeIdDelete);
                }

                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                List<int> pupilIDs = lstPupilIDtmp;
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = Period;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                //ThreadMarkBusiness.InsertActionList(info);
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion

                if (lstSummedUpRecord.Count == 0)
                    return;
                List<SummedUpRecord> listSummedUpInsert = new List<SummedUpRecord>();
                List<SummedUpRecord> lstSUR = new List<SummedUpRecord>();
                lstSUR = lstSummedUpRecord.Where(p => lstPupilIDtmp.Contains(p.PupilID)).ToList();
                #region hoc ki 1
                //Tìm kiếm bản ghi trong bảng SummedUpRecord
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Period != null)
                {

                    List<SummedUpRecord> ListSummedUpRecordFromDB = ListSummedUpRecordFromDBAllClass
                        .Where(o => lstPupilIDtmp.Contains(o.PupilID))
                        .ToList();

                    //Thực hiện xóa bản ghi nếu thấy trong bảng SummedUpRecord
                    if (ListSummedUpRecordFromDB != null & ListSummedUpRecordFromDB.Count > 0)
                    {
                        // SummedUpRecordBusiness.DeleteAll(ListSummedUpRecordFromDB);
                        List<int> lstVal = ListSummedUpRecordFromDB.Select(c => c.PupilID).Distinct().ToList();
                        MarkRecordBusiness.SP_DeleteSummedUpRecord(0, lstVal, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                    }
                    foreach (SummedUpRecord SummedUpRecord in lstSUR)
                    {

                        // Lay diem != - 1 de luu vao db
                        if (SummedUpRecord.JudgementResult != "-1")
                        {
                            SummedUpRecord.CreatedAcademicYear = academicYear.Year;
                            listSummedUpInsert.Add(SummedUpRecord);
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                }
                #endregion hoc ki 1
                #region hoc ky 2
                else
                {
                    #region insert ky II
                    //    Bước 1: với mỗi phần tử trong lstPupilExempted
                    /* Tìm kiếm trong bảng SummedUpRecord theo
                          PupilID = lstPupilExempted [i].PupilID
                          AcademicYearID= AcademicYearID
                          ClassID = ClassID
                          SubjectID = SubjectID
                          Semester = SEMESTER_OF_YEAR_ALL
                        Thực hiện xóa bản ghi nếu thấy */
                    // Diem TBM cua 3 HK de xu ly
                    List<SummedUpRecord> lstSummedUpAll = lstSummedUpAllClass
                        .Where(o => ((o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && o.ClassID == ClassID)).ToList();

                    // Diem TBM cua HK1
                    List<SummedUpRecord> lstSummedUpHKI = lstSummedUpAll.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                    if (lstPupilExempted != null && lstPupilExempted.Count > 0)
                    {
                        List<SummedUpRecord> listSummedUpRecord = lstSummedUpAllClass.Where(p => p.ClassID == ClassID && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .Where(o => lstPupilExempted.Contains(o.PupilID)).ToList();

                        if (listSummedUpRecord.Count > 0)
                        {
                            // SummedUpRecordBusiness.DeleteAll(listSummedUpRecord);
                            List<long> lstVal = listSummedUpRecord.Select(c => c.SummedUpRecordID).Distinct().ToList();
                            string strValDelete = string.Join(",", lstVal);
                            this.context.SP_DELETE_SUMMED_UP_RECORD(0, strValDelete, 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        /*- Kiểm tra học kỳ 1 có Miễn giảm hay không: Gọi hàm
                         * ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                            + Nếu kết quả trả về True => return
                            + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                            + False: thì return
                            + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách gọi hàm
                         * SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST) kết quả trả về nếu:
                            + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                            + JudgeResult != null: Điểm TBM cả năm = Điểm TBM học kỳ 1 (JudgeResult3 = JudgeResult)
                         * Thực hiện insert vào bảng SummedUpRecord
                         */

                        foreach (var pupilid in lstPupilExempted)
                        {
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == pupilid);
                            if (!IsExemptedSubject && subjectLearnInHKI) //Duoc miem giam hoc ky 2, co hoc o HK I
                            {
                                SummedUpRecord surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == pupilid);
                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecord surIII = new SummedUpRecord();
                                    surIII.AcademicYearID = surI.AcademicYearID;
                                    surIII.ClassID = surI.ClassID;
                                    surIII.Comment = surI.Comment;
                                    surIII.IsCommenting = surI.IsCommenting;
                                    //surIII.IsLegalBot = surI.IsLegalBot;
                                    surIII.JudgementResult = surI.JudgementResult;
                                    surIII.PeriodID = surI.PeriodID;
                                    surIII.PupilID = surI.PupilID;
                                    surIII.ReTestJudgement = surI.ReTestJudgement;
                                    surIII.ReTestMark = surI.ReTestMark;
                                    surIII.SchoolID = surI.SchoolID;
                                    surIII.SubjectID = surI.SubjectID;
                                    surIII.SummedUpDate = surI.SummedUpDate;
                                    surIII.SummedUpMark = surI.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }
                    /*
                        Bước 2:
                        // Lưu điểm kỳ 2
                        Tìm kiếm trong bảng SummedUpRecord theo
                        -	PupilID = lstSummedUpRecord[i].PupilID
                        -	AcademicYearID= lstSummedUpRecord[i].AcademicYearID
                        -	ClassID = lstSummedUpRecord[i].ClassID
                        -	SubjectID = lstSummedUpRecord[i].SubjectID
                        -	Semester = lstSummedUpRecord[i].Semester
                        Thực hiện xóa bản ghi nếu thấy
                        Thực hiện insert lstSummedUpRecord vào SummedUpRecord
                     */
                    if (lstSummedUpRecord != null && lstSummedUpRecord.Count > 0)
                    {
                        // Xoa du lieu cu HK2 va ca nam
                        lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && lstPupilIDtmp.Contains(o.PupilID)).ToList();
                        if (lstSummedUpAll.Count > 0)
                        {
                            List<int> lstVal = lstSummedUpAll.Select(c => c.PupilID).Distinct().ToList();
                            string strValDelete = string.Join(",", lstVal);
                            this.context.SP_DELETE_SUMMED_UP_RECORD(0, strValDelete, 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        foreach (SummedUpRecord summedUpRecord in lstSummedUpRecord)
                        {
                            if (summedUpRecord.JudgementResult != "-1")
                            {
                                summedUpRecord.CreatedAcademicYear = academicYear.Year;
                            }
                        }

                        List<SummedUpRecord> listSummedUpInsertTmp = lstSUR.Where(p => p.JudgementResult != "-1" && !p.JudgementResult.Equals(string.Empty)).ToList();
                        if (listSummedUpInsertTmp != null && listSummedUpInsertTmp.Count > 0)
                        {
                            for (int i = 0; i < listSummedUpInsertTmp.Count; i++)
                            {
                                listSummedUpInsert.Add(listSummedUpInsertTmp[i]);
                            }
                        }

                        //SummedUpRecordBusiness.Save();

                        /*
                         // tính cả năm
                        - Kiểm tra học kỳ 1 có Miễn giảm hay không:
                         * Gọi hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                        + Nếu kết quả trả về True => Điểm TBM cả năm = TBM học kỳ 2
                        + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                        + False: thì return Điểm TBM cả năm = TBM học kỳ 2
                        + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách
                         * gọi hàm SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST)
                         * kết quả trả về nếu:
                        + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                        + JudgeResult != null:
                         * Nếu ((sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “Đ”)
                         * or (sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “Đ”)) thì JudgementResult3 = “Đ” – cả năm.
                         Nếu ((sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “CĐ”)
                         * or (sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “CĐ”)) thì JudgementResult3 = “CĐ” – cả năm.
                         */

                        foreach (var summedUpRecord in lstSummedUpRecord)
                        {
                            // lay diem != -1
                            if (summedUpRecord.JudgementResult.Equals("-1"))
                            {
                                continue;
                            }
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == summedUpRecord.PupilID);
                            if (IsExemptedSubject || !subjectLearnInHKI)
                            {
                                SummedUpRecord surIII = new SummedUpRecord();
                                surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                surIII.ClassID = summedUpRecord.ClassID;
                                surIII.Comment = summedUpRecord.Comment;
                                surIII.IsCommenting = summedUpRecord.IsCommenting;
                                //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                surIII.JudgementResult = summedUpRecord.JudgementResult;
                                surIII.PeriodID = summedUpRecord.PeriodID;
                                surIII.PupilID = summedUpRecord.PupilID;
                                surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                surIII.ReTestMark = summedUpRecord.ReTestMark;
                                surIII.SchoolID = summedUpRecord.SchoolID;
                                surIII.SubjectID = summedUpRecord.SubjectID;
                                surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                surIII.CreatedAcademicYear = academicYear.Year;
                                surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                //SummedUpRecordBusiness.Insert(surIII);
                                listSummedUpInsert.Add(surIII);
                            }
                            else
                            {
                                SummedUpRecord surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == summedUpRecord.PupilID);

                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecord surIII = new SummedUpRecord();
                                    surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                    surIII.ClassID = summedUpRecord.ClassID;
                                    surIII.Comment = summedUpRecord.Comment;
                                    surIII.IsCommenting = summedUpRecord.IsCommenting;
                                    //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                    surIII.JudgementResult = summedUpRecord.JudgementResult;
                                    surIII.PeriodID = summedUpRecord.PeriodID;
                                    surIII.PupilID = summedUpRecord.PupilID;
                                    surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                    surIII.ReTestMark = summedUpRecord.ReTestMark;
                                    surIII.SchoolID = summedUpRecord.SchoolID;
                                    surIII.SubjectID = summedUpRecord.SubjectID;
                                    surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                    surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }

                    #endregion insert ky II
                }
                #endregion hoc ky 2


                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, Semester, Period, classSubject);
            }

        }

        #endregion Insert

        #region Insert Judge Record Primary

        /// <summary>
        /// Thêm mới thông tin chi tiết hồ sơ kết quả môn học đánh giá cấp 1 theo giai đoạn / học kỳ / năm học
        /// </summary>
        /// <author>trangdd</author>
        /// <date>02/10/2012</date>

        public void InsertJudgeRecordPrimary(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord)
        {
            if (lstJudgeRecord == null || lstSummedUpRecord == null) return;
            if (lstJudgeRecord.Count == 0 && lstSummedUpRecord.Count == 0) return;
            // Lay list pupilID:
            List<int> lstPupilID = lstSummedUpRecord.Select(u => u.PupilID).Distinct().ToList();

            JudgeRecord firstJudge = lstJudgeRecord.FirstOrDefault();
            SummedUpRecord firstSum = lstSummedUpRecord.FirstOrDefault();
            int schoolID = firstJudge != null ? firstJudge.SchoolID : firstSum.SchoolID;
            int academicYearID = firstJudge != null ? firstJudge.AcademicYearID : firstSum.AcademicYearID;
            int classID = firstJudge != null ? firstJudge.ClassID : firstSum.ClassID;
            int subjectID = firstJudge != null ? firstJudge.SubjectID : firstSum.SubjectID;
            int semester = firstJudge != null ? firstJudge.Semester.Value : firstSum.Semester.Value;
            // Validate trang thai, mien giam hoc sinh
            #region validate
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID}
            }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, classID, subjectID))
                throw new BusinessException("");

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object> { { "SubjectID", subjectID }, { "SchoolID", schoolID } }).FirstOrDefault();
            if (classSubject == null)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (classSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                throw new BusinessException("Common_Validate_IsCommenting ");

            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semester).Where(u => u.SubjectID == subjectID).ToList();
            var listPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ClassID", classID }, { "Status", SystemParamsInFile.PUPIL_STATUS_STUDYING } })
                        .Where(o => lstPupilID.Contains(o.PupilID))
                           join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                           select new { poc.Status, pp.CurrentAcademicYearID, pp.CurrentClassID }
                                ).ToList();
            // Kiem tra trang thai hoc sinh dang hoc
            if (listPOC.Count != lstPupilID.Count)
            {
                throw new BusinessException("PupilProfile_Label_Not_Pupil_Status_Studying");
            }
            // Kiem tra nam hoc va lop hoc hien tai
            if (listPOC.Any(o => o.CurrentAcademicYearID != academicYearID || o.CurrentClassID != classID))
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Thong tin khoa diem
            string LockTitleS = MarkRecordBusiness.GetLockMarkTitlePrimary(schoolID, academicYearID, classID, subjectID, semester);
            // Kiem tra neu nhu con diem hoc ky bi khoa thi coi nhu khong xu ly gi ca
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                if (LockTitleS.Contains(SystemParamsInFile.HKI + ","))
                {
                    return;
                }
            }
            else
            {
                if (LockTitleS.Contains(SystemParamsInFile.HKII + ","))
                {
                    return;
                }
            }

            string[] LockTitle = LockTitleS.Split(',');
            int classGrade = classSubject.ClassProfile.EducationLevelID;
            Dictionary<int, int> dic12 = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 }, { 3, 3 }, { 4, 4 }, { 5, 1 }, { 6, 2 }, { 7, 3 }, { 8, 4 } };
            Dictionary<int, int> dic345 = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 }, { 3, 3 }, { 4, 4 }, { 5, 5 }, { 6, 1 }, { 7, 2 }, { 8, 3 }, { 9, 4 }, { 10, 5 } };
            Dictionary<int, int> dic = new Dictionary<int, int>();
            // Doi voi hoc ky 2 thi he so con diem bi khoa se tang theo bien nay
            int startLockIdx = 0;
            if (classGrade == 1 || classGrade == 2)
            {
                dic = dic12;
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    startLockIdx = 4;
                }
            }
            else
            {
                dic = dic345;
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    startLockIdx = 5;
                }
            }
            //Lấy từng phần tử trong lstJudgeRecord
            foreach (JudgeRecord JudgeRecord in lstJudgeRecord)
            {
                ValidationMetadata.ValidateObject(JudgeRecord);

                if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    throw new BusinessException("Common_Validate_IsExemptedSubject");
                //check record has lock?

            }
            #endregion
            //Tìm kiếm bản ghi
            IDictionary<string, object> SearchJudge = new Dictionary<string, object>();
            SearchJudge["SchoolID"] = schoolID;
            SearchJudge["AcademicYearID"] = academicYearID;
            SearchJudge["ClassID"] = classID;
            SearchJudge["SubjectID"] = subjectID;
            SearchJudge["Semester"] = semester;
            List<JudgeRecord> ListJudgeRecord = this.SearchJudgeRecord(SearchJudge).Where(u => lstPupilID.Contains(u.PupilID)).ToList();
            // Chi xoa nhung con diem khong bi khoa
            #region Kiem tra de khong xoa cac con diem bi khoa
            if (LockTitle.Count() > 0 && LockTitle[0].Length > 0)
            {
                int count = ListJudgeRecord.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    JudgeRecord JudgeRecord = ListJudgeRecord[i];
                    bool isDel = true;
                    if (JudgeRecord.Title.Equals("C1") || JudgeRecord.Title.Equals("HK"))
                    {
                        foreach (string lTitle in LockTitle)
                        {
                            if (string.IsNullOrWhiteSpace(lTitle))
                            {
                                continue;
                            }
                            if (lTitle.Contains("NX"))
                            {
                                int idxLock = 0;
                                int.TryParse(lTitle.Replace("_", "").Replace("NX", ""), out idxLock);
                                // Bo qua cac con diem bi khoa
                                // Doi voi ky 2 thi con diem khoa phai lon hon chi so bat dau
                                if (idxLock > startLockIdx && dic[idxLock] == JudgeRecord.OrderNumber)
                                {
                                    isDel = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (!isDel)
                    {
                        ListJudgeRecord.RemoveAt(i);
                    }
                }
            }
            #endregion

            if (ListJudgeRecord.Count > 0)
            {
                this.DeleteAll(ListJudgeRecord);
                // Luu diem cu
                this.SetOldValue(ListJudgeRecord, lstJudgeRecord);
            }

            foreach (JudgeRecord JudgeRecord in lstJudgeRecord)
            {
                bool isInsert = true;
                if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && (JudgeRecord.Title.Equals("C1") || JudgeRecord.Title.Equals("HK")))
                {
                    foreach (string lTitle in LockTitle)
                    {
                        if (string.IsNullOrWhiteSpace(lTitle))
                        {
                            continue;
                        }
                        if (lTitle.Contains("NX"))
                        {
                            int idxLock = 0;
                            int.TryParse(lTitle.Replace("_", "").Replace("NX", ""), out idxLock);
                            // Bo qua cac con diem bi khoa
                            if (idxLock > startLockIdx && dic[idxLock] == JudgeRecord.OrderNumber)
                            {
                                isInsert = false;
                                break;
                            }
                        }
                    }
                }
                if (isInsert)
                {
                    this.Insert(JudgeRecord);
                }
            }

            if (lstSummedUpRecord.Count == 0)
                return;

            //Tìm kiếm bản ghi trong bảng SummedUpRecord
            IDictionary<string, object> SearchSur = new Dictionary<string, object>();
            SearchSur["AcademicYearID"] = academicYearID;
            SearchSur["ClassID"] = classID;
            SearchSur["SubjectID"] = subjectID;
            if (classSubject.SectionPerWeekSecondSemester > 0)
                SearchSur["Semester"] = semester;
            //else
            //SearchSur["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            List<SummedUpRecord> ListSummedUp = this.SummedUpRecordBusiness.SearchBySchool(schoolID, SearchSur).Where(u => lstPupilID.Contains(u.PupilID)).ToList();
            if (ListSummedUp.Count > 0)
                SummedUpRecordBusiness.DeleteAll(ListSummedUp);

            foreach (SummedUpRecord sur in lstSummedUpRecord)
            {
                if (sur.JudgementResult == null)
                {
                    continue;
                }
                if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && LockTitle.Contains("HK" + semester))
                {
                    throw new BusinessException("Common_Validate_LockMarkTitleError");
                }
                this.SummedUpRecordBusiness.Insert(sur);
                //truong hop chi hoc o ky 1 NAMTA
                if (classSubject.SectionPerWeekSecondSemester == 0 && semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    SummedUpRecord sAll = new SummedUpRecord()
                    {
                        AcademicYearID = sur.AcademicYearID,
                        ClassID = sur.ClassID,
                        Comment = sur.Comment,
                        IsCommenting = sur.IsCommenting,
                        IsOldData = sur.IsOldData,
                        JudgementResult = sur.JudgementResult,
                        PeriodID = sur.PeriodID,
                        PupilID = sur.PupilID,
                        ReTestJudgement = sur.ReTestJudgement,
                        ReTestMark = sur.ReTestMark,
                        SchoolID = sur.SchoolID,
                        Semester = sur.Semester,
                        SubjectID = sur.SubjectID,
                        SummedUpDate = sur.SummedUpDate,
                        SummedUpMark = sur.SummedUpMark,
                        CreatedAcademicYear = sur.CreatedAcademicYear
                    };

                    sAll.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    this.SummedUpRecordBusiness.Insert(sAll);
                }
            }
        }

        #endregion Insert Judge Record Primary

        #region Delete

        /// <summary>
        ///Xoá thông tin chi tiết hồ sơ kết quả môn học đánh giá theo giai đoạn / học kỳ / năm học
        /// <author>trangdd</author>
        /// <date>02/10/2012</date>
        /// </summary>
        /// <param name="MarkRecordID">ID điểm môn học</param>
        public void Delete(int UserID, int JudgeRecordID, int? SchoolID)
        {
            //Kiem tra JudgeRecordID co ton tai
            new JudgeRecordBusiness(null).CheckAvailable((int)JudgeRecordID, "JudgeRecord_Label_JudgeRecordID", false);

            //JudgeRecordID, SchoolID: not compatible(JudgeRecord)
            bool JudgeRecordCompatible = new JudgeRecordRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "JudgeRecord",
                    new Dictionary<string, object>()
                {
                    {"JudgeRecordID",JudgeRecordID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!JudgeRecordCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //Lấy dữ liệu từ DB có JudgeRecordID = JudgeRecordID
            JudgeRecord JudgeRecord = JudgeRecordRepository.Find(JudgeRecordID);

            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
            PupilOfClass PupilOfClass = PupilOfClassBusiness.All.Where(o => o.PupilID == JudgeRecord.PupilID && o.ClassID == JudgeRecord.ClassID
                && o.AcademicYearID == JudgeRecord.AcademicYearID).OrderByDescending(o => o.AssignedDate).FirstOrDefault();

            if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(JudgeRecord.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }

            if (UtilsBusiness.HasSubjectTeacherPermission(UserID, JudgeRecord.ClassID, JudgeRecord.SubjectID))
            {
                base.Delete(JudgeRecordID);
            }
            #region Save ThreadMark
            List<int> pupilIDs = new List<int> { { JudgeRecord.PupilID } };
            // AnhVD 20131225 - Insert into ThreadMark for Auto Process
            ThreadMarkBO info = new ThreadMarkBO();
            info.SchoolID = JudgeRecord.SchoolID;
            info.ClassID = JudgeRecord.ClassID;
            info.AcademicYearID = JudgeRecord.AcademicYearID;
            info.SubjectID = JudgeRecord.SubjectID;
            info.Semester = JudgeRecord.Semester.Value;
            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
            info.PupilIds = pupilIDs;
            ThreadMarkBusiness.InsertActionList(info);
            ThreadMarkBusiness.Save();
            // End
            #endregion
        }

        public void DeleteJudgeRecord(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
            SearchPOC["ClassID"] = ClassID;
            SearchPOC["Check"] = "Check";
            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

            if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");

            string MarkLockRecord = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
            List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
            bool isAdminSchool = UserAccountBusiness.IsSchoolAdmin(UserID);

            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["Semester"] = Semester;
            SearchInfo["checkWithClassMovement"] = "1";
            SearchInfo["Year"] = academicYear.Year; // AnhVD 20131218
            string strMarkTitle = PeriodID.HasValue ? MarkRecordBusiness.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            string strtmp = strMarkTitle;
            string[] arrMarkType = strMarkTitle.Split(',');
            IQueryable<JudgeRecord> listJudgeForDelete = this.SearchJudgeRecord(SearchInfo).Where(u => listPupilID.Contains(u.PupilID)).Where(u => arrMarkType.Contains(u.Title));

            if (PeriodID.HasValue)
                listJudgeForDelete = listJudgeForDelete.Where(u => strMarkTitle.Contains(u.Title));
            var ListJudgeDelete = listJudgeForDelete.ToList();

            if (!isAdminSchool)
            {
                ListJudgeDelete = ListJudgeDelete.Where(u => !markLocks.Contains(u.Title)).ToList();
                for (int i = 0; i < markLocks.Count; i++)
                {
                    if (strtmp.Contains(markLocks[i]))
                    {
                        strtmp = strtmp.Replace(markLocks[i], "");
                    }
                }
            }

            if (ListJudgeDelete.Count > 0)
            {
                // this.DeleteAll(ListJudgeDelete);
                List<int> listPupilIdDelete = ListJudgeDelete.Select(p => p.PupilID).Distinct().ToList();
                MarkRecordBusiness.SP_DeleteJudgeRecord(AcademicYearID, SchoolID, ClassID, Semester, SubjectID, 0, 0, listPupilIdDelete, strtmp);
            }

            #region Save ThreadMark
            // AnhVD 20131225 - Insert into ThreadMark for Auto Process
            List<int> pupilIDs = ListJudgeDelete.Select(o => o.PupilID).ToList();
            ThreadMarkBO info = new ThreadMarkBO();
            info.SchoolID = SchoolID;
            info.ClassID = ClassID;
            info.AcademicYearID = AcademicYearID;
            info.SubjectID = SubjectID;
            info.Semester = Semester;
            info.PeriodID = PeriodID;
            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
            info.PupilIds = pupilIDs;
            //ThreadMarkBusiness.InsertActionList(info);
            //ThreadMarkBusiness.Save();
            ThreadMarkBusiness.BulkInsertActionList(info);

            // End
            #endregion
        }
        #endregion Delete

        #region Update IsSMS is TRUE
        /// <summary>
        /// Anhvd
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilIds"></param>
        /// <returns></returns>
        public bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds)
        {
            IQueryable<JudgeRecord> lstJudge = this.SearchJudgeRecord(dic);
            for (int i = 0; i < pupilIds.Count; i++)
            {
                int pupilId = pupilIds[i];
                IQueryable<JudgeRecord> lstJudgePupil = lstJudge.Where(o => o.PupilID == pupilId);
                foreach (var item in lstJudgePupil)
                {
                    //item.IsSMS = true;
                    this.Update(item);
                }
            }
            this.Save();
            return true;
        }
        #endregion

        #region Get Judge Record Of Class

        /// <summary>
        ///Lấy bảng điểm của lớp theo mon
        /// <author>trangdd</author>
        /// <date>10/10/2012</date>
        /// rewritten by namdv
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<JudgeRecordBO> GetJudgeRecordOfClass(int AcademicYearID, int SchoolID, int SubjectID, int Semester, int ClassID, int? PeriodID)
        {
            IQueryable<JudgeRecordBO> query;
            int? createdAcaYear = AcademicYearBusiness.Find(AcademicYearID).Year;
            IQueryable<PupilOfClass> listPupilOfClassAll = PupilOfClassBusiness.All.Where(o => o.ClassID == ClassID && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID);
            IQueryable<PupilOfClass> listPupilOfClass = listPupilOfClassAll.Where(o => o.AssignedDate == listPupilOfClassAll.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            if (PeriodID.HasValue && PeriodID.Value != 0)
            {
                string strMarkTitle = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                string markTitle = strMarkTitle + ",";
                query = from poc in listPupilOfClass
                        join pp in PupilProfileRepository.All
                        on poc.PupilID equals pp.PupilProfileID
                        join jr in VJudgeRecordBusiness.All.Where(o => o.ClassID == ClassID && o.SubjectID == SubjectID && o.Semester == Semester && o.Last2digitNumberSchool == SchoolID % 100
                             && o.CreatedAcademicYear == createdAcaYear && markTitle.Contains(o.Title))
                        on poc.PupilID equals jr.PupilID
                        into g1
                        from g2 in g1.DefaultIfEmpty()
                        where pp.IsActive == true
                        select new JudgeRecordBO
                        {
                            PupilCode = pp.PupilCode,
                            FullName = pp.FullName,
                            Name = pp.Name,
                            JudgeRecordID = g2.JudgeRecordID,
                            PupilID = poc.PupilID,
                            ClassID = poc.ClassID,
                            SchoolID = poc.SchoolID,
                            AcademicYearID = poc.AcademicYearID,
                            SubjectID = g2.SubjectID,
                            MarkTypeID = g2.MarkTypeID,
                            Semester = g2.Semester,
                            Judgement = g2.Judgement,
                            ReTestJudgement = g2.ReTestJudgement,
                            OrderNumber = g2.OrderNumber,
                            Status = poc.Status,
                            Title = g2.Title,
                            MarkTitle = strMarkTitle,
                            EndDate = poc.EndDate,
                            OrderInClass = poc.OrderInClass
                        };
            }
            else
            {
                query = from poc in listPupilOfClass
                        join pp in PupilProfileRepository.All
                        on poc.PupilID equals pp.PupilProfileID
                        join jr in VJudgeRecordBusiness.All.Where(o => o.ClassID == ClassID && o.SubjectID == SubjectID && (o.Semester == Semester || Semester == 0) && o.Last2digitNumberSchool == SchoolID % 100 && o.CreatedAcademicYear == createdAcaYear)
                        on poc.PupilID equals jr.PupilID
                        into g1
                        from g2 in g1.DefaultIfEmpty()
                        where pp.IsActive == true
                        select new JudgeRecordBO
                        {
                            PupilCode = pp.PupilCode,
                            FullName = pp.FullName,
                            Name = pp.Name,
                            JudgeRecordID = g2.JudgeRecordID,
                            PupilID = poc.PupilID,
                            ClassID = poc.ClassID,
                            SchoolID = poc.SchoolID,
                            AcademicYearID = poc.AcademicYearID,
                            SubjectID = g2.SubjectID,
                            MarkTypeID = g2.MarkTypeID,
                            Semester = g2.Semester,
                            Judgement = g2.Judgement,
                            ReTestJudgement = g2.ReTestJudgement,
                            OrderNumber = g2.OrderNumber,
                            Status = poc.Status,
                            Title = g2.Title,
                            OrderInClass = poc.OrderInClass,
                            EndDate = poc.EndDate
                        };
            }
            return query;
        }

        /// <summary>
        ///Lấy bảng điểm của lớp cua tat ca cac mon nhan xet
        /// <author>hungnd8</author>
        /// <date>10/10/2012</date>
        /// rewritten by namdv
        /// </summary>
        /// <param name="dic">neu truyen semester thi lay diem theo semester,khong truyen se lay len het</param>
        /// <returns></returns>
        public IQueryable<JudgeRecordWSBO> GetAllJudgeRecordOfClass(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SemesterID = Utils.GetInt(dic, "Semester");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");

            int createdAcaYear = -1;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (aca != null)
            {
                createdAcaYear = aca.Year;
            }
            IQueryable<PupilOfClass> listPupilOfClassAll = PupilOfClassBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID);
            if (ClassID > 0)
            {
                listPupilOfClassAll = listPupilOfClassAll.Where(p => p.ClassID == ClassID);
                lstClassID.Add(ClassID);
            }
            if (lstClassID.Count > 0)
            {
                listPupilOfClassAll = listPupilOfClassAll.Where(p => lstClassID.Contains(p.ClassID));
            }
            IQueryable<PupilOfClass> listPupilOfClass = listPupilOfClassAll.Where(o => o.AssignedDate == listPupilOfClassAll.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            IQueryable<JudgeRecordWSBO> query = from poc in listPupilOfClass
                                                join pp in PupilProfileRepository.All
                                                on poc.PupilID equals pp.PupilProfileID
                                                join jr in VJudgeRecordBusiness.All.Where(o => lstClassID.Contains(o.ClassID)
                                                     && o.Last2digitNumberSchool == SchoolID % 100
                                                     && o.CreatedAcademicYear == createdAcaYear
                                                     && (o.Semester == SemesterID || SemesterID == 0))
                                                on poc.PupilID equals jr.PupilID
                                                join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                join s in SubjectCatBusiness.All on jr.SubjectID equals s.SubjectCatID
                                                join t in MarkTypeBusiness.All on jr.MarkTypeID equals t.MarkTypeID
                                                where pp.IsActive == true
                                                && cp.IsActive.Value
                                                select new JudgeRecordWSBO
                                                {
                                                    Judgement = jr.Judgement,
                                                    PupilID = poc.PupilID,
                                                    SubjectID = jr.SubjectID,
                                                    SubjectName = s.DisplayName,
                                                    SubjectOrderNumber = s.OrderInSubject,
                                                    MarkTypeID = jr.MarkTypeID,
                                                    MarkOrderNumber = jr.OrderNumber,
                                                    Title = t.Title,
                                                    EducationLevelID = cp.EducationLevelID,
                                                    MarkedDate = (jr.ModifiedDate == null ? jr.CreatedDate : jr.ModifiedDate), // Neu da sua lai diem lay thoi gian nhap la modifydate
                                                };

            return query;
        }
        #endregion Get Judge Record Of Class

        #region Ham get diem moi
        /// <summary>
        /// Namta
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IEnumerable<JudgeRecordNewBO> GetJudgeRecordOfClassNew(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Semester = Utils.GetInt(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PeriodID = Utils.GetInt(dic, "PeriodID");
            string MarkTitle = "";
            if (PeriodID != 0)
            {
                MarkTitle = MarkRecordBusiness.GetMarkTitle(PeriodID) + ",";
            }
            else
            {
                MarkTitle = MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            }


            var listJudgeRecordObject = this.context.PUPIL_GET_JUDGE_RECORD_OF_CLASS(SchoolID, AcademicYearID, ClassID, Semester, PeriodID, SubjectID, MarkTitle);
            if (listJudgeRecordObject != null && listJudgeRecordObject.Count() > 0)
            {
                return listJudgeRecordObject.ToList();
            }


            return null;
        }
        #endregion

        #region ImportJudgeRecord (Import điểm học sinh)

        /// <summary>
        ///Import điểm học sinh
        /// <author>trangdd</author>
        /// <edit>hieund9 - 23/01/2013</edit>
        /// <fixnew>Namta</fixnew>
        /// <date>02/10/2012</date>
        /// </summary>
        public void ImportJudgeRecord(int UserID, List<JudgeRecord> lstJudgeRecord)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstJudgeRecord.Count == 0)
                    return;

                JudgeRecord firstJudgeRecord = lstJudgeRecord.First();
                int schoolID = firstJudgeRecord.SchoolID;
                int academicYearID = firstJudgeRecord.AcademicYearID;
                int semester = firstJudgeRecord.Semester.Value;
                ClassProfile firstClass = ClassProfileBusiness.Find(firstJudgeRecord.ClassID);
                int educationLevelID = firstClass.EducationLevelID;
                int grade = firstClass.EducationLevel.Grade;
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                #region validate
                //Năm học không phải là năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                    throw new BusinessException("Common_Validate_IsCurrentYear");

                bool timeYear = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                if (!UserAccountBusiness.IsSchoolAdmin(UserID) && !timeYear)
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }

                bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                            new Dictionary<string, object>()
                {
                    {"SchoolID", schoolID},
                    {"AcademicYearID", academicYearID}
                }, null);
                if (!SchoolCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra neu ton tai hoc sinh co trang thai khac dang hoc thi bao loi
                List<int> listPupilID = lstJudgeRecord.Select(o => o.PupilID).Distinct().ToList();
                int countPupil = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "AcademicYearID", academicYearID } })
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where pp.IsActive && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                  && listPupilID.Contains(poc.PupilID)
                                  select poc).Count();

                if (listPupilID.Count != countPupil)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
                // Kiem tra neu co mon hoc va lop hoc khong duoc gan tuong ung trong ClassSubject thi bao loi
                var listClassSubjectID = lstJudgeRecord.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
                List<int> listClassID = listClassSubjectID.Select(o => o.ClassID).Distinct().ToList();
                List<ClassSubject> listClassSubject = ClassSubjectBusiness.All.Where(o => listClassID.Contains(o.ClassID) && o.Last2digitNumberSchool == partitionId).ToList();
                foreach (var classSubjectID in listClassSubjectID)
                {
                    ClassSubject cs = listClassSubject.FirstOrDefault(o => o.ClassID == classSubjectID.ClassID && o.SubjectID == classSubjectID.SubjectID);
                    if (cs == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (cs != null && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                // Kiem tra neu co hoc sinh thuoc dang mien giam nhung van co du lieu thi bao loi      
                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.All.Where(o => o.SchoolID == schoolID
                                        && o.AcademicYearID == academicYearID && ((semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                        && o.FirstSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL)
                                        ||
                                        (semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        && o.SecondSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL))).ToList();

                foreach (JudgeRecord judgeRecord in lstJudgeRecord)
                {
                    // Kiem tra mien giam
                    if (lstExempted.Any(o => o.PupilID == judgeRecord.PupilID && o.SubjectID == judgeRecord.SubjectID && o.ClassID == judgeRecord.ClassID))
                    {
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                    }
                    ValidationMetadata.ValidateObject(judgeRecord);
                    if (!string.IsNullOrWhiteSpace(judgeRecord.Judgement))
                    {
                        UtilsBusiness.CheckValidateMark(grade, SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE, judgeRecord.Judgement);
                    }
                }
                #endregion

                #region them moi hoac cap nhat
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["EducationLevelID"] = educationLevelID;
                SearchInfo["Semester"] = semester;
                // Neu chi import cho 1 lop thi chi can lay diem cua 1 lop la du
                if (!lstJudgeRecord.Any(o => o.ClassID != firstJudgeRecord.ClassID))
                {
                    SearchInfo["ClassID"] = firstJudgeRecord.ClassID;
                }
                var listJudgeAll = this.SearchJudgeRecord(SearchInfo).ToList();
                // Luu lai thong tin de cap nhat vao bang tien trinh tinh TBM tu dong
                Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();
                // Danh sach cac list diem insert de bo sung vao danh sach lay ra dem di tong ket ma khong can
                // phai truy van lai nua
                List<JudgeRecord> listJudgeRecordInsert = new List<JudgeRecord>();

                foreach (JudgeRecord JudgeRecord in lstJudgeRecord)
                {
                    var listJudge = listJudgeAll.Where(o => o.PupilID == JudgeRecord.PupilID &&
                                                            o.ClassID == JudgeRecord.ClassID &&
                                                            o.SubjectID == JudgeRecord.SubjectID &&
                                                            o.Title == JudgeRecord.Title);
                    bool isUpdate = false;
                    JudgeRecord judgeRecordUpdate = listJudge.FirstOrDefault();
                    if (judgeRecordUpdate != null)
                    {
                        if (judgeRecordUpdate.Judgement != JudgeRecord.Judgement)
                        {
                            judgeRecordUpdate.Judgement = JudgeRecord.Judgement;
                            this.Update(judgeRecordUpdate);
                            isUpdate = true;
                        }
                    }
                    else
                    {
                        listJudgeRecordInsert.Add(JudgeRecord);
                    }
                    if (isUpdate)
                    {
                        string key = JudgeRecord.ClassID + "_" + JudgeRecord.SubjectID;
                        if (dicPupilThreadMark.ContainsKey(key))
                        {
                            listPupilID = dicPupilThreadMark[key];
                            if (!listPupilID.Contains(JudgeRecord.PupilID))
                            {
                                listPupilID.Add(JudgeRecord.PupilID);
                                dicPupilThreadMark[key] = listPupilID;
                            }
                        }
                        else
                        {
                            dicPupilThreadMark[key] = new List<int>() { JudgeRecord.PupilID };
                        }
                    }
                }
                // Insert diem
                if (listJudgeRecordInsert.Count > 0)
                {
                    this.BulkInsert(listJudgeRecordInsert, JudgeColumnMappings(), "JudgeRecordID");
                }
                #endregion them moi hoac cap nhat

                this.Save();
                #region tinh diem kiem tra hoc ky va ca nam
                if (lstJudgeRecord.Count > 0)
                {

                    if (listClassID.Count > 1)
                    {
                        //this.context.PROC_LEVEL_JUDGE_SUMMED_UP(schoolID, academicYearID, educationLevelID, semester, null);
                        int classId = 0;
                        int subjectId = 0;
                        List<int> lstSubject;
                        for (int i = 0; i < listClassID.Count; i++)
                        {
                            classId = listClassID[i];
                            List<int> lstPupilId = lstJudgeRecord.Where(s => s.ClassID == classId).Select(s => s.PupilID).Distinct().ToList();
                            lstSubject = lstJudgeRecord.Where(c => c.ClassID == classId).Select(c => c.SubjectID).Distinct().ToList();
                            for (int j = 0; j < lstSubject.Count; j++)
                            {
                                subjectId = lstSubject[j];
                                ClassSubject classSubject = listClassSubject.FirstOrDefault(s => s.ClassID == classId && s.SubjectID == subjectId);
                                //this.context.PROC_CLASS_JUDGE_SUMMED_UP(schoolID, academicYearID, classId, semester, subjectId);
                                //Tinh TBM cho hoc ky
                                this.SummedJudgeMark(listPupilID, academicYear, semester, 1, classSubject);

                                //Tinh TBM cho dot
                                this.SummedJudgeMark(listPupilID, academicYear, semester, null, classSubject);
                            }
                        }
                    }
                    else
                    {
                        foreach (var classSubjectID in listClassSubjectID)
                        {
                            //this.context.PROC_CLASS_JUDGE_SUMMED_UP(schoolID, academicYearID, classSubjectID.ClassID, semester, classSubjectID.SubjectID);
                            List<int> lstPupilId = lstJudgeRecord.Where(s => s.ClassID == classSubjectID.ClassID).Select(s => s.PupilID).Distinct().ToList();
                            ClassSubject classSubject = listClassSubject.FirstOrDefault(s => s.ClassID == classSubjectID.ClassID && s.SubjectID == classSubjectID.SubjectID);
                            this.SummedJudgeMark(listPupilID, academicYear, semester, 1, classSubject);
                            //Tinh TBM cho dot
                            this.SummedJudgeMark(listPupilID, academicYear, semester, null, classSubject);

                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {

                
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportJudgeRecord","null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        #endregion ImportJudgeRecord (Import điểm học sinh)

        #region Search by School

        public IQueryable<JudgeRecord> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.SearchJudgeRecord(dic);
        }

        #endregion Search by School

        #region for report

        public Stream ExportReport(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, int PeriodID, int SubjectID, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null)
        {
            //HS_BangDiemTheoDot_[Subject]_[Period]_[Class]_[Semester]
            string reportCode = "";
            reportCode = SystemParamsInFile.BANG_DIEM_THEO_DOT_MOM_NHAN_XET;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();


            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string subject = classSubject.SubjectCat.DisplayName;
            string period = PeriodDeclarationBusiness.Find(PeriodID).Resolution;
            string classname = classSubject.ClassProfile.DisplayName;

            //FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            FileName = "Bang Diem Mon_" + ReportUtils.StripVNSign(subject) + "_" + ReportUtils.StripVNSign(period) + "_" + ReportUtils.StripVNSign(classname)
                + "_" + ReportUtils.StripVNSign(semester) + ".xls";

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet emptySheet = oBook.GetSheet(2);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "D17");

            //Xoa bang thua
            for (int i = 0; i < 5; i++)
            {
                sheet.DeleteRow(13);
            }
            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheet.CopyPasteSameSize(range, "E6");

            #region Tiêu đề template

            //Change header

            String title = "BẢNG ĐIỂM MÔN ";
            title += classSubject.SubjectCat.SubjectName.ToUpper() + " ";

            title += PeriodDeclarationBusiness.Find(PeriodID).Resolution.ToUpper();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                title += " HKI ";
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                title += " HKII ";
            else
            {
                title += " CẢ NĂM ";
            }
            title += " LỚP ";
            title += classSubject.ClassProfile.DisplayName.ToUpper();
            sheet.SetCellValue("A2", acaYear.SchoolProfile.SchoolName);
            //sheet.SetCellValue("A4", title);
            //sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
            #endregion Tiêu đề template

            //Xac dinh so dau diem

            #region xac dinh so dau diem

            string strMarkTypePeriod = MarkRecordBusiness.GetMarkTitle(PeriodID);

            //Lấy con điểm bị khoá
            string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];

            int cntM = 0, cntP = 0, cntV = 0;
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;
                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }

            #endregion xac dinh so dau diem

            int startM = 5;
            int startP = startM + cntM;
            int startV = startP + cntP;
            int k = 0;
            int P_TBM = 0;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                sheet.SetCellValue(7, startCol - 1, listM[k]);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                sheet.SetCellValue(7, startCol - 1, listP[k]);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                sheet.SetCellValue(7, startCol - 1, listV[k]);
                k++;
            }
            if (strMarkTypePeriod.Contains("HK"))
            {
                //coppy co hoc ki
                copyCol(firstSheet, sheet, 8, startCol);
                //sheet.SetCellValue(6, startCol-1, "HK");
            }

            //copy cot TBM
            P_TBM = startCol;
            copyCol(firstSheet, sheet, 9, startCol);
            // Cột nhận xét
            int P_Nhan_xet = startCol;
            copyCol(firstSheet, sheet, 10, startCol);
            //Merger cot tieu de dau diem
            if (startM != startP)
                sheet.MergeRow(6, startM, startP - 1);
            if (startP != startV)
                sheet.MergeRow(6, startP, startV - 1);
            if (cntV > 0)
            {
                sheet.MergeRow(6, startV, startV + cntV - 1);
            }

            //set tieu de cho cac cot dau diem
            sheet.SetCellValue(6, startM, "Miệng");
            sheet.SetCellValue(6, startP, "15p");
            sheet.SetCellValue(6, startV, "1 tiết");
            sheet.SetCellValue(6, P_TBM, "HL.M");
            //dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["Check"] = "checked";
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                                                                    .OrderBy(o => o.OrderInClass)
                                                                    .ThenBy(o => o.PupilProfile.Name)
                                                                    .ThenBy(o => o.PupilProfile.FullName)
                                                                    .ToList();
            int totalPupil = listPupilOfClass.Count;

            //tao cac dong trang cho ds hs
            int RowEnd = 8;
            if (totalPupil > 5)
            {
                int numberGroupStudent = totalPupil / 5;
                IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Nhan_xet);
                for (int j = 1; j <= numberGroupStudent; j++)
                {
                    RowEnd = RowEnd + 5;
                    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                }
            }

            string[] titleAllMark = MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester).Split(',');
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SchoolID"] = SchoolID;
            search["Year"] = academicYear.Year;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["SchoolID"] = SchoolID;
            search["checkWithClassMovement"] = "1";
            search["AcademicYearID"] = academicYear.AcademicYearID;
            IEnumerable<JudgeRecordBO> listJudgeRecord = VJudgeRecordBusiness.SearchJudgeRecordNoView(search).Where(u => titleAllMark.Contains(u.Title)).ToList();

            search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SchoolID"] = SchoolID;
            search["Year"] = academicYear.Year;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            search["AcademicYearID"] = academicYear.AcademicYearID;
            IEnumerable<SummedUpRecordBO> listSummedUpRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).ToList();

            IEnumerable<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, Semester).Where(u => u.SubjectID == SubjectID).ToList();

            IVTRange rangeStyle1 = firstSheet.GetRange("D13", "D13");
            IVTRange rangeStyle2 = firstSheet.GetRange("D17", "D17");
            IVTRange rangeStyle3 = firstSheet.GetRange("D14", "D14");

            int indexPupil = 0;
            int startRow = 8;
            int endV = P_TBM;

            var ExemptType = new ExemptedSubject();

            foreach (PupilOfClass Pupil in listPupilOfClass)
            {
                //cot stt
                indexPupil++;
                sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = convertPosition(startP - 1, startRow);
                string edP = convertPosition(startV - 1, startRow);
                string edV = string.Empty;// = convertPosition(P_TBM - 1, startRow);
                string HK = string.Empty;// convertPosition(P_HK1 - 2, startRow);
                string cntMark = string.Empty;
                if (strMarkTypePeriod.Contains("HK"))
                {
                    edV = convertPosition(P_TBM - 2, startRow);
                    HK = convertPosition(P_TBM - 1, startRow);
                    cntMark = "=COUNTA(" + stM + ":" + HK + ")";
                }
                else
                {
                    edV = convertPosition(P_TBM - 1, startRow);
                    cntMark = "=COUNTA(" + stM + ":" + edV + ")";
                }

                //string cntMark = "=1*COUNTIF(" + stM + ":" + edM + ";" + "\">=0\")";
                sheet.SetFormulaValue(startRow, 2, cntMark);

                //ma hoc sinh
                sheet.SetCellValue(startRow, 3, Pupil.PupilProfile.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                //ho va ten
                sheet.SetCellValue(startRow, 4, Pupil.PupilProfile.FullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                }

                //Copy row style
                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                    Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED || listExempted.Any(u => u.PupilID == Pupil.PupilID))
                {
                    if ((indexPupil - 1) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle1, startRow);
                    }
                    if ((indexPupil) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle2, startRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle3, startRow);
                    }
                    sheet.SetCellValue(startRow, 4, Pupil.PupilProfile.FullName);
                    sheet.Lock(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                    // Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                    DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;
                    DateTime? endDate = Pupil.EndDate;

                    bool pupilOtherStatus = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL) || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF);

                    bool showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);

                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && pupilOtherStatus)
                    {
                        showPupilData = false;
                    }
                    if (!showPupilData)
                    {
                        startRow++;
                        continue;
                    }
                }

                if (listExempted.Any(u => u.PupilID == Pupil.PupilID))
                {
                    int lockCol = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 3 : 4;
                    sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + lockCol);
                }

                //Lay diem tung dau diem hoc sinh
                int col = 5;
                int j = 0;

                IEnumerable<JudgeRecordBO> listJudgeRecordPupil = listJudgeRecord.Where(o => o.PupilID == Pupil.PupilID);
                SummedUpRecordBO lstSummedUpRecord = listSummedUpRecord.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault();

                for (int i = startM; i < startP; i++)
                {
                    String titleMark = listM[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }
                j = 0;

                for (int i = startP; i < startV; i++)
                {
                    String titleMark = listP[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);

                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }

                if (strMarkTypePeriod.Contains("HK"))
                {
                    endV = P_TBM - 1;
                }
                j = 0;
                for (int i = startV; i < endV; i++)
                {
                    String titleMark = listV[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }

                // hoc ki
                if (strMarkTypePeriod.Contains("HK"))
                {
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals("HK")).OrderByDescending(u => u.CreatedDate).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);

                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                }
                //else
                //{
                //    sheet.SetColumnWidth(col, 0);
                //}
                // Fill giá trị TBM để so sánh
                sheet.SetCellValue(startRow, 25, lstSummedUpRecord != null ? lstSummedUpRecord.JudgementResult : string.Empty);
                //set fomular for TBM
                string TBM = string.Empty;
                bool isClassification = acaYear.IsClassification.HasValue && acaYear.IsClassification.Value;
                if (strMarkTypePeriod.Contains("HK"))
                {
                    if (isClassification)
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") * 3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0;\"Đ\";\"CĐ\");\"\")";
                    }
                    else
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                    }

                }
                else
                {
                    if (isClassification)
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + edV + ";" + "\"" + "Đ" + "\"" + ") * 3 - COUNTA(" + stM + ":" + edV + ")" + "*2 >=0;\"Đ\";\"CĐ\");\"\")";
                    }
                    else
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + edV + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + edV + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                    }

                }
                //TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                sheet.SetFormulaValue(startRow, P_TBM, TBM);

                // Fill cột ghi chú
                string note = string.Empty;
                note = "=IF(AND(Y" + startRow + "<>\"\"," + convertPosition(P_TBM, startRow) + "<>\"\")," + "IF(Y" + startRow + "<>" + convertPosition(P_TBM, startRow) + ",\"HLM trong file excel khác HLM trong hệ thống\",\"\"), \"\")";
                //note = "=IF(AND(I8<>"",K8<>""),IF(I8<>K8,"HLM trong file excel khác HLM trong hệ thống",""),"")
                sheet.SetFormulaValue(startRow, P_Nhan_xet, note);

                startRow++;
            }
            sheet.SetColumnWidth(25, 0);
            sheet.MergeRow(4, 1, P_TBM);
            sheet.SetCellValue("A4", title);
            sheet.MergeRow(5, 1, P_TBM);
            sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());

            sheet.GetRange(8 + totalPupil - 1, 1, 8 + totalPupil - 1, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
            int numberGroupPupil = 8 + totalPupil;
            int cntPupilTemp = (totalPupil / 5 + 1) * 5;
            for (int i = numberGroupPupil; i < cntPupilTemp + 8; i++)
            {
                sheet.DeleteRow(i);
            }
            if (totalPupil > 10)
            {
                sheet.DeleteRow(numberGroupPupil);
            }
            sheet.DeleteRow(numberGroupPupil);
            //sheet.DeleteRow(8 + totalPupil);
            //sheet.DeleteRow(8 + totalPupil);
            //sheet.DeleteRow(8 + totalPupil);
            //sheet.DeleteRow(8 + totalPupil);
            //sheet.DeleteRow(8 + totalPupil);

            sheet.SetColumnWidth(2, 0);

            sheet.ProtectSheet();
            emptySheet.Delete();
            firstSheet.Delete();

            //return oBook.ToStream();
            string[] lstContraint = new string[] { "Đ", "CĐ" };
            return oBook.ToStreamNumberValidationData(1, lstContraint, 8, 5, startRow - 1, P_TBM);
        }

        public Stream ExportReportForSemester(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, int PeriodID, int SubjectID, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null)
        {
            string reportCode = "";
            reportCode = SystemParamsInFile.BANG_DIEM_THEO_HOC_KY_CAP23_MON_NHAN_XET;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);

            //kiem tra co cau hinh so con diem toi thieu hay khong
            bool isMinSemester = false;
            int minM = 0;
            int minP = 0;
            int minV = 0;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                isMinSemester = classSubject != null && ((classSubject.MMinFirstSemester.HasValue && classSubject.MMinFirstSemester.Value > 0)
                                   || classSubject.PMinFirstSemester.HasValue && classSubject.PMinFirstSemester.Value > 0
                                   || classSubject.VMinFirstSemester.HasValue && classSubject.VMinFirstSemester.Value > 0);
                minM = classSubject != null && classSubject.MMinFirstSemester.HasValue ? classSubject.MMinFirstSemester.Value : 0;
                minP = classSubject != null && classSubject.PMinFirstSemester.HasValue ? classSubject.PMinFirstSemester.Value : 0;
                minV = classSubject != null && classSubject.VMinFirstSemester.HasValue ? classSubject.VMinFirstSemester.Value : 0;
            }
            else
            {
                isMinSemester = classSubject != null && ((classSubject.MMinSecondSemester.HasValue && classSubject.MMinSecondSemester.Value > 0)
                                   || classSubject.PMinSecondSemester.HasValue && classSubject.PMinSecondSemester.Value > 0
                                   || classSubject.VMinSecondSemester.HasValue && classSubject.VMinSecondSemester.Value > 0);
                minM = classSubject != null && classSubject.MMinSecondSemester.HasValue ? classSubject.MMinSecondSemester.Value : 0;
                minP = classSubject != null && classSubject.PMinSecondSemester.HasValue ? classSubject.PMinSecondSemester.Value : 0;
                minV = classSubject != null && classSubject.VMinSecondSemester.HasValue ? classSubject.VMinSecondSemester.Value : 0;
            }

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string subject = classSubject.SubjectCat.DisplayName;
            string classname = classSubject.ClassProfile.DisplayName;

            //HS_BangDiemTheoHocKyCap23_[Subject]_[Class]_[Semester]
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subject));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(classname));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));

            //FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            FileName = "Bang Diem Mon_" + ReportUtils.StripVNSign(subject) + "_" + ReportUtils.StripVNSign(classname)
                + "_" + ReportUtils.StripVNSign(semester) + ".xls";

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet emptySheet = oBook.GetSheet(2);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            sheet.ProtectSheet("123456a@");

            //Xoa bang thua
            sheet.DeleteRow(13);
            sheet.DeleteRow(14);
            sheet.DeleteRow(15);
            sheet.DeleteRow(16);
            sheet.DeleteRow(17);

            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheet.CopyPasteSameSize(range, "E6");

            #region Tiêu đề template

            //Change header

            String title = "BẢNG ĐIỂM MÔN ";
            title += subject.ToUpper() + "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                title += " HKI ";
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                title += " HKII ";
            else
            {
                title += " CẢ NĂM ";
            }
            title += " LỚP ";
            title += classname.ToUpper();

            sheet.SetCellValue("A2", acaYear.SchoolProfile.SchoolName);
            sheet.SetCellValue("A4", title);
            sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());

            #endregion Tiêu đề template

            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["Semester"] = Semester;

            #region xac dinh so dau diem

            SemeterDeclaration sd = SemeterDeclarationBusiness.Search(search).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            string strMarkTypePeriod = string.Empty;
            if (sd != null)
            {
                strMarkTypePeriod = MarkRecordBusiness.GetMarkSemesterTitle(sd.SemeterDeclarationID);
            }
            // Lấy con điểm bị khoá
            string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];

            int cntM = 0, cntP = 0, cntV = 0;
            if (listMarkType != null && listMarkType.Count() > 0)
            {
                foreach (string typeMark in listMarkType)
                {
                    if (typeMark.Length < 2) continue;
                    if (typeMark[0] == 'M')
                    {
                        listM[cntM] = typeMark;
                        cntM++;
                    }
                    if (typeMark[0] == 'P')
                    {
                        listP[cntP] = typeMark;
                        cntP++;
                    }
                    if (typeMark[0] == 'V')
                    {
                        listV[cntV] = typeMark;
                        cntV++;
                    }
                }
            }
            int totalMark = listMarkType.Count() - 1;
            sheet.MergeRow(4, 1, (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 7 : 8) + totalMark);
            sheet.MergeRow(5, 1, (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 7 : 8) + totalMark);
            #endregion xac dinh so dau diem

            int startM = 5;
            int startP = startM + cntM;
            int startV = startP + cntP;
            int k = 0;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                k++;
            }

            int P_TBM = startCol;
            int P_KTHT = startCol;
            int P_HK1 = startCol;
            int P_Canam = startCol;

            int P_HLM1 = startCol;
            int P_HLM2 = startCol;
            int P_Ghi_chu = startCol;

            //copy cot KTHK
            copyCol(firstSheet, sheet, 8, startCol);
            sheet.SetCellValue(6, P_KTHT, "KTHK");

            //Neu la hk 1
            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                // copy cot TBM HK 1
                P_HLM1 = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetCellValue(6, P_HLM1, "HLM");
                P_TBM = P_HLM1;
            }
            else
            {
                // copy cot TBM HK 2
                P_HLM2 = startCol;
                copyCol(firstSheet, sheet, 10, startCol);
                sheet.SetCellValue(6, P_HLM2, "HLM");
                //copy cot Canam
                P_Canam = startCol;
                copyCol(firstSheet, sheet, 11, startCol);
                sheet.SetCellValue(6, P_Canam, "Cả năm");
                P_TBM = P_HLM2;
            }
            // copy cot ghi chu
            P_Ghi_chu = startCol;
            copyCol(firstSheet, sheet, 12, startCol);
            sheet.SetCellValue(6, P_Ghi_chu, "Ghi chú");
            P_HK1 = P_KTHT;

            //Merger cot tieu de dau diem
            sheet.MergeRow(6, startM, startV - 1);
            sheet.MergeRow(6, startV, startV + cntV - 1);

            //set tieu de cho cac cot dau diem
            sheet.SetCellValue(6, startM, "Điểm hệ số 1");
            sheet.SetCellValue(6, startV, "Điểm hệ số 2");

            sheet.MergeRow(7, startM, startP - 1);
            sheet.MergeRow(7, startP, startV - 1);
            sheet.MergeRow(7, startV, startV + cntV - 1);

            sheet.SetCellValue(7, startM, "Miệng");
            sheet.SetCellValue(7, startP, "Viết/15P");
            sheet.SetCellValue(7, startV, "Viết");
            sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT);

            //dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["Check"] = "check";
            bool isNotShowPupil = acaYear.IsShowPupil.HasValue && acaYear.IsShowPupil.Value;
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).AddPupilStatus(isNotShowPupil).OrderBy(p => p.OrderInClass);

            int cntPupil = listPupilOfClass.Select(o => o.PupilID).Distinct().Count();

            //tao luoi trong phan diem

            sheet.GetRange(8, startM, 12, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startP, 12, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startV, 12, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);


            int indexPupil = 0;

            // Lấy bảng điểm thành phần
            //AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            search = new Dictionary<string, object>();
            //search["Year"] = objAcademicYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["checkWithClassMovement"] = "1";
            search["Year"] = acaYear.Year;
            List<JudgeRecordBO> listJudgeRecord = VJudgeRecordBusiness.SearchJudgeRecordNoView(search).ToList();

            // Lấy bảng điểm TBM
            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["Year"] = acaYear.Year;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            List<SummedUpRecordBO> listSummerupRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).Where(o => o.PeriodID == null).ToList();

            int startRow = 8;
            List<int> lstPupilID = new List<int>();
            var ExemptType = new ExemptedSubject();
            List<PupilOfClassBO> lst = (from poc in listPupilOfClass
                                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                        join es in ExemptedSubjectBusiness.All.Where(p => p.SubjectID == SubjectID && p.AcademicYearID == AcademicYearID && p.SchoolID == SchoolID)
                                        on new { poc.PupilID, poc.ClassID } equals new { es.PupilID, es.ClassID }
                                        into g1
                                        from g2 in g1.DefaultIfEmpty()
                                            //orderby poc.PupilProfile.Name
                                        select new PupilOfClassBO
                                        {
                                            PupilOfClassID = poc.PupilOfClassID,
                                            PupilID = poc.PupilID,
                                            ClassID = poc.ClassID,
                                            SchoolID = poc.SchoolID,
                                            AcademicYearID = poc.AcademicYearID,
                                            Year = poc.Year,
                                            AssignedDate = poc.AssignedDate,
                                            OrderInClass = poc.OrderInClass,
                                            Description = poc.Description,
                                            NoConductEstimation = poc.NoConductEstimation,
                                            Status = poc.Status,
                                            EndDate = poc.EndDate,
                                            FirstSemesterExemptType = g2.FirstSemesterExemptType,
                                            SecondSemesterExemptType = g2.SecondSemesterExemptType,
                                            PupilCode = pp.PupilCode,
                                            PupilFullName = pp.FullName,
                                            Name = pp.Name,
                                            EthnicName = pp.Ethnic != null ? pp.Ethnic.EthnicName : null,
                                            EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null,
                                        }).ToList().OrderBy(p => p.OrderInClass).ThenBy(u => u.Name).ThenBy(p => p.PupilFullName).ToList();

            // lay thong tin mon hoc cua lop
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() {
            {"SubjectID",SubjectID},
            { "SchoolID", SchoolID }
            }
            ).ToList();
            bool isNoLearnFirst = listClassSubject.Any(o => o.SectionPerWeekFirstSemester <= 0);
            bool isNoLearnSecond = listClassSubject.Any(o => o.SectionPerWeekSecondSemester <= 0);
            bool isClassification = acaYear.IsClassification.HasValue && acaYear.IsClassification.Value ? true : false;
            string TBM = string.Empty;

            sheet.GetRange(8, 1, cntPupil + 7, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, 1, cntPupil + 7, P_Ghi_chu).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
            sheet.GetRange(startRow, P_TBM, cntPupil + 7, P_Ghi_chu).IsLock = true;
            sheet.GetRange(startRow, P_TBM, cntPupil + 7, P_Ghi_chu).FillColor(Color.Yellow);
            sheet.SetColumnWidth(P_Ghi_chu, 297);
            //tao luoi trong phan diem
            sheet.GetRange(8, startM, cntPupil + 7, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startP, cntPupil + 7, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startV, cntPupil + 7, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startM, cntPupil + 7, startV + cntV - 1).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(8, 3, cntPupil + 7, 3).SetHAlign(VTHAlign.xlHAlignCenter);

            for (int i = 0; i < cntPupil; i++)
            {
                if (i % 5 == 0)
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                    sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                    sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                }
            }
            //sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

            foreach (PupilOfClassBO Pupil in lst)
            {
                if (!lstPupilID.Contains(Pupil.PupilID))
                {
                    lstPupilID.Add(Pupil.PupilID);

                    //cot stt
                    indexPupil++;
                    sheet.SetCellValue(startRow, 1, indexPupil.ToString());

                    //ma hoc sinh
                    sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                    sheet.GetRange(startRow, 3, startRow, 3).WrapText();
                    string stM = convertPosition(startM, startRow);
                    string stP = convertPosition(startP, startRow);
                    string stV = convertPosition(startV, startRow);

                    string edM = convertPosition(startP - 1, startRow);
                    string edP = convertPosition(startV - 1, startRow);
                    string edV = convertPosition(P_HK1 - 3, startRow);
                    string HK = convertPosition(P_HK1, startRow);

                    if (isMinSemester)
                    {
                        if (isClassification)
                        {
                            TBM = "=IF(AND(COUNTIF(" + stM + ":" + edM + ",\"**\") >= " + minM + ",COUNTIF(" + stP + ":" + edP + ",\"**\") >= " + minP + ",COUNTIF(" + stV + ":" + edV + ",\"**\") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\",IF(COUNTIF(" + stM + ":" + HK + "," + "\"" + "Đ" + "\"" + ")*3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0, \"Đ\", \"CĐ\"),\"CĐ\"),\"\"),\"\")";
                        }
                        else
                        {
                            TBM = "=IF(AND(COUNTIF(" + stM + ":" + edM + ",\"**\") >= " + minM + ",COUNTIF(" + stP + ":" + edP + ",\"**\") >= " + minP + ",COUNTIF(" + stV + ":" + edV + ",\"**\") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\",IF(COUNTIF(" + stM + ":" + HK + "," + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3, 0) >=0, \"Đ\", \"CĐ\"),\"CĐ\"),\"\"),\"\")";
                        }
                    }
                    else
                    {
                        if (isClassification)
                        {
                            TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\";IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ")*3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0; \"Đ\"; \"CĐ\");\"CĐ\");\"\")";
                        }
                        else
                        {
                            TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\";IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0; \"Đ\"; \"CĐ\");\"CĐ\");\"\")";
                        }
                    }

                    sheet.SetFormulaValue(startRow, P_TBM, TBM);

                    // sheet.LockSheet = true;
                    // sheet.ProtectSheet();
                    sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                    sheet.GetRange(startRow, 4, startRow, 4).WrapText();

                    if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                        || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF
                        || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                        || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED
                      )
                    {

                        sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                        sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                        sheet.Lock(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                        // Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                        DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                        DateTime? endDate = Pupil.EndDate;
                        bool pupilOtherStatus = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL) || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF);

                        bool showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                            || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);

                        if (!showPupilData)
                        {
                            startRow++;
                            continue;
                        }
                    }
                    else
                    {
                        sheet.GetRange(startRow, 5, cntPupil + 7, P_KTHT).IsLock = false;
                    }

                    if ((Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && Pupil.FirstSemesterExemptType.HasValue && Pupil.FirstSemesterExemptType.Value > 0)
                        || (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && Pupil.SecondSemesterExemptType.HasValue && Pupil.SecondSemesterExemptType.Value > 0))
                    {
                        sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                        sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                    }

                    var listJudgeRecordPupil = listJudgeRecord.Where(o => o.PupilID == Pupil.PupilID);

                    //ho va ten
                    //Lay diem tung dau diem hoc sinh
                    int col = 5;
                    int j = 0;
                    for (int i = startM; i < startP; i++)
                    {
                        String titleMark = listM[j];
                        JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                        sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                        if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }

                    j = 0;
                    for (int i = startP; i < startV; i++)
                    {
                        String titleMark = listP[j];
                        JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                        sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                        if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }

                    j = 0;
                    for (int i = startV; i < P_TBM - 1; i++)
                    {
                        String titleMark = listV[j];
                        JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                        sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                        if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }

                    //diem kiem tra hk
                    JudgeRecordBO judgeRecordItemHK = listJudgeRecordPupil.Where(o => o.Title.Equals("HK")).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeRecordItemHK != null ? judgeRecordItemHK.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }

                    IEnumerable<SummedUpRecordBO> listSummedUpRecord = listSummerupRecord.Where(o => o.PupilID == Pupil.PupilID);
                    if (listSummedUpRecord != null && listSummedUpRecord.Count() > 0)
                    {
                        SummedUpRecordBO SummedUpRecord = listSummedUpRecord.FirstOrDefault();
                        sheet.SetCellValue(startRow, 40, SummedUpRecord.JudgementResult);
                    }
                    string formularTBHK = "=IF(" + convertPosition(P_TBM, startRow) + "<>\"\",IF(" + convertPosition(P_TBM, startRow) + "=\"Đ\", \"Đ\", \"CĐ\"),\"\")";
                    if (Pupil.SecondSemesterExemptType != null || isNoLearnSecond)
                    {
                        formularTBHK = "=" + convertPosition(P_HK1, startRow);
                    }

                    if (Pupil.FirstSemesterExemptType != null || isNoLearnFirst)
                    {
                        formularTBHK = "=" + convertPosition(P_TBM, startRow);
                    }
                    //string formularTBHK = "=I
                    //sheet.ProtectSheet();
                    if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        sheet.SetFormulaValue(startRow, P_Canam, formularTBHK);

                    // Cot ghi chu
                    string note = string.Empty;
                    note = "=IF(AND(AN" + startRow + "<>\"\"," + convertPosition(P_TBM, startRow) + "<>\"\")," + "IF(AN" + startRow + "<>" + convertPosition(P_TBM, startRow) + ",\"HLM trong file excel khác HLM trong hệ thống\",\"\"), \"\")";
                    sheet.SetFormulaValue(startRow, P_Ghi_chu, note);

                    startRow++;
                }
            }
            //sheet.SetColumnWidth(P_HK1, 0);
            sheet.SetColumnWidth(40, 0);
            sheet.SetColumnWidth(2, 0);
            //sheet.DeleteRow(8 + cntPupil);
            if (cntPupil <= 10)
            {
                int totalRecordNotFill = 10 + 8; // 8 bat dau tu dong thu 8
                int totalRecordDelete = cntPupil + 8;
                for (int i = totalRecordNotFill; i >= totalRecordDelete; i--)
                {
                    sheet.DeleteRow(i);
                }
            }
            sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                sheet.GetRange(8 + cntPupil - 1, 1, 8 + cntPupil - 1, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                sheet.MergeRow(4, 1, P_Canam - 1);
                sheet.SetCellValue("A4", title);
                sheet.MergeRow(5, 1, P_Canam - 1);
                sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
            }
            else
            {
                sheet.GetRange(8 + cntPupil - 1, 1, 8 + cntPupil - 1, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                sheet.MergeRow(4, 1, P_Canam);
                sheet.SetCellValue("A4", title);
                sheet.MergeRow(5, 1, P_Canam);
                sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
            }

            sheet.Name = Utils.StripVNSignAndSpace(subject);
            sheet.SetFontName("Times New Roman", 11);
            emptySheet.Delete();
            firstSheet.Delete();

            //return oBook.ToStream();
            string[] lstContraint = new string[] { "Đ", "CĐ" };
            int tocolumn = listMarkType.Count();
            return oBook.ToStreamNumberValidationData(1, lstContraint, 8, 5, cntPupil + 7, tocolumn + 4);
        }

        public void copyCol(IVTWorksheet sheetOld, IVTWorksheet sheet, int col, int newPostioncol)
        {
            IVTRange range = sheetOld.GetRange(rowHeaderStart, col, rowHeaderEnd, col);

            sheet.CopyPasteSameRowHeigh(range, rowHeaderStart, newPostioncol);

            startCol++;
        }

        public string convertPosition(int col, int row)
        {
            int i = 0;
            char rs = ' ';
            for (char ch = 'A'; ch <= 'Z'; ch++)
            {
                i++;
                if (i == col)
                    return ch + row.ToString();
            }
            return rs + row.ToString();
        }

        #endregion for report

        #region report

        /// <summary>
        /// GetHashKey
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   4:28 PM
        /// </remarks>
        public string GetHashKey(JudgeRecordForReportBO entity)
        {
            return ReportUtils.GetHashKey(new Dictionary<string, object>()
            {
                {"ClassID",entity.ClassID} ,
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel},
                 {"SubjectID",entity.SubjectID},
                  {"Semester",entity.Semester},
                    {"EducationLevelID",entity.EducationLevelID}
            });
        }

        /// <summary>
        /// InsertJudgeRecordBO
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="Data">The data.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   4:28 PM
        /// </remarks>
        public ProcessedReport InsertJudgeRecordBO(JudgeRecordForReportBO entity, Stream Data)
        {
            if (entity.ClassID.HasValue)
            {
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            string reportCode = SystemParamsInFile.HS_DIEMMONNHANXETCAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //Khởi tạo đối tượng ProcessedReport:
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = SystemParamsInFile.HS_DIEMMONNHANXETCAP1;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string ClassName = "";
            if (entity.ClassID.HasValue && entity.ClassID.Value != 0)
            {
                ClassProfile cp = ClassProfileRepository.Find(entity.ClassID);
                ClassName = cp.DisplayName;
            }
            else
            {
                ClassName = "TC";
            }
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);

            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.ConvertSemesterForReportName(entity.Semester.HasValue ? entity.Semester.Value : 0));

            //EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevelID);
            //outputNamePattern = outputNamePattern.Replace("[EducationLevel]", edu != null ? edu.Resolution : "TC");

            SubjectCat es = SubjectCatBusiness.Find(entity.SubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectName) : "TC");

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
               {"ClassID",entity.ClassID} ,
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel},
                 {"SubjectID",entity.SubjectID},
                  {"Semester",entity.Semester},
                    {"EducationLevelID",entity.EducationLevelID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// <summary>
        /// GetJudgeRecordBO
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   4:28 PM
        /// </remarks>
        public ProcessedReport GetJudgeRecordBO(JudgeRecordForReportBO entity)
        {
            string reportCode = SystemParamsInFile.HS_DIEMMONNHANXETCAP1;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// CreateJudgeRecordBO
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   4:28 PM
        /// </remarks>
        public Stream CreateJudgeRecordBO(JudgeRecordForReportBO entity)
        {
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            if (entity.ClassID.HasValue)
            {
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            string reportCode = SystemParamsInFile.HS_DIEMMONNHANXETCAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplateHK1 = oBook.GetSheet(2);
            IVTWorksheet sheetTemplateHK2 = oBook.GetSheet(3);
            IVTWorksheet sheetTemplateHK1Lop12 = oBook.GetSheet(4);
            IVTWorksheet sheetTemplateHK2Lop12 = oBook.GetSheet(5);


            #region lay du lieu
            AcademicYear acaYear = AcademicYearBusiness.Find(entity.AcademicYearID);

            List<PupilOfClass> lsPupilID = PupilOfClassBusiness.SearchBySchool(entity.SchoolID.Value, new Dictionary<string, object>()
                                                                    {
                                                                        {"EducationLevelID",entity.EducationLevelID},
                                                                        {"ClassID",entity.ClassID},
                                                                        {"AcademicYearID",entity.AcademicYearID.Value},
                                                                        {"Check","Check"}
                                                                    }).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ToList();

            List<SchoolMovement> lsSchoolMovement = SchoolMovementBusiness.SearchBySchool(entity.SchoolID.Value, new Dictionary<string, object>()
                                                        {
                                                             {"ClassID", entity.ClassID}
                                                            ,{"AcademicYearID",entity.AcademicYearID.Value}
                                                        }).ToList();
            List<ClassMovement> lsClassMovement = ClassMovementBusiness.SearchBySchool(entity.SchoolID.Value, new Dictionary<string, object>()
                                                        {
                                                             {"ClassID", entity.ClassID}
                                                            ,{"AcademicYearID",entity.AcademicYearID.Value}
                                                        }).ToList();
            List<PupilLeavingOff> lsPupilLeavingOff = PupilLeavingOffBusiness.SearchBySchool(entity.SchoolID.Value, new Dictionary<string, object>()
                                                        {
                                                             {"ClassID", entity.ClassID}
                                                            ,{"AcademicYearID",entity.AcademicYearID.Value}
                                                        }).ToList();

            List<JudgeRecordForReportBO> lsViewModel = new List<JudgeRecordForReportBO>();

            JudgeRecordForReportBO jrwm = new JudgeRecordForReportBO();

            List<JudgeRecordBO> lsJRBO = VJudgeRecordBusiness.SearchJudgeRecordNoView(new Dictionary<string, object>()
                                                                  {
                                                                        {"ClassID",entity.ClassID},
                                                                        {"SchoolID",entity.SchoolID.Value},
                                                                        {"AcademicYearID",entity.AcademicYearID},
                                                                        {"SubjectID",entity.SubjectID},
                                                                        {"Year", acaYear.Year}
                                                                  }).ToList();

            List<SummedUpRecordBO> lsSUR = VSummedUpRecordBusiness.SearchSummedUpRecord(new Dictionary<string, object>()
                                                {
                                                    {"SchoolID",entity.SchoolID.Value},
                                                  {"AcademicYearID",entity.AcademicYearID.Value},
                                                  {"ClassID",entity.ClassID.Value},
                                                  {"SubjectID",entity.SubjectID},
                                                  {"IsCommenting", SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE},
                                                   {"Year", acaYear.Year}
                                                }).ToList();

            List<JudgeRecordBO> lsJRofPupil = new List<JudgeRecordBO>();
            List<SummedUpRecordBO> lsSURofPupil = new List<SummedUpRecordBO>();

            List<ExemptedSubject> lsExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(entity.ClassID.Value, entity.Semester.Value).Where(u => u.SubjectID == entity.SubjectID.Value).ToList();

            foreach (var item in lsPupilID)
            {
                lsJRofPupil = lsJRBO.Where(o => o.PupilID == item.PupilID).ToList();
                lsSURofPupil = lsSUR.Where(o => o.PupilID == item.PupilID).ToList();

                jrwm = new JudgeRecordForReportBO();
                jrwm.AcademicYearID = entity.AcademicYearID.Value;
                jrwm.ClassID = entity.ClassID;
                jrwm.FullName = item.PupilProfile.FullName;
                jrwm.PupilCode = item.PupilProfile.PupilCode;
                jrwm.BirthDate = item.PupilProfile.BirthDate;
                jrwm.OrderInClass = item.OrderInClass;
                jrwm.PupilID = item.PupilID;
                jrwm.SchoolID = item.SchoolID;
                jrwm.Name = item.PupilProfile.Name;
                jrwm.SubjectID = entity.SubjectID;
                jrwm.Status = item.Status;
                jrwm.IsShowMark = lsJRofPupil.Count > 0;
                jrwm.ExemptedSuject = lsExempteds.Any(u => u.PupilID == item.PupilID);

                if (jrwm.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || jrwm.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                {
                    #region Cac con diem
                    foreach (var order in lsJRofPupil)
                    {
                        if (!string.IsNullOrWhiteSpace(order.Judgement))
                        {
                            if (order.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                switch (order.OrderNumber)
                                {
                                    case 1:
                                        jrwm.H11 = order.Judgement.Length == 1;
                                        break;

                                    case 2:
                                        jrwm.H12 = order.Judgement.Length == 1;
                                        break;

                                    case 3:
                                        jrwm.H13 = order.Judgement.Length == 1;
                                        break;

                                    case 4:
                                        jrwm.H14 = order.Judgement.Length == 1;
                                        break;

                                    case 5:
                                        jrwm.H15 = order.Judgement.Length == 1;
                                        break;

                                    default:
                                        break;
                                }
                            }
                            if (order.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            {
                                switch (order.OrderNumber)
                                {
                                    case 1:
                                        jrwm.H21 = order.Judgement.Length == 1;
                                        break;

                                    case 2:
                                        jrwm.H22 = order.Judgement.Length == 1;
                                        break;

                                    case 3:
                                        jrwm.H23 = order.Judgement.Length == 1;
                                        break;

                                    case 4:
                                        jrwm.H24 = order.Judgement.Length == 1;
                                        break;

                                    case 5:
                                        jrwm.H25 = order.Judgement.Length == 1;
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    #endregion

                    #region lấy điểm tổng kết từ sumuprecord
                    if (lsSURofPupil.Count() > 0)
                    {
                        if (entity.Semester == 1)
                        {
                            SummedUpRecordBO s = lsSURofPupil.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            string temp = s != null ? s.JudgementResult : string.Empty;
                            jrwm.MarkSemester1 = temp;
                        }
                        else if (entity.Semester == 2)
                        {
                            SummedUpRecordBO s1 = lsSURofPupil.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            SummedUpRecordBO s2 = lsSURofPupil.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                            string temphk1 = s1 != null ? s1.JudgementResult : string.Empty;
                            string temphk2 = s2 != null ? s2.JudgementResult : string.Empty;

                            //hk1
                            jrwm.MarkSemester1 = temphk1;

                            //hk2
                            jrwm.MarkSemester2 = temphk2;
                        }
                    }

                    #endregion lấy điểm tổng kết từ sumuprecord
                }
                else //chuyen truong,chuyen lop,thoi hoc
                {
                    #region Cac con diem
                    DateTime dt = new DateTime();
                    if (jrwm.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    {
                        dt = lsSchoolMovement.OrderByDescending(u => u.MovedDate).FirstOrDefault(u => u.PupilID == item.PupilID).MovedDate;
                    }
                    else if (jrwm.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                    {
                        dt = lsClassMovement.OrderByDescending(u => u.MovedDate).FirstOrDefault(u => u.PupilID == item.PupilID).MovedDate;
                    }
                    else if (jrwm.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                    {
                        dt = lsPupilLeavingOff.OrderByDescending(u => u.LeavingDate).FirstOrDefault(u => u.PupilID == item.PupilID).LeavingDate;
                    }

                    bool showMarkHK1 = DateTime.Compare(dt, acaYear.FirstSemesterEndDate.Value) >= 0;
                    bool showMarkHK2 = DateTime.Compare(dt, acaYear.SecondSemesterEndDate.Value) >= 0;
                    if (showMarkHK1) showMarkHK2 = false;
                    foreach (var order in lsJRofPupil)
                    {
                        if (!string.IsNullOrWhiteSpace(order.Judgement))
                        {
                            if (order.Semester == 1 && showMarkHK1)
                            {
                                switch (order.OrderNumber)
                                {
                                    case 1:
                                        jrwm.H11 = order.Judgement.Length == 1;
                                        break;

                                    case 2:
                                        jrwm.H12 = order.Judgement.Length == 1;
                                        break;

                                    case 3:
                                        jrwm.H13 = order.Judgement.Length == 1;
                                        break;

                                    case 4:
                                        jrwm.H14 = order.Judgement.Length == 1;
                                        break;

                                    case 5:
                                        jrwm.H15 = order.Judgement.Length == 1;
                                        break;

                                    default:
                                        break;
                                }
                            }
                            if (order.Semester == 2 && showMarkHK2)
                            {
                                switch (order.OrderNumber)
                                {
                                    case 1:
                                        jrwm.H21 = order.Judgement.Length == 1;
                                        break;

                                    case 2:
                                        jrwm.H22 = order.Judgement.Length == 1;
                                        break;

                                    case 3:
                                        jrwm.H23 = order.Judgement.Length == 1;
                                        break;

                                    case 4:
                                        jrwm.H24 = order.Judgement.Length == 1;
                                        break;

                                    case 5:
                                        jrwm.H25 = order.Judgement.Length == 1;
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    #endregion

                    #region lấy điểm tổng kết từ sumuprecord

                    if (lsSURofPupil.Count() > 0)
                    {
                        if (entity.Semester == 1 && showMarkHK1)
                        {
                            SummedUpRecordBO s = lsSURofPupil.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            string temp = s != null ? s.JudgementResult : string.Empty;
                            jrwm.MarkSemester1 = temp;
                        }

                        if (entity.Semester == 2 && showMarkHK2)
                        {
                            SummedUpRecordBO s1 = lsSURofPupil.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            SummedUpRecordBO s2 = lsSURofPupil.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                            string temphk1 = s1 != null ? s1.JudgementResult : string.Empty;
                            string temphk2 = s2 != null ? s2.JudgementResult : string.Empty;

                            //hk1
                            jrwm.MarkSemester1 = temphk1;

                            //hk2
                            jrwm.MarkSemester2 = temphk2;
                        }
                    }

                    #endregion lấy điểm tổng kết từ sumuprecord
                }
                lsViewModel.Add(jrwm);
            }

            //dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = entity.ClassID;
            dic["Check"] = "checked";
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(entity.SchoolID.Value, dic)
                                                                    .OrderBy(o => o.OrderInClass)
                                                                    .ThenBy(o => o.PupilProfile.Name)
                                                                    .ToList();
            int totalPupil = listPupilOfClass.Count;

            //tao cac dong trang cho ds hs
            int RowEnd = 9;
            int RowLast = RowEnd + totalPupil - 1;


            #endregion lay du lieu

            lsViewModel = lsViewModel.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            string LockTitle = MarkRecordBusiness.GetLockMarkTitlePrimary(entity.SchoolID.Value, entity.AcademicYearID.Value, entity.ClassID.Value, entity.SubjectID.Value, entity.Semester.Value).Replace("_", "");



            //lay du lieu con diem bi khoa

            //lock het tat ca cac sheet
            sheet.LockSheet = true;
            sheetTemplateHK1.LockSheet = true;
            sheetTemplateHK1Lop12.LockSheet = true;
            sheetTemplateHK1Lop12.LockSheet = true;
            sheetTemplateHK2Lop12.LockSheet = true;

            int StartIndexFillFormular = 9;
            int firstColumnMark = 5;
            int firstColumnMarkHK2LessThan3 = 10;
            int firstColumnMarkHK2BiggerThan3 = 11;
            int firstColum = 1;
            int lastColumnEduLessThan3HK2 = 14;
            int lastColumnEduBiggerThan3HK2 = 16;
            int lastColumnEduLessThan3HK1 = 9;
            int lastColumnEduBiggerThan3HK1 = 11;
            int lastColumnCheck = 69;
            string sheetName = "BangHocLucMonHocNX";

            if (entity.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                IVTRange templateRange;
                if (entity.EducationLevelID > 2)
                {
                    templateRange = sheetTemplateHK1.GetRange("A1", "O75");
                    sheetTemplateHK1.GetRange(RowLast, 1, RowLast, lastColumnEduBiggerThan3HK1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }
                else
                {
                    templateRange = sheetTemplateHK1Lop12.GetRange("A1", "O75");
                    sheetTemplateHK1Lop12.GetRange(RowLast, 1, RowLast, lastColumnEduLessThan3HK1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }

                Dictionary<string, object> KingDic = new Dictionary<string, object>();
                List<object> QueenDic = new List<object>();

                for (int i = 0; i < lsViewModel.Count(); i++)
                {
                    JudgeRecordForReportBO thisJudgeRecordForReportBO = lsViewModel[i];
                    Dictionary<string, object> princeDic = new Dictionary<string, object>();
                    princeDic["STT"] = i + 1;
                    princeDic["PupilCode"] = thisJudgeRecordForReportBO.PupilCode;
                    princeDic["FullName"] = thisJudgeRecordForReportBO.FullName;
                    princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisJudgeRecordForReportBO.BirthDate);




                    if (thisJudgeRecordForReportBO.IsShowMark)
                    {
                        princeDic["H11"] = thisJudgeRecordForReportBO.H11 ? "Đ" : "";
                        princeDic["H12"] = thisJudgeRecordForReportBO.H12 ? "Đ" : "";
                        princeDic["H13"] = thisJudgeRecordForReportBO.H13 ? "Đ" : "";
                        princeDic["H14"] = thisJudgeRecordForReportBO.H14 ? "Đ" : "";
                        if (entity.EducationLevelID > 2)
                        {
                            princeDic["H15"] = thisJudgeRecordForReportBO.H15 ? "Đ" : "";
                        }
                        princeDic["MarkSemester1"] = thisJudgeRecordForReportBO.MarkSemester1;

                        if (thisJudgeRecordForReportBO.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && !thisJudgeRecordForReportBO.ExemptedSuject)
                        {
                            if (entity.EducationLevelID > 2)
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, firstColumnMark, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK1).IsLock = false;
                            }
                            else
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, firstColumnMark, StartIndexFillFormular + i, lastColumnEduLessThan3HK1).IsLock = false;
                            }
                        }
                        else
                        {
                            if (entity.EducationLevelID > 2)
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK1).FillColor(Color.Yellow);
                            }
                            else
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduLessThan3HK1).FillColor(Color.Yellow);
                            }
                        }
                    }
                    else
                    {
                        princeDic["H11"] = "";
                        princeDic["H12"] = "";
                        princeDic["H13"] = "";
                        princeDic["H14"] = "";
                        if (entity.EducationLevelID > 2)
                        {
                            princeDic["H15"] = "";
                        }
                        princeDic["MarkSemester1"] = "";
                        if (thisJudgeRecordForReportBO.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING || thisJudgeRecordForReportBO.ExemptedSuject)
                        {
                            if (entity.EducationLevelID > 2)
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, firstColumnMark, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK1).IsLock = true;
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK1).FillColor(Color.Yellow);
                            }
                            else
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, firstColumnMark, StartIndexFillFormular + i, lastColumnEduLessThan3HK1).IsLock = true;
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduLessThan3HK1).FillColor(Color.Yellow);
                            }
                        }
                        else
                        {
                            if (entity.EducationLevelID > 2)
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, firstColumnMark, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK1).IsLock = false;
                            }
                            else
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, firstColumnMark, StartIndexFillFormular + i, lastColumnEduLessThan3HK1).IsLock = false;
                            }
                        }
                    }

                    //binh thuong chi truyen cac diem thuong xuyen vao va de diem cuoi ky co cthuc tu tinh
                    //nhung 1 so doi tuong chi truyen diem cuoi ky vao => neu diem cuoi ky khac rong thi
                    //phai fill du lieu
                    string index = (StartIndexFillFormular + i).ToString();
                    if (entity.EducationLevelID > 2)
                    {
                        //fill formular
                        string cell = "J" + index;

                        string formular = "= IF(COUNTIF(E" + index + ":I" + index + ";\"\")=5;\"\";IF(COUNTIF(E"
                            + index + ":I" + index + ";Ref!$A$2)<3;\"B\";IF(COUNTIF(E" + index + ":I" + index + ";Ref!$A$2)=5;\"A+\";\"A\")))";
                        //if (!string.IsNullOrEmpty(thisJudgeRecordForReportBO.MarkSemester1))
                        //{
                        //    sheetTemplateHK1.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //}
                        //else
                        //{
                        //    sheetTemplateHK1.SetFormulaValue(cell, formular);
                        //}
                        sheetTemplateHK1.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //sheetTemplateHK1.SetFormulaValue(cell, formular);
                    }
                    else
                    {
                        string cell = "I" + index;
                        string formular = "=IF(COUNTIF(E" + index + ":H" + index + ";\"\")=4;\"\";IF(COUNTIF(E"
                            + index + ":H" + index + ";Ref!$A$2)<2;\"B\";IF(COUNTIF(E" + index + ":H" + index + ";Ref!$A$2)=4;\"A+\";\"A\")))";
                        //if (!string.IsNullOrEmpty(thisJudgeRecordForReportBO.MarkSemester1))
                        //{
                        //    sheetTemplateHK1Lop12.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //}
                        //else
                        //{
                        //    sheetTemplateHK1Lop12.SetFormulaValue(cell, formular);
                        //}
                        //sheetTemplateHK1Lop12.SetFormulaValue(cell, formular);
                        sheetTemplateHK1Lop12.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //sheetTemplateHK1.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                    }
                    if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (entity.EducationLevelID > 2)
                        {
                            if (LockTitle.Contains("NX1") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, 5, StartIndexFillFormular + i, 5).IsLock = true;
                            }
                            if (LockTitle.Contains("NX2") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, 6, StartIndexFillFormular + i, 6).IsLock = true;
                            }
                            if (LockTitle.Contains("NX3") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, 7, StartIndexFillFormular + i, 7).IsLock = true;
                            }
                            if (LockTitle.Contains("NX4") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, 8, StartIndexFillFormular + i, 8).IsLock = true;
                            }
                            if (LockTitle.Contains("NX5") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, 9, StartIndexFillFormular + i, 9).IsLock = true;
                            }
                            if (LockTitle.Contains("HKI"))
                            {
                                sheetTemplateHK1.GetRange(StartIndexFillFormular + i, 10, StartIndexFillFormular + i, 10).IsLock = true;
                            }
                        }
                        else
                        {
                            if (LockTitle.Contains("NX1") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, 5, StartIndexFillFormular + i, 5).IsLock = true;
                            }
                            if (LockTitle.Contains("NX2") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, 6, StartIndexFillFormular + i, 6).IsLock = true;
                            }
                            if (LockTitle.Contains("NX3") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, 7, StartIndexFillFormular + i, 7).IsLock = true;
                            }
                            if (LockTitle.Contains("NX4") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, 8, StartIndexFillFormular + i, 8).IsLock = true;
                            }
                            if (LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK1Lop12.GetRange(StartIndexFillFormular + i, 9, StartIndexFillFormular + i, 9).IsLock = true;
                            }
                        }
                    }
                    QueenDic.Add(princeDic);
                }

                SupervisingDept sd = SupervisingDeptBusiness.Find(entity.SupervisingDeptID);
                SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
                string year = "Năm học {0} - {1}";
                AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
                SubjectCat sc = SubjectCatBusiness.Find(entity.SubjectID);
                ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);

                string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));
                string SubjectSemesterAndClass = "BẢNG ĐÁNH GIÁ HỌC LỰC MÔN {0} HỌC KỲ 1 LỚP {1}";
                string strSubjectSemesterAndClass = string.Format(SubjectSemesterAndClass, sc != null ? sc.DisplayName : "Tất cả", cp != null ? cp.DisplayName : "Tất cả");

                KingDic.Add("Rows", QueenDic);
                KingDic.Add("SupervisingDeptName", sd.SupervisingDeptName);
                KingDic.Add("SchoolName", sp.SchoolName);
                KingDic.Add("TitleYear", yearTitle);
                KingDic.Add("SubjectSemesterAndClass", strSubjectSemesterAndClass.ToUpper());

                templateRange.FillVariableValue(KingDic);
                if (entity.EducationLevelID > 2)
                {
                    sheetTemplateHK2.Delete();
                    sheetTemplateHK1Lop12.Delete();
                    sheetTemplateHK2Lop12.Delete();
                    for (var i = StartIndexFillFormular + lsViewModel.Count(); i <= lastColumnCheck; i++)
                    {
                        sheetTemplateHK1.DeleteRow(StartIndexFillFormular + lsViewModel.Count());
                    }
                    sheetTemplateHK1.Name = sheetName;
                    sheetTemplateHK1.ProtectSheet();
                }
                else
                {
                    sheetTemplateHK1.Delete();
                    sheetTemplateHK2.Delete();
                    sheetTemplateHK2Lop12.Delete();
                    for (var i = StartIndexFillFormular + lsViewModel.Count(); i <= lastColumnCheck; i++)
                    {
                        sheetTemplateHK1Lop12.DeleteRow(StartIndexFillFormular + lsViewModel.Count());
                    }
                    sheetTemplateHK1Lop12.Name = sheetName;
                    sheetTemplateHK1Lop12.ProtectSheet();
                }
                //sheet.CopyPasteSameRowHeigh(templateRange, 1, VTVector.dic['A']);
            }
            else  //HK2
            {
                IVTRange templateRange;
                if (entity.EducationLevelID > 2)
                {
                    templateRange = sheetTemplateHK2.GetRange("A1", "Q75");
                    sheetTemplateHK2.GetRange(RowLast, 1, RowLast, lastColumnEduBiggerThan3HK2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }
                else
                {
                    templateRange = sheetTemplateHK2Lop12.GetRange("A1", "Q75");
                    sheetTemplateHK2Lop12.GetRange(RowLast, 1, RowLast, lastColumnEduLessThan3HK2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }
                Dictionary<string, object> KingDic = new Dictionary<string, object>();
                List<object> QueenDic = new List<object>();

                //
                for (int i = 0; i < lsViewModel.Count(); i++)
                {
                    JudgeRecordForReportBO thisJudgeRecordForReportBO = lsViewModel[i];
                    PupilProfile pp = PupilProfileBusiness.Find(thisJudgeRecordForReportBO.PupilID);
                    Dictionary<string, object> princeDic = new Dictionary<string, object>();
                    princeDic["STT"] = i + 1;
                    princeDic["PupilCode"] = thisJudgeRecordForReportBO.PupilCode;
                    princeDic["FullName"] = thisJudgeRecordForReportBO.FullName;
                    princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", pp.BirthDate);

                    if (thisJudgeRecordForReportBO.IsShowMark)
                    {
                        princeDic["H11"] = thisJudgeRecordForReportBO.H11 ? "Đ" : "";
                        princeDic["H12"] = thisJudgeRecordForReportBO.H12 ? "Đ" : "";
                        princeDic["H13"] = thisJudgeRecordForReportBO.H13 ? "Đ" : "";
                        princeDic["H14"] = thisJudgeRecordForReportBO.H14 ? "Đ" : "";
                        if (entity.EducationLevelID > 2)
                        {
                            princeDic["H15"] = thisJudgeRecordForReportBO.H15 ? "Đ" : "";
                            princeDic["H25"] = thisJudgeRecordForReportBO.H25 ? "Đ" : "";
                        }

                        princeDic["H21"] = thisJudgeRecordForReportBO.H21 ? "Đ" : "";
                        princeDic["H22"] = thisJudgeRecordForReportBO.H22 ? "Đ" : "";
                        princeDic["H23"] = thisJudgeRecordForReportBO.H23 ? "Đ" : "";
                        princeDic["H24"] = thisJudgeRecordForReportBO.H24 ? "Đ" : "";

                        princeDic["MarkSemester1"] = thisJudgeRecordForReportBO.MarkSemester1;
                        princeDic["MarkSemester2"] = thisJudgeRecordForReportBO.MarkSemester2;
                    }
                    else
                    {
                        princeDic["H11"] = "";
                        princeDic["H12"] = "";
                        princeDic["H13"] = "";
                        princeDic["H14"] = "";
                        if (entity.EducationLevelID > 2)
                        {
                            princeDic["H15"] = "";
                            princeDic["H25"] = "";
                        }

                        princeDic["H21"] = "";
                        princeDic["H22"] = "";
                        princeDic["H23"] = "";
                        princeDic["H24"] = "";

                        princeDic["MarkSemester1"] = "";
                        princeDic["MarkSemester2"] = "";
                    }
                    string index = (StartIndexFillFormular + i).ToString();
                    if (thisJudgeRecordForReportBO.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING || thisJudgeRecordForReportBO.ExemptedSuject)
                    {
                        if (entity.EducationLevelID > 2)
                        {
                            sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2BiggerThan3, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK2).IsLock = true;
                            sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK2).FillColor(Color.Yellow);
                        }
                        else
                        {
                            sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2LessThan3, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).IsLock = true;
                            sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).FillColor(Color.Yellow);
                        }
                    }
                    else
                    {
                        if (entity.EducationLevelID > 2)
                        {
                            sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2BiggerThan3, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK2).IsLock = false;
                            sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColum + 4, StartIndexFillFormular + i, firstColumnMarkHK2BiggerThan3 - 1).FillColor(Color.Yellow);
                        }
                        else
                        {
                            sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2LessThan3, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).IsLock = false;
                            sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColum + 4, StartIndexFillFormular + i, firstColumnMarkHK2LessThan3 - 1).FillColor(Color.Yellow);
                        }
                    }
                    if (entity.EducationLevelID > 2)
                    {
                        //fill formular
                        string cell = "J" + index;
                        string formular = "= IF(COUNTIF(E" + index + ":I" + index + ";\"\")=5;\"\";IF(COUNTIF(E"
                           + index + ":I" + index + ";Ref!$A$2)<3;\"B\";IF(COUNTIF(E" + index + ":I" + index + ";Ref!$A$2)=5;\"A+\";\"A\")))";

                        //sheetTemplateHK2.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);



                        string cell2 = "P" + index;

                        string formular2 = "= IF(COUNTIF(K" + index + ":O" + index + ";\"\")=5;\"\";IF(COUNTIF(E"
                         + index + ":O" + index + ";Ref!$A$2)<5;\"B\";IF(COUNTIF(E" + index + ":O" + index + ";Ref!$A$2)=10;\"A+\";\"A\")))";

                        //if (!string.IsNullOrEmpty(thisJudgeRecordForReportBO.MarkSemester2))
                        //{
                        //    sheetTemplateHK2.SetCellValue(cell2, thisJudgeRecordForReportBO.MarkSemester2);
                        //    sheetTemplateHK2.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //}
                        //else
                        //{
                        //    sheetTemplateHK2.SetFormulaValue(cell2, formular2);
                        //    sheetTemplateHK2.SetFormulaValue(cell, formular);
                        //}
                        sheetTemplateHK2.SetCellValue(cell2, thisJudgeRecordForReportBO.MarkSemester2);
                        sheetTemplateHK2.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //sheetTemplateHK2.SetFormulaValue(cell2, formular2);
                        //sheetTemplateHK2.SetFormulaValue(cell, formular);
                    }
                    else
                    {
                        string cell = "I" + index;
                        string formular = "=IF(COUNTIF(E" + index + ":H" + index + ";\"\")=4;\"\";IF(COUNTIF(E"
                            + index + ":H" + index + ";Ref!$A$2)<2;\"B\";IF(COUNTIF(E" + index + ":H" + index + ";Ref!$A$2)=4;\"A+\";\"A\")))";

                        string cell2 = "N" + index;
                        string formular2 = "=IF(COUNTIF(J" + index + ":M" + index + ";\"\")=4;\"\";IF(COUNTIF(E"
                          + index + ":M" + index + ";Ref!$A$2)<4;\"B\";IF(COUNTIF(E" + index + ":M" + index + ";Ref!$A$2)=8;\"A+\";\"A\")))";

                        if (thisJudgeRecordForReportBO.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING || thisJudgeRecordForReportBO.ExemptedSuject)
                        {
                            if (entity.EducationLevelID > 2)
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).FillColor(Color.Yellow);
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2BiggerThan3, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).IsLock = true;
                            }
                            else
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColum, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).FillColor(Color.Yellow);
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2LessThan3, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).IsLock = true;
                            }

                        }
                        else
                        {
                            if (entity.EducationLevelID > 2)
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2BiggerThan3, StartIndexFillFormular + i, lastColumnEduBiggerThan3HK2).IsLock = false;
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, firstColum + 4, StartIndexFillFormular + i, firstColumnMarkHK2BiggerThan3 - 1).FillColor(Color.Yellow);
                            }
                            else
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColumnMarkHK2LessThan3, StartIndexFillFormular + i, lastColumnEduLessThan3HK2).IsLock = false;
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, firstColum + 4, StartIndexFillFormular + i, firstColumnMarkHK2LessThan3 - 1).FillColor(Color.Yellow);
                            }
                        }

                        //if (!string.IsNullOrEmpty(thisJudgeRecordForReportBO.MarkSemester2))
                        //{
                        //    sheetTemplateHK2Lop12.SetCellValue(cell2, thisJudgeRecordForReportBO.MarkSemester2);
                        //    sheetTemplateHK2Lop12.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //}
                        //else
                        //{
                        //    sheetTemplateHK2Lop12.SetFormulaValue(cell, formular);
                        //    sheetTemplateHK2Lop12.SetFormulaValue(cell2, formular2);
                        //}
                        sheetTemplateHK2Lop12.SetCellValue(cell2, thisJudgeRecordForReportBO.MarkSemester2);
                        sheetTemplateHK2Lop12.SetCellValue(cell, thisJudgeRecordForReportBO.MarkSemester1);
                        //sheetTemplateHK2Lop12.SetFormulaValue(cell, formular);
                        //sheetTemplateHK2Lop12.SetFormulaValue(cell2, formular2);
                    }
                    {
                        if (entity.EducationLevelID > 2)
                        {
                            if (LockTitle.Contains("NX1") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 5, StartIndexFillFormular + i, 5).IsLock = true;
                            }
                            if (LockTitle.Contains("NX2") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 6, StartIndexFillFormular + i, 6).IsLock = true;
                            }
                            if (LockTitle.Contains("NX3") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 7, StartIndexFillFormular + i, 7).IsLock = true;
                            }
                            if (LockTitle.Contains("NX4") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 8, StartIndexFillFormular + i, 8).IsLock = true;
                            }
                            if (LockTitle.Contains("NX5") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 9, StartIndexFillFormular + i, 9).IsLock = true;
                            }
                            if (LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 10, StartIndexFillFormular + i, 10).IsLock = true;
                            }

                            if (LockTitle.Contains("NX6") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 11, StartIndexFillFormular + i, 11).IsLock = true;
                            }
                            if (LockTitle.Contains("NX7") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 12, StartIndexFillFormular + i, 12).IsLock = true;
                            }
                            if (LockTitle.Contains("NX8") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 13, StartIndexFillFormular + i, 13).IsLock = true;
                            }
                            if (LockTitle.Contains("NX9") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 14, StartIndexFillFormular + i, 14).IsLock = true;
                            }
                            if (LockTitle.Contains("NX10") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 15, StartIndexFillFormular + i, 15).IsLock = true;
                            }
                            if (LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2.GetRange(StartIndexFillFormular + i, 16, StartIndexFillFormular + i, 16).IsLock = true;
                            }
                        }
                        else
                        {
                            if (LockTitle.Contains("NX1") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 5, StartIndexFillFormular + i, 5).IsLock = true;
                            }
                            if (LockTitle.Contains("NX2") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 6, StartIndexFillFormular + i, 6).IsLock = true;
                            }
                            if (LockTitle.Contains("NX3") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 7, StartIndexFillFormular + i, 7).IsLock = true;
                            }
                            if (LockTitle.Contains("NX4") || LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 8, StartIndexFillFormular + i, 8).IsLock = true;
                            }

                            if (LockTitle.Contains("HKI,"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 9, StartIndexFillFormular + i, 9).IsLock = true;
                            }

                            if (LockTitle.Contains("NX5") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 10, StartIndexFillFormular + i, 10).IsLock = true;
                            }
                            if (LockTitle.Contains("NX6") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 11, StartIndexFillFormular + i, 11).IsLock = true;
                            }
                            if (LockTitle.Contains("NX7") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 12, StartIndexFillFormular + i, 12).IsLock = true;
                            }
                            if (LockTitle.Contains("NX8") || LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 13, StartIndexFillFormular + i, 13).IsLock = true;
                            }

                            if (LockTitle.Contains("HKII"))
                            {
                                sheetTemplateHK2Lop12.GetRange(StartIndexFillFormular + i, 14, StartIndexFillFormular + i, 14).IsLock = true;
                            }
                        }
                    }
                    QueenDic.Add(princeDic);
                }

                SupervisingDept sd = SupervisingDeptBusiness.Find(entity.SupervisingDeptID);
                SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
                string year = "Năm học {0} - {1}";
                AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
                SubjectCat sc = SubjectCatBusiness.Find(entity.SubjectID);
                ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);

                string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));
                string SubjectSemesterAndClass = "BẢNG ĐÁNH GIÁ HỌC LỰC MÔN {0} HỌC KỲ 2 LỚP {1}";
                string strSubjectSemesterAndClass = string.Format(SubjectSemesterAndClass, sc != null ? sc.SubjectName : "Tất cả", cp != null ? cp.DisplayName : "Tất cả");

                KingDic.Add("Rows", QueenDic);
                KingDic.Add("SupervisingDeptName", sd != null ? sd.SupervisingDeptName : "");
                KingDic.Add("SchoolName", sp.SchoolName);
                KingDic.Add("TitleYear", yearTitle);
                KingDic.Add("SubjectSemesterAndClass", strSubjectSemesterAndClass.ToUpper());

                templateRange.FillVariableValue(KingDic);

                if (entity.EducationLevelID > 2)
                {
                    sheetTemplateHK1.Delete();
                    sheetTemplateHK1Lop12.Delete();
                    sheetTemplateHK2Lop12.Delete();
                    for (var i = 9 + lsViewModel.Count; i <= lastColumnCheck; i++)
                    {
                        sheetTemplateHK2.DeleteRow(StartIndexFillFormular + lsViewModel.Count);
                    }
                    sheetTemplateHK2.Name = sheetName;
                    sheetTemplateHK2.ProtectSheet();
                }
                else
                {
                    sheetTemplateHK1.Delete();
                    sheetTemplateHK2.Delete();
                    sheetTemplateHK1Lop12.Delete();
                    for (var i = 9 + lsViewModel.Count; i <= lastColumnCheck; i++)
                    {
                        sheetTemplateHK2Lop12.DeleteRow(StartIndexFillFormular + lsViewModel.Count);
                    }
                    sheetTemplateHK2Lop12.Name = sheetName;
                    sheetTemplateHK2Lop12.ProtectSheet();
                }
            }
            sheet.Delete();
            return oBook.ToStream();
        }

        #endregion report

        #region Import Mark

        public void ImportMark(int UserID, List<PupilForImportBO> listPupilForImport, List<PupilForImportBO> listPupilForDelete)
        {
            if (listPupilForImport == null || listPupilForImport.Count == 0)
                return;

            //var listClassSubject = listPupilForImport.Where(u => u.SubjectID.HasValue).Select(u => new { ClassID = u.ClassID, SubjectID = u.SubjectID.Value }).Distinct();

            PupilForImportBO firstPupil = listPupilForImport.First();
            List<ClassSubject> listCS = ClassSubjectBusiness.SearchBySchool(firstPupil.SchoolID, new Dictionary<string, object> { { "AcademicYearID", firstPupil.AcademicYearID }, { "EducationLevelID", ClassProfileBusiness.Find(firstPupil.ClassID).EducationLevelID } }).ToList();

            List<MarkRecord> listMark = new List<MarkRecord>();
            List<JudgeRecord> listJudge = new List<JudgeRecord>();

            foreach (PupilForImportBO pupil in listPupilForImport)
            {
                ClassSubject classSubject = listCS.Where(u => u.SubjectID == pupil.SubjectID && u.ClassID == pupil.ClassID).FirstOrDefault();
                if (classSubject != null)
                {
                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (!string.IsNullOrEmpty(pupil.Judgement))
                        {
                            JudgeRecord jr = new JudgeRecord();
                            jr.AcademicYearID = pupil.AcademicYearID;
                            jr.ClassID = pupil.ClassID;
                            jr.Judgement = pupil.Judgement;
                            jr.MarkedDate = DateTime.Now;
                            jr.MarkTypeID = pupil.MarkTypeID.Value;
                            jr.OrderNumber = pupil.OrderNumber.Value;
                            jr.PupilID = pupil.PupilID;
                            jr.SchoolID = pupil.SchoolID;
                            jr.Semester = pupil.Semester;
                            jr.SubjectID = pupil.SubjectID.Value;
                            jr.Title = pupil.Title;
                            jr.CreatedAcademicYear = pupil.Year.Value;
                            listJudge.Add(jr);
                        }
                    }
                    else
                    {
                        if (pupil.Mark.HasValue)
                        {
                            MarkRecord mr = new MarkRecord();
                            mr.AcademicYearID = pupil.AcademicYearID;
                            mr.ClassID = pupil.ClassID;
                            mr.Mark = pupil.Mark.Value;
                            mr.MarkedDate = DateTime.Now;
                            mr.MarkTypeID = pupil.MarkTypeID.Value;
                            mr.OrderNumber = pupil.OrderNumber.Value;
                            mr.PupilID = pupil.PupilID;
                            mr.SchoolID = pupil.SchoolID;
                            mr.Semester = pupil.Semester;
                            mr.SubjectID = pupil.SubjectID.Value;
                            mr.Title = pupil.Title;
                            mr.CreatedAcademicYear = pupil.Year.Value;
                            listMark.Add(mr);
                        }
                    }
                }
            }

            List<MarkRecord> listMarkRecordDelete = new List<MarkRecord>();
            List<JudgeRecord> listJudgeRecordDelete = new List<JudgeRecord>();
            // Neu co HS bi xoa thi moi lay du lieu
            if (listPupilForDelete.Count > 0)
            {
                int classID = 0;
                if (!listPupilForDelete.Any(o => o.ClassID == firstPupil.ClassID))
                {
                    classID = firstPupil.ClassID;
                }
                // Lay du lieu diem
                IQueryable<MarkRecord> iqMarkRecord = MarkRecordBusiness.SearchBySchool(firstPupil.SchoolID, new Dictionary<string, object>(){
                                                                                                        {"Title",firstPupil.Title},
                                                                                                        {"AcademicYearID", firstPupil.AcademicYearID},
                                                                                                        {"Semester",firstPupil.Semester},
                                                                                                        {"classID", classID}});

                IQueryable<JudgeRecord> iqJudgeRecord = JudgeRecordBusiness.SearchBySchool(firstPupil.SchoolID, new Dictionary<string, object>(){
                                                                                                        {"Title",firstPupil.Title},
                                                                                                        {"Semester",firstPupil.Semester},
                                                                                                        {"AcademicYearID",firstPupil.AcademicYearID},
                                                                                                        {"classID", classID}});

                // Neu du lieu xoa khong nhieu va trong nhieu lop se chi lay diem cua cac hs bi xoa, duoi khoang 100 HS
                if (classID == 0 && listPupilForDelete.Count < 100)
                {
                    List<int> listPupilID = listPupilForDelete.Select(o => o.PupilID).Distinct().ToList();
                    iqMarkRecord = iqMarkRecord.Where(o => listPupilID.Contains(o.PupilID));
                    iqJudgeRecord = iqJudgeRecord.Where(o => listPupilID.Contains(o.PupilID));
                }
                // Lay ra du lieu
                List<MarkRecord> listMarkRecord = iqMarkRecord.ToList();
                List<JudgeRecord> listJudgeRecord = iqJudgeRecord.ToList();
                foreach (PupilForImportBO pupil in listPupilForDelete)
                {
                    ClassSubject classSubject = listCS.Where(u => u.SubjectID == pupil.SubjectID && u.ClassID == pupil.ClassID).FirstOrDefault();
                    if (classSubject != null)
                    {
                        if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            listJudgeRecordDelete.AddRange(listJudgeRecord.Where(u => u.PupilID == pupil.PupilID &&
                                                        u.ClassID == pupil.ClassID &&
                                                        u.SubjectID == pupil.SubjectID).ToList());
                        }
                        else
                        {
                            listMarkRecordDelete.AddRange(listMarkRecord.Where(u => u.PupilID == pupil.PupilID &&
                                                        u.ClassID == pupil.ClassID &&
                                                        u.SubjectID == pupil.SubjectID).ToList());
                        }
                    }
                }
            }

            #region Luu du lieu vao threadMark de chay tien trinh tinh diem TBM
            Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();
            // Mon tinh diem
            foreach (MarkRecord mark in listMarkRecordDelete)
            {
                string key = mark.ClassID + "_" + mark.SubjectID;
                if (dicPupilThreadMark.ContainsKey(key))
                {
                    List<int> listPupilID = dicPupilThreadMark[key];
                    if (!listPupilID.Contains(mark.PupilID))
                    {
                        listPupilID.Add(mark.PupilID);
                        dicPupilThreadMark[key] = listPupilID;
                    }
                }
                else
                {
                    dicPupilThreadMark[key] = new List<int>() { mark.PupilID };
                }
            }

            // Mon nhan xet
            foreach (JudgeRecord judge in listJudgeRecordDelete)
            {
                string key = judge.ClassID + "_" + judge.SubjectID;
                if (dicPupilThreadMark.ContainsKey(key))
                {
                    List<int> listPupilID = dicPupilThreadMark[key];
                    if (!listPupilID.Contains(judge.PupilID))
                    {
                        listPupilID.Add(judge.PupilID);
                        dicPupilThreadMark[key] = listPupilID;
                    }
                }
                else
                {
                    dicPupilThreadMark[key] = new List<int>() { judge.PupilID };
                }
            }

            #endregion

            if (listJudgeRecordDelete != null && listJudgeRecordDelete.Count() > 0)
            {
                JudgeRecordBusiness.DeleteAll(listJudgeRecordDelete);
            }
            if (listMarkRecordDelete != null && listMarkRecordDelete.Count() > 0)
            {
                MarkRecordBusiness.DeleteAll(listMarkRecordDelete);
            }
            MarkRecordBusiness.ImportMarkRecord(UserID, listMark);
            this.ImportJudgeRecord(UserID, listJudge);

        }

        public void ImportMarkHistory(int UserID, List<PupilForImportBO> listPupilForImport, List<PupilForImportBO> listPupilForDelete)
        {
            if (listPupilForImport == null || listPupilForImport.Count == 0) return;

            var listClassSubject = listPupilForImport.Where(u => u.SubjectID.HasValue).Select(u => new { ClassID = u.ClassID, SubjectID = u.SubjectID.Value }).Distinct();

            PupilForImportBO firstPupil = listPupilForImport.First();
            List<ClassSubject> listCS = ClassSubjectBusiness.SearchBySchool(firstPupil.SchoolID, new Dictionary<string, object> { { "AcademicYearID", firstPupil.AcademicYearID }, { "EducationLevelID", ClassProfileBusiness.Find(firstPupil.ClassID).EducationLevelID } }).ToList();

            List<MarkRecordHistory> listMarkHistory = new List<MarkRecordHistory>();
            List<JudgeRecordHistory> listJudgeHitory = new List<JudgeRecordHistory>();

            foreach (PupilForImportBO pupil in listPupilForImport)
            {
                ClassSubject classSubject = listCS.Where(u => u.SubjectID == pupil.SubjectID && u.ClassID == pupil.ClassID).FirstOrDefault();
                if (classSubject != null)
                {
                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (!string.IsNullOrEmpty(pupil.Judgement))
                        {
                            JudgeRecordHistory jr = new JudgeRecordHistory();
                            jr.AcademicYearID = pupil.AcademicYearID;
                            jr.ClassID = pupil.ClassID;
                            jr.Judgement = pupil.Judgement;
                            jr.MarkedDate = DateTime.Now;
                            jr.MarkTypeID = pupil.MarkTypeID.Value;
                            jr.OrderNumber = pupil.OrderNumber.Value;
                            jr.PupilID = pupil.PupilID;
                            jr.SchoolID = pupil.SchoolID;
                            jr.Semester = pupil.Semester;
                            jr.SubjectID = pupil.SubjectID.Value;
                            jr.Title = pupil.Title;
                            jr.CreatedAcademicYear = pupil.Year.Value;
                            listJudgeHitory.Add(jr);
                        }
                    }
                    else
                    {
                        if (pupil.Mark.HasValue)
                        {
                            MarkRecordHistory mr = new MarkRecordHistory();
                            mr.AcademicYearID = pupil.AcademicYearID;
                            mr.ClassID = pupil.ClassID;
                            mr.Mark = pupil.Mark.Value;
                            mr.MarkedDate = DateTime.Now;
                            mr.MarkTypeID = pupil.MarkTypeID.Value;
                            mr.OrderNumber = pupil.OrderNumber.Value;
                            mr.PupilID = pupil.PupilID;
                            mr.SchoolID = pupil.SchoolID;
                            mr.Semester = pupil.Semester;
                            mr.SubjectID = pupil.SubjectID.Value;
                            mr.Title = pupil.Title;
                            mr.CreatedAcademicYear = pupil.Year.Value;
                            listMarkHistory.Add(mr);
                        }
                    }
                }
            }

            List<MarkRecordHistory> listMarkRecordHisDelete = new List<MarkRecordHistory>();
            List<JudgeRecordHistory> listJudgeRecordHisDelete = new List<JudgeRecordHistory>();
            // Neu co HS bi xoa thi moi lay du lieu
            if (listPupilForDelete.Count > 0)
            {
                int classID = 0;
                if (!listPupilForDelete.Any(o => o.ClassID == firstPupil.ClassID))
                {
                    classID = firstPupil.ClassID;
                }
                // Lay du lieu diem
                IQueryable<MarkRecordHistory> iqMarkRecordHis = MarkRecordHistoryBusiness.SearchMarkRecordHistory(new Dictionary<string, object>(){
                                                                                                        {"SchoolID", firstPupil.SchoolID},
                                                                                                        {"Title",firstPupil.Title},
                                                                                                        {"AcademicYearID", firstPupil.AcademicYearID},
                                                                                                        {"Semester",firstPupil.Semester},
                                                                                                        {"classID", classID}});

                IQueryable<JudgeRecordHistory> iqJudgeRecordHis = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(new Dictionary<string, object>(){
                                                                                                        {"SchoolID", firstPupil.SchoolID},
                                                                                                        {"Title",firstPupil.Title},
                                                                                                        {"Semester",firstPupil.Semester},
                                                                                                        {"AcademicYearID",firstPupil.AcademicYearID},
                                                                                                        {"classID", classID}});

                // Neu du lieu xoa khong nhieu va trong nhieu lop se chi lay diem cua cac hs bi xoa, duoi khoang 100 HS
                if (classID == 0 && listPupilForDelete.Count < 100)
                {
                    List<int> listPupilID = listPupilForDelete.Select(o => o.PupilID).Distinct().ToList();
                    iqMarkRecordHis = iqMarkRecordHis.Where(o => listPupilID.Contains(o.PupilID));
                    iqJudgeRecordHis = iqJudgeRecordHis.Where(o => listPupilID.Contains(o.PupilID));
                }
                // Lay ra du lieu
                List<MarkRecordHistory> listMarkRecord = iqMarkRecordHis.ToList();
                List<JudgeRecordHistory> listJudgeRecord = iqJudgeRecordHis.ToList();
                foreach (PupilForImportBO pupil in listPupilForDelete)
                {
                    ClassSubject classSubject = listCS.Where(u => u.SubjectID == pupil.SubjectID && u.ClassID == pupil.ClassID).FirstOrDefault();
                    if (classSubject != null)
                    {
                        if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            listJudgeRecordHisDelete.AddRange(listJudgeRecord.Where(u => u.PupilID == pupil.PupilID &&
                                                        u.ClassID == pupil.ClassID &&
                                                        u.SubjectID == pupil.SubjectID).ToList());
                        }
                        else
                        {
                            listMarkRecordHisDelete.AddRange(listMarkRecord.Where(u => u.PupilID == pupil.PupilID &&
                                                        u.ClassID == pupil.ClassID &&
                                                        u.SubjectID == pupil.SubjectID).ToList());
                        }
                    }
                }
            }


            if (listJudgeRecordHisDelete != null && listJudgeRecordHisDelete.Count() > 0)
            {
                JudgeRecordHistoryBusiness.DeleteAll(listJudgeRecordHisDelete);
            }
            if (listMarkRecordHisDelete != null && listMarkRecordHisDelete.Count() > 0)
            {
                MarkRecordHistoryBusiness.DeleteAll(listMarkRecordHisDelete);
            }

            MarkRecordHistoryBusiness.ImportMarkRecord(UserID, listMarkHistory);
            JudgeRecordHistoryBusiness.ImportJudgeRecordHistory(UserID, listJudgeHitory);
        }
        #endregion Import Mark

        public IQueryable<JudgeRecord> GetJudgeRecordToDelete(int AcademicYearID, int SchoolID, int SubjectID, int Semester, int ClassID, int? PeriodID)
        {
            string strMarkTitle = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
            IQueryable<JudgeRecord> querry = this.All.Where(o => o.SchoolID == SchoolID && o.SubjectID == SubjectID && o.Semester == Semester && o.ClassID == ClassID && strMarkTitle.Contains(o.Title));
            return querry;
        }

        public bool CheckSubjectForSemester(int SchoolID, int AcademicYearID, int ClassID, int Semester, int SubjectID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["SubjectID"] = SubjectID;
            ClassSubject classSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, SearchInfo).FirstOrDefault();
            if (classSubject != null)
            {
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    return classSubject.SectionPerWeekFirstSemester > 0 ? true : false;
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    return classSubject.SectionPerWeekSecondSemester > 0 ? true : false;
                }
            }
            return false;
        }

        /// <summary>
        /// QuangLM
        /// Cap nhat lai thong tin diem cu
        /// </summary>
        /// <param name="listJudgeForDelete"></param>
        /// <param name="listJudgeForInsert"></param>
        private void SetOldValue(List<JudgeRecord> listJudgeForDelete, List<JudgeRecord> listJudgeForInsert)
        {
            foreach (JudgeRecord itemDel in listJudgeForDelete)
            {
                foreach (JudgeRecord itemInsert in listJudgeForInsert)
                {
                    if (itemInsert.PupilID == itemDel.PupilID && itemInsert.OrderNumber == itemDel.OrderNumber)
                    {
                        // Neu diem khac
                        if (itemInsert.Judgement != itemDel.Judgement)
                        {
                            itemInsert.CreatedDate = itemDel.CreatedDate;
                            itemInsert.IsSMS = false;
                            itemInsert.ModifiedDate = DateTime.Now;
                            itemInsert.OldJudgement = itemDel.Judgement;
                            itemInsert.SynchronizeID = itemDel.SynchronizeID;
                        }
                        else
                        {
                            itemInsert.CreatedDate = itemDel.CreatedDate;
                            itemInsert.IsSMS = itemDel.IsSMS;
                            itemInsert.ModifiedDate = null;
                            itemInsert.OldJudgement = itemDel.OldJudgement;
                        }
                        break;
                    }
                }
            }
        }


        #region Webservice Calculate mark - AnhVD 20131223
        private string CalculateSummedUpJudge(List<JudgeRecord> lstJudge)
        {
            AcademicYear objAca = lstJudge.Select(o => o.AcademicYear).FirstOrDefault();
            // Mon nhan xet dat neu so con diem dat > 2/3 tong so con diem
            int sum = lstJudge.Where(o => o.Judgement != null && o.Judgement != String.Empty).Count();
            int total_D = lstJudge.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
            int total_CD = lstJudge.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
            if (sum > 0)
            {
                if (objAca.IsClassification.HasValue && objAca.IsClassification.Value)
                {
                    if (((decimal)total_D * 3 - (decimal)sum * 2) >= 0)
                    {
                        return SystemParamsInFile.JUDGE_MARK_D;
                    }
                    else
                    {
                        return SystemParamsInFile.JUDGE_MARK_CD;
                    }
                }
                else
                {
                    if (((decimal)total_D - Math.Round((decimal)sum * 2 / 3) >= 0))
                    {
                        return SystemParamsInFile.JUDGE_MARK_D;
                    }
                    else
                    {
                        return SystemParamsInFile.JUDGE_MARK_CD;
                    }
                }
            }
            else
            {
                return String.Empty;
            }
        }
        #endregion
        #region Webservice Calculate mark - Namta
        private string CalculateSummedUpJudgeSemester(List<JudgeRecord> lstJudge)
        {
            AcademicYear objAca = lstJudge.Select(o => o.AcademicYear).FirstOrDefault();
            //Neu khong co diem HK thi ""
            if (!lstJudge.Any(u => u.Title == "HK"))
            {
                return String.Empty;
            }
            else
            {
                //Neu Diem HK la CD return CD
                if (lstJudge.Where(u => u.Title == "HK").FirstOrDefault().Judgement == SystemParamsInFile.JUDGE_MARK_CD)
                {
                    return SystemParamsInFile.JUDGE_MARK_CD;
                }
                else
                {
                    // Mon nhan xet dat neu so con diem dat > 2/3 tong so con diem
                    int sum = lstJudge.Where(o => o.Judgement != null && o.Judgement != String.Empty).Count();
                    int total_D = lstJudge.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                    int total_CD = lstJudge.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                    if (sum > 0)
                    {
                        if (objAca.IsClassification.HasValue && objAca.IsClassification.Value)
                        {
                            if (((decimal)total_D * 3 - (decimal)sum * 2) >= 0)
                            {
                                return SystemParamsInFile.JUDGE_MARK_D;
                            }
                            else
                            {
                                return SystemParamsInFile.JUDGE_MARK_CD;
                            }
                        }
                        else
                        {
                            if (((decimal)total_D - Math.Round((decimal)sum * 2 / 3) >= 0))
                            {
                                return SystemParamsInFile.JUDGE_MARK_D;
                            }
                            else
                            {
                                return SystemParamsInFile.JUDGE_MARK_CD;
                            }
                        }
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
            }
        }
        #endregion
        /// <summary>
        /// Tu dong tien hanh tong ket diem 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        /// <returns>1:  </returns>
        public string AutoSummupRecord(int UserID, List<int> lstPupilID, int Semester, int? PeriodID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID)
        {
            //bien ket qua
            //0:Khong co ket qua,1:cap nhat thanh cong,2:co loi xay ra
            string Result = "0";
            lstPupilID = lstPupilID.Distinct().ToList();
            try
            {
                SetAutoDetectChangesEnabled(false);
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
                SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);
                // Lay danh sach mon hoc theo lop
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["AcademicYearID"] = AcademicYearID;
                dicClassSubject["SchoolID"] = SchoolID;
                dicClassSubject["Semester"] = Semester;
                dicClassSubject["ClassID"] = ClassID;
                IQueryable<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicClassSubject);
                List<int> lstSubjectID = lstClassSubject.Select(u => u.SubjectID).Distinct().ToList();
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();

                #region Validate

                #endregion
                ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();

                var lstPoc2 = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID } })
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               select new PupilOfClassBO
                               {
                                   PupilID = poc.PupilID,
                                   ClassID = pp.CurrentClassID,
                                   AcademicYearID = pp.CurrentAcademicYearID,
                                   Status = poc.Status
                               });

                List<PupilOfClassBO> lstPoc = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID } })
                                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   ClassID = pp.CurrentClassID,
                                                   AcademicYearID = pp.CurrentAcademicYearID,
                                                   Status = poc.Status
                                               }).ToList();

                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, Semester).ToList();
                List<ExemptedSubject> lstExemptedSemester1 = lstExempted;
                List<ExemptedSubject> lstExemptedSemester2 = lstExempted;
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    lstExemptedSemester2 = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Where(u => u.SubjectID == SubjectID).ToList();

                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    lstExemptedSemester1 = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Where(u => u.SubjectID == SubjectID).ToList();
                }
                // Lấy thông tin môn nghề
                ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).FirstOrDefault();

                using (TransactionScope scope = new TransactionScope())
                {
                    this.context.Configuration.AutoDetectChangesEnabled = false;
                    this.context.Configuration.ValidateOnSaveEnabled = false;
                    // Lay danh sach con diem moi nhat cua hoc sinh
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = AcademicYearID;
                    dic["SchoolID"] = SchoolID;
                    dic["SubjectID"] = SubjectID;
                    dic["Semester"] = Semester;
                    dic["ClassID"] = ClassID;
                    dic["checkWithClassMovement"] = "check";
                    IQueryable<JudgeRecord> iqJudgeRecord = this.SearchJudgeRecord(dic);
                    // Thong tin ve cac con diem cua cap hoc
                    int Grade = classProfile.EducationLevel.Grade;
                    List<MarkType> listJudgeType = MarkTypeBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", Grade } }).ToList();
                    SemeterDeclaration semeterDeclaration = SemeterDeclarationBusiness.Search(
                                                                                new Dictionary<string, object>() {
                                                                                            { "SchoolID", SchoolID },
                                                                                            { "AcademicYearID", AcademicYearID },
                                                                                            { "Semester", Semester }
                                                                                            }).FirstOrDefault();

                    if (PeriodID != null)
                    {
                        #region Neu nhap diem dot thi tinh lai Diem Hoc ky
                        // Diem hoc ky nen tim kiem khong bao gom dieu kien PeriodID
                        List<JudgeRecord> lstJudgeRecord = iqJudgeRecord.ToList();
                        Dictionary<string, object> dicSum = new Dictionary<string, object>();
                        dicSum["AcademicYearID"] = AcademicYearID;
                        dicSum["SchoolID"] = SchoolID;
                        dicSum["SubjectID"] = SubjectID;
                        dicSum["ClassID"] = ClassID;
                        dicSum["checkWithClassMovement"] = "check";
                        IEnumerable<SummedUpRecord> listSummedRecordDB = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, dicSum).Where(u => u.PeriodID == null).ToList();
                        int PupilID;
                        bool DeleteJudge = false;
                        string summedUpJudge = "";
                        SummedUpRecord summedRecord = null;
                        List<JudgeRecord> LstJudgeEachPupil = new List<JudgeRecord>();
                        for (int i = lstPupilID.Count - 1; i >= 0; i--)
                        {
                            PupilID = lstPupilID[i];
                            LstJudgeEachPupil = lstJudgeRecord.Where(o => o.PupilID == PupilID).ToList();

                            //diem tong ket ky cua hoc sinh
                            if (listSummedRecordDB != null && listSummedRecordDB.Count() > 0)
                            {
                                summedRecord = listSummedRecordDB.Where(o => o.PupilID == PupilID && o.Semester == Semester).FirstOrDefault();
                                //truong hop xoa mat diem hoc ky thi xoa diem tong ket
                                if ((summedRecord != null && !LstJudgeEachPupil.Any(o => o.Title == "HK")) || (summedRecord != null && LstJudgeEachPupil.Count() == 0))
                                {
                                    //xoa diem
                                    DeleteJudge = true;

                                }
                            }

                            if (LstJudgeEachPupil.Count > 0 || DeleteJudge)
                            {
                                if (LstJudgeEachPupil.Any(o => o.Title == "HK") || DeleteJudge) // chi tong ket khi co diem Hoc ky
                                {
                                    if (!DeleteJudge)
                                        summedUpJudge = CalculateSummedUpJudgeSemester(LstJudgeEachPupil);
                                    if (summedUpJudge != string.Empty || DeleteJudge)
                                    {
                                        summedRecord = listSummedRecordDB.Where(o => o.PupilID == PupilID && o.Semester == Semester).FirstOrDefault();
                                        summedRecord.SynchronizeID = 1;
                                        // Neu da tong ket thuc hien cap nhat lai diem tong ket (nhieu record????)
                                        if (summedRecord != null && !DeleteJudge)
                                        {
                                            summedRecord.JudgementResult = summedUpJudge;
                                            this.SummedUpRecordBusiness.Update(summedRecord);
                                        }
                                        else if (summedRecord != null && DeleteJudge)
                                        {
                                            summedRecord.JudgementResult = null;
                                            this.SummedUpRecordBusiness.Update(summedRecord);
                                        }
                                        else
                                        {
                                            // Neu chua tong ket thi them moi
                                            SummedUpRecord sur = new SummedUpRecord();
                                            sur.PupilID = PupilID;
                                            sur.ClassID = ClassID;
                                            sur.SchoolID = SchoolID;
                                            sur.AcademicYearID = AcademicYearID;
                                            sur.SubjectID = SubjectID;
                                            sur.IsCommenting = subjectCat.IsCommenting;
                                            sur.CreatedAcademicYear = academicYear.Year;
                                            sur.JudgementResult = summedUpJudge;
                                            sur.Semester = Semester;
                                            sur.SummedUpDate = DateTime.Now;
                                            sur.SynchronizeID = 1;
                                            this.SummedUpRecordBusiness.Insert(sur);
                                            Result = "1";
                                        }

                                        #region Hoc ky 1
                                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                        {
                                            //luu diem TBCN neu chi hoc o HK1 
                                            if (classSubject.SectionPerWeekSecondSemester == 0 || lstExemptedSemester2.Any(u => u.PupilID == PupilID && u.SubjectID == SubjectID))
                                            {
                                                // Kiem tra ton tai tong ket nam hay chua
                                                summedRecord = listSummedRecordDB.Where(o => o.PupilID == PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                                                summedRecord.SynchronizeID = 1;
                                                if (summedRecord != null && !DeleteJudge)
                                                {
                                                    summedRecord.JudgementResult = summedUpJudge;
                                                    this.SummedUpRecordBusiness.Update(summedRecord);
                                                }
                                                else if (summedRecord != null && DeleteJudge)
                                                {
                                                    summedRecord.JudgementResult = null;
                                                    this.SummedUpRecordBusiness.Update(summedRecord);
                                                }
                                                else
                                                {
                                                    // Thuc hien tong ket nam
                                                    SummedUpRecord surAll = new SummedUpRecord()
                                                    {
                                                        PupilID = PupilID,
                                                        ClassID = ClassID,
                                                        SchoolID = SchoolID,
                                                        AcademicYearID = AcademicYearID,
                                                        SubjectID = SubjectID,
                                                        IsCommenting = subjectCat.IsCommenting,
                                                        CreatedAcademicYear = academicYear.Year,
                                                        JudgementResult = summedUpJudge,
                                                        Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL,
                                                        SummedUpDate = DateTime.Now,
                                                        SynchronizeID = 1
                                                    };
                                                    this.SummedUpRecordBusiness.Insert(surAll);
                                                    Result = "1";
                                                }
                                            }

                                            /* Nếu môn học là môn nghề và học kỳ là 1 và thời gian học chỉ học trong kỳ 1 thì
                                       lấy điểm học kỳ 1 làm điểm cả năm*/

                                            if (apprenticeshipClassSubject != null)
                                            {
                                                SummedUpRecord sur = new SummedUpRecord();
                                                sur.PupilID = PupilID;
                                                sur.ClassID = ClassID;
                                                sur.SchoolID = SchoolID;
                                                sur.AcademicYearID = AcademicYearID;
                                                sur.SubjectID = SubjectID;
                                                sur.IsCommenting = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat.IsCommenting;
                                                sur.CreatedAcademicYear = academicYear.Year;
                                                sur.JudgementResult = summedUpJudge;
                                                sur.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                                sur.SummedUpDate = DateTime.Now;
                                                sur.SynchronizeID = 1;
                                                this.SummedUpRecordBusiness.Insert(sur);
                                                Result = "1";
                                            }
                                        }
                                        #endregion

                                        #region Hoc ky 2
                                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester != 0)
                                        {
                                            #region Tinh diem trung binh ca nam
                                            // Lay diem hoc ky 1
                                            string judgement = CalculateSummedUpJudgeSemester(LstJudgeEachPupil);
                                            summedRecord = listSummedRecordDB.Where(o => o.PupilID == PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                                            summedRecord.SynchronizeID = 1;
                                            if (summedRecord != null && !DeleteJudge)
                                            {
                                                summedRecord.JudgementResult = judgement;
                                                this.SummedUpRecordBusiness.Update(summedRecord);
                                                Result = "1";
                                            }
                                            else if (summedRecord != null && DeleteJudge)
                                            {
                                                summedRecord.JudgementResult = null;
                                                this.SummedUpRecordBusiness.Update(summedRecord);
                                                Result = "1";
                                            }
                                            else
                                            {
                                                // Neu chua tong ket thi them moi
                                                SummedUpRecord sur = new SummedUpRecord();
                                                sur.PupilID = PupilID;
                                                sur.ClassID = ClassID;
                                                sur.SchoolID = SchoolID;
                                                sur.AcademicYearID = AcademicYearID;
                                                sur.SubjectID = SubjectID;
                                                sur.IsCommenting = subjectCat.IsCommenting;
                                                sur.CreatedAcademicYear = academicYear.Year;
                                                sur.JudgementResult = judgement;
                                                sur.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                                sur.SummedUpDate = DateTime.Now;
                                                sur.SynchronizeID = 1;
                                                this.SummedUpRecordBusiness.Insert(sur);
                                                Result = "1";
                                            }
                                            #endregion

                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                        #endregion

                    }
                    else
                    {
                        #region Neu nhap diem hoc ky thi tinh lai diem cac dot trong ky
                        List<JudgeRecord> lstJudgeRecord = iqJudgeRecord.ToList();
                        // Tim thong tin tong ket dot
                        IEnumerable<SummedUpRecord> listSummedRecordDB = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => o.PeriodID != null).ToList();
                        // Lay thong tin cac dot
                        List<PeriodDeclaration> lstPeriodInSemester = PeriodDeclarationBusiness.SearchBySemester(dic).ToList();
                        int PupilID;
                        string summedUpJudge = "";
                        SummedUpRecord summedRecord;
                        List<JudgeRecord> LstJudgeEachPupil = new List<JudgeRecord>();
                        for (int i = lstPupilID.Count - 1; i >= 0; i--)
                        {
                            PupilID = lstPupilID[i];
                            LstJudgeEachPupil = lstJudgeRecord.Where(o => o.PupilID == PupilID).ToList();
                            //if (LstJudgeEachPupil.Count > 0)
                            {


                                foreach (PeriodDeclaration period in lstPeriodInSemester)
                                {
                                    //Lay cac con diem cua dot
                                    string titleForPeriod = MarkRecordBusiness.GetMarkTitle(period.PeriodDeclarationID);

                                    //lay cac con diem cua dot
                                    var LstJudgeEachPupilPeriod = lstJudgeRecord.Where(o => o.PupilID == PupilID && titleForPeriod.Contains(o.Title)).ToList();
                                    var ListsummedRecord = listSummedRecordDB.Where(o => o.PupilID == PupilID && o.Semester == Semester
                                           && o.PeriodID == period.PeriodDeclarationID);
                                    //Neu khong co diem xoa tong ket
                                    if (LstJudgeEachPupilPeriod == null || (LstJudgeEachPupilPeriod != null && LstJudgeEachPupilPeriod.Count() == 0))
                                    {
                                        // Neu da tong ket thuc hien cap nhat lai diem tong ket (nhieu record????)
                                        if (ListsummedRecord != null && ListsummedRecord.Count() > 0)
                                        {
                                            summedRecord = ListsummedRecord.FirstOrDefault();
                                            summedRecord.JudgementResult = null;
                                            summedRecord.SynchronizeID = 1;
                                            this.SummedUpRecordBusiness.Update(summedRecord);
                                            Result = "1";
                                        }
                                    }
                                    else
                                    {

                                        summedUpJudge = CalculateSummedUpJudge(LstJudgeEachPupilPeriod);
                                        if (summedUpJudge != string.Empty)
                                        {

                                            // Neu da tong ket thuc hien cap nhat lai diem tong ket (nhieu record????)
                                            if (ListsummedRecord != null && ListsummedRecord.Count() > 0)
                                            {
                                                summedRecord = ListsummedRecord.FirstOrDefault();
                                                summedRecord.JudgementResult = summedUpJudge;
                                                summedRecord.SynchronizeID = 1;
                                                this.SummedUpRecordBusiness.Update(summedRecord);
                                                Result = "1";
                                            }
                                            else
                                            {
                                                // Neu chua tong ket thi them moi
                                                SummedUpRecord sur = new SummedUpRecord();
                                                sur.PupilID = PupilID;
                                                sur.ClassID = ClassID;
                                                sur.SchoolID = SchoolID;
                                                sur.AcademicYearID = AcademicYearID;
                                                sur.PeriodID = period.PeriodDeclarationID;
                                                sur.SubjectID = SubjectID;
                                                sur.IsCommenting = subjectCat.IsCommenting;
                                                sur.CreatedAcademicYear = academicYear.Year;
                                                sur.JudgementResult = summedUpJudge;
                                                sur.Semester = Semester;
                                                sur.SummedUpDate = DateTime.Now;
                                                sur.SynchronizeID = 1;
                                                this.SummedUpRecordBusiness.Insert(sur);
                                                Result = "1";
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        #endregion
                    }

                    scope.Complete();
                } // End scop 

                //luu vao db
                this.SummedUpRecordBusiness.Save();
                Dictionary<string, object> dicnull = new Dictionary<string, object>();
                dicnull["checkWithClassMovement"] = "check";
                dicnull["AcademicYearID"] = AcademicYearID;
                dicnull["SchoolID"] = SchoolID;
                dicnull["ClassID"] = ClassID;
                dicnull["SubjectID"] = SubjectID;
                IEnumerable<SummedUpRecord> listSummedRecordDBnull = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, dicnull).Where(u => u.SummedUpMark == null && u.JudgementResult == null).ToList();
                if (listSummedRecordDBnull != null && listSummedRecordDBnull.Count() > 0)
                {
                    this.SummedUpRecordBusiness.DeleteAll(listSummedRecordDBnull.ToList());
                    this.SummedUpRecordBusiness.Save();
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result = "-1";
                
                string paramList = string.Format("UserID={0}, Semester={1}, PeriodID={2}, SchoolID-{3}, AcademicYearID-{4}, ClassID={5}, SubjectID={6}",
                                               UserID, Semester, PeriodID, SchoolID, AcademicYearID, ClassID, SubjectID
                                                );
                LogExtensions.ErrorExt(logger, DateTime.Now, "AutoSummupRecord", paramList, ex);
                return Result;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }

        }

        public void ImportFromInputMark(List<Object> insertList, List<Object> updateList)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                List<ThreadMark> lstThreadMark = new List<ThreadMark>();
                for (int i = 0; i < insertList.Count; i++)
                {
                    Object obj = insertList[i];
                    if (obj.GetType() != typeof(JudgeRecord))
                    {
                        continue;
                    }

                    JudgeRecord entity = (JudgeRecord)obj;
                    JudgeRecordBusiness.Insert(entity);

                    ThreadMark tm = new ThreadMark();
                    tm.SchoolID = entity.SchoolID;
                    tm.ClassID = entity.ClassID;
                    tm.AcademicYearID = entity.AcademicYearID;
                    tm.SubjectID = entity.SubjectID;
                    tm.Semester = (short?)entity.Semester;
                    tm.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    tm.PupilID = entity.PupilID;
                    tm.CreatedDate = DateTime.Now;
                    tm.Status = 2;
                    tm.NumberScan = 0;
                    tm.LastDigitSchoolID = UtilsBusiness.GetPartionId(entity.SchoolID);


                    lstThreadMark.Add(tm);

                }
                for (int i = 0; i < updateList.Count; i++)
                {

                    Object obj = updateList[i];
                    if (!obj.GetType().Name.Contains(typeof(JudgeRecord).Name))
                    {
                        continue;
                    }

                    JudgeRecord entity = (JudgeRecord)obj;
                    JudgeRecordBusiness.Update(entity);

                    ThreadMark tm = new ThreadMark();
                    tm.SchoolID = entity.SchoolID;
                    tm.ClassID = entity.ClassID;
                    tm.AcademicYearID = entity.AcademicYearID;
                    tm.SubjectID = entity.SubjectID;
                    tm.Semester = (short?)entity.Semester;
                    tm.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    tm.PupilID = entity.PupilID;
                    tm.CreatedDate = DateTime.Now;
                    tm.Status = 2;
                    tm.NumberScan = 0;
                    tm.LastDigitSchoolID = UtilsBusiness.GetPartionId(entity.SchoolID);


                    lstThreadMark.Add(tm);
                }

                JudgeRecordBusiness.Save();
                if (lstThreadMark != null && lstThreadMark.Count > 0)
                {
                    //Tinh TBM nhan xet
                    int classId = 0;
                    int subjectId = 0;
                    ThreadMark objThread = lstThreadMark.FirstOrDefault();
                    AcademicYear academicYear = AcademicYearBusiness.Find(objThread.AcademicYearID);
                    List<int> lstClass = lstThreadMark.Select(c => c.ClassID.Value).Distinct().ToList();


                    int partitionId = UtilsBusiness.GetPartionId(objThread.SchoolID ?? 0);
                    List<ClassSubject> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                          join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                          where cs.Last2digitNumberSchool == partitionId
                                                          && cp.AcademicYearID == objThread.AcademicYearID
                                                          && cp.IsActive == true
                                                          select cs).ToList();
                    for (int i = 0; i < lstClass.Count; i++)
                    {
                        classId = lstClass[i];
                        List<int> lstPupilId = lstThreadMark.Where(s => s.ClassID == classId).Select(s => s.PupilID ?? 0).Distinct().ToList();

                        List<int> lstSubject = lstThreadMark.Where(c => c.ClassID == classId).Select(c => c.SubjectID.Value).Distinct().ToList();
                        for (int j = 0; j < lstSubject.Count; j++)
                        {
                            subjectId = lstSubject[j];
                            ClassSubject classSubject = lstClassSubject.FirstOrDefault(s => s.ClassID == classId && s.SubjectID == subjectId);
                            this.SummedJudgeMark(lstPupilId, academicYear, (int)objThread.Semester, 1, classSubject);
                            this.SummedJudgeMark(lstPupilId, academicYear, (int)objThread.Semester, null, classSubject);

                        }
                    }
                    //ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);

                }
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        private IDictionary<string, object> SummedUpColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("SummedUpMark", "SUMMED_UP_MARK");
            columnMap.Add("JudgementResult", "JUDGEMENT_RESULT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("SummedUpDate", "SUMMED_UP_DATE");
            columnMap.Add("ReTestMark", "RE_TEST_MARK");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            return columnMap;
        }

        private IDictionary<string, object> JudgeColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Judgement", "JUDGEMENT");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldJudgement", "OLD_JUDGEMENT");
            columnMap.Add("MJudgement", "M_JUDGEMENT");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;
        }
        public string GetMarkSemesterTitle(int SchoolID, int AcademicYearID, int Semester)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["Semester"] = Semester;

            SemeterDeclaration semester = SemeterDeclarationBusiness.Search(dic).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            string strMarkTitle = "";
            if (semester.InterviewMark > 0)
            {
                for (int i = 0; i < semester.InterviewMark; i++)
                {
                    strMarkTitle += "M" + (1 + i).ToString() + ",";
                }
            }
            if (semester.WritingMark > 0)
            {
                for (int i = 0; i < semester.WritingMark; i++)
                {
                    strMarkTitle += "P" + (1 + i).ToString() + ",";
                }
            }
            if (semester.TwiceCoeffiecientMark > 0)
            {
                for (int i = 0; i < semester.TwiceCoeffiecientMark; i++)
                {
                    strMarkTitle += "V" + (1 + i).ToString() + ",";
                }
            }
            strMarkTitle += "HK,";
            return strMarkTitle;
        }

        public void DeleteJudgeRecordByListID(int academicYearID, int schoolID, int? isHistory, List<long> listJudgeRecordId)
        {
            if (listJudgeRecordId == null || listJudgeRecordId.Count == 0)
            {
                return;
            }
            List<List<long>> lstVal = UtilsBusiness.SplitListToMultipleList<long>(listJudgeRecordId, 100);
            if (lstVal == null || lstVal.Count == 0)
            {
                return;
            }
            List<long> lstJudgeDelete = null;
            String strJudgeDelete = "";
            for (int i = 0; i < lstVal.Count; i++)
            {
                lstJudgeDelete = lstVal[i];
                strJudgeDelete = string.Empty;
                if (lstJudgeDelete == null || lstJudgeDelete.Count == 0)
                {
                    continue;
                }
                strJudgeDelete = String.Join(",", lstJudgeDelete.ToArray());
                SpDeteleJudgeRecord(academicYearID, schoolID, isHistory.Value, strJudgeDelete);
            }

        }

        private void SpDeteleJudgeRecord(int academicYearID, int schoolID, int history, string strJudgeIdDelete)
        {
            OracleConnection conn = (OracleConnection)context.Database.Connection;
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            OracleCommand command = new OracleCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "SP_DELETE_JUDGE_RECORD_BY_ID",
                Connection = conn,
            };
            command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearID);
            command.Parameters.Add("P_SCHOOL_ID", schoolID);
            command.Parameters.Add("P_IS_HISTORY", history);
            command.Parameters.Add("P_STR_JUDGERECORD_ID_DEL", strJudgeIdDelete);
            command.ExecuteNonQuery();

        }


        /// <summary>
        /// Tinh diem TBM dot va hoc ky cho mon nhan xet. 
        /// Nếu nhập điểm theo đợt thì tính điểm HK
        /// Nếu nhập điểm theo kỳ thì tính điểm cho đợt
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="schoolId"></param>
        /// <param name="objAcademicYear"></param>
        /// <param name="semesterId"></param>
        /// <param name="PeriodID"></param>
        /// <param name="strMarkTitle"></param>
        /// <param name="classSubject"></param>
        public void SummedJudgeMark(List<int> lstPupilID, AcademicYear objAcademicYear, int semesterId, int? PeriodID, ClassSubject classSubject)
        {
            if (classSubject == null)
            {
                return;
            }
            //Neu la mon tinh diem thi bo qua
            if (classSubject.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
            {
                return;
            }
            if (lstPupilID == null || lstPupilID.Count == 0)
            {
                return;
            }

            String pupilIds = UtilsBusiness.JoinPupilId(lstPupilID);
            int partitionId = UtilsBusiness.GetPartionId(objAcademicYear.SchoolID);
            List<int> lstPupilIDIsMin = new List<int>();
            if (PeriodID.HasValue)
            {
                lstPupilIDIsMin = lstPupilID;
            }
            else
            {
                //Tinh ra so hoc sinh chua nhap du so con diem toi thieu khi duoc cau hinh
                bool isMinSemester = false;
                int MinM = 0;
                int MinP = 0;
                int MinV = 0;
                if (semesterId == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    isMinSemester = classSubject != null && ((classSubject.MMinFirstSemester.HasValue && classSubject.MMinFirstSemester.Value > 0)
                                       || classSubject.PMinFirstSemester.HasValue && classSubject.PMinFirstSemester.Value > 0
                                       || classSubject.VMinFirstSemester.HasValue && classSubject.VMinFirstSemester.Value > 0);
                    MinM = classSubject != null && classSubject.MMinFirstSemester.HasValue ? classSubject.MMinFirstSemester.Value : 0;
                    MinP = classSubject != null && classSubject.PMinFirstSemester.HasValue ? classSubject.PMinFirstSemester.Value : 0;
                    MinV = classSubject != null && classSubject.VMinFirstSemester.HasValue ? classSubject.VMinFirstSemester.Value : 0;
                }
                else
                {
                    isMinSemester = classSubject != null && ((classSubject.MMinSecondSemester.HasValue && classSubject.MMinSecondSemester.Value > 0)
                                       || classSubject.PMinSecondSemester.HasValue && classSubject.PMinSecondSemester.Value > 0
                                       || classSubject.VMinSecondSemester.HasValue && classSubject.VMinSecondSemester.Value > 0);
                    MinM = classSubject != null && classSubject.MMinSecondSemester.HasValue ? classSubject.MMinSecondSemester.Value : 0;
                    MinP = classSubject != null && classSubject.PMinSecondSemester.HasValue ? classSubject.PMinSecondSemester.Value : 0;
                    MinV = classSubject != null && classSubject.VMinSecondSemester.HasValue ? classSubject.VMinSecondSemester.Value : 0;
                }

                if (isMinSemester)
                {
                    var lstJudgeMarkRecord = (from mr in VJudgeRecordBusiness.All
                                              where mr.SchoolID == objAcademicYear.SchoolID
                                                 && mr.Last2digitNumberSchool == partitionId
                                                  && mr.AcademicYearID == objAcademicYear.AcademicYearID
                                                  && mr.ClassID == classSubject.ClassID
                                                  && mr.SubjectID == classSubject.SubjectID
                                                  && mr.Semester == semesterId
                                                  && lstPupilID.Contains(mr.PupilID)
                                                  && (mr.Title.Contains("M") || mr.Title.Contains("P") || mr.Title.Contains("V"))
                                              group mr by new
                                              {
                                                  mr.PupilID,
                                                  mr.Title,
                                                  mr.Judgement
                                              }
                                             into g
                                              select new
                                              {
                                                  pupilID = g.Key.PupilID,
                                                  Title = g.Key.Title,
                                                  Mark = g.Key.Judgement
                                              });
                    List<int> lsttmp = lstJudgeMarkRecord.Select(p => p.pupilID).Distinct().ToList();
                    int countM = 0;
                    int countP = 0;
                    int countV = 0;
                    int pupilIDtmp = 0;
                    for (int i = 0; i < lsttmp.Count; i++)
                    {
                        pupilIDtmp = lsttmp[i];
                        countM = lstJudgeMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("M")).Count();
                        countP = lstJudgeMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("P")).Count();
                        countV = lstJudgeMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("V")).Count();
                        if (countM >= MinM && countP >= MinP && countV >= MinV)
                        {
                            lstPupilIDIsMin.Add(pupilIDtmp);
                        }
                    }
                }
                else
                {
                    lstPupilIDIsMin = lstPupilID;
                }
            }

            string pupilIdIsMin = UtilsBusiness.JoinPupilId(lstPupilIDIsMin);


            #region Store tinh TBM

            SummedJudgeRecord(pupilIdIsMin, objAcademicYear, semesterId, PeriodID, classSubject);

            #endregion

        }

        private void SummedJudgeRecord(string pupilIdIs, AcademicYear objAcademicYear, int semesterId, int? PeriodID, ClassSubject classSubject)
        {

            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            int isClassfication = objAcademicYear.IsClassification.HasValue && objAcademicYear.IsClassification.Value ? 1 : 0;

            List<Task> taskMark = new List<Task>();
            //Neu nhap diem theo dot thi tinh diem TBM HK
            if (PeriodID.HasValue && PeriodID > 0)
            {
                string strMarkTitleSemester = MarkRecordBusiness.GetMarkSemesterTitle(objAcademicYear.SchoolID, objAcademicYear.AcademicYearID, semesterId);
                taskMark.Add(Task.Run(() =>
                {
                    #region Tinh TBM hoc kỳ
                    using (OracleConnection conn = new OracleConnection(connectionString))
                    {
                        try
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            OracleCommand command = new OracleCommand
                            {
                                CommandType = CommandType.StoredProcedure,
                                CommandText = (objAcademicYear.IsMovedHistory ?? 0) == 1 ? "PROC_CLASS_JUDGE_SUMMED_UP_HIS" : "PROC_CLASS_JUDGE_SUMMED_UP_NEW",
                                Connection = conn,
                            };

                            command.Parameters.Add("P_SCHOOL_ID", objAcademicYear.SchoolID);
                            command.Parameters.Add("P_ACADEMIC_YEAR_ID", objAcademicYear.AcademicYearID);
                            command.Parameters.Add("P_YEAR", objAcademicYear.Year);
                            command.Parameters.Add("P_CLASS_ID", classSubject.ClassID);
                            command.Parameters.Add("P_SUBJECT_ID", classSubject.SubjectID);
                            command.Parameters.Add("P_SEMESTER", semesterId);
                            command.Parameters.Add("P_IS_CLASSIFICATION", isClassfication);
                            command.Parameters.Add("P_MARK_TITLE", strMarkTitleSemester);
                            command.Parameters.Add("P_PUPIL_IDS", pupilIdIs);
                            command.Parameters.Add("P_D", GlobalConstants.SHORT_OK);
                            command.Parameters.Add("P_CD", GlobalConstants.SHORT_NOT_OK);
                            command.Parameters.Add("P_HK", GlobalConstants.MARK_TYPE_HK);
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            
                            string para = string.Format("Tinh TBM mon nhan xet hoc ky schoolId={0},academicYearId={1}, ClassID={2},semesterId={3}", objAcademicYear.SchoolID, objAcademicYear.AcademicYearID, classSubject.ClassID, semesterId);
                            LogExtensions.ErrorExt(logger, DateTime.Now, "SummedJudgeRecord", para, ex);
                        }
                        finally
                        {
                            if (conn != null)
                            {
                                conn.Close();
                            }
                        }

                    }
                    #endregion
                }));

            }
            else //Nhap diem hoc ky thi tinh diem cho dot
            {
                //1. Lay danh sach tat cac cac dot trong hoc ky
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = objAcademicYear.AcademicYearID;
                dic["Semester"] = semesterId;
                List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(objAcademicYear.SchoolID, dic).OrderBy(o => o.FromDate).ToList();
                if (lstPeriod != null && lstPeriod.Count > 0)
                {
                    foreach (PeriodDeclaration objPeriod in lstPeriod)
                    {
                        string strMarkTitlePeriod = MarkRecordBusiness.GetMarkTitle(objPeriod.PeriodDeclarationID);
                        taskMark.Add(Task.Run(() =>
                        {
                            #region Tinh TBM dot
                            using (OracleConnection conn = new OracleConnection(connectionString))
                            {
                                try
                                {
                                    if (conn.State != ConnectionState.Open)
                                    {
                                        conn.Open();
                                    }
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = (objAcademicYear.IsMovedHistory ?? 0) == 1 ? "PROC_CLASS_JUDGE_SUMMED_UP_P_H" : "PROC_CLASS_JUDGE_SUMMED_UP_P",
                                        Connection = conn
                                    };
                                    command.Parameters.Add("P_SCHOOL_ID", objAcademicYear.SchoolID);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", objAcademicYear.AcademicYearID);
                                    command.Parameters.Add("P_YEAR", objAcademicYear.Year);
                                    command.Parameters.Add("P_CLASS_ID", classSubject.ClassID);
                                    command.Parameters.Add("P_SUBJECT_ID", classSubject.SubjectID);
                                    command.Parameters.Add("P_SEMESTER", semesterId);
                                    command.Parameters.Add("P_PERIOD_ID", objPeriod.PeriodDeclarationID);
                                    command.Parameters.Add("P_IS_CLASSIFICATION", isClassfication);
                                    command.Parameters.Add("P_MARK_TITLE", strMarkTitlePeriod);
                                    command.Parameters.Add("P_PUPIL_IDS", pupilIdIs);
                                    command.Parameters.Add("P_D", GlobalConstants.SHORT_OK);
                                    command.Parameters.Add("P_CD", GlobalConstants.SHORT_NOT_OK);
                                    command.Parameters.Add("P_HK", GlobalConstants.MARK_TYPE_HK);
                                    command.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    
                                    string para = string.Format("Tinh TBM mon nhan xet theo dot schoolId={0},academicYearId={1}, ClassID={2},semesterId={3},PeriodID={4} ", objAcademicYear.SchoolID, objAcademicYear.AcademicYearID, classSubject.ClassID, semesterId, PeriodID);
                                    LogExtensions.ErrorExt(logger, DateTime.Now, "SummedJudgeRecord", para, ex);
                                }
                                finally
                                {
                                    if (conn != null)
                                    {
                                        conn.Close();
                                    }
                                }
                            }
                            #endregion
                        }));
                    }
                }

            }

            if (taskMark.Count > 0)
            {
                Task.WaitAll(taskMark.ToArray());
            }


        }



    }
}
