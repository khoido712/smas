/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ActivityTypeBusiness
    {
        public IQueryable<ActivityType> Search(IDictionary<string, object> dic)
        {
            int ActivityTypeID = Utils.GetInt(dic, "ActivityTypeID");
            string ActivityTypeName = Utils.GetString(dic, "ActivityTypeName");
            bool? IsActive = Utils.GetNullableBool(dic, "IsActive");
            IQueryable<ActivityType> Query = ActivityTypeRepository.All;
            if (ActivityTypeID != 0)
            {
                Query = Query.Where(evd => evd.ActivityTypeID == ActivityTypeID);
            }
            if (IsActive.HasValue)
            {
                Query = Query.Where(evd => evd.IsActive == IsActive);
            }
            if (ActivityTypeName.Trim().Length > 0)
            {
                Query = Query.Where(evd => evd.ActivityTypeName.ToLower().Contains(ActivityTypeName.ToLower()));
            }

            return Query;
        }
        
    }
}