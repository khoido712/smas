﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ParticularPupilCharacteristicBusiness
    {
        public ParticularPupilCharacteristic Insert(int UserID, ParticularPupilCharacteristic entityToInsert)
        {
            ValidationMetadata.ValidateObject(entityToInsert);

            if (entityToInsert.RealUpdatedDate > DateTime.Now)
                throw new BusinessException("ParticularPupilCharacteristic_Label_RealUpdateDateError");

            //Utils.ValidateNull(particularPupil, "ParticularPupilCharacteristic_Label_ParticularPupilID"); 
            return base.Insert(entityToInsert);
        }

        public ParticularPupilCharacteristic Update(int UserID, ParticularPupilCharacteristic entityToUpdate)
        {
            //Check primary key
            this.CheckAvailable(entityToUpdate.ParticularPupilCharacteristicID, "ParticularPupilCharacteristic_Label_ParticularPupilCharacteristicID");

            //Validate object's properties
            ValidationMetadata.ValidateObject(entityToUpdate);

            if (entityToUpdate.RealUpdatedDate > DateTime.Now)
                throw new BusinessException("ParticularPupilCharacteristic_Label_RealUpdateDateError");

            //Kiem tra hoc sinh ca biet
            //var particularPupil = this.ParticularPupilBusiness.Find(entityToUpdate.ParticularPupilID);
            //Utils.ValidateNull(particularPupil, "ParticularPupilCharacteristic_Label_ParticularPupilID");
            return base.Update(entityToUpdate);
        }

        public void Delete(int UserID, int ParticularPupilCharacteristicID, int SchoolID)
        {
            //Check primary key
            var particularPupilCharacteristic=this.Find(ParticularPupilCharacteristicID);

            Utils.ValidateNull(particularPupilCharacteristic,"ParticularPupilCharacteristic_Label_ParticularPupilCharacteristicID");

            //Kiem tra tuong thich PupilProfile
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile"
                                            , new Dictionary<string, object>()
                                            {
                                                {"PupilProfileID" , particularPupilCharacteristic.ParticularPupil.PupilID},
                                                {"CurrentSchoolID", SchoolID}
                                            }, null);
            if (!PupilProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (!this.AcademicYearBusiness.IsCurrentYear(particularPupilCharacteristic.ParticularPupil.AcademicYearID))
                throw new BusinessException("ParticularPupilTreatment_Validate_NotCurrentYear");

            //Kiem tra trang thai la dang hoc
            if (this.PupilOfClassBusiness.All.FirstOrDefault(u=>u.PupilID==particularPupilCharacteristic.ParticularPupil.PupilID 
                                                                && u.ClassID==particularPupilCharacteristic.ParticularPupil.ClassID
                                                                && u.Status == GlobalConstants.PUPIL_STATUS_STUDYING) == null)
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");

            //Kiem tra quyen cua nguoi dung
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, particularPupilCharacteristic.ParticularPupil.ClassID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            base.Delete(ParticularPupilCharacteristicID);
        }

        public IQueryable<ParticularPupilCharacteristic> Search(IDictionary<string, object> dic)
        {
            string FamilyCharacteristic = Utils.GetString(dic, "FamilyCharacteristic");
            string PsychologyCharacteristic = Utils.GetString(dic, "PsychologyCharacteristic");
            int? ParticularPupilID = Utils.GetNullableInt(dic, "ParticularPupilID");
            int? ParticularPupilCharacteristicID = Utils.GetNullableInt(dic, "ParticularPupilCharacteristicID");
            DateTime? RealUpdatedDate = Utils.GetDateTime(dic, "RealUpdatedDate");

            var lstParticularPupilCharacteristic = this.ParticularPupilCharacteristic.All;

            if (!string.IsNullOrEmpty(FamilyCharacteristic)) 
                lstParticularPupilCharacteristic = lstParticularPupilCharacteristic.Where(u => u.FamilyCharacteristic.Contains(FamilyCharacteristic));

            if (!string.IsNullOrEmpty(PsychologyCharacteristic))
                lstParticularPupilCharacteristic = lstParticularPupilCharacteristic.Where(u => u.PsychologyCharacteristic.Contains(PsychologyCharacteristic));

            if (ParticularPupilID.HasValue && ParticularPupilID.Value > 0)
                lstParticularPupilCharacteristic = lstParticularPupilCharacteristic.Where(u => u.ParticularPupilID == ParticularPupilID.Value);

            if (ParticularPupilCharacteristicID.HasValue && ParticularPupilCharacteristicID.Value > 0)
                lstParticularPupilCharacteristic = lstParticularPupilCharacteristic.Where(u => u.ParticularPupilCharacteristicID == ParticularPupilCharacteristicID.Value);

            if (RealUpdatedDate.HasValue)
                lstParticularPupilCharacteristic = lstParticularPupilCharacteristic.Where(u => u.RealUpdatedDate.Date == RealUpdatedDate.Value.Date);

            return lstParticularPupilCharacteristic;
        }

        public IQueryable<ParticularPupilCharacteristic> SearchByParticularPupil(int particularPupilID, IDictionary<string, object> searchInfo)
        {
            if (particularPupilID == 0) return null;
            else
            {
                searchInfo["ParticularPupilID"] = particularPupilID;
                return this.Search(searchInfo);
            }
        }
    }
}
