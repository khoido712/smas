﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    /// <summary>
    /// DiseasesTypeBusiness
    /// </summary>
    /// <author>
    /// dungnt77
    /// </author>
    /// <remarks>
    /// 28/12/2012   8:53 AM
    /// </remarks>
    public partial class DiseasesTypeBusiness
    {

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 28/12/2012   8:53 AM
        /// </remarks>
        public IQueryable<DiseasesType> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            IQueryable<DiseasesType> iqDT = DiseasesTypeRepository.All.Where(o => o.IsActive == true);
            if(Resolution.Trim().Length !=0)
            {
                iqDT = iqDT.Where(o => o.Resolution.ToUpper().Contains(Resolution.ToUpper()));
            }
            if (Description.Trim().Length != 0)
            {
                iqDT = iqDT.Where(o => o.Description.ToUpper().Contains(Description.ToUpper()));
            }
            return iqDT;
        }
    }
}