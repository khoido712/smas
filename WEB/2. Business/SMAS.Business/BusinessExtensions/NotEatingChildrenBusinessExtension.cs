﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class NotEatingChildrenBusiness
    {

        #region CountNotEatingChildren
        /// <summary>
        /// CountNotEatingChildren
        /// </summary>
        /// <param name="PupilID">The pupil ID.</param>
        /// <param name="Month">The month.</param>
        /// <param name="Year">The year.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <returns>
        /// int
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/12/2012</date>
        public int CountNotEatingChildren(int PupilID, int Month, int Year, int SchoolID, int AcademicYearID)
        {
            int daysInMonth = DateTime.DaysInMonth(Year, Month);
            IQueryable<NotEatingChildren> Query = this.repository.All.Where(o => o.AcademicYearID == AcademicYearID)
                .Where(o => o.SchoolID == SchoolID)
                .Where(o => o.PupilID == PupilID)
                .Where(o => o.NotEatingDate.Year == Year)
                .Where(o => o.NotEatingDate.Month == Month);
            return daysInMonth - Query.Count();
        }


        #endregion

       
        #region GetListChildren

        /// <summary>
        /// GetListChildren
        /// </summary>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <param name="PupilCode">The pupil code.</param>
        /// <param name="FullName">The full name.</param>
        /// <param name="Month">The month.</param>
        /// <param name="Year">The year.</param>
        /// <returns>
        /// List{MoneyPerChildren}
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/12/2012</date>
        public List<MoneyPerChildren> GetListChildren(int AcademicYearID, int SchoolID, int EducationLevelID,
            int ClassID, string PupilCode, string FullName, int Month, int Year)
        {

            IDictionary<string, object> searchInfo = new Dictionary<string, object>();
            searchInfo["AcademicYearID"] = AcademicYearID;
            searchInfo["PupilCode"] = PupilCode;
            searchInfo["FullName"] = FullName;
            searchInfo["EducationLevelID"] = EducationLevelID;
            searchInfo["CurrentClassID"] = ClassID;
            List<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(SchoolID, searchInfo).ToList();
            List<MoneyPerChildren> ListMoneyPerChildren = new List<MoneyPerChildren>();
            foreach (PupilProfileBO PupilProfileBO in lstPupil)
            {
                MoneyPerChildren MoneyPerChildren = new MoneyPerChildren();
                MoneyPerChildren.PupilID = PupilProfileBO.PupilProfileID;
                MoneyPerChildren.PupilCode = PupilProfileBO.PupilCode;
                MoneyPerChildren.FullName = PupilProfileBO.FullName;
                MoneyPerChildren.BirthDate = PupilProfileBO.BirthDate;
                MoneyPerChildren.CurrentClassID = PupilProfileBO.CurrentClassID;
                MoneyPerChildren.CurrentClassName = PupilProfileBO.CurrentClassName;
                MoneyPerChildren.Date = Month + "/" + Year;
                MoneyPerChildren.Genre = PupilProfileBO.Genre;
                MoneyPerChildren.NumberDayOfMonth = this.CountNotEatingChildren(PupilProfileBO.PupilProfileID, Month, Year, SchoolID, AcademicYearID);
                MoneyPerChildren.DailyDishCost = DailyDishCostBusiness.MaxDailyDishCost(new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)), SchoolID);
                MoneyPerChildren.MoneyPerMonth = MoneyPerChildren.NumberDayOfMonth * MoneyPerChildren.DailyDishCost;
                ListMoneyPerChildren.Add(MoneyPerChildren);
            }
            return ListMoneyPerChildren;
        }

        #endregion
        

        #region Insert
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="PupilID">The pupil ID.</param>
        /// <param name="Month">The month.</param>
        /// <param name="Year">The year.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <returns>
        /// </returns>
        /// <author>tungnd</author>
        /// <date>11/15/2012</date>

        public void Insert(int SchoolID, int AcademicYearID, int ClassID, DateTime FromDate, DateTime ToDate, List<NotEatingChildren> lstNotEatingChildren)
        {

            IQueryable<NotEatingChildren> lstNotEatingChidren = this.NotEatingChildrenRepository.All.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID
                                           && o.ClassID == ClassID && FromDate <= o.NotEatingDate && o.NotEatingDate <= ToDate);
            foreach (var itemlstNotEating in lstNotEatingChidren)
            {
                base.Delete(itemlstNotEating.NotEatingChildrenID);

            }


            foreach (var item in lstNotEatingChildren)
            {
                //PupilID, ClassID: not compatible(PupilProfile)
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["PupilProfileID"] = item.PupilID;
                SearchInfo["CurrentClassID"] = item.ClassID;

                bool SearchInfoExist = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", SearchInfo, null);

                if (!SearchInfoExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                if (FromDate > item.NotEatingDate || ToDate < item.NotEatingDate)
                {
                    throw new BusinessException("NotEatingChildren_Validate_Date");
                }

                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["PupilID"] = item.PupilID;
                Dictionary["ClassID"] = item.ClassID;

                IQueryable<PupilOfClass> pupilofclass = this.PupilOfClassBusiness.SearchBySchool(SchoolID, Dictionary);

                if (pupilofclass.FirstOrDefault().Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("Common_Validate_NotStudy");
                }

                

                NotEatingChildren noteatingchildren = new NotEatingChildren();

                noteatingchildren.SchoolID = SchoolID;
                noteatingchildren.PupilID = item.PupilID;
                noteatingchildren.ClassID = item.ClassID;
                noteatingchildren.AcademicYearID = AcademicYearID;
                noteatingchildren.NotEatingDate = item.NotEatingDate;

                base.Insert(noteatingchildren);
            }

        }


        #endregion
        
        #region  Search


        private IQueryable<NotEatingChildren> Search(IDictionary<string, object> SearchNotEatingChildren)
        {
            int SchoolID = Utils.GetInt(SearchNotEatingChildren, "SchoolID");
            int PupilID = Utils.GetInt(SearchNotEatingChildren, "PupilID");
            int AcademicYearID = Utils.GetInt(SearchNotEatingChildren, "AcademicYearID");
            int ClassID = Utils.GetInt(SearchNotEatingChildren, "ClassID");
            string PupilCode = Utils.GetString(SearchNotEatingChildren, "PupilCode");
            string FullName = Utils.GetString(SearchNotEatingChildren, "FullName");
            DateTime? FromDate = Utils.GetDateTime(SearchNotEatingChildren, "FromDate");
            DateTime? NotEatingDate = Utils.GetDateTime(SearchNotEatingChildren, "NotEatingDate");
            DateTime? ToDate = Utils.GetDateTime(SearchNotEatingChildren, "ToDate");

            IQueryable<NotEatingChildren> query = this.NotEatingChildrenRepository.All;

            if (PupilID != 0)
            {
                query = query.Where(o => o.PupilID == PupilID);
            }

            if (PupilCode.Length != 0)
            {
                query = query.Where(o => o.PupilProfile.PupilCode == PupilCode);
            }

            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (FullName.Length != 0)
            {
                query = query.Where(o => o.PupilProfile.FullName == FullName);
            }
            if (FromDate != null && ToDate != null)
            {
                query = query.Where(o => o.NotEatingDate >= FromDate && o.NotEatingDate <= ToDate);
            }

            if (NotEatingDate != null)
                query = query.Where(o => o.NotEatingDate == NotEatingDate.Value);

            if (ClassID != 0)
            {
                query = query.Where(o => o.ClassID == ClassID);
            }

            return query;
        }

        #endregion

        #region SearchBySchool
        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>11/15/2012</date>

        public IQueryable<NotEatingChildren> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);

            }

        }


        #endregion
    }
}