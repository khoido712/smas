/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  Namta
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class DishDetailBusiness
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="listDishDetail">The list dish detail.</param>
        ///<author>Nam ta</author>
        ///<Date>11/16/2012</Date>
        /// <exception cref="BusinessException">DishDetail_Validate_WeightPerRation</exception>
        public void Insert(List<DishDetail> listDishDetail)
        {
            if (listDishDetail != null && listDishDetail.Count > 0)
            {
                DishDetail DishDetail = listDishDetail.FirstOrDefault();
                int DishCatId = DishDetail.DishID;
                IQueryable<DishDetail> listRemove = this.All.Where(o => o.DishID == DishCatId);

                if (listRemove != null && listRemove.Count() > 0)
                {
                    this.DeleteAll(listRemove.ToList());
                }

                foreach (DishDetail entity in listDishDetail)
                {
                    if (entity.WeightPerRation <= 0)
                    {
                        throw new BusinessException("DishDetail_Validate_WeightPerRation");
                    }
                    base.Insert(entity);
                }
            }
        }

        public IQueryable<DishDetail> Search(IDictionary<string, object> SearchInfo)
        {

            var query = this.All;

            int DishDetailID = Utils.GetInt(SearchInfo, "DishDetailID");
            int DishID = Utils.GetInt(SearchInfo, "DishID");
            int FoodID = Utils.GetInt(SearchInfo, "FoodID");

            if (DishDetailID > 0)
                query = query.Where(o => o.DishDetailID == DishDetailID);
            if (DishID > 0)
                query = query.Where(o => o.DishID == DishID);
            if (FoodID > 0)
                query = query.Where(o => o.FoodID == FoodID);
            return query;
        }

        public IQueryable<DishDetailBO> GetListFoodOfDailyMenu(int DailyMenuID)
        {
            //Select DishID from DailyMenuDetail dmd where dmd.DailyMenuID =  DailyMenuID
            //=> List<int> lstDish

            List<int> lstDish = new DailyMenuDetailRepository(this.context).All.Where(o => o.DishID.HasValue && o.DailyMenuID == DailyMenuID).Select(o => o.DishID.Value).ToList();
            //Select dd.*, f.Price  from DishDetail dd join FoodCat f on dd.FoodID = f.FoodID
            //             Where dd.DishID in lstDish
            var querry = from dd in this.All.Where(o => lstDish.Contains(o.DishID))
                         join fc in FoodCatRepository.All on dd.FoodID equals fc.FoodID into tb
                         select new DishDetailBO
                         {
                             FoodID = dd.FoodID,
                             DishID = dd.DishID,
                             WeightPerRation = dd.WeightPerRation
                         };

            return querry;
        }
        #region SearchBySchool
        public IQueryable<DishDetail> SearchBySchool(IDictionary<string, object> dic)
        {
           return this.Search(dic);
        }
        #endregion

    }
}