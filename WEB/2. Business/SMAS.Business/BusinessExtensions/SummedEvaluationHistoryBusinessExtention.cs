﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;


namespace SMAS.Business.Business
{
    public partial class SummedEvaluationHistoryBusiness
    {
        #region Search
        public IQueryable<SummedEvaluationBO> Search(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partition = UtilsBusiness.GetPartionId(schoolID);
            int semester = Utils.GetInt(dic, "Semester");
            int evaluationID = Utils.GetInt(dic, "EvaluationID");

            IQueryable<SummedEvaluationBO> query = from seh in SummedEvaluationHistoryRepository.All
                                                   join pp in PupilProfileRepository.All on seh.PupilID equals pp.PupilProfileID
                                                   join cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && o.SchoolID == schoolID) on seh.ClassID equals cp.ClassProfileID
                                                   where seh.AcademicYearID == academicYearID && seh.LastDigitSchoolID == partition && seh.SchoolID == schoolID
                                                   && cp.IsActive == true
                                                   select new SummedEvaluationBO
                                                   {
                                                       EndingEvaluation = seh.EndingEvaluation,
                                                       Genre = pp.Genre,
                                                       EthnicID = pp.EthnicID,
                                                       IsCombinedClass = cp.IsCombinedClass,
                                                       IsDisabled=pp.IsDisabled,
                                                       SemesterID = seh.SemesterID,
                                                       EvaluationID = seh.EvaluationID,
                                                       EvaluationCriteriaID = seh.EvaluationCriteriaID,
                                                       ClassID = seh.ClassID,
                                                       PupilID=seh.PupilID,
                                                       EducationLevelID = seh.EducationLevelID,
                                                       PeriodicEndingMark=seh.PeriodicEndingMark
                                                   };

            if (semester != 0)
            {
                query = query.Where(o => o.SemesterID == semester);
            }
            if (evaluationID != 0)
            {
                query = query.Where(o => o.EvaluationID == evaluationID);
            }

            return query;
        }

        #endregion

        /// <summary>
        /// Thuc hien them moi hoac cap nhat ket qua vao bang lich su
        /// </summary>
        /// <param name="schoolID">Id truong</param>
        /// <param name="dic">Cac cot khoa ngoai trong bang</param>
        /// <param name="listInsertOrUpdate">Danh sach diem va nhan xet cuoi ky</param>
        public void InsertOrUpdate(int schoolID, IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate,List<ActionAuditDataBO> lstActionAuditData,ref ActionAuditDataBO objActionAuditBO)
        {
            if (dic == null || dic.Count == 0)
            {
                return;
            }
            int partitionId = Utils.GetInt(dic["PartitionId"]);
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int classId = Utils.GetInt(dic["ClassId"]);
            int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            int evaluationId = Utils.GetInt(dic["EvaluationId"]);
            int evaluationCriteriaID = Utils.GetInt(dic["EvaluationCriteriaID"]);
            bool isRetest = Utils.GetBool(dic["IsRetest"]);
            int monthId = Utils.GetInt(dic["MonthId"]);

            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearId},
                {"ClassID",classId}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";

            List<SummedEvaluationHistory> evaluationCommentsList = SummedEvaluationHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionId
                             && p.AcademicYearID == academicYearId
                             && p.ClassID == classId
                             && p.SemesterID == semesterId
                             && p.EvaluationID == evaluationId
                             && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                             ).ToList();

            List<SummedEvaluationHistory> listInsert = new List<SummedEvaluationHistory>();
            List<SummedEvaluationHistory> listUpdate = new List<SummedEvaluationHistory>();
            SummedEvaluationHistory checkInlist = null;
            SummedEvaluation evaluationCommentsObj;
            int pupilFileId = 0;
            bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "CK", "");
            #region tao du lieu ghi log
            StringBuilder objectIDSummed = new StringBuilder();
            StringBuilder descriptionSummed = new StringBuilder();
            StringBuilder paramsSummed = new StringBuilder();
            StringBuilder oldObjectSummed = new StringBuilder();
            StringBuilder newObjectSummed = new StringBuilder();
            StringBuilder userFuntionsSummed = new StringBuilder();
            StringBuilder userActionsSummed = new StringBuilder();
            StringBuilder userDescriptionsSummed = new StringBuilder();

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            ActionAuditDataBO objAc = null;
            bool isChange = false;
            #endregion
            for (int i = 0; i < listInsertOrUpdate.Count; i++)
            {
                isChange = false;
                evaluationCommentsObj = listInsertOrUpdate[i];
                pupilFileId = evaluationCommentsObj.PupilID;
                checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilFileId).FirstOrDefault();
                objAc = lstActionAuditData.Where(p => p.PupilID == evaluationCommentsObj.PupilID && p.ClassID == evaluationCommentsObj.ClassID && p.EvaluationID == evaluationCommentsObj.EvaluationID
                                                     && p.SubjectID == evaluationCommentsObj.EvaluationCriteriaID).FirstOrDefault();
                if (objAc != null)
                {
                    objectIDStrtmp.Append(objAc.ObjID);
                    descriptionStrtmp.Append(objAc.Description);
                    paramsStrtmp.Append(objAc.Parameter);
                    oldObjectStrtmp.Append(objAc.OldData);
                    newObjectStrtmp.Append(objAc.NewData);
                    userFuntionsStrtmp.Append(objAc.UserFunction);
                    userActionsStrtmp.Append(objAc.UserAction);
                    userDescriptionsStrtmp.Append(objAc.UserDescription);
                }
                if (checkInlist == null)
                {
                    evaluationCommentsObj.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                    if (monthId == GlobalConstants.MonthID_11)
                    {
                        if (!string.IsNullOrEmpty(evaluationCommentsObj.EndingComments))
                        {
                            newObjectStrtmp.Append("Nhận xét CK: ").Append(evaluationCommentsObj.EndingComments).Append("; ");
                            oldObjectStrtmp.Append("Nhận xét CK: ").Append("; ");
                            isChange = true;
                        }
                    }

                    if (evaluationCommentsObj.PeriodicEndingMark.HasValue)
                    {
                        newObjectStrtmp.Append("KTĐK CK: ").Append(evaluationCommentsObj.PeriodicEndingMark).Append("; ");
                        oldObjectStrtmp.Append("KTĐK CK: ").Append("; ");
                        isChange = true;
                    }

                    if (!string.IsNullOrEmpty(evaluationCommentsObj.EndingEvaluation))
                    {
                        newObjectStrtmp.Append("Đánh giá: ").Append(evaluationCommentsObj.EndingEvaluation).Append("; ");
                        oldObjectStrtmp.Append("Đánh giá: ").Append("; ");
                        isChange = true;
                    }
                    if (isChange)
                    {
                        evaluationCommentsObj.CreateTime = DateTime.Now;
                        listInsert.Add(ConvertToHistory(evaluationCommentsObj));
                    }
                }
                else
                {
                    if (!lockCK)
                    {

                        if (monthId == GlobalConstants.MonthID_11)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.EndingComments))
                            {
                                if (!checkInlist.EndingComments.Equals(evaluationCommentsObj.EndingComments))
                                {
                                    oldObjectStrtmp.Append("Nhận xét CK: ").Append(checkInlist.EndingComments).Append("; ");
                                    newObjectStrtmp.Append("Nhận xét CK: ").Append(evaluationCommentsObj.EndingComments).Append("; ");
                                    isChange = true;
                                    checkInlist.EndingComments = evaluationCommentsObj.EndingComments;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.EndingComments))
                            {
                                oldObjectStrtmp.Append("Nhận xét CK: ").Append(checkInlist.EndingComments).Append("; ");
                                newObjectStrtmp.Append("Nhận xét CK: ").Append(evaluationCommentsObj.EndingComments).Append("; ");
                                isChange = true;
                                checkInlist.EndingComments = evaluationCommentsObj.EndingComments;
                            }
                        }

                        //update thong tin
                        if (checkInlist.PeriodicEndingMark.HasValue)
                        {
                            if (checkInlist.PeriodicEndingMark != evaluationCommentsObj.PeriodicEndingMark)
                            {
                                oldObjectStrtmp.Append("KTĐK CK: ").Append(checkInlist.PeriodicEndingMark).Append("; ");
                                newObjectStrtmp.Append("KTĐK CK: ").Append(evaluationCommentsObj.PeriodicEndingMark).Append("; ");
                                isChange = true;
                                checkInlist.PeriodicEndingMark = evaluationCommentsObj.PeriodicEndingMark;
                            }
                        }
                        else if (evaluationCommentsObj.PeriodicEndingMark.HasValue)
                        {
                            oldObjectStrtmp.Append("KTĐK CK: ").Append(checkInlist.PeriodicEndingMark).Append("; ");
                            newObjectStrtmp.Append("KTĐK CK: ").Append(evaluationCommentsObj.PeriodicEndingMark).Append("; ");
                            isChange = true;
                            checkInlist.PeriodicEndingMark = evaluationCommentsObj.PeriodicEndingMark;
                        }

                        if (!string.IsNullOrEmpty(checkInlist.EndingEvaluation))
                        {
                            if (!checkInlist.EndingEvaluation.Equals(evaluationCommentsObj.EndingEvaluation))
                            {
                                oldObjectStrtmp.Append("Đánh giá: ").Append(checkInlist.EndingEvaluation).Append("; ");
                                newObjectStrtmp.Append("Đánh giá: ").Append(evaluationCommentsObj.EndingEvaluation).Append("; ");
                                isChange = true;
                                checkInlist.EndingEvaluation = evaluationCommentsObj.EndingEvaluation;
                            }
                        }
                        else if (!string.IsNullOrEmpty(evaluationCommentsObj.EndingEvaluation))
                        {
                            oldObjectStrtmp.Append("Đánh giá: ").Append(checkInlist.EndingEvaluation).Append("; ");
                            newObjectStrtmp.Append("Đánh giá: ").Append(evaluationCommentsObj.EndingEvaluation).Append("; ");
                            isChange = true;
                            checkInlist.EndingEvaluation = evaluationCommentsObj.EndingEvaluation;
                        }
                    }
                    //Neu hoc sinh co diem thi lai thi cap nhat diem thi lai
                    if (isRetest)
                    {
                        checkInlist.RetestMark = evaluationCommentsObj.RetestMark;
                        checkInlist.EvaluationReTraining = evaluationCommentsObj.EvaluationReTraining;
                    }
                    if (isChange)
                    {
                        checkInlist.UpdateTime = DateTime.Now;
                        listUpdate.Add(checkInlist);
                    }
                }
                if (isChange)
                {
                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                    oldObjectSummed.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                    tmpNew = newObjectStrtmp.Length > 0 ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) : "";
                    newObjectSummed.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                    userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                    objectIDSummed.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsSummed.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionSummed.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsSummed.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsSummed.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                }
                else if (objAc != null && objAc.isInsertLog)
                {
                    objectIDSummed.Append(objAc.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsSummed.Append(objAc.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionSummed.Append(objAc.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsSummed.Append(objAc.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsSummed.Append(objAc.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = objAc.OldData.Length > 0 ? objAc.OldData.ToString().Substring(0, objAc.OldData.Length - 2) : "";
                    oldObjectSummed.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                    tmpNew = objAc.NewData.Length > 0 ? objAc.NewData.ToString().Substring(0, objAc.NewData.Length - 2) : "";
                    newObjectSummed.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                    userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");
                    oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }

            objActionAuditBO.ObjID = objectIDSummed;
            objActionAuditBO.Parameter = paramsSummed;
            objActionAuditBO.UserFunction = userFuntionsSummed;
            objActionAuditBO.UserAction = userActionsSummed;
            objActionAuditBO.UserDescription = userDescriptionsSummed;
            objActionAuditBO.Description = descriptionSummed;
            objActionAuditBO.OldData = oldObjectSummed;
            objActionAuditBO.NewData = newObjectSummed;

            if (listInsert != null && listInsert.Count > 0)
            {
                for (int i = 0; i < listInsert.Count; i++)
                {
                    this.Insert(listInsert[i]);
                }
            }
            if (listUpdate != null && listUpdate.Count > 0)
            {
                for (int i = 0; i < listUpdate.Count; i++)
                {
                    this.Update(listUpdate[i]);
                }
            }
            this.Save();
        }


        public void InsertOrUpdatebyClass(IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate)
        {
            int partitionId = Utils.GetInt(dic["PartitionId"]);
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int classId = Utils.GetInt(dic["ClassId"]);
            int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            bool isRetest = Utils.GetBool(dic["IsRetest"]);
            IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolId},
                {"AcademicYearID",academicYearId},
                {"ClassID",classId}
            };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            //Thuc hien cap nhat du lieu hien tai
            List<SummedEvaluationHistory> evaluationCommentsList = SummedEvaluationHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionId
                             && p.AcademicYearID == academicYearId
                             && p.ClassID == classId
                             && p.SemesterID == semesterId
                             ).ToList();

            List<SummedEvaluationHistory> insertList = new List<SummedEvaluationHistory>();
            List<SummedEvaluationHistory> updateList = new List<SummedEvaluationHistory>();
            SummedEvaluationHistory checkInlist = null;
            SummedEvaluation objSE;
            string ck = (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
            bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, ck, "CK");
            
            for (int i = 0; i < listInsertOrUpdate.Count; i++)
            {
                objSE = listInsertOrUpdate[i];
                checkInlist = evaluationCommentsList.Where(p => p.PupilID == objSE.PupilID
                                                           && p.EvaluationID == objSE.EvaluationID
                                                           && p.EvaluationCriteriaID == objSE.EvaluationCriteriaID).FirstOrDefault();
                if (checkInlist == null)
                {
                    objSE.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                    insertList.Add(ConvertToHistory(objSE));
                }
                else
                {
                    if (!lockCK)
                    {
                        //update thong tin
                        checkInlist.PeriodicEndingMark = objSE.PeriodicEndingMark;
                        checkInlist.EndingComments = objSE.EndingComments;
                        checkInlist.EndingEvaluation = objSE.EndingEvaluation;
                    }
                    //Neu hoc sinh co diem thi lai thi cap nhat diem thi lai
                    if (isRetest)
                    {
                        checkInlist.RetestMark = objSE.RetestMark;
                        checkInlist.EvaluationReTraining = objSE.EvaluationReTraining;
                    }
                    //Khong cap nhat diem thi lai 
                    checkInlist.UpdateTime = DateTime.Now;
                    updateList.Add(checkInlist);
                }
            }

            //Kiem tra nam hoc dang thuc hien da chuyen du lieu lich su
            //IDictionary<string, object> columnMap = ColumnMappings();
            if (insertList != null && insertList.Count > 0)
            {
                //BulkInsert(insertList, columnMap);
                for (int i = 0; i < insertList.Count; i++)
                {
                    this.Insert(insertList[i]);
                }
            }
            if (updateList != null && updateList.Count > 0)
            {
                //Cac cot where
                /*List<string> columnWhere = new List<string>();
                columnWhere.Add("LAST_DIGIT_SCHOOL_ID");
                columnWhere.Add("ACADEMIC_YEAR_ID");
                columnWhere.Add("SUMMED_EVALUATION_ID");
                BulkUpdate(updateList, columnMap, columnWhere);*/
                for (int i = 0; i < updateList.Count; i++)
                {
                    this.Update(updateList[i]);
                }
            }
            this.Save();
        }

        /// <summary>
        /// Mapping column tu entity sang table
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("SummedEvaluationID", "SUMMED_EVALUATION_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("EndingComments", "ENDING_COMMENTS");

            return columnMap;
        }

        /// <summary>
        /// Chuyen du lieu tu SummedEvaluation sang SummedEvaluationHistory
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        private SummedEvaluationHistory ConvertToHistory(SummedEvaluation objA)
        {
            if (objA == null)
            {
                return new SummedEvaluationHistory();
            }
            SummedEvaluationHistory objB = new SummedEvaluationHistory
            {
                SummedEvaluationID = objA.SummedEvaluationID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                CreateTime = objA.CreateTime,
                EducationLevelID = objA.EducationLevelID,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                PupilID = objA.PupilID,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                UpdateTime = objA.UpdateTime,
                EndingComments = objA.EndingComments,
                EndingEvaluation = objA.EndingEvaluation,
                EvaluationReTraining = objA.EvaluationReTraining,
                PeriodicEndingMark = objA.PeriodicEndingMark,
                RetestMark = objA.RetestMark
            };
            return objB;
        }

        /// <summary>
        /// Chuyen du lieu 1 list SummedEvaluationHistory sang list SummedEvaluationHistory
        /// </summary>
        /// <param name="lstA"></param>
        /// <returns></returns>
        private List<SummedEvaluationHistory> ConvertListtoHistory(List<SummedEvaluation> lstA)
        {
            List<SummedEvaluationHistory> lstECHistory = lstA.ConvertAll(e => ConvertToHistory(e));
            return lstECHistory;
        }

        public IQueryable<SummedEvaluationHistory> getListSummedEvaluationByListSubject(IDictionary<string, object> dic)
        {
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            List<int> listSubjectId = Utils.GetIntList(dic["listSubjectId"]);
            int evaluationId = Utils.GetInt(dic["EvaluationId"]);
            int educationLevelId = Utils.GetInt(dic["EduationLevelId"]);
            int classId = Utils.GetInt(dic["ClassId"]);

            IQueryable<SummedEvaluationHistory> iQuery = SummedEvaluationHistoryBusiness.All.Where(p=>p.LastDigitSchoolID == partitionId);
            if (schoolId > 0)
            {
                iQuery = iQuery.Where(p => p.SchoolID == schoolId);
            }
            if (partitionId > 0)
            {
                iQuery = iQuery.Where(p => p.LastDigitSchoolID == partitionId);
            }
            if (academicYearId > 0)
            {
                iQuery = iQuery.Where(p => p.AcademicYearID == academicYearId);
            }
            if (semesterId > 0)
            {
                iQuery = iQuery.Where(p => p.SemesterID == semesterId);
            }
            if (evaluationId > 0)
            {
                iQuery = iQuery.Where(p => p.EvaluationID == evaluationId);
            }
            if (educationLevelId > 0)
            {
                iQuery = iQuery.Where(p => p.EducationLevelID == educationLevelId);
            }
            if (classId > 0)
            {
                iQuery = iQuery.Where(p => p.ClassID == classId);
            }
            if (listSubjectId != null && listSubjectId.Count > 0)
            {
                iQuery = iQuery.Where(p => listSubjectId.Contains(p.EvaluationCriteriaID));
            }

            return iQuery;
        }


        public List<string> GetListCommentHistory(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            List<string> lstComment = new List<string>();
            int partition = UtilsBusiness.GetPartionId(SchoolID);

            IQueryable<SummedEvaluationHistory> lstEvaluationComments = (from e in SummedEvaluationHistoryBusiness.All.AsNoTracking()
                                                                  where e.SchoolID == SchoolID
                                                                  && e.AcademicYearID == AcademicYearID
                                                                  && e.ClassID == ClassID
                                                                  && e.EvaluationID == EvaluationID
                                                                  && (e.SemesterID == SemesterID || SemesterID == 0)
                                                                  && e.LastDigitSchoolID == partition
                                                                  select e);

            lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.EndingComments)).Select(p => p.EndingComments).Distinct().ToList();
            lstComment = lstComment.Distinct().ToList();

            return lstComment.OrderBy(p => p).ToList();
        }
    }
}
