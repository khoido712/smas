/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Transactions;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class SummedUpRecordClassBusiness
    {
        public IQueryable<SummedUpRecordClassBO> GetListClassByEducationLevel(IDictionary<string, object> dic)
        {
            int Semester = Utils.GetInt(dic, "Semester");
            int period = Utils.GetInt(dic, "PeriodID");
            int? PeriodID = period == 0 ? null : (int?)period;
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Type = Utils.GetInt(dic, "Type");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN");

            if (PeriodID.HasValue)
            {
                var query = from cp in ClassProfileBusiness.All
                            join surc in SummedUpRecordClassBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.Semester == Semester && o.Type == Type && o.PeriodID == PeriodID.Value) on cp.ClassProfileID equals surc.ClassID into g1
                            from j2 in g1.DefaultIfEmpty()
                            where (cp.AcademicYearID == AcademicYearID &&
                               cp.SchoolID == SchoolID &&
                               cp.EducationLevelID == EducationLevelID)
                               && cp.IsActive == true
                            select new SummedUpRecordClassBO
                            {
                                ClassID = cp.ClassProfileID,
                                ClassName = cp.DisplayName,
                                ClassOrderNumber = cp.OrderNumber,
                                Status = j2.Status,
                                ModifiedDate = j2.ModifiedDate,
                                NumberOfPupil = j2.NumberOfPupil,
                                isVNEN = cp.IsVnenClass
                            };
                if (isVNEN)
                {
                    query = query.Where(p => (p.isVNEN == false || p.isVNEN == null));
                }
                return query;
            }
            else
            {
                var query = from cp in ClassProfileBusiness.All
                            join surc in SummedUpRecordClassBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.Semester == Semester && o.Type == Type && !o.PeriodID.HasValue) on cp.ClassProfileID equals surc.ClassID into g1
                            from j2 in g1.DefaultIfEmpty()
                            where (cp.AcademicYearID == AcademicYearID &&
                               cp.SchoolID == SchoolID &&
                               cp.EducationLevelID == EducationLevelID)
                               && cp.IsActive == true
                            select new SummedUpRecordClassBO
                            {
                                ClassID = cp.ClassProfileID,
                                ClassName = cp.DisplayName,
                                ClassOrderNumber = cp.OrderNumber,
                                Status = j2.Status,
                                ModifiedDate = j2.ModifiedDate,
                                NumberOfPupil = j2.NumberOfPupil,
                                isVNEN = cp.IsVnenClass
                            };
                if (isVNEN)
                {
                    query = query.Where(p => (p.isVNEN == false || p.isVNEN == null));
                }
                return query;
            }
        }

        /// <summary>
        /// Namta + Lỗi thread cập nhật lại trạng thái
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lst1"></param>
        /// <param name="lst2"></param>
        /// <param name="dic"></param>
        public void UpdateStatusSummedUp(int UserID, List<ClassProfile> lst1, List<ClassProfile> lst2, IDictionary<string, object> dic)
        {
            int Semester = Utils.GetInt(dic, "Semester");
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Type = Utils.GetInt(dic, "Type");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            AcademicYear AcademicYear = AcademicYearBusiness.Find(AcademicYearID);

            //Xử lý với lst2 list check
            foreach (var item in lst2)
            {
                SummedUpRecordClass Sur = GetSummedUpRecordClass(SchoolID, item.ClassProfileID, Semester, PeriodID, Type);
                if (Sur == null) //Nếu ko tồn tại
                {
                    Sur = new SummedUpRecordClass();
                    Sur.AcademicYearID = AcademicYearID;
                    Sur.SchoolID = SchoolID;
                    Sur.Semester = Semester;
                    Sur.EducationLevelID = (int)EducationLevelID;
                    Sur.Status = SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE;
                    Sur.PeriodID = PeriodID;
                    Sur.NumberOfPupil = null;
                    Sur.Type = Type;
                    Sur.ClassID = item.ClassProfileID;
                    Sur.SynchronizeID = 0;//dung de phan biet voi tool TKD tu dong
                    this.InsertSummedUpRecordClass(UserID, Sur);
                }
                else //Nếu đã tồn tại
                {
                    Sur.Status = SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE;
                    Sur.ModifiedDate = DateTime.Now;
                    Sur.NumberOfPupil = null;
                    Sur.SynchronizeID = 0;
                    this.UpdateSummedUpRecordClass(UserID, Sur);
                }
            }
            this.Save();

        }

        //Hath8 edit 24/5
        public void InsertOrUpdateSummedUpRecordClass(int UserID, List<ClassProfile> lst1, List<ClassProfile> lst2, IDictionary<string, object> dic)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int Semester = Utils.GetInt(dic, "Semester");
                int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
                int Type = Utils.GetInt(dic, "Type");
                int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
                AcademicYear AcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                searchInfo["AcademicYearID"] = AcademicYearID;
                searchInfo["EducationLevelID"] = EducationLevelID;
                searchInfo["CheckWithClass"] = "CheckWithClass";
                AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                IQueryable<PupilOfClass> lsPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchInfo).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED);
                foreach (var item in lst1)
                {
                    if (CheckExistClassInSummedUpRecordClass(SchoolID, item.ClassProfileID, Semester, PeriodID, Type) == false)
                    {
                        SummedUpRecordClass Sur = new SummedUpRecordClass();
                        Sur.AcademicYearID = AcademicYearID;
                        Sur.SchoolID = SchoolID;
                        Sur.Semester = Semester;
                        Sur.EducationLevelID = EducationLevelID;
                        Sur.Status = SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE;
                        Sur.PeriodID = PeriodID;
                        Sur.NumberOfPupil = null;
                        Sur.Type = Type;
                        Sur.ClassID = item.ClassProfileID;
                        Sur.SynchronizeID = 0;//dung de phan biet voi tool TKD tu dong
                        this.InsertSummedUpRecordClass(UserID, Sur);
                    }
                    this.Save();
                }

                //Xử lý với lst2 list check
                foreach (var item in lst2)
                {
                    SummedUpRecordClass Sur = GetSummedUpRecordClass(SchoolID, item.ClassProfileID, Semester, PeriodID, Type);
                    if (Sur == null) //Nếu ko tồn tại
                    {
                        Sur = new SummedUpRecordClass();
                        Sur.AcademicYearID = AcademicYearID;
                        Sur.SchoolID = SchoolID;
                        Sur.Semester = Semester;
                        Sur.EducationLevelID = EducationLevelID;
                        Sur.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING;
                        Sur.PeriodID = PeriodID;
                        Sur.NumberOfPupil = null;
                        Sur.Type = Type;
                        Sur.ClassID = item.ClassProfileID;
                        Sur.SynchronizeID = 0;
                        this.InsertSummedUpRecordClass(UserID, Sur);
                    }
                    else //Nếu đã tồn tại
                    {
                        Sur.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING;
                        Sur.ModifiedDate = DateTime.Now;
                        Sur.NumberOfPupil = null;
                        Sur.SynchronizeID = 0;
                        this.UpdateSummedUpRecordClass(UserID, Sur);
                    }
                }

                this.Save();

                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(SchoolID);
                bool isGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
                EducationLevel educationLevel = EducationLevelBusiness.Find(EducationLevelID);
                int pupilRanked = 0;
                foreach (var item in lst2)
                {
                    if (Type == SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1)
                    {
                        if (UtilsBusiness.IsMoveHistory(aca))
                        {
                            pupilRanked = PupilRankingHistoryBusiness.RankClassHistory(UserID, SchoolID, AcademicYearID, item.ClassProfileID, Semester, PeriodID, false, GlobalConstants.NOT_AUTO_MARK);
                        }
                        else
                        {
                            pupilRanked = PupilRankingBusiness.RankClass(UserID, SchoolID, AcademicYearID, item.ClassProfileID, Semester, PeriodID, false, GlobalConstants.NOT_AUTO_MARK);
                        }
                        SummedUpRecordClass src = GetSummedUpRecordClass(SchoolID, item.ClassProfileID, Semester, PeriodID, Type);
                        src.Status = pupilRanked > 0 ? SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE : SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE;
                        src.ModifiedDate = DateTime.Now;
                        src.NumberOfPupil = pupilRanked;
                        src.SynchronizeID = 0;
                        this.UpdateSummedUpRecordClass(UserID, src);
                    }

                    if (Type == SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_2) // Xếp loại
                    {
                        //Nếu là cấp 1
                        List<PupilRankingBO> lstPupilToRank = new List<PupilRankingBO>();
                        if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                        {
                            continue;
                        }

                        //Nếu là cấp 2 3
                        List<PupilRankingBO> lstPupilToRank23 = new List<PupilRankingBO>();
                        IDictionary<string, object> PRSearchInfo = new Dictionary<string, object>();
                        PRSearchInfo["AcademicYearID"] = AcademicYearID;
                        PRSearchInfo["SchoolID"] = SchoolID;
                        PRSearchInfo["ClassID"] = item.ClassProfileID;
                        PRSearchInfo["Semester"] = Semester;
                        lstPupilToRank23 = PupilRankingBusiness.GetPupilToRank(PRSearchInfo, false).ToList();

                        lstPupilToRank23 = lstPupilToRank23.Where(u => u.CapacityLevelID.HasValue
                                                                    && (u.ConductLevelID.HasValue || PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, educationLevel.Grade, u.LearningType, u.BirthDate))).ToList();

                        if (UtilsBusiness.IsMoveHistory(aca))
                        {
                            PupilRankingHistoryBusiness.ClassifyPupilHistory(lstPupilToRank23, Semester, false, GlobalConstants.NOT_AUTO_MARK);
                        }
                        else
                        {
                            PupilRankingBusiness.ClassifyPupil(lstPupilToRank23, Semester, false, GlobalConstants.NOT_AUTO_MARK);
                        }
                        SetAutoDetectChangesEnabled(false);
                        IDictionary<string, object> dicPupil = new Dictionary<string, object>();
                        dicPupil["AcademicYearID"] = AcademicYearID;
                        dicPupil["Semester"] = Semester;
                        dicPupil["PeriodID"] = PeriodID;
                        dicPupil["ClassID"] = item.ClassProfileID;
                        dicPupil["EducationLevelID"] = EducationLevelID;
                        int CountPupil = VPupilRankingBusiness.SearchBySchool(SchoolID, dicPupil).Where(o => o.IsCategory == true && lsPupilOfClass.Any(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID)).Count();

                        SummedUpRecordClass src = GetSummedUpRecordClass(SchoolID, item.ClassProfileID, Semester, PeriodID, Type);
                        src.Status = CountPupil > 0 ? SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE : SystemParamsInFile.STATUS_SUR_CLASS_NOTCOMPLETE;
                        src.ModifiedDate = DateTime.Now;
                        src.NumberOfPupil = CountPupil;
                        src.SynchronizeID = 0;
                        this.UpdateSummedUpRecordClass(UserID, src);

                    }
                }
                this.Save();
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateSummedUpRecordClass", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void InsertSummedUpRecordClass(int UserID, SummedUpRecordClass entity)
        {

            //UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID) = false: Bạn không có quyền thực hiện việc thêm mới
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID))
            {
                //            ClassID: require, FK(ClassProfile)
                new ClassProfileBusiness(null).CheckAvailable(entity.ClassID, "ClassProfile_Label_ClassProfileID");
                //SchoolID: require, FK(SchoolProfile)
                new SchoolProfileBusiness(null).CheckAvailable(entity.SchoolID, "SchoolProfile_Label_SchoolProfileID");
                //AcademicYearID: require, FK(AcademicYear)
                new AcademicYearBusiness(null).CheckAvailable(entity.AcademicYearID, "AcademicYear_Label_AcademicYearID");
                //Status : require, in(1,2,3)
                if (entity.Status != 1 && entity.Status != 2 && entity.Status != 3)
                {
                    throw new BusinessException("SummedUpRecordClass_Validation_Status");
                }
                //SchoolID, AcademicYearID: not compatible(AcademicYear)
                bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!AcademicYearCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //ClassID, AcademicYearID: not compatible(ClassProfile)
                bool ToClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                     new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!ToClassProfileCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                base.Insert(entity);
            }
            else
            {
                throw new BusinessException("SummedUpRecordClass_Validation_HasHeadTeacherPermission");
            }
        }

        public void UpdateSummedUpRecordClass(int UserID, SummedUpRecordClass entity)
        {
            //UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID) = false: Bạn không có quyền thực hiện việc cập nhật
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID))
            {
                // ClassID: require, FK(ClassProfile)
                new ClassProfileBusiness(null).CheckAvailable(entity.ClassID, "ClassProfile_Label_ClassProfileID");
                //SchoolID: require, FK(SchoolProfile)
                new SchoolProfileBusiness(null).CheckAvailable(entity.SchoolID, "SchoolProfile_Label_SchoolProfileID");
                //AcademicYearID: require, FK(AcademicYear)
                new AcademicYearBusiness(null).CheckAvailable(entity.AcademicYearID, "AcademicYear_Label_AcademicYearID");
                //Status : require, in(1,2,3)
                if (entity.Status != 1 && entity.Status != 2 && entity.Status != 3)
                {
                    throw new BusinessException("SummedUpRecordClass_Validation_Status");
                }
                //SchoolID, AcademicYearID: not compatible(AcademicYear)
                bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!AcademicYearCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //ClassID, AcademicYearID: not compatible(ClassProfile)
                bool ToClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                     new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!ToClassProfileCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                base.Update(entity);
            }
            else
            {
                throw new BusinessException("SummedUpRecordClass_Validation_HasHeadTeacherPermissionUpdate");
            }
        }

        private IQueryable<SummedUpRecordClass> Search(IDictionary<string, object> dic)
        {
            //            Các điều kiện với ID = 0 sẽ bỏ điều kiện đó
            //Thực hiện tìm kiếm trong bảng SummedUpRecordClass surc join ClassProfile cp on surc.ClassID = cp.ClassProfileID join PupilOfClass poc on poc.ClassID = surc.ClassID
            //-             Check: default = null; null => tìm kiếm All. Nếu != null thì tìm kiếm theo max CreatedDate (nếu ModifiedDate = null) hoặc tìm kiếm theo max ModifiedDate (nếu ModifiedDate != null)
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            int Status = Utils.GetInt(dic, "Status");
            int Type = Utils.GetInt(dic, "Type");
            int SummedUpRecordClassID = Utils.GetInt(dic, "SummedUpRecordClassID");
            List<int> lstClassID = Utils.GetIntList(dic, "ListClassID");

            IQueryable<SummedUpRecordClass> Query;
            Query = SummedUpRecordClassRepository.All;
            //join cp in ClassProfileRepository.All on src.ClassID equals cp.ClassProfileID
            //join poc in PupilOfClassRepository.All on src.ClassID equals poc.ClassID
            //select new SummedUpRecordClassBO
            //{
            //    ClassID = src.ClassID,
            //    ClassName = cp.DisplayName,
            //    Status = src.Status,
            //    ModifiedDate = src.ModifiedDate,
            //    NumberOfPupil = src.NumberOfPupil,
            //    Semester = src.Semester,
            //    SchoolID = src.SchoolID,

            //};
            if (ClassID != 0)
            {
                Query = from sur in Query
                        join cp in ClassProfileBusiness.All on sur.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && sur.ClassID == ClassID
                        && cp.IsActive == true
                        select sur;
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (Semester != 0)
            {
                Query = Query.Where(o => o.Semester == Semester);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SummedUpRecordClassID > 0)
            {
                Query = Query.Where(o => o.SummedUpRecordClassID == SummedUpRecordClassID);
            }
            if (PeriodID.HasValue)
            {
                Query = Query.Where(o => o.PeriodID == PeriodID);
            }
            else
            {
                Query = Query.Where(o => !o.PeriodID.HasValue);
            }
            if (Status > 0)
            {
                Query = Query.Where(o => o.Status == Status);
            }
            if (Type > 0)
            {
                Query = Query.Where(o => o.Type == Type);
            }
            if (lstClassID.Count > 0)
            {
                Query = Query.Where(o => lstClassID.Contains(o.ClassID));
            }
            return Query;

        }

        public IQueryable<SummedUpRecordClass> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);

        }

        public bool CheckExistClassInSummedUpRecordClass(int SchoolID, int ClassID, int Semester, int? PeriodID, int Type)
        {
            IQueryable<SummedUpRecordClass> listsur = SummedUpRecordClassRepository.All.AsNoTracking();
            if (PeriodID == null)
            {
                listsur = listsur.Where(o => o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && o.Type == Type && o.PeriodID == null);
            }
            else
            {
                listsur = listsur.Where(o => o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && o.Type == Type && o.PeriodID == PeriodID);
            }

            if (listsur.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private SummedUpRecordClass GetSummedUpRecordClass(int SchoolID, int ClassID, int Semester, int? PeriodID, int Type)
        {
            IQueryable<SummedUpRecordClass> listsur = SummedUpRecordClassRepository.All.Where(o => o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && o.Type == Type);

            if (PeriodID == null)
                listsur = listsur.Where(o => o.PeriodID == null);
            else
                listsur = listsur.Where(o => o.PeriodID == PeriodID);

            return listsur.FirstOrDefault();
        }

        /// <summary>
        /// 20/03/2014
        /// Quanglm1
        /// Tổng kết điểm tự động
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public string AutoMarkSummary(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID)
        {
            //bien ket qua
            //0:Khong co ket qua,1:cap nhat thanh cong,2:co loi xay ra

            //Log nghiep vu
            string paramList = string.Format("SchoolID={0},AcademicYearID={1}, ClassID={2},Semester={3}", SchoolID, AcademicYearID, ClassID, Semester);

            string Result = "0";
            try
            {
                SetAutoDetectChangesEnabled(false);
                // Loc ID hoc sinh khong trung nhau
                lstPupilID = lstPupilID.Distinct().ToList();
                Dictionary<int, int> retValue = new Dictionary<int, int>();
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

                Dictionary<string, object> dicSubject = new Dictionary<string, object>();
                dicSubject["ClassID"] = ClassID;
                dicSubject["AcademicYearID"] = AcademicYearID;
                dicSubject["IsApprenticeShipSubject"] = false;
                if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL) dicSubject["Semester"] = Semester;


                SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass
                {
                    AcademicYearID = AcademicYearID,
                    ClassID = ClassID,
                    PeriodID = PeriodID,
                    Semester = Semester,
                    CreatedDate = DateTime.Now,
                    EducationLevelID = classProfile.EducationLevelID,
                    SchoolID = SchoolID,
                    NumberOfPupil = 0,
                    Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                    Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING,
                    SynchronizeID = 0,
                };
                //12.02.2018 Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong

                if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
                {
                    string msg = string.Format("Dang co lop thuc hien tong ket/xep loai classId={0} hoc ky={0}", ClassID, Semester);

                    return msg;
                }


                List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject).ToList();
                // Tinh so mon bat buoc nhap diem                    
                List<ClassSubject> lstClassSubjectRequired = lstClassSubject.Where(o => o.AppliedType.HasValue && o.AppliedType == SystemParamsInFile.APPLIED_TYPE_MANDATORY).ToList();
                List<SummedUpRecordBO> lstSummedUp = SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, PeriodID)
                                                                        .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                                        && (u.SummedUpMark.HasValue || u.JudgementResult != null)
                                                                        ).ToList();

                // Kiem tra neu khong ton 

                //neu hoc ky la Ca nam thi lay them danh sach diem trung binh mon cua hoc ky 1
                List<SummedUpRecordBO> listSummedUpHKI = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                    ? SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, ClassID, null).ToList()
                                                                    : new List<SummedUpRecordBO>();

                // Lay danh sach hoc sinh mien giảm
                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, 0).ToList();
                // Mien giam ky 1
                List<ExemptedSubject> lstExemptedI = lstExempted.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                // Mien giam ky 2
                List<ExemptedSubject> lstExemptedII = lstExempted.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<PupilRanking> lstPupilRanking = new List<PupilRanking>();
                List<SummedUpRecord> lstSummedUpRanking = new List<SummedUpRecord>();

                IQueryable<PupilOfClass> iqPoc = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }
                        , {"Status", new List<int>() {SystemParamsInFile.PUPIL_STATUS_STUDYING }}});

                List<PupilRanking> listExistedRanking = PupilRankingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID }, { "Semester", Semester }, { "PeriodID", PeriodID } })
                    .Where(o => iqPoc.Any(p => p.PupilID == o.PupilID))
                    .ToList();

                //lay danh sach hoc sinh dang ky mon chuyen/mon tu chon
                IDictionary<string, object> dicRegis = new Dictionary<string, object>()
                    {
                        {"SchoolID",SchoolID},
                        {"AcademicYearID",AcademicYearID},
                        {"ClassID",ClassID},
                        {"YearID",academicYear.Year}//partion
                    };
                List<RegisterSubjectSpecializeBO> lstRegisterSujectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);
                List<PupilRankingClassBO> lstPupilForRanking = new List<PupilRankingClassBO>();
                foreach (int pupilID in lstPupilID)
                {
                    // thong tin ve TBM cua hoc sinh dang xet
                    PupilRanking pocExistedRanking = listExistedRanking.FirstOrDefault(u => u.PupilID == pupilID);
                    IEnumerable<SummedUpRecordBO> lstSummedOfPupil = lstSummedUp.Where(u => u.PupilID == pupilID);

                    // Kiem tra neu ton tai mon bat buoc nhap diem nhung chua co diem thi khong dua hoc sinh nay vao dien tong ket
                    // Kiem tra neu hoc sinh nay mien giam thi duoc thuc hien
                    // Neu thuc hien tong ket theo ky
                    #region Neu hoc sinh chua co du diem cho cac mon bat buoc thi khong xet tong ket diem
                    if (!PeriodID.HasValue || PeriodID.Value == 0)
                    {
                        bool isNotSum = false;
                        foreach (ClassSubject cs in lstClassSubjectRequired)
                        {
                            // Kiem tra mien giam
                            // Neu HS duoc mien giam mon nay thi duoc phep TK ma ko xet vao mon tong ket
                            if ((Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && lstExemptedI.Any(o => o.SubjectID == cs.SubjectID && o.PupilID == pupilID))
                                || (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && lstExemptedII.Any(o => o.SubjectID == cs.SubjectID && o.PupilID == pupilID)))
                            {
                                continue;
                            }
                            SummedUpRecordBO sumSubject = lstSummedOfPupil.FirstOrDefault(o => o.SubjectID == cs.SubjectID);
                            // Neu chua co diem TBM cho mon bat buoc
                            if (sumSubject == null)
                            {
                                isNotSum = true;
                                break;
                            }
                            // Neu mon nhan xet ma chua co diem
                            if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                if (sumSubject.JudgementResult == null)
                                {
                                    isNotSum = true;
                                    break;
                                }
                            }
                            else if (!sumSubject.SummedUpMark.HasValue)
                            {
                                isNotSum = true;
                                break;
                            }
                        }
                        // Neu khong co thong tin diem TBM cua bat cu mon noa doi voi TKD theo dot
                        // Hoac co mot so mon bat buoc nhap diem nhung chua co diem khi TKD theo ky thi se bo qua 
                        // va gan lai gia tri TBM trong PUPIL_RANKING la null
                        if (isNotSum || lstSummedOfPupil.Count() == 0)
                        {
                            if (pocExistedRanking != null)
                            {
                                // Neu khong thi gan lai gia trin TBM va hoc luc la rong
                                pocExistedRanking.AverageMark = null;
                                pocExistedRanking.CapacityLevelID = null;
                                pocExistedRanking.Rank = null;
                                PupilRankingBusiness.Update(pocExistedRanking);
                                PupilRankingBusiness.Save();
                            }
                            continue;
                        }
                    }
                    #endregion

                    PupilRankingClassBO pupilForRanking = new PupilRankingClassBO();
                    pupilForRanking.SchoolID = SchoolID;
                    pupilForRanking.AcademicYearID = AcademicYearID;
                    pupilForRanking.ClassID = ClassID;
                    pupilForRanking.Semester = Semester;
                    pupilForRanking.PeriodID = PeriodID;
                    pupilForRanking.PupilID = pupilID;
                    pupilForRanking.CapacityLevelID = pocExistedRanking != null ? pocExistedRanking.CapacityLevelID : null;
                    pupilForRanking.ConductLevelID = pocExistedRanking != null ? pocExistedRanking.ConductLevelID : null;
                    pupilForRanking.ListSubject = new List<PupilRankingSubjectBO>();

                    #region Nếu là HK 1 hoặc HK 2
                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        foreach (ClassSubject classSubject in lstClassSubject)
                        {
                            SummedUpRecordBO surBo = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID);
                            bool isExempted = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && lstExemptedI.Any(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID)
                                            || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && lstExemptedII.Any(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID);
                            PupilRankingSubjectBO pupilRankingSubject = new PupilRankingSubjectBO();
                            pupilRankingSubject.IsCommenting = classSubject.IsCommenting.Value;
                            pupilRankingSubject.IsExempted = isExempted;
                            pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (surBo != null ? surBo.JudgementResult : null) : null;
                            pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (surBo != null ? surBo.SummedUpMark : null);
                            pupilRankingSubject.SubjectID = classSubject.SubjectID;
                            pupilRankingSubject.AppliedType = classSubject.AppliedType;

                            pupilForRanking.ListSubject.Add(pupilRankingSubject);
                        }
                    }
                    #endregion

                    #region Nếu là cả năm
                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        foreach (ClassSubject classSubject in lstClassSubject)
                        {
                            SummedUpRecordBO summedUp = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                            //SummedUpRecordBO summedRetest = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST);

                            bool isExemptedI = lstExemptedI.Any(w => w.PupilID == pupilID && w.SubjectID == classSubject.SubjectID);
                            bool isExemptedII = lstExemptedII.Any(w => w.PupilID == pupilID && w.SubjectID == classSubject.SubjectID);

                            PupilRankingSubjectBO pupilRankingSubject = new PupilRankingSubjectBO();
                            pupilRankingSubject.IsCommenting = classSubject.IsCommenting.Value;
                            pupilRankingSubject.IsExempted = isExemptedI && isExemptedII;
                            pupilRankingSubject.SubjectID = classSubject.SubjectID;

                            //Nếu được miễn giảm cả năm
                            if (isExemptedI && isExemptedII)
                            {
                                pupilRankingSubject.RankingJudgement = null;
                                pupilRankingSubject.RankingMark = null;
                            }

                            //Nếu không miễn giảm học kỳ nào
                            if (!isExemptedI && !isExemptedII)
                            {
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedUp != null ? summedUp.SummedUpMark : null);
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedUp != null ? summedUp.JudgementResult : null) : null;
                            }

                            //Nếu chỉ miễn giảm học kỳ 1 hoặc không học trong hk 1 
                            if (isExemptedI || !classSubject.SectionPerWeekFirstSemester.HasValue || classSubject.SectionPerWeekFirstSemester.Value == 0)
                            {
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedUp != null ? summedUp.SummedUpMark : null);
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedUp != null ? summedUp.JudgementResult : null) : null;
                            }

                            //Nếu chỉ được miễn giảm học kỳ 2 hoặc không học trong hk2
                            if (isExemptedII || !classSubject.SectionPerWeekSecondSemester.HasValue || classSubject.SectionPerWeekSecondSemester.Value == 0)
                            {
                                SummedUpRecordBO summedHKI = listSummedUpHKI.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID);
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedHKI != null ? summedHKI.SummedUpMark : null);
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedHKI != null ? summedHKI.JudgementResult : null) : null;
                            }

                            pupilForRanking.ListSubject.Add(pupilRankingSubject);
                        }
                    }
                    #endregion

                    lstPupilForRanking.Add(pupilForRanking);
                }

                List<PupilRankingClassBO> lstPupilForRankingValid = lstPupilForRanking.Where(u => (u.PeriodID.HasValue && u.ListSubject.Any(v => !v.IsExempted && (!string.IsNullOrEmpty(v.RankingJudgement) || v.RankingMark.HasValue)))
                                                                                                || (!u.PeriodID.HasValue && !u.ListSubject.Any(v => !v.IsExempted && (string.IsNullOrEmpty(v.RankingJudgement) && !v.RankingMark.HasValue)))).ToList();

                foreach (PupilRankingClassBO pupilForRanking in lstPupilForRankingValid)
                {
                    PupilRanking pr = new PupilRanking();
                    pr.AcademicYearID = AcademicYearID;
                    pr.PupilID = pupilForRanking.PupilID;
                    pr.SchoolID = classProfile.SchoolID;
                    pr.ClassID = ClassID;
                    pr.EducationLevelID = classProfile.EducationLevelID;
                    pr.CreatedAcademicYear = academicYear.Year;
                    pr.Semester = Semester;
                    pr.PeriodID = PeriodID;
                    pr.CapacityLevelID = pupilForRanking.CapacityLevelID;
                    pr.ConductLevelID = pupilForRanking.ConductLevelID;
                    // AnhVD9 20150407 - Bo sung ghi log data do Tool TKD
                    pr.SynchronizeID = -1; // Function AutoMarkSummary
                    pr.MSourcedb = GlobalConstants.PUPIL_RANKIG_AUTO; // Function AutoMarkSummary
                    lstPupilRanking.Add(pr);
                    List<PupilRankingSubjectBO> listRankingSubject = pupilForRanking.ListSubject.Where(u => !u.IsExempted && (!string.IsNullOrEmpty(u.RankingJudgement) || u.RankingMark.HasValue)
                                                                      && ((u.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE && u.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE && u.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                                                                            || lstRegisterSujectBO.Where(p => p.PupilID == pupilForRanking.PupilID && p.ClassID == pupilForRanking.ClassID && p.SubjectID == u.SubjectID && p.SubjectTypeID == u.AppliedType).Count() > 0)).ToList();

                    foreach (PupilRankingSubjectBO pupilRankingSubject in listRankingSubject)
                    {
                        SummedUpRecord sur = new SummedUpRecord();
                        sur.AcademicYearID = AcademicYearID;
                        sur.ClassID = ClassID;
                        sur.IsCommenting = pupilRankingSubject.IsCommenting;
                        sur.PeriodID = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? PeriodID : null;
                        sur.PupilID = pupilForRanking.PupilID;
                        sur.SchoolID = SchoolID;
                        sur.Semester = Semester;
                        sur.SubjectID = pupilRankingSubject.SubjectID;
                        sur.SummedUpDate = DateTime.Now;
                        sur.CreatedAcademicYear = academicYear.Year;
                        sur.SummedUpMark = pupilRankingSubject.RankingMark;
                        sur.JudgementResult = pupilRankingSubject.RankingJudgement;
                        // AnhVD9 20150407 - Bo sung ghi log data do Tool TKD
                        sur.SynchronizeID = -1; // Function AutoMarkSummary                        
                        lstSummedUpRanking.Add(sur);
                    }
                }

                bool IsMoveHistory = UtilsBusiness.IsMoveHistory(academicYear);
                try
                {

                    if (lstPupilRanking.Count > 0 && lstSummedUpRanking.Count > 0)
                    {
                        // Tong ket diem
                        if (IsMoveHistory)
                        {
                            PupilRankingHistoryBusiness.RankingPupilOfClassNotRankHistory(academicYear.SchoolProfile.AdminID.Value, lstSummedUpRanking, lstPupilRanking, PeriodID, lstClassSubject, true, lstRegisterSujectBO);
                        }
                        else
                        {
                            PupilRankingBusiness.RankingPupilOfClassNotRank(academicYear.SchoolProfile.AdminID.Value, lstSummedUpRanking, lstPupilRanking, PeriodID, lstClassSubject, true, lstRegisterSujectBO);
                        }
                        // Luu lai de lay thong tin di xep hang
                        PupilRankingBusiness.Save();
                        SetAutoDetectChangesEnabled(true);
                        // Xep hang
                        #region Xep hang cho hoc sinh trong toan lop
                        IDictionary<string, object> dicRank = new Dictionary<string, object>();
                        dicRank["ClassID"] = ClassID;
                        dicRank["AcademicYearID"] = AcademicYearID;
                        dicRank["Semester"] = Semester;
                        dicRank["PeriodID"] = PeriodID;
                        dicRank["Status"] = GlobalConstants.PUPIL_STATUS_STUDYING;
                        dicRank["lstPupilId"] = lstPupilID;
                        VPupilRankingBusiness.UpdateRank(SchoolID, IsMoveHistory, dicRank);
                        #endregion
                        objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                        objSummedUpRecordClass.NumberOfPupil = lstPupilID.Count;
                        SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);

                        this.Save();
                        // Thu hien tong ket ca nam luon khi co thay doi diem o ky 1 va ky 2
                        if (Semester < SystemParamsInFile.SEMESTER_OF_YEAR_ALL && (!PeriodID.HasValue || PeriodID == 0))
                        {
                            this.AutoMarkSummary(lstPupilID, SchoolID, AcademicYearID, ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_ALL, null);
                        }
                    }
                }
                finally
                {
                    objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                    objSummedUpRecordClass.NumberOfPupil = lstPupilID.Count;
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
                }

                Result = "1";
            }
            catch (Exception ex)
            {
                Result = ex.StackTrace;
                LogExtensions.ErrorExt(logger, DateTime.Now, "AutoMarkSummary", paramList, ex);
            }
            finally
            {
                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, paramList, "", "");
                SetAutoDetectChangesEnabled(true);
            }

            return Result;
        }

        /// <summary>
        /// 20/03/2014
        /// Quanglm1
        /// Xep hang tu dong khi co hanh kiem thay doi
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public string AutoRankingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID)
        {
            string Result = "0";
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (aca == null)
            {
                return Result;
            }
            int RankingCriteria = aca.RankingCriteria;
            // Neu xep hang khong phu thuoc vao hanh kiem thi khong can phai thuc hien tiep
            if (RankingCriteria != SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT
                && RankingCriteria != SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT)
            {
                return "1";
            }
            // Loc ID hoc sinh khong trung nhau
            lstPupilID = lstPupilID.Distinct().ToList();

            IDictionary<string, object> dicRank = new Dictionary<string, object>();
            dicRank["ClassID"] = ClassID;
            dicRank["AcademicYearID"] = AcademicYearID;
            dicRank["Semester"] = Semester;
            dicRank["PeriodID"] = PeriodID;
            dicRank["Status"] = GlobalConstants.PUPIL_STATUS_STUDYING;
            bool IsMoveHistory = UtilsBusiness.IsMoveHistory(aca);
            VPupilRankingBusiness.UpdateRank(SchoolID, IsMoveHistory, dicRank);
            Result = "1";
            return Result;
        }

        /// <summary>
        /// 20/03/2014
        /// Quanglm1
        /// Tự động xếp loại học sinh
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        public string AutoClassifyingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester)
        {
            string Result = "0";
            try
            {

                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(SchoolID);
                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
                bool isGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
                List<PupilRankingBO> lstPupilToRank23 = new List<PupilRankingBO>();
                IDictionary<string, object> PRSearchInfo = new Dictionary<string, object>();
                PRSearchInfo["AcademicYearID"] = AcademicYearID;
                PRSearchInfo["SchoolID"] = SchoolID;
                PRSearchInfo["ClassID"] = ClassID;
                PRSearchInfo["Semester"] = Semester;
                // Loc ID hoc sinh khong trung nhau
                lstPupilID = lstPupilID.Distinct().ToList();
                lstPupilToRank23 = PupilRankingBusiness.GetPupilToRank(PRSearchInfo, false)
                    .Where(o => lstPupilID.Contains(o.PupilID.Value))
                    .ToList();

                lstPupilToRank23 = lstPupilToRank23.Where(u => u.CapacityLevelID.HasValue
                                                            && (u.ConductLevelID.HasValue || PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, classProfile.EducationLevel.Grade, u.LearningType, u.BirthDate))).ToList();
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);

                SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass
                {
                    AcademicYearID = AcademicYearID,
                    ClassID = ClassID,
                    PeriodID = null,
                    Semester = Semester,
                    CreatedDate = DateTime.Now,
                    EducationLevelID = classProfile.EducationLevelID,
                    SchoolID = objAcademicYear.SchoolID,
                    NumberOfPupil = lstPupilToRank23.Count,
                    Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_2,
                    Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING,
                    SynchronizeID = 0,
                };

                //12.02.2018 Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong

                if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
                {
                    return string.Format("Dang co lop thuc hien tong ket/xep loai classId={0} hoc ky={1}", ClassID, Semester);
                }
                //Danh dau lop dang thuc hien tinh tong ket, xep loai
                SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);

                try
                {
                    if (!UtilsBusiness.IsMoveHistory(objAcademicYear))
                    {
                        PupilRankingHistoryBusiness.ClassifyPupilHistory(lstPupilToRank23, Semester, false, GlobalConstants.AUTO_MARK);
                    }
                    {
                        PupilRankingBusiness.ClassifyPupil(lstPupilToRank23, Semester, false, GlobalConstants.AUTO_MARK);
                    }
                }
                finally
                {
                    objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                    objSummedUpRecordClass.NumberOfPupil = lstPupilToRank23.Count;
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
                }
                Result = "1";
            }
            catch (Exception ex)
            {
                Result = ex.StackTrace;
                string paramList = string.Format("SchoolID={0},AcademicYearID={1}, ClassID={2},Semester={3}", SchoolID, AcademicYearID, ClassID, Semester);
                LogExtensions.ErrorExt(logger, DateTime.Now, "AutoClassifyingPupil", paramList, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
            return Result;
        }


        public void InsertOrSummedUpRecordClass(SummedUpRecordClass entity)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                Dictionary<string, object> DicPupilRanking = new Dictionary<string, object>(){
                                                                            {"AcademicYearID", entity.AcademicYearID},
                                                                            {"Semester", entity.Semester},
                                                                            {"PeriodID", entity.PeriodID},
                                                                            {"ClassID", entity.ClassID},
                                                                            {"Type", entity.Type},
                                                                        };

                SummedUpRecordClass summedUpRecordClass = SearchBySchool(entity.SchoolID, DicPupilRanking).FirstOrDefault();

                if (summedUpRecordClass == null)
                {
                    summedUpRecordClass = new SummedUpRecordClass();
                    summedUpRecordClass.CreatedDate = DateTime.Now;
                }
                else
                {
                    summedUpRecordClass.ModifiedDate = DateTime.Now;
                }

                if (entity.NumberOfPupil > 0)
                {
                    summedUpRecordClass.NumberOfPupil = entity.NumberOfPupil;
                }
                else
                {
                    IQueryable<PupilOfClass> lst = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, DicPupilRanking).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);
                    IQueryable<VPupilRanking> lstPupilRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, DicPupilRanking).Where(o => o.AverageMark != null);
                    int CountRanking = (from p in lstPupilRanking
                                        join s in lst on p.PupilID equals s.PupilID
                                        select p).Count();
                    summedUpRecordClass.NumberOfPupil = CountRanking;
                }

                summedUpRecordClass.SchoolID = entity.SchoolID;
                summedUpRecordClass.ClassID = entity.ClassID;
                summedUpRecordClass.EducationLevelID = entity.EducationLevelID;
                summedUpRecordClass.AcademicYearID = entity.AcademicYearID;
                summedUpRecordClass.PeriodID = entity.PeriodID;
                summedUpRecordClass.Semester = entity.Semester;
                summedUpRecordClass.SynchronizeID = entity.SynchronizeID;
                summedUpRecordClass.MSourcedb = entity.MSourcedb;
                summedUpRecordClass.Status = entity.Status;
                summedUpRecordClass.Type = entity.Type;

                // Thực hiện insert vào SummedUpRecordClass
                if (summedUpRecordClass.SummedUpRecordClassID > 0)
                {
                    SummedUpRecordClassBusiness.Update(summedUpRecordClass);
                }
                else
                {
                    SummedUpRecordClassBusiness.Insert(summedUpRecordClass);
                }

                SummedUpRecordClassBusiness.Save();

            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrSummedUpRecordClass", "null", ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Kiem tra lop co dang thuc hien tong ket, xep loai hanh kiem
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool CheckExistsSummedExcuting(SummedUpRecordClass entity)
        {
            //Neu sau 15 mà chua tong ket thu thuc hien tong ket
            DateTime date = DateTime.Now.AddMinutes(-15);
            IQueryable<SummedUpRecordClass> iq = SummedUpRecordClassBusiness.All.Where(s => s.AcademicYearID == entity.AcademicYearID
                                                                                        && s.ClassID == entity.ClassID
                                                                                        && s.Semester == entity.Semester
                                                                                        && s.Status == SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING
                                                                                        && s.CreatedDate>=date);
            if (entity.PeriodID.HasValue && entity.PeriodID > 0)
            {
                iq = iq.Where(s => s.PeriodID == entity.PeriodID);
            }

            return iq.Any();

        }



    }
}
