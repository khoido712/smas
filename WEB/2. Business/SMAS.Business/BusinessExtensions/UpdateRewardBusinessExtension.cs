﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class UpdateRewardBusiness
    {
        public IQueryable<UpdateReward> Search(IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic,"SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            IQueryable<UpdateReward> iqUpdateReward = UpdateRewardBusiness.All;
            if (SchoolID > 0)
            {
                iqUpdateReward = iqUpdateReward.Where(p => p.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                iqUpdateReward = iqUpdateReward.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID > 0)
            {
                iqUpdateReward = iqUpdateReward.Where(p => p.ClassID == ClassID);
            }
            if (PupilID > 0)
            {
                iqUpdateReward = iqUpdateReward.Where(p => p.PupilID == PupilID);
            }
            if (SemesterID > 0)
            {
                iqUpdateReward = iqUpdateReward.Where(p => p.SemesterID == SemesterID);
            }
            return iqUpdateReward;
        }

        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        public Stream CreateUpdateRewardReport(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int classId = Utils.GetInt(dic["ClassID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);

            //Lay duong dan bao cao
            string reportCode = SystemParamsInFile.REPORT_UPDATE_REWARD;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;

            //Khoi tao
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Dien thong tin vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            string schoolName = school.SchoolName.ToUpper();
            string strAcademicYear = " NĂM HỌC " + academicYear.DisplayTitle;
            string title = ("BẢNG ĐÁNH GIÁ NHẬN XÉT VÀ KHEN THƯỞNG CUỐI " + (semester == 1 ? "HỌC KỲ I - LỚP " : "HỌC KỲ II - LỚP ") + cp.DisplayName).ToUpper();

            oSheet.SetCellValue("A2", schoolName);
            oSheet.SetCellValue("A3", title);
            oSheet.SetCellValue("A4", strAcademicYear);

            //Lay danh sach hoc sinh trong lop
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = cp.EducationLevelID;
            tmpDic["ClassID"] = classId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic).OrderBy(u => u.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName).ToList();

            //Lay thong tin khen thuong
            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.All
                .Where(o => o.PartitionID == partition
            && o.AcademicYearID == academicYearId
                && o.SchoolID == schoolId
                && o.ClassID == classId
                && o.SemesterID == semester).ToList();

            //Lay danh muc khen thuong
            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(o => o.SchoolID == schoolId).ToList();

            //Lay thong tin danh gia nang luc pham chat
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == schoolId
                && o.AcademicYearID == academicYearId
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition).ToList();

            //Lay thong tin nhan xet cac mon 
            int monthId = semester == 1 ? 15 : 16;
            List<TeacherNoteBookMonthBO> lstTeacherNoteBookMonth = (from tnbm in TeacherNoteBookMonthBusiness.All
                                                                    join sc in SubjectCatBusiness.All on tnbm.SubjectID equals sc.SubjectCatID
                                                                    where tnbm.SchoolID == schoolId
                                                                    && tnbm.AcademicYearID == academicYearId
                                                                    && tnbm.ClassID == classId
                                                                    && tnbm.MonthID == monthId
                                                                    select new TeacherNoteBookMonthBO
                                                                    {
                                                                        PupilID = tnbm.PupilID,
                                                                        SubjectID = tnbm.SubjectID,
                                                                        SubjectName = sc.DisplayName,
                                                                        CommentCQ = tnbm.CommentCQ,
                                                                        OrderInSubject = sc.OrderInSubject,
                                                                        CommentSubject = tnbm.CommentSubject
                                                                    }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();

          
            //fill du lieu
            int startRow = 7;
            int lastRow = startRow + lstPoc.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 7;
            int curColumn = startColumn;

            DateTime endsemester = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate.Value : academicYear.SecondSemesterEndDate.Value;
            for (int i = 0; i < lstPoc.Count; i++)
            {
                PupilOfClass poc = lstPoc[i];
                bool isShow = poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                isShow = isShow || ((poc.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    && poc.EndDate.HasValue && poc.EndDate.Value > endsemester);

                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //Ma hoc sinh
                oSheet.SetCellValue(curRow, curColumn, poc.PupilProfile.PupilCode);
                curColumn++;
                //Ho ten
                oSheet.SetCellValue(curRow, curColumn, poc.PupilProfile.FullName);
                curColumn++;
                //Ngay sinh
                oSheet.SetCellValue(curRow, curColumn, String.Format("{0:dd/MM/yyyy}", poc.PupilProfile.BirthDate));
                curColumn++;

                if (isShow)
                {
                    //Load nhan xet GVBM
                    List<TeacherNoteBookMonthBO> lstTnbOfPupil = lstTeacherNoteBookMonth.Where(o => o.PupilID == poc.PupilID).ToList();
                    string comment = String.Empty;
                    for (int j = 0; j < lstTnbOfPupil.Count; j++)
                    {
                        TeacherNoteBookMonthBO tnbm = lstTnbOfPupil[j];
                        if (!string.IsNullOrEmpty(tnbm.CommentSubject))
                        {
                            comment += tnbm.SubjectName + ": " + tnbm.CommentSubject;
                            if (j < lstTnbOfPupil.Count - 1)
                            {
                                comment += "\n";
                            }
                        }
                    }
                    oSheet.SetCellValue(curRow, curColumn, comment);
                    curColumn++;

                    //Load nhan xet GVCN
                    ReviewBookPupil review = lstReviewBook.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();
                    string cqComment = "";
                    if (review != null)
                    {
                        cqComment = review.CQComment;
                    }
                    oSheet.SetCellValue(curRow, curColumn, cqComment);
                    curColumn++;

                    //load tu so tay giao vien
                    UpdateReward ur = lstUpdateReward.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();
                    string strReward = String.Empty;

                    if (ur != null && ur.Rewards != null)
                    {

                        List<int> lstUrId = ur.Rewards.Split(',').Select(tag => tag.Trim()).
                            Where(tag => !string.IsNullOrEmpty(tag)).Select(o => Int32.Parse(o)).ToList();
                        List<RewardFinal> lstRf = lstRewardFinal.Where(o => lstUrId.Contains(o.RewardFinalID)).ToList();


                        for (int j = 0; j < lstRf.Count; j++)
                        {
                            RewardFinal rf = lstRf[j];
                            strReward = strReward + rf.RewardMode;

                            if (j < lstRf.Count - 1)
                            {
                                strReward = strReward + "; ";
                            }
                        }
                    }

                    oSheet.SetCellValue(curRow, curColumn, strReward);
                    curColumn++;
                }
                //Ve khung cho dong
                IVTRange range = oSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                if (poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING && poc.Status != GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    oSheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);

                }

                curRow++;
                curColumn = startColumn;
            }

            if (lstPoc.Count > 0)
            {
                IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);

            }

            //Format header
            IVTRange headerRange = oSheet.GetRange(5, 1, 6, 7);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            headerRange.FillColor(System.Drawing.Color.LightGray);

            oSheet.FitAllColumnsOnOnePage = true;

            //Fill sheet Hinh thuc KT
            oSheet = oBook.GetSheet(2);
            //Lay hinh thuc khen thuong cua truong
            startRow = 4;
            lastRow = startRow + lstRewardFinal.Count - 1;
            curRow = startRow;
            startColumn = 1;
            lastColumn = 3;
            curColumn = startColumn;

            for (int i = 0; i < lstRewardFinal.Count; i++)
            {
                RewardFinal rf = lstRewardFinal[i];
               
                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //ten hinh thuc khen thuong
                oSheet.SetCellValue(curRow, curColumn, rf.RewardMode);
                curColumn++;
                //ghi chu
                oSheet.SetCellValue(curRow, curColumn, rf.Note);
                curColumn++;
               
                curRow++;
                curColumn = startColumn;
            }

            if (lstPoc.Count > 0)
            {
                IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
              
            }

            return oBook.ToStream();
        }

        public ProcessedReport InsertUpdateRewardReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_UPDATE_REWARD;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            string outputNamePattern = reportDef.OutputNamePattern;

            string className = "";
            ClassProfile cp = ClassProfileBusiness.Find(Utils.GetInt(dic, "ClassID"));
            if (cp != null)
            {
                className = cp.DisplayName;
            }

            string semester;
            if (Utils.GetInt(dic, "Semester") == 1)
            {
                semester = "HK1";
            }
            else
            {
                semester = "HK2";
            }

            outputNamePattern = outputNamePattern.Replace("[Class]", className);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return pr;
        }
    }
}
