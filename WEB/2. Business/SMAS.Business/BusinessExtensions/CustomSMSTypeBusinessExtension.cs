/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class CustomSMSTypeBusiness
    {
        public List<SMSTypeBO> GetCustomSMSTypeBySchool(int schoolId, int grade, bool? isLocked = null)
        {
            return this.All.Where(o => o.SCHOOL_ID == schoolId && o.GRADE == grade && (!isLocked.HasValue || (isLocked.HasValue && o.IS_LOCKED == isLocked.Value)) && o.IS_ACTIVE)
                .OrderBy(o => o.CREATE_TIME)
                .Select(o => new SMSTypeBO
                {
                    IsCustom = true,
                    Name = o.TYPE_NAME,
                    TypeCode = o.TYPE_CODE,
                    TypeID = o.CUSTOM_SMS_TYPE_ID,
                    IsForVNEN = o.FOR_VNEN,
                    PeriodType = o.PERIOD_TYPE

                }).ToList();

        }
    }
}
