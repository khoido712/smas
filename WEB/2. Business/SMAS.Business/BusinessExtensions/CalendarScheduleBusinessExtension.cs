﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using System.IO;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;


namespace SMAS.Business.Business
{
    public partial class CalendarScheduleBusiness
    {
        public IQueryable<CalendarSchedule> Search(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            DateTime? DaySelected = Utils.GetDateTime(dic, "DaySelected");
            byte EducationLevelID = (byte)Utils.GetInt(dic, "EducationLevelID");
            int TeacherID = Utils.GetInt(dic, "TeacherID", 0);

            var seachQuery = from calendarSchedule in CalendarScheduleBusiness.All
                             join calendar in CalendarBusiness.All on calendarSchedule.CalendarID equals calendar.CalendarID
                             join cls in ClassProfileBusiness.All on calendar.ClassID equals cls.ClassProfileID
                             where
                             (AcademicYearID == 0 || calendar.AcademicYearID == AcademicYearID)
                             && (ClassID == 0 || calendar.ClassID == ClassID)
                             && (EducationLevelID == 0 || cls.EducationLevelID == EducationLevelID)
                             && cls.IsActive.Value
                             select calendarSchedule;
            //If TeacherID <> 0
            if (TeacherID != 0)
            {
                //Lay nhung lop thuoc giao vien bo mon
                var lstSubjectTeacher = ClassProfileBusiness.GetListClassBySubjectTeacher(TeacherID, AcademicYearID);
                //Lay nhung lop thuoc giao vien giam thi
                var lstOverseeing = ClassProfileBusiness.GetListClassByOverSeeing(TeacherID, AcademicYearID);
                //Union Result
                var lstClass = lstSubjectTeacher.Union(lstOverseeing);
                seachQuery = from query in seachQuery
                             join calendar in CalendarBusiness.All on query.CalendarID equals calendar.CalendarID
                             join cls in lstClass on calendar.ClassID equals cls.ClassProfileID
                             select query;
            }
            //Thoi gian ap dung
            if (DaySelected.HasValue)
            {
                seachQuery = seachQuery.Where(itm => itm.ApplyDate == DaySelected);
            }
            return seachQuery;
        }
        /// <summary>
        /// Insert Calendar schedule Object vao DB
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public CalendarSchedule InsertOrUpdate(CalendarSchedule item)
        {           
            return base.Insert(item);
        }
        /// <summary>
        /// Section class
        /// </summary>
        public class ClassSection
        {
            public int Section { get; set; }

            public string SectionName
            {
                get
                {
                    if (Section == 1) return GlobalConstants.SECTION_MONING;
                    if (Section == 2) return GlobalConstants.SECTION_AFTERNOON;
                    if (Section == 3) return GlobalConstants.SECTION_EVENING;
                    return string.Empty;
                }
            }

            public ClassSection(int section)
            {
                Section = section;
            }

            public const int MORNING = 1;
            public const int AFTERNOON = 2;
            public const int NIGHT = 3;
        }

        #region Private methods
        /// <summary>
        /// Kiem tra section trong mot list
        /// </summary>
        /// <param name="_lst"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        private bool CheckSection(int section, List<ClassSection> _lst)
        {
            if (_lst.Where(a => a.Section == section).FirstOrDefault() != null) return true;
            return false;
        }
        /// <summary>
        /// Lay danh sach buoi hoc
        /// </summary>
        /// <param name="seperateID"></param>
        /// <param name="_lst"></param>
        public void GetSection(int seperateID, ref List<ClassSection> _lst)
        {
            string binary = Convert.ToString(seperateID, 2);
            if (this.CheckPosVal(binary, 3 - ClassSection.MORNING))
            {
                var itm = new ClassSection(ClassSection.MORNING);
                if (_lst.Where(a => a.Section == itm.Section).FirstOrDefault() == null) _lst.Add(itm);
            }
            if (this.CheckPosVal(binary, 3 - ClassSection.AFTERNOON))
            {
                var itm = new ClassSection(ClassSection.AFTERNOON);
                if (_lst.Where(a => a.Section == itm.Section).FirstOrDefault() == null) _lst.Add(itm);
            }
            if (this.CheckPosVal(binary, 3 - ClassSection.NIGHT))
            {
                var itm = new ClassSection(ClassSection.NIGHT);
                if (_lst.Where(a => a.Section == itm.Section).FirstOrDefault() == null) _lst.Add(itm);
            }
        }
        /// <summary>
        /// Chuyen tu luu trong DB sang buoi hoc
        /// </summary>
        /// <param name="listValue"></param>
        /// <returns></returns>
        public short Convert2Bynary(List<int> listValue)
        {
            short res = 0;
            foreach (int value in listValue)
            {
                res += (short)Math.Pow(2, value - 1);
            }
            return res;
        }
        /// <summary>
        /// Kiem tra mot ky tu tai vi tri do xem co trung voi 1
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool CheckPosVal(string s, int pos)
        {
            if (s == null || s.Equals(string.Empty))
            {
                return false;
            }
            if (s.Length < 3)
            {
                for (int i = s.Length; i < 3; i++)
                {
                    s = "0" + s;
                }
            }
            char c = s[pos];
            if (c.Equals('1'))
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
