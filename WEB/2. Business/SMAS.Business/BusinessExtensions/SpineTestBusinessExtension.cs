﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    public partial class SpineTestBusiness
    {
        #region validate
        
        private void validate(SpineTest entity)
        {
            ValidationMetadata.ValidateObject(entity);


            //RcapacityHearing: Kiểu int,  <= 9999
            //LcapacityHearing: Kiểu int, <= 9999

        }
        #endregion

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entity1">The entity1.</param>
        ///<author>Nam ta</author>
        ///<Date>12/25/2012</Date>
        public void Insert(SpineTest entity, MonitoringBook entity1)
        {
            //Thực hiện kiểm tra với PupilID = entity1.PupilID, HealthPeriodID = entity1.HealthPeriodID, SchoolID = entity1.SchoolID, AcademicYearID = entity1.AcademicYearID

            validate(entity);
            int PupilID = entity1.PupilID;
            int HealthPeriodID = entity1.HealthPeriodID.Value;
            int SchoolID = entity1.SchoolID;
            int AcademicYearID = entity1.AcademicYearID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilID;
            dic["HealthPeriodID"] = HealthPeriodID;
            dic["AcademicYearID"] = AcademicYearID;


            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(SchoolID, dic);

            if (lstMonitoringBook == null || lstMonitoringBook.Count()==0)
            {
                MonitoringBookBusiness.Insert(entity1);
                MonitoringBookBusiness.Save();
                entity.MonitoringBookID = entity1.MonitoringBookID;
                this.Insert(entity);
                this.Save();

            }
            else if (lstMonitoringBook.Count() > 0)
            {
                entity.MonitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;
                bool MonitoringBookDiplicate = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "SPINETEST",
                   new Dictionary<string, object>()
                {
                    {"MonitoringBookID",entity.MonitoringBookID},
                    {"IsActive",true}
                }, new Dictionary<string, object>()
                {
                    //{"MonitoringBookID",entity.MonitoringBookID},
                });
                if (MonitoringBookDiplicate)
                {
                    throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
                }

                entity.MonitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;
                this.Insert(entity);
                this.Save();
            }

        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entity1">The entity1.</param>
        ///<author>Nam ta</author>
        ///<Date>12/25/2012</Date>
        public void Update(SpineTest entity, MonitoringBook entity1)
        {
            //Thực hiện kiểm tra với PupilID = entity1.PupilID, HealthPeriodID = entity1.HealthPeriodID, SchoolID = entity1.SchoolID, AcademicYearID = entity1.AcademicYearID
            validate(entity);
            int PupilID = entity1.PupilID;
            int HealthPeriodID = entity1.HealthPeriodID.Value;
            int SchoolID = entity1.SchoolID;
            int AcademicYearID = entity1.AcademicYearID;

            bool MonitoringBookDiplicate = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "SpineTEST",
                   new Dictionary<string, object>()
                {
                    {"MonitoringBookID",entity.MonitoringBookID}
                }, new Dictionary<string, object>()
                {
                    {"MonitoringBookID",entity.MonitoringBookID},
                });
            if (MonitoringBookDiplicate)
            {
                throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
            }

            MonitoringBookBusiness.Update(entity1);
            MonitoringBookBusiness.Save();
            this.Update(entity);
            this.Save();


        }


        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="SpineTestID">The spine test ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        /// <exception cref="BusinessException">Common_Validate_NotCompatible</exception>
        public void Delete(int SpineTestID, int SchoolID)
        {
            this.CheckConstraints("SpineTest", GlobalConstants.HEALTH_SCHEMA, SpineTestID, "SpineTest_Validate_SpineTestID");

            SpineTest entity = this.Find(SpineTestID);
            int MonitoringBookID = entity.MonitoringBookID;
            MonitoringBook entity1 = MonitoringBookBusiness.Find(MonitoringBookID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["MonitoringBookID"] = entity.MonitoringBookID;
            SearchInfo["SchoolID"] = entity1.SchoolID;

            bool compatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            entity.ModifiedDate = DateTime.Now;
            entity.IsActive = false;

            this.Update(entity);
            this.Save();

        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{SpineTest}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        private IQueryable<SpineTest> Search(IDictionary<string, object> SearchInfo)
        {
            int SpineTestID = Utils.GetInt(SearchInfo, "SpineTestID");
            string ExaminationStraight = Utils.GetString(SearchInfo, "ExaminationStraight");
            string ExaminationItalic = Utils.GetString(SearchInfo, "ExaminationItalic");
            bool Other = Utils.GetBool(SearchInfo, "Other");
            bool? IsDeformityOfTheSpine = Utils.GetNullableBool(SearchInfo, "IsDeformityOfTheSpine");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive", true);
            int MonitoringBookID = Utils.GetInt(SearchInfo, "MonitoringBookID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int HealthPeriodID = Utils.GetInt(SearchInfo, "HealthPeriodID");
            string UnitName = Utils.GetString(SearchInfo, "UnitName");
            DateTime? MonitoringDate = Utils.GetDateTime(SearchInfo, "MonitoringDate");
            string FullName = Utils.GetString(SearchInfo, "FullName", "");
            int Grade = Utils.GetInt(SearchInfo, "EducationGrade");
            int EducationLevelID = Utils.GetInt(SearchInfo,"EducationLevelID");
            //bảng ENTTest et  join MonitoringBook mb on et.MonitoringBookID = mb.MonitoringBookID join PupilProfile pp (theo PupilID

            IQueryable<SpineTest> querry = SpineTestRepository.All;
            if (Grade > 0)
            {
                querry = from s in this.All
                         join m in MonitoringBookBusiness.All on s.MonitoringBookID equals m.MonitoringBookID
                         join e in EducationLevelBusiness.All on m.EducationLevelID equals e.EducationLevelID
                         where e.Grade == Grade
                         select s;
            }
            if (EducationLevelID != 0)
            {
                querry = querry.Where(p => p.MonitoringBook.EducationLevelID == EducationLevelID);
            }
            if (!string.IsNullOrEmpty(FullName))
            {
                querry = querry.Where(o => o.MonitoringBook.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
            if (SpineTestID != 0)
            {
                querry = querry.Where(o => o.SpineTestID == SpineTestID);
            }
            if (ExaminationStraight != "")
            {
                querry = querry.Where(o => o.ExaminationStraight == ExaminationStraight);
            }
            if (ExaminationItalic != "")
            {
                querry = querry.Where(o => o.ExaminationItalic == ExaminationItalic);
            }
           
            if (IsDeformityOfTheSpine == true)
            {
                querry = querry.Where(o => o.IsDeformityOfTheSpine == true);
            }
            //if (IsActive)
            //{
            //    querry = querry.Where(o => o.IsActive == true);
            //}
            if (MonitoringBookID != 0)
            {
                querry = querry.Where(o => o.MonitoringBookID == MonitoringBookID);
            }
            if (SchoolID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.PupilID == PupilID);
            }
            if (ClassID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.ClassID == ClassID);
            }
            if (HealthPeriodID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.HealthPeriodID == HealthPeriodID);
            }
            if (UnitName != "")
            {
                querry = querry.Where(o => o.MonitoringBook.UnitName == UnitName);
            }
            if (MonitoringDate != null)
            {
                querry = querry.Where(o => o.MonitoringBook.MonitoringDate == MonitoringDate);
            }
            if (Other == true)
            {
                querry = querry.Where(o => o.Other.Length > 0);
            }

            return querry;

        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{SpineTest}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        public IQueryable<SpineTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return this.Search(SearchInfo);
            }
        }

        /// <summary>
        /// SearchByPupil
        /// </summary>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="PupilID">The pupil ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <returns>
        /// IQueryable{SpineTestBO}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        public IQueryable<SpineTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID)
        {
            //B1: Khởi tạo IQueryable<MonitoringBook> lstMonitoringBook bằng cách gọi hàm MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID)
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID);
            //B2: Thực hiện việc tìm kiếm lstMonitoringBook mb left DentalTest dt on mb.MonitoringBookID = dt.MonitoringBookID. Kêt quả thu được IQueryable<DentalTestBO>
            var query = from mb in lstMonitoringBook
                        join dt in this.All.Where(o => o.IsActive == true) on mb.MonitoringBookID equals dt.MonitoringBookID
                        //into g1
                        //from j2 in g1.DefaultIfEmpty()
                        select new SpineTestBO
                        {
                            MonitoringBookID = mb.MonitoringBookID,
                            MonitoringBookDate = mb.MonitoringDate,
                            SpineTestID = dt.SpineTestID,
                            IsDeformityOfTheSpine = dt.IsDeformityOfTheSpine,
                            ExaminationStraight = dt.ExaminationStraight,
                            ExaminationItalic = dt.ExaminationItalic,
                            Other = dt.Other,
                            CreatedDate = dt.CreatedDate,
                            IsActive = dt.IsActive,
                            ModifiedDate = dt.ModifiedDate,
                            HealthPeriodID = mb.HealthPeriodID,
                            UnitName = mb.UnitName
                        };
            return query;
        }

        // Thuyen - 05/10/2017
        public List<SpineTest> ListSpineTestByMonitoringBookId(List<int> lstMoniBookID)
        {
            return SpineTestBusiness.All.Where(x => lstMoniBookID.Contains(x.MonitoringBookID)).ToList();
        }

    }
}