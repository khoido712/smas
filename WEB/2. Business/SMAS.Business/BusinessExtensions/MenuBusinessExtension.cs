﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Text;
namespace SMAS.Business.Business
{
    public partial class MenuBusiness
    {
        IMenuRepository MenuRepository;
        IRoleMenuRepository RoleMenuRepository;
        IGroupMenuRepository GroupMenuRepository;
        IMenuPermissionRepository MenuPermissionRepository;
        IGroupCatRepository GroupCatRepository;
        public MenuBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.MenuRepository = new MenuRepository(context);
            repository = MenuRepository;
            this.RoleMenuRepository = new RoleMenuRepository(context);
            this.GroupMenuRepository = new GroupMenuRepository(context);
            this.MenuPermissionRepository = new MenuPermissionRepository(context);
            this.GroupCatRepository = new GroupCatRepository(context);
        }

        #region Insert
        /// <summary>
        /// Tao moi menu chuc nang
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="strParentID"></param>
        /// <param name="UserAccountID"></param>
        /// <param name="listRoleID"></param>
        public void InsertMenu(Menu menu, int[] listRoleID)
        {
            //set menuPath
            if (menu.ParentID == null)
            {
                menu.ParentID = null;
                menu.MenuPath = "/";
            }
            else
            {
                menu.ParentID = menu.ParentID.Value;
                Menu menuParent = MenuRepository.All.Where(m => m.MenuID == menu.ParentID).First();
                menu.MenuPath = menuParent.MenuPath + menuParent.MenuID + "/";
            }


            menu.IsActive = true;
            menu.CreatedDate = DateTime.Now;
            //set order number of menu
            //lay so luong menu co cung ParentID
            int numberOfMenu = GetNumberOfSameParentMenu(menu.ParentID);
            numberOfMenu++;
            menu.OrderNumber = Convert.ToByte(numberOfMenu);
            //Validate 
            //Validate Object
            ValidationMetadata.ValidateObject(menu);
            //MenuName đã tồn tại 
            //this.CheckDuplicate(menu.MenuName, GlobalConstants.ADM_SCHEMA, "Menu", "MenuName", true, menu.MenuID, "Menu_Label_Name");
            //check foreignKey ton tai ?
            this.CheckAvailable(menu.ParentID, "Menu_Label_ParentName", true);

            //
            if (listRoleID != null)
            {
                foreach (var item in listRoleID)
                {
                    menu.RoleMenus.Add(new RoleMenu { RoleID = item });
                }
            }

            this.MenuRepository.Insert(menu);
            //this.MenuRepository.Save();
        }


        #endregion

        #region Edit

        public void EditMenu(Menu menu, int[] listRoleID)
        {
            /* ko cho thay doi parent Menu
            if (string.IsNullOrEmpty(strParentID))
            {
                menu.ParentID = null;
                menu.MenuPath = "/";
            }
            else
            {
                menu.ParentID = Convert.ToInt32(strParentID);
                Menu menuParent = MenuRepository.All.Where(m => m.MenuID == menu.ParentID).First();
                menu.MenuPath = menuParent.MenuPath + menuParent.MenuID + "/";
            }
            */
            //menu.CreatedUserID = user.UserAccountID;
            //menu.CreatedUserID = 1;
            // menu.IsActive = true;
            //menu.CreatedDate = DateTime.Now;
            //permission

            //Validate 
            //Validate Object
            ValidationMetadata.ValidateObject(menu);
            //MenuName đã tồn tại 
            //this.CheckDuplicate(menu.MenuName, GlobalConstants.ADM_SCHEMA, "Menu", "MenuName", true, menu.MenuID, "Menu_Label_Name");
            //check foreignKey ton tai ?
            //this.CheckAvailable(menu.ParentID, "Menu_Label_ParentName", true);
            UpdateMenuRole(menu.MenuID, listRoleID);

            //RoleMenuRepository.Save();

            /*
            //table MenuPermission
            if (menu.IsCategory == true && ChkPermission != null)
            {
                if (MenuPermissionRepository.All.Where(mp => mp.MenuID == menu.MenuID).Count() > 0)
                {
                    MenuPermission menuper = MenuPermissionRepository.All.Where(mp => mp.MenuID == menu.MenuID).FirstOrDefault();
                    if (menuper.Permission != permission)
                    {
                        menuper.Permission = permission;
                        MenuPermissionRepository.Update(menuper);
                    }
                }
                else
                {
                    MenuPermissionRepository.Insert(new MenuPermission { Permission = permission, URL = menu.URL });
                }
            }*/

            //table Menu
            
            this.MenuRepository.Update(menu);
            this.MenuRepository.Save();

        }

        #endregion

        public void UpdateMenuRole(int MenuID, int[] listRoleID)
        {
            //delete row in rolemenu table            
            IQueryable<RoleMenu> roleMenus = RoleMenuRepository.All.Where(rm => rm.MenuID == MenuID);
            if (roleMenus.Count() != 0)
            {
                foreach (var item in roleMenus.ToList())
                {
                    RoleMenuRepository.Delete(item.RoleMenuID);
                }
                RoleMenuRepository.Save();
            }
            //and insert new rows  
            if (listRoleID != null)
            {
                foreach (var item in listRoleID)
                {
                    RoleMenuRepository.Insert(new RoleMenu { MenuID = MenuID, RoleID = item });
                    /*
                    if (RoleMenuRepository.All.Where(rm => rm.MenuID == menu.MenuID && rm.RoleID == item).Count() == 0)
                    {                        
                    }
                     */
                }
                RoleMenuRepository.Save();
            }
        }

        #region Delete
        public void DeleteMenu(int id)
        {
            Menu menu = Find(id);
            //DisableMenu(menu);
            //Check Constraints truoc khi xoa
            this.CheckConstraints(GlobalConstants.ADM_SCHEMA, "Menu", id, "Menu_Label_MenuID");
            menu.IsActive = false;
            menu.ParentID = null;
            MenuRepository.Save();
        }
        #endregion

        #region private function
        private void DisableMenu(Menu menu)
        {
            menu.IsActive = false;
            var lsItems = MenuRepository.All.Where(m => m.ParentID == menu.MenuID);
            if (lsItems != null)
            {
                foreach (var item in lsItems)
                {
                    DisableMenu(item);
                }
            }

        }
        #endregion

        public bool CheckPrivilegeAssignMenuForGroup(int GroupID, int LogonUserID)
        {
            if (GroupID == 0 || LogonUserID == 0)
            {
                return false;
            }
            return GroupCatRepository.All.Where(gc => gc.GroupCatID == GroupID && gc.CreatedUserID == LogonUserID && gc.IsActive == true).Any();
        }

        public int GetNumberOfSameParentMenu(int? ParentID)
        {
            IQueryable<Menu> results = MenuRepository.All.Where(m => m.ParentID == ParentID && m.IsActive == true);
            if (results == null) return 0;
            else return results.Count();
        }

        public Menu GetMenuByControllerAndActionName(string ControllerName, string actionName, string areaName)
        {
            if (string.IsNullOrEmpty(ControllerName) || string.IsNullOrEmpty(actionName)) return null;
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(areaName)) sb = sb.Append("/").Append(areaName);
            sb.Append("/").Append(ControllerName)
                .Append("/").Append(actionName);
            IQueryable<Menu> menus = this.All.Where(m => m.URL == sb.ToString() && m.IsActive == true);
            if (menus != null)
            {
                return menus.First();
            }
            else return null;

        }

        public IQueryable<Menu> GetAllSubMenu(int MenuID)
        {
            IQueryable<Menu> result = this.repository.All;
            string str = "/" + MenuID + "/";
            result = result.Where(o => o.MenuPath.Contains(str));
            result = result.Where(o => o.IsActive == true);

            return result;
        }

        public List<Menu> GetMenuExamination(int UserAccountID, int MenuID, bool IsAdmin, int? appliedLevel)
        {
            List<Menu> listMenu = new List<Menu>();
            int roleID = this.GetRoleIDByAppliedLevel(appliedLevel);
            if (IsAdmin)
            {
                listMenu = (from menu in MenuRepository.All
                            join rm in RoleMenuRepository.All on menu.MenuID equals rm.MenuID
                            where menu.ParentID == MenuID
                            && rm.RoleID == roleID
                            && menu.IsActive
                            select menu).ToList();
            }
            else
            {
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);

                var listGroupCatID = objUserGroup.Where(p => p.RoleID == roleID).Select(u => u.GroupCatID).ToList();
                listMenu = (from menu in MenuRepository.All
                            join gmenu in GroupMenuRepository.All on menu.MenuID equals gmenu.MenuID
                            where menu.ParentID == MenuID
                            && listGroupCatID.Contains(gmenu.GroupID)
                            && (gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_DELETE
                              || gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_VIEW
                              || gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_EDIT
                              || gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_CREATE
                            )
                            && menu.IsActive
                            select menu).ToList();

            }
            return listMenu;
        }
        public List<Menu> GetMenuByReport(int UserAccountID, int MenuID, bool IsAdmin, int? appliedLevel)
        {
            List<Menu> listMenu = new List<Menu>();
            int roleID = this.GetRoleIDByAppliedLevel(appliedLevel);
            if (IsAdmin)
            {
                listMenu = (from menu in MenuRepository.All
                            join rm in RoleMenuRepository.All on menu.MenuID equals rm.MenuID
                            where menu.ParentID == MenuID
                            && rm.RoleID == roleID
                            && menu.IsActive
                            && !string.IsNullOrEmpty(menu.ProductVersion)
                            select menu).ToList();
            }
            else
            {
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);
                
                var listGroupCatID = objUserGroup.Where(p => p.RoleID == roleID).Select(u => u.GroupCatID).ToList();
                listMenu = (from menu in MenuRepository.All
                            join gmenu in GroupMenuRepository.All on menu.MenuID equals gmenu.MenuID
                            where menu.ParentID == MenuID
                            && listGroupCatID.Contains(gmenu.GroupID)
                            && (gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_DELETE
                              || gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_VIEW
                              || gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_EDIT
                              || gmenu.Permission == SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_CREATE
                            )
                            && menu.IsActive
                            && !string.IsNullOrEmpty(menu.ProductVersion)
                            select menu).ToList();

            }
            return listMenu;
        }

        public Menu GetMenuByArea(string area)
        {
            List<Menu> menuList = MenuRepository.All.Where(p => p.URL.ToLower().Contains(area.ToLower()) 
               && p.IsVisible && p.ParentID.HasValue && p.IsActive).ToList();
            Menu menuObj = menuList.Where(p => p.MenuPath.Equals("/" + p.ParentID + "/")).FirstOrDefault();
            return menuObj;
        }

        public Menu GetMenuByAreaNotVisiable(string area)
        {
            List<Menu> menuList = MenuRepository.All.Where(p => p.URL.ToLower().Contains(area.ToLower())
               && p.ParentID.HasValue && p.IsActive).ToList();
            Menu menuObj = menuList.Where(p => p.MenuPath.Equals("/" + p.ParentID + "/")).FirstOrDefault();
            return menuObj;
        }

        public Menu GetMenuByAreaNotVisiablePath2(string area)
        {
            List<Menu> menuList = MenuRepository.All.Where(p => p.URL.ToLower().Contains(area.ToLower())
               && p.ParentID.HasValue && p.IsActive).ToList();
            Menu menuObj = menuList.Where(p => p.MenuPath.Contains("/" + p.ParentID + "/")).FirstOrDefault();
            return menuObj;
        }

        private int GetRoleIDByAppliedLevel(int? appliedLevel)
        {
            int result = 0;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                result = GlobalConstants.ROLE_ADMIN_TRUONGC1;
            }else if(appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                result = GlobalConstants.ROLE_ADMIN_TRUONGC2;
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                result = GlobalConstants.ROLE_ADMIN_TRUONGC3;
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                result = GlobalConstants.ROLE_ADMIN_NHATRE;
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                result = GlobalConstants.ROLE_ADMIN_MAUGIAO;
            }
            return result;
        }
    }
}