/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class EthnicBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int ETHNICCODE_MAX_LENGTH = 10; //Độ dài mã tỉnh thành phố
        private const int ETHNICNAME_MAX_LENGTH = 50; //Độ dài tên tỉnh thành phố
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn
        private const int VALUE_MIN = 1;//Giá trị min
        private const int VALUE_MAX = 3;//Giá trị max
        #endregion

        #region Search


        /// <summary>
        /// Tìm kiếm dân tộc
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="EthnicName">Tên dân tộc</param>
        /// <param name="EthnicCode">Mã dân tộc</param>
        /// <param name="Description">Mô tả.</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <param name="IsMinority">Biến xác định có phải dân tộc thiểu số hay ko</param>
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<Ethnic> Search(IDictionary<string, object> dic)
        {
            string EthnicName = Utils.GetString(dic, "EthnicName");
            string EthnicCode = Utils.GetString(dic, "EthnicCode");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            bool? IsMinority = Utils.GetNullableBool(dic, "IsMinority");
            IQueryable<Ethnic> lsEthnic = EthnicRepository.All;
            if (IsActive.HasValue)
            { 
                lsEthnic = lsEthnic.Where(eth => eth.IsActive == IsActive); 
            }
            if (IsMinority.HasValue)
            { 
                lsEthnic = lsEthnic.Where(eth => eth.IsMinority == IsMinority);
            }
            if (EthnicName.Trim().Length != 0)
            { 
                lsEthnic = lsEthnic.Where(eth => eth.EthnicName.Contains(EthnicName)); 
            }
            if (EthnicCode.Trim().Length != 0)
            { 
                lsEthnic = lsEthnic.Where(eth => eth.EthnicCode.Contains(EthnicCode)); 
            }
            if (Description.Trim().Length != 0)
            { 
                lsEthnic = lsEthnic.Where(eth => eth.Description.Contains(Description));
            }

            return lsEthnic;

        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa dân tộc
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="EthnicID"></param>
        public void Delete(int EthnicID)
        {
            //Bạn chưa chọn Dân tộc cần xóa hoặc Dân tộc bạn chọn đã bị xóa khỏi hệ thống
            new EthnicBusiness(null).CheckAvailable((int)EthnicID, "Ethnic_Label_EthnicID", true);

            //Không thể xóa Dân tộc đang sử dụng
            new EthnicBusiness(null).CheckConstraintsWithIsActive(GlobalConstants.LIST_SCHEMA, "Ethnic", EthnicID, "Ethnic_Label_EthnicID");
            base.Delete(EthnicID, true);
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm dân tộc
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ethnic">Đối tượng Ethnic</param>
        /// <returns>Đối tượng Ethnic</returns>
        public override Ethnic Insert(Ethnic ethnic)
        {
            //Mã dân tộc không được để trống 
            Utils.ValidateRequire(ethnic.EthnicCode, "Ethnic_Label_EthnicCode");
            //Độ dài mã dân tộc không được vượt quá 10 
            Utils.ValidateMaxLength(ethnic.EthnicCode, ETHNICCODE_MAX_LENGTH, "Ethnic_Label_EthnicCode");
            //Mã dân tộc đã tồn tại 
            new EthnicBusiness(null).CheckDuplicate(ethnic.EthnicCode, GlobalConstants.LIST_SCHEMA, "Ethnic", "EthnicCode", true, ethnic.EthnicID, "Ethnic_Label_EthnicCode");
            //Tên dân tộc không được để trống 
            Utils.ValidateRequire(ethnic.EthnicName, "Ethnic_Label_EthnicName");
            //Độ dài trường tên dân tộc không được vượt quá 50 
            Utils.ValidateMaxLength(ethnic.EthnicName, ETHNICNAME_MAX_LENGTH, "Ethnic_Label_EthnicName");
            //Tên dân tộc đã tồn tại 
            new EthnicBusiness(null).CheckDuplicate(ethnic.EthnicName, GlobalConstants.LIST_SCHEMA, "Ethnic", "EthnicName", true, ethnic.EthnicID, "Ethnic_Label_EthnicName");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(ethnic.Description, DESCRIPTION_MAX_LENGTH, "Description");


            //Insert
            return base.Insert(ethnic);

        }
        #endregion


        #region Update
        /// <summary>
        /// Sửa dân tộc
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ethnic">Đối tượng Ethnic</param>
        /// <returns>Đối tượng Ethnic</returns>
        public override Ethnic Update(Ethnic ethnic)
        {
            //Bạn chưa chọn dân tộc cần sửa hoặc dân tộc bạn chọn đã bị xóa khỏi hệ thống
            new EthnicBusiness(null).CheckAvailable((int)ethnic.EthnicID, "Ethnic_Label_EthnicID", true);
            //Mã dân tộc không được để trống 
            Utils.ValidateRequire(ethnic.EthnicCode, "Ethnic_Label_EthnicCode");
            //Độ dài mã dân tộc không được vượt quá 10 
            Utils.ValidateMaxLength(ethnic.EthnicCode, ETHNICCODE_MAX_LENGTH, "Ethnic_Label_EthnicCode");
            //Mã dân tộc đã tồn tại 
            new EthnicBusiness(null).CheckDuplicate(ethnic.EthnicCode, GlobalConstants.LIST_SCHEMA, "Ethnic", "EthnicCode", true, ethnic.EthnicID, "Ethnic_Label_EthnicCode");
            //Tên dân tộc không được để trống 
            Utils.ValidateRequire(ethnic.EthnicName, "Ethnic_Label_EthnicName");
            //Độ dài trường tên dân tộc không được vượt quá 50 
            Utils.ValidateMaxLength(ethnic.EthnicName, ETHNICNAME_MAX_LENGTH, "Ethnic_Label_EthnicName");
            //Tên dân tộc đã tồn tại 
            new EthnicBusiness(null).CheckDuplicate(ethnic.EthnicName, GlobalConstants.LIST_SCHEMA, "Ethnic", "EthnicName", true, ethnic.EthnicID, "Ethnic_Label_EthnicName");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(ethnic.Description, DESCRIPTION_MAX_LENGTH, "Description");


            return base.Update(ethnic);
        }
        #endregion
    }
}
