﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author AuNH
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class EmployeePraiseDisciplineBusiness
    {
        public void Validate(EmployeePraiseDiscipline EmployeePraiseDiscipline, int type)
        {
            ValidationMetadata.ValidateObject(EmployeePraiseDiscipline);

            // Kiểm tra SchoolFaculty có tồn tại không
            SchoolFacultyBusiness.CheckAvailable(EmployeePraiseDiscipline.SchoolFacultyID,"EmployeePraiseDiscipline_Label_SchoolFaculty", true);
            // Kiểm tra sự tồn tại EmployeeID
            EmployeeBusiness.CheckAvailable(EmployeePraiseDiscipline.EmployeeID, "EmployeePraiseDiscipline_Label_Employee", true);
            

            // Kiểm tra thời gian quyến định phải nằm trong khoảng thời gian năm học
            AcademicYear AcademicYear = null;
            if (EmployeePraiseDiscipline.AcademicYear == null)
            {
                AcademicYear = new AcademicYearRepository(this.context).Find(EmployeePraiseDiscipline.AcademicYearID);
            }
            else
            {
                AcademicYear = EmployeePraiseDiscipline.AcademicYear;
            }
            if (EmployeePraiseDiscipline.StartDate < AcademicYear.FirstSemesterStartDate || EmployeePraiseDiscipline.StartDate > AcademicYear.SecondSemesterEndDate)
            {
                List<object> Params = new List<object>();
                Params.Add("EmployeePraiseDiscipline_Label_StartDate");
                Params.Add(AcademicYear.FirstSemesterStartDate);
                Params.Add(AcademicYear.SecondSemesterEndDate);
                if (type == 1)
                {
                    throw new BusinessException("EmployeePraise_StartDate_Not_InAcademicYear_Time", Params);
                }
                else
                {
                    throw new BusinessException("EmployeeDiscipline_StartDate_Not_InAcademicYear_Time", Params);
                }
            }

            // Ngày quyết định khen thưởng phải nhỏ hơn ngày hiện tại
            if (EmployeePraiseDiscipline.IsDiscipline == false)
            {
                Utils.ValidatePastDate(EmployeePraiseDiscipline.StartDate, "EmployeePraiseDiscipline_Label_StartDate");
            }
            else
            {
                Utils.ValidatePastDate(EmployeePraiseDiscipline.StartDate, "EmployeeDiscipline_Label_StartDate");
            }

        }
        public EmployeePraiseDiscipline InsertPraise(EmployeePraiseDiscipline EmployeePraiseDiscipline)
        {
            int type = 1;
            Validate(EmployeePraiseDiscipline, type);
            //Lấy FacultyID theo EmployeeID
            Employee ep = EmployeeBusiness.Find(EmployeePraiseDiscipline.EmployeeID);
            EmployeePraiseDiscipline.SchoolFacultyID = ep.SchoolFacultyID;
            //IsDiscipline = FALSE
            EmployeePraiseDiscipline.IsDiscipline = false;
            //Thực hiện insert vào bảng EmployeePraiseDiscipline

            return base.Insert(EmployeePraiseDiscipline);
             
        }

        public EmployeePraiseDiscipline UpdatePraise(EmployeePraiseDiscipline EmployeePraiseDiscipline)
        {
            int type = 1;
            Validate(EmployeePraiseDiscipline, type);
            //EmployeePraiseDisciplineID: PK(EmployeePraiseDiscipline)
            CheckAvailable(EmployeePraiseDiscipline.EmployeePraiseDisciplineID, "EmployeePraiseDiscipline_Label_EmployeePraiseDisciplineID", false);
            //EmployeePraiseDisciplineID, SchoolID: not compatiable(EmployeePraiseDiscipline)
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeePraiseDisciplineID"] = EmployeePraiseDiscipline.EmployeePraiseDisciplineID;
            SearchInfo["SchoolID"] = EmployeePraiseDiscipline.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeePraiseDiscipline", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //EmployeePraiseDisciplineID, AcademicYearID: not compatiable(EmployeePraiseDiscipline)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["EmployeePraiseDisciplineID"] = EmployeePraiseDiscipline.EmployeePraiseDisciplineID;
            SearchInfo2["AcademicYearID"] = EmployeePraiseDiscipline.AcademicYearID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeePraiseDiscipline", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //EmployeePraiseDiscipline(EmployeePraiseDisciplineID).IsDiscipline (lấy trong CSDL) chỉ được nhận giá trị FALSE
            EmployeePraiseDiscipline epd = EmployeePraiseDisciplineRepository.Find(EmployeePraiseDiscipline.EmployeePraiseDisciplineID);
            if (epd.IsDiscipline != false)
            {
                
                throw new BusinessException("Common_Validate_NotEqual");
            }

            return base.Update(EmployeePraiseDiscipline);
            
        }

        public void DeletePraise(int EmployeePraiseDisciplineID,int SchoolID, int AcademicYeaerID)
        {
            //EmployeePraiseDisciplineID: PK(EmployeePraiseDiscipline)
            CheckAvailable(EmployeePraiseDisciplineID, "EmployeePraiseDiscipline_Label_EmployeePraiseDisciplineID", false);
            //EmployeePraiseDiscipline, SchoolID: not compatiable(EmployeePraiseDiscipline)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeePraiseDisciplineID"] = EmployeePraiseDisciplineID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeePraiseDiscipline", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (AcademicYearBusiness.IsCurrentYear(AcademicYeaerID) == false)
            {
                throw new BusinessException("Common_Validate_NotEqual");
            }
            //EmployeePraiseDiscipline(EmployeePraiseDisciplineID).IsDiscipline (lấy trong CSDL) chỉ được nhận giá trị FALSE
            EmployeePraiseDiscipline epd = EmployeePraiseDisciplineRepository.Find(EmployeePraiseDisciplineID);
            if (epd.IsDiscipline != false)
            {
                
                throw new BusinessException("Common_Validate_NotEqual");
            }
            
           
            base.Delete(EmployeePraiseDisciplineID);
        }

        public IQueryable<EmployeePraiseDiscipline> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolFacultyID = Utils.GetInt(SearchInfo, "SchoolFacultyID");
            bool? IsDiscipline = Utils.GetNullableBool(SearchInfo, "IsDiscipline");
            int EmployeeID = Utils.GetInt(SearchInfo, "EmployeeID");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");


            IQueryable<EmployeePraiseDiscipline> Query = this.All;

            if (SchoolFacultyID > 0)
            {
                Query = Query.Where(o => o.SchoolFacultyID == SchoolFacultyID);
            }

            if (IsDiscipline != null)
            {
                Query = Query.Where(o => o.IsDiscipline == IsDiscipline);
            }

            if (EmployeeID > 0)
            {
                Query = Query.Where(o => o.EmployeeID == EmployeeID);
            }

            if (FullName != "")
            {
                Query = Query.Where(o => o.Employee.FullName.ToLower().Contains(FullName.ToLower()));
            }

            if (AcademicYearID > 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (SchoolID > 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }

            return Query;
        }

        public IQueryable<EmployeePraiseDiscipline> SearchPraise(IDictionary<string, object> SearchInfo, int SchoolID)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["IsDiscipline"] = false;
                return this.Search(SearchInfo);
            }
        }

        public EmployeePraiseDiscipline InsertDiscipline(EmployeePraiseDiscipline EmployeePraiseDiscipline)
        {
            int type = 2;
            Validate(EmployeePraiseDiscipline,  type);
            //Lấy FacultyID theo EmployeeID
            Employee ep = EmployeeBusiness.Find(EmployeePraiseDiscipline.EmployeeID);
            EmployeePraiseDiscipline.SchoolFacultyID = ep.SchoolFacultyID;
            //IsDiscipline = FALSE
            EmployeePraiseDiscipline.IsDiscipline = true;
            //Thực hiện insert vào bảng EmployeePraiseDiscipline

            return base.Insert(EmployeePraiseDiscipline);
            
        }

        public EmployeePraiseDiscipline UpdateDiscipline(EmployeePraiseDiscipline EmployeePraiseDiscipline)
        {
            int type = 2;
            Validate(EmployeePraiseDiscipline, type);
            //EmployeePraiseDisciplineID: PK(EmployeePraiseDiscipline)
            CheckAvailable(EmployeePraiseDiscipline.EmployeePraiseDisciplineID, "EmployeePraiseDiscipline_Label_EmployeePraiseDisciplineID", false);
            //EmployeePraiseDisciplineID, SchoolID: not compatiable(EmployeePraiseDiscipline)
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeePraiseDisciplineID"] = EmployeePraiseDiscipline.EmployeePraiseDisciplineID;
            SearchInfo["SchoolID"] = EmployeePraiseDiscipline.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeePraiseDiscipline", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //EmployeePraiseDisciplineID, AcademicYearID: not compatiable(EmployeePraiseDiscipline)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["EmployeePraiseDisciplineID"] = EmployeePraiseDiscipline.EmployeePraiseDisciplineID;
            SearchInfo2["AcademicYearID"] = EmployeePraiseDiscipline.AcademicYearID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeePraiseDiscipline", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //EmployeePraiseDiscipline(EmployeePraiseDisciplineID).IsDiscipline (lấy trong CSDL) chỉ được nhận giá trị FALSE
            EmployeePraiseDiscipline epd = EmployeePraiseDisciplineRepository.Find(EmployeePraiseDiscipline.EmployeePraiseDisciplineID);
            if (epd.IsDiscipline != true)
            {
                
                throw new BusinessException("Common_Validate_NotEqual");
            }

            return base.Update(EmployeePraiseDiscipline);

        }
        public void DeleteDiscipline(int EmployeePraiseDisciplineID, int SchoolID, int AcademicYeaerID)
        {
            //EmployeePraiseDisciplineID: PK(EmployeePraiseDiscipline)
            CheckAvailable(EmployeePraiseDisciplineID, "EmployeePraiseDiscipline_Label_EmployeePraiseDisciplineID", false);
            //EmployeePraiseDiscipline, SchoolID: not compatiable(EmployeePraiseDiscipline)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeePraiseDisciplineID"] = EmployeePraiseDisciplineID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeePraiseDiscipline", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (this.Find(EmployeePraiseDisciplineID).AcademicYearID != AcademicYeaerID)
            {
                List<object> Params = new List<object>();
                Params.Add("EmployeePraiseDiscipline_Label_AcademicYear");
                throw new BusinessException("Common_Validate_Permission", Params);
            }
            //EmployeePraiseDiscipline(EmployeePraiseDisciplineID).IsDiscipline (lấy trong CSDL) chỉ được nhận giá trị FALSE
            EmployeePraiseDiscipline epd = EmployeePraiseDisciplineRepository.Find(EmployeePraiseDisciplineID);
            if (epd.IsDiscipline != true)
            {
               
                throw new BusinessException("Common_Validate_NotEqual");
            }


            base.Delete(EmployeePraiseDisciplineID);
        }

        public IQueryable<EmployeePraiseDiscipline> SearchDiscipline(IDictionary<string, object> SearchInfo, int SchoolID)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["IsDiscipline"] = true;
                return this.Search(SearchInfo);
            }
        }
    }
}