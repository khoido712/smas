﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SchoolSubsidiaryBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường loại đánh giá xếp loại cán bộ
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int ABBREVIATION_MAX_LENGTH = 10; //Độ dài trường tên viết tắt
        private const int SUBSIDIARYCODE_MAX_LENGTH = 120; //Độ dài mã trường phụ
        private const int SUBSIDIARYNAME_MAX_LENGTH = 200; //Độ dài tên trường phụ
        private const int ADDRESS_MAX_LENGTH = 200; // Độ dài địa chỉ trường phụ
        #endregion
        #region Search
        /// <summary>
        /// Tìm kiếm trường phụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="SubsidiaryCode">Mã trường phụ</param>
        /// <param name="SubsidiaryName">Tên trường phụ</param>
        /// <param name="Address">Địa chỉ trường phụ</param>
        /// <param name="Telephone">Điện thoại</param>
        /// <param name="Fax">Fax</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<SchoolSubsidiary></returns>
        public IQueryable<SchoolSubsidiary> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string SubsidiaryCode = Utils.GetString(dic, "SubsidiaryCode");
            string SubsidiaryName = Utils.GetString(dic, "SubsidiaryName");
            string Address = Utils.GetString(dic, "Address");
            string Telephone = Utils.GetString(dic, "Telephone");
            string Fax = Utils.GetString(dic, "Fax");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryRepository.All;

            if (IsActive.HasValue)
            {
                lsSchoolSubsidiary = lsSchoolSubsidiary.Where(dis => dis.IsActive==IsActive);
            }
            if (SubsidiaryCode.Trim().Length != 0)
                lsSchoolSubsidiary =lsSchoolSubsidiary.Where(dis => dis.SubsidiaryCode.Contains(SubsidiaryCode.ToLower()));
            if (SubsidiaryName.Trim().Length != 0)
                lsSchoolSubsidiary =lsSchoolSubsidiary.Where(dis => dis.SubsidiaryName.Contains(SubsidiaryName.ToLower()));
            if (Address.Trim().Length != 0)
                lsSchoolSubsidiary =lsSchoolSubsidiary.Where(dis => dis.Address.Contains(Address.ToLower()));
            if (Telephone.Trim().Length != 0)
                lsSchoolSubsidiary =lsSchoolSubsidiary.Where(dis => dis.Telephone.Contains(Telephone.ToLower()));
            if (Fax.Trim().Length != 0)
                lsSchoolSubsidiary =lsSchoolSubsidiary.Where(dis => dis.Fax.Contains(Fax.ToLower()));

            if (SchoolID != 0)
                lsSchoolSubsidiary =lsSchoolSubsidiary.Where(dis => dis.SchoolID == SchoolID);

            
            return lsSchoolSubsidiary;

        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm trường phụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="schoolSubsidiary"> Đối tượng trường phụ</param>
        /// <returns>Đối tượng trường phụ</returns>
        public override SchoolSubsidiary Insert(SchoolSubsidiary schoolSubsidiary)
        { 
        //Mã trường phụ không được để trống - Kiểm tra SubsidiaryCode
            Utils.ValidateRequire(schoolSubsidiary.SubsidiaryCode, "SchoolSubsidiary_Label_SubsidiaryCode");
        //Tên trường phụ không được để trống – Kiểm tra SubsidiaryName
            Utils.ValidateRequire(schoolSubsidiary.SubsidiaryName, "SchoolSubsidiary_Label_SubsidiaryName");
        //Địa chỉ trường phụ không được để trống – Kiểm tra Address
        //    Utils.ValidateRequire(schoolSubsidiary.Address, "SchoolSubsidiary_Label_Address");
        //Diện tích điểm trường phụ không được để trống – Kiểm tra SquareArea
        //    Utils.ValidateRequire(schoolSubsidiary.SquareArea, "SchoolSubsidiary_Label_SquareArea");
        //Khoảng cách từ trường chính không được để trống – Kiểm tra DistanceFromHQ
        //    Utils.ValidateRequire(schoolSubsidiary.DistanceFromHQ, "SchoolSubsidiary_Label_DistanceFromHQ");
        //Mã trường phụ không được nhập quá 120 ký tự - Kiểm tra SubsidiaryCode
            Utils.ValidateMaxLength(schoolSubsidiary.SubsidiaryCode,SUBSIDIARYCODE_MAX_LENGTH, "SchoolSubsidiary_Label_SubsidiaryCode");
        //Tên trường phụ không được nhập quá 200 ký tự - Kiểm tra SubsidiaryName
            Utils.ValidateMaxLength(schoolSubsidiary.SubsidiaryName, SUBSIDIARYNAME_MAX_LENGTH, "SchoolSubsidiary_Label_SubsidiaryName");
        //Địa chỉ không được nhập quá 200 ký tự - Kiểm tra Address
        //    Utils.ValidateMaxLength(schoolSubsidiary.Address,ADDRESS_MAX_LENGTH , "SchoolSubsidiary_Label_Address");
        //Bạn chưa nhập thông tin trường chính. Nhập thông tin trường chính trước khi nhập thông tin trường phụ. Kiểm tra trường SchoolID
            SchoolProfileBusiness.CheckAvailable(schoolSubsidiary.SchoolID, "SchoolSubsidiary_Label_SchoolID",true);
          
          //  //Diện tích phải nhập kiểu số. – Kiểm tra SquareArea
          //  Utils.ValidateIsNumber(schoolSubsidiary.SquareArea.ToString(), "SchoolSubsidiary_Label_SquareArea");
          ////  Khoảng cách phải nhập kiểu số. – Kiểm tra DistanceFromHQ
          //  Utils.ValidateIsNumber(schoolSubsidiary.DistanceFromHQ.ToString(), "SchoolSubsidiary_Label_SquareArea");
       
        //Mã trường phụ đã tồn tại. Kiểm tra SubsidiaryCode
            new SchoolSubsidiaryBusiness(null).CheckDuplicateCouple(schoolSubsidiary.SubsidiaryCode,schoolSubsidiary.SchoolID.ToString(), GlobalConstants.SCHOOL_SCHEMA, "SchoolSubsidiary", "SubsidiaryCode","SchoolID", true, schoolSubsidiary.SchoolSubsidiaryID, "SchoolSubsidiary_Label_SubsidiaryCode");
            
            
            return base.Insert(schoolSubsidiary);
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa trường phụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="schoolSubsidiary"> Đối tượng trường phụ</param>
        /// <returns>Đối tượng trường phụ</returns>
        public override SchoolSubsidiary Update(SchoolSubsidiary schoolSubsidiary)
        {
            //Đối tượng không tồn tại
            new SchoolSubsidiaryBusiness(null).CheckAvailable(schoolSubsidiary.SchoolSubsidiaryID, "SchoolSubsidiary_Label_SchoolID", true);
            //Mã trường phụ không được để trống - Kiểm tra SubsidiaryCode
            Utils.ValidateRequire(schoolSubsidiary.SubsidiaryCode, "SchoolSubsidiary_Label_SubsidiaryCode");
            //Tên trường phụ không được để trống – Kiểm tra SubsidiaryName
            Utils.ValidateRequire(schoolSubsidiary.SubsidiaryName, "SchoolSubsidiary_Label_SubsidiaryName");
            //Địa chỉ trường phụ không được để trống – Kiểm tra Address
           // Utils.ValidateRequire(schoolSubsidiary.Address, "SchoolSubsidiary_Label_Address");
            //Diện tích điểm trường phụ không được để trống – Kiểm tra SquareArea
           // Utils.ValidateRequire(schoolSubsidiary.SquareArea, "SchoolSubsidiary_Label_SquareArea");
            //Khoảng cách từ trường chính không được để trống – Kiểm tra DistanceFromHQ
          //  Utils.ValidateRequire(schoolSubsidiary.DistanceFromHQ, "SchoolSubsidiary_Label_DistanceFromHQ");
           // Mã trường phụ không được nhập quá 120 ký tự - Kiểm tra SubsidiaryCode
            Utils.ValidateMaxLength(schoolSubsidiary.SubsidiaryCode, SUBSIDIARYCODE_MAX_LENGTH, "SchoolSubsidiary_Label_SubsidiaryCode");
            //Tên trường phụ không được nhập quá 200 ký tự - Kiểm tra SubsidiaryName
            Utils.ValidateMaxLength(schoolSubsidiary.SubsidiaryName, SUBSIDIARYNAME_MAX_LENGTH, "SchoolSubsidiary_Label_SubsidiaryName");
            //Địa chỉ không được nhập quá 200 ký tự - Kiểm tra Address
            Utils.ValidateMaxLength(schoolSubsidiary.Address, ADDRESS_MAX_LENGTH, "SchoolSubsidiary_Label_Address");
            //Bạn chưa nhập thông tin trường chính. Nhập thông tin trường chính trước khi nhập thông tin trường phụ. Kiểm tra trường SchoolID
            SchoolProfileBusiness.CheckAvailable(schoolSubsidiary.SchoolID, "SchoolSubsidiary_Label_SchoolID", true);
            ////Diện tích phải nhập kiểu số. – Kiểm tra SquareArea
            //Utils.ValidateIsNumber(schoolSubsidiary.SquareArea.ToString(), "SchoolSubsidiary_Label_SquareArea");
            ////  Khoảng cách phải nhập kiểu số. – Kiểm tra DistanceFromHQ
            //Utils.ValidateIsNumber(schoolSubsidiary.DistanceFromHQ.ToString(), "SchoolSubsidiary_Label_SquareArea");

            return base.Update(schoolSubsidiary);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa trường phụ
        /// </summary>
        /// <param name="SchoolSubsidiaryID">ID trường phụ</param>
        /// <date>4/9/2012</date>
        /// <author>
        /// hath
        /// </author>
        public void Delete(int SchoolSubsidiaryID)
        {
            //Kiểm tra SchoolSubsidiaryID != null
            Utils.ValidateNull(SchoolSubsidiaryID, "SchoolSubsidiary_Label_SchoolSubsidiaryID");
            if (SchoolSubsidiaryRepository.All.Where(sch => sch.SchoolSubsidiaryID == SchoolSubsidiaryID && sch.IsActive == true).Count() == 0)
            {
                return;
            }
            base.Delete(SchoolSubsidiaryID,true);
        }
        #endregion


    }
}