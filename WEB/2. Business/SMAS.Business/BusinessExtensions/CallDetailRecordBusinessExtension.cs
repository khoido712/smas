﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class CallDetailRecordBusiness
    {
        public int Insert(List<CallDetailRecord> lstCallDetailRecord)
        {
            return -1;
        }

        /// <summary>
        /// Tìm kiếm thông tin CallDetailRecord
        /// </summary>
        /// <author>BaLX</author>
        /// <date>21/12/2012</date>
        /// <param name="dic">Điều kiện tìm kiếm</param>
        /// <returns>Danh sách CallDetailRecord</returns>

        public IQueryable<CallDetailRecordBO> Search(IDictionary<string, object> dic)
        {
            string ContractCode = Utils.GetString(dic, "ContractCode");
            string SchoolCode = Utils.GetString(dic, "SchoolCode");
            string PackageCode = Utils.GetString(dic, "PackageCode");
            string SupervisingDeptCode = Utils.GetString(dic, "SupervisingDeptCode");
            int? Status = Utils.GetNullableInt(dic, "Status");

            // bắt đầu tìm kiếm
            var query = from record in CallDetailRecordRepository.All
                        join contract in SMSTeacherContractRepository.All on record.SMSTeacherContractID equals contract.SMSTeacherContractID
                        select new CallDetailRecordBO
                        {
                            CallDetailRecord = record,
                            ContractCode = contract.ContractCode,
                            SchoolCode = contract.SchoolCode,
                            PackageID = contract.PackageID,
                            PackageCode = contract.Package.PackageCode,
                            SupervisingDeptCode = contract.SupervisingDeptCode,
                            Status = contract.Status
                        };

            if (ContractCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.ContractCode.ToLower().Contains(ContractCode.ToLower())));
            }
            if (SchoolCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.SchoolCode.ToLower().Contains(SchoolCode.ToLower())));
            }
            if (PackageCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.PackageCode.ToLower().Contains(PackageCode.ToLower())));
            }
            if (SupervisingDeptCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.SupervisingDeptCode.ToLower().Contains(SupervisingDeptCode.ToLower())));
            }
            if (Status.HasValue && Status != -1)
            {
                query = query.Where(m => (m.Status == Status));
            }

            return query;
        }

        public List<CallDetailRecordBO> GetCDROfSMSTeacherContract(SMSTeacherContractBO SearchObject)
        {
            if (String.IsNullOrEmpty(SearchObject.SchoolCode)
                && String.IsNullOrEmpty(SearchObject.SupervisingDeptCode))
            {
                return null;
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolCode"] = SearchObject.SchoolCode;
            SearchInfo["SupervisingDeptCode"] = SearchObject.SupervisingDeptCode;
            return Search(SearchInfo).ToList();
        }

    }
}