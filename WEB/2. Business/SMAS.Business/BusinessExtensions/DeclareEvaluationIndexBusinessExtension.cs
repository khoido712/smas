﻿using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using System;
namespace SMAS.Business.Business
{
    public partial class DeclareEvaluationIndexBusiness
    {
        #region Lay danh sach linh vuc cua mot user
        public List<DeclareEvaluationIndexBO> GetListDeclareEvaluationIndexByEvaluation(
            int AcademicYearID,
            int SchoolID,
            int AppliedLevel,
            int EducationLevelID,
            int DeclareEvaluationGroupID)
        {
            // tìm lĩnh vực theo trường
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["AcademicYearID"] = AcademicYearID;
            dicClassSubject["AppliedLevel"] = AppliedLevel;
            dicClassSubject["EducationLevel"] = EducationLevelID;
            dicClassSubject["DeclareEvaluationGroupID"] = DeclareEvaluationGroupID;
            return this.SearchBySchool(SchoolID, dicClassSubject).ToList();

        }
        #endregion


        #region Tim kiem linh vuc trong lop hoc theo truong

        /// <summary>
        /// Tim kiem linh vuc cua lop theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DeclareEvaluationIndexBO> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        #endregion Tim kiem linh vuc trong lop hoc theo truong


        #region Tim kiem
        /// <summary>
        /// Tim kiem linh vuc trong lop.
        /// Khong goi truc tiep ham search ma hoi qua SearchBySchool de an partition
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DeclareEvaluationIndexBO> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevel");
            int DeclareEvalGroID = Utils.GetInt(SearchInfo, "DeclareEvaluationGroupID");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            List<int> ListEvaluationGroupID = Utils.GetIntList(SearchInfo, "ListEvaluationGroupID");

            IQueryable<DeclareEvaluationIndexBO> listResultBO = null;

            IQueryable<DeclareEvaluationGroup> lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.All;
            IQueryable<DeclareEvaluationIndex> lstDeclareEvaluationIndex = DeclareEvaluationIndexRepository.All;
            IQueryable<EvaluationDevelopmentGroup> lstEvaluationDevelopmentGro = EvaluationDevelopmentGroupBusiness.All.Where(x => x.IsActive == true);
            IQueryable<EvaluationDevelopment> lstEvaluationDevelopment = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true 
                                                                                                                    && x.EvaluationDevelopmentCode.Trim().Length != 0);

            if (SchoolID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.SchoolID == SchoolID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.SchoolID == SchoolID);
            }

            if (AcademicYearID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.AcademicYearID == AcademicYearID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (AppliedLevel != 0)
            {
                lstEvaluationDevelopmentGro.Where(x => x.AppliedLevel == AppliedLevel);
            }
            if (EducationLevelID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EducationLevelID == EducationLevelID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.EducationLevelID == EducationLevelID);
                lstEvaluationDevelopmentGro = lstEvaluationDevelopmentGro.Where(x => x.EducationLevelID == EducationLevelID);
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.EducationLevelID == EducationLevelID);
            }

            if (DeclareEvalGroID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EvaluationGroupID == DeclareEvalGroID);
                lstEvaluationDevelopmentGro = lstEvaluationDevelopmentGro.Where(x => x.EvaluationDevelopmentGroupID == DeclareEvalGroID);
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.EvaluationDevelopmentGroupID == DeclareEvalGroID);
            }

            if (ListEvaluationGroupID.Count() > 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => ListEvaluationGroupID.Contains(x.EvaluationGroupID));
                lstEvaluationDevelopmentGro = lstEvaluationDevelopmentGro.Where(x => ListEvaluationGroupID.Contains(x.EvaluationDevelopmentGroupID));
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => ListEvaluationGroupID.Contains(x.EvaluationDevelopmentGroupID));
            }

            if (ProvinceID != 0)
            {
                var temptEDG = lstEvaluationDevelopmentGro.Where(x => x.ProvinceID == ProvinceID).FirstOrDefault();
                if (temptEDG != null)
                {
                    lstEvaluationDevelopmentGro = lstEvaluationDevelopmentGro.Where(x => x.ProvinceID == ProvinceID);
                }
                else
                {
                    lstEvaluationDevelopmentGro = lstEvaluationDevelopmentGro.Where(x => !x.ProvinceID.HasValue || x.ProvinceID == SystemParamsInFile.COUNTRY);
                }

                var temptED = lstEvaluationDevelopmentGro.Where(x => x.ProvinceID == ProvinceID);
                if (temptED.Count() > 0)
                {
                    lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.ProvinceID == ProvinceID);
                }
                else
                {
                    lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => !x.ProvinceID.HasValue || x.ProvinceID == SystemParamsInFile.COUNTRY);
                }
            }

            listResultBO = (from lstED in lstEvaluationDevelopment
                            join lstEDG in lstEvaluationDevelopmentGro
                             on lstED.EvaluationDevelopmentGroupID equals lstEDG.EvaluationDevelopmentGroupID
                            join lstDEG in lstDeclareEvaluationGroup
                            on lstEDG.EvaluationDevelopmentGroupID equals lstDEG.EvaluationGroupID
                            join lstDEI in lstDeclareEvaluationIndex
                             on new { X = lstED.EvaluationDevelopmentID, Y = lstED.EvaluationDevelopmentGroupID }
                                equals new { X = lstDEI.EvaluationDevelopmentID, Y = lstDEI.EvaluationDevelopmentGroID }
                            select new DeclareEvaluationIndexBO
                            {
                                DeclareEvaluationIndexID = lstDEI.DeclareEvaluationIndexID,
                                DeclareEvaluationGroupID = lstDEG.DeclareEvaluationGroupID,
                                EvaluationDevGroupID = lstEDG.EvaluationDevelopmentGroupID,
                                EvaluationDevIndexID = lstED.EvaluationDevelopmentID,
                                EvaluationDevelopmentID = lstED.EvaluationDevelopmentID,
                                EvaluationGroupID = lstDEG.EvaluationGroupID,
                                CurrentSchoolID = lstDEG.SchoolID,
                                CurrentAcademicYearID = lstDEG.AcademicYearID,
                                CreateDate = lstDEG.CreateDate,
                                ModfiedDate = lstDEG.ModifiedDate,
                                Name = lstED.EvaluationDevelopmentName,
                                Description = lstED.Description,
                                EvaluationDevCode = lstED.EvaluationDevelopmentCode,
                                DescriptionIndex = !string.IsNullOrEmpty(lstED.Description) ? lstED.Description : lstED.EvaluationDevelopmentName,
                                OrderID = lstED.OrderID,
                                OrderIDEvaluationGroup = lstEDG.OrderID,
                                AppliedLevel = lstEDG.AppliedLevel,
                                ClassID = lstDEI.ClassID,
                                EducationLevelID_DEG = lstDEG.EducationLevelID,
                                EducationLevelID_EDG = lstEDG.EducationLevelID,
                                EducationLevelID_DEI = lstDEI.EducationLevelID,
                                IsLocked = lstDEG.IsLocked
                            }).OrderBy(x => x.OrderID);

            return listResultBO;
        }
        #endregion

        //Ket thuc phan cua thuyen


        public IQueryable<EvaluationDevelopmentGroup> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null)
        {
            if (AcademicYearID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["AcademicYearID"] = AcademicYearID;
            return this.SearchEvaluation(SearchInfo);
        }

        #region Tim kiem
        /// <summary>
        /// Tim kiem lop hoc
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<EvaluationDevelopmentGroup> SearchEvaluation(IDictionary<string, object> dic)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelId = Utils.GetInt(dic, "EducationLevelID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int? UserAccountID = Utils.GetNullInt(dic, "UserAccountID");
            int? TeacherByRoleID = Utils.GetNullInt(dic, "TeacherByRoleID");
            IQueryable<EvaluationDevelopmentGroup> lstEvaluaDevGroup = this.EvaluationDevelopmentGroupRepository.All.Where(x => x.AppliedLevel == AppliedLevel && x.EducationLevelID == EducationLevelId);
            int? TeacherID = null;
            if (UserAccountID.HasValue && UserAccountID.Value != 0)
            {
                UserAccount objUserAccount = UserAccountBusiness.Find(UserAccountID);
                TeacherID = (objUserAccount == null) ? null : objUserAccount.EmployeeID;
            }
            else if (TeacherByRoleID.HasValue && TeacherByRoleID.Value > 0)
            {
                TeacherID = TeacherByRoleID.Value;
            }
            if (AppliedLevel != 0)
            {
                lstEvaluaDevGroup = lstEvaluaDevGroup.Where(x => x.AppliedLevel == AppliedLevel);
            }

            return lstEvaluaDevGroup;
        }
        #endregion


        public IQueryable<EvaluationDevelopmentGroup> SearchForCb2(int eduID, IDictionary<string, object> dic)
        {

            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            IQueryable<EvaluationDevelopmentGroup> lstClass = null;
            lstClass = SearchByAcademicYear(AcademicYearID, dic)
                .OrderBy(p => p.OrderID.HasValue ? p.OrderID : 0)/*.ThenBy(o => o.DisplayName)*/;
            lstClass = (from oik in lstClass
                        join b in DeclareEvaluationGroupBusiness.All on oik.EvaluationDevelopmentGroupID equals b.EvaluationGroupID
                        where b.AcademicYearID == AcademicYearID && b.SchoolID == SchoolID && b.EducationLevelID == EducationLevelID
                              && oik.EducationLevelID == EducationLevelID
                        orderby oik.OrderID
                        select oik);
            return lstClass;
        }



        public void Insert(List<DeclareEvaluationIndex> ListDeclareEvaluationIndex, Dictionary<string, object> dic, int securiry)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            string EvaluationGroupID = Utils.GetString(dic, "EvaluationGroupID");
            if (securiry == 0)
            {
                List<DeclareEvaluationIndex> lstDelete = this.All.Where(p => p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID
                                                    && p.EducationLevelID == EducationLevelID).ToList();
                if (EvaluationGroupID == "")
                {
                    this.DeleteAll(lstDelete);
                }
                else
                {
                    int EDGID = Utils.GetInt(dic, "EvaluationGroupID");
                    lstDelete = lstDelete.Where(p => p.EvaluationDevelopmentGroID == EDGID).ToList();
                    this.DeleteAll(lstDelete);
                }
                for (int i = 0; i < ListDeclareEvaluationIndex.Count; i++)
                {
                    this.Insert(ListDeclareEvaluationIndex[i]);
                }
            }
            else if (securiry == 1)
            {
                for (int i = 0; i < ListDeclareEvaluationIndex.Count; i++)
                {
                    this.Insert(ListDeclareEvaluationIndex[i]);
                }
                this.DeleteAll(ListDeclareEvaluationIndex);
            }
        }

        public void DeleteWhenUpdateGroup(List<DeclareEvaluationIndex> lstDelete)
        {
            this.DeleteAll(lstDelete);
            this.Save();
        }

        public void InsertWhenUpdateGroup(List<DeclareEvaluationIndex> lstInsert, int academicYear)
        {
            for (int i = 0; i < lstInsert.Count(); i++)
            {
                lstInsert[i].AcademicYearID = academicYear;
            }

            List<DeclareEvaluationIndex> lstDB = this.All.Where(x => x.AcademicYearID == academicYear).ToList();
            DeclareEvaluationIndex objDB = null;
            for (int item = 0; item < lstInsert.Count(); item++)
            {
                objDB = lstDB.Where(x => x.SchoolID == lstInsert[item].SchoolID &&
                                        x.AcademicYearID == lstInsert[item].AcademicYearID &&
                                        x.EducationLevelID == lstInsert[item].EducationLevelID &&
                                        x.EvaluationDevelopmentGroID == lstInsert[item].EvaluationDevelopmentGroID &&
                                        x.EvaluationDevelopmentID == lstInsert[item].EvaluationDevelopmentID).FirstOrDefault();
                if (objDB == null)
                {
                    this.Insert(lstInsert[item]);
                }
            }
            this.Save();
        }

        public IQueryable<DeclareEvaluationIndex> GetListBySchoolAndAcademicYear(int schoolId, int academicYearId)
        {
            IQueryable<DeclareEvaluationIndex> query = DeclareEvaluationIndexBusiness.All;

            if (schoolId != 0)
                query = query.Where(x => x.SchoolID == schoolId);
            if (academicYearId != 0)
                query = query.Where(x => x.AcademicYearID == academicYearId);
            return query;
        }

        public void InsertBySchool(List<DeclareEvaluationIndex> lstDEI) 
        {
            List<DeclareEvaluationIndex> lstResult = new List<DeclareEvaluationIndex>();
            DeclareEvaluationIndex objResult = new DeclareEvaluationIndex();
            foreach (var item in lstDEI)
            {
                this.Insert(item);
            }
        }

        public IQueryable<DeclareEvaluationIndexBO> SearchDEI(Dictionary<string, object> dic) 
        {
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int evaluationGroupID = Utils.GetInt(dic, "EvaluationGroupID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevelID = Utils.GetInt(dic, "AppliedLevelID");

            IQueryable<DeclareEvaluationGroup> lstDEG = DeclareEvaluationGroupBusiness.All;
            IQueryable<DeclareEvaluationIndex> lstDEI = DeclareEvaluationIndexBusiness.All.Where(x => x.ClassID == null);
            IQueryable<EvaluationDevelopmentGroup> lstEDG = EvaluationDevelopmentGroupBusiness.All;
            IQueryable<EvaluationDevelopment> lstEDI = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true);

            if (schoolID != 0) 
            {
                lstDEG = lstDEG.Where(x => x.SchoolID == schoolID);
                lstDEI = lstDEI.Where(x => x.SchoolID == schoolID);
            }

            if (academicYearID != 0) 
            {
                lstDEG = lstDEG.Where(x => x.AcademicYearID == academicYearID);
                lstDEI = lstDEI.Where(x => x.AcademicYearID == academicYearID);
            }

            if (appliedLevelID != 0) 
            {
                lstEDG = lstEDG.Where(x => x.AppliedLevel == appliedLevelID);
            }

            if (evaluationGroupID != 0)
            {
                lstDEG = lstDEG.Where(x => x.EvaluationGroupID == evaluationGroupID);
                lstEDG = lstEDG.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
                lstEDI = lstEDI.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
            }

            if (educationLevelID != 0) 
            {
                lstDEG = lstDEG.Where(x => x.EducationLevelID == educationLevelID);
                lstDEI = lstDEI.Where(x => x.EducationLevelID == educationLevelID);
                lstEDG = lstEDG.Where(x => x.EducationLevelID == educationLevelID);
                lstEDI = lstEDI.Where(x => x.EducationLevelID == educationLevelID);
            }

            var lstResult = (from oik in lstDEG
                            join b in lstEDG on oik.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                            join c in lstEDI on b.EvaluationDevelopmentGroupID equals c.EvaluationDevelopmentGroupID
                            join dei in lstDEI on c.EvaluationDevelopmentID equals dei.EvaluationDevelopmentID into ps
                            from dei in ps.DefaultIfEmpty()
                            select new DeclareEvaluationIndexBO
                            {
                                EvaluationGroupID = oik.EvaluationGroupID,
                                EvaluationIndexID = c.EvaluationDevelopmentID,
                                DeclareEducatioGroupID = oik.DeclareEvaluationGroupID,
                                EvaluationDevelopmentGroupName = b.EvaluationDevelopmentGroupName,
                                EvaluationDevelopmentIndexName = c.EvaluationDevelopmentName,
                                EvaluationGroupOrderID = b.OrderID,
                                EvaluationDevelopmentOrderID = c.OrderID,
                                EvaluationGroupOfDEI = dei.EvaluationDevelopmentGroID,
                                EvaluationDevelomentIndexCode = c.EvaluationDevelopmentCode,
                                CreatedBySchool = c.CreatedBySchool,
                            }).OrderBy(p => p.EvaluationGroupOrderID).ThenBy(p => p.EvaluationDevelopmentOrderID);

            return lstResult;
        }

        public void InsertOrUpdate(List<DeclareEvaluationIndexBO> lstInsert, IDictionary<string, object> dic)
        {
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");

            List<DeclareEvaluationIndex> lstDEI = DeclareEvaluationIndexBusiness.All.Where(x => x.SchoolID == schoolID 
                                                                                            && x.AcademicYearID == academicYearID
                                                                                            && x.EducationLevelID == educationLevelID).ToList();

            for (int i = 0; i < lstInsert.Count(); i++)
            {
                var tempt = lstDEI.Where(x => x.ClassID == lstInsert[i].ClassID 
                                                        && x.EvaluationDevelopmentGroID == lstInsert[i].EvaluationDevGroupID 
                                                        && x.EvaluationDevelopmentID == lstInsert[i].EvaluationDevIndexID).FirstOrDefault();

                if (tempt != null)
                {
                    tempt.ModifiedDate = DateTime.Now;
                    DeclareEvaluationIndexBusiness.Update(tempt);
                    continue;
                }

                DeclareEvaluationIndex objNew = new DeclareEvaluationIndex();
                objNew.SchoolID = lstInsert[i].SchoolID;
                objNew.AcademicYearID = lstInsert[i].AcademicYearID;
                objNew.EducationLevelID = lstInsert[i].EducationID;
                objNew.EvaluationDevelopmentGroID = lstInsert[i].EvaluationDevGroupID;
                objNew.EvaluationDevelopmentID = lstInsert[i].EvaluationDevIndexID;
                objNew.Note = lstInsert[i].Description;
                objNew.CreateDate = lstInsert[i].CreateDate;
                objNew.ModifiedDate = null;
                objNew.ClassID = lstInsert[i].ClassID;
                DeclareEvaluationIndexBusiness.Insert(objNew);
            }
            DeclareEvaluationIndexBusiness.Save();
        }
        

    }
}
