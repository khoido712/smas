﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;
namespace SMAS.Business.Business
{
    public partial class ClassifiedCapacityBySubjectBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// minhh - 26/11/2012
        ///Lấy mảng băm cho các tham số đầu vào 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(ClassifiedCapacityBySubject entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel},
                {"StatisticLevelReportID", entity.StatisticLevelReportID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin
        /// <summary>
        /// minhh - 26/11/2012
        /// Lưu lại thông tin 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertClassifiedCapacityBySubject(ClassifiedCapacityBySubject entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HOC_LUC_MON;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"StatisticLevelReportID", entity.StatisticLevelReportID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy bảng thống kê xếp hạng hoc luc mon được cập nhật mới nhất
        /// <summary>
        /// <author>minhh</author>
        /// Lấy bảng thống kê xếp hạng hạnh kiểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport GetClassifiedCapacityBySubject(ClassifiedCapacityBySubject entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HOC_LUC_MON;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin xếp hạng hoc luc mon
        /// <summary>
        /// <author>minhh</author>
        /// Lưu lại thông tin xếp hạng hạnh kiểm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateClassifiedCapacityBySubject(ClassifiedCapacityBySubject entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HOC_LUC_MON;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicAllSubject = new Dictionary<string, object>
            {
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };

            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchByAcademicYear(entity.AcademicYearID, dicAllSubject)
                .Where(o => o.SubjectCat.IsActive)
                .Select(u => new SubjectCatBO
                {
                    SubjectCatID = u.SubjectID,
                    DisplayName = u.SubjectCat.DisplayName,
                    SubjectName = u.SubjectCat.SubjectName,
                    OrderInSubject = u.SubjectCat.OrderInSubject
                })
                .Distinct()
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName)
                .ToList();
            //Khai báo mức thống kê
            List<StatisticLevelConfig> lstStatisticLevelConfig = new List<StatisticLevelConfig>();
            lstStatisticLevelConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", entity.StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                //{"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClassBO> lstQPoc = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                  join cs in ClassSubjectBusiness.SearchBySchool(entity.SchoolID) on p.ClassID equals cs.ClassID
                                                  join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                  join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                                                  //Viethd4: Loc ra cac hoc sinh hoc lop hoc mon VNEN
                                                  where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                  && r.IsActive.Value
                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      EducationLevelID = r.EducationLevelID,
                                                      Genre = q.Genre,
                                                      EthnicID = q.EthnicID,
                                                      SubjectID = cs.SubjectID,
                                                      AppliedType = cs.AppliedType,
                                                      IsSpecialize = cs.IsSpecializedSubject
                                                  });

            //loc ra nhung hoc sinh khong dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",Aca.Year}
            };



            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, 0, 0);
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                //Khong lay cac hoc sinh mien giam
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID}, 
               // {"Semester", entity.Semester},
                {"PeriodID", null}
            };


            List<SummedUpRecordBO> lstSummedUpRecordAllStatus_Temp = (from p in this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic)
                                                                      join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                                      join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                                                      where p.PeriodID == null
                                                                      && q.IsActive.Value
                                                                      select new SummedUpRecordBO
                                                                      {
                                                                          PupilID = p.PupilID,
                                                                          SubjectID = p.SubjectID,
                                                                          ClassID = p.ClassID,
                                                                          EducationLevelID = q.EducationLevelID,
                                                                          Genre = r.Genre,
                                                                          EthnicID = r.EthnicID,
                                                                          Semester = p.Semester,
                                                                          IsCommenting = p.IsCommenting,
                                                                          SummedUpMark = p.ReTestMark != null ? p.ReTestMark : p.SummedUpMark,
                                                                          JudgementResult = p.ReTestJudgement != null ? p.ReTestJudgement : p.JudgementResult
                                                                      }).ToList();


            List<SummedUpRecordBO> lstSummedUpRecordAllStatus = new List<SummedUpRecordBO>();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstSummedUpRecordAllStatus_Temp = lstSummedUpRecordAllStatus_Temp.Where(o => o.Semester >= entity.Semester).ToList();
                lstSummedUpRecordAllStatus = lstSummedUpRecordAllStatus_Temp.Where(u => u.Semester == lstSummedUpRecordAllStatus_Temp.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstSummedUpRecordAllStatus = lstSummedUpRecordAllStatus_Temp.Where(o => o.Semester == entity.Semester).ToList();
            }
            List<SummedUpRecordBO> listSummedUpRecord = (from u in lstSummedUpRecordAllStatus
                                                         join l in lstPOC on u.ClassID equals l.ClassID
                                                         where u.PupilID == l.PupilID
                                                         && u.SubjectID == l.SubjectID
                                                         select u).ToList();
            //Loai bo cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                listSummedUpRecord = listSummedUpRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            List<SummedUpRecordBO> listSummedUpRecord_Female = listSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            List<SummedUpRecordBO> listSummedUpRecord_Ethnic = listSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            List<SummedUpRecordBO> listSummedUpRecord_FemaleEthnic = listSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            List<PupilOfClassBO> lstQPoc_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            List<PupilOfClassBO> lstQPoc_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            List<PupilOfClassBO> lstQPoc_FemaleEthnic = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            //Lấy danh sách lớp thuoc khối đó
            Dictionary<string, object> dicClass = new Dictionary<string, object>
                                                            {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"IsVNEN",true}
                                                                //{"Semester", entity.Semester},
                                                                //{"SchoolID", entity.SchoolID}
                                                            };
            List<ClassSubject> listFirstCP = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicClass).ToList();

            //bug 16081
            //listFirstCP = listFirstCP.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0).ToList();
            /*if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listFirstCP = listFirstCP.Where(o => o.SectionPerWeekFirstSemester > 0).ToList();
            }
            else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                listFirstCP = listFirstCP.Where(o => o.SectionPerWeekSecondSemester > 0).ToList();
            }
            else
            {
                listFirstCP = listFirstCP.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0).ToList();
            }*/
            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            int col = 0;
            int SL = 0;
            int countAllPupilOfaSubject = 0;
            int countAllPassPupil = 0;
            int countAllFailPupil = 0;
            int startRow = 11;
            #region Tạo Sheet xếp loại học lực môn
            //Lấy sheet 
            IVTWorksheet firstsheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstsheet, "Q10");
            IVTRange rangeNLBC = firstsheet.GetRange("R22", "R22");
            string semester = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semester = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semester = "HỌC KỲ II";
            string semesterAndAcademic = semester + " - NĂM HỌC: " + Aca.DisplayTitle;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                semesterAndAcademic = "NĂM HỌC: " + Aca.DisplayTitle;
            }
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.SetCellValue("J4", provinceAndDate);
            sheet.SetCellValue("A7", semesterAndAcademic);
            int numberConfig = lstStatisticLevelConfig.Count();
            int lastColumn = (numberConfig + 2) * 2 + 3;
            IVTRange range = firstsheet.GetRange(11, 1, 11, lastColumn);
            IVTRange rangeEducation = firstsheet.GetRange(12, 1, 12, lastColumn);
            IVTRange range_level = firstsheet.GetRange("D9", "E10");
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_level, 9, 4 + col);
                sheet.SetCellValue(9, 4 + col, lstStatisticLevelConfig[i].Title);
                col += 2;
            }
            sheet.CopyPasteSameSize(range_level, 9, 4 + col);
            sheet.SetCellValue(9, 4 + col, "Đ");
            col += 2;
            sheet.CopyPasteSameSize(range_level, 9, 4 + col);
            sheet.SetCellValue(9, 4 + col, "CĐ");
            col = 0;
            //Duyet theo tung mon
            for (int i = 0; i < lstSubject.Count; i++)
            {
                //Lấy danh sách lớp thuoc khối đó
                lstClassSubject = listFirstCP.Where(o => o.SubjectID == lstSubject[i].SubjectCatID).ToList();
                countAllPupilOfaSubject = lstPOC.Where(u => lstClassSubject.Any(v => v.ClassID == u.ClassID) && u.SubjectID == lstSubject[i].SubjectCatID).Count();
                col = 0;
                //Copy row style
                if (startRow + i >= 11)
                {
                    sheet.CopyPasteSameRowHeigh(range, startRow);
                }

                //STT
                sheet.SetCellValue(startRow, 1, (i + 1));

                //Ten mon
                sheet.SetCellValue(startRow, 2, lstSubject[i].SubjectName);

                //So luong hoc sinh
                sheet.SetCellValue(startRow, 3, countAllPupilOfaSubject);

                List<decimal?> listMark = listSummedUpRecord.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && (o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE))).Select(o => o.SummedUpMark).ToList();
                if (listMark.Count() > 0)
                {
                    for (int m = 0; m < numberConfig; m++)
                    {
                        StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                        if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                        {
                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                            {
                                SL = listMark.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                            {
                                SL = listMark.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                            {
                                SL = listMark.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else
                            {
                                SL = listMark.Where(o => o >= slc.MinValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                        }
                        else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                        {
                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                            {
                                SL = listMark.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                            {
                                SL = listMark.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                            {
                                SL = listMark.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else
                            {
                                SL = listMark.Where(o => o > slc.MinValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                        }
                        else
                        {
                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                            {
                                SL = listMark.Where(o => o <= slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                            {
                                SL = listMark.Where(o => o < slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                            {
                                SL = listMark.Where(o => o == slc.MaxValue).Count();
                                sheet.SetCellValue(startRow, 4 + col, SL);//E
                                sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                            }
                        }
                        col += 2;
                    }
                }
                else
                {
                    for (int m = 0; m < numberConfig; m++)
                    {
                        StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                        sheet.SetCellValue(startRow, 4 + col, 0);//E
                        sheet.SetCellValue(startRow, 5 + col, 0);
                        col += 2;
                    }
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY || entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    // Fix lỗi ATTT
                    //List<SummedUpRecordBO> listJudgement = listSummedUpRecord.Where(o => o.ClassID == o.ClassID && o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                    List<SummedUpRecordBO> listJudgement = listSummedUpRecord.Where(o => o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();

                    //Dien hanh kiem D
                    countAllPassPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D))).Count();
                    sheet.SetCellValue(startRow, 4 + col, countAllPassPupil);//E
                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    col += 2;
                    //Dien hanh kiem CD
                    countAllFailPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD))).Count();
                    sheet.SetCellValue(startRow, 4 + col, countAllFailPupil);//E
                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");

                }

                startRow++;
            }
            sheet.CopyPasteSameSize(rangeNLBC, UtilsBusiness.GetExcelColumnName(lastColumn - 3) + (startRow + 1));
            sheet.SetCellValue(UtilsBusiness.GetExcelColumnName(lastColumn - 3) + (startRow + 1), "NGƯỜI LẬP BÁO CÁO");
            sheet.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startRow + 1), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");            
            sheet.GetRange(11, 1, 10 + lstSubject.Count, 7 + (lstStatisticLevelConfig.Count * 2)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            sheet.Name = "XLMH";
            sheet.SetFontName("Times New Roman", 0);
            sheet.DeleteRow(200);
            sheet.PageSize = VTXPageSize.VTxlPaperA4;
            sheet.FitAllColumnsOnOnePage = true;
            firstsheet.Delete();
            #endregion

            if (entity.FemaleChecked)
            {
                #region Thống kê xếp loại môn học học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(sheet);
                sheet_Female.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC, HỌC SINH NỮ");
                startRow = 11;
                col = 0;
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    //Lấy danh sách lớp thuoc khối đó
                    lstClassSubject = listFirstCP.Where(o => o.SubjectID == lstSubject[i].SubjectCatID).ToList();
                    countAllPupilOfaSubject = lstQPoc_Female.Where(u => lstClassSubject.Any(v => v.ClassID == u.ClassID) && u.SubjectID == lstSubject[i].SubjectCatID).Count();
                    col = 0;
                    //Copy row style
                    if (startRow + i >= 11)
                    {
                        sheet_Female.CopyPasteSameRowHeigh(range, startRow);
                    }

                    //STT
                    sheet_Female.SetCellValue(startRow, 1, (i + 1));

                    //Ten mon
                    sheet_Female.SetCellValue(startRow, 2, lstSubject[i].SubjectName);

                    //So luong hoc sinh
                    sheet_Female.SetCellValue(startRow, 3, countAllPupilOfaSubject);

                    List<decimal?> listMark = listSummedUpRecord_Female.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && (o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE))).Select(o => o.SummedUpMark).ToList();
                    if (listMark.Count() > 0)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = listMark.Where(o => o > slc.MinValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o <= slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o < slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o == slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            col += 2;
                        }
                    }
                    else
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            sheet_Female.SetCellValue(startRow, 4 + col, 0);//E
                            sheet_Female.SetCellValue(startRow, 5 + col, 0);
                            col += 2;
                        }
                    }
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY || entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                    {
                        // Fix lỗi ATTT
                        //List<SummedUpRecordBO> listJudgement = listSummedUpRecord_Female.Where(o => o.ClassID == o.ClassID && o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                        List<SummedUpRecordBO> listJudgement = listSummedUpRecord_Female.Where(o => o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                        //Dien hanh kiem D
                        countAllPassPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D))).Count();
                        sheet_Female.SetCellValue(startRow, 4 + col, countAllPassPupil);//E
                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                        col += 2;
                        //Dien hanh kiem CD
                        countAllFailPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD))).Count();
                        sheet_Female.SetCellValue(startRow, 4 + col, countAllFailPupil);//E
                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    }

                    startRow++;
                }

                sheet_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(lastColumn - 3) + (startRow + 1), "NGƯỜI LẬP BÁO CÁO");
                sheet_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startRow + 1), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetFontName("Times New Roman", 0);
                sheet_Female.GetRange(11, 1, 10 + lstSubject.Count, 7 + (lstStatisticLevelConfig.Count * 2)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                sheet_Female.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.EthnicChecked)
            {
                #region Thống kê xếp loại môn học học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(sheet);
                sheet_Ethnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC, HỌC SINH DÂN TỘC");
                startRow = 11;
                col = 0;
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    //Lấy danh sách lớp thuoc khối đó
                    lstClassSubject = listFirstCP.Where(o => o.SubjectID == lstSubject[i].SubjectCatID).ToList();
                    countAllPupilOfaSubject = lstQPoc_Ethnic.Where(u => lstClassSubject.Any(v => v.ClassID == u.ClassID) && u.SubjectID == lstSubject[i].SubjectCatID).Count();
                    col = 0;
                    //Copy row style
                    if (startRow + i >= 11)
                    {
                        sheet_Ethnic.CopyPasteSameRowHeigh(range, startRow);
                    }

                    //STT
                    sheet_Ethnic.SetCellValue(startRow, 1, (i + 1));

                    //Ten mon
                    sheet_Ethnic.SetCellValue(startRow, 2, lstSubject[i].SubjectName);

                    //So luong hoc sinh
                    sheet_Ethnic.SetCellValue(startRow, 3, countAllPupilOfaSubject);

                    List<decimal?> listMark = listSummedUpRecord_Ethnic.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && (o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE))).Select(o => o.SummedUpMark).ToList();
                    if (listMark.Count() > 0)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = listMark.Where(o => o > slc.MinValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o <= slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o < slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o == slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            col += 2;
                        }
                    }
                    else
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            sheet_Ethnic.SetCellValue(startRow, 4 + col, 0);//E
                            sheet_Ethnic.SetCellValue(startRow, 5 + col, 0);
                            col += 2;
                        }
                    }
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY || entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                    {
                        // Fix lỗi ATTT
                        //List<SummedUpRecordBO> listJudgement = listSummedUpRecord_Ethnic.Where(o => o.ClassID == o.ClassID && o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                        List<SummedUpRecordBO> listJudgement = listSummedUpRecord_Ethnic.Where(o => o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                        //Dien hanh kiem D
                        countAllPassPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D))).Count();
                        sheet_Ethnic.SetCellValue(startRow, 4 + col, countAllPassPupil);//E
                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                        col += 2;
                        //Dien hanh kiem CD
                        countAllFailPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD))).Count();
                        sheet_Ethnic.SetCellValue(startRow, 4 + col, countAllFailPupil);//E
                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    }

                    startRow++;
                }

                sheet_Ethnic.GetRange(11, 1, 10 + lstSubject.Count, 7 + (lstStatisticLevelConfig.Count * 2)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                sheet_Ethnic.SetCellValue(UtilsBusiness.GetExcelColumnName(lastColumn - 3) + (startRow + 1), "NGƯỜI LẬP BÁO CÁO");
                sheet_Ethnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startRow + 1), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Ethnic.SetFontName("Times New Roman", 0);
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region Thống kê xếp loại môn học học sinh nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(sheet);
                sheet_FemaleEthnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC, HỌC SINH NỮ DÂN TỘC");
                startRow = 11;
                col = 0;
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    //Lấy danh sách lớp thuoc khối đó
                    lstClassSubject = listFirstCP.Where(o => o.SubjectID == lstSubject[i].SubjectCatID).ToList();
                    countAllPupilOfaSubject = lstQPoc_FemaleEthnic.Where(u => lstClassSubject.Any(v => v.ClassID == u.ClassID) && u.SubjectID == lstSubject[i].SubjectCatID).Count();
                    col = 0;
                    //Copy row style
                    if (startRow + i >= 11)
                    {
                        sheet_FemaleEthnic.CopyPasteSameRowHeigh(range, startRow);
                    }

                    //STT
                    sheet_FemaleEthnic.SetCellValue(startRow, 1, (i + 1));

                    //Ten mon
                    sheet_FemaleEthnic.SetCellValue(startRow, 2, lstSubject[i].SubjectName);

                    //So luong hoc sinh
                    sheet_FemaleEthnic.SetCellValue(startRow, 3, countAllPupilOfaSubject);

                    List<decimal?> listMark = listSummedUpRecord_FemaleEthnic.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && (o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE))).Select(o => o.SummedUpMark).ToList();
                    if (listMark.Count() > 0)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = listMark.Where(o => o >= slc.MinValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = listMark.Where(o => o > slc.MinValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o <= slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMark.Where(o => o < slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMark.Where(o => o == slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            col += 2;
                        }
                    }
                    else
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, 0);//E
                            sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, 0);
                            col += 2;
                        }
                    }
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY || entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                    {
                        // Fix lỗi ATTT
                        //List<SummedUpRecordBO> listJudgement = listSummedUpRecord_FemaleEthnic.Where(o => o.ClassID == o.ClassID && o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                        List<SummedUpRecordBO> listJudgement = listSummedUpRecord_FemaleEthnic.Where(o => o.SubjectID == lstSubject[i].SubjectCatID && o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                        //Dien hanh kiem D
                        countAllPassPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D))).Count();
                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, countAllPassPupil);//E
                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                        col += 2;
                        //Dien hanh kiem CD
                        countAllFailPupil = listJudgement.Where(o => (o.SubjectID == lstSubject[i].SubjectCatID && o.JudgementResult != null && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.ReTestJudgement == null) || (o.SubjectID == lstSubject[i].SubjectCatID && o.ReTestJudgement != null && o.ReTestJudgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD))).Count();
                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, countAllFailPupil);//E
                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    }

                    startRow++;
                }
                sheet_FemaleEthnic.GetRange(11, 1, 10 + lstSubject.Count, 7 + (lstStatisticLevelConfig.Count * 2)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                sheet_FemaleEthnic.SetCellValue(UtilsBusiness.GetExcelColumnName(lastColumn - 3) + (startRow + 1), "NGƯỜI LẬP BÁO CÁO");
                sheet_FemaleEthnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startRow + 1), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_FemaleEthnic.Name = "HS_NU_DT";
                sheet_FemaleEthnic.SetFontName("Times New Roman", 0);
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                #endregion
            }
            return oBook.ToStream();
        }
        #endregion
    }
}
