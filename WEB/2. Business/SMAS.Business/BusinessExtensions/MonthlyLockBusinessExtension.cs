/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    /// <summary>
    /// MonthlyLockBusiness
    /// </summary>
    /// <author>Quanglm</author>
    /// <date>11/15/2012</date>
    public partial class MonthlyLockBusiness
    {
        /// <summary>
        /// CheckMonthlyLock
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">To date.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/15/2012</date>
        public bool CheckMonthlyLock(int SchoolID, DateTime FromDate, DateTime ToDate)
        {
            MonthlyLock MonthlyLock = this.MonthlyLockRepository.All
                .Where(o => o.SchoolID == SchoolID)
                .Where(o => o.LockedMonth >= FromDate)
                .Where(o => o.LockedMonth <= ToDate).FirstOrDefault();
            if (MonthlyLock != null)
            {
                return true;
            }
            return false;
        }

    }
}