/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class MarkTypeBusiness
    {

        public IQueryable<MarkType> Search(IDictionary<string, object> SearchInfo)
        {
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            int AppliedLevel = (int) Utils.GetInt(SearchInfo, "AppliedLevel");
            string Title = Utils.GetString(SearchInfo, "Title");
            bool? IsActive = Utils.GetIsActive(SearchInfo, "IsActive");
            IQueryable<MarkType> ListMarkType = MarkTypeRepository.All;
            if (AppliedLevel != 0)
            {
                ListMarkType = ListMarkType.Where(o => o.AppliedLevel == AppliedLevel);
            }
            if (IsActive.HasValue)
            {
                ListMarkType = ListMarkType.Where(o => o.IsActive == IsActive);
            }
            if (!string.IsNullOrEmpty(Title))
            {
                ListMarkType = ListMarkType.Where(o => o.Title.ToLower().Equals(Title.ToLower()));
            }
            return ListMarkType;
        }
    }
}
