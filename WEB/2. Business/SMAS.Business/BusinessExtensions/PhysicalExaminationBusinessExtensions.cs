﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Business.Business;
using System.IO;
using SMAS.VTUtils.Excel.Export;
namespace SMAS.Business.Business
{
    public partial class PhysicalExaminationBusiness
    {
        #region Lay danh sach thang hoc cua nam hoc


        public Dictionary<int, int> GetListMonthAcademicByYear(int UserAccountID, int AcademicYearID, int SchoolID)
        {
            Dictionary<int, int> listMonth = new Dictionary<int, int>();
            //Dictionary<int, AcademicYear> dic = new Dictionary<int, AcademicYear>();
            var academicYear = this.AcademicYearBusiness.All.Where(x => x.AcademicYearID == AcademicYearID && x.SchoolID == SchoolID)
                .Select(x => new { x.FirstSemesterStartDate, x.SecondSemesterEndDate, x.AcademicYearID }).SingleOrDefault();
            if (academicYear != null)
            {
                for (int i = academicYear.FirstSemesterStartDate.Value.Month; i < 13; i++)
                {
                    //listMonth.Add(i, int.Parse(i.ToString() + academicYear.AcademicYearID.ToString()));
                    listMonth.Add(i, academicYear.FirstSemesterStartDate.Value.Year);
                }
                for (int i = 1; i <= academicYear.SecondSemesterEndDate.Value.Month; i++)
                {
                    if (!listMonth.ContainsKey(i))
                    {
                        //listMonth.Add(i, int.Parse(i.ToString() + (academicYear.AcademicYearID + 1).ToString()));
                        listMonth.Add(i, academicYear.SecondSemesterEndDate.Value.Year);
                    }
                }
            }
            return listMonth;
        }


        #endregion

        public IQueryable<PhysicalExamination> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int MonthID = Utils.GetInt(dic, "MonthID");
            int physicalExaminationID = Utils.GetInt(dic, "PhysicalExaminationID");
            //DateTime? DateTimeComment = Utils.GetDateTime(dic, "DateTimeComment");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            IQueryable<PhysicalExamination> iquery = PhysicalExaminationBusiness.All;

            if (physicalExaminationID > 0)
            {
                iquery = iquery.Where(p => p.PhysicalExaminationID == physicalExaminationID);
            }

            if (SchoolID != 0)
            {
                int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
                iquery = iquery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartitionID);
            }
            if (AcademicYearID != 0)
            {
                iquery = iquery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iquery = iquery.Where(p => p.ClassID == ClassID);
            }

            if (lstClassID.Count > 0)
            {
                iquery = iquery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstPupilID.Count > 0)
            {
                iquery = iquery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (PupilID > 0)
            {
                iquery = iquery.Where(p => p.PupilID == PupilID);
            }
            if (MonthID > 0)
            {
                iquery = iquery.Where(p => p.MonthID == MonthID);
            }
            return iquery;
        }

        public void InsertOrUpdatePE(List<PhysicalExaminationBO> lstPhyExBO, IDictionary<string, object> dic)
        {
            List<PhysicalExamination> lstPEdb = this.Search(dic).ToList();
            if (lstPEdb.Count > 0)
            {
                this.PhysicalExaminationBusiness.DeleteAll(lstPEdb);
            }
            int PartitionID = UtilsBusiness.GetPartionId(Utils.GetInt(dic, "SchoolID"));

            for (int i = 0; i < lstPhyExBO.Count; i++)
            {
                var objPhyExBO = lstPhyExBO[i];
                var objInsert = new PhysicalExamination();
                objInsert.SchoolID = objPhyExBO.SchoolID;
                objInsert.AcademicYearID = objPhyExBO.AcademicYearID;
                objInsert.ClassID = objPhyExBO.ClassID;
                objInsert.PupilID = objPhyExBO.PupilID;
                objInsert.MonthID = objPhyExBO.MonthID;
                objInsert.PhysicalHeightStatus = objPhyExBO.PhysicalHeightStatus;
                objInsert.PhysicalWeightStatus = objPhyExBO.PhysicalWeightStatus;
                objInsert.LastDigitSchoolID = PartitionID;
                if (objPhyExBO.LogID > 0)
                {
                    objInsert.LogID = objPhyExBO.LogID;
                }
                objInsert.Height = objPhyExBO.Height;
                objInsert.Weight = objPhyExBO.Weight;
                objInsert.CreateDate = DateTime.Now;
                objInsert.ModifiedDate = DateTime.Now;
                this.PhysicalExaminationBusiness.Insert(objInsert);
            }

            //PhysicalExamination objDB = null;
            //List<PhysicalExamination> lstInsert = new List<PhysicalExamination>();
            //PhysicalExamination objInsert = null;
            //PhysicalExaminationBO objPhyExBO = null;
            //int PartitionID = UtilsBusiness.GetPartionId(Utils.GetInt(dic, "SchoolID"));
            //for (int i = 0; i < lstPhyExBO.Count; i++)
            //{
            //    objPhyExBO = lstPhyExBO[i];
            //    objDB = lstPEdb.Where(p => p.PupilID == objPhyExBO.PupilID && p.MonthID == objPhyExBO.MonthID).FirstOrDefault();
            //    if (objDB != null)
            //    {
            //        objDB.Height = objPhyExBO.Height;
            //        objDB.Weight = objPhyExBO.Weight;
            //        objDB.LogID = objPhyExBO.LogID;
            //        objDB.ModifiedDate = DateTime.Now;
            //        this.PhysicalExaminationBusiness.Update(objDB);
            //    }
            //    else
            //    {
            //        if (objPhyExBO != null)
            //        {
            //            objInsert = new PhysicalExamination();
            //            objInsert.SchoolID = objPhyExBO.SchoolID;
            //            objInsert.AcademicYearID = objPhyExBO.AcademicYearID;
            //            objInsert.ClassID = objPhyExBO.ClassID;
            //            objInsert.PupilID = objPhyExBO.PupilID;
            //            objInsert.MonthID = objPhyExBO.MonthID;
            //            objInsert.LastDigitSchoolID = PartitionID;
            //            objInsert.LogID = objPhyExBO.LogID;
            //            objInsert.Height = objPhyExBO.Height;
            //            objInsert.Weight = objPhyExBO.Weight;
            //            objInsert.CreateDate = DateTime.Now;
            //            objInsert.ModifiedDate = DateTime.Now;
            //            lstInsert.Add(objInsert);
            //        }
            //    }
            //}
            //if (lstInsert.Count > 0)
            //{
            //    for (int i = 0; i < lstInsert.Count; i++)
            //    {
            //        this.PhysicalExaminationBusiness.Insert(lstInsert[i]);
            //    }
            //}
            this.Save();
        }

        public void DeletePE(int MonthId, IDictionary<string, object> dic)
        {
            List<PhysicalExamination> lstDelete = this.Search(dic).ToList();
            PhysicalExamination objDelete = null;
            for (int i = 0; i < lstDelete.Count; i++)
            {
                objDelete = lstDelete[i];
                if (objDelete.MonthID == MonthId)
                {
                    PhysicalExaminationBusiness.Delete(objDelete.PhysicalExaminationID);
                }
            }
            PhysicalExaminationBusiness.Save();
        }

        public void InsertOrUpdateSinglePE(PhysicalExaminationBO objPhyExBO, IDictionary<string, object> dic, int PhyExID)
        {
            if (PhyExID == 0)
            {
                int PartitionID = UtilsBusiness.GetPartionId(Utils.GetInt(dic, "SchoolID"));
                var objInsert = new PhysicalExamination();

                objInsert.SchoolID = objPhyExBO.SchoolID;
                objInsert.AcademicYearID = objPhyExBO.AcademicYearID;
                objInsert.ClassID = objPhyExBO.ClassID;
                objInsert.PupilID = objPhyExBO.PupilID;
                objInsert.MonthID = objPhyExBO.MonthID;
                objInsert.PhysicalHeightStatus = objPhyExBO.PhysicalHeightStatus;
                objInsert.PhysicalWeightStatus = objPhyExBO.PhysicalWeightStatus;
                objInsert.LastDigitSchoolID = PartitionID;
                if (objPhyExBO.LogID > 0)
                {
                    objInsert.LogID = objPhyExBO.LogID;
                }
                objInsert.Height = objPhyExBO.Height;
                objInsert.Weight = objPhyExBO.Weight;
                objInsert.CreateDate = DateTime.Now;
                objInsert.ModifiedDate = DateTime.Now;
                this.PhysicalExaminationBusiness.Insert(objInsert);
                this.Save();
            }
            else
            {
                var objUpdate = this.Search(dic).FirstOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.Height = objPhyExBO.Height;
                    objUpdate.Weight = objPhyExBO.Weight;
                    objUpdate.PhysicalHeightStatus = objPhyExBO.PhysicalHeightStatus;
                    objUpdate.PhysicalWeightStatus = objPhyExBO.PhysicalWeightStatus;
                    if (objPhyExBO.LogID == 0)
                    {
                        objUpdate.LogID = null;
                    }
                    objUpdate.ModifiedDate = DateTime.Now;
                    this.PhysicalExaminationBusiness.Update(objUpdate);
                    this.Save();
                }
            }
        }

        public string checkStandardPE(decimal? heightOrweight, string hw, int monthCount, int? gender)
        {
            string status = "";
            DevelopmentStandard devStand = null;
            if (gender == 1)
            {
                if (hw.Equals("H"))
                {
                    devStand = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthCount && x.DevelopmentStandardType == GlobalConstants.BOY_HEIGHT).FirstOrDefault();
                    if (devStand != null)
                    {
                        status = sttHeight(heightOrweight, devStand.StandardSub2, devStand.Standard2, devStand.StandardSub3);
                    }
                    else
                    {
                        status = "Chưa nhập dữ liệu";
                    }
                }
                else if (hw.Equals("W"))
                {
                    devStand = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthCount && x.DevelopmentStandardType == GlobalConstants.BOY_WEIGHT).FirstOrDefault();
                    if (devStand != null)
                    {
                        status = sttWeight(heightOrweight, devStand.StandardSub2, devStand.Standard2, devStand.StandardSub3);
                    }
                    else
                    {
                        status = "Chưa nhập dữ liệu";
                    }
                }
            }
            else if (gender == 0)
            {
                if (hw.Equals("H"))
                {
                    devStand = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthCount && x.DevelopmentStandardType == GlobalConstants.GIRL_HEIGHT).FirstOrDefault();
                    if (devStand != null)
                    {
                        status = sttHeight(heightOrweight, devStand.StandardSub2, devStand.Standard2, devStand.StandardSub3);
                    }
                    else
                    {
                        status = "Chưa nhập dữ liệu";
                    }
                }
                else if (hw.Equals("W"))
                {
                    devStand = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthCount && x.DevelopmentStandardType == GlobalConstants.GIRL_WEIGHT).FirstOrDefault();
                    if (devStand != null)
                    {
                        status = sttWeight(heightOrweight, devStand.StandardSub2, devStand.Standard2, devStand.StandardSub3);
                    }
                    else
                    {
                        status = "Chưa nhập dữ liệu";
                    }
                }
            }
            return status;
        }

        public string sttHeight(decimal? hOw, decimal? subStd2, decimal? std2, decimal? subStd3)
        {
            string stt = "";
            if (hOw != null)
            {
                if (hOw > std2)
                {
                    stt = "Cao hơn so với tuổi";
                }
                else if (hOw >= subStd2 && hOw <= std2)
                {
                    stt = "Bình thường";
                }
                else if (hOw < subStd2)
                {
                    stt = "Thấp còi độ 1";
                }
                else if (hOw < subStd3)
                {
                    stt = "Thấp còi độ 2";
                }
            }
            else
            {
                stt = "Chưa nhập dữ liệu";
            }
            return stt;
        }

        public string sttWeight(decimal? hOw, decimal? subStd2, decimal? std2, decimal? subStd3)
        {
            string stt = "";
            if (hOw != null)
            {
                if (hOw > std2)
                {
                    stt = "Cân nặng hơn so với tuổi";
                }
                else if (hOw >= subStd2 && hOw <= std2)
                {
                    stt = "Bình thường";
                }
                else if (hOw < subStd2)
                {
                    stt = "Suy dinh dưỡng nhẹ";
                }
                else if (hOw < subStd3)
                {
                    stt = "Suy dinh dưỡng nặng";
                }
            }
            else
            {
                stt = "Chưa nhập dữ liệu";
            }
            return stt;
        }

        public ProcessedReport GetProcessReportOfMonthlyHeathy(IDictionary<string, object> dicReport, string reportCode)
        {
            {
                //string reportCode = SystemParamsInFile.HS_DANHSACHHOCSINH;
                int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
                int SchoolID = Utils.GetInt(dicReport, "SchoolID");
                int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
                int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
                int monthID = Utils.GetInt(dicReport, "monthID");
                int ClassID = Utils.GetInt(dicReport, "ClassID");
                string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID, monthID);
                return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
            }
        }

        private string GetHashKey(int AcademicYearID, int SchoolID, int AppliedLevelID, int EducationLevelID, int ClassID, int monthID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID},
                {"monthID",monthID},
                {"ClassID",ClassID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        private string GetHashKeyNumberOfClass(int AcademicYearID, int SchoolID, int AppliedLevelID, int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        public ProcessedReport InsertProcessReportOfMonthlyHeathy(IDictionary<string, object> dicReport, Stream excel, string reportCode)
        {
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            int month = Utils.GetInt(dicReport, "monthID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID, month);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            if (ClassID > 0)
            {
                ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[Education]", EducationName);
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            if (month != 0)
            {
                dicInsert.Add("monthID", month);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public ProcessedReport GetProcessReportOfMonthlyHeathyDetail(IDictionary<string, object> dicReport, string reportCode)
        {

            //string reportCode = SystemParamsInFile.HS_DANHSACHHOCSINH;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int monthID = Utils.GetInt(dicReport, "monthID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID, monthID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);

        }
        public ProcessedReport InsertProcessReportOfMonthlyHeathyDetail(IDictionary<string, object> dicReport, Stream excel, string reportCode)
        {
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            int month = Utils.GetInt(dicReport, "monthID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID, month);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            if (ClassID > 0)
            {
                ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[Education]", EducationName);
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            if (month != 0)
            {
                dicInsert.Add("monthID", month);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// CreatedBy: KhuongDV-DVC
        /// Created: 2017-Jul-20
        #region KhuongDV-DCV

        /// <summary>
        /// Kiểu trả về là Integer Nullable.Trả về giá trị Trạng thái theo giá trị cân - đo
        /// </summary>
        /// <param name="monthId">Tháng Id</param>
        /// <param name="gender">Giới tính. Nữ = 0, Nam = 1. Mặc định là Nữ</param>
        /// <param name="type">Kiểu chiều cao hoặc cân nặng. chiều cao = 0, cân nặng = 1. Mặc định là chiều cao</param>
        /// <param name="value">Giá trị của Chiều cao hoặc Cân nặng. Mặc định là null</param>
        /// <returns></returns>
        public int GetPhysicalStatus(int monthId, int gender = 0, int type = 0, decimal? value = null)
        {
            if (gender == GlobalConstants.GENRE_FEMALE)
            {
                if (type == 0)//Height
                {
                    var developmentStandard = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthId && x.DevelopmentStandardType == GlobalConstants.GIRL_HEIGHT).FirstOrDefault();
                    if (developmentStandard != null)
                    {
                        return GetStatusForHeight(value, developmentStandard.StandardSub2, developmentStandard.Standard2, developmentStandard.StandardSub3);
                    }
                    return 0;
                }
                else//Weight
                {
                    var developmentStandard = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthId && x.DevelopmentStandardType == GlobalConstants.GIRL_WEIGHT).FirstOrDefault();
                    if (developmentStandard != null)
                    {
                        return GetStatusForWeight(value, developmentStandard.StandardSub2, developmentStandard.Standard2, developmentStandard.StandardSub3);
                    }
                    return 0;
                }
            }
            else if (gender == GlobalConstants.GENRE_MALE)
            {
                if (type == 0)//Height
                {
                    var developmentStandard = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthId && x.DevelopmentStandardType == GlobalConstants.BOY_HEIGHT).FirstOrDefault();
                    if (developmentStandard != null)
                    {
                        return GetStatusForHeight(value, developmentStandard.StandardSub2, developmentStandard.Standard2, developmentStandard.StandardSub3);
                    }
                    return 0;
                }
                else//Weight
                {
                    var developmentStandard = DevelopmentStandardRepository.All.Where(x => x.MonthID == monthId && x.DevelopmentStandardType == GlobalConstants.BOY_WEIGHT).FirstOrDefault();
                    if (developmentStandard != null)
                    {
                        return GetStatusForWeight(value, developmentStandard.StandardSub2, developmentStandard.Standard2, developmentStandard.StandardSub3);
                    }
                    return 0;
                }
            }
            return 0;
        }

        public int GetStatusForHeight(decimal? value, decimal? standardSub2, decimal? standard2, decimal? standardSub3)
        {
            if (value != null)
            {
                if (value > standard2)
                {
                    return 1; //Cao Hon So Voi Tuoi
                }
                else if (value >= standardSub2 && value <= standard2)
                {
                    return 2; // Binh Thuong
                }
                else if (value >= standardSub3 && value < standardSub2)
                {
                    return 3; //Thap Coi Do 1;
                }
                else if (value < standardSub3)
                {
                    return 4; //Thap Coi Do 2
                }
            }

            return 0;
        }

        public int GetStatusForWeight(decimal? value, decimal? standardSub2, decimal? standard2, decimal? standardSub3)
        {
            if (value != null)
            {
                if (value > standard2)
                {
                    return 1; //Can nang Hon So Voi Tuoi
                }
                else if (value >= standardSub2 && value <= standard2)
                {
                    return 2; // Binh Thuong
                }
                else if (value >= standardSub3 && value < standardSub2)
                {
                    return 3; //Suy dinh duong nhe
                }
                else if (value < standardSub3)
                {
                    return 4; //Suy dinh duong nang
                }
            }

            return 0;
        }

        public ProcessedReport InsertPhysicalExaminationReport(IDictionary<string, object> dic, int thang, string className, Stream data/*, int appliedLevel*/)
        {
            string reportCode = SystemParamsInFile.CAN_D0_SUC_KHOE;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            //string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            //outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ReportUtils.StripVNSign(className)).Replace("[Thang]", "Thang" + thang);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public List<KeyValuePair<int, int>> GetListMonthByAcademiId(int UserAccountID, int AcademicYearID, int SchoolID)
        {
            var listMonth = new List<KeyValuePair<int, int>>();
            var academicYear = this.AcademicYearBusiness.All.Where(x => x.AcademicYearID == AcademicYearID && x.SchoolID == SchoolID)
                .Select(x => new { x.FirstSemesterStartDate, x.SecondSemesterEndDate, x.AcademicYearID }).SingleOrDefault();
            if (academicYear != null)
            {
                if (academicYear.FirstSemesterStartDate.HasValue)
                {
                    for (int i = academicYear.FirstSemesterStartDate.Value.Month; i < 13; i++)
                    {
                        listMonth.Add(new KeyValuePair<int, int>(i, academicYear.FirstSemesterStartDate.Value.Year));
                    }
                }

                if (academicYear.SecondSemesterEndDate.HasValue)
                {
                    for (int i = 1; i <= academicYear.SecondSemesterEndDate.Value.Month; i++)
                    {
                        listMonth.Add(new KeyValuePair<int, int>(i, academicYear.SecondSemesterEndDate.Value.Year));
                    }
                }

            }
            return listMonth;
        }
        #endregion

    }
}


