﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// loại công việc của nhân viên
    /// <author>dungnt</author>
    /// <date>06/09/2012</date>
    /// </summary>
    public partial class EmployeeWorkTypeBusiness
    {
        #region private member variable
        private const int EMPLOYEE_TYPE_TEACHER = 1;
        private const int EMPLOYEE_TYPE_SUPERVISINGDEPT = 2;
        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới loại công việc của nhân viên
        /// <author>dungnt</author>
        /// <date>06/09/2012</date>
        /// </summary>
        /// <param name="insertEmployeeWorkType">Đối tượng Insert</param>
        /// <returns></returns>
        public override EmployeeWorkType Insert(EmployeeWorkType insertEmployeeWorkType)
        {
            //Nhân viên không tồn tại - Kiểm tra EmployeeID 
             EmployeeBusiness.CheckAvailable(insertEmployeeWorkType.EmployeeID, "Employee_Label_EmployeeID");
            //Loại công việc không tồn tại - Kiểm tra WorkTypeID 
             WorkTypeBusiness.CheckAvailable(insertEmployeeWorkType.WorkTypeID, "WorkType_Label_WorkTypeID");
            //Ngày bắt đầu phải nhỏ hơn ngày kết thúc. Kiểm tra FromDate, EndDate
            Utils.ValidateAfterDate(insertEmployeeWorkType.EndDate.Value, insertEmployeeWorkType.FromDate.Value, "WorkType_Label_EndDate", "WorkType_Label_FromDate");
            //Kiểm tra sự tồn tại của SchoolID trong trường hợp EmployeeID có EmployeeType là giáo viên
            //Kiểm tra sự tồn tại của SupervisingDeptID trong trường hợp EmployeeID có EmployeeType là cán bộ quản lý phòng ban
            Employee checkEmployee = EmployeeRepository.All.Where(o => (o.EmployeeID == insertEmployeeWorkType.EmployeeID)).FirstOrDefault();

            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {
                SchoolProfileBusiness.CheckAvailable(insertEmployeeWorkType.SchoolID, "SchoolProfile_Label_SchoolID");
            }
            else if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {
                 SupervisingDeptBusiness.CheckAvailable(insertEmployeeWorkType.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID");
            }

            //Thực hiện Insert dữ liệu vào bảng EmployeeWorkType
            insertEmployeeWorkType.IsActive = true;
            return base.Insert(insertEmployeeWorkType);

            
        
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa loại công việc của nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="updateEmployeeWorkType">Đối tượng Update</param>
        /// <returns></returns>
        public override EmployeeWorkType Update(EmployeeWorkType updateEmployeeWorkType)
        {
            //Nhân viên không tồn tại - Kiểm tra EmployeeID 
             EmployeeBusiness.CheckAvailable(updateEmployeeWorkType.EmployeeID, "Employee_Label_EmployeeID");
            //Loại công việc không tồn tại - Kiểm tra WorkTypeID 
             WorkTypeBusiness.CheckAvailable(updateEmployeeWorkType.WorkTypeID, "WorkType_Label_WorkTypeID");
            //Ngày bắt đầu phải nhỏ hơn ngày kết thúc. Kiểm tra FromDate, EndDate
            Utils.ValidateAfterDate(updateEmployeeWorkType.EndDate.Value, updateEmployeeWorkType.FromDate.Value, "WorkType_Label_EndDate", "WorkType_Label_FromDate");
            //Kiểm tra EmployeeWorkTypeID có tồn tại hay không?
             CheckAvailable(updateEmployeeWorkType.EmployeeWorkTypeID, "EmployeeWorkType_Label_EmployeeWorkTypeID");
            
            //Kiểm tra sự tồn tại của SchoolID trong trường hợp EmployeeID có EmployeeType là giáo viên
            //Kiểm tra sự tồn tại của SupervisingDeptID trong trường hợp EmployeeID có EmployeeType là cán bộ quản lý phòng ban
            Employee checkEmployee = EmployeeRepository.All.Where(o => (o.EmployeeID == updateEmployeeWorkType.EmployeeID)).FirstOrDefault();
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {
                SchoolProfileBusiness.CheckAvailable(updateEmployeeWorkType.SchoolID, "SchoolProfile_Label_SchoolID");
            }
            else if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {
                 SupervisingDeptBusiness.CheckAvailable(updateEmployeeWorkType.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID");
            }

            //Thực hiện Update dữ liệu vào bảng EmployeeWorkType
            updateEmployeeWorkType.IsActive = true;
            return base.Update(updateEmployeeWorkType);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa loại công việc của nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="id">ID quá trình công tác của nhân viên</param>
       public void Delete(int EmployeeWorkTypeID, int SchoolID)
        {
         // Bản ghi này đã bị xóa hoặc không tồn tại trong cơ sở dữ liệu. Kiểm tra EmployeeWorkTypeID != NULL
             CheckAvailable(EmployeeWorkTypeID, "EmployeeWorkType_Label_EmployeeWorkTypeID");

            //Bản ghi này không thuộc quyền quản lý của trường. Từ EmployeeWorkTypeID => EmployeeID. 
            //    Từ EmployeeID => Employee.SchoolID. So sánh với SchoolID truyền vào.
            EmployeeWorkType ewt = EmployeeWorkTypeRepository.Find(EmployeeWorkTypeID);
            Employee emp = EmployeeRepository.Find(ewt.EmployeeID);
            if (emp.SchoolID != SchoolID)
            {
                throw new BusinessException("EmployeeWorkType_Label_SchoolIDNotMatchErr");
            }

            base.Delete(EmployeeWorkTypeID, true);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm loại công việc của nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="dic">danh sách tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<EmployeeWorkType> Search(IDictionary<string, object> dic)
        {
            //-	EmployeeID: default = 0; 0 => tìm kiếm All
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            //-	SchoolID: default = 0; 0 => Tìm kiếm All
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            //-	SupervisingDeptID: default = 0 => Tìm kiếm All
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            //-	WorkTypeID: default = 0; 0 => Tìm kiếm All
            int WorkTypeID = Utils.GetInt(dic, "WorkTypeID");
            //-	FromDate: default = “ ” => Tìm kiếm All
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            //-	ToDate: default = “ ” => Tìm kiếm All
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");

            IQueryable<EmployeeWorkType> lsEmployeeWorkType = EmployeeWorkTypeRepository.All;

            if (EmployeeID != 0)
            {
                lsEmployeeWorkType = lsEmployeeWorkType.Where(o=>(o.EmployeeID==EmployeeID));
            }

            if (SchoolID != 0)
            {
                lsEmployeeWorkType = lsEmployeeWorkType.Where(o => (o.SchoolID == SchoolID));
            }
            if (SupervisingDeptID != 0)
            {
                lsEmployeeWorkType = lsEmployeeWorkType.Where(o => (o.SupervisingDeptID == SupervisingDeptID));
            }
            if (WorkTypeID != 0)
            {
                lsEmployeeWorkType = lsEmployeeWorkType.Where(o => (o.WorkTypeID == WorkTypeID));
            }

            if (FromDate.HasValue)
            {
                lsEmployeeWorkType = lsEmployeeWorkType.Where(o=>(o.FromDate==FromDate));
            }
            if (ToDate.HasValue)
            {
                lsEmployeeWorkType = lsEmployeeWorkType.Where(o => (o.EndDate == ToDate));
            }

            return lsEmployeeWorkType;

        }
        #endregion
        
    }
}