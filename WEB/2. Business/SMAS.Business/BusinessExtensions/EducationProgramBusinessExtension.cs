﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class EducationProgramBusiness
    {
        private const int VALUE_MIN = 1;//Giá trị min
        private const int VALUE_MAX = 3;//Giá trị max
        /// <summary>
        /// Thêm mới chương trình giáo dục
        /// Author: Tamhm1
        /// Date: 29/12/2012
        /// </summary>
        /// <param name="lstEducationProgram"></param>
        public void Insert(int AppliedLevel, int TrainingTypeID, List<EducationProgram> lstEducationProgram)
        {
            //Thực hiện tìm kiếm các phần tử với AppliedLevel, TrainingTypeID, IsActive = true truyền vào
            List<EducationProgram> lsEducation = new List<EducationProgram>();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            SearchInfo["TrainingTypeID"] = TrainingTypeID;
            if (TrainingTypeID == 0)
            {
                lsEducation = Search(SearchInfo).ToList();
                lsEducation = lsEducation.Where(o => o.TrainingTypeID == null).ToList();
            }
            else
            {
                lsEducation = Search(SearchInfo).ToList();
            }
            if (lsEducation != null && lsEducation.Count() > 0)
            {
                foreach (EducationProgram educationProgram in lsEducation)
                {
                    this.Delete(educationProgram.EducationProgramID);
                }
            }
            if (lstEducationProgram != null && lstEducationProgram.Count() > 0)
            {
                foreach (EducationProgram educationProgram in lstEducationProgram)
                {
                    //TrainingTypeID: FK(TrainingType)
                    //EducationLevelID: FK(EducationLevel)
                    //SubjectCatID: FK(SubjectCat)
                    //AppliedLevel: require, in(1,2,3)
                    //NumberOfLesson > 0 && < 9.9
                    ValidationMetadata.ValidateObject(educationProgram);
                    //this.CheckAvailable(educationProgram.TrainingTypeID, "EducationProgram_Label_TrainingTypeID");
                    //this.CheckAvailable(educationProgram.EducationLevelID, "EducationProgram_Label_EducationLevelID");
                    //this.CheckAvailable(educationProgram.SubjectCatID, "EducationProgram_Label_SubjectCatID");
                    Utils.ValidateRange((int)educationProgram.AppliedLevel, VALUE_MIN, VALUE_MAX, "EducationProgram_Label_AppliedLevel");
                    if (educationProgram.NumberOfLesson < 0 || Convert.ToDouble(educationProgram.NumberOfLesson) > 9.9)
                    {
                        throw new BusinessException("EducationProgram_Validate_NumberOfLesson");
                    }
                    base.Insert(educationProgram);
                }
            }

        }
        /// <summary>
        /// Xoá chương trình giáo dục
        /// Author: Tamhm1
        /// Date: 29/12/2012
        /// </summary>
        /// <param name="EducationProgramID"></param>
        public void Delete(int EducationProgramID)
        {
            //check avai
            this.CheckAvailable(EducationProgramID, "EducationProgram_Label_EducationProgramID");
            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "EducationProgram", EducationProgramID, "EducationProgram_Label_EducationProgramID");
            base.Delete(EducationProgramID, true);
        }

        /// <summary>
        /// tìm kiếm chương trình giáo dục
        /// Author: Tamhm1
        /// Date: 29/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        public IQueryable<EducationProgram> Search(IDictionary<string, object> dic)
        {
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int trainingTypeID  = Utils.GetInt(dic, "TrainingTypeID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int subjectCatID = Utils.GetInt(dic, "SubjectCatID");
            decimal numberOfLesson = Utils.GetDecimal(dic, "NumberOfLesson");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            
            IQueryable<EducationProgram> lsEducationProgram = this.EducationProgramRepository.All;
        
            if (isActive.HasValue)
            {
                lsEducationProgram = lsEducationProgram.Where(em => (em.IsActive == isActive));
            }

            if (appliedLevel != 0)
            {
                lsEducationProgram = lsEducationProgram.Where(em => (em.AppliedLevel == appliedLevel));
            }

            if (trainingTypeID != 0)
            {
                lsEducationProgram = lsEducationProgram.Where(em => (em.TrainingTypeID == trainingTypeID));
            }

            if (educationLevelID != 0)
            {
                lsEducationProgram = lsEducationProgram.Where(em => (em.EducationLevelID == educationLevelID));
            }

            if (subjectCatID != 0)
            {
                lsEducationProgram = lsEducationProgram.Where(em => (em.SubjectCatID == subjectCatID));
            }

            if (numberOfLesson != 0)
            {
                lsEducationProgram = lsEducationProgram.Where(em => (em.NumberOfLesson == numberOfLesson));
            }
            return lsEducationProgram;
        }
    }
}