/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class AcademicYearBusiness
    {
        #region private member variable
        private const int DISPLAYTITLE_MAX_LENGTH = 20;   //Độ dài trường tiêu đề

        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm theo năm học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolID">ID trường học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="title">Thông tin hiển th</param>
        /// <returns>IQueryable<AcademicYear></returns>
        public IQueryable<AcademicYear> Search(IDictionary<string, object> dic)
        {
            
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int Year = Utils.GetShort(dic, "Year");
            string Title = Utils.GetString(dic, "title");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            bool isActive = Utils.GetBool(dic, "isActive");
            IQueryable<AcademicYear> lsAcademicYear = this.AcademicYearRepository.All.Where(p=>!p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value));
            if (AcademicYearID != 0)
            {
                lsAcademicYear = lsAcademicYear.Where(aca => aca.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsAcademicYear = lsAcademicYear.Where(aca => aca.SchoolID == SchoolID);
            }
            if (Year != 0)
            {
                lsAcademicYear = lsAcademicYear.Where(aca => aca.Year == Year);
            }


            lsAcademicYear = lsAcademicYear.Where(aca => aca.IsActive == true);

            if (Title.Trim().Length != 0)
            {
                lsAcademicYear = lsAcademicYear.Where(aca => aca.DisplayTitle.Contains(Title.ToLower()));
            }
            return lsAcademicYear;
        }
        #endregion

        #region SearchBySchool

        /// <summary>
        /// Tìm kiếm năm học theo từng trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolID">ID trường</param>
        /// <returns>IQueryable<AcademicYear></returns>
        public IQueryable<AcademicYear> SearchBySchool(int SchoolID, Dictionary<string, object> dic = null)
        {
            IQueryable<AcademicYear> lsAcademicYear = AcademicYearRepository.All;
            if (SchoolID != 0)
            {
                if (dic == null) dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                lsAcademicYear = this.Search(dic);
            }
            return lsAcademicYear;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm năm học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="academicYear">Đối tượng năm học</param>
        /// <returns>Đối tượng năm học</returns>
        public override AcademicYear Insert(AcademicYear academicYear)
        {

            //Bạn phải nhập ngày bắt đầu học kỳ 1 trong năm (year) 
            Utils.ValidateRequire(academicYear.FirstSemesterStartDate.Value, "AcademicYear_Label_FirstSemesterStartDate");
            Utils.ValidateRequire(academicYear.FirstSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterStartDate");
            Utils.ValidateRequire(academicYear.SecondSemesterStartDate.Value, "AcademicYear_Label_FirstSemesterStartDate");
            Utils.ValidateRequire(academicYear.SecondSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterStartDate");

            DateTime SecondSemesterEndDate = academicYear.SecondSemesterEndDate.Value;
            DateTime FirstSemesterStartDate = academicYear.FirstSemesterStartDate.Value;

            int yearFirst = (int)(academicYear.Year - 1); // Năm học trước
            int yearSecond = (int)(academicYear.Year + 1); //Năm học tiếp theo            

            List<AcademicYear> lsAcademicYear1 = this.AcademicYearRepository.All.Where(aca => (!aca.IsActive.HasValue || (aca.IsActive.HasValue && aca.IsActive.Value)) && aca.Year == yearFirst && aca.SchoolID == academicYear.SchoolID).ToList();

            if (lsAcademicYear1.Count != 0)
            {//Ngày kết thúc học kì 2 năm trước
                SecondSemesterEndDate = lsAcademicYear1.FirstOrDefault().SecondSemesterEndDate.Value;

                //Ngày bắt đầu học kỳ 1 > ngày kết thúc của năm học trước. 
                Utils.ValidateBeforeDate(academicYear.FirstSemesterStartDate.Value, SecondSemesterEndDate, "AcademicYear_Label_FirstSemesterStartDate", "AcademicYear_Label_SecondSemesterEndDate_Time");
            }
            List<AcademicYear> lsAcademicYear2 = this.AcademicYearRepository.All.Where(aca => (!aca.IsActive.HasValue || (aca.IsActive.HasValue && aca.IsActive.Value)) && aca.Year == yearSecond && aca.SchoolID == academicYear.SchoolID).ToList();
            if (lsAcademicYear2.Count != 0)
            {
                //Ngày bắt đầu học kì 1 năm sau
                FirstSemesterStartDate = lsAcademicYear2.FirstOrDefault().FirstSemesterStartDate.Value;
                //Ngày kết thúc học kỳ 2 < ngày bắt đầu của năm học sau
                Utils.ValidateBeforeDate(FirstSemesterStartDate, academicYear.SecondSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterEndDate_Time", "AcademicYear_Label_SecondSemesterEndDate");
            }


            //Ngày kết thúc học kỳ 2 phải <= năm kết thúc
            Utils.ValidateCompareNumber(academicYear.SecondSemesterEndDate.Value.Year, ((int)academicYear.Year + 1), "AcademicYear_Label_SecondSemesterEndDate", "AcademicYear_Label_Year_Time");

            //Ngày bắt đầu học kỳ 2 phải < ngày kết thúc học  kỳ 2
            Utils.ValidateBeforeDate(academicYear.SecondSemesterEndDate.Value, academicYear.SecondSemesterStartDate.Value, "AcademicYear_Label_SecondSemesterStartDate", "AcademicYear_Label_SecondSemesterEndDate");

            //Ngày bắt đầu học kỳ 1 phải < ngày kết thúc học kỳ 1
            Utils.ValidateBeforeDate(academicYear.FirstSemesterEndDate.Value, academicYear.FirstSemesterStartDate.Value, "AcademicYear_Label_FirstSemesterStartDate", "AcademicYear_Label_FirstSemesterEndDate");

            //Ngày bắt đầu học kỳ 2 phải > ngày kết thúc học kỳ 1
            Utils.ValidateBeforeDate(academicYear.SecondSemesterStartDate.Value, academicYear.FirstSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterEndDate", "AcademicYear_Label_FirstSemesterEndDate");

            //Thông tin hiển thị năm học phải nhỏ hơn 20 ký tự
            Utils.ValidateMaxLength(academicYear.DisplayTitle, DISPLAYTITLE_MAX_LENGTH, "AcademicYear_Label_DisplayTitle");

            // Check them thong tin la ngay bat dau ky 1 va ngay ket thuc cua ky 2 phai nam tu year -> year + 1
            if (academicYear.Year > academicYear.FirstSemesterStartDate.Value.Year)
            {
                List<object> listParam = new List<object>();
                listParam.Add("AcademicYear_Label_FirstSemesterStartDate");
                listParam.Add(academicYear.DisplayTitle);
                throw new BusinessException("AcademicYear_Label_NotInYearRange", listParam);
            }

            if (academicYear.Year + 1 < academicYear.SecondSemesterEndDate.Value.Year)
            {
                List<object> listParam = new List<object>();
                listParam.Add("AcademicYear_Label_SecondSemesterEndDate");
                listParam.Add(academicYear.DisplayTitle);
                throw new BusinessException("AcademicYear_Label_NotInYearRange", listParam);
            }

            //Năm học đã được khai báo 
            this.CheckDuplicateCouple(academicYear.SchoolID.ToString(), academicYear.Year.ToString(), GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", "SchoolID", "Year", true, academicYear.AcademicYearID, "AcademicYear_Label_Year");

            return base.Insert(academicYear);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa năm học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="academicYear">Đối tượng năm học</param>
        /// <returns>Đối tượng năm học</returns>
        public override AcademicYear Update(AcademicYear academicYear)
        {
            //Bạn phải nhập ngày bắt đầu học kỳ 1 trong năm (year) 
            Utils.ValidateRequire(academicYear.FirstSemesterStartDate.Value, "AcademicYear_Label_FirstSemesterStartDate");
            Utils.ValidateRequire(academicYear.FirstSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterEndDate");
            Utils.ValidateRequire(academicYear.SecondSemesterStartDate.Value, "AcademicYear_Label_SecondSemesterStartDate");
            Utils.ValidateRequire(academicYear.SecondSemesterEndDate.Value, "AcademicYear_Label_SecondSemesterEndDate");
            DateTime SecondSemesterEndDate = academicYear.SecondSemesterEndDate.Value;
            DateTime FirstSemesterStartDate = academicYear.FirstSemesterStartDate.Value;
            int yearFirst = (int)(academicYear.Year - 1); // Năm học trước
            int yearSecond = (int)(academicYear.Year + 1); //Năm học tiếp theo
            List<AcademicYear> lsAcademicYear1 = this.AcademicYearRepository.All.Where(aca => aca.Year == yearFirst && aca.SchoolID == academicYear.SchoolID && (aca.IsActive.HasValue && aca.IsActive == true)).ToList();


            if (lsAcademicYear1.Count != 0)
            {//Ngày kết thúc học kì 2 năm trước
                SecondSemesterEndDate = lsAcademicYear1.FirstOrDefault().SecondSemesterEndDate.Value;

                //Ngày bắt đầu học kỳ 1 > ngày kết thúc của năm học trước. 
                Utils.ValidateBeforeDate(academicYear.FirstSemesterStartDate.Value, SecondSemesterEndDate, "AcademicYear_Label_FirstSemesterStartDate", "AcademicYear_Label_SecondSemesterEndDate_Year");


            }
            List<AcademicYear> lsAcademicYear2 = this.AcademicYearRepository.All.Where(aca => aca.Year == yearSecond && aca.SchoolID == academicYear.SchoolID && (aca.IsActive.HasValue && aca.IsActive == true)).ToList();
            if (lsAcademicYear2.Count != 0)
            {
                //Ngày bắt đầu học kì 1 năm sau
                FirstSemesterStartDate = lsAcademicYear2.FirstOrDefault().FirstSemesterStartDate.Value;
                //Ngày kết thúc học kỳ 2 < ngày bắt đầu của năm học sau
                Utils.ValidateBeforeDate(FirstSemesterStartDate, academicYear.SecondSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterStartDate_Date", "AcademicYear_Label_SecondSemesterEndDate");

            }


            //Ngày kết thúc học kỳ 2 phải <= năm kết thúc
            Utils.ValidateCompareNumber(academicYear.SecondSemesterEndDate.Value.Year, ((int)academicYear.Year + 1), "AcademicYear_Label_SecondSemesterEndDate", "AcademicYear_Label_Year");
            //Ngày bắt đầu học kỳ 2 phải < ngày kết thúc học  kỳ 2
            Utils.ValidateBeforeDate(academicYear.SecondSemesterEndDate.Value, academicYear.SecondSemesterStartDate.Value, "AcademicYear_Label_SecondSemesterEndDate", "AcademicYear_Label_SecondSemesterStartDate ");
            //Ngày bắt đầu học kỳ 1 phải < ngày kết thúc học kỳ 1
            Utils.ValidateBeforeDate(academicYear.FirstSemesterEndDate.Value, academicYear.FirstSemesterStartDate.Value, "AcademicYear_Label_FirstSemesterStartDate", "AcademicYear_Label_FirstSemesterEndDate");
            //Ngày bắt đầu học kỳ 2 phải > ngày kết thúc học kỳ 1
            Utils.ValidateBeforeDate(academicYear.SecondSemesterStartDate.Value, academicYear.FirstSemesterEndDate.Value, "AcademicYear_Label_FirstSemesterEndDate", "AcademicYear_Label_FirstSemesterEndDate");
            //Thông tin hiển thị năm học phải nhỏ hơn 20 ký tự
            Utils.ValidateMaxLength(academicYear.DisplayTitle, DISPLAYTITLE_MAX_LENGTH, "AcademicYear_DisplayTitle");
            //Năm học đã được khai báo 
            this.CheckDuplicateCouple(academicYear.SchoolID.ToString(), academicYear.Year.ToString(), GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", "SchoolID", "Year", true, academicYear.AcademicYearID, "AcademicYear_Label_Year");

            //Năm học không tồn tại 
            this.CheckAvailable(academicYear.AcademicYearID, "AcademicYear_Label_AcademicYearID", false);

            return base.Update(academicYear);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Sửa năm học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="AcademicYearID">ID năm học</param>
        public void Delete(int AcademicYearID)
        {
            //Năm học không tồn tại trong hệ thống hoặc đã bị xóa.
            //new AcademicYearBusiness(null).CheckAvailable(AcademicYearID, "AcademicYear_Label_AcademicYearID", false);
            this.AcademicYearBusiness.CheckAvailable(AcademicYearID, "AcademicYear_Label_AcademicYearID", false);
            //Năm học đã được sử dụng. Bạn không được phép xóa. – Kiểm tra trong cơ sở dữ liệu có những bảng nào đang sử dụng nó.
            this.CheckConstraintsWithExceptTable(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", ",SemeterDeclaration,SemesterDeclaration,", AcademicYearID, "AcademicYear_Label_AcademicYearID");
            List<SemeterDeclaration> lstSemes = SemeterDeclarationBusiness.All.Where(o => o.AcademicYearID == AcademicYearID).ToList();
            if (lstSemes.Count > 0)
            {

                SemeterDeclarationBusiness.DeleteAll(lstSemes);
                SemeterDeclarationBusiness.Save();
            }

            base.Delete(AcademicYearID, false);
        }
        #endregion

        #region Kiem tra nam hien tai
        public bool IsCurrentYear(int AcademicYearID)
        {
            AcademicYear AcademicYear = Find(AcademicYearID);
            if (AcademicYear != null)
            {
                DateTime curDateTime = DateTime.Now;
                // So sanh voi ngay bat dau cua ky 1
                if (AcademicYear.FirstSemesterStartDate.HasValue)
                {
                    if (AcademicYear.FirstSemesterStartDate.Value.CompareTo(curDateTime) > 0)
                    {
                        return false;
                    }
                }
                // So sanh voi ngay ket thuc cua hoc ky 2
                if (AcademicYear.FirstSemesterStartDate.HasValue)
                {
                    if (AcademicYear.SecondSemesterEndDate.Value.CompareTo(curDateTime) < 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Quanglm1
        /// 10/04/2014
        /// Kiem tra nam hoc dau tien - Truyng object de khong phai tim kiem lai
        /// </summary>
        /// <param name="AcademicYear"></param>
        /// <returns></returns>
        public bool IsCurrentYear(AcademicYear AcademicYear)
        {
            if (AcademicYear != null)
            {
                DateTime curDateTime = DateTime.Now;
                // So sanh voi ngay bat dau cua ky 1
                if (AcademicYear.FirstSemesterStartDate.HasValue)
                {
                    if (AcademicYear.FirstSemesterStartDate.Value.CompareTo(curDateTime) > 0)
                    {
                        return false;
                    }
                }
                // So sanh voi ngay ket thuc cua hoc ky 2
                if (AcademicYear.FirstSemesterStartDate.HasValue)
                {
                    if (AcademicYear.SecondSemesterEndDate.Value.CompareTo(curDateTime) < 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Kiem tra thoi gian co thuoc nam hoc hay khong
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="checkTime"></param>
        /// <returns></returns>
        public bool CheckTimeInYear(int academicYearID, DateTime checkTime)
        {
            AcademicYear academicYear = this.Find(academicYearID);
            if (academicYear != null)
            {
                if (academicYear.FirstSemesterStartDate <= checkTime && academicYear.FirstSemesterEndDate >= checkTime)
                {
                    return true;
                }
                if (academicYear.SecondSemesterStartDate <= checkTime && academicYear.SecondSemesterEndDate >= checkTime)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Insert cho truong mau giao, mam non
        public AcademicYear InsertAcademicYearOfChildren(AcademicYear AcademicYear, bool isInheritance)
        {

            Dictionary<int, ClassProfile> dicClassProfile = new Dictionary<int, ClassProfile>();
            Dictionary<int, EatingGroup> dicEatingGroup = new Dictionary<int, EatingGroup>();
            if (isInheritance)
            {
                // Thuc hien ke thua du lieu
                AcademicYear PreAcademicYear = this.SearchBySchool(AcademicYear.SchoolID)
                    .Where(o => o.Year < AcademicYear.Year)
                    .OrderByDescending(o => o.Year).FirstOrDefault();

                if (PreAcademicYear != null)
                {

                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = PreAcademicYear.AcademicYearID;
                    // Ke thua mon hoc cho truong
                    List<SchoolSubject> ListSchoolSubject = SchoolSubjectBusiness.Search(dic).ToList();
                    if (ListSchoolSubject != null)
                    {
                        foreach (SchoolSubject SchoolSubject in ListSchoolSubject)
                        {
                            SchoolSubject SchoolSubjectNew = new SchoolSubject();
                            Utils.Clone(SchoolSubject, SchoolSubjectNew, true);
                            SchoolSubjectNew.SchoolSubjectID = 0;
                            SchoolSubjectNew.AcademicYearID = 0;
                            AcademicYear.SchoolSubjects.Add(SchoolSubjectNew);
                        }
                    }

                    List<ClassProfile> ListClassProfile = ClassProfileBusiness.SearchBySchool(AcademicYear.SchoolID, dic).ToList();
                    // Copy du lieu lop hoc, mon hoc cho lop, mon an theo lop
                    foreach (ClassProfile item in ListClassProfile)
                    {
                        ClassProfile ClassProfile = new ClassProfile();
                        Utils.Clone(item, ClassProfile, true);
                        // ke thua mon hoc cho lop                        
                        List<ClassSubject> ListClassSubject = item.ClassSubjects.ToList();
                        // Bo ID de insert
                        ClassProfile.ClassProfileID = 0;
                        // Gan lai theo hoc ky moi
                        if (ListClassSubject != null)
                        {
                            foreach (ClassSubject ClassSubject in ListClassSubject)
                            {
                                ClassSubject ClassSubjectNew = new ClassSubject();
                                Utils.Clone(ClassSubject, ClassSubjectNew, true);
                                ClassSubjectNew.ClassSubjectID = 0;
                                ClassSubjectNew.ClassID = 0;
                                ClassSubjectNew.ClassProfile = null;
                                ClassSubjectNew.Last2digitNumberSchool = UtilsBusiness.GetPartionId(item.SchoolID);
                                ClassProfile.ClassSubjects.Add(ClassSubjectNew);
                            }
                        }
                        //ClassProfile.AcademicYearID = AcademicYear.AcademicYearID;
                        // Luu lai du lieu lop cu va lop moi
                        dicClassProfile[item.ClassProfileID] = ClassProfile;
                        AcademicYear.ClassProfiles.Add(ClassProfile);
                    }

                    // ke thua mon an                        
                    List<EatingGroupBO> ListEatingGroupBO = EatingGroupBusiness.SearchBySchool(PreAcademicYear.SchoolID, dic).ToList();
                    // ke thua bang mon an theo lop
                    if (ListEatingGroupBO != null && ListEatingGroupBO.Count > 0)
                    {
                        foreach (EatingGroupBO EatingGroupBO in ListEatingGroupBO)
                        {
                            int EatingGroupID = EatingGroupBO.EatingGroupID.Value;
                            EatingGroup EatingGroup = new EatingGroup();
                            EatingGroup.EatingGroupID = EatingGroupID;
                            Utils.Clone(EatingGroupBO, EatingGroup, true);
                            EatingGroup.CreatedDate = DateTime.Now;
                            EatingGroup.AcademicYearID = 0;
                            EatingGroup.AcademicYear = null;
                            EatingGroup.IsActive = true;
                            AcademicYear.EatingGroups.Add(EatingGroup);
                            // Add vao dic 
                            dicEatingGroup[EatingGroupID] = EatingGroup;
                        }
                    }

                    // Lay danh sang thuc don
                    //List<DailyMenu> ListDailyMenu = DailyMenuBusiness.SearchBySchool(PreAcademicYear.SchoolID).ToList();
                    //AcademicYear.DailyMenus = ListDailyMenu;
                }
            }
            // Them vao DB
            this.Insert(AcademicYear);
            // Lay thong tin cua mon an theo lop
            if (isInheritance)
            {
                List<int> ListEatingGroupID = dicEatingGroup.Keys.ToList();
                List<int> LisClassID = dicClassProfile.Keys.ToList();
                List<EatingGroupClass> ListEatingGroupClass = EatingGroupClassRepository.All
                    .Where(o => LisClassID.Contains(o.ClassID))
                    .Where(o => ListEatingGroupID.Contains(o.EatingGroupID)).ToList();
                if (ListEatingGroupClass != null)
                {
                    foreach (EatingGroupClass EatingGroupClass in ListEatingGroupClass)
                    {
                        if (dicClassProfile.ContainsKey(EatingGroupClass.ClassID)
                            && dicEatingGroup.ContainsKey(EatingGroupClass.EatingGroupID))
                        {
                            EatingGroupClass EatingGroupClassNew = new EatingGroupClass();
                            EatingGroupClassNew.ClassProfile = dicClassProfile[EatingGroupClass.ClassID];
                            EatingGroupClassNew.EatingGroup = dicEatingGroup[EatingGroupClass.EatingGroupID];
                            EatingGroupClassRepository.Insert(EatingGroupClassNew);
                        }
                    }
                }
            }
            this.Save();
            return AcademicYear;
        }
        #endregion

        #region Tim kiem nam hoc tren So
        //Confirm anh Chien thi hop nam hoc cua Phong so voi truong 
        public List<int> GetListYearForSupervisingDept(int SupervisingDeptID)
        {
            SupervisingDeptBusiness.CheckAvailable(SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptID");
            SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
            List<int> lstAcaProvince = new List<int>();
            List<int> lstAcaSchool = new List<int>();
            int ProvinceID = sd.ProvinceID.Value;
            IQueryable<int> lstAcademicYearOfProvince = AcademicYearOfProvinceRepository.All.Where(o => o.ProvinceID == ProvinceID && o.IsActive == true)
                                                            .Select(u => (int)u.Year);
            if (lstAcademicYearOfProvince != null && lstAcademicYearOfProvince.Count() > 0)
            {
                lstAcaProvince = lstAcademicYearOfProvince.Distinct().OrderByDescending(o => o).ToList();
            }

            IQueryable<int> lstAcademicYear = this.AcademicYearBusiness.All.Where(u => u.SchoolProfile.SupervisingDept.TraversalPath.StartsWith(sd.TraversalPath) && u.IsActive == true)
                                        .Select(u => (int)u.Year);
            if (lstAcademicYear != null && lstAcademicYear.Count() > 0)
            {
                lstAcaSchool = lstAcademicYear.Distinct().OrderByDescending(o => o).ToList();
            }

            List<int> list = lstAcaProvince.Union(lstAcaSchool).Distinct().OrderByDescending(o => o).ToList();
            return list;
        }

        public List<int> GetListYearForSupervisingDept_Pro(int SupervisingDeptID)
        {
            SupervisingDeptBusiness.CheckAvailable(SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptID");
            SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
            List<int> lstAcaProvince = new List<int>();
            List<int> lstAcaSchool = new List<int>();
            int ProvinceID = sd.ProvinceID.Value;
            IQueryable<int> lstAcademicYearOfProvince = AcademicYearOfProvinceRepository.All.Where(o => o.ProvinceID == ProvinceID && o.IsActive == true)
                                                            .Select(u => (int)u.Year);
            if (lstAcademicYearOfProvince != null && lstAcademicYearOfProvince.Count() > 0)
            {
                lstAcaProvince = lstAcademicYearOfProvince.Distinct().OrderByDescending(o => o).ToList();
            }
            if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                SupervisingDeptID = sd.ParentID.Value;
            }
            IQueryable<int> lstAcademicYear = this.AcademicYearBusiness.All.Where(u => u.IsActive == true && u.SchoolProfile.IsActive == true
                && (u.SchoolProfile.SupervisingDept.SupervisingDeptID == SupervisingDeptID || u.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID))
                                        .Select(u => (int)u.Year);
            if (lstAcademicYear != null && lstAcademicYear.Count() > 0)
            {
                lstAcaSchool = lstAcademicYear.Distinct().OrderByDescending(o => o).ToList();
            }

            List<int> list = lstAcaProvince.Union(lstAcaSchool).Distinct().OrderByDescending(o => o).ToList();
            return list;
        }

        #endregion
        #region Tim kiem nam hoc tren so cua thong ke tong hop HL - HK Trung Hoc Co So
        /// <summary>
        /// Tamhm1 - 02/07/2014
        /// </summary>
        /// <param name="SupervisingDeptID">So GD DT</param>
        /// <returns></returns>
        public List<int> GetListYearForSupervisingDept_THCS(int SupervisingDeptID)
        {
            SupervisingDeptBusiness.CheckAvailable(SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptID");
            SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
            List<int> lstAcaProvince = new List<int>();
            List<int> lstAcaSchool = new List<int>();
            int ProvinceID = sd.ProvinceID.Value;
            int grade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_SECONDARY);
            if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                SupervisingDeptID = sd.ParentID.Value;
            }
            IQueryable<int> lstAcademicYearOfProvince = from p in AcademicYearOfProvinceRepository.All
                                                        join q in SupervisingDeptRepository.All on p.ProvinceID equals q.ProvinceID
                                                        where p.IsActive == true && q.SupervisingDeptID == SupervisingDeptID
                                                        select (int)p.Year;

            if (lstAcademicYearOfProvince != null && lstAcademicYearOfProvince.Count() > 0)
            {
                lstAcaProvince = lstAcademicYearOfProvince.ToList();
            }

            IQueryable<int> lstAcademicYear = this.AcademicYearBusiness.All.Where(u => u.IsActive == true && u.SchoolProfile.IsActive == true
                && (!u.IsActive.HasValue || (u.IsActive.HasValue && u.IsActive.Value))
                && (u.SchoolProfile.EducationGrade & grade) != 0
                && (u.SchoolProfile.SupervisingDept.SupervisingDeptID == SupervisingDeptID || u.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID))
                                        .Select(u => (int)u.Year);
            if (lstAcademicYear != null && lstAcademicYear.Count() > 0)
            {
                lstAcaSchool = lstAcademicYear.ToList();
            }

            List<int> list = lstAcaProvince.Union(lstAcaSchool).Distinct().OrderByDescending(o => o).ToList();
            return list;
        }
        #endregion
        #region Tim kiem nam hoc tren so cua thong ke tong hop HL - HK - Cap THPT
        /// <summary>
        /// Tamhm1 - 02/07/2014
        /// </summary>
        /// <param name="SupervisingDeptID">So GD DT</param>
        /// <returns></returns>
        public List<int> GetListYearForSupervisingDept_THPT(int SupervisingDeptID)
        {
            SupervisingDeptBusiness.CheckAvailable(SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptID");
            SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
            List<int> lstAcaProvince = new List<int>();
            List<int> lstAcaSchool = new List<int>();
            int ProvinceID = sd.ProvinceID.Value;
            int grade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_TERTIARY);
            if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                SupervisingDeptID = sd.ParentID.Value;
            }
            IQueryable<int> lstAcademicYearOfProvince = from p in AcademicYearOfProvinceRepository.All
                                                        join q in SupervisingDeptRepository.All on p.ProvinceID equals q.ProvinceID
                                                        where p.IsActive == true && q.SupervisingDeptID == SupervisingDeptID
                                                        select (int)p.Year;

            if (lstAcademicYearOfProvince != null && lstAcademicYearOfProvince.Count() > 0)
            {
                lstAcaProvince = lstAcademicYearOfProvince.Distinct().OrderByDescending(o => o).ToList();
            }

            IQueryable<int> lstAcademicYear = this.AcademicYearBusiness.All.Where(u => u.IsActive == true && u.SchoolProfile.IsActive == true
                && (u.SchoolProfile.EducationGrade & grade) != 0
                && (u.SchoolProfile.SupervisingDept.SupervisingDeptID == SupervisingDeptID || u.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID))
                                        .Select(u => (int)u.Year);
            if (lstAcademicYear != null && lstAcademicYear.Count() > 0)
            {
                lstAcaSchool = lstAcademicYear.Distinct().OrderByDescending(o => o).ToList();
            }

            List<int> list = lstAcaProvince.Union(lstAcaSchool).Distinct().OrderByDescending(o => o).ToList();
            return list;
        }
        #endregion

        /// <summary>
        /// Quanglm1 - 02/26/2014
        /// Lay danh sach nam hoc tren so cap tieu hoc
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <returns></returns>
        public List<int> GetListYearForSupervisingDept_TH(int SupervisingDeptID)
        {
            SupervisingDeptBusiness.CheckAvailable(SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptID");
            SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
            List<int> lstAcaProvince = new List<int>();
            List<int> lstAcaSchool = new List<int>();
            int ProvinceID = sd.ProvinceID.Value;
            int grade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);

            IQueryable<int> lstAcademicYearOfProvince = from p in AcademicYearOfProvinceRepository.All
                                                        join q in SupervisingDeptRepository.All on p.ProvinceID equals q.ProvinceID
                                                        where p.IsActive == true && q.SupervisingDeptID == SupervisingDeptID
                                                        select (int)p.Year;

            if (lstAcademicYearOfProvince != null && lstAcademicYearOfProvince.Count() > 0)
            {
                lstAcaProvince = lstAcademicYearOfProvince.ToList();
            }

            string path = "\\" + SupervisingDeptID + "\\";
            IQueryable<int> lstAcademicYear = this.AcademicYearBusiness.All.Where(u => u.IsActive == true && u.SchoolProfile.IsActive == true
                && (u.SchoolProfile.EducationGrade & grade) != 0
                && (u.SchoolProfile.SupervisingDept.SupervisingDeptID == SupervisingDeptID || u.SchoolProfile.SupervisingDept.TraversalPath.Contains(path)))
                                        .Select(u => (int)u.Year);
            if (lstAcademicYear != null && lstAcademicYear.Count() > 0)
            {
                lstAcaSchool = lstAcademicYear.ToList();
            }

            List<int> list = lstAcaProvince.Union(lstAcaSchool).Distinct().OrderByDescending(o => o).ToList();
            return list;
        }

        /// <summary>
        /// Kiểm tra thời gian có phải kỳ 2 không
        /// Author: phuong1
        /// Date: 122/4/2013
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="Time"></param>
        /// <returns></returns>
        public bool IsSecondSemesterTime(int AcademicYearID, DateTime? Time = null)
        {
            bool isSecondSemester = false;
            if (Time == null)
            {
                Time = DateTime.Now;
            }
            AcademicYear academicYear = this.Find(AcademicYearID);
            if (Time > academicYear.FirstSemesterEndDate)
            {
                isSecondSemester = true;
            }
            return isSecondSemester;
        }

        public int GetDefaultSemester(int academicYearID)
        {
            int semester;
            DateTime dateNow = DateTime.Now.Date;
            AcademicYear ay = AcademicYearBusiness.Find(academicYearID);
            DateTime firstSemesterStartDate = ay.FirstSemesterStartDate.GetValueOrDefault();
            DateTime firstSemesterEndDate = ay.FirstSemesterEndDate.GetValueOrDefault();
            DateTime secondSemesterStartDate = ay.SecondSemesterStartDate.GetValueOrDefault();
            DateTime secondSemesterEndDate = ay.SecondSemesterEndDate.GetValueOrDefault();

            if (DateTime.Compare(dateNow, secondSemesterStartDate) < 0)
            {
                semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }

            return semester;
        }
    }
}
