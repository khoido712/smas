﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class PupilLeavingOffBusiness
    {
        #region private member variable
        private const int DESCRIPTION_MAX_LENGTH = 256; //Độ dài ghi chú
        #endregion

        #region Validate
        /// <summary>     
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/10/2012</date>      
        private void Validate(PupilLeavingOff PupilLeavingOff, bool Insert)
        {
            ValidationMetadata.ValidateObject(PupilLeavingOff);

            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                   new Dictionary<string, object>()
                {
                    {"ClassProfileID",PupilLeavingOff.ClassID},
                    {"AcademicYearID",PupilLeavingOff.AcademicYearID}
                }, null);
            if (!ClassCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, ClassID: not compatilbe(PupilProfile)
            // Kiem tra xem hoc sinh co dang hoc trong cac nam hoc sau hay khong
            var profile = this.PupilProfileBusiness.Find(PupilLeavingOff.PupilID);
            if (profile == null)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            var academicYear = this.AcademicYearBusiness.Find(PupilLeavingOff.AcademicYearID);
            if (academicYear == null)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            if (profile.PupilOfClasses.Any(poc => poc.Year > academicYear.Year && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING))
            {
                throw new BusinessException("Không thể thôi học vì học sinh đang học trong năm học sau.");
            }
            //bool PupilCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
            //      new Dictionary<string, object>()
            //    {
            //        {"CurrentClassID",PupilLeavingOff.ClassID},
            //        {"PupilProfileID",PupilLeavingOff.PupilID}
            //    }, null);
            //if (!PupilCompatible)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",PupilLeavingOff.SchoolID},
                    {"AcademicYearID",PupilLeavingOff.AcademicYearID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ClassID, EducationLevelID: Not compatible(ClassProfile)
            bool EducationLevelCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"ClassProfileID",PupilLeavingOff.ClassID},
                    {"EducationLevelID",PupilLeavingOff.EducationLevelID}
                }, null);
            if (!EducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //LeavingDate <= DateTime.Now            
            Utils.ValidatePastDate(PupilLeavingOff.LeavingDate, "PupilLeavingOff_Label_LeavingDate");
            //if (PupilLeavingOff.LeavingDate.Year != DateTime.Now.Year)
            //{
            //    throw new BusinessException("Common_Validate_LeavingDate_Year");
            //}

            DateTime? lastAssignDateOfPupil = PupilOfClassBusiness.All.Where(u => u.ClassID == PupilLeavingOff.ClassID && u.PupilID == PupilLeavingOff.PupilID).OrderByDescending(u => u.AssignedDate).Select(u => u.AssignedDate).FirstOrDefault();
            if (lastAssignDateOfPupil.HasValue && lastAssignDateOfPupil.Value.Date > PupilLeavingOff.LeavingDate.Date)
            {
                throw new BusinessException("PupilLeavingOff_Validate_LeavingDate_NotCom");
            }

            if (PupilLeavingOff.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                AcademicYear ac = AcademicYearBusiness.Find(PupilLeavingOff.AcademicYearID);
                if (PupilLeavingOff.LeavingDate > DateTime.Now || PupilLeavingOff.LeavingDate < ac.FirstSemesterStartDate || PupilLeavingOff.LeavingDate > ac.FirstSemesterEndDate)
                {
                    throw new BusinessException("PupilLeavingOff_Validate_BeforeCurrentClassAssignDate");
                }
            }
            if (PupilLeavingOff.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                AcademicYear ac = AcademicYearBusiness.Find(PupilLeavingOff.AcademicYearID);
                if (PupilLeavingOff.LeavingDate > DateTime.Now || PupilLeavingOff.LeavingDate < ac.SecondSemesterStartDate || PupilLeavingOff.LeavingDate > ac.SecondSemesterEndDate)
                {
                    throw new BusinessException("PupilLeavingOff_Validate_BeforeCurrentClassAssignDate");
                }
            }
            //SemesterStartDate < LeavingDate < SemesterEndDate (Dựa vào AcademicYearID, Semester để xác định SemesterStartDate, SemesterEndDate)
            AcademicYear AcademicYear = AcademicYearBusiness.Find(PupilLeavingOff.AcademicYearID);
            if (PupilLeavingOff.Semester == 1)
            {
                if (AcademicYear.FirstSemesterStartDate > PupilLeavingOff.LeavingDate || PupilLeavingOff.LeavingDate > AcademicYear.FirstSemesterEndDate)
                {
                    List<object> Params = new List<object>();
                    Params.Add("PupilLeavingOff_Label_LeavingDate");
                    Params.Add(AcademicYear.FirstSemesterStartDate);
                    Params.Add(AcademicYear.FirstSemesterEndDate);
                    throw new BusinessException("PupilLeavingOff_LeavingDate_Not_InAcademicYear_Time1", Params);
                }
            }
            if (PupilLeavingOff.Semester == 2)
            {
                if (AcademicYear.SecondSemesterStartDate > PupilLeavingOff.LeavingDate || PupilLeavingOff.LeavingDate > AcademicYear.SecondSemesterEndDate)
                {
                    List<object> Params = new List<object>();
                    Params.Add("PupilLeavingOff_Label_LeavingDate");
                    Params.Add(AcademicYear.SecondSemesterStartDate);
                    Params.Add(AcademicYear.SecondSemesterEndDate);
                    throw new BusinessException("PupilLeavingOff_LeavingDate_Not_InAcademicYear_Time2", Params);
                }
            }
            if (Insert == true)
            {
                //PupilOfClass(PupilID).Status != 1: Không cho phép thêm mới            
                IQueryable<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.All.Where(o => (o.PupilID == PupilLeavingOff.PupilID && o.AcademicYearID == PupilLeavingOff.AcademicYearID && o.SchoolID == PupilLeavingOff.SchoolID && o.ClassID == PupilLeavingOff.ClassID)).OrderByDescending(o => o.AssignedDate);
                PupilOfClass PupilOfClass = ListPupilOfClass.FirstOrDefault();
                if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
            }


        }
        #endregion
        #region InsertPupilLeavingOff
        /// <summary>
        /// Thêm mới thông tin học sinh nghỉ học
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/10/2012</date>      

        public void InsertPupilLeavingOff(int UserID, PupilLeavingOff PupilLeavingOff)
        {
            Validate(PupilLeavingOff, true);
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, PupilLeavingOff.ClassID))
            {
                this.Insert(PupilLeavingOff);

                PupilOfClass PupilOfClass = PupilOfClassBusiness.All
                                                    .Where(o => o.PupilID == PupilLeavingOff.PupilID
                                                                && o.AcademicYearID == PupilLeavingOff.AcademicYearID
                                                                && o.SchoolID == PupilLeavingOff.SchoolID
                                                                && o.ClassID == PupilLeavingOff.ClassID)
                                                    .OrderByDescending(o => o.AssignedDate).FirstOrDefault();
                PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilLeavingOff.PupilID);
                if (PupilOfClass != null)
                {
                    PupilOfClass.EndDate = PupilLeavingOff.LeavingDate;
                    PupilOfClass.Status = GlobalConstants.PUPIL_STATUS_LEAVED_OFF;
                    PupilOfClassBusiness.Update(PupilOfClass);
                }
                if (PupilProfile != null)
                {
                    PupilProfile.ProfileStatus = GlobalConstants.PUPIL_STATUS_LEAVED_OFF;
                    PupilProfileBusiness.Update(PupilProfile);
                }
            }
        }
        #endregion

        #region UpdatePupilLeavingOff
        /// <summary>
        /// Cập nhật thông tin học sinh nghỉ học
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/10/2012</date>      

        public void UpdatePupilLeavingOff(int UserID, PupilLeavingOff PupilLeavingOff)
        {
            Validate(PupilLeavingOff, false);
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, PupilLeavingOff.ClassID))
            {
                this.Update(PupilLeavingOff);
                PupilOfClass PupilOfClass = PupilOfClassBusiness.All
                                                   .Where(o => o.PupilID == PupilLeavingOff.PupilID
                                                               && o.AcademicYearID == PupilLeavingOff.AcademicYearID
                                                               && o.SchoolID == PupilLeavingOff.SchoolID
                                                               && o.ClassID == PupilLeavingOff.ClassID)
                                                   .OrderByDescending(o => o.AssignedDate).FirstOrDefault();
                if (PupilOfClass != null)
                {
                    PupilOfClass.EndDate = PupilLeavingOff.LeavingDate;
                    PupilOfClass.Status = GlobalConstants.PUPIL_STATUS_LEAVED_OFF;
                    PupilOfClassBusiness.Update(PupilOfClass);
                }
            }
        }
        #endregion

        #region DeletePupilLeavingOff
        /// <summary>
        /// Xoá thông tin học sinh nghỉ học
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/10/2012</date>      

        public void DeletePupilLeavingOff(int UserID, int PupilLeavingOffID, int SchoolID, int semester)
        {
            //PupilLeavingOffID: PK(PupilLeavingOff)            
            CheckAvailable((int)PupilLeavingOffID, "PupilLeavingOff_Label_PupilLeavingOffID", false);

            PupilLeavingOff PupilLeavingOff = PupilLeavingOffRepository.Find(PupilLeavingOffID);

            //PupilID, SchoolID: not compatible(PupilProfile)
            bool PupilCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                  new Dictionary<string, object>()
                {
                    {"PupilProfileID",PupilLeavingOff.PupilID},
                    {"CurrentSchoolID",SchoolID}
                }, null);
            if (!PupilCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại            
            if (!AcademicYearBusiness.IsCurrentYear(PupilLeavingOff.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, PupilLeavingOff.ClassID))
            {
                this.Delete(PupilLeavingOffID);
                //Cập nhật PupilOfClass với Status = PUPIL_STATUS_STUDYING (1): Đang học
                IQueryable<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.All
                                                                    .Where(o => o.PupilID == PupilLeavingOff.PupilID
                                                                                && o.AcademicYearID == PupilLeavingOff.AcademicYearID
                                                                                && o.SchoolID == PupilLeavingOff.SchoolID
                                                                                && o.ClassID == PupilLeavingOff.ClassID).OrderByDescending(u => u.AssignedDate);
                PupilOfClass PupilOfClass = ListPupilOfClass.FirstOrDefault();

                PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilLeavingOff.PupilID);

                if (PupilOfClass != null)
                {
                    PupilOfClass.EndDate = null;
                    PupilOfClass.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
                    PupilOfClassBusiness.Update(PupilOfClass);
                }

                if (PupilProfile != null)
                {
                    PupilProfile.ProfileStatus = GlobalConstants.PUPIL_STATUS_STUDYING;
                    PupilProfileBusiness.Update(PupilProfile);
                }

            }
        }
        #endregion

        #region Search
        private IQueryable<PupilLeavingOff> Search(IDictionary<string, object> SearchInfo)
        {

            int PupilLeavingOffID = Utils.GetInt(SearchInfo, "PupilLeavingOffID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int EducationLevelID = Utils.GetShort(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int LeavingReasonID = Utils.GetShort(SearchInfo, "LeavingReasonID");
            int Semester = (int)Utils.GetInt(SearchInfo, "Semester");
            DateTime? LeavingDate = Utils.GetDateTime(SearchInfo, "LeavingDate");
            string PupilCode = Utils.GetString(SearchInfo, "PupilCode");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            List<int> ListClassID = Utils.GetIntList(SearchInfo, "ListClassID");
            IQueryable<PupilLeavingOff> Query = PupilLeavingOffRepository.All;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (ClassID != 0)
            {
                Query = from pl in Query
                        join cp in ClassProfileBusiness.All on pl.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && pl.ClassID == ClassID
                        select pl;
            }
            if (PupilLeavingOffID != 0)
            {
                Query = Query.Where(o => o.PupilLeavingOffID == PupilLeavingOffID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }
            if (AppliedLevel != 0)
            {
                Query = Query.Where(o => o.EducationLevel.Grade == AppliedLevel);
            }
            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (LeavingReasonID != 0)
            {
                Query = Query.Where(o => o.LeavingReasonID == LeavingReasonID);
            }
            if (Semester != 0 && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && aca != null)
                {
                    //Query = Query.Where(o => o.Semester == Semester);
                    Query = Query.Where(o => o.LeavingDate >= aca.FirstSemesterStartDate && o.LeavingDate <= aca.FirstSemesterEndDate);
                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && aca != null)
                {
                    Query = Query.Where(o => o.LeavingDate >= aca.SecondSemesterStartDate && o.LeavingDate <= aca.SecondSemesterEndDate);
                }
            }
            if (LeavingDate.HasValue)
            {
                Query = Query.Where(o => o.LeavingDate == LeavingDate);
            }


            if (PupilCode.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.PupilCode.Contains(PupilCode.ToLower()));
            }
            if (FullName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.FullName.Contains(FullName.ToLower()));
            }
            if ((ListClassID != null) && (ListClassID.Count() > 0))
            {

                Query = Query.Where(em => ListClassID.Contains(em.ClassID));

            }
            return Query;


        }
        #endregion

        #region Search by School
        public IQueryable<PupilLeavingOff> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }
        #endregion

        public List<PupilLeavingOff> GetListPupilLeavingOff(int AcademicYearID, int SchoolID, int Semester)
        {
            if (AcademicYearID == 0) return null;
            if (Semester == 0) return null;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["Semester"] = Semester;
            List<PupilLeavingOff> query = SearchBySchool(SchoolID, dic).ToList();
            return query;
        }

        public List<PupilLeavingOffBO> GetListPupilLeavingOffOfSchool(int AcademicYearID, int SchoolID, int Semester)
        {
            if (AcademicYearID == 0) return null;
            if (Semester == 0) return null;

            List<PupilLeavingOffBO> list = (from plo in PupilLeavingOffRepository.All
                                            join pp in PupilProfileRepository.All on plo.PupilID equals pp.PupilProfileID
                                            join cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == AcademicYearID && o.IsActive == true) on plo.ClassID equals cp.ClassProfileID
                                            where plo.AcademicYearID == AcademicYearID
                                            && plo.SchoolID == SchoolID
                                            && plo.Semester == Semester
                                            select new PupilLeavingOffBO
                                            {
                                                PupilID = plo.PupilID,
                                                ClassID = plo.ClassID,
                                                Semester = plo.Semester,
                                                LeavingReasonID = plo.LeavingReasonID,
                                                IsCombinedClass = cp.IsCombinedClass,
                                                IsDisabled = pp.IsDisabled,
                                                Genre = pp.Genre,
                                                EthnicID = pp.EthnicID,
                                                EducationLevelID = cp.EducationLevelID
                                            }).ToList();
            return list;

        }

        public IQueryable<PupilLeavingOffBO> ListPupilLeavingOff(List<int> lstAcademicYearID)
        {
            IQueryable<PupilLeavingOffBO> lstPupilLeaving = (from pl in PupilLeavingOffBusiness.All
                                                             join pp in PupilProfileBusiness.All on pl.PupilID equals pp.PupilProfileID
                                                             join poc in PupilOfClassBusiness.All on pl.PupilID equals poc.PupilID
                                                             where pp.IsActive == true && poc.PupilID == pp.PupilProfileID
                                                             && lstAcademicYearID.Contains(pl.AcademicYearID)
                                                             && lstAcademicYearID.Contains(poc.AcademicYearID)
                                                             select new PupilLeavingOffBO
                                                             {
                                                                 PupilID = pp.PupilProfileID,
                                                                 LeavingReasonID = pl.LeavingReasonID,
                                                                 LeavingReason = pl.LeavingReason.Resolution,
                                                                 LeavingDate = pl.LeavingDate,
                                                                 ClassID = pl.ClassID,
                                                                 SchoolID = pl.SchoolID,
                                                                 Genre = pp.Genre,
                                                                 EthnicID = pp.EthnicID
                                                             });

            return lstPupilLeaving;
        }

    }
}
