/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using log4net;
using System.Web;
using System.Data;
using System.Linq;
using SMAS.Models.Models;
using System.Data.Entity;
using SMAS.DAL.Repository;
using SMAS.Business.Common;
using SMAS.DAL.IRepository;
using System.Linq.Expressions;
using SMAS.Business.IBusiness;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ExamRoomBusiness
    {
        public IQueryable<ExamRoom> Search(IDictionary<string, object> search)
        {
            long examinations = Utils.GetLong(search, "Examinations");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");

            IQueryable<ExamRoom> query = this.All;
            if (examinations != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinations);
            }
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            return query;
        }

        public void InsertExamRoom(ExamRoom room)
        {
            if (room == null)
            {
                return;
            }
            room.ExamRoomID = this.GetNextSeq<long>();
            this.Insert(room);
            this.Save();
        }

        public void InsertListExamRoom(List<ExamRoom> listExamRoom)
        {
            try
            {
                if (listExamRoom == null || listExamRoom.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < listExamRoom.Count; i++)
                {
                    ExamRoomBusiness.Insert(listExamRoom[i]);
                }
                ExamRoomBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertListExamRoom", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void UpdateExamRoom(ExamRoom examRoom)
        {
            if (examRoom == null)
            {
                return;
            }
            this.Update(examRoom);
            this.Save();
        }

        public void DeleteExamRoom(List<long> listExamRoomID)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (listExamRoomID == null || listExamRoomID.Count == 0)
                {
                    return;
                }
                for (int i = 0; i < listExamRoomID.Count; i++)
                {
                    this.Delete(listExamRoomID[i]);
                }
                this.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteExamRoom","null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExamRoomCode", "EXAM_ROOM_CODE");
            columnMap.Add("SeatNumber", "SEAT_NUMBER");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdatTime", "UPDATE_TIME");
            return columnMap;
        }

        //Kiem tra ma phong thi da ton tai chua
        public bool CheckExamRoomByExamCode(string examCode, long ExaminationsID, long ExamGroupID)
        {
            return ExamRoomBusiness.All.Where(p => p.ExaminationsID == ExaminationsID && p.ExamGroupID == ExamGroupID && p.ExamRoomCode.ToLower() == examCode.ToLower()).Count() > 0;
        }

        //Kiem tra ma phong thi da ton tai chua update
        public bool CheckExamRoomByExamCodeUpdate(string examCode, long ExaminationsID, long ExamGroupID, long examRoomID)
        {
            return ExamRoomBusiness.All
                .Where(p => p.ExaminationsID == ExaminationsID && p.ExamGroupID == ExamGroupID && p.ExamRoomID != examRoomID && p.ExamRoomCode.ToLower() == examCode.ToLower()).Count() > 0;
        }

        public List<ExamRoom> GetListExamRoom(long examinationID, long examGroupID)
        {
            List<ExamRoom> result = ExamRoomBusiness.All.Where(p => p.ExaminationsID == examinationID && p.ExamGroupID == examGroupID).ToList();
            return result;
        }

        public List<ExamRoomBO> GetListExamRoomBO(long examinationID, List<long> lstExamGroupID)
        {
            List<ExamRoomBO> result = (from er in ExamRoomRepository.All
                                       join eg in ExamGroupRepository.All on er.ExamGroupID equals eg.ExamGroupID
                                       where er.ExaminationsID == examinationID
                                       && lstExamGroupID.Contains(er.ExamGroupID)
                                       orderby eg.ExamGroupCode, er.ExamRoomCode
                                       select new ExamRoomBO
                                       {
                                           ExamRoomID = er.ExamRoomID,
                                           ExamRoomCode = er.ExamRoomCode,
                                           ExamGroupName = eg.ExamGroupName
                                       }).ToList();
            return result;
        }
    }
}