﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Business.Business;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class EyeTestBusiness
    {
        #region Search

        public IQueryable<EyeTest> Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<EyeTest> lsEyeTest = EyeTestRepository.All;
            //- et.Reye: default = -1; -1 => Tìm kiếm All
            int REye = Utils.GetInt(SearchInfo, "Reye", -1);
            //- et.LEye: default = -1; -1 => Tìm kiếmAll
            int LEye = Utils.GetInt(SearchInfo, "Leye", -1);
            //-et.Rmyopic: default = -1; -1 => Tìm kiếm All
            int Rmyopic = Utils.GetInt(SearchInfo, "Rmyopic", -1);
            //- et.Lmyopic: default = -1; -1 => Tìm kiếm All
            int Lmyopic = Utils.GetInt(SearchInfo, "Lmyopic", -1);
            //- et.Rfarsightedness: default = - 1; -1 => Tìm kiếm all
            int Rfarsightedness = Utils.GetInt(SearchInfo, "Rfarsightedness", -1);
            //- et.Lfarsightedness: default = -1; - 1=> Tìm kiếm All
            int Lfarsightedness = Utils.GetInt(SearchInfo, "Lfarsightedness", -1);
            //-et.Rastigmatism: default = -1; -1 => Tìm kiếm All
            int Rastigmatism = Utils.GetInt(SearchInfo, "Rastigmatism", -1);
            //- et.LAstigmatism: default = -1; -1 => Tìm kiếm All
            int LAstigmatism = Utils.GetInt(SearchInfo, "LAstigmatism", -1);

            //- et.IsTreatment: default = false; NULL => tìm kiếm All
            bool? IsTreatment = Utils.GetNullableBool(SearchInfo, "IsTreatment");
            //- et.IsActive: default = TRUE; NULL -> tìm kiếm all
            bool? IsActive = Utils.GetNullableBool(SearchInfo, "IsActive");
            //- et.MonitoringBookID: default = 0; 0 => Tìm kiếm All
            int MonitoringBookID = Utils.GetInt(SearchInfo, "MonitoringBookID");
            //- mb.SchoolID: default  = 0; 0 => Tìm kiếm All
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            //- mb.AcademicYearID: default = 0; 0 => Tìm kiếm All
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            //- mb.PupilID: default = 0; 0 => Tìm kiếm All
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            //- pp.FullName: default = “” ; “” => Tìm kiếm all
            string FullName = Utils.GetString(SearchInfo, "FullName");
            //- mb.ClassID: default = 0; 0 => Tìm kiếm All
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            //- mb.HealthPeriodID: default = 0; 0 => Tìm kiếm All
            int HealthPeriodID = Utils.GetInt(SearchInfo, "HealthPeriodID");
            //- mb.UnitName: default = “” ; “” => tìm kiếm All
            string UnitName = Utils.GetString(SearchInfo, "UnitName");
            //- mb.MonitoringDate: default = NULL; NULL => tìm kiếm All
            DateTime? MonitoringDate = Utils.GetDateTime(SearchInfo, "MonitoringDate");
            int Grade = Utils.GetInt(SearchInfo, "EducationGrade");
            string HfDisabledRefractive = Utils.GetString(SearchInfo, "HfDisabledRefractive");
            bool Other = Utils.GetBool(SearchInfo, "Other");

            if (Grade > 0)
            {
                lsEyeTest = from ps in EyeTestRepository.All
                            join mb in MonitoringBookBusiness.All on ps.MonitoringBookID equals mb.MonitoringBookID
                            join ed in EducationLevelBusiness.All on mb.EducationLevelID equals ed.EducationLevelID
                            where ed.Grade == Grade
                            select ps;
            }
            if (!string.IsNullOrEmpty(HfDisabledRefractive))
            {
                List<int> listint = new List<int>();
                listint = HfDisabledRefractive.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                lsEyeTest = lsEyeTest.Where(o => (listint.Contains(SystemParamsInFile.DISABLED_REFRACTIVE_MYOPIC) && (o.RMyopic != null || o.LMyopic != null))
                    || (listint.Contains(SystemParamsInFile.DISABLED_REFRACTIVE_FARSIGHTEDNESS) && (o.LFarsightedness != null || o.RFarsightedness != null))
                    || (listint.Contains(SystemParamsInFile.DISABLED_REFRACTIVE_ASTIGMATISM) && (o.RAstigmatism != null || o.LAstigmatism != null))
                    || (listint.Contains(SystemParamsInFile.DISABLED_REFRACTIVE_OTHER) && o.Other != null )
                    || (listint.Contains(SystemParamsInFile.DISABLED_REFRACTIVE_NORMAL) && (o.RMyopic == null || o.LMyopic == null) && (o.LFarsightedness == null || o.RFarsightedness == null)
                     && (o.RAstigmatism == null || o.LAstigmatism == null) && o.Other == null));
            }         

            if (REye != -1 && LEye != -1)
            {
                int maxValue = REye;
                int minValue = LEye;
                if (LEye > REye)
                {
                    maxValue = LEye;
                    minValue = REye;
                }
                lsEyeTest = lsEyeTest.Where(o => o.REye >= minValue && o.LEye >= minValue && o.REye <= maxValue && o.LEye <= maxValue);
            }
            else if (REye != -1 && LEye == -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.REye <= REye && o.LEye <= REye);
            }
            else if (REye == -1 && LEye != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.LEye >= LEye && o.REye >= LEye);
            }

            if (Rmyopic != -1 && Lmyopic != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.RMyopic >= Rmyopic && o.LMyopic <= Lmyopic);
            }
            else if (Rmyopic != -1 && Lmyopic == -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.RMyopic >= Rmyopic);
            }
            else if (Rmyopic == -1 && Lmyopic != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.LMyopic <= Lmyopic);
            }

            if (Rfarsightedness != -1 && Lfarsightedness != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.RFarsightedness >= Rfarsightedness && o.LFarsightedness <= Lfarsightedness);
            }
            else if (Rfarsightedness != -1 && Lfarsightedness == -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.RFarsightedness >= Rfarsightedness);
            }
            else if (Rfarsightedness == -1 && Lfarsightedness != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.LFarsightedness <= Lfarsightedness);
            }

            if (Rastigmatism != -1 && LAstigmatism != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.RAstigmatism >= Rastigmatism && o.LAstigmatism <= LAstigmatism);
            }
            else if (Rastigmatism != - 1 && LAstigmatism == -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.RAstigmatism >= Rastigmatism);
            }
            else if (Rastigmatism == - 1 && LAstigmatism != -1)
            {
                lsEyeTest = lsEyeTest.Where(o => o.LAstigmatism <= LAstigmatism);
            }

			if(Other == true)
			{
                lsEyeTest = lsEyeTest.Where(o => o.Other != null);
			}
            
            if (IsTreatment.HasValue)
            {
                lsEyeTest = lsEyeTest.Where(o => o.IsTreatment == IsTreatment);
            }
            //if (IsActive.HasValue)
            //{
            //    lsEyeTest = lsEyeTest.Where(o => o.IsActive == true);
            //}
            if (MonitoringBookID != 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBookID == MonitoringBookID);
            }
            if (SchoolID != 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.PupilID == PupilID);
            }

            if (FullName.Trim().Length > 0)
            {
                lsEyeTest = lsEyeTest.Where(o => (o.MonitoringBook.PupilProfile.FullName.ToLower().Contains(FullName.ToLower())));
            }
            if (ClassID != 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.ClassID == ClassID);
            }
            if (HealthPeriodID != 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.HealthPeriodID == HealthPeriodID);
            }
            if (UnitName.Trim().Length > 0)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.UnitName.Contains(UnitName));
            }

            if (MonitoringDate.HasValue)
            {
                lsEyeTest = lsEyeTest.Where(o => o.MonitoringBook.MonitoringDate == MonitoringDate);
            }
            return lsEyeTest;
        }


        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe theo học sinh – Mắt
        /// author: trangdd
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="PupilID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public IQueryable<EyeTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID)
        {
            //B1: Khởi tạo IQueryable<MonitoringBook> lstMonitoringBook bằng cách gọi hàm MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID) 
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID);
            //B2: : Thực hiện việc tìm kiếm lstMonitoringBook mb left EyeTest et on mb.MonitoringBookID = et.MonitoringBookID. Kêt quả thu được IQueryable<EyeTestBO>
            var query = from mb in lstMonitoringBook
                        join dt in EyeTestRepository.All.Where(o => o.IsActive == true) on mb.MonitoringBookID equals dt.MonitoringBookID
                        //into g1
                        //from j2 in g1.DefaultIfEmpty()
                        select new EyeTestBO
                        {
                            EyeTestID = dt.EyeTestID,
                            MonitoringBookID = mb.MonitoringBookID,
                            //Date = mb.MonitoringDate,
                            REye = dt.REye,
                            LEye = dt.LEye,
                            RMyopic = dt.RMyopic,
                            LMyopic = dt.LMyopic,
                            RFarsightedness = dt.RFarsightedness,
                            LFarsightedness = dt.LFarsightedness,
                            RAstigmatism = dt.RAstigmatism,
                            LAstigmatism = dt.LAstigmatism,
                            Other = dt.Other,
                            IsTreatment = dt.IsTreatment,
                            IsMyopic = dt.IsMyopic,
                            IsFarsightedness = dt.IsFarsightedness,
                            IsAstigmatism = dt.IsAstigmatism,
                            CreateDate = dt.CreatedDate
                        };
            return query;
        }
        public IQueryable<EyeTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
            }
            return Search(SearchInfo);
        }
        #endregion

        /// <summary>
        /// Thêm mới thông tin khám sức khỏe – Mắt
        /// author: trangdd
        /// </summary>
        public void Insert(EyeTest EyeTest, MonitoringBook MonitoringBook)
        {
            ValidationMetadata.ValidateObject(EyeTest);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = MonitoringBook.PupilID;
            SearchInfo["HealthPeriodID"] = MonitoringBook.HealthPeriodID;
            SearchInfo["AcademicYearID"] = MonitoringBook.AcademicYearID;
            List<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(MonitoringBook.SchoolID, SearchInfo).ToList();
            if (lstMonitoringBook.Count == 0)
            {
                if (null == MonitoringBook.MonitoringDate)
                {
                    throw new BusinessException("MonitoringBook_Label_MonitoringDateRepuired");
                }
                EyeTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                base.Insert(EyeTest);
                //Thực hiện insert dữ liệu vào bảng MonitoringBook bằng cách gọi hàm 
                MonitoringBookBusiness.Insert(MonitoringBook);
            }
            else
            {
                //Thực hiện Insert dữ liệu vào bảng EyeTest  với MonitoringBookID = lstMonitoringBook.MonitoringBookID
                EyeTest.MonitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;

                //entity.MonitoringBookID: FK(MonitoringBook), duplicate                
                bool MonitoringBookDiplicate = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "EyeTest",
                 new Dictionary<string, object>()
                {
                    {"MonitoringBookID",EyeTest.MonitoringBookID},
                    {"IsActive",true}
                }, new Dictionary<string, object>()
                {
                    //{"MonitoringBookID",entity.MonitoringBookID},
                });
                if (MonitoringBookDiplicate)
                {
                    throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
                }
                base.Insert(EyeTest);
            }
        }
        /// <summary>
        /// Cập nhật thông tin khám sức khỏe – Mắt
        /// author: trangdd
        /// </summary>
        public void Update(EyeTest EyeTest, MonitoringBook MonitoringBook)
        {
            ValidationMetadata.ValidateObject(EyeTest);
            //Thực hiện update dữ liệu vào bảng MonitoringBook bằng cách gọi hàm 
            MonitoringBookBusiness.Update(MonitoringBook);
            base.Update(EyeTest);
        }
        /// <summary>
        /// Xoa thông tin khám sức khỏe – Mắt
        /// author: trangdd
        /// </summary>
        public void Delete(int EyeTestID, int SchoolID)
        {
            //EyeTestID: PK(EyeTest )          
            new EyeTestBusiness(null).CheckAvailable((int)EyeTestID, "EyeTest_Label_EyeTestID", false);

            int MonitoringBookID = this.Find(EyeTestID).MonitoringBookID;
            //MonitoringBookID, SchoolID: not compatible(MonitoringBook)
            bool SchoolCompatible = new MonitoringBookRepository(this.context).ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                  new Dictionary<string, object>()
                {
                    {"MonitoringBookID",MonitoringBookID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            this.Delete(EyeTestID, true);
        }

        // Thuyen - 05/10/2017
        public List<EyeTest> ListEyeTestByMonitoringBookId(List<int> lstMoniBookID)
        {
            return EyeTestBusiness.All.Where(x => lstMoniBookID.Contains(x.MonitoringBookID)).ToList();
        }
    }
}