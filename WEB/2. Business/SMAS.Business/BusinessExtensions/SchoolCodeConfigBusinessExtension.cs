﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class SchoolCodeConfigBusiness
    {
        public override SchoolCodeConfig Insert(SchoolCodeConfig schoolCodeConfig)
        {
            SMAS.Business.Common.Utils.ValidateRange(schoolCodeConfig.StartNumber, 0, 99999, "SchoolCodeConfig_Label_StartNumber");

            return base.Insert(schoolCodeConfig);
        }

        public override SchoolCodeConfig Update(SchoolCodeConfig schoolCodeConfig)
        {
            Utils.ValidateRange(schoolCodeConfig.StartNumber, 0, 99999, "SchoolCodeConfig_Label_StartNumber");

            return base.Update(schoolCodeConfig);
        }

        public void Delete(int SchoolCodeConfigID)
        {
            this.CheckAvailable(SchoolCodeConfigID, "SchoolCodeConfig_Label_SchoolCodeConfigID", true);

            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "SchoolCodeConfig", SchoolCodeConfigID, "SchoolCodeConfig_Label_SchoolCodeConfigID");

            base.Delete(SchoolCodeConfigID, true);

        }

        public IQueryable<SchoolCodeConfig> Search(IDictionary<string, object> dic)
        {
            int? schoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? codeConfigID = Utils.GetNullableInt(dic, "CodeConfigID");
            IQueryable<SchoolCodeConfig> lsSchoolCodeConfig = this.SchoolCodeConfigRepository.All;
            if (schoolID.HasValue) lsSchoolCodeConfig = lsSchoolCodeConfig.Where(u => u.SchoolID == schoolID.Value);
            if (codeConfigID.HasValue) lsSchoolCodeConfig = lsSchoolCodeConfig.Where(u => u.CodeConfigID == codeConfigID.Value);
            return lsSchoolCodeConfig;
        }

        public SchoolCodeConfig SearchBySchool(int CodeConfigID, int SchoolID)
        {
            var schoolCodeConfig = this.SchoolCodeConfigRepository.All.OrderByDescending(o=>o.CreatedDate).FirstOrDefault(u => u.CodeConfigID == CodeConfigID && u.SchoolID == SchoolID);
            return schoolCodeConfig;
        }
    }
}
