/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ProcessedReportBusiness
    {

        public ProcessedReport GetProcessedReport(string reportCode, string inputParameterHashKey)
        {
            var lst = ProcessedReportRepository.All;
            lst = lst.Where(x => x.ReportCode == reportCode);
            lst = lst.Where(x => x.InputParameterHashKey == inputParameterHashKey);
            lst = lst.OrderByDescending(x => x.ProcessedDate);
            var lt = lst.ToList();
            if (lt != null && lt.Count > 0)
            {
                return lt[0];
            }
            else
            {
                return null;
            }
        }

        public ProcessedReport GetProcessedReport(int processedReportID, IDictionary<string, object> dic, List<string> listReportCode)
        {
            ProcessedReport processedReport = ProcessedReportRepository.Find(processedReportID);
            if (processedReport == null)
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            if (!listReportCode.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            return processedReport;
        }
    }
}