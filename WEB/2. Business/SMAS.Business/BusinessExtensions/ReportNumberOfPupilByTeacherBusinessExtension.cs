﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ReportNumberOfPupilByTeacherBusiness
    {
        #region Lấy dữ liệu cho báo cáo số lượng HS theo giáo viên
        public IQueryable<ReportNumberOfPupilByTeacherBO> GetNumberOfPupilByTecher(int SchoolID, int AcademicYearID, int semester, int FacultyID)
        {
            //Tìm kiếm giáo viên được phân công giảng dạy
            var query = TeachingAssignmentRepository.All;
            query = query.Where(o => o.IsActive == true);
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                query = query.Where(o => o.SchoolID == SchoolID);
            }
            if (semester != 0)
            {
                query = query.Where(o => o.Semester == semester);
            }
            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (FacultyID != 0)
            {
                query = query.Where(o => o.FacultyID == FacultyID);
            }
            if (query.ToList().Count() == 0)
            {
                return null;
            }
            //Lấy danh sách giáo viên trong trong kết quả thu được
            
            List<int?> lstClass = new List<int?>();
            var listTeacher = (from t in query
                               select new
                               {
                                   t.TeacherID,
                                   t.Employee.FullName,
                                   t.Employee.Name,
                                   t.Employee.BirthDate,
                                   t.SchoolFaculty.FacultyName,
                                   t.ClassID
                               }).ToList();
            List<int> lstTeacherID = listTeacher.Select(o => o.TeacherID).Distinct().ToList();
            ReportNumberOfPupilByTeacherBO reportNumberOfPupilByTeacher = new ReportNumberOfPupilByTeacherBO();
            List<ReportNumberOfPupilByTeacherBO> lstReportNumberOfPupilByTeacher = new List<ReportNumberOfPupilByTeacherBO>();
            List<ReportNumberOfPupilByTeacherBO> lstReportNumberOfPupilByTeacherSort = new List<ReportNumberOfPupilByTeacherBO>();

            // Lay thong tin hoc sinh theo lop
            IQueryable<PupilOfClass> iqPoc = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID},
                {"Semester",semester}
            }).AddCriteriaSemester(AcademicYearBusiness.Find(AcademicYearID), semester);

            var listCountPupilInClass = (from poc in iqPoc
                                         group poc by poc.ClassID into g
                                         select new { ClassID = g.Key, TotalPupil = g.Count() }).ToList();

            int teacherid = 0;
            for (int i = 0; i < lstTeacherID.Count(); i++)
            {
                reportNumberOfPupilByTeacher = new ReportNumberOfPupilByTeacherBO();
                teacherid = lstTeacherID[i];
                var listTeachingAssignment = listTeacher.Where(o => o.TeacherID == teacherid).ToList();
                var teachingAssignment = listTeachingAssignment.FirstOrDefault();
                reportNumberOfPupilByTeacher.EmployeeID = teachingAssignment.TeacherID;
                reportNumberOfPupilByTeacher.EmployeeName = teachingAssignment.FullName;
                reportNumberOfPupilByTeacher.Name = teachingAssignment.Name;
                reportNumberOfPupilByTeacher.BirthDate = teachingAssignment.BirthDate;
                reportNumberOfPupilByTeacher.FacultyName = teachingAssignment.FacultyName;
                reportNumberOfPupilByTeacher.NumberPupil = 0;
                lstClass = listTeachingAssignment.Select(o => o.ClassID).Distinct().ToList();
                foreach (var ClassID in lstClass)
                {
                    reportNumberOfPupilByTeacher.NumberPupil += listCountPupilInClass.Where(o => o.ClassID == ClassID).Sum(o => o.TotalPupil);
                }
                lstReportNumberOfPupilByTeacher.Add(reportNumberOfPupilByTeacher);
            }
            lstReportNumberOfPupilByTeacherSort = lstReportNumberOfPupilByTeacher.OrderBy(u => u.Name).ToList();
            return lstReportNumberOfPupilByTeacherSort.AsQueryable();
        }
        #endregion


        #region Tạo báo cáo số lượng HS theo giáo viên
        public Stream CreateReportNumberOfPupilByTeacher(int SchoolID, int AcademicYearID, int semester, int FacultyID, List<int> lstTeacher,int AppliedLevelID = 0)
        {
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string reportCode = SystemParamsInFile.REPORT_SO_LUONG_HOC_SINH_THEO_GIAO_VIEN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 11;
            int lastCol = 7;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "G" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, AppliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            string Semester = SMASConvert.ConvertSemester(semester).ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"AcademicYear", academicYearTitle},
                    {"Semester", Semester}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange toprange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange midrange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);
            IVTRange lastrange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);
            IVTRange rangeCreateUser = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastCol);
            //Lấy dữ liệu
            IQueryable<ReportNumberOfPupilByTeacherBO> iqEmloyee = GetNumberOfPupilByTecher(SchoolID, AcademicYearID, semester, FacultyID);
            if (iqEmloyee.Count() > 0)
            {
                iqEmloyee = iqEmloyee.Where(o => lstTeacher.Contains(o.EmployeeID));
                List<object> listData = new List<object>();
                List<ReportNumberOfPupilByTeacherBO> lstEmployee = iqEmloyee.ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    if ((i + 1) % 5 == 1 && lstEmployee.Count != 1 && lstEmployee.Count != (i + 1))
                    {
                        dataSheet.CopyPasteSameSize(toprange, firstRow + i, 1);
                    }
                    else if ((i + 1) % 5 == 0 || (i + 1) == lstEmployee.Count) //Chia hết cho 5 hoặc hàng cuối 
                    {
                        dataSheet.CopyPasteSameSize(lastrange, firstRow + i, 1);
                    }
                    else
                    {
                        dataSheet.CopyPasteSameSize(midrange, firstRow + i, 1);
                    }
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].EmployeeName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : "";
                    dicData["FacultyName"] = lstEmployee[i].FacultyName;
                    dicData["TotalPupil"] = lstEmployee[i].NumberPupil;
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng  người tạo báo cáo
            dataSheet.CopyPasteSameSize(rangeCreateUser, firstRow + iqEmloyee.Count() + 2, 1);
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion
    }
}
