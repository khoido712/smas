﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class FoodCatBusiness
    {
        #region Validate

        public void Validate(FoodCat foodcat)
        {
            ValidationMetadata.ValidateObject(foodcat);

            if (foodcat.GroupType != 1 && foodcat.GroupType != 2)
            {
                throw new BusinessException("FoodCat_Error_GroupType");
            }

            if (foodcat.DiscardedRate >= 100)
            {
                throw new BusinessException("FoodCat_Error_DiscardedRate");
            }

            if (foodcat.Protein > 100 || foodcat.Protein < 0 || foodcat.Fat > 100 || foodcat.Fat < 0 || foodcat.Sugar > 100 || foodcat.Sugar < 0)
            {
                throw new BusinessException("FoodCat_Error_Percen");
            }

            if (foodcat.CalculationUnit != 1 && foodcat.CalculationUnit != 2)
            {
                throw new BusinessException("FoodCat_Error_CalculationUnit");
            }
        }

        #endregion



        #region Insert
        public void Insert(FoodCat foodcat, List<FoodMineral> lstFoodMineral)
        {
            Validate(foodcat);
            //Tên thực phẩm đã tồn tại 
            //new FoodCatBusiness(null).CheckDuplicate(foodcat.FoodName,GlobalConstants.LIST_SCHEMA, "FoodCat", "FoodName", true, foodcat.FoodID, "FoodCat_Label");
            CheckDuplicate(foodcat.FoodName.ToString());
            foodcat.Calo = foodcat.Protein * 4 + foodcat.Fat * 9 + foodcat.Sugar * 4;
            base.Insert(foodcat);
            for (int i = 0; i < lstFoodMineral.Count; i++)
            {
                var item = lstFoodMineral[i];
                item.FoodCat = foodcat;
                this.FoodMineralRepository.Insert(item);
            }
            //foreach (var item in lstFoodMineral)
            //{
            //    item.FoodCat = foodcat;
            //    this.FoodMineralRepository.Insert(item);
            //}

        }
        #endregion
        #region
        public void CheckDuplicate(string footname)
        {
            IQueryable<FoodCat> footCat = this.FoodCatBusiness.All;
            if (footname != "")
            {
                footCat = footCat.Where(p => p.FoodName == footname && p.IsActive == true);
                if (footCat.Count() > 0)
                {
                    throw new BusinessException("Common_Validate_Duplicate", "FoodCat_Lable_FoodName");
                }
            }

        }
        #endregion

        #region Update
        public void Update(FoodCat foodcat, List<FoodMineral> lstFoodMineral)
        {
            Validate(foodcat);
            //Tên loại thực phẩm đã tồn tại 
            //new FoodCatBusiness(null).CheckDuplicate(foodcat.FoodName, GlobalConstants.LIST_SCHEMA, "FoodCat", "FoodName", true, foodcat.FoodID, "FoodCat_Label");
            //this.CheckDuplicate(foodcat.FoodName, GlobalConstants.LIST_SCHEMA, "FoodCat", "FoodName", true, foodcat.FoodID, "FoodCat_Label");

            IDictionary<string, object> expDic = null;
            if (foodcat.FoodID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["FoodID"] = foodcat.FoodID;
            }
            bool OtherServiceNameCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "FoodCat",
                   new Dictionary<string, object>()
                {
                    {"FoodName",foodcat.FoodName},
                    {"IsActive",true}
                }, expDic);
            if (OtherServiceNameCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("FoodCat_Label");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }

            foodcat.Calo = foodcat.Protein * 4 + foodcat.Fat * 9 + foodcat.Sugar * 4;
            base.Update(foodcat);
            List<FoodMineral> foodmineral = this.FoodMineralRepository.All.Where(o => o.FoodID == foodcat.FoodID).ToList();
            for (int i = 0, size = foodmineral.Count; i < size; i++)
            {
                var itemMineral = foodmineral[i];
                this.FoodMineralRepository.Delete(itemMineral.FoodMineralID);
            }
            //foreach (var itemMineral in foodmineral)
            //{
            //    this.FoodMineralRepository.Delete(itemMineral.FoodMineralID);
            //}
            for (int j = 0, size = lstFoodMineral.Count; j < size; j++)
            {
                var item = lstFoodMineral[j];
                item.FoodID = foodcat.FoodID;
                this.FoodMineralRepository.Insert(item);
            }
            //foreach (var item in lstFoodMineral)
            //{
            //    item.FoodID = foodcat.FoodID;
            //    this.FoodMineralRepository.Insert(item);
            //}

        }

        #endregion

        #region Delete

        public void Delete(int FoodID)
        {

            //Bạn chưa chọn thực phẩm cần xóa hoặc loại thực phẩm bạn chọn đã bị xóa khỏi hệ thống
            new FoodCatBusiness(null).CheckAvailable((int)FoodID, "FoodCat_Label", true);
            //Không thể xóa thực phẩm đang sử dụng
           // new FoodCatBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "FoodCat", FoodID, "FoodCat_Label");
            base.Delete(FoodID, true);

        }

        #endregion

        #region Search
        public IQueryable<FoodCat> Search(IDictionary<string, object> SearchInfo)
        {
            int FoodID = Utils.GetInt(SearchInfo, "FoodID");
            string FoodName = Utils.GetString(SearchInfo, "FoodName");
            string ShortName = Utils.GetString(SearchInfo, "ShortName");
            int TypeOfFoodID = Utils.GetInt(SearchInfo, "TypeOfFoodID");
            int GroupType = Utils.GetInt(SearchInfo, "GroupType");
            int FoodPackingID = Utils.GetInt(SearchInfo, "FoodPackingID");
            int FoodGroupID = Utils.GetInt(SearchInfo, "FoodGroupID");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive");

            IQueryable<FoodCat> lstFoodCat = this.FoodCatRepository.All;

            if (FoodID != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.FoodID == FoodID);
            }
            if (FoodName.Length != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.FoodName.Contains(FoodName));
            }
            if (ShortName.Length != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.ShortName.Contains(ShortName));
            }
            if (TypeOfFoodID != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.TypeOfFoodID == TypeOfFoodID);
            }
            if (GroupType != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.GroupType == GroupType);
            }
            if (FoodPackingID != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.FoodPackingID == FoodPackingID);
            }
            if (FoodGroupID != 0)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.FoodGroupID == FoodGroupID);
            }
            if (IsActive != false)
            {
                lstFoodCat = lstFoodCat.Where(fc => fc.IsActive == IsActive);
            }

            return lstFoodCat;
        }
        #endregion

        #region MineralOfFood
        public MineralOfFoodBO MineralOfFood(int FoodID)
        {
            FoodCat foodcat = this.FoodCatRepository.Find(FoodID);

            MineralOfFoodBO mof = new MineralOfFoodBO();
            mof.FoodID = FoodID;
            mof.Calo = foodcat.Calo.Value;
            mof.Sugar = foodcat.Sugar.Value;
            if (foodcat.GroupType == GlobalConstants.FOOD_GROUP_TYPE_ANIMAL)
            {
                mof.ProteinAnimal = foodcat.Protein.Value;
                mof.FatAnimal = foodcat.Fat.Value;
            }
            if (foodcat.GroupType == GlobalConstants.FOOD_GROUP_TYPE_PLANT)
            {
                mof.ProteinPlant = foodcat.Protein.Value;
                mof.FatPlant = foodcat.Fat.Value;
            }

            List<FoodMineralBO> lstFoodMineral = new List<FoodMineralBO>();

            List<FoodMineral> foodmineral = this.FoodMineralRepository.All.Where(o => o.FoodID == FoodID).ToList();
            for (int i = 0, size = foodmineral.Count; i < size; i++)
            {
                var item = foodmineral[i];
                FoodMineralBO fmBO = new FoodMineralBO();
                fmBO.MineralID = item.MinenalID;
                fmBO.Weight = item.Weight;
                lstFoodMineral.Add(fmBO);
            }
            //foreach (var item in foodmineral)
            //{
            //    FoodMineralBO fmBO = new FoodMineralBO();
            //    fmBO.MineralID = item.MinenalID;
            //    fmBO.Weight = item.Weight;
            //    lstFoodMineral.Add(fmBO);
            //}

            mof.lstFoodMineral = lstFoodMineral;
            return mof;

        }
        #endregion

        #region CaculatorContent

        public MineralOfDailyMenuBO CaculatorContent(List<int> lstFood, List<decimal> lstWeight, int EatingGroupID, int SchoolID, int AcademicYearID)
        {
            MineralOfDailyMenuBO modm = new MineralOfDailyMenuBO();
            FoodMineralMenuBO fmm = new FoodMineralMenuBO();
            if (lstFood.Count > 0 && lstWeight.Count > 0)
            {
                for (int i = 0; i < lstFood.Count; i++)
                {
                    MineralOfFoodBO mof = MineralOfFood(lstFood[i]);
                    FoodCat foodcat = this.FoodCatRepository.Find(lstFood[i]);
                    modm.ProteinAnimal += mof.ProteinAnimal * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                    modm.ProteinPlant += mof.ProteinPlant * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                    modm.FatAnimal += mof.FatAnimal * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                    modm.FatPlant += mof.FatPlant * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                    modm.Sugar += mof.Sugar * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                    modm.Calo += mof.Calo * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                    fmm.Value += mof.lstFoodMineral.FirstOrDefault().Weight * (1 - decimal.Parse(foodcat.DiscardedRate.Value.ToString()) / 100) * lstWeight[i];
                }
            }
            NutritionalNorm nn = this.NutritionalNormRepository.All.Where(o => o.SchoolID == SchoolID && o.EatingGroupID == EatingGroupID && o.EffectDate <= DateTime.Now)
                .OrderByDescending(o => o.EffectDate).FirstOrDefault();
            if (nn != null)
            {
                modm.ProteinAnimalMin = decimal.Parse((nn.DailyDemandFrom * nn.RateFrom * nn.ProteinFrom * nn.AnimalProteinFrom).ToString()) / (100 * 400);
                modm.ProteinAnimalMax = decimal.Parse((nn.DailyDemandTo * nn.RateTo * nn.ProteinTo * nn.AnimalProteinTo).ToString()) / (100 * 400);
                modm.ProteinPlantMin = decimal.Parse((nn.DailyDemandFrom * nn.RateFrom * nn.ProteinFrom * nn.PlantProteinFrom).ToString()) / (100 * 400);
                modm.ProteinPlantMax = decimal.Parse((nn.DailyDemandTo * nn.RateTo * nn.ProteinTo * nn.PlantProteinTo).ToString()) / (100 * 400);
                modm.FatAnimalMin = decimal.Parse((nn.DailyDemandFrom * nn.RateFrom * nn.FatFrom * nn.AnimalFatFrom).ToString()) / (100 * 900);
                modm.FatAnimalMax = decimal.Parse((nn.DailyDemandTo * nn.RateTo * nn.FatTo * nn.AnimalFatTo).ToString()) / (100 * 900);
                modm.FatPlantMin = decimal.Parse((nn.DailyDemandFrom * nn.RateFrom * nn.FatFrom * nn.PlantFatFrom).ToString()) / (100 * 900);
                modm.FatPlantMax = decimal.Parse((nn.DailyDemandTo * nn.RateTo * nn.FatTo * nn.PlantFatTo).ToString()) / (100 * 900);
                modm.SugarMin = decimal.Parse((nn.DailyDemandFrom * nn.RateFrom * nn.SugarFrom * nn.SugarFrom).ToString()) / (100 * 400);
                modm.SugarMax = decimal.Parse((nn.DailyDemandTo * nn.RateTo * nn.SugarTo * nn.SugarTo).ToString()) / (100 * 400);
                modm.CaloMin = decimal.Parse((nn.DailyDemandFrom * nn.RateFrom).ToString()) / 100;
                modm.CaloMax = decimal.Parse((nn.DailyDemandTo * nn.RateTo).ToString()) / 100;

                NutritionalNormMineral nnm = this.NutritionalNormMineralRepository.All.Where(o => o.NutritionalNormID == nn.NutritionalNormID).FirstOrDefault();
                if (nnm != null)
                {
                    fmm.MineralID = nnm.MinenalID;
                    fmm.ValueTo = nnm.ValueTo.Value;
                    fmm.ValueFrom = nnm.ValueFrom.Value;
                    fmm.MineralName = nnm.MinenalCat.MinenalName;
                }
            }
            List<FoodMineralMenuBO> lstFoodMineral = new List<FoodMineralMenuBO>();
            lstFoodMineral.Add(fmm);
            modm.lstFoodMineral = lstFoodMineral;
            modm.CountOfLstFoodMineral = lstFoodMineral.Count;
            return modm;

        }
        #endregion

        #region CaculatorRateByMeal
        public List<RateByMealBO> CaculatorRateByMeal(int DailyMenuID, int SchoolID, int AcademicYearID, int EatingGroupID)
        {
            List<RateByMealBO> lstRateByMeal = new List<RateByMealBO>();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            List<MealCat> lstMeal = this.MealCatBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();

            NutritionalNorm nn = this.NutritionalNormRepository.All.Where(o => o.SchoolID == SchoolID && o.EatingGroupID == EatingGroupID && o.EffectDate < DateTime.Now)
               .OrderByDescending(o => o.EffectDate).FirstOrDefault();
            for (int i = 0, size = lstMeal.Count; i < size; i++)
            {
                var item = lstMeal[i];
                //}
                //foreach (var item in lstMeal)
                //{
                var query = from dmd in DailyMenuDetailRepository.All
                            join dd in DishDetailRepository.All on dmd.DishID equals dd.DishID
                            where (
                               dmd.MealID == item.MealCatID &&
                               dmd.DailyMenuID == DailyMenuID
                               )
                            select new DailyMenuDishBO
                            {
                                WeightPerRation = dd.WeightPerRation,
                                FoodID = dd.FoodID
                            };
                List<int> lstFoodID = new List<int>();
                List<decimal> lstWeightPerRation = new List<decimal>();
                foreach (var itemQuery in query)
                {
                    lstFoodID.Add(itemQuery.FoodID);
                    lstWeightPerRation.Add(itemQuery.WeightPerRation);
                }
                MineralOfDailyMenuBO mineralByMeal = CaculatorContent(lstFoodID, lstWeightPerRation, EatingGroupID, SchoolID, AcademicYearID);
                RateByMealBO rbm = new RateByMealBO();
                rbm.MealID = item.MealCatID;
                rbm.Rate = mineralByMeal.Calo / nn.DailyDemandFrom;
                rbm.MealName = item.MealName;
                IQueryable<EnergyDistribution> energy = this.EnergyDistributionRepository.All.Where(o => o.EatingGroupID == EatingGroupID && o.MealID == item.MealCatID && o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID);
                if (energy.FirstOrDefault() != null)
                {
                    rbm.EnergyDemandFrom = energy.FirstOrDefault().EnergyDemandFrom;
                    rbm.EnergyDemandTo = energy.FirstOrDefault().EnergyDemandTo;
                }
                lstRateByMeal.Add(rbm);
            }

            return lstRateByMeal;
        }
        #region SearchBySchool
        public IQueryable<FoodCat> SearchBySchool(int SchoolID, IDictionary<string, object> food)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                food["SchoolID"] = SchoolID;
                return this.Search(food);
            }
        }
        #endregion
        #endregion

        /// <summary>
        /// Tính danh sách thực phẩm cần bổ sung
        /// author: Quanglm1
        /// </summary>
        /// <param name="lstFood">Danh sách thực phẩm</param>
        /// <param name="lstNutritionalToAdd">Danh sách các hàm lượng cần bổ sung</param>
        /// <param name="lstContentToAdd">Danh sách khối lượng cần bổ sung với mỗi loại.</param>
        /// <returns></returns>
        public List<decimal> CaculatorFoodToAdd(List<int> lstFood, List<int> lstNutritionalToAdd, List<decimal> lstContentToAdd, int numberOfChilden)
        {
            //TODO: Chuyen thanh dau vao la ma tran he so theo linear simplex - Quanglm
            // Code nay hoi ngu vi dang chuyen sang string de ham utils chuyen sang ma tran he so
            // Se tim hieu va lam cai nay sau, hien tai van chay duoc

            // Kiem tra du lieu dau vao
            if (lstNutritionalToAdd == null || lstContentToAdd == null || lstFood == null)
            {
                return null;
            }
            if (lstNutritionalToAdd.Count != lstContentToAdd.Count)
            {
                return null;
            }
            // Lay Dictionary map FoodID va FoodCat
            Dictionary<int, FoodCat> dicFoodCat = new Dictionary<int, FoodCat>();
            List<FoodCat> listFoodCat = this.FoodCatRepository.All.Where(o => lstFood.Contains(o.FoodID)).ToList();
            foreach (FoodCat foodCat in listFoodCat)
            {
                dicFoodCat[foodCat.FoodID] = foodCat;
            }
            string exp = "min P = ";
            int countFood = lstFood.Count;
            Dictionary<string, string> dicFoodVar = new Dictionary<string, string>();
            // Khoi tao ham muc tieu
            // khoi tao luon dieu kien moi bien khong nho thua 0
            string expPositive = string.Empty;
            for (int i = 1; i <= countFood; i++)
            {
                FoodCat foodCat = dicFoodCat[lstFood[i - 1]];
                if (i == 1)
                {
                    exp += foodCat.Price + "x" + i;
                }
                else
                {
                    exp += " + " + foodCat.Price + "x" + i;
                }
                expPositive += "x" + i + " >= 0;";
                // Day vao dictionary de lay khoi luong tuong ung
                dicFoodVar[foodCat.FoodID + "_" + i.ToString()] = "x" + i;
            }
            exp += ";";
            // Them bieu thuc lon hon 0            
            exp += expPositive;
            // Dieu kien rang buoc khac            
            // khoi tao danh sach he so cua phuong trinh           
            for (int i = 0; i < lstNutritionalToAdd.Count; i++)
            {
                int NutritionalToAdd = lstNutritionalToAdd[i];
                decimal ContentToAdd = lstContentToAdd[i];
                if (ContentToAdd <= 0)
                {
                    // Neu ham luong bo sung khong lon hon 0 thi bo qua
                    continue;
                }

                // Bieu thuc rang buoc doi voi moi loai dinh duong
                string expByNutrition = string.Empty;
                for (int j = 1; j <= countFood; j++)
                {
                    FoodCat foodCat = dicFoodCat[lstFood[j - 1]];
                    int discardedRate = foodCat.DiscardedRate.HasValue ? foodCat.DiscardedRate.Value : 0;
                    decimal protein = foodCat.Protein.HasValue ? foodCat.Protein.Value : 0;
                    decimal fat = foodCat.Fat.HasValue ? foodCat.Fat.Value : 0;
                    decimal sugar = foodCat.Sugar.HasValue ? foodCat.Sugar.Value : 0;
                    decimal cof = (1 - (decimal)discardedRate / 100) * 1000 / numberOfChilden;
                    // Dam dong vat
                    if (NutritionalToAdd == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN)
                    {
                        if (foodCat.GroupType != SystemParamsInFile.FOOD_GROUP_TYPE_ANIMAL)
                        {
                            continue;
                        }
                        cof = cof * protein;
                    }
                    else if (NutritionalToAdd == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN)
                    {
                        // Dam thuc vat
                        if (foodCat.GroupType != SystemParamsInFile.FOOD_GROUP_TYPE_PLANT)
                        {
                            continue;
                        }
                        cof = cof * protein;
                    }
                    else if (NutritionalToAdd == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT)
                    {
                        // Chat beo dong vat
                        if (foodCat.GroupType != SystemParamsInFile.FOOD_GROUP_TYPE_ANIMAL)
                        {
                            continue;
                        }
                        cof = cof * fat;
                    }
                    else if (NutritionalToAdd == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT)
                    {
                        // Chat beo thuc vat
                        if (foodCat.GroupType != SystemParamsInFile.FOOD_GROUP_TYPE_PLANT)
                        {
                            continue;
                        }
                        cof = cof * fat;
                    }
                    else if (NutritionalToAdd == SystemParamsInFile.NUTRITION_COMPONENT_SUGAR)
                    {
                        // Duong
                        cof = cof * sugar;
                    }

                    // Lam tron den 10 chu so
                    cof = Math.Round(cof, 10);
                    expByNutrition += " + " + cof + "x" + j;
                }
                // Equal
                if (!string.Empty.Equals(expByNutrition))
                {
                    exp += expByNutrition.Substring(3) + " >= " + ContentToAdd + ";";
                }
            }
            exp = exp.Substring(0, exp.Length - 1);
            Simplex Simplex = new Simplex(exp.Replace('.', ','));
            Simplex.DoIt();
            Dictionary<string, decimal> res = Simplex.DicVariableVal;
            if (res == null)
            {
                // khong co bo nghiem phu hop
                return null;
            }
            List<decimal> listOptionalRes = new List<decimal>();
            for (int i = 1; i <= countFood; i++)
            {
                decimal val = res[dicFoodVar[lstFood[i - 1] + "_" + i.ToString()]];
                listOptionalRes.Add(val);
            }
            return listOptionalRes;
        }
    }
}