﻿using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class StatisticLevelConfigBusiness
    {
        public IQueryable<StatisticLevelConfig> Search(IDictionary<string, object> dic)
        {
            int StatisticLevelReportID = Utils.GetInt(dic, "StatisticLevelReportID");
            IQueryable<StatisticLevelConfig> lsStatisticLevelConfig = this.StatisticLevelConfigRepository.All;

            if (StatisticLevelReportID > 0)
            {
                lsStatisticLevelConfig = lsStatisticLevelConfig.Where(o => o.StatisticLevelReportID == StatisticLevelReportID);
            }
            return lsStatisticLevelConfig;
        }
    }
}
