﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ApprenticeshipGroupBusiness
    {
        

        #region Search

        /// <summary>
        /// Tìm kiếm nhóm môn học nghề
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="GroupName">Tên nhóm</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>Danh sánh nhóm môn học nghề</returns>
        public IQueryable<ApprenticeshipGroup> Search(IDictionary<string, object> dic)
        {
            string GroupName = Utils.GetString(dic,"GroupName");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<ApprenticeshipGroup> lsGroup = ApprenticeshipGroupRepository.All;

            if (IsActive.HasValue)
                lsGroup = lsGroup.Where(app => app.IsActive == IsActive);
            if (GroupName.Trim().Length != 0)
                lsGroup = lsGroup.Where(app => app.GroupName.Contains(GroupName.ToLower()));
            return lsGroup;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm nhóm môn học nghề
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="apprenticeshipGroup">Đối tượng nhóm môn học nghề</param>
        /// <returns>Đối tượng nhóm môn học nghề</returns>
        public override ApprenticeshipGroup Insert(ApprenticeshipGroup apprenticeshipGroup)
        {
            //Tên nhóm môn học nghề không được để trống - Kiểm tra GroupName 
            Utils.ValidateRequire(apprenticeshipGroup.GroupName, "ApprenticeshipGroup_GroupName");
            //Độ dài trường Tên nhóm môn học nghề không được vượt quá 200 - Kiểm tra GroupName 
            Utils.ValidateMaxLength(apprenticeshipGroup.GroupName, 200, "ApprenticeshipGroup_GroupName");
            //Tên nhóm môn học nghề đã tồn tại - Kiểm tra GroupName  với IsActive=TRUE
            this.CheckDuplicate(apprenticeshipGroup.GroupName, GlobalConstants.LIST_SCHEMA, "ApprenticeshipGroup", "GroupName", true, apprenticeshipGroup.ApprenticeshipGroupID, "ApprenticeshipGroup_GroupName");
            return base.Insert(apprenticeshipGroup);
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa nhóm môn học nghề
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="apprenticeshipGroup">Đối tượng nhóm môn học nghề</param>
        /// <returns>Đối tượng nhóm môn học nghề</returns>
        public override ApprenticeshipGroup Update(ApprenticeshipGroup apprenticeshipGroup)
        {
            //Bạn chưa chọn Nhóm môn học nghề cần sửa hoặc Nhóm môn học nghề bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable(apprenticeshipGroup.ApprenticeshipGroupID, "ApprenticeshipGroup_ApprenticeshipGroupID");
            //Tên nhóm môn học nghề không được để trống - Kiểm tra GroupName 
            Utils.ValidateRequire(apprenticeshipGroup.GroupName, "ApprenticeshipGroup_GroupName");
            //Độ dài trường Tên nhóm môn học nghề không được vượt quá 200 - Kiểm tra GroupName 
            Utils.ValidateMaxLength(apprenticeshipGroup.GroupName, 200, "ApprenticeshipGroup_GroupName");
            //Tên nhóm môn học nghề đã tồn tại - Kiểm tra GroupName  với IsActive=TRUE
            this.CheckDuplicate(apprenticeshipGroup.GroupName, GlobalConstants.LIST_SCHEMA, "ApprenticeshipGroup", "GroupName", true, apprenticeshipGroup.ApprenticeshipGroupID, "ApprenticeshipGroup_GroupName");
            
            return base.Update(apprenticeshipGroup);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa nhóm môn học nghề
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ApprenticeshipGroupID"> ID Đối tượng nhóm môn học nghề</param>
        public void Delete(int ApprenticeshipGroupID)
        {
            //Bạn chưa chọn Nhóm môn học nghề cần xóa hoặc Nhóm môn học nghề bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable(ApprenticeshipGroupID, "ApprenticeshipGroup_ApprenticeshipGroupID");
            //Không thể xóa Nhóm môn học nghề đang sử dụng - Kiểm tra xem có bảng nào đang dùng Nhóm môn học nghề này không 
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "ApprenticeshipGroup", ApprenticeshipGroupID, "ApprenticeshipGroup_ApprenticeshipGroupID");

            base.Delete(ApprenticeshipGroupID,true);

        }
        #endregion

    }
}