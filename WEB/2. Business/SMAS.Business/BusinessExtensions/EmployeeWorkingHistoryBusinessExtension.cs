﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    /// <summary>
    /// lịch sử quá trình công tác của nhân viên
    /// <author>dungnt</author>
    /// <date>06/09/2012</date>
    /// </summary>
    public partial class EmployeeWorkingHistoryBusiness
    {
        #region private member variable
        private const int FROMDATE_SUBTRACT_BIRTHDATE = 17; //Ngày bắt đầu công tác phải lớn hơn ngày sinh + 17 năm 
        private const int ORGANIZATION_MAXLENGTH = 400;//Thông tin cơ quan/ đơn vị công tác không được nhập quá 400 ký tự
        private const int POSITION_MAXLENGTH = 200; //Chức vụ không được nhập quá 200 ký tự 
        private const int RESOLUTION_MAXLENGTH = 400;   //Công việc không được nhập quá 400 ký tự 
        private const int DEPARTMENT_MAXLENGTH = 200;   //Phòng ban không được nhập quá 200 ký tự 
        private const int EMPLOYEE_TYPE_TEACHER = 1;
        private const int EMPLOYEE_TYPE_SUPERVISINGDEPT = 2;

        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới lịch sử quá trình công tác của nhân viên
        /// <author>dungnt</author>
        /// <date>06/09/2012</date>
        /// </summary>
        /// <param name="insertEmployeeWorkingHistory">Đối tượng Insert</param>
        /// <returns></returns>
        public override EmployeeWorkingHistory Insert(EmployeeWorkingHistory insertEmployeeWorkingHistory)
        {
            Validate(insertEmployeeWorkingHistory);

            //Thực hiện Insert dữ liệu vào bảng EmployeeWorkingHistory
            return base.Insert(insertEmployeeWorkingHistory);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa lịch sử quá trình công tác của nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="insertEmployeeWorkingHistory">Đối tượng Update</param>
        /// <returns></returns>
        public override EmployeeWorkingHistory Update(EmployeeWorkingHistory insertEmployeeWorkingHistory)
        {
            //Kiểm tra EmployeeWorkingHistoryID có tồn tại hay không?
            CheckAvailable(insertEmployeeWorkingHistory.EmployeeWorkingHistoryID, "EmployeeWorkingHistory_Label_EmployeeWorkingHistoryID", false);

            //EmployeeWorkingHistoryID, SchoolID: not compatible(EmployeeWorkingHistory)
            if (insertEmployeeWorkingHistory.SchoolID != null)
            {
                bool EmployeeWorkingHistoryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkingHistory",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployeeWorkingHistory.SchoolID},
                    {"EmployeeWorkingHistoryID",insertEmployeeWorkingHistory.EmployeeWorkingHistoryID}
                }, null);
                if (!EmployeeWorkingHistoryCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //EmployeeQualificationID, SupervisingDeptID: not compatible(EmployeeQualification)
            if (insertEmployeeWorkingHistory.SupervisingDeptID != null)
            {
                bool EmployeeWorkingHistoryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkingHistory",
                   new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",insertEmployeeWorkingHistory.SupervisingDeptID},
                    {"EmployeeWorkingHistoryID",insertEmployeeWorkingHistory.EmployeeWorkingHistoryID}
                }, null);
                if (!EmployeeWorkingHistoryCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            Validate(insertEmployeeWorkingHistory);

            return base.Update(insertEmployeeWorkingHistory);

        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa lịch sử  quá trình công tác của nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="id">ID quá trình công tác của nhân viên</param>
        public void Delete(int SchoolOrSupervisingDeptID, int EmployeeWorkingHistoryID)
        {

            //Bản ghi này đã bị xóa hoặc không tồn tại trong cơ sở dữ liệu. Kiểm tra EmployeeWorkingHistoryID != NULL
            CheckAvailable(EmployeeWorkingHistoryID, "EmployeeWorkingHistory_Label_EmployeeWorkingHistoryID", false);

            //EmployeeWorkingHistoryID => EmployeeID
            EmployeeWorkingHistory tempEmployeeWorkingHistory = EmployeeWorkingHistoryRepository.Find(EmployeeWorkingHistoryID);
            Employee checkEmployee = EmployeeRepository.Find(tempEmployeeWorkingHistory.EmployeeID);

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SCHOOL_TEACHER: 
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {

                //EmployeeID, SchoolOrSupervisingDeptID: not compatible(Employee)
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolOrSupervisingDeptID},
                    {"EmployeeID",checkEmployee.EmployeeID}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //EmployeeWorkingHistoryID, SchoolOrSupervisingDeptID(SchoolID):  not compatible(EmployeeWorkingHistory)
                bool EmployeeWorkingHistoryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkingHistory",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolOrSupervisingDeptID},
                    {"EmployeeWorkingHistoryID",EmployeeWorkingHistoryID}
                }, null);
                if (!EmployeeWorkingHistoryCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {

                //SupervisingDeptID, SchoolID: not compatible(Employee)
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SchoolOrSupervisingDeptID},
                    {"EmployeeID",checkEmployee.EmployeeID}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //+ + EmployeeWorkingHistoryID, SchoolOrSupervisingDeptID(SupervisingDeptID):  not compatible(EmployeeWorkingHistory
                bool EmployeeWorkingHistoryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkingHistory",
                new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SchoolOrSupervisingDeptID},
                    {"EmployeeWorkingHistoryID",EmployeeWorkingHistoryID}
                }, null);
                if (!EmployeeWorkingHistoryCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            base.Delete(EmployeeWorkingHistoryID, false);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm lịch sử  quá trình công tác của nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="dic">danh sách tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        private IQueryable<EmployeeWorkingHistory> Search(IDictionary<string, object> dic)
        {
            //-	EmployeeID: default = 0; 0 => tìm kiếm All
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            //-	SchoolID: default = 0; 0 => Tìm kiếm All – join sang bảng SchoolProfile
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            //-	SupervisingDeptID: default = 0 => Tìm kiếm All – Join sang bảng SupervisingDept
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            //-	Organization: default = “ ”; “ ” => Tìm kiếm All
            string Organization = Utils.GetString(dic, "Organization");
            //-	Department : default = “ ” ; “ ” => Tìm kiếm All
            string Department = Utils.GetString(dic, "Department");
            //-	Position: default = “ ”; “ ”=> Tìm kiếm All
            string Position = Utils.GetString(dic, "Position");
            //-	FromDate: default = “ ”; “ ” => Tìm kiếm All
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            //-	ToDate: default = “ ”; “ ” => Tìm kiếm All
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");

            IQueryable<EmployeeWorkingHistory> lsEmployeeWorkingHistory = EmployeeWorkingHistoryRepository.All;

            if (EmployeeID != 0)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.EmployeeID == EmployeeID));
            }
            if (SchoolID != 0)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.SchoolID == SchoolID));
            }
            if (SupervisingDeptID != 0)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.SupervisingDeptID == SupervisingDeptID));
            }
            if (Organization.Trim().Length != 0)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.Organization.ToUpper().Contains(Organization.ToUpper())));
            }
            if (Department.Trim().Length != 0)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.Department.ToUpper().Contains(Department.ToUpper())));
            }
            if (Position.Trim().Length != 0)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.Position.ToUpper().Contains(Position.ToUpper())));
            }
            if (FromDate.HasValue)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.FromDate == FromDate));
            }
            if (ToDate.HasValue)
            {
                lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.ToDate == ToDate));
            }
            return lsEmployeeWorkingHistory;

        }

        public IQueryable<EmployeeWorkingHistory> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        public IQueryable<EmployeeWorkingHistory> SearchBySupervisingDept(int SupervisingDeptID, IDictionary<string, object> dic = null)
        {
            if (SupervisingDeptID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SupervisingDeptID"] = SupervisingDeptID;
            return Search(dic);
        }

        public IQueryable<EmployeeBO> SearchTeacher(int schoolID, IDictionary<string, object> dic = null)
        {
            int CurrentSchoolID = Utils.GetInt(dic, "CurrentSchoolID");
            int CurrentEmployeeStatus = Utils.GetInt(dic, "CurrentEmployeeStatus");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EmploymentStatus = Utils.GetInt(dic, "EmploymentStatus");
            string EmployeeCode = Utils.GetString(dic, "EmployeeCode", null);
            string FullName = Utils.GetString(dic, "FullName", null);
            bool? Genre = Utils.GetNullableBool(dic, "Genre");
            int TrainingLevelID = Utils.GetInt(dic, "TrainingLevelID");
            int ContractTypeID = Utils.GetInt(dic, "ContractTypeID");
            int ITQualificationLevelID = Utils.GetInt(dic, "ITQualificationLevelID");
            int QualificationLevelID = Utils.GetInt(dic, "QualificationLevelID");
            int StaffPositionID = Utils.GetInt(dic, "StaffPosition");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int WorkTypeID = Utils.GetInt(dic, "WorkTypeID");
            int WorkGroupTypeID = Utils.GetInt(dic, "WorkGroupTypeID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int UserID = Utils.GetInt(dic, "UserID");
            string FacultyName = Utils.GetString(dic, "FacultyName");
            string LoginName = Utils.GetString(dic, "LoginName");
            int GroupCatID = Utils.GetInt(dic, "GroupCatID");
            int EmployeeType = Utils.GetInt(dic, "EmployeeType");
            DateTime? BirthDate = Utils.GetDateTime(dic, "BirthDate");
            var query = from ehs in EmployeeHistoryStatusBusiness.All
                        join e in EmployeeBusiness.All on ehs.EmployeeID equals e.EmployeeID
                        join p in ProvinceBusiness.All.Where(p => p.IsActive == true) on e.ProvinceId equals p.ProvinceID into empp
                        from ep in empp.DefaultIfEmpty()
                        join d in DistrictBusiness.All.Where(d => d.IsActive == true) on e.DistrictId equals d.DistrictID into empd
                        from ed in empd.DefaultIfEmpty()
                        join c in CommuneBusiness.All.Where(c => c.IsActive == true) on e.CommuneId equals c.CommuneID into empc
                        from ec in empc.DefaultIfEmpty()
                        select new EmployeeBO
                        {

                            EmployeeID = e.EmployeeID,
                            EmployeeCode = e.EmployeeCode,
                            EmployeeType = e.EmployeeType,
                            EmployeeStatus = ehs.EmployeeStatus,
                            Description = e.Description,
                            SupervisingDeptID = ehs.SupervisingDeptID,
                            SupervisingDeptName = ehs.SupervisingDept.SupervisingDeptName,
                            SchoolID = ehs.SchoolID,
                            Name = e.Name,
                            FullName = e.FullName,
                            BirthDate = e.BirthDate,
                            IdentifyNumber = e.IdentityNumber,
                            IdentityIssuedDate = e.IdentityIssuedDate,
                            IdentityIssuedPlace = e.IdentityIssuedPlace,
                            Telephone = e.Telephone,
                            Mobile = e.Mobile,
                            HomeTown = e.HomeTown,
                            Email = e.Email,
                            Genre = e.Genre,
                            JoinedDate = e.JoinedDate,
                            StartingDate = e.StartingDate,
                            HealthStatus = e.HealthStatus,
                            RegularRefresher = e.RegularRefresher,
                            GraduationLevelID = e.GraduationLevelID,
                            GraduationLevelName = e.GraduationLevel.Resolution,
                            ContractID = e.ContractID,
                            ContractName = e.Contract.ContractCode,
                            QualificationTypeID = e.QualificationTypeID,
                            QualificationTypeName = e.QualificationType.Resolution,
                            SpecialityCatID = e.SpecialityCatID,
                            SpecialityCatName = e.SpecialityCat.Resolution,
                            QualificationLevelID = e.QualificationLevelID,
                            QualificationLevelName = e.QualificationLevel.Resolution,
                            ITQualificationLevelID = e.ITQualificationLevelID,
                            ITQualificationLevelName = e.ITQualificationLevel.Resolution,
                            WorkTypeID = e.WorkTypeID,
                            TrainingLevelID = e.TrainingLevelID,
                            StaffPositionID = e.StaffPositionID,
                            WorkTypeName = e.WorkType.Resolution,
                            SchoolFacultyID = e.SchoolFacultyID,
                            SchoolFacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : string.Empty,
                            ContractTypeID = e.ContractTypeID,
                            ContractTypeName = e.ContractType.Resolution,
                            WorkGroupTypeID = e.WorkGroupTypeID,
                            WorkGroupTypeName = e.WorkGroupType.Resolution,
                            TrainingLevelResolution = e.TrainingLevel.Resolution,
                            FromDate = ehs.FromDate,
                            ToDate = ehs.ToDate,
                            IsActive = e.IsActive,
                            EmploymentStatus = e.EmploymentStatus,
                            PermanentResidentalAddress = e.PermanentResidentalAddress,
                            IntoSchoolDate = e.IntoSchoolDate,
                            EthnicName = e.Ethnic != null ? e.Ethnic.EthnicName : null,
                            ReligionName = e.Religion != null ? e.Religion.Resolution : null,
                            AppliedLevel = e.AppliedLevel,
                            FatherFullName = e.FatherFullName,
                            FatherBirthDate = e.FatherBirthDate,
                            FatherJob = e.FatherJob,
                            FatherWorkingPlace = e.FatherWorkingPlace,
                            MotherFullName = e.MotherFullName,
                            MotherBirthDate = e.MotherBirthDate,
                            MotherJob = e.MotherJob,
                            MotherWorkingPlace = e.MotherWorkingPlace,
                            SpouseFullName = e.SpouseFullName,
                            SpouseBirthDate = e.SpouseBirthDate,
                            SpouseJob = e.SpouseJob,
                            SpouseWorkingPlace = e.SpouseWorkingPlace,
                            ForeignLanguageGradeName = e.ForeignLanguageGrade.Resolution,
                            PoliticalGradeName = e.PoliticalGrade.Resolution,
                            StateManagementGradeName = e.StateManagementGrade.Resolution,
                            EducationalManagementGradeName = e.EducationalManagementGrade.Resolution,
                            DedicatedForYoungLeague = e.DedicatedForYoungLeague,
                            IsCommunistPartyMember = e.IsCommunistPartyMember,
                            IsYouthLeageMember = e.IsYouthLeageMember,
                            IsSyndicate = e.IsSyndicate,
                            FamilyTypeName = e.FamilyType.Resolution,
                            SpecialityCatResolution = e.SpecialityCat.Resolution,
                            ITQualificationLevelResolution = e.ITQualificationLevel.Resolution,
                            InsuranceNumber = e.InsuranceNumber,
                            ProvinceName = ep.ProvinceName,
                            DistrictName = ed.DistrictName,
                            CommuneName = ec.CommuneName,
                            
                        };

            query = query.Where(u => u.IsActive);

            if (CurrentSchoolID > 0)
                query = query.Where(u => u.SchoolID == CurrentSchoolID);

            if (CurrentEmployeeStatus > 0)
                query = query.Where(u => u.EmploymentStatus == CurrentEmployeeStatus);

            query = query.Where(u => u.SchoolID == schoolID);

            if (AcademicYearID > 0)
            {
                AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                query = query.Where(u => u.FromDate <= aca.SecondSemesterEndDate && (u.ToDate == null || u.ToDate >= aca.FirstSemesterStartDate || u.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_BREATHER || u.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING));
            }

            if (EmploymentStatus > 0)
                query = query.Where(u => u.EmployeeStatus == EmploymentStatus);

            if (!string.IsNullOrEmpty(EmployeeCode))
                query = query.Where(u => u.EmployeeCode.ToLower().Contains(EmployeeCode.ToLower()));

            if (!string.IsNullOrEmpty(FullName))
                query = query.Where(u => u.FullName.ToLower().Contains(FullName.ToLower()));

            if (Genre.HasValue)
                query = query.Where(u => u.Genre == Genre.Value);

            if (TrainingLevelID > 0)
                query = query.Where(u => u.TrainingLevelID == TrainingLevelID);
            if (BirthDate.HasValue)
            {
                query = query.Where(em => (em.BirthDate.Value.Year == BirthDate.Value.Year)
                    && (em.BirthDate.Value.Month == BirthDate.Value.Month)
                    && (em.BirthDate.Value.Day == BirthDate.Value.Day));
            }

            if (ContractTypeID > 0)
                query = query.Where(u => u.ContractTypeID == ContractTypeID);

            if (ITQualificationLevelID > 0)
                query = query.Where(u => u.ITQualificationLevelID == ITQualificationLevelID);

            if (QualificationLevelID > 0)
                query = query.Where(u => u.QualificationLevelID == QualificationLevelID);

            if (StaffPositionID > 0)
                query = query.Where(u => u.StaffPositionID == StaffPositionID);

            if (FacultyID > 0)
                query = query.Where(u => u.SchoolFacultyID == FacultyID);

            if (WorkTypeID > 0)
                query = query.Where(u => u.WorkTypeID == WorkTypeID);

            if (WorkGroupTypeID > 0)
                query = query.Where(u => u.WorkGroupTypeID == WorkGroupTypeID);

            if (SupervisingDeptID > 0)
                query = query.Where(u => u.SupervisingDeptID == SupervisingDeptID);

            var queryMax = query.Where(u => u.FromDate == query.Where(v => v.SchoolID == u.SchoolID && v.EmployeeID == u.EmployeeID).Select(v => v.FromDate).Max());

            return queryMax;
        }

        #endregion



        #region validate
        public void Validate(EmployeeWorkingHistory insertEmployeeWorkingHistory)
        {
            //Nhân viên không tồn tại - Kiểm tra EmployeeID 
            EmployeeBusiness.CheckAvailable(insertEmployeeWorkingHistory.EmployeeID, "Employee_Label_EmployeeID");

            //Thông tin cơ quan/ đơn vị công tác không được để trống – Kiểm tra Organization
            Utils.ValidateRequire(insertEmployeeWorkingHistory.Organization, "EmployeeWorkingHistory_Label_Organization");
            //Thông tin cơ quan/ đơn vị công tác không được nhập quá 400 ký tự. – Kiểm tra Organization
            Utils.ValidateMaxLength(insertEmployeeWorkingHistory.Organization, ORGANIZATION_MAXLENGTH, "EmployeeWorkingHistory_Label_Organization");

            //Thông tin chức vụ không được để trống- Kiểm tra Position
            Utils.ValidateRequire(insertEmployeeWorkingHistory.Position, "EmployeeWorkingHistory_Label_Position");
            //Chức vụ không được nhập quá 200 ký tự - Kiểm tra Position
            Utils.ValidateMaxLength(insertEmployeeWorkingHistory.Position, POSITION_MAXLENGTH, "EmployeeWorkingHistory_Label_Position");

            //Thông tin công việc không được để trống – Kiểm tra Resolution
            Utils.ValidateRequire(insertEmployeeWorkingHistory.Resolution, "EmployeeWorkingHistory_Label_Resolution");
            //Công việc không được nhập quá 400 ký tự - Kiểm tra Resolution
            Utils.ValidateMaxLength(insertEmployeeWorkingHistory.Resolution, RESOLUTION_MAXLENGTH, "EmployeeWorkingHistory_Label_Resolution");

            //FromDate < ToDate
            //FromDate <= DateTime.Now
            //FromDate > Employee(EmployeeID).BirthDate + 17 years

            Employee checkEmployee = EmployeeRepository.All.Where(o => (o.EmployeeID == insertEmployeeWorkingHistory.EmployeeID)).FirstOrDefault();
            if (checkEmployee.BirthDate != null)
            {
                Utils.ValidateAfterDate(insertEmployeeWorkingHistory.FromDate.Value, checkEmployee.BirthDate.Value, FROMDATE_SUBTRACT_BIRTHDATE, "EmployeeWorkingHistory_Label_FromDate", "Employee_Label_BirthDate");
            }
            if (insertEmployeeWorkingHistory.ToDate.HasValue)
            {
                //Utils.ValidateAfterDate(insertEmployeeWorkingHistory.ToDate.Value, insertEmployeeWorkingHistory.FromDate.Value, "EmployeeWorkingHistory_Label_EndDate", "EmployeeQualification_Label_FromDate");
                Utils.ValidateAfterDate(insertEmployeeWorkingHistory.ToDate.Value, insertEmployeeWorkingHistory.FromDate.Value, "EmployeeWorkingHistory_Label_EndDate", "EmployeeWorkingHistory_Label_FromDate");
            }
            //Utils.ValidateAfterDate(DateTime.Now, insertEmployeeWorkingHistory.FromDate.Value, "Employee_Label_Date_Now", "EmployeeQualification_Label_FromDate");
            Utils.ValidatePastDateNow(insertEmployeeWorkingHistory.FromDate.Value, "EmployeeWorkingHistory_Label_FromDate");

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SCHOOL_TEACHER: 
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {
                //Kiểm tra sự tồn tại của SchoolID truyền vào
                SchoolProfileBusiness.CheckAvailable(insertEmployeeWorkingHistory.SchoolID, "SchoolProfile_Label_SchoolID");

                if (insertEmployeeWorkingHistory.SchoolID.HasValue)
                {
                    //EmployeeID, SchoolID: not compatible(Employee)
                    bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                      new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployeeWorkingHistory.SchoolID},
                    {"EmployeeID",insertEmployeeWorkingHistory.EmployeeID}
                }, null);
                    if (!EmployeeCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {
                //+ SupervisingDeptID: require
                //Phòng ban không tồn tại – Kiểm tra SupervisingDeptID
                SupervisingDeptBusiness.CheckAvailable(insertEmployeeWorkingHistory.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID");

                //SupervisingDeptID, EmployeeID: not compatible(Employee)
                if (insertEmployeeWorkingHistory.EmployeeID.HasValue && insertEmployeeWorkingHistory.SupervisingDeptID.HasValue)
                {
                    bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                      new Dictionary<string, object>()
                {
                    {"EmployeeID",insertEmployeeWorkingHistory.EmployeeID},
                    {"SupervisingDeptID",insertEmployeeWorkingHistory.SupervisingDeptID}
                }, null);
                    if (!EmployeeCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
            }


            //Khoảng thời gian FromDate – ToDate không được giao với khoảng thời gian FromDate – ToDate 
            //    ở bản ghi khác với cùng một nhân viên (Employee) - Khoảng thời gian công tác phải nằm ngoài 
            //        các khoảng thời gian công tác đã nhập trước đó của cán bộ  
            IQueryable<EmployeeWorkingHistory> lsEmployeeWorkingHistory = EmployeeWorkingHistoryRepository.All;
            lsEmployeeWorkingHistory = lsEmployeeWorkingHistory.Where(o => (o.EmployeeID == insertEmployeeWorkingHistory.EmployeeID)).Where(o => (o.EmployeeWorkingHistoryID != insertEmployeeWorkingHistory.EmployeeWorkingHistoryID));

            foreach (EmployeeWorkingHistory item in lsEmployeeWorkingHistory)
            {
                if (insertEmployeeWorkingHistory.FromDate.HasValue)
                {
                    if (item.FromDate.HasValue && item.ToDate.HasValue)
                    {
                        if (Utils.CompareDateTimeRange(insertEmployeeWorkingHistory.FromDate.Value, insertEmployeeWorkingHistory.ToDate, item.FromDate.Value, item.ToDate.Value))
                        {
                            throw new BusinessException("EmployeeWorkingHistory_Label_DateConflict", new List<object>() { });
                        }
                    }
                }
            }
        }
        #endregion

    }
}