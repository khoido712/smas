﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author  trangdd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using System.Drawing;
using SMAS.Models.Models.CustomModels;
using System.Transactions;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class JudgeRecordHistoryBusiness
    {
        public bool InsertJudgeRecordHistory(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int? EmployeeID)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);

                bool isDBChange = false;
                #region validate 1
                if (lstJudgeRecordHistory == null || lstJudgeRecordHistory.Count == 0) return isDBChange;
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

                //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");

                // Kiem tra quyen giao vien bo mon
                if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID, Semester))
                    throw new BusinessException("JudgeRecord_Label_NotAuthen_Add");

                //Academic not compatible
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //PeriodID(PeriodDeclarationID), Semester: not compatible(PeriodDeclaration)
                if (Period != null)
                {
                    bool PeriodDeclarationCompatible = new PeriodDeclarationRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                            new Dictionary<string, object>()
                    {
                    {"PeriodDeclarationID",Period},
                    {"Semester",Semester}
                    }, null);
                    if (!PeriodDeclarationCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion
                var listPupilID = lstJudgeRecordHistory.Select(u => u.PupilID).Distinct().ToList();
                ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();
                ClassProfile classProfile = classSubject.ClassProfile;
                var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               where poc.ClassID == ClassID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                    && listPupilID.Contains(poc.PupilID) && pp.IsActive
                               select new { poc.Status, poc.AcademicYearID, poc.ClassID }
                               ).ToList();

                // Kiem tra trang thai hoc sinh dang hoc
                if (listPOC.Count != listPupilID.Count)
                {
                    throw new BusinessException("Import_Validate_StudyingStatus");
                }
                // Kiem tra nam hoc va lop hoc hien tai
                if (listPOC.Any(o => o.AcademicYearID != AcademicYearID || o.ClassID != ClassID))
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                #region Lay thong tin mien giam
                List<ExemptedSubject> listExemptedByClass = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID).Where(u => u.SubjectID == SubjectID).ToList();

                // HKT
                List<ExemptedSubject> listExemptedHKI = listExemptedByClass.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                //HK2
                List<ExemptedSubject> listExemptedHKII = listExemptedByClass.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<ExemptedSubject> listExempted = new List<ExemptedSubject>();
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    listExempted.AddRange(listExemptedHKI);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listExempted.AddRange(listExemptedHKII);
                }
                #endregion

                #region validate 2
                if (classSubject == null)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = ISCOMMENTING_TYPE_JUDGE
                //(nếu kết quả trả về != null => là môn nhận xét)
                if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    throw new BusinessException("SummedUpRecord_Labal_ErrNXSubject");

                bool subjectLearnInHKI = classSubject.SectionPerWeekFirstSemester > 0;
                bool subjectLearnInHKII = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester > 0;

                foreach (var JudgeRecord in lstJudgeRecordHistory)
                {
                    //Kiem tra diem thi hop le
                    if (JudgeRecord.Judgement != "-1")
                    {
                        UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, GlobalConstants.ISCOMMENTING_TYPE_JUDGE, JudgeRecord.Judgement);
                    }
                }
                #endregion

                string strMarkTitle = Period.HasValue ? MarkRecordBusiness.GetMarkTitle(Period.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<string> lstTitle = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                List<string> ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();

                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                //neu khong phai tai khoan quan tri truong va tat ca con diem bi khoa thi bao loi
                if (lstLockedMarkDetail.Contains("LHK") && !isSchoolAdmin)
                {
                    throw new BusinessException("MarkRecord_Validate_MarkLock");
                }
                // Neu khong phai la tk quan tri truong thi bo qua cac con diem bi khoa
                if (!isSchoolAdmin)
                {
                    lstJudgeRecordHistory = lstJudgeRecordHistory.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                }
                IDictionary<string, object> SearchDelete = new Dictionary<string, object>();
                SearchDelete["AcademicYearID"] = AcademicYearID;
                SearchDelete["SchoolID"] = SchoolID;
                SearchDelete["ClassID"] = ClassID;
                SearchDelete["SubjectID"] = SubjectID;
                SearchDelete["Semester"] = Semester;
                SearchDelete["checkWithClassMovement"] = "1";
                IQueryable<JudgeRecordHistory> iqJudgeDb = this.SearchJudgeRecordHistory(SearchDelete).Where(u => listPupilID.Contains(u.PupilID));
                // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                if (!isSchoolAdmin)
                {
                    iqJudgeDb = iqJudgeDb.Where(v => !ArrLockedMark.Contains(v.Title));
                    for (int i = 0; i < ArrLockedMark.Count; i++)
                    {
                        if (strtmp.Contains(ArrLockedMark[i]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[i], "");
                        }
                    }
                }
                if (Period.HasValue)
                    iqJudgeDb = iqJudgeDb.Where(u => lstTitle.Contains(u.Title));
                List<JudgeRecordHistory> listJudgeDb = iqJudgeDb.ToList();


                //Insert du lieu moi vao DB
                if (lstJudgeRecordHistory != null && lstJudgeRecordHistory.Count > 0)
                {
                    List<JudgeRecordHistory> lstJudgeInsert = lstJudgeRecordHistory.Where(p => !p.Judgement.Equals(string.Empty) && !"-1".Equals(p.Judgement)).ToList();
                    JudgeRecordHistory mrOfPupilObj = null;
                    if (lstJudgeInsert != null && lstJudgeInsert.Count > 0)
                    {
                        int _pupilId = 0;
                        string _title = string.Empty;
                        string _mark = string.Empty;
                        foreach (JudgeRecordHistory itemInsert in lstJudgeInsert)
                        {
                            _pupilId = itemInsert.PupilID;
                            _title = itemInsert.Title;
                            _mark = itemInsert.Judgement;
                            //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                            //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                            mrOfPupilObj = listJudgeDb.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                            if (mrOfPupilObj != null)
                            {
                                if (mrOfPupilObj.Judgement.Equals(_mark))
                                {
                                    itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                }
                                else
                                {
                                    itemInsert.ModifiedDate = DateTime.Now;
                                    itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                                }
                                itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                            }
                            else
                            {
                                itemInsert.CreatedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                            }

                            itemInsert.CreatedAcademicYear = academicYear.Year;
                        }

                        this.BulkInsert(lstJudgeInsert, JudgeColumnMappings(), "JudgeRecordID");
                        if (lstJudgeInsert.Count > 0)
                        {
                            isDBChange = true;
                        }
                    }
                }
                //Xoa tat ca du lieu trong DB
                List<long> lstJudgeIdDelete = lstJudgeRecordHistory.Where(p => p.JudgeRecordID > 0).Select(m => m.JudgeRecordID).Distinct().ToList();
                if (lstJudgeIdDelete.Count > 0)
                {
                    JudgeRecordBusiness.DeleteJudgeRecordByListID(AcademicYearID, SchoolID, 1, lstJudgeIdDelete);
                    isDBChange = true;
                }
                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                //List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listJudgeDb.Select(o => o.PupilID)).ToList();
                List<int> pupilIDs = lstJudgeRecordHistory.Select(p => p.PupilID).Distinct().ToList();
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = Period;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                //ThreadMarkBusiness.InsertActionList(info);
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion
                // Neu co hoc sinh khong thay doi diem moi phai lay lai danh sach TBM
                /*if (lstSummedUpRecord.Count != listPupilID.Count)
                {
                    lstSummedUpRecord = lstSummedUpRecord.Where(o => listPupilID.Contains(o.PupilID)).ToList();
                }*/
                if (lstSummedUpRecordHistory.Count == 0)
                    return isDBChange;
                List<SummedUpRecordHistory> listSummedUpInsert = new List<SummedUpRecordHistory>();
                #region hoc ki 1
                //Tìm kiếm bản ghi trong bảng SummedUpRecord
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Period != null)
                {
                    // Xoa du lieu cu
                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["AcademicYearID"] = AcademicYearID;
                    SearchInfo["ClassID"] = ClassID;
                    SearchInfo["SubjectID"] = SubjectID;
                    SearchInfo["PeriodID"] = Period;
                    SearchInfo["Semester"] = Semester;
                    // Khong can join de tim isactive voi HS va truong
                    SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
                    List<SummedUpRecordHistory> ListSummedUpRecordFromDB = this.SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, SearchInfo)
                        .Where(o => listPupilID.Contains(o.PupilID))
                        .ToList();

                    List<int> listPupilIDEx = listPupilID.Where(u => listExemptedHKII.Select(a => a.PupilID).Contains(u)).ToList();

                    //neu mien giam HK2 hoac khong hoc hoc ky II va nhap diem theo ky
                    if (!Period.HasValue && (classSubject.SectionPerWeekSecondSemester == 0 || (listPupilIDEx != null && listPupilIDEx.Count() > 0)))
                    {
                        // Xoa diem ca nam va lay bang diem hoc ky I
                        SearchInfo["AcademicYearID"] = AcademicYearID;
                        SearchInfo["ClassID"] = ClassID;
                        SearchInfo["SubjectID"] = SubjectID;
                        SearchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                        // Khong can join de tim isactive voi HS va truong
                        SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";

                        // Lay danh sach hoac la mien giam hoac ca lop neu mon do khong hoc o ky 2
                        var ListSummedUpRecordFromDBALL = this.SummedUpRecordBusiness.SearchBySchool(SchoolID, SearchInfo)
                            .Where(o => classSubject.SectionPerWeekSecondSemester == 0 || listPupilIDEx.Contains(o.PupilID))
                            .ToList();

                        if (ListSummedUpRecordFromDBALL != null & ListSummedUpRecordFromDBALL.Count > 0)
                        {
                            //SummedUpRecordBusiness.DeleteAll(ListSummedUpRecordFromDBALL);
                            //Xoa tat ca du lieu trong DB
                            List<int> listValSummed = ListSummedUpRecordFromDBALL.Select(p => p.PupilID).ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }
                    }

                    //Thực hiện xóa bản ghi nếu thấy trong bảng SummedUpRecord
                    if (ListSummedUpRecordFromDB != null & ListSummedUpRecordFromDB.Count > 0)
                    {
                        //SummedUpRecordBusiness.DeleteAll(ListSummedUpRecordFromDB);
                        //Xoa tat ca du lieu trong DB                    
                        List<int> listValSummed = ListSummedUpRecordFromDB.Select(p => p.PupilID).ToList();
                        MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                    }

                    List<SummedUpRecordHistory> lstSummedUpRecordTmp = lstSummedUpRecordHistory.Where(p => !p.JudgementResult.Equals(string.Empty) && p.JudgementResult != "-1").ToList();
                    foreach (SummedUpRecordHistory SummedUpRecord in lstSummedUpRecordTmp)
                    {
                        SummedUpRecord.CreatedAcademicYear = academicYear.Year;
                        listSummedUpInsert.Add(SummedUpRecord);
                        //truong hop chi o hoc ky I hoac mien giam ky 2 va dang nhap diem theo ky
                        if (!Period.HasValue && (classSubject.SectionPerWeekSecondSemester == 0 || (listPupilIDEx != null && listPupilIDEx.Any(u => u == SummedUpRecord.PupilID))))
                        {
                            SummedUpRecordHistory summedUpSemester = new SummedUpRecordHistory()
                            {
                                AcademicYearID = SummedUpRecord.AcademicYearID,
                                ClassID = SummedUpRecord.ClassID,
                                Comment = SummedUpRecord.Comment,
                                IsCommenting = SummedUpRecord.IsCommenting,
                                IsOldData = SummedUpRecord.IsOldData,
                                JudgementResult = SummedUpRecord.JudgementResult,
                                PeriodID = SummedUpRecord.PeriodID,
                                PupilID = SummedUpRecord.PupilID,
                                ReTestJudgement = SummedUpRecord.ReTestJudgement,
                                ReTestMark = SummedUpRecord.ReTestMark,
                                SchoolID = SummedUpRecord.SchoolID,
                                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL,
                                SubjectID = SummedUpRecord.SubjectID,
                                SummedUpDate = SummedUpRecord.SummedUpDate,
                                SummedUpMark = SummedUpRecord.SummedUpMark,
                                CreatedAcademicYear = academicYear.Year
                            };
                            //this.SummedUpRecordBusiness.Insert(summedUpSemester);
                            listSummedUpInsert.Add(summedUpSemester);
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordHistoryBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                }
                #endregion hoc ki 1
                #region hoc ky 2
                else
                {
                    #region insert ky II
                    //    Bước 1: với mỗi phần tử trong lstPupilExempted
                    /* Tìm kiếm trong bảng SummedUpRecord theo
                          PupilID = lstPupilExempted [i].PupilID
                          AcademicYearID= AcademicYearID
                          ClassID = ClassID
                          SubjectID = SubjectID
                          Semester = SEMESTER_OF_YEAR_ALL
                        Thực hiện xóa bản ghi nếu thấy */
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = AcademicYearID;
                    dic["ClassID"] = ClassID;
                    dic["SubjectID"] = SubjectID;
                    // Khong can join de tim isactive voi HS va truong
                    dic["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
                    // Diem TBM cua 3 HK de xu ly
                    List<SummedUpRecordHistory> lstSummedUpAll = SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, dic)
                        .Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL))
                        .ToList();
                    // Diem TBM cua HK1
                    List<SummedUpRecordHistory> lstSummedUpHKI = lstSummedUpAll.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                    if (lstPupilExempted != null && lstPupilExempted.Count > 0)
                    {
                        List<SummedUpRecordHistory> listSummedUpRecord = SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() {
                    {"AcademicYearID", AcademicYearID},
                    {"ClassID", ClassID},
                    {"SubjectID", SubjectID},
                    {"NoCheckIsActivePupilSchool", "NoCheckIsActivePupilSchool"},
                    {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_ALL},
                    }).Where(o => lstPupilExempted.Contains(o.PupilID)).ToList();
                        if (listSummedUpRecord.Count > 0)
                        {
                            //SummedUpRecordBusiness.DeleteAll(listSummedUpRecord);
                            List<int> listValSummed = listSummedUpRecord.Select(p => p.PupilID).ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        /*- Kiểm tra học kỳ 1 có Miễn giảm hay không: Gọi hàm
                         * ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                            + Nếu kết quả trả về True => return
                            + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                            + False: thì return
                            + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách gọi hàm
                         * SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST) kết quả trả về nếu:
                            + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                            + JudgeResult != null: Điểm TBM cả năm = Điểm TBM học kỳ 1 (JudgeResult3 = JudgeResult)
                         * Thực hiện insert vào bảng SummedUpRecord
                         */

                        foreach (var pupilid in lstPupilExempted)
                        {
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == pupilid);
                            if (!IsExemptedSubject && subjectLearnInHKI) //Duoc miem giam hoc ky 2, co hoc o HK I
                            {
                                SummedUpRecordHistory surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == pupilid);
                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                    surIII.AcademicYearID = surI.AcademicYearID;
                                    surIII.ClassID = surI.ClassID;
                                    surIII.Comment = surI.Comment;
                                    surIII.IsCommenting = surI.IsCommenting;
                                    //surIII.IsLegalBot = surI.IsLegalBot;
                                    surIII.JudgementResult = surI.JudgementResult;
                                    surIII.PeriodID = surI.PeriodID;
                                    surIII.PupilID = surI.PupilID;
                                    surIII.ReTestJudgement = surI.ReTestJudgement;
                                    surIII.ReTestMark = surI.ReTestMark;
                                    surIII.SchoolID = surI.SchoolID;
                                    surIII.SubjectID = surI.SubjectID;
                                    surIII.SummedUpDate = surI.SummedUpDate;
                                    surIII.SummedUpMark = surI.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }
                    /*
                        Bước 2:
                        // Lưu điểm kỳ 2
                        Tìm kiếm trong bảng SummedUpRecord theo
                        -	PupilID = lstSummedUpRecord[i].PupilID
                        -	AcademicYearID= lstSummedUpRecord[i].AcademicYearID
                        -	ClassID = lstSummedUpRecord[i].ClassID
                        -	SubjectID = lstSummedUpRecord[i].SubjectID
                        -	Semester = lstSummedUpRecord[i].Semester
                        Thực hiện xóa bản ghi nếu thấy
                        Thực hiện insert lstSummedUpRecord vào SummedUpRecord
                     */
                    if (lstSummedUpRecordHistory != null && lstSummedUpRecordHistory.Count > 0)
                    {
                        // Xoa du lieu cu HK2 va ca nam
                        lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && listPupilID.Contains(o.PupilID)).ToList();
                        if (lstSummedUpAll.Count > 0)
                        {
                            //SummedUpRecordBusiness.DeleteAll(lstSummedUpAll);
                            //SummedUpRecordBusiness.Save();
                            List<int> listValSummed = lstSummedUpAll.Select(p => p.PupilID).ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        List<SummedUpRecordHistory> lstSummedUpRecordTmp = lstSummedUpRecordHistory.Where(p => !p.JudgementResult.Equals(string.Empty) && p.JudgementResult != "-1").ToList();
                        foreach (var summedUpRecord in lstSummedUpRecordTmp)
                        {
                            summedUpRecord.CreatedAcademicYear = academicYear.Year;
                            listSummedUpInsert.Add(summedUpRecord);
                        }
                        // SummedUpRecordBusiness.Save();

                        /*
                         // tính cả năm
                        - Kiểm tra học kỳ 1 có Miễn giảm hay không:
                         * Gọi hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                        + Nếu kết quả trả về True => Điểm TBM cả năm = TBM học kỳ 2
                        + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                        + False: thì return Điểm TBM cả năm = TBM học kỳ 2
                        + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách
                         * gọi hàm SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST)
                         * kết quả trả về nếu:
                        + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                        + JudgeResult != null:
                         * Nếu ((sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “Đ”)
                         * or (sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “Đ”)) thì JudgementResult3 = “Đ” – cả năm.
                         Nếu ((sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “CĐ”)
                         * or (sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “CĐ”)) thì JudgementResult3 = “CĐ” – cả năm.
                         */

                        foreach (var summedUpRecord in lstSummedUpRecordHistory)
                        {
                            // lay diem != -1
                            if (summedUpRecord.JudgementResult.Equals("-1"))
                            {
                                continue;
                            }
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == summedUpRecord.PupilID);
                            if (IsExemptedSubject || !subjectLearnInHKI)
                            {
                                SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                surIII.ClassID = summedUpRecord.ClassID;
                                surIII.Comment = summedUpRecord.Comment;
                                surIII.IsCommenting = summedUpRecord.IsCommenting;
                                //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                surIII.JudgementResult = summedUpRecord.JudgementResult;
                                surIII.PeriodID = summedUpRecord.PeriodID;
                                surIII.PupilID = summedUpRecord.PupilID;
                                surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                surIII.ReTestMark = summedUpRecord.ReTestMark;
                                surIII.SchoolID = summedUpRecord.SchoolID;
                                surIII.SubjectID = summedUpRecord.SubjectID;
                                surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                surIII.CreatedAcademicYear = academicYear.Year;
                                surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                //SummedUpRecordBusiness.Insert(surIII);
                                listSummedUpInsert.Add(surIII);
                            }
                            else
                            {
                                SummedUpRecordHistory surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == summedUpRecord.PupilID);

                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                    surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                    surIII.ClassID = summedUpRecord.ClassID;
                                    surIII.Comment = summedUpRecord.Comment;
                                    surIII.IsCommenting = summedUpRecord.IsCommenting;
                                    //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                    surIII.JudgementResult = summedUpRecord.JudgementResult;
                                    surIII.PeriodID = summedUpRecord.PeriodID;
                                    surIII.PupilID = summedUpRecord.PupilID;
                                    surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                    surIII.ReTestMark = summedUpRecord.ReTestMark;
                                    surIII.SchoolID = summedUpRecord.SchoolID;
                                    surIII.SubjectID = summedUpRecord.SubjectID;
                                    surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                    surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;

                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }

                        if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                        {
                            SummedUpRecordHistoryBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                        }
                    }
                    #endregion insert ky II
                }
                #endregion hoc ky 2
                //Tinh TBM cho dot hoac ky
                this.Save();
                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, Semester, Period, classSubject);

                return isDBChange;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public void InsertJudgeRecordHistoryByListSubject(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, List<int> listSubjectID, int? EmployeeID)
        {
            if (lstJudgeRecordHistory == null || lstJudgeRecordHistory.Count == 0) return;

            if (listSubjectID == null || listSubjectID.Count == 0)
            {
                return;
            }
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");

            int SubjectID = 0;

            var listPupilIDAll = lstJudgeRecordHistory.Select(u => u.PupilID).Distinct().ToList();
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SchoolID", SchoolID } }).ToList();
            var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           where poc.ClassID == ClassID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                           && listPupilIDAll.Contains(poc.PupilID) && pp.IsActive
                           select new { poc.Status, poc.AcademicYearID, poc.ClassID }
                                    ).ToList();
            //Thong tin mien giam cua truong
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID).ToList();

            Dictionary<int, string> dicLockRecord = MarkRecordBusiness.GetLockMarkTitleBySubject(SchoolID, AcademicYearID, ClassID, Semester, 0);

            IDictionary<string, object> SearchDelete = new Dictionary<string, object>();
            SearchDelete["AcademicYearID"] = AcademicYearID;
            SearchDelete["SchoolID"] = SchoolID;
            SearchDelete["ClassID"] = ClassID;
            //SearchDelete["SubjectID"] = SubjectID;
            SearchDelete["Semester"] = Semester;
            SearchDelete["checkWithClassMovement"] = "1";
            List<JudgeRecordHistory> iqJudgeDbAllSubject = this.SearchJudgeRecordHistory(SearchDelete).Where(u => listPupilIDAll.Contains(u.PupilID)).ToList();

            // Tim kiem danh sach them, sua, xoa
            //List<JudgeRecord> listInsert = new List<JudgeRecord>();
            //List<JudgeRecord> listUpdate = new List<JudgeRecord>();

            // Xoa du lieu cu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            //SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["PeriodID"] = Period;
            //SearchInfo["Semester"] = Semester;
            // Khong can join de tim isactive voi HS va truong
            SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            List<SummedUpRecordHistory> ListSummedUpRecordFromDBAllSubject = this.SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, SearchInfo)
                .Where(o => listPupilIDAll.Contains(o.PupilID))
                .ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["ClassID"] = ClassID;
            dic["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            dic["PeriodID"] = Period;
            // Diem TBM cua 3 HK de xu ly
            List<SummedUpRecordHistory> lstSummedUpAllSubject = SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, dic)
                .Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                    || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL))
                .ToList();


            for (int i = 0; i < listSubjectID.Count; i++)
            {
                SubjectID = listSubjectID[i];
                var listPupilID = lstJudgeRecordHistory.Where(p => p.SubjectID == SubjectID).Select(u => u.PupilID).Distinct().ToList();
                // Kiem tra quyen giao vien bo mon
                if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID, Semester))
                    throw new BusinessException("JudgeRecord_Label_NotAuthen_Add");


                //Academic not compatible
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                if (Period != null)
                {
                    bool PeriodDeclarationCompatible = new PeriodDeclarationRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                            new Dictionary<string, object>()
                            {
                                    {"PeriodDeclarationID",Period},
                                    {"Semester",Semester}
                            }, null);
                    if (!PeriodDeclarationCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                ClassSubject classSubject = listClassSubject.Where(p => p.SubjectID == SubjectID).FirstOrDefault();
                ClassProfile classProfile = classSubject.ClassProfile;

                // Kiem tra trang thai hoc sinh dang hoc
                if (listPOC.Count != listPupilID.Count)
                {
                    throw new BusinessException("Import_Validate_StudyingStatus");
                }

                // Kiem tra nam hoc va lop hoc hien tai
                if (listPOC.Any(o => o.AcademicYearID != AcademicYearID || o.ClassID != ClassID))
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                #region Lay thong tin mien giam

                List<ExemptedSubject> listExemptedByClass = listExemptedSubject.Where(u => u.SubjectID == SubjectID).ToList();
                // HKT
                List<ExemptedSubject> listExemptedHKI = listExemptedByClass.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                //HK2
                List<ExemptedSubject> listExemptedHKII = listExemptedByClass.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<ExemptedSubject> listExempted = new List<ExemptedSubject>();
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    listExempted.AddRange(listExemptedHKI);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listExempted.AddRange(listExemptedHKII);
                }
                #endregion

                #region validate 2
                if (classSubject == null)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = ISCOMMENTING_TYPE_JUDGE
                //(nếu kết quả trả về != null => là môn nhận xét)
                if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    throw new BusinessException("SummedUpRecord_Labal_ErrNXSubject");

                bool subjectLearnInHKI = classSubject.SectionPerWeekFirstSemester > 0;
                bool subjectLearnInHKII = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester > 0;

                foreach (var JudgeRecord in lstJudgeRecordHistory)
                {
                    //if (JudgeRecord.Judgement != "-1")
                    //{
                    //    ValidationMetadata.ValidateObject(JudgeRecord);
                    //}
                    //Kiem tra diem thi hop le
                    if (JudgeRecord.Judgement != "-1")
                    {
                        UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, GlobalConstants.ISCOMMENTING_TYPE_JUDGE, JudgeRecord.Judgement);
                    }

                    //Không cho phép nhập điểm (Đối với môn miễn giảm)
                    // if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    //     throw new BusinessException("ExemptedSubject_Label_IsExemptedSubject");
                }
                #endregion

                string strMarkTitle = Period.HasValue ? MarkRecordBusiness.GetMarkTitle(Period.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<string> lstTitle = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                List<int> lstPupilIDExemted = listExempted.Select(p => p.PupilID).ToList();

                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                string lstLockedMarkDetail = dicLockRecord.Where(p => p.Key == SubjectID).Select(p => p.Value).FirstOrDefault();
                //MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                List<string> ArrLockedMark = (lstLockedMarkDetail != null && lstLockedMarkDetail.Count() > 0) ? lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList() : new List<string>();
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                //neu khong phai tai khoan quan tri truong va tat ca con diem bi khoa thi bao loi
                if (ArrLockedMark.Contains("LHK") && !isSchoolAdmin)
                {
                    throw new BusinessException("MarkRecord_Validate_MarkLock");
                }
                List<JudgeRecordHistory> lsttmp = new List<JudgeRecordHistory>();
                // Neu khong phai la tk quan tri truong thi bo qua cac con diem bi khoa
                if (!isSchoolAdmin)
                {
                    lsttmp = lstJudgeRecordHistory.Where(o => !ArrLockedMark.Contains(o.Title) && o.SubjectID == SubjectID).ToList();
                    for (int j = 0; j < ArrLockedMark.Count; j++)
                    {
                        if (strtmp.Contains(ArrLockedMark[j]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[j], "");
                        }
                    }
                }
                else
                {
                    lsttmp = lstJudgeRecordHistory.Where(o => o.SubjectID == SubjectID).ToList();
                }

                List<JudgeRecordHistory> iqJudgeDb = iqJudgeDbAllSubject.Where(p => p.SubjectID == SubjectID).ToList();
                // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                if (!isSchoolAdmin)
                {
                    iqJudgeDb = iqJudgeDb.Where(v => !ArrLockedMark.Contains(v.Title)).ToList();
                }
                if (Period.HasValue)
                {
                    iqJudgeDb = iqJudgeDb.Where(u => lstTitle.Contains(u.Title)).ToList();
                }

                ///Danh sach diem nhan xet theo mon hoc
                List<JudgeRecordHistory> listJudgeDb = iqJudgeDb;
                List<int> lstPupilIDtmp = listPupilID.Where(p => !(lstPupilIDExemted.Contains(p))).ToList();

                //Insert du lieu moi vao DB
                JudgeRecordHistory judgePupilIdObj = null;
                if (lsttmp != null && lsttmp.Count > 0)
                {

                    List<JudgeRecordHistory> lstJudgeInsert = lsttmp.Where(p => !p.Judgement.Equals(string.Empty) && !"-1".Equals(p.Judgement) && lstPupilIDtmp.Contains(p.PupilID)).ToList();

                    int _pupilId = 0;
                    string _title = string.Empty;
                    string _mark = string.Empty;
                    foreach (JudgeRecordHistory itemInsert in lstJudgeInsert)
                    {
                        _pupilId = itemInsert.PupilID;
                        _title = itemInsert.Title;
                        _mark = itemInsert.Judgement;
                        //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                        //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                        judgePupilIdObj = listJudgeDb.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                        if (judgePupilIdObj != null)
                        {
                            if (judgePupilIdObj.Judgement.Equals(_mark))
                            {
                                itemInsert.ModifiedDate = judgePupilIdObj.ModifiedDate;
                            }
                            else
                            {
                                itemInsert.ModifiedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                            }
                            itemInsert.CreatedDate = judgePupilIdObj.CreatedDate;
                        }
                        else
                        {
                            itemInsert.CreatedDate = DateTime.Now;
                            itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                        }

                        itemInsert.CreatedAcademicYear = academicYear.Year;
                    }

                    if (lstJudgeInsert != null && lstJudgeInsert.Count > 0)
                    {
                        this.BulkInsert(lstJudgeInsert, JudgeColumnMappings(), "JudgeRecordID");
                    }
                }

                //Xoa tat ca du lieu trong DB
                List<long> lstJudgeIdDelete = lsttmp.Where(p => p.JudgeRecordID > 0).Select(m => m.JudgeRecordID).Distinct().ToList();
                if (lstJudgeIdDelete.Count > 0)
                {
                    JudgeRecordBusiness.DeleteJudgeRecordByListID(AcademicYearID, SchoolID, 1, lstJudgeIdDelete);
                }

                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                //List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listJudgeDb.Select(o => o.PupilID)).ToList();
                List<int> pupilIDs = lstJudgeRecordHistory.Select(p => p.PupilID).Distinct().ToList();
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = Period;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion
                if (lstSummedUpRecordHistory.Count == 0)
                    return;

                List<SummedUpRecordHistory> listSummedUpInsert = new List<SummedUpRecordHistory>();
                #region hoc ki 1
                //Tìm kiếm bản ghi trong bảng SummedUpRecord
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    List<SummedUpRecordHistory> ListSummedUpRecordFromDB = ListSummedUpRecordFromDBAllSubject.Where(o => lstPupilIDtmp.Contains(o.PupilID) && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).ToList();

                    //Thực hiện xóa bản ghi nếu thấy trong bảng SummedUpRecord
                    if (ListSummedUpRecordFromDB != null & ListSummedUpRecordFromDB.Count > 0)
                    {
                        List<int> listValSummed = ListSummedUpRecordFromDB.Select(p => p.PupilID).ToList();
                        MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listValSummed, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                    }

                    foreach (SummedUpRecordHistory SummedUpRecord in lstSummedUpRecordHistory)
                    {

                        // Lay diem != - 1 de luu vao db
                        if (SummedUpRecord.JudgementResult != "-1")
                        {
                            SummedUpRecord.CreatedAcademicYear = academicYear.Year;
                        }
                    }

                    listSummedUpInsert = lstSummedUpRecordHistory.Where(p => lstPupilIDtmp.Contains(p.PupilID) && p.JudgementResult != "-1").ToList();
                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordHistoryBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                }
                #endregion hoc ki 1
                #region hoc ky 2
                else
                {
                    #region insert ky II

                    //    Bước 1: với mỗi phần tử trong lstPupilExempted
                    /* Tìm kiếm trong bảng SummedUpRecord theo
                          PupilID = lstPupilExempted [i].PupilID
                          AcademicYearID= AcademicYearID
                          ClassID = ClassID
                          SubjectID = SubjectID
                          Semester = SEMESTER_OF_YEAR_ALL
                        Thực hiện xóa bản ghi nếu thấy */

                    List<SummedUpRecordHistory> lstSummedUpAll = lstSummedUpAllSubject.Where(p => p.SubjectID == SubjectID).ToList();
                    // Diem TBM cua HK1
                    List<SummedUpRecordHistory> lstSummedUpHKI = lstSummedUpAll.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();

                    if (lstPupilExempted != null && lstPupilExempted.Count > 0)
                    {
                        List<SummedUpRecordHistory> listSummedUpRecord = ListSummedUpRecordFromDBAllSubject.Where(o => lstPupilExempted.Contains(o.PupilID)
                            && o.SubjectID == SubjectID
                            && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();

                        if (listSummedUpRecord.Count > 0)
                        {
                            // SummedUpRecordBusiness.DeleteAll(listSummedUpRecord);
                            List<int> lstVal = listSummedUpRecord.Select(c => c.PupilID).Distinct().ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(1, lstVal, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        /*- Kiểm tra học kỳ 1 có Miễn giảm hay không: Gọi hàm
                         * ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                            + Nếu kết quả trả về True => return
                            + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                            + False: thì return
                            + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách gọi hàm
                         * SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST) kết quả trả về nếu:
                            + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                            + JudgeResult != null: Điểm TBM cả năm = Điểm TBM học kỳ 1 (JudgeResult3 = JudgeResult)
                         * Thực hiện insert vào bảng SummedUpRecord
                         */

                        foreach (var pupilid in lstPupilExempted)
                        {
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == pupilid);
                            if (!IsExemptedSubject && subjectLearnInHKI) //Duoc miem giam hoc ky 2, co hoc o HK I
                            {
                                SummedUpRecordHistory surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == pupilid);
                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                    surIII.AcademicYearID = surI.AcademicYearID;
                                    surIII.ClassID = surI.ClassID;
                                    surIII.Comment = surI.Comment;
                                    surIII.IsCommenting = surI.IsCommenting;
                                    //surIII.IsLegalBot = surI.IsLegalBot;
                                    surIII.JudgementResult = surI.JudgementResult;
                                    surIII.PeriodID = surI.PeriodID;
                                    surIII.PupilID = surI.PupilID;
                                    surIII.ReTestJudgement = surI.ReTestJudgement;
                                    surIII.ReTestMark = surI.ReTestMark;
                                    surIII.SchoolID = surI.SchoolID;
                                    surIII.SubjectID = surI.SubjectID;
                                    surIII.SummedUpDate = surI.SummedUpDate;
                                    surIII.SummedUpMark = surI.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }
                    /*
                        Bước 2:
                        // Lưu điểm kỳ 2
                        Tìm kiếm trong bảng SummedUpRecord theo
                        -	PupilID = lstSummedUpRecord[i].PupilID
                        -	AcademicYearID= lstSummedUpRecord[i].AcademicYearID
                        -	ClassID = lstSummedUpRecord[i].ClassID
                        -	SubjectID = lstSummedUpRecord[i].SubjectID
                        -	Semester = lstSummedUpRecord[i].Semester
                        Thực hiện xóa bản ghi nếu thấy
                        Thực hiện insert lstSummedUpRecord vào SummedUpRecord
                     */
                    if (lstSummedUpRecordHistory != null && lstSummedUpRecordHistory.Count > 0)
                    {
                        // Xoa du lieu cu HK2 va ca nam
                        if (Period > 0)
                        {
                            lstSummedUpAll = lstSummedUpAll.Where(o => o.PeriodID == Period && lstPupilIDtmp.Contains(o.PupilID)).ToList();
                        }
                        else
                        {
                            lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && lstPupilIDtmp.Contains(o.PupilID)).ToList();
                        }
                        if (lstSummedUpAll != null && lstSummedUpAll.Count > 0)
                        {
                            List<int> lstVal = lstSummedUpAll.Select(c => c.PupilID).Distinct().ToList();
                            MarkRecordBusiness.SP_DeleteSummedUpRecord(1, lstVal, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        foreach (SummedUpRecordHistory summedUpRecord in lstSummedUpRecordHistory)
                        {
                            if (!String.IsNullOrEmpty(summedUpRecord.JudgementResult) && summedUpRecord.JudgementResult != "-1")
                            {
                                summedUpRecord.CreatedAcademicYear = academicYear.Year;
                                //SummedUpRecordBusiness.Insert(summedUpRecord);
                            }
                        }
                        List<SummedUpRecordHistory> listSummedUpInsertTmp = lstSummedUpRecordHistory.Where(p => lstPupilIDtmp.Contains(p.PupilID) && p.JudgementResult != "-1" && p.JudgementResult != "").ToList();
                        for (int k = 0; k < listSummedUpInsertTmp.Count; k++)
                        {
                            listSummedUpInsert.Add(listSummedUpInsertTmp[k]);
                        }

                        //SummedUpRecordBusiness.Save();
                        //var detachContext1 = ((IObjectContextAdapter)context).ObjectContext;
                        //detachContext1.SaveChanges(SaveOptions.DetectChangesBeforeSave);

                        /*
                         // tính cả năm
                        - Kiểm tra học kỳ 1 có Miễn giảm hay không:
                         * Gọi hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                        + Nếu kết quả trả về True => Điểm TBM cả năm = TBM học kỳ 2
                        + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                        + False: thì return Điểm TBM cả năm = TBM học kỳ 2
                        + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách
                         * gọi hàm SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST)
                         * kết quả trả về nếu:
                        + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                        + JudgeResult != null:
                         * Nếu ((sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “Đ”)
                         * or (sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “Đ”)) thì JudgementResult3 = “Đ” – cả năm.
                         Nếu ((sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “CĐ”)
                         * or (sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “CĐ”)) thì JudgementResult3 = “CĐ” – cả năm.
                         */
                        foreach (var summedUpRecord in lstSummedUpRecordHistory)
                        {
                            // lay diem != -1
                            if (summedUpRecord.JudgementResult.Equals("-1"))
                            {
                                continue;
                            }
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == summedUpRecord.PupilID);
                            if (IsExemptedSubject || !subjectLearnInHKI)
                            {
                                SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                surIII.ClassID = summedUpRecord.ClassID;
                                surIII.Comment = summedUpRecord.Comment;
                                surIII.IsCommenting = summedUpRecord.IsCommenting;
                                //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                surIII.JudgementResult = summedUpRecord.JudgementResult;
                                surIII.PeriodID = summedUpRecord.PeriodID;
                                surIII.PupilID = summedUpRecord.PupilID;
                                surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                surIII.ReTestMark = summedUpRecord.ReTestMark;
                                surIII.SchoolID = summedUpRecord.SchoolID;
                                surIII.SubjectID = summedUpRecord.SubjectID;
                                surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                surIII.CreatedAcademicYear = academicYear.Year;
                                surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                //SummedUpRecordBusiness.Insert(surIII);
                                listSummedUpInsert.Add(surIII);
                            }
                            else
                            {
                                SummedUpRecordHistory surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == summedUpRecord.PupilID);

                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                    surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                    surIII.ClassID = summedUpRecord.ClassID;
                                    surIII.Comment = summedUpRecord.Comment;
                                    surIII.IsCommenting = summedUpRecord.IsCommenting;
                                    //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                    surIII.JudgementResult = summedUpRecord.JudgementResult;
                                    surIII.PeriodID = summedUpRecord.PeriodID;
                                    surIII.PupilID = summedUpRecord.PupilID;
                                    surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                    surIII.ReTestMark = summedUpRecord.ReTestMark;
                                    surIII.SchoolID = summedUpRecord.SchoolID;
                                    surIII.SubjectID = summedUpRecord.SubjectID;
                                    surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                    surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordHistoryBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                    #endregion insert ky II
                }
                #endregion hoc ky 2

                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, Semester, Period, classSubject);

            }
        }

        public void InsertJudgeRecordHistoryByListClass(int UserID, List<JudgeRecordHistory> lstJudgeRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, List<int> listClassID, int SubjectID, int? EmployeeID)
        {

            if (lstJudgeRecord == null || lstJudgeRecord.Count == 0) return;
            if (listClassID == null || listClassID.Count == 0)
            {
                return;
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");


            int ClassID = 0;
            var listPupilIDAll = lstJudgeRecord.Select(u => u.PupilID).Distinct().ToList();
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).ToList();
            var listPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                    join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                    where poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                    && listPupilIDAll.Contains(poc.PupilID) && pp.IsActive
                                    select new { poc.Status, poc.AcademicYearID, poc.ClassID }
                                    ).ToList();

            List<ExemptedSubject> listExemptedBySchoolID = ExemptedSubjectBusiness.GetListExemptedSubjectBySchoolID(SchoolID, AcademicYearID, SubjectID).ToList();
            Dictionary<int, string> dicLockRecord = MarkRecordBusiness.GetLockMarkTitleBySchoolID(SchoolID, AcademicYearID, Semester, 0, SubjectID);

            IDictionary<string, object> SearchDelete = new Dictionary<string, object>();
            SearchDelete["AcademicYearID"] = AcademicYearID;
            SearchDelete["SchoolID"] = SchoolID;
            //SearchDelete["ClassID"] = ClassID;
            SearchDelete["SubjectID"] = SubjectID;
            SearchDelete["Semester"] = Semester;
            SearchDelete["checkWithClassMovement"] = "1";
            List<JudgeRecordHistory> iqJudgeDbAllClassID = this.SearchJudgeRecordHistory(SearchDelete).Where(u => listPupilIDAll.Contains(u.PupilID)).ToList();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            //SearchInfo["ClassID"] = ClassID;
            SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["PeriodID"] = Period;
            //SearchInfo["Semester"] = Semester;
            // Khong can join de tim isactive voi HS va truong
            SearchInfo["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            List<SummedUpRecordHistory> ListSummedUpRecordFromDBAllClass = this.SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            //dic["ClassID"] = ClassID;
            dic["SubjectID"] = SubjectID;
            // Khong can join de tim isactive voi HS va truong
            dic["NoCheckIsActivePupilSchool"] = "NoCheckIsActivePupilSchool";
            // Diem TBM cua 3 HK de xu ly
            List<SummedUpRecordHistory> lstSummedUpAllClass = SummedUpRecordHistoryBusiness.SearchBySchool(SchoolID, dic).ToList();


            for (int k = 0; k < listClassID.Count; k++)
            {
                ClassID = listClassID[k];
                var listPupilID = lstJudgeRecord.Where(p => p.ClassID == ClassID).Select(u => u.PupilID).Distinct().ToList();
                // Kiem tra quyen giao vien bo mon
                if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID, Semester))
                    throw new BusinessException("JudgeRecord_Label_NotAuthen_Add");
                //Academic not compatible
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //PeriodID(PeriodDeclarationID), Semester: not compatible(PeriodDeclaration)
                if (Period != null)
                {
                    bool PeriodDeclarationCompatible = new PeriodDeclarationRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                            new Dictionary<string, object>()
                            {
                                    {"PeriodDeclarationID",Period},
                                    {"Semester",Semester}
                            }, null);
                    if (!PeriodDeclarationCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }

                ClassSubject classSubject = listClassSubject.Where(p => p.ClassID == ClassID).FirstOrDefault();
                ClassProfile classProfile = classSubject.ClassProfile;
                var listPOC = listPupilOfClass.Where(p => p.ClassID == ClassID).ToList();

                // Kiem tra trang thai hoc sinh dang hoc
                if (listPOC.Count != listPupilID.Count)
                {
                    throw new BusinessException("Import_Validate_StudyingStatus");
                }

                // Kiem tra nam hoc va lop hoc hien tai
                if (listPOC.Any(o => o.AcademicYearID != AcademicYearID || o.ClassID != ClassID))
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                #region Lay thong tin mien giam
                List<ExemptedSubject> listExemptedByClass = listExemptedBySchoolID.Where(p => p.ClassID == ClassID).ToList();

                // HKT
                List<ExemptedSubject> listExemptedHKI = listExemptedByClass.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
                //HK2
                List<ExemptedSubject> listExemptedHKII = listExemptedByClass.Where(o => o.SecondSemesterExemptType.HasValue).ToList();

                List<ExemptedSubject> listExempted = new List<ExemptedSubject>();
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    listExempted.AddRange(listExemptedHKI);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    listExempted.AddRange(listExemptedHKII);
                }
                #endregion

                #region validate 2
                if (classSubject == null)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = ISCOMMENTING_TYPE_JUDGE
                //(nếu kết quả trả về != null => là môn nhận xét)
                if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    throw new BusinessException("SummedUpRecord_Labal_ErrNXSubject");

                bool subjectLearnInHKI = classSubject.SectionPerWeekFirstSemester > 0;
                bool subjectLearnInHKII = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && classSubject.SectionPerWeekSecondSemester > 0;

                foreach (var JudgeRecord in lstJudgeRecord)
                {
                    //if (JudgeRecord.Judgement != "-1")
                    //{
                    //    ValidationMetadata.ValidateObject(JudgeRecord);
                    //}
                    //Kiem tra diem thi hop le
                    if (JudgeRecord.Judgement != "-1")
                    {
                        UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, GlobalConstants.ISCOMMENTING_TYPE_JUDGE, JudgeRecord.Judgement);
                    }

                    //Không cho phép nhập điểm (Đối với môn miễn giảm)
                    //if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    //     throw new BusinessException("ExemptedSubject_Label_IsExemptedSubject");
                }
                #endregion

                string strMarkTitle = Period.HasValue ? MarkRecordBusiness.GetMarkTitle(Period.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<string> lstTitle = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();

                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                string lstLockedMarkDetail = dicLockRecord.Where(p => p.Key == SubjectID).Select(p => p.Value).FirstOrDefault();
                List<string> ArrLockedMark = !String.IsNullOrEmpty(lstLockedMarkDetail) ? lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList() : new List<string>();
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                //neu khong phai tai khoan quan tri truong va tat ca con diem bi khoa thi bao loi
                if (!String.IsNullOrEmpty(lstLockedMarkDetail) && lstLockedMarkDetail.Contains("LHK") && !isSchoolAdmin)
                {
                    throw new BusinessException("MarkRecord_Validate_MarkLock");
                }
                // Neu khong phai la tk quan tri truong thi bo qua cac con diem bi khoa
                List<JudgeRecordHistory> lsttmp = new List<JudgeRecordHistory>();
                if (!isSchoolAdmin)
                {
                    lsttmp = lstJudgeRecord.Where(o => !ArrLockedMark.Contains(o.Title) && o.ClassID == ClassID).ToList();
                    for (int i = 0; i < ArrLockedMark.Count; i++)
                    {
                        if (strtmp.Contains(ArrLockedMark[i]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[i], "");
                        }
                    }
                }
                else
                {
                    lsttmp = lstJudgeRecord.Where(o => o.ClassID == ClassID).ToList();
                }

                ///Danh sach diem nhan xet theo mon hoc
                List<JudgeRecordHistory> listJudgeDbByClassID = iqJudgeDbAllClassID.Where(p => p.ClassID == ClassID).ToList();
                // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                if (!isSchoolAdmin)
                {
                    listJudgeDbByClassID = listJudgeDbByClassID.Where(v => !ArrLockedMark.Contains(v.Title)).ToList();
                }
                if (Period.HasValue)
                {
                    listJudgeDbByClassID = listJudgeDbByClassID.Where(u => lstTitle.Contains(u.Title)).ToList();
                }

                //Xoa tat ca du lieu trong DB
                List<int> lstPupilExemtedID = listExempted.Select(p => p.PupilID).Distinct().ToList();
                List<int> lstPupilIDtmp = listPupilID.Where(p => !lstPupilExemtedID.Contains(p)).Distinct().ToList();
                //Insert du lieu moi vao DB
                if (lsttmp != null && lsttmp.Count > 0)
                {
                    List<JudgeRecordHistory> lstJudgeInsert = lsttmp.Where(p => lstPupilIDtmp.Contains(p.PupilID) && !"-1".Equals(p.Judgement) && !p.Judgement.Equals(string.Empty)).ToList();
                    JudgeRecordHistory judgePupilIdObj = null;
                    int _pupilId = 0;
                    string _title = string.Empty;
                    string _mark = string.Empty;
                    foreach (JudgeRecordHistory itemInsert in lstJudgeInsert)
                    {
                        _pupilId = itemInsert.PupilID;
                        _title = itemInsert.Title;
                        _mark = itemInsert.Judgement;
                        //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                        //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                        judgePupilIdObj = listJudgeDbByClassID.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                        if (judgePupilIdObj != null)
                        {
                            if (judgePupilIdObj.Judgement.Equals(_mark))
                            {
                                itemInsert.ModifiedDate = judgePupilIdObj.ModifiedDate;
                            }
                            else
                            {
                                itemInsert.ModifiedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                            }
                            itemInsert.CreatedDate = judgePupilIdObj.CreatedDate;
                        }
                        else
                        {
                            itemInsert.CreatedDate = DateTime.Now;
                            itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                        }
                        itemInsert.CreatedAcademicYear = academicYear.Year;
                    }

                    if (lstJudgeInsert != null && lstJudgeInsert.Count > 0)
                    {
                        this.BulkInsert(lstJudgeInsert, JudgeColumnMappings(), "JudgeRecordID");
                    }
                }

                //Xoa tat ca du lieu trong DB
                List<long> lstJudgeIdDelete = lsttmp.Where(p => p.JudgeRecordID > 0).Select(m => m.JudgeRecordID).Distinct().ToList();
                if (lstJudgeIdDelete.Count > 0)
                {
                    JudgeRecordBusiness.DeleteJudgeRecordByListID(AcademicYearID, SchoolID, 1, lstJudgeIdDelete);
                }

                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                List<int> pupilIDs = lstPupilIDtmp;
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = Period;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = null;
                //ThreadMarkBusiness.InsertActionList(info);
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion

                if (lstSummedUpRecord.Count == 0)
                    return;
                List<SummedUpRecordHistory> listSummedUpInsert = new List<SummedUpRecordHistory>();
                List<SummedUpRecordHistory> lstSUR = new List<SummedUpRecordHistory>();
                lstSUR = lstSummedUpRecord.Where(p => lstPupilIDtmp.Contains(p.PupilID)).ToList();
                #region hoc ki 1
                //Tìm kiếm bản ghi trong bảng SummedUpRecord
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Period != null)
                {

                    List<SummedUpRecordHistory> ListSummedUpRecordFromDB = ListSummedUpRecordFromDBAllClass
                        .Where(o => lstPupilIDtmp.Contains(o.PupilID))
                            .ToList();

                    //Thực hiện xóa bản ghi nếu thấy trong bảng SummedUpRecord
                    if (ListSummedUpRecordFromDB != null & ListSummedUpRecordFromDB.Count > 0)
                    {
                        // SummedUpRecordBusiness.DeleteAll(ListSummedUpRecordFromDB);
                        List<int> lstVal = ListSummedUpRecordFromDB.Select(c => c.PupilID).Distinct().ToList();
                        MarkRecordBusiness.SP_DeleteSummedUpRecord(1, lstVal, Period.HasValue ? Period.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                    }
                    foreach (SummedUpRecordHistory SummedUpRecord in lstSUR)
                    {

                        // Lay diem != - 1 de luu vao db
                        if (SummedUpRecord.JudgementResult != "-1")
                        {
                            SummedUpRecord.CreatedAcademicYear = academicYear.Year;
                            listSummedUpInsert.Add(SummedUpRecord);
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordHistoryBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }
                }
                #endregion hoc ki 1
                #region hoc ky 2
                else
                {
                    #region insert ky II
                    //    Bước 1: với mỗi phần tử trong lstPupilExempted
                    /* Tìm kiếm trong bảng SummedUpRecord theo
                          PupilID = lstPupilExempted [i].PupilID
                          AcademicYearID= AcademicYearID
                          ClassID = ClassID
                          SubjectID = SubjectID
                          Semester = SEMESTER_OF_YEAR_ALL
                        Thực hiện xóa bản ghi nếu thấy */
                    // Diem TBM cua 3 HK de xu ly
                    List<SummedUpRecordHistory> lstSummedUpAll = lstSummedUpAllClass
                        .Where(o => ((o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && o.ClassID == ClassID)).ToList();

                    // Diem TBM cua HK1
                    List<SummedUpRecordHistory> lstSummedUpHKI = lstSummedUpAll.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                    if (lstPupilExempted != null && lstPupilExempted.Count > 0)
                    {
                        List<SummedUpRecordHistory> listSummedUpRecord = lstSummedUpAllClass.Where(p => p.ClassID == ClassID && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .Where(o => lstPupilExempted.Contains(o.PupilID)).ToList();

                        if (listSummedUpRecord.Count > 0)
                        {
                            // SummedUpRecordBusiness.DeleteAll(listSummedUpRecord);
                            List<long> lstVal = listSummedUpRecord.Select(c => c.SummedUpRecordID).Distinct().ToList();
                            string strValDelete = string.Join(",", lstVal);
                            this.context.SP_DELETE_SUMMED_UP_RECORD(1, strValDelete, 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        /*- Kiểm tra học kỳ 1 có Miễn giảm hay không: Gọi hàm
                         * ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                            + Nếu kết quả trả về True => return
                            + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                            + False: thì return
                            + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách gọi hàm
                         * SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST) kết quả trả về nếu:
                            + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                            + JudgeResult != null: Điểm TBM cả năm = Điểm TBM học kỳ 1 (JudgeResult3 = JudgeResult)
                         * Thực hiện insert vào bảng SummedUpRecord
                         */

                        foreach (var pupilid in lstPupilExempted)
                        {
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == pupilid);
                            if (!IsExemptedSubject && subjectLearnInHKI) //Duoc miem giam hoc ky 2, co hoc o HK I
                            {
                                SummedUpRecordHistory surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == pupilid);
                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                    surIII.AcademicYearID = surI.AcademicYearID;
                                    surIII.ClassID = surI.ClassID;
                                    surIII.Comment = surI.Comment;
                                    surIII.IsCommenting = surI.IsCommenting;
                                    //surIII.IsLegalBot = surI.IsLegalBot;
                                    surIII.JudgementResult = surI.JudgementResult;
                                    surIII.PeriodID = surI.PeriodID;
                                    surIII.PupilID = surI.PupilID;
                                    surIII.ReTestJudgement = surI.ReTestJudgement;
                                    surIII.ReTestMark = surI.ReTestMark;
                                    surIII.SchoolID = surI.SchoolID;
                                    surIII.SubjectID = surI.SubjectID;
                                    surIII.SummedUpDate = surI.SummedUpDate;
                                    surIII.SummedUpMark = surI.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }
                    /*
                        Bước 2:
                        // Lưu điểm kỳ 2
                        Tìm kiếm trong bảng SummedUpRecord theo
                        -	PupilID = lstSummedUpRecord[i].PupilID
                        -	AcademicYearID= lstSummedUpRecord[i].AcademicYearID
                        -	ClassID = lstSummedUpRecord[i].ClassID
                        -	SubjectID = lstSummedUpRecord[i].SubjectID
                        -	Semester = lstSummedUpRecord[i].Semester
                        Thực hiện xóa bản ghi nếu thấy
                        Thực hiện insert lstSummedUpRecord vào SummedUpRecord
                     */
                    if (lstSummedUpRecord != null && lstSummedUpRecord.Count > 0)
                    {
                        // Xoa du lieu cu HK2 va ca nam
                        lstSummedUpAll = lstSummedUpAll.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) && lstPupilIDtmp.Contains(o.PupilID)).ToList();
                        if (lstSummedUpAll.Count > 0)
                        {
                            List<int> lstVal = lstSummedUpAll.Select(c => c.PupilID).Distinct().ToList();
                            string strValDelete = string.Join(",", lstVal);
                            this.context.SP_DELETE_SUMMED_UP_RECORD(1, strValDelete, 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);
                        }

                        foreach (SummedUpRecordHistory summedUpRecord in lstSummedUpRecord)
                        {
                            if (summedUpRecord.JudgementResult != "-1")
                            {
                                summedUpRecord.CreatedAcademicYear = academicYear.Year;
                            }
                        }

                        List<SummedUpRecordHistory> listSummedUpInsertTmp = lstSUR.Where(p => p.JudgementResult != "-1" && !p.JudgementResult.Equals(string.Empty)).ToList();
                        if (listSummedUpInsertTmp != null && listSummedUpInsertTmp.Count > 0)
                        {
                            for (int i = 0; i < listSummedUpInsertTmp.Count; i++)
                            {
                                listSummedUpInsert.Add(listSummedUpInsertTmp[i]);
                            }
                        }

                        //SummedUpRecordBusiness.Save();

                        /*
                         // tính cả năm
                        - Kiểm tra học kỳ 1 có Miễn giảm hay không:
                         * Gọi hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester):
                        + Nếu kết quả trả về True => Điểm TBM cả năm = TBM học kỳ 2
                        + Nếu kết quả trả về Flase: Kiểm tra môn học đó có học trong học kỳ 1 không
                         * bằng cách gọi hàm JudgeRecordBusiness.CheckSubjectForSemester (SchoolID, AcademicYearID, ClassID, SEMESTER_OF_YEAR_FIRST, SubjectID)
                         * nếu kết quả trả về:
                        + False: thì return Điểm TBM cả năm = TBM học kỳ 2
                        + True: Kiểm tra học kỳ I có điểm TBM chưa bằng cách
                         * gọi hàm SummedUpRecordBusiness.SearchBySchool(SchoolID, PupilID, AcademicYearID, ClassID, SubjectID, SEMESTER_OF_YEAR_FIRST)
                         * kết quả trả về nếu:
                        + JudgeResult = null: Thông báo môn học chưa tính điểm TBM học kỳ 1
                        + JudgeResult != null:
                         * Nếu ((sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “Đ”)
                         * or (sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “Đ”)) thì JudgementResult3 = “Đ” – cả năm.
                         Nếu ((sur1.JudgementResult1 = “CĐ” and sur2.JudgementResult2 = “CĐ”)
                         * or (sur1.JudgementResult1 = “Đ” and sur2.JudgementResult2 = “CĐ”)) thì JudgementResult3 = “CĐ” – cả năm.
                         */

                        foreach (var summedUpRecord in lstSummedUpRecord)
                        {
                            // lay diem != -1
                            if (summedUpRecord.JudgementResult.Equals("-1"))
                            {
                                continue;
                            }
                            bool IsExemptedSubject = listExemptedHKI.Any(u => u.PupilID == summedUpRecord.PupilID);
                            if (IsExemptedSubject || !subjectLearnInHKI)
                            {
                                SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                surIII.ClassID = summedUpRecord.ClassID;
                                surIII.Comment = summedUpRecord.Comment;
                                surIII.IsCommenting = summedUpRecord.IsCommenting;
                                //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                surIII.JudgementResult = summedUpRecord.JudgementResult;
                                surIII.PeriodID = summedUpRecord.PeriodID;
                                surIII.PupilID = summedUpRecord.PupilID;
                                surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                surIII.ReTestMark = summedUpRecord.ReTestMark;
                                surIII.SchoolID = summedUpRecord.SchoolID;
                                surIII.SubjectID = summedUpRecord.SubjectID;
                                surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                surIII.CreatedAcademicYear = academicYear.Year;
                                surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                //SummedUpRecordBusiness.Insert(surIII);
                                listSummedUpInsert.Add(surIII);
                            }
                            else
                            {
                                SummedUpRecordHistory surI = lstSummedUpHKI.FirstOrDefault(u => u.PupilID == summedUpRecord.PupilID);

                                if (surI != null && !string.IsNullOrWhiteSpace(surI.JudgementResult))
                                {
                                    SummedUpRecordHistory surIII = new SummedUpRecordHistory();
                                    surIII.AcademicYearID = summedUpRecord.AcademicYearID;
                                    surIII.ClassID = summedUpRecord.ClassID;
                                    surIII.Comment = summedUpRecord.Comment;
                                    surIII.IsCommenting = summedUpRecord.IsCommenting;
                                    //surIII.IsLegalBot = summedUpRecord.IsLegalBot;
                                    surIII.JudgementResult = summedUpRecord.JudgementResult;
                                    surIII.PeriodID = summedUpRecord.PeriodID;
                                    surIII.PupilID = summedUpRecord.PupilID;
                                    surIII.ReTestJudgement = summedUpRecord.ReTestJudgement;
                                    surIII.ReTestMark = summedUpRecord.ReTestMark;
                                    surIII.SchoolID = summedUpRecord.SchoolID;
                                    surIII.SubjectID = summedUpRecord.SubjectID;
                                    surIII.SummedUpDate = summedUpRecord.SummedUpDate;
                                    surIII.SummedUpMark = summedUpRecord.SummedUpMark;
                                    surIII.CreatedAcademicYear = academicYear.Year;
                                    surIII.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                    //SummedUpRecordBusiness.Insert(surIII);
                                    listSummedUpInsert.Add(surIII);
                                }
                            }
                        }
                    }

                    if (listSummedUpInsert != null && listSummedUpInsert.Count > 0)
                    {
                        SummedUpRecordHistoryBusiness.BulkInsert(listSummedUpInsert, SummedUpColumnMappings(), "SummedUpRecordID");
                    }

                    #endregion insert ky II
                }
                #endregion hoc ky 2
                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, Semester, Period, classSubject);
            }

        }

        public IQueryable<JudgeRecordHistory> SearchJudgeRecordHistory(IDictionary<string, object> dic)
        {
            int JudgeRecordID = Utils.GetInt(dic, "JudgeRecordID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Semester = Utils.GetInt(dic, "Semester", -1);
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            string Judgement = Utils.GetString(dic, "Judgement");
            string ReTestJudgement = Utils.GetString(dic, "ReTestJudgement");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            int OrderNumber = Utils.GetInt(dic, "OrderNumber");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");

            //tien hanh tim
            IQueryable<JudgeRecordHistory> ListJudgeRecordHistory = JudgeRecordHistoryRepository.All.Where(o => o.PupilProfile.IsActive);
            //AnhVD 20131217 
            if (SchoolID > 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.Last2digitNumberSchool == SchoolID % 100));
            }
            //End
            if (JudgeRecordID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.JudgeRecordID == JudgeRecordID));
            }
            if (PupilID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.AcademicYearID == AcademicYearID));
            }
            if (MarkTypeID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (SubjectID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.SubjectID == SubjectID));
            }
            if (Semester != -1)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.Semester == Semester));
            }
            // Quanglm Sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (Judgement != null && !Judgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.Judgement.Contains(Judgement)));
            }
            if (ReTestJudgement != null && !ReTestJudgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.ReTestJudgement.Contains(ReTestJudgement)));
            }
            if (CreatedDate.HasValue)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                   o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (OrderNumber != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.OrderNumber == OrderNumber));
            }
            if (MarkTitle != "")
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.MarkType.Title == MarkTitle));
            }
            if (Title != "")
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.Title == Title));
            }
            if (strMarkType != null && strMarkType != "")
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                ListJudgeRecordHistory = ListJudgeRecordHistory.Where(o => (o.ClassProfile.EducationLevelID == EducationLevelID));
            }

            return ListJudgeRecordHistory;
        }

        public void InsertJudgeRecordPrimaryHistory(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory)
        {
            if (lstJudgeRecordHistory == null || lstSummedUpRecordHistory == null) return;
            if (lstJudgeRecordHistory.Count == 0 && lstSummedUpRecordHistory.Count == 0) return;
            // Lay list pupilID:
            List<int> lstPupilID = lstSummedUpRecordHistory.Select(u => u.PupilID).Distinct().ToList();

            JudgeRecordHistory firstJudge = lstJudgeRecordHistory.FirstOrDefault();
            SummedUpRecordHistory firstSum = lstSummedUpRecordHistory.FirstOrDefault();
            int schoolID = firstJudge != null ? firstJudge.SchoolID : firstSum.SchoolID;
            int academicYearID = firstJudge != null ? firstJudge.AcademicYearID : firstSum.AcademicYearID;
            int classID = firstJudge != null ? firstJudge.ClassID : firstSum.ClassID;
            int subjectID = firstJudge != null ? firstJudge.SubjectID : firstSum.SubjectID;
            int semester = firstJudge != null ? firstJudge.Semester.Value : firstSum.Semester.Value;
            // Validate trang thai, mien giam hoc sinh
            #region validate
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID}
            }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, classID, subjectID))
                throw new BusinessException("");

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object> { { "SubjectID", subjectID }, { "SchoolID", schoolID } }).SingleOrDefault();
            if (classSubject == null)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (classSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                throw new BusinessException("Common_Validate_IsCommenting ");

            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semester).Where(u => u.SubjectID == subjectID).ToList();
            var listPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ClassID", classID }, { "Status", SystemParamsInFile.PUPIL_STATUS_STUDYING } }).Where(o => lstPupilID.Contains(o.PupilID))
                               //join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           select new { poc.Status, poc.AcademicYearID, poc.ClassID })
                           .ToList();
            // Kiem tra trang thai hoc sinh dang hoc
            if (listPOC.Count != lstPupilID.Count)
            {
                throw new BusinessException("PupilProfile_Label_Not_Pupil_Status_Studying");
            }
            // Kiem tra nam hoc va lop hoc hien tai
            if (listPOC.Any(o => o.AcademicYearID != academicYearID || o.ClassID != classID))
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Thong tin khoa diem
            string LockTitleS = MarkRecordBusiness.GetLockMarkTitlePrimary(schoolID, academicYearID, classID, subjectID, semester);
            // Kiem tra neu nhu con diem hoc ky bi khoa thi coi nhu khong xu ly gi ca
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                if (LockTitleS.Contains(SystemParamsInFile.HKI + ","))
                {
                    return;
                }
            }
            else
            {
                if (LockTitleS.Contains(SystemParamsInFile.HKII + ","))
                {
                    return;
                }
            }

            string[] LockTitle = LockTitleS.Split(',');
            int classGrade = classSubject.ClassProfile.EducationLevelID;
            Dictionary<int, int> dic12 = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 }, { 3, 3 }, { 4, 4 }, { 5, 1 }, { 6, 2 }, { 7, 3 }, { 8, 4 } };
            Dictionary<int, int> dic345 = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 }, { 3, 3 }, { 4, 4 }, { 5, 5 }, { 6, 1 }, { 7, 2 }, { 8, 3 }, { 9, 4 }, { 10, 5 } };
            Dictionary<int, int> dic = new Dictionary<int, int>();
            // Doi voi hoc ky 2 thi he so con diem bi khoa se tang theo bien nay
            int startLockIdx = 0;
            if (classGrade == 1 || classGrade == 2)
            {
                dic = dic12;
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    startLockIdx = 4;
                }
            }
            else
            {
                dic = dic345;
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    startLockIdx = 5;
                }
            }
            //Lấy từng phần tử trong lstJudgeRecord
            foreach (JudgeRecordHistory JudgeRecord in lstJudgeRecordHistory)
            {
                ValidationMetadata.ValidateObject(JudgeRecord);

                if (listExempted.Any(u => u.PupilID == JudgeRecord.PupilID))
                    throw new BusinessException("Common_Validate_IsExemptedSubject");
                //check record has lock?

            }
            #endregion
            //Tìm kiếm bản ghi
            IDictionary<string, object> SearchJudge = new Dictionary<string, object>();
            SearchJudge["SchoolID"] = schoolID;
            SearchJudge["AcademicYearID"] = academicYearID;
            SearchJudge["ClassID"] = classID;
            SearchJudge["SubjectID"] = subjectID;
            SearchJudge["Semester"] = semester;
            List<JudgeRecordHistory> ListJudgeRecord = this.SearchJudgeRecordHistory(SearchJudge).Where(u => lstPupilID.Contains(u.PupilID)).ToList();
            // Chi xoa nhung con diem khong bi khoa
            #region Kiem tra de khong xoa cac con diem bi khoa
            if (LockTitle.Count() > 0 && LockTitle[0].Length > 0)
            {
                int count = ListJudgeRecord.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    JudgeRecordHistory JudgeRecord = ListJudgeRecord[i];
                    bool isDel = true;
                    if (JudgeRecord.Title.Equals("C1") || JudgeRecord.Title.Equals("HK"))
                    {
                        foreach (string lTitle in LockTitle)
                        {
                            if (string.IsNullOrWhiteSpace(lTitle))
                            {
                                continue;
                            }
                            if (lTitle.Contains("NX"))
                            {
                                int idxLock = 0;
                                int.TryParse(lTitle.Replace("_", "").Replace("NX", ""), out idxLock);
                                // Bo qua cac con diem bi khoa
                                // Doi voi ky 2 thi con diem khoa phai lon hon chi so bat dau
                                if (idxLock > startLockIdx && dic[idxLock] == JudgeRecord.OrderNumber)
                                {
                                    isDel = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (!isDel)
                    {
                        ListJudgeRecord.RemoveAt(i);
                    }
                }
            }
            #endregion

            if (ListJudgeRecord.Count > 0)
            {
                this.DeleteAll(ListJudgeRecord);
                // Luu diem cu
                SetOldValue(ListJudgeRecord, lstJudgeRecordHistory);
            }

            foreach (JudgeRecordHistory JudgeRecord in lstJudgeRecordHistory)
            {
                bool isInsert = true;
                if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && (JudgeRecord.Title.Equals("C1") || JudgeRecord.Title.Equals("HK")))
                {
                    foreach (string lTitle in LockTitle)
                    {
                        if (string.IsNullOrWhiteSpace(lTitle))
                        {
                            continue;
                        }
                        if (lTitle.Contains("NX"))
                        {
                            int idxLock = 0;
                            int.TryParse(lTitle.Replace("_", "").Replace("NX", ""), out idxLock);
                            // Bo qua cac con diem bi khoa
                            if (idxLock > startLockIdx && dic[idxLock] == JudgeRecord.OrderNumber)
                            {
                                isInsert = false;
                                break;
                            }
                        }
                    }
                }
                if (isInsert)
                {
                    this.Insert(JudgeRecord);
                }
            }

            if (lstSummedUpRecordHistory.Count == 0)
                return;

            //Tìm kiếm bản ghi trong bảng SummedUpRecord
            IDictionary<string, object> SearchSur = new Dictionary<string, object>();
            SearchSur["AcademicYearID"] = academicYearID;
            SearchSur["ClassID"] = classID;
            SearchSur["SubjectID"] = subjectID;
            SearchSur["SchoolID"] = schoolID;
            if (classSubject.SectionPerWeekSecondSemester > 0)
                SearchSur["Semester"] = semester;
            //else
            //SearchSur["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            List<SummedUpRecordHistory> ListSummedUp = this.SummedUpRecordHistoryBusiness.SearchSummedUpHistory(SearchSur).Where(u => lstPupilID.Contains(u.PupilID)).ToList();
            if (ListSummedUp.Count > 0)
                SummedUpRecordHistoryBusiness.DeleteAll(ListSummedUp);

            foreach (SummedUpRecordHistory sur in lstSummedUpRecordHistory)
            {
                if (sur.JudgementResult == null)
                {
                    continue;
                }
                if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && LockTitle.Contains("HK" + semester))
                {
                    throw new BusinessException("Common_Validate_LockMarkTitleError");
                }
                SummedUpRecordHistoryBusiness.Insert(sur);
                //truong hop chi hoc o ky 1 NAMTA
                if (classSubject.SectionPerWeekSecondSemester == 0 && semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    SummedUpRecordHistory sAll = new SummedUpRecordHistory()
                    {
                        AcademicYearID = sur.AcademicYearID,
                        ClassID = sur.ClassID,
                        Comment = sur.Comment,
                        IsCommenting = sur.IsCommenting,
                        IsOldData = sur.IsOldData,
                        JudgementResult = sur.JudgementResult,
                        PeriodID = sur.PeriodID,
                        PupilID = sur.PupilID,
                        ReTestJudgement = sur.ReTestJudgement,
                        ReTestMark = sur.ReTestMark,
                        SchoolID = sur.SchoolID,
                        Semester = sur.Semester,
                        SubjectID = sur.SubjectID,
                        SummedUpDate = sur.SummedUpDate,
                        SummedUpMark = sur.SummedUpMark,
                        CreatedAcademicYear = sur.CreatedAcademicYear
                    };

                    sAll.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    SummedUpRecordHistoryBusiness.Insert(sAll);
                }
            }
        }

        public void DeleteJudgeRecordHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
            SearchPOC["ClassID"] = ClassID;
            SearchPOC["Check"] = "Check";
            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

            if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");

            string MarkLockRecord = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
            List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
            bool isAdminSchool = UserAccountBusiness.IsSchoolAdmin(UserID);

            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["Semester"] = Semester;
            SearchInfo["checkWithClassMovement"] = "1";
            SearchInfo["Year"] = academicYear.Year; // AnhVD 20131218
            string strMarkTitle = PeriodID.HasValue ? MarkRecordBusiness.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            string strtmp = strMarkTitle;
            string[] arrMarkType = strMarkTitle.Split(',');
            IQueryable<JudgeRecordHistory> listJudgeForDelete = this.SearchJudgeRecordHistory(SearchInfo).Where(u => listPupilID.Contains(u.PupilID)).Where(u => arrMarkType.Contains(u.Title));

            if (PeriodID.HasValue)
                listJudgeForDelete = listJudgeForDelete.Where(u => strMarkTitle.Contains(u.Title));
            var ListJudgeDelete = listJudgeForDelete.ToList();

            if (!isAdminSchool)
            {
                ListJudgeDelete = ListJudgeDelete.Where(u => !markLocks.Contains(u.Title)).ToList();
                for (int i = 0; i < markLocks.Count; i++)
                {
                    if (strtmp.Contains(markLocks[i]))
                    {
                        strtmp = strtmp.Replace(markLocks[i], "");
                    }
                }
            }

            if (ListJudgeDelete.Count > 0)
            {
                //  this.DeleteAll(ListJudgeDelete);
                List<int> listPupilIdDelete = ListJudgeDelete.Select(p => p.PupilID).Distinct().ToList();
                MarkRecordBusiness.SP_DeleteJudgeRecord(AcademicYearID, SchoolID, ClassID, Semester, SubjectID, 1, PeriodID.HasValue ? PeriodID.Value : 0, listPupilIdDelete, strtmp);
            }
            #region Save ThreadMark
            // AnhVD 20131225 - Insert into ThreadMark for Auto Process
            List<int> pupilIDs = ListJudgeDelete.Select(o => o.PupilID).ToList();
            ThreadMarkBO info = new ThreadMarkBO();
            info.SchoolID = SchoolID;
            info.ClassID = ClassID;
            info.AcademicYearID = AcademicYearID;
            info.SubjectID = SubjectID;
            info.Semester = Semester;
            info.PeriodID = PeriodID;
            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
            info.PupilIds = pupilIDs;
            //ThreadMarkBusiness.InsertActionList(info);
            //ThreadMarkBusiness.Save();
            ThreadMarkBusiness.BulkInsertActionList(info);
            // End
            #endregion
        }


        /// <summary>
        /// QuangLM
        /// Cap nhat lai thong tin diem cu
        /// </summary>
        /// <param name="listJudgeForDelete"></param>
        /// <param name="listJudgeForInsert"></param>
        private void SetOldValue(List<JudgeRecordHistory> listJudgeForDelete, List<JudgeRecordHistory> listJudgeForInsert)
        {
            foreach (JudgeRecordHistory itemDel in listJudgeForDelete)
            {
                foreach (JudgeRecordHistory itemInsert in listJudgeForInsert)
                {
                    if (itemInsert.PupilID == itemDel.PupilID && itemInsert.OrderNumber == itemDel.OrderNumber)
                    {
                        // Neu diem khac
                        if (itemInsert.Judgement != itemDel.Judgement)
                        {
                            itemInsert.CreatedDate = itemDel.CreatedDate;
                            itemInsert.IsSMS = false;
                            itemInsert.ModifiedDate = DateTime.Now;
                            itemInsert.OldJudgement = itemDel.Judgement;
                        }
                        else
                        {
                            itemInsert.CreatedDate = itemDel.CreatedDate;
                            itemInsert.IsSMS = itemDel.IsSMS;
                            itemInsert.ModifiedDate = null;
                            itemInsert.OldJudgement = itemDel.OldJudgement;
                        }
                        break;
                    }
                }
            }
        }

        public IQueryable<JudgeRecordHistory> SearchJudgeRecordHistory(int AcademicYearID, IDictionary<string, object> dic)
        {
            dic.Add("AcademicYearID", AcademicYearID);
            return this.SearchJudgeRecordHistory(dic);
        }

        /// <summary>
        /// Nhập điểm của năm học trước bằng excel
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstJudgeRecord"></param>
        public void ImportJudgeRecordHistory(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory)
        {

            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstJudgeRecordHistory.Count == 0) return;
                JudgeRecordHistory firstJudgeRecord = lstJudgeRecordHistory.First();
                int schoolID = firstJudgeRecord.SchoolID;
                int academicYearID = firstJudgeRecord.AcademicYearID;
                int semester = firstJudgeRecord.Semester.Value;
                ClassProfile firstClass = ClassProfileBusiness.Find(firstJudgeRecord.ClassID);
                int educationLevelID = firstClass.EducationLevelID;
                int grade = firstClass.EducationLevel.Grade;
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                #region validate
                bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                            new Dictionary<string, object>()
                {
                    {"SchoolID", schoolID},
                    {"AcademicYearID", academicYearID}
                }, null);
                if (!SchoolCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra neu ton tai hoc sinh co trang thai khac dang hoc thi bao loi
                List<int> listPupilID = lstJudgeRecordHistory.Select(o => o.PupilID).Distinct().ToList();
                int countPupil = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "AcademicYearID", academicYearID } })
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where pp.IsActive && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                  && listPupilID.Contains(poc.PupilID)
                                  select poc).Count();

                if (listPupilID.Count != countPupil)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
                // Kiem tra neu co mon hoc va lop hoc khong duoc gan tuong ung trong ClassSubject thi bao loi
                var listClassSubjectID = lstJudgeRecordHistory.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
                List<int> listClassID = listClassSubjectID.Select(o => o.ClassID).Distinct().ToList();
                List<ClassSubject> listClassSubject = ClassSubjectBusiness.All.Where(o => listClassID.Contains(o.ClassID) && o.Last2digitNumberSchool == partitionId).ToList();
                foreach (var classSubjectID in listClassSubjectID)
                {
                    ClassSubject cs = listClassSubject.FirstOrDefault(o => o.ClassID == classSubjectID.ClassID && o.SubjectID == classSubjectID.SubjectID);
                    if (cs == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (cs != null && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                // Kiem tra neu co hoc sinh thuoc dang mien giam nhung van co du lieu thi bao loi      
                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.All.Where(o => o.SchoolID == schoolID
                                        && o.AcademicYearID == academicYearID && ((semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                        && o.FirstSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL)
                                        ||
                                        (semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        && o.SecondSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL))).ToList();

                foreach (JudgeRecordHistory judgeRecord in lstJudgeRecordHistory)
                {
                    // Kiem tra mien giam
                    if (lstExempted.Any(o => o.PupilID == judgeRecord.PupilID && o.SubjectID == judgeRecord.SubjectID && o.ClassID == judgeRecord.ClassID))
                    {
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                    }
                    ValidationMetadata.ValidateObject(judgeRecord);
                    if (!string.IsNullOrWhiteSpace(judgeRecord.Judgement))
                    {
                        UtilsBusiness.CheckValidateMark(grade, SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE, judgeRecord.Judgement);
                    }
                }
                #endregion

                #region them moi hoac cap nhat
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["EducationLevelID"] = educationLevelID;
                SearchInfo["Semester"] = semester;
                // Neu chi import cho 1 lop thi chi can lay diem cua 1 lop la du
                if (!lstJudgeRecordHistory.Any(o => o.ClassID != firstJudgeRecord.ClassID))
                {
                    SearchInfo["ClassID"] = firstJudgeRecord.ClassID;
                }
                List<JudgeRecordHistory> listJudgeAll = this.SearchJudgeRecordHistory(SearchInfo).ToList();
                // Luu lai thong tin de cap nhat vao bang tien trinh tinh TBM tu dong
                Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();
                // Danh sach cac list diem insert de bo sung vao danh sach lay ra dem di tong ket ma khong can
                // phai truy van lai nua
                List<JudgeRecordHistory> listJudgeRecordHistoryIns = new List<JudgeRecordHistory>();

                foreach (JudgeRecordHistory JudgeRecord in lstJudgeRecordHistory)
                {
                    var listJudge = listJudgeAll.Where(o => o.PupilID == JudgeRecord.PupilID &&
                                                            o.ClassID == JudgeRecord.ClassID &&
                                                            o.SubjectID == JudgeRecord.SubjectID &&
                                                            o.Title == JudgeRecord.Title);
                    bool isUpdate = false;
                    JudgeRecordHistory judgeRecordUpdate = listJudge.FirstOrDefault();
                    if (judgeRecordUpdate != null)
                    {
                        if (judgeRecordUpdate.Judgement != JudgeRecord.Judgement)
                        {
                            judgeRecordUpdate.Judgement = JudgeRecord.Judgement;
                            this.Update(judgeRecordUpdate);
                            isUpdate = true;
                        }
                    }
                    else
                    {
                        listJudgeRecordHistoryIns.Add(JudgeRecord);
                    }

                    if (isUpdate)
                    {
                        string key = JudgeRecord.ClassID + "_" + JudgeRecord.SubjectID;
                        if (dicPupilThreadMark.ContainsKey(key))
                        {
                            listPupilID = dicPupilThreadMark[key];
                            if (!listPupilID.Contains(JudgeRecord.PupilID))
                            {
                                listPupilID.Add(JudgeRecord.PupilID);
                                dicPupilThreadMark[key] = listPupilID;
                            }
                        }
                        else
                        {
                            dicPupilThreadMark[key] = new List<int>() { JudgeRecord.PupilID };
                        }
                    }
                }
                // Insert diem
                if (listJudgeRecordHistoryIns.Count > 0)
                {
                    BulkInsert(listJudgeRecordHistoryIns, JudgeColumnMappings(), "JudgeRecordHistoryID");
                }
                #endregion them moi hoac cap nhat

                this.Save();
                #region tinh diem kiem tra hoc ky va ca nam
                if (lstJudgeRecordHistory.Count > 0)
                {

                    if (listClassID.Count > 1)
                    {

                        int classId = 0;
                        int subjectId = 0;
                        List<int> lstSubject;
                        for (int i = 0; i < listClassID.Count; i++)
                        {
                            classId = listClassID[i];
                            List<int> lstPupilId = lstJudgeRecordHistory.Where(s => s.ClassID == classId).Select(s => s.PupilID).Distinct().ToList();
                            lstSubject = lstJudgeRecordHistory.Where(c => c.ClassID == classId).Select(c => c.SubjectID).Distinct().ToList();
                            for (int j = 0; j < lstSubject.Count; j++)
                            {
                                subjectId = lstSubject[j];
                                ClassSubject classSubject = listClassSubject.FirstOrDefault(s => s.ClassID == classId && s.SubjectID == subjectId);
                                //this.context.PROC_CLASS_JUDGE_SUMMED_UP(schoolID, academicYearID, classId, semester, subjectId);
                                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, semester, 1, classSubject);
                                //Tinh TBM cho dot
                                JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, semester, null, classSubject);
                            }
                        }
                    }
                    else
                    {
                        foreach (var classSubjectID in listClassSubjectID)
                        {
                            //this.context.PROC_CLASS_JUDGE_SUMMED_UP(schoolID, academicYearID, classSubjectID.ClassID, semester, classSubjectID.SubjectID);
                            List<int> lstPupilId = lstJudgeRecordHistory.Where(s => s.ClassID == classSubjectID.ClassID).Select(s => s.PupilID).Distinct().ToList();
                            ClassSubject classSubject = listClassSubject.FirstOrDefault(s => s.ClassID == classSubjectID.ClassID && s.SubjectID == classSubjectID.SubjectID);
                            //Tinh TBM cho ky
                            JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, semester, 1, classSubject);
                            //Tinh TBM cho dot
                            JudgeRecordBusiness.SummedJudgeMark(listPupilID, academicYear, semester, null, classSubject);
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportJudgeRecordHistory", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        private IDictionary<string, object> SummedUpColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("SummedUpMark", "SUMMED_UP_MARK");
            columnMap.Add("JudgementResult", "JUDGEMENT_RESULT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("SummedUpDate", "SUMMED_UP_DATE");
            columnMap.Add("ReTestMark", "RE_TEST_MARK");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            return columnMap;
        }
        private IDictionary<string, object> JudgeColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Judgement", "JUDGEMENT");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldJudgement", "OLD_JUDGEMENT");
            columnMap.Add("MJudgement", "M_JUDGEMENT");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;
        }
    }
}
