/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    /// <summary>
    /// smscommunicationGroup - save sms content groupby last sender
    /// </summary>
    public partial class SMSCommunicationGroupBusiness
    {  
        /// <summary>
        /// update smsCommunicationGroup by last sender
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>08/05/2013</date>
        /// <param name="dic"></param>
		public void UpdateSMSGroup(IDictionary<string,object> dic)
        {
            int? teacherID = Utils.GetNullableInt(dic, "teacherID");
            int pupilprofileID = Utils.GetInt(dic, "pupilProfileID");
            int contactGroupID = Utils.GetInt(dic, "contactGroupID");            
            int type = Utils.GetInt(dic, "type");
            int schoolID = Utils.GetInt(dic, "schoolID");                       
            bool isAdmin = Utils.GetBool(dic, "isAdmin", false);
            bool isPrincipal = Utils.GetBool(dic, "isPrincipal", false);
            bool isSubmitChange = Utils.GetBool(dic, "isSubmitChange", true);
            DateTime? dateTime = Utils.GetDateTime(dic, "dateTime");
            if (!dateTime.HasValue)
            {
                dateTime = DateTime.Now;
            }

            SMSCommunication objSMSCommunication = Utils.GetObject<SMSCommunication>(dic, "smsCommunication");

            SMSCommunicationGroup objSMSCommunicationGroup = null;

            switch (type)
            {
                case GlobalConstants.SMS_Communicatoin_Parent_To_Teacher:
                case GlobalConstants.SMS_Communicatoin_Teacher_To_Parent:
                    {
                        //Groupby teacherID and pupilProfileID
                        if (isAdmin)
                        {
                            objSMSCommunicationGroup = (from sg in SMSCommunicationGroupBusiness.All
                                                        where sg.TeacherID == null && sg.PupilProfileID == pupilprofileID
                                                             && sg.SchoolID == schoolID && sg.IsAdmin == isAdmin
                                                        select sg).FirstOrDefault();
                            teacherID = null;
                        }
                        else
                        {
                            objSMSCommunicationGroup = (from sg in SMSCommunicationGroupBusiness.All
                                                        where sg.TeacherID == teacherID && sg.PupilProfileID == pupilprofileID
                                                             && sg.SchoolID == schoolID && sg.IsAdmin == isAdmin
                                                        select sg).FirstOrDefault();
                        }

                        if (objSMSCommunicationGroup == null)
                        {
                            objSMSCommunicationGroup = new SMSCommunicationGroup();
                            objSMSCommunicationGroup.SMSCommunication = objSMSCommunication;                            
                            objSMSCommunicationGroup.TeacherID = teacherID;
                            objSMSCommunicationGroup.PupilProfileID = pupilprofileID;
                            objSMSCommunicationGroup.SchoolID = schoolID;
                            objSMSCommunicationGroup.IsAdmin = isAdmin;
                            objSMSCommunicationGroup.Type = (byte)type;
                            objSMSCommunicationGroup.CreateDate = dateTime.Value;

                            SMSCommunicationGroupBusiness.Insert(objSMSCommunicationGroup);
                        }
                        else
                        {
                            objSMSCommunicationGroup.SMSCommunication = objSMSCommunication;
                            objSMSCommunicationGroup.IsAdmin = isAdmin;
                            objSMSCommunicationGroup.Type = (byte)type;
                            objSMSCommunicationGroup.UpdateDate = dateTime.Value;
                        }
                        break;
                    }
                case GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher:
                    {
                        //groupby contactGroup - teacher                       
                        if (isAdmin)
                        {
                            objSMSCommunicationGroup = (from sg in SMSCommunicationGroupBusiness.All
                                                        where sg.SchoolID == schoolID
                                                            && sg.ContactGroupID == contactGroupID
                                                        select sg).FirstOrDefault();
                            teacherID = null;
                        }
                        else
                        {
                            //kiem tra neu la quyen BGH - va la nhom toan truong thi duoc set nhu la admin truong gui
                            if (isPrincipal)
                            {
                                ContactGroup objContactGroup = ContactGroupBusiness.Find(contactGroupID);
                                if (objContactGroup.IsDefault.HasValue && objContactGroup.IsDefault.Value)
                                {
                                    teacherID = null;
                                    isAdmin = true;
                                }
                            }                                                        
		
                            objSMSCommunicationGroup = (from sg in SMSCommunicationGroupBusiness.All
                                                        where sg.ContactGroupID == contactGroupID && sg.SchoolID == schoolID                                                            
                                                        select sg).FirstOrDefault();                           
                        }

                        if (objSMSCommunicationGroup == null)
                        {
                            objSMSCommunicationGroup = new SMSCommunicationGroup();
                            objSMSCommunicationGroup.SMSCommunication = objSMSCommunication;
                            objSMSCommunicationGroup.ContactGroupID = contactGroupID;
                            objSMSCommunicationGroup.TeacherID = teacherID;
                            objSMSCommunicationGroup.SchoolID = schoolID;
                            objSMSCommunicationGroup.IsAdmin = isAdmin;
                            objSMSCommunicationGroup.Type = (byte)type;
                            objSMSCommunicationGroup.CreateDate = dateTime.Value;

                            SMSCommunicationGroupBusiness.Insert(objSMSCommunicationGroup);
                        }
                        else
                        {
                            objSMSCommunicationGroup.SMSCommunication = objSMSCommunication;
                            objSMSCommunicationGroup.TeacherID = teacherID;
                            objSMSCommunicationGroup.IsAdmin = isAdmin;
                            objSMSCommunicationGroup.UpdateDate = dateTime.Value;
                        }
                        break;
                    }
                default:
                    break;
            }
        
            if (isSubmitChange)
            {
                SMSCommunicationGroupBusiness.Save();
            }
        }        
    }
}