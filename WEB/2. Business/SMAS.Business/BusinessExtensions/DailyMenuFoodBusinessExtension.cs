﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DailyMenuFoodBusiness
    {

        private void Validation()
        {
            /*
                DailyMenuID: key(DailyMenu)
                FoodID: key(FoodCat)
                DailyMenuID, SchoolID: not compatible(DailyMenu)

             */
        }

        /// <summary>
        /// author: namdv3
        /// created date: 15/11/2012
        /// Thêm mới danh sách thực phẩm trong thực đơn ngày
        /// </summary>
        /// <param name="SchoolID">Trường</param>
        /// <param name="DailyMenuID">Thực đơn ngày</param>
        /// <param name="lstDailyMenuFood">Danh sách thực phẩm trong thực đơn ngày</param>
        public void Insert(int SchoolID, int DailyMenuID, List<DailyMenuFood> lstDailyMenuFood)
        {
            Validation();
            //Step1
            /*Tìm kiếm trong bảng DailyMenuFood theo DailyMenuID.
              Xóa tất cả bản ghi tìm được*/
            var listDailyMenuFood = this.All.Where(o => o.DailyMenuID == DailyMenuID).ToList();
            if (listDailyMenuFood.Count > 0)
            {
                this.DeleteAll(listDailyMenuFood);
            }
            /*Step2:
             * Thực hiện insert lstDailyMenuFood vào bảng DailyMenuFood*/
            if (lstDailyMenuFood.Count > 0)
            {
                foreach (var dailumenufood in lstDailyMenuFood)
                {
                    dailumenufood.DailyMenuID = DailyMenuID;
                    this.Insert(dailumenufood);
                }
            }
        }

        /// <summary>
        /// author: namdv3
        /// created date: 19/11/2012
        /// Tìm kiếm thực đơn ngày
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DailyMenuFood> Search(IDictionary<string, object> SearchInfo)
        {
            int DailyMenuFoodID = Utils.GetInt(SearchInfo, "DailyMenuFoodID");
            int FoodID = Utils.GetInt(SearchInfo, "FoodID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            decimal Weight = Utils.GetDecimal(SearchInfo, "Weight");
            int PricePerOne = Utils.GetInt(SearchInfo, "PricePerOne");
            int DailyMenuID = Utils.GetInt(SearchInfo, "DailyMenuID");

            var query = this.All;

            if (SchoolID > 0)
                query = query.Where(u => u.DailyMenu.SchoolID == SchoolID);

            if (DailyMenuFoodID > 0)
                query = query.Where(o => o.DailyMenuFoodID == DailyMenuFoodID);

            if (FoodID > 0)
                query = query.Where(o => o.FoodID == FoodID);

            if (Weight > 0)
                query = query.Where(o => o.Weight == Weight);

            if (PricePerOne > 0)
                query = query.Where(o => o.PricePerOnce == PricePerOne);

            if (DailyMenuID > 0)
                query = query.Where(o => o.DailyMenuID == DailyMenuID);

            return query;
        }

        public IQueryable<DailyMenuFood> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID <= 0) 
                return null;

            if (SearchInfo == null) SearchInfo = new Dictionary<string, object>();

            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
    }
}