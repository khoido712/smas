﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class DevelopmentOfChildrenReportBusiness
    {
        public const int NUMBER_OF_EVALUATIONDEVELOPMENTGROUP = 5;

        #region Lấy mảng băm cho các tham số đầu vào

        /// <summary>
        /// minhh
        ///Lấy mảng băm cho các tham số đầu vào
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(ExcelDevelopmentOfChildren entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ReportType", entity.ReportType},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID}
            };
            return ReportUtils.GetHashKey(dic);
        }

        #endregion Lấy mảng băm cho các tham số đầu vào

        #region

        /// <summary>
        /// minhh
        /// Lưu lại thông tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren entity, Stream data)
        {
            string reportCode = "";


            ProcessedReport pr = new ProcessedReport();
            if (entity.ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DANH_GIA_SU_PHAT_TRIEN;
            }
            else if (entity.ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_KET_QUA_DANH_GIA_TRE;
            }
            else if (entity.ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_TONG_HOP_KET_QUA_DANH_GIA;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string educationName = EducationLevelBusiness.Find(entity.EducationLevelID).Resolution;
            string className = entity.ClassID > 0 ? ClassProfileBusiness.Find(entity.ClassID).DisplayName : educationName;
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationName));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(className));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ReportType", entity.ReportType},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        #endregion Lưu lại thông tin thống kê xep loai mon hoc theo khoi

        public ProcessedReport GetDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren entity, int ReportType)
        {
            string reportCode = "";
            if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DANH_GIA_SU_PHAT_TRIEN;
            }
            else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_KET_QUA_DANH_GIA_TRE;
            }
            else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_TONG_HOP_KET_QUA_DANH_GIA;
            }
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public IQueryable<DevelopmentByClassBO> GetListDevelopmentByClass(int SchoolID, int AcademicYearID, int ClassID, int? Semester, int AppliedLevel)
        {
            IQueryable<DevelopmentByClassBO> query;
            if (Semester.HasValue)
            {
                query = from doc in DevelopmentOfChildrenRepository.All
                        join cp in ClassProfileRepository.All on doc.ClassID equals cp.ClassProfileID
                        join ed in EvaluationDevelopmentRepository.All on doc.EvaluationDevelopmentID equals ed.EvaluationDevelopmentID
                        join edg in EvaluationDevelopmentGroupRepository.All on ed.EvaluationDevelopmentGroupID equals edg.EvaluationDevelopmentGroupID
                        where ((doc.SchoolID == SchoolID)
                        && (doc.AcademicYearID == AcademicYearID)
                        && (doc.ClassID == ClassID)
                        && (doc.Semester == Semester))
                        select new DevelopmentByClassBO
                        {
                            DevelopmentOfChildrenID = doc.DevelopmentOfChildrenID,
                            ClassID = cp.ClassProfileID,
                            EvaluationDevelopmentID = ed.EvaluationDevelopmentID,
                            EvaluationDevelopmentGroupID = edg.EvaluationDevelopmentGroupID,
                            PupilID = doc.PupilID,
                            Semester = doc.Semester,
                            EvaluationDevelopmentName = ed.EvaluationDevelopmentName,
                            BirthDate = doc.PupilProfile.BirthDate
                        };
            }
            else
            {
                query = from doc in DevelopmentOfChildrenRepository.All
                        join cp in ClassProfileRepository.All on doc.ClassID equals cp.ClassProfileID
                        join ed in EvaluationDevelopmentRepository.All on doc.EvaluationDevelopmentID equals ed.EvaluationDevelopmentID
                        join edg in EvaluationDevelopmentGroupRepository.All on ed.EvaluationDevelopmentGroupID equals edg.EvaluationDevelopmentGroupID
                        where ((doc.SchoolID == SchoolID)
                        && (doc.AcademicYearID == AcademicYearID)
                        && (doc.ClassID == ClassID))
                        select new DevelopmentByClassBO
                        {
                            DevelopmentOfChildrenID = doc.DevelopmentOfChildrenID,
                            ClassID = cp.ClassProfileID,
                            EvaluationDevelopmentID = ed.EvaluationDevelopmentID,
                            EvaluationDevelopmentGroupID = edg.EvaluationDevelopmentGroupID,
                            PupilID = doc.PupilID,
                            Semester = doc.Semester,
                            EvaluationDevelopmentName = ed.EvaluationDevelopmentName,
                            BirthDate = doc.PupilProfile.BirthDate
                        };
            }
            List<DevelopmentByClassBO> lstDevelopmentByClass = query.ToList();
            return lstDevelopmentByClass.AsQueryable();
        }

        public IQueryable<EvaluationDevelopmentGroup> GetListEvaluationDevelopmentGroup(int AppliedLevel)
        {
            IQueryable<EvaluationDevelopmentGroup> query = EvaluationDevelopmentGroupRepository.All
                .Where(o => o.AppliedLevel == AppliedLevel)
                .OrderBy(o => o.EvaluationDevelopmentGroupID);
            return query;
        }

        public Stream CreateDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren entity, int SchoolID, int AppliedLevel, int ReportType)
        {
            // Lay schoolProfile
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            if (school == null) throw new BusinessException("Lỗi dữ liệu");

            string reportCode = "";
            if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DANH_GIA_SU_PHAT_TRIEN;
            }
            else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_KET_QUA_DANH_GIA_TRE;
            }
            else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT)
            {
                reportCode = SystemParamsInFile.REPORT_TONG_HOP_KET_QUA_DANH_GIA;
            }
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook;
            try
            {
                oBook = VTExport.OpenWorkbook(templatePath);
            } 
            catch (Exception) 
            {
                throw new BusinessException("Mở file template lỗi");
            }


            // Lay danh sach cac lop can xuat bao cao
            List<ClassProfile> lstClass;
            if (entity.ClassID > 0)
            {
                var temp = this.ClassProfileBusiness.Find(entity.ClassID);
                lstClass = new List<ClassProfile>(1);
                lstClass.Add(temp);
            }
            else
            {
                IDictionary<string, object> paras = new Dictionary<string, object>();
                paras["AcademicYearID"] = entity.AcademicYearID;
                paras["EducationLevelID"] = entity.EducationLevelID;
                lstClass = this.ClassProfileBusiness.SearchBySchool(SchoolID, paras).OrderBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();
            }

            // Lay danh sach cac nhom tieu chi danh gia cua cap hoc
            var listEvaluationDevelopmentGroup = this.EvaluationDevelopmentGroupRepository.All
                                                .Where(p => p.AppliedLevel == AppliedLevel && p.IsActive)
                                                .OrderBy(p => p.EvaluationDevelopmentGroupID)
                                                .Select(p => new
                                                {
                                                    EvaluationDevelopmentGroupID = p.EvaluationDevelopmentGroupID,
                                                    EvaluationDevelopmentGroupName = p.EvaluationDevelopmentGroupName,
                                                    EvaluationDevelopments = p.EvaluationDevelopments
                                                            .Where(o => o.EducationLevelID == entity.EducationLevelID && o.IsActive == true)
                                                            .Select(o=> new
                                                            {
                                                                EvaluationDevelopmentID = o.EvaluationDevelopmentID,
                                                                EvaluationDevelopmentName = o.EvaluationDevelopmentName
                                                            })
                                                            .OrderBy(o => o.EvaluationDevelopmentName)
                                                })
                                                .OrderBy(g=>g.EvaluationDevelopmentGroupName)
                                                .ToList();

            List<int> lstEvaluationDevelopmentID = new List<int>();
          

            var educationLevel = this.EducationLevelBusiness.Find(entity.EducationLevelID);

            //Lấy sheet mau
            IVTWorksheet templateSheet = oBook.GetSheet(1);

            // Xet tung lop trong danh sach can xuat du lieu
            foreach (var currentClass in lstClass)
            {

                // Tao mot sheet moi cho lop dang xet
                IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet);
                sheet.Name = currentClass.DisplayName;

                #region fill dữ liệu thông tin chung
                string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
                string schoolName = school.SchoolName.ToUpper();
                string provinceName = school.Province.ProvinceName;
                string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                string provinceAndDate = provinceName + ", " + dateTime;
                AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
                string semester = "";
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semester = "Học kỳ I";
                else semester = "Học kỳ II";
                string semesterAndAcademic = semester + " Năm học " + academicYear.Year + "-" + (academicYear.Year + 1);
                string className = currentClass.DisplayName;
                if (!(className.StartsWith("Lớp") || className.StartsWith("Lop")))
                {
                    className = "Lớp " + className;
                }
                int startRowEvaluation = 12;
                int startRowResult = 10;
                int startRowGeneral = 9;
                IVTRange range = sheet.GetRange("A12", "M12");
                IVTRange rangeResult = sheet.GetRange("A10", "F10");

                int index = 0;

                if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
                {
                    sheet.SetCellValue("A2", supervisingDeptName);
                    sheet.SetCellValue("A3", schoolName);
                    sheet.SetCellValue("A5", "Sự phát triển của trẻ " + educationLevel.Resolution);
                    sheet.SetCellValue("A6", className);
                }
                else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
                {
                    sheet.SetCellValue("A2", supervisingDeptName);
                    sheet.SetCellValue("A3", schoolName);
                    sheet.SetCellValue("A5", "Sự phát triển của trẻ " + educationLevel.Resolution);
                    sheet.SetCellValue("A6", "Năm học " + academicYear.Year + "-" + (academicYear.Year + 1));
                    sheet.SetCellValue("A7", "Đánh giá đợt " + entity.Semester);
                }
                #endregion

                #region Lay du lieu
                // Lay danh sach hoc sinh dang hoc cua lop. Danh sach bao gom tat ca cac trang thai
                IEnumerable<PupilOfClass> lstPupilOfClass = currentClass.PupilOfClasses.Where(poc => poc.PupilProfile.IsActive); // bo di cac hoc sinh da bi xoa
                // Bao cao so 3 thi chi lay cac hoc sinh dang hoc
                if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT)
                {
                    lstPupilOfClass = lstPupilOfClass.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING);
                }
                lstPupilOfClass = lstPupilOfClass.ToList().OrderBy(poc => poc.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);
                
                //List to lan lon chua nhieu class va evaid
                List<DevelopmentByClassBO> lstEvaluationOfClass = this.GetListDevelopmentByClass(SchoolID, entity.AcademicYearID, currentClass.ClassProfileID, null, AppliedLevel).ToList();

                //Lấy danh sách trẻ theo nhóm tiêu chí của lớp:
                //For từng phần tử trong lstEvaluationDevelopmentGroup:
                //Select trong lstDevelopmentByClass với EvaluationDevelopmentGroupID = lstEvaluationDevelopmentGroup[i].EvaluationDevelopmentGroupID
                //Trả về các danh sách trẻ theo các nhóm tiêu chí lstPupilByCriteriaGroup
                //List<DevelopmentByClassBO> lstPupilByCriteriaGroup = new List<DevelopmentByClassBO>();
                int[,] countGeneral0 = new int[5, 20];
                int[,] countGeneral1 = new int[5, 20];

                List<int> lstAllEvaluationDevelopmentID = new List<int>();
                //Duyet tung tieu chi mot trong 5 tieu chi fix
                #endregion

                #region Xuat du lieu cho tung hoc sinh
                foreach (var pupil in lstPupilOfClass)
                {
                    
                    index++;
                    List<DevelopmentByClassBO> lstPupilByCriteria = new List<DevelopmentByClassBO>();
                   

                    #region Xuat ho ten hoc sinh
                    if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
                    {
                        if (startRowEvaluation + (index - 1) > 12)
                        {
                            sheet.CopyPasteSameRowHeigh(range, startRowEvaluation);
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(range, startRowEvaluation);
                        }
                        sheet.SetCellValue(startRowEvaluation, 1, index);
                        sheet.SetCellValue(startRowEvaluation, 2, pupil.PupilProfile.FullName);
                        sheet.SetCellValue(startRowEvaluation, 3, pupil.PupilProfile.BirthDate.Year);
                        // Gach ngang neu hoc sinh khong phai dang hoc hoac tot nghiep
                        if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pupil.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            sheet.GetRange(startRowEvaluation, 1, startRowEvaluation, 13).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        else
                        {
                            sheet.GetRange(startRowEvaluation, 1, startRowEvaluation, 13).SetFontStyle(false, System.Drawing.Color.Black, false, null, false, false);
                        }
                    }
                    else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
                    {
                        if (startRowEvaluation + (index - 1) > 10)
                        {
                            sheet.CopyPasteSameRowHeigh(rangeResult, startRowResult);
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(rangeResult, startRowResult);
                        }
                        sheet.SetCellValue(startRowResult, 1, index);
                        sheet.SetCellValue(startRowResult, 2, pupil.PupilProfile.FullName);
                        sheet.SetCellValue(startRowResult, 3, pupil.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
                        // Gach ngang neu hoc sinh khong phai dang hoc hoac tot nghiep
                        if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pupil.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            sheet.GetRange(startRowResult, 1, startRowResult, 6).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        else
                        {
                            sheet.GetRange(startRowResult, 1, startRowResult, 6).SetFontStyle(false, System.Drawing.Color.Black, false, null, false, false);
                        }
                    }
                    #endregion

                    
                    #region Xuat du lieu tung tieu chi
                    int[] lstCount = new int[10];
                    int indexGroup = 0;
                    lstAllEvaluationDevelopmentID = new List<int>();
                    foreach (var evaluationDevelopmentGroup in listEvaluationDevelopmentGroup)
                    {
                        int count1 = 0;
                        int count2 = 0;
                        // Danh sach cac danh gia ma hoc sinh dat duoc
                        List<DevelopmentByClassBO> lstEvaluationPass = new List<DevelopmentByClassBO>();
                        lstEvaluationDevelopmentID = evaluationDevelopmentGroup.EvaluationDevelopments.Select(p=>p.EvaluationDevelopmentID).ToList();
                        foreach (int id in lstEvaluationDevelopmentID)
                        {
                            lstAllEvaluationDevelopmentID.Add(id);
                        }
                        foreach (var evaluation in lstEvaluationOfClass)
                        {
                            if (evaluation.EvaluationDevelopmentGroupID == evaluationDevelopmentGroup.EvaluationDevelopmentGroupID)
                            {
                                //Danh sach hoc sinh hoc theo tung nhom tieu chi
                                lstEvaluationPass.Add(evaluation);
                            }
                        }

                        // Thống kê số tiêu chí đạt của mỗi học sinh dựa vào danh sách trẻ theo các nhóm tiêu chí:
                        //For từng phần tử trong lstPupilOfClass: Select trong các lstPupilByCriteriaGroup với PupilID = lstPupilOfClass[i].PupilID
                        //=> Trả về danh sách (lstPupilByCriteria) gồm học sinh và số tiêu chi đạt của học sinh đó => Số tiêu chí đạt

                        //List chua tung cap (PupilID - EvaID) cua 1 lop
                        foreach (var evaluationPass in lstEvaluationPass)
                        {
                            if (pupil.PupilID == evaluationPass.PupilID)
                            {
                                //List chua so tieu chi dat cua 1 hoc sinh
                                lstPupilByCriteria.Add(evaluationPass);
                                if (evaluationPass.Semester == 1) count1++;
                                else if (evaluationPass.Semester == 2) count2++;
                            }
                        }
                        lstCount[indexGroup] = count1;
                        indexGroup++;
                        lstCount[indexGroup] = count2;
                        indexGroup++;
                    }

                    for (int jj = 0; jj < listEvaluationDevelopmentGroup.Count; jj++)
                    {
                        for (int i = 0; i <= listEvaluationDevelopmentGroup[jj].EvaluationDevelopments.Count(); i++)
                        {
                            //Lan 1
                            if (lstCount[2 * jj + 0] == i)
                            {
                                countGeneral0[jj, i]++;
                            }
                            //Lan 2
                            if (lstCount[2 * jj + 1] == i)
                            {
                                countGeneral1[jj, i]++;
                            }
                        }
                    }
                    // Bao cao so 1
                    if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
                    {
                        if (pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            sheet.SetCellValue(startRowEvaluation, 4, lstCount[0] + "/" + listEvaluationDevelopmentGroup[0].EvaluationDevelopments.Count());//So tieu chi dat cua ngon ngu lan 1
                            sheet.SetCellValue(startRowEvaluation, 5, lstCount[1] + "/" + listEvaluationDevelopmentGroup[0].EvaluationDevelopments.Count());//So tieu chi dat cua ngon ngu lan 2
                            sheet.SetCellValue(startRowEvaluation, 6, lstCount[2] + "/" + listEvaluationDevelopmentGroup[1].EvaluationDevelopments.Count());//So tieu chi dat cua nhan thuc lan 1
                            sheet.SetCellValue(startRowEvaluation, 7, lstCount[3] + "/" + listEvaluationDevelopmentGroup[1].EvaluationDevelopments.Count());//So tieu chi dat cua nhan thuc lan 2
                            sheet.SetCellValue(startRowEvaluation, 8, lstCount[4] + "/" + listEvaluationDevelopmentGroup[2].EvaluationDevelopments.Count());//Tham mi 1
                            sheet.SetCellValue(startRowEvaluation, 9, lstCount[5] + "/" + listEvaluationDevelopmentGroup[2].EvaluationDevelopments.Count());//Tham mi 2
                            sheet.SetCellValue(startRowEvaluation, 10, lstCount[6] + "/" + listEvaluationDevelopmentGroup[3].EvaluationDevelopments.Count());//The chat 1
                            sheet.SetCellValue(startRowEvaluation, 11, lstCount[7] + "/" + listEvaluationDevelopmentGroup[3].EvaluationDevelopments.Count());//The chat 2
                            sheet.SetCellValue(startRowEvaluation, 12, lstCount[8] + "/" + listEvaluationDevelopmentGroup[4].EvaluationDevelopments.Count());//Tinh cam XH 1
                            sheet.SetCellValue(startRowEvaluation, 13, lstCount[9] + "/" + listEvaluationDevelopmentGroup[4].EvaluationDevelopments.Count());//Tinh cam XH 2
                        }
                        else
                        {
                            sheet.SetCellValue(startRowEvaluation, 4, "");//So tieu chi dat cua ngon ngu lan 1
                            sheet.SetCellValue(startRowEvaluation, 5, "");//So tieu chi dat cua ngon ngu lan 2
                            sheet.SetCellValue(startRowEvaluation, 6, "");//So tieu chi dat cua nhan thuc lan 1
                            sheet.SetCellValue(startRowEvaluation, 7, "");//So tieu chi dat cua nhan thuc lan 2
                            sheet.SetCellValue(startRowEvaluation, 8, "");//Tham mi 1
                            sheet.SetCellValue(startRowEvaluation, 9, "");//Tham mi 2
                            sheet.SetCellValue(startRowEvaluation, 10, "");//The chat 1
                            sheet.SetCellValue(startRowEvaluation, 11, "");//The chat 2
                            sheet.SetCellValue(startRowEvaluation, 12, "");//Tinh cam XH 1
                            sheet.SetCellValue(startRowEvaluation, 13, "");//Tinh cam XH 2
                        }
                    }
                    else if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
                    {
                        string chiSoChuaDat = "  ";
                        int tongSoTieuChi = listEvaluationDevelopmentGroup.Sum(p => p.EvaluationDevelopments.Count());
                        if (pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            if (entity.Semester == 1) sheet.SetCellValue(startRowResult, 4, (lstCount[0] + lstCount[2] + lstCount[4] + lstCount[6] + lstCount[8]) + "/" + tongSoTieuChi);
                            if (entity.Semester == 2) sheet.SetCellValue(startRowResult, 4, (lstCount[1] + lstCount[3] + lstCount[5] + lstCount[7] + lstCount[9]) + "/" + tongSoTieuChi);
                        }
                        else
                        {
                            if (entity.Semester == 1) sheet.SetCellValue(startRowResult, 4, "");
                            if (entity.Semester == 2) sheet.SetCellValue(startRowResult, 4, "");
                        }
                        foreach (var item in lstPupilByCriteria)
                        {
                            if (item.Semester == entity.Semester)
                            {
                                if (lstAllEvaluationDevelopmentID.Contains(item.EvaluationDevelopmentID))
                                {
                                    lstAllEvaluationDevelopmentID.Remove(item.EvaluationDevelopmentID);
                                }
                            }
                        }
                        var lstEvaluationDevelopmentChuaDat = this.EvaluationDevelopmentBusiness.All
                            .Where(p => lstAllEvaluationDevelopmentID.Contains(p.EvaluationDevelopmentID))
                            .OrderBy(p => p.EvaluationDevelopmentCode)
                            .Select(p => p.EvaluationDevelopmentName)
                            .ToList();
                        foreach (string name in lstEvaluationDevelopmentChuaDat)
                        {
                            chiSoChuaDat += name + " + ";
                        }
                        if (pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            sheet.SetCellValue(startRowResult, 5, chiSoChuaDat.Substring(0, chiSoChuaDat.Length - 2));
                            //sheet.GetRange(startRowResult, 5, startRowResult, 5).AutoFitRowHeight();
                            sheet.SetCellValue(startRowResult, 6, "");
                        }
                        else
                        {
                            sheet.SetCellValue(startRowResult, 5, "");
                            //sheet.GetRange(startRowResult, 5, startRowResult, 5).AutoFitRowHeight();
                            sheet.SetCellValue(startRowResult, 6, "");
                        }
                    }
                    #endregion
                    startRowEvaluation++;
                    startRowResult++;
                }
                //end 1 hoc sinh
                #endregion

                // Bao caom so 3
                if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT)
                {
                    
                    sheet.SetCellValue("A2", supervisingDeptName);
                    sheet.SetCellValue("A3", schoolName);
                    sheet.SetCellValue("A6", className);
                    IVTRange rangeGroup = sheet.GetRange("A9", "B9");
                    IVTRange rangeRow = sheet.GetRange("C9", "D9");
                    IVTRange rangeGroupLastRow = sheet.GetRange("A200", "B200");
                    IVTRange rangeLastRow = sheet.GetRange("C200", "D200");
                    index = 0;
                    int baseRow = startRowGeneral;
                    int offsetRow = 0;
                    foreach (var evaluationDevelopmentGroup in listEvaluationDevelopmentGroup)
                    {
                        sheet.SetCellValue(baseRow + offsetRow, 1, index + 1);
                        sheet.SetCellValue(baseRow + offsetRow, 2, evaluationDevelopmentGroup.EvaluationDevelopmentGroupName.ToUpper());
                        for (int j = evaluationDevelopmentGroup.EvaluationDevelopments.Count(); j >= 0; j--)
                        {
                            double result = (countGeneral0[index, j] * 1.0 * 100) / lstPupilOfClass.Count();
                            double percent = Math.Round(result, 1, MidpointRounding.AwayFromZero);
                            double result2 = (countGeneral1[index, j] * 1.0 * 100) / lstPupilOfClass.Count();
                            double percent2 = Math.Round(result2, 1, MidpointRounding.AwayFromZero);
                            if (countGeneral0[index, j] > 0)
                            {
                                sheet.SetCellValue(baseRow + offsetRow, 3, "Đạt " + j + "/" + evaluationDevelopmentGroup.EvaluationDevelopments.Count() + ": " + countGeneral0[index, j] + " = " + percent.ToString(culture) + "%");
                            }
                            else
                            {
                                sheet.SetCellValue(baseRow + offsetRow, 3, "Đạt " + j + "/" + evaluationDevelopmentGroup.EvaluationDevelopments.Count() + ": ");
                            }
                            if (countGeneral1[index, j] > 0)
                            {
                                sheet.SetCellValue(baseRow + offsetRow, 4, "Đạt " + j + "/" + evaluationDevelopmentGroup.EvaluationDevelopments.Count() + ": " + countGeneral1[index, j] + " = " + percent2.ToString(culture) + "%");
                            }
                            else
                            {
                                sheet.SetCellValue(baseRow + offsetRow, 4, "Đạt " + j + "/" + evaluationDevelopmentGroup.EvaluationDevelopments.Count() + ": ");
                            }
                            offsetRow++;
                        }
                        // Fomat cell
                        // Cot STT
                        sheet.MergeColumn(1, baseRow, baseRow + offsetRow - 1);
                        sheet.GetRange(baseRow, 1, baseRow + offsetRow - 1, 1).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                        // Cot Nhom tieu chi
                        sheet.MergeColumn(2, baseRow, baseRow + offsetRow - 1);
                        sheet.GetRange(baseRow, 2, baseRow + offsetRow - 1, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                        // Cac cot tieu chi
                        sheet.GetRange(baseRow, 3, baseRow + offsetRow - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                        sheet.GetRange(baseRow, 3, baseRow + offsetRow - 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderIndex.InsideAll);
                        
                        // Chinh lai cac chi so
                        baseRow = baseRow + offsetRow;
                        offsetRow = 0;
                        index++;
                    }
                    baseRow += 2;
                    offsetRow = 0;
                    // Nguoi lap bao cao
                    sheet.MergeRow(baseRow + offsetRow, 3, 4);
                    sheet.SetCellValue(baseRow + offsetRow, 3, "Người lập báo cáo");
                    sheet.GetRange(baseRow + offsetRow, 3, baseRow + offsetRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                }

                if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT)
                {
                    IVTRange newRange = sheet.GetRange("A150", "M150");
                    sheet.CopyPasteSameRowHeigh(newRange, startRowEvaluation + 2);
                    sheet.MergeRow(startRowEvaluation + 2, 10, 13);
                    sheet.SetCellValue(startRowEvaluation + 2, 10, "Người lập báo cáo");
                    sheet.GetRange(startRowEvaluation + 2, 10, startRowEvaluation + 2, 10).SetHAlign(VTHAlign.xlHAlignCenter);
                }

                if (ReportType == SystemParamsInFile.CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT)
                {
                    IVTRange newRange = sheet.GetRange("A150", "M150");
                    sheet.CopyPasteSameRowHeigh(newRange, startRowResult + 2);
                    sheet.MergeRow(startRowResult + 2, 4, 6);
                    sheet.SetCellValue(startRowResult + 2, 4, provinceAndDate);
                    sheet.GetRange(startRowResult + 2, 4, startRowResult + 2, 6).SetHAlign(VTHAlign.xlHAlignCenter);

                    sheet.CopyPasteSameRowHeigh(newRange, startRowResult + 3);
                    sheet.MergeRow(startRowResult + 3, 2, 3);
                    sheet.SetCellValue(startRowResult + 3, 2, "GIÁO VIÊN CHỦ NHIỆM");
                    sheet.GetRange(startRowResult + 3, 2, startRowResult + 3, 3).SetHAlign(VTHAlign.xlHAlignCenter);

                    sheet.MergeRow(startRowResult + 3, 4, 6);
                    sheet.SetCellValue(startRowResult + 3, 4, schoolName);
                    sheet.GetRange(startRowResult + 3, 4, startRowResult + 3, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                }
                sheet.DeleteRow(150);
                sheet.DeleteRow(200);
            }
            // Xoa sheet mau
            templateSheet.Delete();
            return oBook.ToStream();
        }
    }
}