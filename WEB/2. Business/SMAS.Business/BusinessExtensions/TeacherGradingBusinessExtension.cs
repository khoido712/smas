﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  AuNH
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class TeacherGradingBusiness
    {
        /// <summary>
        /// Validate dữ liệu trước khi thao tác với db
        /// </summary>
        /// <param name="Entity"></param>
        private void Validate(TeacherGrading Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = Entity.TeacherID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra trạng thái giáo viên phải đang làm việc
            Employee Teacher = Entity.Employee;
            if (Teacher == null)
            {
                Teacher = this.EmployeeBusiness.Find(Entity.TeacherID);
            }
            
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["TeacherID"] = Entity.TeacherID;
            IQueryable<TeacherGrading> lstTeacherGrading = TeacherGradingBusiness.SearchBySchool(Entity.SchoolID, SearchInfo).Where(o => o.TeacherGradingID != Entity.TeacherGradingID);
            if (lstTeacherGrading.Count() > 0)
            {
                throw new BusinessException("ConcurrentWorkAssignment_Validate_Duplicate");
            }
        }

        ///// <summary>
        ///// Thêm mới dữ liệu
        ///// </summary>
        ///// <param name="TeacherGrading"></param>
        ///// <returns></returns>
        //public override TeacherGrading Insert(TeacherGrading TeacherGrading)
        //{
        //    this.Validate(TeacherGrading);
        //    Employee Teacher = TeacherGrading.Employee;
        //    if (Teacher == null)
        //    {
        //        Teacher = this.EmployeeBusiness.Find(TeacherGrading.TeacherID);
        //    }
        //    if (Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING && Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_BREATHER)
        //    {
        //        throw new BusinessException("ConcurrentWorkAssignment_Validate_TeacherNotWorking");
        //    }
        //    TeacherGrading.GradingDate = DateTime.Now;
        //    TeacherGrading.SchoolFacultyID = this.EmployeeBusiness.Find(TeacherGrading.TeacherID).SchoolFacultyID;
            
        //    base.Insert(TeacherGrading);
        //    return TeacherGrading;
        //}

               public void InsertOrUpdateTeacherGrading(List<TeacherGrading> lstTeacherGrading,IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            List<int> lstTeacherID = Utils.GetIntList(dic, "lstTeacherID");
            //lay danh sach danh gia giao vien
            List<TeacherGrading> lstTeacherGradingDB = (from tg in TeacherGradingBusiness.All
                                                        where tg.SchoolID == SchoolID
                                                        && tg.AcademicYearID == AcademicYearID
                                                        && (tg.SchoolFacultyID == FacultyID || FacultyID == 0)
                                                        && lstTeacherID.Contains(tg.TeacherID)
                                                        select tg).ToList();
            TeacherGrading objDB = null; 

            List<TeacherGrading> lstInsert = new List<TeacherGrading>();
            List<TeacherGrading> lstUpdate = new List<TeacherGrading>();
            TeacherGrading objtmp = null;
            for (int i = 0; i < lstTeacherGrading.Count; i++)
            {
                objtmp = lstTeacherGrading[i];
                objDB = lstTeacherGradingDB.Where(p => p.TeacherID == objtmp.TeacherID).FirstOrDefault();
                if (objDB != null)
                {
                    objDB.TeacherGradeID = objtmp.TeacherGradeID;
                    objDB.Description = objtmp.Description;
                    objDB.GradingDate = DateTime.Now.Date;
                    lstUpdate.Add(objDB);
                }
                else
                {
                    objtmp.GradingDate = DateTime.Now.Date;
                    lstInsert.Add(objtmp);
                }
            }

            if (lstInsert.Count > 0)
            {
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    this.Insert(lstInsert[i]);
                }
            }

            if (lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
            }

            this.Save();
        }

        ///// <summary>
        ///// Cập nhật dữ liệu
        ///// </summary>
        ///// <param name="Entity"></param>
        ///// <returns></returns>
        //public override TeacherGrading Update(TeacherGrading Entity)
        //{
        //    this.Validate(Entity);

        //    // Kiểm tra tính hợp lệ dữ liệu
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
        //    SearchInfo["SchoolID"] = Entity.SchoolID;
        //    SearchInfo["TeacherGradingID"] = Entity.TeacherGradingID;
        //    bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeacherGrading", SearchInfo);
        //    if (!compatible)
        //    {
        //        throw new BusinessException("Common_Validate_NotCompatible");
        //    }

        //    // Kiểm tra tính hợp lệ dữ liệu
        //    SearchInfo = new Dictionary<string, object>();
        //    SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
        //    SearchInfo["TeacherGradingID"] = Entity.TeacherGradingID;
        //    compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeacherGrading", SearchInfo);
        //    if (!compatible)
        //    {
        //        throw new BusinessException("Common_Validate_NotCompatible");
        //    }

        //    Entity.SchoolFacultyID = this.EmployeeBusiness.Find(Entity.TeacherID).SchoolFacultyID;

        //    base.Update(Entity);
        //    return Entity;
        //}

        /// <summary>
        /// Xóa dữ liệu
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="TeacherGradingID"></param>
        public void Delete(int SchoolID, int TeacherGradingID)
        {
            this.CheckAvailable(TeacherGradingID, "TeacherGrading_Label_TeacherGradingID");

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["TeacherGradingID"] = TeacherGradingID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeacherGrading", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            base.Delete(TeacherGradingID);
        }
        /// <summary>
        /// Tìm kiếm theo nhiều tiêu chí
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<TeacherGrading> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolFacultyID = Utils.GetInt(SearchInfo, "SchoolFacultyID");
            int TeacherID = Utils.GetInt(SearchInfo, "TeacherID");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int TeacherGradeID = Utils.GetInt(SearchInfo, "TeacherGradeID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            List<int> lstTeacherGradingID = Utils.GetIntList(SearchInfo, "lstTeacherGradingID");


            IQueryable<TeacherGrading> Query = this.All;

            if (SchoolFacultyID > 0)
            {
                Query = Query.Where(o => o.SchoolFacultyID == SchoolFacultyID);
            }

            if (TeacherID > 0)
            {
                Query = Query.Where(o => o.TeacherID == TeacherID);
            }

            if (FullName != "")
            {
                Query = Query.Where(o => o.Employee.FullName.ToLower().Contains(FullName.ToLower()));
            }

            if (SchoolID > 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }

            if (TeacherGradeID > 0)
            {
                Query = Query.Where(o => o.TeacherGradeID == TeacherGradeID);
            }

            if (AcademicYearID > 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (lstTeacherGradingID.Count > 0)
            {
                Query = Query.Where(p => lstTeacherGradingID.Contains(p.TeacherGradingID));
            }

            return Query;
        }

        /// <summary>
        /// Tìm kiếm theo trường học
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<TeacherGrading> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return this.Search(SearchInfo);
            }
        }
    }
}