/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SchoolPropertyCatBusiness
    {
        /// <summary>
        /// Tim kiem tinh chat truong hoc
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<SchoolPropertyCat> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            bool? IsActive = Utils.GetIsActive(dicParam, "IsActive");
            IQueryable<SchoolPropertyCat> listSchoolPropertyCat = this.SchoolPropertyCatRepository.All;
            if (!Resolution.Equals(string.Empty))
            {
                listSchoolPropertyCat = listSchoolPropertyCat.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listSchoolPropertyCat = listSchoolPropertyCat.Where(x => x.IsActive == true);
            }
            int count = listSchoolPropertyCat != null ? listSchoolPropertyCat.Count() : 0;
            if (count == 0)
            {
                return null;
            }
            return listSchoolPropertyCat;

        }
        
    }
}