﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @Tamhm1 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class TrainingLevelBusiness
    {
       
            #region private member variable
            private const int RESOLUTION_MAX_LENGTH = 200;   //do dai lon nhat truong ten
            private const int DESCRIPTION_MAX_LENGTH = 300; //do dai lon nhat truong mieu ta
            #endregion



            #region insert
            /// <summary>
            /// Thêm mới Trình độ đào tạo
            /// <author>tamhm1</author>
            /// <date>28/12/2012</date>
            /// </summary>
            /// <param name="insertTrainingLevel">Đối tượng Insert</param>
            /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
            public override TrainingLevel Insert(TrainingLevel insertTrainingLevel)
            {


                //resolution rong
                Utils.ValidateRequire(insertTrainingLevel.Resolution, "TrainingLevel_Label_Resolution");

                Utils.ValidateMaxLength(insertTrainingLevel.Resolution, RESOLUTION_MAX_LENGTH, "TrainingLevel_Label_Resolution");
                Utils.ValidateMaxLength(insertTrainingLevel.Description, DESCRIPTION_MAX_LENGTH, "TrainingLevel_Column_Description");

                //kiem tra ton tai - theo cap - can lam lai
                this.CheckDuplicate(insertTrainingLevel.Resolution, GlobalConstants.LIST_SCHEMA, "TrainingLevel", "Resolution", true, 0, "TrainingLevel_Label_Resolution");
                //this.CheckDuplicate(insertTrainingLevel.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "TrainingLevel", "AppliedLevel", true, 0, "TrainingLevel_Label_ShoolID");
                // this.CheckDuplicateCouple(insertTrainingLevel.Resolution, insertTrainingLevel.SchoolID.ToString(),
                //GlobalConstants.LIST_SCHEMA, "TrainingLevel", "Resolution", "SchoolID", true, 0, "TrainingLevel_Label_Resolution");
                //kiem tra range



                insertTrainingLevel.IsActive = true;

                return base.Insert(insertTrainingLevel);
            }
            #endregion

            #region update
            /// <summary>
            /// Cập nhật Trình độ đào tạo
            /// <author>tamhm1</author>
            /// <date>28/12/2012</date>
            /// </summary>
            /// <param name="updateTrainingLevel">Đối tượng Update</param>
            /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
            public override TrainingLevel Update(TrainingLevel updateTrainingLevel)
            {

                //check avai
                new TrainingLevelBusiness(null).CheckAvailable(updateTrainingLevel.TrainingLevelID, "TrainingLevel_Label_TrainingLevelID");

                //resolution rong
                Utils.ValidateRequire(updateTrainingLevel.Resolution, "TrainingLevel_Label_Resolution");

                Utils.ValidateMaxLength(updateTrainingLevel.Resolution, RESOLUTION_MAX_LENGTH, "TrainingLevel_Label_Resolution");
                Utils.ValidateMaxLength(updateTrainingLevel.Description, DESCRIPTION_MAX_LENGTH, "TrainingLevel_Column_Description");

                //kiem tra ton tai - theo cap - can lam lai
                this.CheckDuplicate(updateTrainingLevel.Resolution, GlobalConstants.LIST_SCHEMA, "TrainingLevel", "Resolution", true, updateTrainingLevel.TrainingLevelID, "TrainingLevel_Label_Resolution");

                //this.CheckDuplicateCouple(updateTrainingLevel.Resolution, updateTrainingLevel.SchoolID.ToString(),
                //    GlobalConstants.LIST_SCHEMA, "TrainingLevel", "Resolution", "SchoolID", true, updateTrainingLevel.TrainingLevelID, "TrainingLevel_Label_Resolution");

                // this.CheckDuplicate(updateTrainingLevel.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "TrainingLevel", "AppliedLevel", true, updateTrainingLevel.TrainingLevelID, "TrainingLevel_Label_ShoolID");



                //Loi pham quy che thi khong thuoc truong truyen vao
                //tu TrainingLevelID ->vao csdl lay TrainingLevel tuong ung roi 
                //so sanh voi updateTrainingLevel.SchoolID


                //.................



                updateTrainingLevel.IsActive = true;

                return base.Update(updateTrainingLevel);
            }
            #endregion

            #region Delete
            /// <summary>
            /// Xóa Trình độ đào tạo
            /// <author>tamhm1</author>
            /// <date>28/12/2012</date>
            /// </summary>
            /// <param name="TrainingLevelId">ID Trình độ đào tạo</param>
            public void Delete(int TrainingLevelId)
            {
                //check avai
                new TrainingLevelBusiness(null).CheckAvailable(TrainingLevelId, "TrainingLevel_Label_TrainingLevelID");

                //da su dung hay chua
                this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "TrainingLevel", TrainingLevelId, "TrainingLevel_Label_TrainingLevelID");

                base.Delete(TrainingLevelId, true);


            }
            #endregion

            #region Search
            /// <summary>
            /// Tìm kiếm Trình độ đào tạo
            /// <author>tamhm1</author>
            /// <date>28/12/2012</date>
            /// </summary>
            /// <param name="dic">tham so doi tuong tim kiem</param>
            /// <returns>Danh sách kết quả tìm kiếm</returns>
            public IQueryable<TrainingLevel> Search(IDictionary<string, object> dic)
            {
                string resolution = Utils.GetString(dic, "Resolution");
                string description = Utils.GetString(dic, "Description");

                bool? isActive = Utils.GetIsActive(dic, "IsActive");
                IQueryable<TrainingLevel> lsTrainingLevel = this.TrainingLevelRepository.All.OrderBy(o => o.Resolution);

                if (isActive.HasValue)
                {
                    lsTrainingLevel = lsTrainingLevel.Where(em => (em.IsActive == isActive));
                }

                if (resolution.Trim().Length != 0)
                {

                    lsTrainingLevel = lsTrainingLevel.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
                }
                if (description.Trim().Length != 0)
                {

                    lsTrainingLevel = lsTrainingLevel.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
                }


                return lsTrainingLevel;
            }
            #endregion

        }
        
    }
