﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ApprenticeshipClassBusiness
    {
        public ApprenticeshipClass InsertApprenticeshipClass(ApprenticeshipClass entityToInsert)
        {
            //Validate properties
            ValidationMetadata.ValidateObject(entityToInsert);

            //Check Foreign key Employee
            //this.EmployeeBusiness.CheckAvailable(entityToInsert.HeadTeacherID, "ApprenticeshipClass_Label_HeadTeacherID");

            //Check Foreign key ApprenticeshipSubject
            //this.ApprenticeshipSubjectBusiness.CheckAvailable(entityToInsert.ApprenticeshipSubjectID, "ApprenticeshipClass_Label_ApprenticeshipSubjectID");

            //Check Foreign key SchoolProfile
            //this.SchoolProfileBusiness.CheckAvailable(entityToInsert.SchoolID, "ApprenticeshipClass_Label_SchoolID");

            //Check Foreign key Academic Year 
            //this.AcademicYearBusiness.CheckAvailable(entityToInsert.AcademicYearID, "ApprenticeshipClass_Label_AcademicYear");

            //Check SchoolID, AcademicYearID compatible in AcademicYear
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear"
                                            , new Dictionary<string, object>()
                                            {
                                                {"AcademicYearID" , entityToInsert.AcademicYearID},
                                                {"SchoolID", entityToInsert.SchoolID}
                                            }, null);
            if (!AcademicYearCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Check HeadTeacherID, SchoolID compatible in Employee
            bool EmployeeCompatible = new EmployeeRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee"
                                            , new Dictionary<string, object>()
                                            {
                                                {"HeadTeacherID" , entityToInsert.HeadTeacherID},
                                                {"SchoolID", entityToInsert.SchoolID}
                                            }, null);
            if (!EmployeeCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            return base.Insert(entityToInsert);
        }

        public ApprenticeshipClass UpdateApprenticeshipClass(ApprenticeshipClass entityToUpdate)
        {
            //Check Primary key
            this.CheckAvailable(entityToUpdate.ApprenticeshipClassID, "ApprenticeshipClass_Label_ApprenticeshipClassID");

            //Validate properties
            ValidationMetadata.ValidateObject(entityToUpdate);

            //Check Foreign key Employee
            //this.EmployeeBusiness.CheckAvailable(entityToUpdate.HeadTeacherID, "ApprenticeshipClass_Label_HeadTeacherID");

            //Check Foreign key ApprenticeshipSubject
            //this.ApprenticeshipSubjectBusiness.CheckAvailable(entityToUpdate.ApprenticeshipSubjectID, "ApprenticeshipClass_Label_ApprenticeshipSubjectID");

            //Check Foreign key SchoolProfile
            //this.SchoolProfileBusiness.CheckAvailable(entityToUpdate.SchoolID, "ApprenticeshipClass_Label_SchoolID");

            //Check Foreign key Academic Year 
            //this.AcademicYearBusiness.CheckAvailable(entityToUpdate.AcademicYearID, "ApprenticeshipClass_Label_AcademicYear");

            //Check SchoolID, AcademicYearID compatible in AcademicYear
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear"
                                            , new Dictionary<string, object>()
                                            {
                                                {"AcademicYearID" , entityToUpdate.AcademicYearID},
                                                {"SchoolID", entityToUpdate.SchoolID}
                                            }, null);
            if (!AcademicYearCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Check HeadTeacherID, SchoolID compatible in Employee
            bool EmployeeCompatible = new EmployeeRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee"
                                            , new Dictionary<string, object>()
                                            {
                                                {"HeadTeacherID" , entityToUpdate.HeadTeacherID},
                                                {"SchoolID", entityToUpdate.SchoolID}
                                            }, null);
            if (!EmployeeCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            return base.Update(entityToUpdate);
        }

        public void DeleteApprenticeshipClass(int ApprenticeshipClassID, int SchoolID)
        {
            //Check Primary key
            var apprenticeshipClass = this.Find(ApprenticeshipClassID);
            Utils.ValidateNull(apprenticeshipClass, "ApprenticeshipClass_Label_ApprenticeshipClassID");
            this.CheckConstraints(GlobalConstants.PUPIL_SCHEMA, "ApprenticeshipClass", ApprenticeshipClassID, "ApprenticeshipClass_Label_ApprenticeshipClassID");
            //Check compatible ApprenticeshipClassID, SchoolID in ApprenticeshipClass
            bool ApprenticeshipClassCompatible = new ApprenticeshipClassRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ApprenticeshipClass"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ApprenticeshipClassID" , apprenticeshipClass.ApprenticeshipClassID},
                                                {"SchoolID", apprenticeshipClass.SchoolID}
                                            }, null);
            if (!ApprenticeshipClassCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (!this.AcademicYearBusiness.IsCurrentYear(apprenticeshipClass.AcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            base.Delete(ApprenticeshipClassID);
        }

        private IQueryable<ApprenticeshipClass> Search(IDictionary<string, object> dic)
        {
            int? ApprenticeshipClassID = Utils.GetNullableInt(dic, "ApprenticeshipClassID"); 
            int? ApprenticeshipSubjectID =  Utils.GetNullableInt(dic, "ApprenticeshipSubjectID");
            int? HeadTeacherID = Utils.GetNullableInt(dic, "HeadTeacherID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? Semester = Utils.GetNullableInt(dic, "Semester");
            int? AcademicYearID = Utils.GetNullableInt(dic, "AcademicYearID");
            int? IsCommenting = Utils.GetNullableInt(dic, "IsCommenting");
            int? SubjectType = Utils.GetNullableInt(dic, "SubjectType");

            var lstApprenticeshipClass = this.ApprenticeshipClassRepository.All.Where(u => u.ApprenticeshipSubject.SubjectCat.IsActive);

            if (ApprenticeshipClassID.HasValue && ApprenticeshipClassID.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.ApprenticeshipClassID == ApprenticeshipClassID);

            if (ApprenticeshipSubjectID.HasValue && ApprenticeshipSubjectID.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.ApprenticeshipSubject.SubjectID == ApprenticeshipSubjectID);

            if (HeadTeacherID.HasValue && HeadTeacherID.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.HeadTeacherID == HeadTeacherID);

            if (SchoolID.HasValue && SchoolID.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.SchoolID == SchoolID);

            if (Semester.HasValue && Semester.Value >= 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.Semester == Semester || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);

            if (AcademicYearID.HasValue && AcademicYearID.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.AcademicYearID == AcademicYearID);

            if (IsCommenting.HasValue && IsCommenting.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.IsCommenting == IsCommenting);

            if (SubjectType.HasValue && SubjectType.Value > 0)
                lstApprenticeshipClass = lstApprenticeshipClass.Where(u => u.SubjectType == SubjectType);
            return lstApprenticeshipClass;
        }

        public IQueryable<ApprenticeshipClass> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID <= 0) return null;
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
    }
}