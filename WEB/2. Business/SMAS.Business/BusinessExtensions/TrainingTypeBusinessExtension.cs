﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author dungnt 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Loại hình đào tạo
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class TrainingTypeBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion



        #region insert
        /// <summary>
        /// Thêm mới Loại hình đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertTrainingType">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override TrainingType Insert(TrainingType insertTrainingType)
        {


            //resolution rong
            //Utils.ValidateRequire(insertTrainingType.Resolution, "TrainingType_Label_Resolution");

            //Utils.ValidateMaxLength(insertTrainingType.Resolution, RESOLUTION_MAX_LENGTH, "TrainingType_Label_Resolution");
            ValidationMetadata.ValidateObject(insertTrainingType);
            

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertTrainingType.Resolution, GlobalConstants.LIST_SCHEMA, "TrainingType", "Resolution", true, 0, "TrainingType_Label_Resolution");
            //this.CheckDuplicate(insertTrainingType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "TrainingType", "AppliedLevel", true, 0, "TrainingType_Label_ShoolID");
           // this.CheckDuplicateCouple(insertTrainingType.Resolution, insertTrainingType.SchoolID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "TrainingType", "Resolution", "SchoolID", true, 0, "TrainingType_Label_Resolution");
            //kiem tra range

           

            insertTrainingType.IsActive = true;

          return  base.Insert(insertTrainingType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Loại hình đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateTrainingType">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override TrainingType Update(TrainingType updateTrainingType)
        {

            //check avai
            new TrainingTypeBusiness(null).CheckAvailable(updateTrainingType.TrainingTypeID, "TrainingType_Label_TrainingTypeID");

            //resolution rong
            //Utils.ValidateRequire(updateTrainingType.Resolution, "TrainingType_Label_Resolution");

            //Utils.ValidateMaxLength(updateTrainingType.Resolution, RESOLUTION_MAX_LENGTH, "TrainingType_Label_Resolution");

            ValidationMetadata.ValidateObject(updateTrainingType);
            //kiem tra ton tai - theo cap - can lam lai
             this.CheckDuplicate(updateTrainingType.Resolution, GlobalConstants.LIST_SCHEMA, "TrainingType", "Resolution", true, updateTrainingType.TrainingTypeID, "TrainingType_Label_Resolution");

            //this.CheckDuplicateCouple(updateTrainingType.Resolution, updateTrainingType.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "TrainingType", "Resolution", "SchoolID", true, updateTrainingType.TrainingTypeID, "TrainingType_Label_Resolution");

            // this.CheckDuplicate(updateTrainingType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "TrainingType", "AppliedLevel", true, updateTrainingType.TrainingTypeID, "TrainingType_Label_ShoolID");

          

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu TrainingTypeID ->vao csdl lay TrainingType tuong ung roi 
            //so sanh voi updateTrainingType.SchoolID


            //.................



            updateTrainingType.IsActive = true;

          return  base.Update(updateTrainingType);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Loại hình đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="TrainingTypeId">ID Loại hình đào tạo</param>
        public  void Delete(int TrainingTypeId)
        {
            //check avai
            new TrainingTypeBusiness(null).CheckAvailable(TrainingTypeId, "TrainingType_Label_TrainingTypeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "TrainingType", TrainingTypeId, "TrainingType_Label_TrainingTypeIDFailed");

            base.Delete(TrainingTypeId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Loại hình đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">cac thuoc tinh can tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<TrainingType> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
         
            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<TrainingType> lsTrainingType = this.TrainingTypeRepository.All.OrderBy(o => o.Resolution);

          
   

             if (isActive.HasValue)
            {
            lsTrainingType = lsTrainingType.Where(em => (em.IsActive == isActive));
            }
            if (resolution.Trim().Length != 0)
            {

                lsTrainingType = lsTrainingType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            return lsTrainingType;
        }
        #endregion
        
    }
}