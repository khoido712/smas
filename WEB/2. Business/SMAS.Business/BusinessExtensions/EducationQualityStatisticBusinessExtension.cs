﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SMAS.Business.Business
{
    public partial class EducationQualityStatisticBusiness
    {
        #region Report
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetEducationQualityStatisticReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC_NEW;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateEducationQualityStatisticReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC_NEW;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lay ma dan toc Kinh va nguoi nuoc ngoai           
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = GlobalConstants.APPLIED_LEVEL_PRIMARY;
            //Lay danh sach hoc sinh dang hoc hoac da tot nghiep trong hoc ky thong ke
            IQueryable<PupilOfClass> iqPOC = PupilOfClassBusiness.SearchBySchool(schoolID, tmpDic);
            List<PupilOfClass> listPupilOfClassInSemester = iqPOC.AddCriteriaSemester(academicYear, semester).ToList();

            #region Fill tieu de
            //string strSemester = semester == 1 ? "HỌC KÌ I" : "HỌC KÌ II";
            //string schoolName = school.SchoolName;
            string strAcademicYear = academicYear.DisplayTitle;

            firstSheet.SetCellValue("C3", "     Cuối năm học:   " + strAcademicYear);
            firstSheet.SetCellValue("A4", "số: 5363/QĐ-BGDĐT ngày 14/11/2013                       Loại hình(1):         " + school.TrainingType.Resolution);
            firstSheet.SetCellValue("A6", "ngày nhận báo cáo: 30/6 năm báo cáo                                        Đơn vị báo cáo:  " + school.SchoolName);
            //firstSheet.SetCellValue("L4", strAcademicYear);
            //firstSheet.SetCellValue("L5", schoolName);
            #endregion

            #region Ket qua hoc tap
            //Danh sach mon hoc
            List<SubjectCat> listSubjectCat = SubjectCatBusiness.All.Where(o => o.IsActive && o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY).ToList();
            int ID_TIENGVIET = listSubjectCat.Where(o => o.Abbreviation == "TV").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_TOAN = listSubjectCat.Where(o => o.Abbreviation == "Tt").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_DAODUC = listSubjectCat.Where(o => o.Abbreviation == "DD").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_TNXH = listSubjectCat.Where(o => o.Abbreviation == "TNXH").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_KHOAHOC = listSubjectCat.Where(o => o.Abbreviation == "KH").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_LSDL = listSubjectCat.Where(o => o.Abbreviation == "LSDL").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_AMNHAC = listSubjectCat.Where(o => o.Abbreviation == "AM").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_MITHUAT = listSubjectCat.Where(o => o.Abbreviation == "MT").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_TC = listSubjectCat.Where(o => o.Abbreviation == "TC").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_KT = listSubjectCat.Where(o => o.Abbreviation == "Kt").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_THEDUC = listSubjectCat.Where(o => o.Abbreviation == "TD").Select(o => o.SubjectCatID).FirstOrDefault();
            List<int> ID_NGOAINGU = listSubjectCat.Where(o => o.IsForeignLanguage && o.Abbreviation != "TDT").Select(o => o.SubjectCatID).ToList();
            int ID_TINHOC = listSubjectCat.Where(o => o.Abbreviation == "THO").Select(o => o.SubjectCatID).FirstOrDefault();
            int ID_TIENGDT = listSubjectCat.Where(o => o.Abbreviation == "TDT").Select(o => o.SubjectCatID).FirstOrDefault();

            List<object> listSubjectID = new List<object>() {
                ID_TOAN,
                ID_TIENGVIET, 
                ID_KHOAHOC,
                ID_LSDL,
                ID_NGOAINGU,
                ID_TIENGDT,
                ID_TINHOC, 
                ID_DAODUC,
                ID_TNXH,                
                ID_AMNHAC,
                ID_MITHUAT,
                ID_TC,
                ID_KT,
                ID_THEDUC,                                             
            };

            //Lay ra danh sach ket qua hoc tap cua toan truong trong hoc ky
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["Semester"] = semester;
            tmpDic["SemesterID"] = semester;
            List<RatedCommentEQReportBO> listRatedCommentPupil;
            if (UtilsBusiness.IsMoveHistory(academicYear))
            {
                listRatedCommentPupil = (from r in RatedCommentPupilHistoryBusiness.Search(tmpDic)
                                         join pp in PupilProfileBusiness.All on r.PupilID equals pp.PupilProfileID
                                         join cp in ClassProfileBusiness.All on r.ClassID equals cp.ClassProfileID
                                         where pp.IsActive && (cp.IsActive == null || (cp.IsActive.HasValue && cp.IsActive.Value))
                                         select new RatedCommentEQReportBO
                                             {
                                                 Genre = pp.Genre,
                                                 EthnicID = pp.EthnicID,
                                                 IsCombinedClass = cp.IsCombinedClass,
                                                 IsDisabled = pp.IsDisabled,
                                                 SemesterID = r.SemesterID,
                                                 EvaluationID = r.EvaluationID,
                                                 EvaluationCriteriaID = r.SubjectID,
                                                 ClassID = r.ClassID,
                                                 PupilID = r.PupilID,
                                                 EducationLevelID = cp.EducationLevelID,
                                                 PeriodicEndingMark = r.PeriodicEndingMark,
                                                 EndingEvaluation = r.EndingEvaluation,
                                                 CapacityEndingEvaluation = r.CapacityEndingEvaluation,
                                                 QualityEndingEvaluation = r.QualityEndingEvaluation,
                                                 SubjectID = r.SubjectID
                                             }).ToList();
            }
            else
            {
                listRatedCommentPupil = (from r in RatedCommentPupilBusiness.Search(tmpDic)
                                         join pp in PupilProfileBusiness.All on r.PupilID equals pp.PupilProfileID
                                         join cp in ClassProfileBusiness.All on r.ClassID equals cp.ClassProfileID
                                         where pp.IsActive && (cp.IsActive == null || (cp.IsActive.HasValue && cp.IsActive.Value))
                                         select new RatedCommentEQReportBO
                                         {
                                             Genre = pp.Genre,
                                             EthnicID = pp.EthnicID,
                                             IsCombinedClass = cp.IsCombinedClass,
                                             IsDisabled = pp.IsDisabled,
                                             SemesterID = r.SemesterID,
                                             EvaluationID = r.EvaluationID,
                                             EvaluationCriteriaID = r.SubjectID,
                                             ClassID = r.ClassID,
                                             PupilID = r.PupilID,
                                             EducationLevelID = cp.EducationLevelID,
                                             PeriodicEndingMark = r.PeriodicEndingMark,
                                             EndingEvaluation = r.EndingEvaluation,
                                             CapacityEndingEvaluation = r.CapacityEndingEvaluation,
                                             QualityEndingEvaluation = r.QualityEndingEvaluation,
                                             SubjectID = r.SubjectID
                                         }).ToList();
            }

            //Loc lai chi lay du lieu thuoc cac lop hoc cac mon duoc danh gia
            tmpDic = new Dictionary<string, object>();
            //tmpDic["SchoolID"] = schoolID;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = GlobalConstants.APPLIED_LEVEL_PRIMARY;
            tmpDic["Semester"] = semester;

            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            listRatedCommentPupil = (from se in listRatedCommentPupil
                                     where listPupilOfClassInSemester.Any(poc => poc.ClassID == se.ClassID && poc.PupilID == se.PupilID)
                                     select se).ToList();
            List<RatedCommentEQReportBO> listRatedCommentPupilEducation = listRatedCommentPupil.Where(o => listClassSubject.Any(cs => cs.ClassID == o.ClassID && cs.SubjectID == o.EvaluationCriteriaID)
                && o.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP && o.EndingEvaluation.HasValue).ToList();
            // Fill du lieu
            int startColumn = 3;
            int startRow = 17;
            int curRow = startRow;
            int curColumn = startColumn;

            for (int iSubject = 0; iSubject < listSubjectID.Count; iSubject++)
            {
                object subjectID = listSubjectID[iSubject];
                //Lay ra danh sach ket qua hoc tap cua mon nay
                List<RatedCommentEQReportBO> listRatedCommentOfSubject;
                if (subjectID.GetType() == typeof(int))
                {
                    listRatedCommentOfSubject = listRatedCommentPupilEducation.Where(o => o.EvaluationCriteriaID == (int)subjectID).ToList();
                }
                else
                {
                    listRatedCommentOfSubject = listRatedCommentPupilEducation.Where(o => ((List<int>)subjectID).Contains(o.EvaluationCriteriaID)).ToList();
                }


                for (int iEdu = 1; iEdu <= 5; iEdu++)
                {
                    List<RatedCommentEQReportBO> goodDoneList = listRatedCommentOfSubject.Where(o => o.EducationLevelID == iEdu).Where(o => o.EndingEvaluation == GlobalConstants.EVALUATION_GOOD_COMPLETE_VAL).ToList();
                    List<RatedCommentEQReportBO> doneList = listRatedCommentOfSubject.Where(o => o.EducationLevelID == iEdu).Where(o => o.EndingEvaluation == GlobalConstants.EVALUATION_CAPQUA_COMPLETE_VAL).ToList();
                    List<RatedCommentEQReportBO> unDoneList = listRatedCommentOfSubject.Where(o => o.EducationLevelID == iEdu).Where(o => o.EndingEvaluation == GlobalConstants.EVALUATION_NOT_COMPLETE_VAL).ToList();

                    //Tong so
                    var b = firstSheet.GetCellValue(curRow, curColumn);
                    if (b == null)
                        firstSheet.SetCellValue(curRow, curColumn, goodDoneList.Count);
                    b = firstSheet.GetCellValue(curRow + 1, curColumn);
                    if (b == null)
                        firstSheet.SetCellValue(curRow + 1, curColumn, doneList.Count);
                    b = firstSheet.GetCellValue(curRow + 2, curColumn);
                    if (b == null)
                        firstSheet.SetCellValue(curRow + 2, curColumn, unDoneList.Count);

                    curColumn++;
                }

                curRow = curRow + 4;
                curColumn = startColumn;
            }

            #endregion

            #region Nang luc, Pham chat
            //Nang luc
            List<int> lstCapacityExcel = new List<int>();
            lstCapacityExcel.Add(1);
            lstCapacityExcel.Add(2);
            lstCapacityExcel.Add(3);
            var listNL = listRatedCommentPupil.Where(o => o.EvaluationID == GlobalConstants.EVALUATION_CAPACITY && o.CapacityEndingEvaluation.HasValue
                                                    && lstCapacityExcel.Contains(o.SubjectID)).ToList();

            startColumn = 3;
            startRow = 74;
            curRow = startRow;

            for (int i = 1; i <= lstCapacityExcel.Count; i++)
            {
                curColumn = startColumn;
                for (int iEdu = 1; iEdu <= 5; iEdu++)
                {
                    var goodDoneListNL = listNL.Where(o => o.EducationLevelID == iEdu && o.CapacityEndingEvaluation == 1 && o.SubjectID == i).ToList();
                    var doneListNL = listNL.Where(o => o.EducationLevelID == iEdu && o.CapacityEndingEvaluation == 2 && o.SubjectID == i).ToList();
                    var notDoneListNL = listNL.Where(o => o.EducationLevelID == iEdu && o.CapacityEndingEvaluation == 3 && o.SubjectID == i).ToList();

                    firstSheet.SetCellValue(curRow, curColumn, goodDoneListNL.Count);
                    firstSheet.SetCellValue(curRow + 1, curColumn, doneListNL.Count);
                    firstSheet.SetCellValue(curRow + 2, curColumn, notDoneListNL.Count);
                    curColumn = curColumn + 1;
                }
                curRow = curRow + 4;
            }

            //Pham chat
            List<int> lstQuantityExcel = new List<int>();
            lstQuantityExcel.Add(4);
            lstQuantityExcel.Add(5);
            lstQuantityExcel.Add(6);
            lstQuantityExcel.Add(7);
            var listPC = listRatedCommentPupil.Where(o => o.EvaluationID == GlobalConstants.EVALUATION_QUALITY && o.QualityEndingEvaluation.HasValue
                                                    && lstQuantityExcel.Contains(o.SubjectID)).ToList();
            startColumn = 3;
            startRow = 87;
            curRow = startRow;
            curColumn = startColumn;

            for (int i = 4; i <= lstQuantityExcel.Count + 3; i++)
            {
                curColumn = startColumn;
                for (int iEdu = 1; iEdu <= 5; iEdu++)
                {
                    var goodDoneListPC = listPC.Where(o => o.EducationLevelID == iEdu && o.QualityEndingEvaluation == 1 && o.SubjectID == i).ToList();
                    var doneListPC = listPC.Where(o => o.EducationLevelID == iEdu && o.QualityEndingEvaluation == 2 && o.SubjectID == i).ToList();
                    var notDoneListPC = listPC.Where(o => o.EducationLevelID == iEdu && o.QualityEndingEvaluation == 3 && o.SubjectID == i).ToList();

                    firstSheet.SetCellValue(curRow, curColumn, goodDoneListPC.Count);
                    firstSheet.SetCellValue(curRow + 1, curColumn, doneListPC.Count);
                    firstSheet.SetCellValue(curRow + 2, curColumn, notDoneListPC.Count);
                    curColumn = curColumn + 1;
                }
                curRow = curRow + 4;
            }
            #endregion

            #region Khen thuong
            //Lay ra danh sach khen thuong
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["SemesterID"] = semester;
            List<EvaluationRewardEQReportBO> listReward = (from r in EvaluationRewardBusiness.Search(tmpDic)
                                                           join pp in PupilProfileBusiness.All on r.PupilID equals pp.PupilProfileID
                                                           join cp in ClassProfileBusiness.All on r.ClassID equals cp.ClassProfileID
                                                           where pp.IsActive && (cp.IsActive == null || (cp.IsActive.HasValue && cp.IsActive.Value)) && r.RewardID.HasValue && r.RewardID.Value > 0
                                                           select new EvaluationRewardEQReportBO
                                                           {
                                                               PupilID = r.PupilID,
                                                               ClassID = r.ClassID,
                                                               EducationLevelID = cp.EducationLevelID,
                                                               IsCombinedClass = cp.IsCombinedClass,
                                                               IsDisabled = pp.IsDisabled,
                                                               Genre = pp.Genre,
                                                               EthnicID = pp.EthnicID
                                                           }).ToList();
            // Lay danh sach hoc sinh co khen thuong hoc ky
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["IsGetReward"] = 1;
            IQueryable<SummedEndingEvaluationBO> querySE = SummedEndingEvaluationBusiness.Search(tmpDic);
            if (semester == 1)
                querySE = querySE.Where(x => x.SemesterID == semester);
            else
                querySE = querySE.Where(x => x.SemesterID == semester || x.SemesterID == 4);
            List<SummedEndingEvaluationBO> listSummedEndingEvaluation = querySE.ToList();
            listSummedEndingEvaluation = (from see in listSummedEndingEvaluation
                                          where listPupilOfClassInSemester.Any(poc => poc.ClassID == see.ClassID && poc.PupilID == see.PupilID)
                                          select see).ToList();

            listReward = (from rw in listReward
                          where listPupilOfClassInSemester.Any(poc => poc.ClassID == rw.ClassID && poc.PupilID == rw.PupilID)
                          select rw).ToList();
            listReward.AddRange(listSummedEndingEvaluation.Select(o => new EvaluationRewardEQReportBO
                                                           {
                                                               PupilID = o.PupilID,
                                                               ClassID = o.ClassID,
                                                               EducationLevelID = o.EducationLevelID,
                                                               IsCombinedClass = o.IsCombinedClass,
                                                               IsDisabled = o.IsDisabled,
                                                               Genre = o.Genre,
                                                               EthnicID = o.EthnicID
                                                           }).ToList());

            //Fill du lieu
            startColumn = 3;
            startRow = 14;
            curRow = startRow;
            curColumn = startColumn;

            for (int iEdu = 1; iEdu <= 5; iEdu++)
            {
                List<EvaluationRewardEQReportBO> listRewardEdu = listReward.Where(o => o.EducationLevelID == iEdu).ToList();

                firstSheet.SetCellValue(curRow, curColumn, listRewardEdu.Select(o => o.PupilID).Distinct().Count());
                curColumn = curColumn + 1;
            }
            #endregion

            #region Chuong trinh lop hoc
            if (semester == 2)
            {
                //Lay ra danh sach ket qua hoc tap cua toan truong trong hoc ky
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["Semester"] = 4;

                listSummedEndingEvaluation = SummedEndingEvaluationBusiness.Search(tmpDic).Where(o => o.EvaluationID == GlobalConstants.EVALUATION_RESULT).ToList();
                listSummedEndingEvaluation = (from see in listSummedEndingEvaluation
                                              where listPupilOfClassInSemester.Any(poc => poc.ClassID == see.ClassID && poc.PupilID == see.PupilID)
                                              select see).ToList();

                //Fill du lieu
                startColumn = 3;
                startRow = 12;
                curRow = startRow;
                curColumn = startColumn;

                for (int iEdu = 1; iEdu <= 5; iEdu++)
                {
                    List<SummedEndingEvaluationBO> doneList = listSummedEndingEvaluation.Where(o => o.EducationLevelID == iEdu).Where(o => o.EndingEvaluation == "HT").ToList();
                    List<SummedEndingEvaluationBO> unDoneList = listSummedEndingEvaluation.Where(o => o.EducationLevelID == iEdu).Where(o => o.EndingEvaluation == "CHT").ToList();

                    //Tong so
                    firstSheet.SetCellValue(curRow, curColumn, doneList.Count);
                    firstSheet.SetCellValue(curRow + 1, curColumn, unDoneList.Count);
                    curColumn++;
                }
            }
            #endregion

            firstSheet.FitAllColumnsOnOnePage = true;
            string datetime = school.Province.ProvinceName + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;
            firstSheet.SetCellValue("D106", datetime);
            firstSheet.SetCellValue("D111", school.HeadMasterName);
            firstSheet.GetRange("D111", "D111").SetHAlign(VTHAlign.xlHAlignCenter);
            firstSheet.GetRange("D111", "D111").SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertEducationQualityStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC_NEW;

            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            int semester = Utils.GetInt(dic["Semester"]);
            string strSemester = semester == 0 ? "DauKy" : semester == 1 ? "GiuaKy" : "CuoiKy";

            outputNamePattern = outputNamePattern.Replace("{0}", strSemester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion
    }
}
