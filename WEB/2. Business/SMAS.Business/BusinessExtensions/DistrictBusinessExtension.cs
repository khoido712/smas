﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DistrictBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int DISTRICTCODE_MAX_LENGTH = 10; //Độ dài mã Quận huyện
        private const int DISTRICTNAME_MAX_LENGTH = 50; //Độ dài tên Quận huyện
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn
        private const int VALUE_MIN = 1;//Giá trị min
        private const int VALUE_MAX = 3;//Giá trị max
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Quận / Huyện
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="DistrictName">Tên quận/huyện</param>
        /// <param name="DistrictCode">Mã quận/huyện</param>
        /// <param name="ProvinceID">ID Tỉnh/Thành phố</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns> IQueryable<District></returns>
        public IQueryable<District> Search(IDictionary<string, object> dic)
        {
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string DistrictName = Utils.GetString(dic, "DistrictName");
            string DistrictCode = Utils.GetString(dic, "DistrictCode");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<District> lsDistrict = DistrictRepository.All;

            if (DistrictID != 0)
                lsDistrict = lsDistrict.Where(dis => dis.DistrictID == DistrictID);
            if (IsActive != null)
                lsDistrict = lsDistrict.Where(dis => dis.IsActive == IsActive);
            if (DistrictName.Trim().Length != 0)
                lsDistrict = lsDistrict.Where(dis => dis.DistrictName.ToLower().Contains(DistrictName.ToLower()));
            if (DistrictCode.Trim().Length != 0)
                lsDistrict = lsDistrict.Where(dis => dis.DistrictCode.ToLower().Contains(DistrictCode.ToLower()));
            if (Description.Trim().Length != 0)
                lsDistrict = lsDistrict.Where(dis => dis.Description.ToLower().Contains(Description.ToLower()));
            if (ProvinceID != 0)
                lsDistrict = lsDistrict.Where(dis => dis.ProvinceID == ProvinceID);

            return lsDistrict;

        }

        public IQueryable<District> GetListDistrict(int ProvinceID)
        {
            IQueryable<District> query = from d in this.All
                                         where d.ProvinceID == ProvinceID
                                         select d;

            return query;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm  Quận / Huyện
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="district">object District</param>
        /// <returns>object District</returns>
        public override District Insert(District district)
        {
            //Mã Quận huyện không được để trống 
            Utils.ValidateRequire(district.DistrictCode, "District_Label_DistrictCode");
            //Độ dài mã Quận huyện không được vượt quá 10 
            Utils.ValidateMaxLength(district.DistrictCode, DISTRICTCODE_MAX_LENGTH, "District_Label_DistrictCode");
            //Mã Quận huyện đã tồn tại 
            new DistrictBusiness(null).CheckDuplicate(district.DistrictCode, GlobalConstants.LIST_SCHEMA, "District", "DistrictCode", true, district.DistrictID, "District_Label_DistrictCode");
            //Tên Quận huyện không được để trống 
            Utils.ValidateRequire(district.DistrictName, "District_Label_DistrictName");
            //Độ dài trường tên Quận huyện không được vượt quá 50 
            Utils.ValidateMaxLength(district.DistrictName, DISTRICTNAME_MAX_LENGTH, "District_Label_DistrictName");
            //Tên Quận huyện đã tồn tại 
            new DistrictBusiness(null).CheckDuplicate(district.DistrictName, GlobalConstants.LIST_SCHEMA, "District", "DistrictName", true, district.DistrictID, "District_Label_DistrictName");
            //Không tồn tại Tỉnh thành hoặc đã bị xóa - Kiểm tra ProvinceID trong bảng Province với IsActive =1
            new ProvinceBusiness(null).CheckAvailable(district.ProvinceID, "District_ProvinceID",true);
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(district.Description, DESCRIPTION_MAX_LENGTH, "Description");

            //Insert
           return base.Insert(district);

        }
        #endregion


        #region Update

        /// <summary>
        /// Sửa  Quận / Huyện
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="district">object District</param>
        /// <returns>object District</returns>
        public override District Update(District district)
        {
            //Bạn chưa chọn Quận huyện cần sửa hoặc Quận huyện bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable((int)district.DistrictID, "District_Label_DistrictID", true);
            //Mã Quận huyện không được để trống 
            Utils.ValidateRequire(district.DistrictCode, "District_Label_DistrictCode");
            //Độ dài mã Quận huyện không được vượt quá 10 
            Utils.ValidateMaxLength(district.DistrictCode, DISTRICTCODE_MAX_LENGTH, "District_Label_DistrictCode");
            //Mã Quận huyện đã tồn tại 
            this.CheckDuplicate(district.DistrictCode, GlobalConstants.LIST_SCHEMA, "District", "DistrictCode", true, district.DistrictID, "District_Label_DistrictCode");
            //Tên Quận huyện không được để trống 
            Utils.ValidateRequire(district.DistrictName, "District_Label_DistrictName");
            //Độ dài trường tên Quận huyện không được vượt quá 50 
            Utils.ValidateMaxLength(district.DistrictName, DISTRICTNAME_MAX_LENGTH, "District_Label_DistrictName");
            //Tên Quận huyện đã tồn tại 
            this.CheckDuplicate(district.DistrictName, GlobalConstants.LIST_SCHEMA, "District", "DistrictName", true, district.DistrictID, "District_Label_DistrictName");
            //Không tồn tại Tỉnh thành hoặc đã bị xóa - Kiểm tra ProvinceID trong bảng Province với IsActive =1
            this.ProvinceBusiness.CheckAvailable(district.ProvinceID, "District_ProvinceID", true);
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(district.Description, DESCRIPTION_MAX_LENGTH, "Description");


            return base.Update(district);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa  Quận / Huyện
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="DistrictID">ID District</param>
        public void Delete(int DistrictID)
        {
            //Bạn chưa chọn Quận huyện cần xóa hoặc Quận huyện bạn chọn đã bị xóa khỏi hệ thống
           this.CheckAvailable(DistrictID, "District_Label_DistrictID", true);

            //Không thể xóa Quận huyện đang sử dụng
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "District", DistrictID, "District_Label_DistrictID");
            
            base.Delete(DistrictID, true);

        }
        #endregion
    }
}