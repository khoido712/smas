﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;


namespace SMAS.Business.Business
{
    public partial class RoleBusiness
    {

        #region nghiep vu
        public bool IsSuperRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_CAPCAO)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsProvinceRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_CAPTINH)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSuperVisingDeptRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_CAPSO)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSubSuperVisingDeptRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_CAPPHONG)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSchoolRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC1
                || RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC2
                || RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC3
                || RoleID == GlobalConstants.ROLE_ADMIN_NHATRE
                || RoleID == GlobalConstants.ROLE_ADMIN_MAUGIAO)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSchoolRole_Primary(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSchoolRole_SecondarySchool(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC2)
            {
                return true;
            }
            else return false;
        }

        public bool IsSchoolRole_HighSchool(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC3)
            {
                return true;
            }
            else return false;
        }

        public bool IsSchoolRole_PreSchool(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_MAUGIAO)
            {
                return true;
            }
            else return false;
        }

        public bool IsSchoolRole_ChildCare(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (RoleID == GlobalConstants.ROLE_ADMIN_NHATRE)
            {
                return true;
            }
            else return false;

        }

        public bool CheckPriviledgeToCreateRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            return this.IsSuperRole(RoleID);
        }

        public Role GetPreRole(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (this.IsSuperRole(RoleID))
            {
                List<object> Params = new List<object>();
                Params.Add("Role_Label_RoleID");
                throw new BusinessException("Role_Label_IsSuperAdmin", Params);
            }
            int? parentID = RoleRepository.All.Where(r => r.IsActive == true && r.RoleID == RoleID).First().ParentID;
            if (parentID.HasValue)
            {
                return RoleRepository.All.Where(r => r.IsActive == true && r.RoleID == parentID).First();
            }
            else return null;
        }

        public IQueryable<Role> GetAllSuperRoles(int RoleID)
        {
            ValidateRoleID(RoleID);
            if (this.IsSuperRole(RoleID))
            {
                List<object> Params = new List<object>();
                Params.Add("Role_Label_RoleID");
                throw new BusinessException("Role_Label_IsSuperAdmin", Params);
            }
            // Role role = RoleRepository.All.Where(r => r.RoleID == RoleID && r.IsActive == true).FirstOrDefault();
            Role role = RoleRepository.Find(RoleID);
            if (role == null)
            {
                return null;
            }

            string path = role.RolePath;
            string[] sRoleIDs = path.Split('/');
            List<Role> result = new List<Role>();
            foreach (string sRoleID in sRoleIDs)
            {
                try
                {
                    int iRoleID = Convert.ToInt32(sRoleID);
                    Role parent = RoleRepository.Find(iRoleID);
                    if (parent != null) result.Add(parent);
                }
                catch (BusinessException) { }
            }
            if (result != null)
            {
                return result.AsQueryable();
            }
            else return null;
        }

        public IQueryable<Role> GetNextSubRoles(int RoleID)
        {
            ValidateRoleID(RoleID);
            return RoleRepository.All.Where(r => r.ParentID == RoleID && r.IsActive == true);
        }

        public IQueryable<Role> GetAllSubRoles(int RoleID)
        {
            ValidateRoleID(RoleID);
            string sRolePath = "/" + RoleID.ToString() + "/";
            IQueryable<Role> allRoles = RoleRepository.All.Where(r => r.IsActive == true && r.RoleID != RoleID);
            List<Role> result = new List<Role>();
            foreach (Role r in allRoles)
            {
                if (r.RolePath.Contains(sRolePath))
                {
                    result.Add(r);
                }
            }
            if (result != null) return result.AsQueryable();
            else return null;
        }

        public IQueryable<Role> GetAll()
        {
            return RoleRepository.All.Where(r => r.IsActive == true);
        }
        #endregion

        #region ho tro
        public void SetValuesToCreate(Role role, string roleParentID)
        {
            if (string.IsNullOrEmpty(roleParentID))
            {
                role.ParentID = null;
                role.RolePath = "/";
            }
            else
            {
                role.ParentID = Convert.ToInt32(roleParentID);
                Role roleParent = RoleRepository.All.Where(r => r.RoleID == role.ParentID).First();
                role.RolePath = roleParent.RolePath + roleParent.RoleID + "/";
            }
            //da nhap tu tren View: ParentID RoleName IsActive Description
            role.CreatedDate = DateTime.Now;
            role.IsActive = true;
        }
        //kiem tra Role duoc chon luc dang nhap co la SuperRole ko?
        public bool CheckSuperRole(Role selectedRole)
        {
            if (selectedRole.IsActive == true && selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_CAPCAO)
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role admin cap tinh ko?
        public bool CheckProvinceRole(Role selectedRole)
        {
            if (selectedRole.IsActive == true && selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_CAPTINH)
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role Cap So ko?
        public bool CheckSuperVisingDeptRole(Role selectedRole)
        {
            if (selectedRole.IsActive == true && selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_CAPSO)
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role cap Phong khong?
        public bool CheckSubSuperVisingDeptRole(Role selectedRole)
        {
            if (selectedRole.IsActive == true && selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_CAPPHONG)
                return true;
            else return false;
        }

        //kiem tra Role duoc chon luc dang nhap co la Role cap truong ko?
        public bool CheckSchoolRole(Role selectedRole)
        {
            if (selectedRole.IsActive == true && (selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC1 || selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC2 || selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC3 || selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_NHATRE || selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_MAUGIAO))
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role truong cap 1 ko?
        public bool CheckSchoolRole_Primary(Role selectedRole)
        {
            if (selectedRole.IsActive == true && (selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC1))
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role truong cap 2 ko?
        public bool CheckSchoolRole_Secondary(Role selectedRole)
        {
            if (selectedRole.IsActive == true && (selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC2))
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role truong cap 3 ko?
        public bool CheckSchoolRole_HighSchool(Role selectedRole)
        {
            if (selectedRole.IsActive == true && (selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_TRUONGC3))
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role truong mau giao ko?
        public bool CheckSchoolRole_PreSchool(Role selectedRole)
        {
            if (selectedRole.IsActive == true && (selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_MAUGIAO))
                return true;
            else return false;
        }
        //kiem tra Role duoc chon luc dang nhap co la Role nha tre ko?
        public bool CheckSchoolRole_ChildCare(Role selectedRole)
        {
            if (selectedRole.IsActive == true && (selectedRole.RoleID == GlobalConstants.ROLE_ADMIN_NHATRE))
                return true;
            else return false;
        }

        public void ValidateRoleID(int RoleID)
        {
            if (!SMAS.Business.Common.StaticData.ListRole.Any(o => o.RoleID == RoleID))
            {
                List<object> Params = new List<object>();
                Params.Add("Role_Label_RoleID");
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
        }




        /*
        public bool IsSubRoleOf(int subRoleID, int ParentRoleID)
        {
            if (subRoleID == null || ParentRoleID == null)
            {
                throw new BusinessException();
            }            
        }
         */
        public bool AssignRolesForAdmin(int UserAccountID, List<int> ListRoleID, int AdminID, int selectedRoleID)
        {

            if (ListRoleID == null)
            {
                throw new BusinessException();
            }
            //Kiểm tra quyền: danh sách tất cả các Role định gán có là Role con của Role người thực hiện không
            IQueryable<Role> subRoles = this.GetAllSubRoles(selectedRoleID);
            if (subRoles == null) return false;
            foreach (int roleID in ListRoleID)
            {
                Role role = RoleRepository.All.Where(r => r.RoleID == roleID && r.IsActive == true).First();
                if (role == null || !subRoles.Contains(role))
                {
                    //neu roleID ko ton tai hoac ko nam trong role cap duoi cua nguoi thuc hien
                    throw new BusinessException();
                }
            }
            foreach (int roleID in ListRoleID)
            {
                UserRole ur = new UserRole();
                ur.RoleID = roleID;
                ur.UserID = AdminID;
                UserRoleRepository.Insert(ur);
            }
            return true;
        }
        //
        public bool CheckPrivilegeAssignMenuForRole(int RoleID)
        {
            return IsSuperRole(RoleID);
        }

        public IQueryable<Role> Search(Dictionary<string, object> dic)
        {
            IQueryable<Role> result = this.GetAll();
            if (dic.ContainsKey("RoleName"))
            {
                string RoleName = (string)dic["RoleName"];
                if (!string.IsNullOrEmpty(RoleName))
                {
                    result = result.Where(g => g.RoleName.Contains(RoleName));
                }
            }
            if (dic.ContainsKey("RoleParentID"))
            {
                int roleParentID = (int)dic["RoleParentID"];
                if (roleParentID != -1)
                {
                    result = result.Where(g => g.ParentID == roleParentID);
                }
            }
            return result;
        }
        //
        public void Delete(int id)
        {
            Role role = RoleRepository.Find(id);
            role.IsActive = false;
            RoleRepository.Update(role);
        }
        #endregion



    }
}