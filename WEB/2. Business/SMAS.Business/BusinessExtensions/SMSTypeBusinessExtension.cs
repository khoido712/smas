﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class SMSTypeBusiness
    {
        /// <summary>
        /// Get SMSType from School
        /// </summary>
        /// <param name="yTypeCommunication"></param>
        /// <param name="iSchoolID"></param>
        /// <param name="iLevel"></param>
        /// <param name="bIsLock"></param>
        /// <param name="bGetAll"></param>
        /// <returns></returns>
        public List<SMSTypeBO> GetSMSTypeBySchool(byte yTypeCommunication, int? iSchoolID, int? iLevel, bool bIsLock = false, bool bGetAll = false)
        {
            try
            {
                List<SMSTypeBO> lstSMSTypeBO = new List<SMSTypeBO>();
                // Anhvd9 20160419 - Phiên bản rút gọn chỉ có bản tin trao đổi
                SchoolProfile objSP = SchoolProfileBusiness.Find(iSchoolID.Value);
                string appliedLevel = iLevel.HasValue ? iLevel.Value.ToString() : string.Empty;
                if (objSP.ProductVersion == SystemParamsInFile.PRODUCT_BASIC_VERSION)
                {
                    lstSMSTypeBO.Add(new SMSTypeBO()
                    {
                        TypeID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID,
                        TypeCode = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TYPECODE,
                        Name = "Trao đổi"
                    });
                }
                else
                {
                    lstSMSTypeBO = (from sp in SMSTypeBusiness.All
                                    join sd in SMSTypeDetailBusiness.All on sp.SMS_TYPE_ID equals sd.SMS_TYPE_ID
                                    where
                                    ((bIsLock && sd.IS_LOCK) || (!bIsLock && !sd.IS_LOCK) || (bGetAll))
                                    && ( sp.TYPE_ID == yTypeCommunication || yTypeCommunication == 0 )
                                    && sd.SCHOOL_ID == iSchoolID
                                    && (sp.APPLIED_LEVEL.Contains(appliedLevel))
                                    select new SMSTypeBO
                                    {
                                        TypeID = sp.SMS_TYPE_ID,
                                        TypeCode = sp.TYPE_CODE,
                                        Name = sp.SMS_TYPE_NAME,
                                        IsCustom = false,
                                        OrderID = sp.ORDER_ID
                                    }).OrderBy(p=>p.OrderID).ToList();
                }
                return lstSMSTypeBO;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<SMSTypeBO>();
            }
        }
        /// <summary>
        /// Validate SMS Config
        /// </summary>
        /// <param name="schoolID"></param>
        /// <returns></returns>
        public bool ValidateSMSType(int schoolID)
        {
            string para = string.Format("schoolID={0}", schoolID);
            try
            {
                List<SMS_TYPE_DETAIL> smsTypeDetails = SMSTypeDetailBusiness.All.Where(p => p.SCHOOL_ID == schoolID).ToList();
                List<SMS_TYPE> smsTypes = SMSTypeBusiness.All.ToList();
                int count = smsTypes.Count;
                int newCounter = 0;
                SMS_TYPE_DETAIL dtl = null;
                SMS_TYPE typeSMS = null;
                for (int i = 0; i < count; i++)
                {
                    typeSMS = smsTypes[i];
                    dtl = smsTypeDetails.Where(a => a.SMS_TYPE_ID == typeSMS.SMS_TYPE_ID).FirstOrDefault();
                    if (dtl == null)
                    {
                        dtl = new SMS_TYPE_DETAIL();
                        dtl.SMS_TYPE_ID = typeSMS.SMS_TYPE_ID;
                        dtl.SCHOOL_ID = schoolID;
                        dtl.SMS_TEMPLATE = smsTypes[i].DEFAULT_TEMPLATE;
                        dtl.UPDATE_TIME = DateTime.Now;
                        dtl.IS_LOCK = typeSMS.DEFAULT_IS_LOCK;
                        dtl.IS_AUTO_SEND = typeSMS.DEFAULT_IS_AUTO_SEND;
                        dtl.SEND_BEFORE = typeSMS.DEFAULT_SEND_BEFORE;
                        dtl.SEND_TIME = typeSMS.DEFAULT_SEND_TIME;
                        SMSTypeDetailBusiness.Insert(dtl);
                        newCounter++;
                    }
                }

                if (newCounter > 0)
                {
                    SMSTypeDetailBusiness.Save();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return false;
            }
        }
        /// <summary>
        /// Check MarkTime Config
        /// </summary>
        /// <param name="schoolYearID"></param>
        /// <param name="semester"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public bool CheckTimeMark(int schoolYearID, int semester, int year)
        {
            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            //Call service GetTimeMarkBySemester(schoolyearid, semester) from SMAS3.0
            //If resule is more than 0 item return true other else return false;
            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            IDictionary<string, object> SearchDic = new Dictionary<string, object>();
            SearchDic["AcademicYearID"] = schoolYearID;
            //Neu truyen vao hoc ky = 0 thi lay tat ca cac dot cua nam
            if (semester != 0)
            {
                SearchDic["Semester"] = semester;
            }
            IQueryable<PeriodDeclaration> iquery = PeriodDeclarationBusiness.Search(SearchDic);
            return iquery != null && iquery.Count() > 0;
        }
        public List<SMSTypeBO> GetCustomSMSTypeBySchool(int schoolId, int grade, bool? isLocked = null)
        {
            return CustomSMSTypeBusiness.All.Where(o => o.SCHOOL_ID == schoolId && o.GRADE == grade && (!isLocked.HasValue || (isLocked.HasValue && o.IS_LOCKED == isLocked.Value)) && o.IS_ACTIVE)
                .OrderBy(o => o.CREATE_TIME)
                .Select(o => new SMSTypeBO
                {
                    IsCustom = true,
                    Name = o.TYPE_NAME,
                    TypeCode = o.TYPE_CODE,
                    TypeID = o.CUSTOM_SMS_TYPE_ID,
                    IsForVNEN = o.FOR_VNEN,
                    PeriodType = o.PERIOD_TYPE
                }).ToList();
        }
        #region check smsType is lock
        /// <summary>
        /// check smsType is lock, return true if lock
        /// </summary>
        /// <author date="2013/11/20">HoanTV5</author>
        /// <returns></returns>
        public bool CheckSMSTypeIsLock(string cTypeCode, int yTypeCommunication, int? iSchoolID)
        {
            try
            {
                bool isLock = false;
                SMS_TYPE_DETAIL objSMSTypeDetail = (from smsTD in SMSTypeDetailBusiness.All
                                                    join smsT in SMSTypeBusiness.All on smsTD.SMS_TYPE_ID equals smsT.SMS_TYPE_ID
                                                    where smsT.TYPE_CODE.Equals(cTypeCode)
                                                    && smsT.TYPE_ID == yTypeCommunication
                                                    select smsTD).FirstOrDefault();

                if (objSMSTypeDetail != null)
                {
                    isLock = objSMSTypeDetail.IS_LOCK;
                }
                return isLock;
            }
            catch (Exception ex)
            {
                string para = string.Format("message={0},cTypeCode={1}, yTypeCommunication={2}, iSchoolID={3}",ex.Message, cTypeCode, yTypeCommunication, iSchoolID);
                logger.ErrorFormat(para);
                return false;
            }
        }
        #endregion
    }
}
