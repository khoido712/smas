﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Data.Objects;
using System.Text;
using System.Web.Security;
using Oracle.DataAccess.Client;

namespace SMAS.Business.Business
{
    /// <summary>
    /// cán bộ
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class EmployeeBusiness
    {
        #region private member variable
        private const int EMPLOYEE_CODE_MAXLENGTH = 30;     //do dai lon nhat cua ma nhan vien
        private const int EMPLOYEE_FULLNAME_MAXLENGTH = 100;    //do dai lon nhat cua ten nv
        private const int EMPLOYEE_ALIAS_MAXLENGTH = 100;   //do dai lon nhat biet danh
        private const int EMPLOYEE_IDENTIFYNUMBER_MAXLENGTH = 15;   //do dai lon nhat cua so cmtnd
        private const int EMPLOYEE_IDENTIFYISSUEDPLACE_MAXLENGTH = 400; //do dai lon nhat noi cap cmt
        private const int EMPLOYEE_HEALTHSTATUS_MAXLENGTH = 400;    //do dai lon nhat tinh trang sk
        private const int EMPLOYEE_HOMETOWN_MAXLENGTH = 400;    //do dai lon nhat que quan
        private const int EMPLOYEE_PERMANENTRESIDNETALADDRESS_MAXLENGTH = 400;  //do dai lon nhat dia chi thuong tru
        private const int EMPLOYEE_YOUTHLEAGUEJOINEDPLACE_MAXLENGTH = 400;  //do dai lon nhat noi ket nap doan

        private const int EMPLOYEE_COMMUNISTPARTYJOINEDPLACE_MAXLENGTH = 400;   //do dai lon nhat noi ket nap dang
        private const int EMPLOYEE_FATHERFULLNAME_MAXLENGTH = 100;  //do dai lon nhat ten bo
        private const int EMPLOYEE_FATHERJOB_MAXLENGTH = 200;   //do dai lon nhat nghe nghiep bo
        private const int EMPLOYEE_FATHERWORKINGPLACE_MAXLENGTH = 400;  //do dai lon nhat noi lam viec cua bo
        private const int EMPLOYEE_MOTHERFULLNAME_MAXLENGTH = 100;  //do dai lon nhat ten me
        private const int EMPLOYEE_MOTHERJOB_MAXLENGTH = 200;   //do dai lon nhat nghe nghiep cua me
        private const int EMPLOYEE_MOTHERWORKINGPLACE_MAXLENGTH = 400;  //do dai lon nhat noi lam viec cua me
        private const int EMPLOYEE_SPOUSEFULLNAME_MAXLENGTH = 100;  //do dai lon nhat ten vo/chong
        private const int EMPLOYEE_SPOUSEJOB_MAXLENGTH = 200;   //do dai lon nhat nghe nghiep vo chong
        private const int EMPLOYEE_SPOUSEWORKINGPLACE_MAXLENGTH = 400;//do dai lon nhat noi lam viec vo/chong
        private const int EMPLOYEE_DESCRIPTION_MAXLENGTH = 400; //do dai lon nhat mieu ta

        private const int EMPLOYEE_TELEPHONE_MAXLENGTH = 15;    //do dai lon nhat dien thoai
        private const int EMPLOYEE_MOBILE_MAXLENGTH = 15;   //do dai lon nhat di dong

        private const int EMPLOYEE_EMAIL_MAXLENGTH = 60;    //do dai lon nhat email

        //


        private const int EMPLOYEE_AGE_NOW = 16;
        private const int EMPLOYEE_MINIMUM_BIRTHYEAR = 1900;

        private const int EMPLOYEE_TEACHERTYPE_CODE_MAXLENGTH = 30;
        #endregion

        #region insert
        /// <summary>
        ///Thêm mới cán bộ
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertEmployee">Đối tượng Insert</param>
        /// <returns></returns>
        public Employee InsertTeacher(Employee insertEmployee, bool forImport = false)
        {
            //Trường học không chứa cấp tương ứng: kiểm tra SchoolProfile(SchoolID).EducationGrade với AppliedLevel
            //bang hien tai dang thieu truong applied level
            SchoolProfile sf = SchoolProfileRepository.Find(insertEmployee.SchoolID);
            int appliedLevel = insertEmployee.AppliedLevel ?? 0;
            if (appliedLevel > 0 && !SchoolProfileBusiness.GetListAppliedLevel(sf.SchoolProfileID).Contains((int)insertEmployee.AppliedLevel))
            {
                throw new BusinessException("Common_Validate_AppliedLevel");
            }


            //EmployeeCode: require, duplicate, maxlength(30)
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_EmployeeCode");
            Utils.ValidateMaxLength(insertEmployee.EmployeeCode, EMPLOYEE_TEACHERTYPE_CODE_MAXLENGTH, "Employee_Label_EmployeeCode");
            //this.CheckDuplicate(insertEmployee.EmployeeCode, GlobalConstants.SCHOOL_SCHEMA, "Employee", "EmployeeCode", true, 0, "Employee_Label_EmployeeCode");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode}
                    ,{"SchoolID",insertEmployee.SchoolID}
                    ,{"IsActive",true}
                }, new Dictionary<string, object>()
                {
                {"EmployeeID",insertEmployee.EmployeeID}
                });
                if (Exist)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "Employee_Label_EmployeeCode" });
                }
            }
            //SchoolFacultyID: require
            Utils.ValidateRequire(insertEmployee.SchoolFacultyID, "Employee_Label_SchoolFacultyID");
            //SchoolID: require
            Utils.ValidateRequire(insertEmployee.SchoolID, "SchoolProfile_Label_SchoolID");

            //Validate(Entity);
            Validate(insertEmployee, forImport);


            //Employee.EmployeeType =  EMPLOYEE_TYPE_TEACHER
            //Employee.EmploymentStatus = EMPLOYMENT_STATUS_WORKING

            insertEmployee.EmployeeType = GlobalConstants.EMPLOYEE_TYPE_TEACHER;
            insertEmployee.EmploymentStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            insertEmployee.IsActive = true;

            //Từ TrainingLevelID => Resolution = TrainingLevel(TrainingLevelID).Resolution 
            //Resolution, AppliedLevel => GraduationLevelID (Dựa vào bảng GraduationLevel)
            TrainingLevel tl = TrainingLevelBusiness.Find(insertEmployee.TrainingLevelID);
            if (tl != null)
            {
                GraduationLevel gl = GraduationLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true }, { "Resolution", tl.Resolution }, { "AppliedLevel", insertEmployee.AppliedLevel } }).FirstOrDefault();
                if (gl != null)
                {
                    insertEmployee.GraduationLevelID = gl.GraduationLevelID;
                }
            }
            //Khởi tạo đối tượng EmployeeHistoryStatus, gán các giá trị EmployeeID, SchoolID, 
            //EmployeeStatus tương ứng theo Employee; FromDate = DateTime.Now

            //
            //Thực hiện Insert dữ liệu vào bảng Employee
            Employee returnEmployee = base.Insert(insertEmployee);

            //Khởi tạo đối tượng TeacherOfFaculty, gán các giá trị TeacherID(EmployeeID), SchoolID, 
            //FacultyID tương ứng theo Employee; IsActive = True, StartDate = DateTime.Now; insert vào TeacherOfFaculty
            TeacherOfFaculty tof = new TeacherOfFaculty();
            tof.Employee = returnEmployee;
            //tof.TeacherID = returnEmployee.EmployeeID;
            tof.FacultyID = returnEmployee.SchoolFacultyID.Value;
            tof.IsActive = true;
            tof.StartDate = DateTime.Now;
            TeacherOfFacultyRepository.Insert(tof);

            //Khởi tạo đối tượng EmployeeHistoryStatus, gán các giá trị EmployeeID, SchoolID, EmployeeStatus 
            //tương ứng theo Employee; FromDate = DateTime.Now; insert vào EmployeeHistoryStatus
            EmployeeHistoryStatus ehs = new EmployeeHistoryStatus();
            // ehs.EmployeeID = insertEmployee.EmployeeID;
            ehs.Employee = returnEmployee;
            ehs.SchoolID = returnEmployee.SchoolID;
            ehs.EmployeeStatus = (int)returnEmployee.EmploymentStatus;
            ehs.FromDate = insertEmployee.IntoSchoolDate;
            EmployeeHistoryStatusRepository.Insert(ehs);

            return returnEmployee;


        }

        /// <summary>
        /// Thêm mới Nhân viên thuộc các đơn vị quản lý
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertEmployee">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public Employee InsertEmployee(Employee insertEmployee)
        {

            //Mã nhân viên không được trống. Độ dài không quá 50 ký tự - Kiểm tra EmployeeCode
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_EmployeeCode");
            Utils.ValidateMaxLength(insertEmployee.EmployeeCode, EMPLOYEE_CODE_MAXLENGTH, "Employee_Label_EmployeeCode");
            //Mã nhân viên không được trùng – Kiểm tra sự tồn tại EmployeeCode trong bảng Employee
            //this.CheckDuplicate(insertEmployee.EmployeeCode, GlobalConstants.SCHOOL_SCHEMA, "Employee", "EmployeeCode", true, 0, "Employee_Label_EmployeeCode");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode}
                    ,{"SchoolID",insertEmployee.SchoolID}
                    ,{"IsActive",true}
                }, new Dictionary<string, object>()
                {
                {"EmployeeID",insertEmployee.EmployeeID}
                });
                if (Exist)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "Employee_Label_EmployeeCode" });
                }
            }
            /*DungVA - 07/06/2013 check trùng mã cán bộ */
            if (insertEmployee.SchoolID == null)
            {
                IDictionary<string, object> expDic = null;
                if (insertEmployee.EmployeeID > 0)
                {
                    expDic = new Dictionary<string, object>();
                    expDic["EmployeeID"] = insertEmployee.EmployeeID;
                }
                bool EmployeeCodeDuplicate = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                       new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode},
                    {"SupervisingDeptID",insertEmployee.SupervisingDeptID},
                    {"IsActive",true}
                }, expDic);
                if (EmployeeCodeDuplicate)
                {
                    List<object> listParam = new List<object>();
                    listParam.Add("Employee_Label_EmployeeCode");
                    throw new BusinessException("Common_Validate_Duplicate", listParam);
                }
            }

            /*End DungVA - 07/06/2013 check trùng mã cán bộ */

            //StaffPosition: require
            Utils.ValidateRequire(insertEmployee.StaffPositionID, "Employee_Label_StaffPosition");
            //SupervisingDeptID: require
            Utils.ValidateRequire(insertEmployee.SupervisingDeptID, "Employee_Label_SupervisingDept");
            //ReligionID: require
            Utils.ValidateRequire(insertEmployee.ReligionID, "Employee_Label_Religion");
            //Validate(Entity);
            Validate(insertEmployee);
            //Gán: Employee.EmployeeType =  EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            insertEmployee.EmployeeType = GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF;
            //Employee.EmploymentStatus = EMPLOYMENT_STATUS_WORKING
            insertEmployee.EmploymentStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            insertEmployee.IsActive = true;
            return base.Insert(insertEmployee);
        }
        #endregion

        #region update
        /// <summary>
        /// Sửa hồ sơ cán bộ
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertEmployee">Đối tượng Update</param>
        /// <returns></returns>
        public Employee UpdateTeacher(Employee insertEmployee, bool forImport = false)
        {

            //Trường học không chứa cấp tương ứng: kiểm tra SchoolProfile(SchoolID).EducationGrade với AppliedLevel
            //bang hien tai dang thieu truong applied level
            SchoolProfile sf = SchoolProfileRepository.Find(insertEmployee.SchoolID);
            int appliedLevel = insertEmployee.AppliedLevel ?? 0;
            if (appliedLevel > 0 && !SchoolProfileBusiness.GetListAppliedLevel(sf.SchoolProfileID).Contains(appliedLevel))
            {

                throw new BusinessException("Common_Validate_AppliedLevel");

            }



            //EmployeeCode: require, duplicate, maxlength(30)
            //SchoolFacultyID: require
            //SchoolID: require
            //EthnicID: require
            //EmployeeCode: require, duplicate, maxlength(30)
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_EmployeeCode");
            Utils.ValidateMaxLength(insertEmployee.EmployeeCode, EMPLOYEE_TEACHERTYPE_CODE_MAXLENGTH, "Employee_Label_EmployeeCode");
            //this.CheckDuplicate(insertEmployee.EmployeeCode, GlobalConstants.SCHOOL_SCHEMA, "Employee", "EmployeeCode", true, insertEmployee.EmployeeID, "Employee_Label_EmployeeCode");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode}
                    ,{"SchoolID",insertEmployee.SchoolID}
                    ,{"IsActive",true}
                }, new Dictionary<string, object>()
                {
                {"EmployeeID",insertEmployee.EmployeeID}
                });
                if (Exist)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "Employee_Label_EmployeeCode" });
                }
            }
            //SchoolFacultyID: require
            Utils.ValidateRequire(insertEmployee.SchoolFacultyID, "Employee_Label_SchoolFacultyID");
            //SchoolID: require
            Utils.ValidateRequire(insertEmployee.SchoolID, "SchoolProfile_Label_SchoolID");

            //Validate(Entity);
            Validate(insertEmployee, forImport);
            int partitionId = UtilsBusiness.GetPartionId(insertEmployee.SchoolID.Value);
            //EmployeeID, SchoolID: not compatible(Employee)
            if (insertEmployee.SchoolID.HasValue)
            {
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployee.SchoolID},
                    {"EmployeeID",insertEmployee.EmployeeID}
                    ,{"IsActive",true}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            //Kiểm tra EmployeeID có tồn tại hay không?
            CheckAvailable(insertEmployee.EmployeeID, "Employee_Label_EmployeeID");

            insertEmployee.EmployeeType = GlobalConstants.EMPLOYEE_TYPE_TEACHER;
            TrainingLevel tl = TrainingLevelBusiness.Find(insertEmployee.TrainingLevelID);
            if (tl != null)
            {
                GraduationLevel gl = GraduationLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true }, { "Resolution", tl.Resolution }, { "AppliedLevel", insertEmployee.AppliedLevel } }).FirstOrDefault();
                if (gl != null)
                {
                    insertEmployee.GraduationLevelID = gl.GraduationLevelID;
                }
            }


            //Lấy bản ghi ở TeacherOfFacultyBusiness.SearchBySchool(Entity.SchoolID, Dictionary)
            //cập nhật lại giá trị FacultyID = Entity.FacultyID
            //+ Dictionary[“TeacherID”] = Entity.EmployeeID
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["TeacherID"] = insertEmployee.EmployeeID;
            var lstof = from t in TeacherOfFacultyRepository.All
                        where t.TeacherID == insertEmployee.EmployeeID
                             && t.SchoolFaculty.SchoolID == insertEmployee.SchoolID
                             && t.IsActive == true
                        select t;
            int employeeID = insertEmployee.EmployeeID;
            int schoolID = insertEmployee.SchoolID.Value;
            int? schoolFacultyID = EmployeeBusiness.All.Where(o => o.EmployeeID == employeeID).Select(o => o.SchoolFacultyID).FirstOrDefault();
            int SchoolFacultyID = insertEmployee.SchoolFacultyID.Value;
            TeacherOfFaculty tof = lstof.FirstOrDefault();
            if (SchoolFacultyID != schoolFacultyID)
            {
                if (tof != null)
                {
                    tof.FacultyID = insertEmployee.SchoolFacultyID.Value;
                    TeacherOfFacultyBusiness.Update(tof);
                }
                //Lấy dữ liệu trong bảng ConccurrentWorkAssignment
                List<ConcurrentWorkAssignment> lstConcurrentWorkAssignment = ConcurrentWorkAssignmentBusiness.All.Where(o => o.TeacherID == employeeID && o.SchoolID == schoolID && o.IsActive == true).ToList();
                foreach (ConcurrentWorkAssignment concurrentWorkAssignment in lstConcurrentWorkAssignment)
                {
                    concurrentWorkAssignment.FacultyID = SchoolFacultyID;
                    ConcurrentWorkAssignmentBusiness.BaseUpdate(concurrentWorkAssignment);
                }
                //Lấy dữ liệu trong bảng ConcurrentWorkReplacement
                List<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.All.Where(o => o.Employee1.EmployeeID == employeeID && o.AcademicYear.SchoolID == schoolID && o.IsActive == true).ToList();
                foreach (ConcurrentWorkReplacement concurrentWorkReplacement in lstConcurrentWorkReplacement)
                {
                    concurrentWorkReplacement.SchoolFacultyID = SchoolFacultyID;
                    ConcurrentWorkReplacementBusiness.BaseUpdate(concurrentWorkReplacement);
                }
                //Lấy dữ liệu cần update trong bảng TeachingAssignment
                List<TeachingAssignment> lstTeachingAssignment = TeachingAssignmentBusiness.All.Where(o => o.TeacherID == employeeID && o.AcademicYear.SchoolID == schoolID && o.IsActive == true && o.Last2digitNumberSchool == partitionId).ToList();
                foreach (TeachingAssignment teachingAssignment in lstTeachingAssignment)
                {
                    teachingAssignment.FacultyID = SchoolFacultyID;
                    TeachingAssignmentBusiness.BaseUpdate(teachingAssignment);
                }
                //Lấy dữ liệu cần update trong bảng TeacherGrading
                List<TeacherGrading> lstTeacherGrading = TeacherGradingBusiness.All.Where(o => o.TeacherID == employeeID && o.AcademicYear.SchoolID == schoolID).ToList();
                foreach (TeacherGrading teacherGrading in lstTeacherGrading)
                {
                    teacherGrading.SchoolFacultyID = SchoolFacultyID;
                    TeacherGradingBusiness.BaseUpdate(teacherGrading);
                }
                //Lấy dữ liệu cần update trong bảng EmployeePraiseDiscipline
                List<EmployeePraiseDiscipline> lstEmployeePraiseDiscipline = EmployeePraiseDisciplineBusiness.All.Where(o => o.EmployeeID == employeeID && o.SchoolID == schoolID).ToList();
                foreach (EmployeePraiseDiscipline employeePraiseDiscipline in lstEmployeePraiseDiscipline)
                {
                    employeePraiseDiscipline.SchoolFacultyID = SchoolFacultyID;
                    EmployeePraiseDisciplineBusiness.BaseUpdate(employeePraiseDiscipline);
                }
                //Lấy dữ liệu cần update trong bảng TeachingExperience
                List<TeachingExperience> lstTeachingExperience = TeachingExperienceBusiness.All.Where(o => o.TeacherID == employeeID && o.SchoolID == schoolID).ToList();
                foreach (TeachingExperience teachingExperience in lstTeachingExperience)
                {
                    teachingExperience.FacultyID = SchoolFacultyID;
                    TeachingExperienceBusiness.BaseUpdate(teachingExperience);
                }
                //Lấy dữ liệu cần update trong bảng HonourAchivement
                List<HonourAchivement> lstHonourAchivement = HonourAchivementBusiness.All.Where(o => o.EmployeeID == employeeID && o.SchoolID == schoolID).ToList();
                foreach (HonourAchivement honourAchivement in lstHonourAchivement)
                {
                    honourAchivement.FacultyID = SchoolFacultyID;
                    HonourAchivementBusiness.BaseUpdate(honourAchivement);
                }
                //Update thông tin vào bảng EmployeeWorkMovement
                List<EmployeeWorkMovement> lstEmployeeWorkMovement = EmployeeWorkMovementBusiness.All.Where(o => o.TeacherID == employeeID && o.FromSchoolID == schoolID).ToList();
                foreach (EmployeeWorkMovement employeeWorkMovement in lstEmployeeWorkMovement)
                {
                    employeeWorkMovement.FacultyID = SchoolFacultyID;
                    EmployeeWorkMovementBusiness.BaseUpdate(employeeWorkMovement);
                }

            }

            // QuangNN2 - Cap nhat thuoc tinh SynchronizeID = -2
            UserAccountBusiness.UpdateSynchronize(insertEmployee.EmployeeID, GlobalConstants.STATUS_UPDATE_INFO);

            EmployeeHistoryStatus ehs = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == insertEmployee.EmployeeID && o.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING && o.SchoolID == insertEmployee.SchoolID).FirstOrDefault();
            ehs.FromDate = insertEmployee.IntoSchoolDate;
            EmployeeHistoryStatusRepository.Update(ehs);
            return base.Update(insertEmployee);
        }

        /// <summary>
        /// Cập nhật Nhân viên thuộc các đơn vị quản lý
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertEmployee">Đối tượng Update<</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public Employee UpdateEmployee(Employee insertEmployee, bool forUpdate = false)
        {
            //Mã nhân viên không được trống. Độ dài không quá 50 ký tự - Kiểm tra EmployeeCode
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_EmployeeCode");
            Utils.ValidateMaxLength(insertEmployee.EmployeeCode, EMPLOYEE_CODE_MAXLENGTH, "Employee_Label_EmployeeCode");
            //Mã nhân viên không được trùng – Kiểm tra sự tồn tại EmployeeCode trong bảng Employee
            //this.CheckDuplicate(insertEmployee.EmployeeCode, GlobalConstants.SCHOOL_SCHEMA, "Employee", "EmployeeCode", true, insertEmployee.EmployeeID, "Employee_Label_EmployeeCode");
            //{
            //    bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
            //            new Dictionary<string, object>()
            //    {
            //        {"EmployeeCode",insertEmployee.EmployeeCode}
            //        ,{"SchoolID",insertEmployee.SchoolID}
            //        ,{"IsActive",true}
            //    }, new Dictionary<string, object>()
            //    {
            //    {"EmployeeID",insertEmployee.EmployeeID}    
            //    });
            //    if (Exist)
            //    {
            //        throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "Employee_Label_EmployeeCode" });
            //    }
            //}

            /*DungVA - 12/06/2013 check trùng mã cán bộ */
            if (forUpdate != true)
            {
                if (insertEmployee.SchoolID == null)
                {
                    IDictionary<string, object> expDic = null;
                    if (insertEmployee.EmployeeID > 0)
                    {
                        expDic = new Dictionary<string, object>();
                        expDic["EmployeeID"] = insertEmployee.EmployeeID;
                    }
                    bool EmployeeCodeDuplicate = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                           new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode},
                    {"SupervisingDeptID",insertEmployee.SupervisingDeptID},
                    {"IsActive",true}
                }, expDic);
                    if (EmployeeCodeDuplicate)
                    {
                        List<object> listParam = new List<object>();
                        listParam.Add("Employee_Label_EmployeeCode");
                        throw new BusinessException("Common_Validate_Duplicate", listParam);
                    }
                }
            }


            /*End DungVA - 12/06/2013 check trùng mã cán bộ */

            //StaffPosition: require
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_StaffPosition");
            //SupervisingDeptID: require
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_SupervisingDept");
            //ReligionID: require
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_Religion");
            if (forUpdate != true)
            {
                if (insertEmployee.SupervisingDeptID.HasValue)
                {
                    //EmployeeID, SupervisingDeptID: not compatible(Employee)
                    bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                            new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",insertEmployee.SupervisingDeptID},
                    {"EmployeeID",insertEmployee.EmployeeID}
                }, null);
                    if (!EmployeeCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
            }


            insertEmployee.IsActive = true;
            //EmployeeID: PK(Employee) với rằng buộc EmployeeType =  EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            {
                bool EmployeeIDExist = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                       new Dictionary<string, object>()
                {
                    {"EmployeeType",GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF},
                    {"EmployeeID",insertEmployee.EmployeeID}
                    ,{"IsActive",true}
                }, null);
                if (!EmployeeIDExist)
                {
                    List<object> Params = new List<object>();
                    Params.Add("Employee_Label_EmployeeID");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);

                }
            }
            //Validate(Entity);
            Validate(insertEmployee);
            //Gán: Employee.EmployeeType =  EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            insertEmployee.EmployeeType = GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF;
            //Employee.EmploymentStatus = EMPLOYMENT_STATUS_WORKING
            insertEmployee.EmploymentStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;

            // QuangNN2 - Cap nhat thuoc tinh SynchronizeID = -2
            UserAccountBusiness.UpdateSynchronize(insertEmployee.EmployeeID, GlobalConstants.STATUS_UPDATE_INFO);

            return base.Update(insertEmployee);
        }
        #endregion

        #region Delete
        /// <summary>
        ///Xóa hồ sơ cán bộ
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="EmployeeId">ID cán bộ</param>
        public void DeleteTeacher(int EmployeeId, int SchoolID)
        {
            // Kiểm tra đang tồn tại Cán bộ và chưa xóa (isActive = false)
            CheckAvailable(EmployeeId, "Employee_Label_EmployeeID");

            //EmployeeID: using(UserAccount) - AnhVD9 - DeActive UserAccount nếu có
            UserAccountBusiness.DeActiveByEmployeeID(EmployeeId);

            //EmployeeID: using(TeachingAssignment – TeacherID) - AnhVD9 - DeActive các thông tin liên quan tới Phân công giảng dạy
            TeachingAssignmentBusiness.Delete(new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"TeacherID", EmployeeId}
                });

            //EmployeeID: using(ClassSupervisorAssignment – TeacherID) -  AnhVD9 - Xóa các thông tin liên quan tới Phân công Giáo vụ
            ClassSupervisorAssignmentBusiness.DeleteByTeacherID(new Dictionary<string, object>()
                {
                    {"SchoolID", SchoolID},
                    {"TeacherID",EmployeeId}
                });


            //EmployeeID: using(ConcurrentWorkAssignment – TeacherID) - AnhVD9 - Xóa các thông tin liên quan tới Phân công chủ nhiệm
            ConcurrentWorkAssignmentBusiness.Delete(new Dictionary<string, object>()
                {
                    {"SchoolID", SchoolID},
                    {"TeacherID",EmployeeId}
                });


            //EmployeeID: using(ConcurrentWorkReplacement – TeacherID) -  AnhVD9 - Xóa các thông tin liên quan tới làm thay chủ nhiệm
            ConcurrentWorkReplacementBusiness.Delete(new Dictionary<string, object>()
                {
                    {"SchoolID", SchoolID},
                    {"TeacherID",EmployeeId}
                });


            //EmployeeID: using(ConcurrentWorkReplacement – TeacherID) -  AnhVD9 - Xóa các thông tin liên quan tới làm thay chủ nhiệm
            ConcurrentWorkReplacementBusiness.Delete(new Dictionary<string, object>()
                {
                    {"SchoolID", SchoolID},
                    {"ReplacedTeacherID",EmployeeId}
                });

            // EmployeeID: using(ClassProfile - HeadTecherID) - AnhVD9 - Xóa thông tin GVCN
            ClassProfileBusiness.RemoveHeadTeacherInClass(new Dictionary<string, object>()
                {
                    {"SchoolID", SchoolID},
                    {"HeadTeacherID", EmployeeId}
                });
            //EmployeeID: using(EmployeeWorkMovement – TeacherID) -  AnhVD9 - Xóa các thông tin liên quan tới Chuyển trường
            //EmployeeID: using(EmployeeWorkingHistory – TeacherID) -  AnhVD9 - Xóa các thông tin liên quan tới Lịch sử công tác

            // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh SynchronizeID = -3
            UserAccountBusiness.UpdateSynchronize(EmployeeId, GlobalConstants.STATUS_DELETE_USER);

            // Tiến hành DeActive Giáo viên
            base.Delete(EmployeeId, true);
        }

        /// <summary>
        /// Xóa Nhân viên thuộc các đơn vị quản lý
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="EmployeeId">ID Nhân viên thuộc các đơn vị quản lý</param>
        public void DeleteEmployee(int SupervisingDeptID, int EmployeeId)
        {
            //Bạn chưa chọn Nhân viên cần xóa hoặc Nhân viên bạn chọn đã bị xóa khỏi hệ thống
            CheckAvailable(EmployeeId, "Employee_Label_EmployeeID");

            //EmployeeID, SupervisingDeptID: not compatible(Employee)
            bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SupervisingDeptID},
                {"EmployeeID", EmployeeId},
                {"IsActive", true}
                }, null);
            if (!EmployeeCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh SynchronizeID = -3
            UserAccountBusiness.UpdateSynchronize(EmployeeId, GlobalConstants.STATUS_DELETE_USER);

            base.Delete(EmployeeId, true);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm hồ sơ cán bộ
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<Employee> Search(IDictionary<string, object> dic)
        {

            int CurrentSchoolID = Utils.GetInt(dic, "CurrentSchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int CurrentEmployeeStatus = Utils.GetInt(dic, "CurrentEmployeeStatus");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");

            int EmploymentStatus = Utils.GetInt(dic, "EmploymentStatus");
            string EmployeeCode = Utils.GetString(dic, "EmployeeCode", null);

            string FullName = Utils.GetString(dic, "FullName", null);
            DateTime? BirthDate = Utils.GetDateTime(dic, "BirthDate");

            bool? Genre = Utils.GetNullableBool(dic, "Genre");
            int TrainingLevelID = Utils.GetInt(dic, "TrainingLevelID");
            int ContractTypeID = Utils.GetInt(dic, "ContractTypeID");
            int ITQualificationLevelID = Utils.GetInt(dic, "ITQualificationLevelID");
            int QualificationLevelID = Utils.GetInt(dic, "QualificationLevelID");

            int StaffPositionID = Utils.GetInt(dic, "StaffPosition");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int WorkTypeID = Utils.GetInt(dic, "WorkTypeID");
            int WorkGroupTypeID = Utils.GetInt(dic, "WorkGroupTypeID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int ParentID = Utils.GetInt(dic, "ParentID");
            string TraversalPath = Utils.GetString(dic, "TraversalPath");
            int UserID = Utils.GetInt(dic, "UserID");

            string FacultyName = Utils.GetString(dic, "FacultyName");
            bool? isFromEduActive = Utils.GetNullableBool(dic, "isFromEduActive");
            string LoginName = Utils.GetString(dic, "LoginName");
            int GroupCatID = Utils.GetInt(dic, "GroupCatID");

            int EmployeeType = Utils.GetInt(dic, "EmployeeType");
            string PermanentResidentalAddress = Utils.GetString(dic, "PermanentResidentalAddress");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            IQueryable<Employee> lsEmployee = this.EmployeeBusiness.All;

            if (EmployeeID != 0)
            {
                lsEmployee = lsEmployee.Where(p => p.EmployeeID == EmployeeID);
            }
            if (isFromEduActive.HasValue)
            {
                lsEmployee = lsEmployee.Where(em => (em.IsActive == isFromEduActive));
            }
            else
            {
                //mac dinh isActive = true
                lsEmployee = lsEmployee.Where(em => (em.IsActive == true));
            }

            //bat dau tim kiem
            if (CurrentSchoolID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.SchoolID == CurrentSchoolID));
            }

            if (StaffPositionID != 0)
            {
                lsEmployee = lsEmployee.Where(o => o.StaffPositionID == StaffPositionID);
            }
            if (CurrentEmployeeStatus != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.EmploymentStatus == CurrentEmployeeStatus));
            }

            // SchoolID: default = 0; 0 => All ~ tìm kiếm exsits trong EmployeeHistoryStatus ehs 
            //    với ehs.Employee = e.EmployeeID and ehs.SchoolID = SchoolID
            if (SchoolID != 0)
            {
                var lstHistory = EmployeeHistoryStatusBusiness.All.Where(s => s.SchoolID == SchoolID);
                lsEmployee = from em in lsEmployee
                             where lstHistory.Any(o => o.EmployeeID == em.EmployeeID)
                             select em;

            }


            //-	AcademicYearID: default = 0; 0 => All ~ từ AcademicYearID lấy được AcademicYear ay; 
            //tìm kiếm exsits trong EmployeeHistoryStatus ehs với ehs.Employee = e.EmployeeID 
            //and ehs.SchoolID = ay.SchoolID and ehs.FromDate  <= ay.SecondSemesterEndDate 
            //and ehs.ToDate >= ay.FirstSemesterStartDate
            if (AcademicYearID > 0)
            {
                AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
                var lstHistory = EmployeeHistoryStatusBusiness.All.Where(s =>
                    s.SchoolID == ay.SchoolID
                    && s.FromDate < ay.SecondSemesterEndDate
                    && (s.ToDate == null || s.ToDate > ay.FirstSemesterStartDate)
                    );

                lsEmployee = from em in lsEmployee
                             where lstHistory.Any(o => o.EmployeeID == em.EmployeeID)
                             select em;

            }

            //-	EmployeeStatus: default = 0; 0 => All ~ tìm kiếm exsits trong EmployeeHistoryStatus ehs 
            //  với ehs.Employee = e.EmployeeID and ehs.SchoolID = SchoolID and ehs.EmployeeStatus = EmployeeStatus
            if (EmploymentStatus != 0)
            {
                //Fix time out
                var lstHistory = EmployeeHistoryStatusBusiness.All.Where(a => a.SchoolID == SchoolID);
                var lsHis = lstHistory.Where(o => o.FromDate == lstHistory.Where(u => u.EmployeeID == o.EmployeeID)
                    .Max(v => v.FromDate)).Where(o => o.EmployeeStatus == EmploymentStatus);

                lsEmployee = from em in lsEmployee
                             where lsHis.Any(o => o.EmployeeID == em.EmployeeID)
                             select em;

            }


            if ((EmployeeCode != null) && (EmployeeCode.Trim().Length != 0))
            {
                lsEmployee = lsEmployee.Where(em => (em.EmployeeCode.ToUpper().Contains(EmployeeCode.ToUpper())));
            }

            if ((FullName != null) && (FullName.Trim().Length != 0))
            {
                lsEmployee = lsEmployee.Where(em => (em.FullName.ToUpper().Contains(FullName.ToUpper())));
            }

            if (BirthDate.HasValue)
            {
                lsEmployee = lsEmployee.Where(em => em.BirthDate == BirthDate);
            }

            if (Genre.HasValue)
            {
                lsEmployee = lsEmployee.Where(em => em.Genre == Genre);
            }

            if (TrainingLevelID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.TrainingLevelID == TrainingLevelID));
            }

            if (ContractTypeID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.ContractTypeID == ContractTypeID));
            }

            if (ITQualificationLevelID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.ITQualificationLevelID == ITQualificationLevelID));
            }


            if (QualificationLevelID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.QualificationLevelID == QualificationLevelID));
            }

            //-	FacultyID: Default = 0; 0 => All ~ tìm kiếm exsits trong TeacherOfFaculty tf
            //với  tf.TeacherID = e.EmployeeID and tf.FacultyID = FacultyID 
            if (FacultyID != 0)
            {
                //var listTeacherOfFaculty = TeacherOfFacultyRepository.All.Where(s => s.FacultyID == FacultyID);

                lsEmployee = from em in lsEmployee
                             where em.SchoolFacultyID == FacultyID // listTeacherOfFaculty.Any(o=> o.TeacherID == em.EmployeeID)
                             select em;

            }

            if (WorkTypeID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.WorkTypeID == WorkTypeID));
            }
            if (WorkGroupTypeID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.WorkType.WorkGroupTypeID == WorkGroupTypeID));
            }
            if (SupervisingDeptID != 0 && TraversalPath.Length == 0)
            {
                //string temp = "\\" + SupervisingDeptID + "\\";
                //lsEmployee = lsEmployee.Where(em => (em.SupervisingDept.TraversalPath.Contains(temp)));
                lsEmployee = lsEmployee.Where(em => (em.SupervisingDeptID == SupervisingDeptID));
            }

            //if (UserID != 0)
            //{
            //    lsEmployee = lsEmployee.Where(em => (em. == UserID));
            //}


            //FacultyName: default = “”, “” -> All ~ tìm kiếm exsits trong TeacherOfFaculty tf 
            //với  tf.TeacherID = e.EmployeeID and SchoolFaculty(tf.FacultyID).FacultyName like “%” + FacultyName + “%”
            // DungVA - tìm kiếm theo các cấp quản lý và cấp con
            if (TraversalPath.Trim().Length != 0 && SupervisingDeptID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.SupervisingDept.TraversalPath.Contains(TraversalPath)) || em.SupervisingDeptID == SupervisingDeptID);
            }
            //End DungVa

            //them moi day
            if (FacultyName.Trim().Length != 0)
            {

                var listTeacherOfFaculty = TeacherOfFacultyRepository.All.Where(s =>
                    s.SchoolFaculty.FacultyName.ToUpper().Contains(FacultyName.ToUpper())
                    );

                lsEmployee = from em in lsEmployee
                             where listTeacherOfFaculty.Any(o => o.TeacherID == em.EmployeeID)
                             select em;

            }

            if (EmployeeType != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.EmployeeType == EmployeeType));
            }
            if (LoginName.Trim().Length != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.UserAccounts.FirstOrDefault() != null));
                lsEmployee = lsEmployee.Where(em => (em.UserAccounts.FirstOrDefault().aspnet_Users.UserName.ToUpper().Contains(LoginName.ToUpper())));
            }

            if (GroupCatID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.UserAccounts.FirstOrDefault() != null));
                lsEmployee = lsEmployee.Where(em => (em.UserAccounts.FirstOrDefault().UserAccountID == GroupCatID));
            }

            if ((PermanentResidentalAddress != null) && (PermanentResidentalAddress.Trim().Length != 0))
            {
                lsEmployee = lsEmployee.Where(em => (em.PermanentResidentalAddress.ToUpper().Contains(PermanentResidentalAddress.ToUpper())));
            }

            if (AppliedLevelID > 0)
            {
                lsEmployee = lsEmployee.Where(em => em.AppliedLevel == AppliedLevelID);
            }

            return lsEmployee;
        }
        /// <summary>
        /// Tìm kiếm giáo viên
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="SchoolID">tham số</param>
        /// <param name="dic">Danh sách kết quả tìm kiếm</param>
        /// <returns></returns>
        public IQueryable<Employee> SearchTeacher(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;
                dic["EmployeeType"] = GlobalConstants.EMPLOYEE_TYPE_TEACHER;
                return Search(dic);
            }
        }

        public IQueryable<Employee> SearchEmployee(int SupervisingDeptID, IDictionary<string, object> dic)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("vi-VI");
            if (SupervisingDeptID == 0)
            {
                return null;
            }
            else
            {
                dic["SupervisingDeptID"] = SupervisingDeptID;
                dic["EmployeeType"] = GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF;
                return Search(dic);
            }
        }

        /// <summary>
        /// Tìm kiếm giáo viên
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="FalcultyID"></param>
        /// <returns></returns>
        public IQueryable<Employee> SearchWorkingTeacherByFaculty(int SchoolID, int FalcultyID = 0)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["CurrentSchoolID"] = SchoolID;
                SearchInfo["EmployeeType"] = GlobalConstants.EMPLOYEE_TYPE_TEACHER;
                SearchInfo["FacultyID"] = FalcultyID;
                SearchInfo["CurrentEmployeeStatus"] = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Tìm kiếm giáo viên trên giao diện Phòng/Sở
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="SupervisingDeptID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EmployeeCode"></param>
        /// <param name="FullName"></param>
        /// <param name="Genre"></param>
        /// <param name="BirthDate"></param>
        /// <param name="SchoolFacultyID"></param>
        /// <param name="WorkTypeID"></param>
        /// <param name="ContractID"></param>
        /// <param name="HierachyLevel"></param>
        /// <returns></returns>
        public IQueryable<Employee> SearchTeacherBySupervisingDept(Dictionary<string, object> dic)
        {

            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EmployeeStatus = Utils.GetInt(dic, "EmployeeStatus");
            string EmployeeCode = Utils.GetString(dic, "EmployeeCode", null);
            string FullName = Utils.GetString(dic, "FullName", null);
            DateTime? BirthDate = Utils.GetDateTime(dic, "BirthDate");

            int Genre = Utils.GetInt(dic, "Genre");
            int ContractTypeID = Utils.GetInt(dic, "ContractTypeID");
            int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
            int WorkTypeID = Utils.GetInt(dic, "WorkTypeID");
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            bool isSub = Utils.GetBool(dic, "IsSub");
            int grade = AppliedLevel > 0 ? Utils.GradeToBinary(AppliedLevel) : 0;
            //Nếu HierachyLevel = 3, ProvinceID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE) && (ProvinceID == 0))
            {
                return null;
            }
            //Nếu HierachyLevel = 5, SupervisingDeptID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE) && (SupervisingDeptID == 0))
            {
                return null;
            }

            var query = this.EmployeeRepository.All;
            //mac dinh isActive = true
            query = query.Where(em => (em.IsActive == true));
            if (ProvinceID != 0)
            {
                query = query.Where(p => p.SchoolProfile.ProvinceID == ProvinceID);
                //var lstHistory = EmployeeHistoryStatusRepository.All.Where(s => s.SchoolProfile.ProvinceID == ProvinceID);
                /*query = from em in query
                        where lstHistory.Any(o => o.EmployeeID == em.EmployeeID)
                        select em;*/
                //query = query.Where(o => (o.ProvinceID == ProvinceID));
            }
            if (DistrictID != 0 && isSub == true)
            {
                query = query.Where(p => p.SchoolProfile.DistrictID == DistrictID);
                //var lstHistory = EmployeeHistoryStatusRepository.All.Where(s => s.SchoolProfile.DistrictID == DistrictID);
                /*query = from em in query
                        where lstHistory.Any(o => o.EmployeeID == em.EmployeeID)
                        select em;*/
                //query = query.Where(o => (o.DistrictID == DistrictID));
            }
            if (SupervisingDeptID != 0)
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
                if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                query = query.Where(p => p.SchoolProfile.SupervisingDeptID == SupervisingDeptID || p.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID);
                //var lstHistory = EmployeeHistoryStatusRepository.All.Where(s => s.SchoolProfile.SupervisingDeptID == SupervisingDeptID
                //            || s.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID);
                /*query = from em in query
                        where lstHistory.Any(o => o.EmployeeID == em.EmployeeID)
                        select em;*/
                //query = query.Where(o => (o.SupervisingDeptID == SupervisingDeptID));
            }

            // Tìm kiếm theo cấp của trường
            if (grade > 0)
            {
                query = query.Where(o => (o.SchoolProfile.EducationGrade & grade) != 0);
            }

            if (SchoolID != 0)
            {
                query = query.Where(o => (o.SchoolID == SchoolID));
            }
            if (EmployeeCode != null && EmployeeCode != "")
            {
                query = query.Where(o => (o.EmployeeCode.ToUpper().Contains(EmployeeCode.ToUpper())));
            }
            if (FullName != null && FullName.Trim() != "")
            {
                query = query.Where(o => (o.FullName.ToUpper().Contains(FullName.ToUpper())));
            }
            if (Genre != -1)
            {
                if (Genre == 0)
                {
                    query = query.Where(o => (o.Genre == false));
                }
                else
                {
                    query = query.Where(o => (o.Genre == true));
                }
            }
            if (BirthDate.HasValue)
            {
                query = query.Where(o => EntityFunctions.TruncateTime(o.BirthDate) == EntityFunctions.TruncateTime(BirthDate));
            }
            if (SchoolFacultyID != 0)
            {
                query = query.Where(o => (o.SchoolFacultyID == SchoolFacultyID));
            }
            if (WorkTypeID != 0)
            {
                query = query.Where(o => (o.WorkTypeID == WorkTypeID));
            }
            if (ContractTypeID != 0)
            {
                query = query.Where(o => (o.ContractTypeID == ContractTypeID));
            }
            if (EmployeeStatus != 0)
            {
                //var lstHistory = EmployeeHistoryStatusBusiness.All;
                //var lsHis = lstHistory
                //            .Where(o => o.FromDate == lstHistory.Where(u => u.EmployeeID == o.EmployeeID).Max(v => v.FromDate)
                //                        && o.EmployeeStatus == EmployeeStatus);
                query = query.Where(p => p.EmploymentStatus == EmployeeStatus);
                //query = from em in query
                //        join eh in lsHis on em.EmployeeID equals eh.EmployeeID
                //        select em;
                // query = query.Where(o => (o.EmploymentStatus == EmployeeStatus));
            }


            //-	e.IsActive = true
            //-	e.EmployeeType = 1 (Giáo viên)

            query = query.Where(o => (o.IsActive == true)).Where(o => (o.EmployeeType == 1));

            

            return query;
        }

        /// <summary>
        /// Tìm kiếm giáo viên trên giao diện Phòng/Sở 21/06/2018
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="SupervisingDeptID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EmployeeCode"></param>
        /// <param name="FullName"></param>
        /// <param name="Genre"></param>
        /// <param name="BirthDate"></param>
        /// <param name="SchoolFacultyID"></param>
        /// <param name="WorkTypeID"></param>
        /// <param name="ContractID"></param>
        /// <param name="HierachyLevel"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public IQueryable<EmployeeBO> SearchTeacherBySupervisingDeptCustom(Dictionary<string, object> dic)
        {

            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EmployeeStatus = Utils.GetInt(dic, "EmployeeStatus");
            string EmployeeCode = Utils.GetString(dic, "EmployeeCode", null);
            string FullName = Utils.GetString(dic, "FullName", null);
            DateTime? BirthDate = Utils.GetDateTime(dic, "BirthDate");
            int Year = Utils.GetInt(dic, "Year");
            int Genre = Utils.GetInt(dic, "Genre");
            int ContractTypeID = Utils.GetInt(dic, "ContractTypeID");
            int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
            int WorkTypeID = Utils.GetInt(dic, "WorkTypeID");
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            bool isSub = Utils.GetBool(dic, "IsSub");
            int grade = AppliedLevel > 0 ? Utils.GradeToBinary(AppliedLevel) : 0;
            //Nếu HierachyLevel = 3, ProvinceID = 0 thì trả về null
            //if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE) && (ProvinceID == 0))
            //{
            //    return null;
            //}
            ////Nếu HierachyLevel = 5, SupervisingDeptID = 0 thì trả về null
            //if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE) && (SupervisingDeptID == 0))
            //{
            //    return null;
            //}

            

            //var query = this.EmployeeRepository.All.Where(x=>x.);
            var query = from ehs in EmployeeHistoryStatusBusiness.All
                        join e in EmployeeBusiness.All.Where(x=>x.IsActive == true) on ehs.EmployeeID equals e.EmployeeID
                        join ac in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.Year == Year) on e.SchoolID equals ac.SchoolID
                        join p in ProvinceBusiness.All.Where(p => p.IsActive == true) on e.ProvinceId equals p.ProvinceID into empp
                        from ep in empp.DefaultIfEmpty()
                        join d in DistrictBusiness.All.Where(d => d.IsActive == true) on e.DistrictId equals d.DistrictID into empd
                        from ed in empd.DefaultIfEmpty()
                        join c in CommuneBusiness.All.Where(c => c.IsActive == true) on e.CommuneId equals c.CommuneID into empc
                        from ec in empc.DefaultIfEmpty()
                        //join es in EmployeeSalaryBusiness.All on e.EmployeeID equals es.EmployeeID into ess
                        //from esss in ess.DefaultIfEmpty()
                        where ehs.FromDate <= ac.SecondSemesterEndDate && (ehs.ToDate == null || ehs.ToDate >= ac.FirstSemesterStartDate || ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_BREATHER || ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                        select new EmployeeBO
                        {
                            EmployeeCode = e.EmployeeCode,
                            FullName = e.FullName,
                            SchoolName = e.SchoolProfile.SchoolName,
                            SyncCode = ac.SchoolProfile.SyncCode,
                            BirthDate = e.BirthDate,
                            Genre = e.Genre,
                            IdentifyNumber = e.IdentityNumber,
                            IdentityIssuedDate = e.IdentityIssuedDate,
                            IdentityIssuedPlace = e.IdentityIssuedPlace,
                            Mobile = e.Mobile,
                            Telephone = e.Telephone,
                            Email = e.Email,
                            EthnicName = e.Ethnic.EthnicName,
                            ReligionName = e.Religion.Resolution,
                            HomeTown = e.HomeTown,
                            ProvinceName = ep.ProvinceName,
                            DistrictName = ed.DistrictName,
                            CommuneName =  ec.CommuneName,
                            IsYouthLeageMember = e.IsYouthLeageMember,
                            YouthLeagueJoinedDate = e.YouthLeagueJoinedDate,
                            YouthLeagueJoinedPlace = e.YouthLeagueJoinedPlace,
                            IsCommunistPartyMember = e.IsCommunistPartyMember,
                            CommunistPartyJoinedDate = e.CommunistPartyJoinedDate,
                            CommunistPartyJoinedPlace = e.CommunistPartyJoinedPlace,
                            InsuranceNumber = e.InsuranceNumber,
                            IsSyndicate = e.IsSyndicate,
                            SyndicateDate = e.SyndicateDate,
                            Alias = e.Alias,
                            SchoolFacultyName = e.SchoolFaculty.FacultyName,
                            IntoSchoolDate = e.IntoSchoolDate,
                            PermanentResidentalAddress = e.PermanentResidentalAddress,
                            HealthStatus = e.HealthStatus,
                            WorkGroupTypeName = e.WorkGroupType.Resolution,
                            WorkTypeName = e.WorkType.Resolution,
                            ContractTypeName = e.ContractType.Resolution,
                            JoinedDate = e.JoinedDate,
                            StartingDate = e.StartingDate,
                            EmployedBy = e.EmployedBy,
                            FirstJob = e.FirstJob,
                            // ConcurentWorkID
                            ThourPerWeek = e.ThourPerWeek,
                            CoHourPerWeek = e.CoHourPerWeek,
                            Description = e.Description,
                            SoftSkillTrained = e.SoftSkillTrained,
                            DisablePupilTeaching = e.DisablePupilTeaching,
                            SectionPerDay1 = e.SectionPerDay,
                            SectionPerDay2 = e.SectionPerDay,
                            DedicatedForYoungLeague = e.DedicatedForYoungLeague,
                            //EmployeeScaleName = esss.EmployeeScale.Resolution,
                            //EmployeeScaleCode = esss.EmployeeScale.EmployeeScaleCode,
                            //SubLevel = esss.SalaryLevel.Resolution,
                            //Coefficient = esss.Coefficient,
                            //AppliedDate = esss.AppliedDate,
                            //SalaryAmount = esss.SalaryAmount,
                            VocationalAllowance = e.VocationalAllowance,
                            SeniorityAllowance = e.SeniorityAllowance,
                            PreferentialAllowance = e.PreferentialAllowance,
                            //EvaluationField3 
                            TrainingLevelResolution = e.TrainingLevel.Resolution,
                            PoliticalGradeResolution = e.PoliticalGrade.Resolution,
                            EducationalManagementGradeName = e.EducationalManagementGrade.Resolution,
                            MainForeignLanguageId = e.MainForeignLanguageId,
                            ForeignLanguageGradeName = e.ForeignLanguageGrade.Resolution,
                            ITQualificationLevelName = e.ITQualificationLevel.Resolution,
                            SpecialityCatName = e.SpecialityCat.Resolution,
                            QualificationTypeName = e.QualificationType.Resolution,
                            MainGraduationLevelID = e.MainGraduationLevelId,
                            OtherSpecialityCatID = e.OtherSpecialityCatId, 
                            OtherQualificationTypeID = e.OtherQualificationTypeId,
                            OtherGraduationLevelID = e.OtherGraduationLevelId,
                            QualificationLevelName = e.QualificationLevel.Resolution,
                            StateManagementGradeName = e.StateManagementGrade.Resolution,
                            //EvaluationField1
                            //EvaluationField2
                            //EvaluationField4
                            AppellationAward = e.HigiestTitle,
                            AppliedLevel = e.AppliedLevel,
                            PrimarilyAssignedSubject = e.SubjectCat.DisplayName,
                            FamilyTypeName = e.FamilyType.Resolution,
                            MariageStatusID = e.MariageStatus, 
                            FatherFullName = e.FatherFullName,
                            FatherBirthDate = e.FatherBirthDate,
                            FatherJob = e.FatherJob,
                            FatherWorkingPlace = e.FatherWorkingPlace,
                            MotherFullName = e.MotherFullName,
                            MotherBirthDate = e.MotherBirthDate,
                            MotherJob = e.MotherJob,
                            MotherWorkingPlace = e.MotherWorkingPlace,
                            SpouseFullName = e.SpouseFullName,
                            SpouseBirthDate = e.SpouseBirthDate,
                            SpouseJob = e.SpouseJob,
                            SpouseWorkingPlace = e.SpouseWorkingPlace,
                            Note = e.Note,
                            RegularRefresher = e.RegularRefresher,
                            MotelRoomOutsite = e.MotelRoomOutsite,
                            TeacherDuties = e.TeacherDuties,
                            NeedTeacherDuties = e.NeedTeacherDuties,



                            EmployeeID = e.EmployeeID,
                            EmployeeType = e.EmployeeType,
                            EmployeeStatus = ehs.EmployeeStatus,
                            SupervisingDeptID = e.SchoolProfile.SupervisingDeptID,
                            SupervisingDeptName = ehs.SupervisingDept.SupervisingDeptName,
                            SchoolID = ehs.SchoolID,
                            Name = e.Name,
                            GraduationLevelID = e.GraduationLevelID,
                            GraduationLevelName = e.GraduationLevel.Resolution,
                            ContractID = e.ContractID,
                            ContractName = e.Contract.ContractCode,
                            QualificationTypeID = e.QualificationTypeID,
                            SpecialityCatID = e.SpecialityCatID,
                            QualificationLevelID = e.QualificationLevelID,
                            ITQualificationLevelID = e.ITQualificationLevelID,
                            WorkTypeID = e.WorkTypeID,
                            TrainingLevelID = e.TrainingLevelID,
                            StaffPositionID = e.StaffPositionID,
                            SchoolFacultyID = e.SchoolFacultyID,
                            ContractTypeID = e.ContractTypeID,
                            WorkGroupTypeID = e.WorkGroupTypeID,
                            FromDate = ehs.FromDate,
                            ToDate = ehs.ToDate,
                            EmploymentStatus = e.EmploymentStatus,
                            SpecialityCatResolution = e.SpecialityCat.Resolution,
                            ITQualificationLevelResolution = e.ITQualificationLevel.Resolution,
                            ParentID = e.SchoolProfile.SupervisingDept.ParentID,
                            EducationGrade = e.SchoolProfile.EducationGrade,
                            ProvinceID = e.SchoolProfile.ProvinceID,
                            DistrictID = e.SchoolProfile.DistrictID,

                        };

            if (ProvinceID != 0)
            {
                query = query.Where(p => p.ProvinceID == ProvinceID);
            }
            if (DistrictID != 0)
            {
                query = query.Where(p => p.DistrictID == DistrictID);
                
            }
            //if (SupervisingDeptID != 0)
            //{
            //    SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            //    if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            //    {
            //        SupervisingDeptID = su.ParentID.Value;
            //    }
            //    query = query.Where(p => p.SupervisingDeptID == SupervisingDeptID || p.ParentID == SupervisingDeptID);
            //    //var lstHistory = EmployeeHistoryStatusRepository.All.Where(s => s.SchoolProfile.SupervisingDeptID == SupervisingDeptID
            //    //            || s.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID);
            //    /*query = from em in query
            //            where lstHistory.Any(o => o.EmployeeID == em.EmployeeID)
            //            select em;*/
            //    //query = query.Where(o => (o.SupervisingDeptID == SupervisingDeptID));
            //}

            // Tìm kiếm theo cấp của trường
            if (grade > 0)
            {
                query = query.Where(o => (o.EducationGrade & grade) != 0);
            }

            if (SchoolID != 0)
            {
                query = query.Where(o => (o.SchoolID == SchoolID));
            }
            if (EmployeeCode != null && EmployeeCode != "")
            {
                query = query.Where(o => (o.EmployeeCode.ToUpper().Contains(EmployeeCode.ToUpper())));
            }
            if (FullName != null && FullName.Trim() != "")
            {
                query = query.Where(o => (o.FullName.ToUpper().Contains(FullName.ToUpper())));
            }
            if (Genre != -1)
            {
                if (Genre == 0)
                {
                    query = query.Where(o => (o.Genre == false));
                }
                else
                {
                    query = query.Where(o => (o.Genre == true));
                }
            }
            if (BirthDate.HasValue)
            {
                query = query.Where(o => EntityFunctions.TruncateTime(o.BirthDate) == EntityFunctions.TruncateTime(BirthDate));
            }
            if (SchoolFacultyID != 0)
            {
                query = query.Where(o => (o.SchoolFacultyID == SchoolFacultyID));
            }
            if (WorkTypeID != 0)
            {
                query = query.Where(o => (o.WorkTypeID == WorkTypeID));
            }
            if (ContractTypeID != 0)
            {
                query = query.Where(o => (o.ContractTypeID == ContractTypeID));
            }
            if (EmployeeStatus != 0)
            {              
                query = query.Where(p => p.EmploymentStatus == EmployeeStatus);
            }

            query = query.Where(o => (o.EmployeeType == 1));



            return query;
        }

        public DataTable SP_TeacherInfoCustom(Dictionary<string, object> dic)
        {
            int year = Utils.GetInt(dic, "Year");
            int province = Utils.GetInt(dic, "Province");
            int district = Utils.GetInt(dic, "District");
            int educationGrade = Utils.GetInt(dic, "EducationGrade");
            int school = Utils.GetInt(dic, "School");
            int faculty = Utils.GetInt(dic, "Faculty");
            int workType = Utils.GetInt(dic, "WorkType");
            string employeeCode = Utils.GetString(dic, "EmployeeCode");
            string fullName = Utils.GetString(dic, "FullName");
            int genre = Utils.GetInt(dic, "Genre");
            int contractType = Utils.GetInt(dic, "ContractType");
            int employmentStatus = Utils.GetInt(dic, "EmploymentStatus");
            DateTime? birthDate = Utils.GetDateTime(dic, "BirthDate");

            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_EMPLOYEE_BY_SUPERVISING",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = year;
                    //cmd.Parameters.Add("P_SUPERVISING_DEPT_ID", OracleDbType.Int32).Value = superVising;
                    cmd.Parameters.Add("P_PROVINCE", OracleDbType.Int32).Value = province;
                    cmd.Parameters.Add("P_DISTRICT", OracleDbType.Int32).Value = district;
                    cmd.Parameters.Add("P_EDUCATION_GRADE", OracleDbType.Int32).Value = educationGrade;
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = school;
                    cmd.Parameters.Add("P_FACULTY_ID", OracleDbType.Int32).Value = faculty;
                    cmd.Parameters.Add("P_WORK_TYPE_ID", OracleDbType.Int32).Value = workType;
                    cmd.Parameters.Add("P_EMPLOYEE_CODE", OracleDbType.NVarchar2).Value = "%" + employeeCode + "%";
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.NVarchar2).Value = "%" + fullName + "%";
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = genre;
                    cmd.Parameters.Add("P_CONTRACT_TYPE_ID", OracleDbType.Int32).Value = contractType;
                    cmd.Parameters.Add("P_EMPLOYMENT_STATUS_ID", OracleDbType.Int32).Value = employmentStatus;
                    cmd.Parameters.Add("P_BIRTHDATE", OracleDbType.Date).Value = birthDate;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// Tìm kiếm Giao vien/Nhan vien cho nguoi dung cap Phong/So
        /// <author>BaLX</author>
        /// <date>20/12/2012</date>
        /// </summary>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<Employee> SearchByArea(IDictionary<string, object> dic)
        {

            int EmployeeType = Utils.GetInt(dic, "EmployeeType");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int EducationGrade = Utils.GetInt(dic, "EducationGrade");
            int EmployeeStatus = Utils.GetInt(dic, "EmployeeStatus");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string FullName = Utils.GetString(dic, "FullName");
            string TraversalPath = Utils.GetString(dic, "TraversalPath");
            int? Genre = Utils.GetNullableInt(dic, "Genre");

            IQueryable<Employee> lsEmployee = this.EmployeeRepository.All;
            //mac dinh isActive = true
            lsEmployee = lsEmployee.Where(em => (em.IsActive == true));

            //bat dau tim kiem
            if (EmployeeType != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.EmployeeType == EmployeeType));
            }

            if (ProvinceID != 0)
            {
                if (EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
                    lsEmployee = lsEmployee.Where(em => (em.SupervisingDept.ProvinceID == ProvinceID));
                else if (EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER)
                    lsEmployee = lsEmployee.Where(em => (em.SchoolProfile.ProvinceID == ProvinceID));
            }

            if (DistrictID != 0)
            {
                if (EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
                    lsEmployee = lsEmployee.Where(em => (em.SupervisingDept.DistrictID == DistrictID));
                else if (EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER)
                    lsEmployee = lsEmployee.Where(em => (em.SchoolProfile.DistrictID == DistrictID));
            }

            if (SchoolID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.SchoolID == SchoolID));
            }

            if (TrainingTypeID != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.SchoolProfile.TrainingTypeID == TrainingTypeID));
            }

            if (EducationGrade != 0)
            {
                lsEmployee = lsEmployee.Where(em => (em.SchoolProfile.EducationGrade & EducationGrade) != 0);
            }

            if (FullName.Trim() != "")
            {
                lsEmployee = lsEmployee.Where(o => (o.FullName.ToUpper().Contains(FullName.ToUpper())));
            }

            if (TraversalPath.Trim() != "")
            {
                lsEmployee = lsEmployee.Where(o => (o.SchoolProfile.SupervisingDept.TraversalPath.ToUpper().Contains(TraversalPath.ToUpper())));
            }

            if (Genre.HasValue)
            {
                if (Genre == 0)
                    lsEmployee = lsEmployee.Where(em => em.Genre == false);
                else
                    lsEmployee = lsEmployee.Where(em => em.Genre == true);
            }

            if (EmployeeStatus != 0)
            {
                var lstHistory = EmployeeHistoryStatusRepository.All.Where(s =>
                    s.SchoolID == SchoolID
                    && s.EmployeeStatus == EmployeeStatus
                    ).Select(s => s.EmployeeID);
                lsEmployee = from em in lsEmployee
                             where lstHistory.Contains(em.EmployeeID)
                             select em;
            }

            return lsEmployee;
        }

        /// <summary>
        /// Lay danh sach can bo dang lam viec o truong
        /// </summary>
        /// <param name="schoolID"></param>
        /// <returns></returns>
        public IQueryable<EmployeeBO> GetActiveEmployee(int schoolID)
        {
            IQueryable<EmployeeBO> query = from ee in EmployeeRepository.All
                                           join sf in SchoolFacultyRepository.All on ee.SchoolFacultyID equals sf.SchoolFacultyID
                                           where ee.SchoolID == schoolID
                                           && ee.IsActive == true
                                           && ee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING
                                           select new EmployeeBO
                                           {
                                               EmployeeID = ee.EmployeeID,
                                               FullName = ee.FullName,
                                               EmployeeCode = ee.EmployeeCode,
                                               SchoolFacultyID = ee.SchoolFacultyID,
                                               SchoolFacultyName = sf.FacultyName,
                                               EthnicCode = ee.Ethnic.EthnicCode
                                           };
            return query;
        }

        public List<TeacherBO> GetListTeacherOfSchoolForReport(int schoolId, int academicYearId)
        {

            //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
            if (academicYearId == 0)
            {
                return new List<TeacherBO>();
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearId;
            dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;

            IQueryable<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(schoolId, dic);


            return (from e in lstEmployee
                    join u in UserAccountBusiness.All on e.EmployeeID equals u.EmployeeID into des
                    from x in des.DefaultIfEmpty()
                    join a in aspnet_UsersBusiness.All on x.GUID equals a.UserId into des2
                    from y in des2.DefaultIfEmpty()
                    select new TeacherBO
                          {
                              TeacherID = e.EmployeeID,
                              FullName = e.FullName,
                              SchoolFacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : null,
                              Mobile = e.Mobile,
                              AccountName = y != null ? y.UserName : string.Empty,
                              WorkGroupTypeName = e.WorkGroupType != null ? e.WorkGroupType.Resolution : string.Empty
                          }).ToList().OrderBy(p => p.Name).ToList();

        }
        #endregion

        #region validate entity
        public void Validate(Employee insertEmployee, bool forImport = false)
        {
            //Mã nhân viên không được trống. Độ dài không quá 50 ký tự - Kiểm tra EmployeeCode
            Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_EmployeeCode");
            Utils.ValidateMaxLength(insertEmployee.EmployeeCode, EMPLOYEE_CODE_MAXLENGTH, "Employee_Label_EmployeeCode");
            //Mã nhân viên không được trùng – Kiểm tra sự tồn tại EmployeeID trong bảng Employee
            //this.CheckDuplicate(insertEmployee.EmployeeCode, GlobalConstants.SCHOOL_SCHEMA, "Employee", "EmployeeCode", true, insertEmployee.EmployeeID, "Employee_Label_EmployeeCode");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode}
                    ,{"SchoolID",insertEmployee.SchoolID}
                    ,{"IsActive",true}
                }, new Dictionary<string, object>()
                {
                {"EmployeeID",insertEmployee.EmployeeID}
                });
                if (Exist)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "Employee_Label_EmployeeCode" });
                }
            }

            //Họ tên không được trống. Độ dài không quá 100 ký tự - Kiểm tra FullName
            Utils.ValidateRequire(insertEmployee.FullName, "Employee_Label_FullName");
            Utils.ValidateMaxLength(insertEmployee.FullName, EMPLOYEE_FULLNAME_MAXLENGTH, "Employee_Label_FullName");

            //Bí danh không quá 100 ký tự - Kiểm tra Alias
            Utils.ValidateMaxLength(insertEmployee.Alias, EMPLOYEE_ALIAS_MAXLENGTH, "Employee_Label_Alias");

            //Số CMND không quá 30 ký tự - Kiểm tra IdentityNumber
            if (insertEmployee.IdentityNumber != null)
            {
                Utils.ValidateMaxLength(insertEmployee.IdentityNumber, EMPLOYEE_IDENTIFYNUMBER_MAXLENGTH, "Employee_Label_IdentityNumber");
                //Chiendd1: 01/08/2015, bo dieu kien trung CMND
                /*if (insertEmployee.EmployeeID == 0)
                {
                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["IdentityNumber"] = insertEmployee.IdentityNumber;
                    SearchInfo["IsActive"] = true;

                    IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
                    ExceptInfo["EmployeeID"] = insertEmployee.EmployeeID;
                    bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo, ExceptInfo);

                    if (Exist)
                    {
                        throw new BusinessException("Common_Validate_Duplicate", new List<object> { "Employee_Label_IdentityNumber" });
                    }
                }*/
            }

            //Nơi cấp CMND không quá 400 ký tự - Kiểm tra IdentityIssuedPlace
            Utils.ValidateMaxLength(insertEmployee.IdentityIssuedPlace, EMPLOYEE_IDENTIFYISSUEDPLACE_MAXLENGTH, "Employee_Label_IdentityIssuedPlace");

            //Sứa khỏe không được nhập quá 400 ký tự - Kiểm tra HealthStatus
            Utils.ValidateMaxLength(insertEmployee.HealthStatus, EMPLOYEE_HEALTHSTATUS_MAXLENGTH, "Employee_Label_HealthStatus");

            //Quê quán không được nhập quá 400 ký tự - Kiểm tra HomeTown
            Utils.ValidateMaxLength(insertEmployee.HomeTown, EMPLOYEE_HOMETOWN_MAXLENGTH, "Employee_Label_HomeTown");

            //Địa chỉ thường trú không được nhập quá 400 ký tự - Kiểm tra PermanentResidnetalAddress
            Utils.ValidateMaxLength(insertEmployee.PermanentResidentalAddress, EMPLOYEE_PERMANENTRESIDNETALADDRESS_MAXLENGTH, "Employee_Label_PermanentResidentalAddress");

            //Nơi kết nạp đoàn không được nhập quá 400 ký tự.- kiểm tra YouthLeagueJoinedPlace
            Utils.ValidateMaxLength(insertEmployee.YouthLeagueJoinedPlace, EMPLOYEE_YOUTHLEAGUEJOINEDPLACE_MAXLENGTH, "Employee_Label_YouthLeagueJoinedPlace");


            //Nơi kết nạp đảng không được nhập quá 400 ký tự - Kiểm tra CommunistPartyJoinedPlace
            Utils.ValidateMaxLength(insertEmployee.CommunistPartyJoinedPlace, EMPLOYEE_COMMUNISTPARTYJOINEDPLACE_MAXLENGTH, "Employee_Label_CommunistPartyJoinedPlace");

            //Họ tên cha không được nhập quá 100 ký tự - Kiểm tra FatherFullName
            //Nghề nghiệp của cha không được nhập quá 200 ký tự - Kiểm tra FatherJob
            //Nơi làm việc của cha không được nhập quá 400 ký tự - Kiểm tra FatherWorkingPlace
            Utils.ValidateMaxLength(insertEmployee.FatherFullName, EMPLOYEE_FATHERFULLNAME_MAXLENGTH, "Employee_Label_FatherFullName");
            Utils.ValidateMaxLength(insertEmployee.FatherJob, EMPLOYEE_FATHERJOB_MAXLENGTH, "Employee_Label_FatherJob");
            Utils.ValidateMaxLength(insertEmployee.FatherWorkingPlace, EMPLOYEE_FATHERWORKINGPLACE_MAXLENGTH, "Employee_Label_FatherWorkingPlace");

            //            Họ tên mẹ không được nhập quá 100 ký tự - Kiểm tra MotherFullName
            //Nghề nghiệp của mẹ không được nhập quá 200 ký tự - Kiểm tra MotherJob
            //Nơi làm việc của mẹ không được nhập quá 400 ký tự. – Kiểm tra MotherWorkingPlace
            Utils.ValidateMaxLength(insertEmployee.MotherFullName, EMPLOYEE_MOTHERFULLNAME_MAXLENGTH, "Employee_Label_MotherFullName");
            Utils.ValidateMaxLength(insertEmployee.MotherJob, EMPLOYEE_MOTHERJOB_MAXLENGTH, "Employee_Label_MotherJob");
            Utils.ValidateMaxLength(insertEmployee.MotherWorkingPlace, EMPLOYEE_MOTHERWORKINGPLACE_MAXLENGTH, "Employee_Label_MotherWorkingPlace");

            //            Họ tên vợ/ chông không được nhập quá 100 ký tự - Kiểm tra SpouseFullName
            //Nghề nghiệp của vợ/ chồng không được nhập quá 200 ký tự - Kiểm tra SpouseJob
            //Nơi làm việc của vợ/ chồng không được nhập quá 400 ký tự - Kiểm tra SpouseWorkingPlace
            Utils.ValidateMaxLength(insertEmployee.SpouseFullName, EMPLOYEE_SPOUSEFULLNAME_MAXLENGTH, "Employee_Label_SpouseFullName");
            Utils.ValidateMaxLength(insertEmployee.SpouseJob, EMPLOYEE_SPOUSEJOB_MAXLENGTH, "Employee_Label_SpouseJob");
            Utils.ValidateMaxLength(insertEmployee.SpouseWorkingPlace, EMPLOYEE_SPOUSEWORKINGPLACE_MAXLENGTH, "Employee_Label_SpouseWorkingPlace");


            //Mô tả chức danh không được nhập quá 400 ký tự  - Kiểm tra 
            Utils.ValidateMaxLength(insertEmployee.Description, EMPLOYEE_DESCRIPTION_MAXLENGTH, "Employee_Label_Description");

            //Ngày sinh không được bỏ trống – Kiểm tra BirthDate
            if (!forImport)
            {
                Utils.ValidateRequire(insertEmployee.BirthDate, "Employee_Label_BirthDate");
                try
                {
                    //Ngày sinh phải nhỏ hơn ngày hiện tại – 16 năm. Kiểm tra BirthDate
                    if (insertEmployee.BirthDate != null)
                    {
                        Utils.ValidateAfterDate(DateTime.Now, insertEmployee.BirthDate.Value, EMPLOYEE_AGE_NOW, "Employee_Label_Date_Now", "Employee_Label_BirthDate");
                    }
                }
                catch (Exception ex)
                {
                    throw new BusinessException("Employee_Label_ErrorBirthDate");
                }
                //BirthDate .Year > 1900 
                if (insertEmployee.BirthDate != null)
                {
                    Utils.ValidateAfterDate(insertEmployee.BirthDate.Value, new DateTime(1900, 1, 1), "Employee_Label_BirthDate", "Common_Min_BirthYear");
                }
            }


            Utils.ValidateRequire(insertEmployee.IntoSchoolDate, "Employee_Label_IntoSchoolDate");


            Utils.ValidatePastDate(insertEmployee.IntoSchoolDate, "Employee_Label_IntoSchoolDate");
            Utils.ValidatePastDate(insertEmployee.StartingDate, "Employee_Label_StartingDate");
            Utils.ValidatePastDate(insertEmployee.JoinedDate, "Employee_Label_JoinedDate");

            //CommunistPartyJoinedDate < YoungLeageJoinedDate
            //Ngày vào Đảng phải lớn hơn ngày vào đoàn – Kiểm tra CommunistPartyJoinedDate, YoungLeageJoinedDate
            if ((insertEmployee.CommunistPartyJoinedDate.HasValue) && (insertEmployee.YouthLeagueJoinedDate.HasValue))
            {
                Utils.ValidateAfterDate(insertEmployee.CommunistPartyJoinedDate.Value, insertEmployee.YouthLeagueJoinedDate.Value, "Employee_Label_CommunistPartyJoinedDate", "Employee_Label_YoungLeageJoinedDate");            
            }

           /* if (insertEmployee.StartingDate.HasValue)
            {
                Utils.ValidateAfterDate(insertEmployee.IntoSchoolDate, insertEmployee.StartingDate.Value, "Employee_Label_IntoSchoolDate", "Employee_Label_StartingDate");
            }*/

            //Số điện thoại liên lạc không được nhập quá 15 ký tự. – Kiểm tra Telephone
            Utils.ValidateMaxLength(insertEmployee.Telephone, EMPLOYEE_TELEPHONE_MAXLENGTH, "Employee_Label_Telephone");

            //Số điện thoại liên lạc, dùng để gửi tin nhắn không được nhập quá 15 ký tự - Kiểm tra Mobile
            Utils.ValidateMaxLength(insertEmployee.Mobile, EMPLOYEE_MOBILE_MAXLENGTH, "Employee_Label_Mobile");

            //Email không quá 60 ký tự. Đúng định dạng Email – Kiểm tra Email
            Utils.ValidateMaxLength(insertEmployee.Email, EMPLOYEE_EMAIL_MAXLENGTH, "Employee_Label_Email");
            if (insertEmployee.Email != null)
            {
                Utils.ValidateEmail(insertEmployee.Email, "Employee_Label_Email");
            }

            //Kiểm tra FamilyTypeID có tồn tại trong hệ thống không?
            FamilyTypeBusiness.CheckAvailable(insertEmployee.FamilyTypeID, "Employee_Label_FamilyType");

            //            Kiểm tra StaffPositionID có tồn tại hay không?
            //Tôn giáo có tồn tại hay không?Kiểm tra ReligionID trong bảng Religion với IsActive = 1
            StaffPositionBusiness.CheckAvailable(insertEmployee.StaffPositionID, "Employee_Label_StaffPosition");
            ReligionBusiness.CheckAvailable(insertEmployee.ReligionID, "Employee_Label_Religion");


            //EthnicID: require
            Utils.ValidateRequire(insertEmployee.EthnicID, "Employee_Label_Ethnic");
            //PK(Ethnic)
            EthnicBusiness.CheckAvailable(insertEmployee.EthnicID, "Employee_Label_Ethnic");

            //Trình độ đào tạo có tồn tại hay không? Kiểm tra GraduationLevelID trong bảng GraduationLevel với isActive  = 1
            //Hình thức đào tạo có tồn tại hay không? Kiểm tra QualificationTypeID trong bảng QualificationType với isActive = 1
            GraduationLevelBusiness.CheckAvailable(insertEmployee.GraduationLevelID, "Employee_Label_GraduationLevel");
            QualificationTypeBusiness.CheckAvailable(insertEmployee.QualificationTypeID, "Employee_Label_QualificationType");

            //Chuyên ngành đào tạo có tồn tại hay không? Kiểm tra SpecialityCatID trong bảng SpecialityCat với isActive = 1
            //Kiểm tra hợp đồng lao động có tồn tại hay không? Kiểm tra ContactID trong bảng Contract với isActive = 1
            SpecialityCatBusiness.CheckAvailable(insertEmployee.SpecialityCatID, "Employee_Label_SpecialityCat");
            ContractBusiness.CheckAvailable(insertEmployee.ContractID, "Employee_Label_Contract");

            //Trình độ văn hóa có tồn tại hay không? Kiêm tra QualificationLevelID trong bảng QualificationLevel với isActive = 1
            //Trình độ tin học có tồn tại hay không? Kiểm tra ITQualificationLevelID trong bảng ITQualificationLevel với isActive = 1
            QualificationLevelBusiness.CheckAvailable(insertEmployee.QualificationLevelID, "Employee_Label_QualificationLevel");
            ITQualificationLevelBusiness.CheckAvailable(insertEmployee.ITQualificationLevelID, "Employee_Label_ITQualificationLevel");

            //Trình độ ngoại ngữ có tồn tại hay không? Kiểm tra ForeignLanguageGradeID trong bảng ForeignLanguageGrade với isActive = 1
            //Không tồn tại trình độ quản lý nhà nước hoặc đã bị xóa? Kiểm tra StateManagementGradeID trong bảng StateManagementGrade với isActive = 1
            ForeignLanguageGradeBusiness.CheckAvailable(insertEmployee.ForeignLanguageGradeID, "Employee_Label_ForeignLanguageGrade");
            StateManagementGradeBusiness.CheckAvailable(insertEmployee.StateManagementGradeID, "Employee_Label_StateManagementGrade");

            //Không tồn tại trình độ quản lý giáo dục hoặc đã bị xóa? Kiểm tra EducationManagementGradeID trong bảng EducationManagementGrade với IsActive = 1
            //Không tồn tại loại công việc hoặc đã bị xóa? Kiểm tra WorkTypeID trong bảng WorkType với isActive = 1
            EducationalManagementGradeBusiness.CheckAvailable(insertEmployee.EducationalManagementGradeID, "Employee_Label_EducationalManagementGrade");
            WorkTypeBusiness.CheckAvailable(insertEmployee.WorkTypeID, "Employee_Label_WorkType");

            //Kiểm tra SchoolID có tồn tại không 
            SchoolProfileBusiness.CheckAvailable(insertEmployee.SchoolID, "SchoolProfile_Label_SchoolID");

            //Không tồn tại tổ bộ môn hoặc đã bị xóa? Kiểm tra SchoolFacultyID trong bảng SchoolFaculty với isActive = 1
            SchoolFacultyBusiness.CheckAvailable(insertEmployee.SchoolFacultyID, "Employee_Label_SchoolFacultyID");

            //SchoolID, SchoolFacultyID:  not compatible(SchoolFaculty)
            if (insertEmployee.SchoolID.HasValue && insertEmployee.SchoolFacultyID.HasValue)
            {
                bool SchoolFacultyCompatible = SchoolFacultyRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty",
                        new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployee.SchoolID},
                    {"SchoolFacultyID",insertEmployee.SchoolFacultyID}
                    ,{"IsActive",true}
                }, null);
                if (!SchoolFacultyCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

            }



            //IdentityNumber: chỉ được nhập số
            if (insertEmployee.IdentityNumber != null && insertEmployee.IdentityNumber != " " && insertEmployee.IdentityNumber.Length > 0)
            {
                Utils.ValidateIsNumber(insertEmployee.IdentityNumber, "Employee_Label_IdentityNumber");
            }
            //Telephone, Mobile: Kiểu số, chiều dài tối đa 15 ký tự. Bắt đầu phải là 09x,01x,849x,841x
            if (insertEmployee.Mobile != null && insertEmployee.Mobile != " " && insertEmployee.Mobile.Length > 0)
            {
                Utils.ValidatePhoneNumber(insertEmployee.Mobile, "Employee_Label_Mobile");
            }
            //Utils.ValidatePhoneNumber(insertEmployee.Telephone, "Employee_Label_Telephone");


        }

        private void ValidateAll(List<Employee> lstEmployee)
        {
            if (lstEmployee.Count() == 0)
            {
                return;
            }
            Employee employee = new Employee();
            employee = lstEmployee[0];
            SchoolProfileBusiness.CheckAvailable(employee.SchoolID, "SchoolProfile_Label_SchoolID");


            foreach (Employee insertEmployee in lstEmployee)
            {
                #region validate
                //Mã nhân viên không được trống. Độ dài không quá 50 ký tự - Kiểm tra EmployeeCode
                Utils.ValidateRequire(insertEmployee.EmployeeCode, "Employee_Label_EmployeeCode");
                Utils.ValidateMaxLength(insertEmployee.EmployeeCode, EMPLOYEE_CODE_MAXLENGTH, "Employee_Label_EmployeeCode");
                //Họ tên không được trống. Độ dài không quá 100 ký tự - Kiểm tra FullName
                Utils.ValidateRequire(insertEmployee.FullName, "Employee_Label_FullName");
                Utils.ValidateMaxLength(insertEmployee.FullName, EMPLOYEE_FULLNAME_MAXLENGTH, "Employee_Label_FullName");


                //Số CMND không quá 30 ký tự - Kiểm tra IdentityNumber
                Utils.ValidateMaxLength(insertEmployee.IdentityNumber, EMPLOYEE_IDENTIFYNUMBER_MAXLENGTH, "Employee_Label_IdentityNumber");

                //Nơi cấp CMND không quá 400 ký tự - Kiểm tra IdentityIssuedPlace
                Utils.ValidateMaxLength(insertEmployee.IdentityIssuedPlace, EMPLOYEE_IDENTIFYISSUEDPLACE_MAXLENGTH, "Employee_Label_IdentityIssuedPlace");

                //Sứa khỏe không được nhập quá 400 ký tự - Kiểm tra HealthStatus
                Utils.ValidateMaxLength(insertEmployee.HealthStatus, EMPLOYEE_HEALTHSTATUS_MAXLENGTH, "Employee_Label_HealthStatus");

                //Quê quán không được nhập quá 400 ký tự - Kiểm tra HomeTown
                Utils.ValidateMaxLength(insertEmployee.HomeTown, EMPLOYEE_HOMETOWN_MAXLENGTH, "Employee_Label_HomeTown");

                //Địa chỉ thường trú không được nhập quá 400 ký tự - Kiểm tra PermanentResidnetalAddress
                Utils.ValidateMaxLength(insertEmployee.PermanentResidentalAddress, EMPLOYEE_PERMANENTRESIDNETALADDRESS_MAXLENGTH, "Employee_Label_PermanentResidentalAddress");


                Utils.ValidateRequire(insertEmployee.IntoSchoolDate, "Employee_Label_IntoSchoolDate");

                Utils.ValidatePastDate(insertEmployee.IntoSchoolDate, "Employee_Label_IntoSchoolDate");
                Utils.ValidatePastDate(insertEmployee.StartingDate, "Employee_Label_StartingDate");
                Utils.ValidatePastDate(insertEmployee.JoinedDate, "Employee_Label_JoinedDate");

                //Số điện thoại liên lạc không được nhập quá 15 ký tự. – Kiểm tra Telephone
                Utils.ValidateMaxLength(insertEmployee.Telephone, EMPLOYEE_TELEPHONE_MAXLENGTH, "Employee_Label_Telephone");

                //Số điện thoại liên lạc, dùng để gửi tin nhắn không được nhập quá 15 ký tự - Kiểm tra Mobile
                Utils.ValidateMaxLength(insertEmployee.Mobile, EMPLOYEE_MOBILE_MAXLENGTH, "Employee_Label_Mobile");
                if (insertEmployee.BirthDate != null)
                {
                    try
                    {
                        //Ngày sinh phải nhỏ hơn ngày hiện tại – 16 năm. Kiểm tra BirthDate
                        Utils.ValidateAfterDate(DateTime.Now, insertEmployee.BirthDate.Value, EMPLOYEE_AGE_NOW, "Employee_Label_Date_Now", "Employee_Label_BirthDate");
                    }
                    catch (Exception)
                    {
                        throw new BusinessException("Employee_Label_ErrorBirthDate");
                    }
                    //BirthDate .Year > 1900 
                    Utils.ValidateAfterDate(insertEmployee.BirthDate.Value, new DateTime(1900, 1, 1), "Employee_Label_BirthDate", "Common_Min_BirthYear");
                }
                //IdentityNumber: chỉ được nhập số
                if (insertEmployee.IdentityNumber != null)
                {
                    Utils.ValidateIsNumber(insertEmployee.IdentityNumber, "Employee_Label_IdentityNumber");
                }
                //Telephone, Mobile: Kiểu số, chiều dài tối đa 15 ký tự. Bắt đầu phải là 09x,01x,849x,841x

                Utils.ValidatePhoneNumber(insertEmployee.Mobile, "Employee_Label_Mobile");
                //Utils.ValidatePhoneNumber(insertEmployee.Telephone, "Employee_Label_Telephone");
                #endregion
            }

        }
        #endregion

        /// <summary>
        /// GetUserNameForEmployee
        /// </summary>
        /// <param name="EmployeeID">The employee ID.</param>
        /// <returns>
        /// System.String
        /// </returns>
        ///<author>NamTA</author>
        ///<date>3/8/2013</date>
        public string GetUserNameForEmployee(int EmployeeID)
        {
            string UserName = "";

            var Un = from ua in UserAccountBusiness.SearchUser(new Dictionary<string, object>() { { "EmployeeID", EmployeeID } })
                     join mb in aspnet_UsersBusiness.All on ua.GUID equals mb.UserId
                     select mb.UserName;

            if (Un != null && Un.Count() > 0)
            {
                UserName = Un.FirstOrDefault();
            }


            return UserName;
        }

        /// <summary>
        /// ValidateForImport
        /// </summary>
        /// <param name="insertEmployee"></param>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public List<BusinessException> ValidateForImport(Employee insertEmployee)
        {
            List<BusinessException> lsBE = new List<BusinessException>();

            SchoolProfile sf = SchoolProfileRepository.Find(insertEmployee.SchoolID);
            if (insertEmployee.AppliedLevel.HasValue)
            {
                if (!SchoolProfileBusiness.GetListAppliedLevel(sf.SchoolProfileID).Contains((int)insertEmployee.AppliedLevel))
                {
                    lsBE.Add(new BusinessException("Common_Validate_AppliedLevel"));
                }
            }

            //this.CheckDuplicate(insertEmployee.EmployeeCode, GlobalConstants.SCHOOL_SCHEMA, "Employee", "EmployeeCode", true, insertEmployee.EmployeeID, "Employee_Label_EmployeeCode");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeCode",insertEmployee.EmployeeCode}
                    ,{"SchoolID",insertEmployee.SchoolID}
                    ,{"IsActive",true}
                }, new Dictionary<string, object>()
                {
                {"EmployeeID",insertEmployee.EmployeeID}
                });
                if (Exist)
                {
                    lsBE.Add(new BusinessException("Common_Validate_Duplicate", new List<object>() { "Employee_Label_EmployeeCode" }));
                }
            }


            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"IdentityNumber",insertEmployee.IdentityNumber}
                    ,{"IsActive",true}
                }, new Dictionary<string, object>()
                {
                {"EmployeeID",insertEmployee.EmployeeID}
                });
                if (Exist)
                {
                    lsBE.Add(new BusinessException("Common_Validate_Duplicate", new List<object> { "Employee_Label_IdentityNumber" }));
                }
            }

            //SchoolID, SchoolFacultyID:  not compatible(SchoolFaculty)
            if (insertEmployee.SchoolID.HasValue && insertEmployee.SchoolFacultyID.HasValue)
            {
                bool SchoolFacultyCompatible = SchoolFacultyRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty",
                        new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployee.SchoolID},
                    {"SchoolFacultyID",insertEmployee.SchoolFacultyID}
                    ,{"IsActive",true}
                }, null);
                if (!SchoolFacultyCompatible)
                {
                    lsBE.Add(new BusinessException("Employee_Label_SchoolFacultyNotInSchool"));
                }

            }

            try
            {
                //IdentityNumber: chỉ được nhập số
                if (insertEmployee.IdentityNumber != null)
                {
                    Utils.ValidateIsNumber(insertEmployee.IdentityNumber, "Employee_Label_IdentityNumber");
                }
            }
            catch (BusinessException ex)
            {
                lsBE.Add(ex);
            }

            try
            {
                //Telephone, Mobile: Kiểu số, chiều dài tối đa 15 ký tự. Bắt đầu phải là 09x,01x,849x,841x
                Utils.ValidatePhoneNumber(insertEmployee.Mobile, "Employee_Label_Mobile");
            }
            catch (BusinessException ex)
            {
                lsBE.Add(ex);
            }

            try
            {

                if (insertEmployee.Email != null)
                {
                    Utils.ValidateEmail(insertEmployee.Email, "Employee_Label_Email");
                }

            }
            catch (BusinessException ex)
            {
                lsBE.Add(ex);
            }

            try
            {
                Utils.ValidatePastDate(insertEmployee.IntoSchoolDate, "Employee_Label_IntoSchoolDate");
            }
            catch (BusinessException ex)
            {
                lsBE.Add(ex);
            }

            try
            {
                Utils.ValidatePastDate(insertEmployee.StartingDate, "Employee_Label_StartingDate");
            }
            catch (BusinessException ex)
            {
                lsBE.Add(ex);
            }

            try
            {
                Utils.ValidatePastDate(insertEmployee.JoinedDate, "Employee_Label_JoinedDate");
            }
            catch (BusinessException ex)
            {
                lsBE.Add(ex);
            }

            return lsBE;
        }

        #region Kiem tra giao vien da duoc su dung chua
        public List<bool> IsUsedTeacher(List<int> lstEmployeeId)
        {
            List<bool> lsResult = new List<bool>();
            bool check;
            for (int i = 0; i < lstEmployeeId.Count; i++)
            {
                #region old
                //check = false;
                ////EmployeeID: using(UserAccount) kiem tra employee da duoc gan tai khoan chua
                //bool UserAccountConstraint = this.repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "UserAccount",
                //       new Dictionary<string, object>()
                //{
                //    {"EmployeeID",lstEmployeeId[i]}
                //}, null);
                //if (UserAccountConstraint)
                //{
                //    check = true;
                //}

                //if (!check) //
                //{
                //    //EmployeeID: using(TeachingAssignment – TeacherID) kiem tra employee da phan cong giang day chua
                //    bool TeachingAssignmentConstraint = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeachingAssignment",
                //          new Dictionary<string, object>()
                //{
                //    {"IsActive",true},
                //    {"TeacherID",lstEmployeeId[i]}
                //}, null);
                //    if (TeachingAssignmentConstraint)
                //    {
                //        check = true;
                //    }
                //}
                //if (!check) //
                //{
                //    //EmployeeID: using(ClassProfile – HeadTeacherID)
                //    bool ClassProfileConstraint = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                //          new Dictionary<string, object>()
                //    {

                //        {"HeadTeacherID",lstEmployeeId[i]}
                //    }, null);
                //    if (ClassProfileConstraint)
                //    {
                //        check = true;
                //    }
                //}
                //if (!check) //
                //{
                //    //EmployeeID: using(ClassSupervisorAssignment – TeacherID)
                //    bool ClassSupervisorAssignmentConstraint = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSupervisorAssignment",
                //          new Dictionary<string, object>()
                //    {

                //        {"TeacherID",lstEmployeeId[i]}
                //    }, null);
                //    if (ClassSupervisorAssignmentConstraint)
                //    {
                //        check = true;
                //    }

                //}
                //if (!check) //
                //{
                //    //EmployeeID: using(ConcurrentWorkAssignment – TeacherID)
                //    bool ConcurrentWorkAssignmentConstraint = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment",
                //          new Dictionary<string, object>()
                //{
                //    {"IsActive",true},
                //    {"TeacherID",lstEmployeeId[i]}
                //}, null);
                //    if (ConcurrentWorkAssignmentConstraint)
                //    {
                //        check = true;
                //    }
                //}
                //if (!check) //
                //{
                //    //EmployeeID: using(ConcurrentWorkReplacement – TeacherID)
                //    bool ConcurrentWorkReplacementConstraint = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkReplacement",
                //          new Dictionary<string, object>()
                //{
                //    {"IsActive",true},
                //    {"TeacherID",lstEmployeeId[i]}
                //}, null);
                //    if (ConcurrentWorkReplacementConstraint)
                //    {
                //        check = true;
                //    }
                //}
                //if (!check) //
                //{
                //    //EmployeeID: using(ConcurrentWorkReplacement – TeacherID)
                //    bool ConcurrentWorkReplacementConstraint2 = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkReplacement",
                //          new Dictionary<string, object>()
                //{
                //    {"IsActive",true},
                //    {"ReplacedTeacherID",lstEmployeeId[i]}
                //}, null);
                //    if (ConcurrentWorkReplacementConstraint2)
                //    {
                //        check = true;
                //    }
                //}
                //if (!check) //
                //{
                //    bool HeadTeacherSubtitution = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HeadTeacherSubstitution",
                //         new Dictionary<string, object>()
                //{
                //    {"HeadTeacherID",lstEmployeeId[i]}
                //}, null);
                //    if (HeadTeacherSubtitution)
                //    {
                //        check = true;
                //    }
                //}
                //if (!check) //
                //{
                //    bool HeadTeacherSubtitution2 = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HeadTeacherSubstitution",
                //         new Dictionary<string, object>()
                //{
                //    {"SubstituedHeadTeacher",lstEmployeeId[i]}
                //}, null);
                //    if (HeadTeacherSubtitution2)
                //    {
                //        check = true;
                //    }
                //}
                #endregion
                //Vi khi tao giao vien se tao du lieu trong cac bang TeacherOfFaculty, EmployeeHistoryStatus nen khi check se khong check trong cac bang nay
                //HaiVT 17/06/2013 - bo sung them bang bang EmployeeContact
                check = this.repository.CheckConstraintsWithExceptTable(GlobalConstants.SCHOOL_SCHEMA, "Employee", ",TeacherOfFaculty,EmployeeHistoryStatus,EmployeeContact,", lstEmployeeId[i]);
                lsResult.Add(check);
            }
            return lsResult;
        }
        #endregion

        #region Import
        public void ImportTeacher(List<Employee> lstInsertEmployee)
        {
            ValidateAll(lstInsertEmployee);
            List<Employee> listEmployeeInsert = new List<Employee>();
            foreach (Employee insertEmployee in lstInsertEmployee)
            {
                //Employee.EmployeeType =  EMPLOYEE_TYPE_TEACHER
                //Employee.EmploymentStatus = EMPLOYMENT_STATUS_WORKING

                insertEmployee.EmployeeType = GlobalConstants.EMPLOYEE_TYPE_TEACHER;
                insertEmployee.EmploymentStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                insertEmployee.IsActive = true;



                //Khởi tạo đối tượng TeacherOfFaculty, gán các giá trị TeacherID(EmployeeID), SchoolID, 
                //FacultyID tương ứng theo Employee; IsActive = True, StartDate = DateTime.Now; insert vào TeacherOfFaculty
                TeacherOfFaculty tof = new TeacherOfFaculty();
                //tof.Employee = insertEmployee;
                //tof.TeacherID = returnEmployee.EmployeeID;
                tof.FacultyID = insertEmployee.SchoolFacultyID.Value;
                tof.IsActive = true;
                tof.StartDate = DateTime.Now;
                insertEmployee.TeacherOfFaculties.Add(tof);

                //Khởi tạo đối tượng EmployeeHistoryStatus, gán các giá trị EmployeeID, SchoolID, EmployeeStatus 
                //tương ứng theo Employee; FromDate = DateTime.Now; insert vào EmployeeHistoryStatus
                EmployeeHistoryStatus ehs = new EmployeeHistoryStatus();
                // ehs.EmployeeID = insertEmployee.EmployeeID;
                //ehs.Employee = insertEmployee;
                ehs.SchoolID = insertEmployee.SchoolID;
                ehs.EmployeeStatus = (int)insertEmployee.EmploymentStatus;
                ehs.FromDate = insertEmployee.IntoSchoolDate;
                //EmployeeHistoryStatusRepository.Insert(ehs);
                insertEmployee.EmployeeHistoryStatus.Add(ehs);
                //Thực hiện Insert dữ liệu vào bảng Employee
                listEmployeeInsert.Add(insertEmployee);
                base.Insert(insertEmployee);
            }
            //this.BulkInsert(listEmployeeInsert);
        }

        public void ImportUpdateTeacher(List<Employee> lstUpdateEmployee)
        {
            if (lstUpdateEmployee.Count() == 0)
            {
                return;
            }
            this.context.Configuration.AutoDetectChangesEnabled = false;
            Employee employee = lstUpdateEmployee.First();
            ValidateAll(lstUpdateEmployee);
            var lstof = from t in TeacherOfFacultyRepository.All
                        where t.SchoolFaculty.SchoolID == employee.SchoolID
                        && t.IsActive == true
                        select t;
            List<TeacherOfFaculty> LstTeacherOfFaculty = lstof.ToList();
            TeacherOfFaculty tof = new TeacherOfFaculty();
            foreach (Employee insertEmployee in lstUpdateEmployee)
            {
                insertEmployee.EmployeeType = GlobalConstants.EMPLOYEE_TYPE_TEACHER;
                //Lấy bản ghi ở TeacherOfFacultyBusiness.SearchBySchool(Entity.SchoolID, Dictionary)
                //cập nhật lại giá trị FacultyID = Entity.FacultyID
                //+ Dictionary[“TeacherID”] = Entity.EmployeeID
                tof = LstTeacherOfFaculty.Where(o => o.TeacherID == insertEmployee.EmployeeID).FirstOrDefault();

                if (tof != null && insertEmployee.SchoolFacultyID != null)
                {
                    tof.FacultyID = insertEmployee.SchoolFacultyID.Value;
                    TeacherOfFacultyBusiness.Update(tof);
                }
                base.Update(insertEmployee);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
        }
        #endregion

        #region Convert from WCF
        /// <summary>
        /// Lấy danh sách tẩt cả giáo viên đang làm việc trong trường
        /// </summary>
        /// <author date="06/10/2015">AnhVD9</author>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public List<TeacherBO> GetTeachersOfSchool(int schoolID, int academicYearID, bool? IsStatus = null)
        {

            //Khoi tao Business
            //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
            if (academicYearID == 0) return new List<TeacherBO>();

            IQueryable<TeacherBO> iqEmployee = (from e in EmployeeBusiness.All
                                                where e.SchoolID == schoolID
                                                        && e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                                        && e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                        && e.IsActive == true
                                                select new TeacherBO
                                                      {
                                                          TeacherID = e.EmployeeID,
                                                          TeacherCode = e.EmployeeCode,
                                                          FullName = e.FullName,
                                                          Name = e.Name,
                                                          Genre = e.Genre,
                                                          GenreName = e.Genre ? "Nam" : "Nữ",
                                                          Mobile = e.Mobile,
                                                          EmployeeStatus = e.EmploymentStatus.Value,
                                                          IsActive = e.IsActive,
                                                          SchoolFacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : null
                                                      }).OrderBy(p => p.Name);
            return iqEmployee.ToList();
        }

        /// <summary>
        /// Lấy danh sách giáo viên theo danh sách ID truyền vào
        /// </summary>
        /// <param name="lstEmplyeeID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public List<TeacherBO> GetListTeacher(List<int> lstEmplyeeID,int StatusID = 0)
        {

            if (lstEmplyeeID.Count == 0)
            {
                return new List<TeacherBO>();
            }

            List<TeacherBO> lstFilterEmployee = (from e in EmployeeBusiness.All
                                                 join f in lstEmplyeeID on e.EmployeeID equals f
                                                 where e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                                 && (e.EmploymentStatus == StatusID || StatusID == 0)
                                                 select new TeacherBO
                                                       {
                                                           TeacherID = e.EmployeeID,
                                                           TeacherCode = e.EmployeeCode,
                                                           FullName = e.FullName,
                                                           Genre = e.Genre,
                                                           Name = e.Name,
                                                           Mobile = e.Mobile,
                                                           IsActive = e.IsActive,
                                                           EmployeeStatus = e.EmploymentStatus.Value,
                                                           SchoolFacultyID = e.SchoolFaculty.SchoolFacultyID,
                                                           SchoolFacultyName = e.SchoolFaculty.FacultyName

                                                       }).ToList();
            return lstFilterEmployee.ToList();
        }

        /// <summary>
        /// Lay danh sach giao vien cho chuc nang nhom lien lac
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns></returns>
        public List<TeacherBO> GetListTeacherOfSchoolForContactGroup(int schoolID, int academicYearID, bool? IsStatus = null)
        {

            //Khoi tao Business
            //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
            if (academicYearID == 0)
            {
                return new List<TeacherBO>();
            }
            bool? isStatus = IsStatus;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
            if (!isStatus.HasValue)
            {
                dic["CurrentEmployeeStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            }
            dic["isFromEduActive"] = isStatus;
            IQueryable<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(schoolID, dic);
            //lay danh sach GVCN cua truong
            List<int> lstHeadTeacherId = ClassProfileBusiness.All
                                            .Where(o => o.AcademicYearID == academicYearID
                                                && o.HeadTeacherID.HasValue).Select(o => o.HeadTeacherID.Value).Distinct().ToList();

            //Lay danh sach GVBM cua truong
            int partition = UtilsBusiness.GetPartionId(schoolID);
            List<int> lstSubjectTeacherId = TeachingAssignmentBusiness.All
                .Where(o => o.AcademicYearID == academicYearID
                && o.IsActive == true
                && o.Last2digitNumberSchool == partition
                ).Select(o => o.TeacherID).Distinct().ToList();

            List<TeacherBO> lstEmployeeDefault = (from e in lstEmployee
                                                        join u in UserAccountBusiness.All on e.EmployeeID equals u.EmployeeID into des
                                                        from x in des.DefaultIfEmpty()
                                                        join a in aspnet_UsersBusiness.All on x.GUID equals a.UserId into des2
                                                        from y in des2.DefaultIfEmpty()
                                                  select new TeacherBO
                                                        {
                                                            TeacherID = e.EmployeeID,
                                                            TeacherCode = e.EmployeeCode,
                                                            FullName = e.FullName,
                                                            Genre = e.Genre,
                                                            Name = e.Name,
                                                            AccountName = y != null ? y.UserName : null,
                                                            IsActive = e.IsActive,
                                                            Mobile = e.Mobile,
                                                            EmployeeStatus = e.EmploymentStatus.Value,
                                                            SchoolFacultyID = e.SchoolFacultyID.HasValue ? e.SchoolFacultyID.Value : 0,
                                                            SchoolFacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : null,
                                                            WorkGroupTypeID = e.WorkGroupTypeID.HasValue ? e.WorkGroupTypeID.Value : 0,
                                                            WorkGroupTypeName = e.WorkGroupType != null ? e.WorkGroupType.Resolution : null,
                                                            WorkTypeID = e.WorkTypeID.HasValue ? e.WorkTypeID.Value : 0,
                                                            WorkTypeName = e.WorkType != null ? e.WorkType.Resolution : null,
                                                            IsCommunistPartyMember = e.IsCommunistPartyMember.HasValue && e.IsCommunistPartyMember.Value == true,
                                                            IsSyndicate = e.IsSyndicate.HasValue && e.IsSyndicate.Value == true,
                                                            IsYouthLeageMember = e.IsYouthLeageMember.HasValue && e.IsYouthLeageMember.Value == true,
                                                            IsHeadTeacher = lstHeadTeacherId.Contains(e.EmployeeID),
                                                            IsSubjectTecher = lstSubjectTeacherId.Contains(e.EmployeeID)
                                                        }).ToList().OrderBy(p => p.Name).ToList();

            return lstEmployeeDefault;
        }

        /// <summary>
        /// Lấy thông tin nhân viên phòng sở theo List ID truyền vào
        /// </summary>
        /// <param name="EmployeeIDList"></param>
        /// <returns></returns>
        public List<HistoryReceiverBO> GetListEmployeeNameByListEmployeeID(List<int> EmployeeIDList)
        {

            List<EmployeeSupervisingDeptBO> listEmployee = null;
            if (EmployeeIDList != null && EmployeeIDList.Count > 0)
            {
                listEmployee = EmployeeBusiness.AllNoTracking.Where(o => EmployeeIDList.Contains(o.EmployeeID) && o.Mobile != null)
                    .Select(o => new EmployeeSupervisingDeptBO
                    {
                        EmployeeID = o.EmployeeID,
                        EmployeeName = o.FullName,
                        EmployeeCode = o.EmployeeCode,
                        EmployeeMobile = o.Mobile,
                        SupervisingDeptID = o.SupervisingDeptID.HasValue ? o.SupervisingDeptID.Value : 0,
                        ParentSupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                        ParentSupervisingDeptID = o.SupervisingDept.ParentID.HasValue ? o.SupervisingDept.ParentID.Value : 0
                    }).ToList();
            }

            // gan du lieu ten don vi cha
            List<int> superId = listEmployee.Where(p => p.SupervisingDeptID != 0).Select(p => p.SupervisingDeptID).Distinct().ToList();
            if (superId != null && superId.Count > 0)
            {
                string superName = string.Empty;
                SupervisingDept item = null;
                List<EmployeeSupervisingDeptBO> itemList = new List<EmployeeSupervisingDeptBO>();
                string[] traversalPathArray = null;
                for (int ik = superId.Count - 1; ik >= 0; ik--)
                {
                    item = SupervisingDeptBusiness.Find(superId[ik]);
                    itemList = listEmployee.Where(p => p.SupervisingDeptID == item.SupervisingDeptID).ToList();
                    if (item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                    {
                        for (int iz = itemList.Count - 1; iz >= 0; iz--)
                        {
                            itemList[iz].ParentSupervisingDeptName = item.SupervisingDeptName;
                        }
                    }
                    else
                    {
                        traversalPathArray = item.TraversalPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = traversalPathArray.Length - 1; i >= 0; i--)
                        {
                            item = SupervisingDeptBusiness.Find(Convert.ToInt32(traversalPathArray[i]));
                            if (item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                            {
                                break;
                            }
                        }

                        for (int iz = itemList.Count - 1; iz >= 0; iz--)
                        {
                            itemList[iz].ParentSupervisingDeptName = item.SupervisingDeptName;
                        }
                    }

                }

            }

            if (listEmployee != null)
            {
                return listEmployee.Select(e => new HistoryReceiverBO
                    {
                        ReceiverID = e.EmployeeID,
                        ReceiverName = e.EmployeeName,
                        Mobile = e.EmployeeMobile,
                        ReceiverUnitName = e.ParentSupervisingDeptName
                    }).ToList();
            }

            return null;

        }
        #endregion

        public List<TeacherBO> GetHeadTeacherByClassIDs(List<int> LstClassID, int AcademicYearID, int SchoolID)
        {
            try
            {
                DateTime dateTimeLast = DateTime.Now.Date;
                List<TeacherBO> Teachers = (from t in HeadTeacherSubstitutionBusiness.All
                                            where t.SchoolID == SchoolID
                                                && t.AcademicYearID == AcademicYearID
                                                && LstClassID.Contains(t.ClassID)
                                                && t.AssignedDate <= dateTimeLast
                                                && t.EndDate >= dateTimeLast
                                                && t.SubstituedHeadTeacher > 0
                                            select new TeacherBO()
                                               {
                                                   TeacherID = t.SubstituedHeadTeacher,
                                                   ClassID = t.ClassID,
                                                   FullName = t.Employee.FullName,
                                                   Mobile = t.Employee.Mobile
                                               }).ToList();

                List<int> notSubtituedTeacherIds = LstClassID.Except(Teachers.Select(o => o.ClassID).ToList()).ToList();

                if (notSubtituedTeacherIds.Count > 0)
                {
                    Teachers = Teachers.Union(ClassProfileBusiness.All.Where(o => notSubtituedTeacherIds.Contains(o.ClassProfileID)
                                 && o.HeadTeacherID > 0).Select(o => new TeacherBO()
                                 {
                                     TeacherID = o.HeadTeacherID.Value,
                                     ClassID = o.ClassProfileID,
                                     FullName = o.Employee.FullName,
                                     Mobile = o.Employee.Mobile
                                 }).ToList()).ToList();

                    notSubtituedTeacherIds = LstClassID.Except(Teachers.Select(o => o.ClassID).ToList()).ToList();

                    if (notSubtituedTeacherIds.Count > 0)
                    {
                        Teachers = Teachers.Union((from csa in ClassSupervisorAssignmentBusiness.All
                                                   where csa.SchoolID == SchoolID
                                                   && csa.AcademicYearID == AcademicYearID
                                                   && notSubtituedTeacherIds.Contains(csa.ClassID)
                                                   && csa.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                   select new TeacherBO()
                                                   {
                                                       TeacherID = csa.TeacherID,
                                                       ClassID = csa.ClassID,
                                                       FullName = csa.Employee.FullName,
                                                       Mobile = csa.Employee.Mobile
                                                   }).ToList()).ToList();
                    }
                }
                return Teachers;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<TeacherBO>();
            }
        }
        public int GetHeadTeacherID(int ClassID, int AcademicYearID, int SchoolID)
        {
            try
            {
                int ReturnValue = 0;
                DateTime dateTimeLast = DateTime.Now.Date;
                int? headTeacherID = 0;
                HeadTeacherSubstitution objHeadTeachSubstitution = (from t in HeadTeacherSubstitutionBusiness.All
                                                                    where t.SchoolID == SchoolID
                                                                    && t.AcademicYearID == AcademicYearID
                                                                    && t.ClassID == ClassID
                                                                    select t).FirstOrDefault();
                if (objHeadTeachSubstitution != null)
                {
                    if (objHeadTeachSubstitution.SubstituedHeadTeacher > 0 && (objHeadTeachSubstitution.AssignedDate <= dateTimeLast && dateTimeLast <= objHeadTeachSubstitution.EndDate))
                    {
                        headTeacherID = objHeadTeachSubstitution.SubstituedHeadTeacher;
                    }
                }

                if (headTeacherID.HasValue && headTeacherID.Value > 0)
                {
                    ReturnValue = headTeacherID.Value;
                }
                else
                {
                    ClassProfile objClassProfile = ClassProfileBusiness.Find(ClassID);
                    if (objClassProfile != null && objClassProfile.HeadTeacherID != null)
                    {
                        ReturnValue = objClassProfile.HeadTeacherID.Value;
                    }
                    // AnhVD9 - Bổ sung quyền giáo vụ là GVCN
                    if (ReturnValue == 0)
                    {
                        ClassSupervisorAssignment objClassSuperAssignment = (from csa in ClassSupervisorAssignmentBusiness.All
                                                                             where csa.SchoolID == SchoolID
                                                                             && csa.AcademicYearID == AcademicYearID
                                                                             && csa.ClassID == ClassID
                                                                             && csa.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                                             select csa).FirstOrDefault();
                        if (objClassSuperAssignment != null)
                        {
                            ReturnValue = objClassSuperAssignment.TeacherID;
                        }
                    }
                }
                return ReturnValue;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return 0;
            }
        }
        public List<TeacherBO> GetListTeacherWithTeachingSubject(int SchoolID, int AcademicYearID, int AppliedLevel, int EducationLevel, int ClassID, int SubjectID, int Semester, bool IsRolePrincipal = false, int EmployeeID = 0)
        {
            try
            {
                List<TeacherBO> lstEmployee = new List<TeacherBO>();
                List<TeacherBO> lstResult = new List<TeacherBO>();
                TeacherBO objEmployee = null;

                List<int> lstEducationLevel = EducationLevelBusiness.GetByGrade(AppliedLevel).Select(o => o.EducationLevelID).ToList();

                List<int> lstClassId = ClassProfileBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && o.IsActive.HasValue
                    && o.IsActive.Value && lstEducationLevel.Contains(o.EducationLevelID)).Select(o => o.ClassProfileID).ToList();

                //Lay giao vien chu nhiem cua lop
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                if (cp.Employee != null && cp.Employee.IsActive == true && (cp.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING || cp.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_BREATHER))
                {
                    lstEmployee.Add(new TeacherBO
                    {
                        TeacherID = cp.Employee.EmployeeID,
                        FullName = cp.Employee.FullName,
                        SchoolFacultyName = cp.Employee.SchoolFaculty.FacultyName,
                        Mobile = cp.Employee.Mobile
                    });
                }

                //Lay giao vu
                List<ClassSupervisorAssignment> lsClassSupervisorAssignmentAll = ClassSupervisorAssignmentBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && lstClassId.Contains(o.ClassID)
                    && (o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER)).ToList();
                List<ClassSupervisorAssignment> lsClassSupervisorAssignment = lsClassSupervisorAssignmentAll.Where(o => o.ClassID == ClassID && o.Employee.IsActive == true && (o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING || o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_BREATHER)).ToList();

                lstEmployee.AddRange(lsClassSupervisorAssignment.Select(o => new TeacherBO
                {
                    TeacherID = o.TeacherID,
                    FullName = o.Employee.FullName,
                    SchoolFacultyName = o.Employee.SchoolFaculty.FacultyName,
                    Mobile = o.Employee.Mobile
                }));

                //Giao vien bo mon
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["Semester"] = Semester;
                dic["EducationLevel"] = EducationLevel;
                dic["ClassID"] = ClassID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["RemoveVNEN"] = true;
                dic["IsActive"] = true;

                var lstGVBM = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dic).Where(o => o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING || o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_BREATHER);
                lstEmployee.AddRange(lstGVBM.Select(o => new TeacherBO
                {
                    TeacherID = o.TeacherID,
                    FullName = o.Employee.FullName,
                    SchoolFacultyName = o.Employee.SchoolFaculty.FacultyName,
                    Mobile = o.Employee.Mobile
                }));

                //Fill thong tin mon day
                //Lay danh sach cac lop chu nhiem
                List<ClassProfile> lstCp = ClassProfileBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                    && o.IsActive.HasValue && o.IsActive.Value
                    && o.HeadTeacherID.HasValue
                    && lstClassId.Contains(o.ClassProfileID)).ToList();

                //Danh sach phan cong giang day
                dic = new Dictionary<string, object>();
                dic["Semester"] = Semester;
                dic["AcademicYearID"] = AcademicYearID;
                dic["RemoveVNEN"] = true;

                var lstTeachingAssignmentAll = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dic)
                    .Where(o => o.ClassID.HasValue && lstClassId.Contains(o.ClassID.Value)).ToList();

                if (IsRolePrincipal)
                {
                    lstEmployee = lstEmployee.Where(p => p.TeacherID != EmployeeID).ToList();
                }

                var employeetmp = (from e in lstEmployee
                                   select new
                                   {
                                       TeacherID = e.TeacherID,
                                       FullName = e.FullName,
                                       SchoolFacultyName = e.SchoolFacultyName,
                                       Mobile = e.Mobile
                                   }).Distinct().ToList();
                for (int i = 0; i < employeetmp.Count; i++)
                {
                    var emp = employeetmp[i];
                    objEmployee = new TeacherBO();
                    objEmployee.TeacherID = emp.TeacherID;
                    objEmployee.FullName = emp.FullName;
                    objEmployee.SchoolFacultyName = emp.SchoolFacultyName;
                    objEmployee.Mobile = emp.Mobile;
                    StringBuilder sb = new StringBuilder();

                    //Lay danh sach mon day
                    var lstTa = (from ta in lstTeachingAssignmentAll.Where(o => o.TeacherID == emp.TeacherID).OrderBy(o => o.ClassProfile.EducationLevelID)
                                     .ThenBy(o => o.ClassProfile.OrderNumber != null ? o.ClassProfile.OrderNumber : 0).ThenBy(o => o.ClassProfile.DisplayName)
                                 group ta by new { ta.SubjectID, ta.SubjectCat.SubjectName } into g
                                 select new
                                 {
                                     SubjectID = g.Key.SubjectID,
                                     SubjectName = g.Key.SubjectName,
                                     ClassesName = g.Select(o => o.ClassProfile.DisplayName)
                                 }).ToList();


                    for (int j = 0; j < lstTa.Count; j++)
                    {
                        var ta = lstTa[j];
                        sb.Append(string.Format("{0} ({1})", ta.SubjectName, string.Join(", ", ta.ClassesName)));
                        sb.Append(", ");
                    }

                    //Danh sach cac lop chu nhiem
                    var lstHeadClass = lstCp.Where(o => o.HeadTeacherID == emp.TeacherID).OrderBy(o => o.EducationLevelID)
                                     .ThenBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    if (lstHeadClass.Count > 0)
                    {
                        sb.Append(string.Format("Chủ nhiệm ({0})", string.Join(", ", lstHeadClass.Select(o => o.DisplayName))));
                        sb.Append(", ");
                    }

                    //Danh sach cac lop giao vu
                    var lsClassSupervisorAssignmentByTeacher = lsClassSupervisorAssignmentAll.Where(o => o.TeacherID == emp.TeacherID)
                        .OrderBy(o => o.ClassProfile.EducationLevelID)
                                     .ThenBy(o => o.ClassProfile.OrderNumber != null ? o.ClassProfile.OrderNumber : 0).ThenBy(o => o.ClassProfile.DisplayName).ToList();

                    if (lsClassSupervisorAssignmentByTeacher.Count > 0)
                    {
                        sb.Append(string.Format("Giáo vụ ({0})", string.Join(", ", lsClassSupervisorAssignmentByTeacher.Select(o => o.ClassProfile.DisplayName).Distinct().ToList())));
                    }

                    objEmployee.TeacherSubject = sb.ToString();
                    if (objEmployee.TeacherSubject.EndsWith(", "))
                    {
                        objEmployee.TeacherSubject = objEmployee.TeacherSubject.Remove(objEmployee.TeacherSubject.Count() - 2, 2);
                    }
                    lstResult.Add(objEmployee);
                }
                return lstResult;
            }
            catch (Exception e)
            {
                logger.Error(e.Message, e);
                return new List<TeacherBO>();
            }
        }
        public List<UserBO> GetListFullUsernameByEmployeeID(int SchoolID,List<int> lstEmployeeID)
        {
            try
            {
                SchoolProfile objSchoolProfile = SchoolProfileBusiness.Find(SchoolID);
                List<UserAccount> UserAccountList = UserAccountBusiness.All.Where(u => u.CreatedUserID == objSchoolProfile.AdminID.Value && u.IsAdmin == false).ToList();
                List<UserBO> lstAccountByTeacher = (from a in UserAccountList
                                                    where (a.EmployeeID.HasValue && lstEmployeeID.Contains(a.EmployeeID.Value))
                                                    select new UserBO
                                                    {
                                                        EmployeeID = a.EmployeeID.Value,
                                                        UserName = Membership.GetUser(a.GUID).UserName
                                                    }).ToList();
                return lstAccountByTeacher;
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<UserBO>();
            }
        }
        public List<EmployeeBO> GetListTeacherByContractGroup(IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ContactGroupID = Utils.GetInt(dic, "ContactGroupID");
            var iquery = (from e in EmployeeBusiness.All
                          join tc in TeacherContactBusiness.All on e.EmployeeID equals tc.TEACHER_ID into l
                          from l1 in l.DefaultIfEmpty()
                          select new EmployeeBO
                        {
                            SchoolID = e.SchoolID,
                            EmployeeID = e.EmployeeID,
                            ContactGroupID = l1 != null ? l1.CONTACT_GROUP_ID : 0,
                            FullName = e.FullName
                        });
            if (SchoolID > 0)
            {
                iquery = iquery.Where(p => p.SchoolID == SchoolID);
            }
            if (ContactGroupID > 0)
            {
                iquery = iquery.Where(p => p.ContactGroupID == ContactGroupID);
            }
            return iquery.ToList();
        }
    }
}
