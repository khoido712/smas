﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class LockedMarkDetailBusiness
    {
        #region Them moi
        /// <summary>
        /// Them moi
        /// </summary>
        /// <param name="ListLockedMarkDetail"></param>
        public void InsertLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail, int SchoolID, int AcademicYearID, int AppliedLevel, int Semester)
        {
            if (ListLockedMarkDetail == null || ListLockedMarkDetail.Count == 0)
            {
                return;
            }

            List<int> lstSub = SubjectCatBusiness.All.Where(o => o.AppliedLevel == AppliedLevel).Select(o => o.SubjectCatID).ToList();
            foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
            {
                if (!lstSub.Contains(LockedMarkDetail.SubjectID))
                {
                    List<object> Params = new List<object>();
                    Params.Add("SubjectCat_Label_AllTitle");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);
                }

                // Kiem tra kieu diem phai co trong CSDL
                List<int> listMarkTypeID = new List<int>();
                if (LockedMarkDetail.MarkTypeID.HasValue)
                {
                    if (!listMarkTypeID.Contains(LockedMarkDetail.MarkTypeID.Value))
                    {
                        MarkTypeBusiness.CheckAvailable(LockedMarkDetail.MarkTypeID, "MarkType_Label_AllTitle", true);
                    }
                    else
                    {
                        listMarkTypeID.Add(LockedMarkDetail.MarkTypeID.Value);
                    }
                }
            }
            // Kiem tra lop hoc co ton tai trong CSDL
            // Lay danh sach tat cac lop hoc trong nam hoc cua truong
            List<int> listClassID = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID, new Dictionary<string, object>() { { "AppliedLevel", AppliedLevel } })
                .Select(o => o.ClassProfileID).ToList();
            foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
            {
                if (!listClassID.Contains(LockedMarkDetail.ClassID))
                {
                    List<object> Params = new List<object>();
                    Params.Add("ClassProfile_Label_AllTitle");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);
                }
            }
            this.context.Configuration.AutoDetectChangesEnabled = false;
            foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
            {
                base.Insert(LockedMarkDetail);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
        }
        #endregion

        /// <summary>
        /// QuangLM1
        /// Insert vao CSDL thong tin khoa diem. Khong su dung
        /// </summary>
        /// <param name="ListLockedMarkDetail"></param>
        public void InsertLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail)
        {
            try
            {
                if (ListLockedMarkDetail == null || ListLockedMarkDetail.Count == 0)
                {
                    return;
                }
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
                {
                    //LockedMarkDetail.Last2digitNumberSchool = UtilsBusiness.GetPartionId(LockedMarkDetail.SchoolID.Value);
                    this.Insert(LockedMarkDetail);
                }
                this.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertLockedMarkDetail", "null", ex);
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        #region Cap nhat
        /// <summary>
        /// Cap nhat. Khong su dung
        /// </summary>
        /// <param name="ListLockedMarkDetail"></param>
        public void UpdateLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail)
        {
            if (ListLockedMarkDetail == null || ListLockedMarkDetail.Count == 0)
            {
                return;
            }
            foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
            {
                // Kiem tra nam hoc co ton tai trong CSDL
                AcademicYearBusiness.CheckAvailable(LockedMarkDetail.AcademicYearID, "AcademicYear_Label_Year", true);
                // Kiem tra truong hoc co ton tai trong CSDL
                SchoolProfileBusiness.CheckAvailable(LockedMarkDetail.SchoolID, "SchoolProfile_Label_SchoolID", true);
                // Kiem tra lop hoc co ton tai trong CSDL
                ClassProfileBusiness.CheckAvailable(LockedMarkDetail.ClassID, "ClassProfile_Label_AllTitle", true);
                // Kiem tra mon hoc co ton tai trong CSDL
                SubjectCatBusiness.CheckAvailable(LockedMarkDetail.SubjectID, "SubjectCat_Label_AllTitle", true);
                // Kiem tra kieu diem phai co trong CSDL
                MarkTypeBusiness.CheckAvailable(LockedMarkDetail.MarkTypeID, "MarkType_Label_AllTitle", true);
            }
            foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
            {
                LockedMarkDetail.Last2digitNumberSchool = UtilsBusiness.GetPartionId(LockedMarkDetail.SchoolID.Value);
                base.Update(LockedMarkDetail);
            }
        }
        #endregion

        #region Xoa du lieu
        /// <summary>
        /// Xoa du lieu. Khong su dung
        /// </summary>
        /// <param name="ListLockedMarkDetail"></param>
        public void DeleteLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail, int SchoolID)
        {
            foreach (LockedMarkDetail LockedMarkDetail in ListLockedMarkDetail)
            {
                // Kiem tra co trong CSDL
                CheckAvailable(LockedMarkDetail.LockedMarkDetailID, "LockedMarkDetail_Label_LockedMarkDetailID", false);

                if (LockedMarkDetail.SchoolID != SchoolID)
                {
                    throw new BusinessException("LockedMarkDetail_Label_School_Err");
                }
            }
            DeleteAll(ListLockedMarkDetail);
        }
        #endregion

        #region Tim kiem
        /// <summary>
        /// Tim kiem trong bang theo dieu kien tim kiem
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<LockedMarkDetail> SearchLockedMarkDetail(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");

            IQueryable<LockedMarkDetail> ListLockedMarkDetail = LockedMarkDetailRepository.All;
            if (ClassID != 0)
            {
                ListLockedMarkDetail = from lmd in ListLockedMarkDetail
                                       join cp in ClassProfileBusiness.All on lmd.ClassID equals cp.ClassProfileID
                                       where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                       && lmd.ClassID == ClassID
                                       select lmd;
            }
            if (SchoolID != 0)
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolID, 100);
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.SchoolID == SchoolID && x.Last2digitNumberSchool == partitionId);
            }
            if (EducationLevelID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (AppliedLevel != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }
            if (AcademicYearID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (SubjectID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.SubjectID == SubjectID);
            }
            if (Semester != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.Semester == Semester);
            }
            return ListLockedMarkDetail;
        }
        #endregion

        #region Tim kiem theo truong
        /// <summary>
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<LockedMarkDetail> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.SearchLockedMarkDetail(SearchInfo);
        }
        #endregion

        /// <summary>
        /// Quanglm1
        /// Kiem tra xem con diem bi khoa o client truyen len co bi mo khoa tu phia server hay khong
        /// Neu co thi se bao loi de tranh truong hop du lieu diem bi xoa
        /// </summary>
        /// <param name="clientLockMarkTitle"></param>
        /// <param name="classID"></param>
        /// <param name="subjectID"></param>
        public void CheckCurrentLockMark(string clientLockMarkTitle, Dictionary<string, object> dic)
        {
            if (string.IsNullOrWhiteSpace(clientLockMarkTitle))
            {
                return;
            }
            
            // Danh sach con diem bi khoa tu client truyen len
            
            List<string> listLockTitle = clientLockMarkTitle.Split(',').Where(o => !string.IsNullOrWhiteSpace(o)).Select(v => v.Trim()).ToList();

            int semesterId = Utils.GetInt(dic["Semester"], 0);
            int classId = Utils.GetInt(dic["ClassID"], 0);
            int subjectId = Utils.GetInt(dic["SubjectID"], 0);
            int SchoolID = Utils.GetInt(dic["SchoolID"], 0);
            int academicYearID = Utils.GetInt(dic["AcademicYearID"], 0);   
            //Danh sach cac cot diem bi khoa trong he thong
            string strLockMarkTitle = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, classId, semesterId, subjectId, 0);

            /*List<LockedMarkDetail> listLockMark = MarkRecordBusiness.GetLockMarkTitle(dic).ToList();
            if (listLockMark.Count == 0)
            {
                // Tren server khong co thi bao loi
                List<object> listParam = new List<object>();
                listParam.Add(string.Join(",", listLockTitle));
                throw new BusinessException("LockMarkPrimary_Label_ErrorNotSameLock", listParam);
            }*/

            // Danh sach con diem bi khoa hien tai
            List<string> listCurrentLockTitle = strLockMarkTitle.Split(',').ToList(); //listLockMark.Select(o => (o.MarkType.Title != SystemParamsInFile.MARK_TYPE_HK && o.MarkType.Title != SystemParamsInFile.MARK_TYPE_LHK) ? o.MarkType.Title + o.MarkIndex.ToString() : o.MarkType.Title).ToList();
            // Loai bo nhung con diem khoa trung giua client truyen len va du lieu khoa hien tai
            listLockTitle.RemoveAll(s => listCurrentLockTitle.Contains(s));
            // Neu ton tai con diem bi khoa o client nhung hien tai da mo khoa thi bao loi
            if (listLockTitle.Count > 0)
            {
                List<object> listParam = new List<object>();
                listParam.Add(string.Join(",", listLockTitle));
                throw new BusinessException("LockMarkPrimary_Label_ErrorNotSameLock", listParam);
            }
        }

        /// <summary>
        /// 27/03/2014
        /// Quanglm1
        /// Ap dung khoa diem cho toan truong va toan khoi
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        public void ApplyLockMark(int SchoolID, int AcademicYearID, int Semester, int Grade, int ClassID, int EducationLevelID = 0)
        {
            this.context.PROC_APPLY_LOCK_MARK(SchoolID, AcademicYearID, Semester, Grade, EducationLevelID, ClassID);
        }
    }
}