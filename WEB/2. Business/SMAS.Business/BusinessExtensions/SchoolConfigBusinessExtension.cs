/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class SchoolConfigBusiness
    {
        /// <summary>
        /// Insert to database
        /// </summary>
        /// <auther>TuTV</auther>
        /// <param name="schoolConfig"></param>
        public void InsertSchoolConfig(SMS_SCHOOL_CONFIG schoolconfigObj)
        {
            try
            {
                SchoolConfigBusiness.Insert(schoolconfigObj);
                SchoolConfigBusiness.Save();
            }
            catch (Exception ex)
            {
                string para = string.Format("Message={0},SchoolID={1}",ex.Message,schoolconfigObj.SCHOOL_ID);
                logger.ErrorFormat(para);
            }
        }
        /// <summary>
        /// Update School Config
        /// </summary>
        /// <auther>TuTV</auther>
        /// <param name="schoolconfigObj"></param>
        public void UpdateSchoolConfig(SMS_SCHOOL_CONFIG schoolconfigObj)
        {
            try
            {
                SMS_SCHOOL_CONFIG schoolconfigUpdateObj = this.All.Where(p => p.SCHOOL_ID == schoolconfigObj.SCHOOL_ID).FirstOrDefault();
                if (schoolconfigUpdateObj != null)
                {
                    schoolconfigUpdateObj.UPDATED_TIME = DateTime.Now;
                    schoolconfigUpdateObj.SCHOOL_ID = schoolconfigObj.SCHOOL_ID;
                    schoolconfigUpdateObj.IS_SHOW_ALL_MARK = schoolconfigObj.IS_SHOW_ALL_MARK;
                    schoolconfigUpdateObj.IS_SHOW_CAPACITY1 = schoolconfigObj.IS_SHOW_CAPACITY1;
                    schoolconfigUpdateObj.IS_SHOW_CAPACITY2 = schoolconfigObj.IS_SHOW_CAPACITY2;
                    schoolconfigUpdateObj.IS_SHOW_LOCK_MARK = schoolconfigObj.IS_SHOW_LOCK_MARK;
                    schoolconfigUpdateObj.IS_SHOW_TEACHER_MOBILE = schoolconfigObj.IS_SHOW_TEACHER_MOBILE;
                    schoolconfigUpdateObj.IS_SHOW_REGISTER_SLL = schoolconfigObj.IS_SHOW_REGISTER_SLL;
                    schoolconfigUpdateObj.IS_ALLOW_SP2_ONLINE = schoolconfigObj.IS_ALLOW_SP2_ONLINE;
                    schoolconfigUpdateObj.IS_ALLOW_SEND_HEADTEACHER = schoolconfigObj.IS_ALLOW_SEND_HEADTEACHER;
                    schoolconfigUpdateObj.IS_ALLOW_SEND_UNICODE = schoolconfigObj.IS_ALLOW_SEND_UNICODE;
                    schoolconfigUpdateObj.PAYMENT_TYPE = schoolconfigObj.PAYMENT_TYPE;
                    schoolconfigUpdateObj.PRE_FIX = schoolconfigObj.PRE_FIX;
                    schoolconfigUpdateObj.NAME_DISPLAYING = schoolconfigObj.NAME_DISPLAYING;
                    SchoolConfigBusiness.Update(schoolconfigUpdateObj);
                    SchoolConfigBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
        }
        /// <summary>
        /// Check current School Configurations
        /// </summary>
        ///  <auther>TuTV</auther>
        /// <param name="schoolID"></param>
        /// <returns></returns>
        public bool CheckExitSchoolIDInSchoolConfig(int? schoolID)
        {
            SMS_SCHOOL_CONFIG schoolconfigObj = this.All.Where(p => p.SCHOOL_ID == schoolID).FirstOrDefault();
            if (schoolconfigObj != null)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Get data show in view
        /// </summary>
        ///  <auther>TuTV</auther>
        /// <param name="schoolID"></param>
        /// <returns></returns>
        public SMS_SCHOOL_CONFIG GetSchoolConfigBySchoolID(int? schoolID)
        {
            SMS_SCHOOL_CONFIG schoolconfigObj = this.All.Where(p => p.SCHOOL_ID == schoolID).FirstOrDefault();
            if (schoolconfigObj != null)
            {
                return schoolconfigObj;
            }
            return null;
        }
    }
}
