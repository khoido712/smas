/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{ 
    public partial class WeeklyMenuBusiness
    {
        /// <summary>
        /// Tamhm1
        /// 14/11/2012
        /// </summary>
        /// <param name="dic"></param>
        /// <returns>IQueryable<WeeklyMenu></returns>
        private IQueryable<WeeklyMenu> Search(IDictionary<string, object> dic)
        {
            int WeeklyMenuID = Utils.GetInt(dic, "WeeklyMenuID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? MenuType = Utils.GetInt(dic, "MenuType");
            string WeeklyMenuCode = Utils.GetString(dic, "WeeklyMenuCode");
            int? EatingGroupID = Utils.GetInt(dic, "EatingGroupID");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            DateTime? _ToDate = null;
            if (ToDate.HasValue)
            {
                _ToDate = ToDate.Value.AddDays(-5);
            }
            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<WeeklyMenu> lstWeeklyMenu = this.WeeklyMenuRepository.All;

            if (isActive.HasValue)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.IsActive == isActive));
            }

            if (WeeklyMenuID != 0)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.WeeklyMenuID == WeeklyMenuID));
            }

            if (SchoolID != 0)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.SchoolID == SchoolID));
            }

            if (AcademicYearID != 0)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.AcademicYearID == AcademicYearID));
            }

            if (MenuType != 0)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.MenuType == MenuType));
            }

            if (WeeklyMenuCode.Trim().Length != 0)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.WeeklyMenuCode.ToUpper().Contains(WeeklyMenuCode.ToUpper())));
            }
            if (EatingGroupID != 0)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.EatingGroupID == EatingGroupID));
            }
            if (FromDate != null)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.StartDate >= FromDate.Value));
            }
            if (ToDate != null)
            {
                lstWeeklyMenu = lstWeeklyMenu.Where(em => (em.StartDate <= _ToDate.Value));
            }

            return lstWeeklyMenu;
        }
        
     /// <summary>
     ///  Thực đơn tuần theo trường
     /// Tamhm1
     /// 14/11/2012
     /// </summary>
     /// <param name="SchoolID">ID Trường học</param>
     /// <param name="dic"></param>
     /// <returns>IQueryable<WeeklyMenu></returns>
        public IQueryable<WeeklyMenu> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Tìm kiếm thực đơn tuần bình thường
        /// Author: Tamhm1
        /// Dat: 14/11/2012
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<WeeklyMenu> SearchNormalMenu(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["MenuType"] = GlobalConstants.MENU_TYPE_NORMAL;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Tìm kiếm thực đơn tuần bổ sung
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<WeeklyMenu> SearchAdditionalMenu(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                SearchInfo["MenuType"] = GlobalConstants.MENU_TYPE_ADDITIONAL;
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Validate thực đơn
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        private void Validate(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            ValidationMetadata.ValidateObject(entity);
            //AcademicYearID, SchoolID: not compatiable(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", new Dictionary<string, object>()
            {
                {"SchoolID", entity.SchoolID},
                {"AcademicYearID", entity.AcademicYearID}
            }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //WeeklyMenuCode, SchoolID: duplicate(WeeklyMenu)
            bool IsweeklyMenuCode = new WeeklyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "WeeklyMenu", new Dictionary<string, object>()
            {
                {"WeeklyMenuCode", entity.WeeklyMenuCode},
                {"SchoolID", entity.SchoolID}
            },
            new Dictionary<string, object>()
            {
                {"WeeklyMenuID", entity.WeeklyMenuID}
            });
            if (IsweeklyMenuCode)
            {
                string ResKey = "DailyMenu_Lable_Code";
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            //StartDate, SchoolID, EatingGroupID, MenuType: duplicate(WeeklyMenu)
            bool isCheck = new WeeklyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "WeeklyMenu", new Dictionary<string, object>()
            {
                {"StartDate", entity.StartDate}, 
                {"SchoolID", entity.SchoolID},
                {"EatingGroupID", entity.EatingGroupID},
                {"MenuType", entity.MenuType}
            }, new Dictionary<string, object>()
            {
                {"WeeklyMenuID", entity.WeeklyMenuID}
            });
            if (isCheck)
            {
                throw new BusinessException("Common_Validate_NotDuplicate_WeeklyMenu");
            }
            //StartDate >= DateTime.Now
            Utils.ValidateFutureDate(entity.StartDate, "WeeklyMenu_Label_StartDate");
            //StartDate phải là ngày thứ 2.
            if(entity.StartDate.DayOfWeek != DayOfWeek.Monday)
            {
                throw new BusinessException("WeeklyMenu_Label_StartDateIsMonday");
            }
            //Với mỗi WeeklyMenuDetail trong ListWeeklyMenuDetail:
            //DailyMenuID, Entity.EatingGroupID, Entity.SchoolID, Entity.MenuType,  Entity.AcademicYearID: not compatiable (DailyMenu)

            foreach (WeeklyMenuDetail weeklyMenuDetail in lstWeeklyMenuDetail)
            {
                bool isExistWeeklyMenuDetail = new DailyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DailyMenu", new Dictionary<string, object>()
                {
                    {"DailyMenuID", weeklyMenuDetail.DailyMenuID},
                    {"EatingGroupID", entity.EatingGroupID},
                    {"SchoolID", entity.SchoolID},
                    {"MenuType", entity.MenuType},
                    {"AcademicYearID", entity.AcademicYearID}
                }, null);
                if (!isExistWeeklyMenuDetail)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            //Kiểm tra lstWeeklyMenuDetail có bằng 6 phần tử hay không?
            if (lstWeeklyMenuDetail.Count != 6)
            {
                throw new BusinessException("WeeklyMenu_Label_ErrorData");
            }
            if (lstWeeklyMenuDetail.Count == 6)
            {
               //if(lstWeeklyMenuDetail.Contains
                string str = "";
                foreach (WeeklyMenuDetail weeklyMenuDetail in lstWeeklyMenuDetail)
                {
                    str = str + weeklyMenuDetail.DayOfWeek.Value.ToString()+",";
                }
                if (!str.Contains(SystemParamsInFile.DAY_OF_WEEK_MONDAY.ToString()))
                {
                    throw new BusinessException("WeeklyMenu_Label_DayOfWeekMonday");
                }
                if (!str.Contains(SystemParamsInFile.DAY_OF_WEEK_TUESDAY.ToString()))
                {
                    throw new BusinessException("WeeklyMenu_Label_DayOfWeekTuesday");
                }
                if (!str.Contains(SystemParamsInFile.DAY_OF_WEEK_WEDNESDAY.ToString()))
                {
                    throw new BusinessException("WeeklyMenu_Label_DayOfWeekWednesday");
                }
                if (!str.Contains(SystemParamsInFile.DAY_OF_WEEK_THURSDAY.ToString()))
                {
                    throw new BusinessException("WeeklyMenu_Label_DayOfWeekThursday");
                }
                if (!str.Contains(SystemParamsInFile.DAY_OF_WEEK_FRIDAY.ToString()))
                {
                    throw new BusinessException("WeeklyMenu_Label_DayOfWeekFriday");
                }
                if (!str.Contains(SystemParamsInFile.DAY_OF_WEEK_SATURDAY.ToString()))
                {
                    throw new BusinessException("WeeklyMenu_Label_DayOfWeekSaturday");
                }
            }
            //Kiểm tra năm học có phải là năm học hiện tại hay không?
            if (!AcademicYearBusiness.IsCurrentYear(entity.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
        }
        /// <summary>
        /// Thêm mới thực đơn tuần.
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        private void Insert(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            Validate(entity, lstWeeklyMenuDetail);            
            foreach(var item in lstWeeklyMenuDetail)
            {
                item.WeeklyMenuID = entity.WeeklyMenuID;                
                WeeklyMenuDetailBusiness.Insert(item);
            }
            base.Insert(entity);
        }
        /// <summary>
        /// Thêm mới thực đơn tuần bình thường
        /// Author: Tamhm1
        /// Date: 14/10/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        public void InsertNormalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            entity.MenuType = GlobalConstants.MENU_TYPE_NORMAL;
            Insert(entity, lstWeeklyMenuDetail);
        }
        /// <summary>
        /// Thêm mới thực đơn bổ sung
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        public void InsertAdditionalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            entity.MenuType = GlobalConstants.MENU_TYPE_ADDITIONAL;
            Insert(entity, lstWeeklyMenuDetail);
        }

        /// <summary>
        /// Cập nhật thực đơn tuần
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        private void Update(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            Validate(entity, lstWeeklyMenuDetail);
            //WeeklyMenuID, SchoolID: not compatiable(WeeklyMenu)
            bool isCompatible = new WeeklyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "WeeklyMenu", new Dictionary<string, object>()
            {
                {"WeeklyMenuID", entity.WeeklyMenuID},
                {"SchoolID", entity.SchoolID}
            }, null);
            if (!isCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //WeeklyMenu weeklyMenu = WeeklyMenuRepository.Find(entity.WeeklyMenuID);
            //weeklyMenu.WeeklyMenuCode = entity.WeeklyMenuCode;
            //weeklyMenu.Note = entity.Note;
            //weeklyMenu.StartDate = entity.StartDate;
            //weeklyMenu.EatingGroupID = entity.EatingGroupID;
            //weeklyMenu.ModifiedDate = DateTime.Now;
            base.Update(entity);
            IDictionary<string, object> dic = new Dictionary<string, object>();
         
            dic["WeeklyMenuID"] = entity.WeeklyMenuID;
            List<WeeklyMenuDetail> lst = WeeklyMenuDetailBusiness.SearchBySchool(entity.SchoolID, dic).ToList();
            WeeklyMenuDetailBusiness.DeleteAll(lst);
            foreach (var item in lstWeeklyMenuDetail)
            {
                item.WeeklyMenuID = entity.WeeklyMenuID;
                WeeklyMenuDetailBusiness.Insert(item);
            }

        }
        /// <summary>
        /// Update thực đơn tuần bình thường
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        public void UpdateNormalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            if(Find(entity.WeeklyMenuID).MenuType == GlobalConstants.MENU_TYPE_NORMAL)
            Update(entity, lstWeeklyMenuDetail);
        }
        /// <summary>
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// Cap nhat thuc don tuan bo sung
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstWeeklyMenuDetail"></param>
        public void UpdateAdditionalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail)
        {
            if (Find(entity.WeeklyMenuID).MenuType == GlobalConstants.MENU_TYPE_ADDITIONAL)
            {
                Update(entity, lstWeeklyMenuDetail);
            }
        }
        /// <summary>
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// </summary>
        /// <param name="WeeklyMenuID"></param>
        /// <param name="SchoolID"></param>
        private void Delete(int WeeklyMenuID, int SchoolID)
        {
            WeeklyMenu entity = this.Find(WeeklyMenuID);
            //WeeklyMenuID, SchoolID: not compatiable(WeeklyMenu)
            bool IsCompatible = new WeeklyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "WeeklyMenu", new Dictionary<string, object>()
            {
                {"WeeklyMenuID", WeeklyMenuID},
                {"SchoolID", SchoolID}
            }, null);
            if (!IsCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(entity.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            //WeeklyMenuID: PK(WeeklyMenu)
            this.CheckAvailable(WeeklyMenuID, "WeeklyMenu_Label");
            //Thực hien xoa du lieu
            WeeklyMenu weeklyMenu = this.Find(WeeklyMenuID);
            if (weeklyMenu != null)
            {
                weeklyMenu.IsActive = false;
                weeklyMenu.ModifiedDate = DateTime.Now;
                base.Update(weeklyMenu);
            }

        }
        /// <summary>
        /// Author: Tamhm1
        /// Date: 14/11/2012
        /// Xoá thực đơn tuần bình thường
        /// </summary>
        /// <param name="WeeklyMenuID"></param>
        /// <param name="SchoolID"></param>
        public void DeleteNormalMenu(int WeeklyMenuID, int SchoolID)
        {
            if (Find(WeeklyMenuID).MenuType == (int)GlobalConstants.MENU_TYPE_NORMAL)
            {
                Delete(WeeklyMenuID, SchoolID);
            }
            else
            {
                throw new BusinessException("WeeklyMenu_Label_DeleteNormalMenu");
            }
        }

        /// <summary>
        /// Xoá thực đơn bổ sung
        /// Tamhm1
        /// 14/11/2012
        /// </summary>
        /// <param name="WeeklyMenuID"></param>
        /// <param name="SchoolID"></param>
        public void DeleteAdditionalMenu(int WeeklyMenuID, int SchoolID)
        {
            if (Find(WeeklyMenuID).MenuType == (int)GlobalConstants.MENU_TYPE_ADDITIONAL)
            {
                Delete(WeeklyMenuID, SchoolID);
            }
            else
            {
                throw new BusinessException("WeeklyMenu_Label_DeleteAdditionalMenu");
            }
        }
     
    }
}
