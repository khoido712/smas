using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.DAL.IRepository;
using log4net;
using SMAS.DAL.Repository;
using SMAS.Business.IBusiness;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>11/29/2012</date>
    public partial class ClassifiedCapacityByPeriodBusiness : GenericBussiness<ProcessedReport>, IClassifiedCapacityByPeriodBusiness
    {
        public const string REPORT_CODE = "ThongKeXepLoaiHocLucTheoDot";
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ClassifiedCapacityByPeriodBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        private WorkBookData BuildReportData(TranscriptOfClass Entity, List<StatisticsConfig> ListConfig,
            List<EducationLevel> ListEducationLevel, List<ClassInfo> ListClass, List<ReportData> ListRanking)
        {
            WorkBookData WorkBookData = new WorkBookData();

            SheetData SheetData = new SheetData("Report");

            // Bo sung du lieu hoc luc cho ClassInfo
            foreach (ClassInfo Class in ListClass)
            {
                foreach (StatisticsConfig config in ListConfig)
                {
                    int CapacityLevelID = int.Parse(config.EqualValue);
                    Class.CapacityInfo[config.StatisticsConfigID.ToString()] = ListRanking.Where(o => o.ClassID == Class.ClassID && o.CapacityLevelID == CapacityLevelID).Count();
                }
            }

            // Nhom du lieu theo cap hoc
            List<EducationLevelInfo> ListEduInfo = new List<EducationLevelInfo>();
            int index = 1;
            foreach (EducationLevel edu in ListEducationLevel)
            {
                EducationLevelInfo EduInfo = new EducationLevelInfo();
                EduInfo.Index = index++;
                EduInfo.EducationLevelID = edu.EducationLevelID;
                EduInfo.EducationLevelName = edu.Resolution;

                List<ClassInfo> SubListClass = ListClass.Where(o => o.EducationLevelID == edu.EducationLevelID).ToList();
                EduInfo.ListClassInfo = SubListClass;

                ListEduInfo.Add(EduInfo);
            }

            SheetData.Data["ListEduInfo"] = ListEduInfo;
            SheetData.Data["ListConfig"] = ListConfig;


            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = School.Province.ProvinceName;
            string DeptName = School.SupervisingDept.SupervisingDeptName;
            //string SubjectName = SubjectCatBusiness.Find(Entity.SubjectID).DisplayName;
            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            SheetData.Data["AcademicYear"] = AcademicYear;
            SheetData.Data["SchoolName"] = SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = ProvinceName;
            SheetData.Data["DeptName"] = DeptName.ToUpper();
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["PeriodName"] = PeriodName;
            SheetData.Data["Semester"] = Semester;
            SheetData.Data["SubjectName"] = "";


            WorkBookData.ListSheet.Add(SheetData);
            return WorkBookData;
        }

        private IVTWorkbook WriteToExcel(WorkBookData Data, string ReportCode)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet Template = oBook.GetSheet(1);
            IVTWorksheet Sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
            SheetData SheetData = Data.ListSheet.FirstOrDefault();
            List<StatisticsConfig> ListConfig = (List<StatisticsConfig>)SheetData.Data["ListConfig"];
            List<EducationLevelInfo> ListEduInfo = (List<EducationLevelInfo>)SheetData.Data["ListEduInfo"];

            int i = 0;
            int StartRow = 9;
            int StartDataRow = StartRow + 3;
            int NormalRowID = StartDataRow;
            int EduRow = StartDataRow + 2;
            int ThickRowID = StartDataRow + 1;

            // Tao day du cac tieu de cot
            foreach (StatisticsConfig config in ListConfig)
            {
                Sheet.CopyPaste(Template.GetRange("F9", "G14"), StartRow, i * 2 + 6);
                Sheet.GetRange(StartRow, i * 2 + 6, StartRow, i * 2 + 6).Value = config.DisplayName;
                i++;
            }
            Sheet.CopyPaste(Template.GetRange("F9", "G14"), StartRow, i * 2 + 6);

            // Fill du lieu
            int CurrentRow = StartDataRow;
            int EduIndex = 1;
            int ClassIndex = 1;
            int NumOfClass = 0;
            string StrAllSchool = "";
            int j = 0;
            foreach (EducationLevelInfo edu in ListEduInfo)
            {
                // Fill du lieu cho Row Khoi hoc
                NumOfClass = edu.ListClassInfo.Count;
                if (NumOfClass == 0)
                {
                    continue;
                }

                Sheet.CopyAndInsertARow(EduRow, CurrentRow, true);
                NormalRowID++;
                ThickRowID++;
                EduRow++;


                Sheet.SetCellValue(CurrentRow, 1, EduIndex++);
                Sheet.SetCellValue(CurrentRow, 2, edu.EducationLevelName);
                Sheet.SetFormulaValue(CurrentRow, 4, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 4));
                Sheet.SetFormulaValue(CurrentRow, 5, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 5));

                j = 1;

                foreach (StatisticsConfig config in ListConfig)
                {
                    Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 5 + j));
                    j++;
                    Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                    j++;
                }
                Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 5 + j));
                j++;
                Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                j++;

                // Lay cac dong la EducationLevel de lap cong thuc tinh tong cho toan truong
                if (StrAllSchool == "")
                {
                    StrAllSchool = "{0}" + CurrentRow;
                }
                else
                {
                    StrAllSchool += "; {0}" + CurrentRow;
                }


                CurrentRow++;

                // Fill du lieu cho Row Lop hoc
                int k = 0;
                foreach (ClassInfo Class in edu.ListClassInfo)
                {
                    if (k++ < NumOfClass - 1)
                    {
                        Sheet.CopyAndInsertARow(NormalRowID, CurrentRow, true);
                    }
                    else
                    {
                        Sheet.CopyAndInsertARow(ThickRowID, CurrentRow, true);
                    }
                    NormalRowID++;
                    ThickRowID++;
                    EduRow++;

                    Sheet.SetCellValue(CurrentRow, 1, ClassIndex++);
                    Sheet.SetCellValue(CurrentRow, 2, Class.ClassName);
                    Sheet.SetCellValue(CurrentRow, 3, Class.TeacherName);
                    Sheet.SetCellValue(CurrentRow, 4, Class.NumOfPupil);
                    Sheet.SetCellValue(CurrentRow, 5, Class.ActualNumOfPupil);

                    j = 1;
                    string OverTB = "";
                    foreach (StatisticsConfig config in ListConfig)
                    {
                        if (config.MarkType == 2)
                        {
                            if (OverTB == "")
                            {
                                OverTB = VTVector.ColumnIntToString(5 + j) + CurrentRow;
                            }
                            else
                            {
                                OverTB += "; " + VTVector.ColumnIntToString(5 + j) + CurrentRow;
                            }
                        }

                        Sheet.SetCellValue(CurrentRow, 5 + j, Class.CapacityInfo[config.StatisticsConfigID.ToString()]);
                        j++;
                        Sheet.SetCellValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                        j++;
                    }
                    Sheet.SetFormulaValue(CurrentRow, 5 + j, string.Format("=SUM({0})", OverTB));
                    j++;
                    Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                    j++;

                    CurrentRow++;
                }

            }
            Sheet.GetRange(StartRow, 1, CurrentRow - 1, 5 + (ListConfig.Count + 1)*2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);


            // Fill du lieu cho row toan truong
            CurrentRow = StartRow + 2;
            Sheet.SetFormulaValue(CurrentRow, 4, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(4))));
            Sheet.SetFormulaValue(CurrentRow, 5, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(5))));

            j = 1;

            foreach (StatisticsConfig config in ListConfig)
            {
                Sheet.SetFormulaValue(CurrentRow, 5 + j, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(5 + j))));
                j++;
                Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                j++;
            }
            Sheet.SetFormulaValue(CurrentRow, 5 + j, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(5 + j))));
            j++;
            Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
            j++;

            
            Sheet.Name = SheetData.Name;
            Sheet.FillVariableValue(SheetData.Data);

            Sheet.DeleteRow(EduRow);
            Sheet.DeleteRow(ThickRowID);
            Sheet.DeleteRow(NormalRowID);

            oBook.GetSheet(1).Delete();
            return oBook;
        }

        private string GetStrSum(int FromRow, int ToRow, int Col)
        {
            return string.Format("=SUM({0}:{1})", VTVector.ColumnIntToString(Col) + FromRow, VTVector.ColumnIntToString(Col) + ToRow);
        }

        private string GetStrSumPercent(int Col, int Row)
        {
            return string.Format("=ROUND({0}/IF({1}<=0;1;{1});4)*100", VTVector.ColumnIntToString(Col - 1) + Row, VTVector.ColumnIntToString(5) + Row);
        }

        // ============= Public method ============================================

        /// <summary>
        /// Lấy mảng băm
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>AuNH</author>
        /// <date>11/21/2012</date>
        public string GetHashKey(TranscriptOfClass Entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodID"] = Entity.PeriodDeclarationID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            dic["SchoolID"] = Entity.SchoolID;
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertClassifiedCapacityByPeriod(TranscriptOfClass Entity, Stream Data)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = REPORT_CODE;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = this.GetHashKey(Entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            PeriodDeclaration period = this.PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            string periodName = period.Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(period.Semester.HasValue ? period.Semester.Value : 0);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(Entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(periodName));
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodID"] = Entity.PeriodDeclarationID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            dic["SchoolID"] = Entity.SchoolID;
            ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            ProcessedReportRepository.Insert(ProcessedReport);

            ProcessedReportParameterRepository.Save();
            ProcessedReportRepository.Save();

            return ProcessedReport;
        }

        public ProcessedReport GetClassifiedCapacityByPeriod(TranscriptOfClass Entity)
        {
            string ReportCode = REPORT_CODE;

            string HashKey = this.GetHashKey(Entity);
            IQueryable<ProcessedReport> query = this.ProcessedReportRepository.All.Where(o => o.ReportCode == ReportCode);
            query = query.Where(o => o.InputParameterHashKey == HashKey);
            query = query.OrderByDescending(o => o.ProcessedDate);

            return query.FirstOrDefault();
        }

        public Stream CreateClassifiedCapacityByPeriod(TranscriptOfClass Entity)
        {
            #region Danh sach lop, si so
            // Danh sach khoi
            List<EducationLevel> ListEducationLevel = EducationLevelBusiness.GetByGrade(Entity.AppliedLevel).ToList();


            //Danh sach lop hoc
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"IsVNEN",true}}).OrderBy(o => o.DisplayName);

            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"FemaleID",Entity.FemaleID},
                                                {"EthnicID",Entity.EthnicID}
                                                }).AddCriteriaSemester(AcademicYearBusiness.Find(Entity.AcademicYearID), Entity.Semester);

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                {"SubjectID",Entity.SubjectID},
                {"SemesterID",Entity.Semester},
                {"YearID",AcademicYearBusiness.Find(Entity.AcademicYearID).Year}
            };

            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID},
                    {"Semester",Entity.Semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0}
                };
            IQueryable<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATP);

            var iqClassInfo = from g in iqClassInfoBO
                              select new ClassInfo
                              {
                                  ClassID = g.ClassID,
                                  ClassName = g.ClassName,
                                  ClassOrderNumber = g.ClassOrderNumber,
                                  EducationLevelID = g.EducationLevelID,
                                  TeacherName = g.TeacherName,
                                  NumOfPupil = g.NumOfPupil,
                                  ActualNumOfPupil = g.ActualNumOfPupil
                              };
            List<ClassInfo> ListClass = iqClassInfo.ToList();
            #endregion

            IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                    {"AppliedLevel", Entity.AppliedLevel},
                                                    {"AcademicYearID", Entity.AcademicYearID},
                                                    {"PeriodID", Entity.PeriodDeclarationID}
                                                }).Where(u => lsPupilOfClassSemester.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID))
                                                .Where(o => o.CapacityLevelID != null);
            IQueryable<StatisticsConfig> iqStatisticsConfig = StatisticsConfigBusiness.GetIQueryable(Entity.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);
            // Danh sach cau hinh hoc luc
            List<StatisticsConfig> ListConfig = iqStatisticsConfig.ToList();

            var listMarkGroup = from s in ListConfig
                                join m in iqPupilRanking on 1 equals 1
                                where s.EqualValue == m.CapacityLevelID.ToString()
                                select new
                                {
                                    s.StatisticsConfigID,
                                    s.MinValue,
                                    s.MaxValue,
                                    s.MarkType,
                                    s.MappedColumn,
                                    m.SchoolID,
                                    m.EducationLevelID,
                                    m.Semester,
                                    m.PeriodID,
                                    m.ClassID
                                };

            var listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.StatisticsConfigID,
                                          g.MinValue,
                                          g.MaxValue,
                                          g.MarkType,
                                          g.MappedColumn,
                                          g.Semester,
                                          g.PeriodID,
                                          g.ClassID
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.EducationLevelID,
                                          c.Key.StatisticsConfigID,
                                          c.Key.MinValue,
                                          c.Key.MaxValue,
                                          c.Key.MarkType,
                                          c.Key.MappedColumn,
                                          c.Key.Semester,
                                          c.Key.PeriodID,
                                          c.Key.ClassID,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();


            WorkBookData WorkBookData = new WorkBookData();

            SheetData SheetData = new SheetData("Report");

            // Bo sung du lieu hoc luc cho ClassInfo
            foreach (ClassInfo Class in ListClass)
            {
                foreach (StatisticsConfig config in ListConfig)
                {
                    int CapacityLevelID = int.Parse(config.EqualValue);
                    var pupilRankingCount = listMarkGroupCount.Where(o => o.ClassID == Class.ClassID && o.StatisticsConfigID == config.StatisticsConfigID).FirstOrDefault();
                    Class.CapacityInfo[config.StatisticsConfigID.ToString()] = pupilRankingCount != null ? pupilRankingCount.CountMark : 0;
                }
            }

            // Nhom du lieu theo cap hoc
            List<EducationLevelInfo> ListEduInfo = new List<EducationLevelInfo>();
            int index = 1;
            foreach (EducationLevel edu in ListEducationLevel)
            {
                EducationLevelInfo EduInfo = new EducationLevelInfo();
                EduInfo.Index = index++;
                EduInfo.EducationLevelID = edu.EducationLevelID;
                EduInfo.EducationLevelName = edu.Resolution;

                List<ClassInfo> SubListClass = ListClass.Where(o => o.EducationLevelID == edu.EducationLevelID)
                .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();
                EduInfo.ListClassInfo = SubListClass;

                ListEduInfo.Add(EduInfo);
            }

            SheetData.Data["ListEduInfo"] = ListEduInfo;
            SheetData.Data["ListConfig"] = ListConfig;


            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = (School.District != null ? School.District.DistrictName : "");
            string DeptName = UtilsBusiness.GetSupervisingDeptName(School.SchoolProfileID,Entity.AppliedLevel);
            //string SubjectName = SubjectCatBusiness.Find(Entity.SubjectID).DisplayName;
            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            if (Period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            SheetData.Data["AcademicYear"] = AcademicYear;
            SheetData.Data["SchoolName"] = SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = ProvinceName;
            SheetData.Data["DeptName"] = DeptName.ToUpper();
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["PeriodName"] = PeriodName;
            SheetData.Data["Semester"] = Semester;
            SheetData.Data["SubjectName"] = "";


            WorkBookData.ListSheet.Add(SheetData);

            //Data = this.BuildReportData(Entity, ListConfig, ListEducationLevel, ListClass, ListRanking);

            IVTWorkbook oBook = this.WriteToExcel(WorkBookData, REPORT_CODE);
            return oBook.ToStream();
        }


        // ============= Private classes for private businesses ====================
        private class ReportData
        {
            public int ClassID { get; set; }
            public int PupilID { get; set; }
            public int? CapacityLevelID { get; set; }
        }

        private class EducationLevelInfo
        {
            public int Index { get; set; }
            public int EducationLevelID { get; set; }
            public string EducationLevelName { get; set; }
            public List<ClassInfo> ListClassInfo { get; set; }
            public EducationLevelInfo()
            {
                ListClassInfo = new List<ClassInfo>();
            }
        }

        private class ClassInfo
        {
            public int ClassID { get; set; }
            public string ClassName { get; set; }
            public string TeacherName { get; set; }
            public int EducationLevelID { get; set; }
            public int? ClassOrderNumber { get; set; }
            public int NumOfPupil { get; set; }
            public int ActualNumOfPupil { get; set; }

            public Dictionary<string, object> CapacityInfo { get; set; }

            public ClassInfo()
            {
                CapacityInfo = new Dictionary<string, object>();
            }
        }
    }
}
