﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using System.Drawing;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class TeachingScheduleApprovalBusiness
    {
        public IQueryable<TeachingScheduleApproval> Search(IDictionary<string, object> dic)
        {
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            int SchoolWeekID = Utils.GetInt(dic, "SchoolWeekID");
            int? Status = Utils.GetNullableInt(dic, "Status");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            string fromDateWeek = Utils.GetString(dic, "FromDate");

            IQueryable<TeachingScheduleApproval> query = TeachingScheduleApprovalBusiness.All;

            if (!string.IsNullOrEmpty(fromDateWeek))
            {
                DateTime fromDate = Convert.ToDateTime(fromDateWeek);
                query = query.Where(p => p.FromDate == fromDate);
            }

            if (AcademicYearID > 0)
            {
                query = query.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (SchoolID > 0)
            {
                query = query.Where(p => p.SchoolID == SchoolID);
            }
            if (AppliedLevelID > 0)
            {
                query = query.Where(p => p.AppliedLevelID == AppliedLevelID);
            }
            if (SchoolWeekID != 0)
            {
                query = query.Where(p => p.SchoolWeekID == SchoolWeekID);
            }

            if (EmployeeID > 0)
            {
                query = query.Where(p => p.EmployeeID == EmployeeID);
            }
            if (TeacherID > 0)
            {
                query = query.Where(p => p.TeacherID == TeacherID);
            }
            if (Status.HasValue)
            {
                query = query.Where(p => p.Status == Status);
            }
           
            return query;
        }        
    }
}