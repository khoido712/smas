﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ParticularPupilTreatmentBusiness
    {
        public void InsertParticularPupilTreatment(int UserID, ParticularPupilTreatment entity)
        {
            //Validate object
            ValidationMetadata.ValidateObject(entity);

            //Check exist Particular Pupil
            var particularPupil = this.ParticularPupilBusiness.Find(entity.ParticularPupilID);
            //Utils.ValidateNull(particularPupil, "ParticularPupilTreatment_Label_ParticularPupilID");

            //Check Exist Pupil
            //this.PupilProfileBusiness.CheckAvailable(entity.PupilID, "ParticularPupilTreatment_Label_PupilID");

            //Check tuong thich Particular Pupil
            bool ParticularPupilCompatible = new ParticularPupilRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ParticularPupil"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ParticularPupilID" , entity.ParticularPupilID},
                                                {"PupilID", entity.PupilID}
                                            }, null);
            if (!ParticularPupilCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Check UpdateDate < DateTime.Now
            //Check trong Metadata
            if (entity.UpdatedDate > DateTime.Now)
                throw new BusinessException("ParticularPupilTreatment_UpdateDateLessThanCurrentDate");

            if (entity.AppliedDate > DateTime.Now)
                throw new BusinessException("ParticularPupilTreatment_AppliedDateLessThanCurrentDate");
            // Thoi gian cap nhat phai lon hon thoi gian lon nhat cua danh sach truong do doi voi truong hop them moi
            IQueryable<ParticularPupilTreatment> listParticularPupil = this.SearchParticularPupilTreatment(
                new Dictionary<string, object>() {
                { "ParticularPupilID", entity.ParticularPupilID },
                {"PupilID", entity.PupilID}
                });
            if (listParticularPupil.Any(o => o.AppliedDate > entity.AppliedDate))
            {
                throw new BusinessException("ParticularPupil_Label_ExceptionFailMaxDate");
            }
            //Kiem tra quyen cua nguoi dung
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, particularPupil.ClassID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            base.Insert(entity);
        }

        public void UpdateParticularPupilTreatment(int UserID, ParticularPupilTreatment entity)
        {
            //Check exist Particular pupil treatment
            this.CheckAvailable(entity.ParticularPupilTreatmentID, "ParticularPupilTreatment_Label_ParticularPupilTreatmentID");

            //Validate object
            ValidationMetadata.ValidateObject(entity);

            //Check exist Particular Pupil
            var particularPupil = this.ParticularPupilBusiness.Find(entity.ParticularPupilID);
            //Utils.ValidateNull(particularPupil, "ParticularPupilTreatment_Label_ParticularPupilID");

            //Check Exist Pupil
            //this.PupilProfileBusiness.CheckAvailable(entity.PupilID, "ParticularPupilTreatment_Label_PupilID");

            //Check tuong thich Particular Pupil
            bool ParticularPupilCompatible = new ParticularPupilRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ParticularPupil"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ParticularPupilID" , entity.ParticularPupilID},
                                                {"PupilID", entity.PupilID}
                                            }, null);
            if (!ParticularPupilCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Check UpdateDate < DateTime.Now
            //Check trong Metadata
            if (entity.UpdatedDate > DateTime.Now)
                throw new BusinessException("ParticularPupilTreatment_UpdateDateLessThanCurrentDate");

            if (entity.AppliedDate > DateTime.Now)
                throw new BusinessException("ParticularPupilTreatment_AppliedDateLessThanCurrentDate");

            //Kiem tra quyen cua nguoi dung
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, particularPupil.ClassID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            base.Update(entity);
        }

        public void DeleteParticularPupilTreatment(int UserID, int ParticularPupilTreatmentID, int SchoolID)
        {
            var particularPupilTreatment = this.Find(ParticularPupilTreatmentID);
            Utils.ValidateNull(particularPupilTreatment, "ParticularPupilTreatment_Label_ParticularPupilTreatmentID");

            //Check tuong thich PupilProfile
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile"
                                            , new Dictionary<string, object>()
                                            {
                                                {"PupilProfileID" , particularPupilTreatment.PupilID},
                                                {"CurrentSchoolID", SchoolID}
                                            }, null);
            if (!PupilProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra co dung nam hoc hien tai khong
            if (!this.AcademicYearBusiness.IsCurrentYear(particularPupilTreatment.ParticularPupil.AcademicYearID))
                throw new BusinessException("ParticularPupilTreatment_Validate_NotCurrentYear");

            //Kiem tra trang thai cua hoc sinh
            if (this.PupilOfClassBusiness.All.FirstOrDefault(u => u.PupilID == particularPupilTreatment.PupilID && u.ClassID == particularPupilTreatment.PupilProfile.CurrentClassID && u.Status == GlobalConstants.PUPIL_STATUS_STUDYING) == null)
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");

            //Kiem tra quyen han cua nguoi dung
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, particularPupilTreatment.ParticularPupil.ClassID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            base.Delete(ParticularPupilTreatmentID);
        }

        public IQueryable<ParticularPupilTreatment> SearchParticularPupilTreatment(IDictionary<string, object> dic)
        {
            int? ParticularPupilTreatmentID = Utils.GetNullableInt(dic, "ParticularPupilTreatmentID");
            int? ParticularPupilID = Utils.GetNullableInt(dic, "ParticularPupilID");
            int? PupilID = Utils.GetNullableInt(dic, "PupilID");
            string TreatmentResolution = Utils.GetString(dic, "TreatmentResolution");
            string Result = Utils.GetString(dic, "Result");
            DateTime? AppliedDate = Utils.GetDateTime(dic, "AppliedDate");
            DateTime? UpdatedDate = Utils.GetDateTime(dic, "UpdatedDate");

            var lstParticularPupilTreatment = this.ParticularPupilTreatmentRepository.All;

            if (ParticularPupilTreatmentID.HasValue && ParticularPupilTreatmentID.Value > 0)
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.ParticularPupilTreatmentID == ParticularPupilTreatmentID.Value);

            if (ParticularPupilID.HasValue && ParticularPupilID.Value > 0)
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.ParticularPupilID == ParticularPupilID.Value);

            if (PupilID.HasValue && PupilID.Value > 0)
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.PupilID == PupilID.Value);

            if (!string.IsNullOrEmpty(TreatmentResolution))
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.TreatmentResolution.Contains(TreatmentResolution.Trim()));

            if (!string.IsNullOrEmpty(Result))
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.Result.Contains(Result.Trim()));

            if (AppliedDate.HasValue)
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.AppliedDate.Value.Date == AppliedDate.Value.Date);

            if (UpdatedDate.HasValue)
                lstParticularPupilTreatment = lstParticularPupilTreatment.Where(u => u.UpdatedDate.Value.Date == UpdatedDate.Value.Date);

            return lstParticularPupilTreatment;
        }
    }
}