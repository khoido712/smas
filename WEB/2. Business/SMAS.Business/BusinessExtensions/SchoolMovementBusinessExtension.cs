/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class SchoolMovementBusiness
    {
        #region validate
        void Validate(SchoolMovement Entity)
        {
            ValidationMetadata.ValidateObject(Entity);
            //Utils.ValidateMaxLength(Entity.Description, 256, "SchoolMovement_Label_Description");
            //MovedDate <= DateTime.Now, SemesterStartDate < MoveDate < SemesterEndDate (Dựa vào AcademicYearID, Semester để xác định ngày bắt đầu và ngày kết thúc của học kỳ năm học)
            AcademicYear academicYear = AcademicYearRepository.Find(Entity.AcademicYearID);
            Utils.ValidatePastDate(Entity.MovedDate, "SchoolMovement_Label_MovedDate");
            if (Entity.Semester == 1)
            {
                if (academicYear.FirstSemesterStartDate.Value > Entity.MovedDate || academicYear.FirstSemesterEndDate.Value < Entity.MovedDate)
                {
                    throw new BusinessException("SchoolMovement_Validate_NotInFirstSemester");
                }
            }
            else
            {
                if (academicYear.SecondSemesterStartDate.Value > Entity.MovedDate || academicYear.SecondSemesterEndDate.Value < Entity.MovedDate)
                {
                    throw new BusinessException("SchoolMovement_Validate_NotInSecondSemester");
                }
            }
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"ClassProfileID", Entity.ClassID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            ////PupilID, ClassID(CurrentClassID): not compatiblel(PupilProfile)
            //bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
            //    new Dictionary<string, object>()
            //    {
            //        {"PupilProfileID", Entity.PupilID},
            //        {"CurrentClassID", Entity.ClassID}
            //    }, null);
            //if (!PupilProfileCompatible)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}
            //ClassID, EducationLevelID: not compatible(ClassProfile)
            bool EducationLevelCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"EducationLevelID", Entity.EducationLevelID},
                    {"ClassProfileID",Entity.ClassID}
                }, null);
            if (!EducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
               new Dictionary<string, object>()
                {
                    {"SchoolID", Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //If(MoveToSchoolID != null): MoveToSchoolID: FK(SchoolProfile)
            if (Entity.MovedToSchoolID.HasValue)
            {
                bool MovedFromClassCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                new Dictionary<string, object>()
                {
                    {"SchoolProfileID", Entity.MovedToSchoolID.Value}
                });
                if (!MovedFromClassCompatible)
                {
                    List<object> Params = new List<object>();
                    Params.Add("MovementAcceptance_Label_MovedFromSchoolID");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);
                }                
            }

        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới thông tin học sinh chuyển trường
        /// <author>minhh</author>
        /// <date>16/10/2012</date>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="entity"></param>
        public void InsertSchoolMovement(int UserID, SchoolMovement entity)
        {
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID))
            {
                Validate(entity);
                
                //If(PupilOfClass(PupilID).Status != 1): Không cho phép thêm mới học sinh chuyên trường
                Dictionary<string, object> dicPupil = new Dictionary<string, object>() { { "SchoolID", entity.SchoolID }, { "AcademicYearID",entity.AcademicYearID},{"ClassID",entity.ClassID }, { "PupilID", entity.PupilID }, { "Check", "true" } };
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil).OrderByDescending(u=> u.PupilOfClassID).FirstOrDefault();
                if (poc==null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("SchoolMovement_Validate_PupilNotWorking");
                }
                //Ngày chuyển trường phải lớn hơn hoặc bằng ngày được phân công vào lớp.
                if (poc.AssignedDate.Value.Date > entity.MovedDate.Date)
                {
                    throw new BusinessException("SchoolMovement_Validate_MovedDate_NotCom");
                }

                //Cập nhật vào bảng PupilOfClass với Status = PUPIL_STATUS_MOVED (3): Đã chuyển trường
                poc.Status = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL;
                poc.Year = AcademicYearBusiness.Find(entity.AcademicYearID).Year;
                poc.EndDate = entity.MovedDate;
                PupilOfClassBusiness.Update(poc);

                //Cập nhật trạng thái vào bảng PupilProfile với ProfileStatus = PUPIL_STATUS_MOVED (3): Đã chuyển trường
                PupilProfile pp = poc.PupilProfile;
                pp.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL;
                PupilProfileBusiness.Update(pp);

                List<PupilOfSchool> lstpos = PupilOfSchoolBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "PupilID", entity.PupilID } }).ToList();
                foreach (PupilOfSchool pos in lstpos)
                {
                    poc.EndDate = entity.MovedDate;
                    PupilOfSchoolBusiness.Update(pos);
                }

                //Thực hiện insert vào bảng SchoolMovement
                base.Insert(entity);
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa thông tin học sinh chuyển trường
        /// <author>minhh</author>
        /// <date>16/10/2012</date>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="entity"></param>
        public void UpdateSchoolMovement(int UserID, SchoolMovement entity)
        {
            Validate(entity);
            //SchoolMovementID: PK(SchoolMovement)
            this.CheckAvailable(entity.SchoolMovementID, "SchoolMovement_Label");

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID))
            {
                Dictionary<string, object> dicPupil = new Dictionary<string, object>() { { "SchoolID", entity.SchoolID }, { "PupilID", entity.PupilID }, { "Check", "true" } };
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil).OrderByDescending(u => u.PupilOfClassID).FirstOrDefault();
                //Ngày chuyển trường phải lớn hơn ngày được phân công vào lớp.
                if (poc.AssignedDate > entity.MovedDate)
                {
                    throw new BusinessException("SchoolMovement_Validate_MovedDate_NotCom");
                }
                base.Update(entity);
              
                if (poc != null)
                {
                    poc.EndDate = entity.MovedDate;
                    poc.Status = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL;
                    PupilOfClassBusiness.Update(poc);
                }
                List<PupilOfSchool> lstpos = PupilOfSchoolBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "PupilID", entity.PupilID } }).ToList();
                foreach (PupilOfSchool pos in lstpos)
                {
                    poc.EndDate = entity.MovedDate;
                    PupilOfSchoolBusiness.Update(pos);
                }
                PupilOfSchoolBusiness.Save();

            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa thông tin học sinh chuyển trường
        /// <author>minhh</author>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="SchoolMovementID"></param>
        /// <param name="SchoolID"></param>
        public void DeleteSchoolMovement(int UserID, int SchoolMovementID, int SchoolID)
        {
            SchoolMovement Entity = this.Find(SchoolMovementID);
            //PupilID, SchoolID (CurrentSchoolID): not compatible(PupilProfile)
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
               new Dictionary<string, object>()
                {
                    {"PupilProfileID", Entity.PupilID},
                    {"CurrentSchoolID", SchoolID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //SchoolMovementID: PK(SchoolMovement)
            this.CheckAvailable(Entity.SchoolMovementID, "SchoolMovement_Label");
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID))
            {
                throw new BusinessException("SchoolMovement_Validate_IsNotCurrentYear");
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, Entity.ClassID))
            {
                //Thực hiện xoá trong bảng SchoolMovement
                base.Delete(Entity.SchoolMovementID);

                //Cập nhật vào bảng PupilOfClass với Status = 1 (Đang học)
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["PupilID"] = Entity.PupilID;
                dic["SchoolID"] = SchoolID;
                dic["ClassID"] = Entity.ClassID;
                dic["AcademicYearID"] = Entity.AcademicYearID;
                dic["Check"] = "true";

                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).FirstOrDefault();
                if (poc != null)
                {
                    poc.EndDate = null;
                    poc.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                    PupilOfClassBusiness.UpdatePupilOfClass(UserID, poc);
                }

                //Thực hiện cập nhật lại trạng thái PupilProfile với Status = PUPIL_STATUS_STUDYING (Đang học)
                PupilProfile pp = PupilProfileBusiness.Find(Entity.PupilID);
                pp.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                PupilProfileBusiness.Update(pp);
            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm học sinh chuyển trường
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<SchoolMovement> Search(IDictionary<string, object> dic = null)
        {
            int SchoolMovementID = Utils.GetInt(dic, "SchoolMovementID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int Semester = (int)Utils.GetByte(dic, "Semester");
            int? MovedToClassID = Utils.GetNullableInt(dic, "MovedToClassID");
            int? MovedToSchoolID = Utils.GetNullableInt(dic, "MovedToSchoolID");
            string MovedToClassName = Utils.GetString(dic, "MovedToClassName");
            string MovedToSchoolName = Utils.GetString(dic, "MovedToSchoolName");
            string FullName = Utils.GetString(dic, "FullName");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            int EducationLevelID = Utils.GetShort(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            DateTime? MovedDate = Utils.GetDateTime(dic, "MovedDate");
            List<int> lstClassID = Utils.GetIntList(dic, "ListClassID");
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            IQueryable<SchoolMovement> lsSchoolMovement = SchoolMovementRepository.All.Where(o => o.PupilProfile.IsActive == true);
            int AppliedLevel = Utils.GetByte(dic, "AppliedLevel");
            int? Genre = Utils.GetNullableInt(dic, "Genre");

            if (ClassID != 0)
            {
                lsSchoolMovement = from sm in lsSchoolMovement
                                   join cp in ClassProfileBusiness.All on sm.ClassID equals cp.ClassProfileID
                                   where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                   && sm.ClassID == ClassID
                                   select sm;
            }
            if (AppliedLevel != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.EducationLevel.Grade == AppliedLevel);
            }
            if (SchoolMovementID != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.SchoolMovementID == SchoolMovementID);
            }
            if (PupilID != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.PupilID == PupilID);
            }
            if (Semester != 0)
            {
                if (Semester == 1)
                {
                    lsSchoolMovement = lsSchoolMovement.Where(o => (o.MovedDate <= academicYear.FirstSemesterEndDate) && (o.MovedDate >= academicYear.FirstSemesterStartDate));
                }
                if (Semester == 2)
                {
                    lsSchoolMovement = lsSchoolMovement.Where(o => (o.MovedDate <= academicYear.SecondSemesterEndDate) && (o.MovedDate >= academicYear.SecondSemesterStartDate));
                }
            }
            if (MovedToClassID.HasValue)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.MovedToClassID == MovedToClassID);
            }
            if (MovedToSchoolID.HasValue)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.MovedToSchoolID == MovedToSchoolID);
            }
            if (EducationLevelID != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (MovedDate != null)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.MovedDate == MovedDate);
            }
            if (MovedToClassName.Trim().Length != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.MovedToClassName.ToLower().Contains(MovedToClassName.ToLower()));
            }
            if (MovedToSchoolName.Trim().Length != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.MovedToSchoolName.ToLower().Contains(MovedToSchoolName.ToLower()));
            }
            if (FullName.Trim().Length != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
            if (PupilCode.Trim().Length != 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.PupilProfile.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            }
            if (lstClassID.Count > 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => lstClassID.Contains(o.ClassID));
            }
            if (Genre.HasValue && Genre.Value >= 0)
            {
                lsSchoolMovement = lsSchoolMovement.Where(o => o.PupilProfile.Genre == Genre);
            }

            return lsSchoolMovement;
        }
        #endregion

        #region SearchBySchool
        /// <summary>
        /// Tìm thông tin học sinh chuyển trường theo trường
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<SchoolMovement> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0) dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
        #endregion

        //Tamhm1
        public List<SchoolMovement> GetListSchoolMovement(int AcademicYearID, int SchoolID, int Semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["Semester"] = Semester;
            if (AcademicYearID == 0) return null;
            if (Semester == 0) return null;
            IQueryable<SchoolMovement> query = SearchBySchool(SchoolID, dic);
            return query.ToList();
        }

        public IQueryable<SchoolMovementBO> GetListSchoolMovementByParameter(
            List<int> lstSchoolID, List<int> lstAcademicYearID, List<int> lstPupilID)
        {
            IQueryable<SchoolMovementBO> lstSchoolMovement = (from sm in SchoolMovementBusiness.All
                                                        join pp in PupilProfileBusiness.All
                                                        on sm.PupilID equals pp.PupilProfileID
                                                        where lstPupilID.Contains(sm.PupilID)
                                                        && (lstSchoolID.Contains(sm.SchoolID) 
                                                            || lstSchoolID.Contains(sm.MovedToSchoolID.HasValue ? sm.MovedToSchoolID.Value : 0))
                                                       // && lstAcademicYearID.Contains(sm.AcademicYearID)
                                                        && lstPupilID.Contains(pp.PupilProfileID)
                                                        && pp.IsActive == true
                                                        select new SchoolMovementBO
                                                        {
                                                            AcademicYearID = sm.AcademicYearID,
                                                            SchoolID = sm.SchoolID,
                                                            PupilID = sm.PupilID,
                                                            MovedDate = sm.MovedDate,
                                                            MovedToSchoolID = sm.MovedToSchoolID,
                                                            Genre = pp.Genre,
                                                            EthnicID = pp.EthnicID,
                                                            ClassID = sm.ClassID
                                                        });
            return lstSchoolMovement;
        }
    }
}
