﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.DAL.IRepository;
using log4net;
using SMAS.DAL.Repository;
using SMAS.Business.IBusiness;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>11/29/2012</date>
    public partial class AverageMarkByPeriodBusiness : GenericBussiness<ProcessedReport>, IAverageMarkByPeriodBusiness
    {
        public const string REPORT_CODE = "ThongKeDiemTBMTheoDot";
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public AverageMarkByPeriodBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); }
            this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        private WorkBookData BuildReportData(TranscriptOfClass Entity, List<StatisticsConfig> ListConfig,
            List<EducationLevel> ListEducationLevel, List<ClassInfo> ListClass, List<ReportData> ListMark)
        {
            WorkBookData WorkBookData = new WorkBookData();

            SheetData SheetData = new SheetData("Report");

            // Bo sung du lieu hoc luc cho ClassInfo
            foreach (ClassInfo Class in ListClass)
            {
                foreach (StatisticsConfig config in ListConfig)
                {
                    Class.CapacityInfo[config.StatisticsConfigID.ToString()] = ListMark.Where(o => o.ClassID == Class.ClassID && config.MinValue <= o.Mark && o.Mark <= config.MaxValue).Count();
                }
            }

            // Nhom du lieu theo cap hoc
            List<EducationLevelInfo> ListEduInfo = new List<EducationLevelInfo>();
            int index = 1;
            foreach (EducationLevel edu in ListEducationLevel)
            {
                EducationLevelInfo EduInfo = new EducationLevelInfo();
                EduInfo.Index = index++;
                EduInfo.EducationLevelID = edu.EducationLevelID;
                EduInfo.EducationLevelName = edu.Resolution;

                List<ClassInfo> SubListClass = ListClass.Where(o => o.EducationLevelID == edu.EducationLevelID).ToList();
                EduInfo.ListClassInfo = SubListClass;

                ListEduInfo.Add(EduInfo);
            }

            SheetData.Data["ListEduInfo"] = ListEduInfo;
            SheetData.Data["ListConfig"] = ListConfig;


            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = School.Province.ProvinceName;
            string DeptName = School.SupervisingDept.SupervisingDeptName;
            string SubjectName = SubjectCatBusiness.Find(Entity.SubjectID).DisplayName;
            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            SheetData.Data["AcademicYear"] = AcademicYear;
            SheetData.Data["SchoolName"] = SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = ProvinceName;
            SheetData.Data["DeptName"] = DeptName.ToUpper();
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["PeriodName"] = PeriodName;
            SheetData.Data["Semester"] = Semester;
            SheetData.Data["SubjectName"] = SubjectName.ToUpper();


            WorkBookData.ListSheet.Add(SheetData);
            return WorkBookData;
        }

        private IVTWorkbook WriteToExcel(WorkBookData Data, string ReportCode)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet Template = oBook.GetSheet(1);

            for (int iSheet = 0; iSheet < Data.ListSheet.Count; iSheet++)
            {
                IVTWorksheet Sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                SheetData SheetData = Data.ListSheet[iSheet];
                List<StatisticLevelConfig> ListConfig = (List<StatisticLevelConfig>)SheetData.Data["ListConfig"];
                List<EducationLevelInfo> ListEduInfo = (List<EducationLevelInfo>)SheetData.Data["ListEduInfo"];

                int i = 0;
                int StartRow = 10;
                int StartDataRow = StartRow + 3;
                int NormalRowID = StartDataRow;
                int EduRow = StartDataRow + 2;
                int ThickRowID = StartDataRow + 1;

                // Tao day du cac tieu de cot
                foreach (StatisticLevelConfig config in ListConfig)
                {
                    Sheet.CopyPaste(Template.GetRange("F10", "G15"), StartRow, i * 2 + 6);
                    //Sheet.GetRange(StartRow, i * 2 + 6, StartRow, i * 2 + 6).Value = string.Format("{0} ({1}-{2})", config.DisplayName, config.MinValue, config.MaxValue);
                    Sheet.SetCellValue(StartRow, i * 2 + 6, config.Title);
                    i++;
                }

                // Fill du lieu
                int CurrentRow = StartDataRow;
                int EduIndex = 1;
                int ClassIndex = 1;
                int NumOfClass = 0;
                string StrAllSchool = "";
                int j = 0;
                int countClass = 0;
                foreach (EducationLevelInfo edu in ListEduInfo)
                {
                    // Fill du lieu cho Row Khoi hoc
                    NumOfClass = edu.ListClassInfo.Count;
                    if (NumOfClass == 0)
                    {
                        continue;
                    }

                    Sheet.CopyAndInsertARow(EduRow, CurrentRow, true);
                    NormalRowID++;
                    ThickRowID++;
                    EduRow++;


                    Sheet.SetCellValue(CurrentRow, 1, EduIndex++);
                    Sheet.SetCellValue(CurrentRow, 2, edu.EducationLevelName);
                    Sheet.SetFormulaValue(CurrentRow, 4, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 4));
                    Sheet.SetFormulaValue(CurrentRow, 5, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 5));

                    j = 1;

                    foreach (StatisticLevelConfig config in ListConfig)
                    {
                        Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSum(CurrentRow + 1, CurrentRow + NumOfClass, 5 + j));
                        j++;
                        Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                        j++;
                    }


                    // Lay cac dong la EducationLevel de lap cong thuc tinh tong cho toan truong
                    if (StrAllSchool == "")
                    {
                        StrAllSchool = "{0}" + CurrentRow;
                    }
                    else
                    {
                        StrAllSchool += "; {0}" + CurrentRow;
                    }


                    CurrentRow++;
                    countClass += edu.ListClassInfo.Count;
                    // Fill du lieu cho Row Lop hoc
                    int k = 0;
                    foreach (ClassInfo Class in edu.ListClassInfo)
                    {

                        if (k++ < NumOfClass - 1)
                        {
                            Sheet.CopyAndInsertARow(NormalRowID, CurrentRow, true);
                        }
                        else
                        {
                            Sheet.CopyAndInsertARow(ThickRowID, CurrentRow, true);
                        }
                        NormalRowID++;
                        ThickRowID++;
                        EduRow++;

                        Sheet.SetCellValue(CurrentRow, 1, ClassIndex++);
                        Sheet.SetCellValue(CurrentRow, 2, Class.ClassName);
                        Sheet.SetCellValue(CurrentRow, 3, Class.TeacherName);
                        Sheet.SetCellValue(CurrentRow, 4, Class.NumOfPupil);
                        Sheet.SetCellValue(CurrentRow, 5, Class.ActualNumOfPupil);

                        j = 1;

                        foreach (StatisticLevelConfig config in ListConfig)
                        {

                            Sheet.SetCellValue(CurrentRow, 5 + j, Class.CapacityInfo[config.StatisticLevelConfigID.ToString()]);
                            j++;
                            Sheet.SetCellValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                            j++;
                        }


                        CurrentRow++;
                    }

                }

                // Fill du lieu cho row toan truong
                CurrentRow = StartRow + 2;
                Sheet.SetFormulaValue(CurrentRow, 4, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(4))));
                Sheet.SetFormulaValue(CurrentRow, 5, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(5))));

                j = 1;

                foreach (StatisticLevelConfig config in ListConfig)
                {
                    Sheet.SetFormulaValue(CurrentRow, 5 + j, string.Format("=SUM({0})", string.Format(StrAllSchool, VTVector.ColumnIntToString(5 + j))));
                    j++;
                    Sheet.SetFormulaValue(CurrentRow, 5 + j, GetStrSumPercent(5 + j, CurrentRow));
                    j++;
                }

                Sheet.Name = SheetData.Data["SheetName"].ToString();
                Sheet.FillVariableValue(SheetData.Data);
                Sheet.PageMaginTop = 0.5;
                Sheet.PageMaginBottom = 0.5;
                Sheet.FitSheetOnOnePage = true;
                Sheet.DeleteRow(EduRow);
                Sheet.DeleteRow(ThickRowID);
                Sheet.DeleteRow(NormalRowID);

                //fill Nguoi lao bao cao 
                int EndCol = 7 + ListConfig.Count * 2 - 5;
                int EndRow = 12 + ListEduInfo.Count + countClass + 2;
                string title = "Người lập báo cáo";
                Sheet.SetCellValue(EndRow, EndCol, title);
                Sheet.GetRange(EndRow, EndCol, EndRow, EndCol + 3).SetFontStyle(true, null, false, null, false, false);
                IVTRange titleMerge = Sheet.GetRange(EndRow, EndCol, EndRow, EndCol + 3);
                titleMerge.Merge();
            }
            oBook.GetSheet(1).Delete();
            return oBook;
        }

        private string GetStrSum(int FromRow, int ToRow, int Col)
        {
            return string.Format("=SUM({0}:{1})", VTVector.ColumnIntToString(Col) + FromRow, VTVector.ColumnIntToString(Col) + ToRow);
        }

        private string GetStrSumPercent(int Col, int Row)
        {
            return string.Format("=ROUND({0}/IF({1}<=0;1;{1});4)*100", VTVector.ColumnIntToString(Col - 1) + Row, VTVector.ColumnIntToString(5) + Row);
        }

        /// <summary>
        /// Lấy mảng băm
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>AuNH</author>
        /// <date>11/21/2012</date>
        public string GetHashKey(TranscriptOfClass Entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodID"] = Entity.PeriodDeclarationID;
            dic["IsCommenting"] = Entity.IsCommenting;
            dic["SubjectID"] = Entity.SubjectID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            dic["SchoolID"] = Entity.SchoolID;
            dic["TeacherID"] = Entity.TeacherID;
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertAverageMarkByPeriod(TranscriptOfClass Entity, Stream Data)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = REPORT_CODE;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = this.GetHashKey(Entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            string subjectName = this.SubjectCatBusiness.Find(Entity.SubjectID).DisplayName;
            PeriodDeclaration period = this.PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            string periodName = period.Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(period.Semester.HasValue ? period.Semester.Value : 0);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(Entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));
            outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(periodName));
            if (Entity.TeacherID > 0)
            {
                string teacherName = EmployeeBusiness.Find(Entity.TeacherID).FullName;
                outputNamePattern = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(teacherName));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("_[Teacher]", string.Empty);
            }
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodID"] = Entity.PeriodDeclarationID;
            dic["IsCommenting"] = Entity.IsCommenting;
            dic["SubjectID"] = Entity.SubjectID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            dic["SchoolID"] = Entity.SchoolID;
            dic["TeacherID"] = Entity.TeacherID;
            ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            ProcessedReportRepository.Insert(ProcessedReport);

            ProcessedReportParameterRepository.Save();
            ProcessedReportRepository.Save();

            return ProcessedReport;
        }

        public ProcessedReport GetAverageMarkByPeriod(TranscriptOfClass Entity)
        {
            string ReportCode = REPORT_CODE;

            string HashKey = this.GetHashKey(Entity);
            IQueryable<ProcessedReport> query = this.ProcessedReportRepository.All.Where(o => o.ReportCode == ReportCode);
            query = query.Where(o => o.InputParameterHashKey == HashKey);
            query = query.OrderByDescending(o => o.ProcessedDate);

            return query.FirstOrDefault();
        }

        public Stream CreateAverageMarkByPeriod(TranscriptOfClass Entity)
        {

            int partitionId = UtilsBusiness.GetPartionId(Entity.SchoolID);
            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.All.Where(s => s.Last2digitNumberSchool == partitionId && (s.IsSubjectVNEN == null || s.IsSubjectVNEN == false));
            if (Entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekFirstSemester > 0);
            }
            else if (Entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekSecondSemester > 0);
            }
            else
            {
                iqClassSubject = iqClassSubject.Where(s => (s.SectionPerWeekSecondSemester > 0 || s.SectionPerWeekFirstSemester > 0));
            }
            if (Entity.SubjectID > 0)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SubjectID == Entity.SubjectID);
            }


            // Danh sach khoi
            List<EducationLevel> ListEducationLevel = EducationLevelBusiness.GetByGrade(Entity.AppliedLevel).ToList();

            IDictionary<string, object> dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", Entity.AcademicYearID},
                                                                    {"SchoolID", Entity.SchoolID},
                                                                    {"SubjectID", Entity.SubjectID},
                                                                    {"IsVNEN",true}
                                                                };
            // List Class Profile
            IQueryable<ClassSubject> iqClassProfile_Temp = this.ClassSubjectBusiness.SearchBySchool(Entity.SchoolID, dic_ClassProfile);


            //Danh sach lop hoc
            IQueryable<ClassProfile> lsClass = from cs in iqClassProfile_Temp
                                               join c in ClassProfileBusiness.All.Where(o => o.AcademicYearID == Entity.AcademicYearID
                                               && o.EducationLevel.Grade == Entity.AppliedLevel && o.IsActive == true) on cs.ClassID equals c.ClassProfileID
                                               orderby c.OrderNumber.HasValue ? c.OrderNumber : 0, c.DisplayName
                                               select c;
            
            //Neu nhap giao vien, loc danh sach cac lop ma giao vien do day
            //Danh sach phan cong giang day
            if (Entity.TeacherID > 0)
            {
                Dictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
                dicToGetAssignment["TeacherID"] = Entity.TeacherID;
                dicToGetAssignment["AcademicYearID"] = Entity.AcademicYearID;
                dicToGetAssignment["SubjectID"] = Entity.SubjectID;
                IQueryable<TeachingAssignment> lstTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(Entity.SchoolID, dicToGetAssignment);

               



                //Chiendd: loai cac ban ghi trung nhau
                lsClass = (from c in lsClass
                           join cs in iqClassSubject on c.ClassProfileID equals cs.ClassID
                           select c).Distinct();
            }

            IQueryable<PupilOfClass> lsPupilOfClassSemester = PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"CheckWithClass", "CheckWithClass"},
                                                {"AcademicYearID", Entity.AcademicYearID}
                                    }).AddCriteriaSemester(AcademicYearBusiness.Find(Entity.AcademicYearID), Entity.Semester);

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(Entity.AcademicYearID, Entity.Semester, Entity.SubjectID, 0);

            var tmpPoc = (from poc in lsPupilOfClassSemester
                          join cs in iqClassSubject on poc.ClassID equals cs.ClassID
                          where poc.AcademicYearID == Entity.AcademicYearID
                          && poc.SchoolID == Entity.SchoolID

                          select new PupilOfClassBO
                          {
                              PupilID = poc.PupilID,
                              ClassID = poc.ClassID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject,
                              Genre = poc.PupilProfile.Genre,
                              EthnicID = poc.PupilProfile.EthnicID
                          });

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                {"SubjectID",Entity.SubjectID},
                {"SemesterID",Entity.Semester},
                {"YearID",AcademicYearBusiness.Find(Entity.AcademicYearID).Year}
            };

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(tmpPoc, dic);

            if(lstExempSubject!=null && lstExempSubject.Count > 0)
            {
                //Khong lay cac hoc sinh mien giam
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }


            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();

            List<PupilOfClassBO> lstPOCFemale = lstPOC.Where(pp => pp.Genre == GlobalConstants.GENRE_FEMALE).ToList();
            List<PupilOfClassBO> lstPOCEthnic = lstPOC.Where(pp => pp.EthnicID.HasValue && pp.EthnicID != Ethnic_Kinh.EthnicID && pp.EthnicID != Ethnic_ForeignPeople.EthnicID).ToList();
            List<PupilOfClassBO> lstPOCFemaleEthnic = lstPOC.Where(pp => pp.Genre == GlobalConstants.GENRE_FEMALE
                && pp.EthnicID.HasValue && pp.EthnicID != Ethnic_Kinh.EthnicID && pp.EthnicID != Ethnic_ForeignPeople.EthnicID).ToList();


            dic.Add("Type", SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0);
            IQueryable<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic);

            var iqClassInfo = from g in iqClassInfoBO
                              select new ClassInfo
                              {
                                  ClassID = g.ClassID,
                                  ClassName = g.ClassName,
                                  ClassOrderNumber = g.ClassOrderNumber,
                                  EducationLevelID = g.EducationLevelID,
                                  TeacherName = g.TeacherName,
                                  NumOfPupil = g.NumOfPupil,
                                  ActualNumOfPupil = g.ActualNumOfPupil
                              };
            List<ClassInfo> ListClass = iqClassInfo.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();


            dic["FemaleID"] = 1;
            dic["EthnicID"] = 0;
            iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic);

            iqClassInfo = from g in iqClassInfoBO
                          select new ClassInfo
                          {
                              ClassID = g.ClassID,
                              ClassName = g.ClassName,
                              ClassOrderNumber = g.ClassOrderNumber,
                              EducationLevelID = g.EducationLevelID,
                              TeacherName = g.TeacherName,
                              NumOfPupil = g.NumOfPupil,
                              ActualNumOfPupil = g.ActualNumOfPupil
                          };
            List<ClassInfo> ListClassFemale = iqClassInfo.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

            dic["FemaleID"] = 0;
            dic["EthnicID"] = 1;
            iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic);

            iqClassInfo = from g in iqClassInfoBO
                          select new ClassInfo
                          {
                              ClassID = g.ClassID,
                              ClassName = g.ClassName,
                              ClassOrderNumber = g.ClassOrderNumber,
                              EducationLevelID = g.EducationLevelID,
                              TeacherName = g.TeacherName,
                              NumOfPupil = g.NumOfPupil,
                              ActualNumOfPupil = g.ActualNumOfPupil
                          };
            List<ClassInfo> ListClassEthnic = iqClassInfo.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

            dic["FemaleID"] = 1;
            dic["EthnicID"] = 1;
            iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic);

            iqClassInfo = from g in iqClassInfoBO
                          select new ClassInfo
                          {
                              ClassID = g.ClassID,
                              ClassName = g.ClassName,
                              ClassOrderNumber = g.ClassOrderNumber,
                              EducationLevelID = g.EducationLevelID,
                              TeacherName = g.TeacherName,
                              NumOfPupil = g.NumOfPupil,
                              ActualNumOfPupil = g.ActualNumOfPupil
                          };


            List<ClassInfo> ListClassFemaleEthnic = iqClassInfo.OrderBy(o => o.EducationLevelID)
                .ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();


            //Danh sach diem
            List<VSummedUpRecord> tmpSummedUp = VSummedUpRecordBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"SubjectID", Entity.SubjectID},
                                                {"PeriodID", Entity.PeriodDeclarationID},
                                                }).ToList();

            //Loai bo cac hoc sinh mien giảm
            if(lstExempSubject!=null && lstExempSubject.Count > 0)
            {
                tmpSummedUp = tmpSummedUp.Where(s => s.Semester == Entity.Semester && !lstExempSubject.Exists(p => p.PupilID == s.PupilID && p.SubjectID == s.SubjectID && p.ClassID == s.ClassID)).ToList();
            }
            
            //VietHD4: Fix lấy mức thống kê theo bảng mới (STATISTIC_LEVEL_CONFIG)

            //IQueryable<StatisticsConfig> iqStatisticsConfig = StatisticsConfigBusiness.GetIQueryable(Entity.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_TRUNG_BINH_MON);

            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                    { "AcademicYearID", Entity.AcademicYearID } ,
                    { "SubjectID", Entity.SubjectID }
                }).FirstOrDefault();
            List<StatisticLevelConfig> lstStatisticConfig = new List<StatisticLevelConfig>();
            //Neu la mon tinh diem
            if (schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
            {
                int StatisticLevelReportID = Entity.StatisticLevelReportID;
                lstStatisticConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();

            }
            else
            {
                lstStatisticConfig = new List<StatisticLevelConfig>() {
                    new StatisticLevelConfig{
                        Title="Đ",
                        StatisticLevelConfigID=-1
                    },
                    new StatisticLevelConfig
                    {
                        Title="CĐ",
                        StatisticLevelConfigID=-2
                    }
                };
            }

            WorkBookData WorkBookData = new WorkBookData();

            SheetData SheetData = new SheetData("Report");
            FillData(SheetData, tmpSummedUp, lstPOC, lstStatisticConfig, ListClass, ListEducationLevel, Entity);
            SheetData.Data["SheetName"] = "Tat_ca";
            WorkBookData.ListSheet.Add(SheetData);

            if (Entity.FemaleID > 0)
            {
                SheetData = new SheetData("Report");
                FillData(SheetData, tmpSummedUp, lstPOCFemale, lstStatisticConfig, ListClassFemale, ListEducationLevel, Entity);
                SheetData.Data["SheetName"] = "HS_Nu";
                SheetData.Data["PupilName"] = "HỌC SINH NỮ";
                WorkBookData.ListSheet.Add(SheetData);
            }
            if (Entity.EthnicID > 0)
            {
                SheetData = new SheetData("Report");
                FillData(SheetData, tmpSummedUp, lstPOCEthnic, lstStatisticConfig, ListClassEthnic, ListEducationLevel, Entity);
                SheetData.Data["SheetName"] = "HS_DT";
                SheetData.Data["PupilName"] = "HỌC SINH DÂN TỘC";
                WorkBookData.ListSheet.Add(SheetData);
            }

            if (Entity.FemaleEthnicID > 0)
            {
                SheetData = new SheetData("Report");
                FillData(SheetData, tmpSummedUp, lstPOCFemaleEthnic, lstStatisticConfig, ListClassFemaleEthnic, ListEducationLevel, Entity);
                SheetData.Data["SheetName"] = "HS_Nu_DT";
                SheetData.Data["PupilName"] = "HỌC SINH NỮ DÂN TỘC";
                WorkBookData.ListSheet.Add(SheetData);
            }


            IVTWorkbook oBook = this.WriteToExcel(WorkBookData, REPORT_CODE);
            return oBook.ToStream();
        }

        private void FillData(SheetData SheetData, List<VSummedUpRecord> tmpSummedUp, List<PupilOfClassBO> lstPOC, List<StatisticLevelConfig> lstStatisticConfig
            , List<ClassInfo> ListClass, List<EducationLevel> ListEducationLevel, TranscriptOfClass Entity)
        {
            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = (School.District != null ? School.District.DistrictName : "");
            string DeptName = UtilsBusiness.GetSupervisingDeptName(School.SchoolProfileID, Entity.AppliedLevel);
            // Tim dot duoc chon
            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            if (Period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string PeriodName = Period.Resolution;
            // Tim mon duoc chon
            var selectedSubject = SubjectCatBusiness.Find(Entity.SubjectID);
            if (selectedSubject == null)
            {
                throw new BusinessException("Chưa chọn môn học");
            }
            string SubjectName = selectedSubject.DisplayName;

            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            SheetData.Data["AcademicYear"] = AcademicYear;
            SheetData.Data["SchoolName"] = SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = ProvinceName;
            SheetData.Data["DeptName"] = DeptName.ToUpper();
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["PeriodName"] = PeriodName;
            SheetData.Data["Semester"] = Semester;
            SheetData.Data["SubjectName"] = SubjectName.ToUpper();
            SheetData.Data["Teacher"] = Entity.TeacherID > 0 ? "Giáo viên: " + EmployeeBusiness.Find(Entity.TeacherID).FullName : string.Empty;

            var lsSummedUp = (from u in tmpSummedUp
                              join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                              select u);

            var listMarkGroup = from s in lstStatisticConfig
                                join m in lsSummedUp on 1 equals 1
                                where (s.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN
                                        && m.SummedUpMark >= s.MinValue && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                            || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                            || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)
                                                                            || (s.SignMax == null)))
                                    || (s.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN
                                        && m.SummedUpMark > s.MinValue && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                            || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                            || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)
                                                                            || (s.SignMax == null)))
                                    || (s.SignMin == null && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                            || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                            || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)))
                                    || (s.MinValue == null && s.MaxValue == null && s.Title == m.JudgementResult)
                                select new
                                {
                                    s.StatisticLevelConfigID,
                                    m.SchoolID,
                                    Entity.EducationLevelID,
                                    m.SubjectID,
                                    m.Semester,
                                    m.PeriodID,
                                    m.ClassID
                                };

            var listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          Entity.EducationLevelID,
                                          g.SubjectID,
                                          g.StatisticLevelConfigID,
                                          g.Semester,
                                          g.PeriodID,
                                          g.ClassID
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.EducationLevelID,
                                          c.Key.SubjectID,
                                          c.Key.StatisticLevelConfigID,
                                          c.Key.Semester,
                                          c.Key.PeriodID,
                                          c.Key.ClassID,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();

            List<StatisticLevelConfig> ListConfig = lstStatisticConfig;
            foreach (ClassInfo Class in ListClass)
            {
                foreach (StatisticLevelConfig config in ListConfig)
                {
                    var summedUpMark = listMarkGroupCount.Where(o => o.ClassID == Class.ClassID && o.StatisticLevelConfigID == config.StatisticLevelConfigID && o.PeriodID == Entity.PeriodDeclarationID).FirstOrDefault();
                    Class.CapacityInfo[config.StatisticLevelConfigID.ToString()] = summedUpMark != null ? summedUpMark.CountMark : 0;
                }
            }

            List<EducationLevelInfo> ListEduInfo = new List<EducationLevelInfo>();
            int index = 1;
            foreach (EducationLevel edu in ListEducationLevel)
            {
                EducationLevelInfo EduInfo = new EducationLevelInfo();
                EduInfo.Index = index++;
                EduInfo.EducationLevelID = edu.EducationLevelID;
                EduInfo.EducationLevelName = edu.Resolution;

                List<ClassInfo> SubListClass = ListClass.Where(o => o.EducationLevelID == edu.EducationLevelID).ToList();
                EduInfo.ListClassInfo = SubListClass;

                ListEduInfo.Add(EduInfo);
            }

            SheetData.Data["ListEduInfo"] = ListEduInfo;
            SheetData.Data["ListConfig"] = ListConfig;

        }

        // ============= Private classes for private businesses ====================
        private class ReportData
        {
            public int ClassID { get; set; }
            public int PupilID { get; set; }
            public decimal? Mark { get; set; }
        }

        private class EducationLevelInfo
        {
            public int Index { get; set; }
            public int EducationLevelID { get; set; }
            public string EducationLevelName { get; set; }
            public List<ClassInfo> ListClassInfo { get; set; }
            public EducationLevelInfo()
            {
                ListClassInfo = new List<ClassInfo>();
            }
        }

        private class ClassInfo
        {
            public int ClassID { get; set; }
            public string ClassName { get; set; }
            public int? ClassOrderNumber { get; set; }
            public string TeacherName { get; set; }
            public int EducationLevelID { get; set; }
            public int NumOfPupil { get; set; }
            public int ActualNumOfPupil { get; set; }

            public Dictionary<string, object> CapacityInfo { get; set; }

            public ClassInfo()
            {
                CapacityInfo = new Dictionary<string, object>();
            }
        }
    }
}
