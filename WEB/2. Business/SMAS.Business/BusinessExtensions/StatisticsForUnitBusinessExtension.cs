﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    /// <summary>
    /// author: hieund9 & namdv3
    /// </summary>
    public partial class StatisticsForUnitBusiness
    {
        public string GetHashKey(StatisticReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.SemesterID},
                {"SchoolID",entity.SchoolID},
                {"EducationLevelID",entity.EducationLevelID},
                {"FemaleType",entity.FemaleType},
                {"EthnicType",entity.EthnicTypeID},
                {"AppliedLevel", entity.AppliedLevelID},
                {"SubCommitteeID",entity.SubCommitteeID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        public string GetHashKeyPrimary(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }
        public ProcessedReport GetPeriodStatistic(StatisticReportBO entity, int reportTypeID)
        {
            string reportCode = string.Empty;
            switch (reportTypeID)
            {
                case 1:
                    {
                        reportCode = SystemParamsInFile.THPT_THONGKE_DIEMKT_DINHKY;
                        break;
                    }
                case 2:
                    {
                        reportCode = SystemParamsInFile.THPT_THONGKE_DIEMKIEMTRA_HOCKY;
                        break;
                    }
                case 3:
                    {
                        reportCode = SystemParamsInFile.THPT_THONGKE_DIEMTBM;
                        break;
                    }
                case 4:
                    {
                        reportCode = SystemParamsInFile.THPT_THONGKE_XLHOCLUC_THEOBAN;
                        break;
                    }
                case 5:
                    {
                        reportCode = SystemParamsInFile.THPT_THONGKE_XLHANHKIEM_THEOBAN;
                        break;
                    }
            }
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport GetSemesterMarkPrimary(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.TH_THONGKE_DIEMKIEMTRA_HOCKY;
            string inputParameterHashKey = GetHashKeyPrimary(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        #region PRIMARY
        public List<MarkStatisticBO> SearchPeriodicMarkForPrimary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            List<string> listMarkTitle = new List<string>();
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listMarkTitle.Add("GK1");
                listMarkTitle.Add("CK1");
            }
            else if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                listMarkTitle.Add("GK2");
                listMarkTitle.Add("CN");
            }

            var listMarkxAll = from mr in VMarkRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                                                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && listMarkTitle.Contains(u.Title))
                               join cp in ClassProfileBusiness.AllNoTracking
                               on mr.ClassID equals cp.ClassProfileID
                               where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel
                               && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                               && cp.IsActive==true
                               select new
                               {
                                   SchoolID = mr.SchoolID,
                                   AcademicYearID = mr.AcademicYearID,
                                   PupilID = mr.PupilID,
                                   ClassID = mr.ClassID,
                                   EducationLevelID = cp.EducationLevelID,
                                   SubjectID = mr.SubjectID,
                                   Mark = mr.Mark,
                                   Semester = mr.Semester
                               };

            AcademicYear aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });

            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID }                    
            }).AddCriteriaSemester(aca, statisticsForUnit.Semester);

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
            var listMarkx = (from u in listMarkxAll
                             where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                             && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);

            IQueryable<StatisticsConfig> iqStatisticsConfig = StatisticsConfigBusiness.GetIQueryable(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY);
            var listMarkGroupCount = (from s in iqStatisticsConfig
                                      join m in listMarkx on 1 equals 1 into g1
                                      from g in g1
                                      where s.MinValue <= g.Mark && s.MaxValue > g.Mark
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.SubjectID,
                                          s.StatisticsConfigID,
                                          s.MinValue,
                                          s.MaxValue,
                                          s.MarkType,
                                          s.MappedColumn,
                                          g.Semester
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.EducationLevelID,
                                          c.Key.SubjectID,
                                          c.Key.StatisticsConfigID,
                                          c.Key.MinValue,
                                          c.Key.MaxValue,
                                          c.Key.MarkType,
                                          c.Key.MappedColumn,
                                          c.Key.Semester,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();

            int? year = aca.Year;
            var listMarkGroupSubjectEducation = listMarkGroupCount.Select(o => new { o.EducationLevelID, o.SubjectID, o.Semester })
                .Distinct()
                .ToList();
            List<StatisticsConfig> listStatisticsConfig = iqStatisticsConfig.ToList();
            foreach (var mg in listMarkGroupSubjectEducation)
            {
                MarkStatisticBO ms = new MarkStatisticBO();
                ms.SubjectID = mg.SubjectID;
                ms.EducationLevelID = mg.EducationLevelID;
                ms.Semester = mg.Semester.Value;
                ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                ms.SchoolID = statisticsForUnit.SchoolID;
                ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                ms.Year = year.Value;

                var listMarkByEducationSubject = listMarkGroupCount.Where(o => o.EducationLevelID == mg.EducationLevelID
                        && o.SubjectID == mg.SubjectID && o.Semester == mg.Semester);
                foreach (var s in listStatisticsConfig)
                {
                    var item = listMarkByEducationSubject.Where(o => o.StatisticsConfigID == s.StatisticsConfigID).FirstOrDefault();
                    int countMark = item == null ? 0 : item.CountMark;
                    typeof(MarkStatistic).GetProperty(s.MappedColumn).SetValue(ms, countMark, null);
                }
                ms.BelowAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE).Sum(o => o.CountMark);
                ms.OnAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE).Sum(o => o.CountMark);

                ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP1;
                ms.ProcessedDate = DateTime.Now;

                listMarkStatistic.Add(ms);
            }

            listMarkStatistic = listMarkStatistic.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return listMarkStatistic;
        }

        public List<MarkStatisticBO> SearchSemesterMarkForPrimary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            List<string> listMarkTitle = new List<string>();
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listMarkTitle.Add("CK1");
            }
            else if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                listMarkTitle.Add("CN");
            }

            var listMarkxAll = from mr in VMarkRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && listMarkTitle.Contains(u.Title))
                               join cp in ClassProfileBusiness.AllNoTracking
                               on mr.ClassID equals cp.ClassProfileID
                               where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel
                               && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                               && cp.IsActive==true
                               select new
                               {
                                   PupilID = mr.PupilID,
                                   ClassID = mr.ClassID,
                                   EducationLevelID = cp.EducationLevelID,
                                   SubjectID = mr.SubjectID,
                                   Mark = mr.Mark,
                                   Semester = mr.Semester
                               };

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });

            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel},
                {"FemaleID",statisticsForUnit.FemaleType},
                {"EthnicID",statisticsForUnit.EthnicType}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, statisticsForUnit.Semester);

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMarkx = (from u in listMarkxAll
                             where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                             && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);

            var listMark = listMarkx.ToList();

            var listUA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            var listOA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            var listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
            foreach (var mg in listMarkGroup)
            {
                MarkStatisticBO ms = new MarkStatisticBO();
                ms.SubjectID = mg.SubjectID;
                ms.EducationLevelID = (int)mg.EducationLevelID;
                ms.Semester = mg.Semester.Value;
                ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                ms.SchoolID = statisticsForUnit.SchoolID;
                ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                ms.Year = Aca.Year;

                int belowAverage = 0;
                foreach (var ua in listUA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    belowAverage += count;
                    typeof(MarkStatistic).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.BelowAverage = (int)belowAverage;

                int onAverage = 0;
                foreach (var oa in listOA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && oa.MinValue <= u.Mark && u.Mark < oa.MaxValue);
                    onAverage += count;
                    typeof(MarkStatistic).GetProperty(oa.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.OnAverage = (int)onAverage;

                ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRAHOCKYCAP1;
                ms.ProcessedDate = DateTime.Now;

                listMarkStatistic.Add(ms);
            }

            listMarkStatistic = listMarkStatistic.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();

            return listMarkStatistic;
        }

        public List<CapacityStatistic> SearchMarkSubjectCapacityForPrimary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<CapacityStatistic> retVal = new List<CapacityStatistic>();
            var listMarkxAll = from mr in VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && (u.SchoolID == statisticsForUnit.SchoolID)
                                                    && (u.Semester == statisticsForUnit.Semester)
                                                    && !u.PeriodID.HasValue
                                                     && u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                               join pp in PupilProfileBusiness.AllNoTracking
                               on mr.PupilID equals pp.PupilProfileID
                               join cp in ClassProfileBusiness.AllNoTracking
                               on mr.ClassID equals cp.ClassProfileID
                               where pp.IsActive == true && cp.IsActive==true
                              && cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel
                              && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0)
                               select new { mr.PupilID, mr.ClassID, mr.SubjectID, cp.EducationLevelID, mr.Semester, mr.SummedUpMark };
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                // Neu la hoc ky 2 thi kiem tra xem co thi lai thi se lay du lieu thi lai
                IQueryable<VSummedUpRecord> listMarkxRetest = VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.AcademicYearID == statisticsForUnit.AcademicYearID
                                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                                    && !u.PeriodID.HasValue);
                listMarkxAll = from u in listMarkxAll
                               join r in listMarkxRetest on new { u.PupilID, u.SubjectID }
                               equals new { r.PupilID, r.SubjectID } into i
                               from g in i.DefaultIfEmpty()
                               select new
                               {
                                   u.PupilID,
                                   u.ClassID,
                                   u.SubjectID,
                                   u.EducationLevelID,
                                   u.Semester,
                                   SummedUpMark = (g.ReTestMark.HasValue ? g.ReTestMark : u.SummedUpMark)
                               };

            }

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });

            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMarkx = (from u in listMarkxAll
                             where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                             && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);

            var listMark = listMarkx.ToList();

            var listUA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            var listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
            foreach (var mg in listMarkGroup)
            {
                CapacityStatistic cs = new CapacityStatistic();
                cs.SubjectID = mg.SubjectID;
                cs.EducationLevelID = (int)mg.EducationLevelID;
                cs.Semester = mg.Semester;
                cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                cs.SchoolID = statisticsForUnit.SchoolID;
                cs.Year = Aca.Year;
                cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                int belowAverage = 0;
                int aboveAverage = 0;
                foreach (StatisticsConfig ua in listUA)
                {
                    int count = listMark.Count(u => u.SubjectID == cs.SubjectID && u.EducationLevelID == cs.EducationLevelID
                                                        && u.Semester == mg.Semester && ua.MinValue <= u.SummedUpMark.Value && u.SummedUpMark.Value < ua.MaxValue);
                    typeof(CapacityStatistic).GetProperty(ua.MappedColumn).SetValue(cs, (int?)count, null);
                    belowAverage += ua.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE ? count : 0;
                    aboveAverage += ua.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE ? count : 0;
                }
                cs.BelowAverage = belowAverage;
                cs.OnAverage = aboveAverage;
                int totalPupil = belowAverage + aboveAverage;
                cs.PupilTotal = totalPupil;
                cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCMONTINHDIEMCAP1;
                cs.ProcessedDate = DateTime.Now;
                cs.IsCommenting = SystemParamsInFile.ISCOMMENTING_TYPE_MARK;
                retVal.Add(cs);
            }

            retVal = retVal.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return retVal;
        }

        public List<CapacityStatistic> SearchJudgeSubjectCapacityForPrimary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<CapacityStatistic> retVal = new List<CapacityStatistic>();

            var listMarkxAll = from mr in VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                               join v in PupilProfileBusiness.AllNoTracking
                               on mr.PupilID equals v.PupilProfileID
                               where v.IsActive == true
                               join cp in ClassProfileBusiness.AllNoTracking
                               on mr.ClassID equals cp.ClassProfileID
                               where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel
                     && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                && cp.IsActive==true
                               select new
                               {
                                   PupilID = mr.PupilID,
                                   ClassID = mr.ClassID,
                                   EducationLevelID = cp.EducationLevelID,
                                   SubjectID = mr.SubjectID,
                                   JudgementResult = mr.JudgementResult,
                                   Semester = mr.Semester
                               };
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                // Neu la hoc ky 2 thi kiem tra xem co thi lai thi se lay du lieu thi lai
                IQueryable<VSummedUpRecord> listMarkxRetest = VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.AcademicYearID == statisticsForUnit.AcademicYearID
                                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                                    && !u.PeriodID.HasValue);
                listMarkxAll = from u in listMarkxAll
                               join r in listMarkxRetest on new { u.PupilID, u.SubjectID }
                               equals new { r.PupilID, r.SubjectID } into i
                               from g in i.DefaultIfEmpty()
                               select new
                               {
                                   u.PupilID,
                                   u.ClassID,
                                   u.EducationLevelID,
                                   u.SubjectID,
                                   JudgementResult = (g.ReTestJudgement != null ? g.ReTestJudgement : u.JudgementResult),
                                   u.Semester

                               };

            }
            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, statisticsForUnit.Semester);

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMarkx = (from u in listMarkxAll
                             where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                             && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);

            var listMark = listMarkx.ToList();

            var listUA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            var listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();

            foreach (var mg in listMarkGroup)
            {
                CapacityStatistic cs = new CapacityStatistic();
                cs.SubjectID = mg.SubjectID;
                cs.EducationLevelID = (int)mg.EducationLevelID;
                cs.Semester = mg.Semester;
                cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                cs.SchoolID = statisticsForUnit.SchoolID;
                cs.Year = Aca.Year;
                cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                int totalPupil = 0;
                foreach (StatisticsConfig ua in listUA)
                {
                    int count = listMark.Count(u => u.SubjectID == cs.SubjectID && u.EducationLevelID == cs.EducationLevelID
                                                        && u.Semester == mg.Semester && u.JudgementResult == ua.EqualValue);
                    typeof(CapacityStatistic).GetProperty(ua.MappedColumn).SetValue(cs, (int?)count, null);
                    totalPupil += count;
                }
                cs.PupilTotal = totalPupil;
                cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCMONNHANXETCAP1;
                cs.ProcessedDate = DateTime.Now;
                cs.IsCommenting = SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE;
                retVal.Add(cs);
            }

            retVal = retVal.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return retVal;
        }

        /// <summary>
        /// Điểm kiểm tra định kì
        /// </summary>
        /// <param name="statisticsForUnit"></param>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public Stream CreatePeriodicMarkForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TH_THONGKE_DIEMKT_DINHKY);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);


            List<StatisticsConfig> listBelow = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            List<StatisticsConfig> listAbove = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            int lastColIndex = listBelow.Count + listAbove.Count + 8;

            List<MarkStatisticBO> listMarkStatistic = SearchPeriodicMarkForPrimary(statisticsForUnit);
            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<EducationLevel> listEdu = listMarkStatistic.Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet dataSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange markCol = tempSheet.GetRange("A1", "A4");
            IVTRange tbCol = tempSheet.GetRange("B1", "B4");

            for (int i = 0; i < listBelow.Count; i++)
            {
                dataSheet.CopyPaste(markCol, 8, i + 5, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                dataSheet.SetCellValue(8, i + 5, string.Format(format, listBelow[i].MinValue));
            }

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + 5, true);
            dataSheet.SetCellValue(8, listBelow.Count + 5, "TS dưới TB");

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + 6, true);
            dataSheet.SetCellValue(8, listBelow.Count + 6, "TL dưới TB");

            for (int i = 0; i < listAbove.Count; i++)
            {
                dataSheet.CopyPaste(markCol, 8, i + listBelow.Count + 7, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                dataSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(format, listAbove[i].MinValue));
            }

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 7, true);
            dataSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 7, "TS trên TB");

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 8, true);
            dataSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 8, "TL trên TB");

            IVTRange dataRow = dataSheet.GetRange(10, 1, 10, lastColIndex);
            for (int i = 0; i < listMarkStatistic.Count; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 11, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                dataSheet.DeleteRow(9);
            }


            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listMarkStatistic.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                IVTRange eduRange = dataSheet.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 8, colIndex++, index);
                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;
                    dataSheet.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.GetRange(index + 8, colIndex - 1, index + 8, colIndex - 1).AutoFit();
                    dataSheet.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                    foreach (StatisticsConfig sc in listBelow)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    dataSheet.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                    double BelowAveragePer = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                    dataSheet.SetCellValue(index + 8, colIndex++, BelowAveragePer.ToString().Replace(",", ".") + "%");
                    foreach (StatisticsConfig sc in listAbove)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    dataSheet.SetCellValue(index + 8, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 8, colIndex++, (100 - BelowAveragePer).ToString().Replace(",", ".") + "%");

                    index++;
                }
            }

            tempSheet.Delete();
            #endregion

            #region reportName
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            dataSheet.FitAllColumnsOnOnePage = true;
            tempSheet.FitToPage = true;
            return oBook.ToStream();
        }
        //Điểm kiểm tra học kì
        public Stream CreateSemesterMarkForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TH_THONGKE_DIEMKIEMTRA_HOCKY);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);


            List<StatisticsConfig> listBelow = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            List<StatisticsConfig> listAbove = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            int lastColIndex = listBelow.Count + listAbove.Count + 8;

            List<MarkStatisticBO> listMarkStatistic = SearchSemesterMarkForPrimary(statisticsForUnit);
            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });
            // Sap xep cho dung thu tu theo mon hoc
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            List<EducationLevel> listEdu = listMarkStatistic.Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet dataSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange markCol = tempSheet.GetRange("A1", "A4");
            IVTRange tbCol = tempSheet.GetRange("B1", "B4");

            for (int i = 0; i < listBelow.Count; i++)
            {
                dataSheet.CopyPaste(markCol, 8, i + 5, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                dataSheet.SetCellValue(8, i + 5, string.Format(format, listBelow[i].MinValue));
            }

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + 5, true);
            dataSheet.SetCellValue(8, listBelow.Count + 5, "TS dưới TB");

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + 6, true);
            dataSheet.SetCellValue(8, listBelow.Count + 6, "TL dưới TB");

            for (int i = 0; i < listAbove.Count; i++)
            {
                dataSheet.CopyPaste(markCol, 8, i + listBelow.Count + 7, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                dataSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(format, listAbove[i].MinValue));
            }

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 7, true);
            dataSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 7, "TS trên TB");

            dataSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 8, true);
            dataSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 8, "TL trên TB");

            IVTRange dataRow = dataSheet.GetRange(10, 1, 10, lastColIndex);
            for (int i = 0; i < listMarkStatistic.Count; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 11, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                dataSheet.DeleteRow(9);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;
            string SheetName = string.Empty;
            if (statisticsForUnit.FemaleType > 0 && statisticsForUnit.EthnicType > 0)
            {
                PupilName = "HỌC SINH NỮ DÂN TỘC";
                SheetName = "HS_Nu_DT";
            }
            else if (statisticsForUnit.EthnicType > 0)
            {
                PupilName = "HỌC SINH DÂN TỘC";
                SheetName = "HS_DT";
            }
            else if (statisticsForUnit.FemaleType > 0)
            {
                PupilName = "HỌC SINH NỮ";
                SheetName = "HS_Nu";
            }
            else
            {
                SheetName = "DiemKTCuoiKy";
            }

            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;

            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listMarkStatistic.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                IVTRange eduRange = dataSheet.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 8, colIndex++, index);
                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;
                    dataSheet.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.GetRange(index + 8, colIndex - 1, index + 8, colIndex - 1).AutoFit();
                    dataSheet.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                    foreach (StatisticsConfig sc in listBelow)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    dataSheet.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                    double BelowAveragePer = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                    dataSheet.SetCellValue(index + 8, colIndex++, BelowAveragePer.ToString().Replace(",", ".") + "%");
                    foreach (StatisticsConfig sc in listAbove)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    dataSheet.SetCellValue(index + 8, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 8, colIndex++, (100 - BelowAveragePer).ToString().Replace(",", ".") + "%");

                    index++;
                }
            }

            tempSheet.Delete();
            #endregion

            #region reportName
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            dataSheet.FitSheetOnOnePage = true;
            tempSheet.FitToPage = true;
            dataSheet.Name = SheetName;
            return oBook.ToStream();
        }
        //Học lực môn tính điểm
        public Stream CreateMarkSubjectCapacityForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TH_THONGKEHOCLUCMONTINHDIEM);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listMark = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);

            int lastColIndex = listMark.Count * 2 + 6;

            List<CapacityStatistic> listCapacityStatistic = SearchMarkSubjectCapacityForPrimary(statisticsForUnit);
            listCapacityStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });
            // Sap xep cho dung thu tu theo mon hoc
            listCapacityStatistic = listCapacityStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            List<EducationLevel> listEdu = listCapacityStatistic.Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet dataSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange tempCol = tempSheet.GetRange("A1", "B5");

            for (int i = 0; i < listMark.Count; i++)
            {
                dataSheet.CopyPaste(tempCol, 8, i * 2 + 5);
                dataSheet.SetCellValue(8, i * 2 + 5, listMark[i].DisplayName);
            }
            dataSheet.CopyPaste(tempCol, 8, listMark.Count * 2 + 5);
            dataSheet.SetCellValue(8, listMark.Count * 2 + 5, "Trên TB");

            IVTRange dataRow = dataSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < listCapacityStatistic.Count; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 12, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                dataSheet.DeleteRow(10);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);

            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listCapacityStatistic.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;

                IVTRange eduRange = dataSheet.GetRange(index + 9, 2, index + 9 + listItem.Count() - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 9, colIndex++, index);

                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 9, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;

                    dataSheet.SetCellValue(index + 9, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountMark = new List<int?>();
                    foreach (StatisticsConfig sc in listMark)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountMark.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountMark, item.PupilTotal);
                    int pos = 0;
                    double totalAbovePer = 0;
                    foreach (StatisticsConfig sc in listMark)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                        if (percent.HasValue)
                        {
                            if (sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                totalAbovePer += percent.Value;
                            }
                        }
                    }

                    dataSheet.SetCellValue(index + 9, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 9, colIndex++, totalAbovePer.ToString().Replace(",", "."));
                    index++;
                }
            }

            tempSheet.Delete();
            #endregion

            #region reportName
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }
        //Học lực môn nhận xét
        public Stream CreateJudgeSubjectCapacityForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TH_THONGKEHOCLUCMONNX);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listJudge = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_PRIMARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);

            int lastColIndex = listJudge.Count * 2 + 4;

            List<CapacityStatistic> listCapacityStatistic = SearchJudgeSubjectCapacityForPrimary(statisticsForUnit);
            listCapacityStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });
            // Sap xep cho dung thu tu theo mon hoc
            listCapacityStatistic = listCapacityStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            List<EducationLevel> listEdu = listCapacityStatistic.Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet dataSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange tempCol = tempSheet.GetRange("A1", "B5");

            for (int i = 0; i < listJudge.Count; i++)
            {
                dataSheet.CopyPaste(tempCol, 8, i * 2 + 5);
                dataSheet.SetCellValue(8, i * 2 + 5, listJudge[i].DisplayName);
            }

            IVTRange dataRow = dataSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < listCapacityStatistic.Count; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 12, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                dataSheet.DeleteRow(10);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);

            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listCapacityStatistic.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;

                IVTRange eduRange = dataSheet.GetRange(index + 9, 2, index + 9 + listItem.Count() - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 9, colIndex++, index);

                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 9, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;

                    dataSheet.SetCellValue(index + 9, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountMark = new List<int?>();
                    foreach (StatisticsConfig sc in listJudge)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountMark.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountMark, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listJudge)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }
                    index++;
                }
            }

            tempSheet.Delete();
            #endregion

            #region reportName
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            // dataSheet.FitToPage = true;
            tempSheet.FitSheetOnOnePage = true;
            return oBook.ToStream();
        }
        #endregion

        #region SECONDARY
        public List<MarkStatisticBO> SearchPeriodicMarkForSecondary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            IQueryable<VMarkRecordBO> listMarkxAll = from mr in VMarkRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID)
                                                     join pp in PupilProfileBusiness.All on mr.PupilID equals pp.PupilProfileID
                                                     join cs in ClassSubjectBusiness.AllNoTracking on new { mr.ClassID, mr.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                     join cp in ClassProfileBusiness.AllNoTracking on mr.ClassID equals cp.ClassProfileID
                                                     join mt in MarkTypeBusiness.AllNoTracking on mr.MarkTypeID equals mt.MarkTypeID
                                                     where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel && (mt.Title == "V" || mt.Title == "HK")
                                                     && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                                     && cp.IsActive==true
                                                     //&& (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                                     select new VMarkRecordBO
                                                     {
                                                         SchoolID = mr.SchoolID,
                                                         PupilID = mr.PupilID,
                                                         ClassID = mr.ClassID,
                                                         EducationLevelID = cp.EducationLevelID,
                                                         SubjectID = mr.SubjectID,
                                                         Mark = mr.Mark,
                                                         Semester = mr.Semester,
                                                         AppliedType = cs.AppliedType,
                                                         Genre = pp.Genre,
                                                         EthnicID = pp.EthnicID
                                                     };

            AcademicYear aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester },
                    { "IsVNEN", true }
            });

            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID }
                    //{ "EthnicID",statisticsForUnit.EthnicType},
                    //{ "FemaleID",statisticsForUnit.FemaleType}
            }).AddCriteriaSemester(aca, statisticsForUnit.Semester);

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",statisticsForUnit.SchoolID},
                {"AcademicYearID",statisticsForUnit.AcademicYearID},
                {"SemesterID",statisticsForUnit.Semester},
                {"YearID",Aca.Year}
            };
            List<RegisterSubjectSpecialize> lstRegis = new List<RegisterSubjectSpecialize>();
            lstRegis = (from r in RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis)
                        select new RegisterSubjectSpecialize
                        {
                            PupilID = r.PupilID,
                            ClassID = r.ClassID,
                            SubjectID = r.SubjectID,
                            SemesterID = r.SemesterID
                        }).ToList();

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
            IQueryable<VMarkRecordBO> listMarkx = (from u in listMarkxAll
                                                   where listClassSubject.Any(s => s.ClassID == u.ClassID && s.SubjectID == u.SubjectID)
                                                       //&& ((u.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || u.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE || u.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                                                       //        && lstRegis.Where(p => p.ClassID == u.ClassID && p.SubjectID == u.SubjectID && p.PupilID == u.PupilID && p.SemesterID == statisticsForUnit.Semester).Count() > 0)
                             && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                   select new VMarkRecordBO
                                                    {
                                                        SchoolID = u.SchoolID,
                                                        PupilID = u.PupilID,
                                                        ClassID = u.ClassID,
                                                        EducationLevelID = u.EducationLevelID,
                                                        SubjectID = u.SubjectID,
                                                        Mark = u.Mark,
                                                        Semester = u.Semester,
                                                        AppliedType = u.AppliedType,
                                                        Genre = u.Genre,
                                                        EthnicID = u.EthnicID
                                                    });
            int countRegis = 0;
            List<VMarkRecordBO> lstVmark = new List<VMarkRecordBO>();
            foreach (VMarkRecordBO item in listMarkx)
            {
                if (item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                {
                    countRegis = lstRegis.Where(p => p.ClassID == item.ClassID && p.SubjectID == item.SubjectID && p.PupilID == item.PupilID && p.SemesterID == statisticsForUnit.Semester).Count();
                    if (countRegis > 0)
                    {
                        lstVmark.Add(item);
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
                lstVmark.Add(item);
            }

            List<StatisticsConfig> iqStatisticsConfig = StatisticsConfigBusiness.GetIQueryable(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY).ToList();
            int? year = aca.Year;

            #region Thong ke binh thuong
            List<ObjectMarkRecord> listMarkGroup = (from s in iqStatisticsConfig
                                                    join m in lstVmark on 1 equals 1
                                                    where s.MinValue <= m.Mark && s.MaxValue > m.Mark
                                                    select new ObjectMarkRecord
                                {
                                    StatisticsConfigID = s.StatisticsConfigID,
                                    MinValue = s.MinValue,
                                    MaxValue = s.MaxValue,
                                    MarkType = s.MarkType,
                                    MappedColumn = s.MappedColumn,
                                    SchoolID = m.SchoolID,
                                    EducationLevelID = m.EducationLevelID,
                                    SubjectID = m.SubjectID,
                                    Semester = m.Semester
                                }).ToList();

            List<ObjectMarkRecord> listMarkGroupCount = (from g in listMarkGroup
                                                         group g by new
                                                         {
                                                             g.SchoolID,
                                                             g.EducationLevelID,
                                                             g.SubjectID,
                                                             g.StatisticsConfigID,
                                                             g.MinValue,
                                                             g.MaxValue,
                                                             g.MarkType,
                                                             g.MappedColumn,
                                                             g.Semester
                                                         } into c
                                                         select new ObjectMarkRecord
                                      {
                                          SchoolID = c.Key.SchoolID,
                                          EducationLevelID = c.Key.EducationLevelID,
                                          SubjectID = c.Key.SubjectID,
                                          StatisticsConfigID = c.Key.StatisticsConfigID,
                                          MinValue = c.Key.MinValue,
                                          MaxValue = c.Key.MaxValue,
                                          MarkType = c.Key.MarkType,
                                          MappedColumn = c.Key.MappedColumn,
                                          Semester = c.Key.Semester,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();


            var listMarkGroupSubjectEducation = listMarkGroupCount.Select(o => new { o.EducationLevelID, o.SubjectID, o.Semester })
                .Distinct()
                .ToList();
            List<StatisticsConfig> listStatisticsConfig = iqStatisticsConfig.ToList();
            foreach (var mg in listMarkGroupSubjectEducation)
            {
                MarkStatisticBO ms = new MarkStatisticBO();
                ms.SubjectID = mg.SubjectID;
                ms.EducationLevelID = mg.EducationLevelID;
                ms.Semester = mg.Semester.Value;
                ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                ms.SchoolID = statisticsForUnit.SchoolID;
                ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                ms.Year = year.Value;

                var listMarkByEducationSubject = listMarkGroupCount.Where(o => o.EducationLevelID == mg.EducationLevelID
                        && o.SubjectID == mg.SubjectID && o.Semester == mg.Semester);
                foreach (var s in listStatisticsConfig)
                {
                    var item = listMarkByEducationSubject.Where(o => o.StatisticsConfigID == s.StatisticsConfigID).FirstOrDefault();
                    int countMark = item == null ? 0 : item.CountMark;
                    typeof(MarkStatisticBO).GetProperty(s.MappedColumn).SetValue(ms, countMark, null);
                }
                ms.BelowAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE).Sum(o => o.CountMark);
                ms.OnAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE).Sum(o => o.CountMark);

                ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);
                ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP2;
                ms.ProcessedDate = DateTime.Now;
                ms.Critea = "Tất cả";
                ms.OrderCritea = 1;
                listMarkStatistic.Add(ms);
            }

            listMarkStatistic = listMarkStatistic.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            #endregion

            #region Thong ke hoc sinh nu
            List<MarkStatisticBO> listMarkStatisticFemale = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleType > 0)
            {
                List<VMarkRecordBO> listVMarkFemale = lstVmark.Where(p => p.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                listMarkGroup = (from s in iqStatisticsConfig
                                 join m in listVMarkFemale on 1 equals 1
                                 where s.MinValue <= m.Mark && s.MaxValue > m.Mark
                                 select new ObjectMarkRecord
                                 {
                                     StatisticsConfigID = s.StatisticsConfigID,
                                     MinValue = s.MinValue,
                                     MaxValue = s.MaxValue,
                                     MarkType = s.MarkType,
                                     MappedColumn = s.MappedColumn,
                                     SchoolID = m.SchoolID,
                                     EducationLevelID = m.EducationLevelID,
                                     SubjectID = m.SubjectID,
                                     Semester = m.Semester
                                 }).ToList();

                listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.SubjectID,
                                          g.StatisticsConfigID,
                                          g.MinValue,
                                          g.MaxValue,
                                          g.MarkType,
                                          g.MappedColumn,
                                          g.Semester
                                      } into c
                                      select new ObjectMarkRecord
                                      {
                                          SchoolID = c.Key.SchoolID,
                                          EducationLevelID = c.Key.EducationLevelID,
                                          SubjectID = c.Key.SubjectID,
                                          StatisticsConfigID = c.Key.StatisticsConfigID,
                                          MinValue = c.Key.MinValue,
                                          MaxValue = c.Key.MaxValue,
                                          MarkType = c.Key.MarkType,
                                          MappedColumn = c.Key.MappedColumn,
                                          Semester = c.Key.Semester,
                                          CountMark = c.Count()
                                      }
                                         ).ToList();


                listMarkGroupSubjectEducation = listMarkGroupCount.Select(o => new { o.EducationLevelID, o.SubjectID, o.Semester })
                    .Distinct()
                    .ToList();
                listStatisticsConfig = iqStatisticsConfig.ToList();
                foreach (var mg in listMarkGroupSubjectEducation)
                {
                    MarkStatisticBO ms = new MarkStatisticBO();
                    ms.SubjectID = mg.SubjectID;
                    ms.EducationLevelID = mg.EducationLevelID;
                    ms.Semester = mg.Semester.Value;
                    ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                    ms.SchoolID = statisticsForUnit.SchoolID;
                    ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                    ms.Year = year.Value;

                    var listMarkByEducationSubject = listMarkGroupCount.Where(o => o.EducationLevelID == mg.EducationLevelID
                            && o.SubjectID == mg.SubjectID && o.Semester == mg.Semester);
                    foreach (var s in listStatisticsConfig)
                    {
                        var item = listMarkByEducationSubject.Where(o => o.StatisticsConfigID == s.StatisticsConfigID).FirstOrDefault();
                        int countMark = item == null ? 0 : item.CountMark;
                        typeof(MarkStatisticBO).GetProperty(s.MappedColumn).SetValue(ms, countMark, null);
                    }
                    ms.BelowAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE).Sum(o => o.CountMark);
                    ms.OnAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE).Sum(o => o.CountMark);

                    ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);
                    ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP2;
                    ms.ProcessedDate = DateTime.Now;
                    ms.Critea = "HS nữ";
                    ms.OrderCritea = 2;
                    listMarkStatisticFemale.Add(ms);
                }
                listMarkStatisticFemale = listMarkStatisticFemale.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            }
            #endregion

            #region Thong ke hoc sinh dan toc
            List<MarkStatisticBO> listMarkStatisticEthnic = new List<MarkStatisticBO>();
            if (statisticsForUnit.EthnicType > 0)
            {
                List<VMarkRecordBO> listVMarkEthnic = lstVmark.Where(p => p.EthnicID.HasValue && p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_NN).ToList();
                listMarkGroup = (from s in iqStatisticsConfig
                                 join m in listVMarkEthnic on 1 equals 1
                                 where s.MinValue <= m.Mark && s.MaxValue > m.Mark
                                 select new ObjectMarkRecord
                                 {
                                     StatisticsConfigID = s.StatisticsConfigID,
                                     MinValue = s.MinValue,
                                     MaxValue = s.MaxValue,
                                     MarkType = s.MarkType,
                                     MappedColumn = s.MappedColumn,
                                     SchoolID = m.SchoolID,
                                     EducationLevelID = m.EducationLevelID,
                                     SubjectID = m.SubjectID,
                                     Semester = m.Semester
                                 }).ToList();

                listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.SubjectID,
                                          g.StatisticsConfigID,
                                          g.MinValue,
                                          g.MaxValue,
                                          g.MarkType,
                                          g.MappedColumn,
                                          g.Semester
                                      } into c
                                      select new ObjectMarkRecord
                                      {
                                          SchoolID = c.Key.SchoolID,
                                          EducationLevelID = c.Key.EducationLevelID,
                                          SubjectID = c.Key.SubjectID,
                                          StatisticsConfigID = c.Key.StatisticsConfigID,
                                          MinValue = c.Key.MinValue,
                                          MaxValue = c.Key.MaxValue,
                                          MarkType = c.Key.MarkType,
                                          MappedColumn = c.Key.MappedColumn,
                                          Semester = c.Key.Semester,
                                          CountMark = c.Count()
                                      }
                                         ).ToList();


                listMarkGroupSubjectEducation = listMarkGroupCount.Select(o => new { o.EducationLevelID, o.SubjectID, o.Semester })
                    .Distinct()
                    .ToList();
                listStatisticsConfig = iqStatisticsConfig.ToList();
                foreach (var mg in listMarkGroupSubjectEducation)
                {
                    MarkStatisticBO ms = new MarkStatisticBO();
                    ms.SubjectID = mg.SubjectID;
                    ms.EducationLevelID = mg.EducationLevelID;
                    ms.Semester = mg.Semester.Value;
                    ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                    ms.SchoolID = statisticsForUnit.SchoolID;
                    ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                    ms.Year = year.Value;

                    var listMarkByEducationSubject = listMarkGroupCount.Where(o => o.EducationLevelID == mg.EducationLevelID
                            && o.SubjectID == mg.SubjectID && o.Semester == mg.Semester);
                    foreach (var s in listStatisticsConfig)
                    {
                        var item = listMarkByEducationSubject.Where(o => o.StatisticsConfigID == s.StatisticsConfigID).FirstOrDefault();
                        int countMark = item == null ? 0 : item.CountMark;
                        typeof(MarkStatisticBO).GetProperty(s.MappedColumn).SetValue(ms, countMark, null);
                    }
                    ms.BelowAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE).Sum(o => o.CountMark);
                    ms.OnAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE).Sum(o => o.CountMark);

                    ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);
                    ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP2;
                    ms.ProcessedDate = DateTime.Now;
                    ms.Critea = "HS DT";
                    ms.OrderCritea = 3;
                    listMarkStatisticEthnic.Add(ms);
                }
                listMarkStatisticFemale = listMarkStatisticFemale.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            }
            #endregion

            #region Thong ke hoc sinh nu dan toc
            List<MarkStatisticBO> listMarkStatisticFemaleEthnic = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                List<VMarkRecordBO> listVMarkFE = lstVmark.Where(p => p.Genre == SystemParamsInFile.GENRE_FEMALE && p.EthnicID.HasValue && p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_NN).ToList();
                listMarkGroup = (from s in iqStatisticsConfig
                                 join m in listVMarkFE on 1 equals 1
                                 where s.MinValue <= m.Mark && s.MaxValue > m.Mark
                                 select new ObjectMarkRecord
                                 {
                                     StatisticsConfigID = s.StatisticsConfigID,
                                     MinValue = s.MinValue,
                                     MaxValue = s.MaxValue,
                                     MarkType = s.MarkType,
                                     MappedColumn = s.MappedColumn,
                                     SchoolID = m.SchoolID,
                                     EducationLevelID = m.EducationLevelID,
                                     SubjectID = m.SubjectID,
                                     Semester = m.Semester
                                 }).ToList();

                listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.SubjectID,
                                          g.StatisticsConfigID,
                                          g.MinValue,
                                          g.MaxValue,
                                          g.MarkType,
                                          g.MappedColumn,
                                          g.Semester
                                      } into c
                                      select new ObjectMarkRecord
                                      {
                                          SchoolID = c.Key.SchoolID,
                                          EducationLevelID = c.Key.EducationLevelID,
                                          SubjectID = c.Key.SubjectID,
                                          StatisticsConfigID = c.Key.StatisticsConfigID,
                                          MinValue = c.Key.MinValue,
                                          MaxValue = c.Key.MaxValue,
                                          MarkType = c.Key.MarkType,
                                          MappedColumn = c.Key.MappedColumn,
                                          Semester = c.Key.Semester,
                                          CountMark = c.Count()
                                      }
                                         ).ToList();


                listMarkGroupSubjectEducation = listMarkGroupCount.Select(o => new { o.EducationLevelID, o.SubjectID, o.Semester })
                    .Distinct()
                    .ToList();
                listStatisticsConfig = iqStatisticsConfig.ToList();
                foreach (var mg in listMarkGroupSubjectEducation)
                {
                    MarkStatisticBO ms = new MarkStatisticBO();
                    ms.SubjectID = mg.SubjectID;
                    ms.EducationLevelID = mg.EducationLevelID;
                    ms.Semester = mg.Semester.Value;
                    ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                    ms.SchoolID = statisticsForUnit.SchoolID;
                    ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                    ms.Year = year.Value;

                    var listMarkByEducationSubject = listMarkGroupCount.Where(o => o.EducationLevelID == mg.EducationLevelID
                            && o.SubjectID == mg.SubjectID && o.Semester == mg.Semester);
                    foreach (var s in listStatisticsConfig)
                    {
                        var item = listMarkByEducationSubject.Where(o => o.StatisticsConfigID == s.StatisticsConfigID).FirstOrDefault();
                        int countMark = item == null ? 0 : item.CountMark;
                        typeof(MarkStatisticBO).GetProperty(s.MappedColumn).SetValue(ms, countMark, null);
                    }
                    ms.BelowAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE).Sum(o => o.CountMark);
                    ms.OnAverage = listMarkByEducationSubject.Where(o => o.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE).Sum(o => o.CountMark);

                    ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);
                    ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP2;
                    ms.ProcessedDate = DateTime.Now;
                    ms.Critea = "HS nữ DT";
                    ms.OrderCritea = 4;
                    listMarkStatisticFemaleEthnic.Add(ms);
                }
                listMarkStatisticFemaleEthnic = listMarkStatisticFemaleEthnic.OrderBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            }
            #endregion

            listMarkStatistic = listMarkStatistic.Union(listMarkStatisticFemale).Union(listMarkStatisticEthnic).Union(listMarkStatisticFemaleEthnic)
                .OrderBy(u => u.OrderCritea).ThenBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return listMarkStatistic.ToList();
        }

        public List<MarkStatisticBO> SearchSemesterMarkForSecondary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);

            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            IQueryable<VMarkRecordBO> listMarkxAll = from mr in VMarkRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && (u.Title == "HK"))
                                                     join cp in ClassProfileBusiness.AllNoTracking
                                                      on mr.ClassID equals cp.ClassProfileID
                                                     join cs in ClassSubjectBusiness.AllNoTracking on new { mr.ClassID, mr.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                     where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel
                                                     && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                                     && cp.IsActive==true
                                                     select new VMarkRecordBO
                                                     {
                                                         SchoolID = mr.SchoolID,
                                                         PupilID = mr.PupilID,
                                                         ClassID = mr.ClassID,
                                                         EducationLevelID = cp.EducationLevelID,
                                                         SubjectID = mr.SubjectID,
                                                         Mark = mr.Mark,
                                                         Semester = mr.Semester,
                                                         AppliedType = cs.AppliedType,
                                                         SubjectVNEN =cs.IsSubjectVNEN
                                                     };

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });

            //List danh sach mon hoc ENVN
            List<int> listSubjectIdENVN = listClassSubject.Where(p => p.IsSubjectVNEN == true).Select(p => p.SubjectID).Distinct().ToList();
            //Danh sach diem theo mon hoc, theo lop ENVN            
            IQueryable<VMarkRecordBO> iqueryTeacherNoteBookSemester = from a in TeacherNoteBookSemesterBusiness.All
                                                                      join b in ClassProfileBusiness.All on a.ClassID equals b.ClassProfileID
                                                                      join c in ClassSubjectBusiness.All on a.ClassID equals c.ClassID
                                                                      where a.SemesterID == statisticsForUnit.Semester
                                                                      && a.SubjectID == c.SubjectID
                                                                      && a.AcademicYearID == statisticsForUnit.AcademicYearID
                                                                      && listSubjectIdENVN.Contains(a.SubjectID)
                                                                      && a.PERIODIC_SCORE_END.HasValue
                                                                      && b.IsActive.Value
                                                                      select new VMarkRecordBO
                                                                      {
                                                                          SchoolID = a.SchoolID,
                                                                          PupilID = a.PupilID,
                                                                          ClassID = a.ClassID,
                                                                          EducationLevelID = b.EducationLevelID,
                                                                          SubjectID = a.SubjectID,
                                                                          Mark = a.PERIODIC_SCORE_END.HasValue ? a.PERIODIC_SCORE_END.Value : 0,
                                                                          Semester = a.SemesterID,
                                                                          AppliedType = c.AppliedType
                                                                      };

            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            #region Get du lieu sheet dau tien
            Dictionary<string, object> dic1 = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
                //{"EthnicID",statisticsForUnit.EthnicType},
                //{"FemaleID",statisticsForUnit.FemaleType}
            };
            listMarkStatistic = this.GetListDataSemesterMarkForSecondary(statisticsForUnit, dic1, Aca, listMarkxAll, listClassSubject, iqueryTeacherNoteBookSemester,1);
            #endregion

            #region Lay du lieu sheet nu
            List<MarkStatisticBO> listMarkStatisticFemale = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleType > 0)
            {
                Dictionary<string, object> dic2 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    //{"EthnicID",statisticsForUnit.EthnicType},
                    {"FemaleID",statisticsForUnit.FemaleType}
                };
                listMarkStatisticFemale = this.GetListDataSemesterMarkForSecondary(statisticsForUnit, dic2, Aca, listMarkxAll, listClassSubject, iqueryTeacherNoteBookSemester,2);
            }
            #endregion

            #region Lay du lieu sheet dan toc
            List<MarkStatisticBO> listMarkStatisticEthnic = new List<MarkStatisticBO>();
            if (statisticsForUnit.EthnicType > 0)
            {
                Dictionary<string, object> dic3 = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel},
                {"EthnicID",statisticsForUnit.EthnicType}
               // {"FemaleID",statisticsForUnit.FemaleType}
            };
                listMarkStatisticEthnic = this.GetListDataSemesterMarkForSecondary(statisticsForUnit, dic3, Aca, listMarkxAll, listClassSubject, iqueryTeacherNoteBookSemester,3);
            }
            #endregion

            #region Lay du lieu sheet nu dan toc
            List<MarkStatisticBO> listMarkStatisticFE = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                Dictionary<string, object> dic4 = new Dictionary<string, object> 
                 {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"EthnicID",statisticsForUnit.EthnicType},
                    {"FemaleID",statisticsForUnit.FemaleType}
                };
                listMarkStatisticFE = this.GetListDataSemesterMarkForSecondary(statisticsForUnit, dic4, Aca, listMarkxAll, listClassSubject, iqueryTeacherNoteBookSemester,4);
            }
            #endregion

            listMarkStatistic = listMarkStatistic.Union(listMarkStatisticFemale).Union(listMarkStatisticEthnic).Union(listMarkStatisticFE)
            .OrderBy(p => p.OrderCritea).ThenBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();

            return listMarkStatistic;
        }

        public List<MarkStatisticBO> GetListDataSemesterMarkForSecondary(StatisticsForUnitBO statisticsForUnit, Dictionary<string, object> dic,
            AcademicYear Aca, IQueryable<VMarkRecordBO> listMarkxAll, IQueryable<ClassSubject> listClassSubject, IQueryable<VMarkRecordBO> iqueryTeacherNoteBookSemester, 
            int orderCritea )
        {
            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dic).AddCriteriaSemester(Aca, statisticsForUnit.Semester);

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",statisticsForUnit.SchoolID},
                {"AcademicYearID",statisticsForUnit.AcademicYearID},
                {"SemesterID",statisticsForUnit.Semester},
                {"YearID",Aca.Year}
            };
            List<RegisterSubjectSpecializeBO> lstRegis = new List<RegisterSubjectSpecializeBO>();
            lstRegis = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            //Bo ra nhung hoc sinh khong dang ky hoc mon tu chon
            IQueryable<VMarkRecordBO> listMarkx = (from u in listMarkxAll
                                                   where listClassSubject.Any(p => p.SubjectID == u.SubjectID && p.ClassID == u.ClassID)
                                                   && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                   select new VMarkRecordBO
                                                   {
                                                       SchoolID = u.SchoolID,
                                                       PupilID = u.PupilID,
                                                       ClassID = u.ClassID,
                                                       EducationLevelID = u.EducationLevelID,
                                                       SubjectID = u.SubjectID,
                                                       Mark = u.Mark,
                                                       Semester = u.Semester,
                                                       AppliedType = u.AppliedType,
                                                       SubjectVNEN = u.SubjectVNEN
                                                   });
            List<VMarkRecordBO> listMark = new List<VMarkRecordBO>();
            int countRegis = 0;
            foreach (var item in listMarkx)
            {
                if (item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE
                    || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                    || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                {
                    countRegis = lstRegis.Where(p => p.ClassID == item.ClassID && p.SubjectID == item.SubjectID && p.PupilID == item.PupilID && p.SemesterID == statisticsForUnit.Semester).Count();
                    if (countRegis > 0)
                    {
                        listMark.Add(item);
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
                listMark.Add(item);
            }

            var listmarkxENVN = (from u in iqueryTeacherNoteBookSemester
                                 where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID && o.IsSubjectVNEN == true)
                                 && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                 select u);
            var listMarkENVN = listmarkxENVN.ToList();

            //lay danh sach lop hoc mon vnen
            List<int> listSubjectIdRemove = listClassSubject.Where(p=>p.IsSubjectVNEN == true).Select(p => p.SubjectID).Distinct().ToList();
            List<int> listClassIdRemove = listClassSubject.Where(p => listSubjectIdRemove.Contains(p.SubjectID)).Select(p => p.ClassID).Distinct().ToList();
            //Loai bo mon hoc trong listMark
            //listMark = listMark.Where(p => (!listClassIdRemove.Contains(p.ClassID) && !listSubjectIdRemove.Contains(p.SubjectID))).ToList();
            List<VMarkRecordBO> listMarkTmp = new List<VMarkRecordBO>();
            for (int i = 0; i < listClassIdRemove.Count; i++)
            {
                for (int j = 0; j < listSubjectIdRemove.Count; j++)
                {
                    listMarkTmp = listMark.Where(p => p.ClassID == listClassIdRemove[i] && p.SubjectID == listSubjectIdRemove[j] && p.SubjectVNEN == true).ToList();
                    foreach (VMarkRecordBO item in listMarkTmp)
                    {
                        listMark.Remove(item);
                    }
                }
            }
            //Them mon hoc VNEN cho cac lop hoc theo chuong trinh VNEN            
            listMark.AddRange(listMarkENVN);

            var listUA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            var listOA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            var listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
            foreach (var mg in listMarkGroup)
            {
                MarkStatisticBO ms = new MarkStatisticBO();
                ms.SubjectID = mg.SubjectID;
                ms.EducationLevelID = (int)mg.EducationLevelID;
                ms.Semester = mg.Semester.Value;
                ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                ms.SchoolID = statisticsForUnit.SchoolID;
                ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                ms.Year = Aca.Year;

                int belowAverage = 0;
                foreach (var ua in listUA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    belowAverage += count;
                    typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.BelowAverage = (int)belowAverage;

                int onAverage = 0;
                foreach (var ua in listOA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    onAverage += count;
                    typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.OnAverage = (int)onAverage;

                ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRAHOCKYCAP2;
                ms.ProcessedDate = DateTime.Now;
                if (orderCritea == 2)
                {
                    ms.Critea = "HS nữ";
                    ms.OrderCritea = orderCritea;
                }
                else if (orderCritea == 3)
                {
                    ms.Critea = "HS DT";
                    ms.OrderCritea = orderCritea;
                }
                else if (orderCritea == 4)
                {
                    ms.Critea = "HS nữ DT";
                    ms.OrderCritea = orderCritea;
                }
                else if (orderCritea == 1)
                {
                    ms.Critea = "Tất cả";
                    ms.OrderCritea = orderCritea;
                }
                listMarkStatistic.Add(ms);
            }
            return listMarkStatistic;
        }


        public List<CapacityStatisticsBO> SearchSubjectCapacityForSecondary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<CapacityStatisticsBO> retVal = new List<CapacityStatisticsBO>();
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });
            List<int> listSubjectId = listClassSubject.Select(p => p.SubjectID).Distinct().ToList();
            List<int> listSubjectIdVNEN = listClassSubject.Where(p=>p.IsSubjectVNEN == true).Select(p => p.SubjectID).Distinct().ToList();
            List<VSummedUpRecordBO> listMarkxAll = (from mr in VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && !u.PeriodID.HasValue)
                                                          join pp in PupilProfileBusiness.AllNoTracking
                                                          on mr.PupilID equals pp.PupilProfileID
                                                          join cs in ClassSubjectBusiness.AllNoTracking on new { mr.ClassID, mr.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                          join cp in ClassProfileBusiness.AllNoTracking
                                                          on mr.ClassID equals cp.ClassProfileID
                                                          where
                                                          cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY
                                                          && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0)
                                                          && (pp.IsActive)
                                                          && listSubjectId.Contains(mr.SubjectID)
                                                          && cp.IsActive==true
                                                          select new VSummedUpRecordBO
                                                           {
                                                               PupilID = mr.PupilID,
                                                               ClassID = mr.ClassID,
                                                               SubjectID = mr.SubjectID,
                                                               EducationLevelID = cp.EducationLevelID,
                                                               Semester = mr.Semester,
                                                               IsCommenting = cs.IsCommenting,
                                                               SummedUpMark = mr.SummedUpMark,
                                                               JudgementResult = mr.JudgementResult,
                                                               AppliedType = cs.AppliedType,
                                                               subjectVNEN = cs.IsSubjectVNEN
                                                           }).ToList();
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                // Ca nam
                // Lay them ca du lieu thi lai
                List<VSummedUpRecord> listMarkxRetest = (VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.AcademicYearID == statisticsForUnit.AcademicYearID
                                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                                    && !u.PeriodID.HasValue
                                                    && listSubjectId.Contains(u.SubjectID))).ToList();
                listMarkxAll = (from u in listMarkxAll
                               join r in listMarkxRetest on new { u.PupilID, u.SubjectID }
                               equals new { r.PupilID, r.SubjectID } into i
                               from g in i.DefaultIfEmpty()
                               select new VSummedUpRecordBO
                               {
                                   PupilID = u.PupilID,
                                   ClassID = u.ClassID,
                                   SubjectID = u.SubjectID,
                                   EducationLevelID = u.EducationLevelID,
                                   Semester = u.Semester,
                                   IsCommenting = u.IsCommenting,
                                   SummedUpMark = (g != null && g.ReTestMark.HasValue ? g.ReTestMark : u.SummedUpMark),
                                   JudgementResult = ( g!= null && g.ReTestJudgement != null ? g.ReTestJudgement : u.JudgementResult),
                                   AppliedType = u.AppliedType,
                                   subjectVNEN = u.subjectVNEN
                               }).ToList();
            }

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",statisticsForUnit.SchoolID},
                {"AcademicYearID",statisticsForUnit.AcademicYearID},
                {"SemesterID",statisticsForUnit.Semester},
                {"YearID",Aca.Year}
            };
            List<RegisterSubjectSpecializeBO> lstRegis = new List<RegisterSubjectSpecializeBO>();
            lstRegis = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);

            List<StatisticsConfig> listM = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            List<StatisticsConfig> listJ = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);

            //lay danh sach lop hoc mon vnen
            List<int> listSubjectIdRemove = listMarkxAll.Where(p=>listSubjectIdVNEN.Contains(p.SubjectID) && p.subjectVNEN == true).Select(p => p.SubjectID).Distinct().ToList();
            List<int> listClassIdRemove = listMarkxAll.Where(p => listSubjectIdRemove.Contains(p.SubjectID)).Select(p => p.ClassID).Distinct().ToList();
            //Loai bo mon hoc trong listMark
            List<VSummedUpRecordBO> listMarkTmp = new List<VSummedUpRecordBO>();
            for (int i = 0; i < listClassIdRemove.Count; i++)
            {
                for (int j = 0; j < listSubjectIdRemove.Count; j++)
                {
                    listMarkTmp = listMarkxAll.Where(p => p.ClassID == listClassIdRemove[i] && p.SubjectID == listSubjectIdRemove[j] && p.subjectVNEN == true).ToList();
                    foreach (VSummedUpRecordBO item in listMarkTmp)
                    {
                        listMarkxAll.Remove(item);
                    }
                }
            }


            #region Danh sach du lieu dau tien
            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel},
                //{"EthnicID",statisticsForUnit.EthnicType},
                //{"FemaleID",statisticsForUnit.FemaleType},
                {"EducationLevelID",statisticsForUnit.EducationLevelID}
            };
            retVal = this.GetListSubjectCapacityForSecondary(statisticsForUnit, dic, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 1, listM, listJ);
            #endregion

            #region Danh sach hoc sinh nu
            List<CapacityStatisticsBO> listFemale = new List<CapacityStatisticsBO>();
            if (statisticsForUnit.FemaleType > 0)
            {
                Dictionary<string, object> dic1 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    //{"EthnicID",statisticsForUnit.EthnicType},
                    {"FemaleID",statisticsForUnit.FemaleType},
                    {"EducationLevelID",statisticsForUnit.EducationLevelID}
                };
                listFemale = this.GetListSubjectCapacityForSecondary(statisticsForUnit, dic1, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 2, listM, listJ);
            }
            #endregion
            

            #region Danh sach hoc sinh dan toc
            List<CapacityStatisticsBO> listEthnic = new List<CapacityStatisticsBO>();
            if (statisticsForUnit.EthnicType > 0)
            {
                Dictionary<string, object> dic2 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"EthnicID",statisticsForUnit.EthnicType},
                    //{"FemaleID",statisticsForUnit.FemaleType},
                    {"EducationLevelID",statisticsForUnit.EducationLevelID}
                };
                listEthnic = this.GetListSubjectCapacityForSecondary(statisticsForUnit, dic2, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 3, listM, listJ);
            }
            #endregion


            #region Danh sach hoc sinh nu dan toc
            List<CapacityStatisticsBO> listFE = new List<CapacityStatisticsBO>();
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                Dictionary<string, object> dic3 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"EthnicID",statisticsForUnit.EthnicType},
                    {"FemaleID",statisticsForUnit.FemaleType},
                    {"EducationLevelID",statisticsForUnit.EducationLevelID}
                };
                listFE = this.GetListSubjectCapacityForSecondary(statisticsForUnit, dic3, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 4, listM, listJ);
            }
            #endregion

            retVal = retVal.Union(listFemale).Union(listEthnic).Union(listFE).OrderBy(p => p.OrderCritea).ThenBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return retVal;
        }

        private List<CapacityStatisticsBO> GetListSubjectCapacityForSecondary(StatisticsForUnitBO statisticsForUnit, Dictionary<string, object> dic, AcademicYear Aca,
            IDictionary<string, object> dicRegis, List<VSummedUpRecordBO> listMarkxAll, List<RegisterSubjectSpecializeBO> lstRegis, IQueryable<ClassSubject> listClassSubject,
            int orderCritea, List<StatisticsConfig> listM, List<StatisticsConfig> listJ)
        {
            List<CapacityStatisticsBO> retVal = new List<CapacityStatisticsBO>();
            IQueryable<PupilOfClassBO> lstQPoc = (from p in PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dic).AddCriteriaSemester(Aca, statisticsForUnit.Semester)
                                                  join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      SubjectID = cs.SubjectID,
                                                      AppliedType = cs.AppliedType,
                                                      IsVNEN = cs.IsSubjectVNEN,
                                                      EducationLevelID = p.ClassProfile.EducationLevelID
                                                  });

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMarkx = (from u in listMarkxAll.ToList()
                             join poc in lstPOC on new { u.PupilID, u.ClassID, u.SubjectID } equals new { poc.PupilID, poc.ClassID, poc.SubjectID }
                             //where listClassSubject.Any(p=>p.ClassID == u.ClassID && p.SubjectID == u.SubjectID)
                             //&& lstPOC.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);

            var listMarkGroup = listMarkx.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester, u.IsCommenting }).Distinct();
            var listMarkGroupNotSemester = listMarkx.Select(u => new { u.SubjectID, u.EducationLevelID, u.IsCommenting }).Distinct();
            List<PupilSubjectEducationLevel> listPupilSubject = this.CountPupilBySubject(dic, listClassSubject, lstRegis);

            foreach (var mg in listMarkGroupNotSemester)
            {
                CapacityStatisticsBO cs = new CapacityStatisticsBO();
                cs.SubjectID = mg.SubjectID;
                cs.EducationLevelID = (int)mg.EducationLevelID;
                // cs.Semester = mg.Semester;
                cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                cs.SchoolID = statisticsForUnit.SchoolID;
                cs.Year = Aca.Year;
                cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                cs.IsCommenting = mg.IsCommenting;
                cs.Semester = statisticsForUnit.Semester;
                if (mg.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || mg.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                {
                    int belowAverage = 0;
                    int aboveAverage = 0;
                    foreach (StatisticsConfig sc in listM)
                    {
                        int count = listMarkx.Count(u => u.SummedUpMark != null && u.SubjectID == cs.SubjectID && u.EducationLevelID == cs.EducationLevelID
                                                            && ((statisticsForUnit.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && (u.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || u.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) || u.Semester == statisticsForUnit.Semester)
                                                            && (u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                                            && sc.MinValue <= u.SummedUpMark.Value && u.SummedUpMark.Value < sc.MaxValue);
                        typeof(CapacityStatisticsBO).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);
                        belowAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE ? count : 0;
                        aboveAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE ? count : 0;
                    }
                    cs.BelowAverage = (int)belowAverage;
                    cs.OnAverage = (int)aboveAverage;
                }

                if (mg.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    foreach (StatisticsConfig sc in listJ)
                    {
                        List<int> listint = listMarkx.Where(u => u.SubjectID == cs.SubjectID && u.EducationLevelID == cs.EducationLevelID
                                                            && (u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                                            && ((statisticsForUnit.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && (u.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || u.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) || u.Semester == statisticsForUnit.Semester)
                                                            && u.JudgementResult == sc.EqualValue).Select(pp => pp.PupilID).Distinct().ToList();
                        int count = listint.Count;
                        typeof(CapacityStatisticsBO).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);
                    }
                }
                // Tong so hoc sinh
                int totalPupil = 0;
                PupilSubjectEducationLevel pupilSubjectEducationLevel = listPupilSubject.Where(o => o.EducationLevelID == mg.EducationLevelID
                    && o.SubjectID == mg.SubjectID).FirstOrDefault();
                if (pupilSubjectEducationLevel != null)
                {
                    totalPupil = pupilSubjectEducationLevel.TotalPupil;
                }
                cs.PupilTotal = totalPupil;
                cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCMONCAP2;
                cs.ProcessedDate = DateTime.Now;
                cs.OrderCritea = orderCritea;

                if (orderCritea == 1)
                    cs.Critea = "Tất cả";
                else if (orderCritea == 2)
                    cs.Critea = "HS nữ";
                else if (orderCritea == 3)
                    cs.Critea = "HS DT";
                else if (orderCritea == 4)
                    cs.Critea = "HS nữ DT";
                retVal.Add(cs);
            }
            return retVal;
        }


        public List<ConductStatisticsBO> SearchConductForSecondary(StatisticsForUnitBO statisticsForUnit)
        {
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<ConductStatisticsBO> retVal = new List<ConductStatisticsBO>();
            var listMarkxFirst = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester == statisticsForUnit.Semester)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                 join pp in PupilProfileBusiness.AllNoTracking
                                 on pr.PupilID equals pp.PupilProfileID
                                 join cp in ClassProfileBusiness.AllNoTracking
                                 on pr.ClassID equals cp.ClassProfileID
                                 where
                                cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY
                                 && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                 && (pp.IsActive)
                                 && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                 && cp.IsActive==true
                                 select new
                                 {
                                     pr.PupilID,
                                     pr.ClassID,
                                     cp.SubCommitteeID,
                                     pr.EducationLevelID,
                                     pr.ConductLevelID,
                                     pr.Semester,
                                     pp.EthnicID,
                                     pp.Genre
                                 };
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                // Neu la ca nam thi lay them thong tin thi lai
                var listMarkRetest = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                    && !u.PeriodID.HasValue)
                                     join pp in PupilProfileBusiness.AllNoTracking
                                     on pr.PupilID equals pp.PupilProfileID
                                     join cp in ClassProfileBusiness.AllNoTracking
                                     on pr.ClassID equals cp.ClassProfileID
                                     where
                                     cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY
                                     && (pr.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0)
                                     && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                     && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                     && cp.IsActive==true
                                     select new
                                     {
                                         pr.PupilID,
                                         pr.ClassID,
                                         cp.SubCommitteeID,
                                         pr.EducationLevelID,
                                         pr.ConductLevelID,
                                         pr.Semester,
                                         pp.EthnicID,
                                         pp.Genre
                                     };


                // join voi thong tin chua thi lai
                listMarkxFirst = from u in listMarkxFirst
                                 join r in listMarkRetest on new { u.PupilID, u.ClassID }
                                 equals new { r.PupilID, r.ClassID } into i
                                 from g in i.DefaultIfEmpty()
                                 select new
                                 {
                                     u.PupilID,
                                     u.ClassID,
                                     u.SubCommitteeID,
                                     u.EducationLevelID,
                                     ConductLevelID = g.ConductLevelID.HasValue ? g.ConductLevelID : u.ConductLevelID,
                                     u.Semester,
                                     u.EthnicID,
                                     u.Genre
                                 };
            }
            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel},
                {"EducationLevelID", statisticsForUnit.EducationLevelID}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).Where(p=>(!p.ClassProfile.IsVnenClass.HasValue || (p.ClassProfile.IsVnenClass.HasValue && p.ClassProfile.IsVnenClass.Value == false))).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
           
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMark = (from u in listMarkxFirst
                            where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();
            List<StatisticsConfig> listSConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);

            var listGroupAll = (from lm in listMark
                                group lm by new
                                {
                                    lm.EducationLevelID,
                                    lm.Semester
                                } into g
                                select new ListGroup
                                {
                                    EducationLevelID = g.Key.EducationLevelID,
                                    SemesterID = g.Key.Semester
                                }).Distinct().ToList();

            int totalPupil = 0;
            int countFor = 1 + (statisticsForUnit.FemaleEthnicType > 0 ? 1 : 0) + (statisticsForUnit.EthnicType > 0 ? 1 : 0) + (statisticsForUnit.FemaleType > 0 ? 1 : 0);
            int tmpEFType = statisticsForUnit.FemaleEthnicType;
            int tmpEthnicType = statisticsForUnit.EthnicType;
            int tmpFemaleType = statisticsForUnit.FemaleType;

            foreach (var mg in listGroupAll)
            {
                tmpEFType = statisticsForUnit.FemaleEthnicType;
                tmpEthnicType = statisticsForUnit.EthnicType;
                tmpFemaleType = statisticsForUnit.FemaleType;
                for (int i = 0; i < countFor; i++)
                {
                    ConductStatisticsBO cs = new ConductStatisticsBO();
                    cs.EducationLevelID = (int)mg.EducationLevelID;
                    cs.Semester = mg.SemesterID;
                    cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                    cs.SchoolID = statisticsForUnit.SchoolID;
                    cs.Year = Aca.Year;
                    cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                    mg.Genre = null;
                    mg.EthnicID = null;
                    mg.OrderCretia = 1;
                    if (tmpEFType > 0)
                    {
                        mg.Genre = 1;
                        mg.EthnicID = 1;
                        tmpEFType = 0;
                        mg.OrderCretia = 4;
                    }
                    else if (tmpFemaleType > 0)
                    {
                        mg.Genre = 1;
                        tmpFemaleType = 0;
                        mg.OrderCretia = 2;
                    }
                    else if (tmpEthnicType > 0)
                    {
                        mg.EthnicID = 1;
                        tmpEthnicType = 0;
                        mg.OrderCretia = 3;
                    }
                    foreach (StatisticsConfig sc in listSConfig)
                    {
                        int count = listMark.Count(u => u.EducationLevelID == cs.EducationLevelID
                                                        && u.Semester == mg.SemesterID
                                                        && u.ConductLevelID.ToString().Equals(sc.EqualValue)
                                                        && ((mg.OrderCretia == 4 && mg.Genre.HasValue && mg.EthnicID.HasValue && u.Genre == 0 && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (mg.Genre.HasValue && u.Genre == 0 && mg.OrderCretia == 2)
                                                            || (mg.OrderCretia == 3 && mg.EthnicID.HasValue && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (!mg.Genre.HasValue && !mg.EthnicID.HasValue)));
                        typeof(ConductStatistic).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);
                    }
                    if (mg.Genre.HasValue && mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0
                                               && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else if (mg.Genre.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0).Count();
                    }
                    else if (mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID
                                                    && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID).Count();
                    }

                    cs.PupilTotal = totalPupil;
                    cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCCAP2;
                    cs.ProcessedDate = DateTime.Now;
                    cs.GenreType = mg.Genre;
                    cs.EthnicType = mg.EthnicID;
                    cs.OrderCretia = mg.OrderCretia;
                    retVal.Add(cs);
                }
            }

            retVal = retVal.OrderBy(p=>p.OrderCretia).ThenBy(u => u.EducationLevelID).ToList();

            return retVal;
        }

        public List<CapacityStatisticsBO> SearchCapacityForSecondary(StatisticsForUnitBO statisticsForUnit)
        {
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<CapacityStatisticsBO> retVal = new List<CapacityStatisticsBO>();
            var listMarkxFirst = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester == statisticsForUnit.Semester)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                 join pp in PupilProfileBusiness.AllNoTracking
                                 on pr.PupilID equals pp.PupilProfileID
                                 join cp in ClassProfileBusiness.AllNoTracking
                                 on pr.ClassID equals cp.ClassProfileID
                                 where cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY
                                 && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                 && (pp.IsActive)
                                 && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                 && cp.IsActive==true
                                 select new
                                 {
                                     pr.PupilID,
                                     pr.ClassID,
                                     cp.SubCommitteeID,
                                     pr.EducationLevelID,
                                     pr.CapacityLevelID,
                                     pr.Semester,
                                     pp.EthnicID,
                                     pp.Genre
                                 };
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                // Neu la ca nam thi lay them thong tin thi lai
                var listMarkRetest = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                     join pp in PupilProfileBusiness.AllNoTracking
                                     on pr.PupilID equals pp.PupilProfileID
                                     join cp in ClassProfileBusiness.AllNoTracking
                                     on pr.ClassID equals cp.ClassProfileID
                                     where
                                     cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY

                                     && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                     && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                     && cp.IsActive==true
                                     select new 
                                     {
                                         pr.PupilID,
                                         pr.ClassID,
                                         cp.SubCommitteeID,
                                         pr.EducationLevelID,
                                         pr.CapacityLevelID,
                                         pr.Semester,
                                         pp.EthnicID,
                                         pp.Genre
                                     };


                // join voi thong tin chua thi lai
                listMarkxFirst = from u in listMarkxFirst
                                 join r in listMarkRetest on new { u.PupilID, u.ClassID }
                                 equals new { r.PupilID, r.ClassID } into i
                                 from g in i.DefaultIfEmpty()
                                 select new
                                 {
                                     u.PupilID,
                                     u.ClassID,
                                     u.SubCommitteeID,
                                     u.EducationLevelID,
                                     CapacityLevelID = g.CapacityLevelID.HasValue ? g.CapacityLevelID : u.CapacityLevelID,
                                     u.Semester,
                                     u.EthnicID,
                                     u.Genre
                                 };
            }
            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
                //{"EthnicID",statisticsForUnit.EthnicType},
                //{"FemaleID",statisticsForUnit.FemaleType}
            };



            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).Where(p=>(!p.ClassProfile.IsVnenClass.HasValue || (p.ClassProfile.IsVnenClass.HasValue && p.ClassProfile.IsVnenClass.Value == false))).AddCriteriaSemester(Aca, statisticsForUnit.Semester);


            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMark = (from u in listMarkxFirst
                            where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();
            List<StatisticsConfig> listSConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            var listGroupAll = (from lm in listMark
                            group lm by new
                            {
                                lm.EducationLevelID,
                                lm.Semester
                            } into g
                            select new ListGroup
                            {
                                EducationLevelID = g.Key.EducationLevelID,
                                SemesterID = g.Key.Semester
                            }).Distinct().ToList();

            int totalPupil = 0;
            int countFor = 1 + (statisticsForUnit.FemaleEthnicType > 0 ? 1 : 0) + (statisticsForUnit.EthnicType > 0 ? 1 : 0) + (statisticsForUnit.FemaleType > 0 ? 1 : 0);
            int tmpEFType = statisticsForUnit.FemaleEthnicType;
            int tmpEthnicType = statisticsForUnit.EthnicType;
            int tmpFemaleType = statisticsForUnit.FemaleType;

            foreach (var mg in listGroupAll)
            {
                tmpEFType = statisticsForUnit.FemaleEthnicType;
                tmpEthnicType = statisticsForUnit.EthnicType;
                tmpFemaleType = statisticsForUnit.FemaleType;
                for (int i = 0; i < countFor; i++)
                {
                    CapacityStatisticsBO cs = new CapacityStatisticsBO();
                    cs.EducationLevelID = (int)mg.EducationLevelID;
                    cs.Semester = mg.SemesterID;
                    cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                    cs.SchoolID = statisticsForUnit.SchoolID;
                    cs.Year = Aca.Year;
                    cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                    int belowAverage = 0;
                    int aboveAverage = 0;

                    mg.Genre = null;
                    mg.EthnicID = null;
                    mg.OrderCretia = 1;
                    if (tmpEFType > 0)
                    {
                        mg.Genre = 1;
                        mg.EthnicID = 1;
                        tmpEFType = 0;
                        mg.OrderCretia = 4;
                    }
                    else if (tmpFemaleType > 0)
                    {
                        mg.Genre = 1;
                        tmpFemaleType = 0;
                        mg.OrderCretia = 2;
                    }
                    else if (tmpEthnicType > 0)
                    {
                        mg.EthnicID = 1;
                        tmpEthnicType = 0;
                        mg.OrderCretia = 3;
                    }

                    foreach (StatisticsConfig sc in listSConfig)
                    {
                        int count = listMark.Count(u => u.EducationLevelID == cs.EducationLevelID
                                                        && u.Semester == mg.SemesterID
                                                        && u.CapacityLevelID.ToString().Equals(sc.EqualValue)
                                                        && ((mg.OrderCretia==4 && mg.Genre.HasValue && mg.EthnicID.HasValue && u.Genre == 0 && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (mg.Genre.HasValue && u.Genre == 0 && mg.OrderCretia == 2)
                                                            || (mg.OrderCretia ==3 && mg.EthnicID.HasValue && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (!mg.Genre.HasValue && !mg.EthnicID.HasValue)));
                        typeof(CapacityStatistic).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);

                        belowAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE ? count : 0;
                        aboveAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE ? count : 0;
                    }
                    cs.BelowAverage = (int)belowAverage;
                    cs.OnAverage = (int)aboveAverage;
                    if (mg.Genre.HasValue && mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0
                                               && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else if (mg.Genre.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0).Count();
                    }
                    else if (mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID
                                                    && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID).Count();
                    }

                    cs.PupilTotal = totalPupil;
                    cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCCAP2;
                    cs.ProcessedDate = DateTime.Now;
                    cs.Genre = mg.Genre;
                    cs.EthnicID = mg.EthnicID;
                    cs.OrderCritea = mg.OrderCretia;
                    retVal.Add(cs);
                }
            }

            retVal = retVal.OrderBy(p=>p.OrderCritea).ThenBy(u => u.EducationLevelID).ToList();

            return retVal;
        }

        public Stream CreatePeriodicMarkForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THCS_THONGKE_DIEMKT_DINHKY);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);


            List<StatisticsConfig> listBelow = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            List<StatisticsConfig> listAbove = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            int lastColIndex = listBelow.Count + listAbove.Count + 8;

            List<MarkStatisticBO> listMarkStatistic = SearchPeriodicMarkForSecondary(statisticsForUnit);
            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            List<EducationLevel> listEdu = listMarkStatistic.OrderBy(u => u.EducationLevelID).Select(u => u.EducationLevel).Distinct().ToList();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);

            IVTWorksheet patternSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange markCol = tempSheet.GetRange("A1", "A4");
            IVTRange tbCol = tempSheet.GetRange("B1", "B4");

            //var provider = System.Globalization.CultureInfo.GetCultureInfo("en-GB");

            for (int i = 0; i < listBelow.Count; i++)
            {
                patternSheet.CopyPaste(markCol, 8, i + 5, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                //dataSheet.SetCellValue(8, i + 5, string.Format(provider,format,listBelow[i].MinValue));
                patternSheet.SetCellValue(8, i + 5, string.Format(format, listBelow[i].MinValue));
            }

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + 5, true);
            patternSheet.SetCellValue(8, listBelow.Count + 5, "TS dưới TB");

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + 6, true);
            patternSheet.SetCellValue(8, listBelow.Count + 6, "TL dưới TB");

            for (int i = 0; i < listAbove.Count; i++)
            {
                patternSheet.CopyPaste(markCol, 8, i + listBelow.Count + 7, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                //dataSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(provider, format, listAbove[i].MinValue));
                patternSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(format, listAbove[i].MinValue));
            }

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 7, true);
            patternSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 7, "TS trên TB");

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 8, true);
            patternSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 8, "TL trên TB");

            IVTRange dataRow = patternSheet.GetRange(10, 1, 10, lastColIndex);
            for (int i = 0; i < listMarkStatistic.Count; i++)
            {
                patternSheet.CopyAndInsertARow(dataRow, 11, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                patternSheet.DeleteRow(9);
            }

            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            #endregion

            #region do du lieu vao report

            #region Fill Sheet binh thuong
            IVTWorksheet sheetDefault = oBook.CopySheetToLast(patternSheet);
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();
            string PupilName = string.Empty;
            string SupervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            string districtName = (school.District != null ? school.District.DistrictName : "");
            dicVarable["SupervisingDept"] = SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;
            sheetDefault.FillVariableValue(dicVarable);

            int index = 1;
            List<MarkStatisticBO> listMarkDefault = listMarkStatistic.Where(p => p.OrderCritea == 1).ToList();
            int totalRecordItem = listMarkDefault.Count;
            int totalItem = 0;
            int totalReItem = 0;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listMarkDefault.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                totalItem = listItem.Count();
                totalReItem = totalItem > 0 ? totalItem : 1;
                IVTRange eduRange = sheetDefault.GetRange(index + 8, 2, index + 8 + totalReItem - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    sheetDefault.SetCellValue(index + 8, colIndex++, index);
                    if (!eduFilled)
                    {
                        sheetDefault.SetCellValue(index + 8, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else
                    {
                        colIndex++;
                    }
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                    foreach (StatisticsConfig sc in listBelow)
                    {
                        sheetDefault.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                    double percent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                    sheetDefault.SetCellValue(index + 8, colIndex++, percent.ToString().Replace(",", ".") + "%");
                    foreach (StatisticsConfig sc in listAbove)
                    {
                        sheetDefault.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.OnAverage);
                    sheetDefault.SetCellValue(index + 8, colIndex++, (100 - percent).ToString().Replace(",", ".") + "%");

                    index++;
                }
            }
            #region Xoa dong thua
            for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
            {
                sheetDefault.DeleteRow(totalRecordItem + 9);
            }
            #endregion

            #region reportName
            sheetDefault.Name = "DiemKTDinhKy";
            //reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            //reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            sheetDefault.FitSheetOnOnePage = true;
            #endregion

            #endregion

            #region Fill sheet nu

            if (statisticsForUnit.FemaleType > 0)
            {
                IVTWorksheet sheetFemale = oBook.CopySheetToLast(patternSheet);
                dicVarable["PupilName"] = "HỌC SINH NỮ";
                sheetFemale.FillVariableValue(dicVarable);

                index = 1;
                List<MarkStatisticBO> listMarkFemale = listMarkStatistic.Where(p => p.OrderCritea == 2).ToList();
                totalRecordItem = listMarkFemale.Count;
                foreach (EducationLevel edu in listEdu)
                {
                    var listItem = listMarkFemale.Where(u => u.EducationLevelID == edu.EducationLevelID);
                    bool eduFilled = false;
                    totalItem = listItem.Count();
                    totalReItem = totalItem > 0 ? totalItem : 1;
                    IVTRange eduRange = sheetFemale.GetRange(index + 8, 2, index + 8 + totalReItem - 1, 2);
                    eduRange.Merge();

                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        sheetFemale.SetCellValue(index + 8, colIndex++, index);
                        if (!eduFilled)
                        {
                            sheetFemale.SetCellValue(index + 8, colIndex++, edu.Resolution);
                            eduFilled = true;
                        }
                        else
                        {
                            colIndex++;
                        }
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                        foreach (StatisticsConfig sc in listBelow)
                        {
                            sheetFemale.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                        double percent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                        sheetFemale.SetCellValue(index + 8, colIndex++, percent.ToString().Replace(",", ".") + "%");
                        foreach (StatisticsConfig sc in listAbove)
                        {
                            sheetFemale.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.OnAverage);
                        sheetFemale.SetCellValue(index + 8, colIndex++, (100 - percent).ToString().Replace(",", ".") + "%");

                        index++;
                    }
                }
                #region Xoa dong thua
                for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
                {
                    sheetFemale.DeleteRow(totalRecordItem + 9);
                }
                #endregion

                #region reportName
                sheetFemale.Name = "HS_Nu";
                //reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                //reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                sheetDefault.FitSheetOnOnePage = true;
                #endregion
            }
            #endregion

            #region Fill sheet hoc sinh dan toc

            if (statisticsForUnit.EthnicType > 0)
            {
                totalRecordItem = 0;
                IVTWorksheet sheetEthnic = oBook.CopySheetToLast(patternSheet);
                dicVarable["PupilName"] = "HỌC SINH DÂN TỘC";
                sheetEthnic.FillVariableValue(dicVarable);

                index = 1;
                List<MarkStatisticBO> listMarkEthnic = listMarkStatistic.Where(p => p.OrderCritea == 3).ToList();
                totalRecordItem = listMarkEthnic.Count;
                foreach (EducationLevel edu in listEdu)
                {
                    var listItem = listMarkEthnic.Where(u => u.EducationLevelID == edu.EducationLevelID);
                    bool eduFilled = false;
                    totalItem = listItem.Count();
                    totalReItem = totalItem > 0 ? totalItem : 1;
                    IVTRange eduRange = sheetEthnic.GetRange(index + 8, 2, index + 8 + totalReItem - 1, 2);
                    eduRange.Merge();

                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        sheetEthnic.SetCellValue(index + 8, colIndex++, index);
                        if (!eduFilled)
                        {
                            sheetEthnic.SetCellValue(index + 8, colIndex++, edu.Resolution);
                            eduFilled = true;
                        }
                        else
                        {
                            colIndex++;
                        }
                        sheetEthnic.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                        sheetEthnic.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                        foreach (StatisticsConfig sc in listBelow)
                        {
                            sheetEthnic.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetEthnic.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                        double percent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                        sheetEthnic.SetCellValue(index + 8, colIndex++, percent.ToString().Replace(",", ".") + "%");
                        foreach (StatisticsConfig sc in listAbove)
                        {
                            sheetEthnic.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetEthnic.SetCellValue(index + 8, colIndex++, item.OnAverage);
                        sheetEthnic.SetCellValue(index + 8, colIndex++, (100 - percent).ToString().Replace(",", ".") + "%");

                        index++;
                    }
                }
                #region Xoa dong thua
                for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
                {
                    sheetEthnic.DeleteRow(totalRecordItem + 9);
                }
                #endregion
                #region reportName
                sheetEthnic.Name = "HS_DT";
                //reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                //reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                sheetDefault.FitSheetOnOnePage = true;
                #endregion
            }

            #endregion

            #region Fill sheet hoc sinh nu dan toc

            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                totalRecordItem = 0;
                IVTWorksheet sheetFE = oBook.CopySheetToLast(patternSheet);
                dicVarable["PupilName"] = "HỌC SINH NỮ DÂN TỘC";
                sheetFE.FillVariableValue(dicVarable);
                index = 1;
                List<MarkStatisticBO> listMarkFE = listMarkStatistic.Where(p => p.OrderCritea == 4).ToList();
                totalRecordItem = listMarkFE.Count;
                foreach (EducationLevel edu in listEdu)
                {
                    var listItem = listMarkFE.Where(u => u.EducationLevelID == edu.EducationLevelID);
                    bool eduFilled = false;
                    totalItem = listItem.Count();
                    totalReItem = totalItem > 0 ? totalItem : 1;
                    IVTRange eduRange = sheetFE.GetRange(index + 8, 2, index + 8 + totalReItem - 1, 2);
                    eduRange.Merge();

                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        sheetFE.SetCellValue(index + 8, colIndex++, index);
                        if (!eduFilled)
                        {
                            sheetFE.SetCellValue(index + 8, colIndex++, edu.Resolution);
                            eduFilled = true;
                        }
                        else
                        {
                            colIndex++;
                        }
                        sheetFE.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                        sheetFE.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                        foreach (StatisticsConfig sc in listBelow)
                        {
                            sheetFE.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFE.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                        double percent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                        sheetFE.SetCellValue(index + 8, colIndex++, percent.ToString().Replace(",", ".") + "%");
                        foreach (StatisticsConfig sc in listAbove)
                        {
                            sheetFE.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFE.SetCellValue(index + 8, colIndex++, item.OnAverage);
                        sheetFE.SetCellValue(index + 8, colIndex++, (100 - percent).ToString().Replace(",", ".") + "%");

                        index++;
                    }
                }

                #region Xoa dong thua
                for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
                {
                    sheetFE.DeleteRow(totalRecordItem + 9);
                }
                #endregion

                #region reportName
                sheetFE.Name = "HS_Nu_DT";               
                sheetDefault.FitSheetOnOnePage = true;
                #endregion
            }

            #endregion
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            patternSheet.Delete();
            tempSheet.Delete();
            #endregion
            tempSheet.FitToPage = true;
            return oBook.ToStream();
        }

        public Stream CreateSemesterMarkForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THCS_THONGKE_DIEMKIEMTRA_HOCKY);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);


            List<StatisticsConfig> listBelow = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            List<StatisticsConfig> listAbove = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            int lastColIndex = listBelow.Count + listAbove.Count + 8;

            List<MarkStatisticBO> listMarkStatistic = SearchSemesterMarkForSecondary(statisticsForUnit);
            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<EducationLevel> listEdu = listMarkStatistic.OrderBy(u => u.EducationLevelID).Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet parttenSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange markCol = tempSheet.GetRange("A1", "A4");
            IVTRange tbCol = tempSheet.GetRange("B1", "B4");

            //var provider = System.Globalization.CultureInfo.GetCultureInfo("en-GB");

            for (int i = 0; i < listBelow.Count; i++)
            {
                parttenSheet.CopyPaste(markCol, 8, i + 5, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                //dataSheet.SetCellValue(8, i + 5, string.Format(provider,format, listBelow[i].MinValue));
                parttenSheet.SetCellValue(8, i + 5, string.Format(format, listBelow[i].MinValue));
            }

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + 5, true);
            parttenSheet.SetCellValue(8, listBelow.Count + 5, "TS dưới TB");

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + 6, true);
            parttenSheet.SetCellValue(8, listBelow.Count + 6, "TL dưới TB");

            for (int i = 0; i < listAbove.Count; i++)
            {
                parttenSheet.CopyPaste(markCol, 8, i + listBelow.Count + 7, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                //dataSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(provider,format, listAbove[i].MinValue));
                parttenSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(format, listAbove[i].MinValue));
            }

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 7, true);
            parttenSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 7, "TS trên TB");

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 8, true);
            parttenSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 8, "TL trên TB");

            IVTRange dataRow = parttenSheet.GetRange(10, 1, 10, lastColIndex);
            for (int i = 0; i < listMarkStatistic.Count; i++)
            {
                parttenSheet.CopyAndInsertARow(dataRow, 11, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                parttenSheet.DeleteRow(9);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;

            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            #region Fill sheet dau tien
            this.FillSheetCreateSemesterMarkForSecondaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 1,eduLevel,reportName,reportDefinition);
            #endregion

            #region  Fill sheet nu
            if (statisticsForUnit.FemaleType > 0)
            {
                this.FillSheetCreateSemesterMarkForSecondaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 2, eduLevel, reportName, reportDefinition);
            }
            #endregion

            #region  Fill sheet dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                this.FillSheetCreateSemesterMarkForSecondaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 3, eduLevel, reportName, reportDefinition);
            }
            #endregion

            #region  Fill sheet nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                this.FillSheetCreateSemesterMarkForSecondaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 4, eduLevel, reportName, reportDefinition);
            }
            #endregion

            parttenSheet.Delete();
            tempSheet.Delete();
            #endregion
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            tempSheet.FitToPage = true;
            return oBook.ToStream();
        }

        private void FillSheetCreateSemesterMarkForSecondaryReport(IVTWorkbook oBook, IVTWorksheet parttenSheet, Dictionary<string, object> dicVarable,
            List<MarkStatisticBO> listMarkStatistic, List<EducationLevel> listEdu, List<StatisticsConfig> listBelow, List<StatisticsConfig> listAbove,
            StatisticsForUnitBO statisticsForUnit, int orderCretia, EducationLevel eduLevel, string reportName, ReportDefinition reportDefinition)
        {
            string SheetName = string.Empty;
            string PupilName = string.Empty;
            if (orderCretia == 2)
            {
                SheetName = "HS_Nu";
                PupilName = "HỌC SINH NỮ";
            }
            else if (orderCretia == 3)
            {
                SheetName = "HS_DT";
                PupilName = "HỌC SINH DÂN TỘC";
            }
            else if (orderCretia == 4)
            {
                SheetName = "HS_Nu_DT";
                PupilName = "HỌC SINH NỮ DÂN TỘC";
            }
            else if (orderCretia == 1)
            {
                SheetName = "DiemKTHK";
            }
            dicVarable["PupilName"] = PupilName;

            IVTWorksheet firstSheet = oBook.CopySheetToLast(parttenSheet);
            firstSheet.FillVariableValue(dicVarable);

            int index = 1;
            int countListItem = 0;
            int countItem = 0;
            List<MarkStatisticBO> listMarkStatisticDefault = listMarkStatistic.Where(p => p.OrderCritea == orderCretia).ToList();
            int totalRecordItem = listMarkStatisticDefault.Count;

            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listMarkStatisticDefault.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                countListItem = listItem.Count();
                countItem = countListItem > 0 ? countListItem : 1;
                IVTRange eduRange = firstSheet.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    firstSheet.SetCellValue(index + 8, colIndex++, index);
                    if (!eduFilled)
                    {
                        firstSheet.SetCellValue(index + 8, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;
                    firstSheet.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                    firstSheet.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                    foreach (StatisticsConfig sc in listBelow)
                    {
                        firstSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    firstSheet.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                    decimal percent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100m * item.BelowAverage.Value / item.MarkTotal.Value),1, MidpointRounding.AwayFromZero) : 0;
                    firstSheet.SetCellValue(index + 8, colIndex++, percent.ToString().Replace(",", ".") + "%");
                    foreach (StatisticsConfig sc in listAbove)
                    {
                        firstSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    firstSheet.SetCellValue(index + 8, colIndex++, item.OnAverage);
                    firstSheet.SetCellValue(index + 8, colIndex++, (100m - percent).ToString().Replace(",", ".") + "%");

                    index++;
                }
            }
            #region Xoa dong thua
            for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
            {
                firstSheet.DeleteRow(totalRecordItem + 9);
            }
            #endregion

            firstSheet.Name = SheetName;         
            firstSheet.FitSheetOnOnePage = true;
        }

        public Stream CreateSubjectCapacityForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THCS_THONGKE_DIEM_TBM);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listJudge = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            List<StatisticsConfig> listMark = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            int lastColIndex = (listJudge.Count + listMark.Count) * 2 + 6;

            List<CapacityStatisticsBO> listCapacityStatistic = SearchSubjectCapacityForSecondary(statisticsForUnit);
            listCapacityStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listCapacityStatistic = listCapacityStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<EducationLevel> listEdu = listCapacityStatistic.OrderBy(u => u.EducationLevelID).Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet pattarnSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange tempCol = tempSheet.GetRange("A1", "B5");

            for (int i = 0; i < listMark.Count; i++)
            {
                pattarnSheet.CopyPaste(tempCol, 8, i * 2 + 5);
                pattarnSheet.SetCellValue(8, i * 2 + 5, listMark[i].DisplayName);
            }

            for (int i = 0; i < listJudge.Count; i++)
            {
                pattarnSheet.CopyPaste(tempCol, 8, i * 2 + listMark.Count * 2 + 5);
                pattarnSheet.SetCellValue(8, i * 2 + listMark.Count * 2 + 5, listJudge[i].DisplayName);
            }
            pattarnSheet.CopyPaste(tempCol, 8, (listJudge.Count + listMark.Count) * 2 + 5);
            pattarnSheet.SetCellValue(8, (listJudge.Count + listMark.Count) * 2 + 5, "Trên TB");

            IVTRange dataRow = pattarnSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < listCapacityStatistic.Count; i++)
            {
                pattarnSheet.CopyAndInsertARow(dataRow, 12, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                pattarnSheet.DeleteRow(10);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);

            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);

            #region Sheet dau tien
            this.FillSubjectCapacityForSecondaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 1);
            #endregion

            #region sheet nu
            if (statisticsForUnit.FemaleType > 0)
            {
                this.FillSubjectCapacityForSecondaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 2);
            }
            #endregion

            #region sheet dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                this.FillSubjectCapacityForSecondaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 3);
            }
            #endregion

            #region sheet nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                this.FillSubjectCapacityForSecondaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 4);
            }
            #endregion

            pattarnSheet.Delete();
            tempSheet.Delete();
            #endregion

            #region reportName           
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion           
            tempSheet.FitToPage = true;
            return oBook.ToStream();
        }

        private void FillSubjectCapacityForSecondaryReport(IVTWorkbook oBook, IVTWorksheet pattarnSheet, List<CapacityStatisticsBO> listCapacityStatistic
            , List<EducationLevel> listEdu, List<StatisticsConfig> listMark, List<StatisticsConfig> listJudge
            , Dictionary<string, object> dicVarable, int orderCritea)
        {
            IVTWorksheet dataSheet = oBook.CopySheetToLast(pattarnSheet);
            string SheetName = string.Empty;
            string PupilName = string.Empty;
            if (orderCritea == 2)
            {
                SheetName = "HS_Nu";
                PupilName = "HỌC SINH NỮ";
            }
            else if (orderCritea == 3)
            {
                SheetName = "HS_DT";
                PupilName = "HỌC SINH DÂN TỘC";
            }
            else if (orderCritea == 4)
            {
                SheetName = "HS_Nu_DT";
                PupilName = "HỌC SINH NỮ DÂN TỘC";
            }
            else if (orderCritea == 1)
            {
                SheetName = "ThongKeDiemTBM";
            }
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            int totalListItem = 0;
            int totalItem = 0;
            List<CapacityStatisticsBO> listCapacities = listCapacityStatistic.Where(p => p.OrderCritea == orderCritea).ToList();
            int totalRecordItem = listCapacities.Count;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listCapacities.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                totalListItem = listItem.Count();
                totalItem = totalListItem > 0 ? totalListItem : 1;
                IVTRange eduRange = dataSheet.GetRange(index + 9, 2, index + 9 + totalItem - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 9, colIndex++, index);

                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 9, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;

                    dataSheet.SetCellValue(index + 9, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountMark = new List<int?>();
                    foreach (StatisticsConfig sc in listMark)
                    {
                        int? markValue = (int?)typeof(CapacityStatisticsBO).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountMark.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountMark, item.PupilTotal);
                    double totalAbovePer = 0;
                    for (int i = 0; i < listMark.Count; i++)
                    {
                        StatisticsConfig sc = listMark[i];
                        int? markValue = (int?)typeof(CapacityStatisticsBO).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[i];
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                        if (percent.HasValue)
                        {
                            if (sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                totalAbovePer += percent.Value;
                            }
                        }
                    }
                    listCountMark.Clear();
                    foreach (StatisticsConfig sc in listJudge)
                    {
                        int? markValue = (int?)typeof(CapacityStatisticsBO).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountMark.Add(markValue);
                    }
                    listPercent = ReportUtils.GetPerCentInList(listCountMark, item.PupilTotal);
                    for (int i = 0; i < listJudge.Count; i++)
                    {
                        StatisticsConfig sc = listJudge[i];
                        int? markValue = (int?)typeof(CapacityStatisticsBO).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[i];
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }
                    dataSheet.SetCellValue(index + 9, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.OnAverage.HasValue ? totalAbovePer.ToString().Replace(",", ".") : string.Empty);
                    index++;
                }
            }
            #region Xoa dong thua
            for (int i = totalRecordItem; i < listCapacityStatistic.Count; i++)
            {
                dataSheet.DeleteRow(totalRecordItem + 10);
            }
            #endregion
            dataSheet.Name = SheetName;
            dataSheet.FitSheetOnOnePage = true;
        }

        public Stream CreateConductForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THCS_THONGKE_XLHANH_KIEM);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);

            List<ConductStatisticsBO> listConductStatistic = SearchConductForSecondary(statisticsForUnit);
            listConductStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
            });
            List<ConductStatisticsBO> lstTMP = null;
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);     
            #region tat ca
            #region tao khung
            IVTWorksheet dataSheet = oBook.CopySheetToLast(sheet1);
            IVTWorksheet tempSheet = oBook.CopySheetToLast(sheet2);
            IVTRange tbCol = tempSheet.GetRange("A1", "B5");
            IVTRange totalRange = tempSheet.GetRange("A7", "C7");
            lstTMP = listConductStatistic.Where(p => !p.EthnicType.HasValue && !p.GenreType.HasValue).ToList();
            int lastColIndex = listConfig.Count * 2 + 3;
            int lastRowIndex = lstTMP.Count + 10;
            for (int i = 0; i < listConfig.Count; i++)
            {
                dataSheet.CopyPaste(tbCol, 8, i * 2 + 4);
                dataSheet.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
            }

            IVTRange dataRow = dataSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < lstTMP.Count; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 12, true);
            }
            // Xoa 2 dong thua
            for (int i = 0; i < 2; i++)
            {
                dataSheet.DeleteRow(10);
            }
            dataSheet.CopyPaste(totalRange, lstTMP.Count + 10, 1);
            #endregion
            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;
            string SheetName = string.Empty;
            SheetName = "ThongKeXLHK";
            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            foreach (ConductStatisticsBO item in lstTMP)
            {
                int colIndex = 1;
                dataSheet.SetCellValue(index + 9, colIndex++, index);
                dataSheet.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                List<int?> listCountConduct = new List<int?>();
                foreach (StatisticsConfig sc in listConfig)
                {
                    int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                    listCountConduct.Add(markValue);
                }
                List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                int pos = 0;
                foreach (StatisticsConfig sc in listConfig)
                {
                    int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                    double? percent = listPercent[pos];
                    pos++;
                    dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                    dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                }

                index++;
            }

            dataSheet.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
            for (int i = 0; i < listConfig.Count; i++)
            {
                dataSheet.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                dataSheet.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
            }
            #endregion
            #region reportName
            dataSheet.Name = SheetName;
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            #endregion
            #region hoc sinh nu
            if (statisticsForUnit.FemaleType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetFemale = oBook.CopySheetToLast(sheet1);
                lstTMP = listConductStatistic.Where(p => !p.EthnicType.HasValue && p.GenreType > 0).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemale.CopyPaste(tbCol, 8, i * 2 + 4);
                    dataSheetFemale.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
                }

                dataRow = dataSheetFemale.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count; i++)
                {
                    dataSheetFemale.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 2 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetFemale.DeleteRow(10);
                }
                dataSheetFemale.CopyPaste(totalRange, lstTMP.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                SheetName = "HS_Nu";
                PupilName = "HỌC SINH NỮ";
                dicVarable["PupilName"] = PupilName;
                dataSheetFemale.FillVariableValue(dicVarable);

                index = 1;
                foreach (ConductStatisticsBO item in lstTMP)
                {
                    int colIndex = 1;
                    dataSheetFemale.SetCellValue(index + 9, colIndex++, index);
                    dataSheetFemale.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheetFemale.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountConduct = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountConduct.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }

                    index++;
                }

                dataSheetFemale.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemale.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                    dataSheetFemale.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
                }
                #endregion
                #region reportName
                dataSheetFemale.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
            }
            #endregion
            #region hoc sinh dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetEthnic = oBook.CopySheetToLast(sheet1);
                lstTMP = listConductStatistic.Where(p => p.EthnicType > 0 && !p.GenreType.HasValue).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnic.CopyPaste(tbCol, 8, i * 2 + 4);
                    dataSheetEthnic.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
                }

                dataRow = dataSheetEthnic.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count; i++)
                {
                    dataSheetEthnic.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 2 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetEthnic.DeleteRow(10);
                }
                dataSheetEthnic.CopyPaste(totalRange, lstTMP.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                SheetName = "HS_DT";
                PupilName = "HỌC SINH DÂN TỘC";
                dicVarable["PupilName"] = PupilName;
                dataSheetEthnic.FillVariableValue(dicVarable);

                index = 1;
                foreach (ConductStatisticsBO item in lstTMP)
                {
                    int colIndex = 1;
                    dataSheetEthnic.SetCellValue(index + 9, colIndex++, index);
                    dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountConduct = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountConduct.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }

                    index++;
                }

                dataSheetEthnic.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnic.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                    dataSheetEthnic.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
                }
                #endregion
                #region reportName
                dataSheetEthnic.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
            }
            
            #endregion
            #region hoc sinh nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetEthnicFemale = oBook.CopySheetToLast(sheet1);
                lstTMP = listConductStatistic.Where(p => p.EthnicType > 0 && p.GenreType > 0).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnicFemale.CopyPaste(tbCol, 8, i * 2 + 4);
                    dataSheetEthnicFemale.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
                }

                dataRow = dataSheetEthnicFemale.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count; i++)
                {
                    dataSheetEthnicFemale.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 2 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetEthnicFemale.DeleteRow(10);
                }
                dataSheetEthnicFemale.CopyPaste(totalRange, lstTMP.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                SheetName = "HS_Nu_DT";
                PupilName = "HỌC SINH NỮ DÂN TỘC";
                dicVarable["PupilName"] = PupilName;
                dataSheetEthnicFemale.FillVariableValue(dicVarable);

                index = 1;
                foreach (ConductStatisticsBO item in lstTMP)
                {
                    int colIndex = 1;
                    dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, index);
                    dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountConduct = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountConduct.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }

                    index++;
                }

                dataSheetEthnicFemale.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnicFemale.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                    dataSheetEthnicFemale.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
                }
                #endregion
                #region reportName
                dataSheetEthnicFemale.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
            }
            #endregion
            sheet1.Delete();
            sheet2.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }

        public Stream CreateCapacityForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THCS_THONGKE_XLHOCLUC);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_SECONDARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            List<CapacityStatisticsBO> listCapacityStatistic = this.SearchCapacityForSecondary(statisticsForUnit);
            listCapacityStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
            });

            List<CapacityStatisticsBO> lstTMP = null;
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);           
            #region tat ca
            #region tao khung
            IVTWorksheet dataSheet = oBook.CopySheetToLast(sheet1);
            IVTWorksheet tempSheet = oBook.CopySheetToLast(sheet2);
            IVTRange tbCol = tempSheet.GetRange("A1", "B5");
            IVTRange totalRange = tempSheet.GetRange("A7", "C7");
            lstTMP = listCapacityStatistic.Where(p => !p.EthnicID.HasValue && !p.Genre.HasValue).ToList();
            int lastColIndex = listConfig.Count * 2 + 3;
            int lastRowIndex = lstTMP.Count + 10;
            for (int i = 0; i < listConfig.Count; i++)
            {
                dataSheet.CopyPaste(tbCol, 8, i * 2 + 4);
                dataSheet.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
            }

            IVTRange dataRow = dataSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < lstTMP.Count; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 12, true);
            }
            // Xoa 2 dong thua
            for (int i = 0; i < 2; i++)
            {
                dataSheet.DeleteRow(10);
            }

            dataSheet.CopyPaste(totalRange, lstTMP.Count + 10, 1);
            #endregion
            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;
            string SheetName = string.Empty;
            SheetName = "ThongKeXLHL";
            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);
            
            int index = 1;
            foreach (CapacityStatistic item in lstTMP)
            {
                int colIndex = 1;
                dataSheet.SetCellValue(index + 9, colIndex++, index);
                dataSheet.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                List<int?> listCountCapacity = new List<int?>();
                foreach (StatisticsConfig sc in listConfig)
                {
                    int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                    listCountCapacity.Add(markValue);
                }
                List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                int pos = 0;
                foreach (StatisticsConfig sc in listConfig)
                {
                    int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                    double? percent = listPercent[pos];
                    pos++;
                    dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                    dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                }

                index++;
            }

            dataSheet.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
            for (int i = 0; i < listConfig.Count; i++)
            {
                dataSheet.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                dataSheet.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
            }

            
            #endregion
            #region reportName
            dataSheet.Name = SheetName;
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            dataSheet.FitSheetOnOnePage = true;
            tempSheet.FitToPage = true;
            #endregion
            #region hoc sinh nu
            if (statisticsForUnit.FemaleType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetFemale = oBook.CopySheetToLast(sheet1);
                tbCol = tempSheet.GetRange("A1", "B5");
                totalRange = tempSheet.GetRange("A7", "C7");
                lstTMP = listCapacityStatistic.Where(p => p.Genre > 0 && !p.EthnicID.HasValue).ToList();
                lastRowIndex = lstTMP.Count + 10;
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemale.CopyPaste(tbCol, 8, i * 2 + 4);
                    dataSheetFemale.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
                }

                dataRow = dataSheetFemale.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count; i++)
                {
                    dataSheetFemale.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 2 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetFemale.DeleteRow(10);
                }

                dataSheetFemale.CopyPaste(totalRange, lstTMP.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                PupilName = "HỌC SINH NỮ";
                SheetName = "HS_Nu";
                dicVarable["PupilName"] = PupilName;
                dataSheetFemale.FillVariableValue(dicVarable);
                
                index = 1;
                foreach (CapacityStatistic item in lstTMP)
                {
                    int colIndex = 1;
                    dataSheetFemale.SetCellValue(index + 9, colIndex++, index);
                    dataSheetFemale.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheetFemale.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountCapacity = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountCapacity.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }

                    index++;
                }

                dataSheetFemale.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemale.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                    dataSheetFemale.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
                }
                #endregion
                #region reportName
                dataSheetFemale.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
                dataSheetFemale.FitSheetOnOnePage = true;
            }
            #endregion
            #region hoc sinh dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetEthnic = oBook.CopySheetToLast(sheet1);
                tbCol = tempSheet.GetRange("A1", "B5");
                totalRange = tempSheet.GetRange("A7", "C7");
                lstTMP = listCapacityStatistic.Where(p => p.EthnicID > 0 && !p.Genre.HasValue).ToList();
                lastRowIndex = lstTMP.Count + 10;
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnic.CopyPaste(tbCol, 8, i * 2 + 4);
                    dataSheetEthnic.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
                }

                dataRow = dataSheetEthnic.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count; i++)
                {
                    dataSheetEthnic.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 2 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetEthnic.DeleteRow(10);
                }

                dataSheetEthnic.CopyPaste(totalRange, lstTMP.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                PupilName = "HỌC SINH DÂN TỘC";
                SheetName = "HS_DT";
                dicVarable["PupilName"] = PupilName;
                dataSheetEthnic.FillVariableValue(dicVarable);
                
                index = 1;
                foreach (CapacityStatistic item in lstTMP)
                {
                    int colIndex = 1;
                    dataSheetEthnic.SetCellValue(index + 9, colIndex++, index);
                    dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountCapacity = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountCapacity.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }

                    index++;
                }

                dataSheetEthnic.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnic.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                    dataSheetEthnic.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
                }
                #endregion
                #region reportName
                dataSheetEthnic.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
                dataSheetEthnic.FitSheetOnOnePage = true;
            }
            #endregion
            #region hoc sinh nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetEthnicFemale = oBook.CopySheetToLast(sheet1);
                tbCol = tempSheet.GetRange("A1", "B5");
                totalRange = tempSheet.GetRange("A7", "C7");
                lstTMP = listCapacityStatistic.Where(p => p.EthnicID > 0 && p.Genre > 0).ToList();
                lastRowIndex = lstTMP.Count + 10;
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnicFemale.CopyPaste(tbCol, 8, i * 2 + 4);
                    dataSheetEthnicFemale.SetCellValue(8, i * 2 + 4, listConfig[i].DisplayName);
                }

                dataRow = dataSheetEthnicFemale.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count; i++)
                {
                    dataSheetEthnicFemale.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 2 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetEthnicFemale.DeleteRow(10);
                }

                dataSheetEthnicFemale.CopyPaste(totalRange, lstTMP.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                PupilName = "HỌC SINH NỮ DÂN TỘC";
                SheetName = "HS_Nu_DT";
                dicVarable["PupilName"] = PupilName;
                dataSheetEthnicFemale.FillVariableValue(dicVarable);
                
                index = 1;
                foreach (CapacityStatistic item in lstTMP)
                {
                    int colIndex = 1;
                    dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, index);
                    dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountCapacity = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountCapacity.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheetEthnicFemale.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }

                    index++;
                }

                dataSheetEthnicFemale.SetFormulaValue(lastRowIndex, 3, string.Format("=SUM(C10:C{0})", lastRowIndex - 1));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnicFemale.SetFormulaValue(lastRowIndex, i * 2 + 4, string.Format("=SUM({0}10:{0}{1})", (char)('A' + (i * 2 + 3)), lastRowIndex - 1));
                    dataSheetEthnicFemale.SetFormulaValue(lastRowIndex, i * 2 + 5, string.Format("=IF(AND(C{1}<>\"\"; C{1}<>0); ROUND(100*{0}{1}/C{1};1); \"\")", (char)('A' + (i * 2 + 3)), lastRowIndex));
                }
                #endregion
                #region reportName
                dataSheetEthnicFemale.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
                dataSheetEthnicFemale.FitSheetOnOnePage = true;
            }
            #endregion
            tempSheet.Delete();
            sheet1.Delete();
            sheet2.Delete();
            return oBook.ToStream();
        }
        #endregion

        #region TERTIARY
        public List<MarkStatisticBO> SearchPeriodicMarkForTertiary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);

            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            IQueryable<VMarkRecordBO> listMarkxAll = from mr in VMarkRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID)
                                                     join pp in PupilProfileBusiness.AllNoTracking on mr.PupilID equals pp.PupilProfileID
                                                     join cp in ClassProfileBusiness.AllNoTracking on mr.ClassID equals cp.ClassProfileID
                                                     join mt in MarkTypeBusiness.AllNoTracking on mr.MarkTypeID equals mt.MarkTypeID
                                                     where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel && (mt.Title == "V" || mt.Title == "HK")
                                                     && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                                     && cp.IsActive==true
                                                     //&& (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                                     select new VMarkRecordBO
                                                     {
                                                         SchoolID = mr.SchoolID,
                                                         PupilID = mr.PupilID,
                                                         ClassID = mr.ClassID,
                                                         EducationLevelID = cp.EducationLevelID,
                                                         SubjectID = mr.SubjectID,
                                                         Mark = mr.Mark,
                                                         Semester = mr.Semester,
                                                         Genre = pp.Genre,
                                                         EthnicID = pp.EthnicID
                                                     };
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }                    
            });
            var listUA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            var listOA = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            #region Thong ke binh thuong
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
               // {"FemaleID",statisticsForUnit.FemaleType},
               // {"EthnicID",statisticsForUnit.EthnicType}
            };

            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
            var listMark = (from u in listMarkxAll
                            where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                            && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();

            var listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
            foreach (var mg in listMarkGroup)
            {
                MarkStatisticBO ms = new MarkStatisticBO();
                ms.SubjectID = mg.SubjectID;
                ms.EducationLevelID = (int)mg.EducationLevelID;
                ms.Semester = mg.Semester.Value;
                ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                ms.SchoolID = statisticsForUnit.SchoolID;
                ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                ms.Year = Aca.Year;

                int belowAverage = 0;
                foreach (var ua in listUA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    belowAverage += count;
                    typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.BelowAverage = (int)belowAverage;

                int onAverage = 0;
                foreach (var ua in listOA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    onAverage += count;
                    typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.OnAverage = (int)onAverage;

                ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP3;
                ms.ProcessedDate = DateTime.Now;
                ms.OrderCritea = 1;
                ms.Critea = "Tất cả";
                listMarkStatistic.Add(ms);
            }
            #endregion

            #region Thong ke hoc sinh nu

            List<MarkStatisticBO> listMarkStatisticFemale = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleType > 0)
            {
                Dictionary<string, object> dicPupilRankingFemale = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", statisticsForUnit.AcademicYearID},
                        {"SchoolID", statisticsForUnit.SchoolID},
                        {"Semester", statisticsForUnit.Semester},
                        {"AppliedLevel", statisticsForUnit.AppliedLevel},
                        {"FemaleID",statisticsForUnit.FemaleType},
                       // {"EthnicID",statisticsForUnit.EthnicType}
                    };

                lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRankingFemale).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
                listMark = (from u in listMarkxAll
                            where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                            && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();

                listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
                foreach (var mg in listMarkGroup)
                {
                    MarkStatisticBO ms = new MarkStatisticBO();
                    ms.SubjectID = mg.SubjectID;
                    ms.EducationLevelID = (int)mg.EducationLevelID;
                    ms.Semester = mg.Semester.Value;
                    ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                    ms.SchoolID = statisticsForUnit.SchoolID;
                    ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                    ms.Year = Aca.Year;

                    int belowAverage = 0;
                    foreach (var ua in listUA)
                    {
                        int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                        belowAverage += count;
                        typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                    }
                    ms.BelowAverage = (int)belowAverage;

                    int onAverage = 0;
                    foreach (var ua in listOA)
                    {
                        int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                        onAverage += count;
                        typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                    }
                    ms.OnAverage = (int)onAverage;

                    ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                    ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP3;
                    ms.ProcessedDate = DateTime.Now;
                    ms.OrderCritea = 2;
                    ms.Critea = "HS nữ";
                    listMarkStatisticFemale.Add(ms);
                }
            }

            #endregion

            #region Thong ke hoc sinh dan toc
            List<MarkStatisticBO> listMarkStatisticEthnic = new List<MarkStatisticBO>();
            if (statisticsForUnit.EthnicType > 0)
            {
                Dictionary<string, object> dicPupilRankingEthnic = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", statisticsForUnit.AcademicYearID},
                        {"SchoolID", statisticsForUnit.SchoolID},
                        {"Semester", statisticsForUnit.Semester},
                        {"AppliedLevel", statisticsForUnit.AppliedLevel},
                        //{"FemaleID",statisticsForUnit.FemaleType},
                        {"EthnicID",statisticsForUnit.EthnicType}
                    };

                lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRankingEthnic).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
                listMark = (from u in listMarkxAll
                            where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                            && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();

                listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
                foreach (var mg in listMarkGroup)
                {
                    MarkStatisticBO ms = new MarkStatisticBO();
                    ms.SubjectID = mg.SubjectID;
                    ms.EducationLevelID = (int)mg.EducationLevelID;
                    ms.Semester = mg.Semester.Value;
                    ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                    ms.SchoolID = statisticsForUnit.SchoolID;
                    ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                    ms.Year = Aca.Year;

                    int belowAverage = 0;
                    foreach (var ua in listUA)
                    {
                        int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                        belowAverage += count;
                        typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                    }
                    ms.BelowAverage = (int)belowAverage;

                    int onAverage = 0;
                    foreach (var ua in listOA)
                    {
                        int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                        onAverage += count;
                        typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                    }
                    ms.OnAverage = (int)onAverage;

                    ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                    ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP3;
                    ms.ProcessedDate = DateTime.Now;
                    ms.OrderCritea = 3;
                    ms.Critea = "HS DT";
                    listMarkStatisticEthnic.Add(ms);
                }
            }
            #endregion

            #region Thong ke hoc sinh nu dan toc
            List<MarkStatisticBO> listMarkStatisticFemaleEthnic = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                Dictionary<string, object> dicPupilRankingEthnic = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", statisticsForUnit.AcademicYearID},
                        {"SchoolID", statisticsForUnit.SchoolID},
                        {"Semester", statisticsForUnit.Semester},
                        {"AppliedLevel", statisticsForUnit.AppliedLevel},
                        {"FemaleID",statisticsForUnit.FemaleType},
                        {"EthnicID",statisticsForUnit.EthnicType}
                    };

                lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRankingEthnic).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
                listMark = (from u in listMarkxAll
                            where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                            && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();

                listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
                foreach (var mg in listMarkGroup)
                {
                    MarkStatisticBO ms = new MarkStatisticBO();
                    ms.SubjectID = mg.SubjectID;
                    ms.EducationLevelID = (int)mg.EducationLevelID;
                    ms.Semester = mg.Semester.Value;
                    ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                    ms.SchoolID = statisticsForUnit.SchoolID;
                    ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                    ms.Year = Aca.Year;

                    int belowAverage = 0;
                    foreach (var ua in listUA)
                    {
                        int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                        belowAverage += count;
                        typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                    }
                    ms.BelowAverage = (int)belowAverage;

                    int onAverage = 0;
                    foreach (var ua in listOA)
                    {
                        int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                        onAverage += count;
                        typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                    }
                    ms.OnAverage = (int)onAverage;

                    ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                    ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP3;
                    ms.ProcessedDate = DateTime.Now;
                    ms.OrderCritea = 4;
                    ms.Critea = "HS nữ DT";
                    listMarkStatisticEthnic.Add(ms);
                }
            }
            #endregion

            listMarkStatistic = listMarkStatistic.Union(listMarkStatisticFemale).Union(listMarkStatisticEthnic).Union(listMarkStatisticFemaleEthnic)
                                .OrderBy(u => u.OrderCritea).ThenBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return listMarkStatistic;
        }

        public List<MarkStatisticBO> SearchSemesterMarkForTertiary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);

            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            IQueryable<StatisticUnitBO> listMarkxAll = from mr in VMarkRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && (u.Title == "HK"))
                                                       join pp in PupilProfileBusiness.AllNoTracking on mr.PupilID equals pp.PupilProfileID
                                                       join cp in ClassProfileBusiness.AllNoTracking on mr.ClassID equals cp.ClassProfileID
                                                       where (cp.EducationLevel.Grade == statisticsForUnit.AppliedLevel
                                                       && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                                       && cp.IsActive==true
                                                       select new StatisticUnitBO
                                                       {
                                                           SchoolID = mr.SchoolID,
                                                           PupilID = mr.PupilID,
                                                           ClassID = mr.ClassID,
                                                           EducationLevelID = cp.EducationLevelID,
                                                           SubjectID = mr.SubjectID,
                                                           Mark = mr.Mark,
                                                           Semester = mr.Semester,
                                                           Genre = pp.Genre,
                                                           EthnicId = pp.EthnicID
                                                       };

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester },                    
            });


            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);SearchSemesterMarkForSecondary

            #region Get du lieu sheet binh thuong
            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
                //{"EthnicID",statisticsForUnit.EthnicType},
                //{"FemaleID",statisticsForUnit.FemaleType}
            };
            listMarkStatistic = this.GetListDataSemesterMarkForTertiary(dic, statisticsForUnit, listClassSubject, Aca, listMarkxAll,1);

            #endregion end fill sheet binh thuong

            #region Lay du lieu hoc sinh nu
            List<MarkStatisticBO> listMarkStatisticFemale = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleType > 0)
            {
                Dictionary<string, object> dic1 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    //{"EthnicID",statisticsForUnit.EthnicType},
                    {"FemaleID",statisticsForUnit.FemaleType}
                };
                listMarkStatisticFemale = this.GetListDataSemesterMarkForTertiary(dic1, statisticsForUnit, listClassSubject, Aca, listMarkxAll,2);
            }
            #endregion

            #region Lay du lieu hoc sinh dan toc
            List<MarkStatisticBO> listMarkStatisticEthnic = new List<MarkStatisticBO>();
            if (statisticsForUnit.EthnicType > 0)
            {
                Dictionary<string, object> dic2 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"EthnicID",statisticsForUnit.EthnicType}
                   // {"FemaleID",statisticsForUnit.FemaleType}
                };
                listMarkStatisticEthnic = this.GetListDataSemesterMarkForTertiary(dic2, statisticsForUnit, listClassSubject, Aca, listMarkxAll,3);
            }
            #endregion

            #region Lay du lieu hoc sinh nu dan toc
            List<MarkStatisticBO> listMarkStatisticFE = new List<MarkStatisticBO>();
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                Dictionary<string, object> dic3 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"EthnicID",statisticsForUnit.EthnicType},
                    {"FemaleID",statisticsForUnit.FemaleType}
                };
                listMarkStatisticFE = this.GetListDataSemesterMarkForTertiary(dic3, statisticsForUnit, listClassSubject, Aca, listMarkxAll,4);
            }
            #endregion

            listMarkStatistic = listMarkStatistic.Union(listMarkStatisticFemale).Union(listMarkStatisticEthnic).Union(listMarkStatisticFE)
                .OrderBy(p => p.OrderCritea).ThenBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return listMarkStatistic;
        }

        private List<MarkStatisticBO> GetListDataSemesterMarkForTertiary(Dictionary<string, object> dic, StatisticsForUnitBO statisticsForUnit, IQueryable<ClassSubject> listClassSubject
            , AcademicYear Aca, IQueryable<StatisticUnitBO> listMarkxAll, int orderCritea)
        {
            List<MarkStatisticBO> listMarkStatistic = new List<MarkStatisticBO>();
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dic).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
            var listMarkx = (from u in listMarkxAll
                             where listClassSubject.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)
                             && lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);
            var listMark = listMarkx.ToList();
            var listUA = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            var listOA = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);

            var listMarkGroup = listMark.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester }).Distinct();
            foreach (var mg in listMarkGroup)
            {
                MarkStatisticBO ms = new MarkStatisticBO();
                ms.SubjectID = mg.SubjectID;
                ms.EducationLevelID = (int)mg.EducationLevelID;
                ms.Semester = mg.Semester.Value;
                ms.AcademicYearID = statisticsForUnit.AcademicYearID;
                ms.SchoolID = statisticsForUnit.SchoolID;
                ms.Year = Aca.Year;
                ms.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                int belowAverage = 0;
                foreach (var ua in listUA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    belowAverage += count;
                    typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.BelowAverage = (int)belowAverage;

                int onAverage = 0;
                foreach (var ua in listOA)
                {
                    int count = listMark.Count(u => u.SubjectID == ms.SubjectID && u.EducationLevelID == ms.EducationLevelID && u.Semester == mg.Semester && ua.MinValue <= u.Mark && u.Mark < ua.MaxValue);
                    onAverage += count;
                    typeof(MarkStatisticBO).GetProperty(ua.MappedColumn).SetValue(ms, (int?)count, null);
                }
                ms.OnAverage = (int)onAverage;

                ms.MarkTotal = (int)(ms.BelowAverage.Value + ms.OnAverage.Value);

                ms.ReportCode = SystemParamsInFile.THONGKEDIEMKIEMTRAHOCKYCAP3;
                ms.ProcessedDate = DateTime.Now;
                if (orderCritea == 2)
                {
                    ms.Critea = "HS nữ";
                    ms.OrderCritea = orderCritea;
                }
                else if (orderCritea == 3)
                {
                    ms.Critea = "HS DT";
                    ms.OrderCritea = orderCritea;
                }
                else if (orderCritea == 4)
                {
                    ms.Critea = "HS nữ DT";
                    ms.OrderCritea = orderCritea;
                }
                else if (orderCritea == 1)
                {
                    ms.Critea = "Tất cả";
                    ms.OrderCritea = orderCritea;
                }
                listMarkStatistic.Add(ms);
            }
            return listMarkStatistic;
        }

        public List<CapacityStatisticsBO> SearchSubjectCapacityForTertiary(StatisticsForUnitBO statisticsForUnit)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<CapacityStatisticsBO> retVal = new List<CapacityStatisticsBO>();

            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(statisticsForUnit.SchoolID, new Dictionary<string, object>() { 
                    { "AcademicYearID", statisticsForUnit.AcademicYearID },
                    { "AppliedLevel", statisticsForUnit.AppliedLevel } ,
                    { "EducationLevelID", statisticsForUnit.EducationLevelID },
                    { "Semester", statisticsForUnit.Semester }
            });

            IQueryable<VSummedUpRecordBO> listMarkxAll = (from mr in VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.Semester == statisticsForUnit.Semester
                                                    && !u.PeriodID.HasValue)
                                                          join pp in PupilProfileBusiness.AllNoTracking
                                                          on mr.PupilID equals pp.PupilProfileID
                                                          join cs in ClassSubjectBusiness.AllNoTracking on new { mr.ClassID, mr.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                          join cp in ClassProfileBusiness.AllNoTracking
                                                          on mr.ClassID equals cp.ClassProfileID
                                                          where
                                                          cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY
                                                          && (cp.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0)
                                                          && (pp.IsActive)
                                                          && cp.IsActive==true
                                                          select new VSummedUpRecordBO
                                                          {
                                                              PupilID = mr.PupilID,
                                                              ClassID = mr.ClassID,
                                                              SubjectID = mr.SubjectID,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Semester = mr.Semester,
                                                              IsCommenting = mr.IsCommenting,
                                                              SummedUpMark = mr.SummedUpMark,
                                                              JudgementResult = mr.JudgementResult,
                                                              AppliedType = cs.AppliedType,
                                                              subjectVNEN = cs.IsSubjectVNEN
                                                          });
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                // Ca nam
                // Lay them ca du lieu thi lai
                IQueryable<VSummedUpRecord> listMarkxRetest = (VSummedUpRecordBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                                    && u.SchoolID == statisticsForUnit.SchoolID
                                                    && u.AcademicYearID == statisticsForUnit.AcademicYearID
                                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                                    && !u.PeriodID.HasValue));
                listMarkxAll = (from u in listMarkxAll
                               join r in listMarkxRetest on new { u.PupilID, u.SubjectID }
                               equals new { r.PupilID, r.SubjectID } into i
                               from g in i.DefaultIfEmpty()
                               select new VSummedUpRecordBO
                               {
                                   PupilID = u.PupilID,
                                   ClassID = u.ClassID,
                                   SubjectID = u.SubjectID,
                                   EducationLevelID = u.EducationLevelID,
                                   Semester = u.Semester,
                                   IsCommenting = u.IsCommenting,
                                   SummedUpMark = (g.ReTestMark.HasValue ? g.ReTestMark : u.SummedUpMark),
                                   JudgementResult = (g.ReTestJudgement != null ? g.ReTestJudgement : u.JudgementResult),
                                   AppliedType = u.AppliedType,
                                   subjectVNEN = u.subjectVNEN
                               });
            }

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",statisticsForUnit.SchoolID},
                {"AcademicYearID",statisticsForUnit.AcademicYearID},
                {"SemesterID",statisticsForUnit.Semester},
                {"YearID",Aca.Year}
            };
            List<RegisterSubjectSpecializeBO> lstRegis = new List<RegisterSubjectSpecializeBO>();
            lstRegis = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);

            var listM = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            var listJ = StatisticsConfigBusiness.GetList(statisticsForUnit.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
                       
            #region get tin du lieu tat ca
            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel},
               // {"FemaleID",statisticsForUnit.FemaleType},
               // {"EthnicID",statisticsForUnit.EthnicType},
                {"EducationLevelID",statisticsForUnit.EducationLevelID}
            };
            retVal = this.GetListSubjectCapacityForTertiary(statisticsForUnit, dic, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 1, listM, listJ);
            #endregion

            #region get du lieu nu
            List<CapacityStatisticsBO> listFemale = new List<CapacityStatisticsBO>();
            if (statisticsForUnit.FemaleType > 0)
            {
                Dictionary<string, object> dic1 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"FemaleID",statisticsForUnit.FemaleType},
                   // {"EthnicID",statisticsForUnit.EthnicType},
                    {"EducationLevelID",statisticsForUnit.EducationLevelID}
                };
                listFemale = this.GetListSubjectCapacityForTertiary(statisticsForUnit, dic1, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 2, listM, listJ);
            }
            #endregion
            #region get du lieu dan toc
            List<CapacityStatisticsBO> listEthnic = new List<CapacityStatisticsBO>();
            if (statisticsForUnit.EthnicType > 0)
            {
                Dictionary<string, object> dic1 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                   // {"FemaleID",statisticsForUnit.FemaleType},
                    {"EthnicID",statisticsForUnit.EthnicType},
                    {"EducationLevelID",statisticsForUnit.EducationLevelID}
                };
                listEthnic = this.GetListSubjectCapacityForTertiary(statisticsForUnit, dic1, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 3, listM, listJ);
            }
            #endregion
            #region get du lieu nu dan toc
            List<CapacityStatisticsBO> listFE = new List<CapacityStatisticsBO>();
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                Dictionary<string, object> dic1 = new Dictionary<string, object> 
                {
                    {"AcademicYearID", statisticsForUnit.AcademicYearID},
                    {"SchoolID", statisticsForUnit.SchoolID},
                    {"Semester", statisticsForUnit.Semester},
                    {"AppliedLevel", statisticsForUnit.AppliedLevel},
                    {"FemaleID",statisticsForUnit.FemaleType},
                    {"EthnicID",statisticsForUnit.EthnicType},
                    {"EducationLevelID",statisticsForUnit.EducationLevelID}
                };
                listFE = this.GetListSubjectCapacityForTertiary(statisticsForUnit, dic1, Aca, dicRegis, listMarkxAll, lstRegis, listClassSubject, 4, listM, listJ);
            }
            #endregion

            retVal = retVal.Union(listFemale).Union(listEthnic).Union(listFE).OrderBy(u => u.OrderCritea).ThenBy(u => u.EducationLevelID).ThenBy(u => u.SubjectID).ToList();
            return retVal;
        }

        private List<CapacityStatisticsBO> GetListSubjectCapacityForTertiary(StatisticsForUnitBO statisticsForUnit, Dictionary<string, object> dic, AcademicYear Aca,
            IDictionary<string, object> dicRegis, IQueryable<VSummedUpRecordBO> listMarkxAll, List<RegisterSubjectSpecializeBO> lstRegis, IQueryable<ClassSubject> listClassSubject,
            int orderCritea, List<StatisticsConfig> listM, List<StatisticsConfig> listJ)
        {
            List<CapacityStatisticsBO> retVal = new List<CapacityStatisticsBO>();
            IQueryable<PupilOfClassBO> lstQPoc = (from p in PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dic).AddCriteriaSemester(Aca, statisticsForUnit.Semester)
                                                  join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      SubjectID = cs.SubjectID,
                                                      AppliedType = cs.AppliedType,
                                                      EducationLevelID = cs.ClassProfile.EducationLevelID
                                                  });

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMarkx = (from u in listMarkxAll.ToList()
                             join poc in lstPOC on new { u.PupilID, u.ClassID, u.SubjectID } equals new { poc.PupilID, poc.ClassID, poc.SubjectID }
                             //where listClassSubject.Any(p=>p.ClassID == u.ClassID && p.SubjectID == u.SubjectID)
                             //&& lstPOC.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                             select u);

            var listMarkGroup = listMarkx.Select(u => new { u.SubjectID, u.EducationLevelID, u.Semester, u.IsCommenting }).Distinct();
            var listMarkGroupNotSemester = listMarkx.Select(u => new { u.SubjectID, u.EducationLevelID, u.IsCommenting }).Distinct();
            List<PupilSubjectEducationLevel> listPupilSubject = this.CountPupilBySubject(dic, listClassSubject, lstRegis);

            foreach (var mg in listMarkGroupNotSemester)
            {
                CapacityStatisticsBO cs = new CapacityStatisticsBO();
                cs.SubjectID = mg.SubjectID;
                cs.EducationLevelID = (int)mg.EducationLevelID;
                // cs.Semester = mg.Semester;
                cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                cs.SchoolID = statisticsForUnit.SchoolID;
                cs.Year = Aca.Year;
                cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                cs.IsCommenting = mg.IsCommenting;

                if (mg.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || mg.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                {
                    int belowAverage = 0;
                    int aboveAverage = 0;
                    foreach (StatisticsConfig sc in listM)
                    {
                        int count = listMarkx.Count(u => u.SummedUpMark != null && u.SubjectID == cs.SubjectID && u.EducationLevelID == cs.EducationLevelID
                                                            && ((statisticsForUnit.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && (u.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || u.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) || u.Semester == statisticsForUnit.Semester)
                                                            && (u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                                            && sc.MinValue <= u.SummedUpMark.Value && u.SummedUpMark.Value < sc.MaxValue);
                        typeof(CapacityStatistic).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);
                        belowAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE ? count : 0;
                        aboveAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE ? count : 0;
                    }
                    cs.BelowAverage = (int)belowAverage;
                    cs.OnAverage = (int)aboveAverage;
                }

                if (mg.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    foreach (StatisticsConfig sc in listJ)
                    {
                        int count = listMarkx.Count(u => u.SubjectID == cs.SubjectID && u.EducationLevelID == cs.EducationLevelID
                                                            && (u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                                            && ((statisticsForUnit.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && (u.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || u.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) || u.Semester == statisticsForUnit.Semester)
                                                            && u.JudgementResult == sc.EqualValue);
                        typeof(CapacityStatistic).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);
                    }
                }
                // Tong so hoc sinh
                int totalPupil = 0;
                PupilSubjectEducationLevel pupilSubjectEducationLevel = listPupilSubject.Where(o => o.EducationLevelID == mg.EducationLevelID
                    && o.SubjectID == mg.SubjectID).FirstOrDefault();
                if (pupilSubjectEducationLevel != null)
                {
                    totalPupil = pupilSubjectEducationLevel.TotalPupil;
                }
                cs.PupilTotal = totalPupil;
                cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCMONCAP2;
                cs.ProcessedDate = DateTime.Now;
                cs.OrderCritea = orderCritea;

                if (orderCritea == 1)
                    cs.Critea = "Tất cả";
                else if (orderCritea == 2)
                    cs.Critea = "HS nữ";
                else if (orderCritea == 3)
                    cs.Critea = "HS DT";
                else if (orderCritea == 4)
                    cs.Critea = "HS nữ DT";

                retVal.Add(cs);
            }
            return retVal;
        }

        public List<ConductStatisticsBO> SearchConductForTertiary(StatisticsForUnitBO statisticsForUnit)
        {
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<ConductStatisticsBO> retVal = new List<ConductStatisticsBO>();
            var listMarkxFirst = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester == statisticsForUnit.Semester)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                 join pp in PupilProfileBusiness.AllNoTracking
                                 on pr.PupilID equals pp.PupilProfileID
                                 join cp in ClassProfileBusiness.AllNoTracking
                                 on pr.ClassID equals cp.ClassProfileID
                                 where
                                 cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY

                                 && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                 && (pp.IsActive)
                                 && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                 && cp.IsActive==true
                                 select new
                                 {
                                     pr.PupilID,
                                     pr.ClassID,
                                     cp.SubCommitteeID,
                                     pr.EducationLevelID,
                                     pr.ConductLevelID,
                                     pr.Semester,
                                     pp.EthnicID,
                                     pp.Genre
                                 };
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                // Neu la ca nam thi lay them thong tin thi lai
                var listMarkRetest = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                     join pp in PupilProfileBusiness.AllNoTracking
                                      on pr.PupilID equals pp.PupilProfileID
                                     join cp in ClassProfileBusiness.AllNoTracking
                                     on pr.ClassID equals cp.ClassProfileID
                                     where
                 cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY

                                     && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                     && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                     && cp.IsActive==true
                                     select new
                                     {
                                         pr.PupilID,
                                         pr.ClassID,
                                         cp.SubCommitteeID,
                                         pr.EducationLevelID,
                                         pr.ConductLevelID,
                                         pr.Semester,
                                         pp.EthnicID,
                                         pp.Genre
                                     };

                // join voi thong tin chua thi lai
                listMarkxFirst = from u in listMarkxFirst
                                 join r in listMarkRetest on new { u.PupilID, u.ClassID }
                                 equals new { r.PupilID, r.ClassID } into i
                                 from g in i.DefaultIfEmpty()
                                 select new
                                 {
                                     u.PupilID,
                                     u.ClassID,
                                     u.SubCommitteeID,
                                     u.EducationLevelID,
                                     ConductLevelID = g.ConductLevelID.HasValue ? g.ConductLevelID : u.ConductLevelID,
                                     u.Semester,
                                     u.EthnicID,
                                     u.Genre
                                 };
            }
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                //{"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).Where(p=>(!p.ClassProfile.IsVnenClass.HasValue || (p.ClassProfile.IsVnenClass.HasValue && p.ClassProfile.IsVnenClass.Value == false))).AddCriteriaSemester(Aca, statisticsForUnit.Semester);
            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky bao cao
            var listMark = (from u in listMarkxFirst
                            where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();
            List<StatisticsConfig> listSConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);

            var listGroupAll = (from lm in listMark
                                group lm by new
                                {
                                    lm.EducationLevelID,
                                    lm.Semester
                                } into g
                                select new ListGroup
                                {
                                    EducationLevelID = g.Key.EducationLevelID,
                                    SemesterID = g.Key.Semester
                                }).Distinct().ToList();

            int totalPupil = 0;
            int countFor = 1 + (statisticsForUnit.FemaleEthnicType > 0 ? 1 : 0) + (statisticsForUnit.EthnicType > 0 ? 1 : 0) + (statisticsForUnit.FemaleType > 0 ? 1 : 0);
            int tmpEFType = statisticsForUnit.FemaleEthnicType;
            int tmpEthnicType = statisticsForUnit.EthnicType;
            int tmpFemaleType = statisticsForUnit.FemaleType;

            foreach (var mg in listGroupAll)
            {
                tmpEFType = statisticsForUnit.FemaleEthnicType;
                tmpEthnicType = statisticsForUnit.EthnicType;
                tmpFemaleType = statisticsForUnit.FemaleType;
                for (int i = 0; i < countFor; i++)
                {
                    ConductStatisticsBO cs = new ConductStatisticsBO();
                    cs.EducationLevelID = (int)mg.EducationLevelID;
                    cs.Semester = mg.SemesterID;
                    cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                    cs.SchoolID = statisticsForUnit.SchoolID;
                    cs.Year = Aca.Year;
                    cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                    mg.Genre = null;
                    mg.EthnicID = null;
                    mg.OrderCretia = 1;
                    if (tmpEFType > 0)
                    {
                        mg.Genre = 1;
                        mg.EthnicID = 1;
                        tmpEFType = 0;
                        mg.OrderCretia = 4;
                    }
                    else if (tmpFemaleType > 0)
                    {
                        mg.Genre = 1;
                        tmpFemaleType = 0;
                        mg.OrderCretia = 2;
                    }
                    else if (tmpEthnicType > 0)
                    {
                        mg.EthnicID = 1;
                        tmpEthnicType = 0;
                        mg.OrderCretia = 3;
                    }

                    foreach (StatisticsConfig sc in listSConfig)
                    {
                        int count = listMark.Count(u => u.EducationLevelID == cs.EducationLevelID
                                                        && u.Semester == mg.SemesterID
                                                        && u.ConductLevelID.ToString().Equals(sc.EqualValue)
                                                        && ((mg.OrderCretia == 4 && mg.Genre.HasValue && mg.EthnicID.HasValue && u.Genre == 0 && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (mg.Genre.HasValue && u.Genre == 0 && mg.OrderCretia == 2)
                                                            || (mg.OrderCretia == 3 && mg.EthnicID.HasValue && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (!mg.Genre.HasValue && !mg.EthnicID.HasValue)));
                        typeof(ConductStatistic).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);
                    }
                    if (mg.Genre.HasValue && mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0
                                               && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else if (mg.Genre.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0).Count();
                    }
                    else if (mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID
                                                    && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID).Count();
                    }

                    cs.PupilTotal = totalPupil;
                    cs.ReportCode = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
                    cs.ProcessedDate = DateTime.Now;
                    cs.GenreType = mg.Genre;
                    cs.EthnicType = mg.EthnicID;
                    cs.OrderCretia = mg.OrderCretia;
                    retVal.Add(cs);
                }
            }

            retVal = retVal.OrderBy(p=>p.OrderCretia).ThenBy(u => u.EducationLevelID).ToList();

            return retVal;
        }

        public List<CapacityStatisticsBO> SearchCapacityForTertiary(StatisticsForUnitBO statisticsForUnit)
        {
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            List<CapacityStatisticsBO> retVal = new List<CapacityStatisticsBO>();

            var listMarkxFirst = (from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester == statisticsForUnit.Semester)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                  join pp in PupilProfileBusiness.AllNoTracking
                                     on pr.PupilID equals pp.PupilProfileID
                                  join cp in ClassProfileBusiness.AllNoTracking
                                     on pr.ClassID equals cp.ClassProfileID
                                  where cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY

                                       && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                       && (pp.IsActive)
                                       && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                       && cp.IsActive==true
                                  select new
                                  {
                                      pr.PupilID,
                                      pr.ClassID,
                                      cp.SubCommitteeID,
                                      pr.EducationLevelID,
                                      pr.CapacityLevelID,
                                      pr.Semester,
                                      pp.EthnicID,
                                      pp.Genre
                                  });
            if (statisticsForUnit.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                // Neu la ca nam thi lay them thong tin thi lai
                var listMarkRetest = from pr in VPupilRankingBusiness.AllNoTracking.Where(u => u.AcademicYearID == statisticsForUnit.AcademicYearID
                                    && u.Last2digitNumberSchool == (statisticsForUnit.SchoolID % 100)
                                    && u.CreatedAcademicYear == Aca.Year
                                    && u.SchoolID == statisticsForUnit.SchoolID
                                    && (u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                    && !u.PeriodID.HasValue
                                    && (u.EducationLevelID == statisticsForUnit.EducationLevelID || statisticsForUnit.EducationLevelID == 0))
                                     join pp in PupilProfileBusiness.AllNoTracking
                                     on pr.PupilID equals pp.PupilProfileID
                                     join cp in ClassProfileBusiness.AllNoTracking
                                     on pr.ClassID equals cp.ClassProfileID
                                     where
                                     cp.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_TERTIARY

                                     && (cp.SubCommitteeID == statisticsForUnit.SubCommitteeID || statisticsForUnit.SubCommitteeID == 0)
                                     && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                                     && cp.IsActive==true
                                     select new
                                     {
                                         pr.PupilID,
                                         pr.ClassID,
                                         cp.SubCommitteeID,
                                         pr.EducationLevelID,
                                         pr.CapacityLevelID,
                                         pr.Semester,
                                         pp.EthnicID,
                                         pp.Genre
                                     };

                // join voi thong tin chua thi lai
                listMarkxFirst = from u in listMarkxFirst
                                 join r in listMarkRetest on new { u.PupilID, u.ClassID }
                                 equals new { r.PupilID, r.ClassID } into i
                                 from g in i.DefaultIfEmpty()
                                 select new
                                 {
                                     u.PupilID,
                                     u.ClassID,
                                     u.SubCommitteeID,
                                     u.EducationLevelID,
                                     CapacityLevelID = g.CapacityLevelID.HasValue ? g.CapacityLevelID : u.CapacityLevelID,
                                     u.Semester,
                                     u.EthnicID,
                                     u.Genre
                                 };
            }
            //AcademicYear Aca = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", statisticsForUnit.AcademicYearID},
                {"SchoolID", statisticsForUnit.SchoolID},
                {"Semester", statisticsForUnit.Semester},
                {"AppliedLevel", statisticsForUnit.AppliedLevel}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(statisticsForUnit.SchoolID, dicPupilRanking).Where(p=>(!p.ClassProfile.IsVnenClass.HasValue || (p.ClassProfile.IsVnenClass.HasValue && p.ClassProfile.IsVnenClass.Value == false))).AddCriteriaSemester(Aca, statisticsForUnit.Semester);


            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var listMark = (from u in listMarkxFirst
                            where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                            select u).ToList();
            List<StatisticsConfig> listSConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            var listGroupAll = (from lm in listMark
                                group lm by new
                                {
                                    lm.EducationLevelID,
                                    lm.Semester
                                } into g
                                select new ListGroup
                                {
                                    EducationLevelID = g.Key.EducationLevelID,
                                    SemesterID = g.Key.Semester
                                }).Distinct().ToList();

            int totalPupil = 0;
            int countFor = 1 + (statisticsForUnit.FemaleEthnicType > 0 ? 1 : 0) + (statisticsForUnit.EthnicType > 0 ? 1 : 0) + (statisticsForUnit.FemaleType > 0 ? 1 : 0);
            int tmpEFType = statisticsForUnit.FemaleEthnicType;
            int tmpEthnicType = statisticsForUnit.EthnicType;
            int tmpFemaleType = statisticsForUnit.FemaleType;

            foreach (var mg in listGroupAll)
            {
                tmpEFType = statisticsForUnit.FemaleEthnicType;
                tmpEthnicType = statisticsForUnit.EthnicType;
                tmpFemaleType = statisticsForUnit.FemaleType;
                for (int i = 0; i < countFor; i++)
                {
                    CapacityStatisticsBO cs = new CapacityStatisticsBO();
                    cs.EducationLevelID = (int)mg.EducationLevelID;
                    cs.Semester = mg.SemesterID;
                    cs.AcademicYearID = statisticsForUnit.AcademicYearID;
                    cs.SchoolID = statisticsForUnit.SchoolID;
                    cs.Year = Aca.Year;
                    cs.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
                    int belowAverage = 0;
                    int aboveAverage = 0;

                    mg.Genre = null;
                    mg.EthnicID = null;
                    mg.OrderCretia = 1;
                    if (tmpEFType > 0)
                    {
                        mg.Genre = 1;
                        mg.EthnicID = 1;
                        tmpEFType = 0;
                        mg.OrderCretia = 4;
                    }
                    else if (tmpFemaleType > 0)
                    {
                        mg.Genre = 1;
                        tmpFemaleType = 0;
                        mg.OrderCretia = 2;
                    }
                    else if (tmpEthnicType > 0)
                    {
                        mg.EthnicID = 1;
                        tmpEthnicType = 0;
                        mg.OrderCretia = 3;
                    }

                    foreach (StatisticsConfig sc in listSConfig)
                    {
                        int count = listMark.Count(u => u.EducationLevelID == cs.EducationLevelID
                                                        && u.Semester == mg.SemesterID
                                                        && u.CapacityLevelID.ToString().Equals(sc.EqualValue)
                                                        && ((mg.OrderCretia == 4 && mg.Genre.HasValue && mg.EthnicID.HasValue && u.Genre == 0 && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (mg.Genre.HasValue && u.Genre == 0 && mg.OrderCretia == 2)
                                                            || (mg.OrderCretia == 3 && mg.EthnicID.HasValue && u.EthnicID.HasValue && u.EthnicID != Ethnic_Kinh.EthnicID && u.EthnicID != Ethnic_ForeignPeople.EthnicID)
                                                            || (!mg.Genre.HasValue && !mg.EthnicID.HasValue)));
                        typeof(CapacityStatistic).GetProperty(sc.MappedColumn).SetValue(cs, (int?)count, null);

                        belowAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE ? count : 0;
                        aboveAverage += sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE ? count : 0;
                    }
                    cs.BelowAverage = (int)belowAverage;
                    cs.OnAverage = (int)aboveAverage;
                    if (mg.Genre.HasValue && mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0
                                               && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else if (mg.Genre.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID && p.PupilProfile.Genre == 0).Count();
                    }
                    else if (mg.EthnicID.HasValue)
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID
                                                    && p.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && p.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID).Count();
                    }
                    else
                    {
                        totalPupil = lstQPoc.Where(p => p.ClassProfile.EducationLevelID == mg.EducationLevelID).Count();
                    }

                    cs.PupilTotal = totalPupil;
                    cs.ReportCode = SystemParamsInFile.THONGKEHOCLUCTHEOBANCAP3;
                    cs.ProcessedDate = DateTime.Now;
                    cs.Genre = mg.Genre;
                    cs.EthnicID = mg.EthnicID;

                    retVal.Add(cs);
                }
            }

            retVal = retVal.OrderBy(u => u.EducationLevelID).ToList();

            return retVal;
        }

        public Stream CreatePeriodicMarkForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THPT_THONGKE_DIEMKT_DINHKY);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            List<StatisticsConfig> listBelow = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            List<StatisticsConfig> listAbove = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            int lastColIndex = listBelow.Count + listAbove.Count + 8;

            List<MarkStatisticBO> listMarkStatistic = SearchPeriodicMarkForTertiary(statisticsForUnit);
            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<EducationLevel> listEdu = listMarkStatistic.OrderBy(u => u.EducationLevelID).Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet patternSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange markCol = tempSheet.GetRange("A1", "A4");
            IVTRange tbCol = tempSheet.GetRange("B1", "B4");

            for (int i = 0; i < listBelow.Count; i++)
            {
                patternSheet.CopyPaste(markCol, 8, i + 5, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                patternSheet.SetCellValue(8, i + 5, string.Format(format, listBelow[i].MinValue));
            }

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + 5, true);
            patternSheet.SetCellValue(8, listBelow.Count + 5, "TS dưới TB");

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + 6, true);
            patternSheet.SetCellValue(8, listBelow.Count + 6, "TL dưới TB");

            for (int i = 0; i < listAbove.Count; i++)
            {
                patternSheet.CopyPaste(markCol, 8, i + listBelow.Count + 7, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                patternSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(format, listAbove[i].MinValue));
            }

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 7, true);
            patternSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 7, "TS trên TB");

            patternSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 8, true);
            patternSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 8, "TL trên TB");

            IVTRange dataRow = patternSheet.GetRange(10, 1, 10, lastColIndex);
            for (int i = 0; i < listMarkStatistic.Count; i++)
            {
                patternSheet.CopyAndInsertARow(dataRow, 11, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                patternSheet.DeleteRow(9);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;
            string SheetName = "DiemKTDinhKy";

            #region Fill sheet binh thuong

            IVTWorksheet sheetDefault = oBook.CopySheetToLast(patternSheet);
            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;

            sheetDefault.FillVariableValue(dicVarable);
            int index = 1;
            List<MarkStatisticBO> listMarkDefault = listMarkStatistic.Where(p => p.OrderCritea == 1).ToList();
            int totalRecordItem = listMarkDefault.Count;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listMarkDefault.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                IVTRange eduRange = sheetDefault.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    sheetDefault.SetCellValue(index + 8, colIndex++, index);
                    if (!eduFilled)
                    {
                        sheetDefault.SetCellValue(index + 8, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                    foreach (StatisticsConfig sc in listBelow)
                    {
                        sheetDefault.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                    double belowPercent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                    sheetDefault.SetCellValue(index + 8, colIndex++, belowPercent.ToString().Replace(",", ".") + "%");
                    foreach (StatisticsConfig sc in listAbove)
                    {
                        sheetDefault.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    sheetDefault.SetCellValue(index + 8, colIndex++, item.OnAverage);
                    sheetDefault.SetCellValue(index + 8, colIndex++, (100.0 - belowPercent).ToString().Replace(",", ".") + "%");

                    index++;
                }
            }
            #region Xoa dong thua
            for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
            {
                sheetDefault.DeleteRow(totalRecordItem + 9);
            }
            #endregion

            #region reportName
            sheetDefault.Name = SheetName;
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            sheetDefault.FitSheetOnOnePage = true;
            #endregion
            #endregion end fill sheet binh thuong

            #region Fill sheet nu
            if (statisticsForUnit.FemaleType > 0)
            {
                IVTWorksheet sheetFemale = oBook.CopySheetToLast(patternSheet);
                dicVarable["PupilName"] = "HỌC SINH NỮ";
                sheetFemale.FillVariableValue(dicVarable);

                index = 1;
                List<MarkStatisticBO> listMarkFemale = listMarkStatistic.Where(p => p.OrderCritea == 2).ToList();
                totalRecordItem = listMarkFemale.Count;
                foreach (EducationLevel edu in listEdu)
                {
                    var listItem = listMarkFemale.Where(u => u.EducationLevelID == edu.EducationLevelID);
                    bool eduFilled = false;
                    IVTRange eduRange = sheetFemale.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                    eduRange.Merge();

                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        sheetFemale.SetCellValue(index + 8, colIndex++, index);
                        if (!eduFilled)
                        {
                            sheetFemale.SetCellValue(index + 8, colIndex++, edu.Resolution);
                            eduFilled = true;
                        }
                        else colIndex++;
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                        foreach (StatisticsConfig sc in listBelow)
                        {
                            sheetFemale.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                        double belowPercent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                        sheetFemale.SetCellValue(index + 8, colIndex++, belowPercent.ToString().Replace(",", ".") + "%");
                        foreach (StatisticsConfig sc in listAbove)
                        {
                            sheetFemale.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFemale.SetCellValue(index + 8, colIndex++, item.OnAverage);
                        sheetFemale.SetCellValue(index + 8, colIndex++, (100.0 - belowPercent).ToString().Replace(",", ".") + "%");

                        index++;
                    }
                }
                #region Xoa dong thua
                for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
                {
                    sheetFemale.DeleteRow(totalRecordItem + 9);
                }
                #endregion

                #region reportName
                sheetFemale.Name = "HS_Nu";
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                sheetFemale.FitSheetOnOnePage = true;
                #endregion
            }
            #endregion end fill hoc sinh nu

            #region fill sheet hoc sinh dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                IVTWorksheet sheetEThnic = oBook.CopySheetToLast(patternSheet);
                dicVarable["PupilName"] = "HỌC SINH DÂN TỘC";
                sheetEThnic.FillVariableValue(dicVarable);

                index = 1;
                List<MarkStatisticBO> listMarkEThnic = listMarkStatistic.Where(p => p.OrderCritea == 3).ToList();
                totalRecordItem = listMarkEThnic.Count;
                foreach (EducationLevel edu in listEdu)
                {
                    var listItem = listMarkEThnic.Where(u => u.EducationLevelID == edu.EducationLevelID);
                    bool eduFilled = false;
                    IVTRange eduRange = sheetEThnic.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                    eduRange.Merge();

                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        sheetEThnic.SetCellValue(index + 8, colIndex++, index);
                        if (!eduFilled)
                        {
                            sheetEThnic.SetCellValue(index + 8, colIndex++, edu.Resolution);
                            eduFilled = true;
                        }
                        else colIndex++;
                        sheetEThnic.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                        sheetEThnic.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                        foreach (StatisticsConfig sc in listBelow)
                        {
                            sheetEThnic.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetEThnic.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                        double belowPercent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                        sheetEThnic.SetCellValue(index + 8, colIndex++, belowPercent.ToString().Replace(",", ".") + "%");
                        foreach (StatisticsConfig sc in listAbove)
                        {
                            sheetEThnic.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetEThnic.SetCellValue(index + 8, colIndex++, item.OnAverage);
                        sheetEThnic.SetCellValue(index + 8, colIndex++, (100.0 - belowPercent).ToString().Replace(",", ".") + "%");

                        index++;
                    }
                }
                #region Xoa dong thua
                for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
                {
                    sheetEThnic.DeleteRow(totalRecordItem + 9);
                }
                #endregion

                #region reportName
                sheetEThnic.Name = "HS_DT";
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                sheetEThnic.FitSheetOnOnePage = true;
                #endregion
            }

            #endregion

            #region Fill sheet hoc sinh nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                IVTWorksheet sheetFE = oBook.CopySheetToLast(patternSheet);
                dicVarable["PupilName"] = "HỌC SINH NỮ DÂN TỘC";
                sheetFE.FillVariableValue(dicVarable);

                index = 1;
                List<MarkStatisticBO> listMarkFE = listMarkStatistic.Where(p => p.OrderCritea == 4).ToList();
                totalRecordItem = listMarkFE.Count;
                foreach (EducationLevel edu in listEdu)
                {
                    var listItem = listMarkFE.Where(u => u.EducationLevelID == edu.EducationLevelID);
                    bool eduFilled = false;
                    IVTRange eduRange = sheetFE.GetRange(index + 8, 2, index + 8 + listItem.Count() - 1, 2);
                    eduRange.Merge();

                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        sheetFE.SetCellValue(index + 8, colIndex++, index);
                        if (!eduFilled)
                        {
                            sheetFE.SetCellValue(index + 8, colIndex++, edu.Resolution);
                            eduFilled = true;
                        }
                        else colIndex++;
                        sheetFE.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                        sheetFE.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                        foreach (StatisticsConfig sc in listBelow)
                        {
                            sheetFE.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFE.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                        double belowPercent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round((100.0 * item.BelowAverage.Value / item.MarkTotal.Value), 1, MidpointRounding.AwayFromZero) : 0;
                        sheetFE.SetCellValue(index + 8, colIndex++, belowPercent.ToString().Replace(",", ".") + "%");
                        foreach (StatisticsConfig sc in listAbove)
                        {
                            sheetFE.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                        }
                        sheetFE.SetCellValue(index + 8, colIndex++, item.OnAverage);
                        sheetFE.SetCellValue(index + 8, colIndex++, (100.0 - belowPercent).ToString().Replace(",", ".") + "%");

                        index++;
                    }
                }
                #region Xoa dong thua
                for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
                {
                    sheetFE.DeleteRow(totalRecordItem + 9);
                }
                #endregion

                #region reportName
                sheetFE.Name = "HS_Nu_DT";
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                sheetFE.FitSheetOnOnePage = true;
                #endregion
            }
            #endregion

            patternSheet.Delete();
            tempSheet.Delete();
            #endregion
            return oBook.ToStream();
        }

        public Stream CreateSemesterMarkForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THPT_THONGKE_DIEMKIEMTRA_HOCKY);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);


            List<StatisticsConfig> listBelow = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_BELOW_AVERAGE);
            List<StatisticsConfig> listAbove = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE);
            int lastColIndex = listBelow.Count + listAbove.Count + 8;

            List<MarkStatisticBO> listMarkStatistic = SearchSemesterMarkForTertiary(statisticsForUnit);
            listMarkStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listMarkStatistic = listMarkStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<EducationLevel> listEdu = listMarkStatistic.OrderBy(u => u.EducationLevelID).Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet parttenSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange markCol = tempSheet.GetRange("A1", "A4");
            IVTRange tbCol = tempSheet.GetRange("B1", "B4");

            for (int i = 0; i < listBelow.Count; i++)
            {
                parttenSheet.CopyPaste(markCol, 8, i + 5, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                parttenSheet.SetCellValue(8, i + 5, string.Format(format, listBelow[i].MinValue));
            }

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + 5, true);
            parttenSheet.SetCellValue(8, listBelow.Count + 5, "TS dưới TB");

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + 6, true);
            parttenSheet.SetCellValue(8, listBelow.Count + 6, "TL dưới TB");

            for (int i = 0; i < listAbove.Count; i++)
            {
                parttenSheet.CopyPaste(markCol, 8, i + listBelow.Count + 7, true);
                string format = listAbove[i].MinValue < 10.0m ? "{0:0.0}" : "{0:0}"; // Neu so be hon muoi thi xuat ra mot so sau dau phay. Neu bang 10 thi xuat ra so 10
                parttenSheet.SetCellValue(8, i + listBelow.Count + 7, string.Format(format, listAbove[i].MinValue));
            }

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 7, true);
            parttenSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 7, "TS trên TB");

            parttenSheet.CopyPaste(tbCol, 8, listBelow.Count + listAbove.Count + 8, true);
            parttenSheet.SetCellValue(8, listBelow.Count + listAbove.Count + 8, "TL trên TB");

            IVTRange dataRow = parttenSheet.GetRange(10, 1, 10, lastColIndex);
            for (int i = 0; i < listMarkStatistic.Count; i++)
            {
                parttenSheet.CopyAndInsertARow(dataRow, 11, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                parttenSheet.DeleteRow(9);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);

            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);

            #region Fill sheet dau tien
            this.FillSheetCreateSemesterMarkForTertiaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 1, eduLevel, reportName, reportDefinition);
            #endregion
            #region  Fill sheet nu
            if (statisticsForUnit.FemaleType > 0)
            {
                this.FillSheetCreateSemesterMarkForTertiaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 2, eduLevel, reportName, reportDefinition);
            }
            #endregion

            #region  Fill sheet dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                this.FillSheetCreateSemesterMarkForTertiaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 3, eduLevel, reportName, reportDefinition);
            }
            #endregion

            #region  Fill sheet nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                this.FillSheetCreateSemesterMarkForTertiaryReport(oBook, parttenSheet, dicVarable, listMarkStatistic, listEdu, listBelow, listAbove, statisticsForUnit, 4, eduLevel, reportName, reportDefinition);
            }
            #endregion

            parttenSheet.Delete();
            tempSheet.Delete();
            #endregion
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            tempSheet.FitToPage = true;
            return oBook.ToStream();
        }

        private void FillSheetCreateSemesterMarkForTertiaryReport(IVTWorkbook oBook, IVTWorksheet parttenSheet, Dictionary<string, object> dicVarable,
            List<MarkStatisticBO> listMarkStatistic, List<EducationLevel> listEdu, List<StatisticsConfig> listBelow, List<StatisticsConfig> listAbove,
            StatisticsForUnitBO statisticsForUnit, int orderCretia, EducationLevel eduLevel, string reportName, ReportDefinition reportDefinition)
        {
            IVTWorksheet dataSheet = oBook.CopySheetToLast(parttenSheet);
            string SheetName = string.Empty;
            string PupilName = string.Empty;
            if (orderCretia == 2)
            {
                SheetName = "HS_Nu";
                PupilName = "HỌC SINH NỮ";
            }
            else if (orderCretia == 3)
            {
                SheetName = "HS_DT";
                PupilName = "HỌC SINH DÂN TỘC";
            }
            else if (orderCretia == 4)
            {
                SheetName = "HS_Nu_DT";
                PupilName = "HỌC SINH NỮ DÂN TỘC";
            }
            else if (orderCretia == 1)
            {
                SheetName = "DiemKTHK";
            }
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            int countItem = 0;
            int countListItem = 0;
            List<MarkStatisticBO> listMarkStatisticDefault = listMarkStatistic.Where(p => p.OrderCritea == orderCretia).ToList();
            int totalRecordItem = listMarkStatisticDefault.Count;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listMarkStatisticDefault.Where(u => u.EducationLevelID == edu.EducationLevelID);
                bool eduFilled = false;
                countListItem = listItem.Count();
                countItem = countListItem > 0 ? countListItem : 1;
                IVTRange eduRange = dataSheet.GetRange(index + 8, 2, index + 8 + countItem - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 8, colIndex++, index);
                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;
                    dataSheet.SetCellValue(index + 8, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.SetCellValue(index + 8, colIndex++, item.MarkTotal);

                    foreach (StatisticsConfig sc in listBelow)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    dataSheet.SetCellValue(index + 8, colIndex++, item.BelowAverage);
                    decimal belowPercent = item.MarkTotal > 0 && item.BelowAverage.HasValue ? Math.Round(100m * item.BelowAverage.Value / item.MarkTotal.Value,1,MidpointRounding.AwayFromZero) : 0;
                    dataSheet.SetCellValue(index + 8, colIndex++, belowPercent.ToString().Replace(",", ".") + "%");
                    foreach (StatisticsConfig sc in listAbove)
                    {
                        dataSheet.SetCellValue(index + 8, colIndex++, typeof(MarkStatisticBO).GetProperty(sc.MappedColumn).GetValue(item, null));
                    }
                    dataSheet.SetCellValue(index + 8, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 8, colIndex++, (100m - belowPercent).ToString().Replace(",", ".") + "%");

                    index++;
                }
            }
            #region Xoa dong thua
            for (int i = totalRecordItem; i < listMarkStatistic.Count; i++)
            {
                dataSheet.DeleteRow(totalRecordItem + 9);
            }
            #endregion

            #region reportName
            dataSheet.Name = SheetName;            
            #endregion
            dataSheet.FitAllColumnsOnOnePage = true;
        }


        public Stream CreateSubjectCapacityForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THPT_THONGKE_DIEMTBM);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listJudge = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            List<StatisticsConfig> listMark = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON, SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL, SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            int lastColIndex = (listJudge.Count + listMark.Count) * 2 + 6;

            List<CapacityStatisticsBO> listCapacityStatistic = SearchSubjectCapacityForTertiary(statisticsForUnit);
            listCapacityStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubjectCat = SubjectCatBusiness.Find(ms.SubjectID);
            });

            // Sap xep cho dung thu tu theo mon hoc
            listCapacityStatistic = listCapacityStatistic.OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<EducationLevel> listEdu = listCapacityStatistic.OrderBy(u => u.EducationLevelID).Select(u => u.EducationLevel).Distinct().ToList();

            IVTWorksheet pattarnSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            #region tao khung
            IVTRange tempCol = tempSheet.GetRange("A1", "B5");

            for (int i = 0; i < listMark.Count; i++)
            {
                pattarnSheet.CopyPaste(tempCol, 8, i * 2 + 5);
                pattarnSheet.SetCellValue(8, i * 2 + 5, listMark[i].DisplayName);
            }

            for (int i = 0; i < listJudge.Count; i++)
            {
                pattarnSheet.CopyPaste(tempCol, 8, i * 2 + listMark.Count * 2 + 5);
                pattarnSheet.SetCellValue(8, i * 2 + listMark.Count * 2 + 5, listJudge[i].DisplayName);
            }
            pattarnSheet.CopyPaste(tempCol, 8, (listJudge.Count + listMark.Count) * 2 + 5);
            pattarnSheet.SetCellValue(8, (listJudge.Count + listMark.Count) * 2 + 5, "Trên TB");

            IVTRange dataRow = pattarnSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < listCapacityStatistic.Count; i++)
            {
                pattarnSheet.CopyAndInsertARow(dataRow, 12, true);
            }

            // Xoa 3 dong thua
            for (int i = 0; i < 3; i++)
            {
                pattarnSheet.DeleteRow(10);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);


            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            
            #region Sheet dau tien
            this.FillCreateSubjectCapacityForTertiaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 1);
            #endregion

            #region sheet nu
            if (statisticsForUnit.FemaleType > 0)
            {
                this.FillCreateSubjectCapacityForTertiaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 2);
            }
            #endregion

            #region sheet dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                this.FillCreateSubjectCapacityForTertiaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 3);
            }
            #endregion

            #region sheet nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                this.FillCreateSubjectCapacityForTertiaryReport(oBook, pattarnSheet, listCapacityStatistic, listEdu, listMark, listJudge, dicVarable, 4);
            }
            #endregion

            pattarnSheet.Delete();
            tempSheet.Delete();
            #endregion

            #region reportName            
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            
            tempSheet.FitToPage = true;
            return oBook.ToStream();
        }

        public void FillCreateSubjectCapacityForTertiaryReport(IVTWorkbook oBook, IVTWorksheet pattarnSheet, List<CapacityStatisticsBO> listCapacityStatistic
            , List<EducationLevel> listEdu, List<StatisticsConfig> listMark, List<StatisticsConfig> listJudge
            , Dictionary<string, object> dicVarable, int orderCritea)
        {
            IVTWorksheet dataSheet = oBook.CopySheetToLast(pattarnSheet);
            string SheetName = string.Empty;
            string PupilName = string.Empty;
            if (orderCritea == 2)
            {
                SheetName = "HS_Nu";
                PupilName = "HỌC SINH NỮ";
            }
            else if (orderCritea == 3)
            {
                SheetName = "HS_DT";
                PupilName = "HỌC SINH DÂN TỘC";
            }
            else if (orderCritea == 4)
            {
                SheetName = "HS_Nu_DT";
                PupilName = "HỌC SINH NỮ DÂN TỘC";
            }
            else if (orderCritea == 1)
            {
                SheetName = "ThongKeDiemTBM";
            }
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);
            int index = 1;
            int totalItem = 0;
            int totallistItem = 0;
            List<CapacityStatisticsBO> listByOrderCritea = listCapacityStatistic.Where(p => p.OrderCritea == orderCritea).ToList();
            int totalRecordList = listByOrderCritea.Count;
            foreach (EducationLevel edu in listEdu)
            {
                var listItem = listByOrderCritea.Where(u => u.EducationLevelID == edu.EducationLevelID);
                totallistItem = listItem.Count();
                totalItem = totallistItem > 0 ? totallistItem : 1;
                bool eduFilled = false;

                IVTRange eduRange = dataSheet.GetRange(index + 9, 2, index + 9 + totalItem - 1, 2);
                eduRange.Merge();

                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 9, colIndex++, index);

                    if (!eduFilled)
                    {
                        dataSheet.SetCellValue(index + 9, colIndex++, edu.Resolution);
                        eduFilled = true;
                    }
                    else colIndex++;

                    dataSheet.SetCellValue(index + 9, colIndex++, item.SubjectCat.SubjectName);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountMark = new List<int?>();
                    foreach (StatisticsConfig sc in listMark)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountMark.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountMark, item.PupilTotal);
                    double totalAbovePer = 0;
                    for (int i = 0; i < listMark.Count; i++)
                    {
                        StatisticsConfig sc = listMark[i];
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[i];
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                        if (percent.HasValue)
                        {
                            if (sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                totalAbovePer += percent.Value;
                            }
                        }
                    }
                    listCountMark.Clear();
                    foreach (StatisticsConfig sc in listJudge)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountMark.Add(markValue);
                    }
                    listPercent = ReportUtils.GetPerCentInList(listCountMark, item.PupilTotal);
                    for (int i = 0; i < listJudge.Count; i++)
                    {
                        StatisticsConfig sc = listJudge[i];
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[i];
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }
                    dataSheet.SetCellValue(index + 9, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.OnAverage.HasValue ? totalAbovePer.ToString().Replace(",", ".") : string.Empty);
                    index++;
                }
            }

            #region Xoa dong thua
            for (int i = totalRecordList; i < listCapacityStatistic.Count; i++)
            {
                dataSheet.DeleteRow(totalRecordList + 10);
            }
            #endregion

            dataSheet.Name = SheetName;
            dataSheet.FitSheetOnOnePage = true;
        }

        public Stream CreateConductForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THPT_THONGKE_XLHANHKIEM_THEOBAN);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            List<StatisticsConfig> listConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);
            //Danh sach cau hinh hanh kiem
            List<ConductStatisticsBO> listConductStatistic = StatisticsForUnitBusiness.SearchConductForTertiary(statisticsForUnit);
            listConductStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubCommittee = SubCommitteeBusiness.Find(ms.SubCommitteeID);
            });
            //Danh sach khoi
            List<EducationLevel> lstEdu = EducationLevelBusiness.AllNoTracking.Where(o => o.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY).ToList();
            //Danh sách ban học
            List<SubCommittee> listCommittee = listConductStatistic.Select(u => u.SubCommittee).Distinct().ToList();

            int lastColIndex = listConfig.Count * 2 + 4;
            List<ConductStatisticsBO> lstTMP = null;
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            #region tat ca
            #region tao khung
            int StartRow = 10;
            IVTWorksheet dataSheet = oBook.CopySheetToLast(sheet1);
            IVTWorksheet tempSheet = oBook.CopySheetToLast(sheet2);
            //Cot
            IVTRange tbCol = tempSheet.GetRange("A1", "B4");
            lstTMP = listConductStatistic.Where(p => !p.EthnicType.HasValue && !p.GenreType.HasValue).ToList();
            for (int i = 0; i < listConfig.Count; i++)
            {
                dataSheet.CopyPaste(tbCol, 8, i * 2 + 5);
                dataSheet.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
            }
            //Dong
            IVTRange dataRow = dataSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 1; i <= listCommittee.Count + lstTMP.Count; i++)
            {
                //Đánh STT
                dataSheet.CopyPaste(dataRow, StartRow, 1, true);
                dataSheet.SetCellValue(StartRow, 1, i);
                StartRow++;
            }
            #endregion
            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;
            string SheetName = string.Empty;
            SheetName = "ThongKeXLHK";
            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);

            int index = 0;

            foreach (SubCommittee sub in listCommittee)
            {
                int preSumIndex = index + 10;
                var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);
                dataSheet.GetRange(index + 10, 2, listItem.Count() + 10 + index, 2).Merge();
                bool isFilled = false;
                foreach (var item in listItem)
                {
                    int colIndex = 2;
                    //dataSheet.SetCellValue(index + 9, colIndex++, index - count);

                    if (!isFilled)
                    {
                        dataSheet.SetCellValue(index + 10, colIndex++, SubCommitteeBusiness.Find(sub) != null ? SubCommitteeBusiness.Find(sub).Resolution : string.Empty);
                        isFilled = true;
                    }
                    else
                    {
                        colIndex++;
                    }

                    dataSheet.SetCellValue(index + 10, colIndex++, item.EducationLevel.Resolution);
                    dataSheet.SetCellValue(index + 10, colIndex++, item.PupilTotal);

                    List<int?> listCountConduct = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountConduct.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                    int pos = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheet.SetCellValue(index + 10, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 10, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                    }
                    index++;
                }
                dataSheet.SetCellValue(index + 10, 3, "Tổng");
                dataSheet.GetRange(index + 10, 3, index + 10, lastColIndex).SetFontStyle(true, null, false, null, false, false);
                dataSheet.SetFormulaValue(index + 10, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 9));
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheet.SetFormulaValue(index + 10, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 9));
                    dataSheet.SetFormulaValue(index + 10, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 10));
                }
                //count++;
                index++;
            }
            #endregion
            #region reportName
            dataSheet.Name = SheetName;
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            dataSheet.FitSheetOnOnePage = true;
            #endregion
            #endregion
            #region hoc sinh nu
            if (statisticsForUnit.FemaleType > 0)
            {
                #region tao khung
                StartRow = 10;
                IVTWorksheet dataSheetFemale = oBook.CopySheetToLast(sheet1);
                //Cot
                tbCol = tempSheet.GetRange("A1", "B4");
                lstTMP = listConductStatistic.Where(p => !p.EthnicType.HasValue && p.GenreType > 0).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemale.CopyPaste(tbCol, 8, i * 2 + 5);
                    dataSheetFemale.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
                }
                //Dong
                dataRow = dataSheetFemale.GetRange(11, 1, 11, lastColIndex);
                for (int i = 1; i <= listCommittee.Count + lstTMP.Count; i++)
                {
                    //Đánh STT
                    dataSheetFemale.CopyPaste(dataRow, StartRow, 1, true);
                    dataSheetFemale.SetCellValue(StartRow, 1, i);
                    StartRow++;
                }
                #endregion
                #region do du lieu vao report
                PupilName = string.Empty;
                SheetName = string.Empty;
                SheetName = "HS_Nu";
                PupilName = "HỌC SINH SINH NỮ";
                dicVarable["PupilName"] = PupilName;
                dataSheetFemale.FillVariableValue(dicVarable);

                index = 0;

                foreach (SubCommittee sub in listCommittee)
                {
                    int preSumIndex = index + 10;
                    var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID); ;
                    dataSheetFemale.GetRange(index + 10, 2, listItem.Count() + 10 + index, 2).Merge();
                    bool isFilled = false;
                    foreach (var item in listItem)
                    {
                        int colIndex = 2;
                        //dataSheetFemale.SetCellValue(index + 9, colIndex++, index - count);

                        if (!isFilled)
                        {
                            dataSheetFemale.SetCellValue(index + 10, colIndex++, SubCommitteeBusiness.Find(sub) != null ? SubCommitteeBusiness.Find(sub).Resolution : string.Empty);
                            isFilled = true;
                        }
                        else
                        {
                            colIndex++;
                        }

                        dataSheetFemale.SetCellValue(index + 10, colIndex++, item.EducationLevel.Resolution);
                        dataSheetFemale.SetCellValue(index + 10, colIndex++, item.PupilTotal);

                        List<int?> listCountConduct = new List<int?>();
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            listCountConduct.Add(markValue);
                        }
                        List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                        int pos = 0;
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            double? percent = listPercent[pos];
                            pos++;
                            dataSheetFemale.SetCellValue(index + 10, colIndex++, markValue);
                            dataSheetFemale.SetCellValue(index + 10, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                        }
                        index++;
                    }
                    dataSheetFemale.SetCellValue(index + 10, 3, "Tổng");
                    dataSheetFemale.GetRange(index + 10, 3, index + 10, lastColIndex).SetFontStyle(true, null, false, null, false, false);
                    dataSheetFemale.SetFormulaValue(index + 10, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 9));
                    for (int i = 0; i < listConfig.Count; i++)
                    {
                        dataSheetFemale.SetFormulaValue(index + 10, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 9));
                        dataSheetFemale.SetFormulaValue(index + 10, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 10));
                    }
                    //count++;
                    index++;
                }
                #endregion
                #region reportName
                dataSheetFemale.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                dataSheetFemale.FitSheetOnOnePage = true;
                #endregion
            }
            #endregion
            #region hoc sinh dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                #region tao khung
                StartRow = 10;
                IVTWorksheet dataSheetEthnic = oBook.CopySheetToLast(sheet1);
                //Cot
                tbCol = tempSheet.GetRange("A1", "B4");
                lstTMP = listConductStatistic.Where(p => p.EthnicType > 0 && !p.GenreType.HasValue).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnic.CopyPaste(tbCol, 8, i * 2 + 5);
                    dataSheetEthnic.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
                }
                //Dong
                dataRow = dataSheetEthnic.GetRange(11, 1, 11, lastColIndex);
                for (int i = 1; i <= listCommittee.Count + lstTMP.Count; i++)
                {
                    //Đánh STT
                    dataSheetEthnic.CopyPaste(dataRow, StartRow, 1, true);
                    dataSheetEthnic.SetCellValue(StartRow, 1, i);
                    StartRow++;
                }
                #endregion
                #region do du lieu vao report
                PupilName = string.Empty;
                SheetName = string.Empty;
                SheetName = "HS_DT";
                PupilName = "HỌC SINH DÂN TỘC";
                dicVarable["PupilName"] = PupilName;
                dataSheetEthnic.FillVariableValue(dicVarable);

                index = 0;

                foreach (SubCommittee sub in listCommittee)
                {
                    int preSumIndex = index + 10;
                    var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);
                    dataSheetEthnic.GetRange(index + 10, 2, listItem.Count() + 10 + index, 2).Merge();
                    bool isFilled = false;
                    foreach (var item in listItem)
                    {
                        int colIndex = 2;
                        //dataSheetFemale.SetCellValue(index + 9, colIndex++, index - count);

                        if (!isFilled)
                        {
                            dataSheetEthnic.SetCellValue(index + 10, colIndex++, SubCommitteeBusiness.Find(sub) != null ? SubCommitteeBusiness.Find(sub).Resolution : string.Empty);
                            isFilled = true;
                        }
                        else
                        {
                            colIndex++;
                        }

                        dataSheetEthnic.SetCellValue(index + 10, colIndex++, item.EducationLevel.Resolution);
                        dataSheetEthnic.SetCellValue(index + 10, colIndex++, item.PupilTotal);

                        List<int?> listCountConduct = new List<int?>();
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            listCountConduct.Add(markValue);
                        }
                        List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                        int pos = 0;
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            double? percent = listPercent[pos];
                            pos++;
                            dataSheetEthnic.SetCellValue(index + 10, colIndex++, markValue);
                            dataSheetEthnic.SetCellValue(index + 10, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                        }
                        index++;
                    }
                    dataSheetEthnic.SetCellValue(index + 10, 3, "Tổng");
                    dataSheetEthnic.GetRange(index + 10, 3, index + 10, lastColIndex).SetFontStyle(true, null, false, null, false, false);
                    dataSheetEthnic.SetFormulaValue(index + 10, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 9));
                    for (int i = 0; i < listConfig.Count; i++)
                    {
                        dataSheetEthnic.SetFormulaValue(index + 10, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 9));
                        dataSheetEthnic.SetFormulaValue(index + 10, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 10));
                    }
                    //count++;
                    index++;
                }
                #endregion
                #region reportName
                dataSheetEthnic.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                dataSheetEthnic.FitSheetOnOnePage = true;
                #endregion
            }
            #endregion
            #region hoc sinh nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                #region tao khung
                StartRow = 10;
                IVTWorksheet dataSheetFemaleEthnic = oBook.CopySheetToLast(sheet1);
                //Cot
                tbCol = tempSheet.GetRange("A1", "B4");
                lstTMP = listConductStatistic.Where(p => p.EthnicType > 0 && p.GenreType > 0).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemaleEthnic.CopyPaste(tbCol, 8, i * 2 + 5);
                    dataSheetFemaleEthnic.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
                }
                //Dong
                dataRow = dataSheetFemaleEthnic.GetRange(11, 1, 11, lastColIndex);
                for (int i = 1; i <= listCommittee.Count + lstTMP.Count; i++)
                {
                    //Đánh STT
                    dataSheetFemaleEthnic.CopyPaste(dataRow, StartRow, 1, true);
                    dataSheetFemaleEthnic.SetCellValue(StartRow, 1, i);
                    StartRow++;
                }
                #endregion
                #region do du lieu vao report
                PupilName = string.Empty;
                SheetName = string.Empty;
                SheetName = "HS_Nu_DT";
                PupilName = "HỌC SINH NỮ DÂN TỘC";
                dicVarable["PupilName"] = PupilName;
                dataSheetFemaleEthnic.FillVariableValue(dicVarable);

                index = 0;

                foreach (SubCommittee sub in listCommittee)
                {
                    int preSumIndex = index + 10;
                    var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);
                    dataSheetFemaleEthnic.GetRange(index + 10, 2, listItem.Count() + 10 + index, 2).Merge();
                    bool isFilled = false;
                    foreach (var item in listItem)
                    {
                        int colIndex = 2;
                        //dataSheetFemale.SetCellValue(index + 9, colIndex++, index - count);

                        if (!isFilled)
                        {
                            dataSheetFemaleEthnic.SetCellValue(index + 10, colIndex++, SubCommitteeBusiness.Find(sub) != null ? SubCommitteeBusiness.Find(sub).Resolution : string.Empty);
                            isFilled = true;
                        }
                        else
                        {
                            colIndex++;
                        }

                        dataSheetFemaleEthnic.SetCellValue(index + 10, colIndex++, item.EducationLevel.Resolution);
                        dataSheetFemaleEthnic.SetCellValue(index + 10, colIndex++, item.PupilTotal);

                        List<int?> listCountConduct = new List<int?>();
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            listCountConduct.Add(markValue);
                        }
                        List<double?> listPercent = ReportUtils.GetPerCentInList(listCountConduct, item.PupilTotal);
                        int pos = 0;
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(ConductStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            double? percent = listPercent[pos];
                            pos++;
                            dataSheetFemaleEthnic.SetCellValue(index + 10, colIndex++, markValue);
                            dataSheetFemaleEthnic.SetCellValue(index + 10, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);
                        }
                        index++;
                    }
                    dataSheetFemaleEthnic.SetCellValue(index + 10, 3, "Tổng");
                    dataSheetFemaleEthnic.GetRange(index + 10, 3, index + 10, lastColIndex).SetFontStyle(true, null, false, null, false, false);
                    dataSheetFemaleEthnic.SetFormulaValue(index + 10, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 9));
                    for (int i = 0; i < listConfig.Count; i++)
                    {
                        dataSheetFemaleEthnic.SetFormulaValue(index + 10, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 9));
                        dataSheetFemaleEthnic.SetFormulaValue(index + 10, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 10));
                    }
                    //count++;
                    index++;
                }
                #endregion
                #region reportName
                dataSheetFemaleEthnic.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                dataSheetFemaleEthnic.FitSheetOnOnePage = true;
                #endregion
            }
            #endregion
            sheet1.Delete();
            sheet2.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }

        public Stream CreateCapacityForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THPT_THONGKE_XLHOCLUC_THEOBAN);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<StatisticsConfig> listConfig = StatisticsConfigBusiness.GetList(SystemParamsInFile.EDUCATION_GRADE_TERTIARY, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC);

            List<CapacityStatisticsBO> listCapacityStatistic = StatisticsForUnitBusiness.SearchCapacityForTertiary(statisticsForUnit);
            listCapacityStatistic.ForEach(ms =>
            {
                ms.EducationLevel = EducationLevelBusiness.Find(ms.EducationLevelID);
                ms.SubCommittee = SubCommitteeBusiness.Find(ms.SubCommitteeID);
            });

            List<SubCommittee> listCommittee = listCapacityStatistic.Select(u => u.SubCommittee).Distinct().ToList();

            List<CapacityStatisticsBO> lstTMP = null;
            int lastColIndex = listConfig.Count * 2 + 6;

            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);

            #region Tat ca
            #region tao khung
            IVTWorksheet dataSheet = oBook.CopySheetToLast(sheet1);
            IVTWorksheet tempSheet = oBook.CopySheetToLast(sheet2);
            IVTRange tbCol = tempSheet.GetRange("A1", "B5");
            lstTMP = listCapacityStatistic.Where(p => !p.EthnicID.HasValue && !p.Genre.HasValue).ToList();
            for (int i = 0; i < listConfig.Count; i++)
            {
                dataSheet.CopyPaste(tbCol, 8, i * 2 + 5);
                dataSheet.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
            }

            dataSheet.CopyPaste(tbCol, 8, listConfig.Count * 2 + 5);
            dataSheet.SetCellValue(8, listConfig.Count * 2 + 5, "Trên TB");

            IVTRange dataRow = dataSheet.GetRange(11, 1, 11, lastColIndex);
            for (int i = 0; i < lstTMP.Count + listCommittee.Count - 1; i++)
            {
                dataSheet.CopyAndInsertARow(dataRow, 12, true);
            }
            // Xoa 3 dong thua
            for (int i = 0; i < 2; i++)
            {
                dataSheet.DeleteRow(10);
            }

            //dataSheet.CopyPaste(totalRange, listCapacityStatistic.Count + 10, 1);
            #endregion
            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(statisticsForUnit.SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(statisticsForUnit.AcademicYearID);
            EducationLevel eduLevel = EducationLevelBusiness.Find(statisticsForUnit.EducationLevelID);
            string PupilName = string.Empty;
            string SheetName = string.Empty;
            SheetName = "ThongKeXLHL";
            dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, statisticsForUnit.AppliedLevel);
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            dicVarable["Semester"] = SMASConvert.ConvertSemester(statisticsForUnit.Semester);
            dicVarable["PupilName"] = PupilName;
            dataSheet.FillVariableValue(dicVarable);

            int index = 1;
            int count = 0;

            foreach (SubCommittee sub in listCommittee)
            {
                index += count;
                int preSumIndex = index + 9;
                var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);

                IVTRange eduRange = dataSheet.GetRange(index + 9, 2, index + 9 + listItem.Count(), 2);
                eduRange.Merge();

                bool isFilled = false;
                foreach (var item in listItem)
                {
                    int colIndex = 1;
                    dataSheet.SetCellValue(index + 9, colIndex++, index - count);
                    if (!isFilled)
                    {
                        dataSheet.SetCellValue(index + 9, colIndex++, sub != null ? sub.Resolution : string.Empty);
                        isFilled = true;
                    }
                    else colIndex++;
                    dataSheet.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                    dataSheet.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                    List<int?> listCountCapacity = new List<int?>();
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        listCountCapacity.Add(markValue);
                    }
                    List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                    int pos = 0;
                    double totalAbovePer = 0;
                    foreach (StatisticsConfig sc in listConfig)
                    {
                        int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                        double? percent = listPercent[pos];
                        pos++;
                        dataSheet.SetCellValue(index + 9, colIndex++, markValue);
                        dataSheet.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);

                        if (percent.HasValue && sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                        {
                            totalAbovePer += percent.Value;
                        }
                    }
                    dataSheet.SetCellValue(index + 9, colIndex++, item.OnAverage);
                    dataSheet.SetCellValue(index + 9, colIndex++, totalAbovePer);

                    index++;
                }
                dataSheet.SetCellValue(index + 9, 3, "Tổng");
                dataSheet.SetCellValue(index + 9, 1, index - count);

                dataSheet.GetRange(index + 9, 3, index + 9, 18).SetFontStyle(true, null, false, null, false, false);
                dataSheet.SetFormulaValue(index + 9, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 8));
                for (int i = 0; i <= listConfig.Count; i++)
                {
                    dataSheet.SetFormulaValue(index + 9, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 8));
                    dataSheet.SetFormulaValue(index + 9, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 9));
                }
                index++;
            }
            dataSheet.FitSheetOnOnePage = true;
            tempSheet.FitToPage = true;
            #endregion
            #region reportName
            dataSheet.Name = SheetName;
            string semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
            reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion
            #endregion
            #region Hoc sinh nu
            if (statisticsForUnit.FemaleType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetFemale = oBook.CopySheetToLast(sheet1);
                lstTMP = listCapacityStatistic.Where(p => !p.EthnicID.HasValue && p.Genre > 0).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFemale.CopyPaste(tbCol, 8, i * 2 + 5);
                    dataSheetFemale.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
                }

                dataSheetFemale.CopyPaste(tbCol, 8, listConfig.Count * 2 + 5);
                dataSheetFemale.SetCellValue(8, listConfig.Count * 2 + 5, "Trên TB");

                dataRow = dataSheetFemale.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count + listCommittee.Count - 1; i++)
                {
                    dataSheetFemale.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 3 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetFemale.DeleteRow(10);
                }

                //dataSheetFemale.CopyPaste(totalRange, listCapacityStatistic.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                PupilName = string.Empty;
                SheetName = string.Empty;
                PupilName = "HỌC SINH NỮ";
                SheetName = "HS_Nu";
                dicVarable["PupilName"] = PupilName;
                dataSheetFemale.FillVariableValue(dicVarable);

                index = 1;
                count = 0;

                foreach (SubCommittee sub in listCommittee)
                {
                    index += count;
                    int preSumIndex = index + 9;
                    var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);

                    IVTRange eduRange = dataSheetFemale.GetRange(index + 9, 2, index + 9 + listItem.Count(), 2);
                    eduRange.Merge();

                    bool isFilled = false;
                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, index - count);
                        if (!isFilled)
                        {
                            dataSheetFemale.SetCellValue(index + 9, colIndex++, sub != null ? sub.Resolution : string.Empty);
                            isFilled = true;
                        }
                        else colIndex++;
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                        List<int?> listCountCapacity = new List<int?>();
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            listCountCapacity.Add(markValue);
                        }
                        List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                        int pos = 0;
                        double totalAbovePer = 0;
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            double? percent = listPercent[pos];
                            pos++;
                            dataSheetFemale.SetCellValue(index + 9, colIndex++, markValue);
                            dataSheetFemale.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);

                            if (percent.HasValue && sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                totalAbovePer += percent.Value;
                            }
                        }
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, item.OnAverage);
                        dataSheetFemale.SetCellValue(index + 9, colIndex++, totalAbovePer);

                        index++;
                    }
                    dataSheetFemale.SetCellValue(index + 9, 3, "Tổng");
                    dataSheetFemale.SetCellValue(index + 9, 1, index - count);

                    dataSheetFemale.GetRange(index + 9, 3, index + 9, 18).SetFontStyle(true, null, false, null, false, false);
                    dataSheetFemale.SetFormulaValue(index + 9, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 8));
                    for (int i = 0; i <= listConfig.Count; i++)
                    {
                        dataSheetFemale.SetFormulaValue(index + 9, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 8));
                        dataSheetFemale.SetFormulaValue(index + 9, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 9));
                    }
                    index++;
                }
                dataSheetFemale.FitSheetOnOnePage = true;
                tempSheet.FitToPage = true;
                #endregion
                #region reportName
                dataSheetFemale.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
            }
            
            #endregion
            #region Hoc sinh dan toc
            if (statisticsForUnit.EthnicType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetEthnic = oBook.CopySheetToLast(sheet1);
                lstTMP = listCapacityStatistic.Where(p => p.EthnicID > 0 && !p.Genre.HasValue).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetEthnic.CopyPaste(tbCol, 8, i * 2 + 5);
                    dataSheetEthnic.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
                }

                dataSheetEthnic.CopyPaste(tbCol, 8, listConfig.Count * 2 + 5);
                dataSheetEthnic.SetCellValue(8, listConfig.Count * 2 + 5, "Trên TB");

                dataRow = dataSheetEthnic.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count + listCommittee.Count - 1; i++)
                {
                    dataSheetEthnic.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 3 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetEthnic.DeleteRow(10);
                }

                //dataSheetEthnic.CopyPaste(totalRange, listCapacityStatistic.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                PupilName = string.Empty;
                SheetName = string.Empty;
                PupilName = "HỌC SINH DÂN TỘC";
                SheetName = "HS_DT";
                dicVarable["PupilName"] = PupilName;
                dataSheetEthnic.FillVariableValue(dicVarable);

                index = 1;
                count = 0;

                foreach (SubCommittee sub in listCommittee)
                {
                    index += count;
                    int preSumIndex = index + 9;
                    var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);

                    IVTRange eduRange = dataSheetEthnic.GetRange(index + 9, 2, index + 9 + listItem.Count(), 2);
                    eduRange.Merge();

                    bool isFilled = false;
                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, index - count);
                        if (!isFilled)
                        {
                            dataSheetEthnic.SetCellValue(index + 9, colIndex++, sub != null ? sub.Resolution : string.Empty);
                            isFilled = true;
                        }
                        else colIndex++;
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                        List<int?> listCountCapacity = new List<int?>();
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            listCountCapacity.Add(markValue);
                        }
                        List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                        int pos = 0;
                        double totalAbovePer = 0;
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            double? percent = listPercent[pos];
                            pos++;
                            dataSheetEthnic.SetCellValue(index + 9, colIndex++, markValue);
                            dataSheetEthnic.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);

                            if (percent.HasValue && sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                totalAbovePer += percent.Value;
                            }
                        }
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, item.OnAverage);
                        dataSheetEthnic.SetCellValue(index + 9, colIndex++, totalAbovePer);

                        index++;
                    }
                    dataSheetEthnic.SetCellValue(index + 9, 3, "Tổng");
                    dataSheetEthnic.SetCellValue(index + 9, 1, index - count);

                    dataSheetEthnic.GetRange(index + 9, 3, index + 9, 18).SetFontStyle(true, null, false, null, false, false);
                    dataSheetEthnic.SetFormulaValue(index + 9, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 8));
                    for (int i = 0; i <= listConfig.Count; i++)
                    {
                        dataSheetEthnic.SetFormulaValue(index + 9, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 8));
                        dataSheetEthnic.SetFormulaValue(index + 9, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 9));
                    }
                    index++;
                }
                dataSheetEthnic.FitSheetOnOnePage = true;
                tempSheet.FitToPage = true;
                #endregion
                #region reportName
                dataSheetEthnic.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
            }
            #endregion
            #region Hoc sinh nu dan toc
            if (statisticsForUnit.FemaleEthnicType > 0)
            {
                #region tao khung
                IVTWorksheet dataSheetFE = oBook.CopySheetToLast(sheet1);
                lstTMP = listCapacityStatistic.Where(p => p.EthnicID > 0 && p.Genre > 0).ToList();
                for (int i = 0; i < listConfig.Count; i++)
                {
                    dataSheetFE.CopyPaste(tbCol, 8, i * 2 + 5);
                    dataSheetFE.SetCellValue(8, i * 2 + 5, listConfig[i].DisplayName);
                }

                dataSheetFE.CopyPaste(tbCol, 8, listConfig.Count * 2 + 5);
                dataSheetFE.SetCellValue(8, listConfig.Count * 2 + 5, "Trên TB");

                dataRow = dataSheetFE.GetRange(11, 1, 11, lastColIndex);
                for (int i = 0; i < lstTMP.Count + listCommittee.Count - 1; i++)
                {
                    dataSheetFE.CopyAndInsertARow(dataRow, 12, true);
                }
                // Xoa 3 dong thua
                for (int i = 0; i < 2; i++)
                {
                    dataSheetFE.DeleteRow(10);
                }

                //dataSheetEthnic.CopyPaste(totalRange, listCapacityStatistic.Count + 10, 1);
                #endregion
                #region do du lieu vao report
                PupilName = string.Empty;
                SheetName = string.Empty;
                PupilName = "HỌC SINH NỮ DÂN TỘC";
                SheetName = "HS_Nu_DT";
                dicVarable["PupilName"] = PupilName;
                dataSheetFE.FillVariableValue(dicVarable);

                index = 1;
                count = 0;
                foreach (SubCommittee sub in listCommittee)
                {
                    index += count;
                    int preSumIndex = index + 9;
                    var listItem = lstTMP.Where(u => (!u.SubCommitteeID.HasValue && sub == null) || u.SubCommitteeID == sub.SubCommitteeID);

                    IVTRange eduRange = dataSheetFE.GetRange(index + 9, 2, index + 9 + listItem.Count(), 2);
                    eduRange.Merge();

                    bool isFilled = false;
                    foreach (var item in listItem)
                    {
                        int colIndex = 1;
                        dataSheetFE.SetCellValue(index + 9, colIndex++, index - count);
                        if (!isFilled)
                        {
                            dataSheetFE.SetCellValue(index + 9, colIndex++, sub != null ? sub.Resolution : string.Empty);
                            isFilled = true;
                        }
                        else colIndex++;
                        dataSheetFE.SetCellValue(index + 9, colIndex++, item.EducationLevel.Resolution);
                        dataSheetFE.SetCellValue(index + 9, colIndex++, item.PupilTotal);

                        List<int?> listCountCapacity = new List<int?>();
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            listCountCapacity.Add(markValue);
                        }
                        List<double?> listPercent = ReportUtils.GetPerCentInList(listCountCapacity, item.PupilTotal);
                        int pos = 0;
                        double totalAbovePer = 0;
                        foreach (StatisticsConfig sc in listConfig)
                        {
                            int? markValue = (int?)typeof(CapacityStatistic).GetProperty(sc.MappedColumn).GetValue(item, null);
                            double? percent = listPercent[pos];
                            pos++;
                            dataSheetFE.SetCellValue(index + 9, colIndex++, markValue);
                            dataSheetFE.SetCellValue(index + 9, colIndex++, percent.HasValue ? percent.ToString().Replace(",", ".") : string.Empty);

                            if (percent.HasValue && sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                totalAbovePer += percent.Value;
                            }
                        }
                        dataSheetFE.SetCellValue(index + 9, colIndex++, item.OnAverage);
                        dataSheetFE.SetCellValue(index + 9, colIndex++, totalAbovePer);

                        index++;
                    }
                    dataSheetFE.SetCellValue(index + 9, 3, "Tổng");
                    dataSheetFE.SetCellValue(index + 9, 1, index - count);

                    dataSheetFE.GetRange(index + 9, 3, index + 9, 18).SetFontStyle(true, null, false, null, false, false);
                    dataSheetFE.SetFormulaValue(index + 9, 4, string.Format("=SUM(D{0}:D{1})", preSumIndex, index + 8));
                    for (int i = 0; i <= listConfig.Count; i++)
                    {
                        dataSheetFE.SetFormulaValue(index + 9, i * 2 + 5, string.Format("=SUM({0}{1}:{0}{2})", (char)('A' + (i * 2 + 4)), preSumIndex, index + 8));
                        dataSheetFE.SetFormulaValue(index + 9, i * 2 + 6, string.Format("=IF(AND(D{1}<>\"\"; D{1}<>0); ROUND(100*{0}{1}/D{1};1); \"\")", (char)('A' + (i * 2 + 4)), index + 9));
                    }
                    index++;
                }
                dataSheetFE.FitSheetOnOnePage = true;
                tempSheet.FitToPage = true;
                #endregion
                #region reportName
                dataSheetFE.Name = SheetName;
                semester = ReportUtils.ConvertSemesterForReportName(statisticsForUnit.Semester);
                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                #endregion
            }
            #endregion
            sheet1.Delete();
            sheet2.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }
        #endregion

        #region Dem so hoc sinh theo mon trong lop
        /// <summary>
        /// QuangLM
        /// Dem so hoc sinh theo mon trong lop theo mon hoc va khoi
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        /// <param name="Semester"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        private List<PupilSubjectEducationLevel> CountPupilBySubject(IDictionary<string, object> dic, IQueryable<ClassSubject> iqClassSubject, 
            List<RegisterSubjectSpecializeBO> lstRegis)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int semesterID = Utils.GetInt(dic, "Semester");
            AcademicYear Aca = AcademicYearBusiness.Find(academicYearID);
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolID, dic).AddCriteriaSemester(Aca, semesterID);

            List<ObjectPupilSubject> listPupilSubject = (from poc in listPupilOfClass
                                                               join cs in iqClassSubject on poc.ClassID equals cs.ClassID
                                                               select new ObjectPupilSubject
                                                               {
                                                                   PupilID = poc.PupilID,
                                                                   SubjectID = cs.SubjectID,
                                                                   EducationLevelID = cs.ClassProfile.EducationLevelID,
                                                                   AppliedType = cs.AppliedType,
                                                                   ClassID = cs.ClassID,
                                                                   SubjectVNEN = cs.IsSubjectVNEN
                                                               }).ToList();

            //lay danh sach lop hoc mon vnen
            List<int> listSubjectIdRemove = iqClassSubject.Where(p => p.IsSubjectVNEN == true).Select(p => p.SubjectID).Distinct().ToList();
            List<int> listClassIdRemove = iqClassSubject.Where(p => listSubjectIdRemove.Contains(p.SubjectID)).Select(p => p.ClassID).Distinct().ToList();
            //Loai bo mon hoc trong listPupilTmp
            List<ObjectPupilSubject> listPupilTmp = new List<ObjectPupilSubject>();
            for (int i = 0; i < listClassIdRemove.Count; i++)
            {
                for (int j = 0; j < listSubjectIdRemove.Count; j++)
                {
                    listPupilTmp = listPupilSubject.Where(p => p.ClassID == listClassIdRemove[i] && p.SubjectID == listSubjectIdRemove[j] && p.SubjectVNEN == true).ToList();
                    foreach (ObjectPupilSubject item in listPupilTmp)
                    {
                        listPupilSubject.Remove(item);
                    }
                }
            }

            List<ObjectPupilSubject> tmpPupil = new List<ObjectPupilSubject>();
            int countRegis = 0;
            //loc bo nhung hoc sinh khong dang ky hoc mon tu chon
            foreach (var item in listPupilSubject)
            {
                if (item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE
                    || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                    || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                {
                    countRegis = lstRegis.Where(p => p.ClassID == item.ClassID && p.SubjectID == item.SubjectID && p.PupilID == item.PupilID
                        && (semesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL ?
                                                (p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST || p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                                : p.SemesterID == semesterID)).Count();
                    if (countRegis > 0)
                    {
                        tmpPupil.Add(item);
                    }
                    continue;
                }
                tmpPupil.Add(item);
            }

            //Danh sach hoc sinh mien giam            
            if (semesterID == 1)
            {
                dic["HasFirstSemester"] = true;
            }
            if (semesterID == 2)
            {
                dic["HasSecondSemester"] = true;
            }
            List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.Search(dic).ToList();

            List<ObjectPupilSubject> pupilSubjectRemove = null;
            ExemptedSubject exemptedSubjectTmp = null;
            if (lstExempted != null && lstExempted.Count > 0)
            {
                for (int i = 0; i < lstExempted.Count; i++)
                {
                    exemptedSubjectTmp = lstExempted[i];
                    pupilSubjectRemove = tmpPupil.Where(p => p.EducationLevelID == exemptedSubjectTmp.EducationLevelID
                        && p.ClassID == exemptedSubjectTmp.ClassID
                        && p.PupilID == exemptedSubjectTmp.PupilID
                        && p.SubjectID == exemptedSubjectTmp.SubjectID
                        ).ToList();
                    for (int j = 0; j < pupilSubjectRemove.Count; j++)
                    {
                        tmpPupil.Remove(pupilSubjectRemove[j]);
                    }
                }
            }
            //End mien giam

            var lstPupil = (from p in tmpPupil
                            group p by new { p.SubjectID, p.EducationLevelID } into g
                            select new { SubjectID = g.Key.SubjectID, EducationLevelID = g.Key.EducationLevelID, CountPupil = g.Count() }).ToList();

            List<PupilSubjectEducationLevel> listPupilSubjectEducationLevel = new List<PupilSubjectEducationLevel>();
            foreach (var item in lstPupil)
            {
                PupilSubjectEducationLevel data = new PupilSubjectEducationLevel();
                data.EducationLevelID = item.EducationLevelID;
                data.SubjectID = item.SubjectID;
                data.TotalPupil = item.CountPupil;
                listPupilSubjectEducationLevel.Add(data);
            }
            return listPupilSubjectEducationLevel;
        }
        #endregion

        public ProcessedReport InsertPeriodStatistic(StatisticReportBO entity, Stream data, string reportName, int reportTypeID)
        {
            string reportCode = string.Empty;
            if (reportTypeID == 1)//Thống kê điểm KT định kỳ
            {
                reportCode = SystemParamsInFile.THPT_THONGKE_DIEMKT_DINHKY;
            }
            else if (reportTypeID == 2)//Thống kê điểm kiểm tra học kỳ
            {
                reportCode = SystemParamsInFile.THPT_THONGKE_DIEMKIEMTRA_HOCKY;
            }
            else if (reportTypeID == 3)//Thống kê học lực môn
            {
                reportCode = SystemParamsInFile.THPT_THONGKE_DIEMTBM;
            }
            else if (reportTypeID == 4)//Thống kê học lực
            {
                reportCode = SystemParamsInFile.THPT_THONGKE_XLHOCLUC_THEOBAN;
            }
            else if (reportTypeID == 5)//Thống kê hạnh kiểm
            {
                reportCode = SystemParamsInFile.THPT_THONGKE_XLHANHKIEM_THEOBAN;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeXLHL_[Học kỳ]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.SemesterID);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevelID));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            pr.ReportName = reportName;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.SemesterID}
                };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public ProcessedReport InsertSemesterMarkPrimary(StatisticsForUnitBO entity, Stream data, string reportName)
        {
            string reportCode = SystemParamsInFile.TH_THONGKE_DIEMKIEMTRA_HOCKY;
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID",entity.SchoolID},
                {"EducationLevelID",entity.EducationLevelID},
                {"Semester", entity.Semester}
                };
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyPrimary(dic);
            pr.ReportData = ReportUtils.Compress(data);
            pr.ReportName = reportName;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
    }

    class ObjectMarkRecord
    {
        public int StatisticsConfigID { get; set; }
        public decimal? MinValue { get; set; }
        public decimal? MaxValue { get; set; }
        public int? MarkType { get; set; }
        public string MappedColumn { get; set; }
        public int SchoolID { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectID { get; set; }
        public int? Semester { get; set; }
        public int CountMark { get; set; }
    }

    class ObjectPupilSubject
    {
        public int SubjectID { get; set; }
        public int EducationLevelID { get; set; }
        public int PupilID { get; set; }
        public int? AppliedType { get; set; }
        public int ClassID { get; set; }
        public bool? SubjectVNEN { get; set; }
    }

    class PupilSubjectEducationLevel
    {
        public int TotalPupil { get; set; }
        public int SubjectID { get; set; }
        public int EducationLevelID { get; set; }
    }

    class ListGroup
    {
        public int EducationLevelID { get; set; }
        public int? SemesterID { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }
        public int OrderCretia { get; set; }
    }
}
