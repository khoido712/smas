/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ClassPropertyCatBusiness
    {

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dicParam">The dic param.</param>
        /// <returns>
        /// IQueryable{ClassPropertyCat}
        /// </returns>
        /// <date>11/12/2012</date>
        /// <author>
        /// Quanglm
        /// </author>
        public IQueryable<ClassPropertyCat> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            bool? IsActive = Utils.GetIsActive(dicParam, "IsActive");
            int AppliedLevel = Utils.GetByte(dicParam, "AppliedLevel");
            IQueryable<ClassPropertyCat> listClassPropertyCat = this.ClassPropertyCatRepository.All;
            if (Resolution != null && !Resolution.Equals(string.Empty))
            {
                listClassPropertyCat = listClassPropertyCat.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listClassPropertyCat = listClassPropertyCat.Where(x => x.IsActive == IsActive.Value);
            }
            if (AppliedLevel > 0)
            {
                listClassPropertyCat = listClassPropertyCat.Where(x => x.AppliedLevel == 0 || x.AppliedLevel == AppliedLevel);
            }
            return listClassPropertyCat;

        }
        
    }
}
