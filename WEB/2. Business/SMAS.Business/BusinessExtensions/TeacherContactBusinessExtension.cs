/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Business.Common.Extension;
namespace SMAS.Business.Business
{
    public partial class TeacherContactBusiness
    {
        /// <summary>
        /// Lay danh sach giao vien theo nhom
        /// </summary>
        /// <auther>TUTV</auther>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<TeacherBO> GetListTeacherInGroup(Dictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "schoolID");
            int academicYearID = Utils.GetInt(dic, "academicYearID");
            int contactGroupID = Utils.GetInt(dic, "contactGroupID");
            int teacherID = Utils.GetInt(dic, "teacherID");
            bool isIgnoringTeacher = Utils.GetBool(dic, "isIgnoringTeacher", true);
            bool bIsHasContractGroupObject = Utils.GetBool(dic, "bIsHasContractGroupObject", false);
            
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            SMS_TEACHER_CONTACT_GROUP objContactGroup = null;
            if (bIsHasContractGroupObject)
            {
                objContactGroup = Utils.GetObject<SMS_TEACHER_CONTACT_GROUP>(dic, "objContactGroup");
                contactGroupID = objContactGroup.CONTACT_GROUP_ID;
                schoolID = objContactGroup.SCHOOL_ID;
            }
            else
            {
                objContactGroup = (from cg in this.ContactGroupBusiness.All
                                   where cg.CONTACT_GROUP_ID == contactGroupID && cg.SCHOOL_ID == schoolID
                                   select cg).FirstOrDefault();
            }

            //neu la nhom toan truong thi lay toan bo giao vien trong truong, nguoc lai lay giao vien trong nhom tru user dang dang nhap
            List<TeacherBO> lstTeacherBO = null;
            if (objContactGroup.IS_DEFAULT)
            {
                List<TeacherBO> listTeacherOfSchool = EmployeeBusiness.GetTeachersOfSchool(schoolID, academicYearID);
                if (listTeacherOfSchool != null)
                {
                    lstTeacherBO = (from t in listTeacherOfSchool
                                    where t.TeacherID != teacherID
                                    select t).ToList();
                }
            }
            else
            {
                List<int> lstTeacherInGroupID = new List<int>();
                if (isIgnoringTeacher)
                {
                    lstTeacherInGroupID = (from tc in this.TeacherContactBusiness.All
                                           where tc.TEACHER_ID != teacherID && tc.CONTACT_GROUP_ID == contactGroupID && tc.IS_ACTIVE == true
                                           select tc.TEACHER_ID).ToList();
                }
                else
                {
                    lstTeacherInGroupID = (from tc in this.TeacherContactBusiness.All
                                           where tc.CONTACT_GROUP_ID == contactGroupID && tc.IS_ACTIVE == true
                                           select tc.TEACHER_ID).ToList();
                }

                if (lstTeacherInGroupID != null && lstTeacherInGroupID.Count > 0)
                {
                    lstTeacherBO = EmployeeBusiness.GetListTeacher(lstTeacherInGroupID);
                    if (lstTeacherBO != null)
                    {
                        lstTeacherBO = (from t in lstTeacherBO
                                        where lstTeacherInGroupID.Contains(t.TeacherID)
                                        select t).ToList();
                    }

                }
            }

            if (lstTeacherBO == null)
            {
                return new List<TeacherBO>();
            }

            var lstTBO = lstTeacherBO.ToList();
            var objPromotion = PromotionBusiness.GetPromotionProgramByProvinceId(provinceId, schoolID);
            List<int> lstTeacherID = lstTBO.Select(p => p.TeacherID).Distinct().ToList();
            Dictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"Type",GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID},
                    {"SchoolID",schoolID},
                    {"lstTeacherReceived",lstTeacherID},
                    {"dateTimeNow",DateTime.Now}
                };
            List<SMS_RECEIVED_RECORD> lstReceivedRecord = this.ReceivedRecordBusiness.GetListReceiRecord(dicSearch);
            SMS_RECEIVED_RECORD objReceivedRecord;
            int fromOfInternalSMS = objPromotion != null ? objPromotion.FromOfInternalSMS : 0;
            int fromOfExternalSMS = objPromotion != null ? objPromotion.FromOfExternalSMS : 0;
            foreach (var objTeachBO in lstTBO)
            {
                objReceivedRecord = lstReceivedRecord.FirstOrDefault(e => e.EMPLOYEE_ID == objTeachBO.TeacherID);
                objTeachBO.isFreeSMS = objPromotion != null ? objPromotion.IsFreeSMS : null;
                objTeachBO.isFreeExSMS = objPromotion != null ? objPromotion.IsFreeExSMS : null;
                if (objReceivedRecord != null)
                {
                    int totalInternalSMS = objReceivedRecord.TOTAL_INTERNAL_SMS;
                    int totalExternalSMS = objReceivedRecord.TOTAL_EXTERNAL_SMS;
                    objTeachBO.TotalInternalSMS = totalInternalSMS;
                    objTeachBO.TotalExternalSMS = totalExternalSMS;
                    objTeachBO.isNumberVT = false;
                    if (objTeachBO.Mobile.CheckMobileNumberVT())
                    {
                        objTeachBO.TotalSMSPromotion = (totalInternalSMS + totalExternalSMS >= fromOfInternalSMS) ? 0 : fromOfInternalSMS - totalExternalSMS - totalInternalSMS;
                        objTeachBO.isNumberVT = true;
                    }
                    else if ((totalInternalSMS + totalExternalSMS >= fromOfInternalSMS))// Thue bao ngoai mang
                    {
                        objTeachBO.TotalSMSPromotion = 0;
                    }
                    else if (totalExternalSMS >= fromOfExternalSMS)//if K>=N
                    {
                        objTeachBO.TotalSMSPromotion = 0;
                    }
                    else //Min(M- V-K, N- K)
                    {
                        objTeachBO.TotalSMSPromotion = Math.Min(fromOfInternalSMS - totalExternalSMS - totalInternalSMS, fromOfExternalSMS - totalExternalSMS);
                    }
                }
                else
                {
                    objTeachBO.TotalExternalSMS = 0;
                    objTeachBO.TotalInternalSMS = 0;
                    if (objTeachBO.Mobile.CheckMobileNumberVT())
                    {
                        objTeachBO.TotalSMSPromotion = fromOfInternalSMS;
                        objTeachBO.isNumberVT = true;
                    }
                    else
                    {
                        objTeachBO.TotalSMSPromotion = fromOfExternalSMS;
                        objTeachBO.isNumberVT = false;
                    }
                }
            }
            return lstTBO; //lstTeacherBO.ToList();

        }
    }
}
