/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class DevelopmentOfChildrenBusiness
    {
        private void Validate(DevelopmentOfChildren Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

//PupilID, ClassID: not compatitive(PupilProfile)
            bool PupilProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                 new Dictionary<string, object>()
                {
                    {"PupilProfileID",Entity.PupilID},
                    {"CurrentClassID",Entity.ClassID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
//ClassID, AcademicYearID: not compatitive (ClassProfile)
            bool FromClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"ClassProfileID",Entity.ClassID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!FromClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
//SchoolID, AcademicYearID: not compatitive(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }

        public IQueryable<DevelopmentOfChildren> SearchBySchool(int? SchoolID, IDictionary<string, object> dic)
        {   
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }

        private IQueryable<DevelopmentOfChildren> Search(IDictionary<string, object> dic)
        {
            int DevelopmentOfChildrenID = Utils.GetInt(dic, "DevelopmentOfChildrenID");
            int EvaluationDevelopmentID = Utils.GetInt(dic, "EvaluationDevelopmentID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int Semester = Utils.GetByte(dic, "Semester");
            int Year = Utils.GetInt(dic, "Year");
            IQueryable<DevelopmentOfChildren> Query = DevelopmentOfChildrenRepository.All;
            if (Year != 0)
            {
                Query = Query.Where(evd => evd.Year == Year);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(evd => evd.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(evd => evd.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(evd => evd.PupilID == PupilID);
            }
            if (ClassID != 0)
            {
                Query = Query.Where(evd => evd.ClassID == ClassID);
            }
            if (EvaluationDevelopmentID != 0)
            {
                Query = Query.Where(evd => evd.EvaluationDevelopmentID == EvaluationDevelopmentID);
            }
            if (Semester != 0)
            {
                Query = Query.Where(evd => evd.Semester == Semester);
            }
            if (DevelopmentOfChildrenID != 0)
            {
                Query = Query.Where(evd => evd.DevelopmentOfChildrenID == DevelopmentOfChildrenID);
            }
            return Query;
        }

        public void Insert(List<DevelopmentOfChildren> lstDevelopmentOfChildren)
        {
            foreach (DevelopmentOfChildren item in lstDevelopmentOfChildren)
            {
                this.Validate(item);
                base.Insert(item);
            }
        }
    }
}
