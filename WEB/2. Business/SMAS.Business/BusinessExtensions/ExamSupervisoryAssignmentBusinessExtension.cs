﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ExamSupervisoryAssignmentBusiness
    {
        public IQueryable<ExamSupervisoryAssignment> Search(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            long examSupervisoryID = Utils.GetLong(search, "ExamSupervisoryID");

            IQueryable<ExamSupervisoryAssignment> query = this.All;
            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (examSupervisoryID != 0)
            {
                query = query.Where(o => o.ExamSupervisoryID == examSupervisoryID);
            }

            return query;
        }

        public IQueryable<ExamSupervisoryAssignmentBO> AdditionSearch(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examSupervisoryID = Utils.GetLong(search, "ExamSupervisoryID");
            int employeeID = Utils.GetInt(search, "EmployeeID");

            IQueryable<ExamSupervisoryAssignmentBO> query = from esa in this.repository.All
                                                            join e in this.ExaminationsRepository.All on esa.ExaminationsID equals e.ExaminationsID
                                                            join eg in this.ExamGroupRepository.All on esa.ExamGroupID equals eg.ExamGroupID
                                                            join esu in this.ExamSupervisoryRepository.All on esa.ExamSupervisoryID equals esu.ExamSupervisoryID
                                                            join ee in this.EmployeeRepository.All on esu.TeacherID equals ee.EmployeeID
                                                            join sf in this.SchoolFacultyRepository.All on ee.SchoolFacultyID equals sf.SchoolFacultyID
                                                            join en in this.EthnicRepository.All on ee.EthnicID equals en.EthnicID into des
                                                            from x in des.DefaultIfEmpty()
                                                            join es in this.ExamSubjectRepository.All on new { esa.ExaminationsID, esa.ExamGroupID, esa.SubjectID } equals new { es.ExaminationsID, es.ExamGroupID, es.SubjectID }
                                                            join s in this.SubjectCatRepository.All on es.SubjectID equals s.SubjectCatID
                                                            join er in this.ExamRoomRepository.All on esa.ExamRoomID equals er.ExamRoomID
                                                            where ee.IsActive == true
                                                            select new ExamSupervisoryAssignmentBO
                                                            {
                                                                SubjectName = s.SubjectName,
                                                                ExamRoomID = esa.ExamRoomID,
                                                                ExamRoomCode = er.ExamRoomCode,
                                                                SchedulesExam = es.SchedulesExam,
                                                                AcademicYearID = esa.AcademicYearID,
                                                                SubjectID = esa.SubjectID,
                                                                OrderInSubject = s.OrderInSubject,
                                                                ExaminationsID = esa.ExaminationsID,
                                                                ExamGroupID = esa.ExamGroupID,
                                                                ExamSupervisoryID = esa.ExamSupervisoryID,
                                                                ExamSupervisoryAssignmentID = esa.ExamSupervisoryAssignmentID,
                                                                ExamGroupCode = eg.ExamGroupCode,
                                                                ExamGroupName = eg.ExamGroupName,
                                                                EmployeeCode = ee.EmployeeCode,
                                                                EmployeeFullName = ee.FullName,
                                                                EmployeeBirthDate = ee.BirthDate,
                                                                SchoolFacultyName = sf.FacultyName,
                                                                EmployeePhoneNumber = ee.Mobile,
                                                                CreateDate = esa.CreateTime,
                                                                TeacherID = ee.EmployeeID,
                                                                EthnicCode = x != null ? x.EthnicCode : null
                                                            };
            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }

            if (examSupervisoryID != 0)
            {
                query = query.Where(o => o.ExamSupervisoryID == examSupervisoryID);
            }

            if (employeeID != 0) 
            {
                query = query.Where(o => o.TeacherID == employeeID);
            }

            return query;
        }

        public IQueryable<ExamSupervisoryAssignmentBO> SearchByExamSubject(int academicYearID, int schoolID, long examinationsID, long examGroupID, long subjectID, IDictionary<string, object> options)
        {
            long examRoomID = Utils.GetLong(options, "ExamRoomID");
            IQueryable<ExamSupervisoryAssignmentBO> query = from esa in this.ExamSupervisoryAssignmentRepository.All
                                                            join e in this.ExaminationsRepository.All on esa.ExaminationsID equals e.ExaminationsID
                                                            join eg in this.ExamGroupRepository.All on esa.ExamGroupID equals eg.ExamGroupID
                                                            join es in this.ExamSupervisoryRepository.All.Where(o => o.AcademicYearID == academicYearID && o.ExaminationsID == examinationsID) on esa.ExamSupervisoryID equals es.ExamSupervisoryID
                                                            join ee in this.EmployeeRepository.All.Where(o => o.SchoolID == schoolID) on es.TeacherID equals ee.EmployeeID
                                                            join sf in this.SchoolFacultyRepository.All on ee.SchoolFacultyID equals sf.SchoolFacultyID
                                                            join en in this.EthnicRepository.All on ee.EthnicID equals en.EthnicID into des
                                                            from x in des.DefaultIfEmpty()
                                                            where esa.AcademicYearID == academicYearID
                                                            && esa.ExaminationsID == examinationsID
                                                            && esa.ExamGroupID == examGroupID
                                                            //&& esa.SubjectID == subjectID
                                                            && ee.IsActive == true
                                                            select new ExamSupervisoryAssignmentBO
                                                            {
                                                                ExaminationsID = esa.ExaminationsID,
                                                                ExamGroupID = esa.ExamGroupID,
                                                                SubjectID = esa.SubjectID,
                                                                ExamSupervisoryAssignmentID = esa.ExamSupervisoryAssignmentID,
                                                                ExamSupervisoryID = esa.ExamSupervisoryID,
                                                                SchoolFacultyID = ee.SchoolFacultyID,
                                                                TeacherID = es.TeacherID,
                                                                EmployeeCode = ee.EmployeeCode,
                                                                EmployeeFullName = ee.FullName,
                                                                EthnicCode = x != null ? x.EthnicCode : null,
                                                                ExamRoomID = esa.ExamRoomID,
                                                                SchoolFacultyName = sf.FacultyName
                                                            };

            if (subjectID != 0) 
            {
                query = query.Where(o => o.SubjectID == subjectID);    
            }

            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            return query;
        }

        /// <summary>
        /// Kiem tra rang buoc mot giam thi khong duoc phan cong trung mon thi vao cac phong thi khac nhau trong nhom
        /// </summary>
        /// <param name="lsActivity"></param>
        public void CheckDuplicateData(ExamSupervisoryAssignment entity, long exceptID)
        {
            if (this.ExamSupervisoryAssignmentBusiness.All.Where(o => o.AcademicYearID == entity.AcademicYearID)
                                                        .Where(o => o.ExaminationsID == entity.ExaminationsID)
                                                        .Where(o => o.ExamGroupID == entity.ExamGroupID)
                                                        .Where(o => o.SubjectID == entity.SubjectID)
                                                        .Where(o => o.ExamSupervisoryID == entity.ExamSupervisoryID)
                                                        .Where(o => o.ExamSupervisoryAssignmentID != exceptID).Count() > 0)
            {
                throw new BusinessException("ExamSupervisoryAssign_Validate_DuplicateAssignment");
            }
        }

        /// <summary>
        /// Insert list su dung Bulk insert
        /// </summary>
        /// <param name="insertList"></param>
        public void InsertList(List<ExamSupervisoryAssignment> insertList)
        {
            try
            {
                if (insertList == null || insertList.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < insertList.Count; i++)
                {
                    ExamSupervisoryAssignmentBusiness.Insert(insertList[i]);
                }
                ExamSupervisoryAssignmentBusiness.Save();
            }
            catch (Exception ex)
            {

                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// Delete list su dung Bulk delete
        /// </summary>
        /// <param name="insertList"></param>
        public void DeleteList(List<ExamSupervisoryAssignment> deleteList)
        {
            try
            {
                if (deleteList == null || deleteList.Count == 0)
                {
                    return;
                }
                ExamSupervisoryAssignmentBusiness.DeleteAll(deleteList);
                ExamSupervisoryAssignmentBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }

        /// <summary>
        /// Ham lay column mapping de thuc hien bulk insert
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamSupervisoryAssignmentID", "EXAM_SUPERVISORY_ASSIGNMENT_ID");
            columnMap.Add("ExamSupervisoryID", "EXAM_SUPERVISORY_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");

            return columnMap;
        }

        #region bao cao danh sach thi sinh theo phong thi
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamSupervisoryAssignReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_PHAN_CONG_GIAM_THI;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamSupervisoryAssignReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_PHAN_CONG_GIAM_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = "Kỳ thi: " + exam.ExaminationsName;
            string strAcademicYear = "Năm học: " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);
            firstSheet.SetCellValue("E4", provinceAndDate);

            //Lay danh sach mon thi cua nhom thi
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).ToList();
            }

            //Lay danh sach phan cong giam thi cua nhom thi
            List<ExamSupervisoryAssignmentBO> listAssign = new List<ExamSupervisoryAssignmentBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;
                listAssign = ExamSupervisoryAssignmentBusiness.AdditionSearch(tmpDic).ToList();
            }

            //Tao tung sheet cho moi phong thi
            for (int i = 0; i < listExamSubject.Count; i++)
            {
                ExamSubjectBO examSubject = listExamSubject[i];

                //Fill du lieu
                //Danh sach phan cong cua mon thi
                List<ExamSupervisoryAssignmentBO> listResult = listAssign.Where(o => o.SubjectID == examSubject.SubjectID)
                                                             .OrderBy(o => o.ExamRoomCode).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode)))
                                                             .ToList();

                int startRow = 10;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 7;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "G" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = ReportUtils.StripVNSign(examSubject.SubjectName);
                curSheet.SetCellValue("A7", "MÔN THI: " + examSubject.SubjectName.ToUpper());

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamSupervisoryAssignmentBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //Ma phong thi
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamRoomCode);
                    curColumn++;
                    //Ma can bo
                    curSheet.SetCellValue(curRow, curColumn, obj.EmployeeCode);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.EmployeeFullName);
                    curColumn++;
                    //Ngay sinh
                    curSheet.SetCellValue(curRow, curColumn, obj.EmployeeBirthDate);
                    curColumn++;
                    //To bo mon
                    curSheet.SetCellValue(curRow, curColumn, obj.SchoolFacultyName);
                    curColumn++;
                    //Dien thoai
                    curSheet.SetCellValue(curRow, curColumn, obj.EmployeePhoneNumber);
                    curColumn++;

                    curRow++;
                    curColumn = startColumn;
                }

                IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                globalRange.SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);

                int index = 1;
                if (listResult.Count > 0)
                {
                    //Merge cell phong thi
                    long curExamRoomID = listResult[0].ExamRoomID;
                    int prevRow = startRow;

                    for (int j = 0; j <= listResult.Count; j++)
                    {
                        ExamSupervisoryAssignmentBO obj = null;
                        if (j < listResult.Count) obj = listResult[j];
                        if (obj == null || obj.ExamRoomID != curExamRoomID)
                        {
                            //Merge tu prevRow toi curRow-1
                            curSheet.MergeColumn(2, prevRow, startRow + j - 1);
                            curSheet.MergeColumn(1, prevRow, startRow + j - 1);
                            //Danh lai so thu tu
                            curSheet.SetCellValue(prevRow, 1, index);
                            index++;

                            //Ve border lien net sau moi nhom phong thi
                            IVTRange range = curSheet.GetRange(prevRow, startColumn, startRow + j - 1, lastColumn);
                            range.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                            prevRow = startRow + j;
                            curExamRoomID = obj != null ? obj.ExamRoomID : 0;
                        }
                    }
                }
                curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                curSheet.FitAllColumnsOnOnePage = true;

            }

            //Xoa sheet dau tien
            firstSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamSupervisoryAssignReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_PHAN_CONG_GIAM_THI;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup examGroup = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string examGroupCode = "";
            if (examGroup != null) examGroupCode = examGroup.ExamGroupCode;

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", examGroupCode);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        public bool GetSupervisoryAssign(List<int> subjectIDList, long examinationsID, long examGroupID)
        {
            return (from a in ExamSupervisoryAssignmentRepository.All
                    where subjectIDList.Contains(a.SubjectID)
                    && a.ExaminationsID == examinationsID
                    && (a.ExamGroupID == examGroupID || examGroupID == 0)
                    select a.ExamSupervisoryAssignmentID).Count() > 0;
        }
    }
}