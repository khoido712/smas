﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    /// <summary>
    /// DailyExpense
    /// </summary>
    /// <author>Quanglm</author>
    /// <date>11/15/2012</date>
    public partial class DailyExpenseBusiness
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <author>Quanglm</author>
        /// <date>11/15/2012</date>
        //public override void Insert(DailyExpense entity)
        //{
        //    base.Insert(entity);
        //}
        public void Insert(DailyExpense entity)
        {
            base.Insert(entity);
        }

        /// <summary>
        /// GetExpenseDate
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">To date.</param>
        /// <returns>
        /// IQueryable{DailyExpense}
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/15/2012</date>
        public IQueryable<DailyExpense> GetExpenseDate(int SchoolID, DateTime FromDate, DateTime ToDate)
        {
            IQueryable<DailyExpense> QueryDailyExpense = this.DailyExpenseRepository.All
                .Where(o => o.SchoolID == SchoolID)
                .Where(o => o.ExpenseDate >= FromDate)
                .Where(o => o.ExpenseDate <= ToDate);
            return QueryDailyExpense;
        }

        /// <summary>
        /// CheckExpenseDate
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">To date.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/15/2012</date>
        public bool CheckExpenseDate(int SchoolID, DateTime FromDate, DateTime ToDate)
        {
            IQueryable<DailyExpense> QueryDailyExpense = this.GetExpenseDate(SchoolID, FromDate, ToDate);
            DailyExpense DailyExpense = QueryDailyExpense.FirstOrDefault();
            if (DailyExpense == null)
            {
                return true;
            }
            return false;
        }

        public void Delete(int SchoolID, DateTime ExpenseDate)
        {
            IQueryable<DailyExpense> QueryDailyExpense = this.DailyExpenseRepository.All
               .Where(o => o.SchoolID == SchoolID)
               .Where(o => o.ExpenseDate == ExpenseDate);

            if (QueryDailyExpense != null && QueryDailyExpense.Count() > 0)
            {
                base.Delete(QueryDailyExpense.FirstOrDefault().DailyExpenseID);
            }
            
        }

        private IQueryable<DailyExpense> Search(IDictionary<string, object> SearchInfo)
        {
            DateTime? ExpenseDate = Utils.GetDateTime(SearchInfo, "ExpenseDate");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            IQueryable<DailyExpense> ListDailyExpense = this.repository.All;
            if (ExpenseDate != null)
            {
                ListDailyExpense = ListDailyExpense.Where(o => o.ExpenseDate == ExpenseDate);
            }
            if (SchoolID != 0)
            {
                ListDailyExpense = ListDailyExpense.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                ListDailyExpense = ListDailyExpense.Where(o => o.AcademicYearID == AcademicYearID);
            }


            return ListDailyExpense;

        }

        public IQueryable<DailyExpense> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        #region Tạo báo cáo tính tiền đi chợ
        /// <summary>
        /// CreateReportDailyExpense
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <param name="AcademicYearID">The AcademicYearID</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>PhuongTD</author>
        /// <date>14/12/2012</date>
        public Stream CreateReportDailyExpense(int SchoolID, int AcademicYearID, DateTime date)
        {

            string reportCode = SystemParamsInFile.REPORT_TINH_TIEN_CHO;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            string formular = "";
            #region Phần thông tin chung
            int generalInfo_firstRow = 2;
            int generalInfo_endCol = 9; //"I"
            int generalInfo_lastRow = 8;
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange generalInfo_range = firstSheet.GetRange(generalInfo_firstRow, 1, generalInfo_lastRow, generalInfo_endCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(firstSheet, "I" + generalInfo_lastRow);
            //Lấy thông tin cho phần thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Date", date.ToShortDateString()}
                };
            dataSheet.GetRange(generalInfo_firstRow, 1, generalInfo_lastRow, generalInfo_endCol).FillVariableValue(dicGeneralInfo);
            #endregion

            #region Phần thực phẩm
            int food_firstRow = 9;
            int food_lastRow = 15;
            int startDynamicCol = 6;
            int lastCol = startDynamicCol;
            //Danh sách nhóm ăn
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            IQueryable<EatingGroupBO> lsEatingGroup = EatingGroupBusiness.SearchBySchool(SchoolID, SearchInfo);
            //Số trẻ
            int NumberOfChildren = DishInspectionBusiness.GetNumberOfChildren(SchoolID, date);
            //Danh sách thực phẩm
            IQueryable<DishInspectionBO> lsFood = DishInspectionBusiness.GetListFood(SchoolID, date);
            if (lsFood != null && lsFood.Count() > 0)
            {
                IVTRange foodheaderRange = firstSheet.GetRange("A9", "E12");
                dataSheet.CopyPasteSameSize(foodheaderRange, food_firstRow, 1);

                //Danh sách nhóm thực phẩm
                List<int> lstFoodGroupID = lsFood.Select(o => o.FoodGroupID).Distinct().ToList();
                
                IVTRange foodRow = firstSheet.GetRange("A14", "E14");
                
                IVTRange kGRange = firstSheet.GetRange("F12", "F12");
                IVTRange totalMoney = firstSheet.GetRange("G12", "G12");
                IVTRange kGRowRange = firstSheet.GetRange("F13", "F13");
                IVTRange totalRowMoney = firstSheet.GetRange("G13", "G13");
                IVTRange eatingGroupRange = firstSheet.GetRange("F11", "G11");
                IVTRange menuType = firstSheet.GetRange("F10", "G10");
                List<Object> listData = new List<Object>();
                int countFoodGroup = 0;
                int countFood = 0;
                int lastcol = startDynamicCol + lsEatingGroup.Count() * 4 - 1;
                IVTRange foodGroupRow = firstSheet.GetRange("A13", VTVector.ColumnIntToString(lastcol) + 13);
                foreach (int foodGroupID in lstFoodGroupID)
                {
                    IQueryable<DishInspectionBO> lsFoodByGroup = lsFood.Where(o => o.FoodGroupID == foodGroupID);
                    
                    if (lsFoodByGroup != null && lsFoodByGroup.Count() > 0)
                    {
                        FoodGroup foodGroup = FoodGroupBusiness.Find(foodGroupID);
                        //Danh sách thực phẩm trong nhóm
                        List<int> lstFoodID = lsFoodByGroup.Select(o => o.FoodID).Distinct().ToList();
                        if (lstFoodID.Count > 0)
                        {
                            countFoodGroup++;
                            //Điền dữ liệu kho
                            dataSheet.CopyPasteSameSize(foodGroupRow, food_firstRow + 3 + countFood + countFoodGroup, 1);
                            dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, 1, "Thực phẩm xuất " + foodGroup.FoodGroupName);
                            foreach (int foodID in lstFoodID)
                            {
                                countFood++;
                                Dictionary<string, object> dicData = new Dictionary<string, object>();
                                //Điền dữ liệu danh sách thực phẩm
                                DishInspectionBO food = lsFoodByGroup.Where(o => o.FoodID == foodID).ToList().FirstOrDefault();
                                dataSheet.CopyPasteSameSize(foodRow, food_firstRow + 3 + countFood + countFoodGroup, 1);
                                dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, 1, countFood);
                                dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, 2, food.FoodName);
                                dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, 3, food.Weight * NumberOfChildren / 1000);
                                dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, 4, food.PricePerOnce);
                                formular = "=D" + (food_firstRow + 3 + countFood + countFoodGroup) + "*C" + (food_firstRow + 3 + countFood + countFoodGroup);
                                dataSheet.SetFormulaValue(food_firstRow + 3 + countFood + countFoodGroup, 5, formular);
                                if (lsEatingGroup != null && lsEatingGroup.Count() > 0)
                                {
                                    List<EatingGroupBO> ListEatingGroup = lsEatingGroup.ToList();

                                    //Thực đơn thường
                                    int countColumn = 0;
                                    lastCol = startDynamicCol + lsEatingGroup.Count() * 4 - 1;
                                    foreach (EatingGroupBO eatingGroup in ListEatingGroup)
                                    {
                                        dataSheet.CopyPasteSameSize(kGRowRange, food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2);
                                        dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2, "${list[].Get(Normal" + eatingGroup.EatingGroupID + ")}");
                                        formular = "=D" + (food_firstRow + 3 + countFood + countFoodGroup) + "*" + VTVector.ColumnIntToString(startDynamicCol + countColumn * 2) + (food_firstRow + 3 + countFood + countFoodGroup);
                                        dataSheet.CopyPasteSameSize(totalRowMoney, food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2 + 1);
                                        dataSheet.SetFormulaValue(food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2 + 1, formular);
                                        DishInspectionBO dishInspection = lsFoodByGroup.Where(o => o.EatingGroupID == eatingGroup.EatingGroupID && o.MenuType == SystemParamsInFile.MENU_TYPE_NORMAL).FirstOrDefault();
                                        dicData["Normal" + eatingGroup.EatingGroupID.ToString()] = dishInspection != null ? dishInspection.Weight * NumberOfChildren / 1000 : 0;
                                        //FillHeader
                                        dataSheet.CopyPasteSameSize(kGRange, 12, startDynamicCol + countColumn * 2);
                                        dataSheet.CopyPasteSameSize(totalMoney, 12, startDynamicCol + countColumn * 2 + 1);
                                        dataSheet.CopyPasteSameSize(eatingGroupRange, 11, startDynamicCol + countColumn * 2);
                                        dataSheet.SetCellValue(11, startDynamicCol + countColumn * 2, eatingGroup.EatingGroupName);
                                        dataSheet.CopyPaste(menuType, 10, startDynamicCol + countColumn * 2);
                                        countColumn++;
                                    }
                                    dataSheet.GetRange(10, startDynamicCol, 10, startDynamicCol + ListEatingGroup.Count * 2 - 1).Merge();
                                    dataSheet.SetCellValue(10, startDynamicCol, "Thực đơn thường");
                                    //Thực đơn bổ sung
                                    foreach (EatingGroupBO eatingGroup in ListEatingGroup)
                                    {
                                        //tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
                                        dataSheet.CopyPasteSameSize(kGRowRange, food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2);
                                        dataSheet.SetCellValue(food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2, "${list[].Get(Additional" + eatingGroup.EatingGroupID + ")}");
                                        formular = "=D" + (food_firstRow + 3 + countFood + countFoodGroup) + "*" + VTVector.ColumnIntToString(startDynamicCol + countColumn * 2) + (food_firstRow + 3 + countFood + countFoodGroup);
                                        dataSheet.CopyPasteSameSize(totalRowMoney, food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2 + 1);
                                        dataSheet.SetFormulaValue(food_firstRow + 3 + countFood + countFoodGroup, startDynamicCol + countColumn * 2 + 1, formular);
                                        DishInspectionBO dishInspection = lsFoodByGroup.Where(o => o.EatingGroupID == eatingGroup.EatingGroupID && o.MenuType == SystemParamsInFile.MENU_TYPE_ADDITIONAL).FirstOrDefault();
                                        dicData["Additional" + eatingGroup.EatingGroupID.ToString()] = dishInspection != null ? dishInspection.Weight * NumberOfChildren / 1000 : 0;
                                        //FillHeader
                                        dataSheet.CopyPasteSameSize(kGRange, 12, startDynamicCol + countColumn * 2);
                                        dataSheet.CopyPasteSameSize(totalMoney, 12, startDynamicCol + countColumn * 2 + 1);
                                        dataSheet.CopyPasteSameSize(eatingGroupRange, 11, startDynamicCol + countColumn * 2);
                                        dataSheet.SetCellValue(11, startDynamicCol + countColumn * 2, eatingGroup.EatingGroupName);
                                        dataSheet.CopyPaste(menuType, 10, startDynamicCol + countColumn * 2);
                                        countColumn++;
                                    }
                                    dataSheet.GetRange(10, startDynamicCol + ListEatingGroup.Count * 2, 10, startDynamicCol + ListEatingGroup.Count * 4 - 1).Merge();
                                    dataSheet.SetCellValue(10, startDynamicCol + ListEatingGroup.Count * 2, "Thực đơn bổ sung");
                                    listData.Add(dicData);
                                }
                            }
                        }
                    }
                }
                dataSheet.GetRange(food_firstRow + 3, 1, food_firstRow + 3 + countFood + countFoodGroup, lastcol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                food_lastRow = food_firstRow + 4 + countFood + countFoodGroup;
                IVTRange sumRange = firstSheet.GetRange(food_firstRow + 6, 1, food_firstRow + 6, lastCol);
                dataSheet.CopyPasteSameSize(sumRange, food_lastRow, 1);
                //Tính tổng cột thành tiền
                formular = "=SUM(E" + (food_firstRow + 3) + ":E" + (food_lastRow -1) + ")";
                dataSheet.SetFormulaValue("E" + food_lastRow, formular);
                for (int i = startDynamicCol; i < lastCol; i += 2)
                {
                    formular = "=SUM(" + VTVector.ColumnIntToString(i + 1) + (food_firstRow + 3) + ":" + VTVector.ColumnIntToString(i + 1) + (food_lastRow - 1) + ")";
                    dataSheet.SetFormulaValue(VTVector.ColumnIntToString(i + 1) + food_lastRow, formular);
                }
            }

            #endregion

            #region Thông tin chi tiêu
            int spentInfo_firstRow = food_lastRow + 3;
            IVTRange spentInfo_header = firstSheet.GetRange("A18", "D22");
            dataSheet.CopyPasteSameSize(spentInfo_header, spentInfo_firstRow, 1);
            IVTRange spentInfo_listService = firstSheet.GetRange("A23", "D23");
            IVTRange spentInfo_footer = firstSheet.GetRange("A24", "D28");
            List<Object> listService = new List<object>();
            int countService = 0;
            List<DishInspectionBO> ListOtherService = DishInspectionBusiness.GetListOtherService(SchoolID, date).ToList();
            foreach (DishInspectionBO dishInspection in ListOtherService)
            {
                Dictionary<string, object> serviceData = new Dictionary<string, object>();
                countService++;
                dataSheet.CopyPasteSameSize(spentInfo_listService, spentInfo_firstRow + countService + 4, 1);
                formular = "=C" + (spentInfo_firstRow + countService + 4) + "*B" + (spentInfo_firstRow + countService + 4);
                dataSheet.SetFormulaValue(spentInfo_firstRow + countService + 4, 4, formular);
                serviceData["ServiceName"] = dishInspection.OtherServiceName;
                serviceData["Number"] = dishInspection.NumberOfChildren;
                serviceData["Price"] = dishInspection.PriceService;
                listService.Add(serviceData);
            }
            dataSheet.GetRange(spentInfo_firstRow, 1, spentInfo_firstRow + 4 + ListOtherService.Count, 4).FillVariableValue(new Dictionary<string, object>
                    {
                        {"listServices",listService}
                    });
            dataSheet.CopyPasteSameSize(spentInfo_footer, spentInfo_firstRow + 5 + ListOtherService.Count, 1);
            //Tổng tiền dịch vụ
            if (ListOtherService.Count != 0)
            {
                formular = "=SUM(D" + (spentInfo_firstRow + 5) + ":D" + (spentInfo_firstRow + 4 + ListOtherService.Count) + ")";
                dataSheet.SetFormulaValue(spentInfo_firstRow + 4, 4, formular);
            }
            else
            {
                dataSheet.SetCellValue(spentInfo_firstRow + 4, 4, 0);
            }
            //Tiền thực phẩm
            formular = "=E" + (spentInfo_firstRow - 3);
            dataSheet.SetFormulaValue(spentInfo_firstRow + 3, 4, formular);

            //Định mức trong ngày
            int DailyPrice = DailyDishCostBusiness.MaxDailyDishCost(date, SchoolID);
            dataSheet.SetCellValue(spentInfo_firstRow + 6 + ListOtherService.Count, 2, NumberOfChildren);
            dataSheet.SetCellValue(spentInfo_firstRow + 6 + ListOtherService.Count, 3, DailyPrice);
            formular = "=C" + (spentInfo_firstRow + 6 + ListOtherService.Count) + "*B" + (spentInfo_firstRow + 6 + ListOtherService.Count);
            dataSheet.SetFormulaValue(spentInfo_firstRow + 6 + ListOtherService.Count, 4, formular);
            //Tiền chi trong ngày
            formular = "=D" + (spentInfo_firstRow + 3) + "+D" + (spentInfo_firstRow + 4);
            dataSheet.SetFormulaValue(spentInfo_firstRow + 2, 4, formular);

            #region	tính số dư đầu ngày numberModStartDay
            DateTime startMonth = Utils.StartOfMonth(date);
            DateTime endMonth = Utils.EndOfMonth(startMonth);
            IQueryable<DailyExpense> lstDailyExpense = DailyExpenseBusiness.GetExpenseDate(SchoolID, startMonth, endMonth);
            IQueryable<DishInspection> lstDishInspection = DishInspectionBusiness.GetInspectedDate(SchoolID, startMonth, endMonth);
            bool status = DailyExpenseBusiness.CheckExpenseDate(SchoolID, startMonth, endMonth);
            int numberModStartDay = 0;
            //DateTime startDte = date;
            DateTime startLastDte = new DateTime();
            if (date.Month != 1)
            {
                startLastDte = new DateTime(date.Year, date.Month - 1, 1);
            }
            else
            {
                startLastDte = new DateTime(date.Year - 1, 12, 1);
            }
            bool statusMonthLock = MonthlyLockBusiness.CheckMonthlyLock(SchoolID, startLastDte, Utils.EndOfMonth(startLastDte));

            if (lstDailyExpense != null && lstDailyExpense.Where(o => o.ExpenseDate == date).Count() > 0)
            {

                numberModStartDay = lstDailyExpense.Where(o => o.ExpenseDate == date).FirstOrDefault().BalanceStart;

            }
            else
            {
                // tinh so du dau ngay chuc nang tinh
                if (status)
                {
                    if (!statusMonthLock)
                    {
                        IQueryable<DailyExpense> lstDailyExpenseLastMonth = DailyExpenseBusiness.GetExpenseDate(SchoolID, startLastDte, Utils.EndOfMonth(startLastDte));
                        IQueryable<DailyExpense> list = lstDailyExpenseLastMonth.OrderByDescending(o => o.ExpenseDate);
                        if (list != null && list.Count() > 0)
                            numberModStartDay = list.FirstOrDefault().BalanceEnd;

                    }
                    else
                    {
                        numberModStartDay = MonthlyBalanceBusiness.GetMonthylyBalance(SchoolID, startLastDte, Utils.EndOfMonth(startLastDte));
                    }


                }
                else
                {
                    IQueryable<DailyExpense> list = lstDailyExpense.Where(o => o.ExpenseDate < date).OrderByDescending(o => o.ExpenseDate);
                    if (list == null)
                    {

                        numberModStartDay = MonthlyBalanceBusiness.GetMonthylyBalance(SchoolID, startLastDte, Utils.EndOfMonth(startLastDte));
                    }
                    else if (list.Count() > 0)
                    {
                        numberModStartDay = list.FirstOrDefault().BalanceEnd;
                    }
                }
            }
            #endregion
            //Số dư đầu ngày
            dataSheet.SetCellValue(spentInfo_firstRow + 7 + ListOtherService.Count, 4, numberModStartDay);
            //Định mức luỹ kế kể từ đầu tháng
            formular = "=D" + (spentInfo_firstRow + 6 + ListOtherService.Count) + "+D" + (spentInfo_firstRow + 7 + ListOtherService.Count);
            dataSheet.SetFormulaValue(spentInfo_firstRow + 8 + ListOtherService.Count, 4, formular);
            //Số dư cuối ngày
            formular = "=D" + (spentInfo_firstRow + 8 + ListOtherService.Count) + "-D" + (spentInfo_firstRow + 2);
            dataSheet.SetFormulaValue(spentInfo_firstRow + 9 + ListOtherService.Count, 4, formular);

            #endregion
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}