﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using System.Web.Security;
using System.Transactions;
using SMAS.Business.Common;
using System.Security.Cryptography;
//using SMAS.Business.Common.Utils;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common.Util;

namespace SMAS.Business.Business
{
    /// <summary>
    ///UserAccountBusiness
    /// <Author>dungnt chinh sua</Author>
    /// <DateTime>16/10/2012</DateTime>
    /// </summary>
    public partial class UserAccountBusiness
    {
        private const int ROLEID_SO = 8;
        private const int ROLEID_PHONG = 16;

        /// <summary>
        /// lay len UserAccount theo GUID
        /// </summary>
        /// <author> hungnd8 18/07/2013</author>
        /// <param name="GUID"></param>
        /// <returns>UserAccount</returns>
        public UserAccount GetUserByProviderUserKey(Guid GUID)
        {
            return UserAccountRepository.All.Where(u => u.GUID == GUID).FirstOrDefault();
        }

        /// <summary>
        /// Tìm kiếm danh sách UserAccount được tạo bởi một user 
        /// </summary>
        /// <param name="creatorID"></param>
        /// <returns>Danh sách user được tạo bởi user đầu vào. Trả về null nếu không tìm thấy user thỏa mãn</returns>
        public IQueryable<UserAccount> GetUsersCreatedBy(int creatorID)
        {
            //creatorID: require
            Utils.ValidateRequire(creatorID, "UserAccount_Label_UserAccountID");
            if (creatorID == 0)
            {
                return null;
            }
            else
            {
                return UserAccountRepository.All.Where(u => u.CreatedUserID == creatorID && u.IsActive == true);
            }
        }

        /// <summary>
        /// Tìm kiếm danh sách UserAccount admin được tạo bởi một user 
        /// </summary>
        /// <param name="creatorID"></param>
        /// <returns>Danh sách user admin được tạo bởi user đầu vào. Trả về null nếu không tìm thấy user thỏa mãn</returns>
        public IQueryable<UserAccount> GetUsersAdminCreatedBy(int creatorID)
        {
            ValidateExist(creatorID);
            if (creatorID == 0) return null;
            else
            {
                return UserAccountRepository.All.Where(u => u.CreatedUserID == creatorID && u.IsAdmin == true && u.IsActive == true);
            }
        }

        /// <summary>
        /// Tìm kiếm danh sách UserAccount thường được tạo bởi một user 
        /// </summary>
        /// <param name="creatorID"></param>
        /// <returns>Danh sách user thường được tạo bởi user đầu vào. Trả về null nếu không tìm thấy user thỏa mãn</returns>
        public IQueryable<UserAccount> GetUsersNormalCreatedBy(int creatorID)
        {
            ValidateExist(creatorID);
            if (creatorID == 0) return null;
            else
            {
                return UserAccountRepository.All.Where(u => u.CreatedUserID == creatorID && u.IsAdmin == false && u.IsActive == true);
            }
        }

        /// <summary>
        /// Tìm kiếm danh sách UserAccount là người vận hành hệ thống Viettel
        /// </summary>
        /// <returns>Danh sách người vận hành hệ thống Viettel</returns>
        public IQueryable<UserAccount> GetOperationalUsers()
        {
            return UserAccountRepository.All.Where(u => u.IsOperationalUser == true && u.IsActive == true);
        }


        /// <summary>
        /// Ánh xạ đối tượng từ UserAccount sang Employee
        /// </summary>
        /// <param name="userAccountID"></param>
        /// <returns>Employee tương ứng với UserAccount</returns>
        public Employee MapFromUserAccountToEmployee(int userAccountID)
        {
            //UserAccount: not Exist or not active
            ValidateExist(userAccountID);
            if (userAccountID == 0)
            {
                return null;
            }
            UserAccount user = this.UserAccountRepository.Find(userAccountID);
            if (user == null)
            {
                return null;
            }
            Employee employee = EmployeeBusiness.Find(user.EmployeeID);
            return employee;
        }

        /// <summary>
        /// Ánh xạ đối tượng từ Employee sang UserAccount
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns>UserAccount tương ứng với Employee</returns>
        public UserAccount MapFromEmployeeToUserAccount(int employeeID)
        {
            if (employeeID == 0)
            {
                return null;
            }
            //EmployeeID: not Exist
            EmployeeBusiness.CheckAvailable(employeeID, "Employee_Label_EmployeeID");
            //Thực hiện tìm kiếm trên bảng UserAccount theo tham số EmployeeID và isActive
            IQueryable<UserAccount> lsUser = UserAccountRepository.All.Where(o => (o.EmployeeID == employeeID)).Where(o => (o.IsActive == true));
            if (lsUser.Count() == 0)
            {
                return null;
            }
            else
            {
                return lsUser.FirstOrDefault();
            }
        }

        /// <summary>
        /// Chuyển account sang trạng thái không hoạt động, dùng trong trường hợp người dùng không chọn kích hoạt tài khoản
        /// Confirm HungND 01/04/2013
        /// </summary>
        /// <param name="userAccountID"></param>
        /// <returns>Có chuyển thành công hay không</returns>
        public bool DeActiveAccount(int userAccountID)
        {
            if (userAccountID == 0) return false;
            //UserAccountID not Exist
            ValidateExist(userAccountID);
            UserAccount acc = this.Find(userAccountID);
            if (acc == null)
            {
                return false;
            }
            aspnet_Membership usermb = new aspnet_MembershipRepository(this.context).All.Where(o => o.UserId == acc.GUID).FirstOrDefault();
            if (usermb == null)
            {
                return false;
            }
            else
            {
                usermb.IsApproved = 0;
                new aspnet_MembershipRepository(this.context).Update(usermb);
                new aspnet_MembershipRepository(this.context).Save();
                return (true);
            }
        }

        /// <summary>
        /// Chuyển account sang trạng thái hoạt động. Khi người dùng chọn kích hoạt tài khoản
        /// Confirm HungND 01/04/2013
        /// </summary>
        /// <param name="userAccountID"></param>
        /// <returns>Có chuyển thành công hay không</returns>
        public bool ActiveAccount(int userAccountID)
        {
            if (userAccountID == 0) return false;
            //UserAccountID not Exist
            this.CheckAvailable(userAccountID, "SchoolProfile_Label_AdminID", false);

            UserAccount acc = this.Find(userAccountID);
            if (acc == null)
            {
                return false;
            }
            aspnet_Membership usermb = new aspnet_MembershipRepository(this.context).All.Where(o => o.UserId == acc.GUID).FirstOrDefault();
            if (usermb == null)
            {
                return false;
            }
            else
            {
                usermb.IsApproved = 1;
                new aspnet_MembershipRepository(this.context).Update(usermb);
                new aspnet_MembershipRepository(this.context).Save();
                return (true);
            }
        }

        /// <summary>
        /// Kiểm tra tài khoản có phải nhân viên vận hành khai thác Viettel không
        /// </summary>
        /// <param name="userAccountID"></param>
        /// <returns></returns>
        public bool IsOperationalUser(int userAccountID)
        {
            if (userAccountID == 0) return false;
            //UserAccountID not Exist
            ValidateExist(userAccountID);

            UserAccount uc = UserAccountRepository.All.Where(u => u.UserAccountID == userAccountID && u.IsActive == true).FirstOrDefault();
            if (uc == null) return false;
            return uc.IsOperationalUser;
        }

        /// <summary>
        /// Kiểm tra User có phải là admin không
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public bool IsAdmin(int UserAccountID)
        {
            if (UserAccountID == 0) return false;
            //UserAccountID not Exist
            UserAccount userAccount = this.All.Where(o => o.UserAccountID == UserAccountID).FirstOrDefault();
            if (userAccount == null)
            {
                throw new BusinessException("UserAccount_Label_UserAccountID");
            }
            if (!userAccount.IsActive)
            {
                return false;
            }
            return userAccount.IsAdmin;
        }

        /// <summary>
        /// Kiểm tra User có phải là admin trường
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public bool IsSchoolAdmin(int UserAccountID)
        {
            if (UserAccountID == 0) return false;
            //UserAccountID not Exist
            UserAccount userAccount = this.All.Where(o => o.UserAccountID == UserAccountID).FirstOrDefault();
            if (userAccount == null)
            {
                throw new BusinessException("UserAccount_Label_UserAccountID");
            }
            IQueryable<Role> lstRole = UserRoleBusiness.GetRolesOfUser(UserAccountID);
            Role role = lstRole.FirstOrDefault();
            if (role != null && RoleBusiness.IsSchoolRole(role.RoleID))
            {
                if (!userAccount.IsActive)
                {
                    return false;
                }
                return userAccount.IsAdmin;
            }
            return false;
        }

        /// <summary>
        /// Kiểm tra User có phải là admin sở phòng
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public bool IsSupervisingDeptAdmin(int UserAccountID)
        {
            if (UserAccountID == 0) return false;
            //UserAccountID not Exist
            UserAccount userAccount = this.All.Where(o => o.UserAccountID == UserAccountID).FirstOrDefault();
            if (userAccount == null)
            {
                throw new BusinessException("UserAccount_Label_UserAccountID");
            }
            IQueryable<Role> lstRole = UserRoleBusiness.GetRolesOfUser(UserAccountID);
            Role role = lstRole.FirstOrDefault();

            if (role != null && RoleBusiness.IsSuperVisingDeptRole(role.RoleID))
            {
                if (!userAccount.IsActive)
                {
                    return false;
                }
                return userAccount.IsAdmin;
            }
            return false;
        }


        /// <summary>
        /// Tìm kiếm người dùng
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<UserAccount> SearchUser(IDictionary<string, object> dic)
        {
            string username = Utils.GetString(dic, "UserName");
            string fullname = Utils.GetString(dic, "FullName");
            bool? genre = Utils.GetNullableBool(dic, "Genre");
            int schoolFacutyID = Utils.GetInt(dic, "SchoolFacutyID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int groupCat = Utils.GetInt(dic, "GroupCatID");
            //bool? status = Utils.GetNullableBool(dic, "Status");
            int roleID = Utils.GetInt(dic, "RoleID");
            int createdUserID = Utils.GetInt(dic, "CreatedUserID");
            int userAccountID = Utils.GetInt(dic, "UserAccountID");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            bool? isAdmin = Utils.GetNullableBool(dic, "IsAdmin");
            bool? isOperationalUser = Utils.GetNullableBool(dic, "IsOperationalUser");
            DateTime? createdDate = Utils.GetDateTime(dic, "CreatedDate");
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            bool isNewAccount = Utils.GetBool(dic, "isNewAccount", false);
            List<int> lstUserAccountID = Utils.GetIntList(dic, "lstUserAccountID");
            Guid? GUID = null;
            if (dic.ContainsKey("GUID"))
            {
                GUID = (Guid)dic["GUID"];
            }

            IQueryable<UserAccount> query = this.UserAccountRepository.All;
            var lsUserAccount = from e in query
                                join em in EmployeeBusiness.All on e.EmployeeID equals em.EmployeeID into g1
                                from j1 in g1.DefaultIfEmpty()
                                select new { e, j1.SchoolID, j1.Genre, j1.FullName, j1.SchoolFacultyID };


            if (GUID.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.GUID == GUID));
            }
            if (isOperationalUser.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.IsOperationalUser == true));
            }
            if (isAdmin.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.IsAdmin == true));
            }
            if (createdDate.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.CreatedDate == createdDate));
            }
            if (EmployeeID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.EmployeeID == EmployeeID));
            }

            #region old
            if (isActive.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.IsActive == true));
            }
            if (schoolID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.SchoolID == schoolID));
            }
            if (roleID != 0)
            {
                IQueryable<int> listGroup = this.UserGroupRepository.All.Where(G => G.GroupCat.RoleID == roleID).Select(o => o.UserID);
                lsUserAccount = from u in lsUserAccount.Where(o => listGroup.Contains(o.e.UserAccountID))
                                select u;

                //lsUserAccount = lsUserAccount.Where(o => o == o.GroupCats.Where(u => u.RoleID == roleID));

            }
            //if (status.HasValue)
            //{
            //    lsUserAccount = lsUserAccount.Where(em => (em.e.aspnet_Users.aspnet_Membership.IsApproved == (status.Value ? 1 : 0)));
            //}
            if (genre.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.Genre == genre));
            }
            if (username.Trim().Length != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.aspnet_Users.UserName.ToUpper().Contains(username.ToUpper())));
            }
            if (fullname.Trim().Length != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.FullName.ToUpper().Contains(fullname.ToUpper())));
            }
            if (schoolFacutyID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.SchoolFacultyID == schoolFacutyID));
            }
            //if (groupCat != 0)
            //{
            //    lsUserAccount = lsUserAccount.Where(em => em.e.UserGroups.Count() > 0);
            //    lsUserAccount = lsUserAccount.Where(em => (em.e.UserGroups.FirstOrDefault().GroupID == groupCat));
            //}
            #endregion

            if (createdUserID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.CreatedUserID == createdUserID));
            }
            if (userAccountID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.e.UserAccountID == userAccountID));
            }
            if (isNewAccount)
            {
                lsUserAccount = lsUserAccount.Where(em => !em.e.FirstLoginDate.HasValue);
            }
            if (lstUserAccountID.Count > 0)
            {
                lsUserAccount = lsUserAccount.Where(em => lstUserAccountID.Contains(em.e.UserAccountID));
            }

            return lsUserAccount.Select(o => o.e);
        }

        /// <summary>
        /// Tìm kiếm người dùng phòng ban
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<UserAccount> SearchUserByDept(IDictionary<string, object> dic)
        {
            int createdUserID = Utils.GetInt(dic, "CreatedUserID");
            int userAccountID = Utils.GetInt(dic, "UserAccountID");
            //bool? isActive = Utils.GetIsActive(dic, "IsActive");
            bool? isAdmin = Utils.GetNullableBool(dic, "IsAdmin");
            bool? isOperationalUser = Utils.GetNullableBool(dic, "IsOperationalUser");
            DateTime? createdDate = Utils.GetDateTime(dic, "CreatedDate");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            bool isNewAccount = Utils.GetBool(dic, "isNewAccount", false);
            List<int> lstUseraccountID = Utils.GetIntList(dic, "lstUserAccountID");

            IQueryable<UserAccount> lsUserAccount = this.UserAccountRepository.All;
            if (isOperationalUser.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.IsOperationalUser == true));
            }
            if (createdDate.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.CreatedDate == createdDate));
            }

            //anh hungnd viet
            if (SupervisingDeptID != 0 && isAdmin != null && isAdmin.Value)
            {
                lsUserAccount = SupervisingDeptBusiness.All.Where(su => su.SupervisingDeptID == SupervisingDeptID).Select(su => su.UserAccount);

            }
            if (SupervisingDeptID != 0 && (isAdmin == null || !isAdmin.Value))
            {
                //Hungnd 23/04/2013 Fix loi lay len danh sach nguoi dung thuoc so/ Phong voi employee type
                lsUserAccount = lsUserAccount.Where(em => em.Employee.SupervisingDeptID == SupervisingDeptID
                    && em.Employee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF);
            }
            //

            if (createdUserID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.CreatedUserID == createdUserID));
            }
            if (userAccountID != 0)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.UserAccountID == userAccountID));
            }
            if (isAdmin.HasValue)
            {
                lsUserAccount = lsUserAccount.Where(em => (em.IsAdmin == true));
            }
            if (isNewAccount)
            {
                lsUserAccount = lsUserAccount.Where(em => !em.FirstLoginDate.HasValue);
            }
            if (lstUseraccountID.Count > 0)
            {
                lsUserAccount = lsUserAccount.Where(em => lstUseraccountID.Contains(em.UserAccountID));
            }
            return lsUserAccount;
        }

        /// <summary>
        /// Xoá người dùng khỏi hệ thống
        /// </summary>
        /// <modifier date="13/11/2014">AnhVD9</modifier>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public bool DeleteAccount(int UserAccountID)
        {
            ValidateExist(UserAccountID);
            using (TransactionScope sc = new TransactionScope())
            {
                UserAccount userAccount = this.Find(UserAccountID);
                if (userAccount != null)
                {
                    if (userAccount.aspnet_Users != null)
                    {
                        var user = aspnet_UsersBusiness.All.FirstOrDefault(o => o.UserName == userAccount.aspnet_Users.UserName);
                        if (user != null)
                        {
                            string delUserName = string.Format("{0}_{1}", user.UserName, "delete");
                            user.UserName = delUserName;
                            user.LoweredUserName = delUserName.ToLower();
                            user.LastActivityDate = DateTime.Now;
                            aspnet_UsersBusiness.Update(user);
                            aspnet_UsersBusiness.Save();

                            var userms = aspnet_MembershipBusiness.All.FirstOrDefault(o => o.UserId == userAccount.aspnet_Users.UserId);
                            if (userms != null)
                            {
                                userms.IsApproved = 0;
                                aspnet_MembershipBusiness.Update(userms);
                                aspnet_MembershipBusiness.Save();
                            }
                        }
                    }
                    userAccount.IsActive = false;
                    userAccount.EmployeeID = null;

                    // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh Synchronize = -3
                    userAccount.SynchronizeID = GlobalConstants.STATUS_DELETE_USER;

                    this.Update(userAccount);
                    this.Save();
                    sc.Complete();
                    return true;
                }
                else return false;

            }
        }

        /// <summary>
        /// Tạo user phòng sở
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="CreatedUserID"></param>
        /// <param name="IsAdmin"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public int? CreateDepartmentUser(string userName, int CreatedUserID, bool IsAdmin, int? EmployeeID, int RoleID, bool isActive = true, string password = "")
        {
            //Gọi hàm UserAccount.CreateUser(username, new GUID(), CreatedUserID, IsAdmin, EmployeeID ) 
            //trả về ID của user (userID)
            MembershipCreateStatus createStatus;
            int? UserID = CreateUser(userName, CreatedUserID, IsAdmin, EmployeeID, out createStatus, password);
            //  Thực hiện thêm mới vào bảng UserRole với các tham số 
            //  UserID = userID (được thực hiện thành công khi gọi hàm CreateUser())
            //  RoleID = 8 ()
            if (UserID.HasValue)
            {
                UserRole userRole = new UserRole();
                userRole.UserID = UserID.Value;
                userRole.RoleID = RoleID;
                UserRoleBusiness.Insert(userRole);
                UserRoleBusiness.Save();
            }

            return UserID;
        }

        /// <summary>
        /// Thay đổi username tương ứng với UserAccountID
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="newUserName"></param>
        /// <param name="isResetPass"></param>
        /// <param name="employeeID"></param>
        /// <param name="isActive"></param>
        public void UpdateUser(int UserAccountID, string newUserName, bool isResetPass, int? employeeID, bool isActive, string password = "")
        {
            ValidateExist(UserAccountID);
            UserAccount uc = this.All.Where(o => o.aspnet_Users.UserName.ToLower() == newUserName.ToLower() && o.UserAccountID != UserAccountID).FirstOrDefault();
            if (uc != null)
            {
                throw new BusinessException("SchoolAccount_Label_DuplicateUserName");
            }
            UserAccount account = this.Find(UserAccountID);
            string oldUserName = account.aspnet_Users == null ? "" : account.aspnet_Users.UserName;
            bool CompleteTransaction = false;
            using (TransactionScope scope = new TransactionScope())
            {
                //lay useraccount theo UserAccountID
                if (account == null || account.GUID == null)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }

                if (account == null || account.GUID == null) throw new BusinessException("Common_Validate_NotAvailable");

                //lay ID cua aspnet_user
                Guid GUID = account.GUID.Value;
                aspnet_Users membershipUser = aspnet_UsersBusiness.getUsersById(GUID);
                if (membershipUser == null)
                {
                    throw new BusinessException("Validate_MemberShip_SuperDept");
                }
                if (newUserName == membershipUser.UserName)
                {
                    account.EmployeeID = employeeID;
                    account.aspnet_Users.aspnet_Membership.IsApproved = isActive ? 1 : 0;

                }
                else
                {

                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["UserName"] = newUserName;
                    //bool compatible = aspnet_UsersRepository.ExistsRow(GlobalConstants.DBO_SCHEMA, "aspnet_Users", SearchInfo, null);
                    bool compatible = aspnet_UsersRepository.All.Where(o => o.UserName == newUserName && o.UserName.Length == newUserName.Length).Count() > 0 ? true : false;
                    if (compatible)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("SchoolProfile_Label_UserName");
                        throw new BusinessException("Common_Validate_Duplicate", Params);
                    }
                    membershipUser.UserName = newUserName;
                    membershipUser.LoweredUserName = newUserName.ToLower();
                    aspnet_UsersBusiness.Update(membershipUser);
                    aspnet_UsersBusiness.Save();
                    oldUserName = newUserName;
                    account.EmployeeID = employeeID;
                    account.aspnet_Users.aspnet_Membership.IsApproved = isActive ? 1 : 0;
                }

                if (isResetPass)
                {
                    account.FirstLoginDate = null;
                }
                this.Update(account);
                this.Save();

                CompleteTransaction = true;
                scope.Complete();
            }
            if (CompleteTransaction)
            {
                MembershipUser mu = Membership.GetUser(oldUserName);
                if (mu != null)
                {
                    if (isResetPass)
                    {
                        ResetPassword(UserAccountID, password);
                    }
                    mu.IsApproved = isActive;
                    Membership.UpdateUser(mu);
                }
            }
        }

        /// <summary>
        /// Tạo tài khoản sở phòng
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="CreatedUserID"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public int? CreateAdminSupervisingDept(string userName, int CreatedUserID, int RoleID, bool isActive = true, string password = "")
        {
            int? UserID = CreateDepartmentUser(userName, CreatedUserID, true, null, RoleID, isActive, password);
            //Thực hiện Gọi hàm DefaultGroupBusiness.Search() với Dictionary[“RoleID”] = 8 
            //(RoleID tương ứng với cấp Phòng/Sở) => Danh sách các bản ghi lstDefaultGroup.
            IQueryable<DefaultGroup> lstDefaultGroup = DefaultGroupBusiness.Search(new Dictionary<string, object>()
            {
                {"List<RoleID>", new List<int>(){RoleID}},
                {"IsActive",true}
            });
            List<int> lsDefaultGroupID = lstDefaultGroup.Select(o => o.DefaultGroupID).ToList();
            List<DefaultGroupMenu> lstDefaultGroupMenu = DefaultGroupMenuBusiness.All.Where(o => lsDefaultGroupID.Contains(o.DefaultGroupID)).ToList();
            // DungVA - 03/04/2013
            foreach (var item in lstDefaultGroup)
            {
                GroupCat groupcat = new GroupCat();
                groupcat.RoleID = RoleID;
                groupcat.CreatedUserID = CreatedUserID;
                groupcat.GroupName = item.DefaultGroupName;
                groupcat.Description = item.Description;
                groupcat.CreatedDate = DateTime.Now;
                groupcat.IsActive = true;
                groupcat.AdminUserID = UserID.Value;

                IEnumerable<DefaultGroupMenu> lstDGM = lstDefaultGroupMenu.Where(o => o.DefaultGroupID == item.DefaultGroupID);
                foreach (DefaultGroupMenu dgm in lstDGM)
                {
                    GroupMenu gm = new GroupMenu();
                    gm.MenuID = dgm.MenuID;
                    gm.Permission = dgm.Permission;
                    groupcat.GroupMenus.Add(gm);
                }

                GroupCatBusiness.InsertGroup(groupcat);
            }
            return UserID;
        }

        //TODO:dungnt lam sau --nvu like shit
        public int? CreateAdminSchool(string userName, int CreatedUserID, List<int> lstRole, int roleID, string password = "")
        {
            MembershipCreateStatus status;
            int? UserAccountID = this.CreateUser(userName, CreatedUserID, true, null, out status, password);

            if (!UserAccountID.HasValue || UserAccountID.Value == 0)
                return null;

            if (!UserRoleBusiness.AssignRolesForAdmin(CreatedUserID, lstRole, UserAccountID.Value, roleID))
                return null;

            //Thực hiện việc for các phần tử trong lstRole.
            foreach (int role in lstRole)
            {
                //Lấy danh sách các bản ghi lstDefaultGroup.
                Dictionary<string, object> SearchRole = new Dictionary<string, object>();
                SearchRole["RoleID"] = role;
                List<DefaultGroup> lstDefaultGroup = DefaultGroupBusiness.Search(SearchRole).ToList();

                //Lấy danh sách các bản ghi DefaultGroupMenu
                List<int> lstDefaultGroupID = lstDefaultGroup.Select(u => u.DefaultGroupID).ToList();
                List<DefaultGroupMenu> lstDefaultGroupMenu = DefaultGroupMenuBusiness.All.Where(u => lstDefaultGroupID.Contains(u.DefaultGroupID)).ToList();

                foreach (DefaultGroup df in lstDefaultGroup)
                {
                    List<DefaultGroupMenu> lsGroupMenu = lstDefaultGroupMenu.Where(o => o.DefaultGroupID == df.DefaultGroupID).ToList();

                    GroupCat g = new GroupCat();
                    g.RoleID = role;
                    g.CreatedUserID = CreatedUserID;
                    g.GroupName = df.DefaultGroupName;
                    g.Description = df.Description;
                    g.CreatedDate = DateTime.Now;
                    g.IsActive = true;
                    g.AdminUserID = UserAccountID.Value;

                    foreach (DefaultGroupMenu dfm in lsGroupMenu)
                    {
                        GroupMenu gm = new GroupMenu();
                        gm.MenuID = dfm.MenuID;
                        gm.Permission = dfm.Permission;
                        g.GroupMenus.Add(gm);
                    }
                    GroupCatBusiness.Insert(g);
                }
            }

            return UserAccountID;
        }

        public void ResetPassword(int UserAccountID, string newPassword = "")
        {
            //Lấy ra UserAccount  tương ứng với UserAccountID, 
            UserAccount user = UserAccountBusiness.Find(UserAccountID);

            if (user != null && user.aspnet_Users != null && user.aspnet_Users.aspnet_Membership != null)
            {
                if (string.IsNullOrEmpty(newPassword))
                {
                    newPassword = CreateRandomPassword(SystemParamsInFile.MAX_PASS_LENTGTH);
                }
                //Thực hiện gọi hàm 
                MembershipUser mu = Membership.GetUser(user.aspnet_Users.UserName);

                string oldPass = string.Empty;
                // AnhVD9 - Kiểm tra trường hợp LoweredUserName khác UserName
                if (mu == null)
                    user.aspnet_Users.LoweredUserName = user.aspnet_Users.UserName;
                else
                {
                    if (mu.IsLockedOut) mu.UnlockUser();
                    oldPass = mu.ResetPassword();
                    mu.ChangePassword(oldPass, newPassword);
                    //Thuc hien unlock va set lai lan dang nhap dau tien
                    user.aspnet_Users.aspnet_Membership.IsLockedOut = 0;
                    user.FirstLoginDate = null;
                }

                // QuangNN2 - 20/05/2015 - Cap nhat thuoc tinh SynchronizeID = -4
                user.SynchronizeID = GlobalConstants.STATUS_CHANGE_PASSWORD;
                OAuth2.ChangePass(user.aspnet_Users.UserName, oldPass, newPassword);
                UserAccountBusiness.Update(user);
                UserAccountBusiness.Save();
            }
        }

        public bool IsSystemAdmin(int UserAccountID)
        {
            ValidateExist(UserAccountID);
            //Thực hiện tìm kiếm role của User thông qua lstUserRole = UserRole.GetRoleOfUser(UserAccountID)
            IQueryable<Role> lstUserRole = UserRoleBusiness.GetRolesOfUser(UserAccountID);
            lstUserRole = lstUserRole.Where(o => (o.RoleID == GlobalConstants.ROLE_ADMIN_CAPCAO));
            if (lstUserRole.Count() == 0)
            {
                return false;
            }
            //Thực hiện tìm kiếm trên bảng UserAccount theo tham số UserAccountID và isActive
            UserAccount user = UserAccountRepository.All.Where(o => (o.UserAccountID == UserAccountID)).Where(o => (o.IsActive == true)).FirstOrDefault();
            if (user == null)
            {
                return false;
            }
            return user.IsAdmin;

        }

        /// <summary>
        /// update UserName theo UserAccountID
        /// </summary>
        /// <param name="UserAccountID">UserAccountID cua doi tuong can cap nhat</param>
        /// <param name="newUserName">username moi</param>
        /// <returns></returns>
        public void UpdateUser(int UserAccountID, string newUserName)
        {

            if (UserAccountID == 0 || string.IsNullOrEmpty(newUserName)) throw new BusinessException("Common_Validate_NotAvailable");
            //lay useraccount theo UserAccountID
            UserAccount account = this.Find(UserAccountID);

            if (account == null || account.GUID == null) throw new BusinessException("Common_Validate_NotAvailable");

            //lay ID cua aspnet_user
            Guid GUID = account.GUID.Value;
            aspnet_Users membershipUser = aspnet_UsersBusiness.getUsersById(GUID);
            if (membershipUser == null || newUserName == membershipUser.UserName)
            {
                return;
            }
            else
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["UserName"] = newUserName;
                bool compatible = aspnet_UsersRepository.All.Where(o => o.UserName == newUserName && o.UserName.Length == newUserName.Length).Count() > 0 ? true : false;
                if (compatible) //Kiem tra neu tai khoan moi da co thi gan 
                {
                    //List<object> Params = new List<object>();
                    //Params.Add("SchoolProfile_Label_UserName");
                    //throw new BusinessException("Common_Validate_Duplicate", Params);
                    newUserName = newUserName + "_" + account.UserAccountID;
                }
                membershipUser.UserName = newUserName;
                membershipUser.LoweredUserName = newUserName.ToLower();
                aspnet_UsersBusiness.Update(membershipUser);
                //aspnet_UsersBusiness.Save();
            }
        }

        //public IQueryable<UserAccount> SearchUserByDept(IDictionary<string, object> dic)
        //{
        //    string username = Utils.GetString(dic, "UserName");
        //    string fullname = Utils.GetString(dic, "FullName");
        //    int dept = Utils.GetInt(dic, "SupervisingDeptID");
        //    int createdUserID = Utils.GetInt(dic, "CreatedUserID");
        //    int userAccountID = Utils.GetInt(dic, "UserAccountID");
        //    bool? isActive = Utils.GetIsActive(dic, "IsActive");
        //    bool? isAdmin = Utils.GetNullableBool(dic, "IsAdmin");
        //    bool? isOperationalUser = Utils.GetNullableBool(dic, "IsOperationalUser");
        //    DateTime? createdDate = Utils.GetDateTime(dic, "CreatedDate");
        //    IQueryable<UserAccount> lsUserAccount = this.UserAccountRepository.All;

        //    if (isAdmin.HasValue)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.IsAdmin == true));
        //    }
        //    if (isActive.HasValue)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.IsActive == true));
        //    }
        //    if (isOperationalUser.HasValue)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.IsOperationalUser == true));
        //    }
        //    if (createdDate.HasValue)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.CreatedDate == createdDate));
        //    }
        //    if (username.Trim().Length != 0)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.aspnet_Users.UserName.ToUpper().Contains(username.ToUpper())));
        //    }
        //    if (fullname.Trim().Length != 0)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.Employee.FullName.ToUpper().Contains(fullname.ToUpper())));
        //    }
        //    if (dept != 0)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.Employee.SupervisingDeptID == dept));
        //    }
        //    if (createdUserID != 0)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.CreatedUserID == createdUserID));
        //    }
        //    if (userAccountID != 0)
        //    {
        //        lsUserAccount = lsUserAccount.Where(em => (em.UserAccountID == userAccountID));
        //    }
        //    return lsUserAccount;
        //}

        /// <summary>
        /// create useraccount 
        /// <modifider>HaiVT 04/06/2013 - them truong hop phhs</modifider>        
        /// </summary>        
        public int? CreateUser(string userName, int CreatedUserID, bool IsAdmin, int? EmployeeID, out MembershipCreateStatus createStatus, string password = "", bool? isParrent = null)
        {
            MembershipCreateStatus createStatusIn;
            const int PasswordLength = 8;
            if (string.IsNullOrEmpty(password))
            {
                password = CreateRandomPassword(PasswordLength);
            }
            int? userID;
            Membership.CreateUser(userName, password, null, null, null, true, null, out createStatusIn);

            if (createStatusIn == MembershipCreateStatus.Success)
            {
                //Sau khi tao thanh cong trong bang aspnet_User thi tao UserAccount
                MembershipUser createdUser = Membership.GetUser(userName);

                Guid aspnet_userID = (Guid)createdUser.ProviderUserKey;// lay id cua aspnet_User vua duoc tao
                //
                UserAccount userAccount = new UserAccount();
                userAccount.GUID = aspnet_userID;
                userAccount.EmployeeID = EmployeeID;
                userAccount.CreatedUserID = CreatedUserID;
                userAccount.IsAdmin = IsAdmin;
                userAccount.IsActive = true;
                userAccount.CreatedDate = DateTime.Now;
                if (isParrent.HasValue && isParrent.Value)
                {
                    // userAccount.IsParent = isParrent;
                }

                // QuangNN2 - 13/-5/2015 - Khi thêm tài khoản thì set SynchronizeID của UserAccount = -1
                userAccount.SynchronizeID = GlobalConstants.STATUS_CREATE_USER;

                this.Insert(userAccount);
                this.Save();

                AddUserSSO(EmployeeID, userAccount.UserAccountID, userName, password);

                userID = userAccount.UserAccountID;
                createStatus = createStatusIn;



                return userID;
            }
            else
            {
                if (createStatusIn.ToString().Contains("DuplicateUserName"))
                {
                    throw new BusinessException("Common_Validate_DuplicateAdmin");
                }
                throw new BusinessException(createStatusIn.ToString());
            }
        }

        #region cu
        public List<UserAccount> GetUsersByCreator(int creatorID, bool isAdmin)
        {
            List<UserAccount> lsAcc = UserAccountRepository.All.Where(ur => ur.CreatedUserID == creatorID && ur.IsAdmin == isAdmin && ur.IsActive == true).ToList();
            return lsAcc;
        }



        public List<CustomUserAcc> MapFromUserAccountToCustomUserAcc(List<UserAccount> lsAcc)
        {
            List<CustomUserAcc> listCustomUserAcc = new List<CustomUserAcc>();
            foreach (UserAccount ua in lsAcc)
            {
                CustomUserAcc cac = new CustomUserAcc();
                cac.UserName = ua.aspnet_Users.UserName;
                cac.UserAccountID = ua.UserAccountID;
                listCustomUserAcc.Add(cac);
            }
            return listCustomUserAcc;
        }
        #endregion

        #region ho tro

        /// <summary>
        /// kiem tra ID co ton tai trong bang useraccount
        /// </summary>
        /// <param name="ID"></param>
        private void ValidateExist(int ID)
        {
            if (this.All.Where(o => o.UserAccountID == ID).Count() == 0)
            {
                throw new BusinessException("UserAccount_Label_UserAccountID");
            }
        }
        #endregion

        /// <summary>
        /// HungND 22/03/2013
        /// Generate pass with length
        /// </summary>
        /// <param name="PasswordLength"></param>
        /// <returns></returns>
        public string CreateRandomPassword(int PasswordLength)
        {
            //string _allowedChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[]!@$%^&*()";
            //Random randNum = new Random();
            //char[] chars = new char[PasswordLength];
            //int allowedCharCount = _allowedChars.Length;
            //for (int i = 0; i < PasswordLength; i++)
            //{
            //    //chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            //    //chars[i] = _allowedChars[(int)(randNum.Next(_allowedChars.Length))];
            //    chars[i] = _allowedChars[randNum.Next(_allowedChars.Length)];
            //}
            //return new string(chars);
            return Utils.GenPassRandom(PasswordLength);
        }

        /// <summary>
        /// Change with user pass
        /// Hungnd 12/04/2013
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="NormalPass"></param>
        /// <returns></returns>
        public bool ChangePassword(int UserAccountID, string NormalPass)
        {
            //Lấy ra UserAccount  tương ứng với UserAccountID, 
            UserAccount user = UserAccountRepository.Find(UserAccountID);
            aspnet_Users aspnetUser = user.aspnet_Users;
            if (user != null)
            {
                string oldPass = string.Empty;
                //Thực hiện gọi hàm 
                MembershipUser mu = Membership.GetUser(aspnetUser.UserName);
                if (Membership.ValidateUser(aspnetUser.UserName, NormalPass))
                {
                    throw new BusinessException("Common_Validate_DuplicatePassword");
                }
                oldPass = mu.ResetPassword();
                mu.ChangePassword(oldPass, NormalPass);

                // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh SynchronizeID = -4
                user.SynchronizeID = GlobalConstants.STATUS_CHANGE_PASSWORD;

                UserAccountBusiness.Update(user);
                UserAccountBusiness.Save();

                //Chiendd1. 27.07.2017 thay doi user tren he thong SSO VNStudy
                OAuth2.ChangePass(aspnetUser.UserName, oldPass, NormalPass);
            }
            return true;
        }
        public void ChangePasswordByListUseraccount(IDictionary<int, string> lstUserAccount)
        {
            List<int> lstUserAccountID = lstUserAccount.Select(p => p.Key).Distinct().ToList();
            List<UserAccount> lstUserAccountDB = UserAccountBusiness.All.Where(p => lstUserAccountID.Contains(p.UserAccountID)).ToList();
            UserAccount user = null;
            string oldPass = string.Empty;
            for (int i = 0; i < lstUserAccountDB.Count; i++)
            {
                user = lstUserAccountDB[i];
                if (user != null)
                {
                    //Thực hiện gọi hàm 
                    MembershipUser mu = Membership.GetUser(user.aspnet_Users.UserName);
                    if (mu.IsLockedOut)
                    {
                        mu.UnlockUser();
                    }
                    oldPass = mu.ResetPassword();
                    mu.ChangePassword(oldPass, lstUserAccount[user.UserAccountID]);

                    // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh SynchronizeID = -4
                    user.SynchronizeID = GlobalConstants.STATUS_CHANGE_PASSWORD;

                    UserAccountBusiness.Update(user);
                    OAuth2.ChangePass(user.aspnet_Users.UserName, oldPass, lstUserAccount[user.UserAccountID]);
                }
            }
            UserAccountBusiness.Save();
        }
        public void ResetPasswordByListUserAccount(List<int> lstUserAccountID)
        {
            List<UserAccount> lstUserAccountDB = UserAccountBusiness.All.Where(p => lstUserAccountID.Contains(p.UserAccountID)).ToList();
            UserAccount user = null;
            string oldPass = string.Empty;
            for (int i = 0; i < lstUserAccountDB.Count; i++)
            {
                user = lstUserAccountDB[i];
                if (user != null && user.aspnet_Users != null && user.aspnet_Users.aspnet_Membership != null)
                {
                    //Thực hiện gọi hàm 
                    MembershipUser mu = Membership.GetUser(user.aspnet_Users.UserName);
                    if (mu == null)
                        user.aspnet_Users.LoweredUserName = user.aspnet_Users.UserName;
                    else
                    {
                        if (mu.IsLockedOut) mu.UnlockUser();
                        oldPass = mu.ResetPassword();
                        mu.ChangePassword(oldPass, CreateRandomPassword(SystemParamsInFile.MAX_PASS_LENTGTH));
                        //Thuc hien unlock va set lai lan dang nhap dau tien
                        user.aspnet_Users.aspnet_Membership.IsLockedOut = 0;
                        user.FirstLoginDate = null;
                    }
                    user.SynchronizeID = GlobalConstants.STATUS_CHANGE_PASSWORD;
                    UserAccountBusiness.Update(user);
                    OAuth2.ChangePass(user.aspnet_Users.UserName, oldPass, CreateRandomPassword(SystemParamsInFile.MAX_PASS_LENTGTH));
                }
            }
            UserAccountBusiness.Save();
        }


        /// <summary>
        /// Chuyển account sang trạng thái không hoạt động
        /// </summary>
        /// <author>AnhVD9 - 20140924</author>
        /// <param name="userAccountID"></param>
        /// <returns>Có chuyển thành công hay không</returns>
        public bool DeActiveByEmployeeID(int EmployeeID)
        {
            if (EmployeeID <= 0) return false;
            UserAccount acc = this.UserAccountBusiness.All.FirstOrDefault(o => o.EmployeeID == EmployeeID && o.IsActive == true);
            if (acc == null) return false;
            acc.IsActive = false;
            aspnet_Membership usermb = new aspnet_MembershipRepository(this.context).All.FirstOrDefault(o => o.UserId == acc.GUID);
            if (usermb != null)
            {
                usermb.IsApproved = 0;
                string deletedUser = string.Format("{0}_{1}", usermb.aspnet_Users.UserName, "delete");
                usermb.aspnet_Users.UserName = deletedUser;
                usermb.aspnet_Users.LoweredUserName = deletedUser;
                new aspnet_MembershipRepository(this.context).Update(usermb);
                new aspnet_MembershipRepository(this.context).Save();
            }
            UserAccountBusiness.Save();
            return true;
        }

        public void UpdateSynchronize(int UserID, int status)
        {
            UserAccount userAccount = UserAccountBusiness.All.Where(o => o.UserAccountID == UserID || o.EmployeeID == UserID).FirstOrDefault();

            if (userAccount != null)
            {
                userAccount.SynchronizeID = status;
                UserAccountBusiness.Update(userAccount);
                UserAccountBusiness.Save();
            }
        }

        public void UpdateSynchronize(string userName, int status)
        {
            UserAccount userAccount = (from ua in UserAccountBusiness.All
                                       join mem in aspnet_UsersBusiness.All on ua.GUID equals mem.UserId
                                       where mem.UserName.Equals(userName)
                                       select ua).FirstOrDefault();

            if (userAccount != null)
            {
                userAccount.SynchronizeID = status;
                UserAccountBusiness.Update(userAccount);
                UserAccountBusiness.Save();
            }
        }


        /// <summary>
        /// Chiendd1:27.07.2017 Thuc hien them moi nguoi dung vao he thong SSO VnStudy.
        /// Do goi tu ham CreateUser nen khong lay duoc schoolId, supervisingDeptId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="schoolId"></param>
        /// <param name="supervisingDeptId"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void AddUserSSO(int? employeeId, int userAccountId, string userName, string password)
        {
            //Neu la tai khoan nhan vien hoac giao vien
            if (employeeId.HasValue)
            {
                #region Tai khoan nhan vien/giao vien
                Employee objEmployee = EmployeeBusiness.Find(employeeId.Value);
                if (objEmployee == null)
                {
                    return;
                }

                string provinceName = "";
                string districtName = "";
                string unitName = "";
                string jobPosition = "";
                if (objEmployee.SupervisingDeptID.HasValue)
                {
                    SupervisingDept objdept = SupervisingDeptBusiness.Find(objEmployee.SupervisingDeptID.Value);
                    provinceName = (objdept == null || objdept.Province == null) ? "" : objdept.Province.ProvinceName;
                    districtName = (objdept == null || objdept.District == null) ? "" : objdept.District.DistrictName;
                    unitName = (objdept == null) ? "" : objdept.SupervisingDeptName;
                    jobPosition = "1";
                }
                else if (objEmployee.SchoolID.HasValue)
                {
                    SchoolProfile objSchool = SchoolProfileBusiness.Find(objEmployee.SchoolID.Value);
                    provinceName = (objSchool == null || objSchool.Province == null) ? "" : objSchool.Province.ProvinceName;
                    districtName = (objSchool == null || objSchool.District == null) ? "" : objSchool.District.DistrictName;
                    unitName = (objSchool == null) ? "" : objSchool.SchoolName;
                    jobPosition = "16";
                }
                OAuthUserBO ssoUser = new OAuthUserBO
                {
                    Address = objEmployee.TempResidentalAddress,
                    Avatar = objEmployee.Image != null ? Convert.ToBase64String(objEmployee.Image) : "",
                    BirthDate = (!objEmployee.BirthDate.HasValue) ? "" : objEmployee.BirthDate.Value.ToString("dd/MM/yyyy"),
                    District = districtName,
                    Email = objEmployee.Email,
                    FullName = objEmployee.FullName,
                    Gender = objEmployee.Genre ? "1" : "2",
                    JobPosition = jobPosition,
                    Password = password,
                    Phone = objEmployee.Mobile,
                    Province = provinceName,
                    UnitName = unitName,
                    UserName = userName
                };
                OAuth2.AddUser(ssoUser);

                #endregion
            }
            else //Tai khoan quan tri truong, phong/so
            {
                #region Tai khoan quan tri
                SchoolProfile school = SchoolProfileBusiness.All.FirstOrDefault(s => s.AdminID == userAccountId);
                if (school != null)
                {
                    OAuthUserBO ssoUser = new OAuthUserBO
                    {
                        Address = school.Address,
                        Avatar = school.Image != null ? Convert.ToBase64String(school.Image) : "",
                        BirthDate = (!school.EstablishedDate.HasValue) ? "" : school.EstablishedDate.Value.ToString("dd/MM/yyyy"),
                        District = (school == null || school.Province == null) ? "" : school.Province.ProvinceName,
                        Email = school.Email,
                        FullName = school.SchoolName,
                        Gender = "",
                        JobPosition = "13",
                        Password = password,
                        Phone = school.Telephone,
                        Province = (school == null || school.Province == null) ? "" : school.Province.ProvinceName,
                        UnitName = school.SchoolName,
                        UserName = userName
                    };
                    OAuth2.AddUser(ssoUser);
                }
                else //tai khoan quan tri phong/so
                {
                    SupervisingDept objSupervisingDept = SupervisingDeptBusiness.All.FirstOrDefault(s => s.AdminID == userAccountId);
                    if (objSupervisingDept == null)
                    {
                        return;
                    }
                    OAuthUserBO ssoUser = new OAuthUserBO
                    {
                        Address = objSupervisingDept.Address,
                        Avatar = "",
                        BirthDate = objSupervisingDept.CreatedDate.Value.ToString("dd/MM/yyyy"),
                        District = (objSupervisingDept == null || objSupervisingDept.Province == null) ? "" : objSupervisingDept.Province.ProvinceName,
                        Email = objSupervisingDept.Email,
                        FullName = objSupervisingDept.SupervisingDeptName,
                        Gender = "",
                        JobPosition = "1",
                        Password = password,
                        Phone = objSupervisingDept.Telephone,
                        Province = (objSupervisingDept == null || objSupervisingDept.Province == null) ? "" : objSupervisingDept.Province.ProvinceName,
                        UnitName = objSupervisingDept.SupervisingDeptName,
                        UserName = userName
                    };
                    OAuth2.AddUser(ssoUser);
                }
                #endregion
            }
        }


    }
}