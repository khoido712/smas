/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SkillTitleClassBusiness
    {
        public IQueryable<SkillTitleClass> Search(IDictionary<string, object> dic)
        {
            int SkillTitleClassID = Utils.GetInt(dic, "SkillTitleClassID");
            int SkillTitleID = Utils.GetInt(dic, "SkillTitleID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            IQueryable<SkillTitleClass> Query = SkillTitleClassRepository.All;
            if (SkillTitleClassID != 0)
            {
                Query = Query.Where(evd => evd.SkillTitleClassID == SkillTitleClassID);
            }
            if (SkillTitleID != 0)
            {
                Query = Query.Where(evd => evd.SkillTitleID == SkillTitleID);
            }
            if (ClassID != 0)
            {
                Query = Query.Where(evd => evd.ClassID == ClassID);
            }

            return Query;
        }

        public IQueryable<ClassProfile> GetListClass(int SkillTitleID)
        {
            IQueryable<ClassProfile> query = from stc in SkillTitleClassRepository.All
                                             where stc.SkillTitleID == SkillTitleID
                                             select stc.ClassProfile;
            return query;
        }

        public void Insert(List<SkillTitleClass> lsSkillTitleClass)
        {
            foreach (SkillTitleClass item in lsSkillTitleClass)
            {
                ValidationMetadata.ValidateObject(item);
                base.Insert(item);
            }
            this.Save();
        }

        public void Delete(int SkillTitleID, List<int> lstClassID)
        {
            IQueryable<SkillTitleClass> lst = from s in SkillTitleClassRepository.All
                                              where s.SkillTitleID == SkillTitleID
                                                select s;
            foreach (SkillTitleClass item in lst)
            {
                if (lstClassID.Contains(item.ClassID.Value))
                {
                    this.Delete(item.SkillTitleID);
                }

            }
            this.Save();
        }
        
        
    }
}