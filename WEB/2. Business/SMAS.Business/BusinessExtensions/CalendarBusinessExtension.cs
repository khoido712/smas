/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using System.IO;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using System.Data.Objects;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class CalendarBusiness
    {

        #region private member variable
        private const int VALUE_MIN = 0;//Giá trị min
        private const int VALUE_MAX = 5;//Giá trị max
        #endregion
        #region Base Method

        public Stream ExportExcel(int? semester, int? level, int? classID, int? section, string teacherName, DateTime? dateApplied, int? schoolID, int? academicYearID, int? educationLevel, int? appliedLevel)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/GV/ThoiKhoaBieu.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (schoolID.HasValue)
            {
                dic["SchoolID"] = schoolID;
            }
            if (academicYearID.HasValue)
            {
                dic["AcademicYearID"] = academicYearID;
            }
            if (educationLevel.HasValue)
            {
                dic["EducationLevelID"] = educationLevel;
                dic["EducationLevel"] = educationLevel;
            }
            if (classID.HasValue)
            {
                dic["ClassProfileID"] = classID;
                dic["ClassID"] = classID;
            }
            if (section.HasValue)
            {
                dic["Section"] = section;
            }
            if (semester.HasValue)
            {
                dic["Semester"] = semester;
            }
            if (!string.IsNullOrEmpty(teacherName))
            {
                dic["AssignTeacherName"] = teacherName;
            }
            if (appliedLevel.HasValue)
            {
                dic["AppliedLevel"] = appliedLevel;
            }
            List<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName).ToList();
            List<Calendar> lstCalendar = CalendarBusiness.Search(dic).ToList();
            IEnumerable<TeachingAssignmentBO> listTeachingAssignment = TeachingAssignmentBusiness.GetTeachingAssigment(schoolID.Value, dic).ToList();

            // Danh sách phân môn
            List<AssignSubjectConfig> lstASC = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == schoolID
                                                                                        && x.AcademicYearID == academicYearID
                                                                                        ).ToList();

            List<ClassSection> lstSection = new List<ClassSection>();
            SchoolProfile school = null;
            AcademicYear academic = null;
            if (schoolID.HasValue)
            {
                school = SchoolProfileBusiness.Find(schoolID.Value);
            }
            if (academicYearID.HasValue)
            {
                academic = AcademicYearBusiness.Find(academicYearID.Value);
            }
            //int? maxSection = lstClass.Max(a => a.Section);
            foreach (ClassProfile cl in lstClass)
            {
                var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                lstSection = lstSection.Union(listSectionOther).ToList();

                if (lstSection.Count >= 3)
                    break;
            }

            if (section.HasValue)
            {
                lstSection = lstSection.Where(u => u.Section == section.Value).ToList();
            }

            //GetSection((maxSection.HasValue) ? maxSection.Value : 0, ref lstSection);
            //Get Template Field
            var templateRng = sheet2.GetRange("C6", "C111"); //3 buoi
            var headerRng = sheet2.GetRange("AC6", "AE111"); //3 buoi
            if (lstSection.Count == 2)
            {
                templateRng = sheet2.GetRange("C6", "C76");
                if (CheckSection(ClassSection.MORNING, lstSection) && CheckSection(ClassSection.AFTERNOON, lstSection))
                {
                    //Sang chieu
                    headerRng = sheet2.GetRange("Q6", "S76");
                }
                else if (CheckSection(ClassSection.MORNING, lstSection) && CheckSection(ClassSection.NIGHT, lstSection))
                {
                    //Sang toi
                    headerRng = sheet2.GetRange("U6", "W76");
                }
                else if (CheckSection(ClassSection.AFTERNOON, lstSection) && CheckSection(ClassSection.NIGHT, lstSection))
                {
                    //Sang toi
                    headerRng = sheet2.GetRange("Y6", "AA76");
                }
            }
            else if (lstSection.Count == 1)
            {
                templateRng = sheet2.GetRange("C6", "C41");
                if (CheckSection(ClassSection.MORNING, lstSection))
                {
                    //Sang
                    headerRng = sheet2.GetRange("E6", "G41");
                }
                else if (CheckSection(ClassSection.AFTERNOON, lstSection))
                {
                    //Chieu
                    headerRng = sheet2.GetRange("I6", "K41");
                }
                else if (CheckSection(ClassSection.NIGHT, lstSection))
                {
                    //Toi
                    headerRng = sheet2.GetRange("M6", "O41");
                }
            }
            //Copy Header Range
            //column template
            int startRow = 6;
            int startCol = 5;
            int tmpRow, tmpCol;
            int sectionCount = lstSection.Count;
            sheet1.CopyPasteSameSize(headerRng, "B6");
            //Ten Phong/So
            if (school != null)
            {
                sheet1.SetCellValue("B1", (school.SupervisingDept != null) ? school.SupervisingDept.SupervisingDeptName : string.Empty);
                sheet1.SetCellValue("B2", school.SchoolName);
            }
            //Ten Bao cao
            string subName = string.Empty;
            if (academic != null)
            {
                subName += string.Format("NĂM HỌC {0}-{1}", academic.Year, academic.Year + 1);
            }
            if (semester.HasValue)
            {
                subName += " - HỌC KỲ " + ((semester.Value == 1) ? "I" : "II");
            }
            if (educationLevel.HasValue)
            {
                subName += " - KHỐI " + educationLevel.Value;
            }

            sheet1.SetCellValue("F4", subName);
            Dictionary<string, int> listWidth = new Dictionary<string, int>();

            //Duyet qua tung lop de fill TKB
            foreach (var cls in lstClass)
            {
                tmpRow = startRow;
                tmpCol = startCol;
                sheet1.CopyPasteSameSize(templateRng, startRow, startCol);
                //Ten lop
                sheet1.SetCellValue(tmpRow, tmpCol, cls.DisplayName);
                //Fill calendar
                List<Calendar> classCalendar = lstCalendar.Where(a => a.ClassID == cls.ClassProfileID).ToList();
                if (dateApplied != null)
                    classCalendar = classCalendar.Where(x => x.StartDate == dateApplied).ToList();

                if (classCalendar != null)
                {
                    foreach (Calendar cal in classCalendar)
                    {
                        if (cal.Section.HasValue && cal.NumberOfPeriod.HasValue)
                        {
                            List<TeachingAssignmentBO> listTAClass = listTeachingAssignment.Where(u => u.ClassID == cls.ClassProfileID && u.SubjectID == cal.SubjectID).ToList();

                            StringBuilder strFm = new StringBuilder();
                            int col = startCol;
                            int sectionRow = getExcelRow(cal.Section.Value, lstSection);
                            //Tinh ra ROW chua mon nay ung voi tiet, thu, buoi
                            // (Thu - 2) x 5 x tong + (buoi - 1) x 5 + tiet + startRow
                            int row = (cal.DayOfWeek - 2) * 5 * lstSection.Count + (sectionRow - 1) * 5 + cal.NumberOfPeriod.Value + startRow;
                            string strSubject = "";
                            if (listTAClass != null && listTAClass.Count() > 0)
                            {
                                strSubject = String.Join("&Char(10)&", listTAClass.Select(u => "\"" + u.TeacherName + "\"").ToList());
                                int cnt = listTAClass.Count() + 1;
                                //sheet1.SetRowHeight(row, 20 * listTAClass.Count());
                                if (listWidth.Count == 0 || (listWidth.Count > 0 && !listWidth.Keys.Any(u => u == row.ToString())))
                                {
                                    listWidth[row.ToString()] = 20 * cnt;
                                }
                                else
                                {
                                    if (listWidth[row.ToString()] < 20 * cnt)
                                    {
                                        listWidth[row.ToString()] = 20 * cnt;
                                    }
                                }
                            }

                            string stringSubjectName = cal.SubjectCat.SubjectName;
                            if(cal.AssignmentSubjectID != null)
                                stringSubjectName = lstASC.Find(x => x.AssignSubjectConfigID == cal.AssignmentSubjectID).AssignSubjectName;

                            strFm.Append("=\"" + stringSubjectName + "\"");
                            if (strSubject != "")
                            {
                                strFm.Append("&Char(10)&" + strSubject);
                            }
                            sheet1.SetFormulaValue(row, col, strFm.ToString());
                            //sheet1.SetCellValue(row, col, cal.SubjectCat.SubjectName);
                        }
                    }
                }

                //Tăng column
                startCol++;
            }

            //thay doi do cao cua cell

            foreach (string row in listWidth.Keys)
            {
                if (listWidth[row] > 20)
                    sheet1.SetRowHeight(int.Parse(row), listWidth[row]);
            }
            sheet1.FitAllColumnsOnOnePage = true;
            //Xoa sheet template
            sheet2.Delete();
            //Return stream
            return oBook.ToStream();
        }

        public Stream GetDataForImportFile(int? semester, int? level, int? classID, int? section, string teacherName, DateTime? dateApplied, int? schoolID, int? academicYearID, int? educationLevel, int? appliedLevel)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/GV/ThoiKhoaBieu.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet1 = oBook.GetSheet(1);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (schoolID.HasValue)
            {
                dic["SchoolID"] = schoolID;
            }
            if (academicYearID.HasValue)
            {
                dic["AcademicYearID"] = academicYearID;
            }
            if (educationLevel.HasValue)
            {
                dic["EducationLevelID"] = educationLevel;
                dic["EducationLevel"] = educationLevel;
            }
            if (classID.HasValue)
            {
                dic["ClassProfileID"] = classID;
                dic["ClassID"] = classID;
            }
            if (section.HasValue)
            {
                dic["Section"] = section;
            }
            if (semester.HasValue)
            {
                dic["Semester"] = semester;
            }
            if (!string.IsNullOrEmpty(teacherName))
            {
                dic["AssignTeacherName"] = teacherName;
            }
            if (appliedLevel.HasValue)
            {
                dic["AppliedLevel"] = appliedLevel;
            }
            List<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName).ToList();
            List<Calendar> lstCalendar = CalendarBusiness.Search(dic).ToList();
            IEnumerable<TeachingAssignmentBO> listTeachingAssignment = TeachingAssignmentBusiness.GetTeachingAssigment(schoolID.Value, dic).ToList();

            // Danh sách phân môn
            List<AssignSubjectConfig> lstASC = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == schoolID
                                                                                        && x.AcademicYearID == academicYearID
                                                                                        ).ToList();

            List<ClassSection> lstSection = new List<ClassSection>();
            SchoolProfile school = null;
            AcademicYear academic = null;
            if (schoolID.HasValue)
            {
                school = SchoolProfileBusiness.Find(schoolID.Value);
            }
            if (academicYearID.HasValue)
            {
                academic = AcademicYearBusiness.Find(academicYearID.Value);
            }
            //int? maxSection = lstClass.Max(a => a.Section);
            foreach (ClassProfile cl in lstClass)
            {
                var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                lstSection = lstSection.Union(listSectionOther).ToList();

                if (lstSection.Count >= 3)
                    break;
            }

            if (section.HasValue)
            {
                lstSection = lstSection.Where(u => u.Section == section.Value).ToList();
            }

            //GetSection((maxSection.HasValue) ? maxSection.Value : 0, ref lstSection);
            //Get Template Field
            
            //Copy Header Range
            //column template
            int startRow = 6;
            int startCol = 5;
            int tmpRow, tmpCol;
            int sectionCount = lstSection.Count;
                     
            Dictionary<string, int> listWidth = new Dictionary<string, int>();

            //Duyet qua tung lop de fill TKB
            foreach (var cls in lstClass)
            {
                tmpRow = startRow;
                tmpCol = startCol;
                
                //Ten lop
                sheet1.SetCellValue(tmpRow, tmpCol, cls.DisplayName);
                //Fill calendar
                List<Calendar> classCalendar = lstCalendar.Where(a => a.ClassID == cls.ClassProfileID).ToList();
                if (dateApplied != null)
                    classCalendar = classCalendar.Where(x => x.StartDate == dateApplied).ToList();

                if (classCalendar != null)
                {
                    foreach (Calendar cal in classCalendar)
                    {
                        if (cal.Section.HasValue && cal.NumberOfPeriod.HasValue)
                        {
                            List<TeachingAssignmentBO> listTAClass = listTeachingAssignment.Where(u => u.ClassID == cls.ClassProfileID && u.SubjectID == cal.SubjectID).ToList();

                            StringBuilder strFm = new StringBuilder();
                            int col = startCol;
                            int sectionRow = getExcelRow(cal.Section.Value, lstSection);
                            //Tinh ra ROW chua mon nay ung voi tiet, thu, buoi
                            // (Thu - 2) x 5 x tong + (buoi - 1) x 5 + tiet + startRow
                            int row = (cal.DayOfWeek - 2) * 5 * lstSection.Count + (sectionRow - 1) * 5 + cal.NumberOfPeriod.Value + startRow;
                            string strSubject = "";
                            if (listTAClass != null && listTAClass.Count() > 0)
                            {
                                strSubject = String.Join("&Char(10)&", listTAClass.Select(u => "\"" + u.TeacherName + "\"").ToList());
                                int cnt = listTAClass.Count() + 1;
                                //sheet1.SetRowHeight(row, 20 * listTAClass.Count());
                                if (listWidth.Count == 0 || (listWidth.Count > 0 && !listWidth.Keys.Any(u => u == row.ToString())))
                                {
                                    listWidth[row.ToString()] = 20 * cnt;
                                }
                                else
                                {
                                    if (listWidth[row.ToString()] < 20 * cnt)
                                    {
                                        listWidth[row.ToString()] = 20 * cnt;
                                    }
                                }
                            }

                            string stringSubjectName = cal.SubjectCat.SubjectName;
                            if (cal.AssignmentSubjectID != null)
                                stringSubjectName = lstASC.Find(x => x.AssignSubjectConfigID == cal.AssignmentSubjectID).AssignSubjectName;

                            strFm.Append("=\"" + stringSubjectName + "\"");
                            if (strSubject != "")
                            {
                                strFm.Append("&Char(10)&" + strSubject);
                            }
                            sheet1.SetFormulaValue(row, col, strFm.ToString());
                            //sheet1.SetCellValue(row, col, cal.SubjectCat.SubjectName);
                        }
                    }
                }

                //Tăng column
                startCol++;
            }

            //thay doi do cao cua cell

            foreach (string row in listWidth.Keys)
            {
                if (listWidth[row] > 20)
                    sheet1.SetRowHeight(int.Parse(row), listWidth[row]);
            }

            //Return stream
            return oBook.ToStream();
        }

        /// <summary>
        /// Kiem tra de lay ROW khi fill calendar
        /// </summary>
        /// <param name="section"></param>
        /// <param name="lstSection"></param>
        /// <returns></returns>
        public int getExcelRow(int section, List<ClassSection> lstSection)
        {
            int count = lstSection.Count;
            if (count == 3)
            {
                //Sang Chieu Toi
                if (section == ClassSection.MORNING) return 1;
                if (section == ClassSection.AFTERNOON) return 2;
                if (section == ClassSection.NIGHT) return 3;
            }
            else if (count == 2)
            {
                if (CheckSection(ClassSection.MORNING, lstSection) && CheckSection(ClassSection.AFTERNOON, lstSection))
                {
                    //Sang chieu
                    if (section == ClassSection.MORNING) return 1;
                    if (section == ClassSection.AFTERNOON) return 2;
                }
                else if (CheckSection(ClassSection.MORNING, lstSection) && CheckSection(ClassSection.NIGHT, lstSection))
                {
                    //Sang toi
                    if (section == ClassSection.MORNING) return 1;
                    if (section == ClassSection.NIGHT) return 2;
                }
                else if (CheckSection(ClassSection.AFTERNOON, lstSection) && CheckSection(ClassSection.NIGHT, lstSection))
                {
                    //Sang toi
                    if (section == ClassSection.AFTERNOON) return 1;
                    if (section == ClassSection.NIGHT) return 2;
                }
            }
            return 1;
        }
        /// <summary>
        /// Kiem tra section trong mot list
        /// </summary>
        /// <param name="_lst"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        private bool CheckSection(int section, List<ClassSection> _lst)
        {
            if (_lst.Where(a => a.Section == section).FirstOrDefault() != null) return true;
            return false;
        }
        /// <summary>
        /// Section class
        /// </summary>
        public class ClassSection
        {
            public int Section { get; set; }

            public string SectionName
            {
                get
                {
                    if (Section == 1) return GlobalConstants.SECTION_MONING;
                    if (Section == 2) return GlobalConstants.SECTION_AFTERNOON;
                    if (Section == 3) return GlobalConstants.SECTION_EVENING;
                    return string.Empty;
                }
            }

            public ClassSection(int section)
            {
                Section = section;
            }

            public const int MORNING = 1;
            public const int AFTERNOON = 2;
            public const int NIGHT = 3;
        }
        /// <summary>
        /// Lay danh sach buoi hoc
        /// </summary>
        /// <param name="seperateID"></param>
        /// <param name="_lst"></param>
        public void GetSection(int seperateID, ref List<ClassSection> _lst)
        {
            switch (seperateID)
            {
                case ClassSection.MORNING:
                    {
                        var itm = new ClassSection(ClassSection.MORNING);
                        _lst.Add(itm);
                        break;
                    }
                case ClassSection.AFTERNOON:
                    {
                        var itm = new ClassSection(ClassSection.AFTERNOON);
                        _lst.Add(itm);
                        break;
                    }
                case ClassSection.NIGHT:
                    {
                        var itm = new ClassSection(ClassSection.NIGHT);
                        _lst.Add(itm);
                        break;
                    }
                case 4:
                    {
                        var itm = new ClassSection(ClassSection.MORNING);
                        _lst.Add(itm);

                        itm = new ClassSection(ClassSection.AFTERNOON);
                        _lst.Add(itm);
                        break;
                    }
                case 5:
                    {
                        var itm = new ClassSection(ClassSection.MORNING);
                        _lst.Add(itm);

                        itm = new ClassSection(ClassSection.NIGHT);
                        _lst.Add(itm);
                        break;
                    }
                case 6:
                    {
                        var itm = new ClassSection(ClassSection.AFTERNOON);
                        _lst.Add(itm);

                        itm = new ClassSection(ClassSection.NIGHT);
                        _lst.Add(itm);
                        break;
                    }
                case 7:
                    {
                        var itm = new ClassSection(ClassSection.MORNING);
                        _lst.Add(itm);
                        itm = new ClassSection(ClassSection.AFTERNOON);
                        _lst.Add(itm);
                        itm = new ClassSection(ClassSection.NIGHT);
                        _lst.Add(itm);
                        break;
                    }
            }
            /*string binary = Convert.ToString(seperateID, 2);
            if (this.CheckPosVal(binary, 3 - ClassSection.MORNING))
            {
                var itm = new ClassSection(ClassSection.MORNING);
                if (_lst.Where(a => a.Section == itm.Section).FirstOrDefault() == null) _lst.Add(itm);
            }
            if (this.CheckPosVal(binary, 3 - ClassSection.AFTERNOON))
            {
                var itm = new ClassSection(ClassSection.AFTERNOON);
                if (_lst.Where(a => a.Section == itm.Section).FirstOrDefault() == null) _lst.Add(itm);
            }
            if (this.CheckPosVal(binary, 3 - ClassSection.NIGHT))
            {
                var itm = new ClassSection(ClassSection.NIGHT);
                if (_lst.Where(a => a.Section == itm.Section).FirstOrDefault() == null) _lst.Add(itm);
            }*/

        }
        /// <summary>
        /// Chuyen tu luu trong DB sang buoi hoc
        /// </summary>
        /// <param name="listValue"></param>
        /// <returns></returns>
        public short Convert2Bynary(List<int> listValue)
        {
            short res = 0;
            foreach (int value in listValue)
            {
                res += (short)Math.Pow(2, value - 1);
            }
            return res;
        }
        /// <summary>
        /// Kiem tra mot ky tu tai vi tri do xem co trung voi 1
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool CheckPosVal(string s, int pos)
        {
            if (s == null || s.Equals(string.Empty))
            {
                return false;
            }
            if (s.Length < 3)
            {
                for (int i = s.Length; i < 3; i++)
                {
                    s = "0" + s;
                }
            }
            char c = s[pos];
            if (c.Equals('1'))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Tìm kiếm thời khóa biểu
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="AcademicYearID">ID năm học</param>
        /// <param name="ClassID">ID Lớp</param>
        /// <param name="DayOfWeek">Ngày trong tuần</param>
        /// <param name="Section">/Buổi trong ngày</param>
        /// <param name="Semester">Học kì</param>
        /// <returns>IQueryable<Calendar></returns>
        public IQueryable<Calendar> Search(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            byte DayOfWeek = (byte)Utils.GetInt(dic, "DayOfWeek");
            byte Section = (byte)Utils.GetInt(dic, "Section"); ;
            byte Semester = (byte)Utils.GetInt(dic, "Semester");
            byte EducationLevelID = (byte)Utils.GetInt(dic, "EducationLevelID");
            string AssignTeacherName = Utils.GetString(dic, "AssignTeacherName"); //Giao vien day lop nay
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            DateTime? AppliedDate = Utils.GetDateTime(dic, "AppliedDate");
            IQueryable<Calendar> lsCalendar = CalendarRepository.All.Where(a => a.IsActive == true);

            if (ClassID != 0)
            {
                lsCalendar = from c in lsCalendar
                             join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                             where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                             && c.ClassID == ClassID
                             select c;
                    //lsCalendar.Where(cal => cal.ClassID == ClassID);
            }

            if (!string.IsNullOrEmpty(AssignTeacherName))
            {
                AcademicYear acad = AcademicYearBusiness.Find(AcademicYearID);
                int partitionId = UtilsBusiness.GetPartionId(acad.SchoolID);
                IDictionary<string, object> dicEmployee = new Dictionary<string, object>();
                dicEmployee["FullName"] = AssignTeacherName;
                IQueryable<Employee> lsEmployee = EmployeeBusiness.SearchTeacher(acad.SchoolID, dicEmployee);
                IQueryable<ClassProfile> lsClassProfile = ClassProfileBusiness.SearchBySchool(acad.SchoolID, dicEmployee).Where(u => lsEmployee.Contains(u.Employee));
                IQueryable<TeachingAssignment> listTeacherAssign =
                        (from sup in TeachingAssignmentBusiness.All
                         join emp in EmployeeBusiness.All on sup.TeacherID equals emp.EmployeeID
                         where ((ClassID == 0 || sup.ClassID == ClassID)
                         && (AcademicYearID == 0 || sup.AcademicYearID == AcademicYearID))
                         && emp.FullName.ToLower().Contains(AssignTeacherName.ToLower())
                         && sup.ClassID.HasValue && sup.IsActive == true
                         && sup.Last2digitNumberSchool == partitionId
                         select sup);
                IQueryable<Calendar> lsCalendar1 = null;
                IQueryable<Calendar> lscalendar2 = null;
                if (listTeacherAssign != null)
                {
                    lsCalendar1 = from cal in lsCalendar
                                  join lta in listTeacherAssign
                                  on new { cal.ClassID, cal.SubjectID } equals new { ClassID = lta.ClassID.Value, lta.SubjectID }
                                  select cal;



                }
                if (lsClassProfile != null)
                {
                    lscalendar2 = from cal in lsCalendar.Where(u => u.SubjectID == SystemParamsInFile.SINH_HOAT_SUBJECT)
                                  join lta in lsClassProfile
                             on cal.ClassID equals lta.ClassProfileID
                                  select cal;
                }
                lsCalendar = lsCalendar1.Union(lscalendar2);

            }
            if (EmployeeID != 0)
            {
                AcademicYear acad = AcademicYearBusiness.Find(AcademicYearID);

                IDictionary<string, object> dicEmployee = new Dictionary<string, object>();
                dicEmployee["FullName"] = AssignTeacherName;
                IQueryable<Employee> lsEmployee = EmployeeBusiness.SearchTeacher(acad.SchoolID, dicEmployee);
                IQueryable<ClassProfile> lsClassProfile = ClassProfileBusiness.SearchBySchool(acad.SchoolID, dicEmployee).Where(u => lsEmployee.Contains(u.Employee));
                IQueryable<Calendar> lsCalendar1 = null;
                IQueryable<Calendar> lscalendar2 = null;
                IQueryable<TeachingAssignment> listTeacherAssign =
                        (from sup in TeachingAssignmentBusiness.All
                         where ((ClassID == 0 || sup.ClassID == ClassID)
                         && (AcademicYearID == 0 || sup.AcademicYearID == AcademicYearID))
                         && sup.TeacherID == EmployeeID
                         && sup.ClassID.HasValue && sup.IsActive == true
                         select sup);
                if (listTeacherAssign != null)
                {
                    lsCalendar1 = from cal in lsCalendar
                                  join lta in listTeacherAssign
                                  on new { cal.ClassID, cal.SubjectID } equals new { ClassID = lta.ClassID.Value, lta.SubjectID }
                                  select cal;
                }
                if (lsClassProfile != null)
                {
                    lscalendar2 = from cal in lsCalendar.Where(u => u.SubjectID == SystemParamsInFile.SINH_HOAT_SUBJECT)
                                  join lta in lsClassProfile
                             on cal.ClassID equals lta.ClassProfileID
                                  select cal;
                }
                lsCalendar = lsCalendar1.Union(lscalendar2);
            }
            if (AppliedLevel != 0)
            {
                lsCalendar = lsCalendar.Where(cal => cal.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }
            if (Section != 0)
            {
                lsCalendar = lsCalendar.Where(cal => cal.Section == Section);
            }
            if (AcademicYearID != 0)
            {
                lsCalendar = lsCalendar.Where(cal => cal.AcademicYearID == AcademicYearID);
            }
            if (DayOfWeek != 0)
            {
                lsCalendar = lsCalendar.Where(cal => cal.DayOfWeek == DayOfWeek);
            }
            if (Semester != 0)
            {
                lsCalendar = lsCalendar.Where(cal => cal.Semester == Semester);
            }
            if (EducationLevelID != 0)
            {
                lsCalendar = lsCalendar.Where(cal => cal.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (AppliedDate.HasValue)
            {
                lsCalendar = lsCalendar.Where(cal => EntityFunctions.TruncateTime(cal.StartDate) == EntityFunctions.TruncateTime(AppliedDate));
            }
            if (lstClassID.Count > 0)
            {
                lsCalendar = lsCalendar.Where(cal => lstClassID.Contains(cal.ClassID));
            }

            return lsCalendar;
        }

        /// <summary>
        /// Thêm hoặc sửa thời khóa biểu
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="calendar">object Calendar</param>
        /// <returns>object Calendar</returns>
        public Calendar InsertOrUpdate(Calendar calendar)
        {
            //Kiểm tra List<Subject> có null hay không?
            //List<SubjectCat> lsSubjectCat = SubjectCatRepository.All.Where(sub => sub.IsActive == true).ToList();
            //if(lsSubjectCat ==null)
            //{
            //    //.....
            //}
            ////Kiểm tra List<Class> có null hay không?
            //List<ClassProfile> lsClassProfile = ClassProfileRepository.All.ToList();
            //if (lsClassProfile == null)
            //{ 
            //    //..........
            //}

            //Phải khai báo môn học cho lớp.
            //Utils.ValidateRequire(calendar.SubjectCat, "Calendar_SubjectCat");
            //Mỗi buổi không quá 5 tiết học.

            //Số tiết/ giờ của môn học phải nhỏ hơn hoặc bằng 5. Kiểm tra NumberOfPeriod.
            Utils.ValidateRange((int)calendar.NumberOfPeriod, VALUE_MIN, VALUE_MAX, "Calendar_NumberOfPeriod");
            //Lớp học không tồn tại hoặc đã bị xóa 
            new CalendarBusiness(null).CheckAvailable(calendar.ClassID, "Calendar_ClassID");
            //Môn học không tồn tại hoặc đã bị xóa 
            new CalendarBusiness(null).CheckAvailable(calendar.SubjectID, "Calendar_SubjectID");
            //Năm học không tồn tại hoặc dã bị xóa 
            new CalendarBusiness(null).CheckAvailable(calendar.AcademicYearID, "Calendar_AcademicYearID");
            //Trường hợp chưa có dữ liệu thì insert. Có dữ liệu rồi thì update.
            int count = CalendarRepository.All.Where(cal => cal.IsActive == true && cal.CalendarID == calendar.CalendarID).ToList().Count;
            if (count == 0)
            {
                base.Insert(calendar);
            }
            else
            {
                base.Update(calendar);
            }
            return base.Insert(calendar);
        }


        /// <summary>
        /// Xóa thời khóa biểu
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="CalendarID">ID thời khóa biểu.</param>
        public void Delete(int CalendarID)
        {
            this.CheckAvailable(CalendarID, "Calendar_CalendarID");
            this.CheckConstraints(GlobalConstants.SCHOOL_SCHEMA, "Calendar", CalendarID, "Calendar_CalendarID");
            base.Delete(CalendarID, true);
        }

        #region lay thoi khoa bieu theo quyen giao vien la bo mon va chu nhiem
        /// <summary>
        /// Lay thoi khoa bieu theo quyen giao vien chu nhiem va bo mon
        /// </summary>
        /// <author>HaiVT</author>
        /// <date>26/03/2013</date>
        /// <param name="dic">Paramenter</param>
        /// <returns>CalendarBO</returns>
        public IQueryable<CalendarBO> SearchByPermissionTeacher(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            byte semester = (byte)Utils.GetInt(dic, "Semester");
            int employeeID = Utils.GetInt(dic, "EmployeeID");
            int appliedLevel = Utils.GetInt(dic, "appliedLevel");
            bool isParent = Utils.GetBool(dic, "isParent", false);
            int pupilID = Utils.GetInt(dic, "pupilID");

            IQueryable<CalendarBO> lst = null;

            if (isParent)
            {
                //Thoi khoa bieu cua hoc sinh theo pupilID
                lst = from c in CalendarRepository.All
                      join cp in ClassProfileRepository.All on c.ClassID equals cp.ClassProfileID
                      join sc in SubjectCatRepository.All on c.SubjectID equals sc.SubjectCatID
                      join pp in PupilProfileBusiness.All on cp.ClassProfileID equals pp.CurrentClassID
                      join el in EducationLevelBusiness.All on cp.EducationLevelID equals el.EducationLevelID
                      where c.IsActive == true && el.Grade == appliedLevel && c.AcademicYearID == academicYearID && c.Semester == semester
                      && cp.IsActive == true
                      && pp.PupilProfileID == pupilID
                      select new CalendarBO
                      {
                          DayOfWeek = c.DayOfWeek,
                          Section = c.Section,
                          SubjectOrder = c.SubjectOrder,
                          NumberOfPeriod = c.NumberOfPeriod,
                          StartDate = c.StartDate,
                          ClassName = cp.DisplayName,
                          SubjectName = sc.DisplayName,
                          Color = sc.Color,
                          AssignmentSubjectID = c.AssignmentSubjectID
                      };
            }
            else
            {
                //Thoi khoa bieu cua giao vien chi map voi phan cong giang day               
                lst = from c in CalendarRepository.All
                      join cp in ClassProfileRepository.All on c.ClassID equals cp.ClassProfileID
                      join sc in SubjectCatRepository.All on c.SubjectID equals sc.SubjectCatID
                      join el in EducationLevelBusiness.All on cp.EducationLevelID equals el.EducationLevelID
                      join ta in TeachingAssignmentRepository.All.Where(p => p.TeacherID == employeeID && p.AcademicYearID == academicYearID && p.Semester == semester && p.IsActive == true)
                             on new { ClassID = (Int32?)c.ClassID, c.SubjectID } equals new { ta.ClassID, ta.SubjectID }
                      where c.IsActive == true && el.Grade == appliedLevel && c.AcademicYearID == academicYearID && c.Semester == semester
                      && cp.IsActive == true
                      select new CalendarBO
                      {
                          DayOfWeek = c.DayOfWeek,
                          Section = c.Section,
                          SubjectOrder = c.SubjectOrder,
                          NumberOfPeriod = c.NumberOfPeriod,
                          StartDate = c.StartDate,
                          ClassName = cp.DisplayName,
                          SubjectName = sc.DisplayName,
                          Color = sc.Color,
                          ClassID=c.ClassID,
                          SubjectID=c.SubjectID,
                          AssignmentSubjectID = c.AssignmentSubjectID
                      };
            }
            return lst;
        }
        #endregion

        #endregion
        public void InsertCalendar(List<Calendar> lstCalendar, IDictionary<string, object> dic)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                List<Calendar> lstCalendarDB = CalendarBusiness.Search(dic).ToList();
                CalendarBusiness.DeleteAll(lstCalendarDB);
                CalendarBusiness.Save();
                CalendarBusiness.BulkInsert(lstCalendar, this.ColumnMappings());

            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertCalendar", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }
        public List<CalendarBO> GetListCalendarOfPupil(int AcademicYearID, List<int> lstClassID, int Semester)
        {
            try
            {
                List<CalendarBO> lstResult = new List<CalendarBO>();
                CalendarBO objResult = null;
                var listCalendar = CalendarBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                    && lstClassID.Contains(o.ClassID)
                    && o.Semester == Semester && o.IsActive)
                    .Select(o => new
                    {
                        o.ClassID,
                        o.SubjectCat.DisplayName,
                        o.Section,
                        o.DayOfWeek,
                        o.SubjectOrder
                    }).ToList();
                string calendarSchedule = "";
                int ClassID = 0;
                for (int i = 0; i < lstClassID.Count; i++)
                {
                    ClassID = lstClassID[i];
                    objResult = new CalendarBO();
                    objResult.ClassID = ClassID;
                    calendarSchedule = "";
                    // Các thứ lớp có học
                    List<int> listDayOfWeek = listCalendar.Where(p=>p.ClassID == ClassID).Select(o => o.DayOfWeek).Distinct().OrderBy(d => d).ToList();
                    int count = listDayOfWeek.Count;
                    for (int j = 0; j < count; j++)
                    {
                        string calendarByDay = string.Empty;
                        // Thứ
                        int d = listDayOfWeek[j];
                        var listByDay = listCalendar.Where(o =>o.ClassID == ClassID && o.DayOfWeek == d).ToList();
                        var listBySection = listByDay.Select(o => o.Section).Distinct().OrderBy(s => s).ToList();
                        calendarSchedule += "T" + d + "-";
                        foreach (int s in listBySection)
                        {
                            // Buổi
                            string sectionName = this.GetSection(s);
                            calendarByDay += sectionName + ": ";
                            // Môn học
                            var listSubjectName = listByDay.Where(o => o.Section == s).OrderBy(o => o.SubjectOrder);
                            string subjectName = string.Empty;
                            int countSubject = 0;
                            int sumSubjectName = listSubjectName.Count();
                            int index = 0;
                            foreach (var item in listSubjectName)
                            {
                                index++;
                                if (!item.DisplayName.Equals(subjectName))
                                {
                                    if (!string.IsNullOrEmpty(subjectName))
                                    {
                                        if (countSubject > 1)
                                        {
                                            calendarByDay += string.Format("{0}({1}), ", subjectName, countSubject);
                                        }
                                        else
                                        {
                                            calendarByDay += string.Format("{0}, ", subjectName, countSubject);
                                        }
                                    }
                                    subjectName = item.DisplayName;
                                    countSubject = 0;
                                }

                                if (subjectName == item.DisplayName)
                                {
                                    countSubject++;
                                }

                                if (sumSubjectName == index)
                                {
                                    if (countSubject > 1)
                                    {
                                        calendarByDay += string.Format("{0}({1})", subjectName, countSubject);
                                    }
                                    else
                                    {
                                        calendarByDay += string.Format("{0}", subjectName);
                                    }
                                }
                            }

                            // Thêm vào dấu phân cách
                            calendarByDay += "; ";
                        }

                        calendarSchedule += calendarByDay;
                        if (j != count - 1)
                        {
                            // Xuống dòng với mỗi thứ
                            calendarSchedule = calendarSchedule.Substring(0, calendarSchedule.Length - 2);
                            calendarSchedule += "\n";
                        }
                    }
                    calendarSchedule = !string.IsNullOrEmpty(calendarSchedule) ? calendarSchedule.Substring(0, calendarSchedule.Length - 2) : "";
                    objResult.strCalendar = calendarSchedule;
                    lstResult.Add(objResult);
                }
                return lstResult;
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<CalendarBO>();
            }

        }
        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("CalendarID", "CALENDAR_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("SubjectOrder", "SUBJECT_ORDER");
            columnMap.Add("NumberOfPeriod", "NUMBER_OF_PERIOD");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("AssignmentSubjectID", "ASSIGNMENT_SUBJECT_ID");
            return columnMap;
        }
        private string GetSection(int Section)
        {
            if (Section == 1) return GlobalConstants.SECTION_MONING;
            if (Section == 2) return GlobalConstants.SECTION_AFTERNOON;
            if (Section == 3) return GlobalConstants.SECTION_EVENING;
            return string.Empty;
        }
    }
}
