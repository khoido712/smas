﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class ProfileTemplateBusiness
    {
        public List<ProfileTemplate> GetTemplateBySchool(int schoolId, int templateId)
        {

            IQueryable<ProfileTemplate> iQProfileTemplate = ProfileTemplateBusiness.All;
            if (templateId > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(s => s.ProfileTemplateId == templateId);
            }
            if (schoolId > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(p => p.SchoolId == schoolId || p.SchoolId == 0);
            }

            return iQProfileTemplate.OrderByDescending(c => c.CreatedDate).ToList();
        }

        public DataTable GetEmployeeInfo(int schoolId, int academicYearId)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_EMPLOYEE_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = schoolId;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = academicYearId;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetPupilInfo(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetMarkPupil(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_MARK_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_REPORT_TYPE_ID", OracleDbType.Int32).Value = ReportTypeID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetMarkVNENPupil(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_MARK_VNEN_PUPIL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetJudgementPupil(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_JUDGE_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetStudyResult(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_STUDY_RESULT",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetMarkPupilPrimary(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            int semester = 0;
            if (SemesterID == 1 || SemesterID == 2)
            {
                semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_INFO_MARK_SUBJECT_PUPIL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_PARTTION_ID", OracleDbType.Int32).Value = UtilsBusiness.GetPartionId(SchoolID);
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = semester;
                    cmd.Parameters.Add("P_SEMESTER_SELECTED", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetCapQuaPrimary(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            int semester = 0;
            if (SemesterID == 1 || SemesterID == 2)
            {
                semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_CAPACITY_QUALITY_PUPIL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_PARTTION_ID", OracleDbType.Int32).Value = UtilsBusiness.GetPartionId(SchoolID);
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = semester;
                    cmd.Parameters.Add("P_SEMESTER_SELECTED", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }
        public DataTable GetHealthPupil(int SchoolID, int AcademicYearID, int AppliedlevelID, int ProvinceID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_HEALTH_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = objAy.Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = 0;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }


        public System.IO.Stream ExportEmployeeInfo(int schoolId, int academicYearId, int templateId, int startRow)
        {
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(templateId);
            if (profileTemplate == null)
            {
                return null;
            }
            startRow += 1;
            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }


            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All.Where(x => x.ReportType == 1).ToList();

            List<ColumnDescriptionBO> lstColumn = (from temp in lstTemplate
                                                   join cd in lstColumnDescription on temp.ColDesId equals cd.ColumnDescriptionId
                                                   select new ColumnDescriptionBO
                                                   {
                                                       CellName = temp.ColExcel,
                                                       ColumnDescriptionId = cd.ColumnDescriptionId,
                                                       ColumnIndex = new VTVector(temp.ColExcel + "1").Y,
                                                       ColumnName = cd.ColumnName,
                                                       ColumnSize = cd.ColumnSize,
                                                       Resolution = temp.ExcelName,
                                                       ColumnOrder = cd.ColumnOrder,
                                                       ColumnType = cd.ColumnType,
                                                       Description = cd.Description,
                                                       RequiredColumn = cd.RequiredColumn,
                                                       TableName = cd.TableName

                                                   }).ToList();

            lstColumn = lstColumn.OrderBy(c => c.ColumnIndex).ToList();

            DataTable dtEmployee = GetEmployeeInfo(schoolId, academicYearId);

            string template = (profileTemplate.SchoolId == 0) ? "GV_ThongTinCanBo_CSDLGD.xls" : "GV_ThongTinCanBo.xls";
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "GV" + "/" + template;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemp = oBook.GetSheet(1);
            IVTWorksheet sheetCb = oBook.GetSheet(2);
            IVTRange rangeTemp = sheetTemp.GetRange("A2", "A3");

            int row = startRow;
            //Tao column cho
            for (int i = 0; i < lstColumn.Count; i++)
            {
                ColumnDescriptionBO column = lstColumn[i];
                sheetCb.CopyPasteSameSize(rangeTemp, startRow, column.ColumnIndex);
                IVTRange rgWidth = sheetCb.GetRange(startRow, column.ColumnIndex, startRow, column.ColumnIndex);
                rgWidth.Range.ColumnWidth = column.ColumnSize;
                //Header
                sheetCb.SetCellValue(row, column.ColumnIndex, column.Resolution);
            }
            row++;
            int maxColumn = lstColumn.Max(s => s.ColumnIndex);
            IVTRange rangCb = sheetCb.GetRange(row, 1, row, maxColumn);
            for (int j = 0; j < dtEmployee.Rows.Count; j++)
            {
                DataRow dr = dtEmployee.Rows[j];
                if (j > 0)
                {
                    sheetCb.CopyPasteSameSize(rangCb, row, 1);
                }
                for (int i = 0; i < lstColumn.Count; i++)
                {

                    ColumnDescriptionBO column = lstColumn[i];
                    if (dtEmployee.Columns[column.ColumnName] == null)
                    {
                        continue;
                    }
                    Type colType = dtEmployee.Columns[column.ColumnName].DataType;

                    string tempValue = dr[column.ColumnName].ToString();
                    string val = "";
                    //hien thi kieu ngay,thang
                    if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                    {
                        if (!string.IsNullOrEmpty(tempValue))
                        {
                            val = Convert.ToDateTime(tempValue).ToString("dd/MM/yyyy");
                        }
                    }
                    else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_NUMBER && colType.FullName.Contains("System.DateTime"))
                    {
                        //Kieu ngay thang nhung chi hien thi nam
                        if (!string.IsNullOrEmpty(tempValue))
                        {
                            val = Convert.ToDateTime(tempValue).Year.ToString();
                        }
                    }
                    else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_JSON)
                    {
                        if (!string.IsNullOrEmpty(column.Description) && !string.IsNullOrEmpty(tempValue))
                        {
                            List<GenreJsonObject> lstJson = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GenreJsonObject>>(column.Description);
                            if (lstJson == null)
                            {
                                val = "";
                            }
                            else
                            {
                                int genreId = Convert.ToInt32(tempValue);
                                GenreJsonObject genreJson = lstJson.FirstOrDefault(s => s.Id == genreId);
                                val = (genreJson == null) ? "" : genreJson.Name;
                            }
                        }
                        else
                        {
                            val = "";
                        }
                    }
                    else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_X)
                    {
                        val = tempValue == "1" ? "x" : "";
                    }
                    else
                    {
                        val = tempValue;
                    }
                    sheetCb.SetCellValue(row, column.ColumnIndex, val);
                }

                row++;
            }
            sheetTemp.Delete();

            return oBook.ToStream(0);
        }
        public System.IO.Stream ExportPupilInfo(int schoolId, int academicYearId, int templateId, int startRow, List<int> lstReportTypeID, int AppliedLevelID, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(templateId);
            if (profileTemplate == null)
            {
                return null;
            }
            startRow += 1;
            string template = "HS_ThongTinHocSinh.xls";
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + template;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemp = oBook.GetSheet(1);
            IVTWorksheet sheetCb = oBook.GetSheet(2);
            IVTRange rangeTemp = sheetTemp.GetRange("A2", "A3");
            int row = startRow;


            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }


            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All.Where(x => lstReportTypeID.Contains(x.ReportType.Value)).ToList();

            List<ColumnDescriptionBO> lstColumn = (from temp in lstTemplate
                                                   join cd in lstColumnDescription on temp.ColDesId equals cd.ColumnDescriptionId
                                                   select new ColumnDescriptionBO
                                                   {
                                                       CellName = temp.ColExcel,
                                                       ColumnDescriptionId = cd.ColumnDescriptionId,
                                                       ColumnIndex = new VTVector(temp.ColExcel + "1").Y,
                                                       ColumnName = cd.ColumnName,
                                                       ColumnSize = cd.ColumnSize,
                                                       Resolution = temp.ExcelName,
                                                       ColumnOrder = cd.ColumnOrder,
                                                       ColumnType = cd.ColumnType,
                                                       Description = cd.Description,
                                                       RequiredColumn = cd.RequiredColumn,
                                                       TableName = cd.TableName,
                                                       ReportType = cd.ReportType,
                                                       OrderID = cd.OrderID
                                                   }).OrderBy(p => p.OrderID).ThenBy(p => p.ColumnOrder).ToList();




            #region Cap mam non
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {

                DataTable dtPupilInfo = new DataTable();


                //Thong tin hoc sinh


                dtPupilInfo = GetPupilInfo(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID);


                DataTable dtPupilHealth = new DataTable();

                dtPupilHealth = GetHealthPupil(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID);





                //Tao column cho
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    ColumnDescriptionBO column = lstColumn[i];
                    sheetCb.CopyPasteSameSize(rangeTemp, startRow, column.ColumnIndex);
                    IVTRange rgWidth = sheetCb.GetRange(startRow, column.ColumnIndex, startRow, column.ColumnIndex);
                    rgWidth.Range.ColumnWidth = column.ColumnSize;
                    //Header
                    sheetCb.SetCellValue(row, column.ColumnIndex, column.Resolution);
                }
                row++;
                int maxColumn = lstColumn.Max(s => s.ColumnIndex);
                IVTRange rangCb = sheetCb.GetRange(row, 1, row, maxColumn);
                int OrderID = 1;
                for (int j = 0; j < dtPupilInfo.Rows.Count; j++)
                {
                    DataRow dr = dtPupilInfo.Rows[j];
                    if (j > 0)
                    {
                        sheetCb.CopyPasteSameSize(rangCb, row, 1);
                    }
                    for (int i = 0; i < lstColumn.Count; i++)
                    {

                        ColumnDescriptionBO column = lstColumn[i];
                        int PupilID = Int32.Parse(dr["PUPIL_ID"].ToString());
                        int ClassIDtmp = Int32.Parse(dr["CLASS_ID"].ToString());
                        string paraPupil = string.Format("PUPIL_ID='{0}' AND CLASS_ID='{1}'", PupilID, ClassIDtmp);

                        #region Thong tin suc khoe
                        if (column.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_16)
                        {
                            if (dtPupilHealth.Rows.Count > 0)
                            {
                                DataRow drPupilHealth = dtPupilHealth.Select(paraPupil).FirstOrDefault();
                                if (drPupilHealth != null)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, drPupilHealth[column.ColumnName]);
                                }
                                else
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }
                        }
                        #endregion
                        #region Thong tin chung
                        else
                        {
                            if (dtPupilInfo.Columns[column.ColumnName] == null)
                            {
                                continue;
                            }
                            if (column.ColumnName.Equals("ORDER_ID"))
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, OrderID);
                            }
                            else
                            {
                                Type colType = dtPupilInfo.Columns[column.ColumnName].DataType;
                                string tempValue = dr[column.ColumnName].ToString();
                                string val = "";
                                //hien thi kieu ngay,thang
                                if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                                {
                                    if (!string.IsNullOrEmpty(tempValue))
                                    {
                                        val = Convert.ToDateTime(tempValue).ToString("dd/MM/yyyy");
                                    }
                                }
                                else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_NUMBER && colType.FullName.Contains("System.DateTime"))
                                {
                                    //Kieu ngay thang nhung chi hien thi nam
                                    if (!string.IsNullOrEmpty(tempValue))
                                    {
                                        val = Convert.ToDateTime(tempValue).Year.ToString();
                                    }
                                }
                                else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_JSON)
                                {
                                    if (!string.IsNullOrEmpty(column.Description) && !string.IsNullOrEmpty(tempValue))
                                    {
                                        List<GenreJsonObject> lstJson = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GenreJsonObject>>(column.Description);
                                        if (lstJson == null)
                                        {
                                            val = "";
                                        }
                                        else
                                        {
                                            int genreId = Convert.ToInt32(tempValue);
                                            GenreJsonObject genreJson = lstJson.FirstOrDefault(s => s.Id == genreId);
                                            val = (genreJson == null) ? "" : genreJson.Name;
                                        }
                                    }
                                    else
                                    {
                                        val = "";
                                    }
                                }
                                else
                                {
                                    val = tempValue;
                                }
                                sheetCb.SetCellValue(row, column.ColumnIndex, val);
                            }
                        }
                        #endregion
                    }
                    OrderID++;
                    row++;
                }
            }
            #endregion
            #region cap 1
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {

                DataTable dtPupilInfo = new DataTable();
                DataTable dtPupilMarkPrimary = new DataTable();
                DataTable dtPupilCapQua = new DataTable();

                //thong tin hoc sinh

                dtPupilInfo = GetPupilInfo(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID);



                dtPupilMarkPrimary = GetMarkPupilPrimary(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID, SemesterID, ReportTypeID);



                dtPupilCapQua = GetCapQuaPrimary(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID, SemesterID, ReportTypeID);




                //Tao column cho
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    ColumnDescriptionBO column = lstColumn[i];
                    sheetCb.CopyPasteSameSize(rangeTemp, startRow, column.ColumnIndex);
                    IVTRange rgWidth = sheetCb.GetRange(startRow, column.ColumnIndex, startRow, column.ColumnIndex);
                    rgWidth.Range.ColumnWidth = column.ColumnSize;
                    //Header
                    sheetCb.SetRowHeight(row, 30);
                    sheetCb.SetCellValue(row, column.ColumnIndex, column.Resolution);
                    sheetCb.GetRange(row, column.ColumnIndex, row, column.ColumnIndex).WrapText();
                }
                row++;
                int maxColumn = lstColumn.Max(s => s.ColumnIndex);
                IVTRange rangCb = sheetCb.GetRange(row, 1, row, maxColumn);
                int OrderID = 1;
                for (int j = 0; j < dtPupilInfo.Rows.Count; j++)
                {
                    DataRow dr = dtPupilInfo.Rows[j];
                    if (j > 0)
                    {
                        sheetCb.CopyPasteSameSize(rangCb, row, 1);
                    }
                    for (int i = 0; i < lstColumn.Count; i++)
                    {

                        ColumnDescriptionBO column = lstColumn[i];
                        int PupilID = Int32.Parse(dr["PUPIL_ID"].ToString());
                        int ClassIDtmp = Int32.Parse(dr["CLASS_ID"].ToString());
                        int SubjectID = 0;
                        string paraPupil = string.Empty;

                        #region Thong tin mon hoc va HDGD
                        if (column.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_11)
                        {
                            SubjectID = !string.IsNullOrEmpty(column.Description) ? Int32.Parse(column.Description) : 0;
                            paraPupil = string.Format("PUPIL_ID='{0}' AND CLASS_ID='{1}' AND SUBJECT_ID='{2}'", PupilID, ClassIDtmp, SubjectID);
                            if (dtPupilMarkPrimary.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilMarkPrimary.Select(paraPupil).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, drPupil[column.ColumnName].ToString().Replace(",", "."));
                                }
                                else
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }
                        }
                        #endregion
                        #region Thong tin nang luc pham chat ket qua cuoi nam
                        else if (column.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_15)
                        {
                            SubjectID = !string.IsNullOrEmpty(column.Description) ? Int32.Parse(column.Description) : 0;
                            if (SubjectID < 8)
                            {
                                paraPupil = string.Format("PUPIL_ID='{0}' AND CLASS_ID='{1}' AND SUBJECT_ID='{2}'", PupilID, ClassIDtmp, SubjectID);
                            }
                            else
                            {
                                paraPupil = string.Format("PUPIL_ID='{0}' AND CLASS_ID='{1}'", PupilID, ClassIDtmp);
                            }
                            if (dtPupilCapQua.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilCapQua.Select(paraPupil).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, drPupil[column.ColumnName]);
                                }
                                else
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }
                        }
                        #endregion
                        #region Thong tin chung
                        else
                        {
                            if (dtPupilInfo.Columns[column.ColumnName] == null)
                            {
                                continue;
                            }
                            if (column.ColumnName.Equals("ORDER_ID"))
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, OrderID);
                            }
                            else
                            {
                                Type colType = dtPupilInfo.Columns[column.ColumnName].DataType;
                                string tempValue = dr[column.ColumnName].ToString();
                                string val = "";
                                //hien thi kieu ngay,thang
                                if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                                {
                                    if (!string.IsNullOrEmpty(tempValue))
                                    {
                                        val = Convert.ToDateTime(tempValue).ToString("dd/MM/yyyy");
                                    }
                                }
                                else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_NUMBER && colType.FullName.Contains("System.DateTime"))
                                {
                                    //Kieu ngay thang nhung chi hien thi nam
                                    if (!string.IsNullOrEmpty(tempValue))
                                    {
                                        val = Convert.ToDateTime(tempValue).Year.ToString();
                                    }
                                }
                                else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_JSON)
                                {
                                    if (!string.IsNullOrEmpty(column.Description) && !string.IsNullOrEmpty(tempValue))
                                    {
                                        List<GenreJsonObject> lstJson = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GenreJsonObject>>(column.Description);
                                        if (lstJson == null)
                                        {
                                            val = "";
                                        }
                                        else
                                        {
                                            int genreId = Convert.ToInt32(tempValue);
                                            GenreJsonObject genreJson = lstJson.FirstOrDefault(s => s.Id == genreId);
                                            val = (genreJson == null) ? "" : genreJson.Name;
                                        }
                                    }
                                    else
                                    {
                                        val = "";
                                    }
                                }
                                else
                                {
                                    val = tempValue;
                                }
                                sheetCb.SetCellValue(row, column.ColumnIndex, val);
                            }
                        }
                        #endregion
                    }
                    OrderID++;
                    row++;
                }
            }
            #endregion
            #region cap 2,3
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY || AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                DataTable dtPupilInfo = new DataTable();
                DataTable dtPupilJudge = new DataTable();
                DataTable dtPupilMark = new DataTable();
                DataTable dtPupilMarkVNEN = new DataTable();
                DataTable dtStudyResult = new DataTable();

                //List<Task> taskRun = new List<Task>();

                dtPupilInfo = GetPupilInfo(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID);



                if (ReportTypeID == 2)
                {
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                    {
                        SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                    }

                    dtPupilJudge = GetJudgementPupil(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID, SemesterID);

                }

                dtPupilMark = GetMarkPupil(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID, SemesterID, ReportTypeID);

                dtPupilMarkVNEN = GetMarkVNENPupil(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID, SemesterID);


                dtStudyResult = GetStudyResult(schoolId, academicYearId, AppliedLevelID, 0, 0, "", "", null, EducationLevelID, ClassID, SemesterID, ReportTypeID);


                //Tao column cho
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    ColumnDescriptionBO column = lstColumn[i];
                    sheetCb.CopyPasteSameSize(rangeTemp, startRow, column.ColumnIndex);
                    IVTRange rgWidth = sheetCb.GetRange(startRow, column.ColumnIndex, startRow, column.ColumnIndex);
                    rgWidth.Range.ColumnWidth = column.ColumnSize;
                    //Header
                    sheetCb.SetCellValue(row, column.ColumnIndex, column.Resolution);
                }
                row++;
                int maxColumn = lstColumn.Max(s => s.ColumnIndex);
                IVTRange rangCb = sheetCb.GetRange(row, 1, row, maxColumn);
                int OrderID = 1;
                bool isData = false;
                for (int j = 0; j < dtPupilInfo.Rows.Count; j++)
                {
                    DataRow dr = dtPupilInfo.Rows[j];
                    if (j > 0)
                    {
                        sheetCb.CopyPasteSameSize(rangCb, row, 1);
                    }
                    for (int i = 0; i < lstColumn.Count; i++)
                    {

                        ColumnDescriptionBO column = lstColumn[i];
                        int PupilID = Int32.Parse(dr["PUPIL_ID"].ToString());
                        int ClassIDtmp = Int32.Parse(dr["CLASS_ID"].ToString());
                        isData = false;
                        string paraPupil = string.Empty;

                        #region Thong tin mon hoc
                        if (column.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_12
                        || column.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_13)
                        {
                            int SubjectID = !string.IsNullOrEmpty(column.Description) ? Int32.Parse(column.Description) : 0;
                            paraPupil = string.Format("PUPIL_ID='{0}' AND CLASS_ID='{1}' AND SUBJECT_ID='{2}'", PupilID, ClassIDtmp, SubjectID);

                            if (dtPupilMark.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilMark.Select(paraPupil).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    int isCommenting = Int32.Parse(drPupil["IS_COMMENTING"].ToString());
                                    if (ReportTypeID == 1)
                                    {
                                        sheetCb.SetCellValue(row, column.ColumnIndex, isCommenting == 0 ? drPupil["SUMMED_UP_MARK"].ToString().Replace(",", ".") : drPupil["JUDGEMENT_RESULT"]);
                                    }
                                    else
                                    {
                                        sheetCb.SetCellValue(row, column.ColumnIndex, drPupil["MARK"].ToString().Replace(",", "."));
                                    }
                                    isData = true;
                                }
                                else if (!isData)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else if (!isData)
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }

                            if (dtPupilJudge.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilJudge.Select(paraPupil).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, drPupil["MARK"].ToString().Replace(",", "."));
                                    isData = true;
                                }
                                else if (!isData)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else if (!isData)
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }

                            if (dtPupilMarkVNEN.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilMarkVNEN.Select(paraPupil).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    if (ReportTypeID == 1)
                                    {
                                        sheetCb.SetCellValue(row, column.ColumnIndex, drPupil["SUMMED_UP_MARK"].ToString().Replace(",", "."));
                                    }
                                    else
                                    {
                                        sheetCb.SetCellValue(row, column.ColumnIndex, drPupil["MARK"].ToString().Replace(",", "."));
                                    }
                                    isData = true;
                                }
                                else if (!isData)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else if (!isData)
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }
                        }
                        #endregion
                        #region Ket qua hoc tap,ren luyen
                        else if (column.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_GENERAL_14)
                        {
                            if (dtStudyResult.Rows.Count > 0)
                            {
                                paraPupil = string.Format("PUPIL_ID='{0}' AND CLASS_ID='{1}'", PupilID, ClassIDtmp);
                                DataRow drPupil = dtStudyResult.Select(paraPupil).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, column.ColumnName.Equals("AVERAGE_MARK") ? drPupil[column.ColumnName].ToString().Replace(",", ".") : drPupil[column.ColumnName]);
                                }
                                else
                                {
                                    sheetCb.SetCellValue(row, column.ColumnIndex, "");
                                }
                            }
                            else
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, "");
                            }
                        }
                        #endregion
                        #region Thong tin chung
                        else
                        {
                            if (dtPupilInfo.Columns[column.ColumnName] == null)
                            {
                                continue;
                            }
                            if (column.ColumnName.Equals("ORDER_ID"))
                            {
                                sheetCb.SetCellValue(row, column.ColumnIndex, OrderID);
                            }
                            else
                            {
                                Type colType = dtPupilInfo.Columns[column.ColumnName].DataType;
                                string tempValue = dr[column.ColumnName].ToString();
                                string val = "";
                                //hien thi kieu ngay,thang
                                if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                                {
                                    if (!string.IsNullOrEmpty(tempValue))
                                    {
                                        try
                                        {
                                            val = DateTime.ParseExact(tempValue, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error("Loi dinh dang ngay/thang/nam" + tempValue, ex);
                                            val = tempValue;
                                        }
                                    }
                                }
                                else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_NUMBER && colType.FullName.Contains("System.DateTime"))
                                {
                                    //Kieu ngay thang nhung chi hien thi nam
                                    if (!string.IsNullOrEmpty(tempValue))
                                    {
                                        val = Convert.ToDateTime(tempValue).Year.ToString();
                                    }
                                }
                                else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_JSON)
                                {
                                    if (!string.IsNullOrEmpty(column.Description) && !string.IsNullOrEmpty(tempValue))
                                    {
                                        List<GenreJsonObject> lstJson = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GenreJsonObject>>(column.Description);
                                        if (lstJson == null)
                                        {
                                            val = "";
                                        }
                                        else
                                        {
                                            int genreId = Convert.ToInt32(tempValue);
                                            GenreJsonObject genreJson = lstJson.FirstOrDefault(s => s.Id == genreId);
                                            val = (genreJson == null) ? "" : genreJson.Name;
                                        }
                                    }
                                    else
                                    {
                                        val = "";
                                    }
                                }
                                else
                                {
                                    val = tempValue;
                                }
                                sheetCb.SetCellValue(row, column.ColumnIndex, val);
                            }
                        }
                        #endregion
                    }
                    OrderID++;
                    row++;
                }
            }
            #endregion
            sheetTemp.Delete();
            return oBook.ToStream(0);
        }

        public IQueryable<ProfileTemplate> Search(IDictionary<string, object> dic)
        {
            int TemplateID = Utils.GetInt(dic, "TemplateID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ReportType = Utils.GetInt(dic, "ReportType");
            int UnitID = Utils.GetInt(dic, "UnitID");
            string checkUnit = Utils.GetString(dic, "checkUnit");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");

            IQueryable<ProfileTemplate> iQProfileTemplate = ProfileTemplateBusiness.All;

            if (TemplateID > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(s => s.ProfileTemplateId == TemplateID);
            }

            if (SchoolID > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(p => p.SchoolId == SchoolID || p.SchoolId == -1);
            }

            if (ReportType > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(p => p.ReportType == ReportType);
            }

            if (UnitID > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(p => p.UnitID == UnitID || p.UnitID == 0);
            }

            // Lấy mẫu excel cho admin
            if (!string.IsNullOrEmpty(checkUnit) && UnitID == 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(p => p.UnitID == UnitID);
            }
            if (AppliedLevelID > 0)
            {
                iQProfileTemplate = iQProfileTemplate.Where(p => p.AppliedLevelID == AppliedLevelID);
            }
            return iQProfileTemplate.OrderByDescending(c => c.CreatedDate);
        }

        public Stream ExportInfoSchool(IDictionary<string, object> dic)
        {
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int districtId = Utils.GetInt(dic, "DistrictID");

            int templateId = Utils.GetInt(dic, "TemplateID");
            int startRow = Utils.GetInt(dic, "StartRow");

            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(templateId);

            if (profileTemplate == null)
            {
                return null;
            }
            startRow += 1;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Phong_So" + "/" + "DanhSachThongTinTruong.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemp = oBook.GetSheet(1);

            #region // Get data
            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }

            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All
                .Where(x => x.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE).ToList();

            List<ColumnDescriptionBO> lstColumn = (from temp in lstTemplate
                                                   join cd in lstColumnDescription on temp.ColDesId equals cd.ColumnDescriptionId
                                                   select new ColumnDescriptionBO
                                                   {
                                                       CellName = temp.ColExcel,
                                                       ColumnDescriptionId = cd.ColumnDescriptionId,
                                                       ColumnIndex = new VTVector(temp.ColExcel + "1").Y,
                                                       ColumnName = cd.ColumnName,
                                                       ColumnSize = cd.ColumnSize,
                                                       Resolution = temp.ExcelName,
                                                       ColumnOrder = cd.ColumnOrder,
                                                       ColumnType = cd.ColumnType,
                                                       Description = cd.Description,
                                                       RequiredColumn = cd.RequiredColumn,
                                                       TableName = cd.TableName
                                                   }).ToList();
            lstColumn = lstColumn.OrderBy(c => c.ColumnIndex).ToList();
            DataTable dtEmployee = this.GetSchoolInfo(provinceId, districtId);
            #endregion

            #region //Fill data
            int rawBorder = startRow;
            int row = startRow;
            for (int i = 0; i < lstColumn.Count; i++)
            {
                ColumnDescriptionBO column = lstColumn[i];
                sheetTemp.SetCellValue(row, column.ColumnIndex, column.Resolution);
                sheetTemp.SetColumnWidth(column.ColumnIndex, Convert.ToDouble(column.ColumnSize));
            }

            int maxColumn = lstColumn.Max(s => s.ColumnIndex);
            sheetTemp.GetRange(row, 1, row, maxColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
            sheetTemp.GetRange(row, 1, row, maxColumn).FillColor(System.Drawing.Color.Gray);
            row++;

            bool isFillFeatureSchool = false;
            List<PropertyOfSchool> lstPropertyOfSChool = new List<PropertyOfSchool>();
            List<int> lstSchoolID = new List<int>();
            int schoolID = 0;
            if (lstColumn.Where(x => x.ColumnName.Contains("FEATURE_OF_SCHOOL")).Count() > 0)
            {
                isFillFeatureSchool = true;
                for (int j = 0; j < dtEmployee.Rows.Count; j++)
                {
                    DataRow dr = dtEmployee.Rows[j];
                    string tempValue = dr["SCHOOL_PROFILE_ID"].ToString();
                    if (!string.IsNullOrEmpty(tempValue))
                    {
                        schoolID = Convert.ToInt32(tempValue);
                        lstSchoolID.Add(schoolID);
                    }
                }
                lstPropertyOfSChool = (from ps in PropertyOfSchoolBusiness.All.Where(x => x.IsActive && lstSchoolID.Contains(x.SchoolID))
                                       join sc in SchoolPropertyCatBusiness.All on ps.SchoolPropertyCatID equals sc.SchoolPropertyCatID
                                       select ps).ToList();
            }

            List<PropertyOfSchool> lstPropertBySchool = new List<PropertyOfSchool>();
            for (int j = 0; j < dtEmployee.Rows.Count; j++)
            {
                DataRow dr = dtEmployee.Rows[j];

                if (isFillFeatureSchool)
                {
                    string value = dr["SCHOOL_PROFILE_ID"].ToString();
                    schoolID = Convert.ToInt32(value);
                    lstPropertBySchool = lstPropertyOfSChool.Where(x => x.SchoolID == schoolID).ToList();
                }

                for (int i = 0; i < lstColumn.Count; i++)
                {
                    ColumnDescriptionBO column = lstColumn[i];

                    // Fill tính chất của trường
                    if (isFillFeatureSchool && column.ColumnName.Contains("FEATURE_OF_SCHOOL"))
                    {
                        bool check = false;
                        switch (column.ColumnName)
                        {
                            case "FEATURE_OF_SCHOOL_01": // ĐẠT CHUẨN QUỐC GIA
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 2).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_02": // Trường dành cho người tàn tật, khuyết tật
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 13).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_03": //  Có tổ chức dạy nghề PT
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 10).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_04": // Có học sinh bán trú
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 7).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_05": // Trường đạt chuẩn THTT-HSTC
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 12).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_06": // Dạy học 2 buổi/ngày
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 9).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_07": // Có học sinh nội trú
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 8).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_08": //  Đạt mức chất lượng tối thiểu
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 3).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_09": // Thuộc vùng đặc biệt khó khăn
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 1).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_10": // Có thể liên lạc bằng sóng di động
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 25).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_11": // Có chi bộ Đảng
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 5).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_12": // Trường quốc tế
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 4).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_13": // Có điện thoại cố định
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 24).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_14": // Có học sinh khuyết tật
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 6).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_15": // Đóng trên địa bàn xã biên giới
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 23).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_16": // Phổ biến cho CM HS về các CTGD phòng, chống HIV
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 22).FirstOrDefault() != null ? true : false;
                                break;
                            case "FEATURE_OF_SCHOOL_17": //Có lớp không chuyên (đối với trường chuyên)
                                check = lstPropertBySchool.Where(x => x.SchoolPropertyCatID == 11).FirstOrDefault() != null ? true : false;
                                break;
                        }
                        sheetTemp.SetCellValue(row, column.ColumnIndex, check ? "x" : "");
                        sheetTemp.GetRange(row, column.ColumnIndex, row, column.ColumnIndex).SetHAlign(VTHAlign.xlHAlignLeft);
                    }

                    if (column.ColumnName.Equals("PASSWORD"))
                    {
                        sheetTemp.SetCellValue(row, column.ColumnIndex, "********");
                        sheetTemp.GetRange(row, column.ColumnIndex, row, column.ColumnIndex).SetHAlign(VTHAlign.xlHAlignLeft);
                    }

                    if (dtEmployee.Columns[column.ColumnName] == null)
                    {
                        continue;
                    }

                    string tempValue = dr[column.ColumnName].ToString();
                    string val = "";
                    //hien thi kieu ngay,thang
                    if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                    {
                        if (!string.IsNullOrEmpty(tempValue))
                        {
                            val = Convert.ToDateTime(tempValue).ToString("dd/MM/yyyy");
                        }
                    }
                    else if (column.ColumnType == GlobalConstants.COLUMN_TYPE_X)
                    {
                        val = tempValue == "x" ? "x" : "";
                    }
                    else
                    {
                        val = tempValue;
                    }
                    sheetTemp.SetCellValue(row, column.ColumnIndex, column.ColumnName.Equals("PASSWORD") ? "********" : (!string.IsNullOrEmpty(val) ? val : ""));
                    sheetTemp.GetRange(row, column.ColumnIndex, row, column.ColumnIndex).SetHAlign(VTHAlign.xlHAlignLeft);
                }
                row++;
            }
            sheetTemp.GetRange(rawBorder, 1, (dtEmployee.Rows.Count + rawBorder), maxColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheetTemp.SetFontName("Times New Roman", 12);
            #endregion

            return oBook.ToStream(0);
        }

        private DataTable GetSchoolInfo(int provinceId, int districtId)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_SCHOOL_PROFILE",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = provinceId;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = districtId;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

    }
}