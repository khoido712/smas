﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class HabitGroupBusiness
    {
        #region Search


        /// <summary>
        /// Tìm kiếm sở thích chi tiết của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<HabitGroup> Search(IDictionary<string, object> dic)
        {
            string HabitGroupName = Utils.GetString(dic, "HabitGroupName");
            int PositionOrder = Utils.GetInt(dic, "PositionOrder");      
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<HabitGroup> lsHabitGroup = HabitGroupRepository.All;
            if (IsActive.HasValue)
            {
                lsHabitGroup = lsHabitGroup.Where(cfh => cfh.IsActive == IsActive);
            }
            if (HabitGroupName.Trim().Length != 0)
            {
                lsHabitGroup = lsHabitGroup.Where(cfh => cfh.HabitGroupName.Contains(HabitGroupName));
            }
            if (PositionOrder != 0)
            {
                lsHabitGroup = lsHabitGroup.Where(cfh => cfh.PositionOrder == PositionOrder);
            }

            return lsHabitGroup;

        }
        #endregion
        
    }
}