﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// quá trình đào tạo của giáo viên, nhân viên
    /// <author>dungnt</author>
    /// <date>06/09/2012</date>
    /// </summary>
    public partial class EmployeeQualificationBusiness
    {

        #region private member variable
        private const int FROMDATE_SUBTRACT_BIRTHDATE = 17; //Ngày bắt đầu đào tạo lớn hơn ngày sinh 17 năm
        private const int DESCRIPTION_MAXLENGTH = 400;//Ghi chú không được nhập quá 400 ký tự
        private const int QUALIFIEDBY_MAXLENGTH = 200;//Đơn vị đào tạo không được nhập quá 200 ký tự
        private const int QUALIFIEDAT_MAXLENGTH = 400;//Nơi đào tạo không được nhập quá 400 ký tự.

        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới quá trình đào tạo của giáo viên, nhân viên
        /// <author>dungnt</author>
        /// <date>06/09/2012</date>
        /// </summary>
        /// <param name="insertEmployeeQualification">Đối tượng Insert</param>
        /// <returns></returns>
        public override EmployeeQualification Insert(EmployeeQualification insertEmployeeQualification)
        {
            Validate(insertEmployeeQualification);
            //Thực hiện Insert dữ liệu vào bảng EmployeeQualification
            return base.Insert(insertEmployeeQualification);
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa quá trình đào tạo của giáo viên, nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="updateEmployeeQualification">Đối tượng Update</param>
        /// <returns></returns>
        public override EmployeeQualification Update(EmployeeQualification updateEmployeeQualification)
        {
            //Kiểm tra EmployeeQualificationID có tồn tại hay không?
            CheckAvailable(updateEmployeeQualification.EmployeeQualificationID, "EmployeeQualification_Label_EmployeeQualificationID", false);

            //EmployeeQualificationID, SchoolID: not compatible(EmployeeQualification)
            if(updateEmployeeQualification.SchoolID.HasValue)
            {
                bool EmployeeQualificationCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeQualification",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",updateEmployeeQualification.SchoolID},
                    {"EmployeeQualificationID",updateEmployeeQualification.EmployeeQualificationID}
                }, null);
                if (!EmployeeQualificationCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //EmployeeQualificationID, SupervisingDeptID: not compatible(EmployeeQualification)
            if (updateEmployeeQualification.SupervisingDeptID != null)
            {
                bool EmployeeQualificationCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeQualification",
                   new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",updateEmployeeQualification.SupervisingDeptID},
                    {"EmployeeQualificationID",updateEmployeeQualification.EmployeeQualificationID}
                }, null);
                if (!EmployeeQualificationCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            Validate(updateEmployeeQualification);
            //Thực hiện Update dữ liệu vào bảng EmployeeQualification
            return base.Update(updateEmployeeQualification);
        }
        #endregion

        #region Delete
       /// <summary>
       /// Xóa quá trình đào tạo của giáo viên, nhân viên
       /// <Author>dungnt</Author>
       /// <DateTime>06/09/2012</DateTime>
       /// </summary>
        /// <param name="id">ID quá trình đào tạo giáo viên/ nhân viên</param>
        public void Delete(int SchoolOrSupervisingDeptID, int EmployeeQualificationID)
        {
            //Bản ghi này đã bị xóa hoặc không tồn tại trong cơ sở dữ liệu. Kiểm tra EmployeeQualificationID != NULL
             CheckAvailable(EmployeeQualificationID, "EmployeeQualification_Label_EmployeeQualificationID", false);
             EmployeeQualification temp = EmployeeQualificationRepository.Find(EmployeeQualificationID);

             Employee checkEmployee = EmployeeRepository.Find(temp.EmployeeID);
             //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SCHOOL_TEACHER: 
             if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
             {

                 //EmployeeID, SchoolOrSupervisingDeptID: not compatible(Employee)
                 bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                   new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolOrSupervisingDeptID},
                    {"EmployeeID",checkEmployee.EmployeeID}
                }, null);
                 if (!EmployeeCompatible)
                 {
                     throw new BusinessException("Common_Validate_NotCompatible");
                 }

                 //EmployeeQualificationID, SchoolOrSupervisingDeptID(SchoolID): not compatible(EmployeeQualification)
                 bool EmployeeQualificationCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeQualification",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolOrSupervisingDeptID},
                    {"EmployeeQualificationID",EmployeeQualificationID}
                }, null);
                 if (!EmployeeQualificationCompatible)
                 {
                     throw new BusinessException("Common_Validate_NotCompatible");
                 }



             }

             //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
             if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
             {

                 //SupervisingDeptID, SchoolID: not compatible(Employee)
                 bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                   new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SchoolOrSupervisingDeptID},
                    {"EmployeeID",checkEmployee.EmployeeID}
                }, null);
                 if (!EmployeeCompatible)
                 {
                     throw new BusinessException("Common_Validate_NotCompatible");
                 }

                 //+ EmployeeQualificationID, SchoolOrSupervisingDeptID(SupervisingDeptID): not compatible(EmployeeQualification)
                 bool EmployeeQualificationCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeQualification",
                 new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SchoolOrSupervisingDeptID},
                    {"EmployeeQualificationID",EmployeeQualificationID}
                }, null);
                 if (!EmployeeQualificationCompatible)
                 {
                     throw new BusinessException("Common_Validate_NotCompatible");
                 }
             }

            //Thực hiện việc xóa dữ liệu trong bảng EmployeeQualification
             base.Delete(EmployeeQualificationID,false);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm quá trình đào tạo của giáo viên, nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="dic">danh sách tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        private IQueryable<EmployeeQualification> Search(IDictionary<string, object> dic)
        {
            //-	SchoolID: default = 0; 0 => Tìm kiếm All
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            //-	SupervisingDeptID: default = 0; 0 => Tìm kiếm All
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            //-	SpecialityID: default = 0 ; 0 => Tìm kiếm All
            int SpecialityID = Utils.GetInt(dic, "SpecialityID");
            //-	QualificationTypeID: defautl = 0 ; 0 => Tìm kiếm All
            int QualificationTypeID = Utils.GetInt(dic, "QualificationTypeID");
            //-	TrainingLevelID: default = 0 ; 0 => Tìm kiếm All
            int TrainingLevelID = Utils.GetInt(dic, "TrainingLevelID");
            //-	QualificationGrade: default = 0; 0 => tìm kiếm All
            //int QualificationGradeID = Utils.GetInt(dic, "QualificationGradeID");
            //-	QualifiedAt: default = “ ”; “ ” => Tìm kiếm All
            string QualifiedAt = Utils.GetString(dic, "QualifiedAt");
            //-	EmployeeID: default = 0; 0 => Tìm kiếm All
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            string Result = Utils.GetString(dic, "Result");

            IQueryable<EmployeeQualification> lsEmployeeQualification = this.EmployeeQualificationRepository.All;

            if(SchoolID!=0)
            {
                lsEmployeeQualification = lsEmployeeQualification.Where(o=>(o.SchoolID==SchoolID)).OrderByDescending(p=>p.FromDate);
            }
             if(SupervisingDeptID!=0)
            {
                 lsEmployeeQualification = lsEmployeeQualification.Where(o=>(o.SupervisingDeptID==SupervisingDeptID));
            }
             if(SpecialityID!=0)
            {
                  lsEmployeeQualification = lsEmployeeQualification.Where(o=>(o.SpecialityID==SpecialityID));
            }
             if (QualificationTypeID != 0)
             {
                 lsEmployeeQualification = lsEmployeeQualification.Where(o => (o.QualificationTypeID == QualificationTypeID));
             }
             if(TrainingLevelID!=0)
            {
                  lsEmployeeQualification = lsEmployeeQualification.Where(o=>(o.Employee.TrainingLevelID==TrainingLevelID));
            }
            // if(QualificationGradeID!=0)
            //{
            //      lsEmployeeQualification = lsEmployeeQualification.Where(o=>(o.QualificationGradeID==QualificationGradeID));
            //}
             if(EmployeeID!=0)
            {
                  lsEmployeeQualification = lsEmployeeQualification.Where(o=>(o.EmployeeID==EmployeeID));
            }
             if(QualifiedAt.Trim().Length!=0)
            {
                lsEmployeeQualification = lsEmployeeQualification.Where(o => (o.QualifiedAt.ToUpper().Contains(QualifiedAt.ToUpper())));
            }
             if (Result.Trim().Length != 0)
             {
                 lsEmployeeQualification = lsEmployeeQualification.Where(o => (o.Result.ToUpper().Contains(Result.ToUpper())));
             }

             return lsEmployeeQualification;

        }


        public IQueryable<EmployeeQualification> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        public IQueryable<EmployeeQualification> SearchBySupervisingDept(int SupervisingDeptID, IDictionary<string, object> dic = null)
        {
            if (SupervisingDeptID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SupervisingDeptID"] = SupervisingDeptID;
            return Search(dic);
        }
        #endregion

        #region validate
        public void Validate(EmployeeQualification insertEmployeeQualification)
        {
            //Kiểm tra có tồn tại nhân viên này hay không? Kiểm tra EmployeeID != NULL
            EmployeeBusiness.CheckAvailable(insertEmployeeQualification.EmployeeID, "Employee_Label_EmployeeID");

            ////Chuyên nghành được đào tạo không tồn tại hoặc đã bị xóa – Kiểm tra SpecialityID trong bảng SpecialityCat với isActive = 1
            //SpecialityCatBusiness.CheckAvailable(insertEmployeeQualification.SpecialityID, "Employee_Label_SpecialityCatID");

            ////Hình thức đào tạo bằng cấp không tồn tại hoặc đã bị xóa – Kiểm tra QualificationTypeID trong bảng QualificationType với isActive  = 1
            //QualificationTypeBusiness.CheckAvailable(insertEmployeeQualification.QualificationTypeID, "QualificationType_Label_QualificationTypeID");

            //Cấp độ đạo tạo không tồn tại hoặc đã bị xóa – Kiểm tra TrainingLevelID trong bảng TrainingLevel với isActive = 1
            //if (insertEmployeeQualification.Employee != null)
            //{
            //    TrainingLevelBusiness.CheckAvailable(insertEmployeeQualification.Employee.TrainingLevelID, "TrainingLevel_Label_TrainingLevelID");
            //}else
            //{
            //    Employee e = EmployeeBusiness.Find(insertEmployeeQualification.EmployeeID);
            //    TrainingLevelBusiness.CheckAvailable(e.TrainingLevelID, "TrainingLevel_Label_TrainingLevelID");
            //}

            //Xếp loại văn bằng đào tạo không tồn tại hoặc đã bị xóa – Kiểm tra QualificationGradeID trong bảng QualificationGrade với isActive = 1
            //QualificationGradeBusiness.CheckAvailable(insertEmployeeQualification.QualificationGradeID, "QualificationGrade_Label_QualificationGradeID");

            //Ghi chú không được nhập quá 400 ký tự - Kiểm tra Description
            Utils.ValidateMaxLength(insertEmployeeQualification.Description, DESCRIPTION_MAXLENGTH, "EmployeeQualification_Label_Description");

            //Nơi đào tạo không được nhập quá 400 ký tự. – Kiểm tra QualifiedAt
            Utils.ValidateMaxLength(insertEmployeeQualification.QualifiedAt, QUALIFIEDAT_MAXLENGTH, "EmployeeQualification_Label_QualifiedAt");

            //Ngày bắt đầu đào tạo phải lớn hơn ngày sinh + 17 và nhỏ hơn ngày hiện tại – Kiểm tra FromDate
            Employee checkEmployee = EmployeeRepository.All.Where(o => (o.EmployeeID == insertEmployeeQualification.EmployeeID)).FirstOrDefault();
            //if (checkEmployee.BirthDate != null)
            //{
            //    Utils.ValidateAfterDate(insertEmployeeQualification.FromDate.Value, checkEmployee.BirthDate.Value, FROMDATE_SUBTRACT_BIRTHDATE, "EmployeeQualification_Label_FromDate", "Employee_Label_BirthDate");
            //}
            //Utils.ValidateAfterDate(DateTime.Now, insertEmployeeQualification.FromDate.Value, "Employee_Label_Date_Now", "EmployeeQualification_Label_FromDate");
            if (insertEmployeeQualification.FromDate.Value > DateTime.Now)
            {
                throw new BusinessException("EmployeeQualification_Validate_FromDate");
            }
            //FromDate < ToDate (nếu ToDate != NULL)
            if (insertEmployeeQualification.QualifiedDate.HasValue)
            {
                Utils.ValidateAfterDate(insertEmployeeQualification.QualifiedDate.Value, insertEmployeeQualification.FromDate.Value, "Employee_Label_QualifiedDate", "EmployeeQualification_Label_FromDate");
            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SCHOOL_TEACHER: 
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {
                //Kiểm tra sự tồn tại của SchoolID truyền vào
                SchoolProfileBusiness.CheckAvailable(insertEmployeeQualification.SchoolID, "SchoolProfile_Label_SchoolID");
                
                //EmployeeID, SchoolID: not compatible(Employee)
                if (insertEmployeeQualification.SchoolID.HasValue)
                {
                    bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                      new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployeeQualification.SchoolID},
                    {"EmployeeID",insertEmployeeQualification.EmployeeID}
                }, null);
                    if (!EmployeeCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {
                //+ SupervisingDeptID: require
                //Phòng ban không tồn tại – Kiểm tra SupervisingDeptID
                SupervisingDeptBusiness.CheckAvailable(insertEmployeeQualification.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID");

                if (insertEmployeeQualification.SupervisingDeptID.HasValue)
                {
                    //SupervisingDeptID, EmployeeID: not compatible(Employee)
                    bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                      new Dictionary<string, object>()
                {
                    {"EmployeeID",insertEmployeeQualification.EmployeeID},
                    {"SupervisingDeptID",insertEmployeeQualification.SupervisingDeptID}
                }, null);
                    if (!EmployeeCompatible)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
            }
        }
        #endregion
        
    }
}