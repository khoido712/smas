﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SkillBusiness
    {
        public IQueryable<Skill> Search(IDictionary<string, object> dic)
        {
            int SkillID = Utils.GetInt(dic, "SkillID");
            int SkillTitleID = Utils.GetInt(dic, "SkillTitleID");
            string Description = Utils.GetString(dic, "Description");
            System.DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            System.DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");

            String Period = Utils.GetString(dic, "Period");
            System.DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            System.DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            IQueryable<Skill> Query = SkillRepository.All;

            if (SkillID != 0)
            {
                Query = Query.Where(evd => evd.SkillID == SkillID);
            }
            if (SkillTitleID != 0)
            {
                Query = Query.Where(evd => evd.SkillTitleID == SkillTitleID);
            }
            if (FromDate.HasValue)
            {
                Query = Query.Where(evd => evd.FromDate == FromDate);
            }
            if (ToDate.HasValue)
            {
                Query = Query.Where(evd => evd.ToDate == ToDate);
            }
            if (Period.Trim().Length > 0)
            {
                //Query = Query.Where(evd => evd.Period == Period);
            }
            if (Description.Trim().Length != 0) Query = Query.Where(o => o.Description.Contains(Description.ToLower()));

            return Query;
        }

        public void Insert(List<Skill> lstSkill)
        {
            foreach (Skill item in lstSkill)
            {
                ValidationMetadata.ValidateObject(item);
                if (item.FromDate > item.ToDate)
                {
                    throw new BusinessException("Skill_Validate_DOW");
                }
                base.Insert(item);
            }
        }

        public void Delete(List<int> lsSkillID)
        {
            if (lsSkillID == null || lsSkillID.Count <= 0) return;
            for (int i = 0; i < lsSkillID.Count; i++)
            {
                Skill obj = this.Find(lsSkillID[i]);
                this.Delete(obj);
            }
            this.Save();
        }
        
    }
}