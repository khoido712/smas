﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using System.Drawing;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ApprenticeshipTrainingBusiness
    {
        #region Them moi them ham truyen vao truong, nam hoc, khoi, lop
        /// <summary>
        /// QuangLM sua 27/05/2013 - Dua cac cau truy van ra ngoai vong for
        /// Cac cau validate sua lai vi cac thong tin truong, nam hoc, lop hoc, lop nghe thuong la trung nhau
        /// </summary>
        /// <param name="ListApprenticeshipTraining"></param>
        public void InsertApprenticeshipTraining(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID,
            List<ApprenticeshipTraining> ListApprenticeshipTraining)
        {
            //Kiem tra gia tri truyen vao
            if (ListApprenticeshipTraining == null)
            {
                throw new BusinessException("ApprenticeshipTraining_Err_EmptyList");
            }

            //Kiem tra ClassID
            SchoolProfileBusiness.CheckAvailable(SchoolID, "ApprenticeshipTraining_Label_ClassID", true);
            AcademicYearBusiness.CheckAvailable(AcademicYearID, "ApprenticeshipTraining_Label_AcadYear");
            EducationLevelBusiness.CheckAvailable(EducationLevelID, "ApprenticeshipTraining_Label_ClassID", true);
            ClassProfileBusiness.CheckAvailable(ClassID, "ApprenticeshipTraining_Label_ClassID", true);

            // Lay danh sach hoc sinh trong lop
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
            // Danh sach hoc sinh da dang ky hoc lop nghe
            List<ApprenticeshipTraining> listAssignedApprenticeshipTraining = SearchBySchool(SchoolID, SearchInfo).ToList();

            foreach (ApprenticeshipTraining ApprenticeshipTraining in ListApprenticeshipTraining)
            {
                //Kiem tra AcadYearID,PupilID
                PupilProfileBusiness.CheckAvailable(ApprenticeshipTraining.PupilID, "ApprenticeshipTraining_Label_Pupil", true);

                // Kiem tra ProfileStatus ==1

                //Do trong db co nhieu ban ghi co cung (PupilID, ClassID,AcaID) ma chi khac nhau Status nen phai lay ra ban ghi co PupilOfClassID lon nhat la trang thai moi nhat cua hoc sinh do
                PupilOfClass pupilOfClass = listPupilOfClass.Where(o => o.PupilID == ApprenticeshipTraining.PupilID).FirstOrDefault();

                if (pupilOfClass != null)
                {
                    if (listPupilOfClass.FirstOrDefault().Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        Utils.ValidateMaxLength(ApprenticeshipTraining.Description, 200, "ApprenticeshipTraining_Label_Description");
                        //Kiem tra lai xem da ton tai hoc sinh trong lop nghe do chua                       
                        ApprenticeshipTraining ap = listAssignedApprenticeshipTraining.Where(o => o.PupilID == ApprenticeshipTraining.PupilID
                            && o.ApprenticeShipClassID == ApprenticeshipTraining.ApprenticeShipClassID).FirstOrDefault();
                        //Neu chua ton tai thi them moi
                        if (ap == null)
                        {
                            //Du lieu hop le them moi
                            base.Insert(ApprenticeshipTraining);
                        }
                    }
                    else
                    {
                        throw new BusinessException("ApprenticeshipTraining_Validate_ProfileStatus");
                    }
                }
                else
                {
                    throw new BusinessException("ApprenticeshipTraining_Validate_PupilOfClass");

                }
            }
        }
        #endregion

        #region Them moi
        public void InsertApprenticeshipTraining(List<ApprenticeshipTraining> ListApprenticeshipTraining)
        {
            //Kiem tra gia tri truyen vao
            if (ListApprenticeshipTraining == null)
            {
                throw new BusinessException("ApprenticeshipTraining_Err_EmptyList");
            }

            foreach (ApprenticeshipTraining ApprenticeshipTraining in ListApprenticeshipTraining)
            {
                //Kiem tra AcadYearID,PupilID
                AcademicYearBusiness.CheckAvailable(ApprenticeshipTraining.AcademicYearID, "ApprenticeshipTraining_Label_AcadYear");
                PupilProfileBusiness.CheckAvailable(ApprenticeshipTraining.PupilID, "ApprenticeshipTraining_Label_Pupil", true);

                //Kiem tra ClassID
                int ClassID = ApprenticeshipTraining.ClassID.Value;
                ClassProfileBusiness.CheckAvailable(ClassID, "ApprenticeshipTraining_Label_ClassID", true);


                // Kiem tra ProfileStatus ==1
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["PupilID"] = ApprenticeshipTraining.PupilID;
                SearchInfo["ClassID"] = ApprenticeshipTraining.ClassID;
                SearchInfo["AcademicYearID"] = ApprenticeshipTraining.AcademicYearID;
                //Do trong db co nhieu ban ghi co cung (PupilID, ClassID,AcaID) ma chi khac nhau Status nen phai lay ra ban ghi co PupilOfClassID lon nhat la trang thai moi nhat cua hoc sinh do
                List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(ApprenticeshipTraining.SchoolID.Value, SearchInfo).OrderByDescending(o => o.PupilOfClassID).ToList();

                // fix lõi ATTT
                // if (listPupilOfClass != null || listPupilOfClass.Count > 0)
                if (listPupilOfClass != null && listPupilOfClass.Any())
                {
                    if (listPupilOfClass.FirstOrDefault().Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        Utils.ValidateMaxLength(ApprenticeshipTraining.Description, 200, "ApprenticeshipTraining_Label_Description");

                        SchoolProfileBusiness.CheckAvailable(ApprenticeshipTraining.SchoolID, "ApprenticeshipTraining_Label_SchoolID", true);

                        EducationLevel educationLevel = EducationLevelBusiness.Find(ApprenticeshipTraining.EducationLevelID);
                        //kiem tra su ton tai cua educationLevel
                        if (educationLevel == null)
                        {
                            throw new BusinessException("ApprenticeshipTraining_Label_EducationLevel");
                        }
                        //Kiem tra lai xem da ton tai hoc sinh trong lop nghe do chua
                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["PupilID"] = ApprenticeshipTraining.PupilID;
                        dic["ClassID"] = ApprenticeshipTraining.ClassID;
                        dic["AcademicYearID"] = ApprenticeshipTraining.AcademicYearID;
                        dic["EducationLevelID"] = ApprenticeshipTraining.EducationLevelID;
                        dic["ApprenticeshipClassID"] = ApprenticeshipTraining.ApprenticeShipClassID;
                        ApprenticeshipTraining ap = this.SearchBySchool(ApprenticeshipTraining.SchoolID.Value, dic).ToList().FirstOrDefault();
                        //Neu chua ton tai thi them moi
                        if (ap == null)
                        {
                            //Du lieu hop le them moi
                            base.Insert(ApprenticeshipTraining);
                        }
                    }
                    else
                    {
                        throw new BusinessException("ApprenticeshipTraining_Validate_ProfileStatus");
                    }
                }
                else
                {
                    throw new BusinessException("ApprenticeshipTraining_Validate_PupilOfClass");

                }
            }
        }
        #endregion

        #region Update

        public void UpdateApprenticeshipTraining(List<ApprenticeshipTraining> ListApprenticeshipTraining)
        {
            //Kiem tra gia tri truyen vao
            if (ListApprenticeshipTraining == null)
            {
                throw new BusinessException("ApprenticeshipTraining_Err_EmptyList");
            }
            foreach (ApprenticeshipTraining ApprenticeshipTraining in ListApprenticeshipTraining)
            {
                ValidationMetadata.ValidateObject(ApprenticeshipTraining);

                Utils.ValidateRequire(ApprenticeshipTraining.ApprenticeshipTrainingID, "ApprenticeshipTraining_Label_Empty");

                //Kiem tra AcadYearID,PupilID
                AcademicYearBusiness.CheckAvailable(ApprenticeshipTraining.AcademicYearID, "ApprenticeshipTraining_Label_AcadYear", true);
                PupilProfileBusiness.CheckAvailable(ApprenticeshipTraining.PupilID, "ApprenticeshipTraining_Label_Pupil", true);

                //Kiem tra ClassID
                int ClassID = ApprenticeshipTraining.ClassID.Value;
                ClassProfileBusiness.CheckAvailable(ClassID, "ApprenticeshipTraining_Label_ClassID", true);


                // Kiem tra ProfileStatus ==1
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["PupilID"] = ApprenticeshipTraining.PupilID;
                SearchInfo["ClassID"] = ApprenticeshipTraining.ClassID;

                List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(ApprenticeshipTraining.SchoolID.Value, SearchInfo).ToList();

                // fix lỗi ATTT
                // if (listPupilOfClass != null || listPupilOfClass.Count > 0)
                if (listPupilOfClass != null && listPupilOfClass.Any())
                    if (listPupilOfClass.FirstOrDefault().Status == 1)
                    {
                        Utils.ValidateMaxLength(ApprenticeshipTraining.Description, 200, "ApprenticeshipTraining_Label_Description");

                        SchoolProfileBusiness.CheckAvailable(ApprenticeshipTraining.SchoolID, "ApprenticeshipTraining_Label_SchoolID", true);

                        //EducationLevel educationLevel = EducationLevelBusiness.Find(ApprenticeshipTraining.EducationLevelID);
                        ////kiem tra su ton tai cua educationLevel
                        //if (educationLevel == null)
                        //{
                        //    throw new BusinessException("ApprenticeshipTraining_Label_EducationLevel");
                        //}
                        //Du lieu hop le them moi
                        this.Update(ApprenticeshipTraining);
                    }

            }
        }
        #endregion

        #region Xoa du lieu
        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="ApprenticeshipTraining"></param>
        public void DeleteApprenticeshipTraining(int ApprenticeshipTrainingID)
        {
            // Kiem tra da ton tai trong CSDL
            this.CheckAvailable(ApprenticeshipTrainingID, "ApprenticeshipTraining_Label_Empty", true);
            // Kiem tra da ton tai trong CSDL
            CheckAvailable(ApprenticeshipTrainingID, "ApprenticeshipTraining_Label_Empty", true);
            // Khong co loi thi thuc hien xoa toan bo du lieu trong danh sach nay
            base.Delete(ApprenticeshipTrainingID);
        }
        #endregion

        #region Tìm kiếm
        private IQueryable<ApprenticeshipTraining> Search(IDictionary<string, object> dic)
        {
            int ApprenticeshipTrainingID = Utils.GetInt(dic, "ApprenticeshipTrainingID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Result = Utils.GetInt(dic, "Result");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            string Description = Utils.GetString(dic, "Description");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            int ApprenticeshipClassID = Utils.GetInt(dic, "ApprenticeshipClassID");


            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"EducationLevelID",EducationLevelID},
                {"SchoolID",SchoolID},
                {"PupilID",PupilID},
                {"Check", "Check"}
            });

            //lsApprenticeshipTraining = this.ApprenticeshipTrainingRepository.All;
            IQueryable<ApprenticeshipTraining> lsApprenticeshipTraining = from p in ApprenticeshipTrainingRepository.All
                                                                          join s in listPupilOfClass on new { p.PupilID } equals new { s.PupilID }
                                                                          //where s.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || s.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || s.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                                                          select p;

            if (ClassID != 0)
            {
                lsApprenticeshipTraining = from a in lsApprenticeshipTraining
                                           join cp in ClassProfileBusiness.All on a.ClassID equals cp.ClassProfileID
                                           where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                           && a.ClassID == ClassID
                                           select a;
            }
            if (ApprenticeshipTrainingID != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.ApprenticeshipTrainingID == ApprenticeshipTrainingID);
            }

            if (SchoolID != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.SchoolID == SchoolID);
            }

            if (AcademicYearID != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (PupilID != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.PupilID == PupilID);
            }

            if (EducationLevelID != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.EducationLevelID == EducationLevelID);
            }

            if (Result != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.Result == Result);
            }

            if (Description.Trim().Length != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.Description.ToLower().Contains(Description));
            }

            if (CreatedDate != null)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.CreatedDate == CreatedDate);
            }

            if (ModifiedDate != null)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.ModifiedDate == ModifiedDate);
            }

            if (PupilCode.Trim().Length != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.PupilCode.ToLower().Contains(PupilCode));
            }

            if (FullName.Trim().Length != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName));
            }

            if (ApprenticeshipClassID != 0)
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.ApprenticeShipClassID == ApprenticeshipClassID);
            }

            if (!string.IsNullOrEmpty(PupilCode))
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.PupilCode.ToLower().Contains(PupilCode.Trim().ToLower()));
            }

            if (!string.IsNullOrEmpty(FullName))
            {
                lsApprenticeshipTraining = lsApprenticeshipTraining.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.Trim().ToLower()));
            }

            return (lsApprenticeshipTraining);
        }
        #endregion

        /// <summary>
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<ApprenticeshipTraining> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }

        /// <summary>
        /// Tamhm1
        /// 11/12/2012
        /// In phiếu báo điểm môn học nghề
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<ApprenticeshipTrainingBO> GetListPupilApprenticeshipTraining(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            //int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int apprenticeshipClassID = Utils.GetInt(dic, "ApprenticeshipClassID");
            string pupilCode = Utils.GetString(dic, "PupilCode");
            string fullName = Utils.GetString(dic, "FullName");
            //if (apprenticeshipClassID == 0) return null;
            IQueryable<ApprenticeshipTraining> lst = this.SearchBySchool(schoolID, dic);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = academicYearID;
            SearchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            SearchInfo["SchoolID"] = schoolID;
            IQueryable<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(schoolID, SearchInfo);
            var _list = from p in lst
                        join app in ApprenticeshipSubjectBusiness.All on p.ApprenticeshipSubjectID equals app.ApprenticeshipSubjectID
                        join s in lstSummedUpRecord on new { p.PupilID, app.SubjectID } equals new { s.PupilID, s.SubjectID } into g
                        from jn in g.DefaultIfEmpty()
                        select new { p, jn };
            var list = _list.ToList();
            //var list = (from p in lst
            //            join s in lstSummedUpRecord on new { p.PupilID, p.ApprenticeshipSubject.SubjectID } equals new { s.PupilID, s.SubjectID } into g
            //            from jn in g.DefaultIfEmpty()
            //            select new { p, jn }).ToList();
            list = list.OrderBy(o => o.p.PupilProfile.FullName).ToList();
            List<ApprenticeshipTrainingBO> lstApprenticeshipTrainingBO = new List<ApprenticeshipTrainingBO>();
            if (list != null)
            {
                foreach (var q in list)
                {
                    ApprenticeshipTrainingBO bo = new ApprenticeshipTrainingBO();
                    bo.ClassName = q.p.ClassProfile.DisplayName;
                    bo.ApprenticeshipClassID = q.p.ApprenticeShipClassID;
                    bo.ApprenticeshipClassName = q.p.ApprenticeshipClass.ClassName;
                    bo.PupilID = q.p.PupilID;
                    bo.PupilCode = q.p.PupilProfile.PupilCode;
                    bo.FullName = q.p.PupilProfile.FullName;
                    bo.BirthDate = q.p.PupilProfile.BirthDate;
                    bo.Genre = q.p.PupilProfile.Genre == 1 ? "Nam" : "Nữ";
                    bo.BirthPlace = q.p.PupilProfile.BirthPlace;
                    bo.ApprenticeshipSubjectName = q.p.ApprenticeshipSubject.SubjectName;

                    if (q.jn != null)
                    {
                        bo.SummedUpMark = q.jn.SummedUpMark;
                        bo.Category = ConvertSummedUpRecord(q.jn.SummedUpMark);
                    }
                    else
                    {

                        bo.Category = string.Empty;
                    }
                    lstApprenticeshipTrainingBO.Add(bo);
                }
            }

            return lstApprenticeshipTrainingBO;
        }

        /// <summary>
        /// Author: Tamhm1
        /// Date: 11/12/2012
        /// Chuyển từ int => string 
        /// </summary>
        /// <param name="mark"></param>
        /// <returns></returns>
        public string ConvertSummedUpRecord(decimal? mark)
        {
            if (mark == null) return "";
            else
            {
                if ((double)mark >= 8) return SystemParamsInFile.ApprenticeshipTraining_Type_Excellent;
                else if ((double)mark >= 6.5) return SystemParamsInFile.ApprenticeshipTraining_Type_Good;
                else if ((double)mark >= 5) return SystemParamsInFile.ApprenticeshipTraining_Type_Normal;
                else if ((double)mark > 3.5) return SystemParamsInFile.ApprenticeshipTraining_Type_Weak;
                else return SystemParamsInFile.ApprenticeshipTraining_Type_Poor;
            }
        }

        public Stream CreateReportApprenticeshipTraining(int schoolID, int academicYearID, int? apprenticeshipClassID, string pupilCode, string fullName, List<ApprenticeshipTrainingBO> lstPupilID)
        {
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string reportCode = SystemParamsInFile.REPORT_PHIEU_HOC_NGHE;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 68;

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //Tạo template chuẩn
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "Q" + (firstRow));

            //tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName.ToUpper();
            //string apprenticeshipClassName = ApprenticeshipClassRepository.Find(apprenticeshipClassID).ClassName.ToUpper();
            string academicTitle = "THỜI GIAN KHOÁ HỌC TỪ " + academicYear.FirstSemesterStartDate.Value.Month + "/" + academicYear.FirstSemesterStartDate.Value.Year + " ĐẾN " + academicYear.SecondSemesterEndDate.Value.Month + "/" + academicYear.SecondSemesterEndDate.Value.Year;

            //string academicYearTitle = academicYear.DisplayTitle;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    //{"ApprenticeshipClassName", apprenticeshipClassName},                 
                    {"ProvinceName", provinceName}          
                  
                };
            List<int> listPupilID = lstPupilID.Select(o => o.PupilID).ToList();

            //Lấy danh sách thông tin điểm môn tính điểm
            Dictionary<string, object> dicMarkRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", academicYearID}, 
                {"SchoolID", schoolID},
                {"ListPupilID", listPupilID},
                {"Year", academicYear.Year}
            };
            List<VMarkRecord> lstMarkRecord = VMarkRecordBusiness.SearchBySchool(schoolID, dicMarkRecord).ToList();

            //Lấy danh sách thông tin điểm tổng kết
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", academicYearID}, 
                {"SchoolID", schoolID},
                {"ListPupilID", listPupilID}
            };
            //Dữ liệu điểm tổng kết
            List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(schoolID, dicSummedUpRecord).ToList();
            //Dữ liệu điểm danh môn nghề
            List<ApprenticeshipTrainingAbsence> lstPupilAbsence = ApprenticeshipTrainingAbsenceBusiness.SearchBySchool(schoolID, dicSummedUpRecord).ToList();
            //Khai báo các biến để lưu trữ dữ liệu


            //Khai báo các ô cần fill dữ liệu vào:
            //Dữ liệu học kỳ I
            VTVector cell_KTTX_HKI = new VTVector("D41");
            VTVector cell_KTDK_HKI = new VTVector("D43");
            VTVector cell_KTHK_HKI = new VTVector("D45");
            VTVector cell_TBHK_HKI = new VTVector("D47");
            VTVector cell_AbsenceP_HKI = new VTVector("D50");
            VTVector cell_AbsenceKP_HKI = new VTVector("D51");
            //Ô dữ liệu HKII
            VTVector cell_KTTX_HKII = new VTVector("J41");
            VTVector cell_KTDK_HKII = new VTVector("J43");
            VTVector cell_KTHK_HKII = new VTVector("J45");
            VTVector cell_TBHK_HKII = new VTVector("J47");
            VTVector cell_AbsenceP_HKII = new VTVector("M50");
            VTVector cell_AbsenceKP_HKII = new VTVector("M51");
            //Các thông tin chung
            VTVector cell_ProvinceName = new VTVector("J3");
            VTVector cell_SchoolName = new VTVector("L6");
            VTVector cell_PupilName = new VTVector("N29");
            VTVector cell_ClassName = new VTVector("L30");
            VTVector cell_ApprenticeshipClassName = new VTVector("O31");
            VTVector cell_FromDate_ToDate = new VTVector("K32");
            VTVector cell_PupilName_Down = new VTVector("D35");
            VTVector cell_BirthDate = new VTVector("C36");
            VTVector cell_BirthPlace = new VTVector("E36");
            VTVector cell_SchoolNameDown = new VTVector("D37");
            VTVector cell_ClassName_Down = new VTVector("H37");
            VTVector cell_ApprenticeshipClassUnit = new VTVector("M36");
            VTVector cell_ClassApp = new VTVector("M37");
            VTVector cell_HeadTeacher = new VTVector("F68");

            //Ô dữ liệu cả năm
            VTVector cell_MarkTK_CN = new VTVector("C56");
            int count = lstPupilID.Count();

            IVTRange template = firstSheet.GetRange("A2", "Q" + firstRow);
            for (int i = 1; i < count; i++)
            {
                int nextFirstRow = 68 * i + 2;
                sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                sheet.SetBreakPage(nextFirstRow);
            }

            Dictionary<int, Employee> dicTeacher = new Dictionary<int, Employee>();
            Dictionary<int, ApprenticeshipClass> dicApprenticeshipClass = new Dictionary<int, ApprenticeshipClass>();
            for (int i = 0; i < count; i++)
            {

                //Khai báo biến
                string markTKHKI = "";
                string markDKHKI = "";
                string markHKHKI = "";
                string markTBHKHKI = "";
                string markTKHKII = "";
                string markDKHKII = "";
                string markHKHKII = "";
                string markTBHKHKII = "";
                int totalAbsentDayWithPermissionHKI;
                int totalAbsentDayWithoutPermissionHKI;
                int totalAbsentDayWithPermissionHKII;
                int totalAbsentDayWithoutPermissionHKII;
                double markTBCN = 0;
                string markTBCNS = "";
                //Dữ liệu chung
                PupilProfile pupilProfile = PupilProfileRepository.Find(lstPupilID[i].PupilID);
                string pupilName = pupilProfile.FullName;
                string birthDate = pupilProfile.BirthDate.ToString("dd/MM/yyyy");
                string birthPlace = pupilProfile.BirthPlace;
                string className = pupilProfile.ClassProfile.DisplayName;
                int AppID = lstPupilID[i].ApprenticeshipClassID.Value;
                ApprenticeshipClass ApprenticeshipClass = null;
                if (dicApprenticeshipClass.ContainsKey(AppID))
                {
                    ApprenticeshipClass = dicApprenticeshipClass[AppID];
                }
                else
                {
                    ApprenticeshipClass = ApprenticeshipClassRepository.Find(AppID);
                    dicApprenticeshipClass[AppID] = ApprenticeshipClass;
                }
                int SubjectID = ApprenticeshipClass.ApprenticeshipSubject.SubjectID;
                IEnumerable<VMarkRecord> lstMarkPupilSubject = from s in lstMarkRecord
                                                              where s.PupilID == lstPupilID[i].PupilID
                                                              && s.SubjectID == SubjectID
                                                              select s;
                IEnumerable<SummedUpRecord> lstSumPupilSubject = from s in lstSummedUpRecord
                                                                 where s.PupilID == lstPupilID[i].PupilID
                                                                 && s.SubjectID == SubjectID
                                                                 select s;
                //Lấy dữ liệu điểm miệng
                List<VMarkRecord> lstMHKI = (from s in lstMarkPupilSubject
                                            where s.Title == SystemParamsInFile.MARK_TYPE_M
                                            && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                            select s).OrderBy(x => x.OrderNumber).ToList();
                List<VMarkRecord> lstMHKII = (from s in lstMarkPupilSubject
                                             where s.Title == SystemParamsInFile.MARK_TYPE_M
                                             && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                             select s).OrderBy(x => x.OrderNumber).ToList();
                //Lấy dữ liệu điểm 15 phút
                List<VMarkRecord> lstPHKI = (from s in lstMarkPupilSubject
                                            where s.Title == SystemParamsInFile.MARK_TYPE_P
                                            && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                            select s).OrderBy(x => x.OrderNumber).ToList();
                List<VMarkRecord> lstPHKII = (from s in lstMarkPupilSubject
                                             where s.Title == SystemParamsInFile.MARK_TYPE_P
                                             && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                             select s).OrderBy(x => x.OrderNumber).ToList();
                //Lấy dữ liệu điểm định kỳ
                List<VMarkRecord> lstVHKI = (from s in lstMarkPupilSubject
                                            where s.Title == SystemParamsInFile.MARK_TYPE_V
                                            && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                            select s).OrderBy(x => x.OrderNumber).ToList();
                List<VMarkRecord> lstVHKII = (from s in lstMarkPupilSubject
                                             where s.Title == SystemParamsInFile.MARK_TYPE_V
                                             && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                             select s).OrderBy(x => x.OrderNumber).ToList();
                //Lấy dữ liệu điểm học kỳ
                List<VMarkRecord> lstHKI = (from s in lstMarkPupilSubject
                                           where s.Title == SystemParamsInFile.MARK_TYPE_HK
                                           && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                           select s).OrderBy(x => x.OrderNumber).ToList();
                List<VMarkRecord> lstHKII = (from s in lstMarkPupilSubject
                                            where s.Title == SystemParamsInFile.MARK_TYPE_HK
                                            && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                            select s).OrderBy(x => x.OrderNumber).ToList();
                //Lấy dữ liệu điểm tổng kết
                List<SummedUpRecord> lSRHKI = (from s in lstSumPupilSubject
                                               where s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                               select s).ToList();
                List<SummedUpRecord> lSRHKII = (from s in lstSumPupilSubject
                                                where s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                select s).ToList();
                List<SummedUpRecord> lSRHKCN = (from s in lstSumPupilSubject
                                                where s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                select s).ToList();

                //Có phép học kỳ II
                int absenceHasReasonHKII = (from s in lstPupilAbsence
                                            where s.IsAccepted == true
                                            && s.PupilID == lstPupilID[i].PupilID
                                            && s.AbsentDate >= academicYear.SecondSemesterStartDate
                                            select s.IsAccepted).Count();
                //có phép học kỳ I
                int absenceHasReasonHKI = (from s in lstPupilAbsence
                                           where s.IsAccepted == true
                                           && s.PupilID == lstPupilID[i].PupilID
                                           && s.AbsentDate <= academicYear.FirstSemesterEndDate
                                           select s.IsAccepted).Count();
                //Không phép học kỳ I
                int absenceNoReasonHKI = (from s in lstPupilAbsence
                                          where s.IsAccepted == false
                                          && s.PupilID == lstPupilID[i].PupilID
                                          && s.AbsentDate <= academicYear.FirstSemesterEndDate
                                          select s.IsAccepted).Count();
                //Không phép học kỳ II
                int absenceNoReasonHKII = (from s in lstPupilAbsence
                                           where s.IsAccepted == false
                                           && s.PupilID == lstPupilID[i].PupilID
                                           && s.AbsentDate >= academicYear.SecondSemesterStartDate
                                           select s.IsAccepted).Count();
                //Tổng số buổi vắng có phép cả năm
                int absenceAllHKI = (from s in lstPupilAbsence
                                     where s.IsAccepted == true
                                     && s.PupilID == lstPupilID[i].PupilID
                                     select s.IsAccepted).Count();
                //tổng số buổi vắng không phép cả năm
                int absenceAllHKII = (from s in lstPupilAbsence
                                      where s.IsAccepted == false
                                      && s.PupilID == lstPupilID[i].PupilID
                                      select s.IsAccepted).Count();
                //Điểm kiểm tra thường xuyên
                foreach (VMarkRecord mr in lstMHKI)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markTKHKI += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markTKHKI += mr.Mark + " ";
                }
                markTKHKI = markTKHKI.Replace(",", ".");
                foreach (VMarkRecord mr in lstPHKI)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markTKHKI += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markTKHKI += mr.Mark + " ";
                }
                markTKHKII = markTKHKII.Replace(",", ".");
                foreach (VMarkRecord mr in lstMHKII)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markTKHKII += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markTKHKII += mr.Mark + " ";
                }
                markTKHKII = markTKHKII.Replace(",", ".");
                foreach (VMarkRecord mr in lstPHKII)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markTKHKII += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markTKHKII += mr.Mark + " ";
                }
                markTKHKII = markTKHKII.Replace(",", ".");
                //Điểm kiếm tra định kỳ
                foreach (VMarkRecord mr in lstVHKI)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markDKHKI += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markDKHKI += mr.Mark + " ";
                }
                markDKHKI = markDKHKI.Replace(",", ".");
                foreach (VMarkRecord mr in lstVHKII)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markDKHKII += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markDKHKII += mr.Mark + " ";
                }
                markDKHKII = markDKHKII.Replace(",", ".");
                //Điểm kiểm tra học kỳ
                //HKI
                foreach (VMarkRecord mr in lstHKI)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markHKHKI += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markHKHKI += mr.Mark + " ";
                }
                markHKHKI = markHKHKI.Replace(",", ".");
                //HKII
                foreach (VMarkRecord mr in lstHKII)
                {
                    if (Convert.ToString(mr.Mark).Substring(Convert.ToString(mr.Mark).Length - 2, 2) == "00")
                        markHKHKII += Utils.FormatMark(mr.Mark) + " ";
                    else
                        markHKHKII += mr.Mark + " ";
                }
                markHKHKII = markHKHKII.Replace(",", ".");
                //Điểm trung bình học kỳ
                //HKI
                foreach (SummedUpRecord sur in lSRHKI)
                {
                    if (Convert.ToString(sur.SummedUpMark).Substring(Convert.ToString(sur.SummedUpMark).Length - 2, 2) == "00")
                        markTBHKHKI += Utils.FormatMark(sur.SummedUpMark.Value) + " ";
                    else
                        markTBHKHKI += sur.SummedUpMark + " ";

                }
                markTBHKHKI = markTBHKHKI.Replace(",", ".");
                //HKII
                foreach (SummedUpRecord sur in lSRHKII)
                {
                    if (Convert.ToString(sur.SummedUpMark).Substring(Convert.ToString(sur.SummedUpMark).Length - 2, 2) == "00")
                        markTBHKHKII += Utils.FormatMark(sur.SummedUpMark.Value) + " ";
                    else
                        markTBHKHKII += sur.SummedUpMark + " ";
                }
                markTBHKHKII = markTBHKHKII.Replace(",", ".");
                //Cả năm
                //foreach (SummedUpRecord sur in lSRHKCN)
                //{
                //    markTBCN += sur.SummedUpMark + " ";
                //}
                if (lSRHKCN != null && lSRHKCN.Count() > 0)
                {
                    markTBCN = (double)lSRHKCN.FirstOrDefault().SummedUpMark;
                }
                else
                {
                    markTBCNS = "";
                }

                //Điểm danh
                //HKI
                totalAbsentDayWithPermissionHKI = absenceHasReasonHKI;
                totalAbsentDayWithoutPermissionHKI = absenceNoReasonHKI;
                totalAbsentDayWithPermissionHKII = absenceHasReasonHKII;
                totalAbsentDayWithoutPermissionHKII = absenceNoReasonHKII;

                //Fill dữ liệu vào file excel

                VTVector vtIndex = new VTVector(68 * i, 0);
                //Các thông tin chung
                sheet.SetCellValue(cell_ProvinceName + vtIndex, provinceName);
                sheet.SetCellValue(cell_SchoolName + vtIndex, schoolName);
                sheet.SetCellValue(cell_PupilName + vtIndex, pupilName);
                sheet.SetCellValue(cell_ClassName + vtIndex, className);
                sheet.SetCellValue(cell_ApprenticeshipClassName + vtIndex, lstPupilID[i].ApprenticeshipClassName);
                sheet.SetCellValue(cell_FromDate_ToDate + vtIndex, academicTitle.ToUpper());
                sheet.SetCellValue(cell_PupilName_Down + vtIndex, pupilName);
                sheet.SetCellValue(cell_BirthDate + vtIndex, birthDate);
                sheet.SetCellValue(cell_BirthPlace + vtIndex, birthPlace);
                sheet.SetCellValue(cell_SchoolNameDown + vtIndex, schoolName);
                sheet.SetCellValue(cell_ClassName_Down + vtIndex, className);
                sheet.SetCellValue(cell_ApprenticeshipClassUnit + vtIndex, schoolName);
                sheet.SetCellValue(cell_ClassApp + vtIndex, lstPupilID[i].ApprenticeshipClassName);
                //Dữ liệu điểm
                sheet.SetCellValue(cell_KTTX_HKI + vtIndex, markTKHKI);
                sheet.SetCellValue(cell_KTTX_HKII + vtIndex, markTKHKII);
                sheet.SetCellValue(cell_KTDK_HKI + vtIndex, markDKHKI);
                sheet.SetCellValue(cell_KTDK_HKII + vtIndex, markDKHKII);
                sheet.SetCellValue(cell_KTHK_HKI + vtIndex, markHKHKI);
                sheet.SetCellValue(cell_KTHK_HKII + vtIndex, markHKHKII);
                sheet.SetCellValue(cell_TBHK_HKI + vtIndex, markTBHKHKI);
                sheet.SetCellValue(cell_TBHK_HKII + vtIndex, markTBHKHKII);
                sheet.SetCellValue(cell_AbsenceP_HKI + vtIndex, totalAbsentDayWithPermissionHKI);
                sheet.SetCellValue(cell_AbsenceKP_HKI + vtIndex, totalAbsentDayWithoutPermissionHKI);
                sheet.SetCellValue(cell_AbsenceP_HKII + vtIndex, totalAbsentDayWithPermissionHKII);
                sheet.SetCellValue(cell_AbsenceKP_HKII + vtIndex, totalAbsentDayWithoutPermissionHKII);
                if (lSRHKCN != null && lSRHKCN.Count() > 0)
                {
                    sheet.SetCellValue(cell_MarkTK_CN + vtIndex, markTBCN);
                }
                else
                {
                    sheet.SetCellValue(cell_MarkTK_CN + vtIndex, markTBCNS);
                }
                Employee teacher = null;
                if (dicTeacher.ContainsKey(ApprenticeshipClass.HeadTeacherID))
                {
                    teacher = dicTeacher[ApprenticeshipClass.HeadTeacherID];
                }
                else
                {
                    teacher = EmployeeRepository.Find(ApprenticeshipClass.HeadTeacherID);
                    dicTeacher[ApprenticeshipClass.HeadTeacherID] = teacher;
                }
                sheet.SetCellValue(cell_HeadTeacher + vtIndex, teacher.FullName);
            }

            firstSheet.Delete();
            sheet.Name = "PhieuHocNghe";
            return oBook.ToStream();
        }

        /// <summary>
        /// Tamhm1
        /// 23/04/2013
        /// In sổ gọi tên và ghi điểm nghề
        /// </summary>
        /// <param name="schoolID">Trường</param>
        /// <param name="academicYeearID">Năm học</param>
        /// <param name="apprenticeshipClassID">lớp nghề</param>
        /// <param name="semester">Học kỳ</param>
        /// <returns></returns>
        public Stream CreateReportApprenticeshipMark(int schoolID, int academicYearID, int apprenticeshipClassID, int semester)
        {
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string reportCode = SystemParamsInFile.REPORT_SO_GOI_TEN_VA_GHI_DIEM_NGHE;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            #region Tạo Sheet GVBM
            //Fill dữ liệu chung cho template
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            District district = school.District;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName.ToUpper();
            string districtName = district.DistrictName.ToUpper();
            //Lấy sheet template
            IVTWorksheet sheet = oBook.GetSheet(4);
            //Tạo template chuẩn
            //Đặt tên các cell
            VTVector cell_Order = new VTVector("A3");
            VTVector cell_Class = new VTVector("B3");
            VTVector cell_Teacher = new VTVector("C3");
            //STT
            int Order = 1;
            //Lớp nghề
            ApprenticeshipClass ApprenticeshipClass = ApprenticeshipClassBusiness.Find(apprenticeshipClassID);

            //Giáo viên
            Employee teacher = null;
            teacher = EmployeeRepository.Find(ApprenticeshipClass.HeadTeacherID);
            // File dữ liệu
            sheet.SetCellValue(cell_Order, Order);
            sheet.SetCellValue(cell_Class, ApprenticeshipClass.ClassName);
            sheet.SetCellValue(cell_Teacher, teacher.FullName);
            #endregion

            #region Sheet INFO
            IVTWorksheet sheetINFO = oBook.GetSheet(3);

            //Đặt tên các cell
            VTVector cell_School_Name = new VTVector("B3");
            VTVector cell_Province_Name = new VTVector("B5");
            VTVector cell_Class_Name = new VTVector("B7");
            VTVector cell_Faculty_Name = new VTVector("B9");
            VTVector cell_AcademicYear = new VTVector("B11");
            VTVector cell_HeadTeacher = new VTVector("B13");
            VTVector cell_From_Date = new VTVector("D11");
            VTVector cell_End_Date = new VTVector("F11");
            VTVector cell_Day_Report = new VTVector("C25");
            VTVector cell_Month_Report = new VTVector("D25");
            VTVector cell_Year_Report = new VTVector("E25");
            VTVector cell_Sum_Pupil = new VTVector("B15");
            sheetINFO.SetCellValue(cell_School_Name, schoolName);
            sheetINFO.SetCellValue(cell_Province_Name, districtName);
            sheetINFO.SetCellValue(cell_Class_Name, ApprenticeshipClass.ClassName);
            sheetINFO.SetCellValue(cell_Faculty_Name, ApprenticeshipClass.ApprenticeshipSubject.SubjectCat.DisplayName);
            string title_AcademicYear = academicYear.Year + " - " + (academicYear.Year + 1);
            sheetINFO.SetCellValue(cell_AcademicYear, title_AcademicYear);
            sheetINFO.SetCellValue(cell_HeadTeacher, teacher.FullName);
            //Từ ngày
            string title_FromDate = academicYear.FirstSemesterStartDate.Value.Month + "/" + academicYear.FirstSemesterStartDate.Value.Year;
            string title_EndDate = academicYear.SecondSemesterEndDate.Value.Month + "/" + academicYear.SecondSemesterEndDate.Value.Year;
            sheetINFO.SetCellValue(cell_From_Date, title_FromDate);
            sheetINFO.SetCellValue(cell_End_Date, title_EndDate);
            sheetINFO.SetCellValue(cell_Day_Report, DateTime.Now.Day);
            sheetINFO.SetCellValue(cell_Month_Report, DateTime.Now.Month);
            sheetINFO.SetCellValue(cell_Year_Report, DateTime.Now.Year);
            // Lấy tổng số học sinh vào đầu khoá học:
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ApprenticeshipClassID"] = apprenticeshipClassID;
            SearchInfo["AcademicYearID"] = academicYearID;
            SearchInfo["CreatedAcademicYear"] = academicYear.Year; // AnhVD 20131217
            SearchInfo["Year"] = academicYear.Year; // AnhVD 20131217
            IQueryable<ApprenticeshipTraining> lstPupil = ApprenticeshipTrainingBusiness.GetListPupilNoStatus(schoolID, academicYearID, apprenticeshipClassID);
            if (lstPupil != null && lstPupil.Count() > 0)
            {
                sheetINFO.SetCellValue(cell_Sum_Pupil, lstPupil.Count());
            }
            else
            {
                sheetINFO.SetCellValue(cell_Sum_Pupil, 0);
            }
            #endregion

            #region Sheet Bia ngoai
            IVTWorksheet sheetBiaNgoai = oBook.GetSheet(1);

            #endregion
            #region SHEET ALL
            IVTWorksheet sheetAll = oBook.GetSheet(2);
            //Lấy ra danh sách các ngày, tháng
            List<ApprenticeshipTrainingScheBO> lstDayHKI = new List<ApprenticeshipTrainingScheBO>();
            List<ApprenticeshipTrainingScheBO> lstDayAbsenceHKI = new List<ApprenticeshipTrainingScheBO>();
            List<int> lstMonthHKI = new List<int>();
            List<ApprenticeshipTrainingScheBO> lstDayHKII = new List<ApprenticeshipTrainingScheBO>();
            List<ApprenticeshipTrainingScheBO> lstDayAbsenceHKII = new List<ApprenticeshipTrainingScheBO>();
            List<int> lstMonthHKII = new List<int>();
            int orderday1 = 0;
            int orderday2 = 0;
            int firstColumnAbsence1 = ColumnNumber("AC");
            int endColumnAbsence1 = ColumnNumber("AU");
            int range1 = endColumnAbsence1 - firstColumnAbsence1;
            int firstColumnAbsence2 = ColumnNumber("BB");
            int endColumnAbsence2 = ColumnNumber("BT");
            int range2 = endColumnAbsence2 - firstColumnAbsence2;
            // Vị trí ngày học của học kỳ 1
            int orderHKI = 0;
            // Vị trí ngày học của học kỳ 2
            int orderHKII = 0;
            // vị trí dòng ngày
            int startRowDay = 4;
            // vị trí dòng tháng
            int startRowMonth = 5;
            // 4-So cot diem thuong xuyen trong file template
            int numberOfMarkTX = 4;
            // 2 - Số cột điểm định kỳ trong file template
            int numberOfMarkDK = 2;
            // vị trí dòng bắt đầu điền dữ liệu
            int startRowFill = 6;
            IDictionary<string, object> dicApprenticeshipTrainingSchedule = new Dictionary<string, object>();
            dicApprenticeshipTrainingSchedule["AcademicYearID"] = academicYearID;
            //dicApprenticeshipTrainingSchedule["Semester"] = semester;
            dicApprenticeshipTrainingSchedule["ApprenticeshipClassID"] = apprenticeshipClassID;
            List<ApprenticeshipTrainingSchedule> lstApprenticeshipTrainingSchedule = ApprenticeshipTrainingScheduleBusiness.SearchBySchool(schoolID, dicApprenticeshipTrainingSchedule).ToList();
            // Lay cac thang
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstMonthHKI = lstApprenticeshipTrainingSchedule.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).OrderBy(u => u.Month).Select(o => o.Month).ToList();
                foreach (int i in lstMonthHKI)
                {
                    string s = lstApprenticeshipTrainingSchedule.Where(o => o.Month == i && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault().ScheduleDetail;
                    for (int j = 0; j < s.Length; j++)
                    {
                        ApprenticeshipTrainingScheBO apprenticeshipTrainingBO = new ApprenticeshipTrainingScheBO();
                        if (s[j] == 'C')
                        {
                            orderHKI++;
                            apprenticeshipTrainingBO.day = j + 1;
                            apprenticeshipTrainingBO.month = i;
                            apprenticeshipTrainingBO.order = orderHKI;
                            lstDayHKI.Add(apprenticeshipTrainingBO);
                        }
                    }
                }
                // Fill du lieu
                foreach (ApprenticeshipTrainingScheBO apBO in lstDayHKI)
                {
                    if (orderday1 <= range1)
                    {
                        sheetAll.SetCellValue(startRowDay, firstColumnAbsence1 + orderday1, apBO.day);
                        sheetAll.SetCellValue(startRowMonth, firstColumnAbsence1 + orderday1, apBO.month);
                        orderday1++;
                    }
                }
            }
            else
            {
                lstMonthHKI = lstApprenticeshipTrainingSchedule.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).OrderBy(u => u.Month).Select(o => o.Month).ToList();
                lstMonthHKII = lstApprenticeshipTrainingSchedule.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).OrderBy(u => u.Month).Select(o => o.Month).ToList();
                // Ky 1
                foreach (int i in lstMonthHKI)
                {
                    string s = lstApprenticeshipTrainingSchedule.Where(o => o.Month == i && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault().ScheduleDetail;
                    for (int j = 0; j < s.Length; j++)
                    {
                        ApprenticeshipTrainingScheBO apprenticeshipTrainingBO = new ApprenticeshipTrainingScheBO();
                        if (s[j] == 'C')
                        {
                            orderHKI++;
                            apprenticeshipTrainingBO.day = j + 1;
                            apprenticeshipTrainingBO.month = i;
                            apprenticeshipTrainingBO.order = orderHKI;
                            lstDayHKI.Add(apprenticeshipTrainingBO);
                        }
                    }
                }
                foreach (ApprenticeshipTrainingScheBO apBO in lstDayHKI)
                {
                    if (orderday1 <= range1)
                    {
                        sheetAll.SetCellValue(startRowDay, firstColumnAbsence1 + orderday1, apBO.day);
                        sheetAll.SetCellValue(startRowMonth, firstColumnAbsence1 + orderday1, apBO.month);
                        orderday1++;
                    }
                }
                // Ky 2
                foreach (int i in lstMonthHKII)
                {
                    string s = lstApprenticeshipTrainingSchedule.Where(o => o.Month == i && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault().ScheduleDetail;
                    for (int j = 0; j < s.Length; j++)
                    {
                        ApprenticeshipTrainingScheBO apprenticeshipTrainingBO = new ApprenticeshipTrainingScheBO();
                        if (s[j] == 'C')
                        {
                            orderHKII++;
                            apprenticeshipTrainingBO.day = j + 1;
                            apprenticeshipTrainingBO.month = i;
                            apprenticeshipTrainingBO.order = orderHKII;
                            lstDayHKII.Add(apprenticeshipTrainingBO);
                        }
                    }
                }
                foreach (ApprenticeshipTrainingScheBO apBO in lstDayHKII)
                {
                    if (orderday2 <= range2)
                    {
                        sheetAll.SetCellValue(startRowDay, firstColumnAbsence2 + orderday2, apBO.day);
                        sheetAll.SetCellValue(startRowMonth, firstColumnAbsence2 + orderday2, apBO.month);
                        orderday2++;
                    }
                }
            }
            // Lay thong khai bao con diem
            IDictionary<string, object> dicSemesterDeclaration = new Dictionary<string, object>();
            dicSemesterDeclaration["SchoolID"] = schoolID;
            dicSemesterDeclaration["AcademicYearID"] = academicYearID;
            dicSemesterDeclaration["Semester"] = semester;
            SemeterDeclaration semesterDeclaration = SemeterDeclarationBusiness.Search(dicSemesterDeclaration).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            // Lay danh sach hoc sinh hoc nghe
            IQueryable<ApprenticeshipTraining> _lstFirstApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(schoolID, SearchInfo);
            // Lay hoc ky hoc cua lop nghe
            int semesterLearn = ApprenticeshipClass.Semester.Value;
            // Lay thong tin diem danh
            IDictionary<string, object> SearchPupilAbsen = new Dictionary<string, object>();
            SearchPupilAbsen["ApprenticeshipClassID"] = apprenticeshipClassID;
            SearchPupilAbsen["AcademicYearID"] = academicYearID;
            IQueryable<ApprenticeshipTrainingAbsence> _lstApprenticeshipTrainingAbsence = ApprenticeshipTrainingAbsenceBusiness.SearchBySchool(schoolID, SearchPupilAbsen);
            // Lay thong tin mon hoc
            if (ApprenticeshipClass != null && ApprenticeshipClass.ApprenticeshipSubject != null)
            {
                int SubjectID = ApprenticeshipClass.ApprenticeshipSubject.SubjectID;
                SearchInfo["SubjectID"] = SubjectID;
            }
            // Lay diem thanh phan: Tim kiem trong MarkRecord
            List<MarkRecord> _ListMarkRecord = new List<MarkRecord>();
            List<MarkRecord> _ListMarkRecord1 = new List<MarkRecord>();
            List<MarkRecord> _lstMarkRecordTX = new List<MarkRecord>();
            List<MarkRecord> _lstMarkRecordDK = new List<MarkRecord>();
            List<MarkRecord> _lstMarkRecordTX1 = new List<MarkRecord>();
            List<MarkRecord> _lstMarkRecordDK1 = new List<MarkRecord>();
            List<SummedUpRecord> _ListSummedUpRecord = new List<SummedUpRecord>();
            List<SummedUpRecord> _ListSummedUpRecord1 = new List<SummedUpRecord>();
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SearchInfo["Semester"] = semester;
                // diem thanh phan
                _ListMarkRecord = MarkRecordBusiness.SearchBySchool(schoolID, SearchInfo).ToList();
                // diem TBM
                _ListSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(schoolID, SearchInfo).ToList();
            }
            else
            {
                // diem thanh phan ky 1
                _ListMarkRecord1 = MarkRecordBusiness.SearchBySchool(schoolID, SearchInfo).Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                // diem TBM ky 1
                _ListSummedUpRecord1 = SummedUpRecordBusiness.SearchBySchool(schoolID, SearchInfo).Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
                // diem thuong xuyen ky 1
                _lstMarkRecordTX1 = _ListMarkRecord1.Where(u => u.Title.Contains("M") || u.Title.Contains("P")).ToList();
                // diem dinh ky ky 1
                _lstMarkRecordDK1 = _ListMarkRecord1.Where(u => u.Title.Contains("V")).ToList();
                // diem thanh phan ky 2
                _ListMarkRecord = MarkRecordBusiness.SearchBySchool(schoolID, SearchInfo).Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                // diem TBM ky 2
                _ListSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(schoolID, SearchInfo).Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
            }
            // Danh sach diem thuong xuyen = diem M va diem P
            _lstMarkRecordTX = _ListMarkRecord.Where(u => u.Title.Contains("M") || u.Title.Contains("P")).ToList();
            // Danh sach diem dinh ky: diem V
            _lstMarkRecordDK = _ListMarkRecord.Where(u => u.Title.Contains("V")).ToList();
            Dictionary<string, object> DataDicSYLL = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemTX = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemDK = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemHKI = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemHKII = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemTBMI = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemTBMII = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemTBMCN = new Dictionary<string, object>();
            Dictionary<string, object> DataXepLoaiI = new Dictionary<string, object>();
            Dictionary<string, object> DataXepLoaiI1 = new Dictionary<string, object>();
            Dictionary<string, object> DataXepLoaiII = new Dictionary<string, object>();
            Dictionary<string, object> DataXepLoaiCN = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemTX1 = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemDK1 = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemHKI1 = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemTBMI1 = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemDanh1 = new Dictionary<string, object>();
            Dictionary<string, object> DataDiemDanh2 = new Dictionary<string, object>();
            List<Object> lstObjectSYLL = new List<object>();
            List<Object> lstObjectDiemTX = new List<object>();
            List<Object> lstObjectDiemDK = new List<object>();
            List<Object> lstObjectDiemHKI = new List<object>();
            List<Object> lstObjectDiemHKII = new List<object>();
            List<Object> lstObjectDiemTBMI = new List<object>();
            List<Object> lstObjectDiemTBMII = new List<object>();
            List<Object> lstObjectDiemTX1 = new List<object>();
            List<Object> lstObjectDiemDK1 = new List<object>();
            List<Object> lstObjectDiemHKI1 = new List<object>();
            List<Object> lstObjectDiemTBMI1 = new List<object>();
            List<Object> lstObjectDiemDanh1 = new List<object>();
            List<Object> lstObjectDiemDanh2 = new List<object>();
            List<Object> lstObjectXepLoaiI = new List<object>();
            List<Object> lstObjectXepLoaiI1 = new List<object>();
            List<Object> lstObjectXepLoaiII = new List<object>();
            List<Object> lstObjectDiemTBMCN = new List<object>();
            List<Object> lstObjectXepLoaiCN = new List<object>();
            #region Neu danh sach hoc sinh hoc nghe != null
            if (_lstFirstApprenticeshipTraining != null && _lstFirstApprenticeshipTraining.Count() > 0)
            {
                List<ApprenticeshipTraining> lstFirstApprenticeshipTraining = _lstFirstApprenticeshipTraining.OrderBy(o => o.PupilProfile.Name).ToList();
                int order = 0; // Thứ tự dòng
                int posSet = 6; // Vi tri bat dau fill du lieu
                #region Khai bao bien
                Dictionary<string, object> dataDiemTX = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemDK = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemTX1 = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemDK1 = new Dictionary<string, object>();
                Dictionary<string, object> dataSYLL = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemHKI = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemHKI1 = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemHKII = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemTBMI = new Dictionary<string, object>();
                Dictionary<string, object> dataXepLoaiI = new Dictionary<string, object>();
                Dictionary<string, object> dataXepLoaiI1 = new Dictionary<string, object>();
                Dictionary<string, object> dataXepLoaiII = new Dictionary<string, object>();
                Dictionary<string, object> dataXepLoaiCN = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemTBMI1 = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemTBMII = new Dictionary<string, object>();
                Dictionary<string, object> dataDiemTBMCN = new Dictionary<string, object>();
                List<MarkRecord> lstMarkRecordTX = new List<MarkRecord>();
                List<MarkRecord> lstMarkRecordDK = new List<MarkRecord>();
                List<MarkRecord> lstMarkRecordTX1 = new List<MarkRecord>();
                List<MarkRecord> lstMarkRecordDK1 = new List<MarkRecord>();
                MarkRecord ListMarkRecordHK = new MarkRecord();
                MarkRecord ListMarkRecordHK1 = new MarkRecord();
                SummedUpRecord lstSummedUpRecord = new SummedUpRecord();
                SummedUpRecord lstSummedUpRecord1 = new SummedUpRecord();
                List<ApprenticeshipTrainingAbsence> lstApprenticeshipTrainingAbsence = new List<ApprenticeshipTrainingAbsence>();
                string strMarkTX = "";
                string strMarkTX1 = "";
                string strMarkDK = "";
                string strMarkDK1 = "";
                #endregion
                foreach (ApprenticeshipTraining at in lstFirstApprenticeshipTraining) // for mỗi học sinh học nghề
                {
                    order++;
                    posSet++;
                    dataDiemTX = new Dictionary<string, object>();
                    dataDiemDK = new Dictionary<string, object>();
                    dataDiemTX1 = new Dictionary<string, object>();
                    dataDiemDK1 = new Dictionary<string, object>();
                    // Lấy dữ liệu sơ yếu lý lịch
                    dataSYLL = new Dictionary<string, object>();
                    dataSYLL["FullName"] = at.PupilProfile.FullName;
                    dataSYLL["SchoolName"] = at.SchoolProfile.SchoolName;
                    dataSYLL["BirthDate"] = at.PupilProfile.BirthDate;
                    dataSYLL["BirthPlace"] = at.PupilProfile.BirthPlace;
                    dataSYLL["FatherFullName"] = at.PupilProfile.FatherFullName;
                    dataSYLL["FatherJob"] = at.PupilProfile.FatherJob;
                    dataSYLL["MotherFullName"] = at.PupilProfile.MotherFullName;
                    dataSYLL["MotherJob"] = at.PupilProfile.MotherJob;
                    if (at.PupilProfile.PermanentResidentalAddress != "")
                    {
                        dataSYLL["Address"] = at.PupilProfile.PermanentResidentalAddress;
                    }
                    else
                    {
                        dataSYLL["Address"] = at.PupilProfile.HomeTown;
                    }
                    lstObjectSYLL.Add(dataSYLL);
                    #region Hoc sinh co trang thai khac dang hoc
                    if (at.PupilProfile.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        sheetAll.GetRange(6 + order, 16, 6 + order, 110).FillColor(Color.Yellow);
                        // Set tat ca cac ky = ""
                        if (semesterDeclaration != null && (semesterDeclaration.InterviewMark + semesterDeclaration.WritingMark) > numberOfMarkTX)
                        {
                            // ky 1
                            sheetAll.GetRange("CB" + (startRowFill + order).ToString(), "CE" + (startRowFill + order).ToString()).Merge();
                            sheetAll.SetCellValue("CB" + (6 + order).ToString(), "${DiemTX1[].get(KTTX1)}");
                            dataDiemTX1["KTTX1"] = " ";
                            lstObjectDiemTX1.Add(dataDiemTX1);
                            // ky 2
                            sheetAll.GetRange("CO" + (startRowFill + order).ToString(), "CR" + (startRowFill + order).ToString()).Merge();
                            sheetAll.SetCellValue("CO" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                            dataDiemTX["KTTX"] = " ";
                            lstObjectDiemTX.Add(dataDiemTX);
                        }
                        if (semesterDeclaration != null && semesterDeclaration.TwiceCoeffiecientMark > numberOfMarkDK)
                        {
                            // ky 1
                            sheetAll.GetRange("CF" + (startRowFill + order).ToString(), "CG" + (startRowFill + order).ToString()).Merge();
                            sheetAll.SetCellValue("CF" + (startRowFill + order).ToString(), "${DiemDK1[].get(KTDK1)}");
                            dataDiemDK1["KTDK1"] = " ";
                            lstObjectDiemDK1.Add(dataDiemDK1);
                            // ky 2
                            sheetAll.GetRange("CS" + (startRowFill + order).ToString(), "CT" + (startRowFill + order).ToString()).Merge();
                            sheetAll.SetCellValue("CS" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                            dataDiemDK["KTDK"] = " ";
                            lstObjectDiemDK.Add(dataDiemDK);
                        }
                        else
                        {
                            // ky 1
                            for (int i = 0; i < 4; i++)
                            {
                                sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CB") + i, "${DiemTX1[].get(KTTX1" + i + ")}");
                                dataDiemTX1["KTTX1" + i] = " ";
                                lstObjectDiemTX1.Add(dataDiemTX1);
                            }
                            for (int i = 0; i < 2; i++)
                            {
                                sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CF") + i, "${DiemDK1[].get(KTTX1" + i + ")}");
                                dataDiemDK1["KTDK1" + i] = " ";
                                lstObjectDiemDK1.Add(dataDiemDK1);
                            }
                            // ky 2
                            for (int i = 0; i < 4; i++)
                            {
                                sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CO") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                dataDiemTX["KTTX" + i] = " ";
                                lstObjectDiemTX.Add(dataDiemTX);
                            }
                            for (int i = 0; i < 2; i++)
                            {
                                sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CS") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                dataDiemDK["KTDK" + i] = " ";
                                lstObjectDiemDK.Add(dataDiemDK);
                            }
                        }
                        // ky 1
                        dataDiemHKI1 = new Dictionary<string, object>();
                        dataDiemHKI1["KTHKI"] = " ";
                        lstObjectDiemHKI1.Add(dataDiemHKI1);
                        dataDiemTBMI1 = new Dictionary<string, object>();
                        dataDiemTBMI1["TBMI"] = " ";
                        lstObjectDiemTBMI1.Add(dataDiemTBMI1);
                        dataXepLoaiI1 = new Dictionary<string, object>();
                        dataXepLoaiI1["XLHKI"] = " ";
                        lstObjectXepLoaiI1.Add(dataXepLoaiI1);
                        // ky 2
                        dataDiemHKII = new Dictionary<string, object>();
                        dataDiemHKII["KTHKII"] = " ";
                        lstObjectDiemHKII.Add(dataDiemHKII);
                        dataDiemTBMII = new Dictionary<string, object>();
                        dataDiemTBMII["TBMII"] = " ";
                        lstObjectDiemTBMII.Add(dataDiemTBMII);
                        dataXepLoaiII = new Dictionary<string, object>();
                        dataXepLoaiII["XLHKII"] = " ";
                        lstObjectXepLoaiII.Add(dataXepLoaiII);
                        // ca nam
                        // Trung binh ca nam
                        dataDiemTBMCN = new Dictionary<string, object>();
                        dataDiemTBMCN["TBMCN"] = " ";
                        lstObjectDiemTBMCN.Add(dataDiemTBMCN);
                        // Xep loai ca nam
                        dataXepLoaiCN = new Dictionary<string, object>();
                        dataXepLoaiCN["XLCN"] = " ";
                        lstObjectXepLoaiCN.Add(dataXepLoaiCN);
                    }
                    #endregion
                    #region Hoc sinh co trang thai dang hoc
                    else
                    {
                        lstMarkRecordTX = _lstMarkRecordTX.Where(o => o.PupilID == at.PupilID).ToList();
                        lstMarkRecordDK = _lstMarkRecordDK.Where(o => o.PupilID == at.PupilID).ToList();
                        lstSummedUpRecord = _ListSummedUpRecord.Where(o => o.PupilID == at.PupilID && o.SubjectID == at.ApprenticeshipSubject.SubjectID).OrderByDescending(u => u.SummedUpRecordID).FirstOrDefault();
                        // Thong tin diem danh
                        lstApprenticeshipTrainingAbsence = _lstApprenticeshipTrainingAbsence.Where(o => o.PupilID == at.PupilID).ToList();
                        #region Fill du lieu ky 1
                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            // Thong tin diem danh
                            if (lstApprenticeshipTrainingAbsence != null && lstApprenticeshipTrainingAbsence.Count() > 0)
                            {
                                // Lay cac ngay, thang nghi hoc cua hoc sinh
                                foreach (ApprenticeshipTrainingAbsence lstApp in lstApprenticeshipTrainingAbsence)
                                {
                                    int pos = lstDayHKI.Where(o => o.month == lstApp.AbsentDate.Month && o.day == lstApp.AbsentDate.Day).Select(o => o.order).FirstOrDefault();
                                    if (pos > 0)
                                    {
                                        sheetAll.SetCellValue(6 + order, pos + firstColumnAbsence1 - 1, lstApp.IsAccepted ? "P" : "K");
                                    }
                                }
                            }
                            // Thong tin diem
                            if (semesterDeclaration != null && (semesterDeclaration.InterviewMark + semesterDeclaration.WritingMark) > numberOfMarkTX) // 4-So cot diem thuong xuyen trong file template
                            {
                                strMarkTX = "";
                                // Lấy dữ liệu điểm thường xuyên của từng học sinh
                                if (lstMarkRecordTX != null && lstMarkRecordTX.Count() > 0)
                                {
                                    foreach (MarkRecord mr in lstMarkRecordTX)
                                    {
                                        strMarkTX += mr.Mark + "  ";
                                    }
                                    sheetAll.GetRange("CB" + (startRowFill + order).ToString(), "CE" + (startRowFill + order).ToString()).Merge();
                                    sheetAll.SetCellValue("CB" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                                }
                                else
                                {
                                    strMarkTX += " ";
                                    sheetAll.GetRange("CB" + (startRowFill + order).ToString(), "CE" + (startRowFill + order).ToString()).Merge();
                                    sheetAll.SetCellValue("CB" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                                }
                                dataDiemTX["KTTX"] = strMarkTX.Replace(",", ".").Replace(".00", "");
                                lstObjectDiemTX.Add(dataDiemTX);
                            }
                            if (semesterDeclaration != null && semesterDeclaration.TwiceCoeffiecientMark > numberOfMarkDK) // 2-số cột điểm định kỳ trong file template
                            {
                                strMarkDK = "";
                                // Lấy dữ liệu điểm định kỳ (điểm 1 tiết) của từng học sinh
                                if (lstMarkRecordDK != null && lstMarkRecordDK.Count() > 0)
                                {
                                    foreach (MarkRecord mr in lstMarkRecordDK)
                                    {
                                        strMarkDK += mr.Mark + "  ";
                                    }
                                    sheetAll.GetRange("CF" + (startRowFill + order).ToString(), "CG" + (startRowFill + order).ToString()).Merge();
                                    sheetAll.SetCellValue("CF" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                                }
                                else
                                {
                                    strMarkDK += " ";
                                    sheetAll.GetRange("CF" + (startRowFill + order).ToString(), "CG" + (startRowFill + order).ToString()).Merge();
                                    sheetAll.SetCellValue("CF" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                                }
                                dataDiemDK["KTDK"] = strMarkDK.Replace(",", ".").Replace(".00", "");
                                lstObjectDiemDK.Add(dataDiemDK);
                            }
                            else
                            {
                                // Lấy dữ liệu điểm thường xuyên
                                if (lstMarkRecordTX != null && lstMarkRecordTX.Count() > 0)
                                {
                                    int i = 0;
                                    foreach (MarkRecord mr in lstMarkRecordTX)
                                    {
                                        sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CB") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                        dataDiemTX["KTTX" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                        lstObjectDiemTX.Add(dataDiemTX);
                                        i++;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < 4; i++)
                                    {
                                        sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CB") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                        dataDiemTX["KTTX" + i] = " ";
                                        lstObjectDiemTX.Add(dataDiemTX);
                                    }
                                }
                                // Lấy dữ liệu điểm định kỳ
                                if (lstMarkRecordDK != null && lstMarkRecordDK.Count() > 0)
                                {
                                    int i = 0;
                                    foreach (MarkRecord mr in lstMarkRecordDK)
                                    {
                                        sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CF") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                        dataDiemDK["KTDK" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                        lstObjectDiemDK.Add(dataDiemDK);
                                        i++;
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < 2; i++)
                                    {
                                        sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CF") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                        dataDiemDK["KTDK" + i] = " ";
                                        lstObjectDiemDK.Add(dataDiemDK);
                                    }
                                }
                            }
                            // Lấy dữ liệu điểm học kỳ
                            dataDiemHKI = new Dictionary<string, object>();
                            ListMarkRecordHK = _ListMarkRecord.Where(o => o.PupilID == at.PupilID && o.Title == "HK").FirstOrDefault();
                            if (ListMarkRecordHK != null)
                            {
                                dataDiemHKI["KTHKI"] = ListMarkRecordHK.Mark.ToString().Replace(",", ".").Replace(".00", "");
                            }
                            else
                            {
                                dataDiemHKI["KTHKI"] = " ";
                            }
                            lstObjectDiemHKI.Add(dataDiemHKI);
                            dataDiemTBMI = new Dictionary<string, object>();
                            dataXepLoaiI = new Dictionary<string, object>();
                            // Lấy dữ liệu điểm TBM, xep loai
                            if (lstSummedUpRecord != null)
                            {
                                dataDiemTBMI["TBMI"] = lstSummedUpRecord.SummedUpMark;
                                // =IF(CI7="","",IF(CI7>=8,"GIỎI",IF(AND(CI7>=6.5,CI7<8),"KHÁ",IF(AND(CI7>=5,CI7<6.5),"TB",IF(AND(CI7>=3.5,CI7<5),"YẾU","KÉM")))))
                                dataXepLoaiI["XLHKI"] = "=IF(CI" + posSet + "=\"\",\"\",IF(CI" + posSet + ">=8,\"GIỎI\",IF(AND(CI" + posSet + ">=6.5,CI" +
                                                            posSet + "<8),\"KHÁ\",IF(AND(CI" + posSet + ">=5,CI" + posSet + "<6.5),\"TB\",IF(AND(CI" +
                                                            posSet + ">=3.5,CI" + posSet + "<5),\"YẾU\",\"KÉM\")))))"; 
                            }
                            else
                            {
                                dataDiemTBMI["TBMI"] = " ";
                                dataXepLoaiI["XLHKI"] = " ";
                            }
                            lstObjectDiemTBMI.Add(dataDiemTBMI);
                            lstObjectXepLoaiI.Add(dataXepLoaiI);
                            // Neu mon nghe chi hoc trong ky 1 => lay du lieu ky 1 lam du lieu ca nam
                            if (semesterLearn == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            {
                                dataDiemTBMCN = new Dictionary<string, object>();
                                dataXepLoaiCN = new Dictionary<string, object>();
                                if (lstSummedUpRecord != null)
                                {
                                    dataDiemTBMCN["TBMCN"] = lstSummedUpRecord.SummedUpMark;
                                    dataXepLoaiCN["XLCN"] = "=CJ" + posSet;
                                }
                                else
                                {
                                    dataDiemTBMCN["TBMCN"] = " ";
                                    dataXepLoaiCN["XLCN"] = " ";
                                }
                                lstObjectDiemTBMCN.Add(dataDiemTBMCN);
                                lstObjectXepLoaiCN.Add(dataXepLoaiCN);
                            }
                        }
                        #endregion
                        #region Fill du lieu ky 2
                        else
                        {
                            //Neu mon hoc hoc trong ca 2 ky
                            if (semesterLearn == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            {
                                lstMarkRecordTX1 = _lstMarkRecordTX1.Where(o => o.PupilID == at.PupilID).ToList();
                                lstMarkRecordDK1 = _lstMarkRecordDK1.Where(o => o.PupilID == at.PupilID).ToList();
                                lstSummedUpRecord1 = _ListSummedUpRecord1.Where(o => o.PupilID == at.PupilID && o.SubjectID == at.ApprenticeshipSubject.SubjectID).OrderByDescending(u => u.SummedUpRecordID).FirstOrDefault();
                                // Thong tin diem danh ky 1
                                if (lstApprenticeshipTrainingAbsence != null && lstApprenticeshipTrainingAbsence.Count() > 0)
                                {
                                    // Lay cac ngay, thang nghi hoc cua hoc sinh
                                    foreach (ApprenticeshipTrainingAbsence lstApp in lstApprenticeshipTrainingAbsence)
                                    {
                                        int pos = lstDayHKI.Where(o => o.month == lstApp.AbsentDate.Month && o.day == lstApp.AbsentDate.Day).Select(o => o.order).FirstOrDefault();
                                        if (pos > 0)
                                        {
                                            sheetAll.SetCellValue(startRowFill + order, pos + firstColumnAbsence1 - 1, lstApp.IsAccepted ? "P" : "K");
                                        }
                                    }
                                }
                                // Thong tin diem danh ky 2
                                if (lstApprenticeshipTrainingAbsence != null && lstApprenticeshipTrainingAbsence.Count() > 0)
                                {
                                    // Lay cac ngay, thang nghi hoc cua hoc sinh
                                    foreach (ApprenticeshipTrainingAbsence lstApp in lstApprenticeshipTrainingAbsence)
                                    {
                                        int pos = lstDayHKII.Where(o => o.month == lstApp.AbsentDate.Month && o.day == lstApp.AbsentDate.Day).Select(o => o.order).FirstOrDefault();
                                        if (pos > 0)
                                        {
                                            sheetAll.SetCellValue(startRowFill + order, pos + firstColumnAbsence2 - 1, lstApp.IsAccepted ? "P" : "K");
                                        }
                                    }
                                }
                                // Thong tin diem
                                if (semesterDeclaration != null && (semesterDeclaration.InterviewMark + semesterDeclaration.WritingMark) > numberOfMarkTX)
                                // 4-So cot diem thuong xuyen trong file template
                                {
                                    // Lay thong tin ky 1
                                    strMarkTX1 = "";
                                    // Lấy dữ liệu điểm thường xuyên của từng học sinh
                                    if (lstMarkRecordTX1 != null && lstMarkRecordTX1.Count() > 0)
                                    {
                                        foreach (MarkRecord mr in lstMarkRecordTX1)
                                        {
                                            strMarkTX1 += mr.Mark + "  ";
                                        }
                                        sheetAll.GetRange("CB" + (startRowFill + order).ToString(), "CE" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CB" + (6 + order).ToString(), "${DiemTX1[].get(KTTX1)}");
                                    }
                                    else
                                    {
                                        strMarkTX1 = " ";
                                        sheetAll.GetRange("CB" + (startRowFill + order).ToString(), "CE" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CB" + (6 + order).ToString(), "${DiemTX1[].get(KTTX1)}");
                                    }
                                    dataDiemTX1["KTTX1"] = strMarkTX1.Replace(",", ".").Replace(".00", "");
                                    lstObjectDiemTX1.Add(dataDiemTX1);
                                    // Lay thong tin ky 2
                                    strMarkTX = "";
                                    // Lấy dữ liệu điểm thường xuyên của từng học sinh
                                    if (lstMarkRecordTX != null && lstMarkRecordTX.Count() > 0)
                                    {
                                        foreach (MarkRecord mr in lstMarkRecordTX)
                                        {
                                            strMarkTX += mr.Mark + "  ";
                                        }
                                        sheetAll.GetRange("CO" + (startRowFill + order).ToString(), "CR" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CO" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                                    }
                                    else
                                    {
                                        strMarkTX = " ";
                                        sheetAll.GetRange("CO" + (startRowFill + order).ToString(), "CR" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CO" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                                    }
                                    dataDiemTX["KTTX"] = strMarkTX.Replace(",", ".").Replace(".00", "");
                                    lstObjectDiemTX.Add(dataDiemTX);
                                }
                                if (semesterDeclaration != null && semesterDeclaration.TwiceCoeffiecientMark > numberOfMarkDK) // 2-số cột điểm định kỳ trong file template
                                {
                                    // Lay thong tin ky 1
                                    strMarkDK1 = "";
                                    // Lấy dữ liệu điểm định kỳ (điểm 1 tiết) của từng học sinh
                                    if (lstMarkRecordDK1 != null && lstMarkRecordDK1.Count() > 0)
                                    {
                                        foreach (MarkRecord mr in lstMarkRecordDK1)
                                        {
                                            strMarkDK1 += mr.Mark + "  ";
                                        }
                                        sheetAll.GetRange("CF" + (startRowFill + order).ToString(), "CG" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CF" + (startRowFill + order).ToString(), "${DiemDK1[].get(KTDK1)}");
                                    }
                                    else
                                    {
                                        strMarkDK1 = " ";
                                        sheetAll.GetRange("CF" + (startRowFill + order).ToString(), "CG" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CF" + (startRowFill + order).ToString(), "${DiemDK1[].get(KTDK1)}");
                                    }

                                    dataDiemDK1["KTDK1"] = strMarkDK1.Replace(",", ".").Replace(".00", "");
                                    lstObjectDiemDK1.Add(dataDiemDK1);
                                    // lay thong tin ky 2
                                    strMarkDK = "";
                                    // Lấy dữ liệu điểm định kỳ (điểm 1 tiết) của từng học sinh
                                    if (lstMarkRecordDK != null && lstMarkRecordDK.Count() > 0)
                                    {
                                        foreach (MarkRecord mr in lstMarkRecordDK)
                                        {
                                            strMarkDK += mr.Mark + "  ";
                                        }
                                        sheetAll.GetRange("CS" + (startRowFill + order).ToString(), "CT" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CS" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                                    }
                                    else
                                    {
                                        strMarkDK = " ";
                                        sheetAll.GetRange("CS" + (startRowFill + order).ToString(), "CT" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CS" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                                    }
                                    dataDiemDK["KTDK"] = strMarkDK.Replace(",", ".").Replace(".00", "");
                                    lstObjectDiemDK.Add(dataDiemDK);
                                }
                                else
                                {
                                    // Lay thong tin ky 1
                                    // Lấy dữ liệu điểm thường xuyên
                                    if (lstMarkRecordTX1 != null && lstMarkRecordTX1.Count() > 0)
                                    {
                                        int i = 0;
                                        foreach (MarkRecord mr in lstMarkRecordTX1)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CB") + i, "${DiemTX1[].get(KTTX1" + i + ")}");
                                            dataDiemTX1["KTTX1" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                            lstObjectDiemTX1.Add(dataDiemTX1);
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < 4; i++)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CB") + i, "${DiemTX1[].get(KTTX1" + i + ")}");
                                            dataDiemTX1["KTTX1" + i] = " ";
                                            lstObjectDiemTX1.Add(dataDiemTX1);
                                        }
                                    }
                                    // Lấy dữ liệu điểm định kỳ
                                    if (lstMarkRecordDK1 != null && lstMarkRecordDK1.Count() > 0)
                                    {
                                        int i = 0;
                                        foreach (MarkRecord mr in lstMarkRecordDK1)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CF") + i, "${DiemDK1[].get(KTTX1" + i + ")}");
                                            dataDiemDK1["KTDK1" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                            lstObjectDiemDK1.Add(dataDiemDK1);
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < 2; i++)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CF") + i, "${DiemDK1[].get(KTTX1" + i + ")}");
                                            dataDiemDK1["KTDK1" + i] = " ";
                                            lstObjectDiemDK1.Add(dataDiemDK1);
                                        }
                                    }
                                    // Lay thong tin ky 2
                                    // Lấy dữ liệu điểm thường xuyên
                                    if (lstMarkRecordTX != null && lstMarkRecordTX.Count() > 0)
                                    {
                                        int i = 0;
                                        foreach (MarkRecord mr in lstMarkRecordTX)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CO") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                            dataDiemTX["KTTX" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                            lstObjectDiemTX.Add(dataDiemTX);
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < 4; i++)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CO") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                            dataDiemTX["KTTX" + i] = " ";
                                            lstObjectDiemTX.Add(dataDiemTX);
                                        }
                                    }
                                    // Lấy dữ liệu điểm định kỳ
                                    if (lstMarkRecordDK != null && lstMarkRecordDK.Count() > 0)
                                    {
                                        int i = 0;
                                        foreach (MarkRecord mr in lstMarkRecordDK)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CS") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                            dataDiemDK["KTDK" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                            lstObjectDiemDK.Add(dataDiemDK);
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < 2; i++)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CS") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                            dataDiemDK["KTDK" + i] = " ";
                                            lstObjectDiemDK.Add(dataDiemDK);
                                        }
                                    }
                                }
                                // Lay thong tin ky 1
                                // Lấy dữ liệu điểm học kỳ
                                dataDiemHKI1 = new Dictionary<string, object>();
                                ListMarkRecordHK1 = _ListMarkRecord1.Where(o => o.PupilID == at.PupilID && o.Title == "HK").FirstOrDefault();
                                if (ListMarkRecordHK1 != null)
                                {
                                    dataDiemHKI1["KTHKI"] = ListMarkRecordHK1.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                }
                                else
                                {
                                    dataDiemHKI1["KTHKI"] = " ";
                                }
                                lstObjectDiemHKI1.Add(dataDiemHKI1);
                                dataDiemTBMI1 = new Dictionary<string, object>();
                                dataXepLoaiI1 = new Dictionary<string, object>();
                                // Lấy dữ liệu điểm TBM
                                if (lstSummedUpRecord1 != null)
                                {
                                    dataDiemTBMI1["TBMI"] = lstSummedUpRecord1.SummedUpMark;
                                    dataXepLoaiI1["XLHKI"] = "=IF(CI" + posSet + "=\"\";\"\";IF(CI" + posSet + ">=8;\"GIỎI\";IF(AND(CI" + posSet + ">=6.5;CI" +
                                                            posSet + "<8);\"KHÁ\";IF(AND(CI" + posSet + ">=5;CI" + posSet + "<6.5);\"TB\";IF(AND(CI" +
                                                            posSet + ">=3.5;CI" + posSet + "<5);\"YẾU\";\"KÉM\")))))"; 
                                    
                                }
                                else
                                {
                                    dataDiemTBMI1["TBMI"] = " ";
                                    dataXepLoaiI1["XLHKI"] = " ";
                                }
                                lstObjectDiemTBMI1.Add(dataDiemTBMI1);
                                lstObjectXepLoaiI1.Add(dataXepLoaiI1);
                                // Lay thong tin ky 2
                                // Lấy dữ liệu điểm học kỳ
                                dataDiemHKII = new Dictionary<string, object>();
                                ListMarkRecordHK = _ListMarkRecord.Where(o => o.PupilID == at.PupilID && o.Title == "HK").FirstOrDefault();
                                if (ListMarkRecordHK != null)
                                {
                                    dataDiemHKII["KTHKII"] = ListMarkRecordHK.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                }
                                else
                                {
                                    dataDiemHKII["KTHKII"] = " ";
                                }
                                lstObjectDiemHKII.Add(dataDiemHKII);
                                dataDiemTBMII = new Dictionary<string, object>();
                                dataXepLoaiII = new Dictionary<string, object>();
                                // Lấy dữ liệu điểm TBM
                                if (lstSummedUpRecord != null)
                                {
                                    dataDiemTBMII["TBMII"] = lstSummedUpRecord.SummedUpMark;
                                    dataXepLoaiII["XLHKII"] = "=IF(CV" + posSet + "=\"\";\"\";IF(CV" + posSet + ">=8;\"GIỎI\";IF(AND(CV" + posSet + ">=6.5;CV" +
                                                            posSet + "<8);\"KHÁ\";IF(AND(CV" + posSet + ">=5;CV" + posSet + "<6.5);\"TB\";IF(AND(CV" +
                                                            posSet + ">=3.5;CV" + posSet + "<5);\"YẾU\";\"KÉM\")))))";
                                }
                                else
                                {
                                    dataDiemTBMII["TBMII"] = " ";
                                    dataXepLoaiII["XLHKII"] = " ";
                                }
                                lstObjectDiemTBMII.Add(dataDiemTBMII);
                                lstObjectXepLoaiII.Add(dataXepLoaiII);
                                // Add du lieu ca nam
                                dataDiemTBMCN = new Dictionary<string, object>();
                                dataXepLoaiCN = new Dictionary<string, object>();
                                if (lstSummedUpRecord1 != null && lstSummedUpRecord != null)
                                {
                                    // =IF(IF(OR(DC7="",DB7=""),"",ROUND(AVERAGE(DB7:DC7,DC7),1))=10,"10",IF(OR(DC7="",DB7=""),"",ROUND(AVERAGE(DB7:DC7,DC7),1)))
                                    dataDiemTBMCN["TBMCN"] = "=IF(IF(OR(DC" + posSet + "=\"\";DB" + posSet + "=\"\");\"\";ROUND(AVERAGE(DB" + posSet + ":DC" +
                                                                posSet + ";DC" + posSet + ");1))=10;\"10\";IF(OR(DC" + posSet + "=\"\";DB" + posSet +
                                                                "=\"\");\"\";ROUND(AVERAGE(DB" + posSet + ":DC" + posSet + ";DC" + posSet + ");1)))";
                                    // =IF(DD7="","",IF(DD7>=8,"GIỎI",IF(AND(DD7>=6.5,DD7<8),"KHÁ",IF(AND(DD7>=5,DD7<6.5),"TB",IF(AND(DD7>=3.5,DD7<5),"YẾU","KÉM")))))
                                    dataXepLoaiCN["XLCN"] = "=IF(DD" + posSet + "=\"\";\"\";IF(DD" + posSet + ">=8;\"GIỎI\";IF(AND(DD" + posSet + ">=6.5;DD" +
                                                            posSet + "<8);\"KHÁ\";IF(AND(DD" + posSet + ">=5;DD" + posSet + "<6.5);\"TB\";IF(AND(DD" +
                                                            posSet + ">=3.5;DD" + posSet + "<5);\"YẾU\";\"KÉM\")))))";
                                }
                                else
                                {
                                    dataDiemTBMCN["TBMCN"] = " ";
                                    dataXepLoaiCN["XLCN"] = " ";
                                }
                                lstObjectDiemTBMCN.Add(dataDiemTBMCN);
                                lstObjectXepLoaiCN.Add(dataXepLoaiCN);
                            }
                            // Neu mon hoc chi hoc trong ky 2
                            else
                            {
                                // Thong tin diem danh ky 2
                                if (lstApprenticeshipTrainingAbsence != null && lstApprenticeshipTrainingAbsence.Count() > 0)
                                {
                                    // Lay cac ngay, thang nghi hoc cua hoc sinh
                                    foreach (ApprenticeshipTrainingAbsence lstApp in lstApprenticeshipTrainingAbsence)
                                    {
                                        int pos = lstDayHKII.Where(o => o.month == lstApp.AbsentDate.Month && o.day == lstApp.AbsentDate.Day).Select(o => o.order).FirstOrDefault();
                                        if (pos > 0)
                                        {
                                            sheetAll.SetCellValue(startRowFill + order, pos + firstColumnAbsence2 - 1, lstApp.IsAccepted ? "P" : "K");
                                        }
                                    }
                                }
                                // Thong tin diem
                                if (semesterDeclaration != null && (semesterDeclaration.InterviewMark + semesterDeclaration.WritingMark) > numberOfMarkTX)
                                // 4-So cot diem thuong xuyen trong file template
                                {
                                    strMarkTX = "";
                                    // Lấy dữ liệu điểm thường xuyên của từng học sinh
                                    if (lstMarkRecordTX != null && lstMarkRecordTX.Count() > 0)
                                    {
                                        foreach (MarkRecord mr in lstMarkRecordTX)
                                        {
                                            strMarkTX += mr.Mark + "  ";
                                        }
                                        sheetAll.GetRange("CO" + (startRowFill + order).ToString(), "CR" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CO" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                                    }
                                    else
                                    {
                                        strMarkTX += " ";
                                        sheetAll.GetRange("CO" + (startRowFill + order).ToString(), "CR" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CO" + (startRowFill + order).ToString(), "${DiemTX[].get(KTTX)}");
                                    }
                                    dataDiemTX["KTTX"] = strMarkTX.Replace(",", ".").Replace(".00", "");
                                    lstObjectDiemTX.Add(dataDiemTX);
                                }
                                if (semesterDeclaration != null && semesterDeclaration.TwiceCoeffiecientMark > numberOfMarkDK) // 2-số cột điểm định kỳ trong file template
                                {
                                    strMarkDK = "";
                                    // Lấy dữ liệu điểm định kỳ (điểm 1 tiết) của từng học sinh
                                    if (lstMarkRecordDK != null && lstMarkRecordDK.Count() > 0)
                                    {
                                        foreach (MarkRecord mr in lstMarkRecordDK)
                                        {
                                            strMarkDK += mr.Mark + "  ";
                                        }
                                        sheetAll.GetRange("CS" + (startRowFill + order).ToString(), "CT" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CS" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                                    }
                                    else
                                    {
                                        strMarkDK += " ";
                                        sheetAll.GetRange("CS" + (startRowFill + order).ToString(), "CT" + (startRowFill + order).ToString()).Merge();
                                        sheetAll.SetCellValue("CS" + (startRowFill + order).ToString(), "${DiemDK[].get(KTDK)}");
                                    }
                                    dataDiemDK["KTDK"] = strMarkDK.Replace(",", ".").Replace(".00", "");
                                    lstObjectDiemDK.Add(dataDiemDK);
                                }
                                else
                                {
                                    // Lấy dữ liệu điểm thường xuyên
                                    if (lstMarkRecordTX != null && lstMarkRecordTX.Count() > 0)
                                    {
                                        int i = 0;
                                        foreach (MarkRecord mr in lstMarkRecordTX)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CO") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                            dataDiemTX["KTTX" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                            lstObjectDiemTX.Add(dataDiemTX);
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < 4; i++)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CO") + i, "${DiemTX[].get(KTTX" + i + ")}");
                                            dataDiemTX["KTTX" + i] = " ";
                                            lstObjectDiemTX.Add(dataDiemTX);
                                        }
                                    }
                                    // Lấy dữ liệu điểm định kỳ
                                    if (lstMarkRecordDK != null && lstMarkRecordDK.Count() > 0)
                                    {
                                        int i = 0;
                                        foreach (MarkRecord mr in lstMarkRecordDK)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CS") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                            dataDiemDK["KTDK" + i] = mr.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                            lstObjectDiemDK.Add(dataDiemDK);
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < 2; i++)
                                        {
                                            sheetAll.SetCellValue((startRowFill + order), this.ColumnNumber("CS") + i, "${DiemDK[].get(KTTX" + i + ")}");
                                            dataDiemDK["KTDK" + i] = " ";
                                            lstObjectDiemDK.Add(dataDiemDK);
                                        }
                                    }
                                }
                                // Lấy dữ liệu điểm học kỳ
                                dataDiemHKII = new Dictionary<string, object>();
                                ListMarkRecordHK = _ListMarkRecord.Where(o => o.PupilID == at.PupilID && o.Title == "HK").FirstOrDefault();
                                if (ListMarkRecordHK != null)
                                {
                                    dataDiemHKII["KTHKII"] = ListMarkRecordHK.Mark.ToString().Replace(",", ".").Replace(".00", "");
                                }
                                else
                                {
                                    dataDiemHKII["KTHKII"] = " ";
                                }
                                lstObjectDiemHKII.Add(dataDiemHKII);
                                dataDiemTBMII = new Dictionary<string, object>();
                                dataXepLoaiII = new Dictionary<string, object>();
                                // Lấy dữ liệu điểm TBM
                                if (lstSummedUpRecord != null)
                                {
                                    dataDiemTBMII["TBMII"] = lstSummedUpRecord.SummedUpMark;
                                    dataXepLoaiII["XLHKII"] = "=IF(CV" + posSet + "=\"\";\"\";IF(CV" + posSet + ">=8;\"GIỎI\";IF(AND(CV" + posSet + ">=6.5;CV" +
                                                            posSet + "<8);\"KHÁ\";IF(AND(CV" + posSet + ">=5;CV" + posSet + "<6.5);\"TB\";IF(AND(CV" +
                                                            posSet + ">=3.5;CV" + posSet + "<5);\"YẾU\";\"KÉM\")))))";
                                }
                                else
                                {
                                    dataDiemTBMII["TBMII"] = " ";
                                    dataXepLoaiII["XLHKII"] = " ";
                                }
                                lstObjectDiemTBMII.Add(dataDiemTBMII);
                                lstObjectXepLoaiII.Add(dataXepLoaiII);
                                // Add du lieu ca nam
                                dataDiemTBMCN = new Dictionary<string, object>();
                                dataXepLoaiCN = new Dictionary<string, object>();
                                if (lstSummedUpRecord != null)
                                {
                                    dataDiemTBMCN["TBMCN"] = lstSummedUpRecord.SummedUpMark;
                                    dataXepLoaiCN["XLCN"] = "=CW" + posSet;
                                }
                                else
                                {
                                    dataDiemTBMCN["TBMCN"] = " ";
                                    dataXepLoaiCN["XLCN"] = " ";
                                }
                                lstObjectDiemTBMCN.Add(dataDiemTBMCN);
                                lstObjectXepLoaiCN.Add(dataXepLoaiCN);
                            }
                        }
                        #endregion
                    }
                    #endregion
                }

                // Fill du lieu ky 1
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    // Fill dữ kiệu sơ yếu lý lịch
                    DataDicSYLL.Add("SYLL", lstObjectSYLL);
                    sheetAll.GetRange("Q7", "Z65").FillVariableValue(DataDicSYLL);
                    // Fill dữ liệu điểm thường xuyên
                    DataDiemTX.Add("DiemTX", lstObjectDiemTX);
                    sheetAll.GetRange("CB7", "CE65").FillVariableValue(DataDiemTX);
                    // Fill dữ liệu điểm định kỳ
                    DataDiemDK.Add("DiemDK", lstObjectDiemDK);
                    sheetAll.GetRange("CF7", "CG65").FillVariableValue(DataDiemDK);
                    // Fill dữ liệu điểm học kỳ 1
                    DataDiemHKI.Add("DiemHKI", lstObjectDiemHKI);
                    sheetAll.GetRange("CH7", "CH65").FillVariableValue(DataDiemHKI);
                    // Fill  dữ liệu điểm TBM học kỳ 1
                    DataDiemTBMI.Add("DiemTBMI", lstObjectDiemTBMI);
                    sheetAll.GetRange("CO7", "CO65").FillVariableValue(DataDiemTBMI);
                    // Fill du lieu xep loai hoc ky 1
                    DataXepLoaiI.Add("XepLoaiHKI", lstObjectXepLoaiI);
                    sheetAll.GetRange("CJ7", "CJ65").FillVariableValue(DataXepLoaiI);
                    if (semesterLearn == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        // Fill du lieu diem TBMCN
                        DataDiemTBMCN.Add("DiemTBMCN", lstObjectDiemTBMCN);
                        sheetAll.GetRange("DD7", "DD65").FillVariableValue(DataDiemTBMCN);
                        // Fill du lieu xep loai CN
                        DataXepLoaiCN.Add("XepLoaiCN", lstObjectXepLoaiCN);
                        sheetAll.GetRange("DE5", "DE65").FillVariableValue(DataXepLoaiCN);
                    }
                }
                // Fil du lieu ky 2
                else
                {
                    // Fill dữ kiệu sơ yếu lý lịch
                    DataDicSYLL.Add("SYLL", lstObjectSYLL);
                    sheetAll.GetRange("Q7", "Z65").FillVariableValue(DataDicSYLL);
                    // Ky 1
                    // Fill dữ liệu điểm thường xuyên
                    DataDiemTX1.Add("DiemTX1", lstObjectDiemTX1);
                    sheetAll.GetRange("CB7", "CE65").FillVariableValue(DataDiemTX1);
                    // Fill dữ liệu điểm định kỳ
                    DataDiemDK1.Add("DiemDK1", lstObjectDiemDK1);
                    sheetAll.GetRange("CF7", "CG65").FillVariableValue(DataDiemDK1);
                    // Fill dữ liệu điểm học kỳ 1
                    DataDiemHKI1.Add("DiemHKI", lstObjectDiemHKI1);
                    sheetAll.GetRange("CH7", "CH65").FillVariableValue(DataDiemHKI1);
                    // Fill  dữ liệu điểm TBM học kỳ 1
                    DataDiemTBMI1.Add("DiemTBMI", lstObjectDiemTBMI1);
                    //sheetAll.GetRange("DK7", "DK65").FillVariableValue(DataDiemTBMI1);
                    sheetAll.GetRange("CI7", "CI65").FillVariableValue(DataDiemTBMI1);
                    // Fill du lieu xep loai hoc ky 1
                    DataXepLoaiI1.Add("XepLoaiHKI", lstObjectXepLoaiI1);
                    sheetAll.GetRange("CJ7", "CJ65").FillVariableValue(DataXepLoaiI1);
                    //Ky 2
                    // Fill dữ liệu điểm thường xuyên
                    DataDiemTX.Add("DiemTX", lstObjectDiemTX);
                    sheetAll.GetRange("CO7", "CR65").FillVariableValue(DataDiemTX);
                    // Fill dữ liệu điểm định kỳ
                    DataDiemDK.Add("DiemDK", lstObjectDiemDK);
                    sheetAll.GetRange("CS7", "CT65").FillVariableValue(DataDiemDK);
                    // Fill dữ liệu điểm học kỳ 2
                    DataDiemHKII.Add("DiemHKII", lstObjectDiemHKII);
                    sheetAll.GetRange("CU7", "CU65").FillVariableValue(DataDiemHKII);
                    // Fill dữ liệu điểm TBM học kỳ 2
                    DataDiemTBMII.Add("DiemTBMII", lstObjectDiemTBMII);
                    sheetAll.GetRange("CV7", "CV65").FillVariableValue(DataDiemTBMII);
                    // Fill du lieu xep loai hoc ky 2
                    DataXepLoaiII.Add("XepLoaiHKII", lstObjectXepLoaiII);
                    sheetAll.GetRange("CW7", "CW65").FillVariableValue(DataXepLoaiII);
                    // Fill du lieu diem TBM CN
                    DataDiemTBMCN.Add("DiemTBMCN", lstObjectDiemTBMCN);
                    sheetAll.GetRange("DD7", "DD65").FillVariableValue(DataDiemTBMCN);
                    // Fill du lieu xep loai CN
                    DataXepLoaiCN.Add("XepLoaiCN", lstObjectXepLoaiCN);
                    sheetAll.GetRange("DE7", "DE65").FillVariableValue(DataXepLoaiCN);
                }
            }
            #endregion
            else
            {
                // Lấy dữ liệu sơ yếu lý lịch
                Dictionary<string, object> dataSYLL = new Dictionary<string, object>();
                dataSYLL["FullName"] = "";
                dataSYLL["SchoolName"] = "";
                dataSYLL["BirthDate"] = "";
                dataSYLL["BirthPlace"] = "";
                lstObjectSYLL.Add(dataSYLL);
                DataDicSYLL.Add("SYLL", lstObjectSYLL);
                sheetAll.GetRange("Q7", "Z65").FillVariableValue(DataDicSYLL);
                Dictionary<string, object> dataDiemHKI = new Dictionary<string, object>();
                dataDiemHKI["KTHKI"] = "";
                lstObjectDiemHKI.Add(dataDiemHKI);
                DataDiemHKI1.Add("DiemHKI", lstObjectDiemHKI);
                sheetAll.GetRange("CH7", "CH65").FillVariableValue(DataDiemHKI);
                Dictionary<string, object> dataDiemHKII = new Dictionary<string, object>();
                dataDiemHKII["KTHKII"] = "";
                lstObjectDiemHKII.Add(dataDiemHKII);
                DataDiemHKII.Add("DiemHKII", lstObjectDiemHKII);
                sheetAll.GetRange("CU7", "CU65").FillVariableValue(DataDiemHKII);
                Dictionary<string, object> dataDiemTBMI = new Dictionary<string, object>();
                dataDiemTBMI["TBMI"] = "";
                lstObjectDiemTBMI.Add(dataDiemTBMI);
                DataDiemTBMI1.Add("DiemTBMI", lstObjectDiemTBMI);
                sheetAll.GetRange("CI7", "CI65").FillVariableValue(DataDiemTBMI);
                Dictionary<string, object> dataDiemTBMII = new Dictionary<string, object>();
                dataDiemTBMII["TBMII"] = "";
                lstObjectDiemTBMII.Add(dataDiemTBMII);
                DataDiemTBMII.Add("DiemTBMII", lstObjectDiemTBMII);
                sheetAll.GetRange("CV7", "CV65").FillVariableValue(DataDiemTBMII);
            }
            #endregion
            return oBook.ToStream();
        }

        #region CheckValidate

        public bool CheckValidatePupilCode(string PupilCode, int ApprenticeshipClassID)
        {
            if (ApprenticeshipTrainingBusiness.All.Where(o => o.ApprenticeShipClassID == ApprenticeshipClassID && o.PupilCode == PupilCode).Count() > 0)
            {
                return true;
            }
            else
                return false;
        }
        public bool CheckValidateName(string Name, int ApprenticeshipClassID)
        {
            if (ApprenticeshipTrainingBusiness.All.Where(o => o.ApprenticeShipClassID == ApprenticeshipClassID && o.PupilProfile.FullName == Name).Count() > 0)
            {
                return true;
            }
            else
                return false;
        }

        #endregion

        // Chuyển tên cột sang kiểu int
        public int ColumnNumber(string colAdress)
        {
            int[] digits = new int[colAdress.Length];
            for (int i = 0; i < colAdress.Length; ++i)
            {
                digits[i] = Convert.ToInt32(colAdress[i]) - 64;
            }
            int mul = 1; int res = 0;
            for (int pos = digits.Length - 1; pos >= 0; --pos)
            {
                res += digits[pos] * mul;
                mul *= 26;
            }
            return res;
        }

        // Lấy danh sách học sinh bao gồm tất cả các trạng thái
        public IQueryable<ApprenticeshipTraining> GetListPupilNoStatus(int schoolId, int academicYearId, int apprenticeshipClassId)
        {
            IQueryable<ApprenticeshipTraining> lsApprenticeshipTraining = from p in ApprenticeshipTrainingRepository.All
                                                                          where p.SchoolID == schoolId && p.AcademicYearID == academicYearId && p.ApprenticeShipClassID == apprenticeshipClassId
                                                                          select p;
            return (lsApprenticeshipTraining);
        }
    }
}