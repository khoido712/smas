/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class SMSTeacherContractBusiness
    {
        public List<SMSTeacherContractBO> GetSMSTeacherContract(SMSTeacherContractBO SearchObject)
        {
            var query = from contract in SMSTeacherContractRepository.All
                        join package in PackageRepository.All on contract.PackageID equals package.PackageID
                        select new SMSTeacherContractBO
                        {
                            ContractCode = contract.ContractCode,
                            SchoolCode = contract.SchoolCode,
                            PackageID = contract.PackageID,
                            PackageCode = package.PackageCode,
                            SupervisingDeptCode = contract.SupervisingDeptCode,
                            Status = contract.Status,
                            MaxSMS = contract.MaxSMS.HasValue ? contract.MaxSMS.Value : 0,

                        };
            if (SearchObject.ContractCode != null && SearchObject.ContractCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.ContractCode.ToLower().Contains(SearchObject.ContractCode.ToLower())));
            }
            if (SearchObject.SchoolCode != null && SearchObject.SchoolCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.SchoolCode.ToLower().Contains(SearchObject.SchoolCode.ToLower())));
            }
            if (SearchObject.PackageCode != null && SearchObject.PackageCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.PackageCode.ToLower().Contains(SearchObject.PackageCode.ToLower())));
            }
            if (SearchObject.SupervisingDeptCode != null && SearchObject.SupervisingDeptCode.Trim().Length != 0)
            {
                query = query.Where(m => (m.SupervisingDeptCode.ToLower().Contains(SearchObject.SupervisingDeptCode.ToLower())));
            }
            if (SearchObject.Status != -1)
            {
                query = query.Where(m => (m.Status == SearchObject.Status));
            }
            if (SearchObject.MaxSMS != -1)
            {
                query = query.Where(m => (m.MaxSMS == SearchObject.MaxSMS));
            }

            return query.ToList();
        }        
    }
}