﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class PupilRepeatedBusiness
    {
        public const int MAX_EDUCATIONLEVEL_PRIMARY = 5;
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/01/2013   11:35 AM
        /// </remarks>
        public void Insert(PupilRepeated entity)
        {
            //PupilID: FK(PupilProfile)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                        new Dictionary<string, object>()
                {
                    {"PupilProfileID",entity.PupilID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> { "InfectiousDiseas_Label_PupilID" });
                }
            }

            //SchoolID: FK(SchoolProfile)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                        new Dictionary<string, object>()
                {
                    {"SchoolProfileID",entity.SchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> { "InfectiousDiseas_Label_SchoolID" });
                }
            }

            //OldClassID, RepeatedClassID: FK(ClassProfile)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.OldClassID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> { "InfectiousDiseas_Label_Class" });
                }
            }
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.RepeatedClassID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> { "InfectiousDiseas_Label_Class" });
                }
            }
            //AcademicYearID: FK(AcademicYearID)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                        new Dictionary<string, object>()
                {
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object>() { "InfectiousDiseas_Label_AcademicYear" });
                }
            }
            //EducationLevelID: FK(EducationLevel)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel",
                        new Dictionary<string, object>()
                {
                    {"EducationLevelID",entity.EducationLevelID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object>() { "InfectiousDiseas_Label_EducationLevel" });
                }
            }
            //Ngày chuy?n không thu?c h?c ḱ 1 c?a nam h?c m?i. Ki?m tra RepeatedDate
            AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
            Utils.ValidateDateInsideDateRange(entity.RepeatedDate, ay.FirstSemesterStartDate.Value, ay.FirstSemesterEndDate.Value, "PupilRepeated_Label_RepeatedDate");

            //PupilProfile
            var pp = PupilProfileBusiness.Find(entity.PupilID);
            pp.CurrentAcademicYearID = entity.AcademicYearID;
            pp.CurrentClassID = entity.RepeatedClassID;
            PupilProfileBusiness.Update(pp);


            //PupilOfClass
            PupilOfClass poc = new PupilOfClass();
            poc.AcademicYearID = entity.AcademicYearID;
            poc.AssignedDate = DateTime.Now;
            poc.ClassID = entity.RepeatedClassID;
            poc.PupilID = entity.PupilID;
            poc.SchoolID = entity.SchoolID;
            poc.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
            poc.Year = ay.Year;
            PupilOfClassBusiness.Insert(poc);

            PupilOfSchool pos = new PupilOfSchool();
            pos.EnrolmentDate = DateTime.Now;
            pos.PupilID = entity.PupilID;
            pos.SchoolID = entity.SchoolID;
            PupilOfSchoolBusiness.Insert(pos);

            base.Insert(entity);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="PupilRepeatedID">The pupil repeated ID.</param>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/01/2013   11:35 AM      /// </remarks>
        public void Delete(int SchoolID, int PupilRepeatedID)
        {
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PupilRepeated",
                        new Dictionary<string, object>()
                {
                    {"PupilRepeatedID",PupilRepeatedID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object>() { "PupilRepeated_Label_PupilRepeatedID" });
                }
            }

            PupilRepeated pr = repository.Find(PupilRepeatedID);
            //PupilRepeated.PupilID, SchoolID  not compatible(PupilOfClass)
            {
                bool Compatible = repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilOfClass",
                        new Dictionary<string, object>()
                {
                    {"PupilID",pr.PupilID},
                    {"SchoolID",SchoolID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible", new List<object>() { "InfectiousDiseas_Label_SchoolID", "InfectiousDiseas_Label_PupilID" });
                }
            }

            //PupilProfile
            AcademicYear currentAY = AcademicYearBusiness.Find(pr.AcademicYearID);
            AcademicYear oldAY = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentAY.Year - 1 } }).FirstOrDefault();

            if (oldAY == null)
            {
                throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
            }
            var pp = PupilProfileBusiness.Find(pr.PupilID);
            pp.CurrentAcademicYearID = oldAY.AcademicYearID;
            pp.CurrentClassID = pr.OldClassID;
            PupilProfileBusiness.Update(pp);

            //
            base.Delete(PupilRepeatedID);
            //PupilOfClass
            PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"PupilID",pr.PupilID}
                ,{"ClassID",pr.RepeatedClassID}
                ,{"AcademicYearID",pr.AcademicYearID}
                ,{"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
            }).FirstOrDefault();
            if (poc != null)
            {
                PupilOfClassBusiness.DeletePupilOfClass(new List<PupilOfClass>() { poc });
            }

            PupilOfSchool pos = PupilOfSchoolBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"PupilID",pr.PupilID}
                ,{"AcademicYearID",pr.AcademicYearID}
            }).FirstOrDefault();
            if (pos != null)
            {
                PupilOfSchoolBusiness.DeletePupilOfSchool(pos.PupilOfSchoolID, SchoolID);
            }



        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/01/2013   11:35 AM
        /// </remarks>
        public IQueryable<PupilRepeated> Search(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int OldClassID = Utils.GetInt(dic, "OldClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int RepeatedClassID = Utils.GetInt(dic, "RepeatedClassID");
            var lsPR = PupilRepeatedRepository.All;

            if (AcademicYearID != 0)
            {
                lsPR = lsPR.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsPR = lsPR.Where(o => o.SchoolID == SchoolID);
            }
            if (OldClassID != 0)
            {
                lsPR = lsPR.Where(o => o.OldClassID == OldClassID);
            }
            if (EducationLevelID != 0)
            {
                lsPR = lsPR.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (RepeatedClassID != 0)
            {
                lsPR = lsPR.Where(o => o.RepeatedClassID == RepeatedClassID);
            }
            return lsPR;
        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/01/2013   11:35 AM
        /// </remarks>
        public IQueryable<PupilRepeated> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>() { };
            }
            dic["SchoolID"] = SchoolID;
            return Search(dic);

        }

        /// <summary>
        /// GetPupilRepeated
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/01/2013   11:35 AM
        /// </remarks>
        public List<PupilRepeatedBO> GetPupilRepeated(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID)
        {
            int currentYear = AcademicYearBusiness.All.Where(o => o.AcademicYearID == AcademicYearID).Select(o => o.Year).First();
            var oldAY = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentYear - 1 } })
                .Select(o => new { o.AcademicYearID, o.Year})
                .FirstOrDefault();

            if (oldAY == null)
            {
                throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
                //oldAY = currentAY;
            }
           
            List<int> lstOldClass = new List<int>();
            if (ClassID == 0)
            {
                lstOldClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
              {
                  {"AcademicYearID",oldAY.AcademicYearID}
                  ,{"EducationLevelID",EducationLevelID}
              }).Select(o => o.ClassProfileID).ToList();
            }
            else
            {
                lstOldClass.Add(ClassID);
            }

            //anhnph1_s?a k?t chuy?n luu ban cho c?p 1
            //n?u c?p 2 & 3 v?n y lu?ng cu
            //n?u c?p 1: h?c sinh ? l?i khi dánh giá b? sung HK II/CN là CHT
            #region code cu
            //var lsPupilRanking = VPupilRankingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            //  {
            //      {"AcademicYearID",oldAY.AcademicYearID}
            //      ,{"Check","Check"}
            //  }).Where(o => o.PeriodID == null).Where(o => o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
            // .Where(o => lstOldClass.Contains(o.ClassID));
            #endregion
            int? createdAcaYear = oldAY.Year;
            List<PupilTransferBO> lsPupilRanking = null;
            if (EducationLevelID > MAX_EDUCATIONLEVEL_PRIMARY)
            {
                lsPupilRanking = (from prr in VPupilRankingBusiness.All
                                  join cp in ClassProfileBusiness.All on prr.ClassID equals cp.ClassProfileID
                                  where prr.Last2digitNumberSchool == SchoolID % 100
                                  && prr.CreatedAcademicYear == createdAcaYear
                                  && prr.SchoolID == SchoolID
                                  && prr.AcademicYearID == oldAY.AcademicYearID
                                  && prr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS
                                  && prr.PeriodID == null
                                  && lstOldClass.Contains(prr.ClassID)
                                  && (cp.IsVnenClass == null || cp.IsVnenClass == false)
                                  && cp.IsActive.Value
                                  select new PupilTransferBO()
                                  {
                                      PupilID = prr.PupilID,
                                      ClassID = prr.ClassID,
                                      Semester = prr.Semester
                                  }).ToList();

                //viethd4: bo sung cac lop VNEN
                List<PupilTransferBO> lstPupilRankingVNEN = (from rb in ReviewBookPupilBusiness.All
                                                             join cp in ClassProfileBusiness.All on rb.ClassID equals cp.ClassProfileID
                                                             where rb.SchoolID == SchoolID
                                                             && rb.AcademicYearID == oldAY.AcademicYearID
                                                             && (rb.RateAndYear == 2 && rb.RateAdd == 2)
                                                             && rb.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                             && lstOldClass.Contains(rb.ClassID)
                                                             && (cp.IsVnenClass == true)
                                                             && cp.IsActive.Value
                                                             select new PupilTransferBO()
                                                             {
                                                                 PupilID = rb.PupilID,
                                                                 ClassID = rb.ClassID,
                                                                 Semester = rb.SemesterID
                                                             }).ToList();

                lsPupilRanking = lsPupilRanking.Union(lstPupilRankingVNEN).ToList();
            }
            else
            {
                lsPupilRanking = (from sde in SummedEndingEvaluationBusiness.All
                               join cp in ClassProfileBusiness.All on sde.ClassID equals cp.ClassProfileID
                               where sde.LastDigitSchoolID == SchoolID % 100
                               && sde.SchoolID == SchoolID
                               && sde.AcademicYearID == oldAY.AcademicYearID
                               && ((sde.EndingEvaluation != null && "CHT".Equals(sde.EndingEvaluation))
                                         || (sde.EndingEvaluation != null && "CHT".Equals(sde.EndingEvaluation) && sde.RateAdd != null
                                         && sde.RateAdd == SystemParamsInFile.TT22_SUMMEDENDING_RATE_ADD_CHT))
                               && lstOldClass.Contains(sde.ClassID)
                               && cp.IsActive.Value
                               select new PupilTransferBO()
                               {
                                   PupilID = sde.PupilID,
                                   ClassID = sde.ClassID,
                                   Semester = sde.SemesterID
                               }).ToList();
            }


            var lsPupilRanking2 = lsPupilRanking.Where(x => x.Semester == lsPupilRanking.Where(o => o.PupilID == x.PupilID).Max(o => o.Semester));

            var lsPoc = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                {"AcademicYearID",oldAY.AcademicYearID}
                ,{"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
               // }).Where(o => lstPupilRanking.Contains(o.PupilID)).ToList();
                }).Where(o => lstOldClass.Contains(o.ClassID)).Select(o => o.PupilID).ToList();

            lsPupilRanking2 = lsPupilRanking2.Where(o => lsPoc.Contains(o.PupilID));

            var allPupilRepeated = this.All.Where(o => o.SchoolID == SchoolID).Where(o => o.AcademicYearID == AcademicYearID).Where(o => o.EducationLevelID == EducationLevelID);
            if (ClassID != 0)
            {
                allPupilRepeated = allPupilRepeated.Where(o => o.OldClassID == ClassID);
            }

            List<int> lstPupilID = lsPupilRanking2.Select(o => o.PupilID).ToList();
            List<int> lstClassID = lsPupilRanking2.Select(o => o.ClassID).ToList();
            var query = from poc in PupilOfClassBusiness.All.Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)       
                        join pp in PupilProfileBusiness.All
                        on poc.PupilID equals pp.PupilProfileID
                        join apr in allPupilRepeated
                        on poc.PupilID equals apr.PupilID
                        into g1
                        from o in g1.DefaultIfEmpty()
                        where (o == null || ClassID == 0 || (o.OldClassID == ClassID && o.SchoolID == SchoolID))
                        && lstPupilID.Contains(poc.PupilID) && lstClassID.Contains(poc.ClassID)
                        select new PupilRepeatedBO
                        {
                            AcademicYearID = AcademicYearID,
                            BirthDate = pp.BirthDate,
                            EducationLevelID = EducationLevelID,
                            FullName = pp.FullName,
                            Genre = pp.Genre,
                            GenreName = (pp.Genre == (int)SystemParamsInFile.GENRE_MALE) ? SystemParamsInFile.REPORT_LOOKUPINFO_MALE : SystemParamsInFile.REPORT_LOOKUPINFO_FEMALE,
                            OldClassID = poc.ClassID,
                            OldClassName = poc.ClassProfile.DisplayName,
                            PupilCode = pp.PupilCode,
                            PupilID = poc.PupilID,
                            RepeatedClassID = o != null ? o.RepeatedClassID : 0,
                            SchoolID = SchoolID,
                            Year = oldAY.Year,
                            Name = pp.Name,
                            OrderInClass = poc.OrderInClass,
                            PupilRepeatedID = o != null ? o.PupilRepeatedID : 0,
                            PupilProfile = pp,
                            PupilOfClass = poc,
                            PupilRepeated = o
                        };
            return query.ToList();
        }
        //K?t chuy?n luu ban
        //lstPupilID va lstClassRepeatedID tuong ung voi nhau
        public void RepeatedPupil(List<PupilRepeatedBO> lstPupilRepeatedBO, int AcademicYearID, int SchoolID, int EducationLevelID)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
            int currentYear = AcademicYearBusiness.All.Where(o => o.AcademicYearID == AcademicYearID).Select(o => o.Year).First();
            int? oldAYID = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentYear - 1 } })
                .Select(o => o.AcademicYearID).FirstOrDefault();
            List<int> lstPupilProfileID = lstPupilRepeatedBO.Select(o => o.PupilID).Distinct().ToList();
            List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilProfileID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = oldAYID;
            List<PupilOfSchool> lstPupilOfSchoolOld = PupilOfSchoolBusiness.SearchBySchool(SchoolID, dic)
                                                      .Where(o => lstPupilProfileID.Contains(o.PupilID)).ToList();
            foreach (var rpBO in lstPupilRepeatedBO)
            {
                //Insert vao PupilOfClass
                PupilOfClass poc2 = this.PupilOfClassBusiness.All.FirstOrDefault(poc => poc.AcademicYearID == AcademicYearID
                                                                                        && poc.ClassID == rpBO.RepeatedClassID
                                                                                        && poc.PupilID == rpBO.PupilID);
                bool insertPupilIntoNewClass;
                if (poc2 == null)
                {
                    poc2 = new PupilOfClass();
                    insertPupilIntoNewClass = true;
                }
                else
                {
                    insertPupilIntoNewClass = false;
                }
                poc2.ClassID = rpBO.RepeatedClassID;
                poc2.AcademicYearID = AcademicYearID;
                poc2.SchoolID = SchoolID;
                poc2.Year = currentYear;
                poc2.AssignedDate = DateTime.Now;
                poc2.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                poc2.PupilID = rpBO.PupilID;
                if (insertPupilIntoNewClass)
                {
                    this.PupilOfClassBusiness.Insert(poc2);
                }
                else
                {
                    this.PupilOfClassBusiness.Update(poc2);
                }
                //PupilProfile
                PupilProfile pp = lstPupilProfile.Where(o => o.PupilProfileID == rpBO.PupilID).FirstOrDefault();
                pp.CurrentClassID = rpBO.RepeatedClassID;
                pp.CurrentAcademicYearID = AcademicYearID;
                PupilProfileBusiness.Update(pp);
                

                //Insert vao PupilRepeated
                PupilRepeated pupilRepeated = new PupilRepeated();
                pupilRepeated.AcademicYearID = AcademicYearID;
                pupilRepeated.EducationLevelID = EducationLevelID;
                pupilRepeated.OldClassID = rpBO.OldClassID;
                pupilRepeated.PupilID = rpBO.PupilID;
                pupilRepeated.RepeatedClassID = rpBO.RepeatedClassID;
                pupilRepeated.SchoolID = SchoolID;
                pupilRepeated.RepeatedDate = DateTime.Now;
                base.Insert(pupilRepeated);
            }
                this.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public void CancelRepeatedPupil(List<PupilRepeatedBO> lstPupilRepeatedBO, int AcademicYearID, int SchoolID, int EducationLevelID)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
            int currentYear = AcademicYearBusiness.All.Where(o => o.AcademicYearID == AcademicYearID).Select(o => o.Year).First();
            AcademicYear oldAY = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentYear - 1 } }).FirstOrDefault();

            List<int> lstPupilProfileID = lstPupilRepeatedBO.Select(o => o.PupilID).Distinct().ToList();
                //List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilProfileID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
            List<int> lstClassID = lstPupilRepeatedBO.Select(o => o.RepeatedClassID).Distinct().ToList();
            //tim trong PupilOfClass
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["Year"] = currentYear; //AnhVD 20131218
            var hasMr = MarkRecordBusiness.SearchBySchool(SchoolID, dic)
                                .Where(o => lstPupilProfileID.Contains(o.PupilID)).Count() > 0;
                var hasJr = VJudgeRecordBusiness.SearchBySchool(SchoolID, dic)
                                .Where(o => lstPupilProfileID.Contains(o.PupilID)).Count() > 0;
            var hasPr = VPupilRankingBusiness.SearchBySchool(SchoolID, dic)
                                .Where(o => lstPupilProfileID.Contains(o.PupilID)).Count() > 0;
            if (hasMr || hasJr || hasPr)
            {
                throw new BusinessException("TransferData_Label_NewClassHasMark");
            }
            var pupilOfClassAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                                  .Where(o => lstPupilProfileID.Contains(o.PupilID)).ToList();
            //var pupilOfSchoolAll = PupilOfSchoolBusiness.SearchBySchool(SchoolID, dic)
                //                      .Where(o => lstPupilProfileID.Contains(o.PupilID)).ToList();*/
            var pupilRepeatedAll = this.SearchBySchool(SchoolID, dic)
                                   .Where(o => lstPupilProfileID.Contains(o.PupilID)).ToList();
                PupilProfile pp = null;

                if (pupilOfClassAll.Count > 0)
                {
                    PupilOfClassBusiness.DeleteAll(pupilOfClassAll);
                }

                if (pupilRepeatedAll.Count > 0)
                {
                    this.DeleteAll(pupilRepeatedAll);
                }
            foreach (var rpBO in lstPupilRepeatedBO)
            {
                    pp = rpBO.PupilProfile;//lstPupilProfile.Where(o => o.PupilProfileID == rpBO.PupilID).FirstOrDefault();
                pp.CurrentClassID = rpBO.OldClassID;
                pp.CurrentAcademicYearID = oldAY.AcademicYearID;
                PupilProfileBusiness.Update(pp);
                    /*PupilOfClassBusiness.Delete(rpBO.PupilOfClass.PupilOfClassID);
                    if (rpBO.PupilRepeated != null)
            {
                        PupilRepeatedBusiness.Delete(rpBO.PupilRepeated.PupilRepeatedID);
                    }*/
            }

                this.SaveDetect();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
    }
}
