﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ReportResultEndOfYearBusiness
    {


        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// GetHashKey
        /// </summary>
        /// <param name="TranscriptOfClass">The transcript of class.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>hath</author>
        /// <date>11/27/2012</date>
        public string GetHashKey(TranscriptOfClass TranscriptOfClass)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = TranscriptOfClass.AcademicYearID;
            Dictionary["Semester"] = TranscriptOfClass.Semester;
            Dictionary["AppliedLevel"] = TranscriptOfClass.AppliedLevel;
            return ReportUtils.GetHashKey(Dictionary);
        }
        #endregion

        #region Lưu lại thông tin thống kê
        /// <summary>
        /// InsertResultEndOfYear
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>hath</author>
        /// <date>11/27/2012</date>
        public ProcessedReport InsertResultEndOfYear(TranscriptOfClass entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_KET_QUA_CUOI_NAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;



            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"PeriodDeclarationID", entity.PeriodDeclarationID},
                {"SubjectID", entity.SubjectID},

            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo thống kê kết quả cuối năm mới nhất
        /// <summary>
        /// GetReportResultEndOfYear
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>hath</author>
        /// <date>11/27/2012</date>
        public ProcessedReport GetReportResultEndOfYear(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_KET_QUA_CUOI_NAM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin danh sách học sinh thi lại
        /// <summary>
        /// Lưu lại thông tin danh sách học sinh thi lại
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportResultEndOfYear(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_KET_QUA_CUOI_NAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            int firstRow = 10;
            int lastCol = 12;

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "L" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 2, lastCol), "A" + firstRow);

            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = academicYear.SchoolProfile;
            SubjectCat subject = SubjectCatBusiness.Find(entity.SubjectID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"AcademicYear", academicYearTitle}
                };

            tempSheet.GetRange(1, 1, 6, lastCol).FillVariableValue(dicGeneralInfo);
            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);
            IVTRange allSchoolRange = firstSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            IVTRange toprange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange midrange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);

            //B1. Lấy ra danh sách các khối học thuộc cấp học 
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();

            List<int> listBeginRow = new List<int>();
            List<int> listTC = new List<int>();
            int beginRow = firstRow;
            //Danh sach lop hoc (dùng chung)
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPOC = new Dictionary<string, object> 
                {
                {"AcademicYearID", entity.AcademicYearID},
                    {"SchoolID",entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPOC);
            IQueryable<PupilOfClassBO> lstPoc = from p in lstQPoc.AddCriteriaSemester(Aca, entity.Semester)
                                                join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                                                where r.IsActive.Value
                                                select new PupilOfClassBO
                                                {
                                                    PupilID = p.PupilID,
                                                    ClassID = p.ClassID,
                                                    EducationLevelID = r.EducationLevelID,
                                                    Genre = q.Genre,
                                                    Status = p.Status,
                                                    EthnicID = q.EthnicID,
                                                    IsVNEN = r.IsVnenClass
                };
            
            #region Lay danh sach nhan xet cua cac lop VNEN (dùng chung)
            List<ReviewBookPupilBO> listReviewPupil = new List<ReviewBookPupilBO>();
            List<int> listClassIdVNEN = lstPoc.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value == true).Select(p => p.ClassID).Distinct().ToList();
            if (listClassIdVNEN != null && listClassIdVNEN.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", entity.AcademicYearID);
                dic.Add("lstClassID", listClassIdVNEN);
                dic.Add("SemesterID", entity.Semester);
                dic.Add("SchoolID", entity.SchoolID);
                dic.Add("IsGetStudingAndGraduate", true);
                listReviewPupil = ReviewBookPupilBusiness.GetListReviewPupil(dic).ToList();
            }
            #endregion

            //Dung chung
            Dictionary<string, object> dicRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", entity.AppliedLevel},
                {"Year", academicYear.Year}
            };
            IQueryable<VPupilRanking> listQRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dicRanking);
            var lstRanking = (from pr in listQRanking
                              join q in PupilProfileBusiness.All on pr.PupilID equals q.PupilProfileID
                              join r in ClassProfileBusiness.All on pr.ClassID equals r.ClassProfileID
                              where lstPoc.Any(o => o.PupilID == pr.PupilID && o.ClassID == pr.ClassID)
                              && r.IsActive.Value
                              select new
                              {
                                  PupilID = pr.PupilID,
                                  ClassID = pr.ClassID,
                                  Semester = pr.Semester,
                                  StudyingJudgementID = pr.StudyingJudgementID,
                                  ConductLevelID = pr.ConductLevelID,
                                  Genre = q.Genre,
                                  EducationLevelID = r.EducationLevelID,
                                  EthnicID = q.EthnicID
                              }).ToList();

            var lstRanking_Temp = lstRanking.Where(u => u.Semester == lstRanking.Where(v => v.Semester.Value >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL 
                                && v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester));

            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            var listCountCurentPupilByClass = lstPoc.GroupBy(o => new { o.EducationLevelID, o.ClassID,o.Status, o.Genre, o.EthnicID }).Select(o => new { EducationLevelID = o.Key.EducationLevelID, ClassID = o.Key.ClassID,Status = o.Key.Status, Genre = o.Key.Genre, EthnicID = o.Key.EthnicID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_Female = listCountCurentPupilByClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var listCountCurentPupilByClass_Ethnic = listCountCurentPupilByClass.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            var listCountCurentPupilByClass_FemaleEthnic = listCountCurentPupilByClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            var lstRanking_Ethnic = lstRanking.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            var lstRanking_Female = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var lstRanking_FemaleEthnic = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();

            Dictionary<string, object> dicClass = new Dictionary<string, object> 
                                                        {
                                                            {"AcademicYearID", entity.AcademicYearID}, 
                                                            {"SchoolID", entity.SchoolID} ,
                                                            {"AppliedLevel", entity.AppliedLevel}
                                                        };
            List<ClassProfileTempBO> listAllClass = this.ClassProfileBusiness.SearchBySchool(entity.SchoolID, dicClass)
              .Select(o => new ClassProfileTempBO
              {
                  ClassProfileID = o.ClassProfileID,
                  EducationLevelID = o.EducationLevelID,
                  OrderNumber = o.OrderNumber,
                  SchoolID = o.SchoolID,
                  AcademicYearID = o.AcademicYearID,
                  DisplayName = o.DisplayName,
                  EmployeeID = o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0,
                  EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty,
                  isVNEN = o.IsVnenClass
              })
              .ToList();

            int EducationLevelID = 0;
            #region Tạo sheet Fill dữ liệu dau tien
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow, lastCol).ToString());
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                EducationLevelID = educationLevel.EducationLevelID;
                if (EducationLevelID == 5 || EducationLevelID == 9 || EducationLevelID == 12)
                {
                    continue;
                }
                List<object> listData = new List<object>();
                int endRow = beginRow;
                listBeginRow.Add(beginRow);
                List<ClassProfileTempBO> listCP = listAllClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID)
                                        .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                if (listCP == null || listCP.Count == 0)
                {
                    dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    endRow = beginRow - 1;
                    listTC.Add(endRow + 1);
                    dataSheet.SetCellValue(beginRow, 1, educationLevel.Resolution);
                }
                else
                {
                    endRow = beginRow + listCP.Count - 1;
                    listTC.Add(endRow + 1);
                    bool isClassENVN = false;
                    foreach (ClassProfileTempBO cp in listCP)
                    {
                        isClassENVN = cp.isVNEN.HasValue ? cp.isVNEN.Value : false;

                        #region Ve khung fill cong thuc
                        for (int i = beginRow; i <= endRow; i++)
                        {
                            IVTRange copyRange;
                            if (i == beginRow)
                            {
                                copyRange = toprange;
                            }
                            else if (i == endRow)
                            {
                                copyRange = midrange;
                            }
                            else
                            {
                                copyRange = midrange;
                            }
                            dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                            //Tính các cột %
                            string fomular = "=IF(D" + i + "=0;0;ROUND(E" + i + "/D" + i + ";4)*100)";
                            dataSheet.SetFormulaValue("F" + i, fomular);
                            fomular = "=IF(D" + i + "=0;0;ROUND(G" + i + "/D" + i + ";4)*100)";
                            dataSheet.SetFormulaValue("H" + i, fomular);
                            fomular = "=IF(D" + i + "=0;0;ROUND(I" + i + "/D" + i + ";4)*100)";
                            dataSheet.SetFormulaValue("J" + i, fomular);
                            fomular = "=IF(D" + i + "=0;0;ROUND(K" + i + "/D" + i + ";4)*100)";
                            dataSheet.SetFormulaValue("L" + i, fomular);
                        }
                        #endregion

                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["ClassName"] = cp.DisplayName;
                        var objCountPupilSS = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID);
                        //SS
                        dicData["NumberOfPupil"] = objCountPupilSS != null ? objCountPupilSS.Sum(p => p.TotalPupil) : 0;

                        //SS thuc te
                        var objCountPupilSSTT = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID && (p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || p.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED));
                        dicData["RealNumberOfPupil"] = objCountPupilSSTT != null ? objCountPupilSSTT.Sum(p => p.TotalPupil) : 0;


                        //Thi Lại
                        dicData["ReTest"] = lstRanking.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).Count();

                        //Rèn luyện trong hè
                        dicData["BackTraining"] = lstRanking.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).Count();

                        if (isClassENVN)
                        {
                            #region Neu la lop ENVN
                            //Lên lớp                            
                            dicData["UpClass"] = listReviewPupil.Where(o => o.ClassID == cp.ClassProfileID && ((o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT) || (o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))).Count();

                            //Ở lại
                            dicData["StayClass"] = listReviewPupil.Where(o => o.ClassID == cp.ClassProfileID && (o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT && o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT)).Count();
                            #endregion
                        }
                        else
                        {
                            #region Khong phai lop ENVN
                            //Lên lớp
                            dicData["UpClass"] = lstRanking_Temp.Where(o => o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS).Count();

                            //Ở lại
                            dicData["StayClass"] = lstRanking_Temp.Where(o => o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS).Count();
                            #endregion
                        }

                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet.GetRange(beginRow, 1, endRow, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });

                    //Merg cot
                    dataSheet.SetCellValue(beginRow, 1, educationLevel.Resolution);
                    dataSheet.MergeColumn(1, beginRow, endRow + 1);
                }
                //Tạo cột Tổng số của từng khối
                dataSheet.CopyPasteSameSize(summarizeRange, endRow + 1, 1);
                dataSheet.SetCellValue(endRow + 1, 1, educationLevel.Resolution);
                dataSheet.SetCellValue(endRow + 1, 2, "TC");
                dataSheet.SetFormulaValue(endRow + 1, 3, "=SUM(C" + beginRow + ":C" + endRow + ")");
                dataSheet.SetFormulaValue(endRow + 1, 4, "=SUM(D" + beginRow + ":D" + endRow + ")");
                dataSheet.SetFormulaValue(endRow + 1, 5, "=SUM(E" + beginRow + ":E" + endRow + ")");
                dataSheet.SetFormulaValue(endRow + 1, 7, "=SUM(G" + beginRow + ":G" + endRow + ")");
                dataSheet.SetFormulaValue(endRow + 1, 9, "=SUM(I" + beginRow + ":I" + endRow + ")");
                dataSheet.SetFormulaValue(endRow + 1, 11, "=SUM(K" + beginRow + ":K" + endRow + ")");
                int k = endRow + 1;
                string fomular1 = "=IF(D" + k + "=0;0;ROUND(E" + k + "/D" + k + ";4)*100)";
                dataSheet.SetFormulaValue("F" + k, fomular1);
                fomular1 = "=IF(D" + k + "=0;0;ROUND(G" + k + "/D" + k + ";4)*100)";
                dataSheet.SetFormulaValue("H" + k, fomular1);
                fomular1 = "=IF(D" + k + "=0;0;ROUND(I" + k + "/D" + k + ";4)*100)";
                dataSheet.SetFormulaValue("J" + k, fomular1);
                fomular1 = "=IF(D" + k + "=0;0;ROUND(K" + k + "/D" + k + ";4)*100)";
                dataSheet.SetFormulaValue("L" + k, fomular1);
                //Tính vị trí tiếp theo
                beginRow = endRow + 2;
            }
            //Cột toàn trường
            dataSheet.CopyPasteSameSize(allSchoolRange, beginRow, 1);
            string formular = string.Join("+", listTC.Select(u => "$$$" + u));

            dataSheet.SetFormulaValue(beginRow, 3, "=" + formular.Replace("$$$", "C"));
            dataSheet.SetFormulaValue(beginRow, 4, "=" + formular.Replace("$$$", "D"));
            dataSheet.SetFormulaValue(beginRow, 5, "=" + formular.Replace("$$$", "E"));
            dataSheet.SetFormulaValue(beginRow, 6, "=IF(D" + beginRow + "=0;0;ROUND(E" + beginRow + "/D" + beginRow + ";4)*100)");
            dataSheet.SetFormulaValue(beginRow, 7, "=" + formular.Replace("$$$", "G"));
            dataSheet.SetFormulaValue(beginRow, 8, "=IF(D" + beginRow + "=0;0;ROUND(G" + beginRow + "/D" + beginRow + ";4)*100)");
            dataSheet.SetFormulaValue(beginRow, 9, "=" + formular.Replace("$$$", "I"));
            dataSheet.SetFormulaValue(beginRow, 10, "=IF(D" + beginRow + "=0;0;ROUND(I" + beginRow + "/D" + beginRow + ";4)*100)");
            dataSheet.SetFormulaValue(beginRow, 11, "=" + formular.Replace("$$$", "K"));
            dataSheet.SetFormulaValue(beginRow, 12, "=IF(D" + beginRow + "=0;0;ROUND(K" + beginRow + "/D" + beginRow + ";4)*100)");
            dataSheet.Name = "HocSinhLenLop_OLai";
            dataSheet.PageSize = VTXPageSize.VTxlPaperA4;
            dataSheet.FitAllColumnsOnOnePage = true;
            #endregion

            #region sheet du lieu nu
            if (entity.FemaleID > 0)
            {
                firstRow = 10;
                lastCol = 12;
                beginRow = firstRow;
                IVTWorksheet dataSheetFemale = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow, lastCol).ToString());
                List<ReviewBookPupilBO> listReviewPupilFemale = listReviewPupil.Where(p => p.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                listTC = new List<int>();
                foreach (EducationLevel educationLevel in lstEducationLevel)
                {
                    EducationLevelID = educationLevel.EducationLevelID;
                    if (EducationLevelID == 5 || EducationLevelID == 9 || EducationLevelID == 12)
                    {
                        continue;
                    }
                    List<object> listData = new List<object>();
                    int endRow = beginRow;
                    listBeginRow.Add(beginRow);
                    List<ClassProfileTempBO> listCP = listAllClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID)
                                            .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_Female.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var listPupilRankingFemale_Edu = lstRanking_Female.Where(o => o.EducationLevelID == EducationLevelID);
                    if (listCP == null || listCP.Count == 0)
                    {
                        dataSheetFemale.CopyPasteSameSize(summarizeRange, beginRow, 1);
                        endRow = beginRow - 1;
                        listTC.Add(endRow + 1);
                        dataSheetFemale.SetCellValue(beginRow, 1, educationLevel.Resolution);
                    }
                    else
                    {
                        endRow = beginRow + listCP.Count - 1;
                        listTC.Add(endRow + 1);
                        bool isClassENVN = false;
                        foreach (ClassProfileTempBO cp in listCP)
                        {
                            isClassENVN = cp.isVNEN.HasValue ? cp.isVNEN.Value : false;

                            #region Ve khung fill cong thuc
                            for (int i = beginRow; i <= endRow; i++)
                            {
                                IVTRange copyRange;
                                if (i == beginRow)
                                {
                                    copyRange = toprange;
                                }
                                else if (i == endRow)
                                {
                                    copyRange = midrange;
                                }
                                else
                                {
                                    copyRange = midrange;
                                }
                                dataSheetFemale.CopyPasteSameSize(copyRange, "A" + i);
                                //Tính các cột %
                                string fomular = "=IF(D" + i + "=0;0;ROUND(E" + i + "/D" + i + ";4)*100)";
                                dataSheetFemale.SetFormulaValue("F" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(G" + i + "/D" + i + ";4)*100)";
                                dataSheetFemale.SetFormulaValue("H" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(I" + i + "/D" + i + ";4)*100)";
                                dataSheetFemale.SetFormulaValue("J" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(K" + i + "/D" + i + ";4)*100)";
                                dataSheetFemale.SetFormulaValue("L" + i, fomular);
                            }
                            #endregion

                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["ClassName"] = cp.DisplayName;
                            var objCountPupilSS = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID);
                            //SS
                            dicData["NumberOfPupil"] = objCountPupilSS != null ? objCountPupilSS.Sum(p => p.TotalPupil) : 0;

                            //SS thuc te
                            var objCountPupilSSTT = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID && (p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || p.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED));
                            dicData["RealNumberOfPupil"] = objCountPupilSSTT != null ? objCountPupilSSTT.Sum(p => p.TotalPupil) : 0;


                            //Thi Lại
                            dicData["ReTest"] = listPupilRankingFemale_Edu.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).Count();

                            //Rèn luyện trong hè
                            dicData["BackTraining"] = listPupilRankingFemale_Edu.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).Count();

                            if (isClassENVN)
                            {
                                #region Neu la lop ENVN
                                //Lên lớp                            
                                dicData["UpClass"] = listReviewPupil.Where(o => o.ClassID == cp.ClassProfileID && ((o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT) || (o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))).Count();

                                //Ở lại
                                dicData["StayClass"] = listReviewPupil.Where(o => o.ClassID == cp.ClassProfileID && (o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT && o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT)).Count();
                                #endregion
                            }
                            else
                            {
                                #region Khong phai lop ENVN
                                //Lên lớp
                                dicData["UpClass"] = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS).Count();

                                //Ở lại
                                dicData["StayClass"] = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS).Count();
                                #endregion
                            }

                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheetFemale.GetRange(beginRow, 1, endRow, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });

                        //Merg cot
                        dataSheetFemale.SetCellValue(beginRow, 1, educationLevel.Resolution);
                        dataSheetFemale.MergeColumn(1, beginRow, endRow + 1);
                    }


                    //Tạo cột Tổng số của từng khối
                    dataSheetFemale.CopyPasteSameSize(summarizeRange, endRow + 1, 1);
                    dataSheetFemale.SetCellValue(endRow + 1, 1, educationLevel.Resolution);
                    dataSheetFemale.SetCellValue(endRow + 1, 2, "TC");
                    dataSheetFemale.SetFormulaValue(endRow + 1, 3, "=SUM(C" + beginRow + ":C" + endRow + ")");
                    dataSheetFemale.SetFormulaValue(endRow + 1, 4, "=SUM(D" + beginRow + ":D" + endRow + ")");
                    dataSheetFemale.SetFormulaValue(endRow + 1, 5, "=SUM(E" + beginRow + ":E" + endRow + ")");
                    dataSheetFemale.SetFormulaValue(endRow + 1, 7, "=SUM(G" + beginRow + ":G" + endRow + ")");
                    dataSheetFemale.SetFormulaValue(endRow + 1, 9, "=SUM(I" + beginRow + ":I" + endRow + ")");
                    dataSheetFemale.SetFormulaValue(endRow + 1, 11, "=SUM(K" + beginRow + ":K" + endRow + ")");
                    int k = endRow + 1;
                    string fomular1 = "=IF(D" + k + "=0;0;ROUND(E" + k + "/D" + k + ";4)*100)";
                    dataSheetFemale.SetFormulaValue("F" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(G" + k + "/D" + k + ";4)*100)";
                    dataSheetFemale.SetFormulaValue("H" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(I" + k + "/D" + k + ";4)*100)";
                    dataSheetFemale.SetFormulaValue("J" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(K" + k + "/D" + k + ";4)*100)";
                    dataSheetFemale.SetFormulaValue("L" + k, fomular1);
                    //Tính vị trí tiếp theo
                    beginRow = endRow + 2;
                }
                //Cột toàn trường
                dataSheetFemale.CopyPasteSameSize(allSchoolRange, beginRow, 1);
                formular = string.Join("+", listTC.Select(u => "$$$" + u));

                dataSheetFemale.SetFormulaValue(beginRow, 3, "=" + formular.Replace("$$$", "C"));
                dataSheetFemale.SetFormulaValue(beginRow, 4, "=" + formular.Replace("$$$", "D"));
                dataSheetFemale.SetFormulaValue(beginRow, 5, "=" + formular.Replace("$$$", "E"));
                dataSheetFemale.SetFormulaValue(beginRow, 6, "=IF(D" + beginRow + "=0;0;ROUND(E" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFemale.SetFormulaValue(beginRow, 7, "=" + formular.Replace("$$$", "G"));
                dataSheetFemale.SetFormulaValue(beginRow, 8, "=IF(D" + beginRow + "=0;0;ROUND(G" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFemale.SetFormulaValue(beginRow, 9, "=" + formular.Replace("$$$", "I"));
                dataSheetFemale.SetFormulaValue(beginRow, 10, "=IF(D" + beginRow + "=0;0;ROUND(I" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFemale.SetFormulaValue(beginRow, 11, "=" + formular.Replace("$$$", "K"));
                dataSheetFemale.SetFormulaValue(beginRow, 12, "=IF(D" + beginRow + "=0;0;ROUND(K" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFemale.Name = "HS_Nu";
                  dataSheetFemale.FitAllColumnsOnOnePage = true;

            }
            #endregion

            #region sheet du lieu dan toc
            if (entity.EthnicID > 0)
            {
                firstRow = 10;
                lastCol = 12;
                beginRow = firstRow;
                IVTWorksheet dataSheetEthinic = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow, lastCol).ToString());
                List<ReviewBookPupilBO> listReviewPupilEthnic = listReviewPupil.Where(p => (p.EthnicID.HasValue && p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_NN )).ToList();
                listTC = new List<int>();
                foreach (EducationLevel educationLevel in lstEducationLevel)
                {
                    EducationLevelID = educationLevel.EducationLevelID;
                    if (EducationLevelID == 5 || EducationLevelID == 9 || EducationLevelID == 12)
                    {
                        continue;
                    }
                    List<object> listData = new List<object>();
                    int endRow = beginRow;
                    listBeginRow.Add(beginRow);
                    List<ClassProfileTempBO> listCP = listAllClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID)
                                            .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_Ethnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var listPupilRankingEthnic_Edu = lstRanking_Ethnic.Where(o => o.EducationLevelID == EducationLevelID);
                    if (listCP == null || listCP.Count == 0)
                    {
                        dataSheetEthinic.CopyPasteSameSize(summarizeRange, beginRow, 1);
                        endRow = beginRow - 1;
                        listTC.Add(endRow + 1);
                        dataSheetEthinic.SetCellValue(beginRow, 1, educationLevel.Resolution);
                    }
                    else
                    {
                        endRow = beginRow + listCP.Count - 1;
                        listTC.Add(endRow + 1);
                        bool isClassENVN = false;
                        foreach (ClassProfileTempBO cp in listCP)
                        {
                            isClassENVN = cp.isVNEN.HasValue ? cp.isVNEN.Value : false;

                            #region Ve khung fill cong thuc
                            for (int i = beginRow; i <= endRow; i++)
                            {
                                IVTRange copyRange;
                                if (i == beginRow)
                                {
                                    copyRange = toprange;
                                }
                                else if (i == endRow)
                                {
                                    copyRange = midrange;
                                }
                                else
                                {
                                    copyRange = midrange;
                                }
                                dataSheetEthinic.CopyPasteSameSize(copyRange, "A" + i);
                                //Tính các cột %
                                string fomular = "=IF(D" + i + "=0;0;ROUND(E" + i + "/D" + i + ";4)*100)";
                                dataSheetEthinic.SetFormulaValue("F" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(G" + i + "/D" + i + ";4)*100)";
                                dataSheetEthinic.SetFormulaValue("H" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(I" + i + "/D" + i + ";4)*100)";
                                dataSheetEthinic.SetFormulaValue("J" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(K" + i + "/D" + i + ";4)*100)";
                                dataSheetEthinic.SetFormulaValue("L" + i, fomular);
                            }
                            #endregion

                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["ClassName"] = cp.DisplayName;
                            var objCountPupilSS = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID);
                            //SS
                            dicData["NumberOfPupil"] = objCountPupilSS != null ? objCountPupilSS.Sum(p => p.TotalPupil) : 0;

                            //SS thuc te
                            var objCountPupilSSTT = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID && (p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || p.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED));
                            dicData["RealNumberOfPupil"] = objCountPupilSSTT != null ? objCountPupilSSTT.Sum(p => p.TotalPupil) : 0;


                            //Thi Lại
                            dicData["ReTest"] = listPupilRankingEthnic_Edu.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).Count();

                            //Rèn luyện trong hè
                            dicData["BackTraining"] = listPupilRankingEthnic_Edu.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).Count();

                            if (isClassENVN)
                            {
                                #region Neu la lop ENVN
                                //Lên lớp                            
                                dicData["UpClass"] = listReviewPupilEthnic.Where(o => o.ClassID == cp.ClassProfileID && ((o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT) || (o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))).Count();

                                //Ở lại
                                dicData["StayClass"] = listReviewPupilEthnic.Where(o => o.ClassID == cp.ClassProfileID && (o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT && o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT)).Count();
                                #endregion
                            }
                            else
                            {
                                #region Khong phai lop ENVN
                                //Lên lớp
                                dicData["UpClass"] = lstRanking.Where(o =>o.EthnicID != Ethnic_Kinh.EthnicID && o.EthnicID != Ethnic_ForeignPeople.EthnicID
                                                    && o.ClassID == cp.ClassProfileID 
                                                    && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS).Count();

                                //Ở lại
                                dicData["StayClass"] = lstRanking.Where(o => o.EthnicID != Ethnic_Kinh.EthnicID && o.EthnicID != Ethnic_ForeignPeople.EthnicID
                                                    && o.ClassID == cp.ClassProfileID 
                                                    && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS).Count();
                                #endregion
                            }

                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheetEthinic.GetRange(beginRow, 1, endRow, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });

                        //Merg cot
                        dataSheetEthinic.SetCellValue(beginRow, 1, educationLevel.Resolution);
                        dataSheetEthinic.MergeColumn(1, beginRow, endRow + 1);
                    }

                    //Tạo cột Tổng số của từng khối
                    dataSheetEthinic.CopyPasteSameSize(summarizeRange, endRow + 1, 1);
                    dataSheetEthinic.SetCellValue(endRow + 1, 1, educationLevel.Resolution);
                    dataSheetEthinic.SetCellValue(endRow + 1, 2, "TC");
                    dataSheetEthinic.SetFormulaValue(endRow + 1, 3, "=SUM(C" + beginRow + ":C" + endRow + ")");
                    dataSheetEthinic.SetFormulaValue(endRow + 1, 4, "=SUM(D" + beginRow + ":D" + endRow + ")");
                    dataSheetEthinic.SetFormulaValue(endRow + 1, 5, "=SUM(E" + beginRow + ":E" + endRow + ")");
                    dataSheetEthinic.SetFormulaValue(endRow + 1, 7, "=SUM(G" + beginRow + ":G" + endRow + ")");
                    dataSheetEthinic.SetFormulaValue(endRow + 1, 9, "=SUM(I" + beginRow + ":I" + endRow + ")");
                    dataSheetEthinic.SetFormulaValue(endRow + 1, 11, "=SUM(K" + beginRow + ":K" + endRow + ")");
                    int k = endRow + 1;
                    string fomular1 = "=IF(D" + k + "=0;0;ROUND(E" + k + "/D" + k + ";4)*100)";
                    dataSheetEthinic.SetFormulaValue("F" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(G" + k + "/D" + k + ";4)*100)";
                    dataSheetEthinic.SetFormulaValue("H" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(I" + k + "/D" + k + ";4)*100)";
                    dataSheetEthinic.SetFormulaValue("J" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(K" + k + "/D" + k + ";4)*100)";
                    dataSheetEthinic.SetFormulaValue("L" + k, fomular1);
                    //Tính vị trí tiếp theo
                    beginRow = endRow + 2;
                }
                //Cột toàn trường
                dataSheetEthinic.CopyPasteSameSize(allSchoolRange, beginRow, 1);
                formular = string.Join("+", listTC.Select(u => "$$$" + u));

                dataSheetEthinic.SetFormulaValue(beginRow, 3, "=" + formular.Replace("$$$", "C"));
                dataSheetEthinic.SetFormulaValue(beginRow, 4, "=" + formular.Replace("$$$", "D"));
                dataSheetEthinic.SetFormulaValue(beginRow, 5, "=" + formular.Replace("$$$", "E"));
                dataSheetEthinic.SetFormulaValue(beginRow, 6, "=IF(D" + beginRow + "=0;0;ROUND(E" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetEthinic.SetFormulaValue(beginRow, 7, "=" + formular.Replace("$$$", "G"));
                dataSheetEthinic.SetFormulaValue(beginRow, 8, "=IF(D" + beginRow + "=0;0;ROUND(G" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetEthinic.SetFormulaValue(beginRow, 9, "=" + formular.Replace("$$$", "I"));
                dataSheetEthinic.SetFormulaValue(beginRow, 10, "=IF(D" + beginRow + "=0;0;ROUND(I" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetEthinic.SetFormulaValue(beginRow, 11, "=" + formular.Replace("$$$", "K"));
                dataSheetEthinic.SetFormulaValue(beginRow, 12, "=IF(D" + beginRow + "=0;0;ROUND(K" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetEthinic.Name = "HS_DT";
                dataSheetEthinic.FitAllColumnsOnOnePage = true;
            }
            #endregion

            #region sheet du lieu nu dan toc
            if (entity.FemaleEthnicID > 0)
            {
                firstRow = 10;
                lastCol = 12;
                beginRow = firstRow;
                IVTWorksheet dataSheetFE = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow, lastCol).ToString());
                List<ReviewBookPupilBO> listReviewPupilFE = listReviewPupil.Where(p => p.Genre == SystemParamsInFile.GENRE_FEMALE && p.EthnicID.HasValue && (p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_KINH || p.EthnicID.Value != SystemParamsInFile.ETHNIC_ID_NN)).ToList();
                listTC = new List<int>();
                foreach (EducationLevel educationLevel in lstEducationLevel)
                {
                    EducationLevelID = educationLevel.EducationLevelID;
                    if (EducationLevelID == 5 || EducationLevelID == 9 || EducationLevelID == 12)
                    {
                        continue;
                    }
                    List<object> listData = new List<object>();
                    int endRow = beginRow;
                    listBeginRow.Add(beginRow);
                    List<ClassProfileTempBO> listCP = listAllClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID)
                                            .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_FemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var listPupilRankingFE_Edu = lstRanking_FemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID);
                    if (listCP == null || listCP.Count == 0)
                    {
                        dataSheetFE.CopyPasteSameSize(summarizeRange, beginRow, 1);
                        endRow = beginRow - 1;
                        listTC.Add(endRow + 1);
                        dataSheetFE.SetCellValue(beginRow, 1, educationLevel.Resolution);
                    }
                    else
                    {
                        endRow = beginRow + listCP.Count - 1;
                        listTC.Add(endRow + 1);
                        bool isClassENVN = false;
                        foreach (ClassProfileTempBO cp in listCP)
                        {
                            isClassENVN = cp.isVNEN.HasValue ? cp.isVNEN.Value : false;

                            #region Ve khung fill cong thuc
                            for (int i = beginRow; i <= endRow; i++)
                            {
                                IVTRange copyRange;
                                if (i == beginRow)
                                {
                                    copyRange = toprange;
                                }
                                else if (i == endRow)
                                {
                                    copyRange = midrange;
                                }
                                else
                                {
                                    copyRange = midrange;
                                }
                                dataSheetFE.CopyPasteSameSize(copyRange, "A" + i);
                                //Tính các cột %
                                string fomular = "=IF(D" + i + "=0;0;ROUND(E" + i + "/D" + i + ";4)*100)";
                                dataSheetFE.SetFormulaValue("F" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(G" + i + "/D" + i + ";4)*100)";
                                dataSheetFE.SetFormulaValue("H" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(I" + i + "/D" + i + ";4)*100)";
                                dataSheetFE.SetFormulaValue("J" + i, fomular);
                                fomular = "=IF(D" + i + "=0;0;ROUND(K" + i + "/D" + i + ";4)*100)";
                                dataSheetFE.SetFormulaValue("L" + i, fomular);
                            }
                            #endregion

                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["ClassName"] = cp.DisplayName;
                            var objCountPupilSS = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID);
                            //SS
                            dicData["NumberOfPupil"] = objCountPupilSS != null ? objCountPupilSS.Sum(p => p.TotalPupil) : 0;

                            //SS thuc te
                            var objCountPupilSSTT = listCountCurentPupilByClass_Edu.Where(p => p.ClassID == cp.ClassProfileID && (p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || p.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED));
                            dicData["RealNumberOfPupil"] = objCountPupilSSTT != null ? objCountPupilSSTT.Sum(p => p.TotalPupil) : 0;


                            //Thi Lại
                            dicData["ReTest"] = listPupilRankingFE_Edu.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).Count();

                            //Rèn luyện trong hè
                            dicData["BackTraining"] = listPupilRankingFE_Edu.Where(o => o.EducationLevelID == EducationLevelID && o.ClassID == cp.ClassProfileID && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).Count();

                            if (isClassENVN)
                            {
                                #region Neu la lop ENVN
                                //Lên lớp                            
                                dicData["UpClass"] = listReviewPupilFE.Where(o => o.ClassID == cp.ClassProfileID && ((o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT) || (o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))).Count();

                                //Ở lại
                                dicData["StayClass"] = listReviewPupilFE.Where(o => o.ClassID == cp.ClassProfileID && (o.RateAndYear.HasValue && o.RateAndYear.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT && o.RateAdd.HasValue && o.RateAdd.Value == SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT)).Count();
                                #endregion
                            }
                            else
                            {
                                #region Khong phai lop ENVN
                                //Lên lớp
                                dicData["UpClass"] = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE
                                                    && o.EthnicID != Ethnic_Kinh.EthnicID && o.EthnicID != Ethnic_ForeignPeople.EthnicID
                                                    && o.ClassID == cp.ClassProfileID
                                                    && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS).Count();

                                //Ở lại
                                dicData["StayClass"] = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE
                                                    && o.EthnicID != Ethnic_Kinh.EthnicID && o.EthnicID != Ethnic_ForeignPeople.EthnicID
                                                    && o.ClassID == cp.ClassProfileID
                                                    && o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS).Count();
                                #endregion
                            }

                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheetFE.GetRange(beginRow, 1, endRow, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });

                        //Merg cot
                        dataSheetFE.SetCellValue(beginRow, 1, educationLevel.Resolution);
                        dataSheetFE.MergeColumn(1, beginRow, endRow + 1);
                    }
                    //Tạo cột Tổng số của từng khối
                    dataSheetFE.CopyPasteSameSize(summarizeRange, endRow + 1, 1);
                    dataSheetFE.SetCellValue(endRow + 1, 1, educationLevel.Resolution);
                    dataSheetFE.SetCellValue(endRow + 1, 2, "TC");
                    dataSheetFE.SetFormulaValue(endRow + 1, 3, "=SUM(C" + beginRow + ":C" + endRow + ")");
                    dataSheetFE.SetFormulaValue(endRow + 1, 4, "=SUM(D" + beginRow + ":D" + endRow + ")");
                    dataSheetFE.SetFormulaValue(endRow + 1, 5, "=SUM(E" + beginRow + ":E" + endRow + ")");
                    dataSheetFE.SetFormulaValue(endRow + 1, 7, "=SUM(G" + beginRow + ":G" + endRow + ")");
                    dataSheetFE.SetFormulaValue(endRow + 1, 9, "=SUM(I" + beginRow + ":I" + endRow + ")");
                    dataSheetFE.SetFormulaValue(endRow + 1, 11, "=SUM(K" + beginRow + ":K" + endRow + ")");
                    int k = endRow + 1;
                    string fomular1 = "=IF(D" + k + "=0;0;ROUND(E" + k + "/D" + k + ";4)*100)";
                    dataSheetFE.SetFormulaValue("F" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(G" + k + "/D" + k + ";4)*100)";
                    dataSheetFE.SetFormulaValue("H" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(I" + k + "/D" + k + ";4)*100)";
                    dataSheetFE.SetFormulaValue("J" + k, fomular1);
                    fomular1 = "=IF(D" + k + "=0;0;ROUND(K" + k + "/D" + k + ";4)*100)";
                    dataSheetFE.SetFormulaValue("L" + k, fomular1);
                    //Tính vị trí tiếp theo
                    beginRow = endRow + 2;
                }
                //Cột toàn trường
                dataSheetFE.CopyPasteSameSize(allSchoolRange, beginRow, 1);
                formular = string.Join("+", listTC.Select(u => "$$$" + u));

                dataSheetFE.SetFormulaValue(beginRow, 3, "=" + formular.Replace("$$$", "C"));
                dataSheetFE.SetFormulaValue(beginRow, 4, "=" + formular.Replace("$$$", "D"));
                dataSheetFE.SetFormulaValue(beginRow, 5, "=" + formular.Replace("$$$", "E"));
                dataSheetFE.SetFormulaValue(beginRow, 6, "=IF(D" + beginRow + "=0;0;ROUND(E" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFE.SetFormulaValue(beginRow, 7, "=" + formular.Replace("$$$", "G"));
                dataSheetFE.SetFormulaValue(beginRow, 8, "=IF(D" + beginRow + "=0;0;ROUND(G" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFE.SetFormulaValue(beginRow, 9, "=" + formular.Replace("$$$", "I"));
                dataSheetFE.SetFormulaValue(beginRow, 10, "=IF(D" + beginRow + "=0;0;ROUND(I" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFE.SetFormulaValue(beginRow, 11, "=" + formular.Replace("$$$", "K"));
                dataSheetFE.SetFormulaValue(beginRow, 12, "=IF(D" + beginRow + "=0;0;ROUND(K" + beginRow + "/D" + beginRow + ";4)*100)");
                dataSheetFE.Name = "HS_Nu_DT";
                dataSheetFE.FitAllColumnsOnOnePage = true;
            }
            #endregion
          
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();          
            return oBook.ToStream();


        }
        #endregion
    }
}
