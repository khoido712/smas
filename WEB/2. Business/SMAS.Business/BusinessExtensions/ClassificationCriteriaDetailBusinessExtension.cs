﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ClassificationCriteriaDetailBusiness
    {

        #region Insert

        public void Insert(List<ClassificationCriteriaDetail> lstClassificationCriteriaDetail)
        {

            foreach (var item in lstClassificationCriteriaDetail)
            {
                if (item.ClassificationLevelHealth.TypeConfig.TypeConfigID == 1 || item.ClassificationLevelHealth.TypeConfig.TypeConfigID == 2)
                {

                    if (item.Month < 0 || item.Month > 60)
                    {
                        throw new BusinessException("Common_Validate_ClassificationCriteriaDetail_Month");
                    }
                }

                if (item.ClassificationLevelHealth.TypeConfig.TypeConfigID == 3)
                {

                    if (item.Month < 61 || item.Month > 228)
                    {
                        throw new BusinessException("Common_Validate_ClassificationCriteriaDetail_Month");
                    }
                }

                base.Insert(item);

            }
        }

        #endregion

        #region Update

        public void Update(List<ClassificationCriteriaDetail> lstClassificationCriteriaDetail)
        {
            if (lstClassificationCriteriaDetail == null || lstClassificationCriteriaDetail.Count ==0) return;

            //ClassificationCriteriaID: FK(ClassificationCriteria)
            ClassificationCriteriaBusiness.CheckAvailable(lstClassificationCriteriaDetail.FirstOrDefault().ClassificationCriteriaID, "ClassificationCriteria_Label_ClassificationCriteriaID", false);

            int ClassificationCriteriaID = lstClassificationCriteriaDetail.FirstOrDefault().ClassificationCriteriaID;
            ClassificationCriteria CC = ClassificationCriteriaBusiness.Find(ClassificationCriteriaID);
            List<ClassificationCriteriaDetail> ListCCD = this.ClassificationCriteriaDetailRepository.All.Where(o => o.ClassificationCriteriaID == ClassificationCriteriaID).ToList();

            foreach (var item in lstClassificationCriteriaDetail)
            {
                if (CC.TypeConfig.TypeConfigName.ToUpper() == "CÂN NẶNG" || CC.TypeConfig.TypeConfigName.ToUpper() == "CHIỀU CAO")
                {
                    if (item.Month < 0 || item.Month > 60)
                    {
                        throw new BusinessException("Common_Validate_ClassificationCriteriaDetail_Month");
                    }
                }
                if (CC.TypeConfig.TypeConfigName.ToUpper() == "BMI")
                {
                    if (item.Month < 61 || item.Month > 228)
                    {
                        throw new BusinessException("Common_Validate_ClassificationCriteriaDetail_Month");
                    }
                }
                ClassificationCriteriaDetail CCD = ListCCD.Where(o => o.ClassificationCriteriaID == ClassificationCriteriaID && o.ClassificationLevelHealthID == item.ClassificationLevelHealthID && o.Month == item.Month).FirstOrDefault();
                
                if (CCD == null)
                {
                    ClassificationCriteriaDetail tmp = new ClassificationCriteriaDetail();
                    tmp.ClassificationCriteriaID = item.ClassificationCriteriaID;
                    tmp.Month = item.Month;
                    tmp.IndexValue = item.IndexValue;
                    tmp.ClassificationLevelHealthID = item.ClassificationLevelHealthID;
                    base.Insert(tmp);
                }
                else
                {
                    CCD.IndexValue = item.IndexValue;
                    //base.Update(CCD);
                }
                
            }
        }

        #endregion


        #region Delete

        public void Delete(int ClassificationCriteriaDetailID)
        {
            this.CheckAvailable(ClassificationCriteriaDetailID, "ClassificationCriteriaDetail_Label_ClassificationCriteriaDetailID", true);
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "ClassificationCriteriaDetail", ClassificationCriteriaDetailID, "ClassificationCriteriaDetail_Label_ClassificationCriteriaDetailID");
            base.Delete(ClassificationCriteriaDetailID);

        }

        #endregion

        #region Search
        public IQueryable<ClassificationCriteriaDetail> Search(IDictionary<string, object> SearchInfo)
        {
            int ClassificationCriteriaID = Utils.GetInt(SearchInfo, "ClassificationCriteriaID");
            int ClassificationLevelHealthID = Utils.GetInt(SearchInfo, "ClassificationLevelHealthID");
            int Month = Utils.GetInt(SearchInfo, "Month" , -1);

            IQueryable<ClassificationCriteriaDetail> lst = ClassificationCriteriaDetailRepository.All;

            if (ClassificationCriteriaID != 0)
            {
                lst = lst.Where(o => o.ClassificationCriteriaID == ClassificationCriteriaID);
            }

            if (ClassificationLevelHealthID != 0)
            {
                lst = lst.Where(o => o.ClassificationLevelHealthID == ClassificationLevelHealthID);
            }

            if (Month != -1)
            {
                lst = lst.Where(o => o.Month == Month);
            }


            return lst;
        }

        #endregion

        #region SearchBySchool
        public IQueryable<ClassificationCriteriaDetail> SearchBySchool(IDictionary<string, object> dic)
        {
             return this.Search(dic);
        }
        #endregion

        #region GetListClassificationDetail

        public IQueryable<ClassificationCriteriaDetail> GetListClassificationDetail(IDictionary<string, object> SearchInfo)
        {

            DateTime? EffectDate = Utils.GetDateTime(SearchInfo, "EffectDate");
            int GenreID = Utils.GetInt(SearchInfo, "GenreID");
            int TypeConfigID = Utils.GetInt(SearchInfo, "TypeConfigID");
            int ClassificationCriteriaID = 0;
            IQueryable<ClassificationCriteria> lstClassificationCriteria = ClassificationCriteriaBusiness.Search(SearchInfo);
            if (lstClassificationCriteria.Count() > 0)
            {
                ClassificationCriteriaID = lstClassificationCriteria.FirstOrDefault().ClassificationCriteriaID;
            }
            else
                ClassificationCriteriaID = -1;
            Dictionary<string, object> Dic1 = new Dictionary<string, object>();
            Dic1["ClassificationCriteriaID"] = ClassificationCriteriaID;

            IQueryable<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = this.Search(Dic1);

            return lstClassificationCriteriaDetail;

        }


        #endregion


    }
}