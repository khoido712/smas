﻿using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class StatisticLevelReportBusiness
    {
        public IQueryable<StatisticLevelReport> SearchBySchool(int SchoolID, Dictionary<string, object> dic = null)
        {
            IQueryable<StatisticLevelReport> lsStatisticLevel = StatisticLevelReportRepository.All;
            if (SchoolID != 0)
            {
                if (dic == null) dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                lsStatisticLevel = this.Search(dic);
            }
            return lsStatisticLevel;
        }
        public IQueryable<StatisticLevelReport> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string Resolution = Utils.GetString(dic, "Resolution");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<StatisticLevelReport> lsStatisticLevel = this.StatisticLevelReportRepository.All;
            if (SchoolID > 0)
            {
                lsStatisticLevel = lsStatisticLevel.Where(o => o.SchoolID == SchoolID || o.SchoolID == null);
            }
            if (Resolution.Trim().Length > 0)
            {
                lsStatisticLevel = lsStatisticLevel.Where(o => o.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (isActive.HasValue)
            {
                lsStatisticLevel = lsStatisticLevel.Where(o => o.IsActive == isActive);               
            }
            return lsStatisticLevel;
        }

        /// <summary>
        /// Lấy các mức thống kê của trường (bao gom ca muc thong ke chuan)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <returns></returns>
        public List<StatisticLevelReport> getStatisticLevelReportOfSchool(int schoolID)
        {
            var statisticLevelReport = this.All
                .Where(r => (r.SchoolID == schoolID || !r.SchoolID.HasValue)
                            && r.IsActive == true)
                .OrderByDescending(r => r.CreateDate)
                .Select(r => r)
                .ToList();
            var standardReport = statisticLevelReport.FirstOrDefault(r => !r.SchoolID.HasValue);
            // Chuyen muc thong ke chuan len dau danh sach
            if (standardReport != null)
            {
                statisticLevelReport.Remove(standardReport);
                statisticLevelReport.Insert(0, standardReport);
            }
            return statisticLevelReport;
        }
    }
}
