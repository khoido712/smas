﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>26/12/2012 4:17:11 PM</date>
    /// </summary>
    public partial class InfectiousDiseasBusiness
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 26/12/2012   5:17 PM
        /// </remarks>
        public InfectiousDiseas Insert(InfectiousDiseas entity)
        {
            ValidationMetadata.ValidateObject(entity);
            Validate(entity);
            return base.Insert(entity);
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 26/12/2012   5:17 PM
        /// </remarks>
        public InfectiousDiseas Update(InfectiousDiseas entity)
        {
            ValidationMetadata.ValidateObject(entity);
            Validate(entity);
            return base.Update(entity);
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="InfectiousDiseaseID">The infectious disease ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 26/12/2012   5:17 PM
        /// </remarks>
        public void Delete(int InfectiousDiseaseID, int SchoolID)
        {
            {
                bool Exist = repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "InfectiousDiseas",
                        new Dictionary<string, object>()
                {
                    {"InfectiousDiseaseID",InfectiousDiseaseID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }
            InfectiousDiseas entity = InfectiousDiseasRepository.Find(InfectiousDiseaseID);
            {
                bool Compatible = repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilOfSchool",
                        new Dictionary<string, object>()
                {
                    {"PupilID",entity.PupilID},
                    {"SchoolID",entity.SchoolID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            base.Delete(InfectiousDiseaseID);
        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 26/12/2012   5:17 PM
        /// </remarks>
        public IQueryable<InfectiousDiseas> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            var ClassID = Utils.GetInt(dic, "ClassID");
            var AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            var AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            var EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            var SchoolID = Utils.GetInt(dic, "SchoolID");
            var PupilID = Utils.GetInt(dic, "PupilID");
            var FullName = Utils.GetString(dic, "FullName");

            var FromInfectionDate = Utils.GetDateTime(dic, "FromInfectionDate");
            var ToInfectionDate = Utils.GetDateTime(dic, "ToInfectionDate");

            var FromOffDate = Utils.GetDateTime(dic, "FromOffDate");
            var ToOffDate = Utils.GetDateTime(dic, "ToOffDate");
            var Address = Utils.GetString(dic, "Address");
            var CommuneID = Utils.GetInt(dic, "CommuneID");

            var DiseasesName = Utils.GetString(dic, "DiseasesName");
            var DiseasesID = Utils.GetInt(dic, "DiseasesID");

            var Note = Utils.GetString(dic, "Note");

            IQueryable<InfectiousDiseas> iqInfectiousDiseas = InfectiousDiseasRepository.All;

            if (FullName.Trim().Length != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.PupilProfile.FullName.ToUpper().Contains(FullName.ToUpper()));
            }
            if(Resolution.Trim().Length != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.DiseasesType.Resolution.ToUpper().Contains(Resolution.ToUpper()));
            }
            if(ClassID!=0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.ClassID == ClassID);
            }
            if (AcademicYearID != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (AppliedLevel != 0)
            {
                if (AppliedLevel == 1)
                {
                    iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID7);
                }
                else if (AppliedLevel == 2)
                {
                    iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID5 && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID10);
                }
                else if(AppliedLevel == 3)
                {
                    iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID9 && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID13);
                }
                else if (AppliedLevel == 4)//nha tre
                {
                    iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID12 && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID16);
                }
                else if (AppliedLevel == 5)//mau giao
                {
                    iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID15 && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID19);
                }
            }
            if (EducationLevelID != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.SchoolID == SchoolID);
            }
            if (PupilID != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.PupilID == PupilID);
            }
            if (CommuneID != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.CommuneID == CommuneID);
            }

            if (Address.Trim().Length != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.Address.ToUpper().Contains(Address.ToUpper()));
            }

            if (DiseasesName.Trim().Length != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.DiseasesName.ToUpper().Contains(DiseasesName.ToUpper()));
            }

            if (DiseasesID != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.DiseasesTypeID == DiseasesID);
            }

            if (Note.Trim().Length != 0)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => o.Note.ToUpper().Contains(Note.ToUpper()));
            }

            if(FromInfectionDate.HasValue)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => EntityFunctions.TruncateTime(o.InfectionDate) >= EntityFunctions.TruncateTime(FromInfectionDate.Value));
            }
            if (ToInfectionDate.HasValue)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => EntityFunctions.TruncateTime(o.InfectionDate) <= EntityFunctions.TruncateTime(ToInfectionDate.Value));
            }
            if (FromOffDate.HasValue)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o =>EntityFunctions.TruncateTime(o.OffDate.Value) >= EntityFunctions.TruncateTime(FromOffDate.Value));
            }
            if (ToOffDate.HasValue)
            {
                iqInfectiousDiseas = iqInfectiousDiseas.Where(o => EntityFunctions.TruncateTime(o.OffDate.Value) <= EntityFunctions.TruncateTime(ToOffDate.Value));
            }

            return iqInfectiousDiseas;

        }
        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 26/12/2012   5:17 PM
        /// </remarks>
        public IQueryable<InfectiousDiseas> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if(SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            if(SchoolID == 0 )
            {
                return null;
            }
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 26/12/2012   5:17 PM
        /// </remarks>
        private void Validate(InfectiousDiseas entity)
        {
            //DiseasesTypeID: FK
            if (entity.DiseasesTypeID.HasValue)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "DiseasesType",
                        new Dictionary<string, object>()
                {
                    {"DiseasesTypeID",entity.DiseasesTypeID.Value}
                    ,{"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable",new List<object>{"InfectiousDiseas_Label_DiseasesType"});
                }
            }
            //Check date passes datetime now
            Utils.ValidatePastDate(entity.InfectionDate, "InfectiousDiseas_Label_InfectionDate");
            Utils.ValidatePastDate(entity.OffDate, "InfectiousDiseas_Label_OffDate");
            //AcademicYearID: FK
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                        new Dictionary<string, object>()
                {
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> {"AcademicYear_Control_Year" });
                }
            }
            //EducationLevelID: FK
            {
                bool Exist = repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel",
                        new Dictionary<string, object>()
                {
                    {"EducationLevelID",entity.EducationLevelID}
                     ,{"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> {"InfectiousDiseas_Label_EducationLevel" });
                }
            }
            Utils.ValidateRequire(entity.CommuneID, "InfectiousDiseas_Label_Commune");
            //CommuneID: FK, Require
            {
                bool Exist = repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "Commune",
                        new Dictionary<string, object>()
                {
                    {"CommuneID",entity.CommuneID}
                     ,{"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> {"InfectiousDiseas_Label_CommuneID" });
                }
            }
            //PupilID: FK, Exist in MonitoringBook
            {
                bool Exist = repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                        new Dictionary<string, object>()
                {
                    {"PupilProfileID",entity.PupilID}
                    ,{"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> {"InfectiousDiseas_Label_PupilID" });
                }
            }
            {
                bool Exist = repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                        new Dictionary<string, object>()
                {
                    {"PupilID",entity.PupilID}
                }, null);
                if (!Exist)
                {

                    throw new BusinessException("InfectiousDiseas_Label_PupilIDNotExistInMonitoringBook");
                }
            }
            //PupilID, SchoolID: Not compatible
            {
                bool Compatible = repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilOfClass",
                        new Dictionary<string, object>()
                {
                    {"PupilID",entity.PupilID},
                    {"ClassID",entity.ClassID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible", new List<object> {"InfectiousDiseas_Label_PupilID","InfectiousDiseas_Label_SchoolID" });
                }
            }

            //doi voi update,InfectiousDiseaseID: PK
            if(entity.InfectiousDiseasesID!=0)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "InfectiousDiseases",
                        new Dictionary<string, object>()
                {
                    {"InfectiousDiseasesID",entity.InfectiousDiseasesID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object> {"InfectiousDiseas_Label_InfectiousDisease" });
                }
            }

        }
    }
}
