/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Lưu thông tin học sinh chuyển lớp
    /// Liên quan đến 3 table: PupilProfile (trường ProfileStatus) - PupilOfClass và ClassMovement
    /// PupilOfClass sẽ gồm 1 bản ghi lưu thông tin học sinh ở lớp cũ với ProfileStatus = 5 (đã chuyển lớp)
    /// và 1 bản ghi lưu lớp mới với ProfileStatus = 1 (đang học)
    /// <author>minhh</author>
    /// <date>15/10/2012</date>
    /// </summary>
    public partial class ClassMovementBusiness
    {
        #region validate
        private void Validate(ClassMovement Entity, bool isInsert)
        {
            ValidationMetadata.ValidateObject(Entity);
            //SemesterStartDate =< MovedDate <= SemesterEndDate (Ngày bắt đầu, kết thúc học kỳ tùy thuộc vào Semester, AcademicYearID)
            AcademicYear academicYear = AcademicYearRepository.Find(Entity.AcademicYearID);
            Utils.ValidatePastDate(Entity.MovedDate, "ClassMovement_Label_MovedDate");
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                if (academicYear.FirstSemesterStartDate > Entity.MovedDate || academicYear.FirstSemesterEndDate < Entity.MovedDate)
                {
                    throw new BusinessException("ClassMovement_Validate_NotInSemester", new List<object>() { { SystemParamsInFile.SEMESTER_OF_YEAR_FIRST } });
                }
            }
            else if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                if (academicYear.SecondSemesterStartDate > Entity.MovedDate || academicYear.SecondSemesterEndDate < Entity.MovedDate)
                {
                    throw new BusinessException("ClassMovement_Validate_NotInSemester", new List<object>() { { SystemParamsInFile.SEMESTER_OF_YEAR_SECOND } });
                }
            }

            DateTime? lastAssignDateOfPupil = PupilOfClassBusiness.All.Where(u => u.ClassID == Entity.FromClassID && u.PupilID == Entity.PupilID).OrderByDescending(u => u.AssignedDate).Select(u => u.AssignedDate).FirstOrDefault();
            if (!lastAssignDateOfPupil.HasValue || lastAssignDateOfPupil.Value > Entity.MovedDate)
                throw new BusinessException("ClassMovement_Validate_BeforeCurrentClassAssignDate");

            //ClassID, EducationLevelID: not compatible(ClassProfile)
            /*bool FromClassProfileEducationLevelCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                  new Dictionary<string, object>()
                {
                    {"EducationLevelID",Entity.EducationLevelID},
                    {"ClassProfileID",Entity.FromClassID}
                }, null);
            if (!FromClassProfileEducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            bool ToClassProfileEducationLevelCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"EducationLevelID",Entity.EducationLevelID},
                    {"ClassProfileID",Entity.ToClassID}
                }, null);
            if (!ToClassProfileEducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }*/
            //PupilID, AcademicYearID: not compatible(PupilProfile)
            /*bool PupilAcademicYearCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                 new Dictionary<string, object>()
                {
                    {"PupilProfileID",Entity.PupilID},
                    {"CurrentAcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!PupilAcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }*/

            //ClassID, AcademicYearID: not compatible(ClassProfile)
            /*bool FromClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"ClassProfileID",Entity.FromClassID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!FromClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            bool ToClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"ClassProfileID",Entity.ToClassID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!ToClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }*/

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            if (isInsert) //Bo qua check neu la update
            {
                //Kiểm tra PupilOfClass nếu Status != PUPIL_STATUS_STUDYING (1): Không cho phép chuyển lớp học sinh không ở trạng thái đang học
                PupilProfile pp = PupilProfileBusiness.Find(Entity.PupilID);
                if (Entity.PupilID != 0)
                {
                    PupilOfClass item = PupilOfClassBusiness.All.Where(o => (o.PupilID == Entity.PupilID) && (o.ClassID == pp.CurrentClassID)).OrderByDescending(o => o.PupilOfClassID).First();
                    if (item.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        throw new BusinessException("ClassMovement_Validate_PupilNotWorking");
                    }
                }
            }
            //Ngày chuyển null, lớn hơn ngày hiện tại, không thuộc học kì, Kiểm tra MovedDate
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin học sinh chuyển lớp
        /// <author>minhh</author>
        /// <date>12/10/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<ClassMovement> Search(IDictionary<string, object> dic = null)
        {
            int ClassMovementID = Utils.GetInt(dic, "ClassMovementID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int FromClassID = Utils.GetInt(dic, "FromClassID");
            int ToClassID = Utils.GetInt(dic, "ToClassID");
            int? EducationLevelID = Utils.GetShort(dic, "EducationLevelID");
            int? SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            DateTime? MovedDate = Utils.GetDateTime(dic, "MovedDate");
            string Reason = Utils.GetString(dic, "Reason");
            string FullName = Utils.GetString(dic, "FullName");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            List<int> lstClassID = Utils.GetIntList(dic, "ListClassID");
            int Semester = Utils.GetInt(dic, "Semester");
            IQueryable<ClassMovement> Query = this.ClassMovementRepository.All.Where(o => o.PupilProfile.IsActive == true);

            if (FromClassID != 0)
            {
                Query = from cm in Query
                        join cp in ClassProfileBusiness.All on cm.FromClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && cm.FromClassID == FromClassID
                        select cm;
            }
            if (ToClassID != 0)
            {
                Query = from cm in Query
                        join cp in ClassProfileBusiness.All on cm.ToClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && cm.ToClassID == FromClassID
                        select cm;
            }
            if (ClassMovementID != 0)
            {
                Query = Query.Where(o => o.ClassMovementID == ClassMovementID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }
            //if (FromClassID != 0)
            //{
            //    Query = Query.Where(o => o.FromClassID == FromClassID);
            //}
            //if (ToClassID != 0)
            //{
            //    Query = Query.Where(o => o.ToClassID == ToClassID);
            //}
            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (MovedDate.HasValue)
            {
                DateTime nextDay = MovedDate.Value.AddDays(1);
                Query = Query.Where(o => o.MovedDate >= MovedDate && o.MovedDate <= nextDay);
            }
            if (Reason.Trim().Length != 0)
            {
                Query = Query.Where(o => o.Reason.ToLower().Contains(Reason.ToLower()));
            }
            if (FullName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
            if (PupilCode.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            }
            if (lstClassID.Count > 0)
            {
                Query = Query.Where(o => lstClassID.Contains(o.ToClassID));
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(o => (o.MovedDate <= o.AcademicYear.FirstSemesterEndDate && o.MovedDate >= o.AcademicYear.FirstSemesterStartDate));
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                Query = Query.Where(o => (o.MovedDate <= o.AcademicYear.SecondSemesterEndDate && o.MovedDate >= o.AcademicYear.SecondSemesterStartDate));
            }
            return Query;
        }
        #endregion

        #region SearchBySchool
        /// <summary>
        /// Tìm kiếm học sinh chuyển lớp theo trường
        /// </summary>
        /// <param name="SchoolID">ID trường cần tìm kiếm</param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<ClassMovement> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0) dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới học sinh chuyển lớp
        /// <author>minhh</author>
        /// </summary>
        /// <param name="UserID">ID người dùng</param>
        /// <param name="lstMovement">Danh sách học sinh chuyển lớp</param>
        public void InsertClassMovement(int UserID, List<ClassMovement> lstMovement, bool? isClassMovement)
        {
            PupilProfile pupilProfile = new PupilProfile();
            bool? flagRegister = null;
            if (lstMovement != null && lstMovement.Count > 0 && UtilsBusiness.HasHeadTeacherPermission(UserID, lstMovement[0].FromClassID))
            {
                foreach (ClassMovement classMovement in lstMovement)
                {
                    this.Validate(classMovement, true);
                    AcademicYear academicYear = AcademicYearBusiness.Find(classMovement.AcademicYearID);
                    int partitionId = UtilsBusiness.GetPartionId(academicYear.SchoolID);
                    //Kiểm tra có chuyển lớp trong cùng ngày không
                    Dictionary<string, object> search = new Dictionary<string, object>();
                    search["PupilID"] = classMovement.PupilID;
                    search["AcademicYearID"] = classMovement.AcademicYearID;
                    search["MovedDate"] = classMovement.MovedDate;
                    ClassMovement classMove = Search(search).FirstOrDefault();

                    pupilProfile = PupilProfileBusiness.Find(classMovement.PupilID);
                    pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                    pupilProfile.CurrentClassID = classMovement.ToClassID;
                    PupilProfileBusiness.Update(pupilProfile);

                    // Tìm kiếm học sinh trong lớp cũ PupilOfClass
                    // Cập nhật lại Status trong bản ghi này = 5 và gọi hàm UpdatePupilOfClass trong nghiệp vụ PupilOfClassBusiness
                    IDictionary<string, object> paras1 = new Dictionary<string, object>();
                    paras1["PupilID"] = classMovement.PupilID;
                    paras1["SchoolID"] = classMovement.SchoolID;
                    paras1["ClassID"] = classMovement.FromClassID;
                    paras1["AcademicYearID"] = classMovement.AcademicYearID;
                    PupilOfClass oldPupilOfClass = PupilOfClassBusiness.SearchBySchool(classMovement.SchoolID, paras1).OrderByDescending(o => o.AssignedDate).FirstOrDefault();

                    IDictionary<string, object> paras2 = new Dictionary<string, object>();
                    paras2["PupilID"] = classMovement.PupilID;
                    paras2["SchoolID"] = classMovement.SchoolID;
                    paras2["ClassID"] = classMovement.ToClassID;
                    paras2["AcademicYearID"] = classMovement.AcademicYearID;
                    PupilOfClass newPupilOfClass = PupilOfClassBusiness.SearchBySchool(classMovement.SchoolID, paras2).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
                    //if (classMove == null)//Chuyển lớp lần đầu trong ngày
                    //{
                    base.Insert(classMovement);
                    if (oldPupilOfClass != null)
                    {
                        oldPupilOfClass.EndDate = classMovement.MovedDate;

                        oldPupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS;

                        //anhnph1 20150114 - kiểm tra HS co dang ky HD hay khong?
                        flagRegister = oldPupilOfClass.IsRegisterContract;
                        if (flagRegister == true)
                        {
                            oldPupilOfClass.IsRegisterContract = false;
                        }

                        //Không muốn giữ lại thông tin của học sinh thì delete.
                        if (isClassMovement == false)
                        {
                            PupilOfClassBusiness.Delete(oldPupilOfClass.PupilOfClassID);
                        }
                        //Nếu muốn giữ lại thông tin thì update
                        else
                        {
                            PupilOfClassBusiness.Update(oldPupilOfClass);
                        }

                    }

                    if (newPupilOfClass == null)
                    {
                        newPupilOfClass = new PupilOfClass() { PupilOfClassID = 0 };
                    }
                    newPupilOfClass.PupilID = classMovement.PupilID;
                    newPupilOfClass.AcademicYearID = classMovement.AcademicYearID;
                    newPupilOfClass.SchoolID = classMovement.SchoolID;
                    newPupilOfClass.ClassID = classMovement.ToClassID;
                    newPupilOfClass.Year = classMovement.AcademicYear.Year;
                    //Neu khong giu lai thong tin thi lay ngay vao lop cua lop cu
                    if (isClassMovement == false && oldPupilOfClass != null)
                    {
                        newPupilOfClass.AssignedDate = oldPupilOfClass.AssignedDate;
                    }
                    else
                    {
                        newPupilOfClass.AssignedDate = classMovement.MovedDate;
                    }

                    newPupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                    //anhnph1 20150114 - kiểm tra HS co dang ky HD
                    if (flagRegister == true)
                    {
                        newPupilOfClass.IsRegisterContract = true;
                    }

                    newPupilOfClass.OrderInClass = MaxOrderInClass(classMovement.SchoolID, classMovement.AcademicYearID, classMovement.ToClassID);
                    if (newPupilOfClass.PupilOfClassID == 0)
                    {
                        PupilOfClassBusiness.Insert(newPupilOfClass);
                    }
                    else
                    {
                        PupilOfClassBusiness.Update(newPupilOfClass);
                    }
                    PupilOfClassBusiness.Save();
                    
                    #region Xoa diem o lop moi
                    IDictionary<string, object> SearchDic = new Dictionary<string, object>();
                    SearchDic["PupilID"] = classMovement.PupilID;
                    SearchDic["AcademicYearID"] = classMovement.AcademicYearID;
                    SearchDic["ClassID"] = classMovement.ToClassID;
                    SearchDic["checkWithClassMovement"] = "checkWithClassMovement";
                    SearchDic["Year"] = academicYear.Year;  // AnhVD 20131217
                    List<MarkRecord> lstMarkRecord = MarkRecordBusiness.SearchBySchool(classMovement.SchoolID, SearchDic).ToList();
                    if (lstMarkRecord.Count > 0)
                    {
                        MarkRecordBusiness.DeleteAll(lstMarkRecord);
                    }

                    List<JudgeRecord> lstJudgeRecord = JudgeRecordBusiness.SearchBySchool(classMovement.SchoolID, SearchDic).ToList();
                    if (lstJudgeRecord.Count > 0)
                    {
                        JudgeRecordBusiness.DeleteAll(lstJudgeRecord);
                    }

                    List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(classMovement.SchoolID, SearchDic).ToList();
                    if (lstSummedUpRecord.Count > 0)
                    {
                        SummedUpRecordBusiness.DeleteAll(lstSummedUpRecord);
                    }

                    List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(classMovement.SchoolID, SearchDic).ToList();
                    if (lstPupilRanking.Count > 0)
                    {
                        PupilRankingBusiness.DeleteAll(lstPupilRanking);
                    }

                    List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(classMovement.SchoolID, SearchDic).ToList();
                    if (lstPupilEmulation.Count > 0)
                    {
                        PupilEmulationBusiness.DeleteAll(lstPupilEmulation);
                    }

                    List<ConductEvaluationDetail> lstConductEvaluationDetail = ConductEvaluationDetailBusiness.SearchBySchool(classMovement.SchoolID, SearchDic).ToList();
                    if (lstConductEvaluationDetail.Count > 0)
                    {
                        ConductEvaluationDetailBusiness.DeleteAll(lstConductEvaluationDetail);
                    }
                    #endregion

                    // Tìm kiếm điểm của học sinh ở lớp cũ trong MarkRecord bằng hàm SearchMarkRecord theo
                    //-	PupilID = ClassMovement.PupilID
                    //-	AcademicYearID = ClassMovement.AcademicYearID
                    //-	SchoolID = ClassMovement.SchoolID
                    //-	ClassID = ClassMovement.FromClassID
                    //Cập nhật lai bản ghi bảng điểm này theo lớp mới ClassID = ClassMovement.FromClassID và Insert vào MarkRecord
                    IDictionary<string, object> SearchMark = new Dictionary<string, object>();
                    SearchMark["PupilID"] = classMovement.PupilID;
                    SearchMark["AcademicYearID"] = classMovement.AcademicYearID;
                    SearchMark["SchoolID"] = classMovement.SchoolID;
                    SearchMark["ClassID"] = classMovement.FromClassID;
                    SearchMark["checkWithClassMovement"] = "checkWithClassMovement";
                    SearchMark["Year"] = academicYear.Year;  // AnhVD 20131217
                    #region markrecord
                    lstMarkRecord = MarkRecordBusiness.SearchBySchool(classMovement.SchoolID, SearchMark).ToList();
                    if (lstMarkRecord.Count != 0)
                    {
                        MarkRecord newMarkRecord = new MarkRecord();
                        foreach (MarkRecord markRecord in lstMarkRecord)
                        {
                            //Cap nhat IsOldData = true
                            markRecord.IsOldData = true;
                            MarkRecordBusiness.Update(markRecord);

                            //Them diem vao lop moi
                            newMarkRecord = new MarkRecord();
                            newMarkRecord.PupilID = markRecord.PupilID;
                            newMarkRecord.ClassID = classMovement.ToClassID;
                            newMarkRecord.SchoolID = markRecord.SchoolID;
                            newMarkRecord.AcademicYearID = markRecord.AcademicYearID;
                            newMarkRecord.SubjectID = markRecord.SubjectID;
                            newMarkRecord.MarkTypeID = markRecord.MarkTypeID;
                            newMarkRecord.CreatedAcademicYear = markRecord.CreatedAcademicYear;
                            newMarkRecord.Semester = markRecord.Semester;
                            newMarkRecord.Title = markRecord.Title;
                            newMarkRecord.Mark = markRecord.Mark;
                            newMarkRecord.OrderNumber = markRecord.OrderNumber;
                            newMarkRecord.MarkedDate = markRecord.MarkedDate;
                            newMarkRecord.CreatedDate = markRecord.CreatedDate;
                            newMarkRecord.ModifiedDate = markRecord.ModifiedDate;
                            newMarkRecord.IsOldData = false;
                            MarkRecordBusiness.Insert(newMarkRecord);
                        }
                    }
                    #endregion
                    //Tìm kiếm điểm của học sinh ở lớp cũ trong JudgeRecord bằng hàm SearchJudgeRecord theo
                    //-	PupilID = ClassMovement.PupilID
                    //-	AcademicYearID = ClassMovement.AcademicYearID
                    //-	SchoolID = ClassMovement.SchoolID
                    //-	ClassID = ClassMovement.FromClassID
                    //Cập nhật lại bảng điểm này theo lớp mới ClassID = ClassMovement.FromClassID và Insert vào JudgeRecord
                    #region judgerecord
                    lstJudgeRecord = JudgeRecordBusiness.SearchBySchool(classMovement.SchoolID, SearchMark).ToList();
                    if (lstJudgeRecord.Count != 0)
                    {
                        JudgeRecord newJudgeRecord = new JudgeRecord();
                        foreach (JudgeRecord judgeRecord in lstJudgeRecord)
                        {
                            //Cap nhat IsOldData = true
                            judgeRecord.IsOldData = true;
                            JudgeRecordBusiness.Update(judgeRecord);

                            //Them diem vao lop moi
                            newJudgeRecord = new JudgeRecord();
                            newJudgeRecord.PupilID = judgeRecord.PupilID;
                            newJudgeRecord.ClassID = classMovement.ToClassID;
                            newJudgeRecord.SchoolID = judgeRecord.SchoolID;
                            newJudgeRecord.AcademicYearID = judgeRecord.AcademicYearID;
                            newJudgeRecord.SubjectID = judgeRecord.SubjectID;
                            newJudgeRecord.MarkTypeID = judgeRecord.MarkTypeID;
                            newJudgeRecord.CreatedAcademicYear = judgeRecord.CreatedAcademicYear;
                            newJudgeRecord.Semester = judgeRecord.Semester;
                            newJudgeRecord.Title = judgeRecord.Title;
                            newJudgeRecord.Judgement = judgeRecord.Judgement;
                            newJudgeRecord.OrderNumber = judgeRecord.OrderNumber;
                            newJudgeRecord.MarkedDate = judgeRecord.MarkedDate;
                            newJudgeRecord.CreatedDate = judgeRecord.CreatedDate;
                            newJudgeRecord.ModifiedDate = judgeRecord.ModifiedDate;
                            newJudgeRecord.IsOldData = false;

                            JudgeRecordBusiness.Insert(newJudgeRecord);
                        }
                    }
                    #endregion
                    //Tìm kiếm điểm của học sinh ở lớp cũ trong SummedUpRecord bằng hàm SearchBySchool theo
                    //-	PupilID = ClassMovement.PupilID
                    //-	AcademicYearID = ClassMovement.AcademicYearID
                    //-	SchoolID = ClassMovement.SchoolID
                    //-	ClassID = ClassMovement.FromClassID
                    //Cập nhật bản ghi lại bảng điểm này theo lớp mới ClassID = ClassMovement.FromClassID và Insert vào SummedUpRecord
                    #region summedUpRecord
                    lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(classMovement.SchoolID, SearchMark).ToList();
                    if (lstSummedUpRecord.Count != 0)
                    {
                        SummedUpRecord newSummedUpRecord = new SummedUpRecord();
                        foreach (SummedUpRecord summedUpRecord in lstSummedUpRecord)
                        {
                            //Cap nhat IsOldData = true
                            summedUpRecord.IsOldData = true;
                            SummedUpRecordBusiness.Update(summedUpRecord);
                            //Them diem vao lop moi
                            newSummedUpRecord = new SummedUpRecord();
                            newSummedUpRecord.PupilID = summedUpRecord.PupilID;
                            newSummedUpRecord.ClassID = classMovement.ToClassID;
                            newSummedUpRecord.SchoolID = summedUpRecord.SchoolID;
                            newSummedUpRecord.AcademicYearID = summedUpRecord.AcademicYearID;
                            newSummedUpRecord.SubjectID = summedUpRecord.SubjectID;
                            newSummedUpRecord.IsCommenting = summedUpRecord.IsCommenting;
                            newSummedUpRecord.CreatedAcademicYear = summedUpRecord.CreatedAcademicYear;
                            newSummedUpRecord.Semester = summedUpRecord.Semester;
                            newSummedUpRecord.PeriodID = summedUpRecord.PeriodID;
                            newSummedUpRecord.SummedUpMark = summedUpRecord.SummedUpMark;
                            newSummedUpRecord.JudgementResult = summedUpRecord.JudgementResult;
                            newSummedUpRecord.ReTestMark = summedUpRecord.ReTestMark;
                            newSummedUpRecord.ReTestJudgement = summedUpRecord.ReTestJudgement;
                            newSummedUpRecord.Comment = summedUpRecord.Comment;
                            newSummedUpRecord.SummedUpDate = summedUpRecord.SummedUpDate;
                            newSummedUpRecord.IsOldData = false;

                            SummedUpRecordBusiness.Insert(newSummedUpRecord);
                        }
                    }
                    #endregion

                    //Tìm kiếm điểm tổng kết của học sinh ở lớp cũ trong PupilRanking bằng hàm SearchBySchool theo
                    //-	PupilID = ClassMovement.PupilID
                    //-	AcademicYearID = ClassMovement.AcademicYearID
                    //-	SchoolID = ClassMovement.SchoolID
                    //-	ClassID = ClassMovement.FromClassID
                    //Cập nhật bản ghi lại bảng điểm này theo lớp mới ClassID = ClassMovement.FromClassID và Insert vào PupilRanking
                    #region pupiRanking
                    lstPupilRanking = PupilRankingBusiness.SearchBySchool(classMovement.SchoolID, SearchMark).ToList();
                    if (lstPupilRanking.Count != 0)
                    {
                        PupilRanking newPupilRanking = new PupilRanking();
                        foreach (PupilRanking pupilRanking in lstPupilRanking)
                        {
                            //Cap nhat IsOldData = true
                            pupilRanking.IsOldData = true;
                            PupilRankingBusiness.Update(pupilRanking);
                            //Them diem vao lop moi
                            newPupilRanking = new PupilRanking();
                            newPupilRanking.PupilID = pupilRanking.PupilID;
                            newPupilRanking.ClassID = classMovement.ToClassID;
                            newPupilRanking.SchoolID = pupilRanking.SchoolID;
                            newPupilRanking.AcademicYearID = pupilRanking.AcademicYearID;
                            newPupilRanking.EducationLevelID = pupilRanking.EducationLevelID;
                            newPupilRanking.CreatedAcademicYear = pupilRanking.CreatedAcademicYear;
                            newPupilRanking.Semester = pupilRanking.Semester;
                            newPupilRanking.PeriodID = pupilRanking.PeriodID;
                            newPupilRanking.AverageMark = pupilRanking.AverageMark;
                            newPupilRanking.TotalAbsentDaysWithoutPermission = pupilRanking.TotalAbsentDaysWithoutPermission;
                            newPupilRanking.TotalAbsentDaysWithPermission = pupilRanking.TotalAbsentDaysWithPermission;
                            newPupilRanking.CapacityLevelID = pupilRanking.CapacityLevelID;
                            newPupilRanking.ConductLevelID = pupilRanking.ConductLevelID;
                            newPupilRanking.Rank = pupilRanking.Rank;
                            newPupilRanking.RankingDate = pupilRanking.RankingDate;
                            newPupilRanking.StudyingJudgementID = pupilRanking.StudyingJudgementID;
                            newPupilRanking.IsCategory = pupilRanking.IsCategory;
                            newPupilRanking.IsOldData = false;

                            PupilRankingBusiness.Insert(newPupilRanking);
                        }
                    }
                    #endregion

                    #region pupilEmulation
                    lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(classMovement.SchoolID, SearchMark).ToList();
                    if (lstPupilEmulation.Count != 0)
                    {
                        PupilEmulation newPupilEmulation = new PupilEmulation();
                        foreach (PupilEmulation pupilEmulation in lstPupilEmulation)
                        {
                            //Them diem vao lop moi
                            newPupilEmulation = new PupilEmulation();
                            newPupilEmulation.PupilID = pupilEmulation.PupilID;
                            newPupilEmulation.ClassID = classMovement.ToClassID;
                            newPupilEmulation.SchoolID = pupilEmulation.SchoolID;
                            newPupilEmulation.AcademicYearID = pupilEmulation.AcademicYearID;
                            newPupilEmulation.Year = pupilEmulation.Year;
                            newPupilEmulation.Semester = pupilEmulation.Semester;
                            newPupilEmulation.HonourAchivementTypeID = pupilEmulation.HonourAchivementTypeID;
                            newPupilEmulation.CreatedDate = pupilEmulation.CreatedDate;
                            newPupilEmulation.Last2digitNumberSchool = partitionId;
                            PupilEmulationBusiness.Insert(newPupilEmulation);
                        }
                    }
                    #endregion

                    #region conductEvaluationDetail
                    lstConductEvaluationDetail = ConductEvaluationDetailBusiness.SearchBySchool(classMovement.SchoolID, SearchMark).ToList();
                    if (lstConductEvaluationDetail.Count != 0)
                    {
                        ConductEvaluationDetail newConductEvaluationDetail = new ConductEvaluationDetail();
                        foreach (ConductEvaluationDetail conductEvaluationDetail in lstConductEvaluationDetail)
                        {
                            //Them diem vao lop moi
                            newConductEvaluationDetail = new ConductEvaluationDetail();
                            newConductEvaluationDetail.PupilID = conductEvaluationDetail.PupilID;
                            newConductEvaluationDetail.ClassID = classMovement.ToClassID;
                            newConductEvaluationDetail.SchoolID = conductEvaluationDetail.SchoolID;
                            newConductEvaluationDetail.AcademicYearID = conductEvaluationDetail.AcademicYearID;

                            newConductEvaluationDetail.Semester = conductEvaluationDetail.Semester;
                            newConductEvaluationDetail.Task1Evaluation = conductEvaluationDetail.Task1Evaluation;
                            newConductEvaluationDetail.Task2Evaluation = conductEvaluationDetail.Task2Evaluation;
                            newConductEvaluationDetail.Task3Evaluation = conductEvaluationDetail.Task3Evaluation;
                            newConductEvaluationDetail.Task4Evaluation = conductEvaluationDetail.Task4Evaluation;
                            newConductEvaluationDetail.Task5Evaluation = conductEvaluationDetail.Task5Evaluation;
                            newConductEvaluationDetail.EvaluatedDate = conductEvaluationDetail.EvaluatedDate;
                            newConductEvaluationDetail.UpdatedDate = conductEvaluationDetail.UpdatedDate;

                            ConductEvaluationDetailBusiness.Insert(newConductEvaluationDetail);
                        }
                    }
                    #endregion
                }
            }
        }

        public void UpdateClassMovement(int UserID, ClassMovement classMovement)
        {
            this.Validate(classMovement, false);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Update giá trị mới assignDate trong bảng PupilOfClass cho lớp mới

            dic["PupilID"] = classMovement.PupilID;
            dic["SchoolID"] = classMovement.SchoolID;
            dic["ClassID"] = classMovement.ToClassID;
            dic["AcademicYearID"] = classMovement.AcademicYearID;
            PupilOfClass item = PupilOfClassBusiness.SearchBySchool(classMovement.SchoolID, dic).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
            bool isAvaiable = UtilsBusiness.HasHeadTeacherPermission(UserID, classMovement.FromClassID);
            if (item != null)
            {
                if (isAvaiable)
                {
                    item.AssignedDate = classMovement.MovedDate;
                    PupilOfClassBusiness.Update(item);
                }
            }

            //Đồng thời Update giá trị cũ EndDate trong bảng PupilOfClass cho lớp cũ.
            dic = new Dictionary<string, object>();
            dic["PupilID"] = classMovement.PupilID;
            dic["SchoolID"] = classMovement.SchoolID;
            dic["AcademicYearID"] = classMovement.AcademicYearID;
            dic["ClassID"] = classMovement.FromClassID;
            PupilOfClass itemOld = PupilOfClassBusiness.SearchBySchool(classMovement.SchoolID, dic).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
            if (itemOld != null)
            {
                if (isAvaiable)
                {
                    itemOld.EndDate = classMovement.MovedDate;
                    PupilOfClassBusiness.Update(itemOld);
                }
            }

            this.Update(classMovement);
        }

        #endregion

        #region Delete
        /// <summary>
        /// Xóa thông tin học sinh chuyển lớp
        /// <author>minhh</author>
        /// </summary>
        /// <param name="UserID">ID người dùng</param>
        /// <param name="ClassMovementID">ID chuyển lớp</param>
        /// <param name="SchoolID">ID trường học</param>
        public void DeleteClassMovement(int UserID, int ClassMovementID, int SchoolID)
        {
            ClassMovement classMovement = this.ClassMovementRepository.Find(ClassMovementID);
            AcademicYear academicYear = AcademicYearRepository.Find(classMovement.AcademicYearID);
            PupilProfile pupilProfile = PupilProfileBusiness.Find(classMovement.PupilID);

            //PupilID, SchoolID: not compatible(PupilProfile)
            bool PupilSchoolCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", new Dictionary<string, object>() {
                {"PupilProfileID",  classMovement.PupilID},
                {"CurrentSchoolID", SchoolID}
            }, null);
            if (!PupilSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ClassMovementID: PK(ClassMovement)
            this.CheckAvailable(ClassMovementID, "ClassMovement_Label");
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(classMovement.AcademicYearID))
            {
                throw new BusinessException("ClassMovement_Validate_IsNotCurrentYear");
            }
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
            if (classMovement.PupilID != 0)
            {
                List<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All.Where(o => o.PupilID == classMovement.PupilID).ToList();
                foreach (PupilOfClass item in lstPupilOfClass)
                {
                    if (item.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        throw new BusinessException("ClassMovement_Validate_PupilNotWorking");
                    }
                }
            }
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, classMovement.FromClassID))
            {
                //Thực hiện xoá trong bảng ClassMovement
                base.Delete(ClassMovementID);
                //Cập nhật vào bảng PupilProfile với ProfileStatus = 1: Đang học, CurrentClassID = FromClassID
                pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                pupilProfile.CurrentClassID = classMovement.FromClassID;
                PupilProfileBusiness.Update(pupilProfile);

                //Cập nhật thêm mới thông tin trong bảng PupilOfClass: chuyển trạng thái ở lớp cũ về đang học
                IDictionary<string, object> SearchOldPupilOfClass = new Dictionary<string, object>();
                SearchOldPupilOfClass["ClassID"] = classMovement.FromClassID;
                SearchOldPupilOfClass["PupilID"] = classMovement.PupilID;
                SearchOldPupilOfClass["Status"] = SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS;
                PupilOfClass PupilOfOldClass = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchOldPupilOfClass).OrderByDescending(o => o.PupilOfClassID).First();
                if (PupilOfOldClass != null)
                {
                    PupilOfOldClass.EndDate = null;
                    PupilOfOldClass.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
                    PupilOfClassBusiness.UpdatePupilOfClass(UserID, PupilOfOldClass);
                }
                //Xoá thông tin về học sinh ở lớp mới trong bảng PupilOfClass
                IDictionary<string, object> SearchNewPupilOfClass = new Dictionary<string, object>();
                SearchNewPupilOfClass["ClassID"] = classMovement.ToClassID;
                SearchNewPupilOfClass["PupilID"] = classMovement.PupilID;
                SearchNewPupilOfClass["Status"] = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                PupilOfClass PupilOfNewClass = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchOldPupilOfClass).OrderByDescending(o => o.PupilOfClassID).First(); ;
                if (PupilOfNewClass != null)
                {
                    PupilOfClassBusiness.Delete(PupilOfNewClass.PupilOfClassID);
                }
                // Xoá điểm của học sinh:
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["PupilID"] = classMovement.PupilID;
                SearchInfo["AcademicYearID"] = classMovement.AcademicYearID;
                SearchInfo["ClassID"] = classMovement.ToClassID;
                SearchInfo["Year"] = academicYear.Year;  // AnhVD 20131217
                List<MarkRecord> lstMarkRecord = MarkRecordBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
                if (lstMarkRecord.Count > 0)
                {
                    MarkRecordBusiness.DeleteAll(lstMarkRecord);
                }
                List<JudgeRecord> lstJudgeRecord = JudgeRecordBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
                if (lstJudgeRecord.Count > 0)
                {
                    JudgeRecordBusiness.DeleteAll(lstJudgeRecord);
                }
                List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
                if (lstSummedUpRecord.Count > 0)
                {
                    SummedUpRecordBusiness.DeleteAll(lstSummedUpRecord);
                }
                List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
                if (lstPupilRanking.Count > 0)
                {
                    PupilRankingBusiness.DeleteAll(lstPupilRanking);
                }

            }

        }
        #endregion

        public List<ClassMovement> GetListClassMovement(int AcademicYearID, int SchoolID, int Semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["Semester"] = Semester;
            if (AcademicYearID == 0) return null;
            if (Semester == 0) return null;
            IQueryable<ClassMovement> query = SearchBySchool(SchoolID, dic);
            return query.ToList();
        }

        public int MaxOrderInClass(int SchoolID, int AcademicYearID, int ToClassID)
        {
            int OrderInClass = 0;
            try
            {
                List<PupilOfClass> pocList = PupilOfClassBusiness.All.Where(p => p.AcademicYearID == AcademicYearID && p.SchoolID == SchoolID && p.ClassID == ToClassID).ToList();
                if (pocList != null && pocList.Count > 0)
                {
                    OrderInClass = pocList.Max(p => p.OrderInClass) ?? 0;
                }
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("AcademicYearID={0}, SchoolID={1}, ToClassID={2}", AcademicYearID, SchoolID, ToClassID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "MaxOrderInClass", paramList, ex);
            }
            return OrderInClass + 1;
        }

        public IQueryable<ClassMovementBO> ListClassMovement(int schoolID, int academicYearID)
        {
            var lstCM = (from cm in ClassMovementBusiness.All
                         join pp in PupilProfileBusiness.All on cm.PupilID equals pp.PupilProfileID
                         where pp.IsActive == true
                               && cm.SchoolID == schoolID
                               && cm.AcademicYearID == academicYearID
                         select new ClassMovementBO
                         {
                             PupilID = pp.PupilProfileID,
                             FromClassID = cm.FromClassID,
                             ToClassID = cm.ToClassID,
                             MovedDate = cm.MovedDate,
                             Genre = pp.Genre,
                             EthnicID = pp.EthnicID
                         });

            return lstCM;
        }

    }
}
