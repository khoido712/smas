﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class AverageMarkBySubjectBusiness
    {

        #region Lấy mảng băm cho các tham số đầu vào của thống kê điểm trung bình môn
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê điểm trung bình môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKey(AverageMarkBySubjectBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"MarkType", entity.MarkType},
                {"MarkColumn", entity.MarkColumn},
                {"EmployeeID", entity.EmployeeID},
                {"IsPeriod", entity.IsPeriod},
                {"PeriodID",entity.PeriodID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion


        #region Lấy thống kê điểm trung bình môn được cập nhật mới nhất
        /// <summary>
        /// Lấy thống kê điểm trung bình môn được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetAverageMarkBySubject(AverageMarkBySubjectBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport GetAverageMarkBySubjectJudge(AverageMarkBySubjectBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TB_THEOMON_NHANXET;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport GetAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO entity)
        {
            //HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV
            //string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON_THEO_GV;
            string reportCode = SystemParamsInFile.HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport GetAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON_THEO_GV_NHANXET;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion


        #region Lưu lại thông tin của thống kê điểm trung bình môn
        /// <summary>
        /// Lưu lại thông tin của thống kê điểm trung bình môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertAverageMarkBySubject(AverageMarkBySubjectBO entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeTBMon_[Học kỳ]_[Môn học]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string Subject = this.SubjectCatBusiness.All.Where(o => o.SubjectCatID == entity.SubjectID).FirstOrDefault().DisplayName;

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            //Start: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            if (entity.MarkType == 1)
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemKTDK");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemTBM");
            }
            if (entity.EmployeeID > 0)
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(EmployeeBusiness.Find(entity.EmployeeID).FullName));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", String.Empty);
            }
            if (outputNamePattern.EndsWith("_"))
            {
                outputNamePattern = outputNamePattern.Remove(outputNamePattern.Length - 1, 1);
            }
            //End: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion


        #region Lưu lại thông tin thống kê điểm trung bình môn
        /// <summary>
        /// Lưu lại thông tin thống kê điểm trung bình môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateAverageMarkBySubject(AverageMarkBySubjectBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile objSP = SchoolProfileBusiness.Find(entity.SchoolID);
            string SchoolName = objSP.SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string Province = (objSP.District != null ? objSP.District.DistrictName : "");
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }


            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy danh sách mức thống kê
            int StatisticLevelReportID = entity.StatisticLevelReportID;
            List<StatisticLevelConfig> lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            int numberConfig = lstStatisticLevel.Count();
            int lastColumn = numberConfig * 2 + 4;
            //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange range = firstSheet.GetRange(16, 1, 16, lastColumn);
            IVTRange rangeClass = firstSheet.GetRange(14, 1, 14, lastColumn);
            IVTRange rangeLastClass = firstSheet.GetRange(17, 1, 17, lastColumn);
            IVTRange userReport = firstSheet.GetRange(19, 1, 19, lastColumn);
            IVTRange range_School = firstSheet.GetRange(13, 1, 13, lastColumn);
            IVTRange range_level = firstSheet.GetRange("E11", "F12");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "P12");
            int col = 0;
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_level, 11, 5 + col);
                sheet.SetCellValue(11, 5 + col, lstStatisticLevel[i].Title);
                col += 2;
            }
            //Fill dữ liệu chung
            sheet.Name = "Tat_ca";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN"));
            sheet.SetCellValue("H5", Province + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - NĂM HỌC: " + AcademicYear);
            }
            else
            {
                sheet.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            }

            //Neu chon giao vien, dat tieu de cho giao vien
            if (entity.EmployeeID > 0)
            {
                sheet.SetCellValue("A9", "Giáo viên: " + EmployeeBusiness.Find(entity.EmployeeID).FullName);
            }
            else
            {
                sheet.SetCellValue("A9", String.Empty);
            }

            // Lấy danh sách khối học

            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();

            // Điểm môn tính điểm

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SubjectID", entity.SubjectID},
                {"PeriodID", null}
            };

            //Danh sach hoc sinh trong truong
            IDictionary<string, object> dicSchool = new Dictionary<string, object>();
            dicSchool["AcademicYearID"] = entity.AcademicYearID;
            //Danh sach diem neu thong ke trung binh mon
            IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = null;
            //Danh sach diem neu thong ke diem kiem tra dinh ky
            IQueryable<VMarkRecord> lstMarkRecord = null;

            if (entity.MarkType == 2)
            {
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null);
            }
            else
            {
                Dictionary<string, object> dicToGetMarkRecord = new Dictionary<string, object>();
                dicToGetMarkRecord["AcademicYearID"] = entity.AcademicYearID;
                dicToGetMarkRecord["SubjectID"] = entity.SubjectID;
                dicToGetMarkRecord["Semester"] = entity.Semester;
                dicToGetMarkRecord["Title"] = entity.MarkColumn;

                lstMarkRecord = this.VMarkRecordBusiness.SearchBySchool(entity.SchoolID, dicToGetMarkRecord);
            }

            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);

            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.All.Where(s => s.Last2digitNumberSchool == partitionId && (s.IsSubjectVNEN == null || s.IsSubjectVNEN == false));
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekFirstSemester > 0);
            }
            else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekSecondSemester > 0);
            }
            else
            {
                iqClassSubject = iqClassSubject.Where(s => (s.SectionPerWeekSecondSemester > 0 || s.SectionPerWeekFirstSemester > 0));
            }
            if (entity.SubjectID > 0)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SubjectID == entity.SubjectID);
            }
            IQueryable<PupilOfClassBO> lstQPoc = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                  join cs in iqClassSubject on p.ClassID equals cs.ClassID
                                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                  join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                                  where
                                                  q.AcademicYearID == entity.AcademicYearID
                                                  && q.IsActive.Value

                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      EducationLevelID = q.EducationLevelID,
                                                      ClassID = p.ClassID,
                                                      Genre = r.Genre,
                                                      EthnicID = r.EthnicID,
                                                      AppliedType = cs.AppliedType,
                                                      IsSpecialize = cs.IsSpecializedSubject,
                                                      SubjectID = cs.SubjectID
                                                  });

            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",AcademicYearBusiness.Find(entity.AcademicYearID).Year}
                                               };

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID, 0);

            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                //Khong lay cac hoc sinh mien giam
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID)).ToList();
            }

            List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_FemaleEthnic = new List<SummedUpRecordBO>();

            List<MarkRecordBO> lstMarkRecordBO = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_Female = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_Ethnic = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_FemaleEthnic = new List<MarkRecordBO>();
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            if (entity.MarkType == 2)
            {
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                var tmpSummedUp = from u in lstSummedUpRecordAllStatus
                                  join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                  join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                  where c.IsActive.Value
                                  //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select new
                                  {
                                      PupilID = u.PupilID,
                                      ClassID = u.ClassID,
                                      SubjectID = u.SubjectID,
                                      Semester = u.Semester,
                                      SummedUpMark = u.SummedUpMark,
                                      ReTestMark = u.ReTestMark,
                                      EducationLevelID = c.EducationLevelID,
                                      Genre = v.Genre,
                                      EthnicID = v.EthnicID
                                  };
                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);
                if (entity.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                    var iqSummedUpFilter = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester))
                        .Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SummedUpMark = u.ReTestMark != null ? u.ReTestMark : u.SummedUpMark });
                    lstSummedUpRecord = iqSummedUpFilter.ToList();


                }
                else
                {
                    lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SummedUpMark = u.SummedUpMark }).ToList();
                }
                //Khong tinh cac hoc sinh mien giam
                if (lstExempSubject != null && lstExempSubject.Count > 0)
                {
                    lstSummedUpRecord = lstSummedUpRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID)).ToList();
                }
                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_FemaleEthnic = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            }
            else
            {
                //Lay danh sach diem neu la thong ke theo Kiem tra dinh ky
                var tmpMarkRecord = from u in lstMarkRecord
                                    join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                    join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                    where c.IsActive.Value
                                    //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                    select new
                                    {
                                        PupilID = u.PupilID,
                                        ClassID = u.ClassID,
                                        SubjectID = u.SubjectID,
                                        Semester = u.Semester,
                                        Mark = u.Mark,
                                        EducationLevelID = c.EducationLevelID,
                                        Genre = v.Genre,
                                        EthnicID = v.EthnicID,
                                        MarkRecordID = u.MarkRecordID
                                    };
                var iqMarkRecord = (from u in tmpMarkRecord.ToList()
                                    join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                    select u);
                lstMarkRecordBO = iqMarkRecord.Select(u => new MarkRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, Mark = u.Mark, MarkRecordID = u.MarkRecordID }).ToList();
                //Khong tinh cac hoc sinh mien giam
                if (lstExempSubject != null && lstExempSubject.Count > 0)
                {
                    lstMarkRecordBO = lstMarkRecordBO.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID)).ToList();
                }
                listMarkRecordBO_Ethnic = lstMarkRecordBO.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                listMarkRecordBO_Female = lstMarkRecordBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                listMarkRecordBO_FemaleEthnic = lstMarkRecordBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            }
            // Lay danh sach hoc sinh theo lop
            var listCountCurentPupilByClass = lstPOC.GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountCurentPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_FemaleEthnic = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            int startrow = 15;
            int indexCol = 1;
            IDictionary<string, object> dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID},
                                                                    {"IsVNEN",true}
                                                                };
            // List Class Profile
            IQueryable<ClassSubject> iqClassProfile_Temp = this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dic_ClassProfile);
            List<ClassProfileTempBO> lstClassProfileAllEdu = (from cs in iqClassProfile_Temp
                                                              join c in ClassProfileBusiness.All on cs.ClassID equals c.ClassProfileID
                                                              join em in EmployeeBusiness.All on c.HeadTeacherID equals em.EmployeeID into g
                                                              from j in g.DefaultIfEmpty()
                                                              orderby c.OrderNumber.HasValue ? c.OrderNumber : 0, c.DisplayName
                                                              where c.IsActive.Value
                                                              select new ClassProfileTempBO
                                                              {
                                                                  EducationLevelID = c.EducationLevelID,
                                                                  ClassProfileID = cs.ClassID,
                                                                  OrderNumber = c.OrderNumber,
                                                                  DisplayName = c.DisplayName,
                                                                  EmployeeName = j.FullName != null ? j.FullName : string.Empty
                                                              }).ToList();
            //Neu nhap giao vien, loc danh sach cac lop ma giao vien do day
            //Danh sach phan cong giang day
            if (entity.EmployeeID > 0)
            {
                Dictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
                dicToGetAssignment["TeacherID"] = entity.EmployeeID;
                dicToGetAssignment["AcademicYearID"] = entity.AcademicYearID;
                dicToGetAssignment["SubjectID"] = entity.SubjectID;
                IQueryable<TeachingAssignment> lstTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(entity.SchoolID, dicToGetAssignment);

                List<ClassSubject> lstClassSubject = (from cs in iqClassSubject
                                                      join ts in lstTeachingAssignment on cs.ClassID equals ts.ClassID
                                                      where cs.SubjectID == ts.SubjectID
                                                        //Viethd4: Loai cac lop hoc mon nay theo VNEN
                                                       
                                                      //&& ((entity.Semester > 2 && (cs.SectionPerWeekFirstSemester.HasValue
                                                      // || cs.SectionPerWeekSecondSemester.HasValue)) || ts.Semester == entity.Semester)
                                                      // && cs.Last2digitNumberSchool == partitionId
                                                      select cs).ToList();



                //Chiendd: loai cac ban ghi trung nhau
                lstClassProfileAllEdu = (from c in lstClassProfileAllEdu
                                         join cs in lstClassSubject on c.ClassProfileID equals cs.ClassID
                                         select c).Distinct().ToList();
            }
            List<ClassProfileTempBO> lstClassProfile = new List<ClassProfileTempBO>();
            List<SummedUpRecordBO> lstSummedUpRecordEdu = new List<SummedUpRecordBO>();
            List<MarkRecordBO> lstMarkRecordEdu = new List<MarkRecordBO>();
            List<decimal?> lstSummedUpRecordClass = new List<decimal?>();
            int EducationLevelID = 0;
            int SL = 0;

            #region thống kê điểm trung bình môn
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int startmin = startrow;
                int startrowSum = startrow - 1;
                EducationLevelID = lstEducationLevel[i].EducationLevelID;
                lstClassProfile = lstClassProfileAllEdu.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                if (entity.MarkType == 2)
                {
                    lstSummedUpRecordEdu = lstSummedUpRecord.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                }
                else
                {
                    lstMarkRecordEdu = lstMarkRecordBO.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                }
                for (int j = 0; j < lstClassProfile.Count; j++)
                {
                    int ClassID = lstClassProfile[j].ClassProfileID;
                    col = 0;
                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecordClass = lstSummedUpRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                    }
                    else
                    {
                        lstSummedUpRecordClass = lstMarkRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                    }
                    int startrowClass = startrow + 1;
                    if (j == lstClassProfile.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeLastClass, startrow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(range, startrow);
                    }

                    sheet.SetCellValue(startrow, 1, indexCol++);
                    sheet.SetCellValue(startrow, 2, lstClassProfile[j].DisplayName);
                    sheet.SetCellValue(startrow, 3, lstClassProfile[j].EmployeeName);
                    SL = 0;
                    if (listCountCurentPupilByClass.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                    {
                        sheet.SetCellValue(startrow, 4, listCountCurentPupilByClass.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevel[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                    sheet.SetCellValue(startrow, 5 + col, SL);//E
                                    sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                }
                            }
                            col += 2;
                        }
                        startrow++;
                    }
                    else
                    {
                        sheet.SetCellValue(startrow, 4, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet.SetCellValue(startrow, 5 + col, SL);//E
                            sheet.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                            col += 2;
                        }
                        startrow++;
                    }
                }
                if (lstEducationLevel.Count > 1)
                {
                    sheet.CopyPasteSameSize(rangeClass, startrowSum, 1);
                }
                sheet.SetCellValue(startrowSum, 1, i + 1);
                sheet.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);


                col = 0;
                if (lstClassProfile.Count > 0)
                {
                    sheet.SetCellValue(startrowSum, 4, "=SUM(D" + startmin.ToString() + ":D" + (startrow - 1).ToString() + ")");
                    for (int m = 0; m < numberConfig; m++)
                    {
                        string column = UtilsBusiness.GetExcelColumnName(5 + col);
                        sheet.SetCellValue(startrowSum, 5 + col, "=SUM(" + column + startmin.ToString() + ":" + column + (startrow - 1).ToString() + ")");//E
                        sheet.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                        col += 2;
                    }
                }
                else
                {
                    sheet.SetCellValue(startrowSum, 4, 0);
                    for (int m = 0; m < numberConfig; m++)
                    {
                        string column = UtilsBusiness.GetExcelColumnName(5 + col);
                        sheet.SetCellValue(startrowSum, 5 + col, 0);//E
                        sheet.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                        col += 2;
                    }
                }
                startrow++;
            }

            // fill vào excel dữ liệu row tổng của trường
            int startRow_School = 13;
            sheet.CopyPasteSameSize(range_School, 13, 1);
            sheet.SetCellValue(13, 1, "Tổng cộng");
            sheet.SetCellValue(13, 4, "=SUM(D" + 14.ToString() + ":D" + (startrow - 1).ToString() + ")/2");//E
            //sheet.SetCellValue(11, 4, 2);
            col = 0;

            for (int i = 0; i < numberConfig; i++)
            {
                string column = UtilsBusiness.GetExcelColumnName(5 + col);
                sheet.SetCellValue(13, 5 + col, "=SUM(" + column + 14.ToString() + ":" + column + (startrow - 1).ToString() + ")/2");//E
                sheet.SetCellValue(13, 6 + col, "=IF(D13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow_School + "/" + "D" + startRow_School + "*100,2),0)");
                col += 2;
            }
            if (startrow > 17)
                sheet.CopyAndInsertARow(userReport, startrow + 1);
            sheet.GetRange(11, (numberConfig) * 2 + 4, 13 + lstEducationLevel.Count() + lstClassProfileAllEdu.Count(), (numberConfig) * 2 + 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);

            //Xoá sheet template
            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            if (entity.FemaleChecked)
            {
                #region Thống kê điểm trung bình môn học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(sheet);
                sheet_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ");
                sheet_Female.Name = "HS_Nu";
                startrow = 15;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startmin = startrow;
                    int startrowSum = startrow - 1;
                    EducationLevelID = lstEducationLevel[i].EducationLevelID;
                    lstClassProfile = lstClassProfileAllEdu.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecordEdu = lstSummedUpRecord_Female.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                    else
                    {
                        lstMarkRecordEdu = listMarkRecordBO_Female.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int ClassID = lstClassProfile[j].ClassProfileID;
                        col = 0;
                        if (entity.MarkType == 2)
                        {
                            lstSummedUpRecordClass = lstSummedUpRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                        }
                        else
                        {
                            lstSummedUpRecordClass = lstMarkRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                        }
                        int startrowClass = startrow + 1;
                        SL = 0;
                        if (listCountCurentPupilByClass_Female.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            sheet_Female.SetCellValue(startrow, 4, listCountCurentPupilByClass_Female.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                col += 2;
                            }
                            startrow++;
                        }
                        else
                        {
                            sheet_Female.SetCellValue(startrow, 4, 0);
                            for (int m = 0; m < numberConfig; m++)
                            {
                                sheet_Female.SetCellValue(startrow, 5 + col, SL);//E
                                sheet_Female.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                col += 2;
                            }
                            startrow++;
                        }
                    }
                    if (lstEducationLevel.Count > 1)
                    {
                        sheet_Female.CopyPasteSameSize(rangeClass, startrowSum, 1);
                    }
                    sheet_Female.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Female.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);


                    col = 0;
                    if (lstClassProfile.Count > 0)
                    {
                        sheet_Female.SetCellValue(startrowSum, 4, "=SUM(D" + startmin.ToString() + ":D" + (startrow - 1).ToString() + ")");
                        for (int m = 0; m < numberConfig; m++)
                        {
                            string column = UtilsBusiness.GetExcelColumnName(5 + col);
                            sheet_Female.SetCellValue(startrowSum, 5 + col, "=SUM(" + column + startmin.ToString() + ":" + column + (startrow - 1).ToString() + ")");//E
                            sheet_Female.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                            col += 2;
                        }
                    }
                    else
                    {
                        sheet_Female.SetCellValue(startrowSum, 4, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            string column = UtilsBusiness.GetExcelColumnName(5 + col);
                            sheet_Female.SetCellValue(startrowSum, 5 + col, 0);//E
                            sheet_Female.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                            col += 2;
                        }
                    }
                    startrow++;
                }
                // fill vào excel dữ liệu row tổng của trường
                startRow_School = 13;
                sheet_Female.CopyPasteSameSize(range_School, 13, 1);
                sheet_Female.SetCellValue(13, 1, "Tổng cộng");
                sheet_Female.SetCellValue(13, 4, "=SUM(D" + 14.ToString() + ":D" + (startrow - 1).ToString() + ")/2");//E
                //sheet.SetCellValue(11, 4, 2);
                col = 0;

                for (int i = 0; i < numberConfig; i++)
                {
                    string column = UtilsBusiness.GetExcelColumnName(5 + col);
                    sheet_Female.SetCellValue(13, 5 + col, "=SUM(" + column + 14.ToString() + ":" + column + (startrow - 1).ToString() + ")/2");//E
                    sheet_Female.SetCellValue(13, 6 + col, "=IF(D13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow_School + "/" + "D" + startRow_School + "*100,2),0)");
                    col += 2;
                }


                sheet_Female.GetRange(11, (numberConfig) * 2 + 4, 13 + lstEducationLevel.Count() + lstClassProfileAllEdu.Count(), (numberConfig) * 2 + 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                //Xoá sheet_Female template
                sheet_Female.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.EthnicChecked)
            {
                #region Thống kê điểm trung bình môn học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(sheet);
                sheet_Ethnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH DÂN TỘC");
                sheet_Ethnic.Name = "HS_DT";
                startrow = 15;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startmin = startrow;
                    int startrowSum = startrow - 1;
                    EducationLevelID = lstEducationLevel[i].EducationLevelID;
                    lstClassProfile = lstClassProfileAllEdu.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecordEdu = lstSummedUpRecord_Ethnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                    else
                    {
                        lstMarkRecordEdu = listMarkRecordBO_Ethnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int ClassID = lstClassProfile[j].ClassProfileID;
                        col = 0;
                        if (entity.MarkType == 2)
                        {
                            lstSummedUpRecordClass = lstSummedUpRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                        }
                        else
                        {
                            lstSummedUpRecordClass = lstMarkRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                        }
                        int startrowClass = startrow + 1;
                        SL = 0;
                        if (listCountCurentPupilByClass_Ethnic.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            sheet_Ethnic.SetCellValue(startrow, 4, listCountCurentPupilByClass_Ethnic.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                col += 2;
                            }
                            startrow++;
                        }
                        else
                        {
                            sheet_Ethnic.SetCellValue(startrow, 4, 0);
                            for (int m = 0; m < numberConfig; m++)
                            {
                                sheet_Ethnic.SetCellValue(startrow, 5 + col, SL);//E
                                sheet_Ethnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                col += 2;
                            }
                            startrow++;
                        }
                    }
                    if (lstEducationLevel.Count > 1)
                    {
                        sheet_Ethnic.CopyPasteSameSize(rangeClass, startrowSum, 1);
                    }
                    sheet_Ethnic.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Ethnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);


                    col = 0;
                    if (lstClassProfile.Count > 0)
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 4, "=SUM(D" + startmin.ToString() + ":D" + (startrow - 1).ToString() + ")");
                        for (int m = 0; m < numberConfig; m++)
                        {
                            string column = UtilsBusiness.GetExcelColumnName(5 + col);
                            sheet_Ethnic.SetCellValue(startrowSum, 5 + col, "=SUM(" + column + startmin.ToString() + ":" + column + (startrow - 1).ToString() + ")");//E
                            sheet_Ethnic.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                            col += 2;
                        }
                    }
                    else
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 4, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            string column = UtilsBusiness.GetExcelColumnName(5 + col);
                            sheet_Ethnic.SetCellValue(startrowSum, 5 + col, 0);//E
                            sheet_Ethnic.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                            col += 2;
                        }
                    }
                    startrow++;
                }
                // fill vào excel dữ liệu row tổng của trường
                startRow_School = 13;
                sheet_Ethnic.CopyPasteSameSize(range_School, 13, 1);
                sheet_Ethnic.SetCellValue(13, 1, "Tổng cộng");
                sheet_Ethnic.SetCellValue(13, 4, "=SUM(D" + 14.ToString() + ":D" + (startrow - 1).ToString() + ")/2");//E
                //sheet.SetCellValue(11, 4, 2);
                col = 0;

                for (int i = 0; i < numberConfig; i++)
                {
                    string column = UtilsBusiness.GetExcelColumnName(5 + col);
                    sheet_Ethnic.SetCellValue(13, 5 + col, "=SUM(" + column + 14.ToString() + ":" + column + (startrow - 1).ToString() + ")/2");//E
                    sheet_Ethnic.SetCellValue(13, 6 + col, "=IF(D13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow_School + "/" + "D" + startRow_School + "*100,2),0)");
                    col += 2;
                }

                sheet_Ethnic.GetRange(11, (numberConfig) * 2 + 4, 13 + lstEducationLevel.Count() + lstClassProfileAllEdu.Count(), (numberConfig) * 2 + 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                //Xoá sheet_Ethnic template
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region Thống kê điểm trung bình môn học sinh nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(sheet);
                sheet_FemaleEthnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ DÂN TỘC");
                sheet_FemaleEthnic.Name = "HS_Nu_DT";
                startrow = 15;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startmin = startrow;
                    int startrowSum = startrow - 1;
                    EducationLevelID = lstEducationLevel[i].EducationLevelID;
                    lstClassProfile = lstClassProfileAllEdu.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecordEdu = lstSummedUpRecord_FemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                    else
                    {
                        lstMarkRecordEdu = listMarkRecordBO_FemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int ClassID = lstClassProfile[j].ClassProfileID;
                        col = 0;
                        if (entity.MarkType == 2)
                        {
                            lstSummedUpRecordClass = lstSummedUpRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                        }
                        else
                        {
                            lstSummedUpRecordClass = lstMarkRecordEdu.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                        }
                        int startrowClass = startrow + 1;
                        SL = 0;
                        if (listCountCurentPupilByClass_FemaleEthnic.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            sheet_FemaleEthnic.SetCellValue(startrow, 4, listCountCurentPupilByClass_FemaleEthnic.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                    }
                                }
                                col += 2;
                            }
                            startrow++;
                        }
                        else
                        {
                            sheet_FemaleEthnic.SetCellValue(startrow, 4, 0);
                            for (int m = 0; m < numberConfig; m++)
                            {
                                sheet_FemaleEthnic.SetCellValue(startrow, 5 + col, SL);//E
                                sheet_FemaleEthnic.SetCellValue(startrow, 6 + col, "=IF(D" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startrow + "/" + "D" + startrow + "*100,2),0)");
                                col += 2;
                            }
                            startrow++;
                        }
                    }
                    if (lstEducationLevel.Count > 1)
                    {
                        sheet_FemaleEthnic.CopyPasteSameSize(rangeClass, startrowSum, 1);
                    }
                    sheet_FemaleEthnic.SetCellValue(startrowSum, 1, i + 1);
                    sheet_FemaleEthnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);


                    col = 0;
                    if (lstClassProfile.Count > 0)
                    {
                        sheet_FemaleEthnic.SetCellValue(startrowSum, 4, "=SUM(D" + startmin.ToString() + ":D" + (startrow - 1).ToString() + ")");
                        for (int m = 0; m < numberConfig; m++)
                        {
                            string column = UtilsBusiness.GetExcelColumnName(5 + col);
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 5 + col, "=SUM(" + column + startmin.ToString() + ":" + column + (startrow - 1).ToString() + ")");//E
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                            col += 2;
                        }
                    }
                    else
                    {
                        sheet_FemaleEthnic.SetCellValue(startrowSum, 4, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            string column = UtilsBusiness.GetExcelColumnName(5 + col);
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 5 + col, 0);//E
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 6 + col, "=IF(D" + startrowSum + ">0" + "," + "ROUND(" + column + startrowSum + "/" + "D" + startrowSum + "*100,2),0)");
                            col += 2;
                        }
                    }
                    startrow++;
                }
                // fill vào excel dữ liệu row tổng của trường
                startRow_School = 13;
                sheet_FemaleEthnic.CopyPasteSameSize(range_School, 13, 1);
                sheet_FemaleEthnic.SetCellValue(13, 1, "Tổng cộng");
                sheet_FemaleEthnic.SetCellValue(13, 4, "=SUM(D" + 14.ToString() + ":D" + (startrow - 1).ToString() + ")/2");//E
                //sheet.SetCellValue(11, 4, 2);
                col = 0;

                for (int i = 0; i < numberConfig; i++)
                {
                    string column = UtilsBusiness.GetExcelColumnName(5 + col);
                    sheet_FemaleEthnic.SetCellValue(13, 5 + col, "=SUM(" + column + 14.ToString() + ":" + column + (startrow - 1).ToString() + ")/2");//E
                    sheet_FemaleEthnic.SetCellValue(13, 6 + col, "=IF(D13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow_School + "/" + "D" + startRow_School + "*100,2),0)");
                    col += 2;
                }

                sheet_FemaleEthnic.GetRange(11, (numberConfig) * 2 + 4, 13 + lstEducationLevel.Count() + lstClassProfileAllEdu.Count(), (numberConfig) * 2 + 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                //Xoá sheet_Ethnic template
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                #endregion
            }
            firstSheet.Delete();

            //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            return oBook.ToStream();
        }
        #endregion

        public Stream CreateAverageMarkBySubjectJudge(AverageMarkBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TB_THEOMON_NHANXET;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile school = this.SchoolProfileBusiness.Find(entity.SchoolID);
            string SchoolName = school.SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = this.SchoolProfileBusiness.Find(entity.SchoolID).SupervisingDept.SupervisingDeptName.ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }


            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);


            // Lấy danh sách khối học
            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);

            IDictionary<string, object> dic = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                 {"PeriodID", null}
                                                            };

            IQueryable<VJudgeRecord> lstJudgeRecordAllStatus = null;
            IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = null;
            //Neu thong ke diem TBM
            if (entity.MarkType == 2)
            {
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            //Neu thong ke diem kiem tra dinh ky
            else
            {
                Dictionary<string, object> dicToGetJudgeRecord = new Dictionary<string, object>();
                dicToGetJudgeRecord["AcademicYearID"] = entity.AcademicYearID;
                dicToGetJudgeRecord["SubjectID"] = entity.SubjectID;
                dicToGetJudgeRecord["Semester"] = entity.Semester;
                dicToGetJudgeRecord["Title"] = entity.MarkColumn;

                lstJudgeRecordAllStatus = this.VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, dicToGetJudgeRecord);
            }

            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            int partittionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            IQueryable<PupilOfClassBO> lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                 join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where cs.SubjectID == entity.SubjectID
                                                 && cs.Last2digitNumberSchool == partittionId
                                                  //Viethd4: loai bo cac lop hoc mon theo VNEN
                                                  && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     Genre = q.Genre,
                                                     IsSpecialize = cs.IsSpecializedSubject,
                                                     AppliedType = cs.AppliedType,
                                                     SubjectID = cs.SubjectID,
                                                     EthnicID = q.EthnicID,
                                                     EducationLevelID = q.ClassProfile.EducationLevelID
                                                 };
            //loc ra nhung hoc sinh khong dang ky hoc mon chuyen/tu chon tuong ung
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SemesterID",entity.Semester},
                {"SubjectID",entity.SubjectID},
                {"YearID",Aca.Year}
            };
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);


            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID, 0);
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                //Khong lay cac hoc sinh mien giam
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID)).ToList();
            }

            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            var listCountPupilByClass = lstPOC.GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountPupilByClass_EthnicFemale = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });

            List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_EthnicFemale = new List<SummedUpRecordBO>();

            List<JudgeRecordBO> lstJudgeRecord = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_Female = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_Ethnic = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_EthnicFemale = new List<JudgeRecordBO>();


            if (entity.MarkType == 2)
            {
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky

                var tmpSummedUp = (from u in lstSummedUpRecordAllStatus
                                   join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                   join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                   where u.SubjectID == entity.SubjectID
                                   && c.IsActive.Value
                                   //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                   select new
                                   {
                                       PupilID = u.PupilID,
                                       ClassID = u.ClassID,
                                       SubjectID = u.SubjectID,
                                       Semester = u.Semester,
                                       JudgementResult = u.JudgementResult,
                                       ReTestJudgement = u.ReTestJudgement,
                                       ReTestMark = u.ReTestMark,
                                       EducationLevelID = c.EducationLevelID,
                                       Genre = v.Genre,
                                       EthnicID = v.EthnicID
                                   });

                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);


                if (entity.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                    var iqSummedUpFilter = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester))
                        .Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, JudgementResult = u.ReTestJudgement != null ? u.ReTestJudgement : u.JudgementResult });
                    lstSummedUpRecord = iqSummedUpFilter.ToList();

                }
                else
                {
                    lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, JudgementResult = u.JudgementResult }).ToList();
                }

                //Khong lay cac hoc sinh mien giam
                if (lstExempSubject != null && lstExempSubject.Count > 0)
                {
                    lstSummedUpRecord = lstSummedUpRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID)).ToList();
                }
                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_EthnicFemale = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            }
            else
            {
                //Lay danh sach diem neu la thong ke theo Kiem tra dinh ky
                var tmpJudgeRecord = from u in lstJudgeRecordAllStatus
                                     join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                     join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                     where c.IsActive.Value
                                     //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                     select new
                                     {
                                         PupilID = u.PupilID,
                                         ClassID = u.ClassID,
                                         SubjectID = u.SubjectID,
                                         Semester = u.Semester,
                                         JudgeMent = u.Judgement,
                                         EducationLevelID = c.EducationLevelID,
                                         Genre = v.Genre,
                                         EthnicID = v.EthnicID,
                                         JudgeRecordID = u.JudgeRecordID
                                     };
                var iqJudgeRecord = (from u in tmpJudgeRecord.ToList()
                                     join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                     select u);
                lstJudgeRecord = iqJudgeRecord.Select(u => new JudgeRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, Judgement = u.JudgeMent, JudgeRecordID = u.JudgeRecordID }).ToList();

                //Khong lay cac hoc sinh mien giam
                if (lstExempSubject != null && lstExempSubject.Count > 0)
                {
                    lstJudgeRecord = lstJudgeRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID)).ToList();
                }
                lstJudgeRecord_Ethnic = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstJudgeRecord_Female = lstJudgeRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstJudgeRecord_EthnicFemale = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            }

            int startschool = 14;
            int startrow = 14;
            IDictionary<string, object> Dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID},
                                                                    {"IsVNEN", true}
                                                                };
            var lstClassAllEdu = (from p in this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                  where q.IsActive.Value
                                  select new
                                  {
                                      ClassID = q.ClassProfileID,
                                      EducationLevelID = q.EducationLevelID,
                                      OrderNumber = q.OrderNumber,
                                      DisplayName = q.DisplayName,
                                      SubjectID = p.SubjectID
                                  }).ToList();
            //Neu nhap giao vien, loc danh sach cac lop ma giao vien do day
            //Danh sach phan cong giang day
            if (entity.EmployeeID > 0)
            {
                Dictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
                dicToGetAssignment["TeacherID"] = entity.EmployeeID;
                dicToGetAssignment["AcademicYearID"] = entity.AcademicYearID;
                dicToGetAssignment["SubjectID"] = entity.SubjectID;
                IQueryable<TeachingAssignment> lstTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(entity.SchoolID, dicToGetAssignment);

                List<ClassSubject> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                      join ts in lstTeachingAssignment on cs.ClassID equals ts.ClassID
                                                      where cs.SubjectID == ts.SubjectID && cs.Last2digitNumberSchool == partittionId
                                                        //Viethd4: Loai cac lop hoc mon nay theo VNEN
                                                        && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                      && ((entity.Semester > 2 && (cs.SectionPerWeekFirstSemester.HasValue
                                                       || cs.SectionPerWeekSecondSemester.HasValue)) || ts.Semester == entity.Semester)
                                                      select cs).ToList();

                lstClassAllEdu = (from c in lstClassAllEdu
                                  join cs in lstClassSubject on c.ClassID equals cs.ClassID
                                  select c).Distinct().ToList();

            }
            lstClassAllEdu = lstClassAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange range = firstSheet.GetRange("A16", "G16");
            IVTRange rangeEducation = firstSheet.GetRange("A14", "G14");
            IVTRange rangeLastClass = firstSheet.GetRange("A17", "G17");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            int classID = 0;
            int HSD, HSCD = 0;

            #region Tạo dữ liệu thống kê điểm thi học kỳ theo môn
            //Fill dữ liệu chung
            sheet.Name = "Tat_ca";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN"));
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue("A8", "MÔN " + Subject + " - NĂM HỌC: " + AcademicYear);
            }
            else
            {
                sheet.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            }

            //Neu chon giao vien, dat tieu de cho giao vien
            if (entity.EmployeeID > 0)
            {
                sheet.SetCellValue("A9", "Giáo viên: " + EmployeeBusiness.Find(entity.EmployeeID).FullName);
            }
            else
            {
                sheet.SetCellValue("A9", String.Empty);
            }
            sheet.SetCellValue("E5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int startrowSum = startrow;
                var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                List<SummedUpRecordBO> lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
                List<JudgeRecordBO> lstJudgeRecord_Edu = new List<JudgeRecordBO>();
                if (entity.MarkType == 2)
                {
                    lstSummedUpRecord_Edu = lstSummedUpRecord.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                }
                else
                {
                    lstJudgeRecord_Edu = lstJudgeRecord.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                }
                for (int j = 0; j < lstClass.Count; j++)
                {
                    classID = lstClass[j].ClassID;

                    HSD = HSCD = 0;
                    if (entity.MarkType == 2)
                    {
                        // lấy số lượng học sinh Đạt
                        HSD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                        // Lấy số lượng học sinh chưa Đạt
                        HSCD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    }
                    else
                    {
                        // lấy số lượng học sinh Đạt
                        HSD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                        // Lấy số lượng học sinh chưa Đạt
                        HSCD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    }

                    // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                    var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == classID && o.SubjectID == entity.SubjectID).FirstOrDefault();
                    int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                    // fill vào excel
                    if (j == lstClass.Count - 1)
                        sheet.CopyPasteSameRowHeigh(rangeLastClass, startrow + 1);
                    else
                        sheet.CopyPasteSameRowHeigh(range, startrow + 1);

                    sheet.SetCellValue(startrow + 1, 1, j + 1);
                    sheet.SetCellValue(startrow + 1, 2, lstClass[j].DisplayName);
                    sheet.SetCellValue(startrow + 1, 3, SumPupil);
                    sheet.SetCellValue(startrow + 1, 4, HSD);
                    sheet.SetCellValue(startrow + 1, 6, HSCD);
                    startrow++;
                }

                //Copy row style
                if (startrowSum + 1 > 11)
                {
                    sheet.CopyPasteSameRowHeigh(rangeEducation, startrowSum);
                }

                // fill vào excel
                sheet.SetCellValue(startrowSum, 1, i + 1);
                sheet.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                if (lstClass.Count > 0)
                {
                    sheet.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow).ToString() + ")");
                    sheet.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow).ToString() + ")");
                    sheet.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow).ToString() + ")");
                    sheet.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    sheet.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                }
                else
                {
                    sheet.SetCellValue(startrowSum, 3, 0);
                    sheet.SetCellValue(startrowSum, 4, 0);
                    sheet.SetCellValue(startrowSum, 6, 0);
                    sheet.SetCellValue(startrowSum, 5, 0);
                    sheet.SetCellValue(startrowSum, 7, 0);
                }

                startrow++;
            }

            // fill vào excel toàn trường
            sheet.SetCellValue(13, 1, "Toàn trường");
            sheet.SetCellValue(13, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow - 1).ToString() + ")/2");

            sheet.SetCellValue(13, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow - 1).ToString() + ")/2");
            sheet.SetCellValue(13, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

            sheet.SetCellValue(13, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow - 1).ToString() + ")/2");
            sheet.SetCellValue(13, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            if (entity.FemaleChecked)
            {
                #region Tạo dữ liệu báo cáo thống kê điểm thi học kỳ học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(firstSheet);
                //Fill dữ liệu chung
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetCellValue("A2", Suppervising);
                sheet_Female.SetCellValue("A3", SchoolName);
                sheet_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ");
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    sheet_Female.SetCellValue("A8", "MÔN " + Subject + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet_Female.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                //Neu chon giao vien, dat tieu de cho giao vien
                if (entity.EmployeeID > 0)
                {
                    sheet_Female.SetCellValue("A9", "Giáo viên: " + EmployeeBusiness.Find(entity.EmployeeID).FullName);
                }
                else
                {
                    sheet_Female.SetCellValue("A9", String.Empty);
                }
                sheet_Female.SetCellValue("E5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                int startrow_Female = 14;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {

                    int startrowSum = startrow_Female;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    List<SummedUpRecordBO> lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
                    List<JudgeRecordBO> lstJudgeRecord_Edu = new List<JudgeRecordBO>();

                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecord_Edu = lstSummedUpRecord_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    }
                    else
                    {
                        lstJudgeRecord_Edu = lstJudgeRecord_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    }
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        classID = lstClass[j].ClassID;

                        HSD = HSCD = 0;
                        if (entity.MarkType == 2)
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }
                        else
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }

                        // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                        var objCountPupil = listCountPupilByClass_Female.Where(o => o.ClassID == classID && o.SubjectID == entity.SubjectID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Female.CopyPasteSameRowHeigh(rangeLastClass, startrow_Female + 1);
                        else
                            sheet_Female.CopyPasteSameRowHeigh(range, startrow_Female + 1);

                        sheet_Female.SetCellValue(startrow_Female + 1, 1, j + 1);
                        sheet_Female.SetCellValue(startrow_Female + 1, 2, lstClass[j].DisplayName);
                        sheet_Female.SetCellValue(startrow_Female + 1, 3, SumPupil);
                        sheet_Female.SetCellValue(startrow_Female + 1, 4, HSD);
                        sheet_Female.SetCellValue(startrow_Female + 1, 6, HSCD);
                        startrow_Female++;
                    }

                    //Copy row style
                    if (startrowSum + 1 > 11)
                    {
                        sheet_Female.CopyPasteSameRowHeigh(rangeEducation, startrowSum);
                    }

                    // fill vào excel
                    sheet_Female.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Female.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                    if (lstClass.Count > 0)
                    {
                        sheet_Female.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow_Female).ToString() + ")");
                        sheet_Female.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow_Female).ToString() + ")");
                        sheet_Female.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow_Female).ToString() + ")");
                        sheet_Female.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                        sheet_Female.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    }
                    else
                    {
                        sheet_Female.SetCellValue(startrowSum, 3, 0);
                        sheet_Female.SetCellValue(startrowSum, 4, 0);
                        sheet_Female.SetCellValue(startrowSum, 6, 0);
                        sheet_Female.SetCellValue(startrowSum, 5, 0);
                        sheet_Female.SetCellValue(startrowSum, 7, 0);
                    }

                    startrow_Female++;
                }

                // fill vào excel toàn trường
                sheet_Female.SetCellValue(13, 1, "Toàn trường");
                sheet_Female.SetCellValue(13, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow_Female - 1).ToString() + ")/2");

                sheet_Female.SetCellValue(13, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow_Female - 1).ToString() + ")/2");
                sheet_Female.SetCellValue(13, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Female.SetCellValue(13, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow_Female - 1).ToString() + ")/2");
                sheet_Female.SetCellValue(13, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Female.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.EthnicChecked)
            {
                #region Tạo báo cáo thống kê điểm thi học kỳ theo môn học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(firstSheet);
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.SetCellValue("A2", Suppervising);
                sheet_Ethnic.SetCellValue("A3", SchoolName);
                sheet_Ethnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH DÂN TỘC");
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    sheet_Ethnic.SetCellValue("A8", "MÔN " + Subject + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet_Ethnic.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                //Neu chon giao vien, dat tieu de cho giao vien
                if (entity.EmployeeID > 0)
                {
                    sheet_Ethnic.SetCellValue("A9", "Giáo viên: " + EmployeeBusiness.Find(entity.EmployeeID).FullName);
                }
                else
                {
                    sheet_Ethnic.SetCellValue("A9", String.Empty);
                }
                sheet_Ethnic.SetCellValue("E5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                int startrow_Ethnic = 14;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowSum = startrow_Ethnic;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    List<SummedUpRecordBO> lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
                    List<JudgeRecordBO> lstJudgeRecord_Edu = new List<JudgeRecordBO>();
                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecord_Edu = lstSummedUpRecord_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    }
                    else
                    {
                        lstJudgeRecord_Edu = lstJudgeRecord_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    }
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        classID = lstClass[j].ClassID;

                        HSD = HSCD = 0;
                        if (entity.MarkType == 2)
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }
                        else
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }

                        // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                        var objCountPupil = listCountPupilByClass_Ethnic.Where(o => o.ClassID == classID && o.SubjectID == entity.SubjectID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangeLastClass, startrow_Ethnic + 1);
                        else
                            sheet_Ethnic.CopyPasteSameRowHeigh(range, startrow_Ethnic + 1);

                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 1, j + 1);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 2, lstClass[j].DisplayName);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 3, SumPupil);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 4, HSD);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 6, HSCD);
                        startrow_Ethnic++;
                    }

                    //Copy row style
                    if (startrowSum + 1 > 11)
                    {
                        sheet_Ethnic.CopyPasteSameRowHeigh(rangeEducation, startrowSum);
                    }

                    // fill vào excel
                    sheet_Ethnic.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Ethnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                    if (lstClass.Count > 0)
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow_Ethnic).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow_Ethnic).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow_Ethnic).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                        sheet_Ethnic.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    }
                    else
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 3, 0);
                        sheet_Ethnic.SetCellValue(startrowSum, 4, 0);
                        sheet_Ethnic.SetCellValue(startrowSum, 6, 0);
                        sheet_Ethnic.SetCellValue(startrowSum, 5, 0);
                        sheet_Ethnic.SetCellValue(startrowSum, 7, 0);
                    }
                    startrow_Ethnic++;
                }

                // fill vào excel toàn trường
                sheet_Ethnic.SetCellValue(13, 1, "Toàn trường");
                sheet_Ethnic.SetCellValue(13, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow_Ethnic - 1).ToString() + ")/2");

                sheet_Ethnic.SetCellValue(13, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow_Ethnic - 1).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(13, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Ethnic.SetCellValue(13, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow_Ethnic - 1).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(13, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region Tạo báo cáo thống kê điểm thi học kỳ theo môn học sinh nữ dân tộc
                IVTWorksheet sheet_EthnicFemale = oBook.CopySheetToLast(firstSheet);
                sheet_EthnicFemale.Name = "HS_Nu_DT";
                sheet_EthnicFemale.SetCellValue("A2", Suppervising);
                sheet_EthnicFemale.SetCellValue("A3", SchoolName);
                sheet_EthnicFemale.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ DÂN TỘC");
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    sheet_EthnicFemale.SetCellValue("A8", "MÔN " + Subject + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet_EthnicFemale.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                //Neu chon giao vien, dat tieu de cho giao vien
                if (entity.EmployeeID > 0)
                {
                    sheet_EthnicFemale.SetCellValue("A9", "Giáo viên: " + EmployeeBusiness.Find(entity.EmployeeID).FullName);
                }
                else
                {
                    sheet_EthnicFemale.SetCellValue("A9", String.Empty);
                }
                sheet_EthnicFemale.SetCellValue("E5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                int startrow_EthnicFemale = 14;

                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowSum = startrow_EthnicFemale;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    List<SummedUpRecordBO> lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
                    List<JudgeRecordBO> lstJudgeRecord_Edu = new List<JudgeRecordBO>();
                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecord_Edu = lstSummedUpRecord_EthnicFemale.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    }
                    else
                    {
                        lstJudgeRecord_Edu = lstJudgeRecord_EthnicFemale.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    }
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        classID = lstClass[j].ClassID;
                        HSD = HSCD = 0;
                        if (entity.MarkType == 2)
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstSummedUpRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }
                        else
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstJudgeRecord_Edu.Where(u => u.ClassID == classID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }

                        // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                        var objCountPupil = listCountPupilByClass_EthnicFemale.Where(o => o.ClassID == classID && o.SubjectID == entity.SubjectID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_EthnicFemale.CopyPasteSameRowHeigh(rangeLastClass, startrow_EthnicFemale + 1);
                        else
                            sheet_EthnicFemale.CopyPasteSameRowHeigh(range, startrow_EthnicFemale + 1);

                        sheet_EthnicFemale.SetCellValue(startrow_EthnicFemale + 1, 1, j + 1);
                        sheet_EthnicFemale.SetCellValue(startrow_EthnicFemale + 1, 2, lstClass[j].DisplayName);
                        sheet_EthnicFemale.SetCellValue(startrow_EthnicFemale + 1, 3, SumPupil);
                        sheet_EthnicFemale.SetCellValue(startrow_EthnicFemale + 1, 4, HSD);
                        sheet_EthnicFemale.SetCellValue(startrow_EthnicFemale + 1, 6, HSCD);
                        startrow_EthnicFemale++;
                    }

                    //Copy row style
                    if (startrowSum + 1 > 11)
                    {
                        sheet_EthnicFemale.CopyPasteSameRowHeigh(rangeEducation, startrowSum);
                    }

                    // fill vào excel
                    sheet_EthnicFemale.SetCellValue(startrowSum, 1, i + 1);
                    sheet_EthnicFemale.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                    if (lstClass.Count > 0)
                    {
                        sheet_EthnicFemale.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow_EthnicFemale).ToString() + ")");
                        sheet_EthnicFemale.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow_EthnicFemale).ToString() + ")");
                        sheet_EthnicFemale.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow_EthnicFemale).ToString() + ")");
                        sheet_EthnicFemale.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                        sheet_EthnicFemale.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    }
                    else
                    {
                        sheet_EthnicFemale.SetCellValue(startrowSum, 3, 0);
                        sheet_EthnicFemale.SetCellValue(startrowSum, 4, 0);
                        sheet_EthnicFemale.SetCellValue(startrowSum, 6, 0);
                        sheet_EthnicFemale.SetCellValue(startrowSum, 5, 0);
                        sheet_EthnicFemale.SetCellValue(startrowSum, 7, 0);
                    }
                    startrow_EthnicFemale++;
                }

                // fill vào excel toàn trường
                sheet_EthnicFemale.SetCellValue(13, 1, "Toàn trường");
                sheet_EthnicFemale.SetCellValue(13, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow_EthnicFemale - 1).ToString() + ")/2");

                sheet_EthnicFemale.SetCellValue(13, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow_EthnicFemale - 1).ToString() + ")/2");
                sheet_EthnicFemale.SetCellValue(13, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_EthnicFemale.SetCellValue(13, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow_EthnicFemale - 1).ToString() + ")/2");
                sheet_EthnicFemale.SetCellValue(13, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_EthnicFemale.FitAllColumnsOnOnePage = true;
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();
            return oBook.ToStream();
        }

        public ProcessedReport InsertAverageMarkBySubjectJudge(AverageMarkBySubjectBO entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TB_THEOMON_NHANXET;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeTBMon_[Học kỳ]_[Môn học]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string Subject = this.SubjectCatBusiness.All.Where(o => o.SubjectCatID == entity.SubjectID).FirstOrDefault().DisplayName;

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            //Start: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            if (entity.MarkType == 1)
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemKTDK");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemTBM");
            }
            if (entity.EmployeeID > 0)
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(EmployeeBusiness.Find(entity.EmployeeID).FullName));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", String.Empty);
            }

            if (outputNamePattern.EndsWith("_"))
            {
                outputNamePattern = outputNamePattern.Remove(outputNamePattern.Length - 1, 1);
            }
            //End: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        #region
        public Stream CreateAverageMarkBySubjectAndTeacherAll(AverageMarkBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON_THEO_GV;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string SchoolName = this.SchoolProfileBusiness.Find(entity.SchoolID).SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = this.SchoolProfileBusiness.Find(entity.SchoolID).SupervisingDept.SupervisingDeptName.ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string Province = this.SchoolProfileBusiness.Find(entity.SchoolID).Province.ProvinceName;
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }

            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            string periodName = period != null ? period.Resolution : String.Empty;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy danh sách mức thống kê
            int StatisticLevelReportID = entity.StatisticLevelReportID;
            List<StatisticLevelConfig> lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            int numberConfig = lstStatisticLevel.Count();

            //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //IVTRange range = firstSheet.GetRange(16, 1, 16, lastColumn);
            //IVTRange rangeClass = firstSheet.GetRange(14, 1, 14, lastColumn);
            //IVTRange rangeLastClass = firstSheet.GetRange(17, 1, 17, lastColumn);
            //IVTRange userReport = firstSheet.GetRange(19, 1, 19, lastColumn);
            //IVTRange range_School = firstSheet.GetRange(13, 1, 13, lastColumn);
            IVTRange range_level = firstSheet.GetRange("F11", "G12");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "Z1000");
            sheet.Name = "Tat_ca";
            int col = 0;
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_level, 11, 6 + col);
                sheet.SetCellValue(11, 6 + col, lstStatisticLevel[i].Title);
                col += 2;
            }

            //Fill dữ liệu chung
            sheet.Name = "Tat_ca";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN"));
            sheet.SetCellValue("I5", Province + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - NĂM HỌC: " + AcademicYear);
            }
            else
            {
                if (!entity.IsPeriod)
                {
                    sheet.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - " + periodName + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
            }

            // Lấy danh sách khối học

            //List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();

            //Lay danh sach giao vien bo mon
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = entity.SchoolID;
            IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);

            if (entity.EmployeeID > 0)
            {
                query = query.Where(o => o.EmployeeID == entity.EmployeeID);
            }

            //Lay danh sach giao vien duong phan cong day mon duoc chon
            IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
            dicToGetAssignment["AcademicYearID"] = entity.AcademicYearID;
            dicToGetAssignment["SubjectID"] = entity.SubjectID;
            if (entity.Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                dicToGetAssignment["Semester"] = entity.Semester;
            }
            IQueryable<TeachingAssignment> iqTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(entity.SchoolID, dicToGetAssignment);



            iqTeachingAssignment = (from ta in iqTeachingAssignment
                                    join ee in query on ta.TeacherID equals ee.EmployeeID
                                    select ta);
            List<TeachingAssignment> lstTeachingAssignment = iqTeachingAssignment.ToList();

            List<EmployeeBO> lstEmployee = (from ta in iqTeachingAssignment
                                            join ee in query on ta.TeacherID equals ee.EmployeeID
                                            select new EmployeeBO
                                            {
                                                EmployeeID = ee.EmployeeID,
                                                Name = ee.Name,
                                                FullName = ee.FullName
                                            }).Distinct().ToList().OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            // Điểm môn tính điểm

            IDictionary<string, object> dic;

            //Danh sach hoc sinh trong truong
            IDictionary<string, object> dicSchool = new Dictionary<string, object>();
            dicSchool["AcademicYearID"] = entity.AcademicYearID;
            //Danh sach diem neu thong ke trung binh mon
            IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = null;
            //Danh sach diem neu thong ke diem kiem tra dinh ky
            IQueryable<VMarkRecord> lstMarkRecord = null;
            AcademicYear aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            if (entity.IsPeriod)
            {
                dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SubjectID", entity.SubjectID},
                {"PeriodID", entity.PeriodID.GetValueOrDefault()}
                };
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            else if (entity.MarkType == 2)
            {
                dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SubjectID", entity.SubjectID},
                {"PeriodID", null}
                };
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null).Where(o => o.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            else
            {
                Dictionary<string, object> dicToGetMarkRecord = new Dictionary<string, object>();
                dicToGetMarkRecord["AcademicYearID"] = entity.AcademicYearID;
                dicToGetMarkRecord["SubjectID"] = entity.SubjectID;
                dicToGetMarkRecord["Semester"] = entity.Semester;
                dicToGetMarkRecord["Title"] = entity.MarkColumn;

                lstMarkRecord = this.VMarkRecordBusiness.SearchBySchool(entity.SchoolID, dicToGetMarkRecord);
            }

            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            IQueryable<PupilOfClassBO> lstQPoc = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                  join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                  join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                                  where (cs.SubjectID == entity.SubjectID || entity.SubjectID == 0)
                                                  && cs.Last2digitNumberSchool == partitionId
                                                  && q.AcademicYearID == entity.AcademicYearID
                                                  //Viethd4: loai bo cac lop hoc mon theo VNEN
                                                  && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                  && q.IsActive.Value
                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      EducationLevelID = q.EducationLevelID,
                                                      ClassID = p.ClassID,
                                                      Genre = r.Genre,
                                                      EthnicID = r.EthnicID,
                                                      AppliedType = cs.AppliedType,
                                                      IsSpecialize = cs.IsSpecializedSubject,
                                                      SubjectID = cs.SubjectID
                                                  });

            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",AcademicYearBusiness.Find(entity.AcademicYearID).Year}
                                               };

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID, 0);
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                //Khong lay cac hoc sinh mien giam
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_FemaleEthnic = new List<SummedUpRecordBO>();

            List<MarkRecordBO> lstMarkRecordBO = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_Female = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_Ethnic = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_FemaleEthnic = new List<MarkRecordBO>();
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            //Xuat theo dot
            if (entity.IsPeriod)
            {
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                var tmpSummedUp = from u in lstSummedUpRecordAllStatus
                                  join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                  join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                  where c.IsActive.Value
                                  //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select new
                                  {
                                      PupilID = u.PupilID,
                                      ClassID = u.ClassID,
                                      SubjectID = u.SubjectID,
                                      Semester = u.Semester,
                                      SummedUpMark = u.SummedUpMark,
                                      ReTestMark = u.ReTestMark,
                                      EducationLevelID = c.EducationLevelID,
                                      Genre = v.Genre,
                                      EthnicID = v.EthnicID
                                  };
                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);

                lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SummedUpMark = u.SummedUpMark }).ToList();

                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_FemaleEthnic = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            }
            else if (entity.MarkType == 2)
            {
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                var tmpSummedUp = from u in lstSummedUpRecordAllStatus
                                  join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                  join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                  where c.IsActive.Value
                                  //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select new
                                  {
                                      PupilID = u.PupilID,
                                      ClassID = u.ClassID,
                                      SubjectID = u.SubjectID,
                                      Semester = u.Semester,
                                      SummedUpMark = u.SummedUpMark,
                                      ReTestMark = u.ReTestMark,
                                      EducationLevelID = c.EducationLevelID,
                                      Genre = v.Genre,
                                      EthnicID = v.EthnicID
                                  };
                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);
                if (entity.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                    var iqSummedUpFilter = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester))
                        .Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SummedUpMark = u.ReTestMark != null ? u.ReTestMark : u.SummedUpMark });
                    lstSummedUpRecord = iqSummedUpFilter.ToList();

                }
                else
                {
                    lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SummedUpMark = u.SummedUpMark }).ToList();
                }
                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_FemaleEthnic = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            }

            else
            {

                //Lay danh sach diem neu la thong ke theo Kiem tra dinh ky
                var tmpMarkRecord = from u in lstMarkRecord
                                    join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                    join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                    where c.IsActive.Value
                                    //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                    select new
                                    {
                                        PupilID = u.PupilID,
                                        ClassID = u.ClassID,
                                        SubjectID = u.SubjectID,
                                        Semester = u.Semester,
                                        Mark = u.Mark,
                                        EducationLevelID = c.EducationLevelID,
                                        Genre = v.Genre,
                                        EthnicID = v.EthnicID,
                                        MarkRecordID = u.MarkRecordID
                                    };
                var iqMarkRecord = (from u in tmpMarkRecord.ToList()
                                    join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                    select u);
                lstMarkRecordBO = iqMarkRecord.Select(u => new MarkRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, Mark = u.Mark, MarkRecordID = u.MarkRecordID }).ToList();
                listMarkRecordBO_Ethnic = lstMarkRecordBO.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                listMarkRecordBO_Female = lstMarkRecordBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                listMarkRecordBO_FemaleEthnic = lstMarkRecordBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            }
            // Lay danh sach hoc sinh theo lop
            var listCountCurentPupilByClass = lstPOC.GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountCurentPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_FemaleEthnic = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();

            IDictionary<string, object> dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID},
                                                                    {"IsVNEN", true}
                                                                };
            // List Class Profile
            IQueryable<ClassSubject> iqClassProfile_Temp = this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dic_ClassProfile);
            List<ClassProfileTempBO> lstClassProfileAllEdu = (from cs in iqClassProfile_Temp
                                                              join c in ClassProfileBusiness.All on cs.ClassID equals c.ClassProfileID
                                                              join em in EmployeeBusiness.All on c.HeadTeacherID equals em.EmployeeID into g
                                                              from j in g.DefaultIfEmpty()
                                                              orderby c.OrderNumber.HasValue ? c.OrderNumber : 0, c.DisplayName
                                                              where c.IsActive.Value
                                                              select new ClassProfileTempBO
                                                              {
                                                                  EducationLevelID = c.EducationLevelID,
                                                                  ClassProfileID = cs.ClassID,
                                                                  OrderNumber = c.OrderNumber,
                                                                  DisplayName = c.DisplayName,
                                                                  EmployeeName = j.FullName != null ? j.FullName : string.Empty
                                                              }).ToList();
            //Neu nhap giao vien, loc danh sach cac lop ma giao vien do day
            //Danh sach phan cong giang day


            List<ClassSubject> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                  join ts in iqTeachingAssignment on cs.ClassID equals ts.ClassID
                                                  where cs.SubjectID == ts.SubjectID
                                                  //Viethd4: Loai cac lop hoc mon nay theo VNEN
                                                  && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                  && ((entity.Semester > 2 && (cs.SectionPerWeekFirstSemester.HasValue
                                                  || cs.SectionPerWeekSecondSemester.HasValue)) || ts.Semester == entity.Semester)
                                                  && cs.Last2digitNumberSchool == partitionId
                                                  select cs).ToList();



            //Chiendd: loai cac ban ghi trung nhau
            lstClassProfileAllEdu = (from c in lstClassProfileAllEdu
                                     join cs in lstClassSubject on c.ClassProfileID equals cs.ClassID
                                     select c).Distinct().ToList();

            lstClassProfileAllEdu = lstClassProfileAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            List<ClassProfileTempBO> lstClassProfile = new List<ClassProfileTempBO>();

            List<decimal?> lstSummedUpRecordClass = new List<decimal?>();



            #region thống kê điểm trung bình môn
            int SL = 0;
            int startRow = 14;
            int startColumn = 1;
            int curRow = startRow;
            int curColumn = startColumn;
            int lastColumn = 5 + numberConfig * 2;

            int index = 1;
            for (int i = 0; i < lstEmployee.Count; i++)
            {
                EmployeeBO emp = lstEmployee[i];
                List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                if (lstClassProfile.Count == 0)
                {
                    continue;
                }

                int startEmpRow = curRow;
                int lastEmpRow = curRow + lstClassProfile.Count - 1;
                sheet.GetRange(startEmpRow, 1, lastEmpRow + 1, 1).Merge();
                sheet.GetRange(startEmpRow, 2, lastEmpRow + 1, 2).Merge();

                //stt
                sheet.SetCellValue(curRow, curColumn, index++);
                curColumn++;

                //ten giao vien
                sheet.SetCellValue(curRow, curColumn, emp.FullName);
                curColumn++;

                for (int j = 0; j < lstClassProfile.Count; j++)
                {

                    int ClassID = lstClassProfile[j].ClassProfileID;

                    if (entity.MarkType == 2)
                    {
                        lstSummedUpRecordClass = lstSummedUpRecord.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                    }
                    else
                    {
                        lstSummedUpRecordClass = lstMarkRecordBO.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                    }

                    //ten lop
                    sheet.SetCellValue(curRow, curColumn, lstClassProfile[j].DisplayName);
                    curColumn++;

                    //Giao vien chu nhiem
                    sheet.SetCellValue(curRow, curColumn, lstClassProfile[j].EmployeeName);
                    curColumn++;

                    SL = 0;
                    if (listCountCurentPupilByClass.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                    {
                        //Tong so
                        sheet.SetCellValue(curRow, curColumn, listCountCurentPupilByClass.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                        curColumn++;

                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevel[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                    sheet.SetCellValue(curRow, curColumn, SL);//E
                                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                }
                            }
                            curColumn += 2;
                        }
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet.SetCellValue(curRow, curColumn, SL);//E
                            sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;
                        }
                    }

                    curRow++;
                    curColumn = 3;
                }

                //Fill dong tong so
                if (lstClassProfile.Count > 0)
                {
                    sheet.SetCellValue(curRow, curColumn, "Tổng số");
                    sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                    curColumn++;
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, "=SUM(E" + startEmpRow + ":E" + lastEmpRow.ToString() + ")");
                    curColumn++;

                    for (int m = 0; m < numberConfig; m++)
                    {
                        string column = UtilsBusiness.GetExcelColumnName(curColumn);
                        sheet.SetCellValue(curRow, curColumn, "=SUM(" + column + startEmpRow.ToString() + ":" + column + lastEmpRow.ToString() + ")");//E
                        sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + column + curRow + "/" + "E" + curRow + "*100,2),0)");
                        curColumn += 2;
                    }
                }
                else
                {
                    sheet.SetCellValue(curRow, curColumn, "Tổng số");
                    sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                    curColumn++;
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, 0);
                    curColumn++;

                    for (int m = 0; m < numberConfig; m++)
                    {
                        string column = UtilsBusiness.GetExcelColumnName(curColumn);
                        sheet.SetCellValue(curRow, curColumn, 0);//E
                        sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + column + curRow + "/" + "E" + curRow + "*100,2),0)");
                        curColumn += 2;
                    }
                }

                curRow++;
                curColumn = startColumn;
            }

            // fill vào excel dữ liệu row tổng của trường
            int startRow_School = 13;

            sheet.SetCellValue(startRow_School, startColumn, "Tổng cộng");
            sheet.GetRange(startRow_School, startColumn, startRow_School, 4).Merge();
            sheet.GetRange(startRow_School, startColumn, startRow_School, lastColumn).FillColor(194, 240, 194);

            sheet.SetCellValue(startRow_School, 5, "=SUM(E" + startRow.ToString() + ":E" + (curRow - 1).ToString() + ")/2");//E

            curColumn = 6;
            for (int i = 0; i < numberConfig; i++)
            {
                string column = UtilsBusiness.GetExcelColumnName(curColumn);
                sheet.SetCellValue(startRow_School, curColumn, "=SUM(" + column + startRow.ToString() + ":" + column + (curRow - 1).ToString() + ")/2");//E
                sheet.SetCellValue(startRow_School, curColumn + 1, "=IF(E13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + startRow_School + "/" + "E" + startRow_School + "*100,2),0)");
                curColumn += 2;
            }

            sheet.SetCellValue(curRow + 2, lastColumn - 5, "Người lập báo cáo");
            sheet.GetRange(curRow + 2, lastColumn - 5, curRow + 2, lastColumn).Merge();
            sheet.GetRange(curRow + 2, lastColumn - 5, curRow + 2, lastColumn).SetFontStyle(true, null, false, null, false, false);

            IVTRange headerRange = sheet.GetRange(11, startColumn, 12, lastColumn);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

            sheet.GetRange(13, startColumn, curRow - 1, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            //Merge header
            sheet.GetRange(7, startColumn, 7, lastColumn).Merge();
            sheet.GetRange(8, startColumn, 8, lastColumn).Merge();
            sheet.GetRange(5, 9, 5, lastColumn).Merge();

            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            if (entity.FemaleChecked)
            {
                #region Thống kê điểm trung bình môn học sinh dân tộc
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(sheet);
                sheet_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ");
                sheet_Female.Name = "HS_Nu";

                SL = 0;
                startRow = 14;
                startColumn = 1;
                curRow = startRow;
                curColumn = startColumn;
                lastColumn = 5 + numberConfig * 2;

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeBO emp = lstEmployee[i];
                    List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                    lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                    if (lstClassProfile.Count == 0)
                    {
                        continue;
                    }

                    //stt
                    curColumn++;

                    //ten giao vien
                    curColumn++;

                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {

                        int ClassID = lstClassProfile[j].ClassProfileID;

                        if (entity.MarkType == 2)
                        {
                            lstSummedUpRecordClass = lstSummedUpRecord_Female.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                        }
                        else
                        {
                            lstSummedUpRecordClass = listMarkRecordBO_Female.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                        }

                        //ten lop
                        curColumn++;

                        //Giao vien chu nhiem
                        curColumn++;

                        SL = 0;
                        if (listCountCurentPupilByClass_Female.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            //Tong so
                            sheet_Female.SetCellValue(curRow, curColumn, listCountCurentPupilByClass_Female.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                            curColumn++;

                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                curColumn += 2;
                            }
                        }
                        else
                        {
                            sheet_Female.SetCellValue(curRow, curColumn, 0);
                            for (int m = 0; m < numberConfig; m++)
                            {
                                sheet_Female.SetCellValue(curRow, curColumn, SL);//E
                                sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                curColumn += 2;
                            }
                        }

                        curRow++;
                        curColumn = 3;
                    }

                    curRow++;
                    curColumn = startColumn;
                }

                sheet_Female.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.EthnicChecked)
            {
                #region Thống kê điểm trung bình môn học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(sheet);
                sheet_Ethnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH DÂN TỘC");
                sheet_Ethnic.Name = "HS_DT";

                SL = 0;
                startRow = 14;
                startColumn = 1;
                curRow = startRow;
                curColumn = startColumn;
                lastColumn = 5 + numberConfig * 2;

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeBO emp = lstEmployee[i];
                    List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                    lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                    if (lstClassProfile.Count == 0)
                    {
                        continue;
                    }

                    //stt
                    curColumn++;

                    //ten giao vien
                    curColumn++;

                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {

                        int ClassID = lstClassProfile[j].ClassProfileID;

                        if (entity.MarkType == 2)
                        {
                            lstSummedUpRecordClass = lstSummedUpRecord_Ethnic.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                        }
                        else
                        {
                            lstSummedUpRecordClass = listMarkRecordBO_Ethnic.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                        }

                        //ten lop
                        curColumn++;

                        //Giao vien chu nhiem
                        curColumn++;

                        SL = 0;
                        if (listCountCurentPupilByClass_Ethnic.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            //Tong so
                            sheet_Ethnic.SetCellValue(curRow, curColumn, listCountCurentPupilByClass_Ethnic.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                            curColumn++;

                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                curColumn += 2;
                            }
                        }
                        else
                        {
                            sheet_Ethnic.SetCellValue(curRow, curColumn, 0);
                            for (int m = 0; m < numberConfig; m++)
                            {
                                sheet_Ethnic.SetCellValue(curRow, curColumn, SL);//E
                                sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                curColumn += 2;
                            }
                        }

                        curRow++;
                        curColumn = 3;
                    }

                    curRow++;
                    curColumn = startColumn;
                }

                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region Thống kê điểm trung bình môn học sinh dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(sheet);
                sheet_FemaleEthnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ DÂN TỘC");
                sheet_FemaleEthnic.Name = "HS_Nu_DT";

                SL = 0;
                startRow = 14;
                startColumn = 1;
                curRow = startRow;
                curColumn = startColumn;
                lastColumn = 5 + numberConfig * 2;

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeBO emp = lstEmployee[i];
                    List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                    lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                    if (lstClassProfile.Count == 0)
                    {
                        continue;
                    }

                    //stt
                    curColumn++;

                    //ten giao vien
                    curColumn++;

                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {

                        int ClassID = lstClassProfile[j].ClassProfileID;

                        if (entity.MarkType == 2)
                        {
                            lstSummedUpRecordClass = lstSummedUpRecord_FemaleEthnic.Where(o => o.ClassID == ClassID).Select(o => o.SummedUpMark).ToList();
                        }
                        else
                        {
                            lstSummedUpRecordClass = listMarkRecordBO_FemaleEthnic.Where(o => o.ClassID == ClassID).Select(o => o.Mark).ToList();
                        }

                        //ten lop
                        curColumn++;

                        //Giao vien chu nhiem
                        curColumn++;

                        SL = 0;
                        if (listCountCurentPupilByClass_FemaleEthnic.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            //Tong so
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, listCountCurentPupilByClass_FemaleEthnic.Where(o => o.ClassID == ClassID && o.SubjectID == entity.SubjectID).Sum(o => o.TotalPupil));
                            curColumn++;

                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                    }
                                }
                                curColumn += 2;
                            }
                        }
                        else
                        {
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, 0);
                            for (int m = 0; m < numberConfig; m++)
                            {
                                sheet_FemaleEthnic.SetCellValue(curRow, curColumn, SL);//E
                                sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                                curColumn += 2;
                            }
                        }

                        curRow++;
                        curColumn = 3;
                    }

                    curRow++;
                    curColumn = startColumn;
                }

                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                #endregion
            }
            firstSheet.Delete();

            //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            return oBook.ToStream();
        }
        #endregion

        #region Thong ke diem KTDK, TBM nhom theo giao vien bo mon
        public Stream CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV;
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string SchoolName = this.SchoolProfileBusiness.Find(entity.SchoolID).SchoolName.ToUpper();
            string AcademicYear = academicYear.DisplayTitle;
            string Suppervising = this.SchoolProfileBusiness.Find(entity.SchoolID).SupervisingDept.SupervisingDeptName.ToUpper();
            string Subject = "";
            if (entity.SubjectID > 0)
            {
                Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            }
            string Province = this.SchoolProfileBusiness.Find(entity.SchoolID).Province.ProvinceName;
            List<EducationLevel> lstEducation = entity.EducationLevels;

            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            string periodName = period != null ? period.Resolution : String.Empty;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy danh sách mức thống kê
            int StatisticLevelReportID = 0;
            List<StatisticLevelConfig> lstStatisticLevel = new List<StatisticLevelConfig>();
            if (entity.StatisticLevelReportID > 0)
            {
                StatisticLevelReportID = entity.StatisticLevelReportID;
                lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            }
            int numberConfig = lstStatisticLevel.Count();

            //Start: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            IVTWorksheet sheetNX = oBook.CopySheetToLast(secondSheet);
            sheet.Name = "Tat_Ca_Tinh_Diem";
            sheetNX.Name = "Tat_Ca_Nhan_Xet";

            //Fill dữ liệu chung
            //sheet.Name = "Tat_ca";

            #region getData
            //Lay danh sach giao vien bo mon
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = entity.SchoolID;
            dicToGetTeacher["AcademicYearID"] = entity.AcademicYearID;
            dicToGetTeacher["SchoolID"] = entity.SchoolID;
            IQueryable<Employee> qaEmployee = EmployeeBusiness.All.Where(e => e.SchoolID == entity.SchoolID && e.IsActive);
            IQueryable<EmployeeHistoryStatus> qaHistory = EmployeeHistoryStatusBusiness.All.Where(h => h.SchoolID == entity.SchoolID);
            qaHistory = qaHistory.AddCriteriaEmployee(academicYear, entity.Semester);
            //IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);

            if (entity.EmployeeID > 0)
            {
                qaEmployee = qaEmployee.Where(o => o.EmployeeID == entity.EmployeeID);
                qaHistory = qaHistory.Where(h => h.EmployeeID == entity.EmployeeID);
            }
            IQueryable<Employee> query = from e in qaEmployee
                                         join h in qaHistory on e.EmployeeID equals h.EmployeeID
                                         select e;


            //Lay danh sach giao vien duong phan cong day mon duoc chon
            IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
            dicToGetAssignment["AcademicYearID"] = entity.AcademicYearID;
            if (entity.SubjectID != 0)
            {
                dicToGetAssignment["SubjectID"] = entity.SubjectID; //xoa mon hoc
            }
            if (entity.Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                dicToGetAssignment["Semester"] = entity.Semester;
            }
            IQueryable<TeachingAssignment> iqTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(entity.SchoolID, dicToGetAssignment);

            iqTeachingAssignment = (from ta in iqTeachingAssignment
                                    join ee in query on ta.TeacherID equals ee.EmployeeID
                                    select ta);
            List<TeachingAssignment> lstTeachingAssignment = iqTeachingAssignment.ToList();

            List<EmployeeBO> lstEmployee = (from ta in iqTeachingAssignment
                                            join ee in query on ta.TeacherID equals ee.EmployeeID
                                            select new EmployeeBO
                                            {
                                                EmployeeID = ee.EmployeeID,
                                                Name = ee.Name,
                                                FullName = ee.FullName,
                                                SubjectID = ta.SubjectID
                                            }).Distinct().ToList().OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.All.Where(s => s.Last2digitNumberSchool == partitionId && (s.IsSubjectVNEN == null || s.IsSubjectVNEN == false));
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekFirstSemester > 0);
            }
            else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekSecondSemester > 0);
            }
            else
            {
                iqClassSubject = iqClassSubject.Where(s => (s.SectionPerWeekSecondSemester > 0 || s.SectionPerWeekFirstSemester > 0));
            }
            if (entity.SubjectID > 0)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SubjectID == entity.SubjectID);
            }

            // Điểm môn tính điểm

            IDictionary<string, object> dic;

            //Danh sach hoc sinh trong truong
            IDictionary<string, object> dicSchool = new Dictionary<string, object>();
            dicSchool["AcademicYearID"] = entity.AcademicYearID;
            //Danh sach diem neu thong ke trung binh mon
            IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = null;
            //IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatusNX = null;
            //Danh sach diem neu thong ke diem kiem tra dinh ky
            IQueryable<VMarkRecord> lstMarkRecord = null;
            IQueryable<VJudgeRecord> lstJudgeRecordAllStatus = null;
            AcademicYear aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            if (entity.IsPeriod)
            {
                dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID", entity.PeriodID.GetValueOrDefault()}
                };
                if (entity.SubjectID != 0)
                {
                    dic["SubjectID"] = entity.SubjectID;
                }
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic);
                //lstSummedUpRecordAllStatusNX = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            else if (entity.MarkType == 2)
            {
                dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID", null}
                };
                if (entity.SubjectID != 0)
                {
                    dic["SubjectID"] = entity.SubjectID;
                }
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null);
                //lstSummedUpRecordAllStatusNX = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            else
            {
                Dictionary<string, object> dicToGetMarkRecord = new Dictionary<string, object>();
                dicToGetMarkRecord["AcademicYearID"] = entity.AcademicYearID;
                //dicToGetMarkRecord["SubjectID"] = entity.SubjectID;//xoa mon hoc
                dicToGetMarkRecord["Semester"] = entity.Semester;
                dicToGetMarkRecord["Title"] = entity.MarkColumn;
                if (entity.SubjectID != 0)
                {
                    dicToGetMarkRecord["SubjectID"] = entity.SubjectID;
                }
                lstMarkRecord = this.VMarkRecordBusiness.SearchBySchool(entity.SchoolID, dicToGetMarkRecord);
                lstJudgeRecordAllStatus = this.VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, dicToGetMarkRecord);
            }

            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClassBO> lstQPoc = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                  join cs in iqClassSubject on p.ClassID equals cs.ClassID
                                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                  join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                                  where 
                                                  q.AcademicYearID == entity.AcademicYearID
                                                  && p.AcademicYearID==entity.AcademicYearID
                                                  && q.IsActive.Value
                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      EducationLevelID = q.EducationLevelID,
                                                      ClassID = p.ClassID,
                                                      Genre = r.Genre,
                                                      EthnicID = r.EthnicID,
                                                      AppliedType = cs.AppliedType,
                                                      IsSpecialize = cs.IsSpecializedSubject,
                                                      SubjectID = cs.SubjectID
                                                  });
            if (entity.SubjectID != 0)
            {
                lstQPoc = lstQPoc.Where(cs => cs.SubjectID == entity.SubjectID);
            }

            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",entity.SubjectID},//xoa mon hoc
                {"SemesterID",entity.Semester},
                {"YearID",Aca.Year}
            };
            if (entity.SubjectID != 0)
            {
                dicRegis["SubjectID"] = entity.SubjectID;
            }

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID != 0 ? entity.SubjectID : 0, 0); //xoa mon hoc

            //Khong lay cac hoc sinh mien giam
            lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();


            List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_FemaleEthnic = new List<SummedUpRecordBO>();

            List<MarkRecordBO> lstMarkRecordBO = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_Female = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_Ethnic = new List<MarkRecordBO>();
            List<MarkRecordBO> listMarkRecordBO_FemaleEthnic = new List<MarkRecordBO>();

            List<JudgeRecordBO> lstJudgeRecord = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_Female = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_Ethnic = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_EthnicFemale = new List<JudgeRecordBO>();
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            //Xuat theo dot
            if (entity.IsPeriod)
            {
                #region Xuất theo đợt
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                var tmpSummedUp = from u in lstSummedUpRecordAllStatus
                                  join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                  join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                  where c.IsActive.Value
                                  //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select new
                                  {
                                      PupilID = u.PupilID,
                                      ClassID = u.ClassID,
                                      SubjectID = u.SubjectID,
                                      Semester = u.Semester,
                                      SummedUpMark = u.SummedUpMark,
                                      JudgementResult = u.JudgementResult,
                                      ReTestJudgement = u.ReTestJudgement,
                                      ReTestMark = u.ReTestMark,
                                      EducationLevelID = c.EducationLevelID,
                                      Genre = v.Genre,
                                      EthnicID = v.EthnicID
                                  };
                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);

                lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SubjectID = u.SubjectID, SummedUpMark = u.SummedUpMark, JudgementResult = u.JudgementResult }).ToList();
                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_FemaleEthnic = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                #endregion
            }
            else if (entity.MarkType == 2)
            {
                #region Xuất theo loại điểm là TBM
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                var tmpSummedUp = from u in lstSummedUpRecordAllStatus
                                  join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                  join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                  where c.IsActive.Value
                                  //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select new
                                  {
                                      PupilID = u.PupilID,
                                      ClassID = u.ClassID,
                                      SubjectID = u.SubjectID,
                                      Semester = u.Semester,
                                      SummedUpMark = u.SummedUpMark,
                                      JudgementResult = u.JudgementResult,
                                      ReTestJudgement = u.ReTestJudgement,
                                      ReTestMark = u.ReTestMark,
                                      EducationLevelID = c.EducationLevelID,
                                      Genre = v.Genre,
                                      EthnicID = v.EthnicID
                                  };
                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);
                if (entity.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                    var iqSummedUpFilter = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester))
                        .Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, SubjectID = u.SubjectID, EthnicID = u.EthnicID, SummedUpMark = u.ReTestMark != null ? u.ReTestMark : u.SummedUpMark, JudgementResult = u.ReTestJudgement != null ? u.ReTestJudgement : u.JudgementResult });
                    lstSummedUpRecord = iqSummedUpFilter.ToList();
                }
                else
                {
                    lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, SubjectID = u.SubjectID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, SummedUpMark = u.SummedUpMark, JudgementResult = u.JudgementResult }).ToList();
                }
                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_FemaleEthnic = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                #endregion
            }
            else
            {
                #region Xuất theo loại điểm là KTĐK
                //Môn chấm điểm
                var tmpMarkRecord = from u in lstMarkRecord
                                    join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                    join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                    where c.IsActive.Value
                                    //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                    select new
                                    {
                                        PupilID = u.PupilID,
                                        ClassID = u.ClassID,
                                        SubjectID = u.SubjectID,
                                        Semester = u.Semester,
                                        Mark = u.Mark,
                                        EducationLevelID = c.EducationLevelID,
                                        Genre = v.Genre,
                                        EthnicID = v.EthnicID,
                                        MarkRecordID = u.MarkRecordID
                                    };
                var iqMarkRecord = (from u in tmpMarkRecord.ToList()
                                    join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                    select u);
                lstMarkRecordBO = iqMarkRecord.Select(u => new MarkRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, Mark = u.Mark, MarkRecordID = u.MarkRecordID, SubjectID = u.SubjectID }).ToList();
                listMarkRecordBO_Ethnic = lstMarkRecordBO.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                listMarkRecordBO_Female = lstMarkRecordBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                listMarkRecordBO_FemaleEthnic = lstMarkRecordBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();

                //Môn nhận xét
                var tmpJudgeRecord = from u in lstJudgeRecordAllStatus
                                     join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                     join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                     where c.IsActive.Value
                                     //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                     select new
                                     {
                                         PupilID = u.PupilID,
                                         ClassID = u.ClassID,
                                         SubjectID = u.SubjectID,
                                         Semester = u.Semester,
                                         JudgeMent = u.Judgement,
                                         EducationLevelID = c.EducationLevelID,
                                         Genre = v.Genre,
                                         EthnicID = v.EthnicID,
                                         JudgeRecordID = u.JudgeRecordID
                                     };
                var iqJudgeRecord = (from u in tmpJudgeRecord.ToList()
                                     join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                     select u);
                lstJudgeRecord = iqJudgeRecord.Select(u => new JudgeRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, Judgement = u.JudgeMent, JudgeRecordID = u.JudgeRecordID, SubjectID = u.SubjectID }).ToList();
                lstJudgeRecord_Ethnic = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstJudgeRecord_Female = lstJudgeRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstJudgeRecord_EthnicFemale = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                #endregion
            }
            // Lay danh sach hoc sinh theo lop
            List<MarkTotal> listCountCurentPupilByClass = lstPOC.GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new MarkTotal { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            List<MarkTotal> listCountCurentPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new MarkTotal { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            List<MarkTotal> listCountCurentPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new MarkTotal { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            List<MarkTotal> listCountCurentPupilByClass_FemaleEthnic = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new MarkTotal { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();

            IDictionary<string, object> dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    //{"SubjectID", entity.SubjectID}, //xóa môn học
                                                                    {"IsVNEN", true}
                                                                };
            if (entity.SubjectID != 0)
            {
                dic_ClassProfile["SubjectID"] = entity.SubjectID;
            }
            // List Class Profile
            IQueryable<ClassSubject> iqClassProfile_Temp = this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dic_ClassProfile);
            List<ClassProfileTempBO> lstClassProfileAllEdu = (from cs in iqClassProfile_Temp
                                                              join c in ClassProfileBusiness.All on cs.ClassID equals c.ClassProfileID
                                                              join em in EmployeeBusiness.All on c.HeadTeacherID equals em.EmployeeID into g
                                                              from j in g.DefaultIfEmpty()
                                                              orderby c.OrderNumber.HasValue ? c.OrderNumber : 0, c.DisplayName
                                                              where c.IsActive.Value
                                                              select new ClassProfileTempBO
                                                              {
                                                                  EducationLevelID = c.EducationLevelID,
                                                                  ClassProfileID = cs.ClassID,
                                                                  OrderNumber = c.OrderNumber,
                                                                  DisplayName = c.DisplayName,
                                                                  EmployeeName = j.FullName != null ? j.FullName : string.Empty,
                                                                  SubjectID = cs.SubjectID
                                                              }).ToList();
            //Neu nhap giao vien, loc danh sach cac lop ma giao vien do day          
            List<ClassSubject> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                  join ts in iqTeachingAssignment on cs.ClassID equals ts.ClassID
                                                  where cs.SubjectID == ts.SubjectID
                                                  //Viethd4: Loai cac lop hoc mon nay theo VNEN
                                                  && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                  && ((entity.Semester > 2 && (cs.SectionPerWeekFirstSemester.HasValue
                                                  || cs.SectionPerWeekSecondSemester.HasValue)) || ts.Semester == entity.Semester)
                                                  && cs.Last2digitNumberSchool == partitionId
                                                  select cs).ToList();

            //List<ClassSubject> lll = lstClassSubject.Where(x => x.ClassID == 634989).ToList();

            Dictionary<string, object> dicAllSubject = new Dictionary<string, object>
            {
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel},
                //{"SubjectID", entity.SubjectID}
            };
            if (entity.SubjectID != 0)
            {
                dicAllSubject["SubjectID"] = entity.SubjectID;
            }

            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchByAcademicYear(entity.AcademicYearID, dicAllSubject)
                .Where(o => o.SubjectCat.IsActive)
                .Select(u => new SubjectCatBO
                {
                    SubjectCatID = u.SubjectID,
                    DisplayName = u.SubjectCat.DisplayName,
                    SubjectName = u.SubjectCat.SubjectName,
                    OrderInSubject = u.SubjectCat.OrderInSubject,
                    IsCommenting = u.IsCommenting
                })
                .Distinct()
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName)
                .ToList();
            SubjectCatBO schoolSubject = new SubjectCatBO();
            if (entity.SubjectID > 0)
            {
                schoolSubject = lstSubject.Where(x => x.SubjectCatID == entity.SubjectID).FirstOrDefault();
            }

            //Chiendd: loai cac ban ghi trung nhau
            //List<ClassProfileTempBO> cscs = lstClassProfileAllEdu.Where(x => x.EducationLevelID == 8).ToList();
            lstClassProfileAllEdu = (from c in lstClassProfileAllEdu
                                     join cs in lstClassSubject on c.ClassProfileID equals cs.ClassID
                                     select c).Distinct().ToList();

            //lstClassProfileAllEdu = lstClassProfileAllEdu.GroupBy(p => new { p.EducationLevelID, p.ClassProfileID })
            //                              .Select(g => g.First())
            //                              .ToList();

            lstClassProfileAllEdu = lstClassProfileAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            List<ClassProfileTempBO> lstClassProfile = new List<ClassProfileTempBO>();
            List<decimal?> lstSummedUpRecordClass = new List<decimal?>();
            #endregion

            #region thống kê điểm trung bình môn
            SetValueToFile(firstSheet, sheet, sheetNX, numberConfig, lstSubject,
                                    lstEmployee, lstTeachingAssignment,
                                    lstClassProfileAllEdu, lstEducation,
                                    entity, lstSummedUpRecord,
                                    lstMarkRecordBO, listCountCurentPupilByClass,
                                    lstStatisticLevel, lstJudgeRecord,
                                    Suppervising, SchoolName, Province, Subject, periodName,
                                    AcademicYear, 1);
            sheet.FitAllColumnsOnOnePage = true;
            sheetNX.FitAllColumnsOnOnePage = true;
            if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
            {
                sheetNX.Delete();
            }
            else if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
            {
                sheet.Delete();
            }

            if (entity.FemaleChecked)
            {
                #region chọn hs nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(firstSheet);
                IVTWorksheet sheet_FemaleNX = oBook.CopySheetToLast(secondSheet);
                sheet_Female.Name = "HS_Nu_Tinh_Diem";
                sheet_FemaleNX.Name = "HS_Nu_Nhan_Xet";
                SetValueToFile(firstSheet, sheet_Female, sheet_FemaleNX, numberConfig, lstSubject,
                                    lstEmployee, lstTeachingAssignment,
                                    lstClassProfileAllEdu, lstEducation,
                                    entity, lstSummedUpRecord_Female,
                                    listMarkRecordBO_Female, listCountCurentPupilByClass_Female,
                                    lstStatisticLevel, lstJudgeRecord_Female,
                                    Suppervising, SchoolName, Province, Subject, periodName,
                                    AcademicYear, 2);
                sheet_Female.FitAllColumnsOnOnePage = true;
                sheet_FemaleNX.FitAllColumnsOnOnePage = true;
                if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    sheet_FemaleNX.Delete();
                }
                else if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    sheet_Female.Delete();
                }
                #endregion
            }

            if (entity.EthnicChecked)
            {
                #region chọn hs dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(firstSheet);
                IVTWorksheet sheet_EthnicNX = oBook.CopySheetToLast(secondSheet);
                sheet_Ethnic.Name = "HS_DT_Tinh_Diem";
                sheet_EthnicNX.Name = "HS_DT_Nhan_Xet";
                SetValueToFile(firstSheet, sheet_Ethnic, sheet_EthnicNX, numberConfig, lstSubject,
                                    lstEmployee, lstTeachingAssignment,
                                    lstClassProfileAllEdu, lstEducation,
                                    entity, lstSummedUpRecord_Ethnic,
                                    listMarkRecordBO_Ethnic, listCountCurentPupilByClass_Ethnic,
                                    lstStatisticLevel, lstJudgeRecord_Ethnic,
                                    Suppervising, SchoolName, Province, Subject, periodName,
                                    AcademicYear, 3);
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                sheet_EthnicNX.FitAllColumnsOnOnePage = true;
                if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    sheet_EthnicNX.Delete();
                }
                else if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    sheet_Ethnic.Delete();
                }
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region chọn hs nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(firstSheet);
                IVTWorksheet sheet_FemaleEthnicNX = oBook.CopySheetToLast(secondSheet);
                sheet_FemaleEthnic.Name = "HS_Nu_DT_Tinh_Diem";
                sheet_FemaleEthnicNX.Name = "HS_Nu_DT_Nhan_Xet";
                SetValueToFile(firstSheet, sheet_FemaleEthnic, sheet_FemaleEthnicNX, numberConfig, lstSubject,
                                    lstEmployee, lstTeachingAssignment,
                                    lstClassProfileAllEdu, lstEducation,
                                    entity, lstSummedUpRecord_FemaleEthnic,
                                    listMarkRecordBO_FemaleEthnic, listCountCurentPupilByClass_FemaleEthnic,
                                    lstStatisticLevel, lstJudgeRecord_EthnicFemale,
                                    Suppervising, SchoolName, Province, Subject, periodName,
                                    AcademicYear, 4);
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                sheet_FemaleEthnicNX.FitAllColumnsOnOnePage = true;
                if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    sheet_FemaleEthnicNX.Delete();
                }
                else if (schoolSubject != null && entity.SubjectID != 0 && schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    sheet_FemaleEthnic.Delete();
                }
                #endregion
            }

            #endregion
            firstSheet.Delete();
            secondSheet.Delete();

            //End: 2014/11/25 viethd4 sua doi chuc nang Thống kê điểm TBM
            return oBook.ToStream();
        }

        public void SetValueToFile(IVTWorksheet firstSheet, IVTWorksheet sheet, IVTWorksheet sheetNX, int numberConfig, List<SubjectCatBO> lstSubject,
                                    List<EmployeeBO> lstEmployee, List<TeachingAssignment> lstTeachingAssignment,
                                    List<ClassProfileTempBO> lstClassProfileAllEdu, List<EducationLevel> lstEducation,
                                    AverageMarkBySubjectBO entity, List<SummedUpRecordBO> lstSummedUpRecord,
                                    List<MarkRecordBO> lstMarkRecordBO, List<MarkTotal> listCountCurentPupilByClass,
                                    List<StatisticLevelConfig> lstStatisticLevel, List<JudgeRecordBO> lstJudgeRecord,
                                    string Suppervising, string SchoolName, string Province, string Subject, string periodName,
                                    string AcademicYear, int type
           )
        {
            #region header tính điểm
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }

            string bx = type == 2 ? "HỌC SINH NỮ" : type == 3 ? "HỌC SINH DÂN TỘC" : type == 4 ? "HỌC SINH NỮ DÂN TỘC" : "";

            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn + bx : "TRUNG BÌNH MÔN"));
            sheet.SetCellValue("J5", Province + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue("A8", entity.SubjectID != 0 ? "MÔN " + Subject.ToUpper() + " - NĂM HỌC: " + AcademicYear : "NĂM HỌC: " + AcademicYear);
            }
            else
            {
                if (!entity.IsPeriod)
                {
                    sheet.SetCellValue("A8", entity.SubjectID != 0 ? "MÔN " + Subject.ToUpper() + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear : SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet.SetCellValue("A8", entity.SubjectID != 0 ? "MÔN " + Subject.ToUpper() + " - " + periodName + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear : periodName + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
            }
            #endregion

            #region header nhận xét
            sheetNX.SetCellValue("A2", Suppervising);
            sheetNX.SetCellValue("A3", SchoolName);
            sheetNX.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn + bx : "TRUNG BÌNH MÔN"));
            sheetNX.SetCellValue("F5", Province + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheetNX.SetCellValue("A8", entity.SubjectID != 0 ? "MÔN " + Subject + " - NĂM HỌC: " + AcademicYear : "NĂM HỌC: " + AcademicYear);
            }
            else
            {
                if (!entity.IsPeriod)
                {
                    sheetNX.SetCellValue("A8", entity.SubjectID != 0 ? "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear : SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheetNX.SetCellValue("A8", entity.SubjectID != 0 ? "MÔN " + Subject + " - " + periodName + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear : periodName + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
            }

            //Neu chon giao vien, dat tieu de cho giao vien

            sheetNX.SetCellValue("G5", (Province != "" ? Province + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            #endregion

            List<ClassProfileTempBO> lstClassProfile = new List<ClassProfileTempBO>();
            EducationLevel objEducation = new EducationLevel();
            List<int> rowSumEmployee = new List<int>();
            List<ClassProfileTempBO> lstClassProfileEducation = new List<ClassProfileTempBO>();
            List<EmployeeBO> lstSubjectEmployee = new List<EmployeeBO>();
            SubjectCatBO objSubjectEmployee = new SubjectCatBO();
            List<decimal?> lstSummedUpRecordClass = new List<decimal?>();
            List<int> rowSumSubject = new List<int>();
            List<int> rowSumSubjectNX = new List<int>();
            int startRow = 12;
            int startColumn = 1;
            int HSD = 0;//NX
            int HSCD = 0;//NX
            int lastColumnNX = 10;//NX
            int curRowNX = startRow;//NX
            int curColumnNX = startColumn;//NX
            int indexNX;//NX

            int SL = 0;
            int curRow = startRow;
            int curColumn = startColumn;
            int lastColumn = 6 + numberConfig * 2;
            int index = 1;

            int col = 1;
            IVTRange range_level = firstSheet.GetRange("G10", "H11");
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_level, 10, 6 + col);
                sheet.SetCellValue(10, 6 + col, lstStatisticLevel[i].Title);
                col += 2;
            }
            #region nhóm theo bộ môn

            for (int l = 0; l < lstSubject.Count; l++)
            {
                rowSumSubject = new List<int>();
                rowSumSubjectNX = new List<int>();
                objSubjectEmployee = lstSubject[l];
                lstSubjectEmployee = lstEmployee.Where(x => x.SubjectID == objSubjectEmployee.SubjectCatID).ToList();
                #region nhóm theo giáo viên

                #region môn chấm điểm
                if (objSubjectEmployee.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    for (int i = 0; i < lstSubjectEmployee.Count; i++)
                    {
                        ClassProfileTempBO ll = new ClassProfileTempBO();
                        rowSumEmployee = new List<int>();
                        EmployeeBO emp = lstSubjectEmployee[i];
                        List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID && o.SubjectID == objSubjectEmployee.SubjectCatID).ToList();
                        lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID) && o.SubjectID == objSubjectEmployee.SubjectCatID).GroupBy(p => new { p.EducationLevelID, p.ClassProfileID, p.EmployeeID })
                                          .Select(g => g.First())
                                          .ToList();
                        if (lstClassProfile.Count == 0)
                        {
                            continue;
                        }

                        int lastEmpRow = curRow + lstClassProfile.Count - 1;
                        #region nhóm theo khối

                        for (int k = 0; k < lstEducation.Count; k++)
                        {
                            index = 1;
                            int startEmpRow = curRow;
                            curColumn = 1;
                            objEducation = lstEducation[k];
                            lstClassProfileEducation = lstClassProfile.Where(x => x.EducationLevelID == objEducation.EducationLevelID).ToList();
                            #region nhóm theo lớp
                            for (int j = 0; j < lstClassProfileEducation.Count; j++)
                            {
                                int ClassID = lstClassProfileEducation[j].ClassProfileID;
                                if (entity.MarkType == 2)
                                {
                                    lstSummedUpRecordClass = lstSummedUpRecord.Where(o => o.ClassID == ClassID && o.SubjectID == objSubjectEmployee.SubjectCatID).Select(o => o.SummedUpMark).ToList();
                                }
                                else
                                {
                                    lstSummedUpRecordClass = lstMarkRecordBO.Where(o => o.ClassID == ClassID && o.SubjectID == objSubjectEmployee.SubjectCatID).Select(o => o.Mark).ToList();
                                }
                                //stt
                                sheet.SetCellValue(curRow, curColumn, index++);
                                curColumn++;
                                // khối
                                sheet.SetCellValue(curRow, curColumn, objEducation.EducationLevelID);
                                curColumn++;
                                //ten lop
                                sheet.SetCellValue(curRow, curColumn, lstClassProfileEducation[j].DisplayName);
                                curColumn++;
                                //giao vien bo mon
                                sheet.SetCellValue(curRow, curColumn, emp.FullName);
                                curColumn++;
                                //Mon hoc
                                sheet.SetCellValue(curRow, curColumn, objSubjectEmployee.SubjectName);
                                curColumn++;

                                SL = 0;
                                #region fill từng tiêu chí
                                if (listCountCurentPupilByClass.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                                {
                                    //Tong so
                                    sheet.SetCellValue(curRow, curColumn, listCountCurentPupilByClass.Where(o => o.ClassID == ClassID && o.SubjectID == objSubjectEmployee.SubjectCatID).Sum(o => o.TotalPupil));
                                    curColumn++;

                                    for (int m = 0; m < numberConfig; m++)
                                    {
                                        StatisticLevelConfig slc = lstStatisticLevel[m];
                                        if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                        {
                                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o >= slc.MinValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                        }
                                        else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                        {
                                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o > slc.MinValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                        }
                                        else
                                        {
                                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o <= slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o < slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                            {
                                                SL = lstSummedUpRecordClass.Where(o => o == slc.MaxValue).Count();
                                                sheet.SetCellValue(curRow, curColumn, SL);//E
                                                sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                            }
                                        }
                                        curColumn += 2;
                                    }
                                }
                                else
                                {
                                    //Tong so
                                    sheet.SetCellValue(curRow, curColumn, 0);
                                    curColumn++;
                                    for (int m = 0; m < numberConfig; m++)
                                    {
                                        sheet.SetCellValue(curRow, curColumn, SL);//E
                                        sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "F" + curRow + "*100,2),0)");
                                        curColumn += 2;
                                    }
                                }
                                #endregion
                                curRow++;
                                //curColumn = 3;
                                //curRow++;
                                curColumn = startColumn;
                            }

                            #endregion
                            if (lstClassProfileEducation.Count > 0)
                            {
                                //curRow++;
                                rowSumEmployee.Add(curRow);
                                sheet.SetCellValue(curRow, 1, "Cộng theo Khối " + objEducation.EducationLevelID);
                                //Fill dong tong so
                                sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                                sheet.GetRange(curRow, 1, curRow, 5).Merge();
                                sheet.GetRange(curRow, 1, curRow, 1).SetHAlign(VTHAlign.xlHAlignLeft);
                                curColumn = curColumn + 5;

                                sheet.SetFormulaValue(curRow, curColumn, "=SUM(F" + startEmpRow + ":F" + (curRow - 1) + ")");
                                curColumn++;

                                for (int m = 0; m < numberConfig; m++)
                                {
                                    string column = UtilsBusiness.GetExcelColumnName(curColumn);
                                    sheet.SetFormulaValue(curRow, curColumn, "=SUM(" + column + startEmpRow.ToString() + ":" + column + (curRow - 1).ToString() + ")");//E
                                    sheet.SetFormulaValue(curRow, curColumn + 1, "=IF(F" + curRow + ">0" + "," + "ROUND(" + column + curRow + "/" + "F" + curRow + "*100,2),0)");
                                    curColumn += 2;
                                }
                                curRow++;
                            }
                        }

                        #endregion

                        if (emp != null)
                        {
                            curColumn = 1;
                            rowSumSubject.Add(curRow);
                            sheet.SetCellValue(curRow, curColumn, "Cộng theo giáo viên " + emp.FullName);
                            //Fill dong tong so
                            sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                            sheet.GetRange(curRow, curColumn, curRow, 5).Merge();
                            sheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignLeft);
                            curColumn = curColumn + 5;//=SUM(F16,F21,F26)
                            string stringSumEmployee = "=SUM(";
                            for (int u = 0; u < rowSumEmployee.Count(); u++)
                            {
                                stringSumEmployee = stringSumEmployee + "F" + rowSumEmployee[u] + ",";
                            }
                            sheet.SetFormulaValue(curRow, curColumn, stringSumEmployee.Substring(0, stringSumEmployee.Length - 1) + ")");
                            curColumn++;

                            string stringSumEmployeeCompo;
                            for (int m = 0; m < numberConfig; m++)
                            {
                                stringSumEmployeeCompo = "=SUM(";
                                for (int u = 0; u < rowSumEmployee.Count(); u++)
                                {
                                    stringSumEmployeeCompo = stringSumEmployeeCompo + UtilsBusiness.GetExcelColumnName(curColumn) + rowSumEmployee[u] + ",";
                                }
                                string column = UtilsBusiness.GetExcelColumnName(curColumn);
                                sheet.SetFormulaValue(curRow, curColumn, rowSumEmployee.Count > 0 ? stringSumEmployeeCompo.Substring(0, stringSumEmployeeCompo.Length - 1) + ")" : "");//E
                                sheet.SetFormulaValue(curRow, curColumn + 1, rowSumEmployee.Count > 0 ? "=IF(F" + curRow + ">0" + "," + "ROUND(" + column + curRow + "/" + "F" + curRow + "*100,2),0)" : "");
                                curColumn += 2;
                            }
                            curRow++;
                        }
                    }
                    if (objSubjectEmployee != null)
                    {
                        curColumn = 1;
                        sheet.SetCellValue(curRow, curColumn, "Cộng theo môn " + objSubjectEmployee.SubjectName);
                        //Fill dong tong so
                        sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                        sheet.GetRange(curRow, curColumn, curRow, 5).Merge();
                        sheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignLeft);
                        curColumn = curColumn + 5;//=SUM(F16,F21,F26)
                        string stringSumSubject = "=SUM(";
                        for (int u = 0; u < rowSumSubject.Count(); u++)
                        {
                            stringSumSubject = stringSumSubject + "F" + rowSumSubject[u] + ",";
                        }
                        sheet.SetFormulaValue(curRow, curColumn, rowSumSubject.Count > 0 ? stringSumSubject.Substring(0, stringSumSubject.Length - 1) + ",0)" : "");
                        curColumn++;

                        string stringSumSubjectCompo;
                        for (int m = 0; m < numberConfig; m++)
                        {
                            stringSumSubjectCompo = "=SUM(";
                            for (int u = 0; u < rowSumSubject.Count(); u++)
                            {
                                stringSumSubjectCompo = stringSumSubjectCompo + UtilsBusiness.GetExcelColumnName(curColumn) + rowSumSubject[u] + ",";
                            }
                            string column = UtilsBusiness.GetExcelColumnName(curColumn);
                            sheet.SetFormulaValue(curRow, curColumn, rowSumSubject.Count > 0 ? stringSumSubjectCompo.Substring(0, stringSumSubjectCompo.Length - 1) + ",0)" : "");//E
                            sheet.SetFormulaValue(curRow, curColumn + 1, rowSumSubject.Count > 0 ? "=IF(F" + curRow + ">0" + "," + "ROUND(" + column + curRow + "/" + "F" + curRow + "*100,2),0)" : "");
                            curColumn += 2;
                        }
                        curRow++;
                    }
                }
                #endregion
                #region môn nhận xét
                else if (objSubjectEmployee.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    for (int i = 0; i < lstSubjectEmployee.Count; i++)
                    {
                        rowSumEmployee = new List<int>();
                        EmployeeBO emp = lstSubjectEmployee[i];
                        List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID && o.SubjectID == objSubjectEmployee.SubjectCatID).ToList();
                        lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID) && o.SubjectID == objSubjectEmployee.SubjectCatID)
                                                              .GroupBy(p => new { p.EducationLevelID, p.ClassProfileID, p.EmployeeID })
                                                              .Select(g => g.First())
                                                              .ToList();
                        if (lstClassProfile.Count == 0)
                        {
                            continue;
                        }

                        int lastEmpRowNX = curRowNX + lstClassProfile.Count - 1;

                        #region nhóm theo khối
                        for (int k = 0; k < lstEducation.Count; k++)
                        {
                            indexNX = 1;
                            int startEmpRowNX = curRowNX;
                            curColumnNX = 1;
                            objEducation = lstEducation[k];
                            lstClassProfileEducation = lstClassProfile.Where(x => x.EducationLevelID == objEducation.EducationLevelID).ToList();
                            #region nhóm theo lớp
                            for (int j = 0; j < lstClassProfileEducation.Count; j++)
                            {
                                int ClassID = lstClassProfileEducation[j].ClassProfileID;
                                //stt
                                sheetNX.SetCellValue(curRowNX, curColumnNX, indexNX++);
                                curColumnNX++;
                                // khối
                                sheetNX.SetCellValue(curRowNX, curColumnNX, objEducation.EducationLevelID);
                                curColumnNX++;
                                //ten lop
                                sheetNX.SetCellValue(curRowNX, curColumnNX, lstClassProfileEducation[j].DisplayName);
                                curColumnNX++;
                                //giao vien bo mon
                                sheetNX.SetCellValue(curRowNX, curColumnNX, emp.FullName);
                                curColumnNX++;
                                //Mon hoc
                                sheetNX.SetCellValue(curRowNX, curColumnNX, objSubjectEmployee.SubjectName);
                                curColumnNX++;

                                HSD = HSCD = 0;

                                if (entity.MarkType == 2)
                                {
                                    // lấy số lượng học sinh Đạt
                                    HSD = lstSummedUpRecord.Where(u => u.ClassID == ClassID && u.SubjectID == objSubjectEmployee.SubjectCatID).Where(o => !string.IsNullOrEmpty(o.JudgementResult) && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                    // Lấy số lượng học sinh chưa Đạt
                                    HSCD = lstSummedUpRecord.Where(u => u.ClassID == ClassID && u.SubjectID == objSubjectEmployee.SubjectCatID).Where(o => !string.IsNullOrEmpty(o.JudgementResult) && o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                                }
                                else
                                {
                                    // lấy số lượng học sinh Đạt
                                    HSD = lstJudgeRecord.Where(u => u.EducationLevelID == 8).Count();
                                    HSD = lstJudgeRecord.Where(u => u.ClassID == ClassID && u.SubjectID == objSubjectEmployee.SubjectCatID).Count();
                                    HSD = lstJudgeRecord.Where(u => u.ClassID == ClassID && u.SubjectID == objSubjectEmployee.SubjectCatID).Where(o => !string.IsNullOrEmpty(o.Judgement) && o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                    // Lấy số lượng học sinh chưa Đạt
                                    HSCD = lstJudgeRecord.Where(u => u.ClassID == ClassID && u.SubjectID == objSubjectEmployee.SubjectCatID).Where(o => !string.IsNullOrEmpty(o.Judgement) && o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                                }

                                if (listCountCurentPupilByClass.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                                {
                                    //Tong so
                                    sheetNX.SetCellValue(curRowNX, curColumnNX, listCountCurentPupilByClass.Where(o => o.ClassID == ClassID && o.SubjectID == objSubjectEmployee.SubjectCatID).Sum(o => o.TotalPupil));
                                    curColumnNX++;

                                    sheetNX.SetCellValue(curRowNX, curColumnNX, HSD);
                                    sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumnNX) + curRowNX + "/" + "F" + curRowNX + "*100,2),0)");
                                    curColumnNX += 2;

                                    sheetNX.SetCellValue(curRowNX, curColumnNX, HSCD);
                                    sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumnNX) + curRowNX + "/" + "F" + curRowNX + "*100,2),0)");
                                    curColumnNX += 2;
                                }
                                else
                                {
                                    //Tong so
                                    sheetNX.SetCellValue(curRowNX, curColumnNX, 0);
                                    curColumnNX++;

                                    sheetNX.SetCellValue(curRowNX, curColumnNX, HSD);
                                    sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumnNX) + curRowNX + "/" + "F" + curRowNX + "*100,2),0)");
                                    curColumnNX += 2;

                                    sheetNX.SetCellValue(curRowNX, curColumnNX, HSCD);
                                    sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumnNX) + curRowNX + "/" + "F" + curRowNX + "*100,2),0)");
                                    curColumnNX += 2;
                                }

                                curRowNX++;
                                //curColumn = 3;
                                //curRow++;
                                curColumnNX = startColumn;
                            }

                            #endregion
                            if (lstClassProfileEducation.Count > 0)
                            {
                                //curRow++;
                                rowSumEmployee.Add(curRowNX);
                                sheetNX.SetCellValue(curRowNX, 1, "Cộng theo Khối " + objEducation.EducationLevelID);
                                //Fill dong tong so
                                sheetNX.GetRange(curRowNX, startColumn, curRowNX, lastColumnNX).SetFontStyle(true, null, false, null, false, false);
                                sheetNX.GetRange(curRowNX, 1, curRowNX, 5).Merge();
                                sheetNX.GetRange(curRowNX, 1, curRowNX, 1).SetHAlign(VTHAlign.xlHAlignLeft);
                                curColumnNX = curColumnNX + 5;

                                sheetNX.SetFormulaValue(curRowNX, curColumnNX, "=SUM(F" + startEmpRowNX + ":F" + (curRowNX - 1) + ")");
                                curColumnNX++;

                                for (int m = 0; m < 2; m++)
                                {
                                    string column = UtilsBusiness.GetExcelColumnName(curColumnNX);
                                    sheetNX.SetFormulaValue(curRowNX, curColumnNX, "=SUM(" + column + startEmpRowNX.ToString() + ":" + column + (curRowNX - 1).ToString() + ")");//E
                                    sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + column + curRowNX + "/" + "F" + curRowNX + "*100,2),0)");
                                    curColumnNX += 2;
                                }
                                curRowNX++;
                            }
                        }

                        #endregion

                        if (emp != null)
                        {
                            curColumnNX = 1;
                            rowSumSubjectNX.Add(curRowNX);
                            sheetNX.SetCellValue(curRowNX, curColumnNX, "Cộng theo giáo viên " + emp.FullName);
                            //Fill dong tong so
                            sheetNX.GetRange(curRowNX, startColumn, curRowNX, lastColumnNX).SetFontStyle(true, null, false, null, false, false);
                            sheetNX.GetRange(curRowNX, curColumnNX, curRowNX, 5).Merge();
                            sheetNX.GetRange(curRowNX, curColumnNX, curRowNX, curColumnNX).SetHAlign(VTHAlign.xlHAlignLeft);
                            curColumnNX = curColumnNX + 5;//=SUM(F16,F21,F26)
                            string stringSumEmployee = "=SUM(";
                            for (int u = 0; u < rowSumEmployee.Count(); u++)
                            {
                                stringSumEmployee = stringSumEmployee + "F" + rowSumEmployee[u] + ",";
                            }
                            sheetNX.SetFormulaValue(curRowNX, curColumnNX, rowSumEmployee.Count > 0 ? stringSumEmployee.Substring(0, stringSumEmployee.Length - 1) + ")" : "");
                            curColumnNX++;

                            string stringSumEmployeeCompo;
                            for (int m = 0; m < 2; m++)
                            {
                                stringSumEmployeeCompo = "=SUM(";
                                for (int u = 0; u < rowSumEmployee.Count(); u++)
                                {
                                    stringSumEmployeeCompo = stringSumEmployeeCompo + UtilsBusiness.GetExcelColumnName(curColumnNX) + rowSumEmployee[u] + ",";
                                }
                                string column = UtilsBusiness.GetExcelColumnName(curColumnNX);
                                sheetNX.SetFormulaValue(curRowNX, curColumnNX, rowSumEmployee.Count > 0 ? stringSumEmployeeCompo.Substring(0, stringSumEmployeeCompo.Length - 1) + ")" : "");//E
                                sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, rowSumEmployee.Count > 0 ? "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + column + curRowNX + "/" + "F" + curRowNX + "*100,2),0)" : "");
                                curColumnNX += 2;
                            }
                            curRowNX++;
                        }
                    }
                    if (objSubjectEmployee != null)
                    {
                        curColumnNX = 1;
                        sheetNX.SetCellValue(curRowNX, curColumnNX, "Cộng theo môn " + objSubjectEmployee.SubjectName);
                        //Fill dong tong so
                        sheetNX.GetRange(curRowNX, startColumn, curRowNX, lastColumnNX).SetFontStyle(true, null, false, null, false, false);
                        sheetNX.GetRange(curRowNX, curColumnNX, curRowNX, 5).Merge();
                        sheetNX.GetRange(curRowNX, curColumnNX, curRowNX, curColumnNX).SetHAlign(VTHAlign.xlHAlignLeft);
                        curColumnNX = curColumnNX + 5;//=SUM(F16,F21,F26)
                        string stringSumSubject = "=SUM(";
                        for (int u = 0; u < rowSumSubjectNX.Count(); u++)
                        {
                            stringSumSubject = stringSumSubject + "F" + rowSumSubjectNX[u] + ",";
                        }
                        sheetNX.SetFormulaValue(curRowNX, curColumnNX, rowSumSubjectNX.Count > 0 ? stringSumSubject.Substring(0, stringSumSubject.Length - 1) + ",0)" : "");
                        curColumnNX++;

                        string stringSumSubjectCompo;
                        for (int m = 0; m < 2; m++)
                        {
                            stringSumSubjectCompo = "=SUM(";
                            for (int u = 0; u < rowSumSubjectNX.Count(); u++)
                            {
                                stringSumSubjectCompo = stringSumSubjectCompo + UtilsBusiness.GetExcelColumnName(curColumnNX) + rowSumSubjectNX[u] + ",";
                            }
                            string column = UtilsBusiness.GetExcelColumnName(curColumnNX);
                            sheetNX.SetFormulaValue(curRowNX, curColumnNX, rowSumSubjectNX.Count > 0 ? stringSumSubjectCompo.Substring(0, stringSumSubjectCompo.Length - 1) + ",0)" : "");//E
                            sheetNX.SetFormulaValue(curRowNX, curColumnNX + 1, rowSumSubjectNX.Count > 0 ? "=IF(F" + curRowNX + ">0" + "," + "ROUND(" + column + curRowNX + "/" + "F" + curRowNX + "*100,2),0)" : "");
                            curColumnNX += 2;
                        }
                        curRowNX++;
                    }
                }
                #endregion

                #endregion
            }
            #endregion

            sheet.SetCellValue(curRow + 2, lastColumn - 5, "Người lập báo cáo");
            sheet.GetRange(curRow, 1, curRow, lastColumn - 1).Merge();
            sheet.SetCellValue(curRow, 1, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            sheet.GetRange(curRow, 1, curRow, 1).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.GetRange(curRow + 2, lastColumn - 5, curRow + 2, lastColumn).Merge();
            sheet.GetRange(curRow + 2, lastColumn - 5, curRow + 2, lastColumn).SetFontStyle(true, null, false, null, false, false);

            sheet.GetRange(12, startColumn, curRow - 1, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.FitAllColumnsOnOnePage = true;

            //NX
            sheetNX.SetCellValue(curRowNX + 2, lastColumnNX - 1, "Người lập báo cáo");
            sheetNX.SetCellValue(curRowNX, 1, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            sheetNX.GetRange(curRowNX, 1, curRowNX, lastColumnNX - 1).Merge();
            sheetNX.GetRange(curRowNX, 1, curRowNX, 1).SetHAlign(VTHAlign.xlHAlignLeft);
            sheetNX.GetRange(curRowNX + 2, lastColumnNX - 1, curRowNX + 2, lastColumnNX).Merge();
            sheetNX.GetRange(curRowNX + 2, lastColumnNX - 1, curRowNX + 2, lastColumnNX).SetFontStyle(true, null, false, null, false, false);

            sheetNX.GetRange(12, startColumn, curRowNX - 1, lastColumnNX).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheetNX.FitAllColumnsOnOnePage = true;
        }

        public ProcessedReport InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeTBMon_[Học kỳ]_[Môn học]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string Subject = "";
            if (entity.SubjectID != 0)
            {
                Subject = this.SubjectCatBusiness.All.Where(o => o.SubjectCatID == entity.SubjectID).FirstOrDefault().DisplayName;
            }
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            //Start: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            if (entity.MarkType == 1)
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemKTDK");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemTBM");
            }
            if (entity.EmployeeID > 0)
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(EmployeeBusiness.Find(entity.EmployeeID).FullName));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", String.Empty);
            }
            if (outputNamePattern.EndsWith("_"))
            {
                outputNamePattern = outputNamePattern.Remove(outputNamePattern.Length - 1, 1);
            }
            //End: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"MarkType", entity.MarkType},
                {"MarkColumn", entity.MarkColumn},
                {"EmployeeID", entity.EmployeeID}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Thong ke diem KTDK, TBM mon nhan xet nhom theo GV bo mon
        public Stream CreateAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEM_TRUNG_BINH_MON_THEO_GV_NHANXET;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile school = this.SchoolProfileBusiness.Find(entity.SchoolID);
            string SchoolName = school.SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = this.SchoolProfileBusiness.Find(entity.SchoolID).SupervisingDept.SupervisingDeptName.ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            string periodName = period != null ? period.Resolution : String.Empty;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);


            // Lấy danh sách khối học
            //List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            //Lay danh sach giao vien bo mon
            //Lay danh sach giao vien trong truong
            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = entity.SchoolID;
            IQueryable<Employee> query = EmployeeBusiness.Search(dicToGetTeacher);

            if (entity.EmployeeID > 0)
            {
                query = query.Where(o => o.EmployeeID == entity.EmployeeID);
            }

            //Lay danh sach giao vien duong phan cong day mon duoc chon
            IDictionary<string, object> dicToGetAssignment = new Dictionary<string, object>();
            dicToGetAssignment["AcademicYearID"] = entity.AcademicYearID;
            dicToGetAssignment["SubjectID"] = entity.SubjectID;
            if (entity.Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                dicToGetAssignment["Semester"] = entity.Semester;
            }
            IQueryable<TeachingAssignment> iqTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(entity.SchoolID, dicToGetAssignment);



            iqTeachingAssignment = (from ta in iqTeachingAssignment
                                    join ee in query on ta.TeacherID equals ee.EmployeeID
                                    select ta);
            List<TeachingAssignment> lstTeachingAssignment = iqTeachingAssignment.ToList();

            List<EmployeeBO> lstEmployee = (from ta in iqTeachingAssignment
                                            join ee in query on ta.TeacherID equals ee.EmployeeID
                                            select new EmployeeBO
                                            {
                                                EmployeeID = ee.EmployeeID,
                                                Name = ee.Name,
                                                FullName = ee.FullName,
                                                SubjectID = ta.SubjectID
                                            }).Distinct().ToList().OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();


            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);

            IDictionary<string, object> dic = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                 {"PeriodID", null}
                                                            };

            IQueryable<VJudgeRecord> lstJudgeRecordAllStatus = null;
            IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = null;
            if (entity.IsPeriod)
            {
                dic = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                 {"PeriodID", entity.PeriodID.GetValueOrDefault()}
                                                            };
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            //Neu thong ke diem TBM
            else if (entity.MarkType == 2)
            {
                dic = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                 {"PeriodID", null}
                                                            };
                lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null).Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
            }
            //Neu thong ke diem kiem tra dinh ky
            else
            {
                Dictionary<string, object> dicToGetJudgeRecord = new Dictionary<string, object>();
                dicToGetJudgeRecord["AcademicYearID"] = entity.AcademicYearID;
                dicToGetJudgeRecord["SubjectID"] = entity.SubjectID;
                dicToGetJudgeRecord["Semester"] = entity.Semester;
                dicToGetJudgeRecord["Title"] = entity.MarkColumn;

                lstJudgeRecordAllStatus = this.VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, dicToGetJudgeRecord);
            }

            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            int partittionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            IQueryable<PupilOfClassBO> lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                 join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where cs.SubjectID == entity.SubjectID
                                                 && cs.Last2digitNumberSchool == partittionId
                                                  //Viethd4: loai bo cac lop hoc mon theo VNEN
                                                  && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     Genre = q.Genre,
                                                     IsSpecialize = cs.IsSpecializedSubject,
                                                     AppliedType = cs.AppliedType,
                                                     SubjectID = cs.SubjectID,
                                                     EthnicID = q.EthnicID
                                                 };
            //loc ra nhung hoc sinh khong dang ky hoc mon chuyen/tu chon tuong ung
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SemesterID",entity.Semester},
                {"SubjectID",entity.SubjectID},
                {"YearID",Aca.Year}
            };
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID, 0); //xoa mon hoc

            //Khong lay cac hoc sinh mien giam
            lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();

            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            var listCountPupilByClass = lstPOC.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() });
            var listCountPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_EthnicFemale = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();

            List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_EthnicFemale = new List<SummedUpRecordBO>();

            List<JudgeRecordBO> lstJudgeRecord = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_Female = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_Ethnic = new List<JudgeRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord_EthnicFemale = new List<JudgeRecordBO>();
            if (entity.IsPeriod)
            {
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky

                var tmpSummedUp = (from u in lstSummedUpRecordAllStatus
                                   join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                   join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                   where u.SubjectID == entity.SubjectID
                                   && c.IsActive.Value
                                   //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                   select new
                                   {
                                       PupilID = u.PupilID,
                                       ClassID = u.ClassID,
                                       SubjectID = u.SubjectID,
                                       Semester = u.Semester,
                                       JudgementResult = u.JudgementResult,
                                       ReTestJudgement = u.ReTestJudgement,
                                       ReTestMark = u.ReTestMark,
                                       EducationLevelID = c.EducationLevelID,
                                       Genre = v.Genre,
                                       EthnicID = v.EthnicID
                                   });

                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);

                lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, JudgementResult = u.JudgementResult }).ToList();

                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_EthnicFemale = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            }
            if (entity.MarkType == 2)
            {
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky

                var tmpSummedUp = (from u in lstSummedUpRecordAllStatus
                                   join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                   join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                   where u.SubjectID == entity.SubjectID
                                   && c.IsActive.Value
                                   //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                   select new
                                   {
                                       PupilID = u.PupilID,
                                       ClassID = u.ClassID,
                                       SubjectID = u.SubjectID,
                                       Semester = u.Semester,
                                       JudgementResult = u.JudgementResult,
                                       ReTestJudgement = u.ReTestJudgement,
                                       ReTestMark = u.ReTestMark,
                                       EducationLevelID = c.EducationLevelID,
                                       Genre = v.Genre,
                                       EthnicID = v.EthnicID
                                   });

                var iqSummedUpRecord = (from u in tmpSummedUp.ToList()
                                        join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                        select u);


                if (entity.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                    var iqSummedUpFilter = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester))
                        .Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, JudgementResult = u.ReTestJudgement != null ? u.ReTestJudgement : u.JudgementResult });
                    lstSummedUpRecord = iqSummedUpFilter.ToList();

                }
                else
                {
                    lstSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == entity.Semester).Select(u => new SummedUpRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, JudgementResult = u.JudgementResult }).ToList();
                }
                lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstSummedUpRecord_EthnicFemale = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            }
            else
            {
                //Lay danh sach diem neu la thong ke theo Kiem tra dinh ky
                var tmpJudgeRecord = from u in lstJudgeRecordAllStatus
                                     join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                                     join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                     where c.IsActive.Value
                                     //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                     select new
                                     {
                                         PupilID = u.PupilID,
                                         ClassID = u.ClassID,
                                         SubjectID = u.SubjectID,
                                         Semester = u.Semester,
                                         JudgeMent = u.Judgement,
                                         EducationLevelID = c.EducationLevelID,
                                         Genre = v.Genre,
                                         EthnicID = v.EthnicID,
                                         JudgeRecordID = u.JudgeRecordID
                                     };
                var iqJudgeRecord = (from u in tmpJudgeRecord.ToList()
                                     join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                     select u);
                lstJudgeRecord = iqJudgeRecord.Select(u => new JudgeRecordBO { EducationLevelID = u.EducationLevelID, ClassID = u.ClassID, Genre = u.Genre, EthnicID = u.EthnicID, Judgement = u.JudgeMent, JudgeRecordID = u.JudgeRecordID }).ToList();
                lstJudgeRecord_Ethnic = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                lstJudgeRecord_Female = lstJudgeRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                lstJudgeRecord_EthnicFemale = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            }

            IDictionary<string, object> dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID},
                                                                    {"IsVNEN", true}
                                                                };
            // List Class Profile
            IQueryable<ClassSubject> iqClassProfile_Temp = this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dic_ClassProfile);
            List<ClassProfileTempBO> lstClassProfileAllEdu = (from cs in iqClassProfile_Temp
                                                              join c in ClassProfileBusiness.All on cs.ClassID equals c.ClassProfileID
                                                              join em in EmployeeBusiness.All on c.HeadTeacherID equals em.EmployeeID into g
                                                              from j in g.DefaultIfEmpty()
                                                              orderby c.OrderNumber.HasValue ? c.OrderNumber : 0, c.DisplayName
                                                              where c.IsActive.Value
                                                              select new ClassProfileTempBO
                                                              {
                                                                  EducationLevelID = c.EducationLevelID,
                                                                  ClassProfileID = cs.ClassID,
                                                                  OrderNumber = c.OrderNumber,
                                                                  DisplayName = c.DisplayName,
                                                                  EmployeeName = j.FullName != null ? j.FullName : string.Empty
                                                              }).ToList();
            //Neu nhap giao vien, loc danh sach cac lop ma giao vien do day
            //Danh sach phan cong giang day


            List<ClassSubject> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                  join ts in iqTeachingAssignment on cs.ClassID equals ts.ClassID
                                                  where cs.SubjectID == ts.SubjectID
                                                  //Viethd4: Loai cac lop hoc mon nay theo VNEN
                                                  && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                                                  && ((entity.Semester > 2 && (cs.SectionPerWeekFirstSemester.HasValue
                                                  || cs.SectionPerWeekSecondSemester.HasValue)) || ts.Semester == entity.Semester)
                                                  && cs.Last2digitNumberSchool == partittionId
                                                  select cs).ToList();



            //Chiendd: loai cac ban ghi trung nhau
            lstClassProfileAllEdu = (from c in lstClassProfileAllEdu
                                     join cs in lstClassSubject on c.ClassProfileID equals cs.ClassID
                                     select c).Distinct().ToList();

            lstClassProfileAllEdu = lstClassProfileAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "Z1000");


            #region Tạo dữ liệu thống kê điểm thi học kỳ theo môn
            //Fill dữ liệu chung
            sheet.Name = "Tat_ca";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN"));
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue("A8", "MÔN " + Subject + " - NĂM HỌC: " + AcademicYear);
            }
            else
            {
                if (!entity.IsPeriod)
                {
                    sheet.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet.SetCellValue("A8", "MÔN " + Subject + " - " + periodName + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                }
            }

            //Neu chon giao vien, dat tieu de cho giao vien

            sheet.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            int HSD = 0;
            int HSCD = 0;
            int startRow = 14;
            int startColumn = 1;
            int curRow = startRow;
            int curColumn = startColumn;
            int lastColumn = 9;
            int index = 1;
            List<ClassProfileTempBO> lstClassProfile;
            for (int i = 0; i < lstEmployee.Count; i++)
            {
                #region nhóm theo giáo viên
                EmployeeBO emp = lstEmployee[i];
                List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                if (lstClassProfile.Count == 0)
                {
                    continue;
                }

                int startEmpRow = curRow;
                int lastEmpRow = curRow + lstClassProfile.Count - 1;
                sheet.GetRange(startEmpRow, 1, lastEmpRow + 1, 1).Merge();
                sheet.GetRange(startEmpRow, 2, lastEmpRow + 1, 2).Merge();

                //stt
                sheet.SetCellValue(curRow, curColumn, index++);
                curColumn++;

                //ten giao vien
                sheet.SetCellValue(curRow, curColumn, emp.FullName);
                curColumn++;


                for (int j = 0; j < lstClassProfile.Count; j++)
                {
                    #region nhóm theo lớp
                    int ClassID = lstClassProfile[j].ClassProfileID;

                    //ten lop
                    sheet.SetCellValue(curRow, curColumn, lstClassProfile[j].DisplayName);
                    curColumn++;

                    //Giao vien chu nhiem
                    sheet.SetCellValue(curRow, curColumn, lstClassProfile[j].EmployeeName);
                    curColumn++;

                    HSD = HSCD = 0;
                    if (entity.MarkType == 2)
                    {
                        // lấy số lượng học sinh Đạt
                        HSD = lstSummedUpRecord.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                        // Lấy số lượng học sinh chưa Đạt
                        HSCD = lstSummedUpRecord.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    }
                    else
                    {
                        // lấy số lượng học sinh Đạt
                        HSD = lstJudgeRecord.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                        // Lấy số lượng học sinh chưa Đạt
                        HSCD = lstJudgeRecord.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    }

                    //if (listCountPupilByClass.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                    //{
                    //    //Tong so
                    //    sheet.SetCellValue(curRow, curColumn, listCountPupilByClass.Where(o => o.ClassID == ClassID).Sum(o => o.TotalPupil));
                    //    curColumn++;

                    //    sheet.SetCellValue(curRow, curColumn, HSD);
                    //    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    //    curColumn += 2;

                    //    sheet.SetCellValue(curRow, curColumn, HSCD);
                    //    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    //    curColumn += 2;                       
                    //}
                    //else
                    //{
                    //    //Tong so
                    //    sheet.SetCellValue(curRow, curColumn, 0);
                    //    curColumn++;

                    //    sheet.SetCellValue(curRow, curColumn, HSD);
                    //    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    //    curColumn += 2;

                    //    sheet.SetCellValue(curRow, curColumn, HSCD);
                    //    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    //    curColumn += 2;
                    //}
                    #endregion
                    curRow++;
                    curColumn = 3;
                }

                //Fill dong tong so
                if (lstClassProfile.Count > 0)
                {
                    sheet.SetCellValue(curRow, curColumn, "Tổng số");
                    sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                    curColumn++;
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, "=SUM(E" + startEmpRow + ":E" + lastEmpRow.ToString() + ")");
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, "=SUM(F" + startEmpRow + ":F" + lastEmpRow.ToString() + ")");
                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    curColumn += 2;

                    sheet.SetCellValue(curRow, curColumn, "=SUM(H" + startEmpRow + ":H" + lastEmpRow.ToString() + ")");
                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    curColumn += 2;

                }
                else
                {
                    sheet.SetCellValue(curRow, curColumn, "Tổng số");
                    sheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(true, null, false, null, false, false);
                    curColumn++;
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, 0);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, 0);
                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    curColumn += 2;

                    sheet.SetCellValue(curRow, curColumn, 0);
                    sheet.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                    curColumn += 2;
                }
                #endregion
                curRow++;
                curColumn = startColumn;
            }

            //Fill row tong cua truong
            int startRow_School = 13;

            sheet.SetCellValue(startRow_School, startColumn, "Tổng cộng");
            sheet.GetRange(startRow_School, startColumn, startRow_School, 4).Merge();
            sheet.GetRange(startRow_School, startColumn, startRow_School, lastColumn).FillColor(194, 240, 194);

            sheet.SetCellValue(startRow_School, 5, "=SUM(E" + startRow.ToString() + ":E" + (curRow - 1).ToString() + ")/2");//E

            curColumn = 6;

            sheet.SetCellValue(startRow_School, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + startRow.ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ")/2");//E
            sheet.SetCellValue(startRow_School, curColumn + 1, "=IF(E13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + startRow_School + "/" + "E" + startRow_School + "*100,2),0)");
            curColumn += 2;

            sheet.SetCellValue(startRow_School, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + startRow.ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ")/2");//E
            sheet.SetCellValue(startRow_School, curColumn + 1, "=IF(E13" + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + startRow_School + "/" + "E" + startRow_School + "*100,2),0)");
            curColumn += 2;

            sheet.SetCellValue(curRow + 2, lastColumn - 1, "Người lập báo cáo");
            sheet.GetRange(curRow + 2, lastColumn - 1, curRow + 2, lastColumn).Merge();
            sheet.GetRange(curRow + 2, lastColumn - 1, curRow + 2, lastColumn).SetFontStyle(true, null, false, null, false, false);

            IVTRange headerRange = sheet.GetRange(11, startColumn, 12, lastColumn);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

            sheet.GetRange(13, startColumn, curRow - 1, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            if (entity.FemaleChecked)
            {
                #region Thống kê điểm trung bình môn học sinh nu
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(sheet);
                sheet_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ");
                sheet_Female.Name = "HS_Nu";

                HSD = 0;
                HSCD = 0;
                startRow = 14;
                startColumn = 1;
                curRow = startRow;
                curColumn = startColumn;
                lastColumn = 9;

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeBO emp = lstEmployee[i];
                    List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                    lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                    if (lstClassProfile.Count == 0)
                    {
                        continue;
                    }

                    //stt
                    curColumn++;

                    //ten giao vien
                    curColumn++;

                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int ClassID = lstClassProfile[j].ClassProfileID;

                        //ten lop
                        curColumn++;

                        //Giao vien chu nhiem
                        curColumn++;

                        HSD = HSCD = 0;
                        if (entity.MarkType == 2)
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstSummedUpRecord_Female.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstSummedUpRecord_Female.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }
                        else
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstJudgeRecord_Female.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstJudgeRecord_Female.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }

                        if (listCountPupilByClass_Female.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            //Tong so
                            sheet_Female.SetCellValue(curRow, curColumn, listCountPupilByClass_Female.Where(o => o.ClassID == ClassID).Sum(o => o.TotalPupil));
                            curColumn++;

                            sheet_Female.SetCellValue(curRow, curColumn, HSD);
                            sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;

                            sheet_Female.SetCellValue(curRow, curColumn, HSCD);
                            sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;


                        }
                        else
                        {
                            //Tong so
                            sheet_Female.SetCellValue(curRow, curColumn, 0);
                            curColumn++;

                            sheet_Female.SetCellValue(curRow, curColumn, HSD);
                            sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;

                            sheet_Female.SetCellValue(curRow, curColumn, HSCD);
                            sheet_Female.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;
                        }

                        curRow++;
                        curColumn = 3;
                    }

                    curRow++;
                    curColumn = startColumn;

                }

                sheet_Female.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.EthnicChecked)
            {
                #region Thống kê điểm trung bình môn học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(sheet);
                sheet_Ethnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH DÂN TỘC");
                sheet_Ethnic.Name = "HS_DT";

                HSD = 0;
                HSCD = 0;
                startRow = 14;
                startColumn = 1;
                curRow = startRow;
                curColumn = startColumn;
                lastColumn = 9;

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeBO emp = lstEmployee[i];
                    List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                    lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                    if (lstClassProfile.Count == 0)
                    {
                        continue;
                    }
                    //stt
                    curColumn++;

                    //ten giao vien
                    curColumn++;

                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int ClassID = lstClassProfile[j].ClassProfileID;

                        //ten lop
                        curColumn++;

                        //Giao vien chu nhiem
                        curColumn++;

                        HSD = HSCD = 0;
                        if (entity.MarkType == 2)
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstSummedUpRecord_Ethnic.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstSummedUpRecord_Ethnic.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }
                        else
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstJudgeRecord_Ethnic.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstJudgeRecord_Ethnic.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }

                        if (listCountPupilByClass_Ethnic.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            //Tong so
                            sheet_Ethnic.SetCellValue(curRow, curColumn, listCountPupilByClass_Ethnic.Where(o => o.ClassID == ClassID).Sum(o => o.TotalPupil));
                            curColumn++;

                            sheet_Ethnic.SetCellValue(curRow, curColumn, HSD);
                            sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;

                            sheet_Ethnic.SetCellValue(curRow, curColumn, HSCD);
                            sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;


                        }
                        else
                        {
                            //Tong so
                            sheet_Ethnic.SetCellValue(curRow, curColumn, 0);
                            curColumn++;

                            sheet_Ethnic.SetCellValue(curRow, curColumn, HSD);
                            sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;

                            sheet_Ethnic.SetCellValue(curRow, curColumn, HSCD);
                            sheet_Ethnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;
                        }

                        curRow++;
                        curColumn = 3;
                    }

                    curRow++;
                    curColumn = startColumn;

                }

                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region Thống kê điểm trung bình môn học sinh dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(sheet);
                sheet_FemaleEthnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM " + (entity.MarkType == 1 ? "KIỂM TRA ĐỊNH KỲ - " + entity.MarkColumn : "TRUNG BÌNH MÔN") + " HỌC SINH NỮ DÂN TỘC");
                sheet_FemaleEthnic.Name = "HS_Nu_DT";

                HSD = 0;
                HSCD = 0;
                startRow = 14;
                startColumn = 1;
                curRow = startRow;
                curColumn = startColumn;
                lastColumn = 9;

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeBO emp = lstEmployee[i];
                    List<TeachingAssignment> lstTa = lstTeachingAssignment.Where(o => o.TeacherID == emp.EmployeeID).ToList();
                    lstClassProfile = lstClassProfileAllEdu.Where(o => lstTa.Any(u => o.ClassProfileID == u.ClassID)).ToList();
                    if (lstClassProfile.Count == 0)
                    {
                        continue;
                    }

                    //stt
                    curColumn++;

                    //ten giao vien
                    curColumn++;

                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int ClassID = lstClassProfile[j].ClassProfileID;

                        //ten lop
                        curColumn++;

                        //Giao vien chu nhiem
                        curColumn++;

                        HSD = HSCD = 0;
                        if (entity.MarkType == 2)
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstSummedUpRecord_EthnicFemale.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstSummedUpRecord_EthnicFemale.Where(u => u.ClassID == ClassID).Where(o => o.JudgementResult.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }
                        else
                        {
                            // lấy số lượng học sinh Đạt
                            HSD = lstJudgeRecord_EthnicFemale.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = lstJudgeRecord_EthnicFemale.Where(u => u.ClassID == ClassID).Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        }

                        if (listCountPupilByClass_EthnicFemale.Where(o => o.ClassID == ClassID).FirstOrDefault() != null)
                        {
                            //Tong so
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, listCountPupilByClass_EthnicFemale.Where(o => o.ClassID == ClassID).Sum(o => o.TotalPupil));
                            curColumn++;

                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, HSD);
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;

                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, HSCD);
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;


                        }
                        else
                        {
                            //Tong so
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, 0);
                            curColumn++;

                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, HSD);
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;

                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn, HSCD);
                            sheet_FemaleEthnic.SetCellValue(curRow, curColumn + 1, "=IF(E" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "E" + curRow + "*100,2),0)");
                            curColumn += 2;
                        }

                        curRow++;
                        curColumn = 3;
                    }

                    curRow++;
                    curColumn = startColumn;

                }

                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();
            return oBook.ToStream();
        }

        public ProcessedReport InsertAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeTBMon_[Học kỳ]_[Môn học]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string Subject = this.SubjectCatBusiness.All.Where(o => o.SubjectCatID == entity.SubjectID).FirstOrDefault().DisplayName;

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            //Start: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            if (entity.MarkType == 1)
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemKTDK");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[MarkType]", "DiemTBM");
            }
            if (entity.EmployeeID > 0)
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(EmployeeBusiness.Find(entity.EmployeeID).FullName));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", String.Empty);
            }
            if (outputNamePattern.EndsWith("_"))
            {
                outputNamePattern = outputNamePattern.Remove(outputNamePattern.Length - 1, 1);
            }
            //End: 2014/11/24 viethd4 Sua doi Thong ke ket qua hoc tap
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"MarkType", entity.MarkType},
                {"MarkColumn", entity.MarkColumn},
                {"EmployeeID", entity.EmployeeID}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion
    }

    public class MarkTotal
    {
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public int TotalPupil { get; set; }
    }

}
