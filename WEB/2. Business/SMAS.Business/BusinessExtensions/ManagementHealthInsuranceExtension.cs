﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class ManagementHealthInsuranceBusiness
    {
        public IQueryable<ManagementHealthInsurance> Search(Dictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            IQueryable<ManagementHealthInsurance> iquery = ManagementHealthInsuranceRepository.All;

            if (SchoolID != 0)
                iquery = iquery.Where(x => x.SchoolID == SchoolID);
            if (AcademicYearID != 0)
                iquery = iquery.Where(x => x.AcademicYearID == AcademicYearID);
            if (ClassID != 0)
                iquery = iquery.Where(x => x.ClassID == ClassID);
            if (PupilID != 0)
                iquery = iquery.Where(x => x.PupilID == ClassID);

            return iquery;
        }

        public void Create(ManagementHealthInsurance objMDI) 
        {
            Dictionary<string, object> dic = new Dictionary<string,object>();
            ManagementHealthInsurance objMDITemp = ManagementHealthInsuranceBusiness.Search(dic)
                                                    .Where(x => x.AcademicYearID == objMDI.AcademicYearID
                                                    && x.SchoolID == objMDI.SchoolID
                                                    && x.ClassID == objMDI.ClassID
                                                    && x.PupilID == objMDI.PupilID).FirstOrDefault();
            if (objMDITemp == null)
            {
                this.ManagementHealthInsuranceBusiness.Insert(objMDI);
                this.ManagementHealthInsuranceBusiness.Save();
            }
        }

        public void UpdateDB(ManagementHealthInsurance objMDI)
        {
            ManagementHealthInsurance objMHI = ManagementHealthInsuranceBusiness.Find(objMDI.ManagementHealthInsuranceID);
            if (objMDI != null)
            {
                objMHI.ClassID = objMDI.ClassID;
                objMHI.PupilID = objMDI.PupilID;
                objMHI.CardNumber = objMDI.CardNumber;
                objMHI.Address = objMDI.Address;
                objMHI.RegisterAddress = objMDI.RegisterAddress;
                objMHI.CardCode = objMDI.CardCode;
                objMHI.FromDate = objMDI.FromDate;
                objMHI.ToDate = objMDI.ToDate;
                objMHI.DateRange = objMDI.DateRange;
                objMHI.PlaceRange = objMDI.PlaceRange;
                objMHI.ModifiedDate = DateTime.Now;

                this.ManagementHealthInsuranceBusiness.Update(objMHI);
                this.ManagementHealthInsuranceBusiness.Save();    
            }
            
        }

        public void SaveImport(int schoolID, int academicID, int appliLevelID, List<ManagementHealthInsurance> lstMHIBO) 
        {
            ManagementHealthInsurance objMHI = new ManagementHealthInsurance();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["AcademicYearID"] = academicID;
            List<ManagementHealthInsurance> lstMHI = this.Search(dic).ToList();
            foreach (var item in lstMHIBO) 
            {
                objMHI = lstMHI.Where(x => x.ClassID == item.ClassID && x.PupilID == item.PupilID).FirstOrDefault();
                if (objMHI == null)
                {                   
                    this.Insert(item);                   
                }
                else 
                {
                    objMHI.CardNumber = item.CardNumber;
                    objMHI.Address = item.Address;
                    objMHI.RegisterAddress = item.RegisterAddress;
                    objMHI.CardCode = item.CardCode;
                    objMHI.FromDate = item.FromDate;
                    objMHI.ToDate = item.ToDate;
                    objMHI.DateRange = item.DateRange;
                    objMHI.PlaceRange = item.PlaceRange;
                    objMHI.ModifiedDate = DateTime.Now;
                    this.Update(objMHI);
                }

            }

            this.Save();
        }

        //public void Update(ManagementHealthInsurance objMDI) 
        //{ 
           
        //   ManagementHealthInsuranceBusiness objMDIDB = ManagementHealthInsuranceBusiness.All
                                                                
        //}

    }
}
