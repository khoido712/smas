/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class SubCommitteeBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int ABBREVIATION_MAX_LENGHT = 10; //Độ dài trường tên viết tắt

        #endregion

        #region base method
        /// <summary>
        /// Tìm kiếm ban học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution">Diễn giải phân ban/ban học</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="Abbreviation">Tên viết tắt phân ban</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<SubCommittee></returns>
        public IQueryable<SubCommittee> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            string Abbreviation = Utils.GetString(dic, "Abbreviation");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<SubCommittee> lsSubCommittee = SubCommitteeRepository.All;

            if (IsActive.HasValue)
                lsSubCommittee = lsSubCommittee.Where(sub => sub.IsActive == IsActive);
            if (Resolution.Trim().Length != 0)
                lsSubCommittee = lsSubCommittee.Where(sub => sub.Resolution.ToLower().Contains(Resolution.ToLower()));
            if (Abbreviation.Trim().Length != 0)
                lsSubCommittee = lsSubCommittee.Where(sub => sub.Abbreviation.ToLower().Contains(Abbreviation.ToLower()));
           
            return lsSubCommittee;

        }

        /// <summary>
        /// Xóa ban học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SubCommitteeID">ID ban học</param>
        public void Delete(int SubCommitteeID )
        {
            //Bạn chưa chọn ban học cần xóa hoặc ban học bạn chọn đã bị xóa khỏi hệ thống
            new SubCommitteeBusiness(null).CheckAvailable((int)SubCommitteeID, "SubCommittee_Label_SubCommitteeID", true);

            //Không thể xóa ban học đang sử dụng
            new SubCommitteeBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "SubCommittee", SubCommitteeID, "SubCommittee_Label_SubCommitteeID");
            base.Delete(SubCommitteeID, true);
        }

        /// <summary>
        /// Thêm ban học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>>
        /// <param name="subCommittee">Đối tượng SubCommittee</param>
        /// <returns>Đối tượng SubCommittee</returns>
        public override SubCommittee Insert(SubCommittee subCommittee)
        {
            //Tên ban học không được để trống 
            Utils.ValidateRequire(subCommittee.Resolution, "SubCommittee_Label_Resolution");
            //Độ dài tên ban học không được vượt quá 50 
            Utils.ValidateMaxLength(subCommittee.Resolution, RESOLUTION_MAX_LENGTH, "SubCommittee_Label_Resolution");
            //Tên ban học đã tồn tại 
            new SubCommitteeBusiness(null).CheckDuplicate(subCommittee.Resolution, GlobalConstants.LIST_SCHEMA, "SubCommittee", "Resolution", true, subCommittee.SubCommitteeID, "SubCommittee_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(subCommittee.Description, DESCRIPTION_MAX_LENGTH, "Description");
            //Độ dài trường Tên viết tắt không được vượt quá 10 - Kiểm tra Abbreviation
            Utils.ValidateMaxLength(subCommittee.Abbreviation, ABBREVIATION_MAX_LENGHT, "SubCommittee_Label_Abbreviation");

            //Insert
            return base.Insert(subCommittee);

        }

        /// <summary>
        /// Sửa ban học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>>
        /// <param name="subCommittee">Đối tượng SubCommittee</param>
        /// <returns>Đối tượng SubCommittee</returns>
        public override SubCommittee Update(SubCommittee subCommittee)
        {
            //Bạn chưa chọn ban học cần sửa hoặc ban học bạn chọn đã bị xóa khỏi hệ thống
            new SubCommitteeBusiness(null).CheckAvailable((int)subCommittee.SubCommitteeID, "SubCommittee_Label_SubCommitteeID", true);
            //Tên ban học không được để trống 
            Utils.ValidateRequire(subCommittee.Resolution, "SubCommittee_Label_Resolution");
            //Độ dài tên ban học không được vượt quá 50 
            Utils.ValidateMaxLength(subCommittee.Resolution, RESOLUTION_MAX_LENGTH, "SubCommittee_Label_Resolution");
            //Tên ban học đã tồn tại 
            new SubCommitteeBusiness(null).CheckDuplicate(subCommittee.Resolution, GlobalConstants.LIST_SCHEMA, "SubCommittee", "Resolution", true, subCommittee.SubCommitteeID, "SubCommittee_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(subCommittee.Description, DESCRIPTION_MAX_LENGTH, "Description");
            //Độ dài trường Tên viết tắt không được vượt quá 10 - Kiểm tra Abbreviation
            Utils.ValidateMaxLength(subCommittee.Abbreviation, ABBREVIATION_MAX_LENGHT, "SubCommittee_Label_Abbreviation");
            return base.Update(subCommittee);
        }
        #endregion
    }
}
