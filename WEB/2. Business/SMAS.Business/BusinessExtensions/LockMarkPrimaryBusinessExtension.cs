﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class LockMarkPrimaryBusiness
    {
        #region Tim kiem
        /// <summary>
        /// Tim kiem trong bang theo dieu kien tim kiem
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<LockMarkPrimary> SearchLockedMark(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int? IsCommenting = Utils.GetNullableInt(SearchInfo, "IsCommenting");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");

            IQueryable<LockMarkPrimary> ListLockedMarkDetail = LockMarkPrimaryRepository.All;
            // Tim kiem theo dieu kien
            if (SchoolID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.SchoolID == SchoolID);
            }
            if (EducationLevelID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (AppliedLevel != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }
            if (AcademicYearID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.ClassID == ClassID);
            }
            if (IsCommenting.HasValue)
            {
                ListLockedMarkDetail = ListLockedMarkDetail.Where(x => x.IsCommenting == IsCommenting);
            }
            return ListLockedMarkDetail;
        }
        #endregion

        /// <summary>
        /// Quanglm1
        /// Kiem tra xem con diem bi khoa o client truyen len co bi mo khoa tu phia server hay khong
        /// Neu co thi se bao loi de tranh truong hop du lieu diem bi xoa
        /// </summary>
        /// <param name="clientLockMarkTitle"></param>
        /// <param name="classID"></param>
        public void CheckCurrentLockMark(string clientLockMarkTitle, int classID, int isCommenting)
        {
            if (string.IsNullOrWhiteSpace(clientLockMarkTitle))
            {
                return;
            }
            // Danh sach con diem bi khoa tu client truyen len
            List<string> listLockTitle = clientLockMarkTitle.Split(',').Where(o => !string.IsNullOrWhiteSpace(o)).ToList();
            LockMarkPrimary lockPrimary = SearchLockedMark(new Dictionary<string, object>() { 
            { "ClassID", classID }, { "IsCommenting", isCommenting }}).FirstOrDefault();
            // Neu client co ma tren server khong bi khoa thi bao loi
            if (lockPrimary == null)
            {
                List<object> listParam = new List<object>();
                listParam.Add(string.Join(",", listLockTitle));
                throw new BusinessException("LockMarkPrimary_Label_ErrorNotSameLock", listParam);
            }
            string curentLockTitle = lockPrimary.LockPrimary.Replace("_", "");
            // Danh sach con diem bi khoa hien tai
            List<string> listCurrentLockTitle = curentLockTitle.Split(',').Where(o => !string.IsNullOrWhiteSpace(o)).ToList();
            // Loai bo nhung con diem khoa trung giua client truyen len va du lieu khoa hien tai
            listLockTitle.RemoveAll(s => listCurrentLockTitle.Contains(s));
            // Neu ton tai con diem bi khoa o client nhung hien tai da mo khoa thi bao loi
            if (listLockTitle.Count > 0)
            {
                List<object> listParam = new List<object>();
                listParam.Add(string.Join(",", listLockTitle));
                throw new BusinessException("LockMarkPrimary_Label_ErrorNotSameLock", listParam);
            }
        }

        public void InsertAndDelete(List<LockMarkPrimary> lstLockMarkInsert, List<LockMarkPrimary> lstLockMarkDelete)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstLockMarkDelete != null && lstLockMarkDelete.Count > 0)
                {
                    LockMarkPrimaryBusiness.DeleteAll(lstLockMarkDelete);
                }
                if (lstLockMarkInsert != null && lstLockMarkInsert.Count > 0)
                {
                    for (int i = 0; i < lstLockMarkInsert.Count; i++)
                    {
                        LockMarkPrimaryBusiness.Insert(lstLockMarkInsert[i]);
                    }
                }
                LockMarkPrimaryBusiness.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = "List<LockMarkPrimary> lstLockMarkInsert, List<LockMarkPrimary> lstLockMarkDelete";
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertAndDelete", paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }



    }
}
