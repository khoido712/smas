/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SymbolConfigBusiness
    {
        #region Search
        /// <summary>        
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<SymbolConfig> Search(IDictionary<string, object> dic)
        {
            string SymbolConfigName = Utils.GetString(dic, "SymbolConfigName");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<SymbolConfig> Query = SymbolConfigRepository.All;
            if (IsActive.HasValue)
            {
                Query = Query.Where(tcr => tcr.IsActive == IsActive);
            }
            if (SymbolConfigName.Trim().Length != 0)
            {
                Query = Query.Where(tcr => tcr.SymbolConfigName.Contains(SymbolConfigName));
            }

            return Query;

        }
        #endregion
        
    }
}