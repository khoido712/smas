﻿using System;
using System.Collections.Generic;
using System.Linq;

using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;


namespace SMAS.Business.Business
{
    public partial class RewardCommentFinalBusiness
    {
        public List<RewardCommentFinal> GetListRewardCommentFinal(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int mod = Utils.GetInt(dic, "ModPartion");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID, mod);
            List<RewardCommentFinal> lstRewardCommentFinal = (from r in RewardCommentFinalBusiness.All
                                                              where r.SchoolID == schoolID
                                                              && r.AcademicYearID == academicYearID
                                                              && r.ClassID == classID
                                                              && r.SemesterID == semesterID
                                                              && r.LastDigitSchoolID == lastDigit
                                                              select r).ToList();
            return lstRewardCommentFinal;
        }

        //Lay danh sach khen thuong cua truong trong hoc ky
        public List<RewardCommentFinalBO> GetRewardFinalOfSchool(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int semesterID = Utils.GetInt(dic, "Semester");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID, 20);
            List<RewardCommentFinalBO> lstRewardFinal = (from r in RewardCommentFinalRepository.All
                                                         join pp in PupilProfileRepository.All on r.PupilID equals pp.PupilProfileID
                                                         join cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID) on r.ClassID equals cp.ClassProfileID
                                                         where r.SchoolID == schoolID
                                                         && r.AcademicYearID == academicYearID
                                                         && r.SemesterID == semesterID
                                                         && r.LastDigitSchoolID == lastDigit
                                                         && cp.IsActive == true
                                                         select new RewardCommentFinalBO
                                                         {
                                                             PupilID = r.PupilID,
                                                             ClassID = r.ClassID,
                                                             EducationLevelID = cp.EducationLevelID,
                                                             IsCombinedClass = cp.IsCombinedClass,
                                                             IsDisabled = pp.IsDisabled,
                                                             Genre = pp.Genre,
                                                             EthnicID = pp.EthnicID,
                                                             RewardID = r.RewardID

                                                         }).ToList().Where(o => !String.IsNullOrWhiteSpace(o.RewardID)).ToList();
            return lstRewardFinal;
        }

        public List<SubjectViewModel> GetCommentByPupil(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID);
            int year = Utils.GetInt(dic, "Year");
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
            int evaluationId = Utils.GetInt(dic, "EvaluationId");
            List<SubjectViewModel> lstPupilComment = new List<SubjectViewModel>();

            if (isMovedHistory)
            {
                lstPupilComment = (from c in ClassSubjectBusiness.SearchBySchool(schoolID)
                                   join sum in SummedEvaluationHistoryBusiness.All on c.SubjectID equals sum.EvaluationCriteriaID
                                   where c.ClassID == classID
                                   && sum.ClassID == classID
                                   && sum.SchoolID == schoolID
                                   && sum.AcademicYearID == academicYearID
                                   && (sum.PupilID == pupilID || pupilID == 0)
                                   && sum.LastDigitSchoolID == lastDigit
                                   && sum.SemesterID == semesterID
                                   && (sum.EvaluationID==evaluationId || evaluationId==0)
                                   select new SubjectViewModel
                                   {
                                       EvaluationID = sum.EvaluationID,
                                       PupilID = sum.PupilID,
                                       SubjectID = c.SubjectID,
                                       SubjectName = c.SubjectCat.DisplayName,
                                       //DisplayComemnt = sum.EndingComments + ".Điểm KTDK:" + sum.PeriodicEndingMark + ".Đánh giá:" + sum.EndingEvaluation,
                                       EndingComment = sum.EndingComments,
                                       PeriodicEndingMark = sum.PeriodicEndingMark,
                                       EndingEvaluation = sum.EndingEvaluation
                                   }).ToList();
            }
            else
            {
                lstPupilComment = (from c in ClassSubjectBusiness.SearchBySchool(schoolID)
                                   join sum in SummedEvaluationBusiness.All on c.SubjectID equals sum.EvaluationCriteriaID
                                   where c.ClassID == classID
                                   && sum.ClassID == classID
                                   && sum.SchoolID == schoolID
                                   && sum.AcademicYearID == academicYearID
                                   && (sum.PupilID == pupilID || pupilID == 0)
                                   && sum.LastDigitSchoolID == lastDigit
                                   && sum.SemesterID == semesterID
                                   && (sum.EvaluationID == evaluationId || evaluationId == 0)
                                   select new SubjectViewModel
                                   {
                                       EvaluationID = sum.EvaluationID,
                                       PupilID = sum.PupilID,
                                       SubjectID = c.SubjectID,
                                       SubjectName = c.SubjectCat.DisplayName,
                                       //DisplayComemnt = sum.EndingComments + ".Điểm KTDK:" + sum.PeriodicEndingMark + ".Đánh giá:" + sum.EndingEvaluation,
                                       EndingComment = sum.EndingComments,
                                       PeriodicEndingMark = sum.PeriodicEndingMark,
                                       EndingEvaluation = sum.EndingEvaluation
                                   }).ToList();
            }

            return lstPupilComment;
        }

        public List<SummedEvaluationBO> GetCapacityOrQuality(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID);
            int year = Utils.GetInt(dic, "Year");
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
            List<SummedEvaluationBO> lstSummedEvaluation = new List<SummedEvaluationBO>();
            if (isMovedHistory)
            {
                lstSummedEvaluation = (from s in SummedEvaluationHistoryBusiness.All
                                       where s.SchoolID == schoolID
                                       && s.AcademicYearID == academicYearID
                                       && s.ClassID == classID
                                       && s.SemesterID == semesterID
                                       && s.PupilID == pupilID
                                       && (s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                       select new SummedEvaluationBO
                                       {
                                           PupilID = s.PupilID,
                                           ClassID = s.ClassID,
                                           EndingComment = s.EndingComments,
                                           EvaluationID = s.EvaluationID,
                                           EvaluationCriteriaID = s.EvaluationCriteriaID
                                       }).ToList();
            }
            else
            {
                lstSummedEvaluation = (from s in SummedEvaluationBusiness.All
                                       where s.SchoolID == schoolID
                                       && s.AcademicYearID == academicYearID
                                       && s.ClassID == classID
                                       && s.SemesterID == semesterID
                                       && s.PupilID == pupilID
                                       && (s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                       select new SummedEvaluationBO
                                       {
                                           PupilID = s.PupilID,
                                           ClassID = s.ClassID,
                                           EndingComment = s.EndingComments,
                                           EvaluationID = s.EvaluationID,
                                           EvaluationCriteriaID = s.EvaluationCriteriaID
                                       }).ToList();
            }

            return lstSummedEvaluation;
        }

        public void InsertOrUpdate(IDictionary<string, object> dic, List<RewardCommentFinal> lstRewardCommentFinal)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int mode = Utils.GetInt(dic, "ModPartion");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID, mode);
            List<RewardCommentFinal> lstRewardCommentDB = (from r in RewardCommentFinalBusiness.All
                                                           where r.SchoolID == schoolID
                                                           && r.AcademicYearID == academicYearID
                                                           && r.ClassID == classID
                                                           && r.SemesterID == semesterID
                                                           && r.LastDigitSchoolID == lastDigit
                                                           select r).ToList();

            RewardCommentFinal objRewardComment = null;
            RewardCommentFinal objRewardCommentDB = null;
            List<RewardCommentFinal> lstInsert = new List<RewardCommentFinal>();
            List<RewardCommentFinal> lstUpdate = new List<RewardCommentFinal>();
            for (int i = 0; i < lstRewardCommentFinal.Count; i++)
            {
                objRewardComment = lstRewardCommentFinal[i];
                objRewardCommentDB = lstRewardCommentDB.Where(p => p.PupilID == objRewardComment.PupilID).FirstOrDefault();
                if (objRewardCommentDB == null)
                {
                    lstInsert.Add(objRewardComment);
                }
                else
                {
                    objRewardCommentDB.OutstandingAchievement = objRewardComment.OutstandingAchievement;
                    objRewardCommentDB.RewardID = objRewardComment.RewardID;
                    objRewardCommentDB.UpdateTime = DateTime.Now;
                    lstUpdate.Add(objRewardCommentDB);
                }
            }

            if (lstInsert != null && lstInsert.Count > 0)
            {
                foreach (RewardCommentFinal item in lstInsert)
                {
                    RewardCommentFinalBusiness.Insert(item);
                }
            }

            if (lstUpdate != null && lstUpdate.Count > 0)
            {
                foreach (RewardCommentFinal item in lstUpdate)
                {
                    RewardCommentFinalBusiness.Update(item);
                }
            }

            RewardCommentFinalBusiness.Save();
        }

        public class SubjectViewModel
        {
            public int EvaluationID { get; set; }
            public int PupilID { get; set; }
            public int SubjectID { get; set; }
            public string SubjectName { get; set; }
            public string EndingComment { get; set; }//Nhận xét
            public int? PeriodicEndingMark { get; set; }//Điểm KTDK
            public string EndingEvaluation { get; set; }//Đánh giá
        }
    }
}
