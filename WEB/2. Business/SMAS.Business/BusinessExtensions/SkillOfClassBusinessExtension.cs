/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SkillOfClassBusiness
    {
        public IQueryable<SkillOfClass> Search(IDictionary<string, object> dic)
        {
            int SkillID = Utils.GetInt(dic, "SkillID");
            int SkillTitleID = Utils.GetInt(dic, "SkillTitleID");
            string Description = Utils.GetString(dic, "Description");
            System.DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            System.DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");

            string Period = Utils.GetString(dic, "Period");
            System.DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            System.DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            IQueryable<SkillOfClass> Query = SkillOfClassRepository.All;

            if (SkillID != 0)
            {
                Query = Query.Where(evd => evd.SkillOfClassID == SkillID);
            }
            if (SkillTitleID != 0)
            {
                Query = Query.Where(evd => evd.SkillTitleID == SkillTitleID);
            }

            if (Period.Trim().Length >0)
            {
                Query = Query.Where(evd => evd.Period == Period);
            }

            if (!String.IsNullOrEmpty(Description)) Query = Query.Where(o => o.Description.ToLower().Contains(Description.ToLower()));

            if (FromDate.HasValue)
            {
                Query = Query.Where(evd => evd.FromDate.Value.Day == FromDate.Value.Day && evd.FromDate.Value.Month == FromDate.Value.Month
                    && evd.FromDate.Value.Year == FromDate.Value.Year);
            }

            if (ToDate.HasValue)
            {
                Query = Query.Where(evd => evd.ToDate.Value.Day == ToDate.Value.Day && evd.ToDate.Value.Month == ToDate.Value.Month
                    && evd.ToDate.Value.Year == ToDate.Value.Year);
            }

            return Query;
        }

        public void Insert(List<SkillOfClass> lstSkillOfClass)
        {
            foreach (SkillOfClass item in lstSkillOfClass)
            {
                ValidationMetadata.ValidateObject(item);
                if (item.FromDate > item.ToDate)
                {
                    throw new BusinessException("SkillOfClass_Validate_DOW");
                }
                base.Insert(item);
            }
        }

        public void Delete(List<int> lsSkillOfClassID)
        {
            if (lsSkillOfClassID == null || lsSkillOfClassID.Count <= 0) return;
            for (int i = 0; i < lsSkillOfClassID.Count; i++)
            {
                SkillOfClass obj = this.Find(lsSkillOfClassID[i]);
                this.Delete(obj);
            }
            this.Save();
        }
        
    }
}