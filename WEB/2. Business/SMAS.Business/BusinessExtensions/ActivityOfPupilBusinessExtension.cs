﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class ActivityOfPupilBusiness
    {
        public IQueryable<ActivityOfPupil> SearchBySchool(int? SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }

        private IQueryable<ActivityOfPupil> Search(IDictionary<string, object> dic)
        {
            int ActivityOfPupilID = Utils.GetInt(dic, "ActivityOfPupilID");
            string CommentOfTeacher = Utils.GetString(dic, "CommentOfTeacher");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            int ActivityTypeID = Utils.GetInt(dic, "ActivityTypeID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int? ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            short AppliedLevel = Utils.GetShort(dic, "AppliedLevel");
            System.DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            System.DateTime? CreateDate = Utils.GetDateTime(dic, "CreateDate");
            int? ActivityOfClassID = Utils.GetInt(dic, "ActivityOfClassID");
            List<int> lstPuilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstActivity = Utils.GetIntList(dic, "lstActivityID");
            IQueryable<ActivityOfPupil> Query = ActivityOfPupilBusiness.All;
            if (EducationLevelID != 0)
            {
                if (ClassID == 0)
                {
                    if (AppliedLevel != 0)
                    {
                        Query = Query.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel && o.ClassProfile.EducationLevelID == EducationLevelID);
                    }
                    else
                    {
                        Query = Query.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
                    }

                }
                else if (ClassID != 0)
                {
                    Query = Query.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID && o.ClassID == ClassID);
                }
            }
            if (PupilCode.Trim().Length != 0) Query = Query.Where(o => o.PupilProfile.PupilCode.Contains(PupilCode.ToLower()));
            if (FullName.Trim().Length != 0) Query = Query.Where(o => o.PupilProfile.FullName.Contains(FullName.ToLower()));
            if (ActivityTypeID != 0)
            {
                Query = Query.Where(evd => evd.ActivityTypeID == ActivityTypeID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(evd => evd.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(evd => evd.AcademicYearID == AcademicYearID);
            }

            if (lstPuilID.Count > 0)
            {
                Query = Query.Where(p => lstPuilID.Contains(p.PupilID));
            }

            if (PupilID != 0)
            {
                Query = Query.Where(evd => evd.PupilID == PupilID);
            }

            if (ActivityOfPupilID != 0)
            {
                Query = Query.Where(evd => evd.ActivityOfPupilID == ActivityOfPupilID);
            }

            if (ActivityOfClassID != 0)
            {
                Query = Query.Where(evd => evd.ActivityOfClassID == ActivityOfClassID);
            }
            if (lstActivity.Count > 0)
            {
                Query = Query.Where(evd => lstActivity.Contains(evd.ActivityOfClassID.Value));
            }
            if (ModifiedDate != null || CreateDate != null)
            {

                Query = (Query.Where(evd => evd.ModifiedDate.HasValue && evd.ModifiedDate.Value.Year == ModifiedDate.Value.Year &&
                    evd.ModifiedDate.Value.Month == ModifiedDate.Value.Month && evd.ModifiedDate.Value.Day == ModifiedDate.Value.Day))
                    .Union(Query.Where(evd => evd.CreateDate.Value.Year == CreateDate.Value.Year &&
                    evd.CreateDate.Value.Month == CreateDate.Value.Month && evd.CreateDate.Value.Day == CreateDate.Value.Day));
            }

            if (CommentOfTeacher.Trim().Length != 0) Query = Query.Where(o => o.CommentOfTeacher.Contains(CommentOfTeacher.ToLower()));

            return Query;
        }

        private void Validate(List<ActivityOfPupil> entity)
        {
            ValidationMetadata.ValidateObject(entity);
            foreach (var item in entity)
            {
                //SchoolID, AcademicYearID: not compatible(AcademicYear)
                bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                      new Dictionary<string, object>()
                {
                    {"SchoolID",item.SchoolID},
                    {"AcademicYearID",item.AcademicYearID}
                }, null);
                if (!AcademicYearCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //ClassID, AcademicYearID: not compatible(ClassProfile)
                bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                      new Dictionary<string, object>()
                {
                    {"ClassProfileID",item.ClassID},
                    {"AcademicYearID",item.AcademicYearID}
                }, null);
                if (!ClassProfileCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //PupilID, ClassID: not compatible(PupilProfile)
                bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                      new Dictionary<string, object>()
                {
                    {"PupilProfileID",item.PupilID},
                    {"CurrentClassID",item.ClassID}
                }, null);

            }
        }

        public void Insert(List<ActivityOfPupil> lst, IDictionary<string, object> dic)
        {
            //Validate(lst);
            try
            {
                this.SetAutoDetectChangesEnabled(false);
                ActivityOfPupil obj = null;
                List<ActivityOfPupil> listObj = new List<ActivityOfPupil>();
                var listSearch = this.Search(dic).ToList();
                this.DeleteAll(listSearch);

                var lsobj = lst.Where(t => t.CommentOfTeacher != "").ToList();
                for (int i = 0; i < lsobj.Count; i++)
                {
                    obj = lsobj[i];
                    obj.CreateDate = DateTime.Now;
                    obj.IsSMS = false;
                    this.Insert(obj);
                }
                this.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                this.SetAutoDetectChangesEnabled(true);
            }
        }
        public List<ActivityOfPupilToSMS> GetActivityOfPupilToSMS(IDictionary<string, object> dic)
        {
            List<ActivityOfPupilToSMS> lstSMS = new List<ActivityOfPupilToSMS>();
            ActivityOfPupilToSMS sms = new ActivityOfPupilToSMS();
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            DateTime? ActivatedDate = Utils.GetDateTime(dic, "ActivatedDate");
            List<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(SchoolID, dic).ToList();
            string contentActivity = "";

            foreach (PupilProfileBO item in lstPupil)
            {
                sms = new ActivityOfPupilToSMS();
                contentActivity = "";

                List<ActivityOfPupil> lsActivity = this.SearchBySchool(SchoolID, dic).OrderBy(o => o.CreateDate).ToList();
                foreach (ActivityOfPupil aoc in lsActivity)
                {
                    contentActivity = "Kế hoạch hoạt động ngày " + ActivatedDate + " của trẻ: ";
                }
                sms.PupilID = item.PupilProfileID;
                sms.ClassID = item.CurrentClassID;
                sms.ContentActivity = contentActivity;
                lstSMS.Add(sms);
            }
            return lstSMS;
        }
        #region Update IsSMS is TRUE
        /// <summary>
        /// Anhvd
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilIds"></param>
        /// <returns></returns>
        public bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds)
        {
            IQueryable<ActivityOfPupil> lstActivity = this.Search(dic);
            for (int i = 0; i < pupilIds.Count; i++)
            {
                int pupilId = pupilIds[i];
                IQueryable<ActivityOfPupil> lstActPupil = lstActivity.Where(o => o.PupilID == pupilId);
                foreach (var item in lstActPupil)
                {
                    item.IsSMS = true;
                    this.Update(item);
                }
            }
            this.Save();
            return true;
        }
        #endregion
        public void DeleteActivityOfPupil(List<ActivityOfPupil> lst, IDictionary<string, object> dic)
        {
            //Validate(lst);// kiểm tra dữ liệu có tồn tại trong DB không
            try
            {
                SetAutoDetectChangesEnabled(false);
                this.DeleteAll(lst);
                this.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }


        }

        public List<ActivityOfPupilToSMS> GetActivityOfPupilToSMS(int schoolId, int academicYearId, int classId, DateTime date)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolId;
            dic["AcademicYearID"] = academicYearId;
            dic["ClassID"] = classId;

            var iq = (from i in this.SearchBySchool(schoolId, dic)
                      join a in this.ActivityBusiness.All on i.ActivityOfClassID equals a.ActivityID
                      where a.ActivityDate == date
                      orderby a.FromTime
                      select i).ToList();

            return (from i in iq
                    group i by new { i.ClassID, i.PupilID } into g
                    select new ActivityOfPupilToSMS
                    {
                        ClassID = g.Key.ClassID,
                        PupilID = g.Key.PupilID,
                        ContentActivity = string.Join("; ", g.Select(o => o.CommentOfTeacher).ToList())
                    }).ToList();
        }
    }
}