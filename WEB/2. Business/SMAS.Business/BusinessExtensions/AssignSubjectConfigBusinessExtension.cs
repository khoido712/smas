﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.IBusiness;

namespace SMAS.Business.Business
{
    public partial class AssignSubjectConfigBusiness
    {
        public List<AssignSubjectConfig> GetList(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            return this.SearchBySchool(SchoolID, SearchInfo).ToList();
        }

        public IQueryable<AssignSubjectConfig> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        public IQueryable<AssignSubjectConfig> Search(IDictionary<string, object> SearchInfo)
        {
            long assignSubjectConfigID = Utils.GetLong(SearchInfo, "AssignSubjectConfigID");
            int schoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int academicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string assignSubjectName = Utils.GetString(SearchInfo, "AssignSubjectName");
            int educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<int> listSubjectID = Utils.GetIntList(SearchInfo, "Subjects");

            var query = this.AssignSubjectConfigRepository.All;

            if (assignSubjectConfigID > 0) { query = query.Where(x => x.AssignSubjectConfigID == assignSubjectConfigID); }
            if (schoolID > 0) { query = query.Where(x => x.SchoolID == schoolID); }
            if (academicYearID > 0) { query = query.Where(x => x.AcademicYearID == academicYearID); }
            if (subjectID > 0) { query = query.Where(x => x.SubjectID == subjectID); }
            if (!string.IsNullOrEmpty(assignSubjectName)) { query = query.Where(x => x.AssignSubjectName == assignSubjectName); }
            if (educationLevelID > 0) 
            {
                string stringEducationLevelID = educationLevelID.ToString();
                query = query.Where(x => x.EducationApplied.Contains(stringEducationLevelID)); 
            }
            if (listSubjectID.Count > 0)
            {
                query = query.Where(x => listSubjectID.Contains(x.SubjectID));
            }

            return query;
        }

        public void InsertAssignSubjectConfig(List<AssignSubjectConfig> lstInput)
        {
            AssignSubjectConfig newObj = null;
            foreach (var item in lstInput)
            {
            newObj = new AssignSubjectConfig()
            {
                    SchoolID = item.SchoolID,
                    AcademicYearID = item.AcademicYearID,
                    SubjectID = item.SubjectID,
                    AssignSubjectName = item.AssignSubjectName,
                    EducationApplied = item.EducationApplied,
                ModifiedDate = DateTime.Now,
                CreateDate = DateTime.Now
            };
            this.Insert(newObj);
            }                
            this.Save();
        }

        public void UpdateAssignSubjectConfig(AssignSubjectConfigBO objUpdate, IDictionary<string, object> SearchInfo)
        {
            int schoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int academicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");

            AssignSubjectConfig objDb = AssignSubjectConfigBusiness.All
                .Where(x => x.AssignSubjectConfigID == objUpdate.AssignSubjectConfigID
                && x.SchoolID == schoolID && x.AcademicYearID == academicYearID).FirstOrDefault();        
            if (objDb != null)
            {
                objDb.EducationApplied = objUpdate.EducationApplied;
                objDb.AssignSubjectName = objUpdate.AssignSubjectName;
                objDb.SubjectID = objUpdate.SubjectID;
                objDb.ModifiedDate = DateTime.Now;
                this.Update(objDb);
                this.Save();
            }
        }
    }
}
