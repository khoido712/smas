﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Web.Security;
using SMAS.VTUtils.Excel.ContextExt;
using System.Transactions;
using SMAS.VTUtils.Log;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class SchoolProfileBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int SCHOOLCODE_MAX_LENGTH = 25; //Độ dài mã trường 
        private const int SCHOOLNAME_MAX_LENGTH = 200; //Độ dài tên trường 
        private const int ADDRESS_MAX_LENGTH = 100; // Độ dài địa chỉ trường 
        private const int FAX_MAX_LENGTH = 15; // Độ dài fax
        private const int SHORTNAME_MAX_LENGTH = 20; //Độ dài tên ngắn
        private const int TELEPHONE_MAX_LENGTH = 20; //Độ dài điện thoại trường
        private const int WEBSITE_MAX_LENGTH = 100; //Độ dài website
        private const int REPORTTILE_MAX_LENGTH = 200; //Độ dài tiêu đề báo cáo
        private const int HEADMASTERNAME_MAX_LENGTH = 60; // Độ dài tên hiệu trưởng
        private const int HEADMASTERPHONE_MAX_LENGTH = 15; //Độ dài điện thoại hiệu trưởng
        private const int EMAIL_MAX_LENGTH = 50; //Độ dài trường email
        private const int SYNC_CODE_MAX_LENGTH_ = 20; //Độ dài trường email
        private const string MessageErrorSyncCode = "Mã chuẩn đã tồn tại ({0} - {1} - {2} - {3}). Vui lòng kiểm tra lại.";
        #endregion

        #region Search

        /// <summary>
        /// Tìm kiếm trường chính
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolName">Tên trường</param>
        /// <param name="SchoolCode">Mã trường</param>
        /// <param name="ProvinceID">ID tỉnh/thành phố</param>
        /// <param name="DistrictID">ID quận huyện</param>
        /// <param name="EducationGrade">Cấp học</param>
        /// <param name="AreaID">ID khu vực</param>
        /// <param name="CommuneID">ID xã/phường</param>
        /// <param name="SupervisingDeptID">ID Đơn vị quản lý</param>
        /// <param name="TrainingTypeID">ID loại hình đào tạo</param>
        /// <param name="SchoolTypeID">ID loại trường</param>
        /// <param name="Telephone">Số điện thoại</param>
        /// <param name="Email">Email</param>
        /// <param name="Address">Địa chỉ</param>
        /// <param name="Website">website</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <param name="AdminID">ID Admin</param>
        /// <returns></returns>
        public IQueryable<SchoolProfile> Search(IDictionary<string, object> dic)
        {
            string HeadMasterName = Utils.GetString(dic, "HeadMasterName");
            string SchoolName = Utils.GetString(dic, "SchoolName");
            string SchoolCode = Utils.GetString(dic, "SchoolCode");
            int ProvinceID = Utils.GetShort(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int EducationGrade = Utils.GetInt(dic, "EducationGrade");
            int AreaID = Utils.GetShort(dic, "AreaID");
            int CommuneID = Utils.GetInt(dic, "CommuneID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int TrainingTypeID = Utils.GetShort(dic, "TrainingTypeID");
            int SchoolTypeID = Utils.GetShort(dic, "SchoolTypeID");
            string Telephone = Utils.GetString(dic, "Telephone");
            string Email = Utils.GetString(dic, "Email");
            string Address = Utils.GetString(dic, "Address");
            string Website = Utils.GetString(dic, "Website");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            int AdminID = Utils.GetInt(dic, "AdminID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int grade = AppliedLevel > 0 ? Utils.GradeToBinary(AppliedLevel) : (int)0;

            string Administrator = Utils.GetString(dic, "Administrator");

            IQueryable<SchoolProfile> lsSchool = SchoolProfileRepository.All;

            if (IsActive.HasValue)
                lsSchool = lsSchool.Where(sch => sch.IsActive == IsActive);
            if (SchoolName.Trim().Length != 0)
                lsSchool = lsSchool.Where(sch => sch.SchoolName.ToLower().Contains(SchoolName.ToLower()));
            if (SchoolCode.Trim().Length != 0)
                lsSchool = lsSchool.Where(sch => sch.SchoolCode.ToLower().Contains(SchoolCode.ToLower()));
            if (Telephone.Trim().Length != 0)
                lsSchool = lsSchool.Where(sch => sch.Telephone.ToLower().Contains(Telephone.ToLower()));
            if (Email.Trim().Length != 0)
                lsSchool = lsSchool.Where(sch => sch.Email.ToLower().Contains(Email.ToLower()));
            if (Address.Trim().Length != 0)
                lsSchool = lsSchool.Where(sch => sch.Address.ToLower().Contains(Address.ToLower()));
            if (Website.Trim().Length != 0)
                lsSchool = lsSchool.Where(sch => sch.Website.ToLower().Contains(Website.ToLower()));
            if (ProvinceID != 0)
                lsSchool = lsSchool.Where(sch => sch.ProvinceID == ProvinceID);
            if (DistrictID != 0)
                lsSchool = lsSchool.Where(sch => sch.DistrictID == DistrictID);
            if (EducationGrade != 0)
                lsSchool = lsSchool.Where(sch => sch.EducationGrade == EducationGrade);
            if (AreaID != 0)
                lsSchool = lsSchool.Where(sch => sch.AreaID == AreaID);
            if (CommuneID != 0)
                lsSchool = lsSchool.Where(sch => sch.CommuneID == CommuneID);
            if (SupervisingDeptID != 0)
            {
                string sSupervisingDeptID = SupervisingDeptID.ToString();
                lsSchool = lsSchool.Where(sch => sch.SupervisingDeptID == SupervisingDeptID || sch.SupervisingDept.TraversalPath.Contains(sSupervisingDeptID));
            }
            if (TrainingTypeID != 0)
                lsSchool = lsSchool.Where(sch => sch.TrainingTypeID == TrainingTypeID);
            if (SchoolTypeID != 0)
                lsSchool = lsSchool.Where(sch => sch.SchoolTypeID == SchoolTypeID);
            if (AdminID != 0)
                lsSchool = lsSchool.Where(sch => sch.AdminID == AdminID);
            if (HeadMasterName.Trim().Length != 0)
            {
                lsSchool = lsSchool.Where(sch => sch.HeadMasterName.ToLower().Contains(HeadMasterName.ToLower()));
            }
            if (Administrator.Trim().Length != 0)
            {
                lsSchool = lsSchool.Where(sch => sch.UserAccount.aspnet_Users.UserName.ToLower().Contains(Administrator.ToLower()));
            }
            if (grade > 0)
                lsSchool = lsSchool.Where(sch => (sch.EducationGrade & grade) != 0);
            return lsSchool;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm thông tin trường chính
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolProfile">Đối tượng SchoolProfile</param>
        /// <returns>Đối tượng SchoolProfile</returns>
        public void Insert(SchoolProfile SchoolProfile, List<int> lstRole, string UserName, bool IsApproved, List<int> lsPropertyOfSchool, bool IsAutoCode, int account, int roleID, string password = "")
        {
            Utils.ValidateRequire(SchoolProfile.EducationGrade, "SchoolProfile_Label_EducationGrade");
            //Khu vực không tồn tại hoặc đã bị xóa – kiểm tra AreaID trong bảng Area với isActive  = 1
            AreaBusiness.CheckAvailable(SchoolProfile.AreaID, "School_Label_AreaID", true);
            //Thông tin tỉnh/thành không tồn tại hoặc đã bị xóa – Kiểm tra ProvinceID với isActive = 1
            ProvinceBusiness.CheckAvailable(SchoolProfile.ProvinceID, "SchoolProfile_Label_ProvinceID", true);
            //Thông tin quận/huyện không tồn tại hoặc đã bị xóa– Kiểm tra DistrictID với isActive = 1
            DistrictBusiness.CheckAvailable(SchoolProfile.DistrictID, "SchoolProfile_Label_DistrictID", true);
            // Mã trường không được để trống – Kiểm tra SchoolCode
            if (!IsAutoCode)
            {
                Utils.ValidateRequire(SchoolProfile.SchoolCode, "SchoolProfile_Label_SchoolCode");
            }
            //Mã chuẩn không được để trống – Kiểm tra SyncCode
            Utils.ValidateRequire(SchoolProfile.SyncCode, "Label_Standard_Code");

            //Tên trường không được để trống – Kiểm tra SchoolName
            Utils.ValidateRequire(SchoolProfile.SchoolName, "SchoolProfile_Label_SchoolName");
            //Tên viết tắt không được để trống – Kiểm tra ShortName
            Utils.ValidateRequire(SchoolProfile.ShortName, "SchoolProfile_Label_SchortName");
            //Đơn vị quản lý không tồn tại hoặc đã bị xóa– Kiểm tra SupervisingDeptID trong bảng SupervisingDept với isActive = 1
            SupervisingDeptBusiness.CheckAvailable(SchoolProfile.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID", true);
            //Số điện thoại nhà trường không được để trống – Kiểm tra Telephone
            Utils.ValidateRequire(SchoolProfile.Telephone, "SchoolProfile_Label_Telephone");
            //Tên hiệu trưởng không được để trống – kiểm tra HeadMasterName
            Utils.ValidateRequire(SchoolProfile.HeadMasterName, "SchoolProfile_Label_HeadMasterName");
            //Địa chỉ không được để trống – Kiểm tra Address
            Utils.ValidateRequire(SchoolProfile.Address, "SchoolProfile_Label_Address");
            //Loại hình đào tạo không tồn tại hoặc đã bị xóa – Kiểm tra TrainingTypeID trong bảng TrainingType với isActive = 1
            TrainingTypeBusiness.CheckAvailable(SchoolProfile.TrainingTypeID, "SchoolProfile_Label_TrainingTypeID", true);
            //Ngày thành lập không hợp lệ - Kiểm tra EstablishedDate
            Utils.ValidatePastDate(SchoolProfile.EstablishedDate, "SchoolProfile_Label_EstablishedDate");
            //Mã trường đã tồn tại – Kiểm tra SchoolCode
            if (!IsAutoCode)
            {
                CheckDuplicate(SchoolProfile.SchoolCode, GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile", "SchoolCode", true, SchoolProfile.SchoolProfileID, "SchoolProfile_Label_SchoolCode");
            }
            //Mã chuẩn không được nhập quá 20 ký tự. Kiểm tra SyncCode
            Utils.ValidateMaxLength(SchoolProfile.SyncCode, SYNC_CODE_MAX_LENGTH_, "Label_Standard_Code");

            //Kiểm tra mã chuẩn có tồn tại
            IQueryable<SchoolProfile> iquerySP = SchoolProfileBusiness.All.Where(x => x.IsActive && x.SyncCode == SchoolProfile.SyncCode);
            if (iquerySP.Count() > 0)
            {
                SchoolProfile = iquerySP.FirstOrDefault();
                string message = MessageErrorSyncCode;
                message = string.Format(message, SchoolProfile.SyncCode, SchoolProfile.SchoolName, SchoolProfile.District.DistrictName, SchoolProfile.Province.ProvinceName);
                throw new BusinessException(message);
            }

            //Mã trường không được nhập quá 50 ký tự. Kiểm tra SchoolCode
            Utils.ValidateMaxLength(SchoolProfile.SchoolCode, SCHOOLCODE_MAX_LENGTH, "SchoolProfile_Label_SchoolCode");
            //Tên trường không được nhập quá 200 ký tự. Kiểm tra SchoolName
            Utils.ValidateMaxLength(SchoolProfile.SchoolName, SCHOOLNAME_MAX_LENGTH, "SchoolProfile_Label_SchoolName");
            //Tên viết tắt không được nhập quá 20 ký tự - Kiểm tra ShortName
            Utils.ValidateMaxLength(SchoolProfile.ShortName, SHORTNAME_MAX_LENGTH, "SchoolProfile_Label_ShortName");
            //Số điện thoại không được nhập quá 15 ký tự - kiểm tra Telephone
            Utils.ValidateMaxLength(SchoolProfile.Telephone, TELEPHONE_MAX_LENGTH, "SchoolProfile_Label_Telephone");
            //Số Fax không được nhập quá 15 ký tự - Kiểm tra Fax
            Utils.ValidateMaxLength(SchoolProfile.Fax, FAX_MAX_LENGTH, "SchoolProfile_Label_Fax");
            //Email không được nhập quá 50 ký tự. – Kiểm tra Email
            Utils.ValidateMaxLength(SchoolProfile.Email, EMAIL_MAX_LENGTH, "SchoolProfile_Label_Email");
            //Địa chỉ không được nhập qua 100 ký tự - Kiểm tra Address
            Utils.ValidateMaxLength(SchoolProfile.Address, ADDRESS_MAX_LENGTH, "SchoolProfile_Label_Address");
            //Website không được nhập quá 100 ký tự - Kiểm tra Website
            Utils.ValidateMaxLength(SchoolProfile.Website, WEBSITE_MAX_LENGTH, "SchoolProfile_Label_Website");
            //Tiêu đề báo cáo gửi cấp trên không được nhập quá 200 ký tự - Kiểm tra ReportTitle
            Utils.ValidateMaxLength(SchoolProfile.ReportTile, REPORTTILE_MAX_LENGTH, "SchoolProfile_Label_ReportTile");
            //Tên hiệu trưởng không được nhập quá 60 ký tự - Kiểm tra HeadMasterName
            Utils.ValidateMaxLength(SchoolProfile.HeadMasterName, HEADMASTERNAME_MAX_LENGTH, "SchoolProfile_Label_HeadMasterName");
            //Số điện thoại hiệu trưởng không được nhập quá 15 ký tự - Kiểm tra HeadMasterPhone
            Utils.ValidateMaxLength(SchoolProfile.HeadMasterPhone, HEADMASTERPHONE_MAX_LENGTH, "SchoolProfile_Label_HeadMasterPhone");
            if (SchoolProfile.Email != null)
            {
                //Email không hợp lệ. Email hợp lệ bắt buộc phải chứa ký tự @.
                Utils.ValidateEmail(SchoolProfile.Email, "Email");
                //Email nhập trùng với Email trường khác.
                CheckDuplicate(SchoolProfile.Email, GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile", "Email", true, SchoolProfile.SchoolProfileID, "SchoolProfile_Label_Email");
            }
            // AnhVD 20140807  - Validate thông tin về 
            //UserName: Require, MaxLength(100), Duplicate
            Utils.ValidateRequire(UserName, "School_Label_UserName");
            Utils.ValidateMaxLength(UserName, 100, "School_Label_UserName");
            // Kiểm tra UserName chưa tồn tại trong hệ thống
            ValidateUserNameInMenbership(UserName, "SchoolProfile_Label_Admin");
            // Kiểm tra có quyền tạo trường
            //UserID: Require
            if (account == 0)
            {
                throw new BusinessException("Validate_UserAccountID");
            }
            Utils.ValidateRequire(account, "School_Label_CreateAdminID");
            //UserName: not containt  “~!@#$%^&*;’|`”
            if (UserName.Contains("~") || UserName.Contains("!") || UserName.Contains("@") || UserName.Contains("#")
                || UserName.Contains("$") || UserName.Contains("%") || UserName.Contains("^") || UserName.Contains("&")
                || UserName.Contains("*") || UserName.Contains(";") || UserName.Contains("’") || UserName.Contains("|") || UserName.Contains("`"))
            {
                throw new BusinessException("Validate_UserName_Contains");
            }

            // // Kiem tra user create da ton tai trong he thong
            UserAccount UserAcount = UserAccountBusiness.Find(account);
            this.ValidateUserNameInMenbership(UserName, "School_Label_UserName", UserAcount.GUID);

            // Tạo User_account cho trường
            int UserAccountID = UserAccountBusiness.CreateAdminSchool(UserName, account, lstRole, roleID, password).Value;
            if (IsApproved)
            {
                UserAccountBusiness.ActiveAccount(UserAccountID);
            }
            else
            {
                UserAccountBusiness.DeActiveAccount(UserAccountID);
            }
            SchoolProfile.AdminID = UserAccountID;

            //TODO : hath8
            List<PropertyOfSchool> lstPropertyOfSchool = new List<PropertyOfSchool>();
            for (int i = 0; i < lsPropertyOfSchool.Count; i++)
            {
                PropertyOfSchool Property = new PropertyOfSchool();
                Property.IsActive = true;
                Property.SchoolPropertyCatID = (int)lsPropertyOfSchool[i];

                Property.SchoolID = SchoolProfile.SchoolProfileID;
                lstPropertyOfSchool.Add(Property);
            }
            SchoolProfile.PropertyOfSchools = lstPropertyOfSchool;
            base.Insert(SchoolProfile);
        }
        #endregion

        #region Update

        /// <summary>
        /// Sửa thông tin trường chính
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolProfile">Đối tượng SchoolProfile</param>
        /// <returns>Đối tượng SchoolProfile</returns>
        public void Update(SchoolProfile SchoolProfile, List<int> SchoolPropertyCatID, bool IsForgetPassword, bool IsApproved, bool IsActiveSchool, bool IsSystemAdmin, int roleID, string password = "")
        {
            TransactionScope scope = null;
            #region Validate
            // Bắt buộc chọn cấp 
            Utils.ValidateRequire(SchoolProfile.EducationGrade, "SchoolProfile_Label_EducationGrade");
            //Trường học không tồn tại hoặc đã bị xóa  – kiểm tra SchoolProfileID với isActive  = 1
            CheckAvailable(SchoolProfile.SchoolProfileID, "SchoolProfile_Label_SchoolProfile", true);
            //Khu vực không tồn tại hoặc đã bị xóa – kiểm tra AreaID trong bảng Area với isActive  = 1
            AreaBusiness.CheckAvailable(SchoolProfile.AreaID, "School_Label_Area", true);
            //Thông tin tỉnh/thành không tồn tại hoặc đã bị xóa – Kiểm tra ProvinceID với isActive = 1
            ProvinceBusiness.CheckAvailable(SchoolProfile.ProvinceID, "SchoolProfile_Label_Province", true);
            //Thông tin quận/huyện không tồn tại hoặc đã bị xóa– Kiểm tra DistrictID với isActive = 1
            DistrictBusiness.CheckAvailable(SchoolProfile.DistrictID, "SchoolProfile_Label_District", true);

            //Mã chuẩn không được để trống – Kiểm tra SyncCode
            Utils.ValidateRequire(SchoolProfile.SyncCode, "Label_Standard_Code");
            // Mã trường không được để trống – Kiểm tra SchoolCode
            Utils.ValidateRequire(SchoolProfile.SchoolCode, "SchoolProfile_Label_SchoolCode");
            //Tên trường không được để trống – Kiểm tra SchoolName
            Utils.ValidateRequire(SchoolProfile.SchoolName, "SchoolProfile_Label_SchoolName");
            //Tên viết tắt không được để trống – Kiểm tra ShortName
            Utils.ValidateRequire(SchoolProfile.ShortName, "SchoolProfile_Label_SchortName");
            //Đơn vị quản lý không tồn tại hoặc đã bị xóa– Kiểm tra SupervisingDeptID trong bảng SupervisingDept với isActive = 1
            SupervisingDeptBusiness.CheckAvailable(SchoolProfile.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID", true);
            //Số điện thoại nhà trường không được để trống – Kiểm tra Telephone
            Utils.ValidateRequire(SchoolProfile.Telephone, "SchoolProfile_Label_Telephone");
            //Tên hiệu trưởng không được để trống – kiểm tra HeadMasterName
            Utils.ValidateRequire(SchoolProfile.HeadMasterName, "SchoolProfile_Label_HeadMasterName");
            //Địa chỉ không được để trống – Kiểm tra Address
            Utils.ValidateRequire(SchoolProfile.Address, "SchoolProfile_Label_Address");
            // Loại trường có tồn tại
            TrainingTypeBusiness.CheckAvailable(SchoolProfile.TrainingTypeID, "SchoolProfile_Label_TrainingTypeID", true);
            //Ngày thành lập không hợp lệ - Kiểm tra EstablishedDate
            Utils.ValidatePastDate(SchoolProfile.EstablishedDate, "SchoolProfile_Label_EstablishedDate");
            //Mã trường đã tồn tại – Kiểm tra SchoolCode
            CheckDuplicate(SchoolProfile.SchoolCode, GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile", "SchoolCode", true, SchoolProfile.SchoolProfileID, "SchoolProfile_Label_SchoolCode");
            //Mã chuẩn không được nhập quá 20 ký tự. Kiểm tra SyncCode
            Utils.ValidateMaxLength(SchoolProfile.SyncCode, SYNC_CODE_MAX_LENGTH_, "Label_Standard_Code");

            //Kiểm tra mã chuẩn có tồn tại
            IQueryable<SchoolProfile> iquerySP = SchoolProfileBusiness.All.Where(x => x.IsActive && x.SyncCode == SchoolProfile.SyncCode && x.SchoolProfileID != SchoolProfile.SchoolProfileID);
            if (iquerySP.Count() > 0)
            {
                SchoolProfile = iquerySP.FirstOrDefault();
                string message = MessageErrorSyncCode;
                message = string.Format(message, SchoolProfile.SyncCode, SchoolProfile.SchoolName, SchoolProfile.District.DistrictName, SchoolProfile.Province.ProvinceName);
                throw new BusinessException(message);
            }

            //Mã trường không được nhập quá 50 ký tự. Kiểm tra SchoolCode
            Utils.ValidateMaxLength(SchoolProfile.SchoolCode, SCHOOLCODE_MAX_LENGTH, "SchoolProfile_Label_SchoolCode");
            //Tên trường không được nhập quá 200 ký tự. Kiểm tra SchoolName
            Utils.ValidateMaxLength(SchoolProfile.SchoolName, SCHOOLNAME_MAX_LENGTH, "SchoolProfile_Label_SchoolName");
            //Tên viết tắt không được nhập quá 20 ký tự - Kiểm tra ShortName
            Utils.ValidateMaxLength(SchoolProfile.ShortName, SHORTNAME_MAX_LENGTH, "SchoolProfile_Label_ShortName");
            //Số điện thoại không được nhập quá 15 ký tự - kiểm tra Telephone
            Utils.ValidateMaxLength(SchoolProfile.Telephone, TELEPHONE_MAX_LENGTH, "SchoolProfile_Label_Telephone");
            //Số Fax không được nhập quá 15 ký tự - Kiểm tra Fax
            Utils.ValidateMaxLength(SchoolProfile.Fax, FAX_MAX_LENGTH, "SchoolProfile_Label_Fax");
            //Email không được nhập quá 50 ký tự. – Kiểm tra Email
            Utils.ValidateMaxLength(SchoolProfile.Email, EMAIL_MAX_LENGTH, "SchoolProfile_Label_Email");
            //Địa chỉ không được nhập qua 100 ký tự - Kiểm tra Address
            Utils.ValidateMaxLength(SchoolProfile.Address, ADDRESS_MAX_LENGTH, "SchoolProfile_Label_Address");
            //Website không được nhập quá 100 ký tự - Kiểm tra Website
            Utils.ValidateMaxLength(SchoolProfile.Website, WEBSITE_MAX_LENGTH, "SchoolProfile_Label_Website");
            //Tiêu đề báo cáo gửi cấp trên không được nhập quá 200 ký tự - Kiểm tra ReportTitle
            Utils.ValidateMaxLength(SchoolProfile.ReportTile, REPORTTILE_MAX_LENGTH, "SchoolProfile_Label_ReportTile");
            //Tên hiệu trưởng không được nhập quá 60 ký tự - Kiểm tra HeadMasterName
            Utils.ValidateMaxLength(SchoolProfile.HeadMasterName, HEADMASTERNAME_MAX_LENGTH, "SchoolProfile_Label_HeadMasterName");
            //Số điện thoại hiệu trưởng không được nhập quá 15 ký tự - Kiểm tra HeadMasterPhone
            Utils.ValidateMaxLength(SchoolProfile.HeadMasterPhone, HEADMASTERPHONE_MAX_LENGTH, "SchoolProfile_Label_HeadMasterPhone");
            if (SchoolProfile.Email != null)
            {
                //Email không hợp lệ. Email hợp lệ bắt buộc phải chứa ký tự @.
                Utils.ValidateEmail(SchoolProfile.Email, "Email");
                //Email nhập trùng với Email trường khác.
                IQueryable<SchoolProfile> lsmail = SchoolProfileBusiness.All.Where(o => o.SchoolProfileID == SchoolProfile.SchoolProfileID && o.Email == SchoolProfile.Email);
                if (lsmail.Count() == 0)
                {
                    if (SchoolProfileBusiness.All.Where(o => o.Email == SchoolProfile.Email).Count() > 0)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("SchoolProfile_Label_Email");
                        throw new BusinessException("Common_Validate_Duplicate", Params);
                    }
                }
            }

            if (SchoolProfile.Website != null)
            {
                //validate website
                Utils.ValidateWebsite(SchoolProfile.Website, "Website");
                //Website nhập trùng với Email trường khác.
                IQueryable<SchoolProfile> lswebsite = SchoolProfileBusiness.All.Where(o => o.SchoolProfileID == SchoolProfile.SchoolProfileID && o.Website == SchoolProfile.Website);
                if (lswebsite.Count() == 0)
                {
                    if (SchoolProfileBusiness.All.Where(o => o.Website == SchoolProfile.Website).Count() > 0)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("SchoolProfile_Label_Website");
                        throw new BusinessException("Common_Validate_Duplicate", Params);
                    }
                }
            }
            #endregion
            try
            {
                SetAutoDetectChangesEnabled(false);
                using (scope = new TransactionScope())
                {
                    #region Kích hoạt tài khoản trường
                    if (IsApproved) UserAccountBusiness.ActiveAccount(SchoolProfile.AdminID.Value);
                    else UserAccountBusiness.DeActiveAccount(SchoolProfile.AdminID.Value);
                    #endregion

                    #region Kích hoạt tài khoản nhân viên trường - Chỉ áp dụng với admin hệ thống
                    if (IsSystemAdmin)
                    {
                        if (IsActiveSchool)
                        {
                            var IqUsers = from em in this.EmployeeBusiness.All
                                          join ua in this.UserAccountBusiness.All on em.EmployeeID equals ua.EmployeeID
                                          join ms in this.aspnet_MembershipBusiness.All on ua.GUID equals ms.UserId
                                          where em.SchoolID == SchoolProfile.SchoolProfileID
                                          && em.IsActive == true && ms.IsApproved != 1
                                          select ms;
                            if (IqUsers.Count() > 0)
                            {
                                foreach (var item in IqUsers)
                                {
                                    item.IsApproved = 1;
                                    this.aspnet_MembershipBusiness.Update(item);
                                }
                                this.aspnet_MembershipBusiness.Save();
                            }
                        }
                        else
                        {
                            var IqUsers = from em in this.EmployeeBusiness.All
                                          join ua in this.UserAccountBusiness.All on em.EmployeeID equals ua.EmployeeID
                                          join ms in this.aspnet_MembershipBusiness.All on ua.GUID equals ms.UserId
                                          where em.SchoolID == SchoolProfile.SchoolProfileID && ms.IsApproved == 1
                                          select ms;
                            if (IqUsers.Count() > 0)
                            {
                                foreach (var item in IqUsers)
                                {
                                    item.IsApproved = 0;
                                    this.aspnet_MembershipBusiness.Update(item);
                                }
                                this.aspnet_MembershipBusiness.Save();
                            }
                        }
                    }

                    #endregion

                    #region Cập nhật Tính chất trường
                    // Danh sách thuộc tính hiện tại của trường
                    List<PropertyOfSchool> lstCurrentPropertyOfSchool = PropertyOfSchoolBusiness.All.Where(sch => sch.IsActive == true && sch.SchoolID == SchoolProfile.SchoolProfileID).ToList();
                    List<int> lstCurrentPropertyCatID = lstCurrentPropertyOfSchool.Select(o => o.SchoolPropertyCatID).ToList();
                    // Danh sách các thuộc tính chưa có
                    List<int> lstPropertyCatID = SchoolPropertyCatID.Except(lstCurrentPropertyCatID).ToList();
                    // Danh sách thuộc tính bị bỏ đi
                    List<int> lstPropertyCatIDNotChecked = lstCurrentPropertyCatID.Except(SchoolPropertyCatID).ToList();
                    // Thực hiện xóa thuộc tính bị bỏ chọn
                    PropertyOfSchoolBusiness.DeleteAll(lstCurrentPropertyOfSchool.Where(o => lstPropertyCatIDNotChecked.Contains(o.SchoolPropertyCatID)).ToList());

                    // Thực hiện thêm mới các thuộc tính bị bỏ chọn
                    if (lstPropertyCatID.Count > 0)
                    {
                        PropertyOfSchool Property;
                        for (int i = lstPropertyCatID.Count - 1; i >= 0; i--)
                        {
                            Property = new PropertyOfSchool();
                            Property.IsActive = true;
                            Property.SchoolPropertyCatID = lstPropertyCatID[i];
                            Property.SchoolID = SchoolProfile.SchoolProfileID;
                            PropertyOfSchoolBusiness.Insert(Property);
                        }
                        PropertyOfSchoolBusiness.Save();
                    }
                    #endregion

                    #region Phân quyền
                    List<int> lstRole = new List<int>();
                    if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_CHILDREN) != 0)
                        lstRole.Add(GlobalConstants.ROLE_ADMIN_MAUGIAO);
                    if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_CHILDCARE) != 0)
                        lstRole.Add(GlobalConstants.ROLE_ADMIN_NHATRE);
                    if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_TERTIARY) != 0)
                        lstRole.Add(GlobalConstants.ROLE_ADMIN_TRUONGC3);
                    if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_SECONDARY) != 0)
                        lstRole.Add(GlobalConstants.ROLE_ADMIN_TRUONGC2);
                    if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_PRIMARY) != 0)
                        lstRole.Add(GlobalConstants.ROLE_ADMIN_TRUONGC1);

                    // Lấy danh sách quyền hiện tại
                    int AdminID = SchoolProfile.AdminID.Value;
                    IQueryable<UserRole> lsUserRole = UserRoleBusiness.All.Where(o => o.UserID == AdminID);
                    List<int> lstCurrentRoleID = lsUserRole.Select(o => o.RoleID).ToList();
                    if (lstRole.Count > 0)
                    {
                        // Xóa quyền ko được gán
                        List<int> lstRoleIDNotCheck = lstCurrentRoleID.Except(lstRole).ToList();
                        List<UserRole> lstRoleDelete = lsUserRole.Where(o => lstRoleIDNotCheck.Contains(o.RoleID)).ToList();
                        UserRoleBusiness.DeleteAll(lstRoleDelete);

                        //Kiểm tra quyền: danh sách tất cả các Role định gán có là Role con của Role người thực hiện không?
                        string RolePath = RoleBusiness.Find(roleID).RolePath;
                        bool ExistsRole = RoleBusiness.All.Where(o => o.RolePath.StartsWith(RolePath)).Select(o => o.RoleID).Intersect(lstRole).Count() > 0 ? true : false;
                        if (ExistsRole)
                        {
                            List<int> lstRoleIDNotAssign = lstRole.Except(lstCurrentRoleID).ToList();
                            foreach (int RoleID in lstRoleIDNotAssign)
                            {
                                UserRole ur = new UserRole();
                                ur.RoleID = RoleID;
                                ur.UserID = AdminID;
                                UserRoleBusiness.Insert(ur);
                            }
                        }
                        UserRoleBusiness.Save();
                    }
                    #endregion

                    #region Cấp lại mật khẩu cho account quản trị trường
                    if (IsForgetPassword)
                    {
                        UserAccountBusiness.ResetPassword(SchoolProfile.AdminID.Value, password);
                    }
                    #endregion

                    #region QuangNN2 - 13/05/2015 - Cập nhật thuộc tính Synchronize = -2

                    //UserAccountBusiness.UpdateSynchronize(SchoolProfile.AdminID.Value, GlobalConstants.STATUS_UPDATE_INFO);

                    #endregion

                    #region Viethd4 - Huy bo tinh chat lop hoc VNEN
                    if (SchoolProfile.IsNewSchoolModel == false)
                    {
                        //Xoa tinh chat lop VNEN cua cac lop
                        List<ClassProfile> lstClass = ClassProfileBusiness.All.Where(o => o.SchoolID == SchoolProfile.SchoolProfileID
                                                                                    && o.IsVnenClass == true
                                                                                    && o.IsActive == true).ToList();
                        for (int i = 0; i < lstClass.Count; i++)
                        {
                            ClassProfile cp = lstClass[i];
                            cp.IsVnenClass = false;
                            ClassProfileBusiness.Update(cp);
                        }

                        //Xoa tinh chat VNEN cho cac mon
                        List<ClassSubject> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                              join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                              where cs.IsSubjectVNEN == true
                                                              && cp.SchoolID == SchoolProfile.SchoolProfileID
                                                              && cp.IsVnenClass == true
                                                              && cp.IsActive.Value
                                                              select cs).ToList();
                        for (int i = 0; i < lstClassSubject.Count; i++)
                        {
                            ClassSubject cs = lstClassSubject[i];
                            cs.IsSubjectVNEN = false;
                            ClassSubjectBusiness.Update(cs);
                        }

                    }
                    #endregion

                    base.Update(SchoolProfile);
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "Update", "null", ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa thông tin trường chính
        /// chiendd1: 13/09/2017, tra ve danh sach phuc vu viec dong bo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolProfileID">ID trường chính</param>
        public List<RESTORE_DATA_DETAIL> Delete(int SchoolProfileID, int AdminID, RESTORE_DATA objRes)
        {
            List<RESTORE_DATA_DETAIL> lstResDetail = new List<RESTORE_DATA_DETAIL>();
            RESTORE_DATA_DETAIL objResDetail;
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolProfileID);
            //List<SchoolProfile> lsSchoolProfile = SchoolProfileRepository.All.Where(sch=>sch.IsActive ==true&&sch.SchoolProfileID==SchoolProfileID).ToList();
            //SchoolProfile SchoolProfile = lsSchoolProfile.FirstOrDefault();
            if (sp != null)
            {
                //Trường học không tồn tại trong cơ sở dữ liệu. Hoặc đã bị xóa trước đó.
                CheckAvailable(SchoolProfileID, "SchoolProfile_Label_SchoolProfileID", true);

                //Tài khoản không đủ quyền để thực hiện thao tác này.
                //Thực hiện chuyển IsActive=false, ModifiedDate=Sysdate với bản ghi tương ứng trong bảng SchoolProfile
                base.Delete(SchoolProfileID, true);

                RestoreSchoolProfileBO objSqlUndo;
                objSqlUndo = new RestoreSchoolProfileBO
                {
                    ReplaceSchoolCode = "",
                    SchoolCode = sp.SchoolCode,
                    SchoolProfileId = sp.SchoolProfileID,
                    SqlUndo = string.Format(RestoreDataConstant.SQL_UNDO_SCHOOL, sp.SchoolCode, sp.SchoolProfileID),
                };

                //Du lieu truong
                objResDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = 0,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(SchoolProfileID),
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = SchoolProfileID,
                    SQL_UNDO = JsonConvert.SerializeObject(objSqlUndo),
                    TABLE_NAME = RestoreDataConstant.TABLE_SCHOOL_PROFILE,
                    ORDER_ID = 4,
                    IS_VALIDATE = 1,
                    SQL_DELETE = ""
                };
                lstResDetail.Add(objResDetail);

                //Thực hiện chuyển IsActive=false với bản ghi tương ứng trong bảng PropertyOfSchool
                IQueryable<PropertyOfSchool> lsPropertyOfSchool = PropertyOfSchoolBusiness.All.Where(o => o.SchoolID == SchoolProfileID);
                if (lsPropertyOfSchool.Count() != 0)
                {
                    foreach (PropertyOfSchool pos in lsPropertyOfSchool)
                    {
                        PropertyOfSchoolBusiness.Delete(pos.PropertyOfSchoolID, SchoolProfileID);
                    }
                }
                //Thực hiện chuyển IsActive=false, Modified=SysDate với bản ghi tương ứng trong bảng SchoolSubsidiary
                IQueryable<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.All.Where(o => o.SchoolID == SchoolProfileID);
                if (lsSchoolSubsidiary.Count() != 0)
                {
                    foreach (SchoolSubsidiary ss in lsSchoolSubsidiary)
                    {
                        SchoolSubsidiaryBusiness.Delete(ss.SchoolSubsidiaryID);
                    }
                }
                //Thực hiện đổi tên tài khoản quản trị của nhà trường. Gọi hàm UserAccountBusiness.UpdateUser(UserAccountID, UserName) với UserName = .
                UserAccount account = this.UserAccountBusiness.Find(AdminID);
                string userName = account.aspnet_Users.UserName;
                if (account == null || account.GUID == null || account.aspnet_Users == null)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }

                this.UserAccountBusiness.UpdateUser(AdminID, account.aspnet_Users.UserName + "_deleted");



                //Du lieu acc quan tri
                objResDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = 0,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(SchoolProfileID),
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = SchoolProfileID,
                    SQL_UNDO = string.Format(RestoreDataConstant.SQL_UNDO_USER_ACCOUNT, AdminID),
                    IS_VALIDATE = 0,
                    ORDER_ID = 3,
                    TABLE_NAME = RestoreDataConstant.TABLE_USER_ACCOUNT,
                };
                lstResDetail.Add(objResDetail);

                

                String userId = ConvertGuidToUserId(account.GUID.Value);
                RestoreUserBO objResUser = new RestoreUserBO
                {
                    UserId = userId,
                    UserName = userName,
                    ReplaceUser = "",
                    SqlUndo = RestoreDataConstant.SQL_UNDO_USER_NAME
                };
                //Luu thong tin de phuc hoi userName
                objResDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = 0,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(SchoolProfileID),
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = SchoolProfileID,
                    SQL_UNDO = JsonConvert.SerializeObject(objResUser),
                    IS_VALIDATE = 1,
                    ORDER_ID = 2,
                    TABLE_NAME = RestoreDataConstant.TABLE_USERS
                };
                lstResDetail.Add(objResDetail);

                // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh Synchronize = -3
                //UserAccountBusiness.UpdateSynchronize(sp.AdminID.Value, GlobalConstants.STATUS_DELETE_USER);

                // Doi ten cac nguoi dung cua truong thanh acc_deleted
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolProfileID;
                IQueryable<UserAccount> query = this.UserAccountBusiness.SearchUser(SearchInfo);
                foreach (var useraccount in query)
                {
                    if (useraccount.aspnet_Users != null)
                    {
                        useraccount.IsActive = false;
                        string userNameTeacher = useraccount.aspnet_Users.UserName;
                        // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh Synchronize = -3
                        useraccount.SynchronizeID = GlobalConstants.STATUS_DELETE_USER;
                        useraccount.aspnet_Users.UserName = useraccount.aspnet_Users.UserName + "_deleted";
                        useraccount.aspnet_Users.LoweredUserName = useraccount.aspnet_Users.LoweredUserName + "_deleted";

                        //Lu thong tin phuc hoi tai khoan giao vien
                        objResDetail = new RESTORE_DATA_DETAIL
                        {
                            ACADEMIC_YEAR_ID = 0,
                            CREATED_DATE = DateTime.Now,
                            END_DATE = null,
                            LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(SchoolProfileID),
                            RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                            RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                            SCHOOL_ID = SchoolProfileID,
                            SQL_UNDO = string.Format(RestoreDataConstant.SQL_UNDO_USER_ACCOUNT, useraccount.UserAccountID),
                            IS_VALIDATE = 0,
                            ORDER_ID = 2,
                            TABLE_NAME = RestoreDataConstant.TABLE_USER_ACCOUNT
                        };
                        lstResDetail.Add(objResDetail);

                        userId = ConvertGuidToUserId(useraccount.GUID.Value);
                        objResUser = new RestoreUserBO
                        {
                            UserId = userId,
                            UserName = userNameTeacher,
                            ReplaceUser = "",
                            SqlUndo = RestoreDataConstant.SQL_UNDO_USER_NAME
                        };
                        //Luu thong tin de phuc hoi userName
                        objResDetail = new RESTORE_DATA_DETAIL
                        {
                            ACADEMIC_YEAR_ID = 0,
                            CREATED_DATE = DateTime.Now,
                            END_DATE = null,
                            LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(SchoolProfileID),
                            RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                            RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                            SCHOOL_ID = SchoolProfileID,
                            SQL_UNDO = JsonConvert.SerializeObject(objRes),
                            IS_VALIDATE = 1,
                            ORDER_ID = 1,
                            TABLE_NAME = RestoreDataConstant.TABLE_USERS
                        };
                        lstResDetail.Add(objResDetail);
                    }
                }
            }
            return lstResDetail;
        }
        #endregion

        #region GetListAppliedLevel

        public List<int> GetListAppliedLevel(int SchoolID)
        {
            SchoolProfile SchoolProfile = SchoolProfileRepository.Find(SchoolID);
            List<int> ListAppliedLevel = new List<int>();

            if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_CHILDREN) != 0)
                ListAppliedLevel.Add(SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN);
            if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_CHILDCARE) != 0)
                ListAppliedLevel.Add(SystemParamsInFile.APPLIED_LEVEL_CRECHE);
            if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_TERTIARY) != 0)
                ListAppliedLevel.Add(SystemParamsInFile.APPLIED_LEVEL_TERTIARY);
            if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_SECONDARY) != 0)
                ListAppliedLevel.Add(SystemParamsInFile.APPLIED_LEVEL_SECONDARY);
            if ((SchoolProfile.EducationGrade & GlobalConstants.COUNT_PRIMARY) != 0)
                ListAppliedLevel.Add(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);
            return ListAppliedLevel;
        }
        #endregion

        /// <summary>
        /// Kiem tra user da ton tai trong he thong
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="GuID"></param>
        /// <returns></returns>
        private void ValidateUserNameInMenbership(string UserName, string ResKey, Guid? GuID = null)
        {
            MembershipUser User = Membership.GetUser(UserName);
            bool isValid = true;
            // Neu user khac null thi kiem tra guid
            if (User != null)
            {
                if (GuID.HasValue)
                {
                    // Co truyen guid vao
                    if (!User.ProviderUserKey.Equals(GuID.Value))
                    {
                        // Neu nhu guid truyen vao khac voi userId cua user tim duoc thi user da ton tai trong
                        // DB voi userID khac voi guid truyen vao
                        // Ket qua tra lai la false
                        isValid = false;
                    }
                }
                else
                {
                    // Neu khong truyen guid vao thi tra lai false
                    isValid = false;
                }
            }
            // Neu khong hop le se nem ra loi
            if (!isValid)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
        }

        
        //        public List<IDictionary<string, object>> GetStatisticForClassAndPupilOfPrimaySchool(int schoolId, int yearId)
        //        {
        //            string query = @"
        //                SELECT ROWNUM as A, a.* FROM (
        //                  select 
        //                        SP.SCHOOL_NAME as B,
        //                        --(1) Tong so lop theo tung khoi
        //                        count (distinct(case when cp.Is_Combined_Class = 1 then cp.class_profile_id  end))  C,
        //                        count (distinct(case when cp.education_level_id = 1 then cp.class_profile_id  end))  D,
        //                        count (distinct(case when cp.education_level_id = 2 then cp.class_profile_id  end))  K,
        //                        count (distinct(case when cp.education_level_id = 3 then cp.class_profile_id  end))  R,
        //                        count (distinct(case when cp.education_level_id = 4 then cp.class_profile_id  end))  Y,
        //                        count (distinct(case when cp.education_level_id = 5 then cp.class_profile_id  end)) AF,
        //    
        //                        --(2) Tong so lop co sinh lop 1
        //                        count (distinct(case when cp.education_level_id = 1 then poc.pupil_id  end))  E,
        //                        --Chi tiet (2) theo khoi 1
        //                        count (distinct(case when cp.education_level_id = 1 AND PP.GENRE = 0 then poc.pupil_id  end))  F,
        //                        count (distinct(case when cp.education_level_id = 1 AND PP.ETHNIC_ID not in (1,79) then poc.pupil_id end)) G,
        //                        count (distinct(case when cp.education_level_id = 1 AND PP.ETHNIC_ID not in (1,79) AND PP.GENRE = 0 then poc.pupil_id end))  H,
        //                        count (distinct(case when cp.education_level_id = 1 AND cp.Is_Combined_Class = 1 then poc.pupil_id  end))  I,
        //                        count (distinct(case when cp.education_level_id = 1 AND PP.IS_DISABLED = 1 then poc.pupil_id end))  J,
        //        
        //                        --(3) Tong so hoc sinh khoi 2
        //                        count (distinct(case when cp.education_level_id = 2 then poc.pupil_id  end))  L,
        //                        --Chi tiet (3) theo khoi 2
        //                        count (distinct(case when cp.education_level_id = 2 AND PP.GENRE = 0 then poc.pupil_id  end))  M,
        //                        count (distinct(case when cp.education_level_id = 2 AND PP.ETHNIC_ID not in (1,79) then poc.pupil_id end))  N,
        //                        count (distinct(case when cp.education_level_id = 2 AND PP.ETHNIC_ID not in (1,79) AND PP.GENRE = 0 then poc.pupil_id end))  O,
        //                        count (distinct(case when cp.education_level_id = 2 AND cp.Is_Combined_Class = 1 then poc.pupil_id  end))  P,
        //                        count (distinct(case when cp.education_level_id = 2 AND PP.IS_DISABLED = 1 then poc.pupil_id end))  Q,
        //        
        //                        --(4) Tong so hoc khoi 3
        //                        count (distinct(case when cp.education_level_id = 3 then poc.pupil_id  end))  S,
        //                        -- Chi tiet (4) theo khoi 3
        //                        count (distinct(case when cp.education_level_id = 3 AND PP.GENRE = 0 then poc.pupil_id  end))  T,
        //                        count (distinct(case when cp.education_level_id = 3 AND PP.ETHNIC_ID not in (1,79) then poc.pupil_id end))  U,
        //                        count (distinct(case when cp.education_level_id = 3 AND PP.ETHNIC_ID not in (1,79) AND PP.GENRE = 0 then poc.pupil_id end))  V,
        //                        count (distinct(case when cp.education_level_id = 3 AND cp.Is_Combined_Class = 1 then poc.pupil_id  end))  W,
        //                        count (distinct(case when cp.education_level_id = 3 AND PP.IS_DISABLED = 1 then poc.pupil_id end))  X,
        //        
        //                        --(5) Tong so hoc sinh khoi 4
        //                        count (distinct(case when cp.education_level_id = 4 then poc.pupil_id  end))  Z,
        //                        --Chi tiet (5) theo khoi 4
        //                        count (distinct(case when cp.education_level_id = 4 AND PP.GENRE = 0 then poc.pupil_id  end))  AA,
        //                        count (distinct(case when cp.education_level_id = 4 AND PP.ETHNIC_ID not in (1,79) then poc.pupil_id end))  AB,
        //                        count (distinct(case when cp.education_level_id = 4 AND PP.ETHNIC_ID not in (1,79) AND PP.GENRE = 0 then poc.pupil_id end))  AC,
        //                        count (distinct(case when cp.education_level_id = 4 AND cp.Is_Combined_Class = 1 then poc.pupil_id  end))  AD,
        //                        count (distinct(case when cp.education_level_id = 4 AND PP.IS_DISABLED = 1 then poc.pupil_id end))  AE,
        //        
        //                        --(6) Tong so hoc sinh khoi 5
        //                        count (distinct(case when cp.education_level_id = 5 then poc.pupil_id  end))  AG,
        //                        --Chi tiet (5) theo khoi 4
        //                        count (distinct(case when cp.education_level_id = 5 AND PP.GENRE = 0 then poc.pupil_id  end))  AH,
        //                        count (distinct(case when cp.education_level_id = 5 AND PP.ETHNIC_ID not in (1,79) then poc.pupil_id end))  AI,
        //                        count (distinct(case when cp.education_level_id = 5 AND PP.ETHNIC_ID not in (1,79) AND PP.GENRE = 0 then poc.pupil_id end))  AJ,
        //                        count (distinct(case when cp.education_level_id = 5 AND cp.Is_Combined_Class = 1 then poc.pupil_id  end))  AK,
        //                        count (distinct(case when cp.education_level_id = 5 AND PP.IS_DISABLED = 1 then poc.pupil_id end))  AL
        //                from pupil_of_class poc
        //                    join class_profile cp on poc.class_id = cp.class_profile_id
        //                    join pupil_profile pp on poc.pupil_id = pp.pupil_profile_id
        //                    join SCHOOL_PROFILE sp on cp.school_id = sp.SCHOOL_PROFILE_ID
        //                where
        //                  poc.academic_year_id=:YEAR_ID 
        //                  AND 
        //                    cp.school_id=:SCHOOL_ID
        //                  and pp.is_active = 1
        //                  and  (poc.status = 1 or poc.status = 2)
        //                  and cp.education_level_id in (1,2,3,4,5)
        //                Group BY sp.SCHOOL_PROFILE_ID, SP.SCHOOL_NAME) a
        //                ";
        //            Dictionary<string, object> queryParams = new Dictionary<string, object>
        //            {
        //                {"SCHOOL_ID", schoolId},
        //                {"YEAR_ID", yearId}
        //            };
        //            return context.CollectionFromSql(query, queryParams).ToList();
        //        }

        private string ConvertGuidToUserId(Guid guid)
        {            
            return BitConverter.ToString(guid.ToByteArray()).Replace("-", "");
        }

        /// <summary>
        /// lấy danh sách mã trường để cấu hình
        /// </summary>
        /// <author date="27/02/2016">Anhvd9</author>
        /// <param name="req"></param>
        /// <returns></returns>
        public List<int> GetListSchoolID(IDictionary<string, object> dic)
        {
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int districtId = Utils.GetInt(dic, "DistrictID");
            string schoolName = Utils.GetString(dic, "SchoolName").ToLower();
            string userName = Utils.GetString(dic, "SchoolUserName").ToLower();
           
            var iqSchools = (from sp in SchoolProfileBusiness.All
                                                            join ua in UserAccountBusiness.All on sp.AdminID equals ua.UserAccountID
                                                            where sp.IsActive && sp.IsActiveSMAS.HasValue && sp.IsActiveSMAS.Value
                                                            && ua.IsActive
                                                            select new
                                                            {
                                                                SchoolProfileID = sp.SchoolProfileID,
                                                                DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                                                                ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                                                                SchoolName = sp.SchoolName,
                                                                SchoolUserName = ua.aspnet_Users.LoweredUserName
                                                            });
            if (provinceId > 0) iqSchools = iqSchools.Where(o => o.ProvinceID == provinceId);
            if (districtId > 0) iqSchools = iqSchools.Where(o => o.DistrictID == districtId);
            if (!string.IsNullOrEmpty(schoolName)) iqSchools = iqSchools.Where(o => o.SchoolName.ToLower().Contains(schoolName));
            if (!string.IsNullOrEmpty(userName)) iqSchools = iqSchools.Where(o => o.SchoolUserName.ToLower().Contains(userName));

            return iqSchools.Select(o => o.SchoolProfileID).ToList();
          
        }

        /// <summary>
        /// Lấy danh sách thông tin trường có phân trang 
        /// </summary>
        /// <author date="27/02/2016">Anhvd9</author>
        /// <param name="req"></param>
        /// <returns></returns>
        public List<SchoolBO> GetListSchool(IDictionary<string, object> dic, ref int total)
        {
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int districtId = Utils.GetInt(dic, "DistrictID");
            string schoolName = Utils.GetString(dic, "SchoolName").ToLower();
            string userName = Utils.GetString(dic, "SchoolUserName").ToLower();
            int status = Utils.GetInt(dic, "Status");
            List<int> lstSchoolId = Utils.GetIntList(dic, "LstSchoolID");
            List<int> lstSchoolIdExcept = Utils.GetIntList(dic, "LstSchoolIDExcept");
            int currentPage = Utils.GetInt(dic, "page");
            int pageSize = Utils.GetInt(dic, "numRecord");

            List<SchoolBO> result = new List<SchoolBO>();
            
            IQueryable<SchoolBO> iqSchools = (from sp in SchoolProfileBusiness.All
                                                            join ua in UserAccountBusiness.All on sp.AdminID equals ua.UserAccountID
                                                            where sp.IsActive && sp.IsActiveSMAS.HasValue && sp.IsActiveSMAS.Value
                                                            && ua.IsActive
                                                            && !lstSchoolIdExcept.Contains(sp.SchoolProfileID)
                                                        select new SchoolBO()
                                                            {
                                                                SchoolID = sp.SchoolProfileID,
                                                                Address = sp.Address,
                                                                CommuneID = sp.CommuneID.HasValue ? sp.CommuneID.Value : 0,
                                                                DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                                                                DistrictName = sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty,
                                                                EducationGrade = sp.EducationGrade,
                                                                Email = sp.Email,
                                                                EstablishedDate = sp.EstablishedDate.HasValue ? sp.EstablishedDate.Value : new DateTime(),
                                                                HeadMasterName = sp.HeadMasterName,
                                                                HeadMasterPhone = sp.HeadMasterPhone,
                                                                Telephone = sp.Telephone,
                                                                ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                                                                ProvinceName = sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty,
                                                                SchoolCode = sp.SchoolCode,
                                                                SchoolName = sp.SchoolName,
                                                                AreaID = sp.AreaID.HasValue ? sp.AreaID.Value : 0,
                                                                UserName = ua.aspnet_Users.LoweredUserName,
                                                            });

            if (provinceId > 0) iqSchools = iqSchools.Where(o => o.ProvinceID == provinceId);
            if (districtId > 0) iqSchools = iqSchools.Where(o => o.DistrictID == districtId);
            if (!string.IsNullOrEmpty(schoolName)) iqSchools = iqSchools.Where(o => o.SchoolName.ToLower().Contains(schoolName));
            if (!string.IsNullOrEmpty(userName)) iqSchools = iqSchools.Where(o => o.UserName.ToLower().Contains(userName));
            if (lstSchoolId != null && lstSchoolId.Count > 0)
            {
                if (status == 2) iqSchools = iqSchools.Where(o => !lstSchoolId.Contains(o.SchoolID));
                else iqSchools = iqSchools.Where(o => lstSchoolId.Contains(o.SchoolID));
            }

            total = iqSchools.Count();
            return iqSchools.OrderBy(o => o.ProvinceName).ThenBy(o => o.DistrictName).ThenBy(o => o.SchoolName)
                                        .Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            
        }

        public List<SchoolBO> GetListSchoolSP(IDictionary<string, object> dic, IQueryable<SMS_SERVICE_PACKAGE_UNIT> iqSp, bool? applied, ref int total)
        {
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int districtId = Utils.GetInt(dic, "DistrictID");
            string schoolName = Utils.GetString(dic, "SchoolName").ToLower();
            string userName = Utils.GetString(dic, "SchoolUserName").ToLower();
            int currentPage = Utils.GetInt(dic, "page");
            int pageSize = Utils.GetInt(dic, "numRecord");

            List<SchoolBO> result = new List<SchoolBO>();

            IQueryable<SchoolBO> iqSchools = (from sp in SchoolProfileBusiness.All
                                              join ua in UserAccountBusiness.All on sp.AdminID equals ua.UserAccountID
                                              join su in iqSp on sp.SchoolProfileID equals su.UNIT_ID into des
                                              from g in des.DefaultIfEmpty()
                                              where sp.IsActive && sp.IsActiveSMAS.HasValue && sp.IsActiveSMAS.Value
                                              && ua.IsActive
                                              select new SchoolBO()
                                              {
                                                  SchoolID = sp.SchoolProfileID,
                                                  Address = sp.Address,
                                                  CommuneID = sp.CommuneID.HasValue ? sp.CommuneID.Value : 0,
                                                  DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                                                  DistrictName = sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty,
                                                  EducationGrade = sp.EducationGrade,
                                                  Email = sp.Email,
                                                  EstablishedDate = sp.EstablishedDate.HasValue ? sp.EstablishedDate.Value : new DateTime(),
                                                  HeadMasterName = sp.HeadMasterName,
                                                  HeadMasterPhone = sp.HeadMasterPhone,
                                                  Telephone = sp.Telephone,
                                                  ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                                                  ProvinceName = sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty,
                                                  SchoolCode = sp.SchoolCode,
                                                  SchoolName = sp.SchoolName,
                                                  AreaID = sp.AreaID.HasValue ? sp.AreaID.Value : 0,
                                                  UserName = ua.aspnet_Users.LoweredUserName,
                                                  HasBeenApplied = g != null ? true : false
                                              });

            if (provinceId > 0) iqSchools = iqSchools.Where(o => o.ProvinceID == provinceId);
            if (districtId > 0) iqSchools = iqSchools.Where(o => o.DistrictID == districtId);
            if (!string.IsNullOrEmpty(schoolName)) iqSchools = iqSchools.Where(o => o.SchoolName.ToLower().Contains(schoolName));
            if (!string.IsNullOrEmpty(userName)) iqSchools = iqSchools.Where(o => o.UserName.ToLower().Contains(userName));
            if (applied.HasValue)
            {
                if (applied.Value == true)
                {
                    iqSchools = iqSchools.Where(o => o.HasBeenApplied);
                }
                else
                {
                    iqSchools = iqSchools.Where(o => !o.HasBeenApplied);
                }
            }

            total = iqSchools.Count();
            return iqSchools.OrderBy(o => o.ProvinceName).ThenBy(o => o.DistrictName).ThenBy(o => o.SchoolName)
                                        .Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

        }
        public bool IsSMSParentActive(int SchoolID)
        {
            SchoolProfile sp = this.Find(SchoolID);
            if (sp !=null && sp.SMSParentActiveType == SystemParamsInFile.COMMON_STATUS_ISACTIVE)
            {
                //gan gia tri thuoc tinh dinh kem 
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check trường có đăng kí sử dụng SMS teacher hay không
        /// </summary>
        /// <auther>TuTV</auther>
        /// <param name="schoolID">ID trường hiện tại</param>
        /// <returns></returns>
        public bool IsSMSTeacherActive(int schoolID)
        {
            var obj = this.Find(schoolID);
            if (obj != null)
            {
                if (obj.SMSTeacherActiveType > 0)
                    return true;
                return false;
            }
            return false;
        }

        public SchoolBO GetUnitByAdminAccount(string UserName)
        {
            MembershipUser objMembershipUser;
            objMembershipUser = Membership.GetUser(UserName);
            if (objMembershipUser == null)
            {
                return null;
            }
            Guid userId = (Guid)objMembershipUser.ProviderUserKey;
            UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
            SchoolProfile sp = null;
            SupervisingDept sd = null;
            SchoolBO spd = null;
            // Nếu là admin trường
            if (objUserAccount.IsAdmin)
            {
                sp = SchoolProfileBusiness.All.FirstOrDefault(o => o.AdminID == objUserAccount.UserAccountID);
                sd = SupervisingDeptBusiness.All.FirstOrDefault(o => o.AdminID == objUserAccount.UserAccountID);
                if (sp != null)
                {
                    spd = new SchoolBO
                    {
                        SchoolID = sp.SchoolProfileID,
                        SchoolName = sp.SchoolName,
                        ProvinceName = sp.Province != null ? sp.Province.ProvinceName : string.Empty,

                    };
                }
                else if (sd != null)
                {
                    spd = new SchoolBO
                    {
                        SupervisingDeptID = sd.SupervisingDeptID,
                        SupervisingDeptName = sd.SupervisingDeptName,
                        ProvinceName = sd.Province != null ? sd.Province.ProvinceName : string.Empty
                    };
                }
            }

            return spd;
        }

        public string GetUserNameBySchoolID(int schoolID)
        {
            try
            {
                var userName = (from ua in context.UserAccount
                                join asp in context.aspnet_Users on ua.GUID equals asp.UserId
                                join sp in context.SchoolProfile on ua.UserAccountID equals sp.AdminID
                                where sp.SchoolProfileID == schoolID
                                select asp.UserName).FirstOrDefault();

                return userName;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Lấy thông tin nhà trường trực thuộc đơn vị -không phân trang từng phần
        /// </summary>
        /// <param name="EducationGrade"></param>
        /// <param name="SuperVisingDeptID"></param>
        /// <param name="SchoolName"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public List<SchoolBO> GetSchoolByRegionNotPaging(int EducationGrade, int SupervisingDeptID, string SchoolName)
        {
           
                // Lay danh sach tat cac don vi quan ly thuoc quan ly cua don vi truyen vao hoac chinh no
                string traversalPath = "\\" + SupervisingDeptID + "\\";
                IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                    .Where(o => o.IsActive
                        && (o.TraversalPath.Contains(traversalPath) || o.SupervisingDeptID == SupervisingDeptID));
                IQueryable<SchoolProfile> iQSchool = SchoolProfileBusiness.AllNoTracking
                    .Where(o => listSupervisingDept.Any(s => s.SupervisingDeptID == o.SupervisingDeptID)
                    && o.IsActiveSMAS == true
                    && !string.IsNullOrEmpty(o.HeadMasterPhone.Trim())
                     && o.IsActive == true
                    );

                EducationGrade = EducationGrade > 0 ? Utils.GradeToBinary(EducationGrade) : (int)0;
                if (EducationGrade > 0)
                {
                    iQSchool = iQSchool.Where(o => (o.EducationGrade & EducationGrade) != 0);
                }

                if (!string.IsNullOrWhiteSpace(SchoolName))
                {
                    iQSchool = iQSchool.Where(o => o.SchoolName.ToLower().Contains(SchoolName.Trim().ToLower()));
                }

                int totalRecord = iQSchool.Count();
                List<SchoolBO> listSchool = (iQSchool
                    .Select(o => new SchoolBO
                    {
                        SchoolID = o.SchoolProfileID,
                        SchoolName = o.SchoolName,
                        HeadMasterName = o.HeadMasterName,
                        HeadMasterPhone = o.HeadMasterPhone,
                        DistrictName = o.District.DistrictName
                    })).ToList();

                return listSchool;
        }

        /// <summary>
        /// Lấy danh sách trường học theo cấp quản lý và danh sách ID trường
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <param name="SchoolIDList"></param>
        /// <returns></returns>
        public List<HistoryReceiverBO> GetListSchoolByListID(int SupervisingDeptID, List<int> SchoolIDList)
        {
           
                // Lay danh sach tat cac don vi quan ly thuoc quan ly cua don vi truyen vao hoac chinh no
                string traversalPath = "\\" + SupervisingDeptID + "\\";
                IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                    .Where(o => o.IsActive && o.TraversalPath.Contains(traversalPath) || o.SupervisingDeptID == SupervisingDeptID);
                IQueryable<SchoolProfile> iQSchool = SchoolProfileBusiness.AllNoTracking
                    .Where(o => listSupervisingDept.Any(s => s.SupervisingDeptID == o.SupervisingDeptID));
                if (SchoolIDList != null && SchoolIDList.Count > 0)
                {
                    iQSchool = iQSchool.Where(o => SchoolIDList.Contains(o.SchoolProfileID));
                }

                List<HistoryReceiverBO> listSchool = iQSchool.Select(o => new HistoryReceiverBO
                {
                    ReceiverID = o.SchoolProfileID,
                    ReceiverName = o.HeadMasterName,
                    ReceiverUnitName = o.SchoolName,
                    Mobile = o.HeadMasterPhone
                }).ToList();

                return listSchool;
            
        }

        /// <summary>
        /// Lấy thông tin nhà trường trực thuộc đơn vị - có phân trang từng phần
        /// </summary>
        /// <param name="EducationGrade"></param>
        /// <param name="SuperVisingDeptID"></param>
        /// <param name="SchoolName"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public List<SchoolBO> GetSchoolByRegion(int EducationGrade, int SupervisingDeptID, string SchoolName
            , int PageNum, int PageSize, ref int total)
        {
           
                // Lay danh sach tat cac don vi quan ly thuoc quan ly cua don vi truyen vao hoac chinh no
                string traversalPath = "\\" + SupervisingDeptID + "\\";
                IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                    .Where(o => o.IsActive && (o.TraversalPath.Contains(traversalPath) || o.SupervisingDeptID == SupervisingDeptID));
                IQueryable<SchoolProfile> iQSchool = SchoolProfileBusiness.AllNoTracking
                    .Where(o => listSupervisingDept.Any(s => s.SupervisingDeptID == o.SupervisingDeptID)
                    && o.IsActiveSMAS == true
                    && !string.IsNullOrEmpty(o.HeadMasterPhone.Trim())
                     && o.IsActive == true
                    );

                EducationGrade = EducationGrade > 0 ? Utils.GradeToBinary(EducationGrade) : (int)0;
                if (EducationGrade > 0)
                {
                    iQSchool = iQSchool.Where(o => (o.EducationGrade & EducationGrade) != 0);
                }

                if (!string.IsNullOrWhiteSpace(SchoolName))
                {
                    SchoolName = SchoolName.Trim();
                    iQSchool = iQSchool.Where(o => o.SchoolName.ToLower().Contains(SchoolName.ToLower()));
                }

                int totalRecord = iQSchool.Count();
                List<SchoolBO> listSchool = iQSchool
                    .Select(o => new SchoolBO
                    {
                        SchoolID = o.SchoolProfileID,
                        SchoolName = o.SchoolName,
                        HeadMasterName = o.HeadMasterName,
                        HeadMasterPhone = o.HeadMasterPhone,
                        DistrictName = o.District.DistrictName
                    }).OrderBy(o => o.SchoolName).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();

                // Tong so ban ghi
                total = totalRecord;
                return listSchool;
        }
        public IDictionary<int,string> GetListUserNameByListUserID(List<int> lstUserID)
        {
            try
            {
                var lstUserName = (from ua in context.UserAccount
                                join asp in context.aspnet_Users on ua.GUID equals asp.UserId
                                where lstUserID.Contains(ua.UserAccountID)
                                select new
                                {
                                    UserID = ua.UserAccountID,
                                    UserName = asp.UserName
                                }).ToList();
                IDictionary<int, string> dicResult = new Dictionary<int,string>();
                lstUserName.ForEach(p =>
                {
                    dicResult[p.UserID] = p.UserName;
                });
                return dicResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new Dictionary<int, string>();
            }
        }
        public IDictionary<Guid, string> GetListUserNameCBByListUserID(List<Guid> lstUserID)
        {
            try
            {
                var lstUserName = (from ua in context.AppUserAccount
                                   join asp in context.AppUsers on ua.USER_ID equals asp.USER_ID
                                   where lstUserID.Contains(ua.USER_ACCOUNT_ID)
                                   select new
                                   {
                                       UserID = ua.USER_ACCOUNT_ID,
                                       UserName = asp.USERNAME
                                   }).ToList();
                IDictionary<Guid, string> dicResult = new Dictionary<Guid, string>();
                lstUserName.ForEach(p =>
                {
                    dicResult[p.UserID] = p.UserName;
                });
                return dicResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new Dictionary<Guid, string>();
            }
        }
        public string GetUserNameByUserAccount(int UserAccountID)
        {
            return (from ua in context.UserAccount
                               join asp in context.aspnet_Users on ua.GUID equals asp.UserId
                               where ua.UserAccountID == UserAccountID
                               select asp.UserName).FirstOrDefault();
        }
        public List<SchoolProfileAPI> GetSchoolByAPI(IDictionary<string, object> dic)
        {
            bool IsSystemAdmin = Utils.GetBool(dic, "IsSystemAdmin");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string SchoolName = Utils.GetString(dic, "SchoolName");
            string SchoolCode = Utils.GetString(dic, "SchoolCode");

            Province objProvince = ProvinceID > 0 ? ProvinceBusiness.Find(ProvinceID) : null;
            District objDistrict = DistrictID > 0 ? DistrictBusiness.Find(DistrictID) : null;
            string provinceAlias = objProvince != null ? objProvince.ProvinceCode : "all";
            string districtAlias = objDistrict != null ? objDistrict.DistrictCode : "all";

            int page = 1;

            string uri = System.Configuration.ConfigurationManager.AppSettings["VIETTEL_STUDY_API_URL"];
            uri = uri + "csdledu.svc/getSchools/{IdSchools}/{IdProvince}/{IdDistrict}/{currPage}/all";
            //string uri = "http://api.viettelstudy.vn/csdledu.svc/getSchools/{IdSchools}/{IdProvince}/{IdDistrict}/{currPage}/all";
            uri = uri.Replace("{IdSchools}", !string.IsNullOrEmpty(SchoolCode) ? SchoolCode : "all");
            uri = uri.Replace("{IdProvince}", provinceAlias);
            uri = uri.Replace("{IdDistrict}", districtAlias);
            uri = uri.Replace("{currPage}", page.ToString());

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (send, certificate, chain, sslPolicyErrors) => { return true; };
            var client = new System.Net.WebClient();
            client.Headers.Add("Content-Type", "application/json;charset=utf-8");
            var content = client.DownloadString(uri);
            ResponseData resultData = JsonConvert.DeserializeObject<ResponseData>(content);
            List<SchoolProfileAPI> lstResult = new List<SchoolProfileAPI>();

            if (resultData.message.Equals("success"))
            {
                lstResult = resultData.Data;
            }

            if (!string.IsNullOrEmpty(SchoolName))
            {
                lstResult = lstResult.Where(x => x.NameSchool.ToUpper().Contains(SchoolName.ToUpper())).ToList();
            }
            return lstResult;
        }

    }
}
