﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DailyOtherServiceInspectionBusiness
    {
        public void Insert(List<DailyOtherServiceInspection> lstDailyOtherServiceInspection)
        {
            //Price: require, interger
            foreach (var dailyOtherServiceInspection in lstDailyOtherServiceInspection)
            {
                if (dailyOtherServiceInspection.Price <= 0)
                {
                    throw new BusinessException("DailyOtherServiceInspection_Validate_Price");
                }
            }
            /*
             Tìm kiếm trong bảng DailyOtherServiceInspection dosi
            Với điều kiện dosi.DishInspectionID = entity.DishInspectionID
            Xoá các bản ghi trả về.
             */
            foreach (var dailyOtherServiceInspection in lstDailyOtherServiceInspection)
            {
                var listDailyOtherServiceInspection = DailyOtherServiceInspectionRepository.All.Where(o => o.DishInspectionID == dailyOtherServiceInspection.DishInspectionID);
                if (listDailyOtherServiceInspection.Count() > 0)
                {
                    DailyOtherServiceInspectionRepository.DeleteAll(listDailyOtherServiceInspection.ToList());
                }
            }
            /*
            Tạo đối tượng DailyOtherServiceInspection dfi
            dosi.Price = entity.Price
            dfi.DishInspectionID = entity.DishInspectionID
            dosi.OtherServiceID = entity.OtherServiceID
            Thực hiện insert vào bảng DailyOtherServiceInspection
             */
            foreach (var dailyOtherServiceInspection in lstDailyOtherServiceInspection)
            {
                DailyOtherServiceInspection entity = new DailyOtherServiceInspection();
                entity.Price = dailyOtherServiceInspection.Price;
                entity.DishInspectionID = dailyOtherServiceInspection.DishInspectionID;
                entity.OtherServiceID = dailyOtherServiceInspection.OtherServiceID;
                DailyOtherServiceInspectionRepository.Insert(entity);
            }
        }

    }
}