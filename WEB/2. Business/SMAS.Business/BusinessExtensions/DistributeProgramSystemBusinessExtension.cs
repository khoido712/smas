/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class DistributeProgramSystemBusiness
    {
        public IQueryable<DistributeProgramSystem> Search(IDictionary<string, object> dic)
        {
            int DistributeProgramSystemID = Utils.GetInt(dic, "DistributeProgramSystemID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            string AssignSubjectName = Utils.GetString(dic, "AssignSubjectName");
            int SchoolWeekID = Utils.GetInt(dic, "SchoolWeekID");
            int? DistributeProgramOrder = Utils.GetNullableInt(dic, "DistributeProgramOrder");

            IQueryable<DistributeProgramSystem> query = DistributeProgramSystemBusiness.All;

            #region [Where]
            if (DistributeProgramSystemID != 0)
            {
                query = query.Where(p => p.DistributeProgramSystemID == DistributeProgramSystemID);
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(p => p.EducationLevelID == EducationLevelID);
            }
            if (SubjectID != 0)
            {
                query = query.Where(p => p.SubjectID == SubjectID);
            }
            if (!string.IsNullOrEmpty(AssignSubjectName))
            {
                query = query.Where(p => p.AssignSubjectName == AssignSubjectName);
            }
            if (SchoolWeekID != 0)
            {
                query = query.Where(p => p.SchoolWeekID == SchoolWeekID);
            }
            if (DistributeProgramOrder.HasValue)
            {
                query = query.Where(p => p.DistributeProgramOrder == DistributeProgramOrder);
            }
            #endregion

            return query;
        }
        public List<DistributeProgramSystemBO> getListDistributeProgramSystem(IDictionary<string, object> SearchInfo)
        {
            if (SearchInfo == null) SearchInfo = new Dictionary<string, object>();
            var query = this.Search(SearchInfo);
            return query.Select(x => new DistributeProgramSystemBO()
            {
                AssignSubjectName = x.AssignSubjectName,
                DistributeProgramOrder = x.DistributeProgramOrder,
                DistributeProgramSystemID = x.DistributeProgramSystemID,
                EducationLevelID = x.EducationLevelID,
                LessonName = x.LessonName,
                SchoolWeekID = x.SchoolWeekID,
                SubjectID = x.SubjectID
            }).ToList();
        }
        public Boolean CheckShowButon(int subjectID, int educationLevelID)
        {
            var SearchInfo = new Dictionary<string, object>() { 
                {"SubjectID",subjectID},
                {"EducationLevelID",educationLevelID}
            };
            var query = this.Search(SearchInfo);
            return query.Any();
        }
    }
}
