﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Web.Configuration;
using System.Data.Objects;
using Payment.ServiceLoader.Utility;
using Payment.ServiceLoader.Loader;
using Payment.ServiceLoader.ILoader;

namespace SMAS.Business.Business
{
    public partial class TopupBusiness
    {
        /// <summary>
        /// lay thong tin giao dich
        /// </summary>
        /// <param name="dic"></param>
        /// <author date="140530">huentm</author> 
        /// Sua - lay thong tin cac giao dich theo tung loai giao dich
        /// <returns></returns>
        public List<EwalletTransactionBO> FindTransaction(int ewalletID, DateTime? fromDate, DateTime? toDate, int transactionTypeID, ref int total)
        {


            if (ewalletID > 0)
            {
                var result = (from et in this.EWalletTransactionBusiness.All
                              join ts in this.EwalletTransactionTypeBusiness.All on et.TRANS_TYPE_ID equals ts.TRANSACTION_TYPE_ID
                              join pu in this.TopupBusiness.All on et.EWALLET_TRANSACTION_ID equals pu.EWALLET_TRANSACTION_ID into g
                              from spu in g.DefaultIfEmpty()
                              where et.EWALLET_ID == ewalletID
                              && (!fromDate.HasValue || (fromDate.HasValue && EntityFunctions.TruncateTime(et.CREATED_TIME) >= EntityFunctions.TruncateTime(fromDate.Value)))
                              && (!toDate.HasValue || (toDate.HasValue && EntityFunctions.TruncateTime(et.CREATED_TIME) <= EntityFunctions.TruncateTime(toDate.Value)))
                              && et.STATUS == GlobalConstantsEdu.TRANSACTION_STATUS_SUCCESS
                              && (et.TRANS_TYPE_ID == transactionTypeID || transactionTypeID == 0)
                              select new EwalletTransactionBO
                              {
                                  TransactionID = et.EWALLET_TRANSACTION_ID,
                                  TransactionDateTime = et.CREATED_TIME,
                                  TransactionAmount = et.TRANS_AMOUNT,
                                  status = et.STATUS,
                                  BalanceAfterTransaction = et.BALANCE_AFTER,
                                  TransTypeID = ts.TRANSACTION_TYPE_ID,
                                  Name = ts.NAME,
                                  TransTypeDes = ts.DESCRIPTION,
                                  BalanceBefore = et.BALANCE_BEFORE,
                                  BalanceAfter = et.BALANCE_AFTER,
                                  CardSerial = spu.CARD_SERIAL,
                                  ServiceType = (byte?)et.SERVICE_TYPE,
                                  Quantity = et.QUANTITY,
                                  Description = et.DESCRIPTION
                              }).ToList();

                total = result.Count();

                return result;
            }

            return new List<EwalletTransactionBO>();
        }

        public List<EwalletTransactionBO> FindTransactionPaging(int ewalletID, DateTime? fromDate, DateTime? toDate, int index, int numberOfRetrievedRecord, int transactionTypeID, ref int total)
        {


            if (ewalletID > 0)
            {
                var result = (from et in this.EWalletTransactionBusiness.All
                              join ts in this.EwalletTransactionTypeBusiness.All on et.TRANS_TYPE_ID equals ts.TRANSACTION_TYPE_ID
                              join pu in this.TopupBusiness.All on et.EWALLET_TRANSACTION_ID equals pu.EWALLET_TRANSACTION_ID into g
                              from spu in g.DefaultIfEmpty()
                              where et.EWALLET_ID == ewalletID
                              && (!fromDate.HasValue || (fromDate.HasValue && EntityFunctions.TruncateTime(et.CREATED_TIME) >= EntityFunctions.TruncateTime(fromDate.Value)))
                              && (!toDate.HasValue || (toDate.HasValue && EntityFunctions.TruncateTime(et.CREATED_TIME) <= EntityFunctions.TruncateTime(toDate.Value)))
                              && et.STATUS == GlobalConstantsEdu.TRANSACTION_STATUS_SUCCESS
                              && (et.TRANS_TYPE_ID == transactionTypeID || transactionTypeID == 0)
                              select new EwalletTransactionBO
                              {
                                  TransactionID = et.EWALLET_TRANSACTION_ID,
                                  TransactionDateTime = et.CREATED_TIME,
                                  TransactionAmount = et.TRANS_AMOUNT,
                                  status = et.STATUS,
                                  BalanceAfterTransaction = et.BALANCE_AFTER,
                                  TransTypeID = ts.TRANSACTION_TYPE_ID,
                                  Name = ts.NAME,
                                  TransTypeDes = ts.DESCRIPTION,
                                  BalanceBefore = et.BALANCE_BEFORE,
                                  BalanceAfter = et.BALANCE_AFTER,
                                  CardSerial = spu.CARD_SERIAL,
                                  ServiceType = (byte?)et.SERVICE_TYPE,
                                  Quantity = et.QUANTITY,
                                  Description = et.DESCRIPTION
                              });

                total = result.Count();

                return (from p in result
                        orderby p.TransactionDateTime descending
                        select p).Skip(index).Take(numberOfRetrievedRecord).ToList();
            }

            return new List<EwalletTransactionBO>();
        }

        public void MoveTransaction(int fromEwalletID, int toEwalletID, List<long> listTransactionID)
        {
            List<EW_EWALLET_TRANSACTION> lstEwalletTrasaction = this.EWalletTransactionBusiness.All.Where(o => o.EWALLET_ID == fromEwalletID
                && listTransactionID.Contains(o.EWALLET_TRANSACTION_ID)).ToList();

            for (int i = 0; i < lstEwalletTrasaction.Count; i++)
            {
                lstEwalletTrasaction[i].EWALLET_ID = toEwalletID;
                this.EWalletTransactionBusiness.Update(lstEwalletTrasaction[i]);
            }

            this.EWalletTransactionBusiness.Save();
        }

        public TopupResponseBO TopUp(int ewalletID, string cardSerial, string pinCard)
        {
            DateTime startTime = DateTime.Now;
          
            // AnhVD9 - Doi khi tat cac cac giao dich Nap the da xong
            EWalletBusiness.WaitingTopup(ewalletID);
            // AnhVD9 - Thuc hien Khoa vi khi Dang nap the
            EWalletBusiness.LockEWallet(ewalletID, true);

            //PaymentEntities currentContext = new PaymentEntities();
            TopupResponseBO response = new TopupResponseBO();
            var ewallet = this.EWalletBusiness.Find(ewalletID);
            try
            {
                // tao transaction
                if (ewallet != null)
                {
                    // kiem tra trang thai vi dien tu
                    if (ewallet.LAST_TOPUP_FAIL_DATE.HasValue)
                    {
                        DateTime dateTimeNow = DateTime.Now;
                        int lockMin = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LockMin"]);
                        int min = (dateTimeNow - ewallet.LAST_TOPUP_FAIL_DATE.Value).Minutes;
                        if (min >= lockMin)
                        {
                            ewallet.TOPUP_FAIL_COUNT = 0;
                            ewallet.IS_LOCKED_OUT = false;
                        }
                        else
                        {
                            int LockCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LockCount"]);
                            if (ewallet.TOPUP_FAIL_COUNT >= LockCount)
                            {
                                ewallet.IS_LOCKED_OUT = true;
                                response.Success = false;
                                response.ErrorCode = "200";
                                response.RemainTimeToUnLock = lockMin - min;
                                this.EWalletBusiness.Update(ewallet);
                                this.EWalletBusiness.Save();
                            }
                            else
                            {
                                ewallet.IS_LOCKED_OUT = false;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(response.ErrorCode))
                    {
                        // tao transaction, mac dinh la that bai
                        // sau khi thuc hien xong giao dich nap the thi update lai thong tin transaction
                        EW_EWALLET_TRANSACTION etrans = new EW_EWALLET_TRANSACTION();
                        etrans.EWALLET_ID = ewallet.EWALLET_ID;
                        etrans.TRANS_AMOUNT = 0;
                        etrans.TRANS_TYPE_ID = GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_PLUS_TOPUP;
                        etrans.BALANCE_BEFORE = (int)ewallet.BALANCE;
                        etrans.BALANCE_AFTER = (int)ewallet.BALANCE;
                        etrans.STATUS = 0;
                        etrans.CREATED_TIME = DateTime.Now;
                        Guid guid = Guid.NewGuid();
                        etrans.RAW_ID = guid;
                        this.EWalletTransactionBusiness.Insert(etrans);
                        this.EWalletTransactionBusiness.Save();

                        etrans = this.EWalletTransactionBusiness.All.FirstOrDefault(o => o.EWALLET_ID == ewallet.EWALLET_ID
                            && o.RAW_ID == guid);

                        // thuc hien nap card
                        string transID = CreateTransID();
                        TopupLoaderResult result = new TopupLoaderResult();
                        //AnhVD9 20150320 -  GL Nap the
                        if (ConfigEdu.GetString(GlobalConstantsEdu.KEY_SIMULATION_TOPUP) != null && Boolean.Parse(ConfigEdu.GetString(GlobalConstantsEdu.KEY_SIMULATION_TOPUP)))
                        {
                            result = new TopupLoaderResult();
                            result.ErrorCode = "00";
                            result.TransID = transID;
                            result.ErrorMessage = string.Empty;
                            result.TopupTime = DateTime.Now;
                            result.TopupTimeSpecified = true;
                            result.Amount = decimal.Parse(ConfigEdu.GetString(GlobalConstantsEdu.KEY_MONEY));
                        }
                        else
                        {
                            IScratchCardServiceLoader ScratchCardServiceLoader = new ScratchCardServiceLoader();
                            result = ScratchCardServiceLoader.Topup(ConfigEdu.GetString("PartnerID"), ConfigEdu.GetString("passPhare"), cardSerial, pinCard, transID);
                        }

                        if (result != null)
                        {
                            EW_TOPUP topup = new EW_TOPUP();
                            topup.TOPUP_ID = Guid.NewGuid();
                            topup.EWALLET_TRANSACTION_ID = etrans.EWALLET_TRANSACTION_ID;
                            topup.CARD_SERIAL = cardSerial;
                            topup.PIN_CARD = pinCard;
                            topup.ERROR_CODE = result.ErrorCode;
                            topup.ERROR_MESSAGE = result.ErrorMessage;
                            topup.CREATED_TIME = result.TopupTime;
                            topup.TRANS_REQUEST = transID;
                            topup.TRAN_RESPONSE = result.TransID;
                            topup.ORIGIN_TRANS_ID = !string.IsNullOrEmpty(result.OriginTransID) ? result.OriginTransID : " ";
                            topup.TOPUP_TIMES_PECIFIED = result.TopupTimeSpecified;
                            this.TopupBusiness.Insert(topup);

                            if (topup.ERROR_CODE.Equals("00"))
                            {
                                // cap nhat lai transaction
                                etrans.TRANS_AMOUNT = (int)result.Amount;
                                etrans.STATUS = 1;
                                etrans.UPDATED_TIME = DateTime.Now;
                                etrans.BALANCE_AFTER = etrans.BALANCE_BEFORE + (int)result.Amount;
                                
                                // cap nhat lai vi dien tu
                                ewallet.BALANCE = ewallet.BALANCE + (int)result.Amount;
                                ewallet.UPDATED_TIME = DateTime.Now;
                                // update lai trang thai vi dien tu khi co giao dich nap the thanh cong
                                ewallet.TOPUP_FAIL_COUNT = 0;
                                ewallet.IS_LOCKED_OUT = false;
                                
                                // update lai response tra ve
                                response.Success = true;
                                response.TransactionAmount = result.Amount;
                                response.TransactionTime = result.TopupTime;
                                response.BalanceAfterTransaction = ewallet.BALANCE;
                                response.ErrorCode = result.ErrorCode;
                                response.TransactionID = etrans.EWALLET_TRANSACTION_ID;
                            }
                            else
                            {
                                response.Success = false;
                                response.ErrorCode = result.ErrorCode;
                                response.TransactionTime = result.TopupTime;
                                response.TransactionID = etrans.EWALLET_TRANSACTION_ID;
                                // count thoi gian khoa tai khoan
                                ewallet.LAST_TOPUP_FAIL_DATE = DateTime.Now;
                                ewallet.TOPUP_FAIL_COUNT += 1;
                            }

                            this.EWalletTransactionBusiness.Update(etrans);
                            this.EWalletBusiness.Update(ewallet);
                            this.Save();
                            // AnhVD9  20150821 - Bo sung TopupID
                            response.TopupID = topup.TOPUP_ID;
                        }
                        else
                        {
                            response.Success = false;
                            response.ErrorCode = "202";
                        }
                    }
                }
                else
                {
                    response.Success = false;
                    response.ErrorCode = "201";
                }

            }
            catch (Exception ex)
            {

                response.Success = false;
                response.ErrorCode = "199";
               
            }

            // AnhVD9 - Mo khoa vi khi Dang nap xong
            EWalletBusiness.LockEWallet(ewalletID, false);
            return response;
        }

        private string CreateTransID()
        {
            return string.Format("{0}{1}{2}", DateTime.Now.ToString("yyMMddHHmmss"), ConfigEdu.GetString("PartnerID"), DateTime.Now.ToString("fff"));
        }
    }
}
