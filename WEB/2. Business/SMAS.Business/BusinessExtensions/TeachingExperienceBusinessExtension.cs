/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class TeachingExperienceBusiness
    {
        #region Kiem tra du lieu dau vao
        private void Validate(TeachingExperience TeachingExperience)
        {
            // Kiem tra cac du lieu chung
            ValidationMetadata.ValidateObject(TeachingExperience);
            // Kiem tra cac dieu kien khac
            bool AcademicSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                   new Dictionary<string, object>()
                {
                    {"AcademicYearID",TeachingExperience.AcademicYearID},
                    {"SchoolID",TeachingExperience.SchoolID}
                }, null);
            if (!AcademicSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Giao vien va truong
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                   new Dictionary<string, object>()
                {
                    {"EmployeeID",TeachingExperience.TeacherID},
                    {"SchoolID",TeachingExperience.SchoolID}
                }, null);
            if (!AcademicSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Kiem tra phai thuoc nam hoc
            if (!AcademicYearBusiness.IsCurrentYear(TeachingExperience.AcademicYearID))
            {
                throw new BusinessException("Common_IsCurrentYear_Err");
            }

            // Kiem tra trang thai lam viec
            Employee Employee = EmployeeBusiness.Find(TeachingExperience.TeacherID);
            if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("Employee_Label_NotWorkingStatusErrr");
            }

            // Kiem tra Grade in (A,B,C) va RegisteredLevel: range (1-4)
            if (TeachingExperience.Grade != SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_A
                && TeachingExperience.Grade != SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_B
                && TeachingExperience.Grade != SystemParamsInFile.TEACHING_EXPERIENCE_GRADE_C)
            {
                throw new BusinessException("TeachingExperience_Label_GradeErr");
            }
            if (TeachingExperience.RegisteredLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_MINISTRY && TeachingExperience.RegisteredLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE &&
                TeachingExperience.RegisteredLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE && TeachingExperience.RegisteredLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_SCHOOL)
            {
                throw new BusinessException("TeachingExperience_Label_RegisteredLevelErr");
            }

        }
        #endregion

        #region Them moi
        public override TeachingExperience Insert(TeachingExperience TeachingExperience)
        {
            // Kiem tra du lieu dau vao
            this.Validate(TeachingExperience);
            // Lay thong tin to bo mon
            Employee Employee = EmployeeBusiness.Find(TeachingExperience.TeacherID);
            TeachingExperience.FacultyID = Employee.SchoolFacultyID;
            return base.Insert(TeachingExperience);
        }
        #endregion

        #region Cap nhat
        public override TeachingExperience Update(TeachingExperience TeachingExperience)
        {
            // Kiem tra du lieu dau vao
            this.Validate(TeachingExperience);
            // Lay thong tin to bo mon
            Employee Employee = EmployeeBusiness.Find(TeachingExperience.TeacherID);
            TeachingExperience.FacultyID = Employee.SchoolFacultyID;
            return base.Update(TeachingExperience);
        }
        #endregion

        #region Xoa
        public void Delete(int TeachingExperienceID)
        {
            // Kiem tra ID co ton tai trong he thong
            this.CheckAvailable(TeachingExperienceID, "TeachingExperience_Label_TeachingExperienceID");
            // Xoa
            base.Delete(TeachingExperienceID);
        }
        #endregion

        #region Tim kiem
        public IQueryable<TeachingExperience> Search(IDictionary<string, object> dic)
        {
            IQueryable<TeachingExperience> ListTeachingExperience = TeachingExperienceRepository.All;
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
            int ExperienceTypeID = Utils.GetInt(dic, "ExperienceTypeID");
            int RegisteredLevel = Utils.GetInt(dic, "RegisteredLevel");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            string FullName = Utils.GetString(dic, "FullName");
            string Grade = Utils.GetString(dic, "Grade");
            if (TeacherID != 0)
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.TeacherID == TeacherID);
            }
            if (SchoolFacultyID != 0)
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.FacultyID == SchoolFacultyID);
            }
            if (ExperienceTypeID != 0)
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.ExperienceTypeID == ExperienceTypeID);
            }
            if (RegisteredLevel != 0)
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.RegisteredLevel == RegisteredLevel);
            }
            if (AcademicYearID != 0)
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (FullName != null && !FullName.Equals(string.Empty))
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.Employee.FullName.ToLower().Contains(FullName.Trim().ToLower()));
            }
            if (Grade != null && !Grade.Equals(string.Empty))
            {
                ListTeachingExperience = ListTeachingExperience.Where(o => o.Grade.ToLower().Contains(Grade.Trim().ToLower()));
            }
            return ListTeachingExperience;
        }
        #endregion

    }
}