﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class HabitDetailBusiness
    {

        #region Search


        /// <summary>
        /// Tìm kiếm sở thích chi tiết của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<HabitDetail> Search(IDictionary<string, object> dic)
        {
            string HabitDetailName = Utils.GetString(dic, "HabitDetailName");
            int HabitGroupID = Utils.GetInt(dic, "HabitGroupID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<HabitDetail> lsHabitDetail = HabitDetailRepository.All;
            if (IsActive.HasValue)
            {
                lsHabitDetail = lsHabitDetail.Where(cfh => cfh.IsActive == IsActive);
            }
            if (HabitDetailName.Trim().Length != 0)
            {
                lsHabitDetail = lsHabitDetail.Where(cfh => cfh.HabitDetailName.Contains(HabitDetailName));
            }
            if (HabitGroupID != 0)
            {
                lsHabitDetail = lsHabitDetail.Where(cfh => cfh.HabitGroupID == HabitGroupID);
            }

            return lsHabitDetail;

        }
        #endregion
    }
}