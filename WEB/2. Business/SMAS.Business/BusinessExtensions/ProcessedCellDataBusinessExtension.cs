﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class ProcessedCellDataBusiness
    {

        public int? GetYear(int AcademicYearID)
        {
            AcademicYear academicYear = AcademicYearBusiness.All.Where(p => p.AcademicYearID == AcademicYearID).FirstOrDefault();
            return academicYear.Year;
        }

        public string GetProvinceName(int ProvinceID)
        {
            string ProvinceName = "";
            Province province = ProvinceBusiness.All.Where(p => p.ProvinceID == ProvinceID).FirstOrDefault();
            if (province != null)
            {
                ProvinceName = province.ProvinceName;
            }
            return ProvinceName;
        }

        public List<ProcessedCellData> GetListReportEmis(int SchoolID, int Year, string ReportCode)
        {
            int partition = UtilsBusiness.GetPartionId(SchoolID);
            //DateTime datetime = GetDateTimeOrderDescEMIS(SchoolID, Year, ReportCode);
            List<ProcessedCellData> listEmis = this.All.Where(p => p.SchoolID == SchoolID
                && p.Year == Year
                && p.ReportCode == ReportCode
                && p.LastDigitSchoolID == partition).ToList();
            return listEmis;
        }

        public List<ProcessedCellData> GetListReportEmisForSupervisingDept(int? ProvinceID, int? DistrictID, int Year, string ReportCode)
        {
            //Lay ra danh sach truong
            IQueryable<SchoolProfile> iqEr = (from sp in SchoolProfileBusiness.All
                                         join aca in AcademicYearBusiness.All on sp.SchoolProfileID equals aca.SchoolID
                                         where sp.IsActive
                                         && aca.IsActive.HasValue && aca.IsActive.Value == true
                                         && aca.Year == Year
                                         select sp);

            if (ProvinceID.HasValue)
            {
                iqEr = iqEr.Where(o => o.ProvinceID == ProvinceID.Value);
            }

            if (DistrictID.HasValue)
            {
                iqEr = iqEr.Where(o => o.DistrictID == DistrictID.Value);
            }

            List<SchoolProfile> lstSp = iqEr.ToList();

            List<int?> lstSchoolID = lstSp.Select(o => (int?)o.SchoolProfileID).ToList();
            //DateTime datetime = GetDateTimeOrderDescEMIS(SchoolID, Year, ReportCode);
            List<ProcessedCellData> listEmis = this.All.Where(p =>lstSchoolID.Contains( p.SchoolID )
                && p.Year == Year
                && p.ReportCode == ReportCode).ToList();
            return listEmis;
        }
        //Lấy ra ngày thống kê mới nhất để thông kê
        public DateTime GetDateTimeOrderDescEMIS(int SchoolID, int Year, string ReportCode)
        {
            DateTime datetime = new DateTime();
            ProcessedCellData processedCellData = this.All.Where(p => p.SchoolID == SchoolID
                && p.Year == Year
                && p.ReportCode == ReportCode
                ).OrderByDescending(p => p.ProcessedDate).FirstOrDefault();
            if (processedCellData != null)
            {
                datetime = processedCellData.ProcessedDate.Value;
            }
            return datetime;
        }
        public int GetEducationGrade(int schoolID)
        {
            int education = 0;
            SchoolProfile schoolProfile = SchoolProfileBusiness.All.Where(p => p.SchoolProfileID == schoolID).FirstOrDefault();
            if (schoolProfile != null)
            {
                education = schoolProfile.EducationGrade;
            }
            return education;
        }

        public ProcessedReport GetProcessedReport(ProcessedCellData entity)
        {
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(entity.ReportCode + "_" + entity.SchoolID, inputParameterHashKey);
        }

        public string GetHashKey(ProcessedCellData entity)
        {
            return ReportUtils.GetHashKey(new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"ReportCode",entity.ReportCode} ,
                {"DistrictID",entity.DistrictID},
                {"ProvinceID",entity.ProvinceID},
                {"Year",entity.Year}
            });
        }

        public ProcessedReport InsertProcessCellData(ProcessedCellData entity, Stream Data, string ReportCode, string CodeDefinition)
        {

            string reportCode = ReportCode + "_" + entity.SchoolID;// report code = schoolID + ten file excel
            
            //Khởi tạo đối tượng ProcessedReport:
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            if (Data != null)
            {
                pr.ReportData = ReportUtils.Compress(Data);
            }
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(ReportCode);
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID}
               // {"Year",entity.Year}
               // {"DistrictID",entity.DistrictID},
               // {"ProvinceID",entity.ProvinceID}
            };
            this.context.Configuration.AutoDetectChangesEnabled = true;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            ProcessedReport processReportObj = ProcessedReportBusiness.GetProcessedReport(reportCode, pr.InputParameterHashKey);
            if (processReportObj != null)
            {
                pr.ProcessedReportID = processReportObj.ProcessedReportID;
            }
            return pr;
        }

        public void InsertOrUpdateEmisRequest(int SchoolID, int AcademicYearID, int Year, int period, int trainingType, int districtID, int provinceID, string reportCode)
        {
            EmisRequest obj = (from a in EmisRequestBusiness.All
                               where a.SchoolID == SchoolID
                               && a.AcademicYear == AcademicYearID
                               && a.ReportCode == reportCode
                               select a).FirstOrDefault();
            if (obj != null)
            {
                //kiem tra status (chi update nhung truong co status  = 1, vi = 1 thi da tong hop roi, khi tong hop roi thi moi tong hop lai)
                if (obj.Status.Value == 1)
                {
                    obj.Status = 0;
                    obj.Period = period;
                    obj.TrainingTypeID = trainingType;
                    obj.DistrictID = districtID;
                    obj.ProvinceID = provinceID;
                    obj.Grade = GetEducationGrade(SchoolID);
                    obj.UpdateTime = DateTime.Now;
                    obj.ReportCode = reportCode;
                    //obj.ReportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS;
                    EmisRequestBusiness.Update(obj);
                    EmisRequestBusiness.Save();
                }
            }
            else
            {
                // insert dong moi
                EmisRequest objInsert = new EmisRequest();
                objInsert.SchoolID = SchoolID;
                objInsert.AcademicYear = AcademicYearID;
                objInsert.Year = Year;
                objInsert.Status = 0;// insert moi thi bang false
                objInsert.Grade = GetEducationGrade(SchoolID);
                objInsert.Period = period;
                objInsert.TrainingTypeID = trainingType;
                objInsert.DistrictID = districtID;
                objInsert.ProvinceID = provinceID;
                objInsert.CreateTime = DateTime.Now;
                objInsert.ReportCode = reportCode;
                //objInsert.ReportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS;
                EmisRequestBusiness.Insert(objInsert);
                EmisRequestBusiness.Save();
            }
        }

        public void InsertOrUpdateQD5363Request(int SchoolID, int AppliedLevel, int AcademicYearID, int Year, int period, int trainingType, int districtID, int provinceID, string reportCode)
        {
            EmisRequest obj = (from a in EmisRequestBusiness.All
                               where a.SchoolID == SchoolID
                               && a.AcademicYear == AcademicYearID
                               && a.ReportCode == reportCode
                               select a).FirstOrDefault();
            if (obj != null)
            {
                //kiem tra status (chi update nhung truong co status  = 1, vi = 1 thi da tong hop roi, khi tong hop roi thi moi tong hop lai)
                if (obj.Status.Value == 1)
                {
                    obj.Status = 0;
                    obj.Period = period;
                    obj.TrainingTypeID = trainingType;
                    obj.DistrictID = districtID;
                    obj.ProvinceID = provinceID;
                    obj.Grade = AppliedLevel;
                    obj.UpdateTime = DateTime.Now;
                    obj.ReportCode = reportCode;
                    //obj.ReportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS;
                    EmisRequestBusiness.Update(obj);
                    EmisRequestBusiness.Save();
                }
            }
            else
            {
                // insert dong moi
                EmisRequest objInsert = new EmisRequest();
                objInsert.SchoolID = SchoolID;
                objInsert.AcademicYear = AcademicYearID;
                objInsert.Year = Year;
                objInsert.Status = 0;// insert moi thi bang false
                objInsert.Grade = AppliedLevel;
                objInsert.Period = period;
                objInsert.TrainingTypeID = trainingType;
                objInsert.DistrictID = districtID;
                objInsert.ProvinceID = provinceID;
                objInsert.CreateTime = DateTime.Now;
                objInsert.ReportCode = reportCode;
                //objInsert.ReportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS;
                EmisRequestBusiness.Insert(objInsert);
                EmisRequestBusiness.Save();
            }
        }

        public void InsertOrUpdateQD5363ForSupervisingDeptRequest(int? ProvinceID, int? DistrictID, int AppliedLevel, int Year, int period, string reportCode)
        {
            //Lay cac request da co
            IQueryable<EmisRequestBO> iqEr = (from sp in SchoolProfileBusiness.All
                                         join aca in AcademicYearBusiness.All on sp.SchoolProfileID equals aca.SchoolID
                                         join er in EmisRequestBusiness.All.Where(o=>o.ReportCode == reportCode) on sp.SchoolProfileID equals er.SchoolID into des
                                         from x in des.DefaultIfEmpty()
                                         where  sp.IsActive
                                         && aca.IsActive.HasValue && aca.IsActive.Value == true
                                         && aca.Year == Year
                                        select new EmisRequestBO
                                        {
                                            EmisRequest = x,
                                            SchoolID=sp.SchoolProfileID,
                                            TrainingTypeID = sp.TrainingTypeID,
                                            DistrictID = sp.DistrictID,
                                            ProvinceID = sp.ProvinceID,
                                            AcademicYearID = aca.AcademicYearID
                                        });

            if (ProvinceID.HasValue)
            {
                iqEr = iqEr.Where(o => o.ProvinceID == ProvinceID.Value);
            }

            if (DistrictID.HasValue)
            {
                iqEr = iqEr.Where(o => o.DistrictID == DistrictID.Value);
            }

            List<EmisRequestBO> lstEr = iqEr.ToList();
            for (int i = 0; i < lstEr.Count; i++)
            {
                EmisRequestBO er = lstEr[i];
                if (er.EmisRequest != null)
                {
                    if (er.EmisRequest.Status.Value == 1)
                    {
                        er.EmisRequest.Status = 0;
                        er.EmisRequest.Period = period;
                        er.EmisRequest.TrainingTypeID = er.TrainingTypeID;
                        er.EmisRequest.DistrictID = er.DistrictID;
                        er.EmisRequest.ProvinceID = er.ProvinceID;
                        er.EmisRequest.Grade = AppliedLevel;
                        er.EmisRequest.UpdateTime = DateTime.Now;
                        er.EmisRequest.ReportCode = reportCode;
                        EmisRequestBusiness.Update(er.EmisRequest);
                    }
                }
                else
                {
                    // insert dong moi
                    EmisRequest objInsert = new EmisRequest();
                    objInsert.EmisRequestID = EmisRequestBusiness.GetNextSeq<int>("EMISREQUEST_SEQ");
                    objInsert.SchoolID = er.SchoolID;
                    objInsert.AcademicYear = er.AcademicYearID;
                    objInsert.Year = Year;
                    objInsert.Status = 0;
                    objInsert.Grade = AppliedLevel;
                    objInsert.Period = period;
                    objInsert.TrainingTypeID = er.TrainingTypeID;
                    objInsert.DistrictID = er.DistrictID;
                    objInsert.ProvinceID = er.ProvinceID;
                    objInsert.CreateTime = DateTime.Now;
                    objInsert.ReportCode = reportCode;
                    EmisRequestBusiness.Insert(objInsert);
                    
                }
            }
            EmisRequestBusiness.Save();
        }

        public int GetStatusRequest(int SchoolID, int AcademicYearID, string reportCode, int period, int appliedLevel = 0)
        {
            EmisRequest obj = (from a in EmisRequestBusiness.All
                               where a.SchoolID == SchoolID
                               && a.AcademicYear == AcademicYearID
                               && a.ReportCode == reportCode
                               && a.Period == period
                               && (a.Grade == appliedLevel || appliedLevel == 0)
                               select a).FirstOrDefault();
            if (obj != null)
            {
                return obj.Status.Value;
            }

            return -1;
        }

        public int GetStatusRequestForSupervisingDept(int? ProvinceID, int? DistrictID, int Year, string reportCode, int period, int appliedLevel)
        {
            //Lay cac request da co
            IQueryable<EmisRequestBO> iqEr = (from sp in SchoolProfileBusiness.All
                                         join aca in AcademicYearBusiness.All on sp.SchoolProfileID equals aca.SchoolID
                                         join er in EmisRequestBusiness.All.Where(o => o.ReportCode == reportCode && o.Grade == appliedLevel && o.AcademicYear != null) 
                                            on new { SchoolID = (int?)sp.SchoolProfileID, AcademicYearID = (int?)aca.AcademicYearID } equals new { SchoolID = er.SchoolID, AcademicYearID = er.AcademicYear } into des
                                         from x in des.DefaultIfEmpty()
                                         where  sp.IsActive
                                         && aca.IsActive.HasValue && aca.IsActive.Value == true
                                         && aca.Year == Year
                                         select new EmisRequestBO
                                         {
                                             EmisRequest = x,
                                             SchoolID = sp.SchoolProfileID,
                                             TrainingTypeID = sp.TrainingTypeID,
                                             DistrictID = sp.DistrictID,
                                             ProvinceID = sp.ProvinceID,
                                             AcademicYearID = aca.AcademicYearID
                                         });

            if (ProvinceID.HasValue)
            {
                iqEr = iqEr.Where(o => o.ProvinceID == ProvinceID.Value);
            }

            if (DistrictID.HasValue)
            {
                iqEr = iqEr.Where(o => o.DistrictID == DistrictID.Value);
            }

            List<EmisRequestBO> lstEr = iqEr.ToList();
            if (lstEr.Exists(o => o.EmisRequest == null) || lstEr.Exists(o=>o.EmisRequest!=null && o.EmisRequest.Status == 0))
            {
                return 0;
            }
            return 1;
        }

        public EmisRequest GetEmisRequest(int SchoolID, int AcademicYearID, string reportCode, int period)
        {
            EmisRequest obj = (from a in EmisRequestBusiness.All
                               where a.SchoolID == SchoolID
                               && a.AcademicYear == AcademicYearID
                               && a.ReportCode == reportCode
                               && a.Period == period
                               select a).FirstOrDefault();
            if (obj != null)
            {
                return obj;
            }
            return null;
        }

        public List<PhysicalFacilitiesBO> GetListDataFacilities(int schoolId, int year, int period, int provinceId, string reportCode)
        {
            //Lay thong tin bao cao da tong hop
            List<PhysicalFacilitiesBO> listProcessCellData = (from a in this.GetListReportEmis(schoolId, year, reportCode)
                                                              select new PhysicalFacilitiesBO
                                                              {
                                                                  CellReportLCU = a.CellAddress,
                                                                  CellValue = a.CellValue
                                                              }).ToList();
            List<int> lstSchoolId = new List<int>() { schoolId };
            //Lay du lieu cell tang giam so luong
            List<PhysicalFacilitiesBO> listUpDown = this.GetListDataUpDown(provinceId, period, lstSchoolId, year);
            listProcessCellData.AddRange(listUpDown);
            return listProcessCellData;
        }

        private List<PhysicalFacilitiesBO> GetListDataUpDown(int provinceId, int period, List<int> lstSchoolId, int year)
        {
            //Danh sach indicator va dia chi cac cua cac cell Tang Giam so luong
            List<Indicator> listIndiCatorCaled = IndicatorBusiness.All.Where(p => !string.IsNullOrEmpty(p.CellCalPrePeriod)).ToList();
            List<string> listIndicatorCodeCaled = listIndiCatorCaled.Select(p => p.CellCalPrePeriod).ToList();
            int prePeriod = IndicatorDataBusiness.GetPreviosPeriod(period); // id ky truoc
            //Lay danh sach indicator cac cell Tang Giam so luong va du lieu ki hien tai
            List<PhysicalFacilitiesBO> listDataCurrent = IndicatorDataBusiness.GetDataByPeriod(provinceId, period, lstSchoolId, year, listIndicatorCodeCaled);

            //Danh sach du lieu cua ky lien truoc
            int yearID = year;
            List<PhysicalFacilitiesBO> listResult = new List<PhysicalFacilitiesBO>();
            PhysicalFacilitiesBO objResult = null;
            if (period == SystemParamsInFile.FIRST_PERIOD)
            {
                yearID = year - 1;
            }
            List<PhysicalFacilitiesBO> listDataPrePeriod = IndicatorDataBusiness.GetDataByPeriod(provinceId, prePeriod, lstSchoolId, yearID, listIndicatorCodeCaled);
            // Fix lỗi ATTT
            // if (listDataPrePeriod == null && listDataPrePeriod.Count == 0)
            if (listDataPrePeriod == null || !listDataPrePeriod.Any())
            {
                return listDataCurrent;
            }
            else
            {
                //Xu ly thong tin, lay cot tong so ky hien tai tru tong so ky lien truoc
                int valueCurrent = 0;
                int valuePre = 0;
                Indicator objDataTmp = null;
                PhysicalFacilitiesBO objTmp = null;
                for (int i = 0; i < listIndiCatorCaled.Count; i++)
                {
                    objDataTmp = listIndiCatorCaled[i];
                    objResult = new PhysicalFacilitiesBO();
                    valueCurrent = 0;
                    valuePre = 0;
                    //lay du lieu ky hien tai cua ky indicatorId
                    objTmp = listDataCurrent.Where(p => p.IndicatorCode == objDataTmp.CellCalPrePeriod).FirstOrDefault();
                    if (objTmp != null)
                    {
                        valueCurrent = !string.IsNullOrEmpty(objTmp.CellValue) ? Int32.Parse(objTmp.CellValue) : 0;
                    }

                    //lay du lieu cua ky lien truoc
                    objTmp = listDataPrePeriod.Where(p => p.IndicatorCode == objDataTmp.CellCalPrePeriod).FirstOrDefault();
                    if (objTmp != null)
                    {
                        valuePre = !string.IsNullOrEmpty(objTmp.CellValue) ? Int32.Parse(objTmp.CellValue) : 0;
                    }

                    if (valueCurrent > 0 || valuePre > 0)
                    {
                        objResult.CellReportLCU = objDataTmp.CellReportLCU;
                        objResult.CellValue = (valueCurrent - valuePre).ToString();
                        listResult.Add(objResult);
                    }
                }
            }
            return listResult;
        }
        public void RequestGraduationReport(int SchoolID, int AcademicYearID, string ReportCode)
        {
            EmisRequest obj = (from a in EmisRequestBusiness.All
                               where a.SchoolID == SchoolID
                               && a.AcademicYear == AcademicYearID
                               && a.ReportCode == ReportCode
                               select a).FirstOrDefault();
            if (obj != null)
            {
                //kiem tra status (chi update nhung truong co status  = 1, vi = 1 thi da tong hop roi, khi tong hop roi thi moi tong hop lai)
                if (obj.Status.Value == 1)
                {
                    obj.Status = 0;
                    obj.Grade = GetEducationGrade(SchoolID);
                    obj.UpdateTime = DateTime.Now;
                    obj.ReportCode = ReportCode;
                    EmisRequestBusiness.Update(obj);
                    EmisRequestBusiness.Save();
                }
            }
            else
            {
                // insert dong moi
                EmisRequest objInsert = new EmisRequest();
                objInsert.SchoolID = SchoolID;
                objInsert.AcademicYear = AcademicYearID;
                objInsert.Status = 0;// insert moi thi bang false
                objInsert.Grade = GetEducationGrade(SchoolID);
                objInsert.CreateTime = DateTime.Now;
                objInsert.ReportCode = ReportCode;
                //objInsert.ReportCode = SystemParamsInFile.EMIS_REQUEST_REPORT_CODE_EMIS;
                EmisRequestBusiness.Insert(objInsert);
                EmisRequestBusiness.Save();
            }
        }

        public int GetStatusGraduationReport(int SchoolID, int AcademicYearID, string reportCode)
        {
            EmisRequest obj = (from a in EmisRequestBusiness.All
                               where a.SchoolID == SchoolID
                               && a.AcademicYear == AcademicYearID
                               && a.ReportCode == reportCode
                               select a).FirstOrDefault();
            if (obj != null)
            {
                return obj.Status.Value;
            }
            return -1;
        }

        public IDictionary<string, object> GetListReportEmisAsDic(int SchoolID, int Year, string ReportCode, string SheetName)
        {
            int partition = UtilsBusiness.GetPartionId(SchoolID);
            //DateTime datetime = GetDateTimeOrderDescEMIS(SchoolID, Year, ReportCode);
            Dictionary<string, object> listEmis = this.All.Where(p => p.SchoolID == SchoolID
                && p.Year == Year
                && p.ReportCode == ReportCode
                && p.SheetName == SheetName
                && p.LastDigitSchoolID == partition).GroupBy(p => p.CellAddress).ToDictionary(g => g.Key, g => (object)g.First().CellValue);
            return listEmis;
        }
    }
}