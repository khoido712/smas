﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Data.Objects.SqlClient;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Models.Models.CustomModels;

namespace SMAS.Business.Business
{
    public partial class CandidateBusiness
    {
        public void InsertList(List<Candidate> ListCandidate)
        {
            this.context.Configuration.AutoDetectChangesEnabled = false;
            foreach (Candidate can in ListCandidate)
            {
                CandidateBusiness.Insert(can);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
        }

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin thí sinh tham gia kỳ thi và phân công phòng thi
        /// </summary>
        /// <author>hath</author>
        /// <date>7/11/2012</date>
        /// <param name="SearchInfo">Thông tin tìm kiếm</param>
        /// <returns>IQueryable<AcademicYear></returns>
        private IQueryable<Candidate> Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<Candidate> lsCandidate = CandidateRepository.All;

            //CandidateID : default = 0;0   All
            int CandidateID = Utils.GetInt(SearchInfo, "CandidateID");
            //ExaminationID: default =0;0   All
            int ExaminationID = Utils.GetInt(SearchInfo, "ExaminationID");
            //ExaminationSubjectID: default = 0; 0   All
            int ExaminationSubjectID = Utils.GetInt(SearchInfo, "ExaminationSubjectID");
            //RoomID: default = 0; 0   All
            int? RoomID = Utils.GetNullInt(SearchInfo, "RoomID");
            //Khối thi
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            //AppliedLevel: default =0; 0 All; tìm kiếm theo EducationLevel(EducationLevelID).Grade
            int AppliedLevel = Utils.GetByte(SearchInfo, "AppliedLevel");
            //ClassID: default = 0; 0   All
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            //PupilID: default = 0; 0 All
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            //PupilCode: default = ""; ""  All
            string PupilCode = Utils.GetString(SearchInfo, "PupilCode");
            //PupilName: default = ""; ""  All
            string PupilName = Utils.GetString(SearchInfo, "PupilName");
            //PupilName: default = ""; ""  All
            string FullName = Utils.GetString(SearchInfo, "FullName");
            //Genre: default = -1; -1  All ~ tìm kiếm theo PupilProfile(PupilID).Genre
            int Genre = Utils.GetInt(SearchInfo, "Genre", -1);
            //OrderNumber: default = 0; 0   All
            int OrderNumber = Utils.GetInt(SearchInfo, "OrderNumber");
            //NamedListCode: default = ""; ""  -> All
            string NamedListCode = Utils.GetString(SearchInfo, "NamedListCode");
            //Mark: default = -1; -1   All
            int Mark = Utils.GetInt(SearchInfo, "Mark", -1);
            //Judgement: default = ""; ""   All
            string Judgement = Utils.GetString(SearchInfo, "Judgement");
            //HasReason: default =null;null All
            bool? HasReason = Utils.GetNullableBool(SearchInfo, "HasReason");
            //ReasonDetail: default = ""; ""  All
            string ReasonDetail = Utils.GetString(SearchInfo, "ReasonDetail");
            //IsAbsence: default =null;null All. Nếu IsAbsence=TRUE tìm kiếm theo HasReason != NULL; IsAbsence= FALSE tìm kiếm theo HasReason == NULL
            bool? IsAbsence = Utils.GetNullableBool(SearchInfo, "IsAbsence");

            //Khối học tương ứng với lớp
            int EducationLevelID1 = Utils.GetInt(SearchInfo, "EducationLevelID1");

            if (ClassID != 0)
            {
                lsCandidate = from c in lsCandidate
                              join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                              where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                              && c.ClassID == ClassID
                              select c;
            }
            if (CandidateID != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.CandidateID == CandidateID);
            }

            if (ExaminationID != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.ExaminationID == ExaminationID);
            }

            if (ExaminationSubjectID != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.SubjectID == ExaminationSubjectID);
            }


            if (RoomID.HasValue)
            {
                if (RoomID != 0)
                {
                    lsCandidate = lsCandidate.Where(o => o.RoomID == RoomID.Value);
                }
            }
            else
            {
                lsCandidate = lsCandidate.Where(o => !o.RoomID.HasValue);
            }


            if (EducationLevelID != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.EducationLevelID == EducationLevelID);
            }

            if (EducationLevelID1 != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID1);
            }

            if (AppliedLevel != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.EducationLevel.Grade == AppliedLevel);
            }

            if (PupilID != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.PupilID == PupilID);
            }

            if (PupilCode.Length > 0)
            {
                lsCandidate = lsCandidate.Where(o => o.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            }

            if (PupilName.Length > 0)
            {
                lsCandidate = lsCandidate.Where(o => o.PupilProfile.FullName.ToLower().Contains(PupilName.ToLower()));
            }

            if (FullName.Length > 0)
            {
                lsCandidate = lsCandidate.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }

            if (Genre != -1)
            {
                lsCandidate = lsCandidate.Where(o => o.PupilProfile.Genre == (int)Genre);
            }

            if (OrderNumber != 0)
            {
                lsCandidate = lsCandidate.Where(o => o.OrderNumber == OrderNumber);
            }

            if (NamedListCode.Length > 0)
            {
                lsCandidate = lsCandidate.Where(o => o.NamedListCode.ToLower().Contains(NamedListCode.ToLower()));
            }

            if (Mark != -1)
            {
                lsCandidate = lsCandidate.Where(o => o.Mark == Mark);
            }
            if (IsAbsence.HasValue)
            {
                if (IsAbsence == true)
                {
                    lsCandidate = lsCandidate.Where(o => o.HasReason.HasValue);
                }
                else
                {
                    lsCandidate = lsCandidate.Where(o => o.HasReason == null);
                }
            }


            return lsCandidate;

        }
        /// <summary>
        /// Searches the by school.
        /// </summary>
        /// <author>hath</author>
        /// <date>8/11/2012</date>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>IQueryable{Candidate}.</returns>
        public IQueryable<Candidate> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {

            if (SchoolID != 0)
            {
                SearchInfo["SchoolID"] = SchoolID;

            }
            return Search(SearchInfo);
        }
        #endregion

        /// <summary>
        /// Over load ham cu
        /// QuangLM
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ExaminationID"></param>
        /// <param name="EducationLevelID">Khoi thi</param>
        /// <param name="ExaminationSubjectID"></param>
        /// <param name="ListPupilOfClass"></param>
        public void InsertGroupCandidate(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID
            , int ExaminationSubjectID, IQueryable<PupilOfClass> listPoc)
        {
            #region Validate

            Examination Exam = ExaminationBusiness.Find(ExaminationID);
            if (Exam == null) throw new BusinessException("Kỳ thi không tồn tại");

            // Kiem tra ma truong
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolID");

            if (listPoc == null || listPoc.Count() == 0)
            {
                throw new BusinessException("Common_Validate_NotSelectPupil");
            }

                // Trạng thái học sinh
                if (listPoc.Any(o => o.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
            
            // Kiem tra năm học của kỳ thi
            if (AcademicYearBusiness.IsCurrentYear(Exam.AcademicYearID) == false) // Năm học không phải năm hiện tại
                {
                throw new BusinessException("Examination_Label_NotCurrentAcademicYear");
                }
            if (listPoc.Any(o => o.AcademicYearID != Exam.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiem tra ky thi da lap danh sach mon thi hay chua
            if (!ExaminationBusiness.HasExaminationSubject(ExaminationID))
            {
                throw new BusinessException("Common_Candidate_ExaminationSubject");
                }

            List<ExaminationSubject> listExaminationSubject = new List<ExaminationSubject>(); // Danh sach cac mon thi
            if (Exam.UsingSeparateList == 0) // Ky thi co cac mon thi co danh sach hoc sinh giong nhau
                {
                // Them cac mon thi cua khoi nay vao danh sach
                listExaminationSubject.AddRange(Exam.ExaminationSubjects.Where(es=>es.EducationLevelID == EducationLevelID));
            }
            else if (Exam.UsingSeparateList == 1) // Ky thi co cac mon thi co danh sach thi sinh khac nhau
            {
                ExaminationSubject examinationSubject = Exam.ExaminationSubjects.FirstOrDefault(e =>e.ExaminationSubjectID == ExaminationSubjectID);
                if (examinationSubject == null)
                {
                    List<object> listParam = new List<object>();
                    listParam.Add("Candidate_Label_ExaminationSubjectID");
                    throw new BusinessException("Common_Validate_NotAvailable", listParam);
                }
                if (examinationSubject.ExaminationID != ExaminationID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận giá trị < EXAMINATION_STAGE_MARK_COMPLETED (5)
                Utils.ValidateCompareNumber(Exam.CurrentStage, SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED, "Candidate_Label_CurrentStage", "");

                // Them mon thi vao danh sach
                listExaminationSubject.Add(examinationSubject);
            }
            #endregion

            #region Them thi sinh vao ky thi
            var lstExaminationSubjectID = listExaminationSubject.Select(es => es.SubjectID).ToList();
            // Lay danh sach thi sinh da lap danh sach
            var lstCandidateInDB = this.repository.All.Where(c => c.ExaminationID == ExaminationID && lstExaminationSubjectID.Contains(c.SubjectID??0)).ToList();
            // Lay san cac thong tin hoc sinh tu DB
            var pupilProfiles = listPoc.Select(poc => new { poc.PupilID, poc.ClassID, poc.PupilProfile.PupilCode }).ToList();
            foreach (var es in listExaminationSubject)
            {
                // Danh sach cac hoc sinh da them trong danh sach thi sinh cua mon dang xet
                var dathem = lstCandidateInDB.Where(c => c.SubjectID == es.SubjectID).Select(c => c.PupilID);
                // Danh sach cac hoc sinh chua them vao danh sach thi sinh cua mon dang xet
                var chuathem = pupilProfiles.Where(poc => !dathem.Contains(poc.PupilID));
                foreach (var pupilProfile in chuathem)
                {
                    Candidate ca = new Candidate();
                    ca.ExaminationID = ExaminationID;
                    ca.PupilID = pupilProfile.PupilID;
                    ca.PupilCode = pupilProfile.PupilCode;
                    ca.ClassID = pupilProfile.ClassID;
                    ca.EducationLevelID = EducationLevelID;
                    ca.SubjectID = es.ExaminationSubjectID;
                    base.Insert(ca);
                }
            }
            #endregion
        }

        #region NameListCode
        /// <summary>
        /// Names the list code.
        /// </summary>
        /// <author>hath</author>
        /// <date>8/11/2012</date>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="ExaminationID">The examination ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ExaminationSubjectID">The examination subject ID.</param>
        /// <param name="Prefix">The prefix.</param>
        /// <param name="CodeLength">Length of the code.</param>
        /// <param name="OrderByClass">if set to <c>true</c> [order by class].</param>
        /// <param name="OrderByName">if set to <c>true</c> [order by name].</param>
        public void NameListCode(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, int ExaminationSubjectID, string Prefix, int CodeLength, bool OrderByClass, bool OrderByName)
        {
            #region Validate
            //validate ma so bao danh
            if (Prefix != "")
            {
                Utils.ValidateNumbericAndLetters(Prefix, "Candidate_Label_Prefix");
            }

            Examination Exam = ExaminationBusiness.Find(ExaminationID);
            //SchoolID: PK(SchoolProfile)
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolID");
            //ExaminationID: PK(Examination)
            ExaminationBusiness.CheckAvailable(ExaminationID, "Candidate_Label_ExaminationID");
            //SchoolID, ExaminationID: not compatible(Examination)
            IDictionary<string, object> SearchInfo1 = new Dictionary<string, object>();
            SearchInfo1["SchoolID"] = SchoolID;
            SearchInfo1["ExaminationID"] = ExaminationID;
            bool compatible1 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo1);
            if (!compatible1)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AppliedLevel(Grade), EducationLevelID: not compatible(EducationLevel)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["Grade"] = AppliedLevel;
            SearchInfo2["EducationLevelID"] = EducationLevelID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Prefix: maxlength(5), chỉ chứa các ký tự (a-z, A-Z, 0-9)
            Utils.ValidateMaxLength(Prefix, 5, "Candidate_Label_Prefix");

            //CodeLength: range(0-9)

            Utils.ValidateRange(CodeLength, 0, 9, "Candidate_Label_CodeLength");
            //- Nếu Examination(ExaminationID).UsingSeparateList = 1:
            //   + ExaminationSubjectID: PK(ExaminationSubject)
            //   + ExaminationSubjectID, ExaminationID: not compatible(ExaminationSubject)
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận giá trị < EXAMINATION_STAGE_MARK_COMPLETED (5)

            if (Exam.UsingSeparateList == 1)
            {
                ExaminationSubjectBusiness.CheckAvailable(ExaminationSubjectID, "Candidate_Label_ExaminationSubjectID");
                IDictionary<string, object> SearchInfo4 = new Dictionary<string, object>();
                SearchInfo4["ExaminationSubjectID"] = ExaminationSubjectID;
                SearchInfo4["ExaminationID"] = ExaminationID;
                bool compatible4 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "ExaminationSubject", SearchInfo4);
                if (!compatible4)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                if (AcademicYearBusiness.IsCurrentYear(Exam.AcademicYearID) == false)
                {

                    throw new BusinessException("Examination_Label_NotCurrentAcademicYear");
                }
                //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận giá trị < EXAMINATION_STAGE_MARK_COMPLETED (5)
                Utils.ValidateCompareNumber(Exam.CurrentStage, SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED, "Candidate_Label_CurrentStage", "");

            }


            //Lấy danh sách thí sính ListPupil trong nhóm thi cần đánh số báo danh:
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["ExaminationID"] = ExaminationID;
            Dictionary["EducationLevelID"] = EducationLevelID;
            if (Exam != null)
            {
                if (Exam.UsingSeparateList == 1)
                {
                    Dictionary["ExaminationSubjectID"] = ExaminationSubjectID;
                }
            }
            IQueryable<Candidate> iqCandidate = SearchBySchool(SchoolID, Dictionary);
            int countPupil = iqCandidate.Select(o => o.PupilID).Distinct().Count();

            //Select distinct PupilProfile(PupilID) from SearchBySchool(SchoolID, Dictionnary)
            // + Dictionnary[“ExaminationID”] =  ExaminationID
            // + Dictionnary[“EducationLevelID”] =  EducationLevelID
            // + Dictionnary[“ExaminationSubjectID”] =  ExaminationSubjectID (Thêm điều kiện này khi Examination(ExaminationID).UsingSeparateList = 1)

            //Nếu ListPupil.Size().ToString(),Length() > Prefix => Lỗi: “Chiều dài dãy số không đủ để đánh SBD cho [n] thí sinh cần đánh SBD”
            if (CodeLength != 0)
            {
                if (countPupil.ToString().Length > CodeLength)
                {
                    throw new BusinessException("Common_Validate_Prefix", new List<object>() { countPupil });
                }
            }
            //Nếu ListPupil.Size() = 0 => “Môn thi chưa có thí sinh”
            if (countPupil == 0)
            {
                throw new BusinessException("Common_Candidate_Pupil");
            }
            #endregion
            this.context.Configuration.AutoDetectChangesEnabled = false;
            var ListCandidate2Order = (from c in iqCandidate
                                       join pp in PupilProfileBusiness.All on c.PupilID equals pp.PupilProfileID
                                       select new CandidateNameListBO
                                       {
                                           Candidate = c,
                                           PupilProfileID = pp.PupilProfileID,
                                           Name = pp.Name,
                                           Birthdate = pp.BirthDate,
                                           FullName = pp.FullName,
                                           EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : string.Empty,
                                           ClassID = pp.CurrentClassID,
                                           ClassName = c.ClassProfile != null ? c.ClassProfile.DisplayName : null,
                                           EducationLevel = c.ClassProfile != null ? c.ClassProfile.EducationLevelID : -1,
                                           OrderOfClassInEducationLevel =  c.ClassProfile != null ? c.ClassProfile.OrderNumber : null
                                       }).ToList();

            List<CandidateNameListBO> listOnlyPupil = ListCandidate2Order.Select(pp => new CandidateNameListBO
            {
                ClassID = pp.ClassID,
                PupilProfileID = pp.PupilProfileID,
                Name = pp.Name,
                FullName = pp.FullName,
                Birthdate = pp.Birthdate,
                EthnicCode = pp.EthnicCode
            }).Distinct(new CandidateNameListBO()).ToList();

            List<Candidate> listCandidate = ListCandidate2Order.Select(o => o.Candidate).ToList();
            //Lấy số báo danh của học sinh trong ListPupil: 
            //SBD= Prefix + [Số thứ tự của thí sinh trong ListPupil đã chuyển theo chiều dài đánh số]. Ví dụ: Prefix = A, CodeLength  = 5, STT của thí sinh là 21, SBD sẽ là: A00021; Nếu CodeLength  = 0, SBD sẽ là: A21
            //STTBD = Số thứ tự của thí sinh trong ListPupil 
            //if (Prefix != "")
            //{
            //    CodeLength = int.Parse(ListPupil.Count.ToString().Length.ToString());
            //}
            if (OrderByClass)
            {
                listOnlyPupil.Sort(CandidateNameListBO.CompareInClass);
            }
            else
            {
                listOnlyPupil.Sort(CandidateNameListBO.CompareInName);
            }
            List<int> listPupilID = listOnlyPupil.Select(o => o.PupilProfileID).Distinct().ToList();
            Dictionary<int, List<Candidate>> dicCandidate = new Dictionary<int, List<Candidate>>();

            // Dua vao dictionary
            foreach (Candidate c in listCandidate)
            {
                if (dicCandidate.ContainsKey(c.PupilID))
                {
                    List<Candidate> tempCandidate = dicCandidate[c.PupilID];
                    tempCandidate.Add(c);
                    dicCandidate[c.PupilID] = tempCandidate;
                }
                else
                {
                    dicCandidate[c.PupilID] = new List<Candidate>() { c };
                }
            }

            countPupil = listPupilID.Count;
            for (int i = 1; i <= countPupil; i++)
            {
                string SBD = Prefix + Utils.GetSBD(i, CodeLength);
                List<Candidate> listCandidateByPupil = dicCandidate[listPupilID[i - 1]];
                foreach (Candidate ca in listCandidateByPupil)
                {
                    // Cap nhat SBD
                    ca.NamedListCode = SBD;
                    ca.NamedListNumber = i;
                    this.Update(ca);
                }
            }

            //Với mỗi học sinh (PupilID), cập nhật vào bảng Candidate trường NamedListCode = SBD, NamedListNumber= STTBD cho tất cả các bản ghi: Select * from ListCandidate lc where lc.PupilID = PupilID
            //Lấy danh sách môn thi select distinct ExaminationSubject(SubjectID) from ListCandidate
            //Cập nhật lại ExaminationSubject.OrderNumberPrefix = Prefix
            List<int> ListExaminationSubjectID = iqCandidate.Where(o => o.SubjectID.HasValue).Select(o => o.SubjectID.Value).Distinct().ToList();
            List<ExaminationSubject> ListExaminationSubject = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == ExaminationID
                && ListExaminationSubjectID.Contains(o.ExaminationSubjectID))
                .ToList();

            if (ListExaminationSubject.Count() > 0)
            {
                foreach (ExaminationSubject es in ListExaminationSubject)
                {
                    es.OrderNumberPrefix = Prefix;
                    ExaminationSubjectBusiness.Update(es);
                }
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
        }
        #endregion

        #region UpdateRoom
        /// <summary>
        /// Updates the room.
        /// </summary>
        /// <author>hath</author>
        /// <date>8/11/2012</date>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="CandidateID">The candidate ID.</param>
        /// <param name="RoomID">The room ID.</param>
        public void UpdateRoom(int SchoolID, int AppliedLevel, int CandidateID, int RoomID)
        {

            Candidate ca = CandidateRepository.Find(CandidateID);
            Examination Examination = ExaminationBusiness.Find(ca.ExaminationID);
            ExaminationRoom ExaminationRoom = new ExaminationRoom();
            #region Validate
            //SchoolID: PK(SchoolProfile)
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolID");
            //CandidateID: PK(Candidate)
            this.CheckAvailable(CandidateID, "Candidate_Label_CandidateID");
            //RoomID:PK(ExaminationRoom)
            ExaminationRoomBusiness.CheckAvailable(RoomID, "ExaminationRoomBusiness_Label_Room");


            ////SchoolID, CandidateID: not compatible(Candidate)
            //IDictionary<string, object> SearchInfo6 = new Dictionary<string, object>();
            //SearchInfo6["SchoolID"] = SchoolID;
            //SearchInfo6["CandidateID"] = CandidateID;
            //bool compatible6 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Candidate", SearchInfo6);
            //if (!compatible6)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(ca.Examination.AcademicYearID))
            {
                throw new BusinessException("Common_IsCurrentYear_Err");
            }


            //RoomID(ExaminationRoomID), ExaminationSubjectID: not compatible(ExaminationRoom)
            if (RoomID != 0)
            {
                ExaminationRoom = ExaminationRoomBusiness.Find(RoomID);
                IDictionary<string, object> SearchInfo1 = new Dictionary<string, object>();
                SearchInfo1["ExaminationSubjectID"] = ca.ExaminationSubject.ExaminationSubjectID;
                SearchInfo1["ExaminationRoomID"] = RoomID;
                bool compatible1 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "ExaminationRoom", SearchInfo1);
                if (!compatible1)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //Phòng thi đã hết chỗ - Kiểm tra số lượng bản ghi trong Candidate  c với c.RoomID = RoomID với ExaminationRoom(RoomID).NumberOfSeat
                IQueryable<Candidate> lsCa = CandidateBusiness.All.Where(o => o.RoomID == RoomID);
                if (lsCa.Count() > ExaminationRoom.NumberOfSeat)
                {
                    throw new BusinessException("Candidate_Label_OutOfSeat");
                }

            }
            //AppliedLevel(Grade), EducationLevelID: not compatible(EducationLevel)
            IDictionary<string, object> SearchInfo3 = new Dictionary<string, object>();
            SearchInfo3["Grade"] = ca.EducationLevel.Grade;
            SearchInfo3["EducationLevelID"] = ca.EducationLevelID;
            bool compatible3 = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel", SearchInfo3);
            if (!compatible3)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            #endregion

            if (ca != null)
            {
                //Nếu Examination(ExaminationID).UsingSeparateList = 1:
                //  + Cập nhật lại RoomID cho bản ghi tương ứng CandidateID trong bảng Candidate

                //Examination(ExaminationID).CurrentStage (lấy trong CSDL) chỉ được nhận <  EXAMINATION_STAGE_MARK_COMPLETED (5)
                //TODO: hath8
                if (Examination.CurrentStage >= SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
                {
                    throw new BusinessException("Common_Validate_CurrentStage");
                }
                if (Examination != null)
                {

                    if (Examination.UsingSeparateList == 1)
                    {
                        if (RoomID == 0)
                        {
                            ca.RoomID = null;
                            this.Update(ca);
                            int CountPupil = CandidateRepository.All.Where(o => o.RoomID == RoomID).Count();
                        }
                        else
                        {
                            ca.RoomID = RoomID;
                            this.Update(ca);
                        }
                    }
                    //Nếu Examination(ExaminationID).UsingSeparateList = 0:
                    //  + Lấy danh sách thông tin thí sinh (cùng thí sinh trên nhưng ở các môn thi khác nhau) 
                    //ListCandidate: CandidateBusiness.SearchBySchool(SchoolID, Dictionary) ListCandidate
                    //       - Dictionary[“ExaminationID”] = ExaminationID
                    //       - Dictionary[“EducationLevelID”] = EducationLevelID
                    //      - Dictionary[“PupilID”] = PupilID
                    //   + Từ RoomID => ExaminationRoom(RoomID).RoomTitle
                    //   + Lấy cặp Candidate và RoomID để update: select c.Candidate, er.ExaminationRoomID 
                    //from ListCandidate c join ExaminationRoom er on c.SubjectID =  er.ExaminationSubjectID 
                    //where er.RoomTitle = ExaminationRoom(RoomID).RoomTitle
                    if (Examination.UsingSeparateList == 0)
                    {
                        IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                        Dictionary["ExaminationID"] = ca.ExaminationID;
                        Dictionary["EducationLevelID"] = ca.EducationLevelID;
                        Dictionary["PupilID"] = ca.PupilID;
                        IQueryable<Candidate> LsCandidate = SearchBySchool(SchoolID, Dictionary);
                        List<Candidate> ListCandidate = new List<Candidate>();
                        if (LsCandidate.Count() > 0)
                        {
                            ListCandidate = LsCandidate.ToList();
                        }

                        IQueryable<ExaminationRoom> LsRoom = ExaminationRoomBusiness.All.Where(o => o.ExaminationID == ca.ExaminationID);
                        if (RoomID != 0)
                        {
                            ExaminationRoom room = ExaminationRoomBusiness.Find(RoomID);
                            LsRoom = LsRoom.Where(o => o.RoomTitle == room.RoomTitle);
                        }
                        List<ExaminationRoom> ListRoom = LsRoom.ToList();

                        if (ListCandidate.Count > 0 && ListRoom.Count > 0)
                        {

                            foreach (ExaminationRoom Exam in ListRoom)
                            {
                                foreach (Candidate Candidate in ListCandidate)
                                {
                                    if (Exam.ExaminationSubjectID == Candidate.SubjectID)
                                    {
                                        if (RoomID == 0)
                                        {
                                            ca.RoomID = null;
                                            this.Update(ca);
                                        }
                                        else
                                        {
                                            Candidate.RoomID = Exam.ExaminationRoomID;
                                            this.Update(Candidate);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified school ID.
        /// </summary>
        /// <author>hath</author>
        /// <date>8/11/2012</date>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="CandidateID">The candidate ID.</param>
        public void Delete(int SchoolID, int ExaminationID, int AppliedLevel, List<int> listCandidateID)
        {
            if (listCandidateID == null || listCandidateID.Count == 0)
            {
                return;
            }
            //Lấy danh sách thí sinh cần xoá ListCandidateID:
            List<int> listCandidateIDDel = new List<int>();
            Examination Exam = ExaminationBusiness.Find(ExaminationID);
            IDictionary<string, object> Dictinonary = new Dictionary<string, object>();
            Dictinonary["ExaminationID"] = ExaminationID;
            Dictinonary["AcademicYearID"] = Exam.AcademicYearID;
            List<int> listPupilID = this.SearchBySchool(SchoolID, Dictinonary).Where(o => listCandidateID.Contains(o.CandidateID))
                .Select(o => o.PupilID).ToList();
            if (Exam != null)
            {
                if (Exam.UsingSeparateList != 0)
                {
                    listCandidateIDDel.AddRange(listCandidateID);
                }
                else
                {
                    listCandidateIDDel = SearchBySchool(SchoolID, Dictinonary)
                        .Where(o => listPupilID.Contains(o.PupilID))
                        .Select(o => o.CandidateID).Distinct().ToList();
                }

                if (listCandidateIDDel != null && listCandidateIDDel.Count > 0)
                {
                    this.context.Configuration.AutoDetectChangesEnabled = false;
                    // Xoa thong tin phach
                    List<DetachableHeadMapping> lsDeta = DetachableHeadMappingBusiness.All.Where(o => listCandidateIDDel.Contains(o.CandidateID)).ToList();
                    if (lsDeta.Count() > 0)
                    {
                        DetachableHeadMappingBusiness.DeleteAll(lsDeta);
                    }
                    // Xoa thong tin vi pham
                    List<ViolatedCandidate> lsViola = ViolatedCandidateBusiness.All.Where(o => listCandidateIDDel.Contains(o.CandidateID)).ToList();
                    if (lsViola.Count() > 0)
                    {
                        ViolatedCandidateBusiness.DeleteAll(lsViola);
                    }
                    List<Candidate> listCandidateDel = new List<Candidate>();
                    foreach (int candidateID in listCandidateIDDel)
                    {
                        listCandidateDel.Add(new Candidate() { CandidateID = candidateID });
                    }
                    this.DeleteAll(listCandidateDel);
                    this.context.Configuration.AutoDetectChangesEnabled = true;
                }
            }

        }
        #endregion

        #region Update Mark
        /// <summary>
        /// Updates the mark.
        /// </summary>
        /// <author>hath</author>
        /// <date>8/11/2012</date>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="CandidateID">The candidate ID.</param>
        /// <param name="ExamMark">The exam mark.</param>
        public void UpdateMark(int SchoolID, int AppliedLevel, int CandidateID, string ExamMark, int IsCommenting)
        {
            //Thực hiện Update vào bảng Candidate :
            //- Nếu ExaminationSubjectBusiness.GetSubjectMarkType(ExaminationSubjectID) 
            //    = 0 -> Update ExamMark vào trường Mark 
            //- Nếu ExaminationSubjectBusiness.GetSubjectMarkType(ExaminationSubjectID) 
            //    = 1 -> Update ExamMark vào trường Judgement
            Candidate ca = CandidateRepository.Find(CandidateID);
            if (ca != null)
            {
                //ExaminationSubject es = ca.ExaminationSubject;

                if (IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                {

                    if (ExamMark != "" && ExamMark != null)
                    {

                        ca.Mark = decimal.Parse(ExamMark);
                        this.Update(ca);
                    }
                    else
                    {
                        ca.Mark = null;
                        this.Update(ca);
                    }
                }
                else
                {
                    ca.Judgement = ExamMark;
                    this.Update(ca);
                }
            }

        }
        public void UpdateList(List<Candidate> ListCandidate)
        {
            this.context.Configuration.AutoDetectChangesEnabled = false;
            foreach (Candidate can in ListCandidate)
            {
                CandidateBusiness.Update(can);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
        }
        #endregion

        #region DeleteMark
        /// <summary>
        /// Deletes the mark.
        /// </summary>
        /// <author>hath</author>
        /// <date>8/11/2012</date>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="CandidateID">The candidate ID.</param>
        public void DeleteMark(int SchoolID, int AppliedLevel, int CandidateID)
        {
            // Thực hiện Update vào bảng Candidate  trường: Mark = 0, Judgement=0
            Candidate ca = CandidateRepository.Find(CandidateID);
            if (ca != null)
            {
                ca.Mark = null;
                ca.Judgement = "";
                this.Update(ca);
            }

        }
        #endregion

        #region SearchMark
        public IEnumerable<CandidateMark> SearchMark(int SchoolID, int AppliedLevel, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0 || AppliedLevel == 0)
                return null;

            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            int ExaminationID = Utils.GetInt(SearchInfo, "ExaminationID");
            if (ExaminationID == 0)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            if (exam == null)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            int? DetachableHeadBagID = Utils.GetNullableInt(SearchInfo, "DetachableHeadBagID");

            IQueryable<Candidate> LsCandidate = this.Search(SearchInfo);

            IEnumerable<CandidateMark> LsCandidateMark = (from c in LsCandidate
                                                          join pp in PupilProfileBusiness.All on c.PupilID equals pp.PupilProfileID
                                                          join poc in PupilOfClassBusiness.All on c.ClassID equals poc.ClassID
                                                          join dhm in DetachableHeadMappingBusiness.All.Where(u => !DetachableHeadBagID.HasValue || DetachableHeadBagID.Value == 0 || u.DetachableHeadBagID == DetachableHeadBagID.Value)
                                                          on c.CandidateID equals dhm.CandidateID into g1
                                                          from j1 in g1.DefaultIfEmpty()
                                                          where pp.PupilProfileID == poc.PupilID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                          select new CandidateMark
                                                          {
                                                              ClassID = c.ClassID,
                                                              ClassName = c.ClassProfile.DisplayName,
                                                              SubjectID = c.SubjectID,
                                                              BirthDate = pp.BirthDate,
                                                              RoomName = c.ExaminationRoom.RoomTitle,
                                                              PupilID = c.PupilID,
                                                              CandidateID = c.CandidateID,
                                                              Description = string.Empty,
                                                              DetachableHeadBagID = j1 != null ? j1.DetachableHeadBagID : 0,
                                                              DetachableHeadNumber = j1 != null ? j1.DetachableHeadNumber : string.Empty,
                                                              ErrorReason = string.Empty,
                                                              Mark = c.Mark,
                                                              Judgement = c.Judgement,
                                                              IsCheck = false,
                                                              IsDisable = c.HasReason.HasValue,
                                                              NamedListCode = c.NamedListCode,
                                                              NamedListNumber = c.NamedListNumber,
                                                              PenaltyMarkPercent = 0,
                                                              PupilCode = pp.PupilCode,
                                                              PupilName = pp.FullName,
                                                              EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : string.Empty,
                                                              Status = poc.Status,
                                                              RealMark = string.Empty,
                                                              RoomID = c.RoomID,
                                                              Name = pp.Name,
                                                              HasReason = c.HasReason
                                                          }).ToList();
            List<ViolatedCandidate> listViolated = ViolatedCandidateBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();
            List<int> listExaminationSubjectID = LsCandidateMark.Where(o => o.SubjectID.HasValue)
                .Select(o => o.SubjectID.Value).Distinct().ToList();
            var listSubjectExam = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == ExaminationID
                && listExaminationSubjectID.Contains(o.ExaminationSubjectID)).Select(o => new { o.SubjectID, o.ExaminationSubjectID }).ToList();
            List<int> listSubjectID = listSubjectExam.Select(o => o.SubjectID).Distinct().ToList();
            Dictionary<int, int?> dicSubjectType = ExaminationSubjectBusiness.GetSubjectMarkType(exam.AcademicYearID, listSubjectID, AppliedLevel);

            foreach (CandidateMark ca in LsCandidateMark)
            {
                if (ca.SubjectID.HasValue)
                {
                    var subjectExam = listSubjectExam.FirstOrDefault(o => o.ExaminationSubjectID == ca.SubjectID);
                    if (subjectExam != null)
                    {
                        ca.IsCommenting = dicSubjectType.ContainsKey(subjectExam.SubjectID) ?
                            dicSubjectType[subjectExam.SubjectID] : new Nullable<int>();
                    }
                }
                ca.ExamMark = ca.IsCommenting.HasValue
                                    ? (ca.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                        ? ca.Judgement
                                        : (ca.Mark.HasValue ? ca.Mark.Value.ToString(AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY ? "0" : "0.#") : string.Empty))
                                    : string.Empty;

                var violatedCandidates = listViolated.Where(u => u.CandidateID == ca.CandidateID);
                if (violatedCandidates.Count() > 0)
                {
                    ca.PenaltyMarkPercent = violatedCandidates.Sum(u => u.ExamViolationType.PenalizedMark);
                    foreach (var violatedCandidate in violatedCandidates)
                    {
                        ca.Description += "Trừ " + violatedCandidate.ExamViolationType.PenalizedMark + "% điểm,Lý do: " + violatedCandidate.ExamViolationType.Resolution + ",";
                    }
                    ca.RealMark = ca.IsCommenting.HasValue && ca.IsCommenting.Value == 0
                        ? (!ca.PenaltyMarkPercent.HasValue
                            ? ca.ExamMark
                            : (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                            ? (ca.Mark.HasValue ? (ca.Mark.Value * (1 - (decimal)ca.PenaltyMarkPercent.Value / 100)).ToString("0") : string.Empty)
                            : (ca.Mark.HasValue ? (ca.Mark.Value * (1 - (decimal)ca.PenaltyMarkPercent.Value / 100)).ToString("0.#") : string.Empty)
                              )
                           )
                        : ca.ExamMark;
                }
                else
                {
                    ca.RealMark = ca.ExamMark;
                }
            }
            return LsCandidateMark;
        }

        #endregion

        #region TransferMark
        /// <summary>
        /// Chuyển điểm thi vào sổ điểm cho nam hoc hien tai.
        /// Nghiệp vụ chỉ đáp ứng cho cấp 2, 3. Cấp 1 chưa có nghiệp vụ này
        /// </summary>
        /// <param name="SchoolID">ID cua truong</param>
        /// <param name="AppliedLevel">Cap hoc: 2 hoac 3</param>
        /// <param name="ExaminationSubjectID">ID mon hoc trong ky thi</param>
        /// <param name="ListCandidateID">Danh sach CandidateID se duoc thuc hien chuyen diem</param>
        /// <param name="Semester">Hoc ky se duoc chuyen diem vao</param>
        /// <param name="MarkType">Loai diem se duoc chuyen diem vao</param>
        /// <param name="MarkIndex">Cot diem se duoc chuyen diem vao</param>
        public void TransferMark(int SchoolID, int AppliedLevel, int ExaminationSubjectID, List<int> ListCandidateID, int Semester, string MarkType, int MarkIndex)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                //TODO: turning code, bổ sung tính điểm TBM theo kỳ và theo đợt
                ValidateTransfer(SchoolID, AppliedLevel, ExaminationSubjectID, ListCandidateID, Semester, MarkType, MarkIndex);
                //Từ MarkType lấy được MarkTypeID theo bảng MarkType với Tittle = MarkType
                MarkType mt = MarkTypeBusiness.All.Where(o => o.Title == MarkType && o.AppliedLevel == AppliedLevel && o.IsActive == true).FirstOrDefault();
                //Từ ExaminationSubject(ExaminationSubjectID)  lấy được SchoolID, AcademicYearID, SubjectID, Year
                ExaminationSubject es = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
                int AcademicYearID = es.Examination.AcademicYearID;
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                int SubjectID = es.SubjectID;
                int Year = es.Examination.Year.Value;
                string markTitle = "";
                if (MarkType == "V")
                {
                    markTitle = MarkType + MarkIndex;
                }
                else
                {
                    markTitle = "HK";
                }
                //Gọi hàm SearchMarks(SchoolID, AppliedLevel, Dictionary) listMark với Dictionary[“ExaminationSubjectID”] = ExaminationSubjectID để lấy dữ liệu điểm thực tế của các thí sính.
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["ExaminationSubjectID"] = ExaminationSubjectID;
                Dictionary["ExaminationID"] = es.ExaminationID;
                IEnumerable<CandidateMark> lsMark = this.SearchMark(SchoolID, AppliedLevel, Dictionary).Where(o => o.RealMark != "");
                CultureInfo usCulture = new CultureInfo("en-US");
                if (lsMark != null && lsMark.Count() > 0)
                {
                    lsMark = lsMark.Where(o => ListCandidateID.Contains(o.CandidateID)).ToList();
                    List<int> lstPupilIDAll = lsMark.Select(o => o.PupilID).Distinct().ToList();
                    //Kiểm tra học sinh được miễn giảm
                    Dictionary<string, object> search = new Dictionary<string, object>();
                    search["SubjectID"] = SubjectID;
                    search["AcademicYearID"] = AcademicYearID;
                    IQueryable<ExemptedSubject> iqExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(SchoolID, search).Where(o => lstPupilIDAll.Contains(o.PupilID));
                    List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
                    if (iqExemptedSubject != null && iqExemptedSubject.Count() > 0 && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        lstExemptedSubject = iqExemptedSubject.Where(o => o.FirstSemesterExemptType != null).ToList();
                    }
                    else if (iqExemptedSubject != null && iqExemptedSubject.Count() > 0 && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        lstExemptedSubject = iqExemptedSubject.Where(o => o.SecondSemesterExemptType != null).ToList();
                    }

                    List<int> lstPupilExemptedID = lstExemptedSubject.Select(o => o.PupilID).Distinct().ToList();

                    //Danh sách các lớp học môn này
                    search = new Dictionary<string, object>();
                    search["SubjectID"] = SubjectID;
                    search["AcademicYearID"] = AcademicYearID;
                    search["Semester"] = Semester;
                    List<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, search).ToList();
                    List<int> lstClassID = iqClassSubject.Select(o => o.ClassID).Distinct().ToList();
                    //Chỉ chuyển điểm cho học sinh ở các lớp có học môn đó và không bị miễn giảm
                    lsMark = lsMark.Where(o => (lstClassID.Contains(o.ClassID.Value)) && !lstPupilExemptedID.Contains(o.PupilID)).ToList();


                    List<CandidateMark> lsMarkRecord = lsMark.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).ToList();
                    List<CandidateMark> lsJudgeRecord = lsMark.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                    List<int> lstPupilID = new List<int>();
                    // Luu lai thong tin de cap nhat vao bang tien trinh tinh TBM tu dong
                    Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();
                    #region Môn tính điểm
                    if (lsMarkRecord != null && lsMarkRecord.Count > 0)
                    {
                        lstPupilID = lsMarkRecord.Select(o => o.PupilID).Distinct().ToList();
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = AcademicYearID;
                        dic["Title"] = markTitle;
                        dic["Semester"] = Semester;
                        dic["SubjectID"] = SubjectID;
                        dic["Year"] = academicYear.Year; // AnhVD 20131217
                        List<MarkRecord> lstMarkRecordDB = MarkRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => lstPupilID.Contains(o.PupilID)).ToList();
                        List<MarkRecord> listInsertMark = new List<MarkRecord>();
                        List<MarkRecord> listUpdateMark = new List<MarkRecord>();


                        foreach (CandidateMark itemInsert in lsMarkRecord)
                        {
                            // Kiem tra trang thai cua sinh. Chi chuyen diem khi hoc sinh dang hoc
                            if (itemInsert.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                continue;
                            }
                            bool isUpdate = false;
                            MarkRecord itemDb = lstMarkRecordDB.Where(o => o.PupilID == itemInsert.PupilID).FirstOrDefault();
                            if (itemDb == null)
                            {
                                MarkRecord mrecord = new MarkRecord();
                                mrecord.PupilID = itemInsert.PupilID;
                                mrecord.ClassID = itemInsert.ClassID.Value;
                                mrecord.Semester = Semester;
                                mrecord.MarkTypeID = mt.MarkTypeID;
                                mrecord.SchoolID = SchoolID;
                                mrecord.AcademicYearID = AcademicYearID;
                                mrecord.SubjectID = SubjectID;
                                mrecord.CreatedAcademicYear = Year;
                                mrecord.OrderNumber = (MarkType == "V") ? (int)MarkIndex : 1;
                                mrecord.Title = markTitle;
                                mrecord.Mark = decimal.Parse(itemInsert.RealMark.Replace(",", "."), usCulture);
                                listInsertMark.Add(mrecord);
                            }
                            else
                            {
                                if (!itemInsert.RealMark.Equals(itemDb.Mark))
                                {
                                    itemDb.OldMark = itemDb.Mark;
                                    itemDb.Mark = decimal.Parse(itemInsert.RealMark.Replace(",", "."), usCulture);
                                    itemDb.IsSMS = false;
                                    listUpdateMark.Add(itemDb);
                                    isUpdate = true;
                                }
                                lstMarkRecordDB.Remove(itemDb);
                            }
                            // Lay thong tin insert vao bang danh dau thay doi diem
                            if (isUpdate)
                            {
                                string key = itemInsert.ClassID + "_" + SubjectID;
                                if (dicPupilThreadMark.ContainsKey(key))
                                {
                                    lstPupilID = dicPupilThreadMark[key];
                                    if (!lstPupilID.Contains(itemInsert.PupilID))
                                    {
                                        lstPupilID.Add(itemInsert.PupilID);
                                        dicPupilThreadMark[key] = lstPupilID;
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[key] = new List<int>() { itemInsert.PupilID };
                                }
                            }
                        }
                        if (listInsertMark.Count > 0)
                        {
                            List<MarkJudgeBO> listMark = new List<MarkJudgeBO>();
                            foreach (MarkRecord item in listInsertMark)
                            {
                                // Dung procedure thuc hien
                                MarkJudgeBO mj = new MarkJudgeBO();
                                mj.PupilID = item.PupilID;
                                mj.ClassID = item.ClassID;
                                mj.SubjectID = item.SubjectID;
                                mj.MarkTypeID = item.MarkTypeID;
                                mj.Mark = item.Mark;
                                mj.Title = item.Title;
                                mj.OrderNumber = item.OrderNumber;
                                listMark.Add(mj);
                            }
                            this.context.PROC_INSERT_MARK_JUDGE(SchoolID, AcademicYearID, Semester, null, listMark);
                        }
                        if (listUpdateMark.Count > 0)
                        {
                            foreach (MarkRecord item in listUpdateMark)
                            {
                                MarkRecordBusiness.Update(item);
                        }
                        MarkRecordBusiness.Save();
                        }
                        if (listInsertMark.Count > 0 || listUpdateMark.Count > 0) // Neu co Insert hoac Update diem thi goi ham Tinh lai diem TBM cho mon tinh diem
                        {
                        this.context.PROC_LEVEL_MARK_SUMMED_UP(SchoolID, AcademicYearID, es.EducationLevelID, Semester, SubjectID);
                    }
                    }
                    #endregion

                    #region Môn nhận xét
                    if (lsJudgeRecord != null && lsJudgeRecord.Count > 0)
                    {
                        lstPupilID = lsJudgeRecord.Select(o => o.PupilID).Distinct().ToList();
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = AcademicYearID;
                        dic["Title"] = markTitle;
                        dic["Semester"] = Semester;
                        dic["SubjectID"] = SubjectID;
                        dic["Year"] = academicYear.Year;
                        List<JudgeRecord> lstJudgeRecordDB = JudgeRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => lstPupilID.Contains(o.PupilID)).ToList();
                        List<JudgeRecord> listInsertJudge = new List<JudgeRecord>();
                        List<JudgeRecord> listUpdateJudge = new List<JudgeRecord>();

                        foreach (CandidateMark itemInsert in lsJudgeRecord)
                        {
                            // Kiem tra trang thai cua sinh. Chi chuyen diem khi hoc sinh dang hoc
                            if (itemInsert.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                continue;
                            }
                            bool isUpdate = false;
                            JudgeRecord itemDb = lstJudgeRecordDB.Where(o => o.PupilID == itemInsert.PupilID).FirstOrDefault();
                            if (itemDb == null)
                            {
                                JudgeRecord jrecord = new JudgeRecord();
                                jrecord.PupilID = itemInsert.PupilID;
                                jrecord.ClassID = itemInsert.ClassID.Value;
                                jrecord.Semester = Semester;
                                jrecord.MarkTypeID = mt.MarkTypeID;
                                jrecord.SchoolID = SchoolID;
                                jrecord.AcademicYearID = AcademicYearID;
                                jrecord.SubjectID = SubjectID;
                                jrecord.CreatedAcademicYear = Year;
                                jrecord.OrderNumber = (MarkType == "V") ? (int)MarkIndex : 1;
                                jrecord.Title = markTitle;
                                jrecord.Judgement = itemInsert.RealMark;
                                listInsertJudge.Add(jrecord);
                            }
                            else
                            {
                                if (!itemInsert.RealMark.Equals(itemDb.Judgement))
                                {
                                    itemDb.OldJudgement = itemDb.Judgement;
                                    itemDb.Judgement = itemInsert.RealMark;
                                    itemDb.IsSMS = false;
                                    listUpdateJudge.Add(itemDb);
                                    isUpdate = true;
                                }
                                lstJudgeRecordDB.Remove(itemDb);
                            }
                            // Lay thong tin insert vao bang danh dau thay doi diem
                            if (isUpdate)
                            {
                                string key = itemInsert.ClassID + "_" + SubjectID;
                                if (dicPupilThreadMark.ContainsKey(key))
                                {
                                    lstPupilID = dicPupilThreadMark[key];
                                    if (!lstPupilID.Contains(itemInsert.PupilID))
                                    {
                                        lstPupilID.Add(itemInsert.PupilID);
                                        dicPupilThreadMark[key] = lstPupilID;
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[key] = new List<int>() { itemInsert.PupilID };
                                }
                            }
                        }

                        if (listInsertJudge.Count > 0)
                        {
                            List<MarkJudgeBO> listMark = new List<MarkJudgeBO>();
                            foreach (JudgeRecord item in listInsertJudge)
                            {
                                // Dung store thuc hien
                                MarkJudgeBO mj = new MarkJudgeBO();
                                mj.PupilID = item.PupilID;
                                mj.ClassID = item.ClassID;
                                mj.SubjectID = item.SubjectID;
                                mj.MarkTypeID = item.MarkTypeID;
                                mj.Judgement = item.Judgement;
                                mj.Title = item.Title;
                                mj.OrderNumber = item.OrderNumber;
                                listMark.Add(mj);
                            }
                            this.context.PROC_INSERT_MARK_JUDGE(SchoolID, AcademicYearID, Semester, null, listMark);
                        }
                        if (listUpdateJudge.Count > 0)
                        {
                            foreach (JudgeRecord item in listUpdateJudge)
                            {
                                JudgeRecordBusiness.Update(item);
                        }
                        JudgeRecordBusiness.Save();
                        }

                        #region Tinh diem TBM cho mon nhan xet neu co
                        this.context.PROC_LEVEL_JUDGE_SUMMED_UP(SchoolID, AcademicYearID, es.EducationLevelID, Semester, SubjectID);
                        #endregion
                    }
                    #endregion

                    #region Save ThreadMark
                    // Insert into ThreadMark for Auto Process cho nhung HS update diem
                    if (dicPupilThreadMark.Count > 0)
                    {
                        List<string> listClassSubjectStr = dicPupilThreadMark.Keys.ToList();
                        foreach (string key in listClassSubjectStr)
                        {
                            string[] arrKey = key.Split('_');
                            int classID = int.Parse(arrKey[0]);
                            int subjectID = int.Parse(arrKey[1]);
                            ThreadMarkBO info = new ThreadMarkBO();
                            info.SchoolID = SchoolID;
                            info.ClassID = classID;
                            info.AcademicYearID = AcademicYearID;
                            info.SubjectID = subjectID;
                            info.Semester = Semester;
                            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;                            
                            info.PupilIds = dicPupilThreadMark[key];
                            ThreadMarkBusiness.InsertActionList(info);
                        }
                        ThreadMarkBusiness.Save();
                    }
                    // End
                    #endregion
                }
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// Chuyển điểm thi vào sổ điểm cho các năm học cũ.
        /// Nghiệp vụ chỉ đáp ứng cho cấp 2, 3. Cấp 1 chưa có nghiệp vụ này
        /// </summary>
        /// <param name="SchoolID">ID cua truong</param>
        /// <param name="AppliedLevel">Cap hoc: 2 hoac 3</param>
        /// <param name="ExaminationSubjectID">ID mon hoc trong ky thi</param>
        /// <param name="ListCandidateID">Danh sach CandidateID se duoc thuc hien chuyen diem</param>
        /// <param name="Semester">Hoc ky se duoc chuyen diem vao</param>
        /// <param name="MarkType">Loai diem se duoc chuyen diem vao</param>
        /// <param name="MarkIndex">Cot diem se duoc chuyen diem vao</param>
        public void TransferMarkHistory(int SchoolID, int AppliedLevel, int ExaminationSubjectID, List<int> ListCandidateID, int Semester, string MarkType, int MarkIndex)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                //TODO: turning code, bổ sung tính điểm TBM theo kỳ và theo đợt
                ValidateTransfer(SchoolID, AppliedLevel, ExaminationSubjectID, ListCandidateID, Semester, MarkType, MarkIndex);
                //Từ MarkType lấy được MarkTypeID theo bảng MarkType với Tittle = MarkType
                MarkType mt = MarkTypeBusiness.All.Where(o => o.Title == MarkType && o.AppliedLevel == AppliedLevel && o.IsActive == true).FirstOrDefault();
                //Từ ExaminationSubject(ExaminationSubjectID)  lấy được SchoolID, AcademicYearID, SubjectID, Year
                ExaminationSubject es = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
                int AcademicYearID = es.Examination.AcademicYearID;
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                int SubjectID = es.SubjectID;
                int Year = es.Examination.Year.Value;
                string markTitle = "";
                if (MarkType == "V")
                {
                    markTitle = MarkType + MarkIndex;
                }
                else
                {
                    markTitle = "HK";
                }
                //Gọi hàm SearchMarks(SchoolID, AppliedLevel, Dictionary) listMark với Dictionary[“ExaminationSubjectID”] = ExaminationSubjectID để lấy dữ liệu điểm thực tế của các thí sính.
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["ExaminationSubjectID"] = ExaminationSubjectID;
                Dictionary["ExaminationID"] = es.ExaminationID;
                IEnumerable<CandidateMark> lsMark = this.SearchMark(SchoolID, AppliedLevel, Dictionary).Where(o => o.RealMark != "");
                CultureInfo usCulture = new CultureInfo("en-US");
                if (lsMark != null && lsMark.Count() > 0)
                {
                    lsMark = lsMark.Where(o => ListCandidateID.Contains(o.CandidateID)).ToList();
                    List<int> lstPupilIDAll = lsMark.Select(o => o.PupilID).Distinct().ToList();
                    //Kiểm tra học sinh được miễn giảm
                    Dictionary<string, object> search = new Dictionary<string, object>();
                    search["SubjectID"] = SubjectID;
                    search["AcademicYearID"] = AcademicYearID;
                    IQueryable<ExemptedSubject> iqExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(SchoolID, search).Where(o => lstPupilIDAll.Contains(o.PupilID));
                    List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
                    if (iqExemptedSubject != null && iqExemptedSubject.Count() > 0 && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        lstExemptedSubject = iqExemptedSubject.Where(o => o.FirstSemesterExemptType != null).ToList();
                    }
                    else if (iqExemptedSubject != null && iqExemptedSubject.Count() > 0 && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        lstExemptedSubject = iqExemptedSubject.Where(o => o.SecondSemesterExemptType != null).ToList();
                    }

                    List<int> lstPupilExemptedID = lstExemptedSubject.Select(o => o.PupilID).Distinct().ToList();

                    //Danh sách các lớp học môn này
                    search = new Dictionary<string, object>();
                    search["SubjectID"] = SubjectID;
                    search["AcademicYearID"] = AcademicYearID;
                    search["Semester"] = Semester;
                    List<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, search).ToList();
                    List<int> lstClassID = iqClassSubject.Select(o => o.ClassID).Distinct().ToList();
                    //Chỉ chuyển điểm cho học sinh ở các lớp có học môn đó và không bị miễn giảm
                    lsMark = lsMark.Where(o => (lstClassID.Contains(o.ClassID.Value)) && !lstPupilExemptedID.Contains(o.PupilID)).ToList();


                    List<CandidateMark> lsMarkRecord = lsMark.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).ToList();
                    List<CandidateMark> lsJudgeRecord = lsMark.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
                    List<int> lstPupilID = new List<int>();
                    // Luu lai thong tin de cap nhat vao bang tien trinh tinh TBM tu dong
                    Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();

                    #region Môn tính điểm
                    if (lsMarkRecord != null && lsMarkRecord.Count > 0)
                    {
                        lstPupilID = lsMarkRecord.Select(o => o.PupilID).Distinct().ToList();
                        // Lay danh sach diem trong DB
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = AcademicYearID;
                        dic["SchoolID"] = SchoolID;
                        dic["Title"] = markTitle;
                        dic["Semester"] = Semester;
                        dic["SubjectID"] = SubjectID;
                        dic["Year"] = academicYear.Year;
                        List<MarkRecordHistory> lstMarkRecordDB = MarkRecordHistoryBusiness.SearchMarkRecordHistory(dic).Where(o => lstPupilID.Contains(o.PupilID)).ToList();
                        List<MarkRecordHistory> listInsertMark = new List<MarkRecordHistory>();
                        List<MarkRecordHistory> listUpdateMark = new List<MarkRecordHistory>();

                        foreach (CandidateMark itemInsert in lsMarkRecord)
                        {
                            // Kiem tra trang thai cua sinh. Chi chuyen diem khi hoc sinh dang hoc
                            if (itemInsert.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                continue;
                            }
                            bool isUpdate = false;
                            MarkRecordHistory itemDb = lstMarkRecordDB.FirstOrDefault(o => o.PupilID == itemInsert.PupilID);
                            if (itemDb == null)
                            {
                                MarkRecordHistory mrecord = new MarkRecordHistory();
                                mrecord.PupilID = itemInsert.PupilID;
                                mrecord.ClassID = itemInsert.ClassID.Value;
                                mrecord.Semester = Semester;
                                mrecord.MarkTypeID = mt.MarkTypeID;
                                mrecord.SchoolID = SchoolID;
                                mrecord.AcademicYearID = AcademicYearID;
                                mrecord.SubjectID = SubjectID;
                                mrecord.CreatedAcademicYear = Year;
                                mrecord.OrderNumber = (MarkType == "V") ? (int)MarkIndex : 1;
                                mrecord.Title = markTitle;
                                mrecord.Mark = decimal.Parse(itemInsert.RealMark.Replace(",", "."), usCulture);
                                listInsertMark.Add(mrecord);
                            }
                            else
                            {
                                if (!itemInsert.RealMark.Equals(itemDb.Mark))
                                {
                                    itemDb.OldMark = itemDb.Mark;
                                    itemDb.Mark = decimal.Parse(itemInsert.RealMark.Replace(",", "."), usCulture);
                                    itemDb.IsSMS = false;
                                    listUpdateMark.Add(itemDb);
                                    isUpdate = true;
                                }
                                lstMarkRecordDB.Remove(itemDb);
                            }
                            // Lay thong tin insert vao bang danh dau thay doi diem
                            if (isUpdate)
                            {
                                string key = itemInsert.ClassID + "_" + SubjectID;
                                if (dicPupilThreadMark.ContainsKey(key))
                                {
                                    lstPupilID = dicPupilThreadMark[key];
                                    if (!lstPupilID.Contains(itemInsert.PupilID))
                                    {
                                        lstPupilID.Add(itemInsert.PupilID);
                                        dicPupilThreadMark[key] = lstPupilID;
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[key] = new List<int>() { itemInsert.PupilID };
                                }
                            }
                        }
                        if (listInsertMark.Count > 0)
                        {
                            // Dung Business thuc hien vi hien tai khong co procedure xu ly cho cac nam hoc cu
                            foreach (MarkRecordHistory item in listInsertMark)
                            {
                                MarkRecordHistoryBusiness.Insert(item);
                            }

                            /*
                            // Dung procedure thuc hien
                            List<MarkJudgeBO> listMark = new List<MarkJudgeBO>();
                            foreach (MarkRecordHistory item in listInsertMark)
                            {
                             
                                //MarkJudgeBO mj = new MarkJudgeBO();
                                //mj.PupilID = item.PupilID;
                                //mj.ClassID = item.ClassID;
                                //mj.SubjectID = item.SubjectID;
                                //mj.MarkTypeID = item.MarkTypeID;
                                //mj.Mark = item.Mark;
                                //mj.Title = item.Title;
                                //mj.OrderNumber = item.OrderNumber;
                                //listMark.Add(mj);
                            }
                            this.context.PROC_INSERT_MARK_JUDGE(SchoolID, AcademicYearID, Semester, null, listMark);
                            */
                        }
                        if (listUpdateMark.Count > 0)
                        {
                            listUpdateMark.ForEach(mrh => MarkRecordHistoryBusiness.Update(mrh));
                        }
                        MarkRecordBusiness.Save();

                        /// TODO: Tinh diem TBM cho mon tinh diem. Tuy nhien hien cac nam hoc cu khong co StoredProcedure nen khong thuc hien duoc. Nguoi dung phai lam bang tay
                        //this.context.PROC_LEVEL_MARK_SUMMED_UP(SchoolID, AcademicYearID, es.EducationLevelID, Semester, SubjectID);
                    }
                    #endregion

                    #region Môn nhận xét
                    if (lsJudgeRecord != null && lsJudgeRecord.Count > 0)
                    {
                        lstPupilID = lsJudgeRecord.Select(o => o.PupilID).Distinct().ToList();
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = AcademicYearID;
                        dic["SchoolID"] = SchoolID;
                        dic["Title"] = markTitle;
                        dic["Semester"] = Semester;
                        dic["SubjectID"] = SubjectID;
                        dic["Year"] = academicYear.Year;
                        List<JudgeRecordHistory> lstJudgeRecordDB = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(dic).Where(o => lstPupilID.Contains(o.PupilID)).ToList();
                        List<JudgeRecordHistory> listInsertJudge = new List<JudgeRecordHistory>();
                        List<JudgeRecordHistory> listUpdateJudge = new List<JudgeRecordHistory>();

                        foreach (CandidateMark itemInsert in lsJudgeRecord)
                        {
                            // Kiem tra trang thai cua sinh. Chi chuyen diem khi hoc sinh dang hoc
                            if (itemInsert.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                continue;
                            }
                            bool isUpdate = false;
                            JudgeRecordHistory itemDb = lstJudgeRecordDB.FirstOrDefault(o => o.PupilID == itemInsert.PupilID);
                            if (itemDb == null)
                            {
                                JudgeRecordHistory jrecord = new JudgeRecordHistory();
                                jrecord.PupilID = itemInsert.PupilID;
                                jrecord.ClassID = itemInsert.ClassID.Value;
                                jrecord.Semester = Semester;
                                jrecord.MarkTypeID = mt.MarkTypeID;
                                jrecord.SchoolID = SchoolID;
                                jrecord.AcademicYearID = AcademicYearID;
                                jrecord.SubjectID = SubjectID;
                                jrecord.CreatedAcademicYear = Year;
                                jrecord.OrderNumber = (MarkType == "V") ? (int)MarkIndex : 1;
                                jrecord.Title = markTitle;
                                jrecord.Judgement = itemInsert.RealMark;
                                listInsertJudge.Add(jrecord);
                            }
                            else
                            {
                                if (!itemInsert.RealMark.Equals(itemDb.Judgement))
                                {
                                    itemDb.OldJudgement = itemDb.Judgement;
                                    itemDb.Judgement = itemInsert.RealMark;
                                    itemDb.IsSMS = false;
                                    listUpdateJudge.Add(itemDb);
                                    isUpdate = true;
                                }
                                lstJudgeRecordDB.Remove(itemDb);
                            }
                            // Lay thong tin insert vao bang danh dau thay doi diem
                            if (isUpdate)
                            {
                                string key = itemInsert.ClassID + "_" + SubjectID;
                                if (dicPupilThreadMark.ContainsKey(key))
                                {
                                    lstPupilID = dicPupilThreadMark[key];
                                    if (!lstPupilID.Contains(itemInsert.PupilID))
                                    {
                                        lstPupilID.Add(itemInsert.PupilID);
                                        dicPupilThreadMark[key] = lstPupilID;
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[key] = new List<int>() { itemInsert.PupilID };
                                }
                            }
                        }

                        if (listInsertJudge.Count > 0)
                        {
                            // Dung Business thuc hien vi hien tai khong co procedure xu ly cho cac nam hoc cu
                            listInsertJudge.ForEach(j=>JudgeRecordHistoryBusiness.Insert(j));
                            
                            /*
                            // Dung store thuc hien insert
                            List<MarkJudgeBO> listMark = new List<MarkJudgeBO>();
                            foreach (JudgeRecordHistory item in listInsertJudge)
                            {
                                MarkJudgeBO mj = new MarkJudgeBO();
                                mj.PupilID = item.PupilID;
                                mj.ClassID = item.ClassID;
                                mj.SubjectID = item.SubjectID;
                                mj.MarkTypeID = item.MarkTypeID;
                                mj.Judgement = item.Judgement;
                                mj.Title = item.Title;
                                mj.OrderNumber = item.OrderNumber;
                                listMark.Add(mj);
                            }
                            this.context.PROC_INSERT_MARK_JUDGE(SchoolID, AcademicYearID, Semester, null, listMark);
                            */
                        }
                        if (listUpdateJudge.Count > 0)
                        {
                            listUpdateJudge.ForEach(j => JudgeRecordHistoryBusiness.Update(j));
                        }
                        JudgeRecordBusiness.Save();

                        /// TODO: Tinh diem TBM cho mon tinh diem. Tuy nhien hien cac nam hoc cu khong co StoredProcedure nen khong thuc hien duoc. Nguoi dung phai lam bang tay
                        //this.context.PROC_LEVEL_JUDGE_SUMMED_UP(SchoolID, AcademicYearID, es.EducationLevelID, Semester, SubjectID);
                    }
                    #endregion

                    #region Save ThreadMark
                    // Insert into ThreadMark for Auto Process cho nhung HS update diem
                    if (dicPupilThreadMark.Count > 0)
                    {
                        List<string> listClassSubjectStr = dicPupilThreadMark.Keys.ToList();
                        foreach (string key in listClassSubjectStr)
                        {
                            string[] arrKey = key.Split('_');
                            int classID = int.Parse(arrKey[0]);
                            int subjectID = int.Parse(arrKey[1]);
                            ThreadMarkBO info = new ThreadMarkBO();
                            info.SchoolID = SchoolID;
                            info.ClassID = classID;
                            info.AcademicYearID = AcademicYearID;
                            info.SubjectID = subjectID;
                            info.Semester = Semester;
                            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                            info.PupilIds = dicPupilThreadMark[key];
                            ThreadMarkBusiness.InsertActionList(info);
                        }
                        ThreadMarkBusiness.Save();
                    }
                    // End
                    #endregion
                }
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        #endregion

        #region validate Transfer
        /// <summary>
        ///  validate Transfer
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime></DateTime>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ExaminationSubjectID"></param>
        /// <param name="ListCandidateID"></param>
        /// <param name="Semester"></param>
        /// <param name="MarkType"></param>
        /// <param name="MarkIndex"></param>
        private void ValidateTransfer(int SchoolID, int AppliedLevel, int ExaminationSubjectID,
            List<int> ListCandidateID, int Semester, string MarkType, int MarkIndex)
        {
            Candidate ca = this.repository.Find(ListCandidateID.FirstOrDefault());
            ExaminationSubject es = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
            Examination exam = ExaminationBusiness.Find(es.ExaminationID);
            List<string> lsMarkType = new List<string>() { "V", "HK" };
            List<int> lsSemester = new List<int>() { 1, 2 };

            bool Compatible1 = EducationLevelRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel",
                           new Dictionary<string, object>()
                    {
                        {"EducationLevelID",ca.EducationLevelID},
                        {"Grade",AppliedLevel}
                    }, null);
            if (!Compatible1)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Chỉ số con điểm không đúng – Kiểm tra MarkIndex – chỉ nhận giá trị từ 0 tới 
            //SemesterDeclarationBusiness.Search(Dictionary).indexOf(0).TwiceCoeffiecientMark 
            //với Dictionary[“AcademicYearID”] = AcdemicYearID, Dictionary[“Semester”] = Semester
            SemeterDeclaration sd = SemeterDeclarationBusiness.Search(new Dictionary<string, object>()
            {
                {"AcademicYearID",exam.AcademicYearID},
                {"Semester",Semester}
            }).FirstOrDefault();
            if (sd == null)
            {
                throw new BusinessException("SemeterDeclaration_Null");
            }
            Utils.ValidateRange(MarkIndex, 0, sd.TwiceCoeffiecientMark, "Candidate_Label_MarkIndex");
            //SchoolID: PK(SchoolProfile)

            bool Exist1 = SchoolProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                    new Dictionary<string, object>()
                {
                    {"SchoolProfileID",SchoolID}
                }, null);
            if (!Exist1)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
            //MarkType: in (HK, V)
            if (!lsMarkType.Contains(MarkType))
            {
                throw new BusinessException("Common_Validate_NotMarkType");
            }
            //Semester: in(1,2)
            if (!lsSemester.Contains(Semester))
            {
                throw new BusinessException("Common_Validate_NotSemester");
            }
            //Ký thi phải ở trạng thái đánh phách mới được sửa. 
            //Kiểm tra Examination(ExaminationID).CurrentStage  = EXAMINATION_STAGE_MARK_COMPLETED

            if (exam.CurrentStage != SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                throw new BusinessException("Examination_Label_CurrentStageNotMarkComplete");
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(exam.AcademicYearID))
            {
                throw new BusinessException("SchoolMovement_Validate_IsNotCurrentYear");
            }


            //Con điểm đã bị khóa - LockedMarkDetailBusiness.SearchBySchool(SchoolID, Dictionary).Size() > 0
            //+ Dictionary[“AcademicYearID”] = AcdemicYearID
            //+ Dictionary[“Semester”] = Semester
            //+ Dictionary[“MarkTypeTitle”] = MarkType
            //+ Dictionary[“SubjectID”] = SubjectID 
            //+ Dictionary[“MarkIndex”] = MarkIndex (có điều kiệu này khi MarkType = V)
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",exam.AcademicYearID},
                {"Semester",Semester},
                {"MarkTypeTitle",MarkType},
                {"SubjectID",es.SubjectID}
            };
            if (MarkType == "V")
            {
                dic["MarkIndex"] = MarkIndex;
            }
            string MarkTitle = MarkType + MarkIndex.ToString();
            IQueryable<LockedMarkDetail> lsLMD = LockedMarkDetailBusiness.SearchBySchool(SchoolID, dic).Where(u => u.MarkType.Title == MarkTitle);
            if (lsLMD.Count() > 0)
            {
                throw new BusinessException("Candidate_Label_MarkLock");
            }
            foreach (var item in ListCandidateID)
            {
                //CandidateID: PK(Candidate)

                bool Exist = CandidateRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Candidate",
                        new Dictionary<string, object>()
                {
                    {"CandidateID",item}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }

                //ExaminationSubjectID, CandidateID: not compatible(Candidate)

                bool Compatible = CandidateRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Candidate",
                        new Dictionary<string, object>()
                {
                    {"SubjectID",ExaminationSubjectID},
                    {"CandidateID",item}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //AppliedLevel(Grade), EducationLevelID: not compatible(EducationLevel)
            }
        }
        #endregion

        #region report
        /// <summary>
        /// GetHashKey
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   10:57 AM
        /// </remarks>
        public string GetHashKey(CandidateBO entity)
        {
            return ReportUtils.GetHashKey(new Dictionary<string, object>()
            {
                {"ClassID",entity.ClassID} ,
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel},
                 {"ExaminationID",entity.ExaminationID},
                  {"ExaminationSubjectID",entity.ExaminationSubjectID}
            });
        }


        /// <summary>
        /// InsertDSTS
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="Data">The data.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   11:09 AM
        /// </remarks>
        public ProcessedReport InsertDSTS(CandidateBO entity, Stream Data)
        {
            if (entity.ClassID.HasValue)
            {
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            string reportCode = SystemParamsInFile.QLT_DSTS_THICHATLUONGHOCKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //Khởi tạo đối tượng ProcessedReport:
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = SystemParamsInFile.QLT_DSTS_THICHATLUONGHOCKY;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string ClassName = "";
            if (entity.ClassID.HasValue && entity.ClassID.Value != 0)
            {
                ClassProfile cp = ClassProfileRepository.Find(entity.ClassID);
                ClassName = cp.DisplayName;
            }
            else
            {
                ClassName = "TC";
            }
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);

            Examination exam = ExaminationBusiness.Find(entity.ExaminationID);
            outputNamePattern = outputNamePattern.Replace("[Examination]", ReportUtils.StripVNSign(exam.Title));

            EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", edu != null ? edu.Resolution : "TC");

            ExaminationSubject es = ExaminationSubjectBusiness.Find(entity.ExaminationSubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectCat.DisplayName) : "TC");

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel},
                 {"ExaminationID",entity.ExaminationID},
                  {"ExaminationSubjectID",entity.ExaminationSubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }


        /// <summary>
        /// GetDSTS
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   11:14 AM
        /// </remarks>
        public ProcessedReport GetDSTS(CandidateBO entity)
        {
            string reportCode = SystemParamsInFile.QLT_DSTS_THICHATLUONGHOCKY;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// CreateCandidateBO
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   11:14 AM
        /// </remarks>
        public Stream CreateDSTS(CandidateBO entity, out string fileName)
        {
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            if (entity.ClassID.HasValue)
            {
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            string reportCode = SystemParamsInFile.QLT_DSTS_THICHATLUONGHOCKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            // Lấy tên file báo cáo
            string outputNamePattern = reportDef.OutputNamePattern;
            string ClassName = "";
            if (entity.ClassID.HasValue && entity.ClassID.Value != 0)
            {
                ClassProfile cp = ClassProfileRepository.Find(entity.ClassID);
                ClassName = cp.DisplayName;
            }
            else
            {
                ClassName = "TC";
            }
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
            Examination exam = ExaminationBusiness.Find(entity.ExaminationID);
            outputNamePattern = outputNamePattern.Replace("[Examination]", ReportUtils.StripVNSign(exam.Title));

            EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", edu != null ? edu.Resolution : "TC");

            ExaminationSubject es = ExaminationSubjectBusiness.Find(entity.ExaminationSubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectCat.DisplayName) : "TC");

            fileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplate = oBook.GetSheet(2);
            int firstRow = 10;
            IVTRange headRange = sheetTemplate.GetRange("A1", "G9");
            IVTRange copyRange = sheetTemplate.GetRange("A10", "G10");
            IVTRange bghRange = sheetTemplate.GetRange("A12", "G12");
            sheet.CopyPasteSameSize(headRange, 1, 1);
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();
            //
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = entity.AcademicYearID;
            SearchInfo["AppliedLevel"] = entity.AppliedLevel;
            SearchInfo["ExaminationID"] = entity.ExaminationID;
            SearchInfo["EducationLevelID"] = entity.EducationLevel;
            SearchInfo["ClassID"] = entity.ClassID;
            SearchInfo["ExaminationSubjectID"] = entity.ExaminationSubjectID;
            SearchInfo["PupilName"] = entity.FullName;
            SearchInfo["PupilCode"] = entity.PupilCode;
            SearchInfo["Genre"] = entity.Genre;
            SearchInfo["SchoolID"] = entity.SchoolID;
            IQueryable<Candidate> query = Search(SearchInfo);
            var listTemp = (from c in query
                            join pp in PupilProfileBusiness.All on c.PupilID equals pp.PupilProfileID
                            join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                            where cp.IsActive.Value
                            select new CandidateBO()
                            {
                                PupilID = c.PupilID,
                                FullName = pp.FullName,
                                PupilName = pp.Name,
                                PupilCode = pp.PupilCode,
                                BirthDate = pp.BirthDate,
                                BirthPlace = pp.BirthPlace,
                                EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null,
                                OrgGenre = pp.Genre,
                                ClassName = cp.DisplayName,
                                NamedListCode = c.NamedListCode,
                                NamedListNumber = c.NamedListNumber
                            }).ToList();
            List<CandidateBO> list = new List<CandidateBO>();
            if (exam.UsingSeparateList == 0)
            {
                listTemp.ForEach(u =>
                {
                    if (!list.Select(o => o.PupilID).Contains(u.PupilID))
                    {
                        list.Add(u);
                    }
                });
            }
            else
            {
                list.AddRange(listTemp);
            }
            list.Sort();
            for (int i = 0; i < list.Count; i++)
            {
                sheet.CopyPasteSameSize(copyRange, 10 + i, 1);
                CandidateBO thisCandidate = list[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["PupilCode"] = thisCandidate.PupilCode;
                princeDic["FullName"] = thisCandidate.FullName;
                princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisCandidate.BirthDate);
                princeDic["Genre"] = thisCandidate.OrgGenre == 1 ? "Nam" : "Nữ";
                princeDic["Class"] = thisCandidate.ClassName;
                princeDic["BirthPlace"] = thisCandidate.BirthPlace;
                QueenDic.Add(princeDic);
            }

            //
            //SupervisingDept sd = SupervisingDeptBusiness.Find(entity.SupervisingDeptID);

            SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
            SupervisingDept sd = sp.SupervisingDept;
            string year = "Năm học {0} - {1}";
            string date = sp.Province.ProvinceName + ",ngày {0} tháng {1} năm {2}";
            string dateTitle = string.Format(date, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
            string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));
            EducationLevel el = EducationLevelBusiness.Find(entity.EducationLevel);
            string educationAndSubject = string.Format("Khối:{0},môn thi:{1}", el != null ? el.Resolution : "Tất cả", (es != null) ? es.SubjectCat.DisplayName : "Tất cả");
            Examination examinationName = ExaminationBusiness.Find(entity.ExaminationID);
            string examName = examinationName.Title;
            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SupervisingDeptName", sd.SupervisingDeptName);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("TitleAndSubjectName", yearTitle);
            KingDic.Add("EducationLevel", educationAndSubject);
            KingDic.Add("ProvinceNameAndDate", dateTitle);
            KingDic.Add("ExaminationName", examName);
            int lastRow = firstRow + list.Count;
            sheet.GetRange(1, 1, lastRow, 7).FillVariableValue(KingDic);

            sheet.CopyPasteSameSize(bghRange, lastRow + 1, 1);
            sheetTemplate.Delete();

            return oBook.ToStream();
        }


        /// <summary>
        /// InsertSBD
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="Data">The data.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   11:14 AM
        /// </remarks>
        public ProcessedReport InsertSBD(CandidateBO entity, Stream Data)
        {
            if (entity.ClassID.HasValue)
            {
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            string reportCode = SystemParamsInFile.QLT_SBD_THICHATLUONGHOCKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //Khởi tạo đối tượng ProcessedReport:
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = SystemParamsInFile.QLT_SBD_THICHATLUONGHOCKY;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            Examination exam = ExaminationBusiness.Find(entity.ExaminationID);
            outputNamePattern = outputNamePattern.Replace("[Examination]", ReportUtils.StripVNSign(exam.Title));

            EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", edu != null ? edu.Resolution : "TC");

            ExaminationSubject es = ExaminationSubjectBusiness.Find(entity.ExaminationSubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectCat.DisplayName) : "TC");

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel},
                 {"ExaminationID",entity.ExaminationID},
                  {"ExaminationSubjectID",entity.ExaminationSubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// <summary>
        /// GetDSTS
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   11:11 AM
        /// </remarks>
        public ProcessedReport GetSBD(CandidateBO entity)
        {
            string reportCode = SystemParamsInFile.QLT_SBD_THICHATLUONGHOCKY;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// CreateSBD
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 11/12/2012   11:14 AM
        /// </remarks>
        public Stream CreateSBD(CandidateBO entity, out string fileName)
        {
            if (entity.ClassID.HasValue)
            {
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                        new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            string reportCode = SystemParamsInFile.QLT_SBD_THICHATLUONGHOCKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            // Lấy tên báo cáo
            string outputNamePattern = reportDef.OutputNamePattern;

            Examination exam = ExaminationBusiness.Find(entity.ExaminationID);
            outputNamePattern = outputNamePattern.Replace("[Examination]", ReportUtils.StripVNSign(exam.Title));

            EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", edu != null ? edu.Resolution : "TC");

            ExaminationSubject es = ExaminationSubjectBusiness.Find(entity.ExaminationSubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectCat.DisplayName) : "TC");

            fileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplate = oBook.GetSheet(2);
            int firstRow = 10;
            IVTRange headRange = sheetTemplate.GetRange("A1", "G9");
            IVTRange copyRange = sheetTemplate.GetRange("A10", "G10");
            IVTRange bghRange = sheetTemplate.GetRange("A12", "G12");
            sheet.CopyPasteSameSize(headRange, 1, 1);
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();
            //
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = entity.AcademicYearID;
            SearchInfo["AppliedLevel"] = entity.AppliedLevel;
            SearchInfo["ExaminationID"] = entity.ExaminationID;
            SearchInfo["EducationLevelID"] = entity.EducationLevel;
            SearchInfo["ExaminationSubjectID"] = entity.ExaminationSubjectID;
            SearchInfo["SchoolID"] = entity.SchoolID;

            IQueryable<Candidate> query = Search(SearchInfo);
            var listTemp = (from c in query
                            join pp in PupilProfileBusiness.All on c.PupilID equals pp.PupilProfileID
                            join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                            where cp.IsActive.Value
                            select new CandidateBO()
                            {
                                PupilID = c.PupilID,
                                NamedListCode = c.NamedListCode,
                                NamedListNumber = c.NamedListNumber,
                                FullName = pp.FullName,
                                PupilName = pp.Name,
                                BirthDate = pp.BirthDate,
                                EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null,
                                ClassName = cp.DisplayName,
                                OrgGenre = pp.Genre,
                                BirthPlace = pp.BirthPlace
                            })
                            .ToList();
            listTemp.Sort();
            List<CandidateBO> list = new List<CandidateBO>();
            if (exam.UsingSeparateList == 0)
            {
                listTemp.ForEach(u =>
                {
                    if (!list.Select(o => o.PupilID).Contains(u.PupilID))
                    {
                        list.Add(u);
                    }
                });
            }
            else
            {
                list.AddRange(listTemp);
            }
            for (int i = 0; i < list.Count; i++)
            {
                sheet.CopyPasteSameSize(copyRange, 10 + i, 1);
                CandidateBO thisCandidate = list[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["SBD"] = thisCandidate.NamedListCode;
                princeDic["FullName"] = thisCandidate.FullName;
                princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisCandidate.BirthDate);
                princeDic["Genre"] = thisCandidate.OrgGenre == 1 ? "Nam" : "Nữ";
                princeDic["Class"] = thisCandidate.ClassName;
                princeDic["BirthPlace"] = thisCandidate.BirthPlace;
                QueenDic.Add(princeDic);
            }

            //
            //SupervisingDept sd = SupervisingDeptBusiness.Find(entity.SupervisingDeptID);

            SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
            SupervisingDept sd = sp.SupervisingDept;
            string year = "Năm học {0} - {1}";
            string date = sp.Province.ProvinceName + ",ngày {0} tháng {1} năm {2}";
            string dateTitle = string.Format(date, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
            string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));
            EducationLevel el = EducationLevelBusiness.Find(entity.EducationLevel);
            string educationAndSubject = string.Format("Khối:{0},môn thi:{1}", (el != null) ? el.Resolution : "Tất cả", es != null ? es.SubjectCat.DisplayName : "Tất cả");
            string examName = exam.Title;

            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SupervisingDeptName", sd.SupervisingDeptName);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("TitleAndSubjectName", yearTitle);
            KingDic.Add("EducationLevel", educationAndSubject);
            KingDic.Add("ProvinceNameAndDate", dateTitle);
            KingDic.Add("ExaminationName", examName);
            int lastRow = firstRow + list.Count;
            sheet.GetRange(1, 1, lastRow, 7).FillVariableValue(KingDic);
            sheet.CopyPasteSameSize(bghRange, lastRow + 1, 1);

            sheetTemplate.Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// InsertSearchCandidateMark
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="Data">The data.</param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 13/12/2012   3:57 PM
        /// </remarks>
        public ProcessedReport InsertSearchCandidateMark(CandidateBO entity, Stream Data)
        {
            string reportCode = SystemParamsInFile.THI_KETQUATHIMON;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //Khởi tạo đối tượng ProcessedReport:
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = SystemParamsInFile.THI_KETQUATHIMON;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string ClassName = "";
            if (entity.ClassID.HasValue && entity.ClassID.Value != 0)
            {
                ClassProfile cp = ClassProfileRepository.Find(entity.ClassID);
                ClassName = cp.DisplayName;
            }
            else
            {
                ClassName = "TC";
            }
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);

            Examination exam = ExaminationBusiness.Find(entity.ExaminationID);
            outputNamePattern = outputNamePattern.Replace("[Examination]", ReportUtils.StripVNSign(exam.Title));

            EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", edu != null ? edu.Resolution : "TC");

            ExaminationSubject es = ExaminationSubjectBusiness.Find(entity.ExaminationSubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectCat.DisplayName) : "TC");

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"EducationLevel",entity.EducationLevel} ,
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel},
                 {"ExaminationID",entity.ExaminationID},
                  {"ExaminationSubjectID",entity.ExaminationSubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// <summary>
        /// GetSearchCandidateMark
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 13/12/2012   3:57 PM
        /// </remarks>
        public ProcessedReport GetSearchCandidateMark(CandidateBO entity)
        {
            string reportCode = SystemParamsInFile.THI_KETQUATHIMON;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// CreateSearchCandidateMark
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 13/12/2012   3:58 PM
        /// </remarks>
        public Stream CreateSearchCandidateMark(CandidateBO entity, out string fileName)
        {
            string reportCode = SystemParamsInFile.THI_KETQUATHIMON;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string ClassName = "";
            if (entity.ClassID.HasValue && entity.ClassID.Value != 0)
            {
                ClassProfile cp = ClassProfileRepository.Find(entity.ClassID);
                ClassName = cp.DisplayName;
            }
            else
            {
                ClassName = "TC";
            }
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);

            Examination exam = ExaminationBusiness.Find(entity.ExaminationID);
            outputNamePattern = outputNamePattern.Replace("[Examination]", ReportUtils.StripVNSign(exam.Title));

            EducationLevel el = EducationLevelBusiness.Find(entity.EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", el != null ? el.Resolution : "TC");

            ExaminationSubject es = ExaminationSubjectBusiness.Find(entity.ExaminationSubjectID);
            outputNamePattern = outputNamePattern.Replace("[Subject]", es != null ? ReportUtils.StripVNSign(es.SubjectCat.DisplayName) : "TC");

            fileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetTemplate = oBook.GetSheet(2);
           
            int firstRowIdx = 10;
            IVTRange firstRow = sheetTemplate.GetRow(firstRowIdx);
             IVTRange templateRange = null;
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();
            //
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = entity.AcademicYearID;
            SearchInfo["ExaminationID"] = entity.ExaminationID;
            SearchInfo["EducationLevelID"] = entity.EducationLevel;
            SearchInfo["ClassID"] = entity.ClassID;
            SearchInfo["ExaminationSubjectID"] = entity.ExaminationSubjectID;
            SearchInfo["RoomID"] = entity.RoomID;
            SearchInfo["PupilName"] = entity.FullName;
            SearchInfo["PupilCode"] = entity.PupilCode;
            SearchInfo["NamedListCode"] = entity.NamedListCode;

            List<CandidateMark> query = SearchMark(entity.SchoolID, (int)entity.AppliedLevel, SearchInfo)
                .OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.PupilName)
                .ToList();

            if (query != null && query.Count > 0)
            {
                templateRange = sheetTemplate.GetRange("A1", "J" + (query.Count + firstRowIdx));
            }
            else
            {
                templateRange = sheetTemplate.GetRange("A1", "J75");
            }

            for (int i = 0; i < query.Count(); i++)
            {
                CandidateMark thisCandidate = query[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["PupilCode"] = thisCandidate.PupilCode;
                princeDic["FullName"] = thisCandidate.PupilName;

                princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisCandidate.BirthDate);
                princeDic["Class"] = thisCandidate.ClassName;

                princeDic["Room"] = thisCandidate.RoomName;
                princeDic["SBD"] = thisCandidate.NamedListCode;

                princeDic["Mark"] = thisCandidate.ExamMark;
                princeDic["RealMark"] = thisCandidate.RealMark;
                princeDic["Description"] = thisCandidate.Description;

                QueenDic.Add(princeDic);
            }

            //
            SupervisingDept sd = SupervisingDeptBusiness.Find(entity.SupervisingDeptID);

            SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
            //string year = "Năm học {0} - {1}";
            string date = sd.Province.ProvinceName + ",ngày {0} tháng {1} năm {2}";
            string dateTitle = string.Format(date, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
            //string yearTitle = string.Format(year, ay.Year, (ay.Year + 1));
            string semesterAndYear = string.Format("{0} NĂM HỌC {1}-{2}", exam.Title.ToUpper(), ay.Year, ay.Year + 1);
            string educationAndSubject = "KẾT QUẢ ĐIỂM THI";
            if (es != null && es.SubjectCat != null && !string.IsNullOrWhiteSpace(es.SubjectCat.DisplayName))
            {
                educationAndSubject += " " + es.SubjectCat.DisplayName.ToUpper();
            }
            if (es != null && el.Resolution != null)
            {
                educationAndSubject += " " + el.Resolution.ToUpper();
            }

            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SupervisingDeptName", sd.SupervisingDeptName);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("ProvinceNameAndDate", dateTitle);

            KingDic.Add("TitleAndSubjectNameAndClass", educationAndSubject);
            KingDic.Add("SemesterAndYear", semesterAndYear);

            templateRange.FillVariableValue(KingDic);
            sheet.CopyPasteSameRowHeigh(templateRange, 1, VTVector.dic['A']);
            // copy style
            for (int i = 1; i < query.Count(); i++)
            {
                // add style
                sheet.CopyPasteSameRowHeigh(firstRow, i + firstRowIdx, true);
            }
            sheetTemplate.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}
