/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class SMSParentContractInClassDetailBusiness
    {
        public IQueryable<SMS_PARENT_CONTRACT_CLASS> Search(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int Year = Utils.GetInt(dic, "Year");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            IQueryable<SMS_PARENT_CONTRACT_CLASS> iquery = this.All;
            if (AcademicYearID > 0)
            {
                iquery = iquery.Where(p => p.ACADEMIC_YEAR_ID == AcademicYearID);
            }
            if (SchoolID > 0)
            {

                iquery = iquery.Where(p => p.SCHOOL_ID == SchoolID);


            }
            if (ClassID > 0)
            {
                iquery = iquery.Where(p => p.CLASS_ID == ClassID);
            }
            if (Year > 0)
            {
                iquery = iquery.Where(p => p.YEAR == Year);
            }
            if (SemesterID > 0)
            {
                iquery = iquery.Where(p => p.SEMESTER == SemesterID);
            }
            if (lstClassID.Count > 0)
            {
                iquery = iquery.Where(p => lstClassID.Contains(p.CLASS_ID));
            }
            return iquery;
        }

        public SMS_PARENT_CONTRACT_CLASS GetMessageBudgetDetailInClass(int schoolID, int academicYearID, int classID, int semester)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);

            SMS_PARENT_CONTRACT_CLASS obj = this.All.FirstOrDefault(o => o.SCHOOL_ID == schoolID &&
                o.ACADEMIC_YEAR_ID == academicYearID && o.CLASS_ID == classID && o.YEAR == academicYear.Year && o.SEMESTER == semester);
            if (obj != null) return obj;
            else
            {
                //danh sach hoc sinh dang hoc trong lop
                var lstPupilProfile = PupilProfileBusiness.GetListPupilByClassID(schoolID, academicYearID, classID)
                                                            .Select(o => o.PupilProfileID).ToList();
                SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
                {
                    SchoolId = schoolID,
                    //Semester = semester,
                    Year = academicYear.Year
                };
                IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);


                // tinh so luong tin nhan trong quy tin cua lop                
                var LstPupilContractInClass = (from sc in this.SMSParentContractBusiness.All
                                               join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                               join spd in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                               join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                               join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                               join pp in PupilProfileBusiness.All on sc.PUPIL_ID equals pp.PupilProfileID
                                               from x in gj.DefaultIfEmpty(null)
                                               where sc.SCHOOL_ID == schoolID
                                                  && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                                  && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                                  && lstPupilProfile.Contains(sc.PUPIL_ID)
                                                  && (scd.SUBSCRIPTION_TIME == semester)
                                                  && scd.YEAR_ID == academicYear.Year
                                                  && spdd.YEAR == academicYear.Year
                                                  && spdd.STATUS == true
                                                  && spdd.IS_LIMIT != true
                                                  && pp.IsActive
                                               select new
                                               {
                                                   PupilFileID = sc.PUPIL_ID,
                                                   SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                   MaxSMS = (spd.MAX_SMS.HasValue) ? semester == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
                                                   SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                   ServicePackageID = spd.SERVICE_PACKAGE_ID,
                                               })
                                                .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID, x.SentSMS })
                                                .Select(y => new PupilContractBO
                                                {
                                                    PupilID = y.Key.PupilFileID,
                                                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                    ServicePackageID = y.Key.ServicePackageID,
                                                    SentSMS = y.Key.SentSMS,
                                                    TotalSMS = y.Sum(a => a.MaxSMS),
                                                }).ToList();

                var LstPupilContractInClassExtra = (from sc in this.SMSParentContractBusiness.All
                                                    join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                    join scde in this.SMSParentContractExtraBusiness.All on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals scde.SMS_PARENT_CONTRACT_DETAIL_ID
                                                    join spd in this.ServicePackageDetailBusiness.All on scde.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                    join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                    join pp in PupilProfileBusiness.All on sc.PUPIL_ID equals pp.PupilProfileID
                                                    where sc.SCHOOL_ID == schoolID
                                                       && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                                       && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                                       && lstPupilProfile.Contains(sc.PUPIL_ID)
                                                       && (scd.SUBSCRIPTION_TIME == semester)
                                                       && scd.YEAR_ID == academicYear.Year
                                                       && spdd.YEAR == academicYear.Year
                                                       && spdd.STATUS == true
                                                       && spdd.IS_LIMIT != true
                                                       && pp.IsActive
                                                    select new
                                                    {
                                                        PupilFileID = sc.PUPIL_ID,
                                                        SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                        MaxSMS = (spd.MAX_SMS.HasValue) ? semester == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
                                                        ServicePackageID = spd.SERVICE_PACKAGE_ID,
                                                    })
                                                .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID })
                                                .Select(y => new PupilContractBO
                                                {
                                                    PupilID = y.Key.PupilFileID,
                                                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                    ServicePackageID = y.Key.ServicePackageID,
                                                    TotalSMS = y.Sum(a => a.MaxSMS),
                                                }).ToList();

                // chi them chi tiet khi lop co hop dong hoc sinh
                if ((LstPupilContractInClass != null && LstPupilContractInClass.Count > 0) || LstPupilContractInClassExtra.Count > 0)
                {
                    int TotalSMSInClass = LstPupilContractInClass.Sum(o => o.TotalSMS);
                    int TotalSMSInClassExtra = LstPupilContractInClassExtra.Sum(o => o.TotalSMS);
                    TotalSMSInClass = TotalSMSInClass + TotalSMSInClassExtra;
                    int TotalSMSSent = LstPupilContractInClass.Sum(o => o.SentSMS);

                    SMS_PARENT_CONTRACT_CLASS objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                    objClassDetail.CLASS_ID = classID;
                    objClassDetail.ACADEMIC_YEAR_ID = academicYearID;
                    objClassDetail.SCHOOL_ID = schoolID;
                    objClassDetail.YEAR = academicYear.Year;
                    objClassDetail.SEMESTER = (byte)semester;
                    objClassDetail.SENT_TOTAL = TotalSMSSent;
                    objClassDetail.TOTAL_SMS = TotalSMSInClass;
                    objClassDetail.CREATED_DATE = DateTime.Now;
                    this.Insert(objClassDetail);
                    this.Save();
                    return objClassDetail;
                }
                else
                    return new SMS_PARENT_CONTRACT_CLASS
                    {
                        TOTAL_SMS = 0,
                        SENT_TOTAL = 0
                    };
            }
        }

        public List<SMSParentContractClassBO> GetListSMSParentContractClass(int SchoolID, int AcademicYearID, int SemesterID, List<int> lstClassID)
        {
            List<SMSParentContractClassBO> lstSMSParentContractClass = new List<SMSParentContractClassBO>();
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            List<SMS_PARENT_CONTRACT_CLASS> lstSMSParentContractClassDB = new List<SMS_PARENT_CONTRACT_CLASS>();
            SMS_PARENT_CONTRACT_CLASS objDB = null;
            IDictionary<string, object> dicSearchSMSParentContractClass = new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"SchoolID",SchoolID},
                {"lstClassID",lstClassID},
                {"SemesterID",SemesterID}
            };
            lstSMSParentContractClassDB = SMSParentContractInClassDetailBusiness.Search(dicSearchSMSParentContractClass).ToList();
            List<int> lstPCCDB = lstSMSParentContractClassDB.Select(p => p.CLASS_ID).Distinct().ToList();
            List<int> lstClassNotinCICD = lstClassID.Except(lstPCCDB).ToList();
            SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
            {
                SchoolId = SchoolID,
                Year = academicYear.Year
            };
            int paritionId = UtilsBusiness.GetPartionId(SchoolID);
            IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);
            // tinh so luong tin nhan trong quy tin cua lop                
            var LstPupilContractInClass = (from sc in this.SMSParentContractBusiness.All
                                           join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                           join spd in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                           join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                           join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                           join pp in PupilProfileBusiness.All on sc.PUPIL_ID equals pp.PupilProfileID
                                           join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                           from x in gj.DefaultIfEmpty(null)
                                           where sc.SCHOOL_ID == SchoolID
                                              && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                              && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                              && lstClassNotinCICD.Contains(sc.CLASS_ID)
                                              && (scd.SUBSCRIPTION_TIME == SemesterID)
                                              && scd.ACADEMIC_YEAR_ID == academicYear.AcademicYearID
                                              && sc.ACADEMIC_YEAR_ID == academicYear.AcademicYearID
                                              && spdd.YEAR == academicYear.Year
                                              && spdd.STATUS == true
                                              && spdd.IS_LIMIT != true
                                              && pp.IsActive
                                              && sc.PARTITION_ID == paritionId
                                              && scd.PARTITION_ID == paritionId
                                              && poc.AcademicYearID == academicYear.AcademicYearID
                                              && scd.CLASS_ID == poc.ClassID
                                              && sc.CLASS_ID == poc.ClassID
                                              && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                           select new
                                           {
                                               ClassID = sc.CLASS_ID,
                                               PupilFileID = sc.PUPIL_ID,
                                               SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                               MaxSMS = (spd.MAX_SMS.HasValue) ? SemesterID == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
                                               SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                               ServicePackageID = spd.SERVICE_PACKAGE_ID,
                                           })
                                            .GroupBy(x => new { x.ClassID, x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID, x.SentSMS })
                                            .Select(y => new PupilContractBO
                                            {
                                                ClassID = y.Key.ClassID,
                                                PupilID = y.Key.PupilFileID,
                                                SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                ServicePackageID = y.Key.ServicePackageID,
                                                SentSMS = y.Key.SentSMS,
                                                TotalSMS = y.Sum(a => a.MaxSMS),
                                            }).ToList();

            var LstPupilContractInClassExtra = (from sc in this.SMSParentContractBusiness.All
                                                join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                join scde in this.SMSParentContractExtraBusiness.All on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals scde.SMS_PARENT_CONTRACT_DETAIL_ID
                                                join spd in this.ServicePackageDetailBusiness.All on scde.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                join pp in PupilProfileBusiness.All on sc.PUPIL_ID equals pp.PupilProfileID
                                                join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                                where sc.SCHOOL_ID == SchoolID
                                                   && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                                   && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                                   && lstClassNotinCICD.Contains(sc.CLASS_ID)
                                                   && (scd.SUBSCRIPTION_TIME == SemesterID)
                                                   && scd.ACADEMIC_YEAR_ID == academicYear.AcademicYearID
                                                   && sc.ACADEMIC_YEAR_ID == academicYear.AcademicYearID
                                                   && spdd.YEAR == academicYear.Year
                                                   && spdd.STATUS == true
                                                   && spdd.IS_LIMIT != true
                                                   && pp.IsActive
                                                   && sc.PARTITION_ID == paritionId
                                                   && scd.PARTITION_ID == paritionId
                                                    && scd.CLASS_ID == poc.ClassID
                                                   && sc.CLASS_ID == poc.ClassID
                                                   && poc.AcademicYearID == academicYear.AcademicYearID
                                                   && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                select new
                                                {
                                                    ClassID = sc.CLASS_ID,
                                                    PupilFileID = sc.PUPIL_ID,
                                                    SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                    MaxSMS = (spd.MAX_SMS.HasValue) ? SemesterID == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
                                                    ServicePackageID = spd.SERVICE_PACKAGE_ID,
                                                })
                                            .GroupBy(x => new { x.ClassID, x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID })
                                            .Select(y => new PupilContractBO
                                            {
                                                ClassID = y.Key.ClassID,
                                                PupilID = y.Key.PupilFileID,
                                                SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                ServicePackageID = y.Key.ServicePackageID,
                                                TotalSMS = y.Sum(a => a.MaxSMS),
                                            }).ToList();

            int ClassID = 0;
            for (int i = 0; i < lstClassID.Count; i++)
            {
                ClassID = lstClassID[i];
                objDB = lstSMSParentContractClassDB.Where(p => p.CLASS_ID == ClassID).FirstOrDefault();
                if (objDB != null)
                {
                    SMSParentContractClassBO objClassDetail = new SMSParentContractClassBO();
                    objClassDetail.CLASS_ID = ClassID;
                    objClassDetail.ACADEMIC_YEAR_ID = AcademicYearID;
                    objClassDetail.SCHOOL_ID = SchoolID;
                    objClassDetail.YEAR = academicYear.Year;
                    objClassDetail.SEMESTER = (byte)SemesterID;
                    objClassDetail.SENT_TOTAL = objDB.SENT_TOTAL;
                    objClassDetail.TOTAL_SMS = objDB.TOTAL_SMS;
                    objClassDetail.CREATED_DATE = DateTime.Now;
                    lstSMSParentContractClass.Add(objClassDetail);
                }
                else
                {
                    var LstPupilContractInClasstmp = LstPupilContractInClass.Where(p => p.ClassID == ClassID).ToList();
                    var LstPupilContractInClassExtratmp = LstPupilContractInClassExtra.Where(p => p.ClassID == ClassID).ToList();
                    // chi them chi tiet khi lop co hop dong hoc sinh
                    if ((LstPupilContractInClasstmp.Count > 0) || LstPupilContractInClassExtratmp.Count > 0)
                    {
                        int TotalSMSInClass = LstPupilContractInClasstmp.Sum(o => o.TotalSMS);
                        int TotalSMSInClassExtra = LstPupilContractInClassExtratmp.Sum(o => o.TotalSMS);
                        TotalSMSInClass = TotalSMSInClass + TotalSMSInClassExtra;
                        int TotalSMSSent = LstPupilContractInClasstmp.Sum(o => o.SentSMS);

                        SMS_PARENT_CONTRACT_CLASS objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                        objClassDetail.CLASS_ID = ClassID;
                        objClassDetail.ACADEMIC_YEAR_ID = AcademicYearID;
                        objClassDetail.SCHOOL_ID = SchoolID;
                        objClassDetail.YEAR = academicYear.Year;
                        objClassDetail.SEMESTER = (byte)SemesterID;
                        objClassDetail.SENT_TOTAL = TotalSMSSent;
                        objClassDetail.TOTAL_SMS = TotalSMSInClass;
                        objClassDetail.CREATED_DATE = DateTime.Now;
                        this.Insert(objClassDetail);

                        SMSParentContractClassBO objClassDetailBO = new SMSParentContractClassBO();
                        objClassDetailBO.CLASS_ID = ClassID;
                        objClassDetailBO.ACADEMIC_YEAR_ID = AcademicYearID;
                        objClassDetailBO.SCHOOL_ID = SchoolID;
                        objClassDetailBO.YEAR = academicYear.Year;
                        objClassDetailBO.SEMESTER = (byte)SemesterID;
                        objClassDetailBO.SENT_TOTAL = TotalSMSSent;
                        objClassDetailBO.TOTAL_SMS = TotalSMSInClass;
                        objClassDetailBO.CREATED_DATE = DateTime.Now;
                        lstSMSParentContractClass.Add(objClassDetailBO);
                    }
                }
            }
            SMSParentContractInClassDetailBusiness.Save();
            return lstSMSParentContractClass;
        }

        public List<SMSParentContractClassBO> GetListPhoneByListClass(int SchoolID, int AcademicYearID, List<int> lstClassID, int SemesterID)
        {
            List<SMSParentContractClassBO> lstResult = new List<SMSParentContractClassBO>();
            int partitionID = UtilsBusiness.GetPartionId(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            lstResult = (from sc in this.SMSParentContractBusiness.All
                         join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                         join spd in ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                         join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                         join pp in PupilProfileBusiness.All on sc.PUPIL_ID equals pp.PupilProfileID
                         join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                         where sc.SCHOOL_ID == SchoolID
                         && sc.ACADEMIC_YEAR_ID == objAy.AcademicYearID
                         && sc.PARTITION_ID == partitionID
                         && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                         && sc.ACADEMIC_YEAR_ID == AcademicYearID
                         && lstClassID.Contains(sc.CLASS_ID)
                         && pp.IsActive == true
                         && poc.AcademicYearID == AcademicYearID
                         && poc.ClassID == sc.CLASS_ID
                         && poc.ClassID == scd.CLASS_ID
                         && sc.PARTITION_ID == partitionID
                         && scd.PARTITION_ID == partitionID
                         && (poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED || poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                         &&
                         (
                            (SemesterID == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST || scd.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL))
                            ||
                            (SemesterID == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND || scd.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL))
                         )
                         && spdd.YEAR == objAy.Year
                         && spdd.STATUS == true
                         select new SMSParentContractClassBO
                         {
                             CLASS_ID = sc.CLASS_ID,
                             PUPIL_ID = sc.PUPIL_ID,
                             STATUS = scd.SUBSCRIPTION_STATUS
                         }).ToList();

            return lstResult;
        }

        //public List<SMSParentContractClassBO> GetListSMSParentContractClass(int SchoolID, int AcademicYearID, int SemesterID, List<int> lstClassID)
        //{
        //    try
        //    {
        //        List<SMSParentContractClassBO> lstSMSParentContractClass = new List<SMSParentContractClassBO>();
        //        SMS_PARENT_CONTRACT_CLASS objSMSParentContractClass = null;
        //        AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
        //        SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
        //        {
        //            SchoolId = SchoolID,
        //            Year = objAy.Year
        //        };
        //        IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);

        //        // tinh so luong tin nhan trong quy tin cua lop                
        //        var LstPupilContractInClass = (from sc in SMSParentContractBusiness.All
        //                                       join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
        //                                       join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
        //                                       join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
        //                                       join spd in ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
        //                                       join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
        //                                       join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
        //                                       from x in gj.DefaultIfEmpty(null)
        //                                       where sc.SCHOOL_ID == SchoolID
        //                                          && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID
        //                                                  || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_STOP_USING || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID
        //                                                  || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL)
        //                                          && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
        //                                          && lstClassID.Contains(sc.CLASS_ID)
        //                                          && (scd.SUBSCRIPTION_TIME == SemesterID)
        //                                          && scd.YEAR_ID == objAy.Year
        //                                          && spdd.YEAR == objAy.Year
        //                                          && spdd.STATUS == true
        //                                          && pf.IsActive
        //                                          && sc.CLASS_ID == poc.ClassID
        //                                          && poc.IsRegisterContract == true
        //                                       select new
        //                                       {
        //                                           ClassID = sc.CLASS_ID,
        //                                           MaxSMS = (spd.MAX_SMS.HasValue && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED && spdd.IS_LIMIT != true) ? SemesterID == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
        //                                           SentSMS = ((x == null) ? 0 : x.SENT_TOTAL),
        //                                           PupilID = sc.PUPIL_ID,
        //                                           SUBSCRIPTION_STATUS = scd.SUBSCRIPTION_STATUS
        //                                       }).Distinct().ToList();
        //        var lstContractGroup = (from lg in LstPupilContractInClass
        //                                group lg by new
        //                                {
        //                                    lg.ClassID
        //                                } into g
        //                                select new
        //                                {
        //                                    ClassID = g.Key.ClassID,
        //                                    SentSMS = g.Sum(a => a.SentSMS),
        //                                    TotalSMS = g.Sum(a => a.MaxSMS),
        //                                }).ToList();
        //        var LstPupilContractInClassExtra = (from sc in SMSParentContractBusiness.All
        //                                            join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
        //                                            join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
        //                                            join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
        //                                            join scde in SMSParentContractExtraBusiness.All on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals scde.SMS_PARENT_CONTRACT_DETAIL_ID
        //                                            join spd in ServicePackageDetailBusiness.All on scde.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
        //                                            join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
        //                                            where sc.SCHOOL_ID == SchoolID
        //                                              && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
        //                                              && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
        //                                              && lstClassID.Contains(sc.CLASS_ID)
        //                                              && (scd.SUBSCRIPTION_TIME == SemesterID)
        //                                              && scd.YEAR_ID == objAy.Year
        //                                              && spdd.YEAR == objAy.Year
        //                                              && spdd.STATUS == true
        //                                              && spdd.IS_LIMIT != true
        //                                              && pf.IsActive
        //                                              && sc.CLASS_ID == poc.ClassID
        //                                              && poc.IsRegisterContract == true
        //                                            select new
        //                                            {
        //                                                ClassID = sc.CLASS_ID,
        //                                                MaxSMS = (spd.MAX_SMS.HasValue) ? SemesterID == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0
        //                                            })
        //                                        .GroupBy(x => new { x.ClassID, x.MaxSMS })
        //                                        .Select(y => new
        //                                        {
        //                                            ClassID = y.Key.ClassID,
        //                                            TotalSMS = y.Key.MaxSMS
        //                                        }).ToList();
        //        int ClassID = 0;

        //        List<SMS_PARENT_CONTRACT_CLASS> lstSMSParentContractClassDB = new List<SMS_PARENT_CONTRACT_CLASS>();
        //        IDictionary<string, object> dicSearchSMSParentContractClass = new Dictionary<string, object>()
        //        {
        //            {"AcademicYearID",AcademicYearID},
        //            {"SchoolID",SchoolID},
        //            {"lstClassID",lstClassID},
        //            {"SemesterID",SemesterID}
        //        };
        //        lstSMSParentContractClassDB = SMSParentContractInClassDetailBusiness.Search(dicSearchSMSParentContractClass).ToList();
        //        SMS_PARENT_CONTRACT_CLASS objDB = null;
        //        int countTotal = 0;
        //        int countWithStatus = 0;
        //        bool isChange = false;
        //        for (int i = 0; i < lstClassID.Count; i++)
        //        {
        //            ClassID = lstClassID[i];
        //            objSMSParentContractClass = new SMS_PARENT_CONTRACT_CLASS();
        //            var objPupilContractInClass = lstContractGroup.Where(p => p.ClassID == ClassID).FirstOrDefault();
        //            var objPupilContractInClassExtra = LstPupilContractInClassExtra.Where(p => p.ClassID == ClassID).FirstOrDefault();
        //            var lstPupilContractAll = LstPupilContractInClass.Where(p => p.ClassID == ClassID).ToList();
        //            countTotal = lstPupilContractAll.Select(p => p.PupilID).Distinct().Count();
        //            countWithStatus = lstPupilContractAll.Where(p => p.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || p.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID)
        //                                .Select(p => p.PupilID).Distinct().Count();
        //            if (objPupilContractInClass != null || objPupilContractInClassExtra != null)
        //            {

        //                int TotalSMSInClass = objPupilContractInClass != null ? objPupilContractInClass.TotalSMS : 0;
        //                int TotalSMSInClassExtra = objPupilContractInClassExtra != null ? objPupilContractInClassExtra.TotalSMS : 0;
        //                TotalSMSInClass = TotalSMSInClass + TotalSMSInClassExtra;
        //                int TotalSMSSent = objPupilContractInClass != null ? objPupilContractInClass.SentSMS : 0;

        //                objDB = lstSMSParentContractClassDB.Where(p => p.CLASS_ID == ClassID).FirstOrDefault();
        //                if (objDB == null)//neu chua co quy tin thi them quy tin cho lop
        //                {
        //                    SMS_PARENT_CONTRACT_CLASS objClassInsert = new SMS_PARENT_CONTRACT_CLASS();
        //                    objClassInsert.CLASS_ID = ClassID;
        //                    objClassInsert.ACADEMIC_YEAR_ID = AcademicYearID;
        //                    objClassInsert.SCHOOL_ID = SchoolID;
        //                    objClassInsert.YEAR = objAy.Year;
        //                    objClassInsert.SEMESTER = (byte)SemesterID;
        //                    objClassInsert.SENT_TOTAL = TotalSMSSent;
        //                    objClassInsert.TOTAL_SMS = TotalSMSInClass;
        //                    objClassInsert.CREATED_DATE = DateTime.Now;
        //                    SMSParentContractInClassDetailBusiness.Insert(objClassInsert);
        //                    isChange = true;

        //                    SMSParentContractClassBO objClassDetail = new SMSParentContractClassBO();
        //                    objClassDetail.CLASS_ID = ClassID;
        //                    objClassDetail.ACADEMIC_YEAR_ID = AcademicYearID;
        //                    objClassDetail.SCHOOL_ID = SchoolID;
        //                    objClassDetail.YEAR = objAy.Year;
        //                    objClassDetail.SEMESTER = (byte)SemesterID;
        //                    objClassDetail.SENT_TOTAL = TotalSMSSent;
        //                    objClassDetail.TOTAL_SMS = TotalSMSInClass;
        //                    objClassDetail.CREATED_DATE = DateTime.Now;
        //                    objClassDetail.COUNT_PHONE = countTotal;
        //                    objClassDetail.COUNT_PHONE_STATUS = countWithStatus;
        //                    lstSMSParentContractClass.Add(objClassDetail);

        //                }
        //                else
        //                {
        //                    SMSParentContractClassBO objClassDetail = new SMSParentContractClassBO();
        //                    objClassDetail.CLASS_ID = ClassID;
        //                    objClassDetail.ACADEMIC_YEAR_ID = AcademicYearID;
        //                    objClassDetail.SCHOOL_ID = SchoolID;
        //                    objClassDetail.YEAR = objAy.Year;
        //                    objClassDetail.SEMESTER = (byte)SemesterID;
        //                    objClassDetail.SENT_TOTAL = objDB.SENT_TOTAL;
        //                    objClassDetail.TOTAL_SMS = objDB.TOTAL_SMS;
        //                    objClassDetail.CREATED_DATE = DateTime.Now;
        //                    objClassDetail.COUNT_PHONE = countTotal;
        //                    objClassDetail.COUNT_PHONE_STATUS = countWithStatus;
        //                    lstSMSParentContractClass.Add(objClassDetail);
        //                }
        //            }
        //        }
        //        if (isChange)
        //        {
        //            SMSParentContractInClassDetailBusiness.Save();
        //        }
        //        return lstSMSParentContractClass;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message, ex);
        //        return new List<SMSParentContractClassBO>();
        //    }
        //}

        public void UpdateMessageBudgetInClass(List<ContractDetailInClassBO> lstContractDetail, int year)
        {
            if (lstContractDetail != null && lstContractDetail.Count > 0)
            {
                int schoolID = lstContractDetail.FirstOrDefault().SchoolID;
                int academicYearID = lstContractDetail.FirstOrDefault().AcademicYearID;
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                List<ServicePackageBO> LstServicePackage = ServicePackageDetailBusiness.GetListMaxSMSSP(year);
                List<int> LstClassID = lstContractDetail.Select(o => o.ClassID).Distinct().ToList();
                SMS_PARENT_CONTRACT_CLASS objUpdate = null;
                int classID = 0;
                int SumSMSAddClass = 0;
                List<long> LstContractDetailIdinClass = new List<long>();
                for (int i = LstClassID.Count - 1; i >= 0; i--)
                {
                    classID = LstClassID[i];

                    // Ap dung cho HKI/HKII
                    for (int iHK = 1; iHK <= 3; iHK++)
                    {
                        if (IsExistInfoMessageOfClass(schoolID, academicYearID, classID, iHK))
                        {
                            SumSMSAddClass = lstContractDetail.Where(o => o.ClassID == classID && (
                               o.Semester == iHK)).Sum(o => o.MaxSMS.HasValue ? o.MaxSMS.Value : 0);
                            objUpdate = GetMessageBudgetDetailInClass(schoolID, academicYearID, classID, iHK);
                            LstContractDetailIdinClass = lstContractDetail.Where(o => o.ClassID == classID && (o.Semester == iHK))
                                    .Select(o => o.SMSParentContractDetailID).ToList();
                            var sentDetails = this.SMSParentSentDetailBusiness.All.Where(o => o.SCHOOL_ID == schoolID && o.PARTITION_ID == partitionId && LstContractDetailIdinClass.Contains(o.SMS_PARENT_CONTRACT_DETAIL_ID));
                            int SentTotal = 0;
                            if (sentDetails.Count() > 0) SentTotal = sentDetails.Sum(o => o.SENT_TOTAL);
                            objUpdate.TOTAL_SMS += SumSMSAddClass;
                            objUpdate.SENT_TOTAL += SentTotal;
                            objUpdate.UPDATED_DATE = DateTime.Now;
                            this.Save();
                        }
                        else GetMessageBudgetDetailInClass(schoolID, academicYearID, classID, iHK);
                    }
                }
            }
        }

        public bool IsExistInfoMessageOfClass(int schoolID, int academicYearID, int classID, int semester)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            if (academicYear != null)
                return this.All.Count(o => o.SCHOOL_ID == schoolID && o.ACADEMIC_YEAR_ID == academicYearID && o.CLASS_ID == classID && o.YEAR == academicYear.Year && o.SEMESTER == semester) > 0;
            else return false;

        }
    }
}
