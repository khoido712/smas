﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
     public partial class EvaluationCriteriaBusiness
    {
         public List<EvaluationCriteria> getEvaluationCriteria()
         {
             List<EvaluationCriteria> list = EvaluationCriteriaBusiness.All.OrderBy(p=>p.EvaluationCriteriaID).ToList();
             return list;
         }
    }
}
