﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class HeadTeacherSubstitutionBusiness
    {
        #region Validate du lieu dau vao
        private void Validate(HeadTeacherSubstitution HeadTeacherSubstitution)
        {
            //ClassProfileBusiness.CheckAvailable(HeadTeacherSubstitution.ClassID, "ClassProfile_Label_AllTitle", false);
            //EmployeeBusiness.CheckAvailable(HeadTeacherSubstitution.HeadTeacherID, "Employee_Label_EmployeeID", false);
            //EmployeeBusiness.CheckAvailable(HeadTeacherSubstitution.SubstituedHeadTeacher, "Employee_Label_EmployeeID", false);
            //SchoolFacultyBusiness.CheckAvailable(HeadTeacherSubstitution.SchoolFacultyID, "Employee_Label_EmployeeID", false);
            //AcademicYearBusiness.CheckAvailable(HeadTeacherSubstitution.AcademicYearID, "AcademicYear_Label_Year", true);
            //if (!HeadTeacherSubstitution.AssignedDate.HasValue)
            //{
            //    Utils.ValidateRequire(string.Empty, "HeadTeacherSubstitution_Label_AssignedDate");
            //}
            //if (!HeadTeacherSubstitution.EndDate.HasValue)
            //{
            //    Utils.ValidateRequire(string.Empty, "HeadTeacherSubstitution_Label_EndDate");
            //}
            ValidationMetadata.ValidateObject(HeadTeacherSubstitution);
            // Kiem tra comparetiable
            // AcademicYear va School
            bool AcademicSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                   new Dictionary<string, object>()
                {
                    {"AcademicYearID",HeadTeacherSubstitution.AcademicYearID},
                    {"SchoolID",HeadTeacherSubstitution.SchoolID}
                }, null);
            if (!AcademicSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Giao vien va truong
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                   new Dictionary<string, object>()
                {
                    {"EmployeeID",HeadTeacherSubstitution.SubstituedHeadTeacher},
                    {"SchoolID",HeadTeacherSubstitution.SchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // ClassProfile va AcademicYear
            // Giao vien va truong
            bool ClassAcademicCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                   new Dictionary<string, object>()
                {
                    {"AcademicYearID",HeadTeacherSubstitution.AcademicYearID},
                    {"ClassProfileID",HeadTeacherSubstitution.ClassID}
                }, null);
            if (!ClassAcademicCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Thoi gian bat dau khong duoc vuot qua thoi gian ket thuc
            Utils.ValidateAfterDate(HeadTeacherSubstitution.EndDate, HeadTeacherSubstitution.AssignedDate,
                "HeadTeacherSubstitution_Label_EndDate", "HeadTeacherSubstitution_Label_AssignedDate");

            // Kiem tra trang thai lam viec
            Employee Employee = EmployeeBusiness.Find(HeadTeacherSubstitution.SubstituedHeadTeacher);
            if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("HeadTeacherSubstitution_Label_WorkingStatusErrr");
            }
            // Kiem tra khoang thoi gian lam thay chu nhiem phai thuoc vao nam hoc hien tai
            AcademicYear thisAcademicYear = AcademicYearBusiness.Find(HeadTeacherSubstitution.AcademicYearID);
            if (thisAcademicYear != null)
            {
                if (thisAcademicYear.FirstSemesterStartDate.HasValue)
                {
                    try
                    {
                        Utils.ValidateAfterDate(HeadTeacherSubstitution.AssignedDate, thisAcademicYear.FirstSemesterStartDate,
                       "HeadTeacherSubstitution_Label_AssignedDate", "AcademicYear_Label_FirstSemesterStartDate");

                    }catch(Exception)
                    {
                        throw new BusinessException("HeadTeacherSubstitution_Label_OutAcademicDate", new List<object> {"HeadTeacherSubstitution_Label_AssignedDate" });
                    }
                   

                }
                if (thisAcademicYear.SecondSemesterEndDate.HasValue)
                {
                    try
                    {
                        Utils.ValidateAfterDate(thisAcademicYear.SecondSemesterEndDate, HeadTeacherSubstitution.EndDate,
                        "AcademicYear_Label_SecondSemesterEndDate", "HeadTeacherSubstitution_Label_EndDate");
                    }
                    catch (Exception)
                    {
                        throw new BusinessException("HeadTeacherSubstitution_Label_OutAcademicDate", new List<object> { "HeadTeacherSubstitution_Label_EndDate" });
                    }
                   

                }
            }

            // Kiem tra phai thuoc nam hoc
            if (!AcademicYearBusiness.IsCurrentYear(HeadTeacherSubstitution.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }
            //Lấy ra list danh sách giáo viên làm thay chủ nhiệm 
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = HeadTeacherSubstitution.AcademicYearID;
            dic["SchoolID"] = HeadTeacherSubstitution.SchoolID;
            dic["ClassID"] = HeadTeacherSubstitution.ClassID;
            List<HeadTeacherSubstitution> lstHeadSub = HeadTeacherSubstitutionBusiness.Search(dic).Where(o => o.HeadTeacherSubstitutionID != HeadTeacherSubstitution.HeadTeacherSubstitutionID).ToList();

            foreach (HeadTeacherSubstitution item in lstHeadSub)
            {
                if (HeadTeacherSubstitution.AssignedDate.HasValue)
                {
                    if (item.AssignedDate.HasValue && item.EndDate.HasValue)
                    {
                        if (Utils.CompareDateTimeRange(HeadTeacherSubstitution.AssignedDate.Value, HeadTeacherSubstitution.EndDate, item.AssignedDate.Value, item.EndDate.Value))
                        {
                            throw new BusinessException("HeadTeacherSubstitution_Label_DateConflict", new List<object>() { });
                        }
                    }
                }
            }

        }
        #endregion

        #region Them moi
        public override HeadTeacherSubstitution Insert(HeadTeacherSubstitution HeadTeacherSubstitution)
        {
            Validate(HeadTeacherSubstitution);
            ClassProfile ClassProfile = ClassProfileBusiness.Find(HeadTeacherSubstitution.ClassID);
            HeadTeacherSubstitution.HeadTeacherID = ClassProfile.HeadTeacherID.Value;
            base.Insert(HeadTeacherSubstitution);
            return HeadTeacherSubstitution;
        }
        #endregion

        #region Cap nhat
        public override HeadTeacherSubstitution Update(HeadTeacherSubstitution HeadTeacherSubstitution)
        {
            Validate(HeadTeacherSubstitution);
            // Kiem tra comparetiable voi SchoolID
            bool SubSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HeadTeacherSubstitution",
                   new Dictionary<string, object>()
                {
                    {"HeadTeacherSubstitutionID",HeadTeacherSubstitution.HeadTeacherSubstitutionID},
                    {"SchoolID",HeadTeacherSubstitution.SchoolID}
                }, null);
            if (!SubSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            base.Update(HeadTeacherSubstitution);
            return HeadTeacherSubstitution;
        }
        #endregion

        #region Tim kiem
        public IQueryable<HeadTeacherSubstitution> Search(IDictionary<string, object> dic)
        {
            
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            string FullName = Utils.GetString(dic, "FullName");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EmploymentStatus = Utils.GetInt(dic, "EmploymentStatus");
            //IQueryable<HeadTeacherSubstitution> ListHeadTeacherSubstitution = HeadTeacherSubstitutionRepository.All;
            //Sửa chỉ hiển thị đối với giáo viên đang làm việc
            IQueryable<HeadTeacherSubstitution> ListHeadTeacherSubstitution = from p in HeadTeacherSubstitutionRepository.All
                                                                              join q in EmployeeHistoryStatusBusiness.All on p.SubstituedHeadTeacher equals q.EmployeeID
                                                                              where (q.SchoolID == SchoolID || SchoolID == 0) && (q.EmployeeStatus == EmploymentStatus || EmploymentStatus == 0)
                                                                              select p;

            if (ClassID != 0)
            {
                ListHeadTeacherSubstitution = from hts in ListHeadTeacherSubstitution
                                              join cp in ClassProfileBusiness.All on hts.ClassID equals cp.ClassProfileID
                                              where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                              && hts.ClassID == ClassID
                                              select hts;
                    //ListHeadTeacherSubstitution.Where(x => x.ClassID == ClassID);
            }
            if (SchoolID != 0)
            {
                ListHeadTeacherSubstitution = ListHeadTeacherSubstitution.Where(x => x.SchoolID == SchoolID);
            }
            if (TeacherID != 0)
            {
                ListHeadTeacherSubstitution = ListHeadTeacherSubstitution.Where(x => x.HeadTeacherSubstitutionID == TeacherID);
            }
            if (AcademicYearID != 0)
            {
                ListHeadTeacherSubstitution = ListHeadTeacherSubstitution.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (FullName != null && !FullName.Equals(string.Empty))
            {
                ListHeadTeacherSubstitution = ListHeadTeacherSubstitution.Where(x => x.Employee1.FullName.ToLower().Contains(FullName.Trim().ToLower()));
            }
            if(AppliedLevel!=0)
            {
                ListHeadTeacherSubstitution = ListHeadTeacherSubstitution.Where(x => x.ClassProfile.EducationLevel.Grade == AppliedLevel);  
            }
            return ListHeadTeacherSubstitution;
        }
        #endregion

        #region Xoa
        public void Delete(int SchoolID, int HeadTeacherSubstitutionID)
        {
            // Kiem tra da ton tai hay chua
            this.CheckAvailable(HeadTeacherSubstitutionID, "HeadTeacherSubstitution_Lable_ID");
            // Kiem tra compatiable
            HeadTeacherSubstitution o = Find(HeadTeacherSubstitutionID);
            if (o.SchoolID != SchoolID)
            {
                throw new BusinessException("HeadTeacherSubstitution_Lable_School_Err");
            }
            // Xoa
            base.Delete(HeadTeacherSubstitutionID);
        }
        #endregion

        /// <summary>
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<HeadTeacherSubstitution> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }

    }
}