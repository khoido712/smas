using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ReportTeachingAssignmentBusiness
    {
        #region Tạo mảng băm cho các tham số đầu vào của báo cáo phân công giảng dạy
        /// <summary>
        /// Tạo mảng băm cho các tham số đầu vào của báo cáo phân công giảng dạy
        /// </summary>
        /// <returns></returns>
        public string GetHashKey(ReportTeachingAssignmentBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Tạo file báo cáo phân công giảng dạy
        public Stream ExcelCreateTeachingAssignment(ReportTeachingAssignmentBO entity)
        {
            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Fill dữ liệu vào báo cáo
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = oBook.GetSheet(1);
            //Điền các thông tin tiêu đề báo cáo
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevelID).ToUpper();
            string schoolName = school.SchoolName;
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string semester = "";
            if (entity.Semester == 1)
            {
                semester = "HỌC KỲ I";
            }
            else if (entity.Semester == 2)
            {
                semester = "HỌC KỲ II";
            }
            else
            {
                semester = "CẢ NĂM";
            }

            string educationLevel = EducationLevelBusiness.Find(entity.EducationLevelID).Resolution.ToUpper();
            string reportTitle = "DANH SÁCH GIÁO VIÊN GIẢNG DẠY " + educationLevel + " - " + semester + " - NĂM HỌC " + academicYear.Year.ToString() + " - " + (academicYear.Year + 1).ToString();


            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("C4", provinceAndDate);
            oSheet.SetCellValue("A6", reportTitle);

            //Điền dữ liệu phân công giảng dạy
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = entity.AcademicYearID;
            dic["Semester"] = entity.Semester;
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["ClassID"] = entity.ClassID;
            IQueryable<TeachingAssignment> queryTeachingAssignment = GetListTeachingAssignment(entity.SchoolID, dic);
            int startRow = 8;

            //Nếu báo cáo theo lớp 
            if (entity.ClassID != 0)
            {
                List<TeachingAssignment> lstTeachingAssignment = new List<TeachingAssignment>();
                if (queryTeachingAssignment != null && queryTeachingAssignment.Count() > 0)
                {
                    lstTeachingAssignment = queryTeachingAssignment.ToList();
                    lstTeachingAssignment = lstTeachingAssignment.OrderBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName).ToList();
                    FillDataForTeachingAssignmentReport(oSheet, startRow, lstTeachingAssignment);
                }
                else
                {
                    ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);
                    string className = "";
                    if (cp != null)
                    {
                        className = "LỚP: " + cp.DisplayName;
                    }
                    oSheet.SetCellValue(startRow, 1, className);
                }
            }
            //Nếu báo cáo theo khối
            else if (entity.ClassID == 0)
            {
                //Lấy danh sách lớp theo khối
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = entity.AcademicYearID;
                SearchInfo["EducationLevelID"] = entity.EducationLevelID;
                List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, SearchInfo).ToList();
                lstClass = lstClass.OrderBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();
                List<TeachingAssignment> lstTeachingAssignment = new List<TeachingAssignment>();
                int ClassID;
                for (int i = 0; i < lstClass.Count(); i++)
                {
                    ClassID = lstClass[i].ClassProfileID;
                    lstTeachingAssignment = queryTeachingAssignment.Where(o => o.ClassID == ClassID).ToList();
                    lstTeachingAssignment = lstTeachingAssignment.OrderBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName).ToList();
                    if (lstTeachingAssignment.Count() == 0)
                    {
                        continue;
                    }
                    else
                    {
                        FillDataForTeachingAssignmentReport(oSheet, startRow, lstTeachingAssignment);
                        startRow += lstTeachingAssignment.Count() + 2;
                    }
                }

            }
            return oBook.ToStream();
        }

        #endregion
        #region Fill dữ liệu cho từng lớp trong báo cáo phân công giảng dạy
        public void FillDataForTeachingAssignmentReport(IVTWorksheet sheet, int startRow, List<TeachingAssignment> lstTeachingAssignment)
        {
            IVTRange range = sheet.GetRange("A8", "E9");
            if (startRow > 8)
            {
                sheet.CopyPasteSameRowHeigh(range, startRow);
            }
            DateTime dt = new DateTime();
            string className = lstTeachingAssignment[0].ClassProfile.DisplayName;
            className = "LỚP: " + className;
            sheet.SetCellValue(startRow, 1, className);
            for (int i = 0; i < lstTeachingAssignment.Count(); i++)
            {
                //Copy row
                range = sheet.GetRange("A10", "E10");
                if (startRow + i > 8)
                {
                    sheet.CopyPasteSameRowHeigh(range, startRow + 2 + i);
                }
                //STT
                sheet.SetCellValue(startRow + 2 + i, 1, i + 1);
                //Họ và tên
                sheet.SetCellValue(startRow + 2 + i, 2, lstTeachingAssignment[i].Employee.FullName);
                //Ngày sinh
                if (lstTeachingAssignment[i].Employee.BirthDate.HasValue)
                {
                    dt = lstTeachingAssignment[i].Employee.BirthDate.Value;
                    dt.ToShortDateString();
                    sheet.SetCellValue(startRow + 2 + i, 3, dt);
                }
                //Môn học
                sheet.SetCellValue(startRow + 2 + i, 4, lstTeachingAssignment[i].SubjectCat.DisplayName);
            }
        }
        #endregion
        #region Tìm kiếm danh sách phân công giảng dạy
        public IQueryable<TeachingAssignment> GetListTeachingAssignment(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            var query = TeachingAssignmentRepository.All;
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                query = query.Where(o => o.SchoolID == SchoolID && o.Last2digitNumberSchool == partitionId);
            }
            query = query.Where(o => o.IsActive == true);
            int Semester = (int)Utils.GetShort(SearchInfo, "Semester", 1);
            int EducationLevelID = Utils.GetShort(SearchInfo, "EducationLevelID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");

            if (Semester != 0)
            {
                if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    query = query.Where(o => o.Semester == Semester);
                }
                else
                {
                    query = query.Where(o => o.Semester == (int)SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == (int)SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                }
            }
            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (ClassID != 0)
            {
                query = query.Where(o => o.ClassID == ClassID);
            }
            return query;
        }
        #endregion

        #region
        public ProcessedReport ExcelInsertTeachingAssignment(ReportTeachingAssignmentBO entity, Stream data)
        {
            //Tạo và insert vào bảng ProcessedReport
            string reportCode = SystemParamsInFile.REPORT_PHAN_CONG_GIANG_DAY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);
            //Tạo tên file GV_[SchoolLevel]_BC_PhanCongGiangDay_[EducationLevel/Class]_[Semester]
            string outputNamePattern = reportDef.OutputNamePattern;
            EducationLevel educationLevel = EducationLevelBusiness.Find(entity.EducationLevelID);
            string educationLevelOrClass = (entity.ClassID == 0) ? educationLevel.Resolution : ClassProfileBusiness.Find(entity.ClassID).DisplayName;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(educationLevel.Grade);
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            //Insert vào bảng ProcessedReportParameter
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            //Insert vào bảng ProcessedReport
            ProcessedReportBusiness.Insert(pr);
            ProcessedReportBusiness.Save();
            return pr;
        }
        #endregion
    }
}
