﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class MarkOfSemesterByJudgeSubjectBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào của thống kê điểm thi học kỳ theo môn nhận xét
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê điểm thi học kỳ theo môn nhận xét
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKey(MarkOfSemesterByJudgeSubjectBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SubjectID", entity.SubjectID},
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"IsCommenting", entity.IsCommenting},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy thống kê điểm thi học kỳ môn nhận xet được cập nhật mới nhất
        /// <summary>
        /// Lấy thống kê điểm thi học kỳ môn nhận xet được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion


        #region Lưu lại thông tin bảng điểm học sinh theo kỳ
        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);

            //Tạo tên file HS_PTTH_ThongKeDiemThi_[HocKy]_[Môn học]
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string Subject = "";
            if (entity.SubjectID == 0)
            {
                Subject = "TấtCả";
            }
            else
            {
                Subject = this.ClassSubjectBusiness.All.Where(o => o.SubjectID == entity.SubjectID && o.Last2digitNumberSchool == partitionId).FirstOrDefault().SubjectCat.DisplayName;
            }

            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            outputNamePattern = outputNamePattern.Replace("PTTH", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SubjectID ", entity.SubjectID},
                {"Semester", entity.Semester},
                {"AcademicYearID ", entity.AcademicYearID},
                {"IsCommenting ", entity.IsCommenting},
                {"SchoolID ", entity.SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion


        #region Lưu lại thông tin thống kê điểm thi học kỳ môn nhận xét
        /// <summary>
        /// Lưu lại thông tin thống kê điểm thi học kỳ môn nhận xét
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateMarkOfSemesterByJudgeSubject(MarkOfSemesterByJudgeSubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile school = this.SchoolProfileBusiness.Find(entity.SchoolID);
            AcademicYear objAca = this.AcademicYearBusiness.Find(entity.AcademicYearID);
            string SchoolName = school.SchoolName.ToUpper();
            string AcademicYear = objAca.DisplayTitle;
            string Suppervising = this.SchoolProfileBusiness.Find(entity.SchoolID).SupervisingDept.SupervisingDeptName.ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string SemesterName = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);


            // Lấy danh sách khối học

            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            // Lấy danh sách điểm JudgeRecord
            int MarkTypeID = this.MarkTypeBusiness.All.Where(o => o.AppliedLevel == entity.AppliedLevel && o.Title == "HK").SingleOrDefault().MarkTypeID;

            IDictionary<string, object> Dic_JudgeRecord = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                {"Semester", entity.Semester},
                                                                {"MarkTypeID", MarkTypeID}
                                                            };

            IQueryable<VJudgeRecord> lstQJudge = this.VJudgeRecordBusiness
                                                            .SearchBySchool(entity.SchoolID, Dic_JudgeRecord);

            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClassBO> lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                 join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where cs.SubjectID == entity.SubjectID
                                                 && cs.Last2digitNumberSchool == partitionId
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     Genre = q.Genre,
                                                     EthnicID = q.EthnicID,
                                                     AppliedType = cs.AppliedType,
                                                     IsSpecialize = cs.IsSpecializedSubject,
                                                     SubjectID = cs.SubjectID,
                                                     IsVNEN = cs.IsSubjectVNEN,
                                                     EducationLevelID = q.ClassProfile.EducationLevelID
                                                 };
            //loc ra nhung hoc sinh khong dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",objAca.Year}
            };

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            List<PupilOfClassBO> lstPOCNotVnen = this.GetPupilOfClassInSpecialize(lstQPoc.Where(o => o.IsVNEN == null || o.IsVNEN == false), dicRegis);
            List<PupilOfClassBO> lstPOCVnen = this.GetPupilOfClassInSpecialize(lstQPoc.Where(o => o.IsVNEN == true), dicRegis);
            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID, 0);

            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
                lstPOCNotVnen = lstPOCNotVnen.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
                lstPOCVnen = lstPOCVnen.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            //Học sinh dân tộc 
            //Dân tộc kinh
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int Ethnic_IDKinh = 0;
            if (Ethnic_Kinh != null)
            {
                Ethnic_IDKinh = Ethnic_Kinh.EthnicID;
            }
            int Ethnic_IDNN = 0;
            if (Ethnic_ForeignPeople != null)
            {
                Ethnic_IDNN = Ethnic_ForeignPeople.EthnicID;
            }
            var listCountPupilByClass = lstPOC.GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_Ethnic_Female = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN && o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            List<JudgeRecordBO> lsttmp = (from u in lstQJudge
                                          join q in PupilProfileBusiness.All on u.PupilID equals q.PupilProfileID
                                          join r in ClassProfileBusiness.All on u.ClassID equals r.ClassProfileID
                                          where r.IsActive.Value
                                          //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                          select new JudgeRecordBO
                                          {
                                              PupilID = u.PupilID,
                                              ClassID = u.ClassID,
                                              Judgement = u.Judgement,
                                              Genre = q.Genre,
                                              EthnicID = q.EthnicID,
                                              EducationLevelID = r.EducationLevelID,
                                              SubjectID = u.SubjectID
                                          }).ToList();
            List<JudgeRecordBO> lstJudgeRecordBySubject = (from j in lsttmp
                                                           join poc in lstPOCNotVnen on j.ClassID equals poc.ClassID
                                                           where j.PupilID == poc.PupilID
                                                           && j.SubjectID == poc.SubjectID
                                                           select j).ToList();

            //Khong tinh cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstJudgeRecordBySubject = lstJudgeRecordBySubject.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            //lstJudgeRecordBySubject
            var lstJudgeRecordBySubject_Female = lstJudgeRecordBySubject.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();

            var lstJudgeRecordBySubject_Ethnic = lstJudgeRecordBySubject.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).ToList();

            var lstJudgeRecordBySubject_Ethnic_Female = lstJudgeRecordBySubject.Where(o => o.EthnicID.HasValue && o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).ToList();

            //Lay diem cuoi ky cac hoc sinh hoc mon hoc VNEN
            int partition = UtilsBusiness.GetPartionId(entity.SchoolID);
            var tmpTnbs = from tnbs in TeacherNoteBookSemesterBusiness.All
                          join cp in ClassProfileBusiness.All.Where(o => o.AcademicYearID == entity.AcademicYearID && o.SchoolID == entity.SchoolID && o.IsActive == true)
                             on tnbs.ClassID equals cp.ClassProfileID
                          join pp in PupilProfileBusiness.All on tnbs.PupilID equals pp.PupilProfileID
                          where tnbs.AcademicYearID == entity.AcademicYearID
                          && tnbs.PartitionID == partition
                          && tnbs.SchoolID == entity.SchoolID
                          && tnbs.SemesterID == entity.Semester
                          && tnbs.SubjectID == entity.SubjectID
                          select new
                          {
                              EducationLevelID = cp.EducationLevelID,
                              SubjectID = tnbs.SubjectID,
                              Judgement = tnbs.PERIODIC_SCORE_END_JUDGLE,
                              Mark = tnbs.PERIODIC_SCORE_END,
                              Genre = pp.Genre,
                              EthnicID = pp.EthnicID,
                              ClassID = tnbs.ClassID,
                              PupilID = tnbs.PupilID
                          };

            var iqTnbs = (from j in tmpTnbs.ToList()
                          join l in lstPOCVnen on new { j.ClassID, j.PupilID, j.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                          select j);

            var lstTnbsAll = iqTnbs.ToList();
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstTnbsAll = lstTnbsAll.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            var lstTnbs_Female = lstTnbsAll.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var lstTnbs_Ethnic = lstTnbsAll.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDNN && o.EthnicID != Ethnic_IDKinh).ToList();
            var lstTnbs_FemaleEthnic = lstTnbsAll.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != Ethnic_IDNN && o.EthnicID != Ethnic_IDKinh).ToList();

            int startschool = 13;
            int startrow = 13;
            int totalHSD = 0;
            int totalHSCD = 0;
            int TotalSchool = 0;

            IDictionary<string, object> Dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID}
                                                                };
            var lstClassAllEdu = (from p in this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                  where q.IsActive.Value
                                  select new
                                  {
                                      ClassID = q.ClassProfileID,
                                      EducationLevelID = q.EducationLevelID,
                                      OrderNumber = q.OrderNumber,
                                      DisplayName = q.DisplayName,
                                      SubjectID = p.SubjectID
                                  }).ToList();
            lstClassAllEdu = lstClassAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange range = firstSheet.GetRange("A15", "G15");
            IVTRange rangeEducation = firstSheet.GetRange("A13", "G13");
            IVTRange rangeLastClass = firstSheet.GetRange("A16", "G16");
            IVTRange range_Female = firstSheet.GetRange("A15", "G15");
            IVTRange rangeEducation_Female = firstSheet.GetRange("A13", "G13");
            IVTRange rangeLastClass_Female = firstSheet.GetRange("A16", "G16");
            IVTRange range_Ethnic = firstSheet.GetRange("A15", "G15");
            IVTRange rangeEducation_Ethnic = firstSheet.GetRange("A13", "G13");
            IVTRange rangeLastClass_Ethnic = firstSheet.GetRange("A16", "G16");
            IVTRange range_Ethnic_Female = firstSheet.GetRange("A15", "G15");
            IVTRange rangeEducation_Ethnic_Female = firstSheet.GetRange("A13", "G13");
            IVTRange rangeLastClass_Ethnic_Female = firstSheet.GetRange("A16", "G16");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);

            #region Tạo dữ liệu thống kê điểm thi học kỳ theo môn
            //Fill dữ liệu chung
            sheet.Name = "DiemThiHK";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ");
            sheet.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            sheet.SetCellValue("F5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int sumHSD = 0;
                int sumHSCD = 0;
                int totalEducation = 0;
                int startrowSum = startrow;
                var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                var lstTnbsBySubject_Edu = lstTnbsAll.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                for (int j = 0; j < lstClass.Count; j++)
                {
                    int classID = lstClass[j].ClassID;
                    var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                    var lstTnbsOfClass = lstTnbsBySubject_Edu.Where(u => u.ClassID == classID);

                    // lấy số lượng học sinh Đạt
                    int HSD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count()
                        + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                    sumHSD += HSD;

                    // Lấy số lượng học sinh chưa Đạt
                    int HSCD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count()
                        + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    sumHSCD += HSCD;

                    // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                    var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == classID).FirstOrDefault();
                    int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                    totalEducation += SumPupil;
                    // fill vào excel
                    if (j == lstClass.Count - 1)
                        sheet.CopyPasteSameRowHeigh(rangeLastClass, startrow + 1);
                    else
                        sheet.CopyPasteSameRowHeigh(range, startrow + 1);

                    sheet.SetCellValue(startrow + 1, 1, j + 1);
                    sheet.SetCellValue(startrow + 1, 2, lstClass[j].DisplayName);
                    sheet.SetCellValue(startrow + 1, 3, SumPupil);
                    sheet.SetCellValue(startrow + 1, 4, HSD);
                    sheet.SetCellValue(startrow + 1, 6, HSCD);
                    startrow++;
                }

                totalHSD += sumHSD;
                totalHSCD += sumHSCD;
                TotalSchool += totalEducation;

                //Copy row style
                if (startrowSum + 1 > 11)
                {
                    sheet.CopyPasteSameRowHeigh(rangeEducation, startrowSum);
                }

                // fill vào excel
                sheet.SetCellValue(startrowSum, 1, i + 1);
                sheet.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                if (lstClass.Count > 0)
                {
                    sheet.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow).ToString() + ")");
                    sheet.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow).ToString() + ")");
                    sheet.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow).ToString() + ")");
                }
                else
                {
                    sheet.SetCellValue(startrowSum, 3, 0);
                    sheet.SetCellValue(startrowSum, 4, 0);
                    sheet.SetCellValue(startrowSum, 6, 0);
                }
                sheet.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");


                sheet.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                startrow++;
            }

            // fill vào excel toàn trường
            sheet.SetCellValue(12, 1, "Toàn trường");
            sheet.SetCellValue(12, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow - 1).ToString() + ")/2");

            sheet.SetCellValue(12, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow - 1).ToString() + ")/2");
            sheet.SetCellValue(12, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

            sheet.SetCellValue(12, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow - 1).ToString() + ")/2");
            sheet.SetCellValue(12, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

            sheet.FitAllColumnsOnOnePage = true;
            sheet.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            sheet.GetRange("A" + startrow, "A" + startrow).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.SetFontName("Times New Roman", 0);
            #endregion

            if (entity.EthnicChecked)
            {
                #region Tạo báo cáo thống kê điểm thi học kỳ theo môn học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(firstSheet);
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.SetCellValue("A2", Suppervising);
                sheet_Ethnic.SetCellValue("A3", SchoolName);
                sheet_Ethnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH DÂN TỘC");
                sheet_Ethnic.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                sheet_Ethnic.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                int startrow_Ethnic = 13;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int sumHSD = 0;
                    int sumHSCD = 0;
                    int totalEducation = 0;
                    int startrowSum = startrow_Ethnic;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    var lstTnbsBySubject_Edu = lstTnbs_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        int classID = lstClass[j].ClassID;
                        var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                        var lstTnbsOfClass = lstTnbsBySubject_Edu.Where(u => u.ClassID == classID);
                        // lấy số lượng học sinh Đạt
                        int HSD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count()
                            + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                        sumHSD += HSD;

                        // Lấy số lượng học sinh chưa Đạt
                        int HSCD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count()
                            + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        sumHSCD += HSCD;

                        // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                        var objCountPupil = listCountPupilByClass_Ethnic.Where(o => o.ClassID == classID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        totalEducation += SumPupil;
                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangeLastClass_Ethnic, startrow_Ethnic + 1);
                        else
                            sheet_Ethnic.CopyPasteSameRowHeigh(range_Ethnic, startrow_Ethnic + 1);

                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 1, j + 1);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 2, lstClass[j].DisplayName);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 3, SumPupil);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 4, HSD);
                        sheet_Ethnic.SetCellValue(startrow_Ethnic + 1, 6, HSCD);
                        startrow_Ethnic++;
                    }

                    totalHSD += sumHSD;
                    totalHSCD += sumHSCD;
                    TotalSchool += totalEducation;

                    //Copy row style
                    if (startrowSum + 1 > 11)
                    {
                        sheet_Ethnic.CopyPasteSameRowHeigh(rangeEducation_Ethnic, startrowSum);
                    }

                    // fill vào excel
                    sheet_Ethnic.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Ethnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                    if (lstClass.Count > 0)
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow_Ethnic).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow_Ethnic).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow_Ethnic).ToString() + ")");
                    }
                    else
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 3, 0);
                        sheet_Ethnic.SetCellValue(startrowSum, 4, 0);
                        sheet_Ethnic.SetCellValue(startrowSum, 6, 0);
                    }
                    sheet_Ethnic.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");


                    sheet_Ethnic.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    startrow_Ethnic++;
                }

                // fill vào excel toàn trường
                sheet_Ethnic.SetCellValue(12, 1, "Toàn trường");
                sheet_Ethnic.SetCellValue(12, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow_Ethnic - 1).ToString() + ")/2");

                sheet_Ethnic.SetCellValue(12, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow_Ethnic - 1).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(12, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Ethnic.SetCellValue(12, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow_Ethnic - 1).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(12, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                sheet_Ethnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Ethnic.GetRange("A" + startrow, "A" + startrow).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet_Ethnic.SetFontName("Times New Roman", 0);
                #endregion
            }
            if (entity.FemaleChecked)
            {
                #region Tạo dữ liệu báo cáo thống kê điểm thi học kỳ học sinh nữ
                //Fill dữ liệu chung
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(firstSheet);
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetCellValue("A2", Suppervising);
                sheet_Female.SetCellValue("A3", SchoolName);
                sheet_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ");
                sheet_Female.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                sheet_Female.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                int startrow_Female = 13;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int sumHSD = 0;
                    int sumHSCD = 0;
                    int totalEducation = 0;
                    int startrowSum = startrow_Female;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    var lstTnbsBySubject_Edu = lstTnbs_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        int classID = lstClass[j].ClassID;
                        var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                        var lstTnbsOfClass = lstTnbsBySubject_Edu.Where(u => u.ClassID == classID);
                        // lấy số lượng học sinh Đạt
                        int HSD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count()
                            + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                        sumHSD += HSD;

                        // Lấy số lượng học sinh chưa Đạt
                        int HSCD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count()
                            + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        sumHSCD += HSCD;

                        // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                        var objCountPupil = listCountPupilByClass_Female.Where(o => o.ClassID == classID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        totalEducation += SumPupil;
                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Female.CopyPasteSameRowHeigh(rangeLastClass_Female, startrow_Female + 1);
                        else
                            sheet_Female.CopyPasteSameRowHeigh(range_Female, startrow_Female + 1);

                        sheet_Female.SetCellValue(startrow_Female + 1, 1, j + 1);
                        sheet_Female.SetCellValue(startrow_Female + 1, 2, lstClass[j].DisplayName);
                        sheet_Female.SetCellValue(startrow_Female + 1, 3, SumPupil);
                        sheet_Female.SetCellValue(startrow_Female + 1, 4, HSD);
                        sheet_Female.SetCellValue(startrow_Female + 1, 6, HSCD);
                        startrow_Female++;
                    }

                    totalHSD += sumHSD;
                    totalHSCD += sumHSCD;
                    TotalSchool += totalEducation;

                    //Copy row style
                    if (startrowSum + 1 > 11)
                    {
                        sheet_Female.CopyPasteSameRowHeigh(rangeEducation_Female, startrowSum);
                    }

                    // fill vào excel
                    sheet_Female.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Female.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                    if (lstClass.Count > 0)
                    {
                        sheet_Female.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow_Female).ToString() + ")");
                        sheet_Female.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow_Female).ToString() + ")");
                        sheet_Female.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow_Female).ToString() + ")");
                    }
                    else
                    {
                        sheet_Female.SetCellValue(startrowSum, 3, 0);
                        sheet_Female.SetCellValue(startrowSum, 4, 0);
                        sheet_Female.SetCellValue(startrowSum, 6, 0);
                    }
                    sheet_Female.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");


                    sheet_Female.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    startrow_Female++;
                }

                // fill vào excel toàn trường
                sheet_Female.SetCellValue(12, 1, "Toàn trường");
                sheet_Female.SetCellValue(12, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow_Female - 1).ToString() + ")/2");

                sheet_Female.SetCellValue(12, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow_Female - 1).ToString() + ")/2");
                sheet_Female.SetCellValue(12, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Female.SetCellValue(12, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow_Female - 1).ToString() + ")/2");
                sheet_Female.SetCellValue(12, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Female.FitAllColumnsOnOnePage = true;
                sheet_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Female.GetRange("A" + startrow, "A" + startrow).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet_Female.SetFontName("Times New Roman", 0);
                //Xoá sheet template
                #endregion
            }
            if (entity.FemaleEthnicChecked)
            {
                #region Tạo báo cáo thống kê điểm thi học kỳ theo môn học sinh nữ - dân tộc
                IVTWorksheet sheet_Ethnic_Female = oBook.CopySheetToLast(firstSheet);
                sheet_Ethnic_Female.Name = "HS_NU_DT";
                sheet_Ethnic_Female.SetCellValue("A2", Suppervising);
                sheet_Ethnic_Female.SetCellValue("A3", SchoolName);
                sheet_Ethnic_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ DÂN TỘC");
                sheet_Ethnic_Female.SetCellValue("A8", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                sheet_Ethnic_Female.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                int startrow_Ethnic_Female = 13;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int sumHSD = 0;
                    int sumHSCD = 0;
                    int totalEducation = 0;
                    int startrowSum = startrow_Ethnic_Female;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject_Ethnic_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    var lstTnbsBySubject_Edu = lstTnbs_FemaleEthnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        int classID = lstClass[j].ClassID;
                        var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                        var lstTnbsOfClass = lstTnbsBySubject_Edu.Where(u => u.ClassID == classID);
                        // lấy số lượng học sinh Đạt
                        int HSD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count()
                            + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                        sumHSD += HSD;

                        // Lấy số lượng học sinh chưa Đạt
                        int HSCD = lstJudgeOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count()
                            + lstTnbsOfClass.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                        sumHSCD += HSCD;

                        // sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp              
                        var objCountPupil = listCountPupilByClass_Ethnic_Female.Where(o => o.ClassID == classID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        totalEducation += SumPupil;
                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Ethnic_Female.CopyPasteSameRowHeigh(rangeLastClass_Ethnic_Female, startrow_Ethnic_Female + 1);
                        else
                            sheet_Ethnic_Female.CopyPasteSameRowHeigh(range_Ethnic_Female, startrow_Ethnic_Female + 1);

                        sheet_Ethnic_Female.SetCellValue(startrow_Ethnic_Female + 1, 1, j + 1);
                        sheet_Ethnic_Female.SetCellValue(startrow_Ethnic_Female + 1, 2, lstClass[j].DisplayName);
                        sheet_Ethnic_Female.SetCellValue(startrow_Ethnic_Female + 1, 3, SumPupil);
                        sheet_Ethnic_Female.SetCellValue(startrow_Ethnic_Female + 1, 4, HSD);
                        sheet_Ethnic_Female.SetCellValue(startrow_Ethnic_Female + 1, 6, HSCD);
                        startrow_Ethnic_Female++;
                    }

                    totalHSD += sumHSD;
                    totalHSCD += sumHSCD;
                    TotalSchool += totalEducation;

                    //Copy row style
                    if (startrowSum + 1 > 11)
                    {
                        sheet_Ethnic_Female.CopyPasteSameRowHeigh(rangeEducation_Ethnic_Female, startrowSum);
                    }

                    // fill vào excel
                    sheet_Ethnic_Female.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Ethnic_Female.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);

                    if (lstClass.Count > 0)
                    {
                        sheet_Ethnic_Female.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + (startrow_Ethnic_Female).ToString() + ")");
                        sheet_Ethnic_Female.SetCellValue(startrowSum, 4, "=SUM(D" + (startrowSum + 1).ToString() + ":D" + (startrow_Ethnic_Female).ToString() + ")");
                        sheet_Ethnic_Female.SetCellValue(startrowSum, 6, "=SUM(F" + (startrowSum + 1).ToString() + ":F" + (startrow_Ethnic_Female).ToString() + ")");
                    }
                    else
                    {
                        sheet_Ethnic_Female.SetCellValue(startrowSum, 3, 0);
                        sheet_Ethnic_Female.SetCellValue(startrowSum, 4, 0);
                        sheet_Ethnic_Female.SetCellValue(startrowSum, 6, 0);
                    }
                    sheet_Ethnic_Female.SetCellValue(startrowSum, 5, "=IF(C" + startrowSum + ">0" + "," + "ROUND(D" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");


                    sheet_Ethnic_Female.SetCellValue(startrowSum, 7, "=IF(C" + startrowSum + ">0" + "," + "ROUND(F" + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                    startrow_Ethnic_Female++;
                }

                // fill vào excel toàn trường
                sheet_Ethnic_Female.SetCellValue(12, 1, "Toàn trường");
                sheet_Ethnic_Female.SetCellValue(12, 3, "=SUM(C" + startschool.ToString() + ":C" + (startrow_Ethnic_Female - 1).ToString() + ")/2");

                sheet_Ethnic_Female.SetCellValue(12, 4, "=SUM(D" + startschool.ToString() + ":D" + (startrow_Ethnic_Female - 1).ToString() + ")/2");
                sheet_Ethnic_Female.SetCellValue(12, 5, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(D" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Ethnic_Female.SetCellValue(12, 6, "=SUM(F" + startschool.ToString() + ":F" + (startrow_Ethnic_Female - 1).ToString() + ")/2");
                sheet_Ethnic_Female.SetCellValue(12, 7, "=IF(C" + (startschool - 1) + ">0" + "," + "ROUND(F" + (startschool - 1) + "/" + "C" + (startschool - 1) + "*100,2),0)");

                sheet_Ethnic_Female.FitAllColumnsOnOnePage = true;
                sheet_Ethnic_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Ethnic_Female.GetRange("A" + startrow, "A" + startrow).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet_Ethnic_Female.SetFontName("Times New Roman", 0);
                //Xoá sheet template
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion

    }
}
