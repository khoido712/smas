﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    public partial class CommentOfPupilBusiness
    {
        public IQueryable<CommentOfPupil> Search(IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int MonthID = Utils.GetInt(dic, "MonthID");
            DateTime? DateTimeComment = Utils.GetDateTime(dic, "DateTimeComment");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            IQueryable<CommentOfPupil> iquery = CommentOfPupilBusiness.All;
            if (SchoolID != 0)
            {
                int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
                iquery = iquery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartitionID);
            }
            if (AcademicYearID != 0)
            {
                iquery = iquery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iquery = iquery.Where(p => p.ClassID == ClassID);
            }
            if (SubjectID != 0)
            {
                iquery = iquery.Where(p => p.SubjectID == SubjectID);
            }
            if (PupilID != 0)
            {
                iquery = iquery.Where(p => p.PupilID == PupilID);
            }
            if (SemesterID != 0)
            {
                iquery = iquery.Where(p => p.SemesterID == SemesterID);
            }
            if (MonthID != 0)
            {
                iquery = iquery.Where(p => p.MonthID == MonthID);
            }
            if (DateTimeComment.HasValue)
            {
                iquery = iquery.Where(p => p.DateTimeComment == DateTimeComment);
            }
            if (lstSubjectID.Count > 0)
            {
                iquery = iquery.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            if (lstClassID.Count > 0)
            {
                iquery = iquery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstPupilID.Count > 0)
            {
                iquery = iquery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            return iquery;
        }
        public void InsertOrUpdateComment(List<CommentOfPupilBO> lstCommentOfPupilBO,IDictionary<string,object> dic)
        {
            List<CommentOfPupil> lstCommentOfPupilDB = this.Search(dic).ToList();
            CommentOfPupil objDB = null;
            List<CommentOfPupil> lstInsert = new List<CommentOfPupil>();
            CommentOfPupil objInsert = null;
            CommentOfPupilBO objCommentBO = null;
            int PartitionID = UtilsBusiness.GetPartionId(Utils.GetInt(dic,"SchoolID"));
            for (int i = 0; i < lstCommentOfPupilBO.Count; i++)
            {
                objCommentBO = lstCommentOfPupilBO[i];
                objDB = lstCommentOfPupilDB.Where(p => p.PupilID == objCommentBO.PupilID).FirstOrDefault();
                if (objDB != null)
                {
                    if ((!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objCommentBO.Comment)) || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objCommentBO.Comment))
                        || ((!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objCommentBO.Comment) && objDB.Comment != objCommentBO.Comment)))
                    {
                        objDB.Comment = objCommentBO.Comment;
                        objDB.LogChangeID = objCommentBO.LogChangeID;
                        objDB.UpdateTime = DateTime.Now;
                        CommentOfPupilBusiness.Update(objDB);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(objCommentBO.Comment))
                    {
                        objInsert = new CommentOfPupil();
                        objInsert.SchoolID = objCommentBO.SchoolID;
                        objInsert.AcademicYearID = objCommentBO.AcademicYearID;
                        objInsert.ClassID = objCommentBO.ClassID;
                        objInsert.SubjectID = objCommentBO.SubjectID;
                        objInsert.SemesterID = objCommentBO.SemesterID;
                        objInsert.PupilID = objCommentBO.PupilID;
                        objInsert.MonthID = objCommentBO.MonthID;
                        objInsert.DateTimeComment = objCommentBO.DateTimeComment;
                        objInsert.Comment = objCommentBO.Comment;
                        objInsert.LastDigitSchoolID = PartitionID;
                        objInsert.LogChangeID = objCommentBO.LogChangeID;
                        objInsert.CreateTime = DateTime.Now;
                        objInsert.UpdateTime = DateTime.Now;
                        lstInsert.Add(objInsert);
                    }
                }
            }
            if (lstInsert.Count > 0)
            {
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    CommentOfPupilBusiness.Insert(lstInsert[i]);
                }
            }
            CommentOfPupilBusiness.SaveDetect();
        }
        public void DeleteComment(IDictionary<string,object> dic)
        {
            List<CommentOfPupil> lstDelete = this.Search(dic).ToList();
            CommentOfPupil objDelete = null;
            for (int i = 0; i < lstDelete.Count; i++)
            {
                objDelete = lstDelete[i];
                if (!string.IsNullOrEmpty(objDelete.Comment))
                {
                    objDelete.Comment = "";
                    objDelete.UpdateTime = DateTime.Now;
                    CommentOfPupilBusiness.Update(objDelete);
                }
            }
            CommentOfPupilBusiness.Save();
        }

        public List<CommentOfPupilBO> GetCommentOfPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int SemesterID, int DayOfMonth, DateTime FromDate, DateTime ToDate, int NoDate, int NoSubject)
        {
            string SPACE = "\r\n";
            List<CommentOfPupilBO> lstResult = new List<CommentOfPupilBO>();
            CommentOfPupilBO objResult = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstPupilID",lstPupilID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID}
            };

            DateTime fDate = DateTime.ParseExact(FromDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null);
            DateTime tDate = DateTime.ParseExact(ToDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null);

            List<CommentOfPupilBO> lstCommentOfPupil = new List<CommentOfPupilBO>();
            List<CommentOfPupilBO> lsttmp = null;
            CommentOfPupilBO objCommentOfPupil = null;
            lstCommentOfPupil = (from cop in CommentOfPupilBusiness.Search(dic).Where(p => fDate <= p.DateTimeComment && p.DateTimeComment <= tDate)
                                 join sc in SubjectCatBusiness.All on cop.SubjectID equals sc.SubjectCatID
                                 select new CommentOfPupilBO
                                 {
                                     PupilID = cop.PupilID,
                                     ClassID = cop.ClassID,
                                     SubjectID = cop.SubjectID,
                                     SubjectName = sc.DisplayName,
                                     DateTimeComment = cop.DateTimeComment,
                                     MonthID = cop.MonthID,
                                     Comment = cop.Comment,
                                     OrderInSubject = sc.OrderInSubject,
                                     CreateTime = cop.CreateTime,
                                     UpdateTime = cop.UpdateTime,
                                     LogChangeID = cop.LogChangeID
                                 }).ToList();
            int PupilID = 0;
            string CommentOfPupils = string.Empty;
            for (int i = 0; i < lstPupilID.Count; i++)
            {
                objResult = new CommentOfPupilBO();
                CommentOfPupils = string.Empty;
                PupilID = lstPupilID[i];
                objResult.PupilID = PupilID;
                lsttmp = lstCommentOfPupil.Where(p => p.PupilID == PupilID).OrderBy(p => p.DateTimeComment).ThenBy(p => p.OrderInSubject).ToList();
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    objCommentOfPupil = lsttmp[j];
                    if (!string.IsNullOrEmpty(objCommentOfPupil.Comment))
                    {
                        if (DayOfMonth > 0 && SubjectID > 0)
                        {
                            CommentOfPupils += objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                        }
                        else
                        {
                            if (NoDate == 1)
                            {
                                if (NoSubject == 1)
                                {
                                    CommentOfPupils += objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                                else
                                {
                                    CommentOfPupils += "[" + objCommentOfPupil.SubjectName + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                            }
                            else
                            {
                                if (NoSubject == 1)
                                {
                                    CommentOfPupils += "[" + objCommentOfPupil.DateTimeComment.ToString("dd/MM/yyyy") + "]" + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                                else
                                {
                                    CommentOfPupils += "[" + objCommentOfPupil.DateTimeComment.ToString("dd/MM/yyyy") + "][" + objCommentOfPupil.SubjectName + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                            }
                        }
                    }
                }
                objResult.Comment = CommentOfPupils;
                lstResult.Add(objResult);
            }
            return lstResult;
        }
    }
}