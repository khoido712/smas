﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Configuration;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    /// <summary>
    /// thông tin xếp loại học    lực / hạnh kiểm và xếp thứ bậc học sinh trong lớp
    /// <author>QuangLM</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class PupilRankingBusiness
    {
        private const int MIN_AVERAGEMARK = 0;
        private const int MAX_AVERAGEMARK = 10;
        private const int ISSECONDSEMESTERTOSEMESTERALL = 1;
        public const int MAX_EDUCATIONLEVEL_PRIMARY = 5;
        /// <summary>
        /// string.Format "Nghi co phep: {0}"
        /// </summary>
        private const string FORMAT_ABSENCE_P_0 = "Nghỉ có phép: {0}";

        /// <summary>
        /// string.Format "Nghi khong phep: {0}"
        /// </summary>
        private const string FORMAT_ABSENCE_K_0 = "Nghỉ không phép: {0}";

        /// <summary>
        /// format "{0}({1}), "
        /// </summary>
        private const string FORMAT_FAULT_0_1 = "{0}{1} ({2}), ";
        /// <summary>
        ///Lấy ra lớp học cùng với số học sinh lên lớp, ở lại lớp
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm:</returns>
        public List<PupilOfClassRanking> TransferData(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            string Period = Utils.GetString(dic, "Period");
            int Semester = Utils.GetInt(dic, "Semester");
            int ProfileStatus = Utils.GetInt(dic, "ProfileStatus");
            int? StudyingJudgementID = Utils.GetShort(dic, "StudyingJudgementID");
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            int year = aca.Year;
            int modSchoolID = aca.SchoolID % 100;

            IQueryable<VPupilRanking> lsPupilRanking = VPupilRankingBusiness.All.Where(o => o.CreatedAcademicYear == year && o.Last2digitNumberSchool == modSchoolID);
            IQueryable<PupilOfClass> lsPupilOfClass = PupilOfClassRepository.All.Where(c => c.AcademicYearID == AcademicYearID);

            var lsPupilRankingRanking = from pr in lsPupilRanking
                                        join poc in lsPupilOfClass on pr.PupilID equals poc.PupilID
                                        where (pr.StudyingJudgementID == StudyingJudgementID)
                                        select new PupilOfClassRanking
                                        {
                                            ClassID = pr.ClassID,
                                            PupilID = pr.PupilID,
                                            AcademicYearID = pr.AcademicYearID,
                                            EducationLevelID = pr.EducationLevelID,
                                            //Period = pr.PeriodDeclaration.Resolution,
                                            ProfileStatus = poc.Status,
                                            Semester = pr.Semester,
                                            StudyingJudgementID = pr.StudyingJudgementID

                                        };

            if (ClassID != 0)
            {
                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.ClassID == ClassID));
            }

            if (AcademicYearID != 0)
            {
                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.AcademicYearID == AcademicYearID));
            }


            if (EducationLevelID != 0)
            {
                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.EducationLevelID == EducationLevelID));
            }


            if (Period.Trim().Length != 0)
            {
                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.Period == Period));
            }


            if (Semester != 0)
            {
                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.Semester == Semester));
            }


            if (ProfileStatus != 0)
            {

                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.ProfileStatus == ProfileStatus));
            }


            if (StudyingJudgementID != 0)
            {

                lsPupilRankingRanking = lsPupilRankingRanking.Where(em => (em.StudyingJudgementID == StudyingJudgementID));
            }


            return lsPupilRankingRanking.ToList();


        }

        /// <summary>
        /// QuangLM
        /// Cap nhat xep loai hanh kiem
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="Semester"></param>
        /// <param name="IsSecondSemesterToSemesterAll"></param>
        public void RankingPupilConduct(int UserID, List<PupilRanking> lstPupilRanking, int Semester, bool IsSecondSemesterToSemesterAll)
        {
            try
            {
                if (lstPupilRanking == null || lstPupilRanking.Count == 0) return;
                PupilRanking firstPr = lstPupilRanking.First();

                SetAutoDetectChangesEnabled(false);
                #region validate du lieu

                int semester = firstPr.Semester.Value;
                int? periodID = firstPr.PeriodID;
                int? grade = EducationLevelBusiness.Find(firstPr.EducationLevelID).Grade;

                var lstClassVsSchool = lstPupilRanking.Select(u => new { u.ClassID, u.EducationLevelID, u.SchoolID, u.AcademicYearID }).Distinct();
                foreach (var item in lstClassVsSchool)
                {
                    // Validate du lieu dau vao
                    ClassProfile classProfile = ClassProfileBusiness.Find(item.ClassID);
                    if (classProfile == null || classProfile.EducationLevelID != item.EducationLevelID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (classProfile.AcademicYearID != item.AcademicYearID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    AcademicYear academicYear = AcademicYearBusiness.Find(item.AcademicYearID);
                    if (academicYear == null || academicYear.SchoolID != item.SchoolID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    // Nam hoc hien tai
                    if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    {
                        throw new BusinessException("Common_Validate_IsCurrentYear");
                    }

                    var lstPoc = PupilOfClassBusiness.SearchBySchool(item.SchoolID, new Dictionary<string, object> { { "ClassID", item.ClassID }, { "Check", "Check" } })
                        .Select(o => new { o.PupilID, o.Status })
                        .ToList();

                    List<PupilRanking> lstPrOfClass = lstPupilRanking.Where(u => u.ClassID == item.ClassID).ToList();
                    foreach (PupilRanking pr in lstPrOfClass)
                    {
                        var poc = lstPoc.FirstOrDefault(u => u.PupilID == pr.PupilID);
                        if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            throw new BusinessException("PupilRanking_Validate_ProfileStatus");
                        // Chi can check co phai la nam hoc hien tai (nhu tren) la du.
                        //if (poc.CurrentAcademicYearID != pr.AcademicYearID)
                        //    throw new BusinessException("Common_Validate_NotCompatible");
                    }

                }
                #endregion

                Dictionary<int, bool> dicPermission = new Dictionary<int, bool>();
                lstPupilRanking.Select(u => u.ClassID).Distinct().ToList().ForEach(u =>
                {
                    dicPermission[u] = UtilsBusiness.HasHeadTeacherPermission(UserID, u);
                });

                Dictionary<int, List<PupilAbsenceBO>> dicAbsence = new Dictionary<int, List<PupilAbsenceBO>>();
                lstClassVsSchool.ToList().ForEach(u =>
                {
                    dicAbsence[u.ClassID] = PupilAbsenceBusiness.GetListPupilAbsenceBySection(new Dictionary<string, object> { { "SchoolID", u.SchoolID }, { "AcademicYearID", u.AcademicYearID }, { "ClassID", u.ClassID } }).ToList();
                });


                Dictionary<int, List<PupilRanking>> dicPupilRanking = new Dictionary<int, List<PupilRanking>>();
                Dictionary<int, List<PupilRanking>> dicPupilRankingAll = new Dictionary<int, List<PupilRanking>>();
                Dictionary<int, List<PupilRanking>> dicPupilRankingNotRLL = new Dictionary<int, List<PupilRanking>>();
                lstClassVsSchool.ToList().ForEach(u =>
                {
                    dicPupilRanking[u.ClassID] = this.SearchBySchool(u.SchoolID, new Dictionary<string, object> { { "ClassID", u.ClassID }, { "Semester", semester }, { "PeriodID", periodID } }).ToList();
                    dicPupilRankingNotRLL[u.ClassID] = this.SearchBySchool(u.SchoolID, new Dictionary<string, object> { { "ClassID", u.ClassID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING }, { "PeriodID", periodID } }).ToList();
                    if (IsSecondSemesterToSemesterAll == true && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        dicPupilRankingAll[u.ClassID] = this.SearchBySchool(u.SchoolID, new Dictionary<string, object> { { "ClassID", u.ClassID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_ALL } }).ToList();
                });
                Dictionary<int, List<int>> dicPupilThreadMark = new Dictionary<int, List<int>>();
                foreach (var PupilRanking in lstPupilRanking)
                {
                    if (dicPermission[PupilRanking.ClassID])
                    {
                        PupilRanking existedPupilRanking = dicPupilRanking[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);

                        if (existedPupilRanking != null)//Nếu tìm thấy thì thêm vào đối tượng tìm được các thuộc tính của lstPupilRanking[i] và thực hiện update
                        {
                            // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                            // tinh lai xep hang va xep loai cho hoc sinh
                            #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                            if (existedPupilRanking.ConductLevelID != PupilRanking.ConductLevelID)
                            {
                                if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                {
                                    List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                    if (!listPupilVal.Contains(PupilRanking.PupilID))
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                    dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                }
                            }
                            #endregion

                            existedPupilRanking.ConductLevelID = PupilRanking.ConductLevelID;
                            existedPupilRanking.TotalAbsentDaysWithoutPermission = PupilRanking.TotalAbsentDaysWithoutPermission;
                            existedPupilRanking.TotalAbsentDaysWithPermission = PupilRanking.TotalAbsentDaysWithPermission;
                            existedPupilRanking.RankingDate = DateTime.Now;
                            existedPupilRanking.PupilRankingComment = PupilRanking.PupilRankingComment;
                            this.Update(existedPupilRanking);
                            if (grade != SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                            {
                                if (existedPupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                {
                                    if (existedPupilRanking.StudyingJudgementID != SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                    {
                                        int totalAbsenceWithoutPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == false).Count();
                                        int totalAbsenceWithPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == true).Count();
                                        PupilRanking pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                        if (pupilRankingNotRLL != null)
                                        {
                                            pupilRankingNotRLL.ConductLevelID = PupilRanking.ConductLevelID;
                                            pupilRankingNotRLL.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                            pupilRankingNotRLL.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                            pupilRankingNotRLL.RankingDate = DateTime.Now;
                                            this.Update(pupilRankingNotRLL);
                                        }
                                    }
                                }
                            }

                            if (grade == SystemParamsInFile.EDUCATION_GRADE_PRIMARY && existedPupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (existedPupilRanking.StudyingJudgementID != SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                {
                                    int totalAbsenceWithoutPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == false).Count();
                                    int totalAbsenceWithPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == true).Count();
                                    PupilRanking pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                    if (pupilRankingNotRLL != null)
                                    {
                                        pupilRankingNotRLL.ConductLevelID = PupilRanking.ConductLevelID;
                                        pupilRankingNotRLL.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                        pupilRankingNotRLL.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                        pupilRankingNotRLL.RankingDate = DateTime.Now;
                                        this.Update(pupilRankingNotRLL);
                                    }
                                }
                            }

                            if (existedPupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                            {
                                PupilRanking pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                if (pupilRankingNotRLL != null)
                                {
                                    if (PupilRanking.ConductLevelID == null)
                                    {
                                        this.Delete(pupilRankingNotRLL.PupilRankingID);
                                    }
                                }
                            }
                        }
                        else//Nếu không tìm thấy thực hiện insert vào bảng PupilRanking đối tượng lstPupilRanking[i]
                        {
                            PupilRanking.RankingDate = DateTime.Now;
                            if (PupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                            {

                                if (PupilRanking.ConductLevelID != null)
                                {
                                    this.Insert(PupilRanking);
                                }

                            }
                            else
                            {
                                this.Insert(PupilRanking);
                                // Khong xu ly doi voi truong hop ren luyen lai
                                // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                                // tinh lai xep hang va xep loai cho hoc sinh
                                #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                                if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                {
                                    List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                    if (!listPupilVal.Contains(PupilRanking.PupilID))
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                    dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                }
                                #endregion
                            }
                        }

                        if (IsSecondSemesterToSemesterAll == true && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            PupilRanking pr2 = dicPupilRankingAll[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);

                            int totalAbsenceWithoutPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == false).Count();
                            int totalAbsenceWithPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == true).Count();

                            if (pr2 != null)
                            {
                                // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                                // tinh lai xep hang va xep loai cho hoc sinh
                                #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                                if (pr2.ConductLevelID != PupilRanking.ConductLevelID)
                                {
                                    if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                    {
                                        List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                        if (!listPupilVal.Contains(PupilRanking.PupilID))
                                        {
                                            dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                        }
                                    }
                                    else
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                #endregion
                                pr2.ConductLevelID = PupilRanking.ConductLevelID;
                                pr2.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                pr2.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                pr2.RankingDate = DateTime.Now;
                                pr2.PupilRankingComment = PupilRanking.PupilRankingComment;
                                this.Update(pr2);
                                PupilRanking pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                if (pupilRankingNotRLL != null)
                                {
                                    pupilRankingNotRLL.ConductLevelID = PupilRanking.ConductLevelID;
                                    pupilRankingNotRLL.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                    pupilRankingNotRLL.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                    pupilRankingNotRLL.RankingDate = DateTime.Now;
                                    this.Update(pupilRankingNotRLL);
                                }
                            }
                            else if (pr2 == null)
                            {
                                PupilRanking pr = new PupilRanking();
                                pr.SchoolID = PupilRanking.SchoolID;
                                pr.AcademicYearID = PupilRanking.AcademicYearID;
                                pr.CapacityLevelID = PupilRanking.CapacityLevelID;
                                pr.ClassID = PupilRanking.ClassID;
                                pr.ConductLevelID = PupilRanking.ConductLevelID;
                                pr.EducationLevelID = PupilRanking.EducationLevelID;
                                pr.PupilID = PupilRanking.PupilID;
                                pr.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                pr.TotalAbsentDaysWithoutPermission = totalAbsenceWithoutPer;
                                pr.TotalAbsentDaysWithPermission = totalAbsenceWithPer;
                                pr.RankingDate = DateTime.Now;
                                pr.CreatedAcademicYear = PupilRanking.CreatedAcademicYear;
                                pr.Last2digitNumberSchool = PupilRanking.SchoolID % 100;
                                pr.PupilRankingComment = PupilRanking.PupilRankingComment;
                                this.Insert(pr);
                                // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                                // tinh lai xep hang va xep loai cho hoc sinh
                                #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                                if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                {
                                    List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                    if (!listPupilVal.Contains(PupilRanking.PupilID))
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                    dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                }
                                #endregion
                            }
                        }
                    }
                }

                this.Save();
                #region Save ThreadMark
                if (grade != SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    foreach (int key in dicPupilThreadMark.Keys)
                    {
                        List<int> listPupilVal = dicPupilThreadMark[key];
                        ThreadMarkBO info = new ThreadMarkBO();
                        info.SchoolID = firstPr.SchoolID;
                        info.ClassID = key;
                        info.AcademicYearID = firstPr.AcademicYearID;
                        info.Semester = Semester;
                        info.PeriodID = periodID;
                        info.Type = GlobalConstants.CONDUCT_AUTO_TYPE;
                        info.PupilIds = listPupilVal;
                        ThreadMarkBusiness.InsertActionList(info);
                    }
                }
                #endregion

            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin xếp loại học lực / hạnh kiểm và xếp thứ bậc học sinh trong lớp theo đợt (giai đoạn) / học kỳ / năm học
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<PupilRanking> Search(IDictionary<string, object> dic)
        {
            IQueryable<PupilRanking> lsPupilRanking = this.PupilRankingRepository.All;
            int StudyingJudgementID = Utils.GetInt(dic, "StudyingJudgementID");
            int PupilRankingID = Utils.GetInt(dic, "PupilRankingID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AppliedLevel = Utils.GetByte(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetByte(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            decimal? AverageMark = Utils.GetNullableDecimal(dic, "AverageMark");
            int? TotalAbsentDaysWithPermission = Utils.GetNullableByte(dic, "TotalAbsentDaysWithPermission");
            int? TotalAbsentDaysWithoutPermission = Utils.GetNullableByte(dic, "TotalAbsentDaysWithoutPermission");
            int CapacityLevelID = Utils.GetInt(dic, "CapacityLevelID");
            int ConductLevelID = Utils.GetInt(dic, "ConductLevelID");
            int? Rank = Utils.GetNullableInt(dic, "Rank");
            DateTime? RankingDay = Utils.GetDateTime(dic, "RankingDay");
            bool? IsCategory = Utils.GetNullableBool(dic, "IsCategory");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");

            if (ClassID != 0)
            {
                lsPupilRanking = from pr in lsPupilRanking
                                 join cp in ClassProfileBusiness.All on pr.ClassID equals cp.ClassProfileID
                                 where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                 && pr.ClassID == ClassID
                                 select pr;
            }
            if (PupilRankingID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.PupilRankingID == PupilRankingID));
            if (PupilID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.PupilID == PupilID));
            if (AcademicYearID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.AcademicYearID == AcademicYearID));
                /*var aca = this.AcademicYearBusiness.All
                  .Where(p => p.AcademicYearID == AcademicYearID)
                  .Select(p => new { AcademicYearID = p.AcademicYearID, SchoolID = p.SchoolID, Year = p.Year })
                  .FirstOrDefault();
                if (aca == null) throw new BusinessException("Năm học không tồn tại");
                int? createAcaYear = aca.Year;
                lsPupilRanking = lsPupilRanking.Where(o => (o.CreatedAcademicYear == createAcaYear));
                 * */
                if (SchoolID <= 0)
                {
                    var aca = this.AcademicYearBusiness.Find(AcademicYearID);
                    if (aca != null)
                    {
                        lsPupilRanking = lsPupilRanking.Where(o => (o.Last2digitNumberSchool == aca.SchoolID % 100));
                    }
                }
            }
            if (SchoolID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.SchoolID == SchoolID));
                lsPupilRanking = lsPupilRanking.Where(em => em.Last2digitNumberSchool == (SchoolID % 100));
            }
            if (AppliedLevel != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => em.EducationLevelID == EducationLevelID);
            }
            if (Year != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.CreatedAcademicYear == Year));
            }

            if (Semester != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.Semester == Semester));
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID.HasValue)
                {
                    if (PeriodID != 0)
                    {
                        lsPupilRanking = lsPupilRanking.Where(em => (em.PeriodID == PeriodID));
                    }
                    else
                    {
                        lsPupilRanking = lsPupilRanking.Where(em => !em.PeriodID.HasValue);
                    }
                }
                else
                {
                    lsPupilRanking = lsPupilRanking.Where(em => !em.PeriodID.HasValue);
                }
            }
            if (AverageMark != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.AverageMark == AverageMark));
            }
            if (TotalAbsentDaysWithPermission != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.TotalAbsentDaysWithPermission == TotalAbsentDaysWithPermission));
            }
            if (TotalAbsentDaysWithoutPermission != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.TotalAbsentDaysWithoutPermission == TotalAbsentDaysWithoutPermission));
            }
            if (CapacityLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.CapacityLevelID == CapacityLevelID));
            }
            if (ConductLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.ConductLevelID == ConductLevelID));
            }
            if (Rank != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.Rank == Rank));
            }
            if (RankingDay != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.RankingDate == RankingDay));
            }
            if (IsCategory.HasValue)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.IsCategory == IsCategory.Value));
            }
            if (StudyingJudgementID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(o => o.StudyingJudgementID == StudyingJudgementID);
            }


            return lsPupilRanking;

        }

        /// <summary>
        ///Lấy danh sách học sinh để thực hiện xếp loại học sinh
        ///lastest updated date 07-01-2013
        ///updated by namdv3
        /// </summary>
        /// <param name="dic">
        /// AcademicYearID (int)
        /// Semester (int)
        /// ClassID (int) </param>
        /// <returns></returns>
        public IQueryable<PupilRankingBO> GetPupilToRank(IDictionary<string, object> dic, bool isShowRetetResult)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? Semester = Utils.GetByte(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string strGDTX = "GDTX";
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
            bool isGDTX = (sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX);
            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
            int AppliedLevel = cp.EducationLevel.Grade;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            bool isNotShowPupil = aca.IsShowPupil.HasValue && aca.IsShowPupil.Value;
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            search["Check"] = "Check";
            IQueryable<PupilOfClass> lsPupilOfClassAll = PupilOfClassBusiness.SearchBySchool(SchoolID, search).AddPupilStatus(isNotShowPupil);

            //2015/03/24: viethd4: Fix cach lay so ngay nghi


            int modSchoolID = SchoolID % 100;

            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                DateTime startDateOfSemester, dueDateOfSemester;
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    startDateOfSemester = aca.FirstSemesterStartDate ?? DateTime.MaxValue;
                    dueDateOfSemester = aca.FirstSemesterEndDate ?? DateTime.MinValue;
                }
                else
                {
                    startDateOfSemester = aca.SecondSemesterStartDate ?? DateTime.MaxValue;
                    dueDateOfSemester = aca.SecondSemesterEndDate ?? DateTime.MinValue;
                }
                List<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassAll.AddCriteriaSemester(aca, Semester.Value).ToList();
                var temp = //from poc in PupilOfClassRepository.All
                           from poc in lsPupilOfClassAll
                               //join poc2 in lsPupilOfClassSemester on poc.PupilID equals poc2.PupilID into g4
                           join pr in VPupilRankingBusiness.All.Where(o => o.CreatedAcademicYear == aca.Year && o.Last2digitNumberSchool == modSchoolID && o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && !o.PeriodID.HasValue) on poc.PupilID equals pr.PupilID into g1
                           join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID into g2
                           join pe in PupilEmulationRepository.All.Where(o => o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && o.Last2digitNumberSchool == modSchoolID)
                           on poc.PupilID equals pe.PupilID into g3
                           from j1 in g1.DefaultIfEmpty()
                           from j2 in g2.DefaultIfEmpty()
                           from j3 in g3.DefaultIfEmpty()
                               //from j4 in g4.DefaultIfEmpty()
                           where
                            //poc.AcademicYearID == AcademicYearID
                            //&& poc.ClassID == ClassID && 
                            !j1.PeriodID.HasValue
                           && j2.IsActive
                           select new PupilRankingBO
                           {
                               PupilRankingID = j1.PupilRankingID,
                               PupilID = poc.PupilID,
                               Semester = j1.Semester.Value,
                               ClassID = poc.ClassID,
                               EducationLevelID = j1.EducationLevelID,
                               SchoolID = poc.SchoolID,
                               AcademicYearID = poc.AcademicYearID,
                               ProfileStatus = poc.Status,
                               PupilEmulationID = j3.PupilEmulationID,
                               HonourAchivementTypeID = j3.HonourAchivementTypeID,
                               PupilCode = j2.PupilCode,
                               FullName = j2.FullName,
                               CapacityLevelID = j1.CapacityLevelID,
                               ConductLevelID = j1.ConductLevelID,
                               Genre = j2.Genre,
                               StudyingJudgementID = j1.StudyingJudgementID,
                               BirthDate = j2.BirthDate,
                               AssignedDate = poc.AssignedDate,
                               //TotalAbsentDaysWithoutPermission = j1.TotalAbsentDaysWithoutPermission,
                               //TotalAbsentDaysWithPermission = j1.TotalAbsentDaysWithPermission,
                               //TotalAbsentDaysWithoutPermission = j2.PupilAbsences.Where(p => p.SchoolID == poc.SchoolID
                               //    && p.AcademicYearID == poc.AcademicYearID && p.ClassID == poc.ClassID
                               //    && p.AbsentDate >= startDateOfSemester && p.AbsentDate <= dueDateOfSemester
                               //    && p.IsAccepted == false)
                               //    .Count(),
                               //TotalAbsentDaysWithPermission = j2.PupilAbsences.Where(p => p.SchoolID == poc.SchoolID
                               //    && p.AcademicYearID == poc.AcademicYearID && p.ClassID == poc.ClassID
                               //    && p.AbsentDate >= startDateOfSemester && p.AbsentDate <= dueDateOfSemester
                               //    && p.IsAccepted == true)
                               //    .Count(),
                               OrderInClass = poc.OrderInClass,
                               Name = j2.Name,
                               Status = poc.Status,
                               //ShowMark = j4 != null ? true : (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) ? true : false,
                               LearningType = j2.PupilLearningType,
                               EndDate = poc.EndDate,
                               IsCategory = j1.IsCategory != null ? j1.IsCategory.Value : false
                           };
                var lstPupilRankingBO = temp.ToList();
                List<int> lstPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();
                /*List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                                                 .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
                PupilProfile pupilProfile = new PupilProfile();*/

                //viethd4: sua cach tinh so ngay nghi
                Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                dicAbsence["AcademicYearID"] = AcademicYearID;
                dicAbsence["ClassID"] = ClassID;
                dicAbsence["SchoolID"] = SchoolID;
                dicAbsence["FromAbsentDate"] = startDateOfSemester;
                dicAbsence["ToAbsentDate"] = dueDateOfSemester;
                List<int> lstSectionKeyID = cp.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(cp.SeperateKey.Value) : new List<int>();
                List<PupilAbsence> lstAbsenceAll = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsence).Where(p => lstSectionKeyID.Contains(p.Section)).ToList();

                foreach (var prBO in lstPupilRankingBO)
                {
                    //pupilProfile = lstPupilProfile.FirstOrDefault(o => o.PupilProfileID == prBO.PupilID);
                    prBO.IsKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, prBO.LearningType, prBO.BirthDate);
                    var poc = lsPupilOfClassSemester.FirstOrDefault(o => o.PupilID == prBO.PupilID);
                    if (poc != null || (prBO.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || prBO.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                    {
                        prBO.ShowMark = true;
                    }

                    prBO.ToTalAbsentDay = lstAbsenceAll.Where(o => o.PupilID == prBO.PupilID).Count();
                    prBO.TotalAbsentDaysWithoutPermission = lstAbsenceAll.Where(o => o.IsAccepted != true && o.PupilID == prBO.PupilID).Count();
                    prBO.TotalAbsentDaysWithPermission = lstAbsenceAll.Where(o => o.IsAccepted == true && o.PupilID == prBO.PupilID).Count();
                }
                return lstPupilRankingBO.AsQueryable();
            }

            else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                List<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassAll.AddCriteriaSemester(aca, SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                var lst1 = VPupilRankingBusiness.All.Where(o => o.ClassID == ClassID && o.CreatedAcademicYear == aca.Year && o.Last2digitNumberSchool == modSchoolID && o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !o.PeriodID.HasValue);
                var lst2 = VPupilRankingBusiness.All.Where(o => o.ClassID == ClassID && o.CreatedAcademicYear == aca.Year && o.Last2digitNumberSchool == modSchoolID && o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !o.PeriodID.HasValue);
                var temp = from poc in lsPupilOfClassAll
                               //join poc2 in lsPupilOfClassSemester on poc.PupilID equals poc2.PupilID into g6
                           join pr in VPupilRankingBusiness.All.Where(o => o.CreatedAcademicYear == aca.Year && o.Last2digitNumberSchool == modSchoolID && o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester >= GlobalConstants.SEMESTER_OF_YEAR_ALL && !o.PeriodID.HasValue)
                           on poc.PupilID equals pr.PupilID into u1
                           from u2 in u1.DefaultIfEmpty()
                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           join pe in PupilEmulationBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && o.Last2digitNumberSchool == modSchoolID)
                           on poc.PupilID equals pe.PupilID into g1
                           from j2 in g1.DefaultIfEmpty()
                           join pr1 in lst1 on poc.PupilID equals pr1.PupilID into g2
                           from g3 in g2.DefaultIfEmpty()
                           join pr2 in lst2 on poc.PupilID equals pr2.PupilID into g4
                           from g5 in g4.DefaultIfEmpty()
                               //from j6 in g6.DefaultIfEmpty()
                           where pp.IsActive
                           select new PupilRankingBO
                           {
                               PupilRankingID = u2.PupilRankingID,
                               PupilID = poc.PupilID,
                               Semester = u2.Semester.Value,
                               ClassID = poc.ClassID,
                               EducationLevelID = u2.EducationLevelID,
                               SchoolID = poc.SchoolID,
                               AcademicYearID = poc.AcademicYearID,
                               ProfileStatus = poc.Status,
                               PupilEmulationID = j2.PupilEmulationID,
                               HonourAchivementTypeID = j2.HonourAchivementTypeID,
                               PupilCode = pp.PupilCode,
                               FullName = pp.FullName,
                               CapacityLevelID = u2.CapacityLevelID,
                               ConductLevelID = u2.ConductLevelID,
                               Genre = pp.Genre,
                               StudyingJudgementID = u2.StudyingJudgementID,
                               BirthDate = pp.BirthDate,
                               AssignedDate = poc.AssignedDate,
                               //TotalAbsentDaysWithoutPermission = pp.PupilAbsences.Where(p => p.SchoolID == poc.SchoolID
                               //    && p.AcademicYearID == poc.AcademicYearID && p.ClassID == poc.ClassID
                               //    && p.AbsentDate >= aca.FirstSemesterStartDate && p.AbsentDate <= aca.SecondSemesterEndDate
                               //    && p.IsAccepted == false)
                               //    .Count(),
                               ////TotalAbsentDaysWithoutPermission = u2.TotalAbsentDaysWithoutPermission,
                               //TotalAbsentDaysWithPermission = pp.PupilAbsences.Where(p => p.SchoolID == poc.SchoolID
                               //    && p.AcademicYearID == poc.AcademicYearID && p.ClassID == poc.ClassID
                               //    && p.AbsentDate >= aca.FirstSemesterStartDate && p.AbsentDate <= aca.SecondSemesterEndDate
                               //    && p.IsAccepted == true)
                               //    .Count(),
                               //TotalAbsentDaysWithPermission = u2.TotalAbsentDaysWithPermission,
                               OrderInClass = poc.OrderInClass,
                               Status = poc.Status,
                               //ShowMark = j6 != null ? true : false,
                               LearningType = pp.PupilLearningType,
                               EndDate = poc.EndDate,
                               IsCategory = u2.IsCategory != null ? u2.IsCategory.Value : false
                           };

                //Chiendd1: 24/06/2016, sua lai luong hien thi ket qua thi lai. Doi voi hoc ky ca nam thi khong hien thi ket qua thi lai
                List<PupilRankingBO> lstPupilRankingBO = new List<PupilRankingBO>();
                List<PupilRankingBO> semesterAll = new List<PupilRankingBO>();
                var temp2 = temp.ToList();
                if (isShowRetetResult)
                {
                    var maxSemester = temp2.Where(u => u.Semester == null || u.Semester == temp2.Where(v => v.PupilID == u.PupilID).Max(v => v.Semester));
                    semesterAll = temp2.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                    lstPupilRankingBO = maxSemester.ToList();
                }
                else
                {
                    lstPupilRankingBO = temp2.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                }
                List<int> lstPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();
                /*List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                                                 .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();

                PupilProfile pupilProfile = new PupilProfile();*/

                Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                dicAbsence["AcademicYearID"] = AcademicYearID;
                dicAbsence["ClassID"] = ClassID;
                dicAbsence["SchoolID"] = SchoolID;
                List<int> lstSectionKeyID = cp.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(cp.SeperateKey.Value) : new List<int>();

                List<PupilAbsence> lstAbsenceAll = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsence)
                                                                        .Where(o => ((o.AbsentDate >= aca.FirstSemesterStartDate && o.AbsentDate <= aca.FirstSemesterEndDate)
                                                                        || (o.AbsentDate >= aca.SecondSemesterStartDate && o.AbsentDate <= aca.SecondSemesterEndDate)) && lstSectionKeyID.Contains(o.Section)).ToList();



                foreach (var prBO in lstPupilRankingBO)
                {
                    //pupilProfile = lstPupilProfile.FirstOrDefault(o => o.PupilProfileID == prBO.PupilID);
                    prBO.IsKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, prBO.LearningType, prBO.BirthDate);
                    var poc = lsPupilOfClassSemester.FirstOrDefault(o => o.PupilID == prBO.PupilID);
                    if (poc != null || (prBO.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || prBO.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                    {
                        prBO.ShowMark = true;
                    }

                    //viethd4: sua cach tinh so ngay nghi
                    //List<PupilAbsence> lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == prBO.PupilID).ToList();
                    prBO.ToTalAbsentDay = lstAbsenceAll.Where(o => o.PupilID == prBO.PupilID).Count();
                    prBO.TotalAbsentDaysWithoutPermission = lstAbsenceAll.Where(o => o.IsAccepted != true && o.PupilID == prBO.PupilID).Count();
                    prBO.TotalAbsentDaysWithPermission = lstAbsenceAll.Where(o => o.IsAccepted == true && o.PupilID == prBO.PupilID).Count();

                    //Nếu học sinh không phải rèn luyện lại thì lấy hạnh kiểm, số buổi nghỉ ở bản ghi cả năm
                    if (isShowRetetResult)
                    {
                        var pr = semesterAll.FirstOrDefault(o => o.PupilID == prBO.PupilID);
                        if (pr != null && pr.StudyingJudgementID != SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                        {
                            prBO.ConductLevelID = pr.ConductLevelID;
                            prBO.TotalAbsentDaysWithoutPermission = pr.TotalAbsentDaysWithoutPermission;
                            prBO.TotalAbsentDaysWithPermission = pr.TotalAbsentDaysWithPermission;
                            prBO.ConductLevel = pr.ConductLevel;
                            prBO.ToTalAbsentDay = pr.ToTalAbsentDay;
                        }
                    }
                }
                return lstPupilRankingBO.AsQueryable();
            }
            return null;
        }

        public IQueryable<PupilRankingBO> GetPupilToSaveSummedUpClass(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? Semester = Utils.GetByte(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
            int AppliedLevel = cp.EducationLevel.Grade;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            int year = aca.Year;
            int modSchoolID = SchoolID % 100;
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            search["Check"] = "Check";
            IQueryable<PupilOfClass> lsPupilOfClassAll = PupilOfClassBusiness.SearchBySchool(SchoolID, search);
            var temp =
                       from poc in lsPupilOfClassAll
                       join pr in VPupilRankingBusiness.All.Where(o => o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year && o.AcademicYearID == AcademicYearID && o.SchoolID == SchoolID && o.ClassID == ClassID && o.Semester == Semester && !o.PeriodID.HasValue) on poc.PupilID equals pr.PupilID into g1
                       join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID into g2
                       from j1 in g1.DefaultIfEmpty()
                       from j2 in g2.DefaultIfEmpty()
                       where !j1.PeriodID.HasValue
                       && j2.IsActive
                       select new PupilRankingBO
                       {
                           PupilRankingID = j1.PupilRankingID,
                           PupilID = poc.PupilID,
                           Semester = j1.Semester.Value,
                           ClassID = poc.ClassID,
                           EducationLevelID = j1.EducationLevelID,
                           SchoolID = poc.SchoolID,
                           AcademicYearID = poc.AcademicYearID,
                           ProfileStatus = poc.Status,
                           Genre = j2.Genre,
                           OrderInClass = poc.OrderInClass,
                           Status = poc.Status,
                           EndDate = poc.EndDate,
                           IsCategory = j1.IsCategory != null ? j1.IsCategory.Value : false
                       };
            //var lstPupilRankingBO = temp.ToList();
            return temp;
        }

        /*public IQueryable<PupilRankingBO> GetListPupilReward(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Semester = Utils.GetInt(dic, "Semester");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2["AcademicYearID"] = AcademicYearID;
            dic2["Check"] = "Check";

            var pupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic2);
            var pupilEmulation = PupilEmulationBusiness.SearchBySchool(SchoolID, dic);
            var pupilRanking = VPupilRankingBusiness.SearchBySchool(SchoolID, dic);

            var query = from pc in pupilOfClass
                        join cp in ClassProfileBusiness.All on pc.ClassID equals cp.ClassProfileID
                        join pe in pupilEmulation on new { pc.SchoolID, pc.AcademicYearID, pc.ClassID, pc.PupilID } equals
                                                                     new { pe.SchoolID, pe.AcademicYearID, pe.ClassID, pe.PupilID }
                        join pr in pupilRanking on new { pe.SchoolID, pe.AcademicYearID, pe.ClassID, pe.Semester, pe.PupilID } equals
                                                                     new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.Semester, pr.PupilID }
                        join cal in CapacityLevelBusiness.All on pr.CapacityLevelID equals cal.CapacityLevelID
                        join con in ConductLevelBusiness.All on pr.ConductLevelID equals con.ConductLevelID
                        where pe.AcademicYearID == AcademicYearID
                        && pe.SchoolID == SchoolID
                        && pe.Semester == Semester
                        && (cp.EducationLevel.Grade == AppliedLevel || AppliedLevel == 0)
                        && (cp.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                        && (pc.Status == 1 || pc.Status == 2)
                        select new PupilRankingBO
                        {
                            PupilRankingID = pr.PupilRankingID,
                            ClassID = pe.ClassID,
                            ClassName = cp.DisplayName,
                            PupilID = pe.PupilID,
                            FullName = pc.PupilProfile.FullName,
                            AverageMark = pr.AverageMark,
                            CapacityLevelID = pr.CapacityLevelID,
                            ConductLevelID = pr.ConductLevelID,
                            Semester = pe.Semester,
                            PupilEmulationID = pe.PupilEmulationID,
                            CapacityLevel = cal.CapacityLevel1,
                            ConductLevelName = con.Resolution,
                            HonourAchivementTypeResolution = pe.HonourAchivementType.Resolution,
                            OrderInClass = pc.OrderInClass,
                            EducationLevelID = cp.EducationLevelID
                        };
            return query;
        }*/

        /// <summary>
        /// Lấy danh sách học sinh để thực hiện xếp loại học sinh cấp 1
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilToRankPrimary(IDictionary<string, object> dic)
        {
            //D? li?u truy?n vào IDictionary; AcademicYearID (int); Semester (int) ; ClassID (int) 
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            /* Th?c hi?n tìm ki?m trong b?ng
                PupilOfClass poc leftjoin PupilRanking pr on poc.PupilID = pr.PupilID
                leftjoin PupilEmulation  pe on poc.PupilID = pe.PupilID
                where poc.AcademicYearID  = AcademicYearID
                and poc.SchoolID = SchoolID
                and poc.ClassID = ClassID
                and pr.Semester >= 2 and pr.Semester = max(pr.Semester) //L?y ra k?t qu? c?a h?c k? 2, n?u h?c sinh dã rèn luy?n l?i ho?c thi l?i thì l?y ra k?t qu? sau khi dã rèn luy?n l?i và thi l?i
                and pr.SchoolID = SchoolID
                and pr.AcademicYearID = AcademicYearID
                and pe.Semester = SEMESTER_OF_YEAR_SECOND(2)
                	lstPupilPrimary
             */
            IDictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1["AcademicYearID"] = AcademicYearID;
            dic1["SchoolID"] = SchoolID;
            dic1["ClassID"] = ClassID;
            //dic1["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            dic1["Check"] = "Check";
            int modSchoolID = SchoolID % 100;


            IQueryable<PupilOfClass> lsPupilOfClassAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic1);
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassAll.AddCriteriaSemester(aca, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            int year = aca.Year;
            var data = //from poc in PupilOfClassRepository.All
                       from poc in lsPupilOfClassAll
                       join poc2 in lsPupilOfClassSemester on poc.PupilID equals poc2.PupilID into g3
                       join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                       join pr in PupilRankingBusiness.All.Where(u => u.CreatedAcademicYear == year && u.Last2digitNumberSchool == modSchoolID && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) on new { SchoolID, AcademicYearID, poc.PupilID, ClassID }
                      equals new { pr.SchoolID, pr.AcademicYearID, pr.PupilID, pr.ClassID } into g1
                       join pe in PupilEmulationBusiness.All on new { SchoolID, AcademicYearID, poc.PupilID }
                      equals new { pe.SchoolID, pe.AcademicYearID, pe.PupilID } into g2
                       from j1 in g1.DefaultIfEmpty()
                       from j2 in g2.DefaultIfEmpty()
                       from j3 in g3.DefaultIfEmpty()
                       where poc.AcademicYearID == AcademicYearID && poc.SchoolID == SchoolID && poc.ClassID == ClassID
                       select new PupilRankingBO
                       {
                           PupilRankingID = j1 != null ? j1.PupilRankingID : new Nullable<long>(),
                           PupilID = poc.PupilID,
                           Semester = j1 != null ? j1.Semester : new Nullable<int>(),
                           ClassID = poc.ClassID,
                           EducationLevelID = j1 != null ? j1.EducationLevelID : new Nullable<int>(),
                           SchoolID = poc.SchoolID,
                           AcademicYearID = AcademicYearID,
                           ProfileStatus = poc.Status,
                           Year = year,
                           PupilEmulationID = j2 != null ? j2.PupilEmulationID : new Nullable<int>(),
                           HonourAchivementTypeID = j2 != null ? j2.HonourAchivementTypeID : new Nullable<int>(),
                           PupilCode = pp.PupilCode,
                           Name = pp.Name,
                           FullName = pp.FullName,
                           CapacityLevelID = j1 != null ? j1.CapacityLevelID : new Nullable<int>(),
                           ConductLevelID = j1 != null ? j1.ConductLevelID : new Nullable<int>(),
                           Genre = poc.PupilProfile.Genre,
                           StudyingJudgementID = j1 != null ? j1.StudyingJudgementID : new Nullable<int>(),
                           TotalAbsentDaysWithoutPermission = j1 != null ? j1.TotalAbsentDaysWithoutPermission : new Nullable<int>(),
                           TotalAbsentDaysWithPermission = j1 != null ? j1.TotalAbsentDaysWithPermission : new Nullable<int>(),
                           BirthDate = pp.BirthDate,
                           AssignedDate = poc.AssignedDate,
                           OrderInClass = poc.OrderInClass,
                           ShowMark = j3 != null ? true : false
                       };
            List<PupilRankingBO> lstPupilRankingBO = data.ToList();
            //var semsterOverSecond = data.Where(u => u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            var maxSemester = lstPupilRankingBO.Where(u => u.Semester == null || u.Semester == lstPupilRankingBO.Where(v => v.PupilID == u.PupilID).Max(v => v.Semester));
            List<PupilRankingBO> result = new List<PupilRankingBO>();
            /*
            V?i m?i ph?n t? trong lstPupilPrimary l?y ra lstPupilPrimary PupilID, ClassID, Semester
            N?u Semester = 2
            C?p nh?t l?i lstPupilPrimary[i].CapacityLevelID = PupilRankingBusiness.CategoryOfPupil(PupilID, ClassID)
            N?u Semester = 4
            C?p nh?t l?i lstPupilPrimary[i].CapacityLevelID = PupilRankingBusiness.CategoryOfPupilAfterRetest(PupilID, ClassID)
            */
            List<int> listPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();
            Dictionary<int, int?> dicCapSecond = new Dictionary<int, int?>();
            Dictionary<int, int?> dicCapBackTraining = new Dictionary<int, int?>();
            if (maxSemester.Any(o => o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
            {
                dicCapSecond = this.CategoryOfPupil(ClassID, listPupilID);
            }
            if (maxSemester.Any(o => o.Semester == GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING))
            {
                dicCapBackTraining = this.CategoryOfPupilAfterRetest(ClassID, lstPupilRankingBO);
            }
            foreach (PupilRankingBO bo in maxSemester)
            {
                if (bo.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    if (dicCapSecond.ContainsKey(bo.PupilID.Value))
                    {
                        bo.CapacityLevelID = dicCapSecond[bo.PupilID.Value];
                    }
                }
                if (bo.Semester == GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING)
                {
                    if (dicCapBackTraining.ContainsKey(bo.PupilID.Value))
                    {
                        bo.CapacityLevelID = dicCapBackTraining[bo.PupilID.Value];
                    }
                }
                result.Add(bo);
            }
            return result;
        }


        /// Lấy danh sách học sinh để thực hiện xếp loại học sinh cấp 1
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        /// Phuongtd5 19/08/2013
        /// Dùng cho cả học kỳ 1 và 2
        public List<PupilRankingBO> GetPupilToRankPrimaryv2(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int Semester = Utils.GetInt(dic, "Semester");
            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
            int EducationLevelID = cp.EducationLevelID;
            //Lấy danh sách học sinh học trong học kỳ
            IDictionary<string, object> dic1 = new Dictionary<string, object>();
            dic1["AcademicYearID"] = AcademicYearID;
            dic1["SchoolID"] = SchoolID;
            dic1["ClassID"] = ClassID;
            dic1["Check"] = "Check";
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            bool isNotShowPupil = aca.IsShowPupil.HasValue && aca.IsShowPupil.Value;
            IQueryable<PupilOfClass> lsPupilOfClassAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic1).AddPupilStatus(isNotShowPupil);
            List<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassAll.AddCriteriaSemester(aca, Semester).ToList();
            int year = aca.Year;
            int modSchoolID = SchoolID % 100;
            //Nếu học kỳ 1 - không lấy danh hiệu, thuộc diện
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                var data = //from poc in PupilOfClassRepository.All
                      from poc in lsPupilOfClassAll
                          //join poc2 in lsPupilOfClassSemester on poc.PupilID equals poc2.PupilID into g3
                      join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                      join pr in VPupilRankingBusiness.All.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == aca.Year) on new { SchoolID, AcademicYearID, poc.PupilID, ClassID }
                     equals new { pr.SchoolID, pr.AcademicYearID, pr.PupilID, pr.ClassID } into g1
                      from j1 in g1.DefaultIfEmpty()
                          //from j3 in g3.DefaultIfEmpty()
                      where poc.AcademicYearID == AcademicYearID && poc.SchoolID == SchoolID && poc.ClassID == ClassID
                      select new PupilRankingBO
                      {
                          PupilRankingID = j1 != null ? j1.PupilRankingID : new Nullable<long>(),
                          PupilID = poc.PupilID,
                          Semester = j1 != null ? j1.Semester : new Nullable<int>(),
                          ClassID = poc.ClassID,
                          EducationLevelID = EducationLevelID,
                          SchoolID = poc.SchoolID,
                          AcademicYearID = AcademicYearID,
                          ProfileStatus = poc.Status,
                          Year = year,
                          PupilCode = pp.PupilCode,
                          Name = pp.Name,
                          FullName = pp.FullName,
                          CapacityLevelID = j1 != null ? j1.CapacityLevelID : new Nullable<int>(),
                          ConductLevelID = j1 != null ? j1.ConductLevelID : new Nullable<int>(),
                          Genre = pp.Genre,
                          BirthDate = pp.BirthDate,
                          //AssignedDate = poc.AssignedDate,
                          OrderInClass = poc.OrderInClass,
                          //ShowMark = j3 != null ? true : false
                      };
                List<PupilRankingBO> lstPupilRankingBO = data.ToList();
                Dictionary<string, object> dicCS = new Dictionary<string, object>();
                dicCS["ClassID"] = ClassID;
                dicCS["AcademicYearID"] = AcademicYearID;
                IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicCS);
                iqClassSubject = iqClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0);
                //Chỉ lấy những môn bắt buộc, không xét môn tự chọn
                iqClassSubject = iqClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY);
                List<ClassSubject> lstClassSubject = iqClassSubject.ToList();
                int countSubject = lstClassSubject.Count(); //Tổng số môn tính điểm
                List<int> lstSubjectID = lstClassSubject.Select(o => o.SubjectID).ToList();
                List<int> lstSubjectMark = lstClassSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).Select(o => o.SubjectID).ToList();
                List<int> lstSubjectJudge = lstClassSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).Select(o => o.SubjectID).ToList();

                Dictionary<string, object> dicES = new Dictionary<string, object>();
                dicES["ClassID"] = ClassID;
                dicES["AcademicYearID"] = AcademicYearID;
                IQueryable<ExemptedSubject> iqExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicES).Where(o => o.FirstSemesterExemptType.HasValue);
                List<ExemptedSubject> lstExemptedSubject = iqExemptedSubject.ToList();
                Dictionary<string, object> dicSum = new Dictionary<string, object>();
                dicSum["ClassID"] = ClassID;
                dicSum["AcademicYearID"] = AcademicYearID;
                IQueryable<VSummedUpRecord> iqSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dicSum).Where(o => lstSubjectID.Contains(o.SubjectID));
                iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
                List<VSummedUpRecord> lstSummedUpRecord = new List<VSummedUpRecord>();
                lstSummedUpRecord = iqSummedUpRecord.ToList();

                Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                dicAbsence["AcademicYearID"] = AcademicYearID;
                dicAbsence["ClassID"] = ClassID;
                dicAbsence["SchoolID"] = SchoolID;
                dicAbsence["FromAbsentDate"] = aca.FirstSemesterStartDate;
                dicAbsence["ToAbsentDate"] = aca.FirstSemesterEndDate;
                List<PupilAbsence> lstAbsenceAll = PupilAbsenceBusiness.GetPupilAbsenceByDate(PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsence).ToList());
                foreach (PupilRankingBO bo in lstPupilRankingBO)
                {
                    var poc = lsPupilOfClassSemester.FirstOrDefault(o => o.PupilID == bo.PupilID);
                    if (poc != null || (bo.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || bo.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                    {
                        bo.ShowMark = true;
                    }
                    List<PupilAbsence> lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == bo.PupilID).ToList();
                    bo.ToTalAbsentDay = lstAbsencePupil.Count;
                    bo.TotalAbsentDaysWithoutPermission = lstAbsencePupil.Where(o => o.IsAccepted != true).Count();
                    bo.TotalAbsentDaysWithPermission = lstAbsencePupil.Where(o => o.IsAccepted == true).Count();
                    //AnhVD9 20140925 - Môn miễn giảm phải bắt buộc
                    int countSubjectExempted = lstExemptedSubject.Where(o => o.PupilID == bo.PupilID && lstSubjectID.Contains(o.SubjectID)).Count();
                    List<VSummedUpRecord> lstSummedUpHasMarkPupil = lstSummedUpRecord.Where(o => o.PupilID == bo.PupilID && (o.SummedUpMark.HasValue || (o.JudgementResult != null && o.JudgementResult != ""))).ToList();
                    int countSubjectHasMark = lstSummedUpHasMarkPupil.Count;//Nếu tất cả các môn học đủ điểm
                    if ((countSubjectExempted + countSubjectHasMark == countSubject) && countSubject > 0 && bo.ConductLevelID.HasValue)
                    {
                        List<VSummedUpRecord> lstSumMark = lstSummedUpHasMarkPupil.Where(o => lstSubjectMark.Contains(o.SubjectID)).ToList();
                        List<VSummedUpRecord> lstSumJudge = lstSummedUpHasMarkPupil.Where(o => lstSubjectJudge.Contains(o.SubjectID)).ToList();
                        bo.CapacityLevelID = CategoryOfPupilPrimaryV2(lstSumMark, lstSumJudge, bo.ConductLevelID.Value);
                    }
                    else
                    {
                        bo.CapacityLevelID = new Nullable<int>();
                    }
                }
                return lstPupilRankingBO;
            }
            else
            {
                var data = //from poc in PupilOfClassRepository.All
                           from poc in lsPupilOfClassAll
                               //join poc2 in lsPupilOfClassSemester on poc.PupilID equals poc2.PupilID into g3
                           join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                           join pr in VPupilRankingBusiness.All.Where(u => u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == aca.Year) on new { SchoolID, AcademicYearID, poc.PupilID, ClassID }
                          equals new { pr.SchoolID, pr.AcademicYearID, pr.PupilID, pr.ClassID } into g1
                           join pe in PupilEmulationBusiness.All.Where(p => p.Last2digitNumberSchool == modSchoolID && p.AcademicYearID == aca.AcademicYearID) on new { SchoolID, AcademicYearID, poc.PupilID, poc.ClassID }
                          equals new { pe.SchoolID, pe.AcademicYearID, pe.PupilID, pe.ClassID } into g2
                           from j1 in g1.DefaultIfEmpty()
                           from j2 in g2.DefaultIfEmpty()
                               //from j3 in g3.DefaultIfEmpty()
                           where poc.AcademicYearID == AcademicYearID && poc.SchoolID == SchoolID && poc.ClassID == ClassID
                           select new PupilRankingBO
                           {
                               PupilRankingID = j1 != null ? j1.PupilRankingID : new Nullable<long>(),
                               PupilID = poc.PupilID,
                               Semester = j1 != null ? j1.Semester : new Nullable<int>(),
                               ClassID = poc.ClassID,
                               EducationLevelID = EducationLevelID,
                               SchoolID = poc.SchoolID,
                               AcademicYearID = AcademicYearID,
                               ProfileStatus = poc.Status,
                               Year = year,
                               PupilEmulationID = j2 != null ? j2.PupilEmulationID : new Nullable<int>(),
                               HonourAchivementTypeID = j2 != null ? j2.HonourAchivementTypeID : new Nullable<int>(),
                               PupilCode = pp.PupilCode,
                               Name = pp.Name,
                               FullName = pp.FullName,
                               CapacityLevelID = j1 != null ? j1.CapacityLevelID : new Nullable<int>(),
                               ConductLevelID = j1 != null ? j1.ConductLevelID : new Nullable<int>(),
                               Genre = pp.Genre,
                               StudyingJudgementID = j1 != null ? j1.StudyingJudgementID : new Nullable<int>(),
                               BirthDate = pp.BirthDate,
                               AssignedDate = poc.AssignedDate,
                               OrderInClass = poc.OrderInClass,
                               //ShowMark = j3 != null ? true : false
                           };
                //var semsterOverSecond = data.Where(u => u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                var maxSemester2 = data.ToList();
                var maxSemester = maxSemester2.Where(u => u.Semester == maxSemester2.Where(v => v.PupilID == u.PupilID).Max(v => v.Semester)).ToList();

                //Danh sách các môn học trong học kỳ:
                Dictionary<string, object> dicCS = new Dictionary<string, object>();
                dicCS["ClassID"] = ClassID;
                dicCS["AcademicYearID"] = AcademicYearID;
                IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicCS);
                iqClassSubject = iqClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
                //Chỉ lấy những môn bắt buộc, không xét môn tự chọn
                iqClassSubject = iqClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY);

                List<ClassSubject> lstClassSubject = iqClassSubject.ToList();
                int countSubject = lstClassSubject.Count(); //Tổng số môn tính điểm
                List<int> lstSubjectID = lstClassSubject.Select(o => o.SubjectID).ToList();
                List<int> lstSubjectMark = lstClassSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).Select(o => o.SubjectID).ToList();
                List<int> lstSubjectJudge = lstClassSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).Select(o => o.SubjectID).ToList();

                Dictionary<string, object> dicES = new Dictionary<string, object>();
                dicES["ClassID"] = ClassID;
                dicES["AcademicYearID"] = AcademicYearID;
                IQueryable<ExemptedSubject> iqExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicES).Where(o => o.SecondSemesterExemptType.HasValue);

                List<ExemptedSubject> lstExemptedSubject = iqExemptedSubject.ToList();

                Dictionary<string, object> dicSum = new Dictionary<string, object>();
                dicSum["ClassID"] = ClassID;
                dicSum["AcademicYearID"] = AcademicYearID;
                IQueryable<VSummedUpRecord> iqSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dicSum).Where(o => lstSubjectID.Contains(o.SubjectID));

                //IQueryable<SummedUpRecord>  iqSummedUpRecord1 = iqSummedUpRecord.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);

                List<VSummedUpRecord> iqSummedUpRecordMax = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
                List<long> iquery = new List<long>();
                List<long> iquery1 = new List<long>();
                List<long> iquery2 = new List<long>();
                List<VSummedUpRecord> lstSummedUpRecord = new List<VSummedUpRecord>();
                foreach (ClassSubject cs in lstClassSubject)
                {
                    //IQueryable<SummedUpRecord>  iqSummedUpRecordMaxTemp = iqSummedUpRecordMax.Where(o => o.SubjectID == cs.SubjectID);
                    if (cs.SectionPerWeekSecondSemester == 0)
                    {
                        iquery = iqSummedUpRecordMax.Where(o => o.SubjectID == cs.SubjectID && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Select(u => u.SummedUpRecordID).ToList();
                        iquery2.AddRange(iquery);
                    }
                    else
                    {
                        iquery1 = iqSummedUpRecordMax.Where(o => o.SubjectID == cs.SubjectID && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => u.SummedUpRecordID).ToList();
                        iquery2.AddRange(iquery1);
                    }
                }
                if (iquery2 != null)
                {
                    lstSummedUpRecord = iqSummedUpRecordMax.Where(x => iquery2.Contains(x.SummedUpRecordID)).ToList();
                }

                Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                dicAbsence["AcademicYearID"] = AcademicYearID;
                dicAbsence["ClassID"] = ClassID;
                dicAbsence["SchoolID"] = SchoolID;
                List<PupilAbsence> lstAbsenceAll = PupilAbsenceBusiness.GetPupilAbsenceByDate(PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsence)
                                                                        .Where(o => (o.AbsentDate >= aca.FirstSemesterStartDate && o.AbsentDate <= aca.FirstSemesterEndDate)
                                                                        || (o.AbsentDate >= aca.SecondSemesterStartDate && o.AbsentDate <= aca.SecondSemesterEndDate)).ToList());

                foreach (PupilRankingBO bo in maxSemester)
                {
                    var poc = lsPupilOfClassSemester.FirstOrDefault(o => o.PupilID == bo.PupilID);
                    if (poc != null || (bo.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || bo.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                    {
                        bo.ShowMark = true;
                    }
                    List<PupilAbsence> lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == bo.PupilID).ToList();
                    bo.ToTalAbsentDay = lstAbsencePupil.Count;
                    bo.TotalAbsentDaysWithoutPermission = lstAbsencePupil.Where(o => o.IsAccepted != true).Count();
                    bo.TotalAbsentDaysWithPermission = lstAbsencePupil.Where(o => o.IsAccepted == true).Count();
                    // AnhVD9 20140925 - Môn miễn giảm phải bắt buộc
                    int countSubjectExempted = lstExemptedSubject.Where(o => o.PupilID == bo.PupilID && lstSubjectID.Contains(o.SubjectID)).Count();
                    List<VSummedUpRecord> lstSummedUpHasMarkPupil = lstSummedUpRecord.Where(o => o.PupilID == bo.PupilID && (o.SummedUpMark.HasValue || (o.JudgementResult != null && o.JudgementResult != ""))).ToList();

                    int countSubjectHasMark = lstSummedUpHasMarkPupil.Count;
                    //Nếu tất cả các môn học đủ điểm
                    if ((countSubjectExempted + countSubjectHasMark == countSubject) && countSubject > 0 && bo.ConductLevelID.HasValue)
                    {
                        List<VSummedUpRecord> lstSumMark = lstSummedUpHasMarkPupil.Where(o => lstSubjectMark.Contains(o.SubjectID)).ToList();
                        List<VSummedUpRecord> lstSumJudge = lstSummedUpHasMarkPupil.Where(o => lstSubjectJudge.Contains(o.SubjectID)).ToList();
                        bo.CapacityLevelID = CategoryOfPupilPrimaryV2(lstSumMark, lstSumJudge, bo.ConductLevelID.Value);
                    }
                    else
                    {
                        bo.CapacityLevelID = new Nullable<int>();
                    }

                }
                return maxSemester;
            }
        }


        /// <summary>
        /// namdv3
        /// </summary>
        /// <param name="PupilID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public Dictionary<int, int?> CategoryOfPupilAfterRetest(int ClassID, List<PupilRankingBO> listPupilRankingBO)
        {
            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
            //Danh sách các môn học trong học kỳ:
            Dictionary<string, object> dicCS = new Dictionary<string, object>();
            dicCS["ClassID"] = ClassID;
            dicCS["AcademicYearID"] = cp.AcademicYearID;
            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(cp.SchoolID, dicCS);
            iqClassSubject = iqClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
            //Chỉ lấy những môn bắt buộc, không xét môn tự chọn
            iqClassSubject = iqClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY);
            int countSubject = iqClassSubject.Count(); //Tổng số môn tính điểm
            List<ClassSubject> lstClassSubject = iqClassSubject.ToList();
            List<int> lstSubjectID = lstClassSubject.Select(o => o.SubjectID).ToList();
            List<int> lstSubjectMark = lstClassSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE).Select(o => o.SubjectID).ToList();
            List<int> lstSubjectJudge = lstClassSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).Select(o => o.SubjectID).ToList();

            Dictionary<string, object> dicES = new Dictionary<string, object>();
            dicES["ClassID"] = ClassID;
            dicES["AcademicYearID"] = cp.AcademicYearID;
            IQueryable<ExemptedSubject> iqExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(cp.SchoolID, dicES);

            iqExemptedSubject = iqExemptedSubject.Where(o => o.FirstSemesterExemptType.HasValue && o.SecondSemesterExemptType.HasValue);

            List<ExemptedSubject> lstExemptedSubject = iqExemptedSubject.ToList();

            Dictionary<string, object> dicSum = new Dictionary<string, object>();
            dicSum["ClassID"] = ClassID;
            dicSum["AcademicYearID"] = cp.AcademicYearID;
            IQueryable<VSummedUpRecord> iqSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(cp.SchoolID, dicSum).Where(o => lstSubjectID.Contains(o.SubjectID));

            iqSummedUpRecord = iqSummedUpRecord.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);

            var iqSummedUpRecordMax = iqSummedUpRecord.Where(u => u.Semester == iqSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.Semester));
            List<VSummedUpRecord> lstSummedUpRecord = new List<VSummedUpRecord>();
            lstSummedUpRecord = iqSummedUpRecordMax.ToList();

            var lstPrr = this.PupilRetestRegistrationRepository.All
                                       .Where(u => u.SchoolID == cp.SchoolID
                                                   && u.AcademicYearID == cp.AcademicYearID
                                                   && u.ClassID == ClassID);
            var lstPrrMax = lstPrr.Where(u => u.RegisteredDate == lstPrr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.RegisteredDate)).ToList();



            Dictionary<int, int?> dicCapacity = new Dictionary<int, int?>();
            listPupilRankingBO = listPupilRankingBO.Where(o => o.ClassID == ClassID).ToList();
            foreach (var prBO in listPupilRankingBO)
            {
                var countSubjectNotRetest = lstPrrMax.Where(o => o.PupilID == prBO.PupilID && o.ModifiedDate == null).Count(); //Số môn đã đăng ký thi lại mà chưa nhập điểm
                // AnhVD9 20140925 - Môn miễn giảm phải bắt buộc
                int countSubjectExempted = lstExemptedSubject.Where(o => o.PupilID == prBO.PupilID && lstSubjectID.Contains(o.SubjectID)).Count();
                List<VSummedUpRecord> lstSummedUpHasMarkPupil = lstSummedUpRecord.Where(o => o.PupilID == prBO.PupilID && (o.SummedUpMark.HasValue || (o.JudgementResult != null && o.JudgementResult != ""))).ToList();
                int countSubjectHasMark = lstSummedUpHasMarkPupil.Count;
                //Nếu tất cả các môn học đủ điểm
                if ((countSubjectExempted + countSubjectHasMark == countSubject) && countSubject > 0 && prBO.ConductLevelID.HasValue && (countSubjectNotRetest == 0))
                {
                    List<VSummedUpRecord> lstSumMark = lstSummedUpHasMarkPupil.Where(o => lstSubjectMark.Contains(o.SubjectID)).ToList();
                    List<VSummedUpRecord> lstSumJudge = lstSummedUpHasMarkPupil.Where(o => lstSubjectJudge.Contains(o.SubjectID)).ToList();
                    dicCapacity[prBO.PupilID.Value] = CategoryOfPupilPrimaryV2(lstSumMark, lstSumJudge, prBO.ConductLevelID.Value);
                }
            }
            return dicCapacity;
        }

        /*
        /// <summary>
        /// Lấy danh sách học sinh để thực hiện xếp loại hạnh kiểm (Chua done)
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        public List<PupilRankingBO> GetListPupilToRankConduct(int AcademicYearID, int SchoolID, int ClassID, int Semester)
        {
            AcademicYear AcademicYear = AcademicYearRepository.All.FirstOrDefault(o => o.AcademicYearID == AcademicYearID);
            int year = AcademicYear.Year;
            int modSchoolID = SchoolID % 100;
            DateTime? SemesterStartYear = Semester == 1 ? AcademicYear.FirstSemesterStartDate : AcademicYear.SecondSemesterStartDate;
            DateTime? SemesterEndDate = Semester == 1 ? AcademicYear.FirstSemesterEndDate : AcademicYear.SecondSemesterEndDate;
            IDictionary<string, object> dic = null;
            //Step 1
            //      Thực hiện tìm kiếm trong bảng
            //      PupilOfClass poc leftjoin PupilRanking pr on poc.PupilID = pr.PupilID
            //      where poc.AcademicYearID  = AcademicYearID and poc.SchoolID = SchoolID and poc.ClassID = ClassID and pr.Semester = Semester
            //      => lstPupilConduct
            var lstPupilConduct = from poc in PupilOfClassBusiness.All.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID
                                      && o.ClassID == ClassID)
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  join pr in VPupilRankingBusiness.All.Where(o => o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year) on poc.PupilID equals pr.PupilID into g1
                                  from j2 in g1.DefaultIfEmpty()
                                  where poc.AcademicYearID == AcademicYearID && poc.SchoolID == SchoolID
                                      && poc.ClassID == ClassID && j2.Semester == Semester
                                  select new PupilRankingBO
                                  {
                                      PupilRankingID = j2.PupilRankingID,
                                      PupilID = poc.PupilID,
                                      Semester = j2.Semester.Value,
                                      ClassID = poc.ClassID,
                                      EducationLevelID = j2.EducationLevelID,
                                      SchoolID = poc.SchoolID,
                                      AcademicYearID = j2.AcademicYearID,
                                      ProfileStatus = poc.Status,
                                      PupilCode = pp.PupilCode,
                                      FullName = pp.FullName,
                                      Genre = poc.PupilProfile.Genre,
                                      BirthDate = pp.BirthDate,
                                      AssignedDate = poc.AssignedDate
                                  };
            IEnumerable<PupilRankingBO> lstPupilPrimary = from t in lstPupilConduct.AsEnumerable()
                                                          where t.AssignedDate == (from poc in PupilOfClassBusiness.All
                                                                                   where poc.PupilID == t.PupilID
                                                                                   select poc.AssignedDate).Max()
                                                          select t;
            List<PupilRankingBO> result = new List<PupilRankingBO>();
            //Với mỗi phần tử trong lstPupilConduct  lấy ra PupilID, ClassID, Semester.
            foreach (PupilRankingBO bo in lstPupilPrimary)
            {
                if (bo.PupilID != 0 && bo.ClassID != 0 && bo.Semester == null)
                {
                    //Thực hiện tìm kiếm trong bảng SummedUpRecord với các tham số truyền vào: PupilID, ClassID, Semester.
                    //Lấy được giá trị Comment. Thêm comment vào lstPupilConduct thu được.
                    bo.Comment = GetComment(bo.PupilID.Value, bo.ClassID.Value, bo.Semester.Value);
                    //Thực hiện gọi hàm PupilAbsenceBusiness.CountTotalAbsenceWithPermission(PupilID, AcademicYearID, Semester) = totalAbsenceWithPermission
                    //Thực hiện gọi hàm PupilAbsenceBusiness.CountTotalAbsenceWithPermission(PupilID, AcademicYearID, Semester) = totalAbsenceWithoutPermission
                    //Thêm TotalAbsenceWithPermission và TotalAbsenceWithoutPermission vào lstPupilConduct.
                    bo.TotalAbsentDaysWithPermission = int.Parse(PupilAbsenceBusiness.CountTotalAbsenceWithPermission(bo.PupilID.Value, AcademicYearID, Semester).ToString());
                    bo.TotalAbsentDaysWithoutPermission = int.Parse(PupilAbsenceBusiness.CountTotalAbsenceWithoutPermission(bo.PupilID.Value, AcademicYearID, Semester).ToString());

                    //Thực hiện tính ra điểm cộng hạnh kiểm bằng cách gọi hàm PupilPraiseBusiness.SearchPupilPraise(Dictionary) +
                    //    + với các tham số truyền vào: PupilID, ClassID, Semester 
                    //    (Kiểm tra SemesterStartYear =< PraiseDate <= SemesterEndDate (Dựa vào Semester, AcademicYearID truyền vào))
                    //    => LstPupilPraise 
                    dic["PupilID"] = bo.PupilID;
                    dic["ClassID"] = ClassID;
                    dic["Semester"] = Semester;
                    var LstPupilPraise = (IEnumerable<PupilPraiseBO>)PupilPraiseBusiness.SearchBySchool(SchoolID, dic)
                        .Where(o => o.PraiseDate >= SemesterStartYear && o.PraiseDate <= SemesterEndDate).ToList();
                    //    For từng phần tử trong lstPupilPraise => Sum(LstPupilPraise[i]. ConductMark)
                    //    => sumConductMark
                    decimal? sumConductMark = 0;
                    foreach (var item in LstPupilPraise)
                    {
                        sumConductMark += item.ConductMark;
                    }
                    //Thực hiện tính điểm trừ bằng cách gọi hàm PupilDisciplineBusiness.SearchPupilDiscipline(Dictionary) + 
                    //+ với các tham số truyền vào PupilID, ClassID, Semester
                    //(Kiểm tra SemesterStartYear =< DisciplinedDate <= SemesterEndDate (Dựa vào Semester, AcademicYearID truyền vào))
                    // 	 LstDisciplinedDate
                    var LstDisciplinedDate = (IEnumerable<PupilDisciplineBO>)PupilDisciplineBusiness.SearchBySchool(SchoolID, dic)
                       .Where(o => o.DisciplinedDate >= SemesterStartYear && o.DisciplinedDate <= SemesterEndDate).ToList();
                    //For từng phần tử trong lstDisciplinedDate => Sum(ListPupilDiscipline[i].ConductMinusMark)
                    //=> sumConductMinusMark
                    decimal? sumConductMinusMark = 0;
                    foreach (var item in LstDisciplinedDate)
                    {
                        sumConductMinusMark += item.ConductMinusMark;
                    }
                    //Thực hiện tính điểm trừ vi phạm bằng cách gọi hàm PupilFaultBusiness.SearchPupilFault(Dictionary) +
                    //+ với các tham số truyền vào PupilID, ClassID, Semester
                    //(Kiểm tra SemesterStartYear =< DisciplinedDate <= SemesterEndDate (Dựa vào Semester, AcademicYearID truyền vào))
                    //	LstPupilFault
                    var LstPupilFault = PupilFaultBusiness.SearchBySchool(SchoolID, dic).ToList();
                    //.Where(o => o.DisciplinedDate >= SemesterStartYear && o.DisciplinedDate <= SemesterEndDate).ToList();

                    //For từng phần tử lstPupilFault => Sum(LstPupilFault[i].TotalPenalizedMark)
                    //	sumTotalPenalizedMark.
                    decimal? sumTotalPenalizedMark = LstPupilFault.Sum(o => o.TotalPenalizedMark);

                    //Thực hiện tính tổng điểm:
                    //Lấy ra MaxValue trong bảng ConductConfigByViolation theo SchoolID.
                    decimal? MaxValue = ConductConfigByViolationRepository.All.Where(o => o.SchoolID == SchoolID).Max(o => o.MaxValue);
                    //Sau đó sumMark = MaxValue + sumConductMark – SumConductMinusMark - sumTotalPenalizedMark
                    decimal? sumMark = MaxValue + sumConductMark - sumConductMinusMark - sumTotalPenalizedMark;
                    //	Thực hiện thêm sumConductMark, sumConductMinusMark, sumTotalPenalizedMark, sumMark vào lstPupilConduct.
                    bo.sumConductMark = sumConductMark;
                    bo.sumConductMinusMark = sumConductMinusMark;
                    bo.sumTotalPenalizedMark = sumTotalPenalizedMark;
                    bo.sumMark = sumMark;
                    result.Add(bo);
                }
            }
            return result;
        }
        */

        /// <summary>
        ///Tính học lực của học sinh cấp 1
        ///lastest update date: 07-01-2013
        ///updated by namdv3
        /// </summary>
        /// <param name="PupilID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public Dictionary<int, int?> CategoryOfPupil(int ClassID, List<int> listPupilID)
        {
            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
            int modSchoolID = cp.SchoolID % 100;
            int year = AcademicYearBusiness.Find(cp.AcademicYearID).Year;
            List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();
            var lstSubject = ClassSubjectRepository.All.Where(o => o.ClassID == ClassID && o.Last2digitNumberSchool == modSchoolID).ToList();
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.All.Where(o => o.ClassID == ClassID).ToList();
            List<VPupilRanking> listPupilRanking = VPupilRankingBusiness.All.Where(o => o.ClassID == ClassID
                    && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.CreatedAcademicYear == year && o.Last2digitNumberSchool == modSchoolID).ToList();
            List<SummedUpRecord> listSumedUp = SummedUpRecordBusiness.All
                            .Where(o => o.ClassID == ClassID
                                && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                ).ToList();
            Dictionary<int, int?> dicCapacity = new Dictionary<int, int?>();
            foreach (int PupilID in listPupilID)
            {
                int c = 0;
                if (lstSubject != null && lstSubject.Count() > 0)
                {
                    foreach (var subject in lstSubject)
                    {
                        int ci = listExemptedSubject.Where(o => o.PupilID == PupilID
                            && o.SubjectID == subject.SubjectID
                            && (subject.FirstSemesterCoefficient == 0 || o.FirstSemesterExemptType == GlobalConstants.EXEMPT_TYPE_ALL)
                            && (subject.SecondSemesterCoefficient == 0 || o.SecondSemesterExemptType == GlobalConstants.EXEMPT_TYPE_ALL)).Count();
                        if (ci > 0)
                        {
                            c += ci;
                        }

                        SummedUpRecord sum = listSumedUp
                            .Where(o => o.PupilID == PupilID
                                && o.SubjectID == subject.SubjectID
                                ).OrderBy(o => o.Semester).FirstOrDefault();
                        if (sum != null)
                        {
                            lstSummedUpRecord.Add(sum);
                        }
                    }
                }
                /*
                 Lấy những phần tử trong lstSummedUpRecord có IsAppliedTypeSubject(lstSubject [i].SubjectID, ClassID) = true => lstSummedUpRecord _Voluntary
                lstSummedUpRecord_Mandatory = lstSummedUpRecord – lstSummedUpRecord_Voluntary
                 */
                List<SummedUpRecord> lstSummedUpRecord_Mandatory = new List<SummedUpRecord>();
                if (lstSummedUpRecord != null && lstSummedUpRecord.Count() > 0)
                {
                    foreach (var sum in lstSummedUpRecord)
                    {
                        if (!IsAppliedTypeSubject(lstSubject, ClassID, sum.SubjectID))
                        {
                            lstSummedUpRecord_Mandatory.Add(sum);
                        }
                    }
                }
                //Nếu LstSubject.Count != c +  lstSummedUpRecord.Count: Chưa nhập đủ điểm => return 0
                if (lstSubject.Count() != c + lstSummedUpRecord.Count())
                {
                    return null;
                }

                VPupilRanking pr = listPupilRanking.Where(o => o.PupilID == PupilID).OrderByDescending(o => o.Semester).FirstOrDefault();
                int? ConductLevelID = pr != null ? pr.ConductLevelID : null;
                int? CapacityLevelID = null;

                var lstSummedUpRecord_Mandatory_Type_Mark = lstSummedUpRecord_Mandatory.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK);
                var lstSummedUpRecord_Mandatory_Type_Judge = lstSummedUpRecord_Mandatory.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE);
                if (lstSummedUpRecord_Mandatory_Type_Mark.All(o => o.SummedUpMark >= GlobalConstants.SUMMEDUPMARK_TYPE_EXCELLENT) && (lstSummedUpRecord_Mandatory_Type_Judge.All(o => o.JudgementResult != GlobalConstants.JUDGEMENTRESULT_B)))
                {
                    if (ConductLevelID.HasValue && ConductLevelID.Value == GlobalConstants.CONDUCT_TYPE_D)
                        CapacityLevelID = GlobalConstants.CAPACITY_TYPE_EXCELLENT;
                    else CapacityLevelID = GlobalConstants.CAPACITY_TYPE_WEAK;
                }

                else if (lstSummedUpRecord_Mandatory_Type_Mark.All(o => o.SummedUpMark >= GlobalConstants.SUMMEDUPMARK_TYPE_GOOD) && lstSummedUpRecord_Mandatory_Type_Judge.All(o => o.JudgementResult != GlobalConstants.JUDGEMENTRESULT_B))
                {
                    if (ConductLevelID.HasValue && ConductLevelID.Value == GlobalConstants.CONDUCT_TYPE_D)
                        CapacityLevelID = GlobalConstants.CAPACITY_TYPE_GOOD;
                    else CapacityLevelID = GlobalConstants.CAPACITY_TYPE_WEAK;
                }

                else if (lstSummedUpRecord_Mandatory_Type_Mark.All(o => o.SummedUpMark >= GlobalConstants.SUMMEDUPMARK_TYPE_NORMAL) && lstSummedUpRecord_Mandatory_Type_Judge.All(o => o.JudgementResult != GlobalConstants.JUDGEMENTRESULT_B))
                {
                    if (ConductLevelID.HasValue && ConductLevelID.Value == GlobalConstants.CONDUCT_TYPE_D)
                        CapacityLevelID = GlobalConstants.CAPACITY_TYPE_NORMAL;
                    else CapacityLevelID = GlobalConstants.CAPACITY_TYPE_WEAK;
                }
                else CapacityLevelID = GlobalConstants.CAPACITY_TYPE_WEAK;
                dicCapacity[PupilID] = CapacityLevelID;
            }
            return dicCapacity;
        }

        //List<SummedUpRecord>: Điểm tổng kết các môn của học sinh (bỏ môn miễn giảm, môn tự chọn). Chia làm list môn tính điểm và nhận xét
        //Kiểm tra học sinh đã nhập đủ điểm trước khi gọi hàm này.
        //ConductLevelID: hạnh kiểm của học sinh
        //Dùng cho cả HK1, HK2
        public int CategoryOfPupilPrimaryV2(List<VSummedUpRecord> lstSummedUpRecord_Mark, List<VSummedUpRecord> lstSummedUpRecord_Judge, int ConductLevelID)
        {
            //Nếu hạnh kiểm không đạt thì xét loại Yếu
            if (ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_CD)
            {
                return SystemParamsInFile.CAPACITY_TYPE_WEAK;
            }
            else
            {
                //Lấy điểm thi lại gán vào điểm tổng kết để xét cho tiện
                foreach (VSummedUpRecord su in lstSummedUpRecord_Mark)
                {
                    if (su.ReTestMark.HasValue)
                    {
                        su.SummedUpMark = su.ReTestMark;
                    }
                }
                foreach (VSummedUpRecord su in lstSummedUpRecord_Judge)
                {
                    if (su.ReTestJudgement != null && su.ReTestJudgement != " ")
                    {
                        su.JudgementResult = su.ReTestJudgement;
                    }
                }
                //XLGD Giỏi
                if (lstSummedUpRecord_Mark.All(o => o.SummedUpMark >= GlobalConstants.SUMMEDUPMARK_TYPE_EXCELLENT) && lstSummedUpRecord_Judge.All(o => o.JudgementResult != GlobalConstants.JUDGEMENTRESULT_B))
                {
                    return SystemParamsInFile.CAPACITY_TYPE_EXCELLENT;
                }
                else if (lstSummedUpRecord_Mark.All(o => o.SummedUpMark >= GlobalConstants.SUMMEDUPMARK_TYPE_GOOD) && lstSummedUpRecord_Judge.All(o => o.JudgementResult != GlobalConstants.JUDGEMENTRESULT_B))
                {
                    return SystemParamsInFile.CAPACITY_TYPE_GOOD;
                }
                else if (lstSummedUpRecord_Mark.All(o => o.SummedUpMark >= GlobalConstants.SUMMEDUPMARK_TYPE_NORMAL) && lstSummedUpRecord_Judge.All(o => o.JudgementResult != GlobalConstants.JUDGEMENTRESULT_B))
                {
                    return SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                }
                else
                {
                    return SystemParamsInFile.CAPACITY_TYPE_WEAK;
                }
            }
        }





        /// <summary>
        /// Lấy danh sách học sinh thi lại.Reviewed
        /// </summary>
        /// <param name="dic">
        /// AcademicYearID (Năm học)
        /// ClassID (Lớp học) 
        /// EducationLevelID (Khối học)
        /// Semester (Học kỳ)
        /// StudyingJudgementID (Thuộc diện thi lại)
        /// </param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilRetest(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int Semester = Utils.GetInt(dic, "Semester");
            int StudyingJudgementID = Utils.GetInt(dic, "StudyingJudgementID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> LstClassID = Utils.GetIntList(dic, "ListClassID");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN", false);
            int modSchoolID = SchoolID % 100;
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            int AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;//Set mặc đinh cấp 2, không ảnh hưởng vì ở dưới sẽ set lại
            if (edu != null)
                AppliedLevel = edu.Grade;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Check"] = "Check";
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["ListClassID"] = LstClassID;
            IQueryable<PupilOfClass> lsPoc = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchInfo).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);
            IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(SchoolID, dic);
            IQueryable<ClassProfile> iqClassProfile = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID, dic);
            IQueryable<PupilRetestRegistration> iqPupilRetest = PupilRetestRegistrationBusiness.SearchBySchool(SchoolID, dic);

            var lstPupilRetestRegistration = from pr in iqPupilRanking
                                             join poc in lsPoc on pr.PupilID equals poc.PupilID
                                             join prr in iqPupilRetest on pr.PupilID equals prr.PupilID into g1
                                             from j2 in g1.DefaultIfEmpty()
                                             join pp in PupilProfileBusiness.All on pr.PupilID equals pp.PupilProfileID
                                             join cl in iqClassProfile on pr.ClassID equals cl.ClassProfileID
                                             select new PupilRankingBO
                                             {
                                                 FullName = pp.FullName,
                                                 PupilRankingID = pr.PupilRankingID,
                                                 CapacityLevelID = pr.CapacityLevelID,
                                                 Name = pp.Name,
                                                 PupilID = pr.PupilID,
                                                 Semester = Semester,
                                                 ClassID = pr.ClassID,
                                                 EducationLevelID = pr.EducationLevelID,
                                                 SchoolID = pr.SchoolID,
                                                 AcademicYearID = pr.AcademicYearID,
                                                 OrderInClass = poc.OrderInClass,
                                                 ClassName = cl.DisplayName,
                                                 ClassOrderNumber = cl.OrderNumber,
                                                 IsVNEN = cl.IsVnenClass
                                             };
            if (isVNEN && AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                lstPupilRetestRegistration = lstPupilRetestRegistration.Where(p => (!p.IsVNEN.HasValue || p.IsVNEN.HasValue && p.IsVNEN.Value == false));
            }

            List<PupilRankingBO> list = lstPupilRetestRegistration.Distinct().ToList();
            list = list.OrderBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            return list;

        }

        public List<PupilRankingBO> GetPupilConductRetest(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int Semester = (int)Utils.GetByte(dic, "Semester");
            int modSchoolID = SchoolID % 100;
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);

            dic["Check"] = "Check";

            var temp = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                       join pr in VPupilRankingBusiness.All.Where(o => o.SchoolID == SchoolID
                                                                        && o.AcademicYearID == AcademicYearID
                                                                        && o.ClassID == ClassID
                                                                        && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                                        && o.Last2digitNumberSchool == modSchoolID
                                                                        && o.CreatedAcademicYear == ac.Year)
                                on poc.PupilID equals pr.PupilID
                       join pr1 in VPupilRankingBusiness.All.Where(o => o.SchoolID == SchoolID
                           && o.AcademicYearID == AcademicYearID && o.ClassID == ClassID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING
                            && o.Last2digitNumberSchool == modSchoolID
                             && o.CreatedAcademicYear == ac.Year)
                       on poc.PupilID equals pr1.PupilID into g3
                       from g4 in g3.DefaultIfEmpty()
                       join co in ConductLevelBusiness.All on g4.ConductLevelID equals co.ConductLevelID into g5
                       from g6 in g5.DefaultIfEmpty()
                       join ced in ConductEvaluationDetailRepository.All
                                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING
                                                        && o.SchoolID == SchoolID
                                                        && o.AcademicYearID == AcademicYearID)
                                on poc.PupilID equals ced.PupilID into g1
                       from g2 in g1.DefaultIfEmpty()
                       where pr.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            && pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING
                            && poc.ClassID == ClassID
                            && poc.SchoolID == SchoolID
                            && poc.AcademicYearID == AcademicYearID
                       select new PupilRankingBO
                       {
                           PupilRankingID = g4.PupilRankingID,
                           PupilID = poc.PupilID,
                           PupilCode = pp.PupilCode,
                           Name = pp.Name,
                           OrderInClass = poc.OrderInClass,
                           FullName = pp.FullName,
                           BirthDate = pp.BirthDate,
                           ClassID = poc.ClassID,
                           SchoolID = poc.SchoolID,
                           AcademicYearID = poc.AcademicYearID,
                           Task1Evaluation = g2.Task1Evaluation,
                           Task2Evaluation = g2.Task2Evaluation,
                           Task3Evaluation = g2.Task3Evaluation,
                           Task4Evaluation = g2.Task4Evaluation,
                           Task5Evaluation = g2.Task5Evaluation,
                           ConductLevelID = g4.ConductLevelID,
                           ConductLevelName = g6.ConductLevelID != null ? g6.Resolution : null,
                           ProfileStatus = poc.Status,
                           AssignedDate = poc.AssignedDate,
                           EndDate = poc.EndDate
                       };
            return temp.ToList();
        }
        /// <summary>
        /// Lấy ra danh sách học sinh ứng với hạnh kiểm và chi tiết hạnh kiểm
        /// </summary>
        /// <param name="dic">
        /// AcademicYearID (int)
        /// SchoolID (int)
        /// ClassID (int)
        /// EducationLevelID (int)
        /// Semester (int)
        /// ConductLevelID (int) : Default = 0 => Tìm kiếm All
        /// </param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilConduct(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int Semester = (int)Utils.GetByte(dic, "Semester");
            //Thực hiện tìm kiếm trong bảng
            //PupilOfClass poc join PupilRanking pr on poc.PupilID = pr.PupilID
            //Join ConductEvaluationConduct cec on pr.PupilID = cec.PupilID 
            //join ConductLevel col on pr.ConductLevelID = col.ConductLevelID
            //join PupilProfile pp on poc.PupilID = pp.PupilID 
            ////theo các điều kiện
            //AcademicYearID, SchoolID, ClassID, EducationLevelID, Semester.
            //ConductLevelID: Nếu bằng 0 thì thực hiện tìm kiếm All.
            IDictionary<string, object> str = new Dictionary<string, object>();
            str["AcademicYearID"] = AcademicYearID;
            str["SchoolID"] = SchoolID;
            str["ClassID"] = ClassID;
            str["Semester"] = Semester;
            str["Check"] = "check";
            int modSchoolID = SchoolID % 100;
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
            var temp = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, str)
                       join pr in VPupilRankingBusiness.All.Where(o => o.SchoolID == SchoolID && o.ClassID == ClassID && o.AcademicYearID == AcademicYearID && o.Semester == Semester && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == ac.Year) on poc.PupilID equals pr.PupilID into g1
                       from g2 in g1.DefaultIfEmpty()
                       join cec in ConductEvaluationDetailRepository.All.Where(o => o.SchoolID == SchoolID && o.ClassID == ClassID && o.AcademicYearID == AcademicYearID && o.Semester == Semester) on g2.PupilID equals cec.PupilID into g3
                       from g4 in g3.DefaultIfEmpty()
                       join col in ConductLevelRepository.All on g2.ConductLevelID equals col.ConductLevelID into g5
                       from g6 in g5.DefaultIfEmpty()
                       join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID into g7
                       from g8 in g7.DefaultIfEmpty()
                       where g8.IsActive == true
                       select new PupilRankingBO
                       {
                           PupilRankingID = g2.PupilRankingID,
                           PupilID = poc.PupilID,
                           PupilCode = g8.PupilCode,
                           OrderInClass = poc.OrderInClass,
                           Name = g8.Name,
                           FullName = g8.FullName,
                           BirthDate = g8.BirthDate,
                           ClassID = poc.ClassID,
                           SchoolID = poc.SchoolID,
                           AcademicYearID = poc.AcademicYearID,
                           Task1Evaluation = g4.Task1Evaluation,
                           Task2Evaluation = g4.Task2Evaluation,
                           Task3Evaluation = g4.Task3Evaluation,
                           Task4Evaluation = g4.Task4Evaluation,
                           Task5Evaluation = g4.Task5Evaluation,
                           ConductLevelID = g6.ConductLevelID,
                           ConductLevelName = g6.Resolution,
                           ProfileStatus = poc.Status,
                           EndDate = poc.EndDate,
                           AssignedDate = poc.AssignedDate
                       };
            //IEnumerable<PupilRankingBO> lstPupilConduct = (from t in temp.AsEnumerable()
            //                                               where t.AssignedDate == (from poc in PupilOfClassBusiness.All
            //                                                                        where poc.PupilID == t.PupilID
            //                                                                        select poc.AssignedDate).Max()
            //                                               select t).ToList();
            IEnumerable<PupilRankingBO> lstPupilConduct = temp.AsEnumerable();
            if (AcademicYearID != 0)
            {
                lstPupilConduct = lstPupilConduct.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (SchoolID != 0)
            {
                lstPupilConduct = lstPupilConduct.Where(o => o.SchoolID == SchoolID);
            }

            if (ClassID != 0)
            {
                lstPupilConduct = lstPupilConduct.Where(o => o.ClassID == ClassID);
            }


            return lstPupilConduct.ToList();
        }

        /// <summary>
        /// QuangLM
        /// Kiem tra danh sach hoc sinh voi dieu kien xep hang xem co ton tai du lieu loi
        /// </summary>
        /// <param name="rankingCriteria"></param>
        /// <param name="listPupilRanking"></param>
        private void ValidateRanking(int rankingCriteria, List<PupilRanking> listPupilRanking)
        {
            if (rankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK)
            {
                foreach (PupilRanking pr in listPupilRanking)
                {
                    if (!pr.AverageMark.HasValue)
                    {
                        throw new BusinessException("PupilRanking_Label_NotAverage");
                    }
                }
            }
            else if (rankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)
            {
                foreach (PupilRanking pr in listPupilRanking)
                {
                    if (!pr.AverageMark.HasValue || !pr.CapacityLevelID.HasValue)
                    {
                        throw new BusinessException("PupilRanking_Label_NotAverageCapacityLevel");
                    }
                }
            }
            else if (rankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT)
            {
                foreach (PupilRanking pr in listPupilRanking)
                {
                    if (!pr.AverageMark.HasValue || !pr.ConductLevelID.HasValue)
                    {
                        throw new BusinessException("PupilRanking_Label_NotAverageConduct");
                    }
                }
            }
            else if (rankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT)
            {
                foreach (PupilRanking pr in listPupilRanking)
                {
                    if (!pr.AverageMark.HasValue || !pr.ConductLevelID.HasValue || !pr.CapacityLevelID.HasValue)
                    {
                        throw new BusinessException("PupilRanking_Label_NotAverageConductCapacityLevel");
                    }
                }
            }
        }

        /// <summary>
        /// Xep loai danh sach
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="listPupilRanking"></param>
        public void Rank(int RankingCriteria, List<PupilRanking> lstPupilRanking, bool isValidate = true)
        {
            if (lstPupilRanking == null || lstPupilRanking.Count == 0)
            {
                return;
            }
            // Kiem tra tinh hop le cua danh sach hoc sinh
            if (isValidate)
            {
                this.ValidateRanking(RankingCriteria, lstPupilRanking);
            }
            // Xep thu tu trong lop cho hoc sinh
            // Neu hoc sinh co 1 tieu chi trong so cac tieu chi de xep hang chua co thi se khong xep hang cho hoc sinh nay
            if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK)
            {
                lstPupilRanking = lstPupilRanking.OrderByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    // Neu khong diem TBM thi khong xep hang
                    if (!lstPupilRanking[i].AverageMark.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }
            }
            else if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)
            {
                lstPupilRanking = lstPupilRanking.OrderBy(o => o.CapacityLevelID ?? int.MaxValue)
                    .ThenByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    // Co diem TBM thi se co hoc luc nen chi can kiem tra diem TBM la du
                    if (!lstPupilRanking[i].AverageMark.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].CapacityLevelID == lstPupilRanking[i].CapacityLevelID
                            && lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }
            }
            else if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT)
            {
                lstPupilRanking = lstPupilRanking.OrderBy(o => (o.ConductLevelID.HasValue && o.AverageMark.HasValue) ? o.ConductLevelID.Value : int.MaxValue)
                    .ThenByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue && lstPupilRanking[0].ConductLevelID.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    if (!lstPupilRanking[i].AverageMark.HasValue || !lstPupilRanking[i].ConductLevelID.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].ConductLevelID == lstPupilRanking[i].ConductLevelID
                            && lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }

            }
            else if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT)
            {
                lstPupilRanking = lstPupilRanking
                    .OrderBy(o => (o.ConductLevelID.HasValue && o.AverageMark.HasValue) ? o.CapacityLevelID.Value : int.MaxValue)
                    .ThenBy(o => (o.ConductLevelID.HasValue && o.AverageMark.HasValue) ? o.ConductLevelID.Value : int.MaxValue)
                    .ThenByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue && lstPupilRanking[0].ConductLevelID.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    // TBM tuong duong voi hoc luc nen chi can kiem tra 1 cai la du
                    if (!lstPupilRanking[i].AverageMark.HasValue || !lstPupilRanking[i].ConductLevelID.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].CapacityLevelID == lstPupilRanking[i].CapacityLevelID
                            && lstPupilRanking[i - 1].ConductLevelID == lstPupilRanking[i].ConductLevelID
                            && lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }

            }
        }

        public Stream CreatePupilToRankConductByFaultReport(int appliedLevel, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.BANGXEPLOAI_HANHKIEM_THEODOT_VP);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            PeriodDeclaration periodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
            DateTime? semesterEnddate = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? periodDeclaration.AcademicYear.FirstSemesterEndDate : periodDeclaration.AcademicYear.SecondSemesterEndDate;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<PupilRankingBO> listPR = GetPupilToRankConductByFault(SchoolID, AcademicYearID, ClassID, Semester, PeriodID);

            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            IVTRange topRow = tempSheet.GetRange("A1", "I1");
            IVTRange midRow = tempSheet.GetRange("A2", "I2");
            IVTRange botRow = tempSheet.GetRange("A3", "I3");

            IVTRange topYRow = tempSheet.GetRange("A5", "I5");
            IVTRange midYRow = tempSheet.GetRange("A6", "I6");
            IVTRange botYRow = tempSheet.GetRange("A7", "I7");

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile school = acaYear.SchoolProfile;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string className = ClassProfileBusiness.Find(ClassID).DisplayName;
            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear != null ? (acaYear.Year + " - " + (acaYear.Year + 1)) : "Tất cả";
            dicVarable["Semester"] = Semester;
            dicVarable["ClassName"] = className;
            if (periodDeclaration != null && periodDeclaration.Resolution != null && periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
            {
                string periodTitle = periodDeclaration.Resolution.ToLower().Replace("đợt", "") + " (" + Utils.ConvertDateToString(periodDeclaration.FromDate)
                    + " - " + Utils.ConvertDateToString(periodDeclaration.EndDate) + ")";
                dicVarable["Period"] = periodTitle;
            }
            int index = 0;
            List<object> listData = new List<object>();
            string strGDTX = "GDTX";
            bool isGDTX = (school.TrainingType != null && school.TrainingType.Resolution == strGDTX);
            if (isGDTX)
            {
                listPR.ForEach(u =>
                {
                    listData.Add(new
                    {
                        Index = ++index,
                        PupilCode = u.PupilCode,
                        FullName = u.FullName,
                        TotalAbsentDaysWithPermission = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.TotalAbsentDaysWithPermission : null,
                        TotalAbsentDaysWithoutPermission = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.TotalAbsentDaysWithoutPermission : null,
                        NumberOfFault = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.NumberOfFault : null,
                        ConductPlusMark = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.sumConductMark : null,
                        ConductMinusMark = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.sumConductMinusMark : null,
                        PenalizedMark = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.sumTotalPenalizedMark : null,
                        TotalMark = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.TotalMark : null,
                        Comment = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.Comment : string.Empty,
                        ConductLevelName = (PupilProfileBusiness.IsNotConductRankingPupilImport(isGDTX, appliedLevel, u.LearningType, u.BirthDate) && (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))))
                                        ? (u.ConductLevelName != null ? (u.ConductLevelName.ToLower().Equals(("Tốt").ToLower()) ? "T" :
                                            u.ConductLevelName.ToLower().Equals(("Khá").ToLower()) ? "K" :
                                            u.ConductLevelName.ToLower().Equals(("Trung bình").ToLower()) || u.ConductLevelName.ToLower().Equals("tb") ? "TB" : "Y") : string.Empty)
                                        : string.Empty,
                        ProfileStatus = u.ProfileStatus
                    });

                    //tao border + boi vang voi cac hoc sinh co ProfileStatus khong phai la Dang Hoc
                    if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topRow, index + 6, 1, true);
                        else sheet.CopyPaste(midRow, index + 6, 1, true);
                        // boi vang doi voi dong co HS da tot nghiep
                        if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            int curRow = index + 6;
                            sheet.GetRange("A" + curRow, "K" + curRow).FillColor(System.Drawing.Color.Yellow);
                        }
                    }
                    else
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botYRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topYRow, index + 6, 1, true);
                        else sheet.CopyPaste(midYRow, index + 6, 1, true);
                    }
                });
            }
            else
            {
                listPR.ForEach(u =>
                {
                    listData.Add(new
                    {
                        Index = ++index,
                        PupilCode = u.PupilCode,
                        FullName = u.FullName,
                        TotalAbsentDaysWithPermission = ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))) ? u.TotalAbsentDaysWithPermission : null,
                        TotalAbsentDaysWithoutPermission = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.TotalAbsentDaysWithoutPermission : null,
                        NumberOfFault = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.NumberOfFault : null,
                        ConductPlusMark = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.sumConductMark : null,
                        ConductMinusMark = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.sumConductMinusMark : null,
                        PenalizedMark = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.sumTotalPenalizedMark : null,
                        TotalMark = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.TotalMark : null,
                        Comment = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.Comment : string.Empty,
                        ConductLevelName = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))
                                        ? (u.ConductLevelName != null ? (u.ConductLevelName.ToLower().Equals(("Tốt").ToLower()) ? "T" :
                                            u.ConductLevelName.ToLower().Equals(("Khá").ToLower()) ? "K" :
                                            u.ConductLevelName.ToLower().Equals(("Trung bình").ToLower()) || u.ConductLevelName.ToLower().Equals("tb") ? "TB" : "Y") : string.Empty)
                                        : string.Empty,
                        ProfileStatus = u.ProfileStatus
                    });

                    //tao border + boi vang voi cac hoc sinh co ProfileStatus khong phai la Dang Hoc
                    if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topRow, index + 6, 1, true);
                        else sheet.CopyPaste(midRow, index + 6, 1, true);
                        // boi vang doi voi dong co HS da tot nghiep
                        if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            int curRow = index + 6;
                            sheet.GetRange("A" + curRow, "K" + curRow).FillColor(System.Drawing.Color.Yellow);
                        }
                    }
                    else
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botYRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topYRow, index + 6, 1, true);
                        else sheet.CopyPaste(midYRow, index + 6, 1, true);
                    }
                });
            }
            dicVarable["Rows"] = listData;
            sheet.FillVariableValue(dicVarable);
            tempSheet.Delete();
            #endregion

            #region reportName
            //BangXepLoaiHanhKiem_[Period]_[Semester]_[ClassName] 
            reportName = reportName.Replace("[Semester]", semester);
            reportName = reportName.Replace("[Period]", ReportUtils.StripVNSign(periodDeclaration != null ? periodDeclaration.Resolution : string.Empty));
            reportName = reportName.Replace("[ClassName]", ReportUtils.StripVNSign(className));
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }

        public Stream CreatePupilToRankConductByCapacityReport(int appliedLevel, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, out string reportName)
        {
            PeriodDeclaration periodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
            DateTime? semesterEnddate = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? periodDeclaration.AcademicYear.FirstSemesterEndDate : periodDeclaration.AcademicYear.SecondSemesterEndDate;
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.BANGXEPLOAI_HANHKIEM_THEODOT_HL);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            List<PupilRankingBO> listPR = GetPupilToRankConductByCapacity(SchoolID, AcademicYearID, ClassID, Semester, PeriodID);

            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);

            IVTRange topRow = tempSheet.GetRange("A1", "J1");
            IVTRange midRow = tempSheet.GetRange("A2", "J2");
            IVTRange botRow = tempSheet.GetRange("A3", "J3");

            IVTRange topYRow = tempSheet.GetRange("A5", "J5");
            IVTRange midYRow = tempSheet.GetRange("A6", "J6");
            IVTRange botYRow = tempSheet.GetRange("A7", "J7");

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string className = ClassProfileBusiness.Find(ClassID).DisplayName;
            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["AcademicYear"] = acaYear != null ? (acaYear.Year + " - " + (acaYear.Year + 1)) : "Tất cả";
            dicVarable["Semester"] = Semester;
            dicVarable["ClassName"] = className;
            if (periodDeclaration != null && periodDeclaration.Resolution != null && periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
            {
                string periodTitle = periodDeclaration.Resolution.ToLower().Replace("đợt", "") + " (" + Utils.ConvertDateToString(periodDeclaration.FromDate)
                    + " - " + Utils.ConvertDateToString(periodDeclaration.EndDate) + ")";
                dicVarable["Period"] = periodTitle;
            }
            int index = 0;
            List<object> listData = new List<object>();
            bool isGDTX = school.TrainingType != null && school.TrainingType.Resolution == "GDTX";
            if (!isGDTX)
            {
                listPR.ForEach(u =>
                {
                    listData.Add(new
                    {
                        Index = ++index,
                        PupilCode = u.PupilCode,
                        FullName = u.FullName,
                        TotalAbsentDaysWithPermission = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.TotalAbsentDaysWithPermission : null,
                        TotalAbsentDaysWithoutPermission = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.TotalAbsentDaysWithoutPermission : null,
                        CapacityLevel = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.CapacityLevel : null,
                        Comment = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.Comment : string.Empty,
                        ConductLevelName = (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || ((u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                        || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))
                                        ? (u.ConductLevelName != null ? (u.ConductLevelName.ToLower().Equals(("Tốt").ToLower()) ? "T" :
                                            u.ConductLevelName.ToLower().Equals(("Khá").ToLower()) ? "K" :
                                            u.ConductLevelName.ToLower().Equals(("Trung bình").ToLower()) || u.ConductLevelName.ToLower().Equals("tb") ? "TB" : "Y") : string.Empty)
                                        : string.Empty,
                        ProfileStatus = u.ProfileStatus
                    });

                    //tao border + boi vang voi cac hoc sinh co ProfileStatus khong phai la Dang Hoc
                    if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topRow, index + 6, 1, true);
                        else sheet.CopyPaste(midRow, index + 6, 1, true);
                        // boi vang doi voi dong co HS da tot nghiep
                        if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            int curRow = index + 6;
                            sheet.GetRange("A" + curRow, "J" + curRow).FillColor(System.Drawing.Color.Yellow);
                        }
                    }
                    else
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botYRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topYRow, index + 6, 1, true);
                        else sheet.CopyPaste(midYRow, index + 6, 1, true);
                    }
                });
            }
            else
            {
                foreach (var u in listPR)
                {
                    bool DoNotRankConductOfPupil = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, appliedLevel, u.LearningType, u.BirthDate);
                    listData.Add(new
                    {
                        Index = ++index,
                        PupilCode = u.PupilCode,
                        FullName = u.FullName,
                        TotalAbsentDaysWithPermission = ( //!DoNotRankConductOfPupil && 
                                (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.TotalAbsentDaysWithPermission : null,
                        TotalAbsentDaysWithoutPermission = ( //!DoNotRankConductOfPupil &&
                                (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.TotalAbsentDaysWithoutPermission : null,
                        CapacityLevel = ( //!DoNotRankConductOfPupil &&
                                (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.CapacityLevel : null,
                        Comment = ( //!DoNotRankConductOfPupil &&
                                (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value))) ? u.Comment : string.Empty,
                        ConductLevelName = (!DoNotRankConductOfPupil &&
                                (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)
                                || (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && u.EndDate.HasValue && semesterEnddate.HasValue && u.EndDate.Value > semesterEnddate.Value)))
                                        ? (u.ConductLevelName != null ? (u.ConductLevelName.ToLower().Equals(("Tốt").ToLower()) ? "T" :
                                            u.ConductLevelName.ToLower().Equals(("Khá").ToLower()) ? "K" :
                                            u.ConductLevelName.ToLower().Equals(("Trung bình").ToLower()) || u.ConductLevelName.ToLower().Equals("tb") ? "TB" : "Y") : string.Empty)
                                        : string.Empty,
                        ProfileStatus = u.ProfileStatus
                    });

                    //tao border + boi vang voi cac hoc sinh co ProfileStatus khong phai la Dang Hoc
                    if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topRow, index + 6, 1, true);
                        else sheet.CopyPaste(midRow, index + 6, 1, true);
                        // boi vang doi voi dong co HS da tot nghiep
                        if (u.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            int curRow = index + 6;
                            sheet.GetRange("A" + curRow, "J" + curRow).FillColor(System.Drawing.Color.Yellow);
                        }
                    }
                    else
                    {
                        if (index % 5 == 0 || index == listPR.Count()) sheet.CopyPaste(botYRow, index + 6, 1, true);
                        else if (index % 5 == 1) sheet.CopyPaste(topYRow, index + 6, 1, true);
                        else sheet.CopyPaste(midYRow, index + 6, 1, true);
                    }
                }
            }
            dicVarable["Rows"] = listData;
            sheet.FillVariableValue(dicVarable);
            tempSheet.Delete();
            #endregion

            #region reportName
            //BangXepLoaiHanhKiem_[Period]_[Semester]_[ClassName]

            reportName = reportName.Replace("[Semester]", semester);
            reportName = reportName.Replace("[Period]", ReportUtils.StripVNSign(periodDeclaration != null ? periodDeclaration.Resolution : string.Empty));
            reportName = reportName.Replace("[ClassName]", ReportUtils.StripVNSign(className));
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }

        /// <summary>
        /// Lấy danh sách học sinh của lớp để thực hiện xếp loại hạnh kiểm theo học lực
        /// author hieund9
        /// </summary>
        /// <param name="SchoolID">SchoolID (int) : Lấy từ hệ thống trong phiên giao dịch hiện tại của người dùng</param>
        /// <param name="AcademicYearID">AcademicYearID (int) : Lấy từ hệ thống trong phiên giao dịch hiện tại của người dùng</param>
        /// <param name="dic">
        /// PupilID (int)
        /// Semester (int) 
        /// ClassID (int) 
        /// PeriodID
        /// </param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilToRankConductByCapacity(int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID)
        {
            PeriodDeclaration period = this.PeriodDeclarationRepository.Find(PeriodID);
            DateTime? startDate = period != null ? period.FromDate : null;
            DateTime? endDate = period != null ? period.EndDate : null;
            int modSchoolID = SchoolID % 100;
            List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
            AcademicYear ac = this.AcademicYearBusiness.Find(AcademicYearID);
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            bool isNotShowPupil = ac.IsShowPupil.HasValue && ac.IsShowPupil.Value;
            DateTime? firstsemester = null;
            DateTime? endsemester = null;
            if (Semester == 1)
            {
                firstsemester = ac.FirstSemesterStartDate;
                endsemester = ac.FirstSemesterEndDate;
            }
            else
                if (Semester == 2)
            {
                firstsemester = ac.SecondSemesterStartDate;
                endsemester = ac.SecondSemesterEndDate;
            }
            //Lấy danh sách thông tin học sinh nghỉ học
            Dictionary<string, object> dicPupilAbsence = new Dictionary<string, object>
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID", AcademicYearID},
                    {"ClassID", ClassID},
                    {"SemesterID",Semester},
                    {"lstSectionKeyID",objCP.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(objCP.SeperateKey.Value) : null}
                };

            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
            {
                #region period co gia tri
                if (PeriodID.HasValue && PeriodID.Value > 0)
                {
                    dicPupilAbsence.Add("FromAbsentDate", startDate);
                    dicPupilAbsence.Add("ToAbsentDate", endDate);
                    var pupilAbsents = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, ac);
                    var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = u.Count() }).ToList();

                    var data = from poc in this.PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } }).AddPupilStatus(isNotShowPupil)
                               join pp in this.PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               join pr in this.VPupilRankingBusiness.All.Where(u => u.Semester == Semester && u.PeriodID == PeriodID && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                                    on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID } into g1
                               from j1 in g1.DefaultIfEmpty()
                               join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into g2
                               from j2 in g2.DefaultIfEmpty()
                               join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into g3
                               from j3 in g3.DefaultIfEmpty()
                               select new PupilRankingBO()
                               {
                                   PupilCode = pp.PupilCode,
                                   FullName = pp.FullName,
                                   PupilID = poc.PupilID,
                                   CapacityLevelID = j1.CapacityLevelID,
                                   CapacityLevel = j2.CapacityLevel1,
                                   ConductLevel = j3.Resolution,
                                   ConductLevelName = j3.Resolution,
                                   BirthDate = pp.BirthDate,
                                   Semester = j1.Semester.Value,
                                   ClassID = ClassID,
                                   AcademicYearID = AcademicYearID,
                                   SchoolID = SchoolID,
                                   Comment = j1.PupilRankingComment,
                                   ProfileStatus = poc.Status,
                                   ConductLevelID = j1.ConductLevelID,
                                   AverageMark = j1.AverageMark,
                                   AssignedDate = poc.AssignedDate,
                                   OrderInClass = poc.OrderInClass,
                                   EndDate = poc.EndDate,
                                   Status = poc.Status,
                                   Name = pp.Name,
                                   LearningType = pp.PupilLearningType
                               };
                    listPupilRanking = data.ToList();

                    listPupilRanking.ForEach(u =>
                    {
                        u.TotalAbsentDaysWithPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && v.IsAccepted).Sum(v => v.Count);
                        u.TotalAbsentDaysWithoutPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && !v.IsAccepted).Sum(v => v.Count);
                    });
                }
                #endregion

                #region period ko co gia tri
                else
                {
                    dicPupilAbsence.Add("FromAbsentDate", firstsemester);
                    dicPupilAbsence.Add("ToAbsentDate", endsemester);
                    var pupilAbsents = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, ac);
                    var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = u.Count() }).ToList();

                    var data = from poc in this.PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } }).AddPupilStatus(isNotShowPupil)
                               join pp in this.PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               join pr in this.VPupilRankingBusiness.All.Where(u => u.Semester == Semester && !u.PeriodID.HasValue && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                                    on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID } into g1
                               from j1 in g1.DefaultIfEmpty()
                               join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into g2
                               from j2 in g2.DefaultIfEmpty()
                               join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into g3
                               from j3 in g3.DefaultIfEmpty()
                               select new PupilRankingBO()
                               {
                                   PupilCode = pp.PupilCode,
                                   FullName = pp.FullName,
                                   PupilID = poc.PupilID,
                                   CapacityLevelID = j1.CapacityLevelID,
                                   CapacityLevel = j2.CapacityLevel1,
                                   ConductLevel = j3.Resolution,
                                   ConductLevelName = j3.Resolution,
                                   BirthDate = pp.BirthDate,
                                   Semester = j1.Semester.Value,
                                   ClassID = ClassID,
                                   AcademicYearID = AcademicYearID,
                                   SchoolID = SchoolID,
                                   Comment = j1.PupilRankingComment,
                                   ProfileStatus = poc.Status,
                                   ConductLevelID = j1.ConductLevelID,
                                   AverageMark = j1.AverageMark,
                                   AssignedDate = poc.AssignedDate,
                                   OrderInClass = poc.OrderInClass,
                                   EndDate = poc.EndDate,
                                   Status = poc.Status,
                                   Name = pp.Name,
                                   LearningType = pp.PupilLearningType,
                               };
                    //updated by hieund9
                    listPupilRanking = data.ToList();
                    listPupilRanking.ForEach(u =>
                    {
                        u.TotalAbsentDaysWithPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && v.IsAccepted).Sum(v => v.Count);
                        u.TotalAbsentDaysWithoutPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && !v.IsAccepted).Sum(v => v.Count);
                    });
                }
                #endregion
            }
            else
            {
                #region Dang xet ren luyen lai
                dicPupilAbsence.Add("FromAbsentDate", firstsemester);
                dicPupilAbsence.Add("ToAbsentDate", endsemester);
                var pupilAbsents = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, ac);
                var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = u.Count() }).ToList();

                var data = from poc in this.PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } }).AddPupilStatus(isNotShowPupil)
                           join pp in this.PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                           join pr in this.VPupilRankingBusiness.All.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                              on new { poc.PupilID, poc.SchoolID, poc.AcademicYearID, poc.ClassID } equals new { pr.PupilID, pr.SchoolID, pr.AcademicYearID, pr.ClassID }
                           join prIV in this.VPupilRankingBusiness.All.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && u.CreatedAcademicYear == ac.Year && u.Last2digitNumberSchool == modSchoolID)
                              on new { poc.PupilID, poc.SchoolID, poc.AcademicYearID, poc.ClassID } equals new { prIV.PupilID, prIV.SchoolID, prIV.AcademicYearID, prIV.ClassID }
                              into g1
                           from pr2 in g1.DefaultIfEmpty()
                           join ca in CapacityLevelBusiness.All on pr.CapacityLevelID equals ca.CapacityLevelID into g2
                           from j2 in g2.DefaultIfEmpty()
                           join caBack in CapacityLevelBusiness.All on pr2.CapacityLevelID equals caBack.CapacityLevelID into g3
                           from j3 in g3.DefaultIfEmpty()
                           join co in ConductLevelBusiness.All on pr2.ConductLevelID equals co.ConductLevelID into g4
                           from j4 in g4.DefaultIfEmpty()
                           select new PupilRankingBO
                           {
                               PupilCode = pp.PupilCode,
                               FullName = pp.FullName,
                               PupilID = poc.PupilID,
                               CapacityLevelID = pr2.CapacityLevelID != null ? pr2.CapacityLevelID : pr.CapacityLevelID,
                               CapacityLevel = pr2.CapacityLevelID != null ? j3.CapacityLevel1 : j2.CapacityLevel1,
                               ConductLevel = j4.Resolution,
                               ConductLevelName = j4.Resolution,
                               BirthDate = pp.BirthDate,
                               Semester = pr2.Semester.Value,
                               ClassID = ClassID,
                               AcademicYearID = AcademicYearID,
                               SchoolID = SchoolID,
                               Comment = pr2.PupilRankingComment,
                               ProfileStatus = poc.Status,
                               ConductLevelID = pr2.ConductLevelID,
                               AverageMark = pr2.AverageMark != null ? pr2.AverageMark : pr.AverageMark,
                               AssignedDate = poc.AssignedDate,
                               OrderInClass = poc.OrderInClass,
                               Status = poc.Status,
                               EndDate = poc.EndDate,
                               Name = pp.Name,
                               LearningType = pp.PupilLearningType
                           };
                //updated by hieund9
                listPupilRanking = data.ToList();
                listPupilRanking.ForEach(u =>
                {
                    u.TotalAbsentDaysWithPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && v.IsAccepted).Sum(v => v.Count);
                    u.TotalAbsentDaysWithoutPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && !v.IsAccepted).Sum(v => v.Count);
                });
                #endregion
            }

            #region Lay gia tri Comment
            Dictionary<string, object> dicSummed = new Dictionary<string, object>();
            dicSummed["AcademicYearID"] = AcademicYearID;
            dicSummed["ClassID"] = ClassID;
            dicSummed["Semester"] = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND : Semester;
            if (period != null)
                dicSummed["PeriodID"] = period.PeriodDeclarationID;
            else
                dicSummed["PeriodID"] = null;

            //var lstSummed = (from p in VSummedUpRecordBusiness.SearchBySchool(SchoolID, dicSummed)
            //                 join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
            //                 select new
            //                 {
            //                     SummedUpRecordID = p.SummedUpRecordID,
            //                     PupilID = p.PupilID,
            //                     SubjectID = p.SubjectID,
            //                     Comment = p.Comment,
            //                     IsCommenting = q.IsCommenting
            //                 })
            //                .ToList();

            foreach (PupilRankingBO pupilRanking in listPupilRanking)
            {
                //IEnumerable<string> comments = lstSummed.Where(u => u.PupilID == pupilRanking.PupilID && u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE).Select(u => u.Comment).Where(u => !string.IsNullOrEmpty(u));
                //if (comments.Count() > 0) pupilRanking.Comment = string.Join(",", comments);

                pupilRanking.IsShow = pupilRanking.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pupilRanking.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (pupilRanking.EndDate.HasValue && pupilRanking.EndDate.Value > endsemester);
            }
            #endregion

            return listPupilRanking.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ToList();
        }

        /// <summary>
        /// Lấy danh sách học sinh của lớp để thực hiện xếp loại hạnh kiểm theo vi phạm
        /// </summary>
        /// <param name="SchoolID">SchoolID (int) : Lấy từ hệ thống trong phiên giao dịch hiện tại của người dùng</param>
        /// <param name="AcademicYearID">AcademicYearID (int) : Lấy từ hệ thống trong phiên giao dịch hiện tại của người dùng</param>
        /// <param name="dic">
        /// PupilID (int)
        /// Semester (int) 
        /// ClassID (int) 
        /// PeriodID
        /// </param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilToRankConductByFault(int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID)
        {
            List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
            PeriodDeclaration period = this.PeriodDeclarationRepository.Find(PeriodID);
            DateTime? startDate = period != null ? period.FromDate : null;
            DateTime? endDate = period != null ? period.EndDate : null;
            AcademicYear ac = this.AcademicYearBusiness.Find(AcademicYearID);
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            bool isNotShowPupil = ac.IsShowPupil.HasValue && ac.IsShowPupil.Value;
            int modSchoolID = SchoolID % 100;
            DateTime? firstsemester = null;
            DateTime? endsemester = null;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                firstsemester = ac.FirstSemesterStartDate;
                endsemester = ac.FirstSemesterEndDate;
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                firstsemester = ac.SecondSemesterStartDate;
                endsemester = ac.SecondSemesterEndDate;
            }
            //Lấy danh sách thông tin học sinh nghỉ học
            Dictionary<string, object> dicPupilAbsence = new Dictionary<string, object>
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID", AcademicYearID},
                    {"ClassID", ClassID},
                    {"SemesterID",Semester},
                    {"lstSectionKeyID",UtilsBusiness.GetListSectionID(objCP.SeperateKey.Value)}
                };

            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING) //Khong phai ren luyen lai
            {
                if (period != null)
                {
                    dicPupilAbsence.Add("FromAbsentDate", startDate);
                    dicPupilAbsence.Add("ToAbsentDate", endDate);
                    var pupilAbsents = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, ac);
                    var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = u.Count() }).ToList();

                    var pupilPraises = PupilPraiseBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromPraiseDate", startDate }, { "ToPraiseDate", endDate } });
                    var pupilPraisesGroup = pupilPraises.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, ConductMark = u.Sum(v => v.PraiseType.ConductMark) }).ToList();

                    var pupilDisciplines = PupilDisciplineBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromDisciplinedDate", startDate }, { "ToDisciplinedDate", endDate } });
                    var pupilDisciplinesGroup = pupilDisciplines.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, ConductMinusMark = u.Sum(v => v.DisciplineType.ConductMinusMark) }).ToList();

                    var pupilFaults = PupilFaultBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromViolatedDate", startDate }, { "ToViolatedDate", endDate } });
                    var pupilFaultsGroup = pupilFaults.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, TotalPenalizedMark = u.Sum(v => v.TotalPenalizedMark), Count = u.Count() }).ToList();

                    var data = from poc in this.PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } }).AddPupilStatus(isNotShowPupil)
                               join pp in this.PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               join pr in this.PupilRankingBusiness.All.Where(u => u.Semester == Semester && u.PeriodID == PeriodID && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                                    on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID } into g1
                               from j1 in g1.DefaultIfEmpty()
                               join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into g2
                               from j2 in g2.DefaultIfEmpty()
                               join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into g3
                               from j3 in g3.DefaultIfEmpty()
                               select new PupilRankingBO
                               {
                                   PupilCode = pp.PupilCode,
                                   FullName = pp.FullName,
                                   SchoolID = SchoolID,
                                   AcademicYearID = AcademicYearID,
                                   PupilID = poc.PupilID,
                                   sumMark = 0,
                                   ProfileStatus = poc.Status,
                                   ConductLevelID = j1.ConductLevelID,
                                   CapacityLevelID = j1.CapacityLevelID,
                                   EducationLevelID = j1.EducationLevelID,
                                   AverageMark = j1.AverageMark,
                                   AssignedDate = poc.AssignedDate,
                                   ConductLevelName = j3.Resolution,
                                   ConductLevel = j3.Resolution,
                                   CapacityLevel = j2.CapacityLevel1,
                                   OrderInClass = poc.OrderInClass,
                                   Comment = j1.PupilRankingComment,
                                   Name = pp.Name,
                                   BirthDate = pp.BirthDate,
                                   EndDate = poc.EndDate,
                                   LearningType = pp.PupilLearningType
                               };
                    //updated by hieund9
                    listPupilRanking = data.ToList();
                    listPupilRanking.ForEach(u =>
                    {
                        u.TotalAbsentDaysWithPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && v.IsAccepted).Sum(v => v.Count);
                        u.TotalAbsentDaysWithoutPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && !v.IsAccepted).Sum(v => v.Count);
                        u.sumConductMark = pupilPraisesGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.ConductMark);
                        u.sumConductMinusMark = pupilDisciplinesGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.ConductMinusMark);
                        u.sumTotalPenalizedMark = pupilFaultsGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.TotalPenalizedMark);
                        u.NumberOfFault = pupilFaultsGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.Count);
                    });
                }
                else
                {
                    dicPupilAbsence.Add("FromAbsentDate", firstsemester);
                    dicPupilAbsence.Add("ToAbsentDate", endsemester);
                    var pupilAbsents = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, ac);
                    var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = u.Count() }).ToList();

                    //var pupilAbsents = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                    //                    ? PupilAbsenceBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID } })
                    //                    : PupilAbsenceBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "FromAbsentDate", firstsemester }, { "ToAbsentDate", endsemester } });
                    //var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = (int?)u.Count() }).ToList();

                    var pupilPraises = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        ? PupilPraiseBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } })
                                        : PupilPraiseBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromPraiseDate", firstsemester }, { "ToPraiseDate", endsemester } });
                    var pupilPraisesGroup = pupilPraises.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, ConductMark = u.Sum(v => v.PraiseType.ConductMark) }).ToList();

                    var pupilDisciplines = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        ? PupilDisciplineBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } })
                                        : PupilDisciplineBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromDisciplinedDate", firstsemester }, { "ToDisciplinedDate", endsemester } });
                    var pupilDisciplinesGroup = pupilDisciplines.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, ConductMinusMark = u.Sum(v => v.DisciplineType.ConductMinusMark) }).ToList();

                    var pupilFaults = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        ? PupilFaultBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } })
                                        : PupilFaultBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromViolatedDate", firstsemester }, { "ToViolatedDate", endsemester } });
                    var pupilFaultsGroup = pupilFaults.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, TotalPenalizedMark = u.Sum(v => v.TotalPenalizedMark), Count = u.Sum(v => v.NumberOfFault) }).ToList();

                    var data = from poc in this.PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } }).AddPupilStatus(isNotShowPupil)
                               join pp in this.PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               join pr in this.PupilRankingBusiness.All.Where(u => u.Semester == Semester && !u.PeriodID.HasValue && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                                    on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID } into g1
                               from j1 in g1.DefaultIfEmpty()
                               join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into g2
                               from j2 in g2.DefaultIfEmpty()
                               join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into g3
                               from j3 in g3.DefaultIfEmpty()
                               select new PupilRankingBO
                               {
                                   PupilCode = pp.PupilCode,
                                   FullName = pp.FullName,
                                   PupilID = poc.PupilID,
                                   SchoolID = SchoolID,
                                   AcademicYearID = AcademicYearID,
                                   sumMark = 0,
                                   ProfileStatus = poc.Status,
                                   ConductLevelID = j1.ConductLevelID,
                                   CapacityLevelID = j1.CapacityLevelID,
                                   EducationLevelID = j1.EducationLevelID,
                                   AverageMark = j1.AverageMark,
                                   AssignedDate = poc.AssignedDate,
                                   ConductLevelName = j3.Resolution,
                                   ConductLevel = j3.Resolution,
                                   CapacityLevel = j2.CapacityLevel1,
                                   OrderInClass = poc.OrderInClass,
                                   Comment = j1.PupilRankingComment,
                                   Name = pp.Name,
                                   BirthDate = pp.BirthDate,
                                   EndDate = poc.EndDate,
                                   LearningType = pp.PupilLearningType
                               };
                    //updated by hieund9
                    //(với mỗi học sinh lấy max(poc.AssigedDate))
                    listPupilRanking = data.ToList();
                    listPupilRanking.ForEach(u =>
                    {
                        u.TotalAbsentDaysWithPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && v.IsAccepted).Sum(v => v.Count);
                        u.TotalAbsentDaysWithoutPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && !v.IsAccepted).Sum(v => v.Count);
                        u.sumConductMark = pupilPraisesGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.ConductMark);
                        u.sumConductMinusMark = pupilDisciplinesGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.ConductMinusMark);
                        u.sumTotalPenalizedMark = pupilFaultsGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.TotalPenalizedMark);
                        u.NumberOfFault = pupilFaultsGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.Count);
                    });
                }
            }
            else //Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING (4)
            {
                dicPupilAbsence.Add("FromAbsentDate", firstsemester);
                dicPupilAbsence.Add("ToAbsentDate", endsemester);
                var pupilAbsents = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, ac);
                var pupilAbsentsGroup = pupilAbsents.GroupBy(u => new { u.PupilID, u.IsAccepted }).Select(u => new { u.Key.PupilID, IsAccepted = u.Key.IsAccepted.HasValue && u.Key.IsAccepted.Value, Count = u.Count() }).ToList();

                var pupilPraises = PupilPraiseBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } });
                var pupilPraisesGroup = pupilPraises.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, ConductMark = u.Sum(v => v.PraiseType.ConductMark) }).ToList();

                var pupilDisciplines = PupilDisciplineBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } });
                var pupilDisciplinesGroup = pupilDisciplines.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, ConductMinusMark = u.Sum(v => v.DisciplineType.ConductMinusMark) }).ToList();

                //var pupilFaults = PupilFaultBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } });
                //var pupilFaultsGroup = pupilFaults.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, TotalPenalizedMark = u.Sum(v => v.TotalPenalizedMark), Count = u.Count() }).ToList();

                var pupilFaults = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && Semester != SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        ? PupilFaultBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID } })
                                        : PupilFaultBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "FromViolatedDate", firstsemester }, { "ToViolatedDate", endsemester } });
                var pupilFaultsGroup = pupilFaults.GroupBy(u => u.PupilID).Select(u => new { PupilID = u.Key, TotalPenalizedMark = u.Sum(v => v.TotalPenalizedMark), Count = u.Sum(v => v.NumberOfFault) }).ToList();

                var data = from poc in this.PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } }).AddPupilStatus(isNotShowPupil)
                           join pp in this.PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                           join pr in this.VPupilRankingBusiness.All.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                           on new { poc.PupilID, poc.SchoolID, poc.AcademicYearID, poc.ClassID } equals new { pr.PupilID, pr.SchoolID, pr.AcademicYearID, pr.ClassID }
                           join pr1 in this.PupilRankingBusiness.All.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year)
                           on new { poc.PupilID, poc.SchoolID, poc.AcademicYearID, poc.ClassID } equals new { pr1.PupilID, pr1.SchoolID, pr1.AcademicYearID, pr1.ClassID } into g1
                           from g in g1.DefaultIfEmpty()
                           join ca in CapacityLevelBusiness.All on pr.CapacityLevelID equals ca.CapacityLevelID into g2
                           from j2 in g2.DefaultIfEmpty()
                           join caBack in CapacityLevelBusiness.All on g.CapacityLevelID equals caBack.CapacityLevelID into g3
                           from j3 in g3.DefaultIfEmpty()
                           join co in ConductLevelBusiness.All on g.ConductLevelID equals co.ConductLevelID into g4
                           from j4 in g4.DefaultIfEmpty()
                           select new PupilRankingBO
                           {
                               PupilCode = pp.PupilCode,
                               FullName = pp.FullName,
                               PupilID = poc.PupilID,
                               SchoolID = SchoolID,
                               AcademicYearID = AcademicYearID,
                               sumMark = 0,
                               ProfileStatus = poc.Status,
                               ConductLevelID = g.ConductLevelID,
                               CapacityLevelID = g.CapacityLevelID != null ? g.CapacityLevelID : pr.CapacityLevelID,
                               EducationLevelID = poc.ClassProfile.EducationLevelID,
                               ClassID = ClassID,
                               AverageMark = g.AverageMark != null ? g.AverageMark : pr.AverageMark,
                               AssignedDate = poc.AssignedDate,
                               ConductLevelName = g.ConductLevelID != null ? j4.Resolution : string.Empty,
                               ConductLevel = g.ConductLevelID != null ? j4.Resolution : string.Empty,
                               CapacityLevel = g.CapacityLevelID != null ? j3.CapacityLevel1 : j2.CapacityLevel1,
                               OrderInClass = poc.OrderInClass,
                               Name = pp.Name,
                               BirthDate = pp.BirthDate,
                               EndDate = poc.EndDate,
                               LearningType = pp.PupilLearningType,
                               Comment = g.PupilRankingComment
                           };
                listPupilRanking = data.ToList();
                listPupilRanking.ForEach(u =>
                {
                    u.TotalAbsentDaysWithPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && v.IsAccepted).Sum(v => v.Count);
                    u.TotalAbsentDaysWithoutPermission = pupilAbsentsGroup.Where(v => v.PupilID == u.PupilID && !v.IsAccepted).Sum(v => v.Count);
                    u.sumConductMark = pupilPraisesGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.ConductMark);
                    u.sumConductMinusMark = pupilDisciplinesGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.ConductMinusMark);
                    u.sumTotalPenalizedMark = pupilFaultsGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.TotalPenalizedMark);
                    u.NumberOfFault = pupilFaultsGroup.Where(v => v.PupilID == u.PupilID).Sum(v => v.Count);
                });
            }

            var maxValue = this.ConductConfigByViolationRepository.All.Where(u => u.SchoolID == SchoolID && u.IsActive == true).Max(u => u.MaxValue);
            foreach (var item in listPupilRanking)
            {
                maxValue = maxValue.HasValue ? maxValue.Value : 0;
                item.sumConductMark = item.sumConductMark.HasValue ? item.sumConductMark.Value : 0;
                item.sumConductMinusMark = item.sumConductMinusMark.HasValue ? item.sumConductMinusMark.Value : 0;
                item.sumTotalPenalizedMark = item.sumTotalPenalizedMark.HasValue ? item.sumTotalPenalizedMark.Value : 0;
                item.TotalMark = maxValue.Value + item.sumConductMark.Value - item.sumConductMinusMark.Value - item.sumTotalPenalizedMark.Value;
                item.IsShow = item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || item.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (item.EndDate.HasValue && item.EndDate.Value > endsemester);
            }
            return listPupilRanking.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ToList();
        }

        public IQueryable<PupilRanking> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
                return null;
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        /// <summary>
        /// Tổng số điểm (còn lại)
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilID"></param>
        /// <returns></returns>
        private decimal GetTotalMark(int SchoolID, int AcademicYearID, int? EducationLevelID, int ClassID, int PupilID)
        {
            //+ Tìm kiếm trong bảng ConductConfigByViolation theo SchoolID => lấy Max (MaxValue) trong các bản ghi tìm được.
            decimal? MaxValue = ConductConfigByViolationRepository.All.Where(o => o.SchoolID == SchoolID).Max(o => o.MaxValue);
            //+ Tìm kiếm trong bảng PupilFaul theo PupilID, SchoolID, ClassID, AcademicYearID, EducationLevelID => lấy Sum(TotalPenalizedMark)
            decimal? TotalPenalizedMark = PupilFaultRepository.All.Where(o => o.PupilID == PupilID && o.SchoolID == SchoolID
                && o.ClassID == ClassID && o.AcademicYearID == AcademicYearID && o.EducationLevelID == EducationLevelID).Sum(o => o.TotalPenalizedMark);
            //Tổng điểm = Max (MaxValue) - Sum(TotalPenalizedMark)
            return (MaxValue.HasValue ? MaxValue.Value : 0) - (TotalPenalizedMark.HasValue ? TotalPenalizedMark.Value : 0);
        }

        /// <summary>
        /// lay comment cua pupil
        /// </summary>
        /// <param name="PupilID"></param>
        /// <returns></returns>
        private string GetComment(int PupilID, int? ClassID, int? Semester)
        {
            return SummedUpRecordRepository.All.FirstOrDefault(o => o.PupilID == PupilID && o.ClassID == ClassID && o.Semester == Semester.Value).Comment;
        }
        /// <summary>
        ///TotalAbsentDaysWithPermission (Số buổi nghỉ có phép)
        ///IsAccepted (trong bảng PupilAbsence) = 1 (có phép)
        ///TotalAbsentDaysWithoutPermission (Số buổi nghỉ không có phép)
        ///IsAccepted (trong bảng PupilAbsence) = 0 (không phép)
        ///</summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="PupilID"></param>
        /// <param name="IsAccepted"></param>
        /// <param name="FromDate">FromDate <= AbsentDate <= EndDate</param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        private int? GetTotalAbsentDays(int SchoolID, int AcademicYearID, int PupilID, bool IsAccepted, DateTime? FromDate, DateTime? EndDate)
        {
            int total = PupilAbsenceRepository.All.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID)
                                              .Where(o => o.PupilID == PupilID && o.IsAccepted == IsAccepted)
                                              .Where(o => o.AbsentDate >= FromDate && o.AbsentDate <= EndDate).Count();
            return int.Parse(total.ToString());
        }

        //Lấy từng phần tử trong lstPupilRanking
        //Thực hiện tìm kiếm trong bảng PupilRanking theo
        //PupilID = lstPupilRanking[i].PupilID; ClassID = lstPupilRanking[i].ClassID; 
        //Semester = lstPupilRanking[i].Semester; Period = lstPupilRanking[i].PeriodID
        private PupilRanking _SearchPupilRanking(PupilRanking pupilranking)
        {
            return this.Search(
               new Dictionary<string, object> {
                    { "PupilID", pupilranking.PupilID },
                    { "ClassID", pupilranking.ClassID },
                    { "Semester", pupilranking.Semester },
                    { "PeriodID", pupilranking.PeriodID }
                }
           ).FirstOrDefault();
        }

        /// <summary>
        /// Lay xep loai va xep hang cua cac hoc sinh trong lop
        /// </summary>
        public IQueryable<PupilOfClassRanking> GetPupilRankingOfClass(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID)
        {
            int? createdAcaYear = AcademicYearBusiness.Find(AcademicYearID).Year;
            int last2SchoolNumber = SchoolID % 100;
            if (PeriodID.HasValue)
            {
                var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                               && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && u.ClassID == ClassID && u.Semester == Semester && u.PeriodID == PeriodID.Value)
                           on poc.PupilID equals pr.PupilID into g1
                           from j1 in g1.DefaultIfEmpty()
                           join pd in PeriodDeclarationBusiness.All on j1.PeriodID equals pd.PeriodDeclarationID into pd1
                           from pd2 in pd1.DefaultIfEmpty()
                           join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into ca1
                           from ca2 in ca1.DefaultIfEmpty()
                           join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into co1
                           from co2 in co1.DefaultIfEmpty()
                           select new PupilOfClassRanking
                           {
                               PupilRankingID = j1.PupilRankingID,
                               ClassID = ClassID,
                               PupilID = poc.PupilID,
                               AcademicYearID = poc.AcademicYearID,
                               EducationLevelID = poc.ClassProfile.EducationLevelID,
                               Period = pd2.Resolution,
                               PeriodID = j1.PeriodID,
                               ProfileStatus = pp.ProfileStatus,
                               Semester = (int)Semester,
                               StudyingJudgementID = j1.StudyingJudgementID,
                               Rank = j1.Rank,
                               ConductLevelID = j1.ConductLevelID,
                               ConductLevel = co2.Resolution,
                               AverageMark = j1.AverageMark,
                               CapacityLevelID = j1.CapacityLevelID,
                               CapacityLevel = ca2.CapacityLevel1,
                               Year = poc.Year,
                               AssignDate = poc.AssignedDate,
                               SchoolID = SchoolID,
                               Status = poc.Status,
                               RankingDate = j1.RankingDate,
                               Name = pp.Name,
                               OrderInClass = poc.OrderInClass,
                               FullName = pp.FullName,
                               TotalAbsentDaysWithoutPermission = j1.TotalAbsentDaysWithoutPermission,
                               TotalAbsentDaysWithPermission = j1.TotalAbsentDaysWithPermission,
                               PupilRankingComment = j1.PupilRankingComment
                           };
                return data;
            }
            else
            {
                var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                               && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && u.ClassID == ClassID && u.Semester == Semester && !u.PeriodID.HasValue)
                           on poc.PupilID equals pr.PupilID into g1
                           from j1 in g1.DefaultIfEmpty()
                           join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into ca1
                           from ca2 in ca1.DefaultIfEmpty()
                           join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into co1
                           from co2 in co1.DefaultIfEmpty()
                           select new PupilOfClassRanking
                           {
                               PupilRankingID = j1.PupilRankingID,
                               ClassID = ClassID,
                               PupilID = poc.PupilID,
                               AcademicYearID = poc.AcademicYearID,
                               EducationLevelID = poc.ClassProfile.EducationLevelID,
                               Period = string.Empty,
                               PeriodID = j1.PeriodID,
                               ProfileStatus = pp.ProfileStatus,
                               Semester = (int)Semester,
                               StudyingJudgementID = j1.StudyingJudgementID,
                               Rank = j1.Rank,
                               ConductLevelID = j1.ConductLevelID,
                               ConductLevel = co2.Resolution,
                               AverageMark = j1.AverageMark,
                               CapacityLevelID = j1.CapacityLevelID,
                               CapacityLevel = ca2.CapacityLevel1,
                               Year = poc.Year,
                               AssignDate = poc.AssignedDate,
                               SchoolID = SchoolID,
                               Status = poc.Status,
                               RankingDate = j1.RankingDate,
                               Name = pp.Name,
                               OrderInClass = poc.OrderInClass,
                               FullName = pp.FullName,
                               TotalAbsentDaysWithoutPermission = j1.TotalAbsentDaysWithoutPermission,
                               TotalAbsentDaysWithPermission = j1.TotalAbsentDaysWithPermission,
                               PupilRankingComment = j1.PupilRankingComment
                           };
                return data;
            }
        }
        public IQueryable<PupilOfClassRanking> GetPupilRankingByClassID(List<int> lstClassID, int AcademicYearID, int SchoolID, int Semester, int? PeriodID)
        {
            int? createdAcaYear = AcademicYearBusiness.Find(AcademicYearID).Year;
            int last2SchoolNumber = SchoolID % 100;
            if (PeriodID.HasValue)
            {
                var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "lstClassID", lstClassID }, { "Check", "Check" } })
                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                               && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClassID.Contains(u.ClassID) && u.Semester == Semester && u.PeriodID == PeriodID.Value)
                           on poc.PupilID equals pr.PupilID into g1
                           from j1 in g1.DefaultIfEmpty()
                           join pd in PeriodDeclarationBusiness.All on j1.PeriodID equals pd.PeriodDeclarationID into pd1
                           from pd2 in pd1.DefaultIfEmpty()
                           join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into ca1
                           from ca2 in ca1.DefaultIfEmpty()
                           join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into co1
                           from co2 in co1.DefaultIfEmpty()
                           select new PupilOfClassRanking
                           {
                               PupilRankingID = j1.PupilRankingID,
                               ClassID = poc.ClassID,
                               PupilID = poc.PupilID,
                               AcademicYearID = poc.AcademicYearID,
                               EducationLevelID = poc.ClassProfile.EducationLevelID,
                               Period = pd2.Resolution,
                               PeriodID = j1.PeriodID,
                               ProfileStatus = pp.ProfileStatus,
                               Semester = (int)Semester,
                               StudyingJudgementID = j1.StudyingJudgementID,
                               Rank = j1.Rank,
                               ConductLevelID = j1.ConductLevelID,
                               ConductLevel = co2.Resolution,
                               AverageMark = j1.AverageMark,
                               CapacityLevelID = j1.CapacityLevelID,
                               CapacityLevel = ca2.CapacityLevel1,
                               Year = poc.Year,
                               AssignDate = poc.AssignedDate,
                               SchoolID = SchoolID,
                               Status = poc.Status,
                               RankingDate = j1.RankingDate,
                               Name = pp.Name,
                               OrderInClass = poc.OrderInClass,
                               FullName = pp.FullName,
                               TotalAbsentDaysWithoutPermission = j1.TotalAbsentDaysWithoutPermission,
                               TotalAbsentDaysWithPermission = j1.TotalAbsentDaysWithPermission,
                               PupilRankingComment = j1.PupilRankingComment
                           };
                return data;
            }
            else
            {
                var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "lstClassID", lstClassID }, { "Check", "Check" } })
                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                           join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                               && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClassID.Contains(u.ClassID) && u.Semester == Semester && !u.PeriodID.HasValue)
                           on poc.PupilID equals pr.PupilID into g1
                           from j1 in g1.DefaultIfEmpty()
                           join ca in CapacityLevelBusiness.All on j1.CapacityLevelID equals ca.CapacityLevelID into ca1
                           from ca2 in ca1.DefaultIfEmpty()
                           join co in ConductLevelBusiness.All on j1.ConductLevelID equals co.ConductLevelID into co1
                           from co2 in co1.DefaultIfEmpty()
                           select new PupilOfClassRanking
                           {
                               PupilRankingID = j1.PupilRankingID,
                               ClassID = poc.ClassID,
                               PupilID = poc.PupilID,
                               AcademicYearID = poc.AcademicYearID,
                               EducationLevelID = poc.ClassProfile.EducationLevelID,
                               Period = string.Empty,
                               PeriodID = j1.PeriodID,
                               ProfileStatus = pp.ProfileStatus,
                               Semester = (int)Semester,
                               StudyingJudgementID = j1.StudyingJudgementID,
                               Rank = j1.Rank,
                               ConductLevelID = j1.ConductLevelID,
                               ConductLevel = co2.Resolution,
                               AverageMark = j1.AverageMark,
                               CapacityLevelID = j1.CapacityLevelID,
                               CapacityLevel = ca2.CapacityLevel1,
                               Year = poc.Year,
                               AssignDate = poc.AssignedDate,
                               SchoolID = SchoolID,
                               Status = poc.Status,
                               RankingDate = j1.RankingDate,
                               Name = pp.Name,
                               OrderInClass = poc.OrderInClass,
                               FullName = pp.FullName,
                               TotalAbsentDaysWithoutPermission = j1.TotalAbsentDaysWithoutPermission,
                               TotalAbsentDaysWithPermission = j1.TotalAbsentDaysWithPermission,
                               PupilRankingComment = j1.PupilRankingComment
                           };
                return data;
            }
        }

        /// <summary>
        /// RankingPupilOfClass: thuc hien tong ket diem va xep hang.Reviewed
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstSummedUpRecord"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public void RankingPupilOfClass(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isShowRetetResult, string AutoMark, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            try
            {

                if (lstSummedUpRecord == null || lstSummedUpRecord.Count == 0)
                    throw new BusinessException("PupilRanking_Label_AllPupilNotMark");
                SetAutoDetectChangesEnabled(false);
                SummedUpRecord Sum = lstSummedUpRecord.FirstOrDefault();

                int ClassID = Sum.ClassID;
                int AcademicYearID = Sum.AcademicYearID;
                int SchoolID = Sum.SchoolID;
                int Semester = Sum.Semester.Value;

                // Kiem tra nam hoc hien tai
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                //int Year = academicYear.Year;
                //bool isMovedHistory = UtilsBusiness.IsMoveHistory(academicYear);

                if (academicYear == null || !AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("ClassMovement_Validate_IsNotCurrentYear");

                // Kiem tra neu he so mon cua lop hoc ko co thi bao loi
                #region
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.FirstSemesterCoefficient.HasValue && o.FirstSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.SecondSemesterCoefficient.HasValue && o.SecondSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }
                #endregion

                List<int> listPupilID = lstPupilRanking.Select(o => o.PupilID).Distinct().ToList();
                List<int> listSubjectID = lstSummedUpRecord.Select(o => o.SubjectID).Distinct().ToList();
                var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               where poc.ClassID == ClassID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                    && pp.IsActive
                               select new { ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, poc.PupilID, poc.Status, pp.BirthDate, pp.PupilLearningType })
                    .ToList();
                List<ExemptedSubject> listExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, Semester).ToList();
                #region Validate du lieu dau vao
                if (academicYear.SchoolID != SchoolID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                /*if (PeriodID != null)
                {
                    bool PeriodSemester = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                                                                    new Dictionary<string, object>()
                                                                    {
                                                                        {"Semester",Sum.Semester},
                                                                        {"PeriodDeclarationID",Sum.PeriodID}
                                                                    }, null);
                    if (!PeriodSemester)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }*/

                //Cac mon phai co diem
                foreach (ClassSubject classSubject in lstSubject)
                {
                    if (lstSummedUpRecord.Any(u => u.ClassID == classSubject.ClassID && u.SubjectID == classSubject.SubjectID
                            && u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && string.IsNullOrEmpty(u.JudgementResult)))
                        Utils.ValidateRequire(string.Empty, "SummedUpRecord_Label_SummedUpMark");

                    if (lstSummedUpRecord.Any(u => u.ClassID == classSubject.ClassID && u.SubjectID == classSubject.SubjectID
                           && (u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                           && !u.SummedUpMark.HasValue))
                        Utils.ValidateRequire(string.Empty, "SummedUpRecord_Label_SummedUpMark");
                }

                //neu ton tai mot hoc sinh duoc mien giam thi bao loi
                if (listExempteds.Any(u => lstSummedUpRecord.Any(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID)))
                    throw new BusinessException("SummedUpRecord_Labal_ErrMGSubject");

                foreach (int PupilID in listPupilID)
                {
                    var poc = listPOC.FirstOrDefault(u => u.PupilID == PupilID);
                    if (poc == null || (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                        throw new BusinessException("ClassMovement_Validate_PupilNotWorking");

                    if (poc.ClassID != ClassID)
                        throw new BusinessException("Common_Validate_NotCompatible");

                    if (poc.AcademicYearID != AcademicYearID)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }
                #endregion

                // Process
                // Lay 2 danh sach tu 2 danh sach dau vao join qua dieu kien pupilID     
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(SchoolID);
                List<int> listCheckedPupilID = new List<int>();
                bool isGDTX = schoolProfile.TrainingType != null && schoolProfile.TrainingType.Resolution == "GDTX";
                int EducationLevelID = 0;
                int AppliedLevel = 0;
                if (lstPupilRanking != null && lstPupilRanking.Count > 0)
                {
                    EducationLevelID = lstPupilRanking[0].EducationLevelID;
                    if (isGDTX)
                        AppliedLevel = EducationLevelBusiness.Find(EducationLevelID).Grade;
                }
                List<PupilRanking> ListPupilRanking4Save = new List<PupilRanking>();

                TrainingType traningType = schoolProfile.TrainingType;

                // Lay du lieu trong PupilRanking theo truong, nam hoc, ky, dot neu co truoc
                Dictionary<string, object> dicRanking = new Dictionary<string, object>();
                dicRanking["AcademicYearID"] = AcademicYearID;
                dicRanking["ClassID"] = ClassID;
                dicRanking["Semester"] = Semester;
                if (PeriodID.HasValue)
                {
                    dicRanking["PeriodID"] = PeriodID.Value;
                }
                else
                {
                    dicRanking["PeriodID"] = null;
                }
                List<PupilRanking> listPupilRankingByClass = this.SearchBySchool(SchoolID, dicRanking).ToList();
                List<PupilRanking> lstPupilRankingRe = new List<PupilRanking>();

                if (!PeriodID.HasValue)
                {
                    listPupilRankingByClass = listPupilRankingByClass.Where(o => !o.PeriodID.HasValue).ToList();
                }
                List<int> listPupilRetestID = listPupilRankingByClass.Where(o => o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                   .Select(o => o.PupilID).ToList();
                //Neu hoc ky la ca nam thi lay ra cac ban ghi thi lai, ren luyen lai de cap nhat.
                // Lay danh sach hoc sinh da co nhap diem thi lai roi thi moi tinh vao truong hop nay
                // Neu chi moi xep loai la thi lai ma chua nhap diem thi lai thi van cap nhat vao ban ghi ca nam

                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && isShowRetetResult)
                {
                    // Co HS thi lai moi tim dan sach chi tiet
                    if (listPupilRetestID.Count > 0)
                    {
                        dicRanking["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                        lstPupilRankingRe = this.Search(dicRanking).ToList();
                        List<int> listPupilIDInputMarkRetest = null;
                        listPupilIDInputMarkRetest = VSummedUpRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                           {
                                {"AcademicYearID", AcademicYearID},
                                {"ClassID", ClassID}
                           }).Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL && !o.PeriodID.HasValue).Select(o => o.PupilID).ToList();
                        listPupilRetestID.RemoveAll(o => !listPupilIDInputMarkRetest.Contains(o));

                    }
                }
                List<int> listPupilIDNotRankConduct = new List<int>();
                Dictionary<string, ClassSubject> dicClassSubject = new Dictionary<string, ClassSubject>();

                foreach (PupilRanking PupilRanking in lstPupilRanking)
                {
                    List<SummedUpRecord> lstPupilRankingOfPupil_Mandatory = new List<SummedUpRecord>();
                    List<SummedUpRecord> lstPupilRankingOfPupil_Voluntary = new List<SummedUpRecord>();
                    if (listCheckedPupilID.Contains(PupilRanking.PupilID))
                    {
                        continue;
                    }

                    //if(classSubject.FirstSemesterCoefficient.HasValue
                    foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
                    {
                        string key = SummedUpRecord.ClassID + "_" + SummedUpRecord.SubjectID;
                        // Lay mon hoc cua lop
                        ClassSubject classSubject = null;
                        if (dicClassSubject.ContainsKey(key))
                        {
                            classSubject = dicClassSubject[key];
                        }
                        else
                        {
                            classSubject = lstSubject.Where(u => u.ClassID == SummedUpRecord.ClassID && u.SubjectID == SummedUpRecord.SubjectID).FirstOrDefault();
                            dicClassSubject[key] = classSubject;
                        }
                        if (classSubject == null)
                        {
                            throw new BusinessException("ClassSubject_label_NotExist");
                        }
                        // Kiem tra neu mon nay la mon tinh diem co he so = 0 (khong co he so) trong ky thi khong xem cho viec tong ket diem va xet hoc luc
                        // ky 1
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            // He so mon hoc trong ky 1
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                && (!classSubject.FirstSemesterCoefficient.HasValue || classSubject.FirstSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            // He so mon hoc trong ky 2
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                && (!classSubject.SecondSemesterCoefficient.HasValue || classSubject.SecondSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }
                        // Gan vao danh sach diem tinh trung binh mon hay la mon cong diem
                        if (SummedUpRecord.PupilID.Equals(PupilRanking.PupilID))
                        {
                            if (classSubject != null && classSubject.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_PRIORITIZE)
                            {
                                lstPupilRankingOfPupil_Voluntary.Add(SummedUpRecord);
                            }
                            else
                            {
                                lstPupilRankingOfPupil_Mandatory.Add(SummedUpRecord);
                            }
                        }
                    }

                    // Neu khong co thong tin diem thi bo qua
                    if (lstPupilRankingOfPupil_Mandatory.Count == 0)
                    {
                        continue;
                    }

                    // Tinh diem
                    decimal? AverageMark = this.CaculatorAverageSubject(lstPupilRankingOfPupil_Mandatory, lstSubject, Semester, lstRegisterSubjectBO);

                    // Tinh them diem trung binh trong danh sach con lai
                    foreach (SummedUpRecord SummedUpRecord in lstPupilRankingOfPupil_Voluntary)
                    {
                        if (SummedUpRecord.SummedUpMark.HasValue)
                        {
                            decimal SummedUpMark = SummedUpRecord.SummedUpMark.Value;
                            if (SummedUpMark >= GlobalConstants.EXCELLENT_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_EXCELLENT_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.GOOD_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_GOOD_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_NORMAL_MARK;
                            }
                        }
                    }

                    if (AverageMark > GlobalConstants.MAX_MARK)
                    {
                        AverageMark = GlobalConstants.MAX_MARK;
                    }

                    // Lay thong tin cua xep loai Capacity
                    int? CapacityID = null;
                    if (AverageMark != null)
                    {
                        CapacityID = this.GetCapacityTypeByMark(AverageMark, lstPupilRankingOfPupil_Mandatory, lstSubject, SchoolID, traningType, lstRegisterSubjectBO);
                    }

                    // Xep hang hoc sinh
                    // Lay thong tin xep hang
                    if (CapacityID.HasValue)
                    {
                        PupilRanking pr = new PupilRanking();
                        bool isNotRank = false;
                        int ConductLevelID_Tot = 0;
                        if (isGDTX)
                        {
                            var poc = listPOC.FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                            isNotRank = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, poc.PupilLearningType, poc.BirthDate);
                            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                            {
                                ConductLevelID_Tot = SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY;
                            }
                            else if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            {
                                ConductLevelID_Tot = SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY;
                            }
                            if (isNotRank)
                            {
                                listPupilIDNotRankConduct.Add(poc.PupilID);
                            }
                        }
                        pr.PupilID = PupilRanking.PupilID;
                        pr.ClassID = PupilRanking.ClassID;
                        pr.SchoolID = PupilRanking.SchoolID;
                        pr.AcademicYearID = PupilRanking.AcademicYearID;
                        pr.CreatedAcademicYear = academicYear.Year;
                        pr.Last2digitNumberSchool = PupilRanking.SchoolID % 100;
                        pr.Semester = PupilRanking.Semester;
                        pr.AverageMark = AverageMark;
                        pr.CapacityLevelID = CapacityID;
                        pr.ConductLevelID = isNotRank ? ConductLevelID_Tot : PupilRanking.ConductLevelID;
                        pr.PeriodID = PeriodID;
                        pr.EducationLevelID = EducationLevelID;
                        pr.RankingDate = DateTime.Now;
                        pr.MSourcedb = AutoMark;
                        ListPupilRanking4Save.Add(pr);
                    }
                }

                if (ListPupilRanking4Save.Count() > 0)
                {
                    if ((academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT
                        || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT))
                    {
                        if ((ListPupilRanking4Save.Any(o => o.ConductLevelID.HasValue)))
                        {

                            List<PupilRanking> ListPupilRankingConduct = ListPupilRanking4Save.Where(o => o.ConductLevelID.HasValue).ToList();
                            this.Rank(academicYear.RankingCriteria, ListPupilRankingConduct);
                            ListPupilRanking4Save.RemoveAll(o => o.ConductLevelID.HasValue);
                            ListPupilRanking4Save.AddRange(ListPupilRankingConduct);
                        }
                    }
                    else
                    {
                        // Neu ko lien quan den hanh kiem thi chi can co diem trung binh la du xep hang
                        this.Rank(academicYear.RankingCriteria, ListPupilRanking4Save);
                    }

                    foreach (PupilRanking item in ListPupilRanking4Save)
                    {

                        PupilRanking PupilRankingUpdated = listPupilRankingByClass.Where(o => o.PupilID == item.PupilID).FirstOrDefault();
                        if (PupilRankingUpdated != null)
                        {
                            // Neu da co thong tin tu truoc
                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && listPupilRetestID.Contains(item.PupilID) && isShowRetetResult)
                            {
                                // Neu thuoc danh sach thi lai
                                // Neu da co diem thi lai thi chi cap nhat diem thi lai
                                PupilRanking prBackTraining = lstPupilRankingRe.Where(o => o.PupilID == item.PupilID).FirstOrDefault();
                                if (prBackTraining != null)
                                {
                                    // Neu cp diem thi lai roi
                                    prBackTraining.AverageMark = item.AverageMark;
                                    prBackTraining.CapacityLevelID = item.CapacityLevelID;
                                    prBackTraining.RankingDate = DateTime.Now;
                                    prBackTraining.Rank = item.Rank;
                                    prBackTraining.MSourcedb = AutoMark;
                                    base.Update(prBackTraining);
                                }
                                else
                                {
                                    PupilRanking prRetest = new PupilRanking();
                                    prRetest.PupilID = item.PupilID;
                                    prRetest.ClassID = item.ClassID;
                                    prRetest.SchoolID = item.SchoolID;
                                    prRetest.AcademicYearID = item.AcademicYearID;
                                    prRetest.CreatedAcademicYear = academicYear.Year;
                                    prRetest.Last2digitNumberSchool = item.SchoolID % 100;
                                    prRetest.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                                    prRetest.AverageMark = item.AverageMark;
                                    prRetest.CapacityLevelID = item.CapacityLevelID;
                                    prRetest.ConductLevelID = item.ConductLevelID;
                                    prRetest.PeriodID = PeriodID;
                                    prRetest.EducationLevelID = EducationLevelID;
                                    prRetest.RankingDate = DateTime.Now;
                                    prRetest.MSourcedb = AutoMark;
                                    base.Insert(prRetest);
                                }
                            }
                            else
                            {
                                // Neu khong thuoc danh sach thi lai thi cap nhat vao diem ca nam
                                PupilRankingUpdated.AverageMark = item.AverageMark;
                                PupilRankingUpdated.CapacityLevelID = item.CapacityLevelID;
                                PupilRankingUpdated.RankingDate = DateTime.Now;
                                PupilRankingUpdated.Rank = item.Rank;
                                PupilRankingUpdated.MSourcedb = AutoMark;
                                base.Update(PupilRankingUpdated);
                            }
                        }
                        else
                        {
                            // Neu khong thuoc dien xep loai hanh kiem thi cap nhat lai hanh kiem la null
                            if (listPupilIDNotRankConduct.Contains(item.PupilID))
                            {
                                item.MSourcedb = AutoMark;
                                item.ConductLevelID = null;
                            }
                            base.Insert(item);
                        }
                    }
                }
                this.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// RankingPupilOfClassNotRank: chi thuc hien tong ket, ko xep hang
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstSummedUpRecord"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public void RankingPupilOfClassNotRank(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isPassRetest = false, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            try
            {
                if (lstSummedUpRecord == null || lstSummedUpRecord.Count == 0)
                    throw new BusinessException("PupilRanking_Label_AllPupilNotMark");
                if (lstPupilRanking == null || lstPupilRanking.Count == 0)
                    return;
                SetAutoDetectChangesEnabled(false);
                SummedUpRecord firstSum = lstSummedUpRecord[0];

                int schoolID = firstSum.SchoolID;
                int academicYearID = firstSum.AcademicYearID;
                int classID = firstSum.ClassID;
                int semester = firstSum.Semester.Value;
                int modSchoolID = schoolID % 100;
                // Kiem tra neu he so mon cua lop hoc ko co thi bao loi
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.FirstSemesterCoefficient.HasValue && o.FirstSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }
                else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.SecondSemesterCoefficient.HasValue && o.SecondSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }

                #region Validate du lieu dau vao

                var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               where poc.ClassID == classID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && pp.IsActive
                                && poc.AcademicYearID == academicYearID
                               select new
                               {
                                   CurrentClassID = poc.ClassID,
                                   CurrentAcademicYearID = poc.AcademicYearID,
                                   poc.PupilID,
                                   poc.Status
                               })
                    .ToList();
                List<ExemptedSubject> listExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semester).ToList();
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);

                // Kiem tra nam hoc hien tai
                if (academicYear == null || !AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("ClassMovement_Validate_IsNotCurrentYear");

                /*if (PeriodID != null)
                {
                    bool PeriodSemester = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                      new Dictionary<string, object>()
                    {
                        {"Semester", semester},
                        {"PeriodDeclarationID",PeriodID.Value}
                    }, null);
                    if (!PeriodSemester)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }*/

                //SchoolID, AcademicYearID: not compatible(AcademicYearID)
                if (academicYear.SchoolID != schoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //neu ton tai mot hoc sinh duoc mien giam thi bao loi
                if (listExempteds.Any(u => lstSummedUpRecord.Any(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID)))
                    throw new BusinessException("SummedUpRecord_Labal_ErrMGSubject");

                foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
                {
                    ValidationMetadata.ValidateObject(SummedUpRecord);
                    if (!lstSubject.Any(u => u.ClassID == SummedUpRecord.ClassID && u.SubjectID == SummedUpRecord.SubjectID))
                        throw new BusinessException("Common_Validate_NotCompatible");

                    var poc = listPOC.FirstOrDefault(u => u.PupilID == SummedUpRecord.PupilID);
                    if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        throw new BusinessException("ClassMovement_Validate_PupilNotWorking");

                    if (poc.CurrentClassID != SummedUpRecord.ClassID)
                        throw new BusinessException("Common_Validate_NotCompatible");

                    if (poc.CurrentAcademicYearID != SummedUpRecord.AcademicYearID)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }
                #endregion
                // Process
                // Lay 2 danh sach tu 2 danh sach dau vao join qua dieu kien pupilID            
                List<int> listCheckedPupilID = new List<int>();
                PupilRanking firstRanking = lstPupilRanking[0];
                int EducationLevelID = firstRanking.EducationLevelID;

                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolID);
                TrainingType traningType = schoolProfile.TrainingType;

                // Lay du lieu trong PupilRanking theo truong, nam hoc, ky, dot neu co truoc
                Dictionary<string, object> dicRanking = new Dictionary<string, object>();
                dicRanking["AcademicYearID"] = academicYearID;
                dicRanking["ClassID"] = classID;
                dicRanking["Semester"] = semester;
                if (PeriodID.HasValue)
                {
                    dicRanking["PeriodID"] = PeriodID.Value;
                }
                else
                {
                    dicRanking["PeriodID"] = null;
                }
                List<PupilRanking> listPupilRankingByClass = this.SearchBySchool(schoolID, dicRanking).ToList();
                List<PupilRanking> lstPupilRankingRe = new List<PupilRanking>();

                if (!PeriodID.HasValue)
                {
                    listPupilRankingByClass = listPupilRankingByClass.Where(o => !o.PeriodID.HasValue).ToList();
                }
                List<int> listPupilRetestID = listPupilRankingByClass.Where(o => o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                   .Select(o => o.PupilID).ToList();
                //Neu hoc ky la ca nam thi lay ra cac ban ghi thi lai, ren luyen lai de cap nhat.
                // Lay danh sach hoc sinh da co nhap diem thi lai roi thi moi tinh vao truong hop nay
                // Neu chi moi xep loai la thi lai ma chua nhap diem thi lai thi van cap nhat vao ban ghi ca nam

                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    if (listPupilRetestID.Count > 0)
                    {
                        dicRanking["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                        lstPupilRankingRe = this.Search(dicRanking).ToList();
                        List<int> listPupilIDInputMarkRetest = SummedUpRecordBusiness.SearchBySchool(schoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID", academicYearID},
                    {"ClassID", classID}
                }).Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL && !o.PeriodID.HasValue).Select(o => o.PupilID).ToList();
                        listPupilRetestID.RemoveAll(o => !listPupilIDInputMarkRetest.Contains(o));
                    }
                }
                Dictionary<string, ClassSubject> dicClassSubject = new Dictionary<string, ClassSubject>();
                List<SummedUpRecord> lstSummedUpRecordCheck = new List<SummedUpRecord>();
                foreach (PupilRanking PupilRanking in lstPupilRanking)
                {
                    List<SummedUpRecord> lstPupilRankingOfPupil_Mandatory = new List<SummedUpRecord>();
                    List<SummedUpRecord> lstPupilRankingOfPupil_Voluntary = new List<SummedUpRecord>();
                    if (listCheckedPupilID.Contains(PupilRanking.PupilID))
                    {
                        continue;
                    }
                    listCheckedPupilID.Add(PupilRanking.PupilID);
                    //Sua lai vong lap
                    lstSummedUpRecordCheck = lstSummedUpRecord.Where(s => s.PupilID == PupilRanking.PupilID).ToList();
                    if (lstSummedUpRecordCheck == null || lstSummedUpRecordCheck.Count == 0)
                    {
                        continue;
                    }

                    foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecordCheck)
                    {
                        string key = SummedUpRecord.ClassID + "_" + SummedUpRecord.SubjectID;
                        // Lay mon hoc cua lop
                        ClassSubject classSubject = null;
                        if (dicClassSubject.ContainsKey(key))
                        {
                            classSubject = dicClassSubject[key];
                        }
                        else
                        {
                            classSubject = lstSubject.FirstOrDefault(u => u.ClassID == SummedUpRecord.ClassID && u.SubjectID == SummedUpRecord.SubjectID);
                            dicClassSubject[key] = classSubject;
                        }
                        if (classSubject == null)
                        {
                            throw new BusinessException("Không tồn tại môn học của lớp");
                        }
                        // Kiem tra neu mon nay khong co he so = 0 trong ky thi khong xem cho viec tong ket diem va xet hoc luc
                        // ky 1
                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            // He so mon hoc trong ky 1
                            // Chi xet cho mon tinh diem
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            && (!classSubject.FirstSemesterCoefficient.HasValue || classSubject.FirstSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            // He so mon hoc trong ky 2
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            && (!classSubject.SecondSemesterCoefficient.HasValue || classSubject.SecondSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }

                        // Gan vao danh sach diem tinh trung binh mon hay la mon cong uu tien
                        if (classSubject != null && classSubject.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_PRIORITIZE)
                        {
                            lstPupilRankingOfPupil_Voluntary.Add(SummedUpRecord);
                        }
                        else
                        {
                            lstPupilRankingOfPupil_Mandatory.Add(SummedUpRecord);
                        }
                    }

                    // Neu khong co thong tin diem thi bo qua
                    if (lstPupilRankingOfPupil_Mandatory.Count == 0)
                    {
                        continue;
                    }

                    // Tinh diem
                    decimal? AverageMark = this.CaculatorAverageSubject(lstPupilRankingOfPupil_Mandatory, lstSubject, semester, lstRegisterSubjectBO);

                    // Tinh them diem trung binh trong danh sach con lai
                    foreach (SummedUpRecord SummedUpRecord in lstPupilRankingOfPupil_Voluntary)
                    {
                        if (SummedUpRecord.SummedUpMark.HasValue)
                        {
                            decimal SummedUpMark = SummedUpRecord.SummedUpMark.Value;
                            if (SummedUpMark >= GlobalConstants.EXCELLENT_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_EXCELLENT_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.GOOD_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_GOOD_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_NORMAL_MARK;
                            }
                        }
                    }

                    if (AverageMark > GlobalConstants.MAX_MARK)
                    {
                        AverageMark = GlobalConstants.MAX_MARK;
                    }

                    // Lay thong tin cua xep loai Capacity
                    int? CapacityID = null;
                    if (AverageMark.HasValue)
                    {
                        CapacityID = this.GetCapacityTypeByMark(AverageMark, lstPupilRankingOfPupil_Mandatory, lstSubject, schoolID, traningType, lstRegisterSubjectBO);
                    }

                    // Luu du lieu vao bang PupilRanking
                    if (CapacityID.HasValue)
                    {
                        PupilRanking PupilRankingUpdated = listPupilRankingByClass.FirstOrDefault(o => o.PupilID == PupilRanking.PupilID);
                        if (PupilRankingUpdated != null) // thuc hien update
                        {
                            // Neu da co thong tin tu truoc
                            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && !isPassRetest && listPupilRetestID.Contains(PupilRanking.PupilID))
                            {
                                // Neu thuoc danh sach thi lai
                                // Neu da co diem thi lai thi chi cap nhat diem thi lai
                                PupilRanking prBackTraining = lstPupilRankingRe.FirstOrDefault(o => o.PupilID == PupilRanking.PupilID);
                                if (prBackTraining != null)
                                {
                                    // Neu cp diem thi lai roi
                                    prBackTraining.AverageMark = AverageMark;
                                    prBackTraining.CapacityLevelID = CapacityID;
                                    prBackTraining.RankingDate = DateTime.Now;
                                    prBackTraining.Rank = PupilRanking.Rank;
                                    base.Update(prBackTraining);
                                }
                                else
                                {
                                    PupilRanking prRetest = new PupilRanking();
                                    prRetest.PupilID = PupilRanking.PupilID;
                                    prRetest.ClassID = PupilRanking.ClassID;
                                    prRetest.SchoolID = PupilRanking.SchoolID;
                                    prRetest.AcademicYearID = PupilRanking.AcademicYearID;
                                    prRetest.CreatedAcademicYear = academicYear.Year;
                                    prRetest.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                                    prRetest.AverageMark = AverageMark;
                                    prRetest.CapacityLevelID = CapacityID;
                                    prRetest.ConductLevelID = PupilRanking.ConductLevelID;
                                    prRetest.PeriodID = PeriodID;
                                    prRetest.EducationLevelID = EducationLevelID;
                                    prRetest.RankingDate = DateTime.Now;
                                    prRetest.Rank = PupilRanking.Rank;
                                    prRetest.Last2digitNumberSchool = modSchoolID;
                                    prRetest.MSourcedb = "1";
                                    base.Insert(prRetest);
                                }
                            }
                            else
                            {
                                // Neu khong thuoc danh sach thi lai thi cap nhat vao diem ca nam
                                PupilRankingUpdated.AverageMark = AverageMark;
                                PupilRankingUpdated.CapacityLevelID = CapacityID;
                                PupilRankingUpdated.RankingDate = DateTime.Now;
                                PupilRankingUpdated.Rank = PupilRanking.Rank;
                                PupilRankingUpdated.MSourcedb = "1";
                                base.Update(PupilRankingUpdated);
                            }
                        }
                        else // thuc hien insert
                        {
                            PupilRanking pr = new PupilRanking();
                            pr.PupilID = PupilRanking.PupilID;
                            pr.ClassID = PupilRanking.ClassID;
                            pr.SchoolID = PupilRanking.SchoolID;
                            pr.AcademicYearID = PupilRanking.AcademicYearID;
                            pr.CreatedAcademicYear = academicYear.Year;
                            pr.Last2digitNumberSchool = modSchoolID;
                            pr.Semester = PupilRanking.Semester;
                            pr.AverageMark = AverageMark;
                            pr.CapacityLevelID = CapacityID;
                            pr.PeriodID = PeriodID;
                            pr.EducationLevelID = EducationLevelID;
                            pr.RankingDate = DateTime.Now;
                            pr.MSourcedb = "1";
                            this.Insert(pr);
                        }
                    }
                }
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Lay xep loai hoc luc chi theo mon tinh diem va chua xet den nang bac - QuangLM
        /// </summary>
        /// <param name="averageMark"></param>
        /// <param name="ListSummedUpRecord"></param>
        /// <returns></returns>
        public int GetCapacityTypeByOnlyMark(decimal? averageMark, List<SummedUpRecord> lstMandatoryMark, List<ClassSubject> lstClassSubject, bool isGDTX, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO)
        {
            if (lstMandatoryMark == null || lstMandatoryMark.Count == 0)
            {
                return 0;
            }
            Func<ClassSubject, bool> IsSpecializedSubject = u => u != null && u.IsSpecializedSubject.HasValue && u.IsSpecializedSubject.Value;
            // Bổ sung nghiệp vụ GDTX
            int capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
            bool continueToCheck = true;
            int pupilID = lstMandatoryMark.Select(p => p.PupilID).FirstOrDefault();
            #region Truong GDTX
            if (isGDTX)
            {
                // TODO GDTX
                /*•	CapacityID = CAPACITY_TYPE_EXCELLENT nếu:
                Điểm trung bình các môn học từ 8.0 trở lên (AverageMark đợt đang xét >= 8.0), trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 8.0 trở lên 
                (SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 8.0); 
                 	Không có môn học nào điểm trung bình dưới 6.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 6.5) */
                if (continueToCheck
                    && averageMark >= GlobalConstants.EXCELLENT_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.EXCELLENT_MARK)) //Không có môn chính hoặc phải Có môn chính trên 8
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.GOOD_MARK) //Không có điểm trung bình dưới 6.5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_EXCELLENT;
                    continueToCheck = false;
                }
                /*•	CapacityID = CAPACITY_TYPE_GOOD nếu:
                	Điểm trung bình các môn học từ 6.5 trở lên (AverageMark đợt đang xét >= 6.5), trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 6.5 trở lên 
                (SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 6.5); 
                	Không có môn học nào điểm trung bình dưới 5.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 5.0);*/
                if (continueToCheck
                    && averageMark >= GlobalConstants.GOOD_MARK //Điểm trung bình các môn học từ 6.5 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.GOOD_MARK)) //Không có môn chính hoặc phải Có môn chính trên 6.5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.NORMAL_MARK) //Không có điểm trung bình dưới 5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_GOOD;
                    continueToCheck = false;
                }
                /*•	CapacityID = CAPACITY_TYPE_NORMAL nếu:
                	Điểm trung bình các môn học từ 5.0 trở lên (AverageMark đợt đang xét >= 5.0), 
                trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 5.0 trở lên 
                (SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 5.0); 
                	Không có môn học nào điểm trung bình dưới 3.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 3.5);*/
                if (continueToCheck
                    && averageMark >= GlobalConstants.NORMAL_MARK //Điểm trung bình các môn học từ 5.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.NORMAL_MARK)) //Không có môn chính hoặc phải Có môn chính tren 5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.WEAK_MARK) //Không có điểm trung bình dưới 3.5
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                    continueToCheck = false;
                }
                /*•	CapacityID = CAPACITY_TYPE_WEAK nếu:
                //	Điểm trung bình các môn từ 3.5 trở lên (AverageMark đợt đang xét >= 3.5), 
                //không có môn nào điểm trung bình dưới 2.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 2.0)*/
                if (continueToCheck
                    && averageMark >= GlobalConstants.WEAK_MARK //Điểm trung bình các môn học từ 3.5 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.WEAK_MARK)) //Không có môn chính hoặc phải Có môn chính tren 3.5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.POOR_MARK) //Không có điểm trung bình dưới 2                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                    continueToCheck = false;
                }

                if (continueToCheck)
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_POOR;
                    continueToCheck = false;
                }
            }
            #endregion
            #region Truong binh thuong
            else
            {
                //•	CapacityID = CAPACITY_TYPE_EXCELLENT nếu:
                //	Điểm trung bình các môn học từ 8.0 trở lên (AverageMark đợt đang xét >= 8.0), 
                //trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 8.0 trở lên 
                //(SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 8.0); 
                //riêng đối với học sinh lớp chuyên của trường THPT chuyên phải thêm điều kiện điểm trung bình môn chuyên từ 8,0 trở lên 
                //(SummedUpMark đợt đang xét của môn có IsSpecializedSubject (trong bảng ClassSubject) = 1 >= 8.0); 
                //	Không có môn học nào điểm trung bình dưới 6.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 6.5);
                //	Các môn học đánh giá bằng nhận xét đạt loại Đ (lstPupilRankingOfPupil_Mandatory [i].JudgementResult = “Đ”) --> Chuyen xuong xet o duoi
                if (continueToCheck
                    && averageMark >= GlobalConstants.EXCELLENT_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark.Value >= GlobalConstants.EXCELLENT_MARK)) //Không có môn chính hoặc phải Có môn chính trên 8
                    && (this.checkIsSpecialize(pupilID, lstRegisterSubjectBO, lstClassSubject, lstMandatoryMark, GlobalConstants.EXCELLENT_MARK)) //không có môn chuyên dưới 8
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.GOOD_MARK) //Không có điểm trung bình dưới 6.5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_EXCELLENT;
                    continueToCheck = false;
                }

                //•	CapacityID = CAPACITY_TYPE_GOOD nếu:
                //	Điểm trung bình các môn học từ 6.5 trở lên (AverageMark đợt đang xét >= 6.5), trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 6.5 trở lên 
                //(SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 6.5); 
                //riêng đối với học sinh lớp chuyên của trường THPT chuyên phải thêm điều kiện điểm trung bình môn chuyên từ 6.5 trở lên 
                //(SummedUpMark đợt đang xét của môn có IsSpecializedSubject (trong bảng ClassSubject) = 1 >= 6.5); 
                //	Không có môn học nào điểm trung bình dưới 5.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 5.0);
                //	Các môn học đánh giá bằng nhận xét đạt loại Đ (lstPupilRankingOfPupil_Mandatory [i].JudgementResult = “Đ”) --> Chuyen xuong xet o duoi
                if (continueToCheck
                    && averageMark >= GlobalConstants.GOOD_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.GOOD_MARK)) //Không có môn chính hoặc phải Có môn chính trên 6.5
                    && (this.checkIsSpecialize(pupilID, lstRegisterSubjectBO, lstClassSubject, lstMandatoryMark, GlobalConstants.GOOD_MARK)) //Là trường chuyên và không có môn chuyên dưới 6.5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.NORMAL_MARK) //Không có điểm trung bình dưới 5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_GOOD;
                    continueToCheck = false;
                }

                //•	CapacityID = CAPACITY_TYPE_NORMAL nếu:
                //	Điểm trung bình các môn học từ 5.0 trở lên (AverageMark đợt đang xét >= 5.0), 
                //trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 5.0 trở lên 
                //(SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 5.0); 
                //riêng đối với học sinh lớp chuyên của trường THPT chuyên phải thêm điều kiện điểm trung bình môn chuyên từ 5.0trở lên 
                //(SummedUpMark đợt đang xét của môn có IsSpecializedSubject (trong bảng ClassSubject) = 1 >= 5.0); 
                //	Không có môn học nào điểm trung bình dưới 3.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 3.5);
                //	Các môn học đánh giá bằng nhận xét đạt loại Đ (lstPupilRankingOfPupil_Mandatory [i].JudgementResult = “Đ”) --> Chuyen xuong xet o duoi
                if (continueToCheck
                    && averageMark >= GlobalConstants.NORMAL_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.NORMAL_MARK)) //Không có môn chính hoặc phải Có môn chính tren 5
                    && (this.checkIsSpecialize(pupilID, lstRegisterSubjectBO, lstClassSubject, lstMandatoryMark, GlobalConstants.NORMAL_MARK)) //Là trường chuyên và không có môn chuyên dưới 5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.WEAK_MARK) //Không có điểm trung bình dưới 3.5
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                    continueToCheck = false;
                }

                //•	CapacityID = CAPACITY_TYPE_WEAK nếu:
                //	Điểm trung bình các môn từ 3.5 trở lên (AverageMark đợt đang xét >= 3.5), 
                //không có môn nào điểm trung bình dưới 2.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 2.0)
                if (continueToCheck
                    && averageMark >= GlobalConstants.WEAK_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.POOR_MARK) //Không có điểm trung bình dưới 2                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                    continueToCheck = false;
                }

                if (continueToCheck)
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_POOR;
                    continueToCheck = false;
                }
                // Neu sau khi xet nang van bi loai kem thi giu nguyen
                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                {
                    return capacityID;
                }
            }
            #endregion
            return capacityID;
        }



        /// <summary>
        /// Get xep loai hoc luc cho hoc sinh
        /// </summary>
        /// <param name="AverageMark"></param>
        /// <param name="ListSummedUpRecord"></param>
        /// <returns></returns>
        public int GetCapacityTypeByMark(decimal? averageMark, List<SummedUpRecord> ListSummedUpRecord, List<ClassSubject> lstClassSubject, int schoolID, TrainingType traningType, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO)
        {
            // Lay thong tin mon hoc tu ID   
            SubjectCat objSubjectCat;
            List<SubjectCat> lstSubjectCat = lstClassSubject.Select(cs => cs.SubjectCat).ToList();
            foreach (SummedUpRecord s in ListSummedUpRecord)
            {
                if (s.SubjectCat == null)
                {
                    objSubjectCat = lstSubjectCat.Find(c => c.SubjectCatID == s.SubjectID);
                    s.SubjectCat = objSubjectCat;
                }
            }
            //Chiendd1: 17/08/2015, mon tang cuong khong tham gia tinh diem tong ket
            //Danh sach mon hoc tang cuong
            List<int> lstClassSubjectPlus = lstClassSubject.Where(c => c.SubjectIDIncrease.HasValue && c.SubjectIDIncrease > 0).Select(c => c.SubjectIDIncrease.Value).ToList();
            //Danh sach mon tinh diem
            List<int> lstSubjectMark = lstClassSubject.Where(s => s.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK).Select(s => s.SubjectID).ToList();
            //Danh sach mon nhan xet
            List<int> lstSubjectJudge = lstClassSubject.Where(s => s.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT).Select(s => s.SubjectID).ToList();
            // Sắp xếp lại danh sách điểm theo thứ thự giảm dần
            // Lấy n - 1 môn để xếp loại, môn cuối cùng thấp điểm nhất dùng để xem xét việc nâng hạng
            //Bo cac mon tang cuong
            List<SummedUpRecord> lstMandatoryMark = ListSummedUpRecord.Where(u => (lstSubjectMark != null && lstSubjectMark.Contains(u.SubjectID))
                                                                            && (lstClassSubjectPlus != null && !lstClassSubjectPlus.Contains(u.SubjectID)))
                .OrderByDescending(o => o.SummedUpMark)
                .ToList();
            //Chỉ lay cac mon tinh diem co he so >0 de xet hoc luc
            lstMandatoryMark = (from m in lstMandatoryMark
                                join cs in lstClassSubject on m.SubjectID equals cs.SubjectID
                                where cs.FirstSemesterCoefficient > 0 && cs.SecondSemesterCoefficient > 0
                                select m).ToList();

            // Chi lay n - 1 mon neu tat ca cac mon deu D
            List<SummedUpRecord> lstMandatoryJudge = ListSummedUpRecord.Where(u => (lstSubjectJudge != null && lstSubjectJudge.Contains(u.SubjectID))).ToList();

            decimal lowestMark = -1;
            // Mon hoc khai bao cho lop o mon thap nhat
            List<ClassSubject> lstClassSubjectLowest = new List<ClassSubject>();
            // Diem mon thap nhat
            List<SummedUpRecord> lstSummedUpRecordLowest = new List<SummedUpRecord>();
            // Co mon tin diem va mon nhan xet nao cung D
            int countCD = lstMandatoryJudge.Where(u => (!string.IsNullOrEmpty(u.ReTestJudgement) && u.ReTestJudgement == SystemParamsInFile.JUDGE_MARK_CD) ||
                (string.IsNullOrEmpty(u.ReTestJudgement) && u.JudgementResult != SystemParamsInFile.JUDGE_MARK_D)).Count();  // AnhVD 20140808 - cap nhat lay diem sau khi thi lai

            if (lstMandatoryMark.Count > 1 && countCD == 0)
            {
                lstClassSubjectLowest = lstClassSubject.Where(o => (o.IsSpecializedSubject.HasValue && o.IsSpecializedSubject.Value) || o.SubjectCat.IsCoreSubject).ToList();
                lstSummedUpRecordLowest = lstMandatoryMark.Where(u => lstClassSubjectLowest.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)).ToList();
                SummedUpRecord lastSu = lstMandatoryMark.Last();
                lowestMark = (lastSu.SummedUpMark.HasValue) ? lastSu.SummedUpMark.Value : -1;
                lstMandatoryMark.RemoveAt(lstMandatoryMark.Count - 1);
                if (!lstClassSubjectLowest.Any(o => o.SubjectID == lastSu.SubjectID && o.ClassID == lastSu.ClassID))
                {
                    lstSummedUpRecordLowest.Add(lastSu);
                    List<ClassSubject> lstTemp = lstClassSubject.Where(o => o.SubjectID == lastSu.SubjectID
                                                                        && o.ClassID == lastSu.ClassID
                                                                        && (lstClassSubjectPlus != null && !lstClassSubjectPlus.Contains(o.SubjectID))
                                                                        ).ToList();
                    lstClassSubjectLowest.AddRange(lstTemp);
                }
            }
            string type_GDTX = "GDTX";
            bool isGDTX = (traningType != null && traningType.Resolution == type_GDTX);
            int capacityID = lstMandatoryMark.Count > 0 ?
                this.GetCapacityTypeByOnlyMark(averageMark, lstMandatoryMark, lstClassSubject, isGDTX, lstRegisterSubjectBO) : 0;
            // Neu khong phai la GDTX thi xet them mon nhan xet truoc khi xet toi viec nang bac            
            #region Xet mon nhan xet cho truong khong phai GDTX
            if (!isGDTX)
            {
                // Neu co 2 mon nhan xet CD thi xep loai yeu, neu TBM cac mon tinh diem tot hon la diem yeu thi xep loai hoc luc la yeu
                if (countCD > 1)
                {
                    if (capacityID < SystemParamsInFile.CAPACITY_TYPE_WEAK)
                    {
                        capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                    }
                }
                else if (countCD == 1)
                {
                    // Neu sau khi xet theo mon tinh diem co loai xep hang tren trung binh thi moi tinh viec nang loai doi voi mon nhan xet chua dat
                    // Neu co mon nhan xet chua dat thi moi xet
                    if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT || capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    {
                        capacityID = SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                    }
                    else
                    {
                        // Neu TBM tinh diem tot hon loai yeu thi xep loai hoc luc la yeu
                        if (capacityID < SystemParamsInFile.CAPACITY_TYPE_WEAK)
                        {
                            capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                        }
                    }
                }
            }
            #endregion
            // Xet viec nang diem neu co mon tinh diem
            if (lowestMark > -1)
            {
                // Hoc luc cua phan tu cuoi cung
                int lowestCapacityID = this.GetCapacityTypeByOnlyMark(averageMark, lstSummedUpRecordLowest, lstClassSubjectLowest, isGDTX, lstRegisterSubjectBO);
                // Xet nang bac
                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT
                    || capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD
                    || (isGDTX && capacityID == SystemParamsInFile.CAPACITY_TYPE_NORMAL))
                {
                    capacityID = this.GetUpCapacity(capacityID, lowestCapacityID, isGDTX);
                }
                else
                {
                    // Neu khong thuoc dang nang bac thi lay bac cua diem thap nhat neu hoc luc diem thap nha kem hon cai hien tai
                    if (capacityID < lowestCapacityID)
                    {
                        capacityID = lowestCapacityID;
                    }
                }
            }

            return capacityID;
        }


        private bool checkIsSpecialize(int pupilID, List<RegisterSubjectSpecializeBO> lstRegister, List<ClassSubject> lstClassSubject, List<SummedUpRecord> lstSummed, decimal equalMark)
        {
            ClassSubject objClassSubject = new ClassSubject();
            SummedUpRecord objSummed = new SummedUpRecord();
            int count = 0;
            bool check = true;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                objClassSubject = lstClassSubject[i];
                if (objClassSubject.IsCommenting == 1)
                {
                    continue;
                }
                objSummed = lstSummed.Where(p => p.SubjectID == objClassSubject.SubjectID).FirstOrDefault();
                if (objSummed == null)
                {
                    continue;
                }
                count = lstRegister.Where(p => p.PupilID == pupilID && p.ClassID == objClassSubject.ClassID && p.SubjectID == objClassSubject.SubjectID).Count();
                if (objClassSubject.IsSpecializedSubject.HasValue && objClassSubject.IsSpecializedSubject.Value && objSummed.SummedUpMark < equalMark && count > 0)
                {
                    check = false;
                    break;
                }
                else
                {
                    check = true;
                }
            }
            return check;
        }



        /// <summary>
        /// Xet nang bac hoc luc cho hoc sinh - Quanglm
        /// </summary>
        /// <param name="capacityID"></param>
        /// <param name="lowestMark"></param>
        /// <returns></returns>
        public int GetUpCapacity(int capacityID, int lowestCapacityID, bool isGDTX)
        {
            //Neu la truong GDTX thi nang bac nhu sau
            //Nếu capacityID giỏi and kết quả của một môn lam xuong TB thì CapacityID = CAPACITY_TYPE_GOOD
            //Nếu capacityID giỏi and kết quả của một môn lam xuong Yeu hoac kem thì CapacityID = CAPACITY_TYPE_NORMAL
            //Nếu capacityID khá and kết quả của một môn lam xuong Yeu thì CapacityID = CAPACITY_TYPE_NORMAL  
            //Nếu capacityID khá hoac TB and kết quả của một môn lam xuong Kem thì CapacityID = CAPACITY_TYPE_WEAK            
            if (isGDTX)
            {
                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT
                    && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_GOOD;
                }

                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT
                    && (lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_WEAK || lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_POOR))
                {
                    return SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                }

                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD
                    && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                }

                if ((capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD || capacityID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                    && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_WEAK;
                }
            }
            else
            {
                // Truong binh thuong nang bac nhu sau
                //Nếu capacityID giỏi and kết quả của một môn lam xuong TB thì CapacityID = CAPACITY_TYPE_GOOD
                //Nếu capacityID giỏi and kết quả của một môn lam xuong Yeu thì CapacityID = CAPACITY_TYPE_NORMAL
                //Nếu capacityID giỏi and kết quả của một môn lam xuong Kem thì CapacityID = CAPACITY_TYPE_WEAK  
                //Nếu capacityID khá and kết quả của một môn lam xuong Yeu thì CapacityID = CAPACITY_TYPE_NORMAL
                //Nếu capacityID khá and kết quả của một môn lam xuong Kem thì CapacityID = CAPACITY_TYPE_WEAK            
                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_NORMAL)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_GOOD;
                }

                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                }

                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_WEAK;
                }

                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD
                    && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_WEAK)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                }

                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD
                    && lowestCapacityID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                {
                    return SystemParamsInFile.CAPACITY_TYPE_WEAK;
                }
            }
            // Neu van khong thuoc truong hop nang bac thi gan bang hoc luc kem hon
            if (capacityID < lowestCapacityID)
            {
                return lowestCapacityID;
            }
            return capacityID;
        }

        /// <summary>
        /// Tinh diem trung binh cho hoc sinh
        /// </summary>
        /// <param name="ListSummedUpRecord"></param>
        /// <returns></returns>
        public decimal? CaculatorAverageSubject(List<SummedUpRecord> ListSummedUpRecord, List<ClassSubject> lstSubject, int semester, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            if (ListSummedUpRecord == null || ListSummedUpRecord.Count == 0)
            {
                return 0;
            }

            //Chiendd1: 17/08/2015, mon tang cuong khong tham gia tinh diem tong ket
            //Danh sach mon hoc tang cuong
            List<int> lstClassSubjectPlus = lstSubject.Where(c => c.SubjectIDIncrease.HasValue && c.SubjectIDIncrease > 0).Select(c => c.SubjectIDIncrease.Value).ToList();
            // Lay thong tin he so cua mon hoc theo mon hoc va hoc ky
            Dictionary<int, int> dicCof = new Dictionary<int, int>();
            ClassSubject objClassSubject = null;
            foreach (ClassSubject cs in lstSubject)
            {
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    //Neu la mon tang cuong thi set he so =0 de khong tham gia tinh TBM
                    if (lstClassSubjectPlus != null && lstClassSubjectPlus.Contains(cs.SubjectID))
                    {
                        dicCof[cs.SubjectID] = 0;
                    }
                    else
                    {
                        dicCof[cs.SubjectID] = cs.FirstSemesterCoefficient.HasValue ? cs.FirstSemesterCoefficient.Value : 0;
                    }
                }
                else
                {
                    if (lstClassSubjectPlus != null && lstClassSubjectPlus.Contains(cs.SubjectID))
                    {
                        dicCof[cs.SubjectID] = 0;
                    }
                    else
                    {
                        dicCof[cs.SubjectID] = cs.SecondSemesterCoefficient.HasValue ? cs.SecondSemesterCoefficient.Value : 0;
                    }
                }
            }
            // Tinh diem
            decimal sumMark = 0;
            int count = 0;
            int countPupil = 0;
            foreach (SummedUpRecord SummedUpRecord in ListSummedUpRecord)
            {
                //Lay thong tin he so diem, bo cac mon tang cuong
                int cof = 0;
                objClassSubject = lstSubject.Where(p => p.ClassID == SummedUpRecord.ClassID && p.SubjectID == SummedUpRecord.SubjectID).FirstOrDefault();
                //Neu la mon tang cuong thi set he so =0 de khong tham gia tinh tong ket
                if (objClassSubject.IsSpecializedSubject.HasValue && objClassSubject.IsSpecializedSubject.Value)
                {
                    countPupil = lstRegisterSubjectBO != null ? lstRegisterSubjectBO.Where(p => p.PupilID == SummedUpRecord.PupilID && p.SubjectID == SummedUpRecord.SubjectID && p.SubjectTypeID == GlobalConstants.APPLIED_SUBJECT_SPECIALIZE).Count() : 0;
                    if (countPupil > 0 && dicCof.ContainsKey(SummedUpRecord.SubjectID))
                    {
                        cof = dicCof[SummedUpRecord.SubjectID];
                    }
                    else
                    {
                        cof = 1;
                    }
                }
                else if (objClassSubject.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE
                        || objClassSubject.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                         || objClassSubject.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                {
                    countPupil = lstRegisterSubjectBO != null ? lstRegisterSubjectBO.Where(p => p.PupilID == SummedUpRecord.PupilID && p.SubjectID == SummedUpRecord.SubjectID && p.SubjectTypeID == objClassSubject.AppliedType && (semester > GlobalConstants.SEMESTER_OF_YEAR_SECOND ? (p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST || p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) : p.SemesterID == semester)).Count() : 0;
                    if (countPupil > 0 && dicCof.ContainsKey(SummedUpRecord.SubjectID))
                    {
                        cof = dicCof[SummedUpRecord.SubjectID];
                    }
                    else
                    {
                        cof = 0;
                    }
                }
                else if (dicCof.ContainsKey(SummedUpRecord.SubjectID))
                {
                    cof = dicCof[SummedUpRecord.SubjectID];
                }
                // Lay diem
                if (SummedUpRecord.ReTestMark.HasValue)
                {
                    sumMark += cof * SummedUpRecord.ReTestMark.Value;
                    count = count + cof;
                }
                else
                {
                    if (SummedUpRecord.SummedUpMark.HasValue)
                    {
                        sumMark += cof * SummedUpRecord.SummedUpMark.Value;
                        count = count + cof;
                    }
                }
            }
            if (count == 0)
            {
                return null;
            }
            return decimal.Round(sumMark / count, 1, MidpointRounding.AwayFromZero);
        }


        public bool IsAppliedTypeSubject(List<ClassSubject> lstClassSubject, int ClassID, int SubjectID)
        {
            //Dictionary<string, object> dic = new Dictionary<string, object>();
            //dic["SubjectID"] = SubjectID;
            //IQueryable<ClassSubject> ListClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, dic)
            //   .Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK);
            int count = lstClassSubject.Count(c => c.ClassID == ClassID && c.SubjectID == SubjectID && c.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_PRIORITIZE);
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Từ xếp loại giáo dục và hạnh kiểm của học sinh => danh hiệu và thuộc diện
        /// namdv3
        /// </summary>
        /// <param name="lstPupilRankingBO"></param>
        public void ClassifyPupilPrimary(List<PupilRankingBO> lstPupilRankingBO)
        {
            if (lstPupilRankingBO == null || lstPupilRankingBO.Count == 0)
            {
                return;
            }
            //Cac phan tu trong list co cung ClassID nen lay ban ghi dau tien de tim kiem cac danh sach ban dau.
            PupilRankingBO pupilRankingBO = lstPupilRankingBO[0];
            List<SummedUpRecord> lstSummedUpRecord_Voluntary = new List<SummedUpRecord>();
            //Danh sach mon hoc tu chon cong diem
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = pupilRankingBO.AcademicYearID;
            search["ClassID"] = pupilRankingBO.ClassID;
            IQueryable<ClassSubject> lstSubjectVoluntaryPlus = ClassSubjectBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, search).Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK);
            List<int> lstSubjectID = lstSubjectVoluntaryPlus.Select(o => o.SubjectID).ToList();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
            dic["ClassID"] = pupilRankingBO.ClassID;
            dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL; //TungNT
            //List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).Where(o => o.PeriodID == null && lstSubjectID.Contains(o.SubjectID)).ToList();
            List<SummedUpRecord> lstSummedUpRecord_Mark = new List<SummedUpRecord>();
            List<SummedUpRecord> lstSummedUpRecord_Judge = new List<SummedUpRecord>();
            lstSummedUpRecord_Mark = lstSummedUpRecord_Voluntary.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK).ToList();
            lstSummedUpRecord_Judge = lstSummedUpRecord_Voluntary.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE).ToList();
            List<SummedUpRecord> lstSummedUpRecord_Voluntary_Type_Mark = new List<SummedUpRecord>();
            List<SummedUpRecord> lstSummedUpRecord_Voluntary_Type_Judge = new List<SummedUpRecord>();
            EducationLevel educationLevel = EducationLevelBusiness.All.FirstOrDefault(o => o.EducationLevelID == pupilRankingBO.EducationLevelID);

            var HonourType1 = HonourAchivementTypeBusiness.All.FirstOrDefault(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_GOOD
                            && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL);
            var HonourType2 = HonourAchivementTypeBusiness.All.FirstOrDefault(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_EXCELLENT
                           && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL);

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
            dic["ClassID"] = pupilRankingBO.ClassID;
            dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).ToList();
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
            dic["ClassID"] = pupilRankingBO.ClassID;
            //dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).Where(o => o.PeriodID == null).ToList();
            //Thêm vào để check trường hợp học sinh thi lại thì thêm mới thêm một bản ghi
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
            dic["ClassID"] = pupilRankingBO.ClassID;
            List<PupilRetestRegistration> lstPupilRetest = PupilRetestRegistrationBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).ToList();
            if (lstPupilRankingBO != null && lstPupilRankingBO.Count() > 0)
            {
                //Step1: Lấy từng phần tử trong lstPupilToRank.

                foreach (var PupilRankingBO in lstPupilRankingBO)
                {
                    PupilRankingBO.HonourAchivementTypeID = null;
                    lstSummedUpRecord_Voluntary_Type_Mark = lstSummedUpRecord_Mark.Where(o => o.PupilID == PupilRankingBO.PupilID).ToList();
                    lstSummedUpRecord_Voluntary_Type_Judge = lstSummedUpRecord_Judge.Where(o => o.PupilID == PupilRankingBO.PupilID).ToList();
                    //Nếu xếp loại giáo dục Giỏi thì cập nhật danh hiệu là Học sinh giỏi
                    if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT)
                    {

                        if (lstSummedUpRecord_Voluntary_Type_Judge != null && lstSummedUpRecord_Voluntary_Type_Judge.Count > 0 && lstSummedUpRecord_Voluntary_Type_Mark.Count == 0)
                        {
                            if (lstSummedUpRecord_Voluntary_Type_Mark.All(o => o.SummedUpMark >= 9))
                            {
                                goto SetData;
                            }
                        }
                        else if (lstSummedUpRecord_Voluntary_Type_Mark != null && lstSummedUpRecord_Voluntary_Type_Mark.Count > 0 && lstSummedUpRecord_Voluntary_Type_Judge.Count == 0)
                        {
                            if (lstSummedUpRecord_Voluntary_Type_Judge.All(o => o.JudgementResult != "B"))
                            {
                                goto SetData;
                            }
                        }
                        else if (lstSummedUpRecord_Voluntary_Type_Mark.Count > 0 && lstSummedUpRecord_Voluntary_Type_Judge.Count > 0)
                        {
                            if (lstSummedUpRecord_Voluntary_Type_Mark.All(o => o.SummedUpMark >= 9) && lstSummedUpRecord_Voluntary_Type_Judge.All(o => o.JudgementResult != "B"))
                            {
                                goto SetData;
                            }
                        }
                        else if (lstSummedUpRecord_Voluntary_Type_Mark.Count == 0 && lstSummedUpRecord_Voluntary_Type_Judge.Count == 0)
                        {
                            goto SetData;
                        }
                        SetData:
                        {
                            if (HonourType2 != null)
                            {
                                PupilRankingBO.HonourAchivementTypeID = HonourType2.HonourAchivementTypeID;
                            }
                            if (educationLevel != null)
                            {
                                //Nếu EducationLevel(lstPupilToRank[i].EducationLevelID).IsLastYear = false 
                                //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_UPCLASS(1)
                                if (educationLevel.IsLastYear == false)
                                    //Không phải năm cuối cấp thì thuộc diện Lên lớp
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                //Nếu EducationLevel(lstPupilToRank[i].EducationLevelID).IsLastYear = true
                                //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_GRADUATION (5)
                                else if (educationLevel.IsLastYear == true)
                                    //Nếu là năm cuối cấp thì thuộc diện Hoàn thành chương trình tiểu học
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                            }
                        }
                    }
                    //Nếu xếp loại giáo dục Khá thì cập nhật danh hiệu là Học sinh tiên tiến
                    if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_GOOD)
                    //if (PupilRankingBO.CapacityType == GlobalConstants.CAPACITY_TYPE_GOOD)
                    {

                        if (lstSummedUpRecord_Voluntary_Type_Judge != null && lstSummedUpRecord_Voluntary_Type_Judge.Count > 0 && lstSummedUpRecord_Voluntary_Type_Mark.Count == 0)
                        {
                            if (lstSummedUpRecord_Voluntary_Type_Mark.All(o => o.SummedUpMark >= 7))
                            {
                                goto SetDataGood;
                            }
                        }
                        else if (lstSummedUpRecord_Voluntary_Type_Mark != null && lstSummedUpRecord_Voluntary_Type_Mark.Count > 0 && lstSummedUpRecord_Voluntary_Type_Judge.Count == 0)
                        {
                            if (lstSummedUpRecord_Voluntary_Type_Judge.All(o => o.JudgementResult != "B"))
                            {
                                goto SetDataGood;
                            }
                        }
                        else if (lstSummedUpRecord_Voluntary_Type_Mark.Count > 0 && lstSummedUpRecord_Voluntary_Type_Judge.Count > 0)
                        {
                            if (lstSummedUpRecord_Voluntary_Type_Mark.All(o => o.SummedUpMark >= 7) && lstSummedUpRecord_Voluntary_Type_Judge.All(o => o.JudgementResult != "B"))
                            {
                                goto SetDataGood;
                            }
                        }
                        else if (lstSummedUpRecord_Voluntary_Type_Mark.Count == 0 && lstSummedUpRecord_Voluntary_Type_Judge.Count == 0)
                        {
                            goto SetDataGood;
                        }
                        SetDataGood:
                        {
                            if (HonourType1 != null)
                            {
                                PupilRankingBO.HonourAchivementTypeID = HonourType1.HonourAchivementTypeID;
                            }
                            if (educationLevel != null)
                            {
                                //Nếu EducationLevel(lstPupilToRank[i].EducationLevelID).IsLastYear = false 
                                //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_UPCLASS(1)
                                if (educationLevel.IsLastYear == false)
                                    //Không phải năm cuối cấp thì thuộc diện Lên lớp
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                //Nếu EducationLevel(lstPupilToRank[i].EducationLevelID).IsLastYear = true
                                //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_GRADUATION (5)
                                else if (educationLevel.IsLastYear == true)
                                    //Nếu là năm cuối cấp thì thuộc diện Hoàn thành chương trình tiểu học
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                            }
                        }
                    }
                    //Nếu xếp loại giáo dục trung bình
                    if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_NORMAL)
                    {
                        if (educationLevel != null)
                        {
                            //Nếu EducationLevel(lstPupilToRank[i].EducationLevelID).IsLastYear = false 
                            //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_UPCLASS(1)
                            if (educationLevel.IsLastYear == false)
                                //Không phải năm cuối cấp thì thuộc diện Lên lớp
                                PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                            //Nếu EducationLevel(lstPupilToRank[i].EducationLevelID).IsLastYear = true
                            //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_GRADUATION (5)
                            else if (educationLevel.IsLastYear == true)
                                //Nếu là năm cuối cấp thì thuộc diện Hoàn thành chương trình tiểu học
                                PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                        }
                    }
                    //Nếu xếp loại giáo dục yếu
                    if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_WEAK)
                    {
                        //Nếu lstPupilToRank[i].CapacityLevelID = CAPACITY_TYPE_WEAK (4) and CheckNumberOfRetest(lstPupilToRank[i].AcademicYearID, lstPupilToRank[i].SchoolID, lstPupilToRank[i].PupilID, lstPupilToRank[i].ClassID) = False thì
                        var checknumberofretest = PupilRetestRegistrationBusiness.CheckNumberOfRetest(PupilRankingBO.AcademicYearID.Value, PupilRankingBO.SchoolID.Value, PupilRankingBO.ClassID.Value, PupilRankingBO.PupilID.Value);
                        //CheckNumberOfRetest(lstPupilToRank[i].AcademicYearID, lstPupilToRank[i].SchoolID, lstPupilToRank[i].PupilID, lstPupilToRank[i].ClassID) = False
                        //thì
                        if (!checknumberofretest)
                        {
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_RETEST;
                        }
                        //CheckNumberOfRetest(lstPupilToRank[i].AcademicYearID, lstPupilToRank[i].SchoolID, lstPupilToRank[i].PupilID, lstPupilToRank[i].ClassID) = true
                        //thì
                        else
                        {
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                        }
                    }
                    //Nếu hạnh kiểm chưa thực hiện đầy đủ
                    if (PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_CD && PupilRankingBO.CapacityLevelID != 0 && PupilRankingBO.CapacityLevelID != null)
                    {
                        if (PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                        {
                            //Học sinh có hạnh kiểm thực hiện chưa đầy đủ thì thuộc diện rèn luyện lại
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING;
                        }
                        //Nếu hạnh kiểm chưa thực hiện đầy đủ sau khi rèn luyện lại
                        if (PupilRankingBO.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                        {
                            //Học sinh có hạnh kiểm thực hiện chưa đầy đủ thì thuộc diện rèn luyện lại
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                        }
                    }

                    //Step2: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking nếu tìm thấy bản ghi.
                    var pr = lstPupilRanking.FirstOrDefault(o => o.PupilRankingID == PupilRankingBO.PupilRankingID);
                    var prr = lstPupilRetest.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);

                    if (pr != null && prr == null)
                    {
                        pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                        pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                        pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                        pr.IsCategory = true;
                        this.Update(pr);
                    }
                    else
                    {
                        //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                        PupilRanking prExist = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING);
                        if (prExist == null)
                        {
                            PupilRanking _pr = new PupilRanking();
                            _pr.PupilID = PupilRankingBO.PupilID.Value;
                            _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                            _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                            _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                            _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                            _pr.PeriodID = PupilRankingBO.PeriodID;
                            _pr.CreatedAcademicYear = PupilRankingBO.Year.Value;
                            _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value & 100;
                            _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            _pr.ClassID = PupilRankingBO.ClassID.Value;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            _pr.IsCategory = true;
                            this.Insert(_pr);
                        }
                        else
                        {
                            prExist.ConductLevelID = PupilRankingBO.ConductLevelID;
                            prExist.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            prExist.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            prExist.IsCategory = true;
                            this.Update(prExist);
                        }
                    }

                    //Step3: Nếu lstPupilToRank[i].HonourAchivementTypeID = null
                    //Thực hiện tìm kiếm trong bảng PupilEmulation theo PupilID, ClassID, SchoolID, AcademicYearID, Semester = 2.
                    var pupilemulation = lstPupilEmulation.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                    if (PupilRankingBO.HonourAchivementTypeID == null)
                    {
                        //Nếu tìm thấy pupilemulation thì xóa
                        if (pupilemulation != null)
                        {
                            PupilEmulationBusiness.Delete(pupilemulation.PupilEmulationID);
                        }
                    }
                    else if (PupilRankingBO.HonourAchivementTypeID != null)
                    {
                        //-	Nếu tìm thấyc pupilemulation thì cập nhật lại HonourAchivementTypeID và update
                        if (pupilemulation != null)
                        {
                            pupilemulation.HonourAchivementTypeID = PupilRankingBO.HonourAchivementTypeID.Value;
                            PupilEmulationBusiness.Update(pupilemulation);
                        }
                        //-	Nếu không tìm thấy pupilemulation thì insert pe vào bảng PupilEmulation
                        if (pupilemulation == null)
                        {
                            PupilEmulation pe = new PupilEmulation();
                            pe.PupilID = PupilRankingBO.PupilID.Value;
                            pe.ClassID = PupilRankingBO.ClassID.Value;
                            pe.SchoolID = PupilRankingBO.SchoolID.Value;
                            pe.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                            pe.Semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                            pe.HonourAchivementTypeID = PupilRankingBO.HonourAchivementTypeID.Value;
                            pe.Year = pupilRankingBO.Year.Value;
                            pe.Last2digitNumberSchool = UtilsBusiness.GetPartionId(PupilRankingBO.SchoolID.Value);
                            PupilEmulationBusiness.Insert(pe);
                        }
                    }
                }
            }
        }

        public void ClassifyPupilPrimaryv2(List<PupilRankingBO> lstPupilRankingBO, int Semester)
        {
            if (lstPupilRankingBO == null || lstPupilRankingBO.Count == 0)
            {
                return;
            }
            //Cac phan tu trong list co cung ClassID nen lay ban ghi dau tien de tim kiem cac danh sach ban dau.
            PupilRankingBO pupilRankingBO = lstPupilRankingBO[0];
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                EducationLevel educationLevel = EducationLevelBusiness.All.FirstOrDefault(o => o.EducationLevelID == pupilRankingBO.EducationLevelID);

                var HonourType1 = HonourAchivementTypeBusiness.All.FirstOrDefault(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_GOOD
                                && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL);
                var HonourType2 = HonourAchivementTypeBusiness.All.FirstOrDefault(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_EXCELLENT
                               && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
                dic["ClassID"] = pupilRankingBO.ClassID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).ToList();
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
                dic["ClassID"] = pupilRankingBO.ClassID;
                //dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).Where(o => o.PeriodID == null).ToList();
                //Thêm vào để check trường hợp học sinh thi lại thì thêm mới thêm một bản ghi
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
                dic["ClassID"] = pupilRankingBO.ClassID;
                List<PupilRetestRegistration> lstPupilRetest = PupilRetestRegistrationBusiness.SearchBySchool(pupilRankingBO.SchoolID.Value, dic).ToList();
                if (lstPupilRankingBO != null && lstPupilRankingBO.Count() > 0)
                {
                    //Step1: Lấy từng phần tử trong lstPupilToRank.

                    foreach (var PupilRankingBO in lstPupilRankingBO)
                    {
                        PupilRankingBO.HonourAchivementTypeID = null;
                        var prr = lstPupilRetest.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                        //Nếu xếp loại giáo dục Giỏi, không thi lại hay rèn luyện lại thì cập nhật danh hiệu là Học sinh giỏi
                        if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT && PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && prr == null)
                        {

                            if (HonourType2 != null)
                            {
                                PupilRankingBO.HonourAchivementTypeID = HonourType2.HonourAchivementTypeID;
                            }

                        }
                        //Nếu xếp loại giáo dục Khá, không thi lại hay rèn luyện lại thì cập nhật danh hiệu là Học sinh tiên tiến
                        if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_GOOD && PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && prr == null)
                        //if (PupilRankingBO.CapacityType == GlobalConstants.CAPACITY_TYPE_GOOD)
                        {

                            if (HonourType1 != null)
                            {
                                PupilRankingBO.HonourAchivementTypeID = HonourType1.HonourAchivementTypeID;
                            }
                        }
                        // Neu lop cuoi cap 1
                        if (educationLevel.IsLastYear)
                        {
                            // Neu cuoi cap thi HK D
                            // HL mon NX A hoac A+, mon tinh diem tu TB tro len la du dieu kien du thi tot nghiep
                            // Tuc la HL la TB va HK D la duoc
                            if (PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_D
                                && (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT)
                                || (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_GOOD)
                                || (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_NORMAL))
                            {
                                PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                            }
                            else
                            {
                                PupilRankingBO.StudyingJudgementID = null;
                            }
                        }
                        else
                        {
                            //Nếu xếp loại giáo dục trên trung bình thì lên lớp
                            if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_NORMAL || PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT || PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_GOOD)
                            {
                                //Không phải năm cuối cấp thì thuộc diện Lên lớp
                                PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                            }
                            //Nếu xếp loại giáo dục yếu
                            if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_WEAK)
                            {
                                //Nếu lstPupilToRank[i].CapacityLevelID = CAPACITY_TYPE_WEAK (4) and CheckNumberOfRetest(lstPupilToRank[i].AcademicYearID, lstPupilToRank[i].SchoolID, lstPupilToRank[i].PupilID, lstPupilToRank[i].ClassID) = False thì
                                var checknumberofretest = false;//PupilRetestRegistrationBusiness.CheckNumberOfRetest(PupilRankingBO.AcademicYearID.Value, PupilRankingBO.SchoolID.Value, PupilRankingBO.ClassID.Value, PupilRankingBO.PupilID.Value);
                                //Luôn cho học sinh cấp 1 thi lại nếu chưa đủ điểm
                                if (!checknumberofretest)
                                {
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_RETEST;
                                }
                                //CheckNumberOfRetest(lstPupilToRank[i].AcademicYearID, lstPupilToRank[i].SchoolID, lstPupilToRank[i].PupilID, lstPupilToRank[i].ClassID) = true
                                //thì
                                else
                                {
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                            }
                            //Nếu hạnh kiểm chưa thực hiện đầy đủ
                            if (PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_CD && PupilRankingBO.CapacityLevelID != 0 && PupilRankingBO.CapacityLevelID != null)
                            {
                                if (PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                                {
                                    //Học sinh có hạnh kiểm thực hiện chưa đầy đủ thì thuộc diện rèn luyện lại
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING;
                                }
                                //Nếu hạnh kiểm chưa thực hiện đầy đủ sau khi rèn luyện lại thì ở lại lớp
                                if (PupilRankingBO.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                                {
                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                            }
                        }
                        //Step2: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking nếu tìm thấy bản ghi.
                        var pr = lstPupilRanking.FirstOrDefault(o => o.PupilRankingID == PupilRankingBO.PupilRankingID);


                        if (pr != null && prr == null)
                        {
                            pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                            pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            pr.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                            pr.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                            pr.IsCategory = true;
                            this.Update(pr);
                        }
                        else
                        {
                            //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                            PupilRanking prExist = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING);
                            if (PupilRankingBO.CapacityLevelID != null && PupilRankingBO.ConductLevelID != null)
                            {
                                if (prExist == null)
                                {
                                    PupilRanking _pr = new PupilRanking();
                                    _pr.PupilID = PupilRankingBO.PupilID.Value;
                                    _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                                    _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                                    _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                                    _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                                    _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                                    _pr.PeriodID = PupilRankingBO.PeriodID;
                                    _pr.CreatedAcademicYear = PupilRankingBO.Year.Value;
                                    _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value % 100;
                                    _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                                    _pr.ClassID = PupilRankingBO.ClassID.Value;
                                    _pr.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                                    _pr.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                                    _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                                    _pr.IsCategory = true;
                                    this.Insert(_pr);
                                }
                                else
                                {
                                    prExist.ConductLevelID = PupilRankingBO.ConductLevelID;
                                    prExist.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                                    prExist.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                                    prExist.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                                    prExist.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                                    prExist.IsCategory = true;
                                    this.Update(prExist);
                                }
                            }
                        }

                        //Step3: Nếu lstPupilToRank[i].HonourAchivementTypeID = null
                        //Thực hiện tìm kiếm trong bảng PupilEmulation theo PupilID, ClassID, SchoolID, AcademicYearID, Semester = 2.
                        var pupilemulation = lstPupilEmulation.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                        if (PupilRankingBO.HonourAchivementTypeID == null)
                        {
                            //Nếu tìm thấy pupilemulation thì xóa
                            if (pupilemulation != null)
                            {
                                PupilEmulationBusiness.Delete(pupilemulation.PupilEmulationID);
                            }
                        }
                        else if (PupilRankingBO.HonourAchivementTypeID != null)
                        {
                            //-	Nếu tìm thấyc pupilemulation thì cập nhật lại HonourAchivementTypeID và update
                            if (pupilemulation != null)
                            {
                                pupilemulation.HonourAchivementTypeID = PupilRankingBO.HonourAchivementTypeID.Value;
                                context.Configuration.AutoDetectChangesEnabled = true;
                                PupilEmulationBusiness.Update(pupilemulation);
                            }
                            //-	Nếu không tìm thấy pupilemulation thì insert pe vào bảng PupilEmulation
                            if (pupilemulation == null)
                            {
                                PupilEmulation pe = new PupilEmulation();
                                pe.PupilID = PupilRankingBO.PupilID.Value;
                                pe.ClassID = PupilRankingBO.ClassID.Value;
                                pe.SchoolID = PupilRankingBO.SchoolID.Value;
                                pe.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                                pe.Semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                                pe.HonourAchivementTypeID = PupilRankingBO.HonourAchivementTypeID.Value;
                                pe.Last2digitNumberSchool = UtilsBusiness.GetPartionId(PupilRankingBO.SchoolID.Value);
                                PupilEmulationBusiness.Insert(pe);
                            }
                        }
                    }
                }
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) //Nếu là học kỳ 1 thì chỉ lưu lại
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pupilRankingBO.AcademicYearID;
                dic["ClassID"] = pupilRankingBO.ClassID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                List<PupilRanking> lstPr = SearchBySchool(pupilRankingBO.SchoolID.Value, dic).Where(o => o.PeriodID == null).ToList();
                foreach (var prBO in lstPupilRankingBO)
                {
                    var pr = lstPr.FirstOrDefault(o => o.PupilID == prBO.PupilID);
                    if (pr != null)
                    {
                        pr.CapacityLevelID = prBO.CapacityLevelID;
                        pr.TotalAbsentDaysWithoutPermission = prBO.TotalAbsentDaysWithoutPermission;
                        pr.TotalAbsentDaysWithPermission = prBO.TotalAbsentDaysWithPermission;
                        pr.IsCategory = true;
                        this.Update(pr);
                    }
                    else
                    {
                        PupilRanking _pr = new PupilRanking();
                        _pr.PupilID = prBO.PupilID.Value;
                        _pr.SchoolID = prBO.SchoolID.Value;
                        _pr.AcademicYearID = prBO.AcademicYearID.Value;
                        _pr.ConductLevelID = prBO.ConductLevelID;
                        _pr.CapacityLevelID = prBO.CapacityLevelID;
                        _pr.EducationLevelID = prBO.EducationLevelID.Value;
                        _pr.PeriodID = null;
                        _pr.CreatedAcademicYear = prBO.Year.Value;
                        _pr.Last2digitNumberSchool = prBO.SchoolID.Value % 100;
                        _pr.StudyingJudgementID = null;
                        _pr.ClassID = prBO.ClassID.Value;
                        _pr.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                        _pr.TotalAbsentDaysWithoutPermission = prBO.TotalAbsentDaysWithoutPermission;
                        _pr.TotalAbsentDaysWithPermission = prBO.TotalAbsentDaysWithPermission;
                        _pr.IsCategory = true;
                        this.Insert(_pr);
                    }
                }
            }
        }

        /// <summary>
        /// Từ học lực, hạnh kiểm, số buổi nghỉ => danh hiệu, thuộc diện
        /// namdv3
        /// </summary>
        /// <param name="lstPupilRankingBO"></param>
        /// <param name="Semester"></param>
        /// 

        public void ClassifyPupil(List<PupilRankingBO> lstPupilRankingBO, int Semester, bool isShowRetetResult, string auto)
        {
            try
            {

                if (lstPupilRankingBO == null || lstPupilRankingBO.Count == 0)
                {
                    return;
                }
                //SetAutoDetectChangesEnabled(false);
                
                List<PupilRanking> lstPupilRankingUpdate = new List<PupilRanking>();

                PupilRankingBO pro = lstPupilRankingBO.FirstOrDefault();
                int schoolId = pro.SchoolID.Value;
                int classId = pro.ClassID.Value;
                //int AcademicYearID = pro.AcademicYearID.Value;
                ClassProfile cp = ClassProfileBusiness.Find(classId);
                EducationLevel educationLevel = cp.EducationLevel;
                int appliedLevel = educationLevel.Grade;
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                var honourType1 = HonourAchivementTypeBusiness.All.FirstOrDefault(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_GOOD
                                                                                       && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL);
                var honourType2 = HonourAchivementTypeBusiness.All.FirstOrDefault(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_EXCELLENT
                                                                                       && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL);
                //List<int> lstHKPass = new List<int> {GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY, GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY, GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY,
                //GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY, GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY, GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY};
                string strGDTX = "GDTX";
                List<int> lstPupilId = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();

                SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
                bool isGDTX = (sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX);



                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pro.AcademicYearID;
                dic["SchoolID"] = pro.SchoolID;
                dic["ClassID"] = pro.ClassID;

                //Chi lay HS thi lai khi HK la ca nam va co hien thi ket qua thi lai
                List<PupilRetestRegistration> listP = new List<PupilRetestRegistration>();
                List<int> listPupil = new List<int>();
                List<int> lstPupilRetest = new List<int>();
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && isShowRetetResult)
                {
                    listP = PupilRetestRegistrationBusiness.SearchPupilRetestRegistration(dic).ToList();
                    listPupil = listP.Where(o => o.ModifiedDate == null).Select(o => o.PupilID).Distinct().ToList();
                    lstPupilRetest = listP.Select(o => o.PupilID).Distinct().ToList();
                }

                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pro.AcademicYearID;
                dic["ClassID"] = pro.ClassID;
                dic["Semester"] = Semester;
                List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(pro.SchoolID.Value, dic).ToList();

                if (lstPupilEmulation.Any())
                {
                    PupilEmulationBusiness.DeleteAll(lstPupilEmulation);
                }
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pro.AcademicYearID;
                dic["ClassID"] = pro.ClassID;
                List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(pro.SchoolID.Value, dic).Where(o => o.PeriodID == null && lstPupilId.Contains(o.PupilID)).ToList();

                if (!lstPupilRankingBO.Any())
                {
                    return;
                }
                //Step1: Lấy từng phần tử trong lstPupilToRank.
                foreach (var pupilRankingBo in lstPupilRankingBO)
                {
                    //int TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission.HasValue ? PupilRankingBO.TotalAbsentDaysWithPermission.Value : 0;
                    //int TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission.HasValue ? PupilRankingBO.TotalAbsentDaysWithoutPermission.Value : 0;
                    bool isKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, appliedLevel, pupilRankingBo.LearningType, pupilRankingBo.BirthDate);
                    if (isKXLHK)
                    {
                        pupilRankingBo.ConductLevelID = appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                            ? GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY
                            : GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY;
                    }
                    if (pupilRankingBo.CapacityLevelID.HasValue) //Nếu có học lực, hạnh kiểm thì mới xếp loại
                    {
                        if (pupilRankingBo.TotalAbsentDaysWithPermission == null)
                        {
                            pupilRankingBo.TotalAbsentDaysWithPermission = 0;
                        }
                        if (pupilRankingBo.TotalAbsentDaysWithoutPermission == null)
                        {
                            pupilRankingBo.TotalAbsentDaysWithoutPermission = 0;
                        }
                        pupilRankingBo.HonourAchivementTypeID = null;

                        //Nếu học lực khá trở lên, hạnh kiểm khá trở lên buổi thì đạt học sinh tiên tiến
                        if (pupilRankingBo.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && ((!lstPupilRetest.Contains(pupilRankingBo.PupilID.Value) && Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL) || Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL) &&
                            (pupilRankingBo.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT || pupilRankingBo.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_GOOD)

                            && (((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY || pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY
                                || pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY || pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY)))
                                )
                        {

                            if (honourType1 != null)
                                pupilRankingBo.HonourAchivementTypeID = honourType1.HonourAchivementTypeID;
                        }


                        //Nếu học lực giỏi, hạnh kiểm tốt, buổi thì đạt học sinh giỏi
                        if (pupilRankingBo.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && ((!lstPupilRetest.Contains(pupilRankingBo.PupilID.Value) && Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL) || Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL) &&
                            pupilRankingBo.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT
                            && (((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY || pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY)))
                                )
                        {

                            if (honourType2 != null)
                                pupilRankingBo.HonourAchivementTypeID = honourType2.HonourAchivementTypeID;
                        }


                        if (pupilRankingBo.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING && (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                        {
                            var pr = lstPupilRanking.FirstOrDefault(o => o.PupilRankingID == pupilRankingBo.PupilRankingID);
                            pr.TotalAbsentDaysWithoutPermission = pupilRankingBo.TotalAbsentDaysWithoutPermission;
                            pr.TotalAbsentDaysWithPermission = pupilRankingBo.TotalAbsentDaysWithPermission;
                            if (pr != null)
                            {
                                pr.IsCategory = true;
                                pr.MSourcedb = auto;
                                //PupilRankingBusiness.Update(pr);
                                lstPupilRankingUpdate.Add(pr);
                            }
                        }
                        //Xét thuộc diện: Chỉ với Semester = 3
                        if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                        {
                            // Neu la lop cuoi cap thi xu ly khac
                            if (educationLevel.IsLastYear)
                            {
                                // Neu la cap 3 thi HL >= Yeu va HK >= TB la du dieu kien du thi tot ngiep
                                // Cap 2 thi HL >= TB
                                if (educationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY || educationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                                {
                                    // Cap 2, 3
                                    if ((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY))
                                    {
                                        if (educationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                        {
                                            // Cap 3 thi HL tu Yeu tro len la duoc du dk du thi tot nghiep
                                            if (pupilRankingBo.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_WEAK)
                                            {
                                                pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                                            }
                                            else
                                            {
                                                pupilRankingBo.StudyingJudgementID = null;
                                            }
                                        }
                                        else
                                        {
                                            // Cap 2 thi HL tu TB tro len la duoc du dk du thi tot nghiep
                                            if (pupilRankingBo.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_NORMAL)
                                            {
                                                pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                                            }
                                            else
                                            {
                                                pupilRankingBo.StudyingJudgementID = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        pupilRankingBo.StudyingJudgementID = null;
                                    }
                                }

                            }
                            else
                            {
                                // Nếu nghỉ quá 45 buổi => ở lại lớp
                                if (((pupilRankingBo.TotalAbsentDaysWithPermission + pupilRankingBo.TotalAbsentDaysWithoutPermission > 45 && !isGDTX)
                                    || (pupilRankingBo.TotalAbsentDaysWithPermission + pupilRankingBo.TotalAbsentDaysWithoutPermission > 45 && isGDTX)))
                                {
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                                //Else
                                //Nếu lstPupilToRank[i].CapacityLevelID = CAPACITY_TYPE_POOR thì
                                //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_STAYCLASS (4)
                                //Hạnh kiểm cả năm xếp loại yếu mà rèn luyện lại vẫn không đạt => ở lại lớp
                                else if (pupilRankingBo.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_POOR)
                                {
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                                //Else
                                //Nếu lstPupilToRank[i].ConductLevelID  = CONDUCT_TYPE_WEAK_SECONDARY/ CONDUCT_TYPE_WEAK_TERTIARY && lstPupilToRank[i].pr.Semester = 4 thì:
                                //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_STAYCLASS (4)
                                else if ((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY
                                    || pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)
                                    && pupilRankingBo.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                                {
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                                //Nếu hạnh kiểm yếu, học lực dưới TB thì ở lại lớp
                                else if ((((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY ||
                                     pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)))
                                     && pupilRankingBo.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_WEAK)
                                {
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                                //Nếu hạnh kiểm trên TB, học lực dưới TB thì ở lại lớp:
                                else if ((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY)
                                    && pupilRankingBo.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK
                                    && !listPupil.Any(o => o == pupilRankingBo.PupilID) && listP.Count(o => o.PupilID == pupilRankingBo.PupilID) > 0)
                                {
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                }
                                //Nếu hạnh kiểm yếu, học lực trên TB thì rèn luyện lại:
                                else if ((((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)))
                                    && pupilRankingBo.CapacityLevelID < GlobalConstants.CAPACITY_TYPE_WEAK && pupilRankingBo.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                                {
                                    // Doi voi lop cuoi cap thi de tron
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING;
                                }
                                //Nếu hạnh kiểm trên TB, học lực dưới TB thì thi lại:
                                else if ((((pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                    pupilRankingBo.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY)))
                                    && pupilRankingBo.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK
                                    && (listPupil.Any(o => o == pupilRankingBo.PupilID) || listP.Count(o => o.PupilID == pupilRankingBo.PupilID) == 0))
                                {
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_RETEST;
                                }
                                else
                                {
                                    //Các trường hợp còn lại được lên lớp
                                    pupilRankingBo.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;

                                }
                            }
                            //Step2: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking
                            var pr = lstPupilRanking.FirstOrDefault(o => o.PupilRankingID == pupilRankingBo.PupilRankingID);
                            if (pr != null)
                            {
                                pr.ConductLevelID = pupilRankingBo.IsKXLHK ? new Nullable<Int32>() : pupilRankingBo.ConductLevelID;
                                pr.CapacityLevelID = pupilRankingBo.CapacityLevelID;
                                pr.StudyingJudgementID = pupilRankingBo.StudyingJudgementID;
                                pr.IsCategory = true;
                                pr.MSourcedb = auto;
                                //PupilRankingBusiness.Update(pr);
                                lstPupilRankingUpdate.Add(pr);
                            }

                        }
                        // DungVA
                        //Step 3:
                        /* Nếu lstPupilToRank[i].HonourAchivementTypeID = null
                         Thực hiện tìm kiếm trong bảng PupilEmulation theo PupilID, ClassID, SchoolID, AcademicYearID, Semester
                         */


                        if (pupilRankingBo.HonourAchivementTypeID.HasValue)
                        {
                            PupilEmulation pe = new PupilEmulation
                            {
                                PupilID = pupilRankingBo.PupilID.Value,
                                ClassID = pupilRankingBo.ClassID.Value,
                                SchoolID = pupilRankingBo.SchoolID.Value,
                                AcademicYearID = pupilRankingBo.AcademicYearID.Value,
                                Semester = Semester,
                                HonourAchivementTypeID = pupilRankingBo.HonourAchivementTypeID.Value,
                                MSourcedb = auto,
                                Last2digitNumberSchool = partitionId
                            };
                            PupilEmulationBusiness.Insert(pe);
                        }



                    }

                }
                if (lstPupilRankingUpdate.Count > 0)
                {
                    for (int i = 0; i < lstPupilRankingUpdate.Count; i++)
                    {
                        PupilRankingBusiness.Update(lstPupilRankingUpdate[i]);
                    }
                }
                PupilRankingBusiness.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Từ học lực, hạnh kiểm, số buổi nghỉ => Xếp loại HS. Áp dụng cho cấp 2-3
        /// namdv3
        /// </summary>
        /// <param name="lstPupilRankingBO"></param>
        public void ClassifyPupilAfterRetest(List<PupilRankingBO> lstPupilRankingBO, List<PupilRetestRegistration> lstPupilRetest, int SchoolID, int AppliedLevel, int AcademicYearID)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                List<int> lstHKPass = new List<int> {GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY, GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY, GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY,
                                                 GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY, GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY, GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY};
                string strGDTX = "GDTX";
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                // Danh sach Semester=3
                if (lstPupilRankingBO == null || lstPupilRankingBO.Count == 0)
                {
                    return;
                }
                int Year = objAy.Year;
                List<int> lstPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();
                List<int> lstClassID = lstPupilRankingBO.Select(o => o.ClassID.Value).Distinct().ToList();
                //List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                //                                    .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
                // Thông tin tổng kết sau thi lại
                List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> {
                                                            {"AcademicYearID", AcademicYearID },
                                                            {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING}
                                                            })
                                                        .Where(o => lstPupilID.Contains(o.PupilID) && o.PeriodID == null).ToList();

                List<int> lstPupilBacktrain = (from p in VPupilRankingBusiness.All
                                               .Where(u => u.Last2digitNumberSchool == SchoolID % 100
                                                        && u.CreatedAcademicYear == objAy.Year
                                                        && u.SchoolID == SchoolID
                                                        && u.AcademicYearID == AcademicYearID
                                                        && lstClassID.Contains(u.ClassID) && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                        && (u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING))
                                               select p.PupilID).ToList();

                #region Nghiệp vụ GDTX
                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                bool isGDTX = sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX;
                bool isAbsentContest = false;
                List<int> lstPupilRetestID = lstPupilRetest.Select(p => p.PupilID).Distinct().ToList();

                Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                dicAbsence["AcademicYearID"] = AcademicYearID;
                dicAbsence["lstClassID"] = lstClassID;
                dicAbsence["lstPupilID"] = lstPupilID;
                dicAbsence["SchoolID"] = SchoolID;
                List<int> lstSectionKeyID = new List<int>();
                List<PupilAbsence> lstAbsenceAll = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsence)
                                                                        .Where(o => ((o.AbsentDate >= objAy.FirstSemesterStartDate && o.AbsentDate <= objAy.FirstSemesterEndDate)
                                                                        || (o.AbsentDate >= objAy.SecondSemesterStartDate && o.AbsentDate <= objAy.SecondSemesterEndDate))).ToList();
                List<PupilAbsence> lstAbsencePupil = null;
                int TotalAbsentDaysWithPermission = 0;
                int TotalAbsentDaysWithoutPermission = 0;
                if (isGDTX) //Nghiep vu GDTX
                {
                    foreach (var PupilRankingBO in lstPupilRankingBO)
                    {
                        var pr = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                        isAbsentContest = lstPupilRetest.Where(p => p.PupilID == PupilRankingBO.PupilID && p.IsAbsentContest.HasValue && p.IsAbsentContest.Value).Count() > 0;
                        //Kiểm tra học sinh có thuộc diện xếp loại hạnh kiểm không
                        //•	Các trường thuộc hệ GDTX  có các đối tượng sau không thuộc diện xếp loại hạnh kiểm: 
                        //-	Học viên có hình thức học là vừa làm vừa học
                        //-	Người lao động từ 20 tuổi trở lên đối với cấp THCS và 25 tuổi trở lên đối với cấp THPT
                        //-	Học viên học theo hình thức tự học có hướng dẫn - Bo qua vi chua luu vao db
                        if (pr != null)
                        {
                            lstSectionKeyID = PupilRankingBO.SectionKeyID.HasValue ? UtilsBusiness.GetListSectionID(PupilRankingBO.SectionKeyID.Value) : new List<int>();
                            lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == pr.PupilID && lstSectionKeyID.Contains(o.Section)).ToList();
                            if ((isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))
                                && !lstPupilBacktrain.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                TotalAbsentDaysWithoutPermission = lstAbsencePupil.Where(o => o.IsAccepted != true).Count();
                                TotalAbsentDaysWithPermission = lstAbsencePupil.Where(o => o.IsAccepted == true).Count();
                                bool isKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, PupilRankingBO.LearningType, PupilRankingBO.BirthDate);
                                if (pr.CapacityLevelID.HasValue && (pr.ConductLevelID.HasValue || isKXLHK))
                                {
                                    if (isKXLHK)  //Nếu không thuộc diện xếp loại hạnh kiểm
                                    {
                                        if (pr.CapacityLevelID < GlobalConstants.CAPACITY_TYPE_WEAK  //Nếu học lực trên TB
                                        && (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission) <= 45)  //Nghỉ không quá 45 buổi
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                        }
                                        else
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                        }
                                    }
                                    else   //Có xếp loại hạnh kiểm
                                    {
                                        if (pr.CapacityLevelID < GlobalConstants.CAPACITY_TYPE_WEAK  //Nếu học lực trên TB
                                        && (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission) <= 45 //Nghỉ không quá 45 buổi
                                            && pr.ConductLevelID.HasValue && lstHKPass.Contains(pr.ConductLevelID.Value))
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                        }
                                        else
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                        }
                                    }
                                }
                            }
                            this.Update(pr);
                        }
                        else //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                        {
                            PupilRanking _pr = new PupilRanking();
                            _pr.PupilID = PupilRankingBO.PupilID.Value;
                            _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                            _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                            _pr.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                            _pr.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                            _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                            _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            _pr.PeriodID = PupilRankingBO.PeriodID;
                            _pr.Semester = PupilRankingBO.Semester;
                            _pr.CreatedAcademicYear = Year;
                            _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value % 100;
                            if (isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                _pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            }
                            _pr.ClassID = PupilRankingBO.ClassID.Value;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                            this.Insert(_pr);
                        }
                    }
                }
                #endregion
                //Lấy từng phần tử trong lstPupilToRank.
                else
                {
                    #region Trường bình thường
                    // danh sach pupil_ranking voi Semester=3
                    foreach (var PupilRankingBO in lstPupilRankingBO)
                    {
                        var pr = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                        isAbsentContest = lstPupilRetest.Where(p => p.PupilID == PupilRankingBO.PupilID && p.IsAbsentContest.HasValue && p.IsAbsentContest.Value).Count() > 0;
                        //Step1: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking nếu tìm thấy bản ghi.
                        if (pr != null)
                        {
                            lstSectionKeyID = PupilRankingBO.SectionKeyID.HasValue ? UtilsBusiness.GetListSectionID(PupilRankingBO.SectionKeyID.Value) : new List<int>();
                            lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == pr.PupilID && lstSectionKeyID.Contains(o.Section)).ToList();
                            if ((isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))
                                && !lstPupilBacktrain.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                if (pr.CapacityLevelID.HasValue && pr.ConductLevelID.HasValue)
                                {
                                    TotalAbsentDaysWithoutPermission = lstAbsencePupil.Where(o => o.IsAccepted != true).Count();
                                    TotalAbsentDaysWithPermission = lstAbsencePupil.Where(o => o.IsAccepted == true).Count();
                                    // Nếu học lực trên TB, hạnh kiểm trên TB và nghỉ không quá 45 buổi thì thuộc diện lên lớp
                                    if (pr.ConductLevelID.HasValue && lstHKPass.Contains(pr.ConductLevelID.Value)
                                    && (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission <= GlobalConstants.TOTALABSENTDAY)
                                        && pr.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_WEAK)
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                    }
                                    // Nếu học lực trên TB và hạnh kiểm yếu thì rèn luyện lại
                                    else if ((pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY
                                        || pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)
                                        && pr.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_WEAK && pr.Semester == 4)
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    else
                                        //Nếu nghỉ học quá 45 buổi thì ở lại lớp
                                        if (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission > GlobalConstants.TOTALABSENTDAY)
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    if ((pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY)
                                    && pr.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK) // HK Tot-Kha-TB va HL >= Yeu
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    else
                                        if ((pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY
                                            || pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)
                                            && pr.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK)
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                }
                            }
                            this.Update(pr);
                        }
                        else
                        {
                            //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                            PupilRanking _pr = new PupilRanking();
                            _pr.PupilID = PupilRankingBO.PupilID.Value;
                            _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                            _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                            _pr.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                            _pr.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                            _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                            _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            _pr.PeriodID = PupilRankingBO.PeriodID;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            if (isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                _pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            }
                            _pr.ClassID = PupilRankingBO.ClassID.Value;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                            _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value % 100;
                            _pr.CreatedAcademicYear = Year;
                            this.Insert(_pr);
                        }
                    }
                    #endregion
                }

                this.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// namdv3
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilRankingBO"></param>
        public void RankingPupilRetest(int UserID, List<PupilRetestToCategory> lstPupilRetestToCategoryBO, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;

                int schoolID = lstPupilRetestToCategoryBO[0].lstSummedUpRecord[0].SchoolID;
                int modSchoolID = schoolID % 100;
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolID);
                AcademicYear AcademicYear = AcademicYearBusiness.Find(lstPupilRetestToCategoryBO[0].pr.AcademicYearID);
                TrainingType traningType = schoolProfile.TrainingType;
                if (lstPupilRetestToCategoryBO != null && lstPupilRetestToCategoryBO.Count > 0)
                {
                    List<int> ListClass = new List<int>();
                    foreach (var PupilRetestToCategoryBO in lstPupilRetestToCategoryBO)
                    {
                        foreach (var item in PupilRetestToCategoryBO.lstSummedUpRecord)
                        {
                            if (!ListClass.Any(o => o == item.ClassID))
                                ListClass.Add(item.ClassID);
                        }
                    }
                    foreach (var ClassID in ListClass)
                    {
                        if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) == false)
                        {
                            throw new BusinessException("PuPilRanking_Not_Authen");
                        }
                    }
                    // Lay Dictionary luu danh sach mon hoc cua tung lop
                    Dictionary<int, List<ClassSubject>> dicClassSubject = new Dictionary<int, List<ClassSubject>>();
                    // Danh sach Sem = 3
                    IDictionary<string, object> diccs;// = new Dictionary<string, object>();

                    foreach (var PupilRetestToCategoryBO in lstPupilRetestToCategoryBO)
                    {
                        int classID = PupilRetestToCategoryBO.pr.ClassID;
                        if (!dicClassSubject.ContainsKey(classID))
                        {
                            diccs = new Dictionary<string, object>();
                            diccs["SchoolID"] = schoolID;
                            dicClassSubject[classID] = ClassSubjectBusiness.SearchByClass(classID, diccs).ToList();
                        }
                    }
                    foreach (var PupilRetestToCategoryBO in lstPupilRetestToCategoryBO)
                    {
                        List<string> lstmark = PupilRetestToCategoryBO.lstMark;
                        List<SummedUpRecord> listsummeduprecord = PupilRetestToCategoryBO.lstSummedUpRecord;
                        if (lstmark.Count != listsummeduprecord.Count)
                        {
                            continue;
                        }
                        List<SummedUpRecordMarkBO> lstPupilRankingOfPupil = new List<SummedUpRecordMarkBO>();
                        for (var i = 0; i < lstmark.Count; i++)
                        {
                            SummedUpRecordMarkBO SummedUpRecordMarkBO = new SummedUpRecordMarkBO();
                            SummedUpRecordMarkBO.SummedUpRecord = listsummeduprecord[i];
                            SummedUpRecordMarkBO.Mark = lstmark[i];
                            lstPupilRankingOfPupil.Add(SummedUpRecordMarkBO);
                        }
                        var lstPupilRankingOfPupil_Voluntary = new List<SummedUpRecordMarkBO>();
                        var lstPupilRankingOfPupil_Mandatory = new List<SummedUpRecordMarkBO>();
                        if (lstPupilRankingOfPupil != null && lstPupilRankingOfPupil.Count > 0)
                        {
                            foreach (var PupilRankingOfPupil in lstPupilRankingOfPupil)
                            {
                                var isAppliedTypeSubject = IsAppliedTypeSubject(dicClassSubject[PupilRankingOfPupil.SummedUpRecord.ClassID], PupilRankingOfPupil.SummedUpRecord.ClassID, PupilRankingOfPupil.SummedUpRecord.SubjectID);
                                if (isAppliedTypeSubject)
                                {
                                    lstPupilRankingOfPupil_Voluntary.Add(PupilRankingOfPupil);
                                }
                                else
                                {
                                    lstPupilRankingOfPupil_Mandatory.Add(PupilRankingOfPupil);
                                }
                            }
                        }

                        //If (lstPupilRankingOfPupil_Mandatory.Count >0)
                        //thì tính điểm trung bình các môn AverageMark = CaculatorAverageSubject(lstPupilRankingOfPupil_Mandatory)
                        if (lstPupilRankingOfPupil_Mandatory == null || lstPupilRankingOfPupil_Mandatory.Count == 0)
                        {
                            continue;
                        }
                        decimal? AverageMark = 0;
                        List<SummedUpRecord> ListSummedUpRecord = lstPupilRankingOfPupil_Mandatory.Select(o => o.SummedUpRecord).ToList();
                        AverageMark = CaculatorAverageSubject(ListSummedUpRecord, dicClassSubject[PupilRetestToCategoryBO.pr.ClassID], SystemParamsInFile.SEMESTER_OF_YEAR_ALL, lstRegisterSubjectBO);
                        //For(lstPupilRankingOfPupil_Voluntary)
                        foreach (var item in lstPupilRankingOfPupil_Voluntary)
                        {
                            if (item.SummedUpRecord.ReTestMark.HasValue)
                            {
                                item.SummedUpRecord.SummedUpMark = item.SummedUpRecord.ReTestMark;
                            }
                            //Nếu lstPupilRankingOfPupil_Voluntary[i].SummedUpMark >= 8.0 thì AverageMark = AverageMark + 0.3
                            if (item.SummedUpRecord.SummedUpMark >= GlobalConstants.EXCELLENT_MARK)
                            {
                                AverageMark = AverageMark + GlobalConstants.ADD_EXCELLENT_MARK;
                            }
                            //Nếu lstPupilRankingOfPupil_Voluntary[i].SummedUpMark >= 6.5 và < 8.0 thì AverageMark = AverageMark + 0.2
                            else if (item.SummedUpRecord.SummedUpMark >= GlobalConstants.GOOD_MARK && item.SummedUpRecord.SummedUpMark < GlobalConstants.EXCELLENT_MARK)
                            {
                                AverageMark = AverageMark + GlobalConstants.ADD_GOOD_MARK;
                            }
                            //Nếu lstPupilRankingOfPupil_Voluntary[i].SummedUpMark >= 5.0 và < 6.5 thì AverageMark = AverageMark + 0.1
                            else if (item.SummedUpRecord.SummedUpMark >= GlobalConstants.NORMAL_MARK && item.SummedUpRecord.SummedUpMark < GlobalConstants.GOOD_MARK)
                            {
                                AverageMark = AverageMark + GlobalConstants.ADD_NORMAL_MARK;
                            }
                        }
                        if (AverageMark > GlobalConstants.MAX_MARK)
                        {
                            AverageMark = GlobalConstants.MAX_MARK;
                        }
                        // Set lai diem trung binh mon de lay xep loai hoc luc
                        foreach (SummedUpRecord item in ListSummedUpRecord)
                        {
                            if (item.ReTestMark.HasValue)
                            {
                                item.SummedUpMark = item.ReTestMark;
                            }
                            if (item.ReTestJudgement != null && item.ReTestJudgement != " ")
                            {
                                item.JudgementResult = item.ReTestJudgement;
                            }
                        }

                        int CapacityLevelID = GetCapacityTypeByMark(AverageMark, ListSummedUpRecord, dicClassSubject[PupilRetestToCategoryBO.pr.ClassID], schoolID, traningType, lstRegisterSubjectBO);

                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["PupilID"] = PupilRetestToCategoryBO.PupilID;
                        dic["AcademicYearID"] = PupilRetestToCategoryBO.pr.AcademicYearID;
                        dic["ClassID"] = PupilRetestToCategoryBO.pr.ClassID;
                        dic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                        IQueryable<PupilRanking> IQueryableRanking = this.Search(dic);
                        if (IQueryableRanking.Count() > 0)
                        {
                            PupilRanking PupilRanking = IQueryableRanking.FirstOrDefault();
                            PupilRanking.AverageMark = AverageMark;
                            PupilRanking.CapacityLevelID = CapacityLevelID;
                            base.Update(PupilRanking);
                        }
                        else
                        {
                            PupilRanking pr = new PupilRanking();

                            pr.PupilID = PupilRetestToCategoryBO.PupilID;
                            pr.ClassID = PupilRetestToCategoryBO.pr.ClassID;
                            pr.SchoolID = PupilRetestToCategoryBO.pr.SchoolID;
                            pr.TotalAbsentDaysWithoutPermission = PupilRetestToCategoryBO.pr.TotalAbsentDaysWithoutPermission;
                            pr.TotalAbsentDaysWithPermission = PupilRetestToCategoryBO.pr.TotalAbsentDaysWithPermission;
                            pr.AcademicYearID = PupilRetestToCategoryBO.pr.AcademicYearID;
                            pr.EducationLevelID = PupilRetestToCategoryBO.pr.EducationLevelID;
                            pr.ConductLevelID = PupilRetestToCategoryBO.pr.ConductLevelID;
                            pr.CreatedAcademicYear = AcademicYear.Year;
                            pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            pr.AverageMark = AverageMark;
                            pr.CapacityLevelID = CapacityLevelID;
                            pr.RankingDate = DateTime.Now;
                            pr.Last2digitNumberSchool = modSchoolID;
                            base.Insert(pr);
                        }
                    }

                    this.Save();

                }
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        // <summary>
        /// Lấy danh sách học sinh để thực hiện xếp loại học sinh sau thi lại cấp 1
        /// Chưa gán hết các thuộc tính vào đối tượng PupilRankingBO. Ai gọi hàm này thì tự thêm nhé.
        /// namdv3
        /// </summary>
        /// <param name="dic">AcademicYearID 
        /// EducationLevelID
        /// SchoolID
        /// ListClassID
        /// </param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilToRankPrimaryAfterRetest(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");
            int modSchoolID = SchoolID % 100;
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);

            var lstPrr = this.PupilRetestRegistrationRepository.All
                                       .Where(u => u.SchoolID == SchoolID
                                                   && u.AcademicYearID == AcademicYearID
                                                   && ListClassID.Contains(u.ClassID));
            var lstPrrMax = lstPrr.Where(u => u.RegisteredDate == lstPrr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.RegisteredDate));

            //var lstPr = lstPrrMax.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
            //                    .Join(VPupilRankingBusiness.All
            //                                .Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && ListClassID.Contains(u.ClassID) && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND),
            //                                            u => new { u.PupilID, u.ClassID },
            //                                            v => new { v.PupilID, v.ClassID },
            //                                            (u, v) => v).Where(o => o.PeriodID == null);

            var lstPr = from p in lstPrrMax.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                        join q in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && ListClassID.Contains(u.ClassID) && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        on new { p.PupilID, p.ClassID } equals new { q.PupilID, q.ClassID }
                        join r in ClassProfileBusiness.All on q.ClassID equals r.ClassProfileID
                        join s in ConductLevelBusiness.All on q.ConductLevelID equals s.ConductLevelID
                        into g1
                        from j1 in g1.DefaultIfEmpty()
                        join l in CapacityLevelBusiness.All on q.CapacityLevelID equals l.CapacityLevelID into g2
                        from j2 in g2.DefaultIfEmpty()
                        join m in StudyingJudgementBusiness.All on q.StudyingJudgementID equals m.StudyingJudgementID into g3
                        from j3 in g3.DefaultIfEmpty()
                        where q.PeriodID == null
                        && r.IsActive.Value
                        select new PupilRankingBO
                        {
                            PupilRankingID = q.PupilRankingID,
                            PupilID = q.PupilID,
                            ClassID = q.ClassID,
                            Semester = q.Semester,
                            SchoolID = q.SchoolID,
                            AcademicYearID = q.AcademicYearID,
                            ClassName = r.DisplayName,
                            ConductLevelName = j1.Resolution,
                            ConductLevelID = q.ConductLevelID,
                            CapacityLevelID = q.CapacityLevelID,
                            CapacityLevel = j2.CapacityLevel1,
                            EducationLevelID = q.EducationLevelID,
                            PeriodID = q.PeriodID,
                            Year = q.CreatedAcademicYear,
                            StudyingJudgementID = q.StudyingJudgementID,
                            StudyingJudgementResolution = j3.Resolution,
                            TotalAbsentDaysWithPermission = q.TotalAbsentDaysWithPermission,
                            TotalAbsentDaysWithoutPermission = q.TotalAbsentDaysWithoutPermission

                        };
            var lstPrMax = lstPr.Where(u => u.Semester == lstPr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();
            var lstPrSemesterSecond = lstPr.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
            var lstPrSemesterAll = lstPrSemesterSecond.Where(u => u.Semester == lstPrSemesterSecond.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();

            List<PupilRankingBO> lstPupilRankingBO = new List<PupilRankingBO>();
            PupilRankingBO prrSemesterAll = new PupilRankingBO();
            List<int?> lstPupilID = lstPrMax.Select(o => o.PupilID).Distinct().ToList();
            List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                                                 .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
            PupilProfile pp = new PupilProfile();
            foreach (var pr in lstPrMax)
            {
                PupilRankingBO prtc = new PupilRankingBO();
                pp = lstPupilProfile.FirstOrDefault(o => o.PupilProfileID == pr.PupilID);
                if (pp != null)
                {
                    prrSemesterAll = lstPrSemesterAll.FirstOrDefault(o => o.PupilID == pr.PupilID);
                    prtc.PupilCode = pp.PupilCode;
                    prtc.FullName = pp.FullName;
                    prtc.ProfileStatus = pp.ProfileStatus;
                    prtc.HonourAchivementTypeID = null;
                    prtc.BirthDate = pp.BirthDate;
                    prtc.Name = pp.Name;
                    prtc.ClassName = pr.ClassName;
                    prtc.Genre = pp.Genre;
                    prtc.ClassID = pr.ClassID;
                    prtc.SchoolID = pr.SchoolID;
                    prtc.AcademicYearID = pr.AcademicYearID;
                    prtc.EducationLevelID = pr.EducationLevelID;
                    prtc.PupilID = pr.PupilID;
                    prtc.ConductLevelID = prrSemesterAll != null ? prrSemesterAll.ConductLevelID : null;
                    prtc.ConductLevel = prrSemesterAll != null ? prrSemesterAll.ConductLevelName != null ? prrSemesterAll.ConductLevelName : "" : "";
                    prtc.TotalAbsentDaysWithPermission = prrSemesterAll != null ? prrSemesterAll.TotalAbsentDaysWithPermission : 0;
                    prtc.TotalAbsentDaysWithoutPermission = prrSemesterAll != null ? prrSemesterAll.TotalAbsentDaysWithoutPermission : 0;
                    prtc.Year = pr.Year;
                    if (pr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        prtc.CapacityLevelID = pr.CapacityLevelID;
                        prtc.CapacityLevel = pr.CapacityLevel;
                        prtc.StudyingJudgementID = pr.StudyingJudgementID;
                    }
                    lstPupilRankingBO.Add(prtc);
                }
            }

            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AcademicYearID"] = AcademicYearID;
            if (ListClassID.Count() == 1)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC).Where(o => ListClassID.Contains(o.ClassID)).ToList();

            var lstPrtc2 = (from su in lstPupilRankingBO
                            join poc in iqPupilOfClass on new { PupilID = su.PupilID.Value, ClassID = su.ClassID.Value } equals new { poc.PupilID, poc.ClassID }
                            where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            orderby ListClassID.Count() == 1 ? poc.OrderInClass : 0, su.Name, su.FullName
                            select su).ToList();
            List<CapacityLevel> lstCapacity = CapacityLevelBusiness.All
                                              .Where(o => o.IsActive == true).ToList();
            Dictionary<int, int?> dicCapacity = new Dictionary<int, int?>();
            foreach (int ClassID in ListClassID)
            {
                Dictionary<int, int?> dicTemp = this.CategoryOfPupilAfterRetest(ClassID, lstPrtc2);
                foreach (int key in dicTemp.Keys)
                {
                    dicCapacity[key] = dicTemp[key];
                }
            }
            foreach (PupilRankingBO bo in lstPrtc2.ToList())
            {
                if (dicCapacity.ContainsKey(bo.PupilID.Value))
                {
                    bo.CapacityLevelID = dicCapacity[bo.PupilID.Value];
                    bo.CapacityLevel = lstCapacity.FirstOrDefault(o => o.CapacityLevelID == bo.CapacityLevelID).Description;
                }
                else
                {
                    bo.CapacityLevelID = null;
                    bo.CapacityLevel = "";
                }
            }
            return lstPrtc2;
        }

        /// <summary>
        /// Từ xếp loại giáo dục và hạnh kiểm của học sinh => thuộc diện sau thi lại
        /// namdv3
        /// </summary>
        /// <param name="lstPupilRankingBO"></param>
        public void ClassifyPupilPrimaryAfterRetest(List<PupilRankingBO> lstPupilRankingBO, int SchoolID, int AcademicYearID)
        {
            //Step 1
            if (lstPupilRankingBO != null && lstPupilRankingBO.Count > 0)
            {
                //Lấy từng phần tử trong lstPupilToRank.
                List<int> lstPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();
                List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                                                     .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
                List<PupilRanking> lstPupilRanking = PupilRankingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> {
                                                            {"AcademicYearID", AcademicYearID },
                                                            {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING}})
                                                        .Where(o => lstPupilID.Contains(o.PupilID) && o.PeriodID == null).ToList();
                PupilProfile item = new PupilProfile();
                foreach (var PupilRankingBO in lstPupilRankingBO)
                {
                    //Nếu xếp loại giáo trung binh
                    if (PupilRankingBO.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_NORMAL)
                    {
                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                    }
                    //Nếu xếp loại giáo dục yếu
                    if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_WEAK)
                    {
                        //Chỉ cho thi lại 1 lần nên không xét số lần thi lại nữa
                        var flag = true;//PupilRetestRegistrationBusiness.CheckNumberOfRetest(PupilRankingBO.AcademicYearID.Value, PupilRankingBO.SchoolID.Value, PupilRankingBO.ClassID.Value, PupilRankingBO.PupilID.Value);
                        if (!flag)
                        {
                            //Học sinh có học lực yếu thuộc diện thi lại
                            //Nếu xếp loại giáo dục là yếu
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_RETEST;
                        }
                        else if (flag)
                        {
                            //Học sinh có học lực yếu thuộc diện ở lại lớp
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                        }
                    }
                    //Nếu hạnh kiểm chưa thực hiện đầy đủ
                    if (PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_CD)
                    {
                        if (PupilRankingBO.Semester != 4)
                        {
                            //Học sinh có hạnh kiểm thực hiện chưa đầy đủ thì thuộc diện rèn luyện lại
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING;
                        }
                        else if (PupilRankingBO.Semester == 4)
                        {
                            PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                        }
                    }
                    //Step2: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking nếu tìm thấy bản ghi.
                    var pr = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                    if (pr != null)
                    {
                        pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                        pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                        pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                        this.Update(pr);
                    }
                    else
                    {
                        //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                        PupilRanking _pr = new PupilRanking();
                        _pr.PupilID = PupilRankingBO.PupilID.Value;
                        _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                        _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                        _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                        _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                        _pr.PeriodID = PupilRankingBO.PeriodID;
                        _pr.Semester = PupilRankingBO.Semester;
                        _pr.CreatedAcademicYear = PupilRankingBO.Year.Value;
                        _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value % 100;
                        _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                        _pr.ClassID = PupilRankingBO.ClassID.Value;
                        _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                        _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                        this.Insert(_pr);
                    }
                }
            }
        }

        /// <summary>
        /// Lấy danh sách học sinh để thực hiện xếp loại học sinh sau thi lại cấp 2,3
        /// Chưa gán hết các thuộc tính vào đối tượng PupilRankingBO. Ai gọi hàm này thì tự thêm nhé.
        /// namdv3
        /// </summary>
        /// <param name="dic">
        /// AcademicYearID 
        ///EducationLevelID
        ///SchoolID
        ///ListClassID
        ///</param>
        /// <returns></returns>
        public List<PupilRankingBO> GetPupilToRankAfterRetest(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int _appliedlevel = Utils.GetInt(dic, "AppliedLevel");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN", false);
            int modSchoolID = SchoolID % 100;
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);

            var lstPrr = this.PupilRetestRegistrationRepository.All
                                       .Where(u => u.SchoolID == SchoolID
                                                   && u.AcademicYearID == AcademicYearID
                                                   && ListClassID.Contains(u.ClassID));

            List<PupilRetestRegistration> lstPrrAll = lstPrr.ToList();

            //Danh sach hoc sinh ren luyen lai
            List<int> lstPupilBacktrain = (from p in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID
                                                                            && u.CreatedAcademicYear == ac.Year
                                                                            && u.SchoolID == SchoolID
                                                                            && u.AcademicYearID == AcademicYearID
                                                                            && ListClassID.Contains(u.ClassID) && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                            && (u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING
                                                                                || u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST))
                                           select p.PupilID).ToList();

            //Danh sach hoc sinh thi lai va ren luyen lai
            List<int> lstPupilIdBackTrain = lstPrrAll.Select(c => c.PupilID).Union(lstPupilBacktrain).Distinct().ToList();

            //Danh sacsh hoc sinh thi lai
            var lstPr = from q in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && ListClassID.Contains(u.ClassID) && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                        join l in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID && c.IsActive == true) on q.ClassID equals l.ClassProfileID
                        join n in PupilProfileBusiness.AllNoTracking.Where(p => p.IsActive) on q.PupilID equals n.PupilProfileID
                        join pc in PupilOfClassBusiness.AllNoTracking.Where(p => p.AcademicYearID == ac.AcademicYearID && p.SchoolID == ac.SchoolID) on n.PupilProfileID equals pc.PupilID
                        join r in CapacityLevelBusiness.All on q.CapacityLevelID equals r.CapacityLevelID into g1
                        from j1 in g1.DefaultIfEmpty()
                        join s in ConductLevelBusiness.All on q.ConductLevelID equals s.ConductLevelID into g2
                        from j2 in g2.DefaultIfEmpty()
                        join m in StudyingJudgementBusiness.All on q.StudyingJudgementID equals m.StudyingJudgementID into g3
                        from j3 in g3.DefaultIfEmpty()
                        where q.PeriodID == null
                        && (isVNEN == true && (!l.IsVnenClass.HasValue || (l.IsVnenClass.HasValue && l.IsVnenClass.Value == false)))
                        && lstPupilIdBackTrain.Contains(q.PupilID)
                        && l.ClassProfileID == pc.ClassID
                        && pc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                        select new PupilRankingBO
                        {
                            PupilRankingID = q.PupilRankingID,
                            PupilID = q.PupilID,
                            ClassID = q.ClassID,
                            SchoolID = q.SchoolID,
                            AcademicYearID = q.AcademicYearID,
                            TotalAbsentDaysWithoutPermission = q.TotalAbsentDaysWithoutPermission,
                            TotalAbsentDaysWithPermission = q.TotalAbsentDaysWithPermission,
                            Semester = q.Semester,
                            PeriodID = q.PeriodID,
                            EducationLevelID = q.EducationLevelID,
                            ClassName = l.DisplayName,
                            ConductLevelID = q.ConductLevelID,
                            CapacityLevelID = q.CapacityLevelID,
                            ConductLevelName = j2.Resolution,
                            CapacityLevel = j1.CapacityLevel1,
                            StudyingJudgementID = q.StudyingJudgementID,
                            StudyingJudgementResolution = j3.Resolution,
                            PupilCode = n.PupilCode,
                            FullName = n.FullName,
                            BirthDate = n.BirthDate,
                            Genre = n.Genre,
                            ProfileStatus = pc.Status,
                            Name = n.Name,
                            LearningType = n.PupilLearningType,
                            SectionKeyID = l.SeperateKey,
                            IsBackTrain = q.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING ? true : false
                        };


            var lstPrMax = lstPr.Where(u => u.Semester == lstPr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();

            // Chi lay HS co Hoc luc Ca nam la thi lai hoac ren luyen lai
            var lstPrSemesterAll = lstPr.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                && (o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST
                                                     || o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                                    ).ToList();

            List<PupilRankingBO> lstPupilRankingBO = new List<PupilRankingBO>();
            PupilRankingBO prrSemesterMax = new PupilRankingBO();
            List<int?> lstPupilID = lstPrSemesterAll.Select(o => o.PupilID).ToList();
            /*List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                                                 .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();*/

            string strGDTX = "GDTX";
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
            bool isGDTX = (sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX);
            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            
            int AppliedLevel = 0;
            if (edu != null)
                AppliedLevel = edu.Grade;
            else
                AppliedLevel = _appliedlevel;
            bool isAbsentContest = false;
            int countPupil = 0;

            // Danh sach Hs tong ket ca nam
            foreach (var pr in lstPrSemesterAll)
            {
                isAbsentContest = lstPrr.Where(p => p.PupilID == pr.PupilID && p.IsAbsentContest.HasValue && p.IsAbsentContest.Value).Count() > 0;
                countPupil = lstPrr.Where(p => p.PupilID == pr.PupilID && (!p.IsAbsentContest.HasValue || !p.IsAbsentContest.Value)).Count();
                pr.IsKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, pr.LearningType, pr.BirthDate);
                prrSemesterMax = lstPrMax.FirstOrDefault(o => o.PupilID == pr.PupilID);
                if (prrSemesterMax != null)
                {
                    //Doi voi hoc sinh co RLL nhung chua co ket qua RLL thi khong hien thi du lieu de xep loai lai
                    if (prrSemesterMax.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && prrSemesterMax.StudyingJudgementID == GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING)
                    {
                        pr.ConductLevelID = null;
                        pr.ConductLevel = null;
                    }
                    else
                    {
                        pr.ConductLevelID = prrSemesterMax.ConductLevelID;
                        pr.ConductLevel = prrSemesterMax.ConductLevelName;
                    }

                    //Doi voi hoc sinh thi lai thi chi lay hoc luc sau thi lai
                    if (prrSemesterMax.StudyingJudgementID == GlobalConstants.STUDYING_JUDGEMENT_RETEST && prrSemesterMax.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                    {
                        pr.CapacityLevel = null;
                        pr.CapacityLevelID = null;
                    }
                    else
                    {
                        pr.CapacityLevel = prrSemesterMax.CapacityLevel;
                        pr.CapacityLevelID = prrSemesterMax.CapacityLevelID;
                    }
                    if (isAbsentContest)
                    {
                        pr.StudyingJudgementID = prrSemesterMax != null ? prrSemesterMax.StudyingJudgementID : null;
                        pr.StatusName = "Vắng thi";
                    }
                    else if (countPupil == 0)
                    {
                        pr.StatusName = "Chưa đăng ký thi";
                        pr.StudyingJudgementID = prrSemesterMax.StudyingJudgementID;
                    }
                    else
                    {
                        if ((pr.IsKXLHK || pr.ConductLevelID.HasValue) && pr.CapacityLevelID.HasValue)
                        {
                            pr.StudyingJudgementID = prrSemesterMax.StudyingJudgementID;
                        }
                        else
                        {
                            pr.StudyingJudgementID = null;
                        }

                        if (!prrSemesterMax.StudyingJudgementID.HasValue || (prrSemesterMax.StudyingJudgementID.HasValue && prrSemesterMax.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST))
                        {
                            pr.StatusName = "Đã đăng ký";
                        }
                        else if (prrSemesterMax.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                        {
                            pr.StatusName = "Rèn luyện lại";
                        }
                    }
                }
                lstPupilRankingBO.Add(pr);

            }

            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AcademicYearID"] = AcademicYearID;
            if (ListClassID.Count() == 1)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC).Where(o => ListClassID.Contains(o.ClassID)).ToList();

            var lstPrtc2 = (from su in lstPupilRankingBO
                            join poc in iqPupilOfClass on new { PupilID = su.PupilID.Value, ClassID = su.ClassID.Value } equals new { poc.PupilID, poc.ClassID }
                            where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            orderby ListClassID.Count() == 1 ? poc.OrderInClass : 0, su.Name, su.FullName
                            select su).ToList();

            return lstPrtc2;
        }

        public List<PupilRankingBO> GetListPupilRetest(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");
            int Status = Utils.GetInt(dic, "Status");
            int StudyingJudgementID = Utils.GetInt(dic, "StudyingJudgementID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["AppliedLevel"] = AppliedLevel;
            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(SchoolID, search)
                                                               .AddCriteriaSemester(AcademicYearBusiness.Find(AcademicYearID), SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["AppliedLevel"] = AppliedLevel;
            IQueryable<PupilRetestRegistration> iqPupilRetestRegistration = PupilRetestRegistrationBusiness.SearchBySchool(SchoolID, search);

            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["AppliedLevel"] = AppliedLevel;
            search["Semester"] = Semester;
            search["StudyingJudgementID"] = StudyingJudgementID;
            IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(SchoolID, search).Where(o => o.PeriodID == null);
            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["AppliedLevel"] = AppliedLevel;
            search["Semester"] = Semester;
            IQueryable<VSummedUpRecord> iqSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, search).Where(o => o.PeriodID == null);


            var lstPupilRetest = from pc in lsPupilOfClassSemester
                                 join pr in iqPupilRanking
                                        on new { pc.SchoolID, pc.AcademicYearID, pc.ClassID, pc.PupilID }
                                        equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID }
                                 join cap in CapacityLevelBusiness.All
                                 on pr.CapacityLevelID equals cap.CapacityLevelID
                                 join con in ConductLevelBusiness.All on pr.ConductLevelID equals con.ConductLevelID into cd1
                                 from cd in cd1.DefaultIfEmpty()
                                 join sur in iqSummedUpRecord
                                        on new { pc.SchoolID, pc.AcademicYearID, pc.ClassID, pc.PupilID }
                                        equals new { sur.SchoolID, sur.AcademicYearID, sur.ClassID, sur.PupilID }
                                 join Sub in SubjectCatBusiness.All
                                 on sur.SubjectID equals Sub.SubjectCatID
                                 join prr in iqPupilRetestRegistration
                                        on new { pc.SchoolID, pc.AcademicYearID, pc.ClassID, pc.PupilID }
                                        equals new { prr.SchoolID, prr.AcademicYearID, prr.ClassID, prr.PupilID } into g1
                                 join pp in PupilProfileBusiness.All
                                 on pc.PupilID equals pp.PupilProfileID
                                 from j1 in g1.DefaultIfEmpty()
                                 where pc.ClassProfile.IsVnenClass == null || pc.ClassProfile.IsVnenClass == false
                                 select new PupilRankingBO
                                 {
                                     EducationLevelID = pr.EducationLevelID,
                                     ClassName = pc.ClassProfile.DisplayName,
                                     ClassOrderNumber = pc.ClassProfile.OrderNumber,
                                     FullName = pp.FullName,
                                     SummedUpMark = sur.SummedUpMark,
                                     JudgementResult = sur.JudgementResult,
                                     AverageMark = pr.AverageMark,
                                     CapacityLevelID = cap.CapacityLevelID,
                                     CapacityLevel = cap.CapacityLevel1,
                                     ConductLevel = cd.Resolution,
                                     PupilRankingID = pr.PupilRankingID,
                                     PupilID = pr.PupilID,
                                     ClassID = pr.ClassID,
                                     SubjectID = sur.SubjectID,
                                     IsCommenting = sur.IsCommenting,
                                     SubjectName = Sub.SubjectName,
                                     SubjectNameRetest = j1.SubjectCat.SubjectName,
                                     OrderInSubject = Sub.OrderInSubject,
                                     Mobile = pc.PupilProfile.Mobile,
                                     OrderInClass = pc.OrderInClass,
                                 };
            return lstPupilRetest.ToList();
        }


        //Lay ra ID cua hoc sinh duoc len lop theo khoi
        //Nghiep vu ket chuyen du lieu
        //dung de join voi danh sach hoc sinh dang hoc trong lop
        //PhuongTD5
        public List<PupilForwardingBO> GetPupilUpClass(int EducationLevelID, int AcademicYearID, List<int> lstClassID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            int year = academicYear.Year;
            int modSchoolID = academicYear.SchoolID % 100;
            List<PupilForwardingBO> listPupilUp = new List<PupilForwardingBO>();
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
            };
            if (EducationLevelID > MAX_EDUCATIONLEVEL_PRIMARY)
            {
                listPupilUp = (from prr in VPupilRankingBusiness.All.Where(o => o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year)
                               join cp in ClassProfileBusiness.All on prr.ClassID equals cp.ClassProfileID
                               where prr.AcademicYearID == AcademicYearID
                               && prr.EducationLevelID == EducationLevelID
                               && prr.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                               && prr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS
                               && lstClassID.Contains(prr.ClassID)
                               && (cp.IsVnenClass == null || cp.IsVnenClass == false)
                               && cp.IsActive.Value
                               select new PupilForwardingBO()
                               {
                                   PupilID = prr.PupilID,
                                   ClassID = prr.ClassID
                               }).ToList();

                //viethd4: Bo sung lop VNEN
                List<PupilForwardingBO> listPupilUpVNEN = (from rb in ReviewBookPupilBusiness.All
                                                           join cp in ClassProfileBusiness.All on rb.ClassID equals cp.ClassProfileID
                                                           where rb.AcademicYearID == AcademicYearID
                                                           && rb.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                           && (rb.RateAndYear == 1 || rb.RateAdd == 1)
                                                           && lstClassID.Contains(rb.ClassID)
                                                           && (cp.IsVnenClass == true)
                                                           && cp.IsActive.Value
                                                           select new PupilForwardingBO()
                                                           {
                                                               PupilID = rb.PupilID,
                                                               ClassID = rb.ClassID
                                                           }).ToList();

                listPupilUp = listPupilUp.Union(listPupilUpVNEN).ToList();
            }
            else
            {
                listPupilUp = (from sde in SummedEndingEvaluationBusiness.All
                               join poc in PupilOfClassBusiness.SearchBySchool(academicYear.SchoolID, dicSearch) on sde.PupilID equals poc.PupilID
                               where sde.LastDigitSchoolID == modSchoolID
                               && sde.SchoolID == academicYear.SchoolID
                               && sde.AcademicYearID == AcademicYearID
                               && ((sde.EndingEvaluation != null && "HT".Contains(sde.EndingEvaluation))
                                         || (sde.RateAdd != null && sde.RateAdd == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))
                               && lstClassID.Contains(sde.ClassID)
                               && sde.SemesterID >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                               select new PupilForwardingBO()
                               {
                                   PupilID = sde.PupilID,
                                   ClassID = sde.ClassID,
                               }).ToList();
            }


            return listPupilUp.Distinct().ToList();
        }

        public int RankClass(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? Period, bool isShowRetetResult, string auto)
        {
            Dictionary<int, int> retValue = new Dictionary<int, int>();

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            //lay danh sach hoc sinh dang ky mon chuyen,mon tu chon
            IDictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"ClassID",ClassID},
                //{"SemesterID",Semester},
                {"YearID",academicYear.Year}
            };

            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegister);

            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = AcademicYearID;
            dicSubject["IsApprenticeShipSubject"] = false;
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL) dicSubject["Semester"] = Semester;
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject).ToList();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstClassSubject = lstClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0).ToList();
            }
            //loc ra nhung mon tang cuong
            List<int?> lstInCreaseID = lstClassSubject.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease).ToList();
            lstClassSubject = lstClassSubject.Where(p => !lstInCreaseID.Contains(p.SubjectID)).ToList();
            List<SummedUpRecordBO> lstSummedUp = SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, Period)
                                                                    .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

            //neu hoc ky la Ca nam thi lay them danh sach diem trung binh mon cua hoc ky 1
            List<SummedUpRecordBO> listSummedUpHKI = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                ? SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, ClassID, null).ToList()
                                                                : new List<SummedUpRecordBO>();

            // Lay danh sach hoc sinh mien giảm
            // ky 1
            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            // ky 2
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();

            List<int> lstPupilID = lstSummedUp.Select(u => u.PupilID).Distinct().ToList();

            List<PupilRanking> lstPupilRanking = new List<PupilRanking>();
            List<SummedUpRecord> lstSummedUpRanking = new List<SummedUpRecord>();

            List<PupilRanking> listExistedRanking = this.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID }, { "Semester", Semester }, { "PeriodID", Period } }).ToList();
            List<PupilRanking> listExistedBackTraining = new List<PupilRanking>();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && isShowRetetResult)
            {
                listExistedBackTraining = this.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING }, { "PeriodID", null } }).ToList();
            }

            List<PupilRankingClassBO> lstPupilForRanking = new List<PupilRankingClassBO>();
            List<RegisterSubjectSpecializeBO> lstRegistmp = null;
            foreach (int pupilID in lstPupilID)
            {
                IEnumerable<SummedUpRecordBO> lstSummedOfPupil = lstSummedUp.Where(u => u.PupilID == pupilID);
                PupilRanking pocExistedRanking = listExistedRanking.FirstOrDefault(u => u.PupilID == pupilID);

                PupilRanking pocRanking = null;
                if (isShowRetetResult)
                {
                    pocRanking = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? (listExistedBackTraining.FirstOrDefault(u => u.PupilID == pupilID) ?? pocExistedRanking) : pocExistedRanking;
                }
                else
                {
                    pocRanking = pocExistedRanking;
                }

                PupilRankingClassBO pupilForRanking = new PupilRankingClassBO();
                pupilForRanking.SchoolID = SchoolID;
                pupilForRanking.AcademicYearID = AcademicYearID;
                pupilForRanking.ClassID = ClassID;
                pupilForRanking.Semester = Semester;
                pupilForRanking.PeriodID = Period;
                pupilForRanking.PupilID = pupilID;
                pupilForRanking.CapacityLevelID = pocRanking != null ? pocRanking.CapacityLevelID : null;
                pupilForRanking.ConductLevelID = pocRanking != null ? pocRanking.ConductLevelID : null;
                pupilForRanking.ListSubject = new List<PupilRankingSubjectBO>();
                lstRegistmp = lstRegisterSubjectBO.Where(p => p.PupilID == pupilID).ToList();

                #region Nếu là HK 1 hoặc HK 2
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    foreach (ClassSubject classSubject in lstClassSubject)
                    {
                        if ((classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                            && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                            || (lstRegistmp.Count > 0 && lstRegistmp.Where(p => p.SubjectID == classSubject.SubjectID && p.SemesterID == Semester && p.SubjectTypeID == classSubject.AppliedType).Count() > 0)
                            )
                        {
                            SummedUpRecordBO surBo = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID);
                            bool isExempted = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && lstExemptedI.Any(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID)
                                            || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && lstExemptedII.Any(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID);
                            PupilRankingSubjectBO pupilRankingSubject = new PupilRankingSubjectBO();
                            pupilRankingSubject.IsCommenting = classSubject.IsCommenting.Value;
                            pupilRankingSubject.IsExempted = isExempted;
                            pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (surBo != null ? surBo.JudgementResult : null) : null;
                            pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (surBo != null ? surBo.SummedUpMark : null);
                            pupilRankingSubject.SubjectID = classSubject.SubjectID;
                            pupilRankingSubject.AppliedType = classSubject.AppliedType;

                            pupilForRanking.ListSubject.Add(pupilRankingSubject);
                        }
                    }
                }
                #endregion

                #region Nếu là cả năm
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    foreach (ClassSubject classSubject in lstClassSubject)
                    {
                        if ((classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                            && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                            || (lstRegistmp.Count > 0 && lstRegistmp.Where(p => p.SubjectID == classSubject.SubjectID && p.SubjectTypeID == classSubject.AppliedType).Count() > 0))
                        {

                            SummedUpRecordBO summedUp = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                            SummedUpRecordBO summedRetest = null;
                            if (isShowRetetResult)
                            {
                                summedRetest = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST);
                            }

                            bool isExemptedI = lstExemptedI.Any(w => w.PupilID == pupilID && w.SubjectID == classSubject.SubjectID);
                            bool isExemptedII = lstExemptedII.Any(w => w.PupilID == pupilID && w.SubjectID == classSubject.SubjectID);

                            PupilRankingSubjectBO pupilRankingSubject = new PupilRankingSubjectBO();
                            pupilRankingSubject.IsCommenting = classSubject.IsCommenting.Value;
                            pupilRankingSubject.IsExempted = isExemptedI && isExemptedII;
                            pupilRankingSubject.SubjectID = classSubject.SubjectID;
                            pupilRankingSubject.AppliedType = classSubject.AppliedType;

                            //Nếu được miễn giảm cả năm
                            if (isExemptedI && isExemptedII)
                            {
                                pupilRankingSubject.RankingJudgement = null;
                                pupilRankingSubject.RankingMark = null;
                            }

                            //Nếu không miễn giảm học kỳ nào
                            if (!isExemptedI && !isExemptedII)
                            {
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedRetest != null ? summedRetest.ReTestMark : (summedUp != null ? summedUp.SummedUpMark : null));
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedRetest != null ? summedRetest.ReTestJudgement : (summedUp != null ? summedUp.JudgementResult : null)) : null;
                            }

                            //Nếu chỉ miễn giảm học kỳ 1 hoặc không học trong hk 1 
                            if (isExemptedI || !classSubject.SectionPerWeekFirstSemester.HasValue || classSubject.SectionPerWeekFirstSemester.Value == 0)
                            {
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedRetest != null ? summedRetest.ReTestMark : (summedUp != null ? summedUp.SummedUpMark : null));
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedRetest != null ? summedRetest.ReTestJudgement : (summedUp != null ? summedUp.JudgementResult : null)) : null;
                            }

                            //Nếu chỉ được miễn giảm học kỳ 2 hoặc không học trong hk2
                            if (isExemptedII || !classSubject.SectionPerWeekSecondSemester.HasValue || classSubject.SectionPerWeekSecondSemester.Value == 0)
                            {
                                SummedUpRecordBO summedHKI = listSummedUpHKI.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID);
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedHKI != null ? summedHKI.SummedUpMark : null);
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedHKI != null ? summedHKI.JudgementResult : null) : null;
                            }

                            pupilForRanking.ListSubject.Add(pupilRankingSubject);
                        }
                    }
                }
                #endregion

                lstPupilForRanking.Add(pupilForRanking);
            }


            List<PupilRankingClassBO> lstPupilForRankingValid = lstPupilForRanking.Where(u => (u.PeriodID.HasValue && u.ListSubject.Any(v => !v.IsExempted && (!string.IsNullOrEmpty(v.RankingJudgement) || v.RankingMark.HasValue)))
                                                                                            || (!u.PeriodID.HasValue && !u.ListSubject.Any(v => !v.IsExempted && (string.IsNullOrEmpty(v.RankingJudgement) && !v.RankingMark.HasValue)))).ToList();

            foreach (PupilRankingClassBO pupilForRanking in lstPupilForRankingValid)
            {
                PupilRanking pr = new PupilRanking();
                pr.AcademicYearID = AcademicYearID;
                pr.PupilID = pupilForRanking.PupilID;
                pr.SchoolID = classProfile.SchoolID;
                pr.ClassID = ClassID;
                pr.EducationLevelID = classProfile.EducationLevelID;
                pr.CreatedAcademicYear = academicYear.Year;
                pr.Semester = Semester;
                pr.PeriodID = Period;
                pr.CapacityLevelID = pupilForRanking.CapacityLevelID;
                pr.ConductLevelID = pupilForRanking.ConductLevelID;
                pr.MSourcedb = auto;
                lstPupilRanking.Add(pr);

                foreach (PupilRankingSubjectBO pupilRankingSubject in pupilForRanking.ListSubject.Where(u => !u.IsExempted && (!string.IsNullOrEmpty(u.RankingJudgement) || u.RankingMark.HasValue)))
                {
                    SummedUpRecord sur = new SummedUpRecord();
                    sur.AcademicYearID = AcademicYearID;
                    sur.ClassID = ClassID;
                    sur.IsCommenting = pupilRankingSubject.IsCommenting;
                    sur.PeriodID = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? Period : null;
                    sur.PupilID = pupilForRanking.PupilID;
                    sur.SchoolID = SchoolID;
                    sur.Semester = Semester;
                    sur.SubjectID = pupilRankingSubject.SubjectID;
                    sur.SummedUpDate = DateTime.Now;
                    sur.CreatedAcademicYear = academicYear.Year;
                    sur.SummedUpMark = pupilRankingSubject.RankingMark;
                    sur.JudgementResult = pupilRankingSubject.RankingJudgement;
                    sur.MSourcedb = auto;

                    lstSummedUpRanking.Add(sur);
                }
            }

            if (lstPupilRanking.Count > 0 && lstSummedUpRanking.Count > 0)
                this.RankingPupilOfClass(UserID, lstSummedUpRanking, lstPupilRanking, Period, lstClassSubject, false, GlobalConstants.NOT_AUTO_MARK, lstRegisterSubjectBO);

            return lstPupilRanking.Count;
        }


        public ProcessedReport GetProcessedReport(PupilRanking entity, string ReportCode)
        {

            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(ReportCode, inputParameterHashKey);
        }

        public string GetHashKey(PupilRanking entity)
        {
            return ReportUtils.GetHashKey(new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID} ,
                {"Semester",entity.Semester},
                {"EducationLevelID",entity.EducationLevelID}
            });
        }

        public ProcessedReport InsertProcessReport(PupilRanking entity, Stream Data, string ReportCode)
        {
            //Khởi tạo đối tượng ProcessedReport:
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = ReportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            if (Data != null)
            {
                pr.ReportData = ReportUtils.Compress(Data);
            }
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(ReportCode);
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID}
               // {"Year",entity.Year}
               // {"DistrictID",entity.DistrictID},
               // {"ProvinceID",entity.ProvinceID}
            };
            this.context.Configuration.AutoDetectChangesEnabled = true;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            ProcessedReport processReportObj = ProcessedReportBusiness.GetProcessedReport(ReportCode, pr.InputParameterHashKey);
            if (processReportObj != null)
            {
                pr.ProcessedReportID = processReportObj.ProcessedReportID;
            }

            return pr;
        }

        public List<PupilRepeaterBO> GetListPupilRepeater(int Grade, int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, bool? isVNEN = false)
        {
            int MinEducation = 0;
            int MaxEducation = 0;
            if (Grade == 1)
            {
                MinEducation = GlobalConstants.EDUCATION_LEVEL_ID1;
                MaxEducation = GlobalConstants.EDUCATION_LEVEL_ID5;
            }
            else if (Grade == 2)
            {
                MinEducation = GlobalConstants.EDUCATION_LEVEL_ID6;
                MaxEducation = GlobalConstants.EDUCATION_LEVEL_ID9;
            }
            else if (Grade == 3)
            {
                MinEducation = GlobalConstants.EDUCATION_LEVEL_ID10;
                MaxEducation = GlobalConstants.EDUCATION_LEVEL_ID12;
            }
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            List<PupilRepeaterBO> ListPupilRepeater = (from pr in VPupilRankingBusiness.All
                                                       join pp in PupilProfileBusiness.All on pr.PupilID equals pp.PupilProfileID
                                                       join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                                       join cp in ClassProfileBusiness.All on pr.ClassID equals cp.ClassProfileID
                                                       where poc.AcademicYearID == AcademicYearID
                                                       && pr.SchoolID == SchoolID
                                                       && pr.Last2digitNumberSchool == partitionId
                                                       && (pr.EducationLevelID >= MinEducation && pr.EducationLevelID < MaxEducation)
                                                       && (pr.EducationLevelID == EducationLevelID || 0 == EducationLevelID)
                                                       && (pr.ClassID == ClassID || 0 == ClassID)
                                                       && pp.IsActive == true
                                                       && pr.StudyingJudgementID == GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS
                                                       && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                       && pr.ClassID == poc.ClassID
                                                       && (isVNEN.Value && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false)))
                                                       && cp.IsActive.Value
                                                       select new PupilRepeaterBO
                                                       {
                                                           PupilID = pr.PupilID,
                                                           PupilFullName = pp.FullName,
                                                           PupilShortName = pp.Name,
                                                           ClassID = pr.ClassID,
                                                           ClassName = cp.DisplayName,
                                                           CapacityID = pr.CapacityLevelID == null ? 0 : pr.CapacityLevelID.Value,
                                                           ConductID = pr.ConductLevelID == null ? 0 : pr.ConductLevelID.Value,
                                                           Semester = pr.Semester == null ? 0 : pr.Semester.Value,
                                                           EducationLevelID = pr.EducationLevelID,
                                                           OrderClass = cp.OrderNumber,
                                                           OrderID = poc.OrderInClass
                                                       }).ToList();



            return ListPupilRepeater;
        }

        public int EducationGrade(int schoolID)
        {
            int education = 0;
            SchoolProfile schoolProfile = SchoolProfileBusiness.All.FirstOrDefault(p => p.SchoolProfileID == schoolID);
            if (schoolProfile != null)
            {
                education = schoolProfile.EducationGrade;
            }
            return education;

        }

        public List<PupilMarkBO> GetListTraningInfoToday(int SchoolID, int AcademicYearID, List<int> lstClassID,List<int> lstPupilID)
        {
            DateTime DateTimeNow = DateTime.Now;
            try
            {
                List<PupilMarkBO> listPupilTraining = new List<PupilMarkBO>();
                #region Thông tin điểm danh
                //lay len thong tin diem danh cua lop
                IDictionary<string, object> dicAbsent = new Dictionary<string, object>();
                dicAbsent["lstClassID"] = lstClassID;
                dicAbsent["AcademicYearID"] = AcademicYearID;
                dicAbsent["AbsentDate"] = DateTimeNow;
                List<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsent).Where(p => lstPupilID.Contains(p.PupilID)).OrderBy(p => p.Section).ToList();
                List<int> listPupilID = listPupilAbsence.Select(o => o.PupilID).Distinct().ToList();
                Dictionary<int, string> dicPupilContent = new Dictionary<int, string>();
                List<PupilAbsence> lstAbsenceByPupil = null;
                //Tao content cho object
                foreach (int pupilID in listPupilID)
                {
                    lstAbsenceByPupil = listPupilAbsence.Where(o => o.PupilID == pupilID).ToList();
                    string absenceInfo = string.Empty;
                    foreach (var item in lstAbsenceByPupil)
                    {
                        string reason = "không phép";
                        if (item.IsAccepted.HasValue && item.IsAccepted.Value)
                        {
                            reason = "có phép";
                        }
                        absenceInfo += this.GetSection(item.Section) + ": Nghỉ học " + reason + "\n";
                    }
                    dicPupilContent[pupilID] = absenceInfo;
                }
                #endregion
                #region Thông tin vi phạm
                //Loc thong tin vi pham cua lop
                IDictionary<string, object> dicFault = new Dictionary<string, object>();
                dicFault["lstClassID"] = lstClassID;
                dicFault["AcademicYearID"] = AcademicYearID;
                dicFault["ViolatedDate"] = DateTimeNow;
                List<PupilFault> lstPupilFault = PupilFaultBusiness.SearchBySchool(SchoolID, dicFault).Where(p => lstPupilID.Contains(p.PupilID)).ToList();

                var listFault = (from p in lstPupilFault
                                 select new
                                 {
                                     PupilID = p.PupilID,
                                     FaultName = p.FaultCriteria.Resolution,
                                     NumberOfFault = p.NumberOfFault,
                                     Note = p.Note
                                 }).Distinct().ToList().GroupBy(u => new { u.PupilID })
                                   .Select(u => new
                                   {
                                       PupilID = u.Key.PupilID,
                                       FaultName = (u.Select(v => v.FaultName + " " + v.Note + (string.IsNullOrEmpty(v.Note) ? string.Empty : " ") + (v.NumberOfFault.HasValue && v.NumberOfFault.Value > 1 ? "(" + v.NumberOfFault + ")" : string.Empty))).Aggregate((a, b) => (a + ", " + b))
                                   }).ToList();


                foreach (var f in listFault)
                {
                    if (dicPupilContent.ContainsKey(f.PupilID))
                    {
                        dicPupilContent[f.PupilID] = dicPupilContent[f.PupilID] + f.FaultName;
                    }
                    else
                    {
                        dicPupilContent[f.PupilID] = f.FaultName;
                    }
                }
                listPupilID = dicPupilContent.Keys.ToList();
                foreach (int pupilID in listPupilID)
                {
                    PupilMarkBO ppic = new PupilMarkBO();
                    ppic.PupilProfileID = pupilID;
                    ppic.Content = dicPupilContent[pupilID];
                    listPupilTraining.Add(ppic);
                }

                #endregion
                return listPupilTraining;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
        }
        public List<PupilMarkBO> GetListTraningInfoByTime(int SchoolID, int AcademicYearID, List<int> lstClassID,List<int> lstPupilID, DateTime startTime, DateTime endTime)
        {
            DateTime DateTimeNow = DateTime.Now;
            try
            {
                List<PupilMarkBO> listPupilTraining = new List<PupilMarkBO>();
                #region Thông tin điểm danh/ vi pham
                //lay len thong tin diem danh cua lop
                IDictionary<string, object> dicAbsent = new Dictionary<string, object>();
                dicAbsent["lstClassID"] = lstClassID;
                dicAbsent["AcademicYearID"] = AcademicYearID;
                // thong tin diem danh cua hoc sinh
                List<PupilAbsence> lstAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsent)
                    .Where(p => lstPupilID.Contains(p.PupilID) &&  EntityFunctions.TruncateTime(p.AbsentDate) >= EntityFunctions.TruncateTime(startTime)
                        && EntityFunctions.TruncateTime(p.AbsentDate) <= EntityFunctions.TruncateTime(endTime)).ToList();

                // thong tin vi pham cua hoc sinh
                var lstFault = (from f in PupilFaultBusiness.AllNoTracking
                                join lf in FaultCriteriaBusiness.AllNoTracking on f.FaultID equals lf.FaultCriteriaID
                                join fg in FaultGroupBusiness.AllNoTracking on lf.GroupID equals fg.FaultGroupID
                                where f.SchoolID == SchoolID
                                && f.AcademicYearID == AcademicYearID
                                && lstClassID.Contains(f.ClassID)
                                && lstPupilID.Contains(f.PupilID)
                                && EntityFunctions.TruncateTime(f.ViolatedDate) >= EntityFunctions.TruncateTime(startTime)
                                && EntityFunctions.TruncateTime(f.ViolatedDate) <= EntityFunctions.TruncateTime(endTime)
                                select new FaultByTime
                                {
                                    PupilID = f.PupilID,
                                    FaultID = f.FaultID,
                                    NumberOfFault = f.NumberOfFault,
                                    Resolution = lf.Resolution,
                                    Note = f.Note
                                }).ToList();

                List<int> pupilIds = new List<int>();
                if (lstAbsence != null && lstAbsence.Count > 0)
                {
                    pupilIds = lstAbsence.Select(p => p.PupilID).Distinct().ToList();
                }

                if (lstFault != null && lstFault.Count > 0)
                {
                    pupilIds.AddRange(lstFault.Select(p => p.PupilID).Distinct().ToList());
                    pupilIds = pupilIds.Distinct().ToList();
                }

                List<PupilAbsence> itemAbsences = null;

                //Tao content cho object
                int pupilID = 0;
                string content = string.Empty;
                int cp = 0, kp = 0;
                PupilMarkBO itemResponse = null;

                for (int i = pupilIds.Count - 1; i >= 0; i--)
                {
                    content = string.Empty;
                    itemResponse = new PupilMarkBO();
                    pupilID = pupilIds[i];
                    // diem danh
                    itemAbsences = lstAbsence.Where(o => o.PupilID == pupilID).ToList();
                    if (itemAbsences != null && itemAbsences.Count > 0)
                    {
                        cp = itemAbsences.Where(p => p.IsAccepted == true).Count();
                        kp = itemAbsences.Where(p => p.IsAccepted != true).Count();
                        if (cp > 0)
                        {
                            content += string.Format(GlobalConstantsEdu.FORMAT_0_1, string.Format(FORMAT_ABSENCE_P_0, cp), Environment.NewLine);
                        }

                        if (kp > 0)
                        {
                            content += string.Format(GlobalConstantsEdu.FORMAT_0_1, string.Format(FORMAT_ABSENCE_K_0, kp), Environment.NewLine);
                        }
                    }

                    //Vi pham
                    var itemFaults = lstFault.Where(a => a.PupilID == pupilID).GroupBy(l => new { l.FaultID, l.Resolution })
                            .Select(g => new
                            {
                                FaultID = g.Key.FaultID,
                                Resolution = g.Key.Resolution,
                                Note = string.Join(", ", g.Where(o => !string.IsNullOrWhiteSpace(o.Note))
                                    .Select(x => x.Note).Distinct()),
                                Number = g.Sum(x => x.NumberOfFault)
                            }).ToList();


                    if (itemFaults != null && itemFaults.Count > 0)
                    {
                        for (int ik = 0, iksize = itemFaults.Count; ik < iksize; ik++)
                        {
                            var itemFaulst = itemFaults[ik];
                            if (itemFaulst.Number > 1)
                            {
                                content += string.Format("{0}{1} ({2}), ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note, itemFaulst.Number);
                            }
                            else
                            {
                                content += string.Format("{0}{1}, ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note);
                            }


                        }

                        // cat bo dau phay
                        content = content.Substring(0, content.Length - 2);
    }

                    itemResponse.PupilProfileID = pupilID;
                    itemResponse.Content = content;
                    listPupilTraining.Add(itemResponse);
                }
                #endregion

                return listPupilTraining;
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
}
        public string GetSection(int Section)
        {
            if (Section == 1) return GlobalConstants.SECTION_MONING;
            if (Section == 2) return GlobalConstants.SECTION_AFTERNOON;
            if (Section == 3) return GlobalConstants.SECTION_EVENING;
            return string.Empty;
        }
        private class FaultByTime
        {
            public int PupilID { get; set; }

            public int FaultID { get; set; }

            public int? NumberOfFault { get; set; }

            public string Resolution { get; set; }

            public string Note { get; set; }
        }
    }
}
public class PupilRankingComparer : IEqualityComparer<PupilRanking>
{
    public bool Equals(PupilRanking x, PupilRanking y)
    {
        return x.PupilRankingID.Equals(y.PupilRankingID);
    }

    public int GetHashCode(PupilRanking obj)
    {
        return obj.GetHashCode();
    }
}
