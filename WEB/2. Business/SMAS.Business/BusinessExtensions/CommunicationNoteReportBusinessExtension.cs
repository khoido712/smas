﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Security.Application;

namespace SMAS.Business.Business
{
    public partial class CommunicationNoteReportBusiness
    {
        public readonly int firstColumn = 1;
        public readonly int lastColumn = 19;
   
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        #region Phieu lien lac tieu hoc
        public ProcessedReport GetCommunicationNoteReport(IDictionary<string, object> dic)
        {
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            string reportCode = string.Empty;
            if (SemesterID == 1 || SemesterID == 3)//Giua HKI + Giua HKII
            {
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_GHKI;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_GHKI;
                }

            }
            else if (SemesterID == 2 || SemesterID == 4)//Cuoi HKI + Cuoi HKII
            {
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_HKII;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_HKII;
                }

            }
            
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport InsertCommunicationNoteReport(IDictionary<string, object> dic, Stream data)
        {

            int SemesterID = Utils.GetInt(dic["SemesterID"]);
            int ClassID = Utils.GetInt(dic["ClassID"]);
            int EducationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            string reportCode = string.Empty;
            string SemesterTypeName = string.Empty;
            if (SemesterID == 1 || SemesterID == 3)//Giua HKI + Giua HKII
            {
                SemesterTypeName = SemesterID == 1 ? "GHKI" : "GHKII";
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_GHKI;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_GHKI;
                }

            }
            else if (SemesterID == 2 || SemesterID == 4)//Cuoi HKI + Cuoi HKII
            {
                SemesterTypeName = SemesterID == 2 ? "CHKI" : "CHKII";
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_HKII;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_HKII;
                }

            }
            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            string outputNamePattern = reportDef.OutputNamePattern;

            ClassProfile cp = ClassProfileBusiness.Find(ClassID);
            string className = string.Empty;
            string EducationLevelName = "Khoi" + EducationLevelID;
            if (cp != null)
            {
                className = "_" + cp.DisplayName;
            }
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevelName);
            outputNamePattern = outputNamePattern.Replace("[ClassName]", className);
            outputNamePattern = outputNamePattern.Replace("[SemesterType]", SemesterTypeName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return pr;
        }
        public Stream CreateCommunicationNoteReport(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int SchoolID = Utils.GetInt(dic["SchoolID"]);
            int Partition = UtilsBusiness.GetPartionId(SchoolID);
            int AppliedLevelID = Utils.GetInt(dic["AppliedLevel"]);
            int UserAccountID = Utils.GetInt(dic["UserAccountID"]);
            int SemesterType = Utils.GetInt(dic["SemesterID"]);
            int EducationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            int ClassID = Utils.GetInt(dic["ClassID"]);
            int SemesterID = 0;
            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            DateTime semesterStartDate;
            DateTime semesterEndDate;
            if (SemesterType == 1 || SemesterType == 2)
            {
                semesterStartDate = ay.FirstSemesterStartDate.GetValueOrDefault();
                semesterEndDate = ay.FirstSemesterEndDate.GetValueOrDefault();
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                semesterStartDate = ay.SecondSemesterStartDate.GetValueOrDefault();
                semesterEndDate = ay.SecondSemesterEndDate.GetValueOrDefault();
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            string reportCode = string.Empty;
            string TitleSemester1 = string.Empty;
            string TitleSemester2 = string.Empty;
            if (SemesterType == 1 || SemesterType == 3)//Giua HKI + Giua HKII
            {
                if (SemesterType == 1)
                {
                    TitleSemester1 = "Giữa học kỳ I";
                    TitleSemester2 = "Cuối học kỳ I";
                }
                else
                {
                    TitleSemester1 = "Giữa học kỳ II";
                    TitleSemester2 = "Cuối học kỳ II";
                }
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_GHKI;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_GHKI;
                }

            }
            else if (SemesterType == 2 || SemesterType == 4)//Cuoi HKI + Cuoi HKII
            {
                if (SemesterType == 2)
                {
                    TitleSemester1 = "Giữa học kỳ I";
                    TitleSemester2 = "Cuối học kỳ I";
                }
                else
                {
                    TitleSemester1 = "Giữa học kỳ II";
                    TitleSemester2 = "Cuối học kỳ II";
                }
                if (EducationLevelID < 4)
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K1_HKII;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE_K5_HKII;
                }

            }
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            //fill sheet thong tin chung
            thirdSheet.SetCellValue("B3", school.SchoolName.ToUpper());
            string strSemester = (SemesterType == 1 ? "GIỮA KỲ I" : SemesterType == 2 ? "HỌC KỲ I" : SemesterType == 3 ? "GIỮA KỲ II" : SemesterType == 4 ? "HỌC KỲ II" : string.Empty) + " - năm học ";
            string Title = "Phiếu liên lạc " + strSemester + ay.DisplayTitle;
            thirdSheet.SetCellValue("B4", Title.ToUpper());
            thirdSheet.SetCellValue("B6", school.HeadMasterName);
            string TitleDate = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            thirdSheet.SetCellValue("B7", TitleDate);
            thirdSheet.SetCellValue("B8", TitleSemester1);
            thirdSheet.SetCellValue("B9", TitleSemester2);

            int firstRow = 2;

            //Lay range mau cho 1 hoc sinh
            IVTRange templateHeaderRange;
            IVTRange templateFooterRange;
            IVTRange templateLearningRange;
            if (SemesterType == 1 || SemesterType == 3)
            {
                templateHeaderRange = firstSheet.GetRange(1, firstColumn, 9, lastColumn);
                templateLearningRange = firstSheet.GetRange(10, firstColumn, 15, lastColumn);
                templateFooterRange = firstSheet.GetRange(17, firstColumn, 35, lastColumn);
            }
            else
            {
                templateHeaderRange = firstSheet.GetRange(1, firstColumn, 10, lastColumn);
                templateLearningRange = firstSheet.GetRange(11, firstColumn, 22, lastColumn);
                templateFooterRange = firstSheet.GetRange(24, firstColumn, 54, lastColumn);
            }
            

            //Lay danh sach hoc sinh cua toan khoi
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = EducationLevelID;
            tmpDic["SchoolID"] = SchoolID;
            tmpDic["AcademicYearID"] = AcademicYearID;
            tmpDic["AppliedLevel"] = AppliedLevelID;
            tmpDic["ClassID"] = ClassID;
            tmpDic["Status"] = new List<int> { GlobalConstants.PUPIL_STATUS_STUDYING };

            IQueryable<PupilOfClass> lstPocEdu = PupilOfClassBusiness.Search(tmpDic).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);
            List<int> lstPupilID = lstPocEdu.Select(p => p.PupilID).Distinct().ToList();
            //Lay danh sach lop
            List<ClassProfile> lstClass;
            IQueryable<ClassProfile> query = ClassProfileBusiness.getClassByRoleAppliedLevel(AcademicYearID, SchoolID, AppliedLevelID, UserAccountID)
               .Where(o => o.EducationLevelID == EducationLevelID);
            if (ClassID != 0)
            {
                query = query.Where(o => o.ClassProfileID == ClassID);
            }
            lstClass = query.OrderBy(o => o.OrderNumber).ToList();
            List<int> lstClassID = lstClass.Select(p=>p.ClassProfileID).Distinct().ToList();

            List<int> lstHeadTeacherId = lstClass.Select(o => o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0).ToList();

            List<Employee> lstHeadTeacher = EmployeeBusiness.All.Where(o => o.SchoolID == SchoolID && lstHeadTeacherId.Contains(o.EmployeeID)).ToList();

            //Danh sach mon hoc
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = SchoolID;
            tmpDic["AcademicYearID"] = AcademicYearID;
            tmpDic["Semester"] = SemesterID;
            tmpDic["EducationLevelID"] = EducationLevelID;
            tmpDic["ClassID"] = ClassID;
            tmpDic["ListClassID"] = lstClassID;

            List<ClassSubject> lstClassSubjectEdu = ClassSubjectBusiness.SearchBySchool(SchoolID, tmpDic).OrderBy(o => o.IsCommenting).ThenBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            int healthPeriodId = SemesterID == 1 ? 1 : 2;
            // Lấy tất cả các thông số về sức khỏe và số ngày nghỉ của học sinh
            List<InfoEvaluationPupil> lstPupilInfoEdu = (from pt in PhysicalTestBusiness.All
                                                         join mn in MonitoringBookBusiness.All on pt.MonitoringBookID equals mn.MonitoringBookID
                                                         where mn.AcademicYearID == AcademicYearID
                                                         && mn.SchoolID == SchoolID
                                                         && mn.EducationLevelID == EducationLevelID
                                                         && mn.HealthPeriodID == healthPeriodId
                                                         && (mn.ClassID == ClassID || lstClassID.Contains(mn.ClassID))
                                                         && lstPupilID.Contains(mn.PupilID)
                                                         orderby mn.MonitoringDate descending
                                                         select new InfoEvaluationPupil
                                                         {
                                                             PupilID = mn.PupilID,
                                                             Height = pt.Height,
                                                             Weight = pt.Weight,
                                                             EvaluationHealth = pt.PhysicalClassification,
                                                             AcademicYear = mn.AcademicYearID,
                                                             SchoolID = mn.SchoolID,
                                                             ClassID = mn.ClassID,
                                                             MonitoringDate = mn.MonitoringDate

                                                         }).ToList();

            //thong tin nghi hoc
            List<PupilAbsence> lstPupilAbenceEdu = PupilAbsenceBusiness.All
                .Where(o => o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && o.EducationLevelID == EducationLevelID
                && o.Last2digitNumberSchool == Partition
                && o.AbsentDate >= semesterStartDate
                && o.AbsentDate <= semesterEndDate).ToList();
            //thong tin diem va nhan xet cac mon
            
            IDictionary<string, object> dicRate = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"SchoolID",SchoolID},
                    {"ClassID",ClassID},
                    {"lstPupilID",lstPupilID}
                    //{"SemesterID",SemesterID}
                };
            //List<RatedCommentPupil>  lstRatedCommentPupil = RatedCommentPupilBusiness.Search(dicRate).ToList();


            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            if (UtilsBusiness.IsMoveHistory(ay))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dicRate)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID,
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dicRate)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID
                                        }).ToList();
            }



            IDictionary<string, object> dicSEE = new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"SchoolID",SchoolID},
                {"SemesterID",GlobalConstants.SEMESTER_OF_EVALUATION_RESULT},
                {"EvaluationID",GlobalConstants.EVALUATION_RESULT},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"lstPupilID",lstPupilID}
            };
            List<SummedEndingEvaluationBO> lstSEEBO = SummedEndingEvaluationBusiness.Search(dicSEE).ToList();


            //Thong tin khen thuong
            IDictionary<string,object> dicER = new Dictionary<string,object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID},
                {"SemesterID",SemesterID},
                {"lstPupilID",lstPupilID}
            };
            List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(dicER).ToList();

            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.OrderBy(p => p.EvaluationCriteriaID).ToList();

            for (int iClass = 0; iClass < lstClass.Count; iClass++)
            {
                ClassProfile curClass = lstClass[iClass];
                

                //Lay danh sach mon hoc cua lop
                List<ClassSubject> lstClassSubjectClass = lstClassSubjectEdu.Where(o => o.ClassID == curClass.ClassProfileID).OrderBy(o=>o.IsCommenting).ToList();

                //Lay danh sach hoc sinh cua lop
                List<PupilOfClass> lstPocClass = lstPocEdu.Where(o => o.ClassID == curClass.ClassProfileID).ToList();
                if (lstClassSubjectClass.Count == 0 || lstPocClass.Count == 0)
                {
                    continue;
                }
                IVTWorksheet curSheet = oBook.CopySheetToBeforeLast(secondSheet,"Z1000");
                //Lay gvcn
                Employee headTeacher = lstHeadTeacher.Where(o => o.EmployeeID == curClass.HeadTeacherID).FirstOrDefault();

                int pocStartRow = firstRow;
                for (int iPoc = 0; iPoc < lstPocClass.Count; iPoc++)
                {
                    PupilOfClass poc = lstPocClass[iPoc];

                    //Lay thong tin chieu cao, can nang
                    InfoEvaluationPupil info = lstPupilInfoEdu.Where(o => o.ClassID == curClass.ClassProfileID && o.PupilID == poc.PupilID).FirstOrDefault();

                    //Thong tin nghi hoc
                    List<PupilAbsence> lstPupilAbencePupil = lstPupilAbenceEdu.Where(o => o.ClassID == curClass.ClassProfileID && o.PupilID == poc.PupilID).ToList();

                    List<RatedCommentPupilBO> lstRatedCommentPupiltmp = lstRatedCommentPupil.Where(p => p.ClassID == curClass.ClassProfileID && p.PupilID == poc.PupilID).ToList();

                    SummedEndingEvaluationBO objSEEBO = lstSEEBO.Where(p => p.ClassID == curClass.ClassProfileID && p.PupilID == poc.PupilID).FirstOrDefault();


                    //Fill data for header here
                    FillHeader(pocStartRow, curSheet, templateHeaderRange, poc, info, lstPupilAbencePupil, SemesterType);

                    if (SemesterType == 1 || SemesterType == 3)
                    {
                        //Fill mon va diem cac mon
                        this.FillLearningResultGHK(pocStartRow, curSheet, templateLearningRange, lstClassSubjectClass, lstRatedCommentPupiltmp,EducationLevelID,SemesterID);
                        //Fill footer
                        FillFooterGHK(pocStartRow, curSheet, templateFooterRange, lstClassSubjectClass, lstRatedCommentPupiltmp, lstEvaluationCriteria, SemesterID, (headTeacher != null ? headTeacher.FullName : String.Empty));
                        pocStartRow += 35;
                    }
                    else
                    {
                        List<EvaluationReward> lstERtmp = lstEvaluationReward.Where(p => p.ClassID == curClass.ClassProfileID && p.PupilID == poc.PupilID).ToList();
                        //Fill mon va diem cac mon
                        this.FillLearningResultCHK(pocStartRow, curSheet, templateLearningRange, lstClassSubjectClass, lstRatedCommentPupiltmp,EducationLevelID,SemesterID);
                        //Fill footer
                        FillFooterCHK(pocStartRow, curSheet, templateFooterRange, lstClassSubjectClass, lstRatedCommentPupiltmp, objSEEBO, lstERtmp, lstEvaluationCriteria, SemesterID, SemesterType, (headTeacher != null ? headTeacher.FullName : String.Empty));
                        pocStartRow += 55;
                    }
                    
                    curSheet.SetBreakPage(pocStartRow);
                }

                curSheet.Name = ReportUtils.StripVNSign(Sanitizer.GetSafeHtmlFragment(curClass.DisplayName));
                //curSheet.SetFontName("Times New Roman", 13);
                //curSheet.SetPrintArea("$A2:$Q" + (pocStartRow - 1));
                curSheet.Worksheet.Zoom = 70;
                curSheet.FitAllColumnsOnOnePage = true;

            }

            firstSheet.Delete();
            secondSheet.Delete();
            return oBook.ToStream();
        }
        private void FillHeader(int pocStartRow, IVTWorksheet sheet, IVTRange template, PupilOfClass poc, InfoEvaluationPupil info, List<PupilAbsence> lstPupilAbsence,int SemesterTypeID)
        {
            sheet.CopyPasteSameSize(template, pocStartRow, 1);
            VTVector vtStart = new VTVector(pocStartRow, firstColumn);
            VTVector vtTitle = new VTVector(2, 0);
            VTVector vtName = new VTVector(3, 2);
            VTVector vtClass = new VTVector(3, 16);
            VTVector vtPupilHeight = new VTVector(4, 2);
            VTVector vtPupilWeight = new VTVector(4, 9);
            VTVector vtHealthy = new VTVector(4, 16);
            VTVector vtAbsence = new VTVector(5, 2);
            VTVector vtAbsenceP = new VTVector(5, 9);
            VTVector vtAbsenceK = new VTVector(5, 16);
            VTVector vtHKI = new VTVector(8,4);
            VTVector vtHKII = new VTVector(8, 9);

            sheet.SetCellValue(vtStart, "='Thong tin chung'!B3");
            sheet.SetCellValue(vtStart + vtTitle, "='Thong tin chung'!B4");
            if (SemesterTypeID == 2 || SemesterTypeID == 4)
            {
                sheet.SetCellValue(vtStart + vtHKI, "='Thong tin chung'!B8");
                sheet.SetCellValue(vtStart + vtHKII, "='Thong tin chung'!B9");
            }
            sheet.SetCellValue(vtStart + vtName, poc.PupilProfile.FullName);
            sheet.SetCellValue(vtStart + vtClass, poc.ClassProfile.DisplayName);
            sheet.SetCellValue(vtStart + vtPupilHeight, info != null ? (info.Height % 1 == 0 ? info.Height + " cm" : string.Format("{0:0.0}", info.Height) + " cm") : string.Empty);
            sheet.SetCellValue(vtStart + vtPupilWeight, info != null ? (info.Weight % 1 == 0 ? info.Weight + " kg" : string.Format("{0:0.0}", info.Weight) + " kg") : string.Empty);
            sheet.SetCellValue(vtStart + vtHealthy, info != null && info.EvaluationHealth == 1 ? "Loại I (Tốt)" : info != null && info.EvaluationHealth == 2 ? "Loại II (Khá)" :
                                                 info != null && info.EvaluationHealth == 3 ? "Loại III (Trung bình)" : info != null && info.EvaluationHealth == 4 ? "Loại IV (Yếu)" :
                                                 info != null && info.EvaluationHealth == 5 ? "Loại V (Rất yếu)" : string.Empty);
            sheet.SetCellValue(vtStart + vtAbsence, lstPupilAbsence.Count);
            List<PupilAbsence> lstFinalPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(lstPupilAbsence);
            sheet.SetCellValue(vtStart + vtAbsence, lstFinalPupilAbsence.Count);
            sheet.SetCellValue(vtStart + vtAbsenceP, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).Count());
            sheet.SetCellValue(vtStart + vtAbsenceK, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).Count());
        }
        private void FillLearningResultGHK(int pocStartRow, IVTWorksheet sheet,IVTRange range, List<ClassSubject> lstClassSubject, List<RatedCommentPupilBO> lstRatedCommentPupil,int EducationLevelID,int SemesterID)
        {
            //Lay ra vi tri bat dau fill danh sach mon
            int startRow = pocStartRow + 9;
            int lastRow = startRow + lstClassSubject.Count - 1;
            int curRow = startRow;
            int rowBoder = startRow;
            int rowHeight = 33;
            int count = 0;
            if (lstClassSubject.Count % 2 != 0)
            {
                count = lstClassSubject.Count / 2 + 1;
            }
            else
            {
                count = lstClassSubject.Count / 2;
            }
            int starttmp = startRow;
            sheet.CopyPasteSameSize(range, startRow, 1);
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs = lstClassSubject[i];
                //ten mon

                RatedCommentPupilBO objRatedComemnt = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP
                                        && p.SubjectID == cs.SubjectID && p.SemesterID == SemesterID).FirstOrDefault();
                if (i < count)
                {
                    sheet.GetRange(startRow, 1, startRow, 4).Merge();
                    sheet.SetCellValue(startRow, 1, cs.SubjectCat.DisplayName);
                    if (EducationLevelID < 4)
                    {
                        sheet.GetRange(startRow, 5, startRow, 9).Merge();
                        if (objRatedComemnt != null)
                        {
                            sheet.SetCellValue(startRow, 5, objRatedComemnt.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComemnt.MiddleEvaluation == 2 ?
                                GlobalConstants.EVALUATION_COMPLETE : objRatedComemnt.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                        }
                    }
                    else
                    {
                        if (cs.SubjectCat.Abbreviation == "Tt" || cs.SubjectCat.Abbreviation == "TV")
                        {
                            sheet.GetRange(startRow, 5, startRow, 7).Merge();
                            sheet.GetRange(startRow, 8, startRow, 9).Merge();
                            if (objRatedComemnt != null)
                            {
                                sheet.SetCellValue(startRow, 5, objRatedComemnt.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComemnt.MiddleEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_COMPLETE : objRatedComemnt.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                sheet.SetCellValue(startRow, 8, objRatedComemnt.PeriodicMiddleMark);
                            }
                        }
                        else
                        {
                            sheet.GetRange(startRow, 5, startRow, 9).Merge();
                            if (objRatedComemnt != null)
                            {
                                sheet.SetCellValue(startRow, 5, objRatedComemnt.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComemnt.MiddleEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_COMPLETE : objRatedComemnt.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                            }
                        }
                        
                    }
                    
                    
                    startRow++;
                }
                else
                {
                    sheet.GetRange(starttmp, 10, starttmp, 15).Merge();
                    sheet.SetCellValue(starttmp, 10, cs.SubjectCat.DisplayName);
                    sheet.GetRange(starttmp, 16, starttmp, 19).Merge();
                    if (objRatedComemnt != null)
                    {
                        sheet.SetCellValue(starttmp, 16, objRatedComemnt.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComemnt.MiddleEvaluation == 2 ?
                            GlobalConstants.EVALUATION_COMPLETE : objRatedComemnt.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                    }
                    starttmp++;
                }
                sheet.SetRowHeight(curRow, rowHeight);
                sheet.GetRange(curRow, firstColumn, curRow, lastColumn).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(curRow, firstColumn, curRow, lastColumn).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                curRow++;
            }
            if (lstClassSubject.Count % 2 != 0)
            {
                sheet.GetRange(starttmp, 10, starttmp, 15).Merge();
                sheet.GetRange(starttmp, 16, starttmp, 19).Merge();
            }

            if (lstClassSubject.Count > 12)
            {
                IVTRange globalRange = sheet.GetRange(rowBoder, firstColumn, rowBoder + count - 1, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            
            //sheet.SetBreakPage(lastRow + 2);
        }
        private void FillFooterGHK(int pocStartRow, IVTWorksheet sheet, IVTRange template, List<ClassSubject> lstClassSubject, List<RatedCommentPupilBO> lstRatedCommentPupil,
             List<EvaluationCriteria> lstEvaluationCriteria, int semester, string headTeacherName)
        {
            int startRow = 0;
            if (lstClassSubject.Count < 12)
	        {
		        startRow = pocStartRow + 16;
	        }
            else
	        {
                if (lstClassSubject.Count % 2 == 0)
                {
                    startRow = pocStartRow + 9 + (lstClassSubject.Count / 2);       
                }
                else
                {
                    startRow = pocStartRow + 10 + (lstClassSubject.Count / 2);   
                }
                
	        }
             
            sheet.CopyPasteSameSize(template, startRow, 1);
            VTVector vectorStart = new VTVector(startRow, firstColumn);

            VTVector vectorProvinceAndDate = new VTVector(startRow + 12, 10);
            VTVector vectorHeadMasterTitle = new VTVector(startRow + 13, 1);
            VTVector vectorHeadMasterName = new VTVector(startRow + 18, 1);
            VTVector vectorHeadTeacherName = new VTVector(startRow + 18, 10);

            int startNL = startRow + 3;
            int startPC = startRow + 3;
            //Fill NL PC
            EvaluationCriteria objEvaluationCriteria = null;
            RatedCommentPupilBO objRatedComment = null;
            for (int i = 0; i < lstEvaluationCriteria.Count; i++)
            {
                objEvaluationCriteria = lstEvaluationCriteria[i];
                objRatedComment = lstRatedCommentPupil.Where(p => p.EvaluationID == objEvaluationCriteria.TypeID && p.SubjectID == objEvaluationCriteria.EvaluationCriteriaID
                                                            && p.SemesterID == semester).FirstOrDefault();
                if (objRatedComment != null)
	            {
                    if (objEvaluationCriteria.TypeID == 2)
                    {
                        sheet.SetCellValue(startNL, 6, objRatedComment.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 2 ?
                                GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                        startNL++;
                    }
                    else
                    {
                        sheet.SetCellValue(startPC, 18, objRatedComment.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityMiddleEvaluation == 2 ?
                                GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                        startPC++;
                    }
	            }
            }

            //Footer
            sheet.SetCellValue(vectorProvinceAndDate, "='Thong tin chung'!$B$7");
            sheet.SetCellValue(vectorHeadMasterName, "='Thong tin chung'!$B$6");
            sheet.SetCellValue(vectorHeadMasterTitle, "='Thong tin chung'!$B$5");
            sheet.SetCellValue(vectorHeadTeacherName, headTeacherName);

        }
        private void FillLearningResultCHK(int pocStartRow, IVTWorksheet sheet,IVTRange range, List<ClassSubject> lstClassSubject, List<RatedCommentPupilBO> lstRatedCommentPupil,int EducationLevelID,int SemesterID)
        {
            //Lay ra vi tri bat dau fill danh sach mon
            int startRow = pocStartRow + 10;
            int breakRow = startRow;
            int lastRow = startRow + lstClassSubject.Count - 1;
            int rowHeight = 45;
            sheet.CopyPasteSameSize(range, startRow, 1);
            int count = lstClassSubject.Count;
            //Fill MH HDGD
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs = lstClassSubject[i];
                RatedCommentPupilBO objRatedComemnt = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP
                                        && p.SubjectID == cs.SubjectID && p.SemesterID == SemesterID).FirstOrDefault();
                sheet.SetCellValue(startRow, 1, cs.SubjectCat.DisplayName);
                sheet.GetRange(startRow, 1, startRow, 4).Merge();
                sheet.GetRange(startRow, 16, startRow, 19).Merge();
                if (objRatedComemnt != null)
                {
                    sheet.SetCellValue(startRow, 5, objRatedComemnt.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComemnt.MiddleEvaluation == 2 ?
                        GlobalConstants.EVALUATION_COMPLETE : objRatedComemnt.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                    if (EducationLevelID > 3 && (cs.SubjectCat.Abbreviation == "Tt" || cs.SubjectCat.Abbreviation == "TV"))
                    {
                        sheet.GetRange(startRow, 5, startRow, 7).Merge();
                        sheet.SetCellValue(startRow, 8, objRatedComemnt.PeriodicMiddleMark);
                        sheet.GetRange(startRow, 8, startRow, 9).Merge();
                    }
                    else
                    {
                        sheet.GetRange(startRow, 5, startRow, 9).Merge();
                    }
                    sheet.SetCellValue(startRow, 10, objRatedComemnt.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComemnt.EndingEvaluation == 2 ?
                            GlobalConstants.EVALUATION_COMPLETE : objRatedComemnt.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                    if (cs.IsCommenting == 0)
                    {
                        sheet.GetRange(startRow, 10, startRow, 12).Merge();
                        sheet.SetCellValue(startRow, 13, objRatedComemnt.PeriodicEndingMark);
                        sheet.GetRange(startRow, 13, startRow, 15).Merge();
                    }
                    else
                    {
                        sheet.GetRange(startRow, 10, startRow, 15).Merge();
                    }

                    sheet.SetCellValue(startRow, 16, objRatedComemnt.Comment);
                    sheet.GetRange(startRow, 16, startRow, 16).SetHAlign(VTHAlign.xlHAlignLeft);
                }
                else
                {
                    if (EducationLevelID > 3 && (cs.SubjectCat.Abbreviation == "Tt" || cs.SubjectCat.Abbreviation == "TV"))
                    {
                        sheet.GetRange(startRow, 5, startRow, 7).Merge();
                        sheet.GetRange(startRow, 8, startRow, 9).Merge();
                    }
                    else
                    {
                        sheet.GetRange(startRow, 5, startRow, 9).Merge();
                    }

                    if (cs.IsCommenting == 0)
                    {
                        sheet.GetRange(startRow, 10, startRow, 12).Merge();
                        sheet.GetRange(startRow, 13, startRow, 15).Merge();
                    }
                    else
                    {
                        sheet.GetRange(startRow, 10, startRow, 15).Merge();
                    }
                }
                sheet.SetRowHeight(startRow, rowHeight);
                startRow++;
            }
            if (count > 12)
            {
                sheet.GetRange(breakRow, 1, breakRow + count - 1, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            sheet.SetBreakPage(breakRow + 13);
        }
        private void FillFooterCHK(int pocStartRow, IVTWorksheet sheet, IVTRange template, List<ClassSubject> lstClassSubject, List<RatedCommentPupilBO> lstRatedCommentPupil,
            SummedEndingEvaluationBO objSEEBO, List<EvaluationReward> lstER, List<EvaluationCriteria> lstEvaluationCriteria, int semester,int SemesterType, string headTeacherName)
        {
            int startRow = 0;
            if (lstClassSubject.Count <= 12)
            {
                startRow = pocStartRow + 24;
            }
            else
            {
                startRow = pocStartRow + 10 + lstClassSubject.Count;
            }

            sheet.CopyPasteSameSize(template, startRow, 1);

            VTVector vectorHKI = new VTVector(startRow + 2, 8);
            VTVector vectorHKII = new VTVector(startRow + 2, 10);
            VTVector vectorPCHKI = new VTVector(startRow + 9, 8);
            VTVector vectorPCHKII = new VTVector(startRow + 9, 10);
            VTVector vectorProvinceAndDate = new VTVector(startRow + 23, 10);
            VTVector vectorHeadMasterName = new VTVector(startRow + 30, 1);
            VTVector vectorHeadTeacherName = new VTVector(startRow + 30, 10);

            int startNL = startRow + 3;
            int startPC = startRow + 10;
            int startrowReward = startPC + 6;
            //Fill NL PC
            EvaluationCriteria objEvaluationCriteria = null;
            RatedCommentPupilBO objRatedComment = null;
            for (int i = 0; i < lstEvaluationCriteria.Count; i++)
            {
                objEvaluationCriteria = lstEvaluationCriteria[i];
                objRatedComment = lstRatedCommentPupil.Where(p => p.EvaluationID == objEvaluationCriteria.TypeID && p.SubjectID == objEvaluationCriteria.EvaluationCriteriaID && p.SemesterID == semester).FirstOrDefault();
                if (objEvaluationCriteria.TypeID == 2)
                {
                    if (objRatedComment != null)
                    {
                        sheet.SetCellValue(startNL, 8, objRatedComment.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 2 ?
                            GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");

                        sheet.SetCellValue(startNL, 10, objRatedComment.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityEndingEvaluation == 2 ?
                            GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                        sheet.SetCellValue(startNL, 13, objRatedComment.Comment);
                    }
                    startNL++;
                }
                else
                {
                    if (objRatedComment != null)
                    {
                        sheet.SetCellValue(startPC, 8, objRatedComment.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityMiddleEvaluation == 2 ?
                            GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");

                        sheet.SetCellValue(startPC, 10, objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2 ?
                            GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                        sheet.SetCellValue(startPC, 13, objRatedComment.Comment);
                    }
                    startPC++;
                }
            }

            //fill Khen thuong
            string RewardName = (objSEEBO != null && objSEEBO.RewardID.HasValue ) ? "Học sinh xuất sắc," : "";
            EvaluationReward objER = null;
            for (int i = 0; i < lstER.Count; i++)
            {
                objER = lstER[i];
                RewardName += objER.Content + (i < lstER.Count - 1 ? "," : "");
            }
            sheet.SetCellValue(startrowReward, 1, RewardName);
            if (SemesterType == 2)
            {
                sheet.SetRowHeight(startrowReward + 1, 0);
            }
            else
            {
                //len lop
                sheet.SetCellValue(startrowReward + 1, 8, objSEEBO != null 
                    && objSEEBO.EndingEvaluation == "HT" ? "x" : "");
                //ren luyen bo sung
                sheet.SetCellValue(startrowReward + 1, 16, objSEEBO != null && objSEEBO.EndingEvaluation == "CHT" ? "x" : "");
            }
            //Footer
            sheet.SetCellValue(vectorHKI, "='Thong tin chung'!B8");
            sheet.SetCellValue(vectorHKII, "='Thong tin chung'!B9");
            sheet.SetCellValue(vectorPCHKI, "='Thong tin chung'!B8");
            sheet.SetCellValue(vectorPCHKII, "='Thong tin chung'!B9");
            sheet.SetCellValue(vectorProvinceAndDate, "='Thong tin chung'!$B$7");
            sheet.SetCellValue(vectorHeadMasterName, "='Thong tin chung'!$B$6");
            sheet.SetCellValue(vectorHeadTeacherName, headTeacherName);
        }
        #region Code cu
        /*
        public Stream CreateCommunicationNoteReport(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int userAccountID = Utils.GetInt(dic["UserAccountID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            int classId = Utils.GetInt(dic["Class"]);

            AcademicYear ay = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school=SchoolProfileBusiness.Find(schoolId);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
            DateTime semesterStartDate;
            DateTime semesterEndDate;
            if(semester==GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                semesterStartDate=ay.FirstSemesterStartDate.GetValueOrDefault();
                semesterEndDate=ay.FirstSemesterEndDate.GetValueOrDefault();
            }
            else
            {
                semesterStartDate=ay.SecondSemesterStartDate.GetValueOrDefault();
                semesterEndDate=ay.SecondSemesterEndDate.GetValueOrDefault();
            }

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_COMMUNICATION_NOTE;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string[] arrTemplateName = rp.TemplateName.Split('|');
            string templateName;
            if (semester == 1)
            {
                templateName = arrTemplateName[0];
            }
            else
            {
                templateName = arrTemplateName[1];
            }
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templateName;

            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);

            int firstRow = 2;
           
            //Lay range mau cho 1 hoc sinh
            IVTRange templateHeaderRange;
            templateHeaderRange = firstSheet.GetRange(1, firstColumn, 9, lastColumn);

            IVTRange templateFooterRange;
            templateFooterRange = firstSheet.GetRange(22, firstColumn, 57, lastColumn);
            

            //Lay danh sach hoc sinh cua toan khoi
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["Status"] = new List<int> { GlobalConstants.PUPIL_STATUS_STUDYING };

            IQueryable<PupilOfClass> lstPocEdu = PupilOfClassBusiness.Search(tmpDic).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);

            //Lay danh sach lop
            List<ClassProfile> lstClass;
            IQueryable<ClassProfile> query = ClassProfileBusiness.getClassByRoleAppliedLevel(academicYearId, schoolId, appliedLevel, userAccountID)
               .Where(o => o.EducationLevelID == educationLevel);
            if (classId != 0)
            {
                query = query.Where(o => o.ClassProfileID == classId);
            }
            lstClass = query.OrderBy(o => o.OrderNumber).ToList();

            List<int> lstHeadTeacherId = lstClass.Select(o => o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0).ToList();

            List<Employee> lstHeadTeacher = EmployeeBusiness.All.Where(o => o.SchoolID == schoolId && lstHeadTeacherId.Contains(o.EmployeeID)).ToList();

            //Danh sach mon hoc
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["Semester"] = semester;
            tmpDic["EducationLevelID"] = educationLevel;

            List<ClassSubject> lstClassSubjectEdu = ClassSubjectBusiness.SearchBySchool(schoolId, tmpDic).OrderBy(o => o.IsCommenting).ThenBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            int healthPeriodId = semester == 1 ? 1 : 2;
            // Lấy tất cả các thông số về sức khỏe và số ngày nghỉ của học sinh
            List<InfoEvaluationPupil> lstPupilInfoEdu = (from pt in PhysicalTestBusiness.All
                                                         join mn in MonitoringBookBusiness.All on pt.MonitoringBookID equals mn.MonitoringBookID
                                                         where mn.AcademicYearID == academicYearId
                                                         && mn.SchoolID == schoolId
                                                         && mn.EducationLevelID == educationLevel
                                                         && mn.HealthPeriodID == healthPeriodId
                                                         orderby mn.MonitoringDate descending
                                                         select new InfoEvaluationPupil
                                                         {
                                                             PupilID = mn.PupilID,
                                                             Height = pt.Height,
                                                             Weight = pt.Weight,
                                                             EvaluationHealth = pt.PhysicalClassification,
                                                             AcademicYear = mn.AcademicYearID,
                                                             SchoolID = mn.SchoolID,
                                                             ClassID = mn.ClassID,
                                                             MonitoringDate = mn.MonitoringDate

                                                         }).ToList();

            //thong tin nghi hoc
            List<PupilAbsence> lstPupilAbenceEdu = PupilAbsenceBusiness.All
                .Where(o => o.AcademicYearID == academicYearId
                && o.SchoolID == schoolId
                && o.EducationLevelID == educationLevel
                && o.Last2digitNumberSchool == partition
                && o.AbsentDate >= semesterStartDate
                && o.AbsentDate <= semesterEndDate).ToList();

            // Lấy nhận xét và điểm các môn - SummedEvaluation và SummedEvaluationHistory
            List<SummedEvaluationBO> lstSummedEvaluationEdu;
            if (!UtilsBusiness.IsMoveHistory(ay))
            {
                lstSummedEvaluationEdu = SummedEvaluationBusiness.All
                    .Where(o => o.AcademicYearID == academicYearId
                    && o.LastDigitSchoolID == partition
                    && o.SchoolID == schoolId
                    && o.EducationLevelID == educationLevel
                    && o.SemesterID == semester)
                    .Select(o => new SummedEvaluationBO
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        EndingComment = o.EndingComments,
                        EndingEvaluation = o.EndingEvaluation,
                        EvaluationCriteriaID = o.EvaluationCriteriaID,
                        EvaluationID = o.EvaluationID,
                        PeriodicEndingMark=o.PeriodicEndingMark
                    }).ToList();
            }
            else
            {
                lstSummedEvaluationEdu = SummedEvaluationHistoryBusiness.All
                    .Where(o => o.AcademicYearID == academicYearId
                    && o.LastDigitSchoolID == partition
                    && o.SchoolID == schoolId
                    && o.EducationLevelID == educationLevel
                    && o.SemesterID == semester)
                    .Select(o => new SummedEvaluationBO
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        EndingComment = o.EndingComments,
                        EndingEvaluation = o.EndingEvaluation,
                        EvaluationCriteriaID = o.EvaluationCriteriaID,
                        EvaluationID = o.EvaluationID,
                        PeriodicEndingMark=o.PeriodicEndingMark
                    }).ToList();
            }

            //Lay thong tin danh gia nang luc pham chat
            List<SummedEndingEvaluation> lstSummedEndingEvaluationEdu = SummedEndingEvaluationBusiness.All
                .Where(o => o.SchoolID == schoolId
                && o.LastDigitSchoolID == partition
                && o.AcademicYearID == academicYearId).ToList();

            //Thong tin khen thuong
            List<RewardCommentFinal> lstRewardCommentFinal=RewardCommentFinalBusiness.All
                .Where(o=>o.SchoolID==schoolId
                && o.AcademicYearID==academicYearId
                && o.SemesterID == semester).ToList();

            //Danh muc khen thuong
            List<RewardFinal> lstRewardFinal=RewardFinalBusiness.All.Where(o=>o.SchoolID==schoolId).ToList();

            for (int iClass = 0; iClass < lstClass.Count; iClass++)
            {
                ClassProfile curClass=lstClass[iClass];
                IVTWorksheet curSheet = oBook.CopySheetToBeforeLast(secondSheet);

                //Lay danh sach mon hoc cua lop
                List<ClassSubject> lstClassSubjectClass = lstClassSubjectEdu.Where(o => o.ClassID == curClass.ClassProfileID).ToList();

                //Lay danh sach hoc sinh cua lop
                List<PupilOfClass> lstPocClass = lstPocEdu.Where(o => o.ClassID == curClass.ClassProfileID).ToList();

                //Lay gvcn
                Employee headTeacher = lstHeadTeacher.Where(o => o.EmployeeID == curClass.HeadTeacherID).FirstOrDefault();

                int pocStartRow = firstRow;
                for (int iPoc = 0; iPoc < lstPocClass.Count; iPoc++)
                {
                    PupilOfClass poc = lstPocClass[iPoc];

                    //Lay thong tin chieu cao, can nang
                    InfoEvaluationPupil info = lstPupilInfoEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).FirstOrDefault();

                    //Thong tin nghi hoc
                    List<PupilAbsence> lstPupilAbencePupil=lstPupilAbenceEdu.Where(o=>o.ClassID==poc.ClassID && o.PupilID==poc.PupilID).ToList();

                    //Danh gia cac mon
                    List<SummedEvaluationBO> lstSummedEvaluationPupilMH = lstSummedEvaluationEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID && o.EvaluationID == 1).ToList();

                    //Nhan xet nang luc
                    List<SummedEvaluationBO> lstSummedEvaluationPupilNL = lstSummedEvaluationEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID && o.EvaluationID == 2).ToList();

                    //Nhan xet nang luc
                    List<SummedEvaluationBO> lstSummedEvaluationPupilPC = lstSummedEvaluationEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID && o.EvaluationID == 3).ToList();

                    //Fill data for header here
                    FillHeader(pocStartRow, curSheet, templateHeaderRange, poc, info, lstPupilAbencePupil);

                    //Fill mon va diem cac mon
                    FillLearningResult(pocStartRow, curSheet, lstClassSubjectClass, lstSummedEvaluationPupilMH);

                    //Fill footer
                    SummedEndingEvaluation seeNL = lstSummedEndingEvaluationEdu.Where(o => o.SemesterID == semester
                        && o.ClassID == poc.ClassID
                        && o.PupilID == poc.PupilID
                        && o.EvaluationID == 2).FirstOrDefault();

                    SummedEndingEvaluation seePC = lstSummedEndingEvaluationEdu.Where(o => o.SemesterID == semester
                        && o.ClassID == poc.ClassID
                        && o.PupilID == poc.PupilID
                        && o.EvaluationID == 3).FirstOrDefault();

                    SummedEndingEvaluation seeFinal = null;
                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        seeFinal = lstSummedEndingEvaluationEdu.Where(o => o.SemesterID == 4
                            && o.ClassID == poc.ClassID
                            && o.PupilID == poc.PupilID
                            && o.EvaluationID == 4).FirstOrDefault();
                    }

                    RewardCommentFinal rcf=lstRewardCommentFinal.Where(o=>o.PupilID==poc.PupilID && o.ClassID==poc.ClassID).FirstOrDefault();

                    
                    FillFooter(pocStartRow, curSheet, templateFooterRange, lstClassSubjectClass, seeNL, seePC, seeFinal, lstSummedEvaluationPupilNL, lstSummedEvaluationPupilPC
                        , rcf, lstRewardFinal, semester,headTeacher!=null?headTeacher.FullName:String.Empty);

                    pocStartRow += lstClassSubjectClass.Count + 46;
                    curSheet.SetBreakPage(pocStartRow);
                }

                curSheet.Name = ReportUtils.StripVNSign(curClass.DisplayName);
                curSheet.SetFontName("Times New Roman", 14);
                curSheet.SetPrintArea("$A2:$Q" + (pocStartRow-1));
                curSheet.Worksheet.Zoom = 70;
                curSheet.FitAllColumnsOnOnePage = true;
                
            }

            //Fill sheet thong tin chung
            //Ten truong
            thirdSheet.SetCellValue("B3", school.SchoolName);
            string title1 = "PHIẾU LIÊN LẠC " + ((semester == 1) ? "HỌC KỲ I - NĂM HỌC " : "HỌC KỲ II - NĂM HỌC ") + ay.DisplayTitle;
            thirdSheet.SetCellValue("B4", title1);
            thirdSheet.SetCellValue("B6", school.HeadMasterName);
            DateTime date = DateTime.Now.Date;
            string title2 = school.Province.ProvinceName + ", ngày " + date.Day + " tháng " + date.Month + " năm " + date.Year;
            thirdSheet.SetCellValue("B7", title2);

           ////Xoa sheet dau tien

            firstSheet.Delete();
            secondSheet.Delete();

            return oBook.ToStream();
        }

        private void FillHeader(int pocStartRow, IVTWorksheet sheet, IVTRange template, PupilOfClass poc, InfoEvaluationPupil info, List<PupilAbsence> lstPupilAbsence)
        {
            sheet.CopyPasteSameSize(template, pocStartRow, 1);
            VTVector vtStart = new VTVector(pocStartRow, firstColumn);
            VTVector vtTitle = new VTVector(2,0);
            VTVector vtName = new VTVector(3, 2);
            VTVector vtClass = new VTVector(3, 14);
            VTVector vtPupilHeight = new VTVector(4, 2);
            VTVector vtPupilWeight = new VTVector(4, 7);
            VTVector vtHealthy = new VTVector(4, 14);
            VTVector vtAbsence = new VTVector(5, 2);
            VTVector vtAbsenceP = new VTVector(5, 7);
            VTVector vtAbsenceK = new VTVector(5, 14);

            sheet.SetCellValue(vtStart, "='Thong tin chung'!B3");
            sheet.SetCellValue(vtStart + vtTitle, "='Thong tin chung'!B4");
            sheet.SetCellValue(vtStart + vtName, poc.PupilProfile.FullName);
            sheet.SetCellValue(vtStart + vtClass, poc.ClassProfile.DisplayName);
            sheet.SetCellValue(vtStart + vtPupilHeight, info != null ? (info.Height % 1 == 0 ? info.Height + " cm" : string.Format("{0:0.0}", info.Height) + " cm") : string.Empty);
            sheet.SetCellValue(vtStart + vtPupilWeight, info != null ? (info.Weight % 1 == 0 ? info.Weight + " kg" : string.Format("{0:0.0}", info.Weight) + " kg") : string.Empty);
            sheet.SetCellValue(vtStart + vtHealthy, info != null && info.EvaluationHealth == 1 ? "Loại I (Tốt)" : info != null && info.EvaluationHealth == 2 ? "Loại II (Khá)" :
                                                 info != null && info.EvaluationHealth == 3 ? "Loại III (Trung bình)" : info != null && info.EvaluationHealth == 4 ? "Loại IV (Yếu)" :
                                                 info != null && info.EvaluationHealth == 5 ? "Loại V (Rất yếu)" : string.Empty);
            sheet.SetCellValue(vtStart + vtAbsence, lstPupilAbsence.Count);
            List<PupilAbsence> lstFinalPupilAbsence=  PupilAbsenceBusiness.GetPupilAbsenceByDate(lstPupilAbsence);
            sheet.SetCellValue(vtStart + vtAbsence, lstFinalPupilAbsence.Count);
            sheet.SetCellValue(vtStart + vtAbsenceP, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).Count());
            sheet.SetCellValue(vtStart + vtAbsenceK, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).Count());
        }

        private void FillFooter(int pocStartRow, IVTWorksheet sheet, IVTRange template, List<ClassSubject> lstClassSubject,SummedEndingEvaluation seeNL, SummedEndingEvaluation seePC,
            SummedEndingEvaluation seeFinal, List<SummedEvaluationBO> lstSeNL, List<SummedEvaluationBO> lstSePC,RewardCommentFinal reward, List<RewardFinal> lstRewardFinal, int semester,
            string headTeacherName)
        {
            int startRow = pocStartRow + 9 + lstClassSubject.Count;
            sheet.CopyPasteSameSize(template, startRow, 1);

            VTVector vectorStart = new VTVector(startRow, firstColumn);

            VTVector vectorNL_D = new VTVector(3, 10);
            VTVector vectorNL_CD = new VTVector(3, 14);
            //Fill nhan xet nang luc
            VTVector vectorNLComment1 = new VTVector(6, 6);
            VTVector vectorNLComment2 = new VTVector(7, 6);
            VTVector vectorNLComment3 = new VTVector(8, 6);

            VTVector vectorPC_D = new VTVector(10, 10);
            VTVector vectorPC_CD = new VTVector(10, 14);
            //Fill nhan xet pham chat
            VTVector vectorPCComment1 = new VTVector(13, 6);
            VTVector vectorPCComment2 = new VTVector(14, 6);
            VTVector vectorPCComment3 = new VTVector(15, 6);
            VTVector vectorPCComment4 = new VTVector(16, 6);

            VTVector vectorOutstanding = new VTVector(19, 0);
            VTVector vectorReward = new VTVector(21, 0);
            VTVector vectorFinalHT = new VTVector(22, 7);
            VTVector vectorFinalCHT = new VTVector(22, 14);

            VTVector vectorProvinceAndDate = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? new VTVector(27, 7) : new VTVector(28, 7);
            VTVector vectorHeadMaster = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? new VTVector(28, 0) : new VTVector(29, 0);
            VTVector vectorHeadMasterName = new VTVector(35,0);
            VTVector vectorHeadTeacherName = new VTVector(35, 7);

            if (seeNL != null)
            {
                if (seeNL.EndingEvaluation == "Đ")
                {
                    sheet.SetCellValue(vectorStart + vectorNL_D, "x");
                }
                else if (seeNL.EndingEvaluation == "CĐ")
                {
                    sheet.SetCellValue(vectorStart + vectorNL_CD, "x");
                }
            }

            SummedEvaluationBO tmpSE = lstSeNL.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID1).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorNLComment1, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            tmpSE = lstSeNL.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID2).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorNLComment2, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            tmpSE = lstSeNL.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID3).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorNLComment3, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            if (seePC != null)
            {
                if (seePC.EndingEvaluation == "Đ")
                {
                    sheet.SetCellValue(vectorStart + vectorPC_D, "x");
                }
                else if (seePC.EndingEvaluation == "CĐ")
                {
                    sheet.SetCellValue(vectorStart + vectorPC_CD, "x");
                }
            }

            tmpSE = lstSePC.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID4).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorPCComment1, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            tmpSE = lstSePC.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID5).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorPCComment2, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            tmpSE = lstSePC.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID6).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorPCComment3, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            tmpSE = lstSePC.Where(o => o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID7).FirstOrDefault();
            sheet.SetCellValue(vectorStart + vectorPCComment4, tmpSE != null ? tmpSE.EndingComment : String.Empty);

            //Thanh tich noi bat
            sheet.SetCellValue(vectorStart + vectorOutstanding, reward != null ? reward.OutstandingAchievement : String.Empty);

            //khen thuong
            string strRewardId = reward != null && reward.RewardID!=null ? reward.RewardID : String.Empty;
            List<int> lstRewardId = strRewardId.Split(',').Select(tag => tag.Trim()).
                          Where(tag => !string.IsNullOrEmpty(tag)).Select(o => Int32.Parse(o)).ToList();
            List<RewardFinal> lstRf = lstRewardFinal.Where(o => lstRewardId.Contains(o.RewardFinalID)).ToList();

            string strReward = String.Empty;
            for (int j = 0; j < lstRf.Count; j++)
            {
                RewardFinal rf = lstRf[j];
                strReward = strReward + rf.RewardMode;

                strReward = strReward + "; ";
            }

            sheet.SetCellValue(vectorStart + vectorReward, strReward);

            //Ket qua cuoi nam
            if (seeFinal != null)
            {
                if (seeFinal.EndingEvaluation == "HT" || seeFinal.RateAdd == 1)
                {
                    sheet.SetCellValue(vectorStart + vectorFinalHT, "x");
                }
                else if (seeFinal.EndingEvaluation == "CHT" && seeFinal.RateAdd != 1)
                {
                    sheet.SetCellValue(vectorStart + vectorFinalCHT, "x");
                }
            }

            //Footer
            sheet.SetCellValue(vectorStart + vectorProvinceAndDate, "='Thong tin chung'!B7");
            sheet.SetCellValue(vectorStart + vectorHeadMaster, "='Thong tin chung'!B5");
            sheet.SetCellValue(vectorStart + vectorHeadMasterName, "='Thong tin chung'!B6");
            sheet.SetCellValue(vectorStart + vectorHeadTeacherName, headTeacherName);

        }

        private void FillLearningResult(int pocStartRow, IVTWorksheet sheet, List<ClassSubject> lstClassSubject, List<SummedEvaluationBO> lstSummedEvaluation)
        {
            //Lay ra vi tri bat dau fill danh sach mon
            int startRow = pocStartRow + 9;
            int lastRow = startRow + lstClassSubject.Count - 1;
            int curRow = startRow;
            int rowHeight = 55;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs=lstClassSubject[i];
                //ten mon
                sheet.SetCellValue(curRow, 1, cs.SubjectCat.DisplayName);
                
                SummedEvaluationBO se = lstSummedEvaluation.Where(o => o.EvaluationCriteriaID == cs.SubjectID).FirstOrDefault();
                if (se != null)
                {
                    //Nhan xet
                    sheet.SetCellValue(curRow, 5, se.EndingComment);

                    if (cs.IsCommenting != 1)
                    {
                        //Diem kiem tra cuoi ky
                        sheet.SetCellValue(curRow, 16, se.PeriodicEndingMark.HasValue ? se.PeriodicEndingMark.Value.ToString() : string.Empty);
                    }
                    //Danh gia
                    sheet.SetCellValue(curRow, 17, se.EndingEvaluation);
                }
               

                //Mergecell
                sheet.GetRange(curRow, 1, curRow, 4).Merge();
                if (cs.IsCommenting == 1)
                {
                    sheet.GetRange(curRow, 5, curRow, 16).Merge();
                   
                }
                else
                {
                    sheet.GetRange(curRow, 5, curRow, 15).Merge();
                    
                }
                

                sheet.SetRowHeight(curRow, rowHeight);
                sheet.GetRange(curRow, firstColumn, curRow, lastColumn).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(curRow, firstColumn, curRow, lastColumn).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                sheet.GetRange(curRow, 5, curRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                sheet.GetRange(curRow, 5, curRow, 5).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet.GetRange(curRow, 5, curRow, 5).WrapText();


                curRow++;
            }

            IVTRange globalRange = sheet.GetRange(startRow, firstColumn, lastRow, lastColumn);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.SetBreakPage(lastRow + 2);
        }
        */
        
        #endregion
        #endregion

        #region Phieu lien lac VNEN
        public ProcessedReport GetVnenCommunicationNoteReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateVNENCommunicationNoteMonthReport(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int userAccountID = Utils.GetInt(dic["UserAccountID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            int classId = Utils.GetInt(dic["Class"]);
            string monthId = Utils.GetString(dic["MonthID"]);
            int typeExport = Utils.GetInt(dic["TypeExport"]);
            AcademicYear ay = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
            DateTime semesterStartDate;
            DateTime semesterEndDate;
            int monthAbsence = 0;
            int yearAbsence = 0;
            if (typeExport == 1)//HK
            {
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    semesterStartDate = ay.FirstSemesterStartDate.GetValueOrDefault();
                    semesterEndDate = ay.FirstSemesterEndDate.GetValueOrDefault();
                }
                else
                {
                    semesterStartDate = ay.SecondSemesterStartDate.GetValueOrDefault();
                    semesterEndDate = ay.SecondSemesterEndDate.GetValueOrDefault();
                }    
            }
            else//thang
            {
                monthAbsence = int.Parse(monthId.Substring(4, (monthId.Length - 4)));
                yearAbsence = int.Parse(monthId.ToString().Substring(0, 4));
                semesterStartDate = new DateTime(yearAbsence, monthAbsence, 1);
                int lastDayinMonth = DateTime.DaysInMonth(semesterStartDate.Year, semesterStartDate.Month);
                semesterEndDate = new DateTime(yearAbsence, monthAbsence, lastDayinMonth);
            }
            string reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE_MONTH;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy ra sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            int firstRow = 2;
            IVTRange templateHeaderRange;
            templateHeaderRange = firstSheet.GetRange(2, firstColumn, 9, lastColumn);

            IVTRange templateFooterRange;
            templateFooterRange = firstSheet.GetRange(23, firstColumn, 34, lastColumn);
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["Status"] = new List<int> { GlobalConstants.PUPIL_STATUS_STUDYING };
            string MonthName = Utils.GetString(dic, "MonthName");
            string Title = "Phiếu liên lạc " + MonthName + " - năm học " + ay.DisplayTitle;
            thirdSheet.SetCellValue("B4", school.SchoolName);
            thirdSheet.SetCellValue("B5", Title.ToUpper());
            thirdSheet.SetCellValue("B7", school.HeadMasterName);
            Title = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            Title = (school.District != null ? school.District.DistrictName : "") + ", ngày " + dateTimeNow.Day + " tháng " + dateTimeNow.Month + " năm " + dateTimeNow.Year;
            thirdSheet.SetCellValue("B8", Title);

            List<PupilOfClass> lstPocEdu = PupilOfClassBusiness.Search(tmpDic).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName).ToList();

            //Lay danh sach lop
            List<ClassProfile> lstClass;
            IQueryable<ClassProfile> query = ClassProfileBusiness.getClassByRoleAppliedLevel(academicYearId, schoolId, appliedLevel, userAccountID)
               .Where(o => o.EducationLevelID == educationLevel && o.IsVnenClass == true);
            if (classId != 0)
            {
                query = query.Where(o => o.ClassProfileID == classId);
            }
            lstClass = query.OrderBy(o => o.OrderNumber).ToList();
            List<int> lstClassID = lstClass.Select(p => p.ClassProfileID).Distinct().ToList();

            List<int> lstHeadTeacherId = lstClass.Select(o => o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0).ToList();

            List<Employee> lstHeadTeacher = EmployeeBusiness.All.Where(o => o.SchoolID == schoolId && lstHeadTeacherId.Contains(o.EmployeeID)).ToList();

            //Danh sach mon hoc
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["Semester"] = semester;
            tmpDic["ListClassID"] = lstClassID;
            //tmpDic["IsVNEN"] = true;

            List<ClassSubject> lstTmp = ClassSubjectBusiness.SearchBySchool(schoolId, tmpDic).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<ClassSubject> lstClassSubjectEdu = new List<ClassSubject>();
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN == true && o.IsCommenting != 1));
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN == true && o.IsCommenting == 1));
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN != true && o.IsCommenting != 1));
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN != true && o.IsCommenting == 1));

            //thong tin nghi hoc
            List<PupilAbsence> lstPupilAbenceEdu = PupilAbsenceBusiness.All
                .Where(o => o.AcademicYearID == academicYearId
                && o.SchoolID == schoolId
                && o.EducationLevelID == educationLevel
                && o.Last2digitNumberSchool == partition
                && o.AbsentDate >= semesterStartDate
                && o.AbsentDate <= semesterEndDate).ToList();

            //lay danh sach nhan xet cua cac mon theo thang
            List<int> lstSubjectID = lstClassSubjectEdu.Select(p => p.SubjectID).Distinct().ToList();
            IDictionary<string, object> dicSearchTNM = new Dictionary<string, object>()
            {
                {"SchoolID",schoolId},
                {"AcademicYearID",academicYearId},
                {"lstClassID",lstClassID},
                {"lstSubjectID",lstSubjectID},
                {"MonthID",monthId}
            };
            List<TeacherNoteBookMonth> lstTeacherNoteBookMonth = TeacherNoteBookMonthBusiness.Search(dicSearchTNM).ToList();
            List<TeacherNoteBookMonth> lstTNMtmp = null;
            for (int iClass = 0; iClass < lstClass.Count; iClass++)
            {
                ClassProfile curClass = lstClass[iClass];
                IVTWorksheet curSheet = oBook.CopySheetToBeforeLast(secondSheet);
                //Danh sach cac mon hoc Vnen cua lop
                List<ClassSubject> lstVnenSubject = lstClassSubjectEdu.Where(o => o.ClassID == curClass.ClassProfileID && o.IsSubjectVNEN == true).ToList();
                //Lay danh sach hoc sinh cua lop
                List<PupilOfClass> lstPocClass = lstPocEdu.Where(o => o.ClassID == curClass.ClassProfileID).ToList();
                //Lay gvcn
                Employee headTeacher = lstHeadTeacher.Where(o => o.EmployeeID == curClass.HeadTeacherID).FirstOrDefault();
                int pocStartRow = firstRow;
                for (int iPoc = 0; iPoc < lstPocClass.Count; iPoc++)
                {
                    PupilOfClass poc = lstPocClass[iPoc];
                    lstTNMtmp = lstTeacherNoteBookMonth.Where(p => p.ClassID == curClass.ClassProfileID && p.PupilID == poc.PupilID).ToList();
                    //Thong tin nghi hoc
                    List<PupilAbsence> lstPupilAbencePupil = lstPupilAbenceEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).ToList();
                    //Fill data for header here
                    FillHeaderVnenMonth(pocStartRow, curSheet, templateHeaderRange, poc, lstPupilAbencePupil);
                    //Fill data SubjectComment
                    FillCommentSubject(pocStartRow, curSheet, lstVnenSubject, lstTNMtmp);
                    //Fill footer
                    FillFooterVnenMonth(pocStartRow, curSheet, templateFooterRange, lstVnenSubject, headTeacher != null ? headTeacher.FullName : String.Empty);

                    pocStartRow += lstSubjectID.Count + 24;
                    curSheet.SetBreakPage(pocStartRow);
                }
                curSheet.Name = ReportUtils.StripVNSign(curClass.DisplayName);
                curSheet.SetFontName("Times New Roman", 13);
                curSheet.SetPrintArea("$A2:$Q" + (pocStartRow - 1));
                curSheet.FitAllColumnsOnOnePage = true;
                curSheet.Worksheet.Zoom = 70;
            }
            //Fill sheet thong tin chung
            ////Ten truong
            //thirdSheet.SetCellValue("B4", school.SchoolName);
            //string title1 = "PHIẾU LIÊN LẠC " + ((semester == 1) ? "HỌC KỲ I - NĂM HỌC " : "HỌC KỲ II - NĂM HỌC ") + ay.DisplayTitle;
            //thirdSheet.SetCellValue("B5", title1);
            //thirdSheet.SetCellValue("B7", school.HeadMasterName);
            //DateTime date = DateTime.Now.Date;
            //string title2 = school.Province.ProvinceName + ", ngày " + date.Day + " tháng " + date.Month + " năm " + date.Year;
            //thirdSheet.SetCellValue("B8", title2);

            ////Xoa sheet dau tien
            firstSheet.Delete();
            secondSheet.Delete();

            return oBook.ToStream();
        }

        public Stream CreateVnenCommunicationNoteReport(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int userAccountID = Utils.GetInt(dic["UserAccountID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            int classId = Utils.GetInt(dic["Class"]);

            AcademicYear ay = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
            DateTime semesterStartDate;
            DateTime semesterEndDate;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                semesterStartDate = ay.FirstSemesterStartDate.GetValueOrDefault();
                semesterEndDate = ay.FirstSemesterEndDate.GetValueOrDefault();
            }
            else
            {
                semesterStartDate = ay.SecondSemesterStartDate.GetValueOrDefault();
                semesterEndDate = ay.SecondSemesterEndDate.GetValueOrDefault();
            }

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string[] arrTemplateName = rp.TemplateName.Split('|');
            string templateName;
            if (semester == 1)
            {
                templateName = arrTemplateName[0];
            }
            else
            {
                templateName = arrTemplateName[1];
            }
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templateName;

            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);

            int firstRow = 2;

            //Lay range mau cho 1 hoc sinh
            IVTRange templateHeaderRange;
            templateHeaderRange = firstSheet.GetRange(1, firstColumn, 9, 16);

            IVTRange templateFooterRange;
            templateFooterRange = firstSheet.GetRange(21, firstColumn, 35, 16);


            //Lay danh sach hoc sinh cua toan khoi
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["Status"] = new List<int> { GlobalConstants.PUPIL_STATUS_STUDYING };

            IQueryable<PupilOfClass> lstPocEdu = PupilOfClassBusiness.Search(tmpDic).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);

            //Lay danh sach lop
            List<ClassProfile> lstClass;
            IQueryable<ClassProfile> query = ClassProfileBusiness.getClassByRoleAppliedLevel(academicYearId, schoolId, appliedLevel, userAccountID)
               .Where(o => o.EducationLevelID == educationLevel && o.IsVnenClass == true);
            if (classId != 0)
            {
                query = query.Where(o => o.ClassProfileID == classId);
            }
            lstClass = query.OrderBy(o => o.OrderNumber).ToList();

            List<int> lstHeadTeacherId = lstClass.Select(o => o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0).ToList();

            List<Employee> lstHeadTeacher = EmployeeBusiness.All.Where(o => o.SchoolID == schoolId && lstHeadTeacherId.Contains(o.EmployeeID)).ToList();

            //Danh sach mon hoc
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["Semester"] = semester;
            
            IEnumerable<ClassSubject> lstTmp = ClassSubjectBusiness.SearchBySchool(schoolId, tmpDic).ToList().OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName);

            List<ClassSubject> lstClassSubjectEdu = new List<ClassSubject>();
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN == true && o.IsCommenting != 1));
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN == true && o.IsCommenting == 1));
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN != true && o.IsCommenting != 1));
            lstClassSubjectEdu.AddRange(lstTmp.Where(o => o.IsSubjectVNEN != true && o.IsCommenting == 1));

            //thong tin nghi hoc
            List<PupilAbsence> lstPupilAbenceEdu = PupilAbsenceBusiness.All
                .Where(o => o.AcademicYearID == academicYearId
                && o.SchoolID == schoolId
                && o.EducationLevelID == educationLevel
                && o.Last2digitNumberSchool == partition
                && o.AbsentDate >= semesterStartDate
                && o.AbsentDate <= semesterEndDate).ToList();

            //Lay danh gia cac mon cua hoc sinh
            IQueryable<TeacherNoteBookSemesterBO> iqueryTNBS = (from tnbs in TeacherNoteBookSemesterBusiness.All
                                                                    where tnbs.SchoolID == schoolId
                                                                    && tnbs.AcademicYearID == academicYearId
                                                                    && tnbs.PartitionID == partition
                                                                    select new TeacherNoteBookSemesterBO
                                                                    {
                                                                        PupilID = tnbs.PupilID,
                                                                        ClassID = tnbs.ClassID,
                                                                        SubjectID = tnbs.SubjectID,
                                                                        PERIODIC_SCORE_END = tnbs.PERIODIC_SCORE_END,
                                                                        PERIODIC_SCORE_END_JUDGLE = tnbs.PERIODIC_SCORE_END_JUDGLE,
                                                                        PERIODIC_SCORE_MIDDLE = tnbs.PERIODIC_SCORE_MIDDLE,
                                                                        PERIODIC_SCORE_MIDDLE_JUDGE = tnbs.PERIODIC_SCORE_MIDDLE_JUDGE,
                                                                        AVERAGE_MARK = tnbs.AVERAGE_MARK,
                                                                        AVERAGE_MARK_JUDGE = tnbs.AVERAGE_MARK_JUDGE,
                                                                        Rate = tnbs.Rate,
                                                                        SemesterID = tnbs.SemesterID
                                                                    });

            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqueryTNBS = iqueryTNBS.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST);
            }
            else
            {
                iqueryTNBS = iqueryTNBS.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND || p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL);
            }

            List<TeacherNoteBookSemesterBO> lstNoteBookEdu = iqueryTNBS.ToList();

            //Lay diem mon hoc theo TT58
            tmpDic = new Dictionary<string, object>();
            //tmpDic["ClassID"] = classId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            //if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            //{
            //    tmpDic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            //}
            //else
            //{
            //    tmpDic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_ALL;
            //}

            tmpDic["EducationLevelID"] = educationLevel;
            
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(ay);

            List<SummedUpRecordBO> lstSummedUpRecordEdu;
            List<MarkRecordBO> lstMarkRecoreEdu;
            List<JudgeRecordBO> lstJudgeRecordEdu;
            if (isMovedHistory)
            {
                lstSummedUpRecordEdu = SummedUpRecordHistoryBusiness.SearchBySchool(schoolId, tmpDic)
                    .Where(o => o.PeriodID == null)
                    //.Where(o => lstTT58SubjectId.Contains(o.SubjectID))
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester
                    }).ToList();
                tmpDic.Add("Title", "HK");
                lstMarkRecoreEdu = MarkRecordHistoryBusiness.SearchMarkRecordHistory(tmpDic)
                    .Select(o => new MarkRecordBO
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        Mark = o.Mark,
                        Semester = o.Semester
                    }).ToList();
                lstJudgeRecordEdu = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(tmpDic)
                                    .Select(o => new JudgeRecordBO
                                    {
                                        PupilID = o.PupilID,
                                        ClassID = o.ClassID,
                                        SubjectID = o.SubjectID,
                                        Judgement = o.Judgement,
                                        Semester = o.Semester
                                    }).ToList();
            }
            else
            {
                lstSummedUpRecordEdu = SummedUpRecordBusiness.SearchBySchool(schoolId, tmpDic)
                    .Where(o => o.PeriodID == null)
                    //.Where(o => lstTT58SubjectId.Contains(o.SubjectID))
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester
                    }).ToList();
                tmpDic.Add("Title", "HK");
                lstMarkRecoreEdu = MarkRecordBusiness.SearchBySchool(schoolId,tmpDic)
                    .Select(o => new MarkRecordBO
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        Mark = o.Mark,
                        Semester = o.Semester
                    }).ToList();
                lstJudgeRecordEdu = JudgeRecordBusiness.SearchBySchool(schoolId,tmpDic)
                                    .Select(o => new JudgeRecordBO
                                    {
                                        PupilID = o.PupilID,
                                        ClassID = o.ClassID,
                                        SubjectID = o.SubjectID,
                                        Judgement = o.Judgement,
                                        Semester = o.Semester
                                    }).ToList();
            }

            //Nang luc pham chat
            List<ReviewBookPupil> lstReviewBookEdu = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == schoolId
                && o.AcademicYearID == academicYearId
               // && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition).ToList();

            //Lay thong tin mien giam
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["SemesterID"] = semester;
            List<ExemptedSubject> lstExemptedSubjectsEdu = ExemptedSubjectBusiness.Search(tmpDic).ToList();
            //Lay thong tin dang ky mon tu chon
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["SemesterID"] = semester;

            List<RegisterSubjectSpecialize> lstSubjectSpecializesEdu = RegisterSubjectSpecializeBusiness.Search(tmpDic).ToList();

            //Thong tin khen thuong
            List<UpdateReward> lstUpdateRewardEdu = UpdateRewardBusiness.All.Where(o => o.AcademicYearID == academicYearId
                && o.PartitionID == partition
                && o.SchoolID == schoolId
                && o.SemesterID == semester).ToList();

            //Danh muc khen thuong
            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(o => o.SchoolID == schoolId).ToList();

            for (int iClass = 0; iClass < lstClass.Count; iClass++)
            {
                ClassProfile curClass = lstClass[iClass];
                IVTWorksheet curSheet = oBook.CopySheetToBeforeLast(secondSheet);

                //Lay danh sach mon hoc cua lop
                List<ClassSubject> lstClassSubjectClass = lstClassSubjectEdu.Where(o => o.ClassID == curClass.ClassProfileID).ToList();

                //Danh sach cac mon hoc Vnen cua lop
                List<ClassSubject> lstVnenSubject = lstClassSubjectClass.Where(o => o.IsSubjectVNEN == true).ToList();
                List<int> lstVnenSubjectId = lstClassSubjectClass.Where(o => o.IsSubjectVNEN == true).Select(o => o.SubjectID).ToList();

                //Danh sach cac mon khong phai mon Vnen
                List<ClassSubject> lstTT58Subject = lstClassSubjectClass.Where(o => o.IsSubjectVNEN != true).ToList();
                List<int> lstTT58SubjectId = lstClassSubjectClass.Where(o => o.IsSubjectVNEN != true).Select(o => o.SubjectID).ToList();

               //Thong tin danh gia cac mon cua lop
                List<TeacherNoteBookSemesterBO> lstNoteBookClass = lstNoteBookEdu.Where(o => o.ClassID == curClass.ClassProfileID && lstVnenSubjectId.Contains(o.SubjectID)).ToList();
                
                //Join voi ClassSubject de lay isCommenting
                lstNoteBookClass = (from nb in lstNoteBookClass
                                    join cs in lstClassSubjectClass on nb.SubjectID equals cs.SubjectID
                               select new TeacherNoteBookSemesterBO
                               {
                                   PupilID = nb.PupilID,
                                   SubjectID = nb.SubjectID,
                                   PERIODIC_SCORE_END = nb.PERIODIC_SCORE_END,
                                   PERIODIC_SCORE_END_JUDGLE = nb.PERIODIC_SCORE_END_JUDGLE,
                                   PERIODIC_SCORE_MIDDLE = nb.PERIODIC_SCORE_MIDDLE,
                                   PERIODIC_SCORE_MIDDLE_JUDGE = nb.PERIODIC_SCORE_MIDDLE_JUDGE,
                                   AVERAGE_MARK = nb.AVERAGE_MARK,
                                   AVERAGE_MARK_JUDGE = nb.AVERAGE_MARK_JUDGE,
                                   Rate = nb.Rate,
                                   SemesterID = nb.SemesterID,
                                   isCommenting = cs.IsCommenting
                               }).ToList();

                //Lay diem TBM mon hoc TT58 cua lop
                List<SummedUpRecordBO> lstSummedUpRecordClass = lstSummedUpRecordEdu.Where(o => o.ClassID == curClass.ClassProfileID && lstTT58SubjectId.Contains(o.SubjectID.GetValueOrDefault())).ToList();
                //lay diem CK mon TT58
                List<MarkRecordBO> lstMarkRecordClass = lstMarkRecoreEdu.Where(o => o.ClassID == curClass.ClassProfileID && lstTT58SubjectId.Contains(o.SubjectID.GetValueOrDefault())).ToList();
                List<JudgeRecordBO> lstJudgeRecordClass = lstJudgeRecordEdu.Where(o => o.ClassID == curClass.ClassProfileID && lstTT58SubjectId.Contains(o.SubjectID.GetValueOrDefault())).ToList();
                
                //Join voi ClassSubject de lay isCommenting
                lstSummedUpRecordClass = (from sur in lstSummedUpRecordClass
                                          join cs in lstClassSubjectClass on sur.SubjectID equals cs.SubjectID
                                     select new SummedUpRecordBO
                                     {
                                         PupilID = sur.PupilID,
                                         SubjectID = sur.SubjectID,
                                         SummedUpMark = sur.SummedUpMark,
                                         JudgementResult = sur.JudgementResult,
                                         Semester = sur.Semester,
                                         IsCommenting = cs.IsCommenting
                                     }).ToList();

                //Lay danh sach hoc sinh cua lop
                List<PupilOfClass> lstPocClass = lstPocEdu.Where(o => o.ClassID == curClass.ClassProfileID).ToList();

                //Lay gvcn
                Employee headTeacher = lstHeadTeacher.Where(o => o.EmployeeID == curClass.HeadTeacherID).FirstOrDefault();

                int pocStartRow = firstRow;
                for (int iPoc = 0; iPoc < lstPocClass.Count; iPoc++)
                {
                    PupilOfClass poc = lstPocClass[iPoc];

                    //Thong tin nghi hoc
                    List<PupilAbsence> lstPupilAbencePupil = lstPupilAbenceEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).ToList();

                    //Danh gia cac mon
                    List<TeacherNoteBookSemesterBO> lstNoteBookPupil = lstNoteBookClass.Where(o => o.PupilID == poc.PupilID).ToList();

                   //Diem TBM mon hoc TT58
                    List<SummedUpRecordBO> lstSummedUpRecordPupil = lstSummedUpRecordClass.Where(o => o.PupilID == poc.PupilID).ToList();

                    //Diem KTHK mon TT58
                    List<MarkRecordBO> lstMarkRecordPupil = lstMarkRecordClass.Where(o => o.PupilID == poc.PupilID).ToList();

                    List<JudgeRecordBO> lstJudgeRecordPupil = lstJudgeRecordClass.Where(o => o.PupilID == poc.PupilID).ToList();

                    //Nang luc, pham chat, danh gia cuoi ky
                    ReviewBookPupil reviewBookPupil = lstReviewBookEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).FirstOrDefault();

                    //Thong tin mien giam
                    List<ExemptedSubject> lstExemptedSubjectsPupil = lstExemptedSubjectsEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).ToList();

                    //Thong tin mon tu chon
                    List<RegisterSubjectSpecialize> lstSubjectSpecializesPupil = lstSubjectSpecializesEdu.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).ToList();

                    //Khen thuong
                    UpdateReward rcf = lstUpdateRewardEdu.Where(o => o.PupilID == poc.PupilID && o.ClassID == poc.ClassID).FirstOrDefault();

                    //Fill data for header here
                    FillHeaderVnen(pocStartRow, curSheet, templateHeaderRange, poc, lstPupilAbencePupil);

                    //Fill mon va diem cac mon
                    FillLearningResultVnen(pocStartRow, curSheet, lstClassSubjectClass, lstNoteBookPupil, lstSummedUpRecordPupil, lstMarkRecordPupil, lstJudgeRecordPupil, reviewBookPupil,
                        lstExemptedSubjectsPupil, lstSubjectSpecializesPupil, rcf, lstRewardFinal, semester);

                    //Fill footer
                   
                    FillFooterVnen(pocStartRow, curSheet, templateFooterRange, lstClassSubjectClass, headTeacher != null ? headTeacher.FullName : String.Empty);

                    pocStartRow += lstClassSubjectClass.Count + 25;
                    curSheet.SetBreakPage(pocStartRow);
                }

                curSheet.Name = ReportUtils.StripVNSign(curClass.DisplayName);
                curSheet.SetFontName("Times New Roman", 13);
                curSheet.SetPrintArea("$A2:$P" + (pocStartRow - 1));
                curSheet.PageMaginRight = 0.5;
                curSheet.PageMaginLeft = 0.5;
                curSheet.PageMaginBottom = 0.5;
                curSheet.PageMaginTop = 0.5;
                curSheet.FitAllColumnsOnOnePage = true;
                curSheet.Worksheet.Zoom = 70;
            }

            //Fill sheet thong tin chung
            //Ten truong
            thirdSheet.SetCellValue("B4", school.SchoolName);
            string title1 = "PHIẾU LIÊN LẠC " + ((semester == 1) ? "HỌC KỲ I - NĂM HỌC " : "HỌC KỲ II - NĂM HỌC ") + ay.DisplayTitle;
            thirdSheet.SetCellValue("B5", title1);
            thirdSheet.SetCellValue("B7", school.HeadMasterName);
            DateTime date = DateTime.Now.Date;
            string title2 = (school.District != null ? school.District.DistrictName : "") + ", ngày " + date.Day + " tháng " + date.Month + " năm " + date.Year;
            thirdSheet.SetCellValue("B8", title2);

            ////Xoa sheet dau tien

            firstSheet.Delete();
            secondSheet.Delete();

            return oBook.ToStream();
        }

        private void FillHeaderVnen(int pocStartRow, IVTWorksheet sheet, IVTRange template, PupilOfClass poc, List<PupilAbsence> lstPupilAbsence)
        {
            sheet.CopyPasteSameSize(template, pocStartRow, 1);
            VTVector vtStart = new VTVector(pocStartRow, firstColumn);
            VTVector vtNational1 = new VTVector(0, 6);
            VTVector vtNational2 = new VTVector(1, 6);
            VTVector vtTitle = new VTVector(2, 0);
            VTVector vtName = new VTVector(3, 2);
            VTVector vtClass = new VTVector(3, 12);
            VTVector vtAbsence = new VTVector(4, 2);
            VTVector vtAbsenceP = new VTVector(4, 8);
            VTVector vtAbsenceK = new VTVector(4, 14);

            sheet.SetCellValue(vtStart, "='Thong tin chung'!B4");
            sheet.SetCellValue(vtStart + vtNational1, "='Thong tin chung'!B2");
            sheet.SetCellValue(vtStart + vtNational2, "='Thong tin chung'!B3");
            sheet.SetCellValue(vtStart + vtTitle, "='Thong tin chung'!B5");
            sheet.SetCellValue(vtStart + vtName, poc.PupilProfile.FullName);
            sheet.SetCellValue(vtStart + vtClass, poc.ClassProfile.DisplayName);
            sheet.SetCellValue(vtStart + vtAbsence, lstPupilAbsence.Count);
            List<PupilAbsence> lstFinalPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(lstPupilAbsence);
            sheet.SetCellValue(vtStart + vtAbsence, lstFinalPupilAbsence.Count);
            sheet.SetCellValue(vtStart + vtAbsenceP, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).Count());
            sheet.SetCellValue(vtStart + vtAbsenceK, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).Count());
        }
        private void FillHeaderVnenMonth(int pocStartRow, IVTWorksheet sheet, IVTRange template, PupilOfClass poc, List<PupilAbsence> lstPupilAbsence)
        {
            sheet.CopyPasteSameSize(template, pocStartRow, 1);
            VTVector vtStart = new VTVector(pocStartRow, firstColumn);
            VTVector vtNational1 = new VTVector(0, 5);
            VTVector vtNational2 = new VTVector(1, 5);
            VTVector vtTitle = new VTVector(2, 0);
            VTVector vtName = new VTVector(3, 2);
            VTVector vtClass = new VTVector(3, 14);
            VTVector vtAbsence = new VTVector(4, 2);
            VTVector vtAbsenceP = new VTVector(4, 7);
            VTVector vtAbsenceK = new VTVector(4, 14);

            sheet.SetCellValue(vtStart, "='Thong tin chung'!B4");
            sheet.SetCellValue((vtStart + vtNational1), "='Thong tin chung'!B2");
            sheet.SetCellValue((vtStart + vtNational2), "='Thong tin chung'!B3");
            sheet.SetCellValue(vtStart + vtTitle, "='Thong tin chung'!B5");
            sheet.SetCellValue(vtStart + vtName, poc.PupilProfile.FullName);
            sheet.SetCellValue(vtStart + vtClass, poc.ClassProfile.DisplayName);
            sheet.SetCellValue(vtStart + vtAbsence, lstPupilAbsence.Count);
            List<PupilAbsence> lstFinalPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(lstPupilAbsence);
            sheet.SetCellValue(vtStart + vtAbsence, lstFinalPupilAbsence.Count);
            sheet.SetCellValue(vtStart + vtAbsenceP, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).Count());
            sheet.SetCellValue(vtStart + vtAbsenceK, lstFinalPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).Count());
        }
        private void FillFooterVnen(int pocStartRow, IVTWorksheet sheet, IVTRange template, List<ClassSubject> lstClassSubject, string headTeacherName)
        {
            int startRow = pocStartRow + 10 + lstClassSubject.Count;
            sheet.CopyPasteSameSize(template, startRow, 1);

            VTVector vectorStart = new VTVector(startRow, firstColumn);

            VTVector vectorProvinceAndDate =  new VTVector(6, 7);
            VTVector vectorHeadMaster = new VTVector(7, 0);
            VTVector vectorHeadMasterName = new VTVector(14, 0);
            VTVector vectorHeadTeacherName = new VTVector(14, 7);

           
            //Footer
            sheet.SetCellValue(vectorStart + vectorProvinceAndDate, "='Thong tin chung'!B8");
            sheet.SetCellValue(vectorStart + vectorHeadMaster, "='Thong tin chung'!$B$6");
            sheet.SetCellValue(vectorStart + vectorHeadMasterName, "='Thong tin chung'!$B$7");
            sheet.SetCellValue(vectorStart + vectorHeadTeacherName, headTeacherName);

        }
        private void FillFooterVnenMonth(int pocStartRow, IVTWorksheet sheet, IVTRange template, List<ClassSubject> lstClassSubject, string headTeacherName)
        {
            int startRow = pocStartRow + 12 + lstClassSubject.Count;
            sheet.CopyPasteSameSize(template, startRow, 1);

            VTVector vectorStart = new VTVector(startRow, firstColumn);

            VTVector vectorProvinceAndDate = new VTVector(5, 7);
            VTVector vectorHeadMaster = new VTVector(6, 0);
            VTVector vectorHeadMasterName = new VTVector(11, 0);
            VTVector vectorHeadTeacherName = new VTVector(11, 7);
            

            //Footer
            sheet.SetCellValue(vectorStart + vectorProvinceAndDate, "='Thong tin chung'!B8");
            sheet.SetCellValue(vectorStart + vectorHeadMaster, "='Thong tin chung'!$B$6");
            sheet.SetCellValue(vectorStart + vectorHeadMasterName, "='Thong tin chung'!B7");
            sheet.SetCellValue(vectorStart + vectorHeadTeacherName, headTeacherName);

        }

        private void FillCommentSubject(int pocStartRow, IVTWorksheet sheet, List<ClassSubject> lstClassSubject, List<TeacherNoteBookMonth> lstTNM)
        {
            int startRow = pocStartRow + 8;
            int curRow = startRow - 1;
            int lastRow = startRow + lstClassSubject.Count + 1;
            int rowHeight = 40;
            ClassSubject objCS = null;
            TeacherNoteBookMonth objTNM = null;
            int SubjectID = 0;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                objCS = lstClassSubject[i];
                SubjectID = objCS.SubjectID;
                sheet.SetCellValue(startRow, 1, objCS.SubjectCat.DisplayName);
                sheet.GetRange(startRow, 1, startRow, 4).Merge();
                sheet.GetRange(startRow, 5, startRow, 11).Merge();
                sheet.GetRange(startRow, 12, startRow, 17).Merge();
                sheet.GetRange(startRow, 1, startRow, 17).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet.GetRange(startRow, 1, startRow, 17).WrapText();
                sheet.SetRowHeight(startRow, rowHeight);
                objTNM = lstTNM.Where(p => p.SubjectID == SubjectID).FirstOrDefault();
                if (objTNM != null)
                {
                    sheet.SetCellValue(startRow, 5, objTNM.CommentSubject);
                    sheet.SetCellValue(startRow, 12, objTNM.CommentCQ);
                }
                startRow++;
            }
            sheet.GetRange(curRow, 1, startRow - 1, 17).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All); 
        }

        private void FillLearningResultVnen(int pocStartRow, IVTWorksheet sheet, List<ClassSubject> lstClassSubject, List<TeacherNoteBookSemesterBO> lstNoteBook,
            List<SummedUpRecordBO> lstSummedUpRecord,List<MarkRecordBO> lstMarkRecord,List<JudgeRecordBO> lstJudgeRecord, ReviewBookPupil reviewBookPupil, List<ExemptedSubject> lstExemptedSubjects,
            List<RegisterSubjectSpecialize> lstSubjectSpecializes, UpdateReward updateReward, List<RewardFinal> lstRewardFinal, int semester)
        {
            //Lay ra vi tri bat dau fill danh sach mon
            int startRow = pocStartRow + 9;
            int lastRow = startRow + lstClassSubject.Count + 1;
            int curRow = startRow;
            int rowHeight = 25;
            double rewardRowHeight = 82.5;

            string endMark = String.Empty;
            string MidMark = string.Empty;
            string TBM = string.Empty;
            string TBMCN = string.Empty;
            string evaluation = String.Empty;
            SummedUpRecordBO sur = new SummedUpRecordBO();
            MarkRecordBO objMark = new MarkRecordBO();
            JudgeRecordBO objJudge = new JudgeRecordBO();
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs = lstClassSubject[i];
                //ten mon
                sheet.SetCellValue(curRow, 1, cs.SubjectCat.DisplayName);
                //Merge
                sheet.GetRange(curRow, 1, curRow, 4).Merge();

                MidMark = string.Empty;
                endMark = String.Empty;
                evaluation = String.Empty;
                TBM = string.Empty;
                TBMCN = string.Empty;

                //Co phai mon mien giam
                bool IsExempted;
                //Co phai mon khong hoc
                bool IsNotLearning;

                IsExempted = lstExemptedSubjects.Select(o=>o.SubjectID).Contains(cs.SubjectID);
                IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializes.Select(o=>o.SubjectID).Contains(cs.SubjectID);


                string strSpecial = String.Empty;
                if (IsNotLearning)
                {
                    strSpecial = "KH";
                }
                if (IsExempted)
                {
                    strSpecial = "MG";
                }
                #region Mon VNEN
                if (cs.IsSubjectVNEN == true)
                {
                    if (strSpecial == String.Empty)
                    {
                        TeacherNoteBookSemesterBO tnbs;

                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            tnbs = lstNoteBook.Where(o => o.SubjectID == cs.SubjectID).FirstOrDefault();
                            if (tnbs != null)
                            {
                                //if (tnbs.Rate == 1)
                                //{
                                //    evaluation = "HT";
                                //}
                                //else if (tnbs.Rate == 2)
                                //{
                                //    evaluation = "CHT";
                                //}
                                //Mon nhan xet
                                if (cs.IsCommenting == 1)
                                {
                                    MidMark = tnbs.PERIODIC_SCORE_MIDDLE_JUDGE;
                                    endMark = tnbs.PERIODIC_SCORE_END_JUDGLE;
                                    TBM = tnbs.AVERAGE_MARK_JUDGE;
                                }
                                //Mon tinh diem
                                else
                                {
                                    MidMark = tnbs.PERIODIC_SCORE_MIDDLE != 0 && tnbs.PERIODIC_SCORE_MIDDLE != 10 ? String.Format("{0:0.0}", tnbs.PERIODIC_SCORE_MIDDLE) : String.Format("{0:0}", tnbs.PERIODIC_SCORE_MIDDLE);
                                    endMark = tnbs.PERIODIC_SCORE_END != 0 && tnbs.PERIODIC_SCORE_END != 10 ? String.Format("{0:0.0}", tnbs.PERIODIC_SCORE_END) : String.Format("{0:0}", tnbs.PERIODIC_SCORE_END);
                                    TBM = tnbs.AVERAGE_MARK != 0 && tnbs.AVERAGE_MARK != 10 ? String.Format("{0:0.0}", tnbs.AVERAGE_MARK) : String.Format("{0:0}", tnbs.AVERAGE_MARK);
                                }
                            }
                        }
                        else
                        {
                            tnbs = lstNoteBook.Where(o => o.SubjectID == cs.SubjectID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();//Diem HKII
                            if (tnbs != null)
                            {
                                //Mon nhan xet
                                if (cs.IsCommenting == 1)
                                {
                                    MidMark = tnbs.PERIODIC_SCORE_MIDDLE_JUDGE;
                                    endMark = tnbs.PERIODIC_SCORE_END_JUDGLE;
                                    TBM = tnbs.AVERAGE_MARK_JUDGE;
                                }
                                //Mon tinh diem
                                else
                                {
                                    MidMark = tnbs.PERIODIC_SCORE_MIDDLE != 0 && tnbs.PERIODIC_SCORE_MIDDLE != 10 ? String.Format("{0:0.0}", tnbs.PERIODIC_SCORE_MIDDLE) : String.Format("{0:0}", tnbs.PERIODIC_SCORE_MIDDLE);
                                    endMark = tnbs.PERIODIC_SCORE_END != 0 && tnbs.PERIODIC_SCORE_END != 10 ? String.Format("{0:0.0}", tnbs.PERIODIC_SCORE_END) : String.Format("{0:0}", tnbs.PERIODIC_SCORE_END);
                                    TBM = tnbs.AVERAGE_MARK != 0 && tnbs.AVERAGE_MARK != 10 ? String.Format("{0:0.0}", tnbs.AVERAGE_MARK) : String.Format("{0:0}", tnbs.AVERAGE_MARK);
                                }
                            }
                            tnbs = lstNoteBook.Where(o => o.SubjectID == cs.SubjectID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL).FirstOrDefault();//TBMCN
                            if (tnbs != null)
                            {
                                //Mon nhan xet
                                if (cs.IsCommenting == 1)
                                {
                                    TBMCN = tnbs.AVERAGE_MARK_JUDGE;
                                }
                                //Mon tinh diem
                                else
                                {
                                    TBMCN = tnbs.AVERAGE_MARK != 0 && tnbs.AVERAGE_MARK != 10 ? String.Format("{0:0.0}", tnbs.AVERAGE_MARK) : String.Format("{0:0}", tnbs.AVERAGE_MARK);
                                }
                            }
                        }
                    }
                    else
                    {
                        endMark = strSpecial;
                    }
                }
                #endregion
                #region Mon TT58
                else
                {
                    if (strSpecial == String.Empty)
                    {
                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            sur = lstSummedUpRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            if (cs.IsCommenting == 1)
                            {
                                objJudge = lstJudgeRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (objJudge != null)
                                {
                                    endMark = objJudge.Judgement;
                                }

                                if (sur != null)
                                {
                                    TBM = sur.JudgementResult;
                                }
                            }
                            else
                            {
                                objMark = lstMarkRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (objMark != null)
                                {
                                    endMark = objMark.Mark != 0 && objMark.Mark != 10 ? String.Format("{0:0.0}", objMark.Mark) : String.Format("{0:0}", objMark.Mark);
                                }

                        if (sur != null)
                        {
                                    TBM = sur.SummedUpMark != 0 && sur.SummedUpMark != 10 ? String.Format("{0:0.0}", sur.SummedUpMark) : String.Format("{0:0}", sur.SummedUpMark);
                                }
                            }
                        }
                        else
                        {
                            sur = lstSummedUpRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                            if (cs.IsCommenting == 1)
                            {
                                objJudge = lstJudgeRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (objJudge != null)
                                {
                                    endMark = objJudge.Judgement;
                                }
                                if (sur != null)
                                {
                                    TBM = sur.JudgementResult;
                                }
                            }
                            else
                            {
                                objMark = lstMarkRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (objMark != null)
                                {
                                    endMark = objMark.Mark != 0 && objMark.Mark != 10 ? String.Format("{0:0.0}", objMark.Mark) : String.Format("{0:0}", objMark.Mark);
                                }

                                if (sur != null)
                                {
                                    TBM = sur.SummedUpMark != 0 && sur.SummedUpMark != 10 ? String.Format("{0:0.0}", sur.SummedUpMark) : String.Format("{0:0}", sur.SummedUpMark);
                                }
                            }

                            sur = lstSummedUpRecord.Where(o => o.SubjectID == cs.SubjectID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                            if (sur != null)
                            {
                                if (cs.IsCommenting == 1)
                                {
                                    TBMCN = sur.JudgementResult;
                                }
                                else
                                {
                                    TBMCN = sur.SummedUpMark != 0 && sur.SummedUpMark != 10 ? String.Format("{0:0.0}", sur.SummedUpMark) : String.Format("{0:0}", sur.SummedUpMark);
                                }
                            }
                        }
                    }
                    else
                    {
                        endMark = strSpecial;
                    }
                }
                #endregion
                //Diem KTCK
                sheet.SetCellValue(curRow, 5, MidMark);
                sheet.SetCellValue(curRow, 6, endMark);

                //TBM
                sheet.SetCellValue(curRow, 7, TBM);
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    //TBMCN
                    sheet.SetCellValue(curRow, 8, TBMCN);    
                }
                else
                {
                    sheet.HideColumn(8);
                }

                sheet.SetRowHeight(curRow, rowHeight);

                curRow++;
            }

            //Danh gia nang luc, pham chat
            int midleRow =  startRow + (int)Math.Ceiling((double)lstClassSubject.Count / 2);
            int rewardRow = startRow + lstClassSubject.Count;
            
            string comment1 = String.Empty;
            string comment2 = String.Empty;
            string strCapacity = "Năng lực: ";
            string strQuality = "Phẩm chất: ";
            string strEvaluation = "Kết quả học tập, rèn luyện cả năm: ";

            if (reviewBookPupil != null)
            {
                //Nhan xet
                comment1 = reviewBookPupil.CQComment;
                comment2 = reviewBookPupil.CommentAdd;

                //Danh gia nang luc
                int? capacity = reviewBookPupil.CapacityRate;
                if (reviewBookPupil.CapacitySummer != null)
                {
                    capacity = reviewBookPupil.CapacitySummer;
                }
                
                if (capacity == 1)
                {
                    strCapacity = strCapacity + "Tốt";
                }
                else if (capacity == 2)
                {
                    strCapacity = strCapacity + "Đạt";
                }
                else if (capacity == 3)
                {
                    strCapacity = strCapacity + "Cần cố gắng";
                }

                //Danh gia pham chat
                int? quality = reviewBookPupil.QualityRate;
                if (reviewBookPupil.QualitySummer != null)
                {
                    quality = reviewBookPupil.QualitySummer;
                }
                
                if (quality == 1)
                {
                    strQuality = strQuality + "Tốt";
                }
                else if (quality == 2)
                {
                    strQuality = strQuality + "Đạt";
                }
                else if (quality == 3)
                {
                    strQuality = strQuality + "Cần cố gắng";
                }

                //Danh gia cuoi nam
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    if (reviewBookPupil.RateAndYear == 1 || reviewBookPupil.RateAdd == 1)
                    {
                        strEvaluation = strEvaluation + "Được lên lớp";
                    }
                    else if (reviewBookPupil.RateAdd == 2 || (reviewBookPupil.RateAndYear == 2 && reviewBookPupil.RateAdd == null))
                    {
                        strEvaluation = strEvaluation + "Ở lại lớp";
                    }

                }
            }

            sheet.SetCellValue(startRow, 9, "Biểu hiện nổi bật về sự tiến bộ, mức độ hình thành và phát triển phẩm chất, năng lực; đạt thành tích nổi bật trong các phong trào, các hoạt động giáo dục.");
            sheet.SetCellValue(midleRow, 9, "Những nội dung chưa hoàn thành chương trình; những điều cần khắc phục, giúp đỡ, bổ sung, rèn luyện thêm khi bắt đầu học kì 2.");
            sheet.SetCellValue(startRow, 13, comment1);
            sheet.SetCellValue(midleRow, 13, comment2);

            sheet.SetCellValue(rewardRow, 9, strCapacity);
            sheet.Worksheet.Cells[rewardRow, 9].Font.Bold = true;
            sheet.Worksheet.Cells[rewardRow, 9].Characters[1,"Năng lực:".Length].Font.Bold = false;

            sheet.SetCellValue(rewardRow, 13, strQuality);
            sheet.Worksheet.Cells[rewardRow, 13].Font.Bold = true;
            sheet.Worksheet.Cells[rewardRow, 13].Characters[1, "Phẩm chất:".Length].Font.Bold = false;

            //if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            //{
            //    sheet.SetCellValue(lastRow, 1, strEvaluation);
            //    sheet.Worksheet.Cells[lastRow, 1].Font.Bold = true;
            //    sheet.Worksheet.Cells[lastRow, 1].Characters[1, "Kết quả học tập, rèn luyện cả năm:".Length].Font.Bold = false;
            //    sheet.SetRowHeight(lastRow, endRowHeight);
            //}
            //else
            //{
            //    sheet.SetRowHeight(lastRow, 10);
            //}

            //Fill khen thuong
            string strRewardId = updateReward != null && updateReward.Rewards != null ? updateReward.Rewards : String.Empty;
            List<int> lstRewardId = strRewardId.Split(',').Select(tag => tag.Trim()).
                          Where(tag => !string.IsNullOrEmpty(tag)).Select(o => Int32.Parse(o)).ToList();
            List<RewardFinal> lstRf = lstRewardFinal.Where(o => lstRewardId.Contains(o.RewardFinalID)).ToList();

            string strReward = "Khen thưởng: ";
            for (int i = 0; i < lstRf.Count; i++)
            {
                RewardFinal rf = lstRf[i];
                strReward = strReward + rf.RewardMode;

                if (i < lstRf.Count - 1)
                {
                    strReward = strReward + "; ";
                }
            }

            sheet.SetCellValue(rewardRow, firstColumn, strReward);


            //Merge
            IVTRange globalRange = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? sheet.GetRange(startRow, firstColumn, lastRow - 1, 16) : sheet.GetRange(startRow, firstColumn, lastRow, 16);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            globalRange.SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            globalRange.SetHAlign(VTHAlign.xlHAlignCenter);

            sheet.GetRange(startRow, 9, midleRow - 1, 12).Merge();
            sheet.GetRange(startRow, 9, midleRow - 1, 12).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.GetRange(startRow, 9, midleRow - 1, 12).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
            sheet.GetRange(startRow, 9, midleRow - 1, 12).Range.WrapText = true;

            sheet.GetRange(startRow, 13, midleRow - 1, 16).Merge();
            sheet.GetRange(startRow, 13, midleRow - 1, 16).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.GetRange(startRow, 13, midleRow - 1, 16).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
            sheet.GetRange(startRow, 13, midleRow - 1, 16).Range.WrapText = true;

            sheet.GetRange(midleRow, 9, startRow + lstClassSubject.Count - 1, 12).Merge();
            sheet.GetRange(midleRow, 9, startRow + lstClassSubject.Count - 1, 12).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.GetRange(midleRow, 9, startRow + lstClassSubject.Count - 1, 12).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
            sheet.GetRange(midleRow, 9, startRow + lstClassSubject.Count - 1, 12).Range.WrapText = true;

            sheet.GetRange(midleRow, 13, startRow + lstClassSubject.Count - 1, 16).Merge();
            sheet.GetRange(midleRow, 13, startRow + lstClassSubject.Count - 1, 16).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.GetRange(midleRow, 13, startRow + lstClassSubject.Count - 1, 16).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
            sheet.GetRange(midleRow, 13, startRow + lstClassSubject.Count - 1, 16).Range.WrapText = true;

            sheet.GetRange(rewardRow, 1, rewardRow, 8).Merge();
            sheet.GetRange(rewardRow, 1, rewardRow, 8).SetHAlign(VTHAlign.xlHAlignLeft);
            sheet.GetRange(rewardRow, 1, rewardRow, 8).Range.WrapText = true;

            sheet.GetRange(rewardRow, 9, rewardRow, 12).Merge();
            sheet.GetRange(rewardRow, 13, rewardRow, 16).Merge();
            sheet.GetRange(lastRow, firstColumn, lastRow, lastColumn).Merge();

            sheet.SetRowHeight(rewardRow, rewardRowHeight);


        }

        public ProcessedReport InsertVnenCommunicationNoteReport(IDictionary<string, object> dic, Stream data)
        {

            int semester = Utils.GetInt(dic["Semester"]);
            int classId = Utils.GetInt(dic["Class"]);
            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            int typeExport = Utils.GetInt(dic["TypeExport"]);
            string monthName = string.Empty;

            string reportCode = string.Empty;
            if (typeExport == 1)
            {
                reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_VNEN_COMMUNICATION_NOTE_MONTH;
                monthName = Utils.GetString(dic["MonthName"]);
            }
            

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            string outputNamePattern = reportDef.OutputNamePattern;

            ClassProfile cp = ClassProfileBusiness.Find(classId);
            String className = String.Empty;
            if (cp != null)
            {
                className = cp.DisplayName;
            }
            if (String.IsNullOrEmpty(className))
            {
                className = "Khoi" + educationLevel;
            }
            outputNamePattern = outputNamePattern.Replace("[Class]", className);

            if (typeExport == 1)
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", semester.ToString());
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[MonthName]", Utils.StripVNSignAndSpace(monthName));
            }
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return pr;
        }
        #endregion
    }
}
