﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Lý do thôi học
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class LeavingReasonBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion

        #region insert
        /// <summary>
        /// Thêm mới Lý do thôi học
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertLeavingReason">doi tuong can them</param>
        /// <returns></returns>
        public override LeavingReason Insert(LeavingReason insertLeavingReason)
        {


            //resolution rong
            Utils.ValidateRequire(insertLeavingReason.Resolution, "LeavingReason_Label_Resolution");

            Utils.ValidateMaxLength(insertLeavingReason.Resolution, RESOLUTION_MAX_LENGTH, "LeavingReason_Label_Resolution");
            Utils.ValidateMaxLength(insertLeavingReason.Description, DESCRIPTION_MAX_LENGTH, "LeavingReason_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertLeavingReason.Resolution, GlobalConstants.LIST_SCHEMA, "LeavingReason", "Resolution", true, 0, "LeavingReason_Label_Resolution");
            //this.CheckDuplicate(insertLeavingReason.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "LeavingReason", "AppliedLevel", true, 0, "LeavingReason_Label_ShoolID");
           // this.CheckDuplicateCouple(insertLeavingReason.Resolution, insertLeavingReason.SchoolID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "LeavingReason", "Resolution", "SchoolID", true, 0, "LeavingReason_Label_Resolution");
            //kiem tra range

           


            insertLeavingReason.IsActive = true;

            return base.Insert(insertLeavingReason);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Lý do thôi học
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateLeavingReason">Đối tượng Update</param>
        /// <returns></returns>
        public override LeavingReason Update(LeavingReason updateLeavingReason)
        {

            //check avai
            new LeavingReasonBusiness(null).CheckAvailable(updateLeavingReason.LeavingReasonID, "LeavingReason_Label_LeavingReasonID");

            //resolution rong
            Utils.ValidateRequire(updateLeavingReason.Resolution, "LeavingReason_Label_Resolution");

            Utils.ValidateMaxLength(updateLeavingReason.Resolution, RESOLUTION_MAX_LENGTH, "LeavingReason_Label_Resolution");
            Utils.ValidateMaxLength(updateLeavingReason.Description, DESCRIPTION_MAX_LENGTH, "LeavingReason_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
             this.CheckDuplicate(updateLeavingReason.Resolution, GlobalConstants.LIST_SCHEMA, "LeavingReason", "Resolution", true, updateLeavingReason.LeavingReasonID, "LeavingReason_Label_Resolution");

            //this.CheckDuplicateCouple(updateLeavingReason.Resolution, updateLeavingReason.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "LeavingReason", "Resolution", "SchoolID", true, updateLeavingReason.LeavingReasonID, "LeavingReason_Label_Resolution");

            // this.CheckDuplicate(updateLeavingReason.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "LeavingReason", "AppliedLevel", true, updateLeavingReason.LeavingReasonID, "LeavingReason_Label_ShoolID");

           
            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu LeavingReasonID ->vao csdl lay LeavingReason tuong ung roi 
            //so sanh voi updateLeavingReason.SchoolID


            //.................



            updateLeavingReason.IsActive = true;

            return base.Update(updateLeavingReason);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Lý do thôi học
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="LeavingReasonId">id doi tuong can xoa</param>
        public  void Delete(int LeavingReasonId)
        {
            //check avai
            new LeavingReasonBusiness(null).CheckAvailable(LeavingReasonId, "LeavingReason_Label_LeavingReasonID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "LeavingReason",LeavingReasonId, "LeavingReason_Label_LeavingReasonIDFailed");

            base.Delete(LeavingReasonId,true);


        }
        #endregion

        #region Search
        /// <summary>
        ///Tìm kiếm Lý do thôi học
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<LeavingReason> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");

            bool? isActive = Utils.GetIsActive(dic,"IsActive");

            IQueryable<LeavingReason> lsLeavingReason = this.LeavingReasonRepository.All;

         

             if (isActive.HasValue)
            {
            lsLeavingReason = lsLeavingReason.Where(em => (em.IsActive == isActive));
            }



          

            if (resolution.Trim().Length != 0)
            {

                lsLeavingReason = lsLeavingReason.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (description.Trim().Length != 0)
            {

                lsLeavingReason = lsLeavingReason.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

           
            return lsLeavingReason;
        }
        #endregion
        
    }
}