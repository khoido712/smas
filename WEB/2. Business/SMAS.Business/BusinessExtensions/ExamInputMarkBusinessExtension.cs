﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;

namespace SMAS.Business.Business
{

    public partial class ExamInputMarkBusiness
    {
        #region Search
        public IQueryable<ExamInputMark> Search(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic, "ExaminationsID");
            long examGroupID = Utils.GetLong(dic, "ExamGroupID");
            int subjectID = Utils.GetInt(dic, "SubjectID");

            IQueryable<ExamInputMark> query = from eim in ExamInputMarkRepository.All
                                              where eim.AcademicYearID == academicYearID && eim.SchoolID == schoolID && eim.LastDigitSchoolID == lastDigitSchoolID
                                              select eim;


            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }

            return query;
        }

        public IQueryable<ExamInputMarkBO> SearchBO(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic, "ExaminationsID");
            long examGroupID = Utils.GetLong(dic, "ExamGroupID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            long examRoomID = Utils.GetLong(dic, "ExamRoomID");
            string examineeNumber = Utils.GetString(dic, "ExamineeNumber");
            string pupilName = Utils.GetString(dic, "PupilName");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");

            IQueryable<ExamInputMarkBO> query = from eim in ExamInputMarkRepository.All.Where(e => e.LastDigitSchoolID == lastDigitSchoolID && e.AcademicYearID == academicYearID && e.ExaminationsID == examinationsID)
                                                join ep in ExamPupilRepository.All.Where(e => e.LastDigitSchoolID == lastDigitSchoolID) on new { eim.ExaminationsID, eim.ExamGroupID, eim.PupilID }
                                                      equals new { ep.ExaminationsID, ep.ExamGroupID, ep.PupilID }
                                                join pp in PupilProfileRepository.All on eim.PupilID equals pp.PupilProfileID
                                                join poc in PupilOfClassRepository.All on ep.PupilID equals poc.PupilID
                                                join cp in ClassProfileRepository.All.Where(c => c.AcademicYearID == academicYearID && c.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                                                join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID) on ep.ExamRoomID equals er.ExamRoomID
                                                join sc in SubjectCatRepository.All on eim.SubjectID equals sc.SubjectCatID
                                                where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                && poc.AcademicYearID == academicYearID
                                                select new ExamInputMarkBO
                                                {

                                                    AcademicYearID = eim.AcademicYearID,
                                                    ActualMark = eim.ActualMark,
                                                    ExamGroupID = eim.ExamGroupID,
                                                    ExaminationsID = eim.ExaminationsID,
                                                    ExamineeNumber = ep.ExamineeNumber,
                                                    ExamPupilID = ep.ExamPupilID,
                                                    ExamInputMarkID = eim.ExamInputMarkID,
                                                    ExamMark = eim.ExamMark,
                                                    ExamRoomID = er.ExamRoomID,
                                                    ExamRoomCode = er.ExamRoomCode,
                                                    LastDigitSchoolID = eim.LastDigitSchoolID,
                                                    PupilCode = pp.PupilCode,
                                                    PupilID = eim.PupilID,
                                                    PupilName = pp.FullName,
                                                    BirthDay = pp.BirthDate,
                                                    EthnicID = pp.EthnicID,
                                                    Genre = pp.Genre,
                                                    SchoolID = eim.SchoolID,
                                                    SubjectID = eim.SubjectID,
                                                    SubjectName = sc.SubjectName,
                                                    ClassID = poc.ClassID,
                                                    ClassName = cp.DisplayName,
                                                    ExamJudgeMark = eim.ExamJudgeMark,
                                                    EducationLevelID = cp.EducationLevelID

                                                };
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (examineeNumber.Trim().Length > 0)
            {
                query = query.Where(o => (o.ExamineeNumber.ToUpper().Contains(examineeNumber.ToUpper())));
            }
            if (pupilName.Trim().Length > 0)
            {
                query = query.Where(o => (o.PupilName.ToUpper().Contains(pupilName.ToUpper())));
            }
            if (educationLevelID != 0)
            {
                query = query.Where(o => o.EducationLevelID == educationLevelID);
            }
            if (classID != 0)
            {
                query = query.Where(o => o.ClassID == classID);
            }
            return query;


        }

        public IQueryable<ExamInputMarkBO> SearchForResultSearch(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic, "ExaminationsID");
            long examGroupID = Utils.GetLong(dic, "ExamGroupID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            long examRoomID = Utils.GetLong(dic, "ExamRoomID");
            string examineeNumber = Utils.GetString(dic, "ExamineeNumber");
            string pupilName = Utils.GetString(dic, "PupilName");

            IQueryable<ExamInputMarkBO> query = from ep in ExamPupilRepository.All
                                                join eim in ExamInputMarkRepository.All.Where(o => o.ExaminationsID == examinationsID && o.SubjectID == subjectID && o.LastDigitSchoolID == lastDigitSchoolID && o.AcademicYearID == academicYearID) on new { ep.ExaminationsID, ep.ExamGroupID, ep.PupilID }
                                                      equals new { eim.ExaminationsID, eim.ExamGroupID, eim.PupilID } into des
                                                from x in des.DefaultIfEmpty()
                                                join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                join cp in ClassProfileRepository.All.Where(c => c.AcademicYearID == academicYearID && c.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                                                join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID) on ep.ExamRoomID equals er.ExamRoomID into erl
                                                from ler in erl.DefaultIfEmpty()
                                                where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                && poc.AcademicYearID == academicYearID
                                                && ep.ExaminationsID == examinationsID
                                                && ep.ExamGroupID == examGroupID
                                                && ep.LastDigitSchoolID == lastDigitSchoolID
                                                select new ExamInputMarkBO
                                                {

                                                    ActualMark = x != null ? x.ActualMark : null,
                                                    ExamineeNumber = ep.ExamineeNumber,
                                                    ExamPupilID = ep.ExamPupilID,
                                                    ExamMark = x != null ? x.ExamMark : null,
                                                    ExamRoomID = ep.ExamRoomID,
                                                    ExamRoomCode = ler != null ? ler.ExamRoomCode : null,
                                                    PupilCode = pp.PupilCode,
                                                    PupilID = ep.PupilID,
                                                    PupilName = pp.FullName,
                                                    BirthDay = pp.BirthDate,
                                                    EthnicID = pp.EthnicID,
                                                    Genre = pp.Genre,
                                                    ClassID = poc.ClassID,
                                                    ClassName = cp.DisplayName,
                                                    ExamJudgeMark = x != null ? x.ExamJudgeMark : null
                                                };

            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (examineeNumber.Trim().Length > 0)
            {
                query = query.Where(o => (o.ExamineeNumber.ToUpper().Contains(examineeNumber.ToUpper())));
            }
            if (pupilName.Trim().Length > 0)
            {
                query = query.Where(o => (o.PupilName.ToUpper().Contains(pupilName.ToUpper())));
            }

            return query;

        }

        /// <summary>
        /// Ham tim kiem cac diem thi co the chuyen (Không chuyển điểm với học sinh đã xóa, trạng thái chuyển trường, thôi học, đã tốt nghiệp)
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="ExaminationsID"></param>
        /// <returns></returns>
        public IQueryable<ExamInputMark> SearchForTransfer(int? AcademicYearID, int? SchoolID, long ExaminationsID)
        {
            int partition = UtilsBusiness.GetPartionId(SchoolID.GetValueOrDefault());
            IQueryable<ExamInputMark> query = from eim in ExamInputMarkRepository.All
                                              join ep in ExamPupilRepository.All.Where(c => c.LastDigitSchoolID == partition) on new { eim.ExaminationsID, eim.ExamGroupID, eim.PupilID }
                                                    equals new { ep.ExaminationsID, ep.ExamGroupID, ep.PupilID }
                                              join pp in PupilProfileRepository.All on eim.PupilID equals pp.PupilProfileID
                                              join poc in PupilOfClassRepository.All on ep.PupilID equals poc.PupilID
                                              where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                              && poc.AcademicYearID == AcademicYearID && eim.SchoolID == SchoolID
                                              && eim.LastDigitSchoolID == partition && eim.ExaminationsID == ExaminationsID
                                              && eim.AcademicYearID == AcademicYearID
                                              select eim;
            return query;
        }
        #endregion

        #region Report Danh sach diem thi
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamInputMarkReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_EXAM_INPUT_MARK;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamInputMarkReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int subjectID = Utils.GetLong(dic["SubjectID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_EXAM_INPUT_MARK;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Điền các thông tin tiêu đề báo cáo
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = exam.ExaminationsName;
            string strAcademicYear = " năm học " + academicYear.DisplayTitle;

            StringBuilder sb = new StringBuilder();
            sb.Append("KẾT QUẢ ĐIỂM THI  MÔN ");
            sb.Append(subject.SubjectName.ToUpper());
            sb.Append(" ");
            sb.Append(examGroup.ExamGroupName.ToUpper());
            string reportTitle = sb.ToString();

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("E4", provinceAndDate);
            oSheet.SetCellValue("A6", reportTitle);
            oSheet.SetCellValue("A7", examinationsName + strAcademicYear);

            //Fill du lieu
            List<ExamInputMarkBO> listResult;
            if (dic["ExaminationsID"] == null || dic["ExamGroupID"] == null || dic["SubjectID"] == null)
            {
                listResult = new List<ExamInputMarkBO>();
            }
            else
            {
                listResult = this.ExamInputMarkBusiness.SearchForResultSearch(dic).OrderBy(o => o.ExamineeNumber).ToList();
            }

            //Lay danh sach loi vi pham
            List<ExamPupilViolateBO> listViolate = ExamPupilViolateBusiness.GetListExamPupilViolate(dic);

            for (int i = 0; i < listResult.Count(); i++)
            {
                ExamInputMarkBO entity = listResult[i];
                entity.listViolate = listViolate.Where(o => o.ExamPupilID == entity.ExamPupilID).ToList();
            }

            //Lay kieu mon

            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SubjectID"] = subjectID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).FirstOrDefault();
            int isCommentSubject = GlobalConstants.ISCOMMENTING_TYPE_MARK;
            if (schoolSubject != null && schoolSubject.IsCommenting != null)
            {
                isCommentSubject = schoolSubject.IsCommenting.Value;
            }

            int startRow = 10;
            int lastRow = startRow + listResult.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 10;
            int curColumn = startColumn;

            for (int i = 0; i < listResult.Count; i++)
            {
                ExamInputMarkBO obj = listResult[i];
                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //Ma hoc sinh
                oSheet.SetCellValue(curRow, curColumn, obj.PupilCode);
                curColumn++;
                //Ho va ten
                oSheet.SetCellValue(curRow, curColumn, obj.PupilName);
                curColumn++;
                //Ngay sinh
                oSheet.SetCellValue(curRow, curColumn, obj.BirthDay.ToString("dd/MM/yyyy"));
                curColumn++;
                //Lop
                oSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                curColumn++;
                //Phong thi
                oSheet.SetCellValue(curRow, curColumn, obj.ExamRoomCode);
                curColumn++;
                //SBD
                oSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                curColumn++;
                //Diem
                if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    oSheet.SetCellValue(curRow, curColumn, obj.ExamMark);
                }
                else
                {
                    if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        oSheet.SetCellValue(curRow, curColumn, obj.ExamMark != 0 && obj.ExamMark != 10 ? String.Format("{0:0.0}", obj.ExamMark) : String.Format("{0:0}", obj.ExamMark));
                    }
                    else
                    {
                        oSheet.SetCellValue(curRow, curColumn, obj.ExamJudgeMark);
                    }
                }
                curColumn++;
                //Diem thuc te
                if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    oSheet.SetCellValue(curRow, curColumn, obj.ActualMark);
                }
                else
                {
                    if (obj.isAbsence == 0)
                    {
                        if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                        {
                            oSheet.SetCellValue(curRow, curColumn, obj.ActualMark != 0 && obj.ActualMark != 10 ? String.Format("{0:0.0}", obj.ActualMark) : String.Format("{0:0}", obj.ActualMark));
                        }
                        else
                        {
                            oSheet.SetCellValue(curRow, curColumn, obj.ExamJudgeMark);
                        }
                    }
                }
                curColumn++;
                //Ma hoc sinh
                oSheet.SetCellValue(curRow, curColumn, obj.Note);
                curColumn++;

                //Ve khung cho dong
                IVTRange range = oSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Dotted;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                curRow++;
                curColumn = startColumn;
            }

            IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamInputMarkReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_EXAM_INPUT_MARK;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            //int subjectID = Utils.GetInt(dic, "SubjectID");
            SubjectCat sc = SubjectCatBusiness.Find(dic["SubjectID"]);
            string subjectName = sc != null ? sc.DisplayName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Report Danh sach diem thi theo lop
        public ProcessedReport GetExamMarkByClassReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamMarkByClassReport(IDictionary<string, object> dic)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            // Xét nếu có tất cả hay không 
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);



            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "NĂM HỌC: " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);

            IDictionary<string, object> tmpDic;
            //Lay danh sach diem thi cua nhom thi
            // Xét có lấy theo nhóm thi hay không
            List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
            if (examinationsID != 0 || examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["LastDigitSchoolID"] = partition;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
            }

            //Lay danh sach vi pham quy che thi cua nhom thi
            // Xet có lấy theo tất cả hay không
            List<ExamPupilViolateBO> listPupilViolate = new List<ExamPupilViolateBO>();
            if (examinationsID != 0 || examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listPupilViolate = this.ExamPupilViolateBusiness.GetListExamPupilViolate(tmpDic).OrderBy(o => o.OrderInSubject).ToList();
            }

            //Lay danh sach thi sinh cua nhom thi
            // Xet có lấy theo tất cả hay không
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();

            if (examinationsID != 0 && examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listExamPupil = ExamPupilBusiness.Search(tmpDic, academicYearID, partition).ToList();
            }

            //Danh sach ClassID cua cac thi sinh
            List<int> listClassID = listExamPupil.Select(o => o.ClassID).Distinct().ToList();

            //Danh sach lop cua cac thi sinh
            List<ClassProfile> listClass = ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && listClassID.Contains(o.ClassProfileID) && o.IsActive == true).ToList();

            //Lay danh sach mon thi cua nhom thi
            // Xet có lấy theo tất cả hay không
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList();
            }

            //Lay danh sach lop thi hoc cac mon
            tmpDic = new Dictionary<string, object>();
            //tmpDic["SchoolID"] = schoolID;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["Semester"] = exam.SemesterID;

            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            //Danh sach mon hoc cho truong
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            //So mon thi
            int subjectNum = listExamSubject.Count;

            //fill mon thi
            int subjectColumn = 5;
            int subjectRow = 9;

            for (int j = 0; j < listExamSubject.Count; j++)
            {
                ExamSubjectBO subject = listExamSubject[j];

                firstSheet.SetCellValue(subjectRow, subjectColumn, subject.SubjectName);
                firstSheet.GetRange(subjectRow, subjectColumn, subjectRow, subjectColumn).AutoFit();
                subjectColumn++;
            }

            //Fill cot ghi chu
            firstSheet.SetCellValue(subjectRow - 1, subjectColumn, "Ghi chú");
            firstSheet.SetColumnWidth(subjectColumn, 29.57);
            firstSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);

            //Fill tieu de, quoc hieu, ngay thang
            int titleStartCol;
            int titleEndCol;
            if (subjectColumn - 3 > 3)
            {
                titleStartCol = subjectColumn - 3;
                titleEndCol = subjectColumn;
            }
            else
            {
                titleStartCol = 4;
                titleEndCol = 7;
            }

            firstSheet.SetCellValue(2, titleStartCol, "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            firstSheet.SetCellValue(3, titleStartCol, "Độc lập - Tự do - Hạnh phúc");
            firstSheet.SetCellValue(4, titleStartCol, provinceAndDate);
            firstSheet.MergeRow(2, titleStartCol, titleEndCol);
            firstSheet.MergeRow(3, titleStartCol, titleEndCol);
            firstSheet.MergeRow(4, titleStartCol, titleEndCol);
            firstSheet.MergeRow(6, 1, subjectColumn);

            //Fill Cell Diem thi
            if (subjectNum > 0)
            {
                firstSheet.SetCellValue(subjectRow - 1, 5, "Điểm thi");
                firstSheet.MergeRow(subjectRow - 1, 5, subjectColumn - 1);
            }


            ////Fill du lieu
            for (int i = 0; i < listClass.Count; i++)
            {
                ClassProfile Class = listClass[i];

                //Danh sach thi sinh cua lop
                // Sẽ lấy được có bao nhiêu thí sinh trong lớp thuộc nhiều nhóm thi
                List<ExamPupilBO> listExamPupilOfClass = listExamPupil.Where(o => o.ClassID == Class.ClassProfileID)
                                                             .OrderBy(o => o.OrderInClass).ToList();
                // CẦN 1 LIST int theo


                int startRow = 10;
                int lastRow = startRow + listExamPupilOfClass.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 5 + subjectNum;
                int curColumn = startColumn;

                //Tao sheet cho lop
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "II" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = ReportUtils.StripVNSign(Class.DisplayName);
                curSheet.SetCellValue("A6", "KẾT QUẢ THI LỚP " + Class.DisplayName.ToUpper());

                for (int j = 0; j < listExamPupilOfClass.Count; j++)
                {
                    ExamPupilBO obj = listExamPupilOfClass[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //Ma so
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilCode);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;

                    //Diem thi cac mon
                    for (int k = 0; k < listExamSubject.Count; k++)
                    {
                        ExamSubjectBO subject = listExamSubject[k];

                        //Lay khai bao mon hoc cho truong
                        SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();

                        //Lay khai bao mon hoc cho lop
                        ClassSubject cs = listClassSubject.Where(o => o.ClassID == Class.ClassProfileID)
                                                        .Where(o => o.SubjectID == subject.SubjectID)
                                                        .FirstOrDefault();

                        if (cs != null && ss != null)
                        {
                            ExamInputMarkBO mark = listExamMark.Where(o => o.SubjectID == subject.SubjectID)
                                                                .Where(o => o.PupilID == obj.PupilID)
                                                                .FirstOrDefault();

                            if (mark != null)
                            {
                                if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, mark.ActualMark);
                                }
                                else
                                {
                                    if (ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                                    {
                                        curSheet.SetCellValue(curRow, curColumn, mark.ActualMark != 0 && mark.ActualMark != 10 ? String.Format("{0:0.0}", mark.ActualMark) : String.Format("{0:0}", mark.ActualMark));
                                    }
                                    else
                                    {
                                        curSheet.SetCellValue(curRow, curColumn, mark.ExamJudgeMark);
                                    }

                                }
                            }
                        }
                        curColumn++;
                    }

                    //Ghi chu
                    StringBuilder note = new StringBuilder();
                    List<ExamPupilViolateBO> listViolateOfThisPupil = listPupilViolate.Where(o => o.ExamPupilID == obj.ExamPupilID).ToList();
                    for (int k = 0; k < listViolateOfThisPupil.Count; k++)
                    {
                        ExamPupilViolateBO violate = listViolateOfThisPupil[k];
                        note.Append(violate.ContentViolated + " môn thi " + violate.SubjectName);
                        if (k < listViolateOfThisPupil.Count - 1)
                        {
                            note.Append(", ");
                        }
                    }
                    curSheet.SetCellValue(curRow, curColumn, note.ToString());
                    curSheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignLeft);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                //Ve border 
                if (listExamPupilOfClass.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                //Ve border cho tieu de
                IVTRange headerRange = curSheet.GetRange(8, startColumn, 9, lastColumn);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                headerRange.FillColor(System.Drawing.Color.LightGray);

                curSheet.Orientation = VTXPageOrientation.VTxlLandscape;
                curSheet.FitAllColumnsOnOnePage = true;

            }

            //Xoa sheet dau tien
            firstSheet.Delete();

            return oBook.ToStream();
        }

        public Stream CreateExamMarkByClassReportAll(IDictionary<string, object> dic)
        {
            char[] lstAlphabet = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");
            bool checkSumMark = Utils.GetBool(dic["checkSumMark"]);
            bool checkClassLevel = Utils.GetBool(dic["checkClassLevel"]);
            bool checkSchoolLevel = Utils.GetBool(dic["checkSchoolLevel"]);


            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);//GetRange("BB10", "BB100");
            IVTRange rang = firstSheet.GetRange("BB10", "BB100");
            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "NĂM HỌC: " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);

            IDictionary<string, object> tmpDic;
            //Lay danh sach diem thi cua nhom thi 
            // Xét có lấy theo nhóm thi hay không
            List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
            if (examinationsID != 0) //|| examGroupID != 0
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["LastDigitSchoolID"] = partition;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["EducationLevelID"] = educationLevelID;
                tmpDic["ClassID"] = classID;
               
                listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
            }
            //Lay danh sach vi pham quy che thi cua nhom thi
            // Xet có lấy theo tất cả hay không
            List<ExamPupilViolateBO> listPupilViolate = new List<ExamPupilViolateBO>();
            if (examinationsID != 0) //|| examGroupID != 0
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["EducationLevelID"] = educationLevelID;

                listPupilViolate = this.ExamPupilViolateBusiness.GetListExamPupilViolate(tmpDic).OrderBy(o => o.OrderInSubject).ToList();
            }

            //Lay danh sach thi sinh cua nhom thi
            // Xet có lấy theo tất cả hay không
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();

            if (examinationsID != 0) //&& examGroupID != 0
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["EducationLevelID"] = educationLevelID;

                listExamPupil = ExamPupilBusiness.Search(tmpDic, academicYearID, partition).ToList();
            }

            //Danh sach ClassID cua cac thi sinh
            List<int> listClassID = new List<int>();
            if (classID != 0)
                listClassID = listExamPupil.Where(x=>x.ClassID == classID).Select(o => o.ClassID).Distinct().ToList();
            else 
                listClassID = listExamPupil.Select(o => o.ClassID).Distinct().ToList();
            

            //Danh sach lop cua cac thi sinh
            List<ClassProfile> listClass = ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && listClassID.Contains(o.ClassProfileID) && o.IsActive == true).ToList();

            //Lay danh sach mon thi cua nhom thi
            // Xet có lấy theo tất cả hay không
            List<int> listSubjectId = new List<int>();
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0) //&& examGroupID != 0
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["ExaminationsID"] = examinationsID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList();
                //Loai cac mon thi trung nhau
                listSubjectId = listExamSubject.Select(s => s.SubjectID).Distinct().ToList();
            }

            //Lay danh sach lop thi hoc cac mon
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["Semester"] = exam.SemesterID;
            tmpDic["ClassID"] = classID;
            tmpDic["EducationLevelID"] = educationLevelID;
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            //Danh sach mon hoc cho truong
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["EducationLevelID"] = educationLevelID;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            // Lấy danh sách khi check bổ sung dữ liệu 
            List<ExamInputMarkBO> listExamMarkTemp = new List<ExamInputMarkBO>();
            listExamMarkTemp = (from em in listExamMark
                                join cs in listClassSubject on new { em.ClassID,em.SubjectID} equals new {cs.ClassID,cs.SubjectID}
                                join lst in listSchoolSubject on em.SubjectID equals lst.SubjectID
                                select em).ToList();

                                    
            var lstcheckLevel = listExamMarkTemp.GroupBy(s => new { s.EducationLevelID, s.ClassID, s.PupilID })
                                                    .Select(g => new
                                                    {
                                                        educationLevelID = g.Key.EducationLevelID,
                                                        classID = g.Key.ClassID,
                                                        pupilID = g.Key.PupilID,
                                                        levelInClass = g.Sum(x => x.ActualMark)
                                                    }).OrderByDescending(x => x.levelInClass).ToList();

            //So mon thi
            int subjectNum = listSubjectId.Count;

            //fill mon thi
            int subjectColumn = 5;
            int subjectRow = 9;
            int countCheck = 0;

            for (int j = 0; j < listSubjectId.Count; j++)
            {
                int subjectId = listSubjectId[j];
                ExamSubjectBO subject = listExamSubject.FirstOrDefault(s => s.SubjectID == subjectId);
                firstSheet.CopyPasteSameSize(rang, subjectRow + 1, subjectColumn);
                firstSheet.SetCellValue(subjectRow, subjectColumn, subject.SubjectName);
                firstSheet.GetRange(subjectRow, subjectColumn, subjectRow, subjectColumn).AutoFit();
                subjectColumn++;
            }

            //Fill cột ghi chú, ổng điểm, hạng
            if (checkSumMark)
            {
                firstSheet.SetCellValue(subjectRow - 1, subjectColumn, "Tổng điểm các môn thi");
                firstSheet.SetColumnWidth(subjectColumn, 14.14);
                firstSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);
                firstSheet.GetRange(subjectRow - 1, subjectColumn, subjectRow, subjectColumn).WrapText();
                subjectColumn++;
                countCheck++;
            }

            if (checkClassLevel)
            {
                firstSheet.SetCellValue(subjectRow - 1, subjectColumn, "Thứ hạng trong lớp");
                firstSheet.SetColumnWidth(subjectColumn, 14.14);
                firstSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);
                firstSheet.GetRange(subjectRow - 1, subjectColumn, subjectRow, subjectColumn).WrapText();
                subjectColumn++; 
                countCheck++;
            }

            if (checkSchoolLevel)
            {
                firstSheet.SetCellValue(subjectRow - 1, subjectColumn, "Thứ hạng trong khối");
                firstSheet.SetColumnWidth(subjectColumn, 14.14);
                firstSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);
                firstSheet.GetRange(subjectRow - 1, subjectColumn, subjectRow, subjectColumn).WrapText();
                subjectColumn++;
                countCheck++;
            }

            firstSheet.SetCellValue(subjectRow - 1, subjectColumn, "Ghi chú");
            firstSheet.SetColumnWidth(subjectColumn, 29.57);
            firstSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);

            //Fill tieu de, quoc hieu, ngay thang
            int titleStartCol;
            int titleEndCol;
            if (subjectColumn - 3 > 3)
            {
                titleStartCol = subjectColumn - 3;
                titleEndCol = subjectColumn;
            }
            else
            {
                titleStartCol = 4;
                titleEndCol = 7;
            }

            firstSheet.SetCellValue(2, titleStartCol, "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            firstSheet.SetCellValue(3, titleStartCol, "Độc lập - Tự do - Hạnh phúc");
            firstSheet.SetCellValue(4, titleStartCol, provinceAndDate);
            firstSheet.MergeRow(2, titleStartCol, titleEndCol);
            firstSheet.MergeRow(3, titleStartCol, titleEndCol);
            firstSheet.MergeRow(4, titleStartCol, titleEndCol);
            firstSheet.MergeRow(6, 1, subjectColumn);

            //Fill Cell Diem thi
            if (subjectNum > 0)
            {
                firstSheet.SetCellValue(subjectRow - 1, 5, "Điểm thi");

                int rowTemp = subjectColumn - 1;
                if (checkSumMark)
                    rowTemp = rowTemp - 1;
                if (checkClassLevel)
                    rowTemp = rowTemp - 1;
                if (checkSchoolLevel)
                    rowTemp = rowTemp - 1;

                firstSheet.MergeRow(subjectRow - 1, 5, rowTemp);
            }


            ////Fill du lieu
            for (int i = 0; i < listClass.Count; i++)
            {
                ClassProfile Class = listClass[i];
                var lstCheckLevelByEduction = lstcheckLevel.Where(x => x.educationLevelID == Class.EducationLevelID).ToList();
                var lstcheckLevelClass = lstCheckLevelByEduction.Where(x => x.classID == Class.ClassProfileID).ToList();
                //Danh sach thi sinh cua lop
                // Sẽ lấy được có bao nhiêu thí sinh trong lớp thuộc nhiều nhóm thi
                List<ExamPupilBO> listExamPupilOfClass = listExamPupil.Where(o => o.ClassID == Class.ClassProfileID)
                                                             .OrderBy(o => o.OrderInClass).ToList();
                // CẦN 1 LIST int theo
                List<int> lstPupilID = listExamPupilOfClass.Select(x => x.PupilID).Distinct().ToList();

                int startRow = 10;
                int lastRow = startRow + lstPupilID.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 5 + subjectNum + countCheck;
                int curColumn = startColumn;

                //Tao sheet cho lop
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "II" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = ReportUtils.StripVNSign(ReportUtils.RemoveSpecialCharacters(Class.DisplayName));
                curSheet.SetCellValue("A6", "KẾT QUẢ THI LỚP " + Class.DisplayName.ToUpper());
                ExamPupilBO obj = null;

                #region fill data
                for (int j = 0; j < lstPupilID.Count; j++)
                {
                    obj = new ExamPupilBO();
                    obj = listExamPupilOfClass.Where(x => x.PupilID == lstPupilID[j]).ToList().FirstOrDefault();

                    // Lấy thứ hạng theo lớp và khối
                    var lstcheckLevelPupil = lstcheckLevelClass.Where(x => x.pupilID == obj.PupilID).FirstOrDefault();
                    int levelOfPupilForClass = -2;
                    int levelOfPupilForEducation = -2;
                    if (lstcheckLevelPupil != null)
                    {
                        levelOfPupilForClass = lstcheckLevelClass.FindIndex(x => x.levelInClass == lstcheckLevelPupil.levelInClass);
                        levelOfPupilForEducation = lstCheckLevelByEduction.FindIndex(x => x.levelInClass == lstcheckLevelPupil.levelInClass);
                        if (levelOfPupilForClass == -1)
                            levelOfPupilForClass = 0;
                        if (levelOfPupilForEducation == -1)
                            levelOfPupilForEducation = 0;
                    }

                    List<string> lstExamineeNumber = listExamPupilOfClass.Where(x => x.PupilID == lstPupilID[j]).Select(x => x.ExamineeNumber).Distinct().ToList();
                    string ExaminationCode = "";
                    if (lstExamineeNumber != null && lstExamineeNumber.Count > 0)
                    {
                        ExaminationCode = string.Join(",", lstExamineeNumber);
                    }
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //Ma so
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilCode);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //SBD
                    //curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curSheet.SetCellValue(curRow, curColumn, ExaminationCode);
                    //curSheet.GetRange(curRow, curColumn).
                    curColumn++;

                    //Diem thi cac mon
                    for (int k = 0; k < listSubjectId.Count; k++)
                    {
                        //ExamSubjectBO subject = listSubjectId[k];
                        int subjectId = listSubjectId[k];

                        //Lay khai bao mon hoc cho truong
                        SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == subjectId).FirstOrDefault();

                        //Lay khai bao mon hoc cho lop
                        ClassSubject cs = listClassSubject.Where(o => o.ClassID == Class.ClassProfileID)
                                                        .Where(o => o.SubjectID == subjectId)
                                                        .FirstOrDefault();

                        if (cs != null && ss != null)
                        {
                            ExamInputMarkBO mark = listExamMark.Where(o => o.SubjectID == subjectId)
                                                                .Where(o => o.PupilID == obj.PupilID)
                                                                .FirstOrDefault();

                            if (mark != null)
                            {
                                if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, mark.ActualMark);
                                }
                                else
                                {
                                    if (ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                                    {                           
                                        //curSheet.SetCellValue(curRow, curColumn, mark.ActualMark != 0 && mark.ActualMark != 10 ? String.Format("{0:0.0}", mark.ActualMark) : String.Format("{0:0}", mark.ActualMark));
                                        curSheet.SetCellValue(curRow, curColumn, mark.ActualMark);
                                    }
                                    else
                                    {
                                        curSheet.SetCellValue(curRow, curColumn, mark.ExamJudgeMark);
                                    }
                                }
                            }
                        }
                        curColumn++;
                    }
                    // Tổng điểm
                    if (checkSumMark)
                    {
                        curSheet.SetCellValue(curRow, curColumn, "=SUM(" + lstAlphabet[4] + curRow + ":" + lstAlphabet[3 + subjectNum] + curRow + ")");
                        curColumn++;
                    }

                    // Hạng theo lớp
                    if (checkClassLevel)
                    {
                        curSheet.SetCellValue(curRow, curColumn, levelOfPupilForClass == -2 ? "" : (levelOfPupilForClass + 1) + "");
                        curColumn++;
                    }

                    // Hạng theo khối
                    if (checkSchoolLevel)
                    {
                        curSheet.SetCellValue(curRow, curColumn, levelOfPupilForEducation == -2 ? "" : (levelOfPupilForEducation + 1) + "");
                        curColumn++;
                    }
                    //Ghi chu
                    StringBuilder note = new StringBuilder();
                    List<ExamPupilViolateBO> listViolateOfThisPupil = listPupilViolate.Where(o => o.ExamPupilID == obj.ExamPupilID).ToList();
                    for (int k = 0; k < listViolateOfThisPupil.Count; k++)
                    {
                        ExamPupilViolateBO violate = listViolateOfThisPupil[k];
                        note.Append(violate.ContentViolated + " môn thi " + violate.SubjectName);
                        if (k < listViolateOfThisPupil.Count - 1)
                        {
                            note.Append(", ");
                        }
                    }
                    curSheet.SetCellValue(curRow, curColumn, note.ToString());
                    curSheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignLeft);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }
                #endregion


                //Ve border 
                if (listExamPupilOfClass.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                //Ve border cho tieu de
                IVTRange headerRange = curSheet.GetRange(8, startColumn, 9, lastColumn);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                headerRange.FillColor(System.Drawing.Color.LightGray);

                curSheet.Orientation = VTXPageOrientation.VTxlLandscape;
                curSheet.FitAllColumnsOnOnePage = true;

            }

            //Xoa sheet dau tien
            firstSheet.Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamMarkByClassReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup examGroup = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string examGroupCode = examGroup != null ? examGroup.ExamGroupCode : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(examGroupCode));
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Report Danh sach diem thi theo phong thi
        public ProcessedReport GetExamMarkByRoomReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_PHONG;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamMarkByRoomReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);
            IDictionary<string, object> tmpDic;
            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_PHONG;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "NĂM HỌC: " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);
            // firstSheet.SetCellValue("F4", provinceAndDate);

            //Lay danh sach diem thi cua nhom thi
            List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
            if (examinationsID != 0 || examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["LastDigitSchoolID"] = partition;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
            }

            //Lay danh sach vi pham quy che thi cua nhom thi
            List<ExamPupilViolateBO> listPupilViolate = new List<ExamPupilViolateBO>();
            if (examinationsID != 0 || examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listPupilViolate = this.ExamPupilViolateBusiness.GetListExamPupilViolate(tmpDic);
            }

            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();

            if (examinationsID != 0 && examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listExamPupil = ExamPupilBusiness.Search(tmpDic, academicYearID, partition).ToList();
            }

            //Danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                 .Where(o => o.ExamGroupID == examGroupID)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            if (examRoomID != 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }

            //Lay danh sach mon thi cua nhom thi
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList();
            }

            //Danh sach mon hoc cho truong
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            //So mon thi
            int subjectNum = listExamSubject.Count;

            //fill mon thi
            int subjectColumn = 5;
            int subjectRow = 9;

            for (int j = 0; j < listExamSubject.Count; j++)
            {
                ExamSubjectBO subject = listExamSubject[j];

                firstSheet.SetCellValue(subjectRow, subjectColumn, subject.SubjectName);
                firstSheet.GetRange(subjectRow, subjectColumn, subjectRow, subjectColumn).AutoFit();
                subjectColumn++;
            }

            //Fill cot ghi chu
            firstSheet.SetCellValue(subjectRow - 1, subjectColumn, "Ghi chú");
            firstSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);
            firstSheet.SetColumnWidth(subjectColumn, 29.57);

            //Fill tieu de, quoc hieu, ngay thang
            int titleStartCol;
            int titleEndCol;
            if (subjectColumn - 3 > 3)
            {
                titleStartCol = subjectColumn - 3;
                titleEndCol = subjectColumn;
            }
            else
            {
                titleStartCol = 4;
                titleEndCol = 7;
            }

            firstSheet.SetCellValue(2, titleStartCol, "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            firstSheet.SetCellValue(3, titleStartCol, "Độc lập - Tự do - Hạnh phúc");
            firstSheet.SetCellValue(4, titleStartCol, provinceAndDate);
            firstSheet.MergeRow(2, titleStartCol, titleEndCol);
            firstSheet.MergeRow(3, titleStartCol, titleEndCol);
            firstSheet.MergeRow(4, titleStartCol, titleEndCol);
            firstSheet.MergeRow(6, 1, subjectColumn);

            //Fill Cell Diem thi
            if (subjectNum > 0)
            {
                firstSheet.SetCellValue(subjectRow - 1, 5, "Điểm thi");
                firstSheet.MergeRow(subjectRow - 1, 5, subjectColumn - 1);
            }

            ////Fill du lieu
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom room = listExamRoom[i];

                //Danh sach thi sinh cua phong thi
                List<ExamPupilBO> listExamPupilOfRoom = listExamPupil.Where(o => o.ExamRoomID == room.ExamRoomID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 10;
                int lastRow = startRow + listExamPupilOfRoom.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 5 + subjectNum;
                int curColumn = startColumn;

                //Tao sheet cho lop
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "II" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = ReportUtils.StripVNSign(room.ExamRoomCode);
                curSheet.SetCellValue("A6", "KẾT QUẢ THI PHÒNG THI " + room.ExamRoomCode.ToUpper());

                for (int j = 0; j < listExamPupilOfRoom.Count; j++)
                {
                    ExamPupilBO obj = listExamPupilOfRoom[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //Ma so
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilCode);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;

                    //Diem thi cac mon
                    for (int k = 0; k < listExamSubject.Count; k++)
                    {
                        ExamSubjectBO subject = listExamSubject[k];

                        //Lay khai bao mon hoc cho truong
                        SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();
                        if (ss != null)
                        {
                            ExamInputMarkBO mark = listExamMark.Where(o => o.SubjectID == subject.SubjectID)
                                                                .Where(o => o.PupilID == obj.PupilID)
                                                                .FirstOrDefault();
                            if (mark != null)
                            {
                                if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, mark.ActualMark);
                                }
                                else
                                {
                                    if (ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                                    {
                                        curSheet.SetCellValue(curRow, curColumn, mark.ActualMark != 0 && mark.ActualMark != 10 ? String.Format("{0:0.0}", mark.ActualMark) : String.Format("{0:0}", mark.ActualMark));
                                    }
                                    else
                                    {
                                        curSheet.SetCellValue(curRow, curColumn, mark.ExamJudgeMark);
                                    }

                                }
                            }
                        }
                        curColumn++;

                    }

                    //Ghi chu
                    StringBuilder note = new StringBuilder();
                    List<ExamPupilViolateBO> listViolateOfThisPupil = listPupilViolate.Where(o => o.ExamPupilID == obj.ExamPupilID).ToList();
                    for (int k = 0; k < listViolateOfThisPupil.Count; k++)
                    {
                        ExamPupilViolateBO violate = listViolateOfThisPupil[k];
                        note.Append(violate.ContentViolated + " môn thi " + violate.SubjectName);
                        if (k < listViolateOfThisPupil.Count - 1)
                        {
                            note.Append(", ");
                        }
                    }
                    curSheet.SetCellValue(curRow, curColumn, note.ToString());

                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                //Ve border 
                if (listExamPupilOfRoom.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                //Ve border cho tieu de
                IVTRange headerRange = curSheet.GetRange(8, startColumn, 9, lastColumn);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                headerRange.FillColor(System.Drawing.Color.LightGray);

                curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                curSheet.FitAllColumnsOnOnePage = true;

            }

            //Xoa sheet dau tien
            firstSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamMarkByRoomReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_PHONG;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup examGroup = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string examGroupCode = examGroup != null ? examGroup.ExamGroupCode : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(examGroupCode));
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Report Thong ke diem thi theo lop
        public ProcessedReport GetExamMarkByClassStatisticReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_LOP;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamMarkByClassStatisticReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            int statisticLevelReportID = Utils.GetInt(dic["StatisticLevelReportID"]);
            bool femalePupil = Utils.GetBool(dic["FemalePupil"]);
            bool ethnicPupil = Utils.GetBool(dic["EthnicPupil"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            int appliedLevel = exam.AppliedLevel;

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_LOP;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = rp.TemplateName;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            //Danh sach mon hoc cho truong
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            #region Cap 2,3
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[0]);

                //Lấy ra sheet đầu tiên
                IVTWorksheet markSheet = oBook.GetSheet(1);

                //Lấy ra sheet template mon tinh diem
                IVTWorksheet judgeSheet = oBook.GetSheet(2);

                //Dien du lieu vao phan tieu de
                markSheet.SetCellValue("A2", supervisingDeptName);
                markSheet.SetCellValue("A3", schoolName);
                markSheet.SetCellValue("A4", examinationsName);
                markSheet.SetCellValue("A5", strAcademicYear);
                markSheet.SetCellValue("K4", provinceAndDate);

                judgeSheet.SetCellValue("A2", supervisingDeptName);
                judgeSheet.SetCellValue("A3", schoolName);
                judgeSheet.SetCellValue("A4", examinationsName);
                judgeSheet.SetCellValue("A5", strAcademicYear);
                judgeSheet.SetCellValue("E4", provinceAndDate);

                //Fill muc thong ke
                //Lấy danh sách mức thống kê
                List<StatisticLevelConfig> lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", statisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();

                int numberConfig = lstStatisticLevel.Count();
                int levelColumn = 5;
                int levelRow = 8;
                for (int i = 0; i < numberConfig; i++)
                {
                    markSheet.SetCellValue(levelRow, levelColumn, lstStatisticLevel[i].Title);
                    markSheet.MergeRow(levelRow, levelColumn, levelColumn + 1);
                    markSheet.SetCellValue(levelRow + 1, levelColumn, "SL");
                    markSheet.SetCellValue(levelRow + 1, levelColumn + 1, "%");
                    levelColumn = levelColumn + 2;
                }

                IVTRange headerRange = markSheet.GetRange(levelRow, 1, levelRow + 1, 4 + numberConfig * 2);
                headerRange.FillColor(System.Drawing.Color.LightGray);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

                //Lay danh sach mon thi cua ky thi
                List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
                    listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList()
                                                        .Select(o => new ExamSubjectBO
                                                        {
                                                            SubjectID = o.SubjectID,
                                                            SubjectName = o.SubjectName
                                                        }).Distinct().ToList();
                }
                if (subjectID != 0)
                {
                    listExamSubject = listExamSubject.Where(o => o.SubjectID == subjectID).ToList();
                }

                //Lay danh sach diem thi cua ky thi
                List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>();
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["LastDigitSchoolID"] = partition;
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["ExaminationsID"] = examinationsID;

                    listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
                }

                if (femalePupil)
                {
                    listExamMark = listExamMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamMark = listExamMark.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Lay danh sach thi sinh co thi trong ky thi
                List<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).ToList();
                if (femalePupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach ClassID cua cac thi sinh
                List<int> listClassID = listExamPupilOfExam.Select(o => o.ClassID).Distinct().ToList();

                //Danh sach lop cua cac thi sinh
                //List<ClassProfileTempBO> listClass = ClassProfileRepository.All.Where(o => listClassID.Contains(o.ClassProfileID)).ToList();
                List<ClassProfileTempBO> listClassBO = (from cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && o.IsActive == true)
                                                        join em in EmployeeRepository.All.Where(o => o.SchoolID == schoolID) on cp.HeadTeacherID equals em.EmployeeID into g
                                                        from x in g.DefaultIfEmpty()
                                                        where listClassID.Contains(cp.ClassProfileID)
                                                        orderby cp.OrderNumber.HasValue ? cp.OrderNumber : 0, cp.DisplayName
                                                        select new ClassProfileTempBO
                                                        {
                                                            EducationLevelID = cp.EducationLevelID,
                                                            ClassProfileID = cp.ClassProfileID,
                                                            OrderNumber = cp.OrderNumber,
                                                            DisplayName = cp.DisplayName,
                                                            EmployeeName = x.FullName != null ? x.FullName : string.Empty
                                                        }).ToList();

                //Lay si so cua cac lop
                IQueryable<ClassProfile> iqClass = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearID && listClassID.Contains(o.ClassProfileID));
                IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"Semester",exam.SemesterID},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1}
                };
                List<ClassInfoBO> listClassWithTotalPupil = PupilOfClassBusiness.GetClassAndTotalPupil(iqClass, dicCATP).ToList();

                //Danh sach khoi
                List<int> listEducationdLevelID = listClassBO.Select(o => o.EducationLevelID).ToList();
                List<EducationLevel> listEduLevel = EducationLevelBusiness.All.Where(o => listEducationdLevelID.Contains(o.EducationLevelID)).ToList();

                ////Fill du lieu
                for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                {
                    ExamSubjectBO subject = listExamSubject[iSubject];

                    //Lay loai mon
                    int? isCommentSubject = null;

                    //Lay khai bao mon hoc cho truong
                    SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();

                    if (ss != null)
                    {
                        isCommentSubject = ss.IsCommenting;
                    }

                    //Loc lai danh sach thi sinh co thi mon thi nay
                    List<ExamPupilBO> listExamPupilOfSubject = listExamPupilOfExam.Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .Where(o => o.ExamPupilAbsenceID == 0).ToList();

                    int startRow = 10;
                    int lastRow = startRow + listClassBO.Count + listEduLevel.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;

                    #region Mon tinh diem
                    if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        int lastColumn = 4 + numberConfig * 2;
                        //Tao sheet cho mon thi
                        IVTWorksheet curSheet = oBook.CopySheetToLast(markSheet, "II" + (lastRow).ToString());

                        //Fill tieu de
                        string additionString = String.Empty;
                        if (femalePupil && ethnicPupil)
                        {
                            additionString = " HỌC SINH NỮ DÂN TỘC";
                        }
                        else if (femalePupil)
                        {
                            additionString = " HỌC SINH NỮ";
                        }
                        else if (ethnicPupil)
                        {
                            additionString = " HỌC SINH DÂN TỘC";
                        }
                        curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                        curSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI MÔN " + subject.SubjectName.ToUpper() + additionString);

                        for (int iEdu = 0; iEdu < listEduLevel.Count; iEdu++)
                        {
                            EducationLevel edu = listEduLevel[iEdu];
                            List<ClassProfileTempBO> listClassOfEdu = listClassBO.Where(o => o.EducationLevelID == edu.EducationLevelID).ToList();

                            //Lop
                            curSheet.SetCellValue(curRow, curColumn, edu.Resolution);
                            curColumn++;
                            //Giao vien chu nhiem
                            curColumn++;

                            //Si so
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                            curColumn++;
                            string abc = "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")";
                            //So bai KT
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                            curColumn++;

                            for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                            {
                                curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                            }

                            IVTRange eduRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);
                            eduRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            eduRange.SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);

                            curRow++;
                            curColumn = startColumn;

                            for (int iClass = 0; iClass < listClassOfEdu.Count; iClass++)
                            {
                                ClassProfileTempBO Class = listClassOfEdu[iClass];
                                ClassInfoBO ClassInfo = listClassWithTotalPupil.Where(o => o.ClassID == Class.ClassProfileID).FirstOrDefault();
                                List<ExamPupilBO> listExamPupilOfClass = listExamPupilOfSubject.Where(o => o.ClassID == Class.ClassProfileID).ToList();
                                List<ExamInputMarkBO> listMarkOfClass = listExamMark.Where(o => o.ClassID == Class.ClassProfileID)
                                                                                   .Where(o => o.SubjectID == subject.SubjectID)
                                                                                   .ToList();


                                //Lop
                                curSheet.SetCellValue(curRow, curColumn, Class.DisplayName);
                                curColumn++;
                                //Giao vien chu nhiem
                                curSheet.SetCellValue(curRow, curColumn, Class.EmployeeName);
                                curColumn++;
                                //Si so
                                if (ClassInfo != null)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, ClassInfo.ActualNumOfPupil);
                                }
                                curColumn++;
                                //So bai kiem tra
                                curSheet.SetCellValue(curRow, curColumn, listExamPupilOfClass.Count());
                                curColumn++;

                                //Fill cac cot muc diem
                                //Diem thi cac mon
                                int SL = 0;
                                for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                                {
                                    StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                    if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                                        }
                                    }
                                    else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                                        }
                                    }
                                    else
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                                        }
                                    }

                                    curSheet.SetCellValue(curRow, curColumn, SL);//E
                                    curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                    curColumn += 2;

                                }

                                IVTRange classRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                                if (iClass < listClassOfEdu.Count - 1)
                                {
                                    classRange.SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                }
                                else
                                {
                                    classRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                                }

                                curRow++;
                                curColumn = startColumn;
                            }
                        }

                        IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                        //Neu la muc thong ke chuan, to vang cot cuoi
                        if (statisticLevelReportID == 1)
                        {
                            IVTRange lastColRange = curSheet.GetRange(levelRow, lastColumn - 1, lastRow, lastColumn);
                            lastColRange.FillColor(System.Drawing.Color.Yellow);
                        }

                        curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                        curSheet.FitAllColumnsOnOnePage = true;
                    }
                    #endregion
                    #region Mon nhan xet
                    else if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        int lastColumn = 8;

                        //Tao sheet cho mon thi
                        IVTWorksheet curSheet = oBook.CopySheetToLast(judgeSheet, "J" + (lastRow).ToString());

                        //Fill tieu de
                        string additionString = String.Empty;
                        if (femalePupil && ethnicPupil)
                        {
                            additionString = " HỌC SINH NỮ DÂN TỘC";
                        }
                        else if (femalePupil)
                        {
                            additionString = " HỌC SINH NỮ";
                        }
                        else if (ethnicPupil)
                        {
                            additionString = " HỌC SINH DÂN TỘC";
                        }
                        curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                        curSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI MÔN " + subject.SubjectName.ToUpper() + additionString);

                        for (int iEdu = 0; iEdu < listEduLevel.Count; iEdu++)
                        {
                            EducationLevel edu = listEduLevel[iEdu];
                            List<ClassProfileTempBO> listClassOfEdu = listClassBO.Where(o => o.EducationLevelID == edu.EducationLevelID).ToList();

                            //Lop
                            curSheet.SetCellValue(curRow, curColumn, edu.Resolution);
                            curColumn++;
                            //Giao vien chu nhiem
                            curColumn++;

                            //Si so
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                            curColumn++;

                            //So bai KT
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                            curColumn++;

                            for (int iSLevel = 0; iSLevel < 2; iSLevel++)
                            {
                                curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                            }

                            IVTRange eduRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);
                            eduRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            eduRange.SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);

                            curRow++;
                            curColumn = startColumn;

                            for (int iClass = 0; iClass < listClassOfEdu.Count; iClass++)
                            {
                                ClassProfileTempBO Class = listClassOfEdu[iClass];
                                ClassInfoBO ClassInfo = listClassWithTotalPupil.Where(o => o.ClassID == Class.ClassProfileID).FirstOrDefault();
                                List<ExamPupilBO> listExamPupilOfClass = listExamPupilOfSubject.Where(o => o.ClassID == Class.ClassProfileID).ToList();
                                List<ExamInputMarkBO> listMarkOfClass = listExamMark.Where(o => o.ClassID == Class.ClassProfileID)
                                                                                   .Where(o => o.SubjectID == subject.SubjectID)
                                                                                   .ToList();


                                //Lop
                                curSheet.SetCellValue(curRow, curColumn, Class.DisplayName);
                                curColumn++;
                                //Giao vien chu nhiem
                                curSheet.SetCellValue(curRow, curColumn, Class.EmployeeName);
                                curColumn++;
                                //Si so
                                if (ClassInfo != null)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, ClassInfo.ActualNumOfPupil);
                                }
                                curColumn++;
                                //So bai kiem tra
                                curSheet.SetCellValue(curRow, curColumn, listExamPupilOfClass.Count());
                                curColumn++;

                                //Fill cac cot muc diem
                                //Diem thi cac mon
                                int HSD;
                                int HSCD;
                                //Fill cac cot muc diem
                                // Lấy số lượng diem thi Đạt
                                HSD = listMarkOfClass.Where(o => o.ExamJudgeMark != null && o.ExamJudgeMark.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                // Lấy số lượng học sinh chưa Đạt
                                HSCD = listMarkOfClass.Where(o => o.ExamJudgeMark != null && o.ExamJudgeMark.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                                curSheet.SetCellValue(curRow, curColumn, HSD);
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                                curSheet.SetCellValue(curRow, curColumn, HSCD);
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                                IVTRange classRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                                if (iClass < listClassOfEdu.Count - 1)
                                {
                                    classRange.SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                                }
                                else
                                {
                                    classRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                                }

                                curRow++;
                                curColumn = startColumn;
                            }
                        }

                        IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                        curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                        curSheet.FitAllColumnsOnOnePage = true;
                    }
                    #endregion
                }

                //Xoa sheet dau tien
                markSheet.Delete();
                judgeSheet.Delete();

                return oBook.ToStream();
            }
            #endregion
            #region Cap 1
            else
            {
                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[1]);

                //Lấy ra sheet đầu tiên
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Dien du lieu vao phan tieu de
                firstSheet.SetCellValue("A2", supervisingDeptName);
                firstSheet.SetCellValue("A3", schoolName);
                firstSheet.SetCellValue("A4", examinationsName);
                firstSheet.SetCellValue("A5", strAcademicYear);
                firstSheet.SetCellValue("H4", provinceAndDate);

                //Fill muc thong ke
                //Lấy danh sách mức thống kê
                List<StatisticLevelConfig> lstStatisticLevel = getStatisticLevelForPrimary();
                int numberConfig = lstStatisticLevel.Count();

                //Lay danh sach mon thi cua ky thi
                List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
                    listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList()
                                                        .Select(o => new ExamSubjectBO
                                                        {
                                                            SubjectID = o.SubjectID,
                                                            SubjectName = o.SubjectName
                                                        }).Distinct().ToList();
                }
                if (subjectID != 0)
                {
                    listExamSubject = listExamSubject.Where(o => o.SubjectID == subjectID).ToList();
                }

                List<int> listExamSubjectID = listExamSubject.Select(p => p.SubjectID).ToList();
                //Lay danh sach thi sinh co thi trong ky thi
                List<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).ToList();
                if (femalePupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach ClassID cua cac thi sinh
                List<int> listClassID = listExamPupilOfExam.Select(o => o.ClassID).Distinct().ToList();

                //Lay danh sach mon hoc cua cac lop (chi lay mon tinh diem)
                listExamSubject = (from l in SchoolSubjectBusiness.GetListSubjectMarkByListSubjectID(listExamSubjectID)
                                   select new ExamSubjectBO
                                   {
                                       SubjectName = l.SubjectName,
                                       SubjectID = l.SubjectCatID
                                   }).Distinct().ToList();

                if (listExamSubject == null || listExamSubject.Count == 0)
                {
                    throw new BusinessException("ExamResult_Report_Not_Success_Judge");
                }

                //Lay danh sach diem thi cua ky thi
                List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>();
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["LastDigitSchoolID"] = partition;
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["ExaminationsID"] = examinationsID;

                    listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
                }

                if (femalePupil)
                {
                    listExamMark = listExamMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamMark = listExamMark.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach lop cua cac thi sinh
                //List<ClassProfileTempBO> listClass = ClassProfileRepository.All.Where(o => listClassID.Contains(o.ClassProfileID)).ToList();
                List<ClassProfileTempBO> listClassBO = (from cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && o.IsActive == true)
                                                        join em in EmployeeRepository.All on cp.HeadTeacherID equals em.EmployeeID into g
                                                        from x in g.DefaultIfEmpty()
                                                        where listClassID.Contains(cp.ClassProfileID)
                                                        orderby cp.OrderNumber.HasValue ? cp.OrderNumber : 0, cp.DisplayName
                                                        select new ClassProfileTempBO
                                                        {
                                                            EducationLevelID = cp.EducationLevelID,
                                                            ClassProfileID = cp.ClassProfileID,
                                                            OrderNumber = cp.OrderNumber,
                                                            DisplayName = cp.DisplayName,
                                                            EmployeeName = x.FullName != null ? x.FullName : string.Empty
                                                        }).ToList();

                //Lay si so cua cac lop
                IQueryable<ClassProfile> iqClass = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearID && listClassID.Contains(o.ClassProfileID));
                IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"Semester",exam.SemesterID},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1}
                };
                List<ClassInfoBO> listClassWithTotalPupil = PupilOfClassBusiness.GetClassAndTotalPupil(iqClass, dicCATP).ToList();

                //Danh sach khoi
                List<int> listEducationdLevelID = listClassBO.Select(o => o.EducationLevelID).ToList();
                List<EducationLevel> listEduLevel = EducationLevelBusiness.All.Where(o => listEducationdLevelID.Contains(o.EducationLevelID)).ToList();

                ////Fill du lieu
                for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                {
                    ExamSubjectBO subject = listExamSubject[iSubject];

                    //Loc lai danh sach thi sinh co thi mon thi nay
                    List<ExamPupilBO> listExamPupilOfSubject = listExamPupilOfExam.Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .Where(o => o.ExamPupilAbsenceID == 0).ToList();

                    int startRow = 10;
                    int lastRow = startRow + listClassBO.Count + listEduLevel.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;


                    int lastColumn = 4 + numberConfig * 2;
                    //Tao sheet cho mon thi
                    IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "R" + (lastRow).ToString());

                    //Fill tieu de
                    string additionString = String.Empty;
                    if (femalePupil && ethnicPupil)
                    {
                        additionString = " HỌC SINH NỮ DÂN TỘC";
                    }
                    else if (femalePupil)
                    {
                        additionString = " HỌC SINH NỮ";
                    }
                    else if (ethnicPupil)
                    {
                        additionString = " HỌC SINH DÂN TỘC";
                    }
                    curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                    curSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI MÔN " + subject.SubjectName.ToUpper() + additionString);

                    for (int iEdu = 0; iEdu < listEduLevel.Count; iEdu++)
                    {
                        EducationLevel edu = listEduLevel[iEdu];
                        List<ClassProfileTempBO> listClassOfEdu = listClassBO.Where(o => o.EducationLevelID == edu.EducationLevelID).ToList();

                        //Lop
                        curSheet.SetCellValue(curRow, curColumn, edu.Resolution);
                        curColumn++;
                        //Giao vien chu nhiem
                        curColumn++;

                        //Si so
                        curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                        curColumn++;

                        //So bai KT
                        curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                        curColumn++;

                        for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                        {
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow + listClassOfEdu.Count).ToString() + ")");
                            curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                            curColumn += 2;

                        }

                        IVTRange eduRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);
                        eduRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                        eduRange.SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);

                        curRow++;
                        curColumn = startColumn;

                        for (int iClass = 0; iClass < listClassOfEdu.Count; iClass++)
                        {
                            ClassProfileTempBO Class = listClassOfEdu[iClass];
                            ClassInfoBO ClassInfo = listClassWithTotalPupil.Where(o => o.ClassID == Class.ClassProfileID).FirstOrDefault();
                            List<ExamPupilBO> listExamPupilOfClass = listExamPupilOfSubject.Where(o => o.ClassID == Class.ClassProfileID).ToList();
                            List<ExamInputMarkBO> listMarkOfClass = listExamMark.Where(o => o.ClassID == Class.ClassProfileID)
                                                                                .Where(o => o.SubjectID == subject.SubjectID)
                                                                                .ToList();


                            //Lop
                            curSheet.SetCellValue(curRow, curColumn, Class.DisplayName);
                            curColumn++;
                            //Giao vien chu nhiem
                            curSheet.SetCellValue(curRow, curColumn, Class.EmployeeName);
                            curColumn++;
                            //Si so
                            if (ClassInfo != null)
                            {
                                curSheet.SetCellValue(curRow, curColumn, ClassInfo.ActualNumOfPupil);
                            }
                            curColumn++;
                            //So bai kiem tra
                            curSheet.SetCellValue(curRow, curColumn, listExamPupilOfClass.Count());
                            curColumn++;

                            //Fill cac cot muc diem
                            //Diem thi cac mon
                            int SL = 0;
                            for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                    else
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                    else
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                }

                                curSheet.SetCellValue(curRow, curColumn, SL);//E
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                            }

                            IVTRange classRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                            if (iClass < listClassOfEdu.Count - 1)
                            {
                                classRange.SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                            }
                            else
                            {
                                classRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            }

                            curRow++;
                            curColumn = startColumn;
                        }
                    }

                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                    IVTRange lastColRange = curSheet.GetRange(startRow, lastColumn - 1, lastRow, lastColumn);
                    lastColRange.FillColor(System.Drawing.Color.Yellow);

                    curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                    curSheet.FitAllColumnsOnOnePage = true;


                }

                //Xoa sheet dau tien
                firstSheet.Delete();

                return oBook.ToStream();
            }
            #endregion

        }

        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamMarkByClassStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_LOP;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Report Thong ke diem thi theo phong thi
        public ProcessedReport GetExamMarkByRoomStatisticReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_PHONG;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamMarkByRoomStatisticReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            int statisticLevelReportID = Utils.GetInt(dic["StatisticLevelReportID"]);
            bool femalePupil = Utils.GetBool(dic["FemalePupil"]);
            bool ethnicPupil = Utils.GetBool(dic["EthnicPupil"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            int appliedLevel = exam.AppliedLevel;

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_PHONG;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = rp.TemplateName;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            #region Cap 2,3
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[0]);

                //Lấy ra sheet template mon tinh diem
                IVTWorksheet markSheet = oBook.GetSheet(1);

                //Lay ra sheet template mon nhan xet
                IVTWorksheet judgeSheet = oBook.GetSheet(2);

                //Dien du lieu vao phan tieu de
                markSheet.SetCellValue("A2", supervisingDeptName);
                markSheet.SetCellValue("A3", schoolName);
                markSheet.SetCellValue("A4", examinationsName);
                markSheet.SetCellValue("A5", strAcademicYear);
                markSheet.SetCellValue("J4", provinceAndDate);
                string additionString = String.Empty;
                if (femalePupil && ethnicPupil)
                {
                    additionString = " HỌC SINH NỮ DÂN TỘC";
                }
                else if (femalePupil)
                {
                    additionString = " HỌC SINH NỮ";
                }
                else if (ethnicPupil)
                {
                    additionString = " HỌC SINH DÂN TỘC";
                }
                markSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI THEO PHÒNG THI" + additionString);


                judgeSheet.SetCellValue("A2", supervisingDeptName);
                judgeSheet.SetCellValue("A3", schoolName);
                judgeSheet.SetCellValue("A4", examinationsName);
                judgeSheet.SetCellValue("A5", strAcademicYear);
                judgeSheet.SetCellValue("F4", provinceAndDate);

                //Fill muc thong ke
                //Lấy danh sách mức thống kê
                List<StatisticLevelConfig> lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", statisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();

                int numberConfig = lstStatisticLevel.Count();
                int levelColumn = 4;
                int levelRow = 9;
                for (int i = 0; i < numberConfig; i++)
                {
                    markSheet.SetCellValue(levelRow, levelColumn, lstStatisticLevel[i].Title);
                    markSheet.MergeRow(levelRow, levelColumn, levelColumn + 1);
                    markSheet.SetCellValue(levelRow + 1, levelColumn, "SL");
                    markSheet.SetCellValue(levelRow + 1, levelColumn + 1, "%");
                    levelColumn = levelColumn + 2;
                }

                IVTRange headerRange = markSheet.GetRange(levelRow, 1, levelRow + 1, 3 + numberConfig * 2);
                headerRange.FillColor(System.Drawing.Color.LightGray);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

                IDictionary<string, object> tmpDic;
                //Lay danh sach mon thi cua ky thi
                List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
                    listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList()
                                                        .Select(o => new ExamSubjectBO
                                                        {
                                                            SubjectID = o.SubjectID,
                                                            SubjectName = o.SubjectName
                                                        }).Distinct().ToList();
                }
                if (subjectID != 0)
                {
                    listExamSubject = listExamSubject.Where(o => o.SubjectID == subjectID).ToList();
                }

                //Lay danh sach diem thi cua ky thi
                List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>();
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["LastDigitSchoolID"] = partition;
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["ExaminationsID"] = examinationsID;

                    listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
                }

                if (femalePupil)
                {
                    listExamMark = listExamMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamMark = listExamMark.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Lay danh sach thi sinh co thi trong ky thi
                List<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).ToList();
                if (femalePupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach phong thi cua ky thi
                List<ExamRoom> listExamRoom = new List<ExamRoom>(); ;
                if (examinationsID != 0)
                {
                    listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                     .OrderBy(o => o.ExamGroupID)
                                                     .ThenBy(o => o.ExamRoomCode)
                                                     .ToList();
                }

                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["AppliedLevel"] = exam.AppliedLevel;
                List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();
                ////Fill du lieu
                for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                {
                    ExamSubjectBO subject = listExamSubject[iSubject];
                    //Lay loai mon
                    SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();

                    int? isCommentSubject = null;
                    if (ss != null)
                    {
                        isCommentSubject = ss.IsCommenting;
                    }

                    //Loc lai danh sach thi sinh co thi mon thi nay
                    List<ExamPupilBO> listExamPupilOfSubject = listExamPupilOfExam.Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .Where(o => o.ExamPupilAbsenceID == 0).ToList();

                    int startRow = 11;
                    int lastRow = startRow + listExamRoom.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;

                    #region Mon tinh diem
                    if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        int lastColumn = 3 + numberConfig * 2;
                        //Tao sheet cho mon thi
                        IVTWorksheet curSheet = oBook.CopySheetToLast(markSheet, "II" + (lastRow).ToString());

                        //Fill tieu de
                        curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                        curSheet.SetCellValue("A7", "MÔN THI " + subject.SubjectName.ToUpper());


                        for (int iRoom = 0; iRoom < listExamRoom.Count; iRoom++)
                        {
                            ExamRoom room = listExamRoom[iRoom];

                            List<ExamPupilBO> listExamPupilOfRoom = listExamPupilOfSubject.Where(o => o.ExamRoomID == room.ExamRoomID).ToList();
                            List<ExamInputMarkBO> listMarkOfRoom = listExamMark.Where(o => o.ExamRoomID == room.ExamRoomID)
                                                                                .Where(o => o.SubjectID == subject.SubjectID)
                                                                                .ToList();

                            //STT
                            curSheet.SetCellValue(curRow, curColumn, iRoom + 1);
                            curColumn++;

                            //Phong thi
                            curSheet.SetCellValue(curRow, curColumn, room.ExamRoomCode);
                            curColumn++;

                            //So bai kiem tra
                            curSheet.SetCellValue(curRow, curColumn, listExamPupilOfRoom.Count());
                            curColumn++;

                            //Fill cac cot muc diem
                            //Diem thi cac mon
                            int SL = 0;
                            for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                    else
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                    else
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                }

                                curSheet.SetCellValue(curRow, curColumn, SL);//E
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(C" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "C" + curRow + "*100,2),0)");
                                curColumn += 2;

                            }

                            //Ve khung cho dong
                            IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                            VTBorderStyle style;
                            VTBorderWeight weight;
                            if ((curRow - startRow + 1) % 5 != 0)
                            {
                                style = VTBorderStyle.Dotted;
                                weight = VTBorderWeight.Thin;
                            }
                            else
                            {
                                style = VTBorderStyle.Solid;
                                weight = VTBorderWeight.Medium;
                            }
                            range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);
                            curRow++;
                            curColumn = startColumn;
                        }


                        IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                        //Neu la muc thong ke chuan, to vang cot cuoi
                        if (statisticLevelReportID == 1)
                        {
                            IVTRange lastColRange = curSheet.GetRange(levelRow, lastColumn - 1, lastRow, lastColumn);
                            lastColRange.FillColor(System.Drawing.Color.Yellow);
                        }

                        curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                        curSheet.FitAllColumnsOnOnePage = true;
                    }
                    #endregion
                    #region Mon nhan xet
                    else if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        int lastColumn = 7;
                        //Tao sheet cho mon thi
                        IVTWorksheet curSheet = oBook.CopySheetToLast(judgeSheet, "R" + (lastRow).ToString());

                        //Fill tieu de
                        curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                        curSheet.SetCellValue("A7", "MÔN THI " + subject.SubjectName.ToUpper());


                        for (int iRoom = 0; iRoom < listExamRoom.Count; iRoom++)
                        {
                            ExamRoom room = listExamRoom[iRoom];

                            List<ExamPupilBO> listExamPupilOfRoom = listExamPupilOfSubject.Where(o => o.ExamRoomID == room.ExamRoomID).ToList();
                            List<ExamInputMarkBO> listMarkOfRoom = listExamMark.Where(o => o.ExamRoomID == room.ExamRoomID)
                                                                                .Where(o => o.SubjectID == subject.SubjectID)
                                                                                .ToList();

                            //STT
                            curSheet.SetCellValue(curRow, curColumn, iRoom + 1);
                            curColumn++;

                            //Phong thi
                            curSheet.SetCellValue(curRow, curColumn, room.ExamRoomCode);
                            curColumn++;

                            //So bai kiem tra
                            curSheet.SetCellValue(curRow, curColumn, listExamPupilOfRoom.Count());
                            curColumn++;

                            int HSD;
                            int HSCD;
                            //Fill cac cot muc diem
                            // Lấy số lượng diem thi Đạt
                            HSD = listMarkOfRoom.Where(o => o.ExamJudgeMark != null && o.ExamJudgeMark.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            // Lấy số lượng học sinh chưa Đạt
                            HSCD = listMarkOfRoom.Where(o => o.ExamJudgeMark != null && o.ExamJudgeMark.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                            curSheet.SetCellValue(curRow, curColumn, HSD);//E
                            curSheet.SetCellValue(curRow, curColumn + 1, "=IF(C" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "C" + curRow + "*100,2),0)");
                            curColumn += 2;

                            curSheet.SetCellValue(curRow, curColumn, HSCD);//E
                            curSheet.SetCellValue(curRow, curColumn + 1, "=IF(C" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "C" + curRow + "*100,2),0)");
                            curColumn += 2;

                            //Ve khung cho dong
                            IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                            VTBorderStyle style;
                            VTBorderWeight weight;
                            if ((curRow - startRow + 1) % 5 != 0)
                            {
                                style = VTBorderStyle.Dotted;
                                weight = VTBorderWeight.Thin;
                            }
                            else
                            {
                                style = VTBorderStyle.Solid;
                                weight = VTBorderWeight.Medium;
                            }
                            range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);
                            curRow++;
                            curColumn = startColumn;
                        }


                        IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                        curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                        curSheet.FitAllColumnsOnOnePage = true;
                    }
                    #endregion
                }
                //Xoa sheet dau tien
                markSheet.Delete();
                judgeSheet.Delete();

                return oBook.ToStream();
            }
            #endregion
            #region Cap 1
            else
            {
                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[1]);

                //Lấy ra sheet template mon tinh diem
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Dien du lieu vao phan tieu de
                firstSheet.SetCellValue("A2", supervisingDeptName);
                firstSheet.SetCellValue("A3", schoolName);
                firstSheet.SetCellValue("A4", examinationsName);
                firstSheet.SetCellValue("A5", strAcademicYear);
                firstSheet.SetCellValue("H4", provinceAndDate);

                //Lấy danh sách mức thống kê
                List<StatisticLevelConfig> lstStatisticLevel = getStatisticLevelForPrimary();

                int numberConfig = lstStatisticLevel.Count();


                IDictionary<string, object> tmpDic;
                //Lay danh sach mon thi cua ky thi
                List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
                    listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList()
                                                        .Select(o => new ExamSubjectBO
                                                        {
                                                            SubjectID = o.SubjectID,
                                                            SubjectName = o.SubjectName
                                                        }).Distinct().ToList();
                }
                if (subjectID != 0)
                {
                    listExamSubject = listExamSubject.Where(o => o.SubjectID == subjectID).ToList();
                }
                //Lay danh sach thi sinh co thi trong ky thi
                List<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).ToList();
                if (femalePupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach ClassID cua cac thi sinh
                List<int> listClassID = listExamPupilOfExam.Select(o => o.ClassID).Distinct().ToList();
                List<int> listExamSubjectID = listExamSubject.Select(p => p.SubjectID).ToList();
                //Lay danh sach mon hoc cua cac lop (chi lay mon tinh diem)
                listExamSubject = (from l in SchoolSubjectBusiness.GetListSubjectMarkByListSubjectID(listExamSubjectID)
                                   select new ExamSubjectBO
                                   {
                                       SubjectName = l.SubjectName,
                                       SubjectID = l.SubjectCatID
                                   }).Distinct().ToList();

                if (listExamSubject == null || listExamSubject.Count == 0)
                {
                    throw new BusinessException("ExamResult_Report_Not_Success_Judge");
                }

                //Lay danh sach diem thi cua ky thi
                List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>();
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["LastDigitSchoolID"] = partition;
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["ExaminationsID"] = examinationsID;

                    listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
                }

                if (femalePupil)
                {
                    listExamMark = listExamMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamMark = listExamMark.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach phong thi cua ky thi
                List<ExamRoom> listExamRoom = new List<ExamRoom>(); ;
                if (examinationsID != 0)
                {
                    listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                     .OrderBy(o => o.ExamGroupID)
                                                     .ThenBy(o => o.ExamRoomCode)
                                                     .ToList();
                }

                ////Fill du lieu
                for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                {
                    ExamSubjectBO subject = listExamSubject[iSubject];

                    //Loc lai danh sach thi sinh co thi mon thi nay
                    List<ExamPupilBO> listExamPupilOfSubject = listExamPupilOfExam.Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .Where(o => o.ExamPupilAbsenceID == 0).ToList();

                    int startRow = 11;
                    int lastRow = startRow + listExamRoom.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;
                    int lastColumn = 3 + numberConfig * 2;

                    //Tao sheet cho mon thi
                    IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "R" + (lastRow).ToString());

                    //Fill tieu de
                    curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                    curSheet.SetCellValue("A7", "MÔN THI " + subject.SubjectName.ToUpper());


                    for (int iRoom = 0; iRoom < listExamRoom.Count; iRoom++)
                    {
                        ExamRoom room = listExamRoom[iRoom];

                        List<ExamPupilBO> listExamPupilOfRoom = listExamPupilOfSubject.Where(o => o.ExamRoomID == room.ExamRoomID).ToList();
                        List<ExamInputMarkBO> listMarkOfRoom = listExamMark.Where(o => o.ExamRoomID == room.ExamRoomID)
                                                                            .Where(o => o.SubjectID == subject.SubjectID)
                                                                            .ToList();

                        //STT
                        curSheet.SetCellValue(curRow, curColumn, iRoom + 1);
                        curColumn++;

                        //Phong thi
                        curSheet.SetCellValue(curRow, curColumn, room.ExamRoomCode);
                        curColumn++;

                        //So bai kiem tra
                        curSheet.SetCellValue(curRow, curColumn, listExamPupilOfRoom.Count());
                        curColumn++;

                        //Fill cac cot muc diem
                        //Diem thi cac mon
                        int SL = 0;
                        for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                }
                                else
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                }
                                else
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = listMarkOfRoom.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                                }
                            }

                            curSheet.SetCellValue(curRow, curColumn, SL);//E
                            curSheet.SetCellValue(curRow, curColumn + 1, "=IF(C" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "C" + curRow + "*100,2),0)");
                            curColumn += 2;

                        }

                        //Ve khung cho dong
                        IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                        VTBorderStyle style;
                        VTBorderWeight weight;
                        if ((curRow - startRow + 1) % 5 != 0)
                        {
                            style = VTBorderStyle.Dotted;
                            weight = VTBorderWeight.Thin;
                        }
                        else
                        {
                            style = VTBorderStyle.Solid;
                            weight = VTBorderWeight.Medium;
                        }
                        range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);
                        curRow++;
                        curColumn = startColumn;
                    }


                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                    IVTRange lastColRange = curSheet.GetRange(startRow, lastColumn - 1, lastRow, lastColumn);
                    lastColRange.FillColor(System.Drawing.Color.Yellow);


                    curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                    curSheet.FitAllColumnsOnOnePage = true;
                }
                //Xoa sheet dau tien
                firstSheet.Delete();

                return oBook.ToStream();
            }
            #endregion
        }

        public ProcessedReport InsertExamMarkByRoomStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_PHONG;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region InputMark

        // Load danh sach nhom thi giam thi duoc phan quyen
        public List<ExamGroup> GetGroupAssignment(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long teacherID = Utils.GetLong(search, "TeacherID");

            List<ExamGroup> query = (from eg in ExamGroupRepository.All
                                     join esa in ExamInputMarkAssignedRepository.All on eg.ExamGroupID equals esa.ExamGroupID
                                     where eg.ExaminationsID == examinationsID && esa.TeacherID == teacherID
                                     select eg).Distinct().OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();
            return query;
        }

        // Load danh sach mon thi giam thi duoc phan quyen
        public List<ExamSubjectBO> GetSubjectAssignment(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long teacherID = Utils.GetLong(search, "TeacherID");

            List<ExamSubjectBO> query = (from esa in ExamInputMarkAssignedRepository.All
                                         join sc in SubjectCatRepository.All on esa.SubjectID equals sc.SubjectCatID
                                         where esa.ExaminationsID == examinationsID && esa.ExamGroupID == examGroupID && esa.TeacherID == teacherID
                                         && sc.IsActive == true
                                         select new ExamSubjectBO
                                         {
                                             SubjectID = sc.SubjectCatID,
                                             SubjectName = sc.SubjectName,
                                             OrderInSubject = sc.OrderInSubject

                                         }).Distinct().OrderBy(o => o.OrderInSubject).ToList();
            return query;
        }

        public List<ExamInputMarkBO> GetListInputMarkCandenceBag(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetInt(search, "ExaminationsID");
            long examGroupID = Utils.GetInt(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examCandenceBagID = Utils.GetLong(search, "ExamCandenceBagID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            List<long> lstExamRoomID = null;
            if (search.ContainsKey("ListExamRoomID"))
            {
                lstExamRoomID = (List<long>)search["ListExamRoomID"];
            }

            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
            int isCommenting = 0;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue && schoolSubject.IsCommenting.Value == 1)
            {
                isCommenting = 1;
            }

            var iq = (from db in ExamDetachableBagRepository.All.Where(o => o.LastDigitSchoolID == lastDigitSchoolID)
                      join ep in ExamPupilRepository.All.Where(o => o.SchoolID == schoolID && o.LastDigitSchoolID == lastDigitSchoolID) on db.ExamPupilID equals ep.ExamPupilID
                      join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                      join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                      join ec in ExamCandenceBagRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID) on db.ExamCandenceBagID equals ec.ExamCandenceBagID
                      where poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID && db.ExaminationsID == examinationsID
                      && db.ExamGroupID == examGroupID && db.SubjectID == subjectID && pp.IsActive == true
                      && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                      select new ExamInputMarkBO
                      {
                          ExamDetachableBagID = db.ExamDetachableBagID,
                          ExamDetachableBagCode = db.ExamDetachableBagCode,
                          ExamPupilID = ep.ExamPupilID,
                          PupilID = ep.PupilID,
                          ClassID = poc.ClassID,
                          ExamCandenceBagID = db.ExamCandenceBagID,
                          ExamCandenceBagCode = ec.ExamCandenceBagCode,
                          ExamRoomID = ep.ExamRoomID

                      });

            if (lstExamRoomID != null)
            {
                iq = iq.Where(o => o.ExamRoomID.HasValue ? lstExamRoomID.Contains(o.ExamRoomID.Value) : false);
            }
            if (examCandenceBagID != 0)
            {
                iq = iq.Where(o => o.ExamCandenceBagID == examCandenceBagID);
            }

            List<ExamInputMarkBO> query = iq.OrderBy(o => o.ExamDetachableBagCode).ToList();
            // Danh dau thi sinh vang thi
            List<long> listAbsence = ExamPupilAbsenceBusiness.Search(search).Select(o => o.ExamPupilID).ToList();

            // Tinh % diem cho thi sinh vi pham quy che thi
            IQueryable<ExamPupilViolateMarkBO> listViolate = (from ep in ExamPupilViolateRepository.All
                                                              join ev in ExamViolationTypeRepository.All on ep.ExamViolationTypeID equals ev.ExamViolationTypeID
                                                              where ep.ExaminationsID == examinationsID && ep.ExamGroupID == examGroupID
                                                              && ep.SubjectID == subjectID
                                                              select new ExamPupilViolateMarkBO
                                                              {
                                                                  ExamPupilID = ep.ExamPupilID,
                                                                  ExamViolationTypeID = ep.ExamViolationTypeID,
                                                                  penalizedMark = ev.PenalizedMark

                                                              });

            // Lay danh sach "vao diem thi" doi voi nhung hoc sinh da duoc nhap diem
            IQueryable<ExamInputMark> listInput = this.Search(search);

            ExamInputMarkBO temp = null;
            ExamInputMark input = null;
            List<ExamPupilViolateMarkBO> tempViolate = null;
            for (int i = 0; i < query.Count; i++)
            {
                temp = query[i];

                // Danh dau vang thi
                if (listAbsence.Contains(temp.ExamPupilID))
                {
                    query[i].isAbsence = 1;
                }
                else
                {
                    query[i].isAbsence = 0;
                }

                // Tinh % diem
                int penalized = 100;
                tempViolate = listViolate.Where(o => o.ExamPupilID == temp.ExamPupilID).ToList();
                for (int j = 0; j < tempViolate.Count; j++)
                {
                    if (tempViolate[j].penalizedMark.HasValue)
                    {
                        penalized = penalized - tempViolate[j].penalizedMark.Value;
                    }
                }
                query[i].penalizedMark = penalized;

                // Xac dinh diem thi va diem thuc te neu da vao diem thi
                input = listInput.Where(o => o.PupilID == temp.PupilID).ToList().FirstOrDefault();
                if (input != null)
                {
                    query[i].ExamJudgeMark = input.ExamJudgeMark;
                    query[i].ExamMark = input.ExamMark;
                    query[i].ActualMark = input.ActualMark;
                    if (isCommenting == 0)
                    {
                        if (input.ExamMark.HasValue)
                        {
                            if (input.ActualMark.HasValue) // TH da co diem thuc te
                            {
                                if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                {
                                    query[i].ExamMarkInput = Decimal.Round(input.ExamMark.Value, 0).ToString();
                                    query[i].ActualMarkInput = Decimal.Round(input.ActualMark.Value, 0).ToString();
                                }
                                else // Cấp 2, 3
                                {
                                    query[i].ExamMarkInput = Decimal.Round(input.ExamMark.Value, 1).ToString().Replace(",", ".");
                                    query[i].ActualMarkInput = Decimal.Round(input.ActualMark.Value, 1).ToString().Replace(",", ".");
                                }
                            }
                            else // TH diem thuc te chua duoc tinh
                            {
                                // Tinh diem thuc te theo cong thuc
                                decimal actualMark = (input.ExamMark.Value * query[i].penalizedMark) / 100;
                                if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                {
                                    query[i].ExamMarkInput = Decimal.Round(input.ExamMark.Value, 0).ToString();
                                    query[i].ActualMarkInput = Decimal.Round(actualMark, 0).ToString();
                                }
                                else // Cấp 2, 3
                                {
                                    query[i].ExamMarkInput = Decimal.Round(input.ExamMark.Value, 1).ToString().Replace(",", ".");
                                    query[i].ActualMarkInput = Decimal.Round(actualMark, 1).ToString().Replace(",", ".");
                                }
                            }
                        }
                    }
                    else
                    {
                        query[i].ExamMarkInput = input.ExamJudgeMark;
                        query[i].ActualMarkInput = input.ExamJudgeMark;
                    }
                }
            }

            return query;
        }

        public List<ExamInputMarkBO> GetListInputMarkExamRoom(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetInt(search, "ExaminationsID");
            long examGroupID = Utils.GetInt(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            int markInputType = Utils.GetInt(search, "MarkInputType");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            List<long> lstExamRoomID = null;
            if (search.ContainsKey("ListExamRoomID"))
            {
                lstExamRoomID = (List<long>)search["ListExamRoomID"];
            }

            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
            int isCommenting = 0;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(search).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue && schoolSubject.IsCommenting.Value == 1)
            {
                isCommenting = 1;
            }

            List<ExamInputMarkBO> query = null;
            if (markInputType == SystemParamsInFile.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                var iq = (from db in ExamDetachableBagRepository.All.Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.AcademicYearID == academicYearID)
                          join ep in ExamPupilRepository.All.Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.SchoolID == schoolID) on db.ExamPupilID equals ep.ExamPupilID
                          join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                          join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                          join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID) on ep.ExamRoomID equals er.ExamRoomID
                          where poc.AcademicYearID == academicYearID && poc.SchoolID == schoolID && db.ExaminationsID == examinationsID
                          && db.ExamGroupID == examGroupID && db.SubjectID == subjectID && pp.IsActive == true
                          && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                          select new ExamInputMarkBO
                          {
                              ExamDetachableBagID = db.ExamDetachableBagID,
                              ExamDetachableBagCode = db.ExamDetachableBagCode,
                              ExamPupilID = ep.ExamPupilID,
                              PupilID = ep.PupilID,
                              ClassID = poc.ClassID,
                              ExamineeNumber = ep.ExamineeNumber,
                              ExamRoomID = ep.ExamRoomID,
                              ExamRoomCode = er.ExamRoomCode

                          });

                if (lstExamRoomID != null)
                {
                    iq = iq.Where(o => o.ExamRoomID.HasValue ? lstExamRoomID.Contains(o.ExamRoomID.Value) : false);
                }
                if (examRoomID != 0)
                {
                    iq = iq.Where(o => o.ExamRoomID == examRoomID);
                }

                query = iq.ToList();

                // Danh dau thi sinh vang thi
                List<long> listAbsence = ExamPupilAbsenceBusiness.Search(search).Select(o => o.ExamPupilID).ToList();

                // Tinh % diem cho thi sinh vi pham quy che thi
                IQueryable<ExamPupilViolateMarkBO> listViolate = (from ep in ExamPupilViolateRepository.All
                                                                  join ev in ExamViolationTypeRepository.All on ep.ExamViolationTypeID equals ev.ExamViolationTypeID
                                                                  where ep.ExaminationsID == examinationsID && ep.ExamGroupID == examGroupID
                                                                  && ep.SubjectID == subjectID && (ep.ExamRoomID == examRoomID || examRoomID == 0)
                                                                  select new ExamPupilViolateMarkBO
                                                                  {
                                                                      ExamPupilID = ep.ExamPupilID,
                                                                      ExamViolationTypeID = ep.ExamViolationTypeID,
                                                                      penalizedMark = ev.PenalizedMark

                                                                  });

                // Lay danh sach "vao diem thi" doi voi nhung hoc sinh da duoc nhap diem
                IQueryable<ExamInputMark> listInput = this.Search(search);

                ExamInputMarkBO temp = null;
                ExamInputMark input = null;
                List<ExamPupilViolateMarkBO> tempViolate = null;
                for (int i = 0; i < query.Count; i++)
                {
                    temp = query[i];

                    // Danh dau vang thi
                    if (listAbsence.Contains(temp.ExamPupilID))
                    {
                        query[i].isAbsence = 1;
                    }
                    else
                    {
                        query[i].isAbsence = 0;
                    }

                    // Tinh % diem
                    int penalized = 100;
                    tempViolate = listViolate.Where(o => o.ExamPupilID == temp.ExamPupilID).ToList();
                    for (int j = 0; j < tempViolate.Count; j++)
                    {
                        if (tempViolate[j].penalizedMark.HasValue)
                        {
                            penalized = penalized - tempViolate[j].penalizedMark.Value;
                        }
                    }
                    query[i].penalizedMark = penalized;

                    // Xac dinh diem thi va diem thuc te neu da vao diem thi
                    input = listInput.Where(o => o.PupilID == temp.PupilID).ToList().FirstOrDefault();
                    if (input != null)
                    {
                        query[i].ExamJudgeMark = input.ExamJudgeMark;
                        query[i].ExamMark = input.ExamMark;
                        query[i].ActualMark = input.ActualMark;
                        if (isCommenting == 0)
                        {
                            if (input.ExamMark.HasValue)
                            {
                                if (input.ActualMark.HasValue) // TH da co diem thuc te
                                {
                                    if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(input.ActualMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else // Cấp 2, 3
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(input.ActualMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                    }
                                }
                                else // TH diem thuc te chua duoc tinh
                                {
                                    // Tinh diem thuc te theo cong thuc
                                    decimal actualMark = (input.ExamMark.Value * query[i].penalizedMark) / 100;
                                    if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(actualMark, 0, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else // Cấp 2, 3
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(actualMark, 1, MidpointRounding.AwayFromZero).ToString();
                                    }
                                }
                            }
                            else
                            {
                                query[i].ExamMarkInput = input.ExamMark.ToString();
                                query[i].ActualMarkInput = input.ActualMark.ToString();
                            }
                        }
                        else
                        {
                            query[i].ExamMarkInput = input.ExamJudgeMark;
                            query[i].ActualMarkInput = input.ExamJudgeMark;
                        }
                    }
                }
            }
            else if (markInputType == SystemParamsInFile.MARK_INPUT_TYPE_EXAMINEE_CODE)
            {
                var iq = (from ep in ExamPupilRepository.All.Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.SchoolID == schoolID)
                          join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                          join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                          join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID) on ep.ExamRoomID equals er.ExamRoomID
                          where poc.AcademicYearID == academicYearID && poc.SchoolID == schoolID
                          && ep.ExaminationsID == examinationsID && ep.ExamGroupID == examGroupID && pp.IsActive == true
                          && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                          select new ExamInputMarkBO
                          {
                              ExamPupilID = ep.ExamPupilID,
                              PupilID = ep.PupilID,
                              ClassID = poc.ClassID,
                              ExamineeNumber = ep.ExamineeNumber,
                              ExamRoomID = ep.ExamRoomID,
                              ExamRoomCode = er.ExamRoomCode

                          });

                if (lstExamRoomID != null)
                {
                    iq = iq.Where(o => o.ExamRoomID.HasValue ? lstExamRoomID.Contains(o.ExamRoomID.Value) : false);
                }

                if (examRoomID != 0)
                {
                    iq = iq.Where(o => o.ExamRoomID == examRoomID);
                }
                query = iq.ToList();
                // Danh dau thi sinh vang thi
                List<long> listAbsence = ExamPupilAbsenceBusiness.Search(search).Select(o => o.ExamPupilID).ToList();

                // Tinh % diem cho thi sinh vi pham quy che thi
                IQueryable<ExamPupilViolateMarkBO> listViolate = (from ep in ExamPupilViolateRepository.All
                                                                  join ev in ExamViolationTypeRepository.All on ep.ExamViolationTypeID equals ev.ExamViolationTypeID
                                                                  where ep.ExaminationsID == examinationsID && ep.ExamGroupID == examGroupID
                                                                  && ep.SubjectID == subjectID && (ep.ExamRoomID == examRoomID || examRoomID == 0)
                                                                  select new ExamPupilViolateMarkBO
                                                                  {
                                                                      ExamPupilID = ep.ExamPupilID,
                                                                      ExamViolationTypeID = ep.ExamViolationTypeID,
                                                                      penalizedMark = ev.PenalizedMark

                                                                  });

                // Lay danh sach "vao diem thi" doi voi nhung hoc sinh da duoc nhap diem
                IQueryable<ExamInputMark> listInput = this.Search(search);

                ExamInputMarkBO temp = null;
                ExamInputMark input = null;
                List<ExamPupilViolateMarkBO> tempViolate = null;
                for (int i = 0; i < query.Count; i++)
                {
                    temp = query[i];

                    // Danh dau vang thi
                    if (listAbsence.Contains(temp.ExamPupilID))
                    {
                        query[i].isAbsence = 1;
                    }
                    else
                    {
                        query[i].isAbsence = 0;
                    }

                    // Tinh % diem
                    int penalized = 100;
                    tempViolate = listViolate.Where(o => o.ExamPupilID == temp.ExamPupilID).ToList();
                    for (int j = 0; j < tempViolate.Count; j++)
                    {
                        if (tempViolate[j].penalizedMark.HasValue)
                        {
                            penalized = penalized - tempViolate[j].penalizedMark.Value;
                        }
                    }
                    query[i].penalizedMark = penalized;

                    // Xac dinh diem thi va diem thuc te neu da vao diem thi
                    input = listInput.Where(o => o.PupilID == temp.PupilID).ToList().FirstOrDefault();
                    if (input != null)
                    {
                        query[i].ExamJudgeMark = input.ExamJudgeMark;
                        query[i].ExamMark = input.ExamMark;
                        query[i].ActualMark = input.ActualMark;
                        if (isCommenting == 0)
                        {
                            if (input.ExamMark.HasValue)
                            {
                                if (input.ActualMark.HasValue) // TH da co diem thuc te
                                {
                                    if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(input.ActualMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else // Cấp 2, 3
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(input.ActualMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                    }
                                }
                                else // TH diem thuc te chua duoc tinh
                                {
                                    // Tinh diem thuc te theo cong thuc
                                    decimal actualMark = (input.ExamMark.Value * query[i].penalizedMark) / 100;
                                    if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(actualMark, 0, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else // Cấp 2, 3
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Decimal.Round(actualMark, 1, MidpointRounding.AwayFromZero).ToString();
                                    }
                                }
                            }
                            else
                            {
                                query[i].ExamMarkInput = input.ExamMark.ToString();
                                query[i].ActualMarkInput = input.ActualMark.ToString();
                            }
                        }
                        else
                        {
                            query[i].ExamMarkInput = input.ExamJudgeMark;
                            query[i].ActualMarkInput = input.ExamJudgeMark;
                        }
                    }
                }
            }
            else if (markInputType == SystemParamsInFile.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                var iq = (from ep in ExamPupilRepository.All.Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.SchoolID == schoolID)
                          join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                          join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                          join cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && o.SchoolID == schoolID && o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                          join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID) on ep.ExamRoomID equals er.ExamRoomID
                          where poc.AcademicYearID == academicYearID && poc.SchoolID == schoolID
                          && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING && pp.IsActive == true
                          && ep.ExaminationsID == examinationsID && ep.ExamGroupID == examGroupID
                          select new ExamInputMarkBO
                          {
                              ExamPupilID = ep.ExamPupilID,
                              PupilID = ep.PupilID,
                              PupilCode = pp.PupilCode,
                              Name = pp.Name,
                              FullName = pp.FullName,
                              EthnicCode = pp.Ethnic.EthnicCode,
                              ClassID = cp.ClassProfileID,
                              ClassName = cp.DisplayName,
                              ExamRoomID = ep.ExamRoomID,
                              ExamRoomCode = er.ExamRoomCode

                          });

                if (lstExamRoomID != null)
                {
                    iq = iq.Where(o => o.ExamRoomID.HasValue ? lstExamRoomID.Contains(o.ExamRoomID.Value) : false);
                }

                if (examRoomID != 0)
                {
                    iq = iq.Where(o => o.ExamRoomID == examRoomID);
                }

                query = iq.ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName.getOrderingName(o.EthnicCode))).ToList();

                // Danh dau thi sinh vang thi
                List<long> listAbsence = ExamPupilAbsenceBusiness.Search(search).Select(o => o.ExamPupilID).ToList();

                // Tinh % diem cho thi sinh vi pham quy che thi
                IQueryable<ExamPupilViolateMarkBO> listViolate = (from ep in ExamPupilViolateRepository.All
                                                                  join ev in ExamViolationTypeRepository.All on ep.ExamViolationTypeID equals ev.ExamViolationTypeID
                                                                  where ep.ExaminationsID == examinationsID && ep.ExamGroupID == examGroupID
                                                                  && ep.SubjectID == subjectID && (ep.ExamRoomID == examRoomID || examRoomID == 0)
                                                                  select new ExamPupilViolateMarkBO
                                                                  {
                                                                      ExamPupilID = ep.ExamPupilID,
                                                                      ExamViolationTypeID = ep.ExamViolationTypeID,
                                                                      penalizedMark = ev.PenalizedMark

                                                                  });

                // Lay danh sach "vao diem thi" doi voi nhung hoc sinh da duoc nhap diem
                IQueryable<ExamInputMark> listInput = this.Search(search);

                ExamInputMarkBO temp = null;
                ExamInputMark input = null;
                List<ExamPupilViolateMarkBO> tempViolate = null;
                for (int i = 0; i < query.Count; i++)
                {
                    temp = query[i];

                    // Danh dau vang thi
                    if (listAbsence.Contains(temp.ExamPupilID))
                    {
                        query[i].isAbsence = 1;
                    }
                    else
                    {
                        query[i].isAbsence = 0;
                    }

                    // Tinh % diem
                    int penalized = 100;
                    tempViolate = listViolate.Where(o => o.ExamPupilID == temp.ExamPupilID).ToList();
                    for (int j = 0; j < tempViolate.Count; j++)
                    {
                        if (tempViolate[j].penalizedMark.HasValue)
                        {
                            penalized = penalized - tempViolate[j].penalizedMark.Value;
                        }
                    }
                    query[i].penalizedMark = penalized;

                    // Xac dinh diem thi va diem thuc te neu da vao diem thi
                    input = listInput.Where(o => o.PupilID == temp.PupilID).ToList().FirstOrDefault();
                    if (input != null)
                    {
                        query[i].ExamJudgeMark = input.ExamJudgeMark;
                        query[i].ExamMark = input.ExamMark;
                        query[i].ActualMark = input.ActualMark;
                        if (isCommenting == 0)
                        {
                            if (input.ExamMark.HasValue)
                            {
                                if (input.ActualMark.HasValue) // TH da co diem thuc te
                                {
                                    if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(input.ActualMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else // Cấp 2, 3
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(input.ActualMark.Value, 1).ToString();
                                    }
                                }
                                else // TH diem thuc te chua duoc tinh
                                {
                                    // Tinh diem thuc te theo cong thuc
                                    decimal actualMark = (input.ExamMark.Value * query[i].penalizedMark) / 100;
                                    if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY) // Cấp 1
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 0, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(actualMark, 0, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else // Cấp 2, 3
                                    {
                                        query[i].ExamMarkInput = Math.Round(input.ExamMark.Value, 1, MidpointRounding.AwayFromZero).ToString();
                                        query[i].ActualMarkInput = Math.Round(actualMark, 1, MidpointRounding.AwayFromZero).ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            query[i].ExamMarkInput = input.ExamJudgeMark;
                            query[i].ActualMarkInput = input.ExamJudgeMark;
                        }
                    }
                }
            }

            return query;
        }

        public void InputMark(IDictionary<string, object> info, int[] ListPupilID, long[] CheckPupilID, string[] ExamMark, string[] ActualMark, int[] ClassID)
        {
            int academicYearID = Utils.GetInt(info, "AcademicYearID");
            int schoolID = Utils.GetInt(info, "SchoolID");
            long examinationsID = Utils.GetLong(info, "ExaminationsID");
            long examGroupID = Utils.GetLong(info, "ExamGroupID");
            int subjectID = Utils.GetInt(info, "SubjectID");

            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
            int isCommenting = 0;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(info).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue && schoolSubject.IsCommenting.Value == 1)
            {
                isCommenting = 1;
            }

            List<ExamInputMark> listInput = this.Search(info).ToList();
            ExamInputMark checkUpdate = null;

            ExamInputMark insert = null;
            for (int i = 0; i < ListPupilID.Length; i++)
            {
                if (CheckPupilID != null && CheckPupilID.Contains(ListPupilID[i]))
                {
                    checkUpdate = listInput.Where(o => o.PupilID == ListPupilID[i]).ToList().FirstOrDefault();
                    decimal examMark = 0, actualMark = 0;
                    bool markA = false, markB = false;
                    if (ExamMark != null && ActualMark != null && ExamMark[i] != null && ActualMark[i] != null)
                    {
                        markA = Decimal.TryParse(ExamMark[i].Replace('.', ','), out examMark);
                        markB = Decimal.TryParse(ActualMark[i].Replace('.', ','), out actualMark);
                    }
                    if (checkUpdate == null)
                    {
                        insert = new ExamInputMark();
                        insert.ExamInputMarkID = this.GetNextSeq<long>();
                        insert.AcademicYearID = academicYearID;
                        insert.SchoolID = schoolID;
                        insert.LastDigitSchoolID = schoolID % 100;
                        insert.ExaminationsID = examinationsID;
                        insert.ExamGroupID = examGroupID;
                        insert.SubjectID = subjectID;
                        insert.PupilID = ListPupilID[i];
                        if (isCommenting == 0)
                        {
                            if (markA == true && markB == true)
                            {
                                insert.ExamMark = examMark;
                                insert.ActualMark = Math.Round(actualMark, 1, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                insert.ExamMark = null;
                                insert.ActualMark = null;
                            }
                        }
                        else
                        {
                            insert.ExamJudgeMark = ActualMark[i];
                        }
                        insert.ClassID = ClassID[i];
                        insert.CreateTime = DateTime.Now;
                        this.Insert(insert);
                    }
                    else
                    {
                        if (isCommenting == 0)
                        {
                            if (markA == true && markB == true)
                            {
                                checkUpdate.ExamMark = examMark;
                                checkUpdate.ActualMark = Math.Round(actualMark, 1, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                checkUpdate.ExamMark = null;
                                checkUpdate.ActualMark = null;
                            }
                        }
                        else
                        {
                            checkUpdate.ExamJudgeMark = ActualMark[i];
                        }
                        checkUpdate.UpdateTime = DateTime.Now;
                        this.Update(checkUpdate);
                    }
                }
            }

            //tutv4 : nghiep vu khong yeu cau
            // Chuyen trang thai ky thi dang xet thanh "Vao diem thi"
            //Examinations exam = ExaminationsBusiness.Find(examinationsID);            
            //if (exam != null)
            //{
            //    exam.MarkInput = true;
            //    ExaminationsBusiness.Update(exam);
            //    ExaminationsBusiness.Save();
            //}

            this.Save();
        }
        #endregion

        #region Report Nhap Diem Theo So Phach

        public ProcessedReport GetReportDetachableBag(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_SO_PHACH;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReportDetachableBag(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            long examCandenceBagID = Utils.GetLong(dic["ExamCandenceBagID"]);
            int isGetAll = Utils.GetInt(dic["IsGetAll"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);
            List<long> lstExamCandenceBagID = null;
            if (dic.ContainsKey("ListExamCandenceBagID"))
            {
                lstExamCandenceBagID = (List<long>)dic["ListExamCandenceBagID"];
            }

            List<long> lstExamRoomID = null;
            if (dic.ContainsKey("ListExamRoomID"))
            {
                lstExamRoomID = (List<long>)dic["ListExamRoomID"];
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);

            //Lay kieu mon
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = dic["AcademicYearID"];
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["SubjectID"] = dic["SubjectID"];
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(Utils.GetInt(dic["SchoolID"]), tmpDic)
                .OrderBy(o => o.EducationLevelID).FirstOrDefault();
            int? isCommenting = null;
            if (schoolSubject != null)
            {
                isCommenting = schoolSubject.IsCommenting;
            }

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_SO_PHACH;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY ? oBook.GetSheet(3) : (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK ? oBook.GetSheet(1) : oBook.GetSheet(2));

            //Điền các thông tin tiêu đề báo cáo
            string schoolName = "TRƯỜNG " + school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = "KỲ THI " + exam.ExaminationsName.ToUpper();
            string academicYearName = "Năm học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("A4", examinationsName);
            oSheet.SetCellValue("D4", provinceAndDate);
            oSheet.SetCellValue("A5", academicYearName);


            //Fill du lieu
            //Lay danh sach tui phach
            List<ExamCandenceBag> listExamCandenceBag = ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                                                && o.ExamGroupID == examGroupID
                                                                                                && o.SubjectID == subjectID)
                                                                                                .OrderBy(o => o.ExamCandenceBagCode).ToList();
            if (lstExamCandenceBagID != null)
            {
                listExamCandenceBag = listExamCandenceBag.Where(o => lstExamCandenceBagID.Contains(o.ExamCandenceBagID)).ToList();
            }


            if (isGetAll == 0)
            {
                listExamCandenceBag = listExamCandenceBag.Where(o => o.ExamCandenceBagID == examCandenceBagID).ToList();
            }

            tmpDic = new Dictionary<string, object>();

            tmpDic["AcademicYearID"] = dic["AcademicYearID"];
            tmpDic["SchoolID"] = dic["SchoolID"];
            tmpDic["ExaminationsID"] = dic["ExaminationsID"];
            tmpDic["ExamGroupID"] = dic["ExamGroupID"];
            tmpDic["SubjectID"] = dic["SubjectID"];
            if (lstExamRoomID != null)
            {
                tmpDic["ListExamRoomID"] = lstExamRoomID;
            }


            List<ExamInputMarkBO> listResult;
            if (tmpDic["ExaminationsID"] == null || tmpDic["ExamGroupID"] == null || tmpDic["SubjectID"] == null)
            {
                listResult = new List<ExamInputMarkBO>();
            }
            else
            {
                listResult = this.ExamInputMarkBusiness.GetListInputMarkCandenceBag(tmpDic).ToList();
            }

            //Tao tung sheet cho moi tui phach
            for (int i = 0; i < listExamCandenceBag.Count; i++)
            {
                ExamCandenceBag ecb = listExamCandenceBag[i];
                List<ExamInputMarkBO> list = listResult.Where(o => o.ExamCandenceBagID == ecb.ExamCandenceBagID).OrderBy(o => o.ExamDetachableBagCode).ToList();

                int startRow = SystemParamsInFile.START_ROW_REPORT_DETACHABLE_BAG;
                int lastRow = startRow + list.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 4;
                int curColumn = startColumn;

                //Tao sheet cho tui phach
                IVTWorksheet curSheet = oBook.CopySheetToLast(oSheet, "II" + (lastRow).ToString());

                StringBuilder sb = new StringBuilder();
                sb.Append("MÔN THI: ");
                sb.Append(subject.SubjectName.ToUpper());
                sb.Append(" - TÚI PHÁCH: ");
                sb.Append(ecb.ExamCandenceBagCode.ToUpper());
                string reportTitle = sb.ToString();

                curSheet.SetCellValue("A7", reportTitle);

                for (int j = 0; j < list.Count; j++)
                {
                    ExamInputMarkBO obj = list[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //So phach
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamDetachableBagCode);
                    curColumn++;
                    //Diem thi
                    if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                    {
                        curSheet.SetCellValue(curRow, curColumn, obj.ExamMark);
                    }
                    else
                    {
                        if (obj.isAbsence == 0)
                        {
                            if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamMark != 0 && obj.ExamMark != 10 ? String.Format("{0:0.0}", obj.ExamMark).Replace(",", ".") : String.Format("{0:0}", obj.ExamMark));
                            }
                            else
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamJudgeMark);
                            }
                        }
                    }
                    curColumn++;
                    //Ghi chu
                    curSheet.SetCellValue(curRow, curColumn, obj.NoteReport);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                //Doi ten sheet thanh ten cua Tui phach
                curSheet.Name = ecb.ExamCandenceBagCode.ToUpper();
            }

            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();

            return oBook.ToStream();
        }

        public ProcessedReport InsertReportDetachableBag(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_SO_PHACH;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup group = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string groupName = group != null ? group.ExamGroupName : string.Empty;
            SubjectCat sc = SubjectCatBusiness.Find(dic["SubjectID"]);
            string subjectName = sc != null ? sc.DisplayName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(groupName));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        #endregion

        #region Report Nhap Diem Theo Phong Thi

        public ProcessedReport GetReportExamRoom(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_PHONG_THI;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReportExamRoom(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int markInputType = Utils.GetInt(dic["MarkInputType"]);
            int isGetAll = Utils.GetInt(dic["IsGetAll"]);
            List<long> lstExamRoomID = null;
            if (dic.ContainsKey("ListExamRoomID"))
            {
                lstExamRoomID = (List<long>)dic["ListExamRoomID"];
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);

            //Lay kieu mon
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = dic["AcademicYearID"];
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["SubjectID"] = dic["SubjectID"];
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(Utils.GetInt(dic["SchoolID"]), tmpDic)
                .OrderBy(o => o.EducationLevelID).FirstOrDefault();

            int? isCommenting = null;
            if (schoolSubject != null)
            {
                isCommenting = schoolSubject.IsCommenting;
            }

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_PHONG_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY ? oBook.GetSheet(3) : (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK ? oBook.GetSheet(1) : oBook.GetSheet(2));

            //Điền các thông tin tiêu đề báo cáo
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = "KỲ THI " + exam.ExaminationsName.ToUpper();
            string academicYearName = "Năm học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("A4", examinationsName);
            oSheet.SetCellValue("D4", provinceAndDate);
            oSheet.SetCellValue("A5", academicYearName);


            //Fill du lieu
            //Lay danh sach phong thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                    && o.ExamGroupID == examGroupID).OrderBy(o => o.ExamRoomCode).ToList();

            if (lstExamRoomID != null)
            {
                listExamRoom = listExamRoom.Where(o => lstExamRoomID.Contains(o.ExamRoomID)).ToList();
            }
            if (isGetAll == 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }

            tmpDic = new Dictionary<string, object>();

            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["ExaminationsID"] = examinationsID;
            tmpDic["ExamGroupID"] = examGroupID;
            tmpDic["SubjectID"] = subjectID;
            tmpDic["MarkInputType"] = markInputType;

            List<ExamInputMarkBO> listResult;
            if (tmpDic["ExaminationsID"] == null || tmpDic["ExamGroupID"] == null || tmpDic["SubjectID"] == null)
            {
                listResult = new List<ExamInputMarkBO>();
            }
            else
            {
                listResult = this.ExamInputMarkBusiness.GetListInputMarkExamRoom(tmpDic).ToList();
            }

            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom er = listExamRoom[i];

                List<ExamInputMarkBO> list = listResult.Where(o => o.ExamRoomID == er.ExamRoomID).OrderBy(o => o.ExamDetachableBagCode).ToList();

                int startRow = SystemParamsInFile.START_ROW_REPORT_EXAM_ROOM;
                int lastRow = startRow + list.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 4;
                int curColumn = startColumn;

                //Tao sheet cho tui phach
                IVTWorksheet curSheet = oBook.CopySheetToLast(oSheet, "II" + (lastRow).ToString());

                StringBuilder sb = new StringBuilder();
                sb.Append("PHÒNG THI: ");
                sb.Append(er.ExamRoomCode.ToUpper());
                sb.Append(", MÔN THI: ");
                sb.Append(subject.SubjectName.ToUpper());
                string reportTitle = sb.ToString();

                curSheet.SetCellValue("A7", reportTitle);


                for (int j = 0; j < list.Count; j++)
                {
                    ExamInputMarkBO obj = list[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //So phach
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamDetachableBagCode);
                    curColumn++;
                    //Diem thi
                    if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                    {
                        curSheet.SetCellValue(curRow, curColumn, obj.ExamMark);
                    }
                    else
                    {
                        if (obj.isAbsence == 0)
                        {
                            if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamMark != 0 && obj.ExamMark != 10 ? String.Format("{0:0.0}", obj.ExamMark) : String.Format("{0:0}", obj.ExamMark));
                            }
                            else
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamJudgeMark);
                            }
                        }
                    }
                    curColumn++;
                    //Ghi chu
                    curSheet.SetCellValue(curRow, curColumn, obj.NoteReport);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                // Doi ten sheet thanh ma Phong thi
                curSheet.Name = er.ExamRoomCode.ToUpper();
            }

            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();

            return oBook.ToStream();
        }

        public ProcessedReport InsertReportExamRoom(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_PHONG_THI;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup group = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string groupName = group != null ? group.ExamGroupName : string.Empty;
            SubjectCat sc = SubjectCatBusiness.Find(dic["SubjectID"]);
            string subjectName = sc != null ? sc.DisplayName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(groupName));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        #endregion

        #region Report Nhap Diem Theo Ma Hoc Sinh

        public ProcessedReport GetReportPupilCode(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_MA_HOC_SINH;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReportPupilCode(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int markInputType = Utils.GetInt(dic["MarkInputType"]);
            int isGetAll = Utils.GetInt(dic["IsGetAll"]);
            List<long> lstExamRoomID = null;
            if (dic.ContainsKey("ListExamRoomID"))
            {
                lstExamRoomID = (List<long>)dic["ListExamRoomID"];
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);

            //Lay kieu mon
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = dic["AcademicYearID"];
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["SubjectID"] = dic["SubjectID"];
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(Utils.GetInt(dic["SchoolID"]), tmpDic)
                .OrderBy(o => o.EducationLevelID).FirstOrDefault();

            int? isCommenting = null;
            if (schoolSubject != null)
            {
                isCommenting = schoolSubject.IsCommenting;
            }

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_MA_HOC_SINH;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY ? oBook.GetSheet(3) : (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK ? oBook.GetSheet(1) : oBook.GetSheet(2));

            //Điền các thông tin tiêu đề báo cáo

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = "KỲ THI " + exam.ExaminationsName.ToUpper();
            string academicYearName = "Năm học: " + academicYear.DisplayTitle;

            //StringBuilder sb = new StringBuilder();
            //sb.Append("PHÒNG THI: ");
            //sb.Append(room.ExamRoomCode.ToUpper());
            //sb.Append(", MÔN THI: ");
            //sb.Append(subject.SubjectName.ToUpper());
            //string reportTitle = sb.ToString();

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("A4", examinationsName);
            oSheet.SetCellValue("D4", provinceAndDate);
            oSheet.SetCellValue("A5", academicYearName);
            //oSheet.SetCellValue("A7", reportTitle);

            //Fill du lieu
            //Lay danh sach phong thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                    && o.ExamGroupID == examGroupID).OrderBy(o => o.ExamRoomCode).ToList();
            if (lstExamRoomID != null)
            {
                listExamRoom = listExamRoom.Where(o => lstExamRoomID.Contains(o.ExamRoomID)).ToList();
            }
            if (isGetAll == 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }

            tmpDic = new Dictionary<string, object>();

            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["ExaminationsID"] = examinationsID;
            tmpDic["ExamGroupID"] = examGroupID;
            tmpDic["SubjectID"] = subjectID;
            tmpDic["MarkInputType"] = markInputType;

            List<ExamInputMarkBO> listResult;
            if (tmpDic["ExaminationsID"] == null || tmpDic["ExamGroupID"] == null || tmpDic["SubjectID"] == null)
            {
                listResult = new List<ExamInputMarkBO>();
            }
            else
            {
                listResult = this.ExamInputMarkBusiness.GetListInputMarkExamRoom(tmpDic).ToList();
            }

            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom er = listExamRoom[i];

                List<ExamInputMarkBO> list = listResult.Where(o => o.ExamRoomID == er.ExamRoomID).ToList();

                int startRow = SystemParamsInFile.START_ROW_REPORT_PUPIL_CODE;
                int lastRow = startRow + list.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 5;
                int curColumn = startColumn;

                //Tao sheet cho tui phach
                IVTWorksheet curSheet = oBook.CopySheetToLast(oSheet, "II" + (lastRow).ToString());

                StringBuilder sb = new StringBuilder();
                sb.Append("PHÒNG THI: ");
                sb.Append(er.ExamRoomCode.ToUpper());
                sb.Append(", MÔN THI: ");
                sb.Append(subject.SubjectName.ToUpper());
                string reportTitle = sb.ToString();

                curSheet.SetCellValue("A7", reportTitle);

                for (int j = 0; j < list.Count; j++)
                {
                    ExamInputMarkBO obj = list[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //Ma hoc sinh
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilCode);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.FullName);
                    curColumn++;
                    //Diem thi
                    if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                    {
                        curSheet.SetCellValue(curRow, curColumn, obj.ExamMark);
                    }
                    else
                    {
                        if (obj.isAbsence == 0)
                        {
                            if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamMark != 0 && obj.ExamMark != 10 ? String.Format("{0:0.0}", obj.ExamMark) : String.Format("{0:0}", obj.ExamMark));
                            }
                            else
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamJudgeMark);
                            }
                        }
                    }
                    curColumn++;
                    //Ghi chu
                    curSheet.SetCellValue(curRow, curColumn, obj.NoteReport);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                // Doi ten sheet thanh ma Phong thi
                curSheet.Name = er.ExamRoomCode.ToUpper();
            }

            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();

            return oBook.ToStream();
        }

        public ProcessedReport InsertReportPupilCode(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_MA_HOC_SINH;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup group = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string groupName = group != null ? group.ExamGroupName : string.Empty;
            SubjectCat sc = SubjectCatBusiness.Find(dic["SubjectID"]);
            string subjectName = sc != null ? sc.DisplayName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(groupName));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        #endregion

        #region Report Nhap Diem Theo So Bao Danh

        public ProcessedReport GetReportExamineeCode(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_SO_BAO_DANH;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReportExamineeCode(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int markInputType = Utils.GetInt(dic["MarkInputType"]);
            int isGetAll = Utils.GetInt(dic["IsGetAll"]);
            List<long> lstExamRoomID = null;
            if (dic.ContainsKey("ListExamRoomID"))
            {
                lstExamRoomID = (List<long>)dic["ListExamRoomID"];
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);

            //Lay kieu mon
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = dic["AcademicYearID"];
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            tmpDic["SubjectID"] = dic["SubjectID"];
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(Utils.GetInt(dic["SchoolID"]), tmpDic)
                .OrderBy(o => o.EducationLevelID).FirstOrDefault();

            int? isCommenting = null;
            if (schoolSubject != null)
            {
                isCommenting = schoolSubject.IsCommenting;
            }

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_SO_BAO_DANH;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY ? oBook.GetSheet(3) : (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK ? oBook.GetSheet(1) : oBook.GetSheet(2));

            //Điền các thông tin tiêu đề báo cáo

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = "KỲ THI " + exam.ExaminationsName.ToUpper();
            string academicYearName = "Năm học: " + academicYear.DisplayTitle;

            //StringBuilder sb = new StringBuilder();
            //sb.Append("PHÒNG THI: ");
            //sb.Append(room.ExamRoomCode.ToUpper());
            //sb.Append(", MÔN THI: ");
            //sb.Append(subject.SubjectName.ToUpper());
            //string reportTitle = sb.ToString();

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("A4", examinationsName);
            oSheet.SetCellValue("D4", provinceAndDate);
            oSheet.SetCellValue("A5", academicYearName);
            //oSheet.SetCellValue("A7", reportTitle);


            //Fill du lieu

            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                   && o.ExamGroupID == examGroupID).OrderBy(o => o.ExamRoomCode).ToList();
            if (lstExamRoomID != null)
            {
                listExamRoom = listExamRoom.Where(o => lstExamRoomID.Contains(o.ExamRoomID)).ToList();
            }

            if (isGetAll == 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }

            tmpDic = new Dictionary<string, object>();

            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["ExaminationsID"] = examinationsID;
            tmpDic["ExamGroupID"] = examGroupID;
            tmpDic["SubjectID"] = subjectID;
            tmpDic["MarkInputType"] = markInputType;

            List<ExamInputMarkBO> listResult;
            if (tmpDic["ExaminationsID"] == null || tmpDic["ExamGroupID"] == null || tmpDic["SubjectID"] == null)
            {
                listResult = new List<ExamInputMarkBO>();
            }
            else
            {
                listResult = this.ExamInputMarkBusiness.GetListInputMarkExamRoom(tmpDic).OrderBy(o => o.ExamineeNumber).ToList();
            }

            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom er = listExamRoom[i];

                List<ExamInputMarkBO> list = listResult.Where(o => o.ExamRoomID == er.ExamRoomID).ToList();

                int startRow = SystemParamsInFile.START_ROW_REPORT_EXAMINEE_CODE;
                int lastRow = startRow + list.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 4;
                int curColumn = startColumn;

                //Tao sheet cho tui phach
                IVTWorksheet curSheet = oBook.CopySheetToLast(oSheet, "II" + (lastRow).ToString());

                StringBuilder sb = new StringBuilder();
                sb.Append("PHÒNG THI: ");
                sb.Append(er.ExamRoomCode.ToUpper());
                sb.Append(", MÔN THI: ");
                sb.Append(subject.SubjectName.ToUpper());
                string reportTitle = sb.ToString();

                curSheet.SetCellValue("A7", reportTitle);

                for (int j = 0; j < list.Count; j++)
                {
                    ExamInputMarkBO obj = list[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //So bao danh
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //Diem thi
                    if (exam.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                    {
                        curSheet.SetCellValue(curRow, curColumn, obj.ExamMark);
                    }
                    else
                    {
                        if (obj.isAbsence == 0)
                        {
                            if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamMark != 0 && obj.ExamMark != 10 ? String.Format("{0:0.0}", obj.ExamMark) : String.Format("{0:0}", obj.ExamMark));
                            }
                            else
                            {
                                curSheet.SetCellValue(curRow, curColumn, obj.ExamJudgeMark);
                            }
                        }
                    }
                    curColumn++;
                    //Ghi chu
                    curSheet.SetCellValue(curRow, curColumn, obj.NoteReport);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                // Doi ten sheet thanh ma Phong thi
                curSheet.Name = er.ExamRoomCode.ToUpper();
            }

            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();


            return oBook.ToStream();
        }

        public ProcessedReport InsertReportExamineeCode(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_NHAP_DIEM_THEO_SO_BAO_DANH;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup group = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string groupName = group != null ? group.ExamGroupName : string.Empty;
            SubjectCat sc = SubjectCatBusiness.Find(dic["SubjectID"]);
            string subjectName = sc != null ? sc.DisplayName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(groupName));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        #endregion

        #region Report Thong ke diem thi theo giao vien
        public ProcessedReport GetExamMarkByTeacherStatisticReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_GIAO_VIEN;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamMarkByTeacherStatisticReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);
            int statisticLevelReportID = Utils.GetInt(dic["StatisticLevelReportID"]);
            bool femalePupil = Utils.GetBool(dic["FemalePupil"]);
            bool ethnicPupil = Utils.GetBool(dic["EthnicPupil"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            int appliedLevel = exam.AppliedLevel;

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_GIAO_VIEN;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = rp.TemplateName;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            //Danh sach mon hoc cho truong
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            #region Cap 2,3
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[0]);

                //Lấy ra sheet đầu tiên
                IVTWorksheet markSheet = oBook.GetSheet(1);

                //Lay ra sheet template mon nhan xet
                IVTWorksheet judgeSheet = oBook.GetSheet(2);

                //Dien du lieu vao phan tieu de
                markSheet.SetCellValue("A2", supervisingDeptName);
                markSheet.SetCellValue("A3", schoolName);
                markSheet.SetCellValue("A4", examinationsName);
                markSheet.SetCellValue("A5", strAcademicYear);
                markSheet.SetCellValue("K4", provinceAndDate);

                judgeSheet.SetCellValue("A2", supervisingDeptName);
                judgeSheet.SetCellValue("A3", schoolName);
                judgeSheet.SetCellValue("A4", examinationsName);
                judgeSheet.SetCellValue("A5", strAcademicYear);
                judgeSheet.SetCellValue("E4", provinceAndDate);

                //Fill muc thong ke
                //Lấy danh sách mức thống kê
                List<StatisticLevelConfig> lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", statisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();

                int numberConfig = lstStatisticLevel.Count();
                int levelColumn = 5;
                int levelRow = 8;
                for (int i = 0; i < numberConfig; i++)
                {
                    markSheet.SetCellValue(levelRow, levelColumn, lstStatisticLevel[i].Title);
                    markSheet.MergeRow(levelRow, levelColumn, levelColumn + 1);
                    markSheet.SetCellValue(levelRow + 1, levelColumn, "SL");
                    markSheet.SetCellValue(levelRow + 1, levelColumn + 1, "%");
                    levelColumn = levelColumn + 2;
                }

                IVTRange headerRange = markSheet.GetRange(levelRow, 1, levelRow + 1, 4 + numberConfig * 2);
                headerRange.FillColor(System.Drawing.Color.LightGray);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

                //Lay danh sach mon thi cua ky thi
                List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
                    listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList()
                                                        .Select(o => new ExamSubjectBO
                                                        {
                                                            SubjectID = o.SubjectID,
                                                            SubjectName = o.SubjectName
                                                        }).Distinct().ToList();
                }
                if (subjectID != 0)
                {
                    listExamSubject = listExamSubject.Where(o => o.SubjectID == subjectID).ToList();
                }

                //Lay danh sach diem thi cua ky thi
                List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>();
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["LastDigitSchoolID"] = partition;
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["ExaminationsID"] = examinationsID;

                    listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
                }

                if (femalePupil)
                {
                    listExamMark = listExamMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamMark = listExamMark.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Lay danh sach thi sinh co thi trong ky thi
                List<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).ToList();
                if (femalePupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Lay cac phan cong giang day
                tmpDic = new Dictionary<string, object>();
                tmpDic["Semester"] = exam.SemesterID;
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["AppliedLevel"] = exam.AppliedLevel;

                List<TeachingAssignmentBO> listTA = TeachingAssignmentBusiness.GetSubjectTeacher(schoolID, tmpDic).ToList();

                //Lay si so cua cac lop
                IQueryable<ClassProfile> iqClass = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearID && o.IsActive == true);
                IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"Semester",exam.SemesterID},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1}
                };
                List<ClassInfoBO> listClassWithTotalPupil = PupilOfClassBusiness.GetClassAndTotalPupil(iqClass, dicCATP).ToList();


                ////Fill du lieu
                for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                {
                    ExamSubjectBO subject = listExamSubject[iSubject];

                    //Lay loai mon
                    //Lay loai mon
                    SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();

                    int? isCommentSubject = null;
                    if (ss != null)
                    {
                        isCommentSubject = ss.IsCommenting;
                    }

                    //Loc lai danh sach thi sinh co thi mon thi nay
                    List<ExamPupilBO> listExamPupilOfSubject = listExamPupilOfExam.Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .Where(o => o.ExamPupilAbsenceID == 0).ToList();

                    //Loc lai cac phan cong giang day cua mon nay
                    List<TeachingAssignmentBO> listTAOfSubject = listTA.Where(o => o.SubjectID == subject.SubjectID)
                                                                        .OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.TeacherName.getOrderingName(o.EthnicCode)))
                                                                        .ThenBy(o => o.ClassOrderNumber)
                                                                        .ToList();
                    //Danh sach giao vien co day mon thi
                    List<int?> listTeacherID = listTAOfSubject.Select(o => o.TeacherID).Distinct().ToList();

                    int startRow = 10;
                    int lastRow = startRow + listTAOfSubject.Count + listTeacherID.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    //int lastColumn = 4 + numberConfig * 2;
                    int curColumn = startColumn;

                    #region Mon tinh diem
                    if (isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        int lastColumn = 4 + numberConfig * 2;

                        //Tao sheet cho mon thi
                        IVTWorksheet curSheet = oBook.CopySheetToLast(markSheet, "II" + (lastRow).ToString());

                        //Fill tieu de
                        string additionString = String.Empty;
                        if (femalePupil && ethnicPupil)
                        {
                            additionString = " HỌC SINH NỮ DÂN TỘC";
                        }
                        else if (femalePupil)
                        {
                            additionString = " HỌC SINH NỮ";
                        }
                        else if (ethnicPupil)
                        {
                            additionString = " HỌC SINH DÂN TỘC";
                        }
                        curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                        curSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI THEO GIÁO VIÊN MÔN THI " + subject.SubjectName.ToUpper() + additionString);

                        for (int iTeacher = 0; iTeacher < listTeacherID.Count; iTeacher++)
                        {
                            int? teacherID = listTeacherID[iTeacher];
                            List<TeachingAssignmentBO> listTaOfTeacherSubject = listTAOfSubject.Where(o => o.TeacherID == teacherID).ToList();
                            for (int iTa = 0; iTa < listTaOfTeacherSubject.Count; iTa++)
                            {
                                TeachingAssignmentBO ta = listTaOfTeacherSubject[iTa];
                                ClassInfoBO ClassInfo = listClassWithTotalPupil.Where(o => o.ClassID == ta.ClassID).FirstOrDefault();
                                List<ExamPupilBO> listExamPupilOfClass = listExamPupilOfSubject.Where(o => o.ClassID == ta.ClassID).ToList();
                                List<ExamInputMarkBO> listMarkOfClass = listExamMark.Where(o => o.ClassID == ta.ClassID)
                                                                                    .Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .ToList();


                                //Lop
                                curSheet.SetCellValue(curRow, curColumn, ta.ClassName);
                                curColumn++;
                                //Giao vien chu nhiem
                                curSheet.SetCellValue(curRow, curColumn, ta.TeacherName);
                                curColumn++;
                                //Si so
                                if (ClassInfo != null)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, ClassInfo.ActualNumOfPupil);
                                }
                                curColumn++;
                                //So bai kiem tra
                                curSheet.SetCellValue(curRow, curColumn, listExamPupilOfClass.Count());
                                curColumn++;

                                //Fill cac cot muc diem
                                //Diem thi cac mon
                                int SL = 0;
                                for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                                {
                                    StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                    if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                                        }
                                    }
                                    else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                                        }
                                    }
                                    else
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                                        }
                                    }

                                    curSheet.SetCellValue(curRow, curColumn, SL);//E
                                    curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                    curColumn += 2;

                                }

                                curSheet.GetRange(curRow, startColumn, curRow, lastColumn).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                                curRow++;
                                curColumn = startColumn;
                            }

                            //Fill dong tong
                            //Lop
                            curColumn++;
                            //GVBM
                            curSheet.SetCellValue(curRow, curColumn, "Tổng");//E
                            curSheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignCenter);
                            curColumn++;

                            //Si so
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                            curColumn++;
                            //So bai kt
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                            curColumn++;

                            //Cac cot thong ke
                            for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                            {
                                curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;
                            }

                            IVTRange sumRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);
                            sumRange.SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);
                            sumRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            curRow++;
                            curColumn = startColumn;
                        }

                        IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                        //Neu la muc thong ke chuan, to vang cot cuoi
                        if (statisticLevelReportID == 1)
                        {
                            IVTRange lastColRange = curSheet.GetRange(levelRow, lastColumn - 1, lastRow, lastColumn);
                            lastColRange.FillColor(System.Drawing.Color.Yellow);
                        }

                        curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                        curSheet.FitAllColumnsOnOnePage = true;
                    }
                    #endregion
                    #region Mon nhan xet
                    else
                    {
                        int lastColumn = 8;

                        //Tao sheet cho mon thi
                        IVTWorksheet curSheet = oBook.CopySheetToLast(judgeSheet, "II" + (lastRow).ToString());

                        //Fill tieu de
                        string additionString = String.Empty;
                        if (femalePupil && ethnicPupil)
                        {
                            additionString = " HỌC SINH NỮ DÂN TỘC";
                        }
                        else if (femalePupil)
                        {
                            additionString = " HỌC SINH NỮ";
                        }
                        else if (ethnicPupil)
                        {
                            additionString = " HỌC SINH DÂN TỘC";
                        }
                        curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                        curSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI THEO GIÁO VIÊN MÔN THI " + subject.SubjectName.ToUpper() + additionString);

                        for (int iTeacher = 0; iTeacher < listTeacherID.Count; iTeacher++)
                        {
                            int? teacherID = listTeacherID[iTeacher];
                            List<TeachingAssignmentBO> listTaOfTeacherSubject = listTAOfSubject.Where(o => o.TeacherID == teacherID).ToList();
                            for (int iTa = 0; iTa < listTaOfTeacherSubject.Count; iTa++)
                            {
                                TeachingAssignmentBO ta = listTaOfTeacherSubject[iTa];
                                ClassInfoBO ClassInfo = listClassWithTotalPupil.Where(o => o.ClassID == ta.ClassID).FirstOrDefault();
                                List<ExamPupilBO> listExamPupilOfClass = listExamPupilOfSubject.Where(o => o.ClassID == ta.ClassID).ToList();
                                List<ExamInputMarkBO> listMarkOfClass = listExamMark.Where(o => o.ClassID == ta.ClassID)
                                                                                    .Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .ToList();


                                //Lop
                                curSheet.SetCellValue(curRow, curColumn, ta.ClassName);
                                curColumn++;
                                //Giao vien chu nhiem
                                curSheet.SetCellValue(curRow, curColumn, ta.TeacherName);
                                curColumn++;
                                //Si so
                                if (ClassInfo != null)
                                {
                                    curSheet.SetCellValue(curRow, curColumn, ClassInfo.ActualNumOfPupil);
                                }
                                curColumn++;
                                //So bai kiem tra
                                curSheet.SetCellValue(curRow, curColumn, listExamPupilOfClass.Count());
                                curColumn++;

                                //Fill cac cot muc diem
                                int HSD;
                                int HSCD;
                                // Lấy số lượng diem thi Đạt
                                HSD = listMarkOfClass.Where(o => o.ExamJudgeMark != null && o.ExamJudgeMark.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                // Lấy số lượng học sinh chưa Đạt
                                HSCD = listMarkOfClass.Where(o => o.ExamJudgeMark != null && o.ExamJudgeMark.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                                curSheet.SetCellValue(curRow, curColumn, HSD);
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                                curSheet.SetCellValue(curRow, curColumn, HSCD);
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                                curSheet.GetRange(curRow, startColumn, curRow, lastColumn).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                                curRow++;
                                curColumn = startColumn;
                            }

                            //Fill dong tong
                            //Lop
                            curColumn++;
                            //GVBM
                            curSheet.SetCellValue(curRow, curColumn, "Tổng");//E
                            curSheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignCenter);
                            curColumn++;

                            //Si so
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                            curColumn++;
                            //So bai kt
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                            curColumn++;

                            //Cac cot thong ke
                            for (int iSLevel = 0; iSLevel < 2; iSLevel++)
                            {
                                curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;
                            }

                            IVTRange sumRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);
                            sumRange.SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);
                            sumRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            curRow++;
                            curColumn = startColumn;
                        }

                        IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                        globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                        curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                        curSheet.FitAllColumnsOnOnePage = true;
                    }
                    #endregion
                }
                //Xoa sheet dau tien
                markSheet.Delete();
                judgeSheet.Delete();

                return oBook.ToStream();
            }
            #endregion
            #region Cap 1
            else
            {
                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[1]);

                //Lấy ra sheet đầu tiên
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Dien du lieu vao phan tieu de
                firstSheet.SetCellValue("A2", supervisingDeptName);
                firstSheet.SetCellValue("A3", schoolName);
                firstSheet.SetCellValue("A4", examinationsName);
                firstSheet.SetCellValue("A5", strAcademicYear);
                firstSheet.SetCellValue("H4", provinceAndDate);

                //Fill muc thong ke
                //Lấy danh sách mức thống kê
                List<StatisticLevelConfig> lstStatisticLevel = getStatisticLevelForPrimary();
                int numberConfig = lstStatisticLevel.Count();

                //Lay danh sach mon thi cua ky thi
                List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
                    listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).OrderBy(o => o.OrderInSubject).ToList()
                                                        .Select(o => new ExamSubjectBO
                                                        {
                                                            SubjectID = o.SubjectID,
                                                            SubjectName = o.SubjectName
                                                        }).Distinct().ToList();
                }
                if (subjectID != 0)
                {
                    listExamSubject = listExamSubject.Where(o => o.SubjectID == subjectID).ToList();
                }

                //Lay danh sach thi sinh co thi trong ky thi
                List<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).ToList();
                if (femalePupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamPupilOfExam = listExamPupilOfExam.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }

                //Danh sach ClassID cua cac thi sinh
                //List<int> listClassID = listExamPupilOfExam.Select(o => o.ClassID).Distinct().ToList();
                List<int> listExamSubjectID = listExamSubject.Select(p => p.SubjectID).ToList();
                //Lay danh sach mon hoc cua cac lop (chi lay mon tinh diem)
                listExamSubject = (from l in SchoolSubjectBusiness.GetListSubjectMarkByListSubjectID(listExamSubjectID)
                                   select new ExamSubjectBO
                                   {
                                       SubjectName = l.SubjectName,
                                       SubjectID = l.SubjectCatID
                                   }).Distinct().ToList();

                if (listExamSubject == null || listExamSubject.Count == 0)
                {
                    throw new BusinessException("ExamResult_Report_Not_Success_Judge");
                }

                //Lay danh sach diem thi cua ky thi
                List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
                if (examinationsID != 0)
                {
                    tmpDic = new Dictionary<string, object>();
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["LastDigitSchoolID"] = partition;
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["ExaminationsID"] = examinationsID;

                    listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
                }

                if (femalePupil)
                {
                    listExamMark = listExamMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                }
                if (ethnicPupil)
                {
                    listExamMark = listExamMark.Where(o => o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
                }



                //Lay cac phan cong giang day
                tmpDic = new Dictionary<string, object>();
                tmpDic["Semester"] = exam.SemesterID;
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["AppliedLevel"] = exam.AppliedLevel;

                List<TeachingAssignmentBO> listTA = TeachingAssignmentBusiness.GetSubjectTeacher(schoolID, tmpDic).ToList();

                //Lay si so cua cac lop
                IQueryable<ClassProfile> iqClass = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearID && o.IsActive == true);
                IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"Semester",exam.SemesterID},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1}
                };
                List<ClassInfoBO> listClassWithTotalPupil = PupilOfClassBusiness.GetClassAndTotalPupil(iqClass, dicCATP).ToList();


                ////Fill du lieu
                for (int iSubject = 0; iSubject < listExamSubject.Count; iSubject++)
                {
                    ExamSubjectBO subject = listExamSubject[iSubject];

                    //Loc lai danh sach thi sinh co thi mon thi nay
                    List<ExamPupilBO> listExamPupilOfSubject = listExamPupilOfExam.Where(o => o.SubjectID == subject.SubjectID)
                                                                                    .Where(o => o.ExamPupilAbsenceID == 0).ToList();

                    //Loc lai cac phan cong giang day cua mon nay
                    List<TeachingAssignmentBO> listTAOfSubject = listTA.Where(o => o.SubjectID == subject.SubjectID)
                                                                        .OrderBy(o => o.TeacherName)
                                                                        .ThenBy(o => o.ClassOrderNumber)
                                                                        .ToList();
                    //Danh sach giao vien co day mon thi
                    List<int?> listTeacherID = listTAOfSubject.Select(o => o.TeacherID).Distinct().ToList();

                    int startRow = 10;
                    int lastRow = startRow + listTAOfSubject.Count + listTeacherID.Count - 1;
                    int curRow = startRow;
                    int startColumn = 1;
                    int curColumn = startColumn;
                    int lastColumn = 4 + numberConfig * 2;

                    //Tao sheet cho mon thi
                    IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "R" + (lastRow).ToString());

                    //Fill tieu de
                    string additionString = String.Empty;
                    if (femalePupil && ethnicPupil)
                    {
                        additionString = " HỌC SINH NỮ DÂN TỘC";
                    }
                    else if (femalePupil)
                    {
                        additionString = " HỌC SINH NỮ";
                    }
                    else if (ethnicPupil)
                    {
                        additionString = " HỌC SINH DÂN TỘC";
                    }
                    curSheet.Name = ReportUtils.StripVNSign(subject.SubjectName);
                    curSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI THEO GIÁO VIÊN MÔN THI " + subject.SubjectName.ToUpper() + additionString);

                    for (int iTeacher = 0; iTeacher < listTeacherID.Count; iTeacher++)
                    {
                        int? teacherID = listTeacherID[iTeacher];
                        List<TeachingAssignmentBO> listTaOfTeacherSubject = listTAOfSubject.Where(o => o.TeacherID == teacherID).ToList();
                        for (int iTa = 0; iTa < listTaOfTeacherSubject.Count; iTa++)
                        {
                            TeachingAssignmentBO ta = listTaOfTeacherSubject[iTa];
                            ClassInfoBO ClassInfo = listClassWithTotalPupil.Where(o => o.ClassID == ta.ClassID).FirstOrDefault();
                            List<ExamPupilBO> listExamPupilOfClass = listExamPupilOfSubject.Where(o => o.ClassID == ta.ClassID).ToList();
                            List<ExamInputMarkBO> listMarkOfClass = listExamMark.Where(o => o.ClassID == ta.ClassID)
                                                                                .Where(o => o.SubjectID == subject.SubjectID)
                                                                                .ToList();


                            //Lop
                            curSheet.SetCellValue(curRow, curColumn, ta.ClassName);
                            curColumn++;
                            //Giao vien chu nhiem
                            curSheet.SetCellValue(curRow, curColumn, ta.TeacherName);
                            curColumn++;
                            //Si so
                            if (ClassInfo != null)
                            {
                                curSheet.SetCellValue(curRow, curColumn, ClassInfo.ActualNumOfPupil);
                            }
                            curColumn++;
                            //So bai kiem tra
                            curSheet.SetCellValue(curRow, curColumn, listExamPupilOfClass.Count());
                            curColumn++;

                            //Fill cac cot muc diem
                            //Diem thi cac mon
                            int SL = 0;
                            for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                    else
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                    else
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = listMarkOfClass.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                                    }
                                }

                                curSheet.SetCellValue(curRow, curColumn, SL);//E
                                curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                                curColumn += 2;

                            }

                            curSheet.GetRange(curRow, startColumn, curRow, lastColumn).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                            curRow++;
                            curColumn = startColumn;
                        }

                        //Fill dong tong
                        //Lop
                        curColumn++;
                        //GVBM
                        curSheet.SetCellValue(curRow, curColumn, "Tổng");//E
                        curSheet.GetRange(curRow, curColumn, curRow, curColumn).SetHAlign(VTHAlign.xlHAlignCenter);
                        curColumn++;

                        //Si so
                        curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                        curColumn++;
                        //So bai kt
                        curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                        curColumn++;

                        //Cac cot thong ke
                        for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                        {
                            curSheet.SetCellValue(curRow, curColumn, "=SUM(" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curColumn) + (curRow - listTaOfTeacherSubject.Count).ToString() + ")");
                            curSheet.SetCellValue(curRow, curColumn + 1, "=IF(D" + curRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + curRow + "/" + "D" + curRow + "*100,2),0)");
                            curColumn += 2;
                        }

                        IVTRange sumRange = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);
                        sumRange.SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);
                        sumRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                        curRow++;
                        curColumn = startColumn;
                    }

                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

                    IVTRange lastColRange = curSheet.GetRange(startRow, lastColumn - 1, lastRow, lastColumn);
                    lastColRange.FillColor(System.Drawing.Color.Yellow);

                    curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                    curSheet.FitAllColumnsOnOnePage = true;

                }

                //Xoa sheet dau tien
                firstSheet.Delete();

                return oBook.ToStream();
            }
            #endregion
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamMarkByTeacherStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_GIAO_VIEN;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Report Thong ke diem thi theo  mon
        public ProcessedReport GetExamMarkBySubjectStatisticReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_MON;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamMarkBySubjectStatisticReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int statisticLevelReportID = Utils.GetInt(dic["StatisticLevelReportID"]);
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            int appliedLevel = exam.AppliedLevel;

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_MON;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = rp.TemplateName;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templatePath.Split('|')[0]);

            //Lấy ra sheet template mon tinh diem
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);

            //Dien du lieu vao phan tieu de
            sheet1.SetCellValue("A2", supervisingDeptName);
            sheet1.SetCellValue("A3", schoolName);
            sheet1.SetCellValue("A4", examinationsName);
            sheet1.SetCellValue("A5", strAcademicYear);
            sheet1.SetCellValue("M4", provinceAndDate);
            sheet1.SetCellValue("A7", "KHỐI " + educationLevelID);

            //Lay cac mon thi 
            List<ExamSubjectBO> lstSubject = (from ex in ExaminationsRepository.All
                                              join eg in ExamGroupRepository.All on ex.ExaminationsID equals eg.ExaminationsID
                                              join ep in ExamPupilRepository.All on new { eg.ExaminationsID, eg.ExamGroupID } equals new { ep.ExaminationsID, ep.ExamGroupID }
                                              join es in ExamSubjectRepository.All on new { eg.ExaminationsID, eg.ExamGroupID } equals new { es.ExaminationsID, es.ExamGroupID }
                                              join sc in SubjectCatRepository.All on es.SubjectID equals sc.SubjectCatID
                                              join ss in SchoolSubjectRepository.All on es.SubjectID equals ss.SubjectID
                                              where ex.ExaminationsID == examinationsID
                                              && ep.LastDigitSchoolID == partition
                                              && ep.SchoolID == schoolID
                                              && ep.EducationLevelID == educationLevelID
                                              && ss.AcademicYearID == academicYearID
                                              && ss.EducationLevelID == educationLevelID
                                              && ss.SchoolID == schoolID
                                              && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK
                                              select new
                                              {
                                                  SubjectID = es.SubjectID,
                                                  SubjectName = sc.DisplayName,
                                                  Order = ss.SubjectCat.OrderInSubject
                                              }).Distinct().ToList().Select(o => new ExamSubjectBO
                                              {
                                                  SubjectID = o.SubjectID,
                                                  SubjectName = o.SubjectName,
                                                  OrderInSubject = o.Order
                                              }).OrderBy(o => o.OrderInSubject).ToList();

            //Fill tieu de cac mon
            int subjectHeaderRow = 10;
            int subjectHeaderCol = 2;
            IVTRange headerTemplate = sheet2.GetRange(1, 1, 2, 4);
            for (int i = 0; i < lstSubject.Count; i++)
            {
                ExamSubjectBO subject = lstSubject[i];
                sheet1.CopyPasteSameSize(headerTemplate, subjectHeaderRow, subjectHeaderCol);
                sheet1.SetCellValue(subjectHeaderRow, subjectHeaderCol, subject.SubjectName);
                subjectHeaderCol += 4;
            }
            sheet1.GetRange(9, 2, 9, subjectHeaderCol - 1).Merge();
            sheet1.GetRange(9, 2, 9, subjectHeaderCol - 1).SetBorder(VTBorderStyle.Solid, VTBorderIndex.Around);

            //Fill muc thong ke
            //Lấy danh sách mức thống kê
            List<StatisticLevelConfig> lstStatisticLevel;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                lstStatisticLevel = this.getStatisticLevelForPrimary();
            }
            else
            {
                lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", statisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            }

            IDictionary<string, object> tmpDic;

            //Lay danh sach diem thi cua ky thi
            List<ExamInputMarkBO> listExamMark = new List<ExamInputMarkBO>();
            if (examinationsID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["AcademicYearID"] = academicYearID;
                tmpDic["LastDigitSchoolID"] = partition;
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["EducationLevelID"] = educationLevelID;

                listExamMark = this.ExamInputMarkBusiness.SearchBO(tmpDic).ToList();
            }

            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = exam.AppliedLevel;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();
            //Luu cac gia tri tong cua tung mon hoc
            IDictionary<int, List<int>> sumdic = new Dictionary<int, List<int>>();
            List<int> lstSumSubject = new List<int>();
            //Fill du lieu
            int startRow = 12;
            int startCol = 1;
            int curRow = startRow;
            int curCol = startCol;
            for (int i = 0; i < lstStatisticLevel.Count; i++)
            {
                StatisticLevelConfig slc = lstStatisticLevel[i];

                //Diem
                if (slc.MinValue.HasValue && slc.MaxValue.HasValue)
                {
                    sheet1.SetCellValue(curRow, curCol, (slc.MinValue.Value == 0 || slc.MinValue.Value == 10 ? slc.MinValue.Value.ToString() : String.Format("{0:0.0}", slc.MinValue.Value))
                        + " - " + (slc.MaxValue.Value == 0 || slc.MaxValue.Value == 10 ? slc.MaxValue.Value.ToString() : String.Format("{0:0.0}", slc.MaxValue.Value)));

                }
                curCol++;

                //fill thong ke cac mon
                for (int j = 0; j < lstSubject.Count; j++)
                {
                    ExamSubjectBO subject = lstSubject[j];
                    IEnumerable<ExamInputMarkBO> query = listExamMark.Where(o => o.SubjectID == subject.SubjectID);
                    IEnumerable<ExamInputMarkBO> queryFemale = listExamMark.Where(o => o.SubjectID == subject.SubjectID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    IEnumerable<ExamInputMarkBO> queryEthnic = listExamMark.Where(o => o.SubjectID == subject.SubjectID && o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople);
                    IEnumerable<ExamInputMarkBO> queryFemaleEthnic = listExamMark.Where(o => o.SubjectID == subject.SubjectID && o.Genre == SystemParamsInFile.GENRE_FEMALE &&
                        o.EthnicID != null && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople);

                    int count = 0;
                    int countFemale = 0;
                    int countEthnic = 0;
                    int countFemaleEthnic = 0;
                    lstSumSubject = new List<int>();
                    lstSumSubject.Add(query.Where(o => o.ActualMark.HasValue && o.ActualMark >= 0 && o.ActualMark <= 10).Count());// tong so hoc sinh thuong
                    lstSumSubject.Add(queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark >= 0 && o.ActualMark <= 10).Count());// tong so hoc nu
                    lstSumSubject.Add(queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= 0 && o.ActualMark <= 10).Count());// tong so hoc sinh dan toc
                    lstSumSubject.Add(queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= 0 && o.ActualMark <= 10).Count());// tong so hoc sinh dan toc
                    if (!sumdic.Keys.Contains(subject.SubjectID))
                    {
                        sumdic.Add(subject.SubjectID, lstSumSubject);
                    }
                    if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                    {
                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                        }
                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                        }
                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                        }
                        else
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark >= slc.MinValue).Count();
                        }
                    }
                    else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                    {
                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark <= slc.MaxValue).Count();
                        }
                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark < slc.MaxValue).Count();
                        }
                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue && o.ActualMark == slc.MaxValue).Count();
                        }
                        else
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark > slc.MinValue).Count();
                        }
                    }
                    else
                    {
                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark <= slc.MaxValue).Count();
                        }
                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark < slc.MaxValue).Count();
                        }
                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                        {
                            count = query.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                            countFemale = queryFemale.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                            countEthnic = queryEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();
                            countFemaleEthnic = queryFemaleEthnic.Where(o => o.ActualMark.HasValue && o.ActualMark == slc.MaxValue).Count();

                        }
                    }



                    //Tong
                    sheet1.SetCellValue(curRow, curCol, count);
                    curCol++;

                    //Nu
                    sheet1.SetCellValue(curRow, curCol, countFemale);
                    curCol++;

                    //dan toc
                    sheet1.SetCellValue(curRow, curCol, countEthnic);
                    curCol++;

                    //nu dan toc
                    sheet1.SetCellValue(curRow, curCol, countFemaleEthnic);
                    curCol++;
                }

                curRow++;
                curCol = startCol;
            }

            //fill dong tong so
            sheet1.SetCellValue(curRow, curCol, "TS");
            curCol++;

            /*for (int i = 0; i < lstSubject.Count*4; i++)
            {
                sheet1.SetCellValue(curRow, curCol, "=SUM(" + UtilsBusiness.GetExcelColumnName(curCol) + (startRow).ToString() + ":" + UtilsBusiness.GetExcelColumnName(curCol) + (curRow -1).ToString() + ")");
                curCol++;

            }*/

            //Dien so luong tong
            int totalCol = 1 + 4 * lstSubject.Count;
            List<int> lstValTotal = new List<int>();
            for (int i = 0; i < lstSubject.Count; i++)
            {
                lstValTotal = sumdic[lstSubject[i].SubjectID];
                if (lstValTotal == null)
                {
                    continue;
                }
                sheet1.SetCellValue(curRow, 2 + i * 4, lstValTotal[0]);//Tong so 
                sheet1.SetCellValue(curRow, 3 + i * 4, lstValTotal[1]);//Tong so nu
                sheet1.SetCellValue(curRow, 4 + i * 4, lstValTotal[2]);//Tong so DT
                sheet1.SetCellValue(curRow, 5 + i * 4, lstValTotal[3]);//Tong so DT

            }

            sheet1.GetRange(curRow, startCol, curRow, totalCol).SetFontStyle(true, null, false, null, false, false);
            sheet1.GetRange(startRow, startCol, curRow, totalCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

            curRow += 2;
            curCol -= 7;
            if (curCol <= 0)
            {
                curCol = 1;
            }
            sheet1.SetCellValue(curRow, curCol, "Người lập");
            sheet1.GetRange(curRow, curCol, curRow, curCol).SetFontStyle(true, null, false, null, false, false);
            sheet1.GetRange(curRow, curCol, curRow, curCol + 6).Merge();


            //Xoa sheet dau tien
            sheet2.Delete();
            return oBook.ToStream();
        }

        public ProcessedReport InsertExamMarkBySubjectStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_MON;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        private List<StatisticLevelConfig> getStatisticLevelForPrimary()
        {
            List<StatisticLevelConfig> lstStatisticLevel = new List<StatisticLevelConfig>();

            StatisticLevelConfig config;
            //Gioi
            config = new StatisticLevelConfig();
            config.MaxValue = 10;
            config.MinValue = 9;
            config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN;
            config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
            lstStatisticLevel.Add(config);

            //Kha
            config = new StatisticLevelConfig();
            config.MaxValue = 8;
            config.MinValue = 7;
            config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN;
            config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
            lstStatisticLevel.Add(config);

            //TB
            config = new StatisticLevelConfig();
            config.MaxValue = 6;
            config.MinValue = 5;
            config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN;
            config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
            lstStatisticLevel.Add(config);

            //Yeu
            config = new StatisticLevelConfig();
            config.MaxValue = 5;
            config.MinValue = 0;
            config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_SIGN;
            config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
            lstStatisticLevel.Add(config);

            //TB tro len
            config = new StatisticLevelConfig();
            config.MaxValue = 10;
            config.MinValue = 5;
            config.SignMax = SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN;
            config.SignMin = SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN;
            lstStatisticLevel.Add(config);


            return lstStatisticLevel;
        }

        #region ImportInputMark

        // Danh sach so phach (co thong tin ma tui phach, ma phong thi) cua ky thi, nhom thi va mon thi
        public List<ExamDetachableBagBO> GetListDetachableContains(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examCandenceBagID = Utils.GetLong(search, "ExamCandenceBagID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            int partitionId = UtilsBusiness.GetPartionId(schoolID);

            IQueryable<ExamDetachableBagBO> query = from ec in ExamCandenceBagRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID && o.SubjectID == subjectID)
                                                    join ed in ExamDetachableBagRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID && o.SubjectID == subjectID) on ec.ExamCandenceBagID equals ed.ExamCandenceBagID
                                                    join ep in ExamPupilRepository.All.Where(o => o.SchoolID == schoolID && o.LastDigitSchoolID == partitionId && o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID) on ed.ExamPupilID equals ep.ExamPupilID
                                                    join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID) on ep.ExamRoomID equals er.ExamRoomID
                                                    select new ExamDetachableBagBO
                                                    {
                                                        ExamDetachableBagID = ed.ExamDetachableBagID,
                                                        ExamDetachableBagCode = ed.ExamDetachableBagCode,
                                                        ExamCandenceBagID = ec.ExamCandenceBagID,
                                                        ExamCandenceBagCode = ec.ExamCandenceBagCode,
                                                        ExamRoomID = er.ExamRoomID,
                                                        ExamRoomCode = er.ExamRoomCode
                                                    };

            if (examCandenceBagID != 0)
            {
                query = query.Where(o => o.ExamCandenceBagID == examCandenceBagID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }

            return query.ToList();
        }

        // Danh sach hoc sinh tham gia vao ki thi, nhom thi va mon thi dang xet: Ma hoc sinh, so bao danh
        public List<ExamPupilInfoBO> GetListPupilContains(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            List<ExamPupilInfoBO> query = (from ep in ExamPupilRepository.All.Where(c => c.LastDigitSchoolID == partitionId)
                                           join poc in PupilOfClassRepository.All on ep.PupilID equals poc.PupilID
                                           join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                           join er in ExamRoomRepository.All on ep.ExamRoomID equals er.ExamRoomID
                                           where pp.IsActive == true && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                           && poc.AcademicYearID == academicYearID && poc.SchoolID == schoolID && ep.ExaminationsID == examinationsID
                                           && ep.ExamGroupID == examGroupID && (ep.ExamRoomID == examRoomID || examRoomID == 0)
                                           select new ExamPupilInfoBO
                                           {
                                               ExamPupilID = ep.ExamPupilID,
                                               PupilCode = pp.PupilCode,
                                               ExamineeCode = ep.ExamineeNumber,
                                               PupilID = ep.PupilID,
                                               ClassName = poc.ClassProfile.DisplayName,
                                               ExamRoomID = er.ExamRoomID,
                                               ExamRoomCode = er.ExamRoomCode

                                           }).ToList();
            return query;
        }

        public void ImportInputMark(IDictionary<string, object> info, List<ExamInputMarkBO> listInput, int markInputType, ref int successCount)
        {
            int academicYearID = Utils.GetInt(info, "AcademicYearID");
            int schoolID = Utils.GetInt(info, "SchoolID");
            long examinationsID = Utils.GetLong(info, "ExaminationsID");
            long examGroupID = Utils.GetLong(info, "ExamGroupID");
            int subjectID = Utils.GetInt(info, "SubjectID");
            int appliedLevel = Utils.GetInt(info, "AppliedLevel");

            // Kiem tra mon hoc la mon nhan xet hay mon tinh diem
            int isCommenting = 0;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.Search(info).OrderBy(o => o.EducationLevelID).FirstOrDefault();
            if (schoolSubject != null && schoolSubject.IsCommenting.HasValue && schoolSubject.IsCommenting.Value == 1)
            {
                isCommenting = 1;
            }

            List<ExamInputMarkBO> listInputMark = null;
            IQueryable<ExamInputMark> listData = this.Search(info);
            ExamInputMark import = null;
            ExamInputMarkBO tempInput = null; // Bien tam chua du lieu moi
            ExamInputMarkBO temp = null; // Bien tam chua du lieu hien tai

            // Lay danh sach vao diem hien tai
            if (markInputType == SystemParamsInFile.MARK_INPUT_TYPE_DETACHABLE_BAG)
            {
                #region Import theo so phach
                listInputMark = this.GetListInputMarkCandenceBag(info);
                // Duyet danh sach, them hoac cap nhat diem thi theo ket qua import
                for (int i = 0; i < listInput.Count; i++)
                {
                    tempInput = listInput[i];
                    temp = listInputMark.Where(o => o.ExamDetachableBagCode.ToUpper() == tempInput.ExamDetachableBagCode.ToUpper() && o.ExamCandenceBagCode.ToUpper() == tempInput.ExamCandenceBagCode.ToUpper()).FirstOrDefault();
                    if (temp != null && temp.isAbsence == 0) // Co the vao diem
                    {
                        import = listData.Where(o => o.PupilID == temp.PupilID && o.ClassID == temp.ClassID).FirstOrDefault();
                        if (import != null) // Cap nhat
                        {
                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }
                            temp.UpdateTime = DateTime.Now;
                            this.Update(import);
                        }
                        else // Thêm moi
                        {
                            import = new ExamInputMark();
                            import.ExamInputMarkID = this.GetNextSeq<long>();
                            import.ExaminationsID = examinationsID;
                            import.ExamGroupID = examGroupID;
                            import.SubjectID = subjectID;
                            import.PupilID = temp.PupilID;
                            import.ClassID = temp.ClassID;
                            import.AcademicYearID = academicYearID;
                            import.SchoolID = schoolID;
                            import.LastDigitSchoolID = schoolID % 100;
                            import.CreateTime = DateTime.Now;

                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }

                            this.Insert(import);
                        }
                        successCount++;
                    }
                }
                #endregion
            }
            else if (markInputType == SystemParamsInFile.MARK_INPUT_TYPE_EXAM_ROOM)
            {
                #region Import theo phong thi
                listInputMark = this.GetListInputMarkExamRoom(info);

                // Duyet danh sach, them hoac cap nhat diem thi theo ket qua import
                for (int i = 0; i < listInput.Count; i++)
                {
                    tempInput = listInput[i];
                    temp = listInputMark.Where(o => o.ExamDetachableBagCode.ToUpper() == tempInput.ExamDetachableBagCode.ToUpper() && o.ExamRoomCode.ToUpper() == tempInput.ExamRoomCode.ToUpper()).FirstOrDefault();
                    if (temp != null && temp.isAbsence == 0) // Co the vao diem
                    {
                        import = listData.Where(o => o.PupilID == temp.PupilID && o.ClassID == temp.ClassID).FirstOrDefault();
                        if (import != null) // Cap nhat
                        {
                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }
                            temp.UpdateTime = DateTime.Now;
                            this.Update(import);
                        }
                        else // Thêm moi
                        {
                            import = new ExamInputMark();
                            import.ExamInputMarkID = this.GetNextSeq<long>();
                            import.ExaminationsID = examinationsID;
                            import.ExamGroupID = examGroupID;
                            import.SubjectID = subjectID;
                            import.PupilID = temp.PupilID;
                            import.ClassID = temp.ClassID;
                            import.AcademicYearID = academicYearID;
                            import.SchoolID = schoolID;
                            import.LastDigitSchoolID = schoolID % 100;
                            import.CreateTime = DateTime.Now;

                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }

                            this.Insert(import);
                        }
                        successCount++;
                    }
                }
                #endregion
            }
            else if (markInputType == SystemParamsInFile.MARK_INPUT_TYPE_PUPIL_CODE)
            {
                #region Import theo ma hoc sinh
                listInputMark = this.GetListInputMarkExamRoom(info);

                // Duyet danh sach, them hoac cap nhat diem thi theo ket qua import
                for (int i = 0; i < listInput.Count; i++)
                {
                    tempInput = listInput[i];
                    temp = listInputMark.Where(o => o.PupilCode.ToUpper() == tempInput.PupilCode.ToUpper() && o.ExamRoomCode.ToUpper() == tempInput.ExamRoomCode.ToUpper()).FirstOrDefault();
                    if (temp != null && temp.isAbsence == 0) // Co the vao diem
                    {
                        import = listData.Where(o => o.PupilID == temp.PupilID && o.ClassID == temp.ClassID).FirstOrDefault();
                        if (import != null) // Cap nhat
                        {
                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }
                            temp.UpdateTime = DateTime.Now;
                            this.Update(import);
                        }
                        else // Thêm moi
                        {
                            import = new ExamInputMark();
                            import.ExamInputMarkID = this.GetNextSeq<long>();
                            import.ExaminationsID = examinationsID;
                            import.ExamGroupID = examGroupID;
                            import.SubjectID = subjectID;
                            import.PupilID = temp.PupilID;
                            import.ClassID = temp.ClassID;
                            import.AcademicYearID = academicYearID;
                            import.SchoolID = schoolID;
                            import.LastDigitSchoolID = schoolID % 100;
                            import.CreateTime = DateTime.Now;

                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }

                            this.Insert(import);
                        }
                        successCount++;
                    }
                }
                #endregion
            }
            else
            {
                #region Import theo SBD
                listInputMark = this.GetListInputMarkExamRoom(info);

                // Duyet danh sach, them hoac cap nhat diem thi theo ket qua import
                for (int i = 0; i < listInput.Count; i++)
                {
                    tempInput = listInput[i];
                    temp = listInputMark.Where(o => o.ExamineeNumber.ToUpper() == tempInput.ExamineeNumber.ToUpper() && o.ExamRoomCode.ToUpper() == tempInput.ExamRoomCode.ToUpper()).FirstOrDefault();
                    if (temp != null && temp.isAbsence == 0) // Co the vao diem
                    {
                        import = listData.Where(o => o.PupilID == temp.PupilID && o.ClassID == temp.ClassID).FirstOrDefault();
                        if (import != null) // Cap nhat
                        {
                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }
                            temp.UpdateTime = DateTime.Now;
                            this.Update(import);
                        }
                        else // Thêm moi
                        {
                            import = new ExamInputMark();
                            import.ExamInputMarkID = this.GetNextSeq<long>();
                            import.ExaminationsID = examinationsID;
                            import.ExamGroupID = examGroupID;
                            import.SubjectID = subjectID;
                            import.PupilID = temp.PupilID;
                            import.ClassID = temp.ClassID;
                            import.AcademicYearID = academicYearID;
                            import.SchoolID = schoolID;
                            import.LastDigitSchoolID = schoolID % 100;
                            import.CreateTime = DateTime.Now;

                            decimal examMark = 0, actualMark = 0;
                            if (isCommenting == 1)
                            {
                                import.ExamJudgeMark = tempInput.ExamMarkInput;
                            }
                            else
                            {
                                bool mark = decimal.TryParse(tempInput.ExamMarkInput.Replace(".", ","), out examMark);
                                if (mark)
                                {
                                    import.ExamMark = examMark;
                                    // Tính và gán điểm thực tế
                                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 0, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        actualMark = Math.Round((examMark * temp.penalizedMark / 100), 1, MidpointRounding.AwayFromZero);
                                    }
                                    import.ActualMark = actualMark;
                                }
                                else
                                {
                                    import.ExamMark = null;
                                    import.ActualMark = null;
                                }
                            }

                            this.Insert(import);
                        }
                        successCount++;
                    }
                }
                #endregion
            }

            this.Save();
        }

        #endregion

        public bool CheckExamSubjectBySubjectIDReportPrimary(long examinationsID, int schoolID, int academicYearID, int partition, int subjectID)
        {
            IQueryable<ExamPupilBO> listExamPupilOfExam = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition);
            //Danh sach lop hoc dau tien voi mon duoc chon
            ExamPupilBO examPupilObj = listExamPupilOfExam.Where(p => p.SubjectID == subjectID).OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassOrder).ThenBy(p => p.ClassName).FirstOrDefault();
            // Kiem tra mon nay la mon nhan xet hay tinh diem trong ClassSubject            
            if (examPupilObj != null)
            {
                ClassSubject classSubjectBO = ClassSubjectBusiness.All.Where(p => p.SubjectID == subjectID
                                                                             && p.ClassID == examPupilObj.ClassID
                                                                             && p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK
                                                                             && p.Last2digitNumberSchool == partition).FirstOrDefault();
                if (classSubjectBO != null)
                {
                    return true;
                }
            }
            return false;
        }
        #region Report SumamnyMark
        public ProcessedReport InsertSummanyMarkReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_SUMMANY_MARK;
            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);
            var appliedLevel = Utils.GetNullableInt(dic, "AppliedLevelID");
            //Tạo tên file
            string outputNamePattern = appliedLevel != null && appliedLevel == 3 ? "HS_THPT_KetQuaThiLaiRenLuyenLai.xls" : reportDef.OutputNamePattern;

            //string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ////int subjectID = Utils.GetInt(dic, "SubjectID");
            //SubjectCat sc = SubjectCatBusiness.Find(dic["SubjectID"]);
            //string subjectName = sc != null ? sc.DisplayName : String.Empty;

            //outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));
            //outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        public ProcessedReport GetSummanyMarkReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_SUMMANY_MARK;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion
    }
}