﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SkillTitleBusiness
    {
        public void Delete(int UserAccountID, int SkillTitleID, int SchoolID)
        {
            SkillTitle ap = this.Find(SkillTitleID);
            if (!UserAccountBusiness.IsSchoolAdmin(UserAccountID))
            {
                throw new BusinessException("Validate_UserAccountID");
            }
            bool SkillTitleCompatible = new SkillTitleRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "SkillTitle",
                new Dictionary<string, object>()
                {
                    {"SkillTitleID",SkillTitleID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!SkillTitleCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            ap.IsActive = false;
            base.Update(ap);
        }

        private IQueryable<SkillTitleBO> Search(IDictionary<string, object> SearchInfo)
        {
            //  Thực hiện tìm kiếm trong bảng SkillTitle st join SkillTitleClass stc on st.SkillTitleID = stc.SkillTitleID

            //AppliedLevel: default = 0, 0 => tìm kiếm All, AppliedLevel != 0, tìm kiếm theo EducationLevel(ClassProfile(apc.ClassID).EducationLevelID).Grade = AppliedLevel

            //ClassID: default = 0, 0 => tìm kiếm All, AppliedLevel != 0, tìm kiếm theo apc.ClassID = ClassID
            int AppliedLevel = Utils.GetShort(SearchInfo, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int? ClassID = Utils.GetNullableInt(SearchInfo, "ClassID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            System.DateTime? FromDate = Utils.GetDateTime(SearchInfo, "FromDate");
            System.DateTime? ToDate = Utils.GetDateTime(SearchInfo, "ToDate");
            System.DateTime? CreatedDate = Utils.GetDateTime(SearchInfo, "CreatedDate");
            System.DateTime? ModifiedDate = Utils.GetDateTime(SearchInfo, "ModifiedDate");
            string Subject = Utils.GetString(SearchInfo, "Subject");
            string Description = Utils.GetString(SearchInfo, "Description");
            bool? IsActive = Utils.GetNullableBool(SearchInfo, "IsActive");
            IQueryable<SkillTitleBO> query;
            if (EducationLevelID != 0)
            {
                if (ClassID == null)
                {
                    query = from st in SkillTitleRepository.All
                            join stc in SkillTitleClassRepository.All.Where(o => (o.ClassProfile.EducationLevel.Grade == AppliedLevel && o.ClassProfile.EducationLevelID == EducationLevelID))
                            on st.SkillTitleID equals stc.SkillTitleID
                            select new SkillTitleBO
                            {
                                SchoolID = st.SchoolID,
                                AcademicYearID = st.AcademicYearID,
                                SkillTitleID = st.SkillTitleID,
                                EducationLevelID = stc.ClassProfile.EducationLevelID,
                                FromDate = st.FromDate,
                                ToDate = st.ToDate,
                                //SkillTitle = st.Title,
                                Description = st.Description,
                                AppliedLevel = stc.ClassProfile.EducationLevel.Grade,
                                IsActive = st.IsActive,
                                CreatedDate = st.CreatedDate,
                                ModifiedDate = st.ModifiedDate
                            };
                }
                else
                {
                    query = from st in SkillTitleRepository.All
                            join stc in SkillTitleClassRepository.All.Where(o => (o.ClassID == ClassID && o.ClassProfile.EducationLevel.Grade == AppliedLevel && o.ClassProfile.EducationLevelID == EducationLevelID))
                            on st.SkillTitleID equals stc.SkillTitleID
                            select new SkillTitleBO
                            {
                                SchoolID = st.SchoolID,
                                AcademicYearID = st.AcademicYearID,
                                SkillTitleID = st.SkillTitleID,
                                EducationLevelID = stc.ClassProfile.EducationLevelID,
                                ClassID = stc.ClassID,
                                ClassName = stc.ClassProfile.DisplayName,
                                FromDate = st.FromDate,
                                ToDate = st.ToDate,
                               // SkillTitle = st.Title,
                                Description = st.Description,
                                AppliedLevel = stc.ClassProfile.EducationLevel.Grade,
                                IsActive = st.IsActive,
                                CreatedDate = st.CreatedDate,
                                ModifiedDate = st.ModifiedDate
                            };
                }
            }
            else
            {
                query = from st in SkillTitleRepository.All
                        join stc in SkillTitleClassRepository.All.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel) on st.SkillTitleID equals stc.SkillTitleID into j
                        from j2 in j.DefaultIfEmpty()
                        select new SkillTitleBO
                        {
                            SchoolID = st.SchoolID,
                            AcademicYearID = st.AcademicYearID,
                            SkillTitleID = st.SkillTitleID,
                            EducationLevelID = j2.ClassProfile.EducationLevelID,
                            ClassID = j2.ClassID,
                            ClassName = j2.ClassProfile.DisplayName,
                            FromDate = st.FromDate,
                            ToDate = st.ToDate,
                           // SkillTitle = st.Title,
                            Description = st.Description,
                            AppliedLevel = j2.ClassProfile.EducationLevel.Grade,
                            IsActive = st.IsActive,
                            CreatedDate = st.CreatedDate,
                            ModifiedDate = st.ModifiedDate
                        };
            }

            if (IsActive.HasValue)
            {
                query = query.Where(o => o.IsActive == IsActive);
            }
            if (FromDate.HasValue)
            {
                query = query.Where(o => o.FromDate >= FromDate);
            }
            if (ToDate.HasValue)
            {
                query = query.Where(o => o.ToDate <= ToDate);
            }

            if (CreatedDate.HasValue)
            {
                query = query.Where(o => o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month >= CreatedDate.Value.Month && o.CreatedDate.Value.Year >= CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                query = query.Where(o => o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year >= ModifiedDate.Value.Year);
            }
            if (Subject.Trim().Length != 0) query = query.Where(o => o.SkillTitle.Contains(Subject.ToLower()));
            if (Description.Trim().Length != 0) query = query.Where(o => o.Description.Contains(Description.ToLower()));
            if (SchoolID > 0)
                query = query.Where(o => o.SchoolID == SchoolID);
            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            return query;
        }

        public IQueryable<SkillTitleBO> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0)
                dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
        
    }
}