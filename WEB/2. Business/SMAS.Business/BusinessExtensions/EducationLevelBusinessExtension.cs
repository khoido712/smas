/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class EducationLevelBusiness
    {

        /// <summary>
        /// Tìm kiếm khối học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="IsActive">Biến kiểm tra</param>
        /// <returns>IQueryable<EducationLevel></returns>
        public IQueryable<EducationLevel> Search(IDictionary<string, object> dic)
        {
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            int? Grade = Utils.GetShort(dic, "Grade");
            IQueryable<EducationLevel> lsEducationLevel = EducationLevelRepository.All;

            if (IsActive.HasValue)
                lsEducationLevel = lsEducationLevel.Where(sub => sub.IsActive == IsActive);
            if (Grade!=0)
                lsEducationLevel = lsEducationLevel.Where(sub => sub.Grade == Grade);
            return lsEducationLevel;
        }

        public IQueryable<EducationLevel> GetByGrade(int appliedLevel)
        {
            return EducationLevelRepository.All.Where(ed => ed.Grade == appliedLevel && ed.IsActive == true);
        }


    }
}
