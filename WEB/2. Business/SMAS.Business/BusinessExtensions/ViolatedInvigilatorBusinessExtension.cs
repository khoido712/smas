﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  hieund
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.Business
{
    public partial class ViolatedInvigilatorBusiness
    {
        public List<ViolatedInvigilator> InsertAll(int SchoolID, int AppliedLevel, string ViolationDetail, string DisciplinedForm, List<int> ListInvigilatorAsignmentID)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            foreach (int invigilatorAsignmentID in ListInvigilatorAsignmentID)
            {
                InvigilatorAssignmentBusiness.CheckAvailable(invigilatorAsignmentID, "Invigilator_Label_CandidateID");

                InvigilatorAssignment invigilatorAssignment = InvigilatorAssignmentBusiness.Find(invigilatorAsignmentID);

                //Kiem tra tuong thich Examination
                bool examinationCompatible = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , invigilatorAssignment.Invigilator.ExaminationID },
                                                {"SchoolID", SchoolID}
                                            }, null);
                if (!examinationCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Kiem tra tuong thich EducationLevel
                bool educationLevelCompatible = new CandidateRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                                , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , invigilatorAssignment.ExaminationRoom.ExaminationSubject.EducationLevelID },
                                                {"Grade", AppliedLevel}
                                            }, null);

                if (!educationLevelCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (invigilatorAssignment.Invigilator.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                    && invigilatorAssignment.Invigilator.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
                    throw new BusinessException("ViolatedInvigilator_Validate_CurrentStage");

                if (!AcademicYearBusiness.IsCurrentYear(invigilatorAssignment.Invigilator.Examination.AcademicYearID))
                    throw new BusinessException("Common_IsCurrentYear_Err");
            }

            Utils.ValidateMaxLength(ViolationDetail, 160, "Common_Validate_MaxLength");

            #endregion

            List<ViolatedInvigilator> lstViolatedInvigilator = new List<ViolatedInvigilator>();
            foreach (int invigilatorAsignmentID in ListInvigilatorAsignmentID)
            {
                InvigilatorAssignment invigilatorAssignment = InvigilatorAssignmentBusiness.Find(invigilatorAsignmentID);
                ViolatedInvigilator vi = new ViolatedInvigilator();
                vi.EducationLevelID = invigilatorAssignment.ExaminationRoom.ExaminationSubject.EducationLevelID;
                vi.ExaminationID = invigilatorAssignment.Invigilator.ExaminationID;
                vi.InvigilatorID = invigilatorAssignment.InvigilatorID;
                vi.RoomID = invigilatorAssignment.RoomID;
                vi.SubjectID = invigilatorAssignment.ExaminationRoom.ExaminationSubjectID;
                vi.TeacherID = invigilatorAssignment.Invigilator.TeacherID;
                vi.ViolationDetail = ViolationDetail;
                vi.DisciplinedForm = DisciplinedForm;
                this.Insert(vi);
                lstViolatedInvigilator.Add(vi);
            }
            return lstViolatedInvigilator;
        }

        public ViolatedInvigilator Update(int ViolatedInvigilatorID, int SchoolID, int AppliedLevel, string ViolationDetail, string DisciplinedForm)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            this.CheckAvailable(ViolatedInvigilatorID, "Invigilator_Label_CandidateID");

            ViolatedInvigilator violatedInvigilator = this.Find(ViolatedInvigilatorID);

            //Kiem tra tuong thich Examination
            bool examinationCompatible = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , violatedInvigilator.ExaminationID},
                                                {"SchoolID", SchoolID}
                                            }, null);
            if (!examinationCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra tuong thich EducationLevel
            bool educationLevelCompatible = new CandidateRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                            , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , violatedInvigilator.EducationLevelID },
                                                {"Grade", AppliedLevel}
                                            }, null);

            if (!educationLevelCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (violatedInvigilator.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                    && violatedInvigilator.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
                throw new BusinessException("ViolatedInvigilator_Validate_CurrentStage");

            if (!AcademicYearBusiness.IsCurrentYear(violatedInvigilator.Examination.AcademicYearID))
                throw new BusinessException("Common_IsCurrentYear_Err");

            Utils.ValidateMaxLength(ViolationDetail, 160, "Common_Validate_MaxLength");

            #endregion

            violatedInvigilator.DisciplinedForm = DisciplinedForm;
            violatedInvigilator.ViolationDetail = ViolationDetail;

            return violatedInvigilator;
        }

        public void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListViolatedInvigilatorID)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            foreach (int ViolatedInvigilatorID in ListViolatedInvigilatorID)
            {
                this.CheckAvailable(ViolatedInvigilatorID, "ViolatedInvigilator_Label_ViolatedInvigilatorID");

                var violatedInvigilator = this.Find(ViolatedInvigilatorID);

                //Kiem tra tuong thich Examination
                bool examinationCompatible = ViolatedInvigilatorRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , violatedInvigilator.ExaminationID },
                                                {"SchoolID", SchoolID}
                                            }, null);
                if (!examinationCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Kiem tra tuong thich EducationLevel
                bool educationLevelCompatible = new CandidateRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                                , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , violatedInvigilator.EducationLevelID },
                                                {"Grade", AppliedLevel}
                                            }, null);

                if (!educationLevelCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (violatedInvigilator.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                    && violatedInvigilator.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
                    throw new BusinessException("ViolatedInvigilator_Validate_CurrentStage");

                if (!AcademicYearBusiness.IsCurrentYear(violatedInvigilator.Examination.AcademicYearID))
                    throw new BusinessException("Common_IsCurrentYear_Err");
            }
            #endregion

            foreach (int ViolatedInvigilatorID in ListViolatedInvigilatorID)
            {
                this.Delete(ViolatedInvigilatorID);
            }
        }

        private IQueryable<ViolatedInvigilator> Search(IDictionary<string, object> dic)
        {
            string TeacherName = Utils.GetString(dic, "TeacherName");
            int? ViolatedInvigilatorID = Utils.GetNullableInt(dic, "ViolatedInvigilatorID");
            int? ExaminationID = Utils.GetNullableInt(dic, "ExaminationID");
            int? InvigilatorID = Utils.GetNullableInt(dic, "InvigilatorID");
            int? ExaminationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");
            int? EducationLevelID = Utils.GetNullableShort(dic, "EducationLevelID");
            int? TeacherID = Utils.GetNullableInt(dic, "TeacherID");
            int? RoomID = Utils.GetNullableInt(dic, "RoomID");
            string DisciplinedForm = Utils.GetString(dic, "DisciplinedForm");
            string ViolationDetail = Utils.GetString(dic, "ViolationDetail");
            int? AcademicYearID = Utils.GetNullableInt(dic, "AcademicYearID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? AppliedLevel = Utils.GetNullableInt(dic, "AppliedLevel");
            int? SchoolFacultyID = Utils.GetNullableInt(dic, "SchoolFacultyID");

            var listViolatedInvigilator = this.All;

            if (!string.IsNullOrEmpty(TeacherName))
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.Employee.FullName.ToLower().Contains(TeacherName.ToLower()));
            if (ViolatedInvigilatorID.HasValue && ViolatedInvigilatorID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.ViolatedInvigilatorID == ViolatedInvigilatorID);
            if (ExaminationID.HasValue && ExaminationID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.ExaminationID == ExaminationID);
            if (InvigilatorID.HasValue && InvigilatorID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.InvigilatorID == InvigilatorID);
            if (ExaminationSubjectID.HasValue && ExaminationSubjectID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.SubjectID == ExaminationSubjectID);
            if (EducationLevelID.HasValue && EducationLevelID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.EducationLevelID == EducationLevelID);
            if (TeacherID.HasValue && TeacherID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.TeacherID == TeacherID);
            if (RoomID.HasValue && RoomID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.RoomID == RoomID);
            if (!string.IsNullOrEmpty(DisciplinedForm))
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.DisciplinedForm.ToLower().Contains(DisciplinedForm.ToLower()));
            if (!string.IsNullOrEmpty(ViolationDetail))
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.ViolationDetail.ToLower().Contains(ViolationDetail.ToLower()));
            if (AcademicYearID.HasValue && AcademicYearID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.Examination.AcademicYearID == AcademicYearID);
            if (SchoolID.HasValue && SchoolID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.Examination.SchoolID == SchoolID);
            if (AppliedLevel.HasValue && AppliedLevel.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.EducationLevel.Grade == AppliedLevel);
            if (SchoolFacultyID.HasValue && SchoolFacultyID.Value > 0)
                listViolatedInvigilator = listViolatedInvigilator.Where(u => u.Employee.SchoolFacultyID == SchoolFacultyID.Value);
            return listViolatedInvigilator;
        }

        public IQueryable<ViolatedInvigilator> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0 || dic == null) return null;
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        public Stream CreateViolatedInvigilatorReport(IDictionary<string, object> dic, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THI_GIAMTHIVPQC);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? educationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            int? examinationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");

            List<ViolatedInvigilator> listViolatedInvigilator = Search(dic).ToList().
                OrderBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName).ToList();

            IVTWorksheet templateSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet,null);//copy toan bo

            #region tao khung
            //IVTRange topRow = templateSheet.GetRange("A17", "I17");
            //IVTRange midRow = templateSheet.GetRange("A18", "I18");
            //IVTRange botRow = templateSheet.GetRange("A19", "I19");
            int count = 0;
            int StartRow = 11;
            int LastColumn = 9;
            for (int i = 0; i < listViolatedInvigilator.Count; i++)
            {
                count++;
                if (count % 5 == 0)
                {
                    //sheet.CopyPaste(botRow, i + 11, 1, true);
                //else if (i % 5 == 4) sheet.CopyPaste(botRow, i + 16, 1, true);
                    sheet.GetRange((StartRow + i), 1, (StartRow + i), LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                    sheet.GetRange((StartRow + i), 1, (StartRow + i), LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange((StartRow + i), 1, (StartRow + i), LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                }
                else
                {
                    //sheet.CopyPaste(midRow, i + 11, 1, true);
                    sheet.GetRange((StartRow + i), 1, (StartRow + i), LastColumn).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    sheet.GetRange((StartRow + i), 1, (StartRow + i), LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                    sheet.GetRange((StartRow + i), 1, (StartRow + i), LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                }
                if (count == listViolatedInvigilator.Count)
            {
                    sheet.GetRange(listViolatedInvigilator.Count + StartRow - 1, 1, listViolatedInvigilator.Count + StartRow - 1, LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                    sheet.GetRange(listViolatedInvigilator.Count + StartRow - 1, 1, listViolatedInvigilator.Count + StartRow - 1, LastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                }
                sheet.GetRange("A" + (StartRow + i), "A" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("B" + (StartRow + i), "B" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("C" + (StartRow + i), "C" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("D" + (StartRow + i), "D" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("E" + (StartRow + i), "E" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("F" + (StartRow + i), "F" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("G" + (StartRow + i), "G" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("H" + (StartRow + i), "H" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange("I" + (StartRow + i), "I" + (StartRow + i)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examination exam = ExaminationBusiness.Find(examinationID);
            AcademicYear acaYear = AcademicYearBusiness.Find(academicYearID);

            dicVarable["SupervisingDeptName"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["ExaminationName"] = examinationID > 0 && exam != null ? exam.Title : "Tất cả";
            dicVarable["AcademicYearTitle"] = acaYear != null ? (acaYear.Year + " - " + (acaYear.Year + 1)) : "Tất cả";

            int index = 1;
            List<object> listData = new List<object>();
            listViolatedInvigilator.ForEach(u =>
            {
                ViolatedInvigilatorBO obj = new ViolatedInvigilatorBO();
                Dictionary<string, object> inDic = new Dictionary<string, object>();
                inDic.Add("Index", index++);
                inDic.Add("FullName", u.Employee.FullName);
                inDic.Add("BirthDate", u.Employee.BirthDate.HasValue ? u.Employee.BirthDate.Value.ToShortDateString() : "");
                //inDic.Add("Genre", u.Employee.Genre);
                inDic.Add("Genre", u.Employee.Genre ? "Nam" : "Nữ");
                inDic.Add("EducationLevelName", u.EducationLevel.Resolution);
                inDic.Add("SubjectName", u.ExaminationSubject.SubjectCat.SubjectName);
                inDic.Add("ExaminationRoomName", u.ExaminationRoom.RoomTitle);
                inDic.Add("ViolateDetail", u.ViolationDetail);
                inDic.Add("Process", u.DisciplinedForm);
                listData.Add(inDic);
            });
            dicVarable["list"] = listData;
            sheet.FillVariableValue(dicVarable);
            //sheet.DeleteRow(11);
            templateSheet.Delete();
            #endregion

            //Xoa dong template neu list rong
            if (listData.Count == 0)
                sheet.DeleteRow(11);

            #region reportName

            string examTitle = exam != null ? ReportUtils.StripVNSign(exam.Title) : "TatCa";
            string educationLevel = educationLevelID.HasValue && educationLevelID.Value > 0 ? ReportUtils.StripVNSign(EducationLevelBusiness.Find(educationLevelID.Value).Resolution) : "TatCa";
            string subjectName = examinationSubjectID.HasValue && examinationSubjectID.Value > 0 ? ReportUtils.StripVNSign(ExaminationSubjectBusiness.Find(examinationSubjectID.Value).SubjectCat.SubjectName) : "TatCa";

            reportName = reportName.Replace("Examination", examTitle)
                            .Replace("EducationLevel", educationLevel)
                            .Replace("Subject", subjectName);
            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            sheet.Name = "Sheet1";
            #endregion

            return oBook.ToStream();
        }
    }
}
