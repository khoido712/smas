/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;


namespace SMAS.Business.Business
{ 
    public partial class SendInfoBusiness
    {
        #region Validate
        public void CheckSendInfoIDAvailable(int sendInfoID)
        {
            bool AvailableSendInfoID = this.repository.ExistsRow(GlobalConstants.TRS_SCHEMA, "SendInfo",
                   new Dictionary<string, object>()
                {
                    {"SendInfoID",sendInfoID}
                    
                }, null);
            if (!AvailableSendInfoID)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        public void ValidateValue(SendInfo sendInfo) {
            List<int> lsTypeOfReceiver = new List<int>() {
                SystemParamsInFile.RECEIVE_TYPE_PUPIL,
                SystemParamsInFile.RECEIVE_TYPE_FATHER,
                SystemParamsInFile.RECEIVE_TYPE_MOTHER,
                SystemParamsInFile.RECEIVE_TYPE_PARENT,
                SystemParamsInFile.RECEIVE_TYPE_TEACHER,
                SystemParamsInFile.RECEIVE_TYPE_EMPLOYEE,
                SystemParamsInFile.RECEIVE_TYPE_SUB_EMPLOYEE
            };

            List<int> lsSchedularType = new List<int>() {
                SystemParamsInFile.SCHEDULE_DAY,
                SystemParamsInFile.SCHEDULE_WEEK,
                SystemParamsInFile.SCHEDULE_DISTANCE,
                SystemParamsInFile.SCHEDULE_MONTH,
                SystemParamsInFile.SCHEDULE_YEAR,
                SystemParamsInFile.SCHEDULE_UNEXPECTED
            };

            List<int> lsInfoType = new List<int>() {
                SystemParamsInFile.INFO_TYPE_PARENTS,
                SystemParamsInFile.INFO_TYPE_SUMUP,
                SystemParamsInFile.INFO_TYPE_RANKING,
                SystemParamsInFile.INFO_TYPE_CONDUCT_CAPACITY,
                SystemParamsInFile.INFO_TYPE_CONDUCT_CAPACITY_SEMESTER,
                SystemParamsInFile.INFO_TYPE_FAULTS_ABSENT,
                SystemParamsInFile.INFO_TYPE_PARENT_UNEXPECTED,
                SystemParamsInFile.INFO_TYPE_TEACHER_UNEXPECTED,
                SystemParamsInFile.INFO_TYPE_EMPLOYEE_UNEXPECTED
            };

            if (sendInfo.TypeOfReceiver != null && !lsTypeOfReceiver.Contains(sendInfo.SchedularType.Value))
            {
                List<object> listParam = new List<object>();
                listParam.Add("SendInfo_Label_ReceiveType");
                throw new BusinessException("Common_Validate_SendInfoNumber", listParam);
            }
            if (sendInfo.SchedularType != null && !lsSchedularType.Contains(sendInfo.SchedularType.Value))
            {
                List<object> listParam = new List<object>();
                listParam.Add("SendInfo_Label_SchedularType");
                throw new BusinessException("Common_Validate_SendInfoSchedularType", listParam);
            }
            
            if (sendInfo.InfoType != null && !lsInfoType.Contains(sendInfo.InfoType.Value))
            {
                List<object> listParam = new List<object>();
                listParam.Add("SendInfo_Label_InfoType");
                throw new BusinessException("Common_Validate_SendInfoInfoType", listParam);
            }
            
            if (sendInfo.SendType != SystemParamsInFile.SEND_TYPE_MOBILE 
                && sendInfo.SendType != SystemParamsInFile.SEND_TYPE_EMAIL)
            {
                List<object> listParam = new List<object>();
                listParam.Add("SendInfo_Label_SendType");
                throw new BusinessException("Common_Validate_SendInfoSendType", listParam);
            }
        }
        #endregion
        #region Insert
        public SendInfo Insert(SendInfo sendInfo)
        {
            
            // Check Require, Max Length
            ValidationMetadata.ValidateObject(sendInfo);
            if (sendInfo.ReceiverEmail != null) {
                Utils.ValidateEmail(sendInfo.ReceiverEmail, "SendInfo_Label_EmailOfFa");
            }

            // Check Email
            if (sendInfo.ReceiverEmail != null)
            {
                Utils.ValidateEmail(sendInfo.ReceiverEmail, "SendInfo_Label_EmailOfPu");
            }

            // Check value of ReceiveType, SchedularType, InfoType, SendType
            ValidateValue(sendInfo);

            sendInfo.CreateTime = DateTime.Now;
            
            return base.Insert(sendInfo);
            
        }
        public void InsertSendInfo(List<SendInfo> lsSendInfo) {
            if (lsSendInfo.Count() > 0)
            {
                for (int i = 0; i < lsSendInfo.Count(); i++)
                {
                    Insert(lsSendInfo[i]);
                }
            }
            else {
                throw new BusinessException("Common_Validate_SendInfoInsertLS");
            }
        }
        #endregion
        #region Update
        public SendInfo Update(SendInfo sendInfo) {
            //Check Require, Max Length
            ValidationMetadata.ValidateObject(sendInfo);

            //Check Email
            Utils.ValidateEmail(sendInfo.ReceiverEmail, "SendInfo_Label_EmailOfFa");
            Utils.ValidateEmail(sendInfo.ReceiverEmail, "SendInfo_Label_EmailOfPu");

            // Check value of ReceiveType, SchedularType, InfoType, SendType
            ValidateValue(sendInfo);
            
            return base.Update(sendInfo);
        }
        #endregion
        #region Delete
        public bool Delete(int sendInfoID) {
            try
            {
                //Check available
                CheckSendInfoIDAvailable(sendInfoID);

                //Check Using
                this.CheckConstraints(GlobalConstants.TRS_SCHEMA, "SendInfo", sendInfoID, "SendInfo_Label_SendInfoID");

                base.Delete(sendInfoID);
                return true;
            }
            catch {
                return false;
            }
            
        }
        #endregion
        #region Search
        public IQueryable<SendInfo> Search(IDictionary<string, object> dic)
        {
            string serviceCode = Utils.GetString(dic, "ServiceCode");
            string packageCode = Utils.GetString(dic, "PackageCode");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            string pupilCode = Utils.GetString(dic, "PupilCode");
            string emailOfFa = Utils.GetString(dic, "EmailOfFa");
            string mobileOfFa = Utils.GetString(dic, "MobileOfFa");
            string emailOfPu = Utils.GetString(dic, "EmailOfPu");
            string mobileOfPu = Utils.GetString(dic, "MobileOfPu");
            int receiveType = Utils.GetShort(dic, "ReceiveType");
            int schedularType = Utils.GetShort(dic, "SchedularType");
            string title = Utils.GetString(dic, "Title");
            int sendType = Utils.GetShort(dic, "SendType");
            int infoType = Utils.GetShort(dic, "InfoType");
            int retry_num = Utils.GetShort(dic, "Retry_num");
            string create_user = Utils.GetString(dic, "create_user");
           
            
            IQueryable<SendInfo> listSendInfo = this.SendInfoRepository.All;
            //if (!serviceCode.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.ServiceCode.ToLower().Contains(serviceCode.ToLower()));
            //}
            //if (!packageCode.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.PackageCode.ToLower().Contains(packageCode.ToLower()));
            //}

            //if (!pupilCode.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.PupilCode.ToLower().Contains(pupilCode.ToLower()));
            //}

            //if (!emailOfFa.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.EmailOfFa.ToLower().Contains(emailOfFa.ToLower()));
            //}

            //if (!mobileOfFa.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.MobileOfFa.ToLower().Contains(mobileOfFa.ToLower()));
            //}

            //if (!emailOfPu.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.EmailOfPu.ToLower().Contains(emailOfPu.ToLower()));
            //}
            //if (!mobileOfPu.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.MobileOfPu.ToLower().Contains(mobileOfPu.ToLower()));
            //}

            //if (!title.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.Title.ToLower().Contains(title.ToLower()));
            //}
            //if (!create_user.Equals(string.Empty))
            //{
            //    listSendInfo = listSendInfo.Where(x => x.create_user.ToLower().Contains(create_user.ToLower()));
            //}
            //if(receiveType!=-1){
            //    listSendInfo = listSendInfo.Where(x => x.ReceiveType == receiveType);
            //}

            //if (schedularType != -1)
            //{
            //    listSendInfo = listSendInfo.Where(x => x.SchedularType == schedularType);
            //}
            //if (sendType != -1)
            //{
            //    listSendInfo = listSendInfo.Where(x => x.SendType == sendType);
            //}
            //if (retry_num != -1)
            //{
            //    listSendInfo = listSendInfo.Where(x => x.Retry_num == retry_num);
            //}

            //if (schoolID != 0)
            //{
            //    listSendInfo = listSendInfo.Where(x => x.SchoolID == schoolID);
            //}
            //if (infoType != 0)
            //{
            //    listSendInfo = listSendInfo.Where(x => x.InfoType == infoType);
            //}
            
            return listSendInfo;
        }
        #endregion
        
        
    }
}
