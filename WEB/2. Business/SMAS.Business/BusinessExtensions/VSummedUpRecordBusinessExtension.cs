﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Configuration;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class VSummedUpRecordBusiness
    {
        /// <summary>
        /// Quanglm
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<VSummedUpRecord> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Quanglm
        /// Tim kiem chi tiet
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<VSummedUpRecord> Search(IDictionary<string, object> dic)
        {
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int? SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            int Semester = Utils.GetInt(dic, "Semester", -1);
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");
            string JudgementResult = Utils.GetString(dic, "JudgementResult");
            string Comment = Utils.GetString(dic, "Comment");
            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");
            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");
            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            IQueryable<PupilProfile> IqPupilProfile = PupilProfileBusiness.AllNoTracking;
            IQueryable<VSummedUpRecord> lsSummedUpRecord = this.VSummedUpRecordBusiness.AllNoTracking.Where(o => IqPupilProfile.Any(p => p.PupilProfileID == o.PupilID && p.IsActive));

            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SchoolID == SchoolID));
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.Last2digitNumberSchool == (SchoolID % 100)));
            }
            if (AcademicYearID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.AcademicYearID == AcademicYearID));
                AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.CreatedAcademicYear == aca.Year);
                if (SchoolID <= 0)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.Last2digitNumberSchool == (aca.SchoolID % 100)));
                }
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => lstPupilID.Any(v => v == o.PupilID));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c => c.IsActive == true);
                lsSummedUpRecord = lsSummedUpRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c => c.IsActive == true);
                lsSummedUpRecord = lsSummedUpRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID));
            }
            if (Semester != -1)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == PeriodID.Value);
                }
            }

            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecord;
        }

        public IQueryable<SummedUpRecordBO> SearchSummedUpRecord(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            if (SchoolID == 0)
            {
                return null;
            }
            int Year = Utils.GetInt(dic, "Year");
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");

            int Semester = Utils.GetInt(dic, "Semester", -1);
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");
            string JudgementResult = Utils.GetString(dic, "JudgementResult");
            string Comment = Utils.GetString(dic, "Comment");
            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");
            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");
            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            List<int> lstSubjectID = Utils.GetIntList(dic, "ListSubjectID");
            IQueryable<PupilProfile> IqPupilProfile = PupilProfileBusiness.AllNoTracking;
            IQueryable<SummedUpRecordBO> lsSummedUpRecord;
            //AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            //bool isMovedHistory = UtilsBusiness.IsMoveHistory(academicYear);
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            lsSummedUpRecord = from sur in this.VSummedUpRecordBusiness.AllNoTracking
                               join pp in IqPupilProfile on sur.PupilID equals pp.PupilProfileID
                               where pp.IsActive && sur.Last2digitNumberSchool == partitionId
                               select new SummedUpRecordBO
                                   {
                                       SummedUpRecordID = sur.SummedUpRecordID,
                                       PupilID = sur.PupilID,
                                       ClassID = sur.ClassID,
                                       SchoolID = sur.SchoolID,
                                       Last2digitNumberSchool = sur.Last2digitNumberSchool,
                                       AcademicYearID = sur.AcademicYearID,
                                       SubjectID = sur.SubjectID,
                                       Semester = sur.Semester,
                                       PeriodID = sur.PeriodID,
                                       SummedUpMark = sur.SummedUpMark,
                                       JudgementResult = sur.JudgementResult,
                                       SummedUpDate = sur.SummedUpDate,
                                       CreatedAcademicYear = sur.CreatedAcademicYear,
                                       ReTestMark = sur.ReTestMark,
                                       ReTestJudgement = sur.ReTestJudgement,
                                       Comment = sur.Comment,
                                       IsCommenting = sur.IsCommenting
                                   };

            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.PupilID == PupilID));
            }
            if (lstClassID.Count > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => lstClassID.Contains(su.ClassID));
            }

            if (ClassID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassID == ClassID));
            }

            if (AcademicYearID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.AcademicYearID == AcademicYearID));
                /*AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.CreatedAcademicYear == aca.Year);
                if (SchoolID <= 0)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.Last2digitNumberSchool == (aca.SchoolID % 100)));
                }*/
            }
            if (Year > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.CreatedAcademicYear == Year);
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => lstPupilID.Any(v => v == o.PupilID));
            }
            if (lstSubjectID != null && lstSubjectID.Count() > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => lstSubjectID.Any(v => v == o.SubjectID));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c => c.IsActive == true);
                lsSummedUpRecord = lsSummedUpRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c => c.IsActive == true);
                lsSummedUpRecord = lsSummedUpRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID));
            }
            if (Semester != -1)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == PeriodID.Value);
                }
            }

            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecord;
        }
    }
}