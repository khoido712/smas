/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using Oracle.DataAccess.Client;
using System.Data;
using System.Threading.Tasks;
using SMAS.VTUtils.Log;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class MarkStatisticBusiness
    {
        public void SendAll(List<MarkStatisticBO> ListData)
        {
            MarkStatistic objInsert = null;
            foreach (MarkStatisticBO ms in ListData)
            {
                objInsert = new MarkStatistic
                {
                    CriteriaReportID = ms.CriteriaReportID,
                    DistrictID = ms.DistrictID,
                    Last2DigitNumberProvince = ms.Last2DigitNumberProvince,
                    MarkType = ms.MarkType,
                    ProvinceID = ms.ProvinceID,
                    PupilTotal = ms.PupilTotal,
                    SuperVisingDeptID = ms.SuperVisingDeptID,
                    ProcessedDate = ms.ProcessedDate,
                    ReportCode = ms.ReportCode,
                    SchoolID = ms.SchoolID,
                    AcademicYearID = ms.AcademicYearID,
                    Year = ms.Year,
                    Semester = ms.Semester,
                    SubjectID = ms.SubjectID,
                    EducationLevelID = ms.EducationLevelID,
                    MarkLevel00 = ms.MarkLevel00,
                    MarkLevel01 = ms.MarkLevel01,
                    MarkLevel02 = ms.MarkLevel02,
                    MarkLevel03 = ms.MarkLevel03,
                    MarkLevel04 = ms.MarkLevel04,
                    MarkLevel05 = ms.MarkLevel05,
                    MarkLevel06 = ms.MarkLevel06,
                    MarkLevel07 = ms.MarkLevel07,
                    MarkLevel08 = ms.MarkLevel08,
                    MarkLevel09 = ms.MarkLevel09,
                    MarkLevel10 = ms.MarkLevel10,
                    MarkLevel11 = ms.MarkLevel11,
                    MarkLevel12 = ms.MarkLevel12,
                    MarkLevel13 = ms.MarkLevel13,
                    MarkLevel14 = ms.MarkLevel14,
                    MarkLevel15 = ms.MarkLevel15,
                    MarkLevel16 = ms.MarkLevel16,
                    MarkLevel17 = ms.MarkLevel17,
                    MarkLevel18 = ms.MarkLevel18,
                    MarkLevel19 = ms.MarkLevel19,
                    MarkLevel20 = ms.MarkLevel20,
                    MarkLevel21 = ms.MarkLevel21,
                    MarkLevel22 = ms.MarkLevel22,
                    MarkLevel23 = ms.MarkLevel23,
                    MarkLevel24 = ms.MarkLevel24,
                    MarkLevel25 = ms.MarkLevel25,
                    MarkLevel26 = ms.MarkLevel26,
                    MarkLevel27 = ms.MarkLevel27,
                    MarkLevel28 = ms.MarkLevel28,
                    MarkLevel29 = ms.MarkLevel29,
                    MarkLevel30 = ms.MarkLevel30,
                    MarkLevel31 = ms.MarkLevel31,
                    MarkLevel32 = ms.MarkLevel32,
                    MarkLevel33 = ms.MarkLevel33,
                    MarkLevel34 = ms.MarkLevel34,
                    MarkLevel35 = ms.MarkLevel35,
                    MarkLevel36 = ms.MarkLevel36,
                    MarkLevel37 = ms.MarkLevel37,
                    MarkLevel38 = ms.MarkLevel38,
                    MarkLevel39 = ms.MarkLevel39,
                    MarkLevel40 = ms.MarkLevel40,
                    MarkTotal = ms.MarkTotal,
                    BelowAverage = ms.BelowAverage,
                    OnAverage = ms.OnAverage,
                    AppliedLevel = ms.AppliedLevel
                };

                Insert(objInsert);
            }
        }
        //trangdd
        public IQueryable<SubjectCatBO> SearchSubjectHasReportTHCS(IDictionary<string, object> dic)
        {
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int? Semester = Utils.GetInt(dic, "Semester");
            bool SentToSupervisor = Utils.GetBool(dic, "SentToSupervisor");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? SubCommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? ProvinceID = Utils.GetInt(dic, "ProvinceID");
            var query = from ms in MarkStatisticRepository.All
                        join sc in SubjectCatRepository.All on ms.SubjectID equals sc.SubjectCatID
                        join sp in SchoolProfileRepository.All on ms.SchoolID equals sp.SchoolProfileID
                        where ms.ReportCode == ReportCode
                        && ms.Year == Year
                        && ms.Semester == Semester
                        //&& ms.SentToSupervisor == SentToSupervisor
                        && (ms.EducationLevelID == EducationLevelID)
                        //&& (ms.SubCommitteeID == SubCommitteeID || SubCommitteeID == 0)
                        && (sp.TrainingTypeID == TrainingTypeID || TrainingTypeID == 0)
                        && (sp.SupervisingDeptID == SupervisingDeptID || SupervisingDeptID == 0)
                        && (sp.ProvinceID == ProvinceID || ProvinceID == 0)
                        select new SubjectCatBO
                        {
                            SubjectCatID = ms.SubjectID,
                            SubjectName = sc.SubjectName,
                            DisplayName = sc.DisplayName,
                            Description = sc.Description,
                            OrderInSubject = sc.OrderInSubject
                        };
            if (query.Count() == 0)// neu phong so khong khai bao mon hoc thi vao lay mon hoc cua truong
            {
                query = from ps in ProvinceSubjectBusiness.All
                        join p in ProvinceBusiness.All on ps.ProvinceID equals p.ProvinceID
                        join sp in SupervisingDeptBusiness.All on ps.ProvinceID equals sp.ProvinceID
                        join sc in SubjectCatBusiness.All on ps.SubjectID equals sc.SubjectCatID
                        join ayp in AcademicYearOfProvinceBusiness.All on ps.AcademicYearOfProvinceID equals ayp.AcademicYearOfProvinceID
                        where sp.ProvinceID == ProvinceID
                              && ps.EducationLevelID == EducationLevelID
                             && ayp.Year == Year
                              && sc.IsActive == true
                              && (sc.IsCommenting == 0 || sc.IsCommenting == 2)
                        select new SubjectCatBO
                        {
                            SubjectCatID = sc.SubjectCatID,
                            SubjectName = sc.SubjectName,
                            DisplayName = sc.DisplayName,
                            Description = sc.Description,
                            OrderInSubject = sc.OrderInSubject
                        };
            }
            return query.Distinct().OrderBy(o => o.OrderInSubject);
        }
        //trangdd
        public List<MarkStatisticsOfProvince23> SearchByProvince23(IDictionary<string, object> dic)
        {
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int? Semester = Utils.GetInt(dic, "Semester");
            bool SentToSupervisor = Utils.GetBool(dic, "SentToSupervisor");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? SubCommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int? DistrictID = Utils.GetInt(dic, "DistrictID");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int grade = Utils.GradeToBinary(AppliedLevel.Value);
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (supervisingDept != null && (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = supervisingDept.ParentID.Value;
            }
            IQueryable<MarkStatistic> lstMarkStatistics = this.MarkStatisticRepository.All.Where(o => o.ReportCode == ReportCode
                && o.Year == Year);

            /*if (SubCommitteeID.Value > 0)
            {
                lstMarkStatistics = lstMarkStatistics.Where(o => (o.SubCommitteeID == SubCommitteeID));
            }*/
            if (EducationLevelID > 0)
            {
                lstMarkStatistics = lstMarkStatistics.Where(o => (o.EducationLevelID == EducationLevelID));
            }
            if (SubjectID > 0)
            {
                lstMarkStatistics = lstMarkStatistics.Where(o => (o.SubjectID == SubjectID));
            }
            if (Semester > 0)
            {
                lstMarkStatistics = lstMarkStatistics.Where(o => (o.Semester == Semester));
            }
            //IQueryable<MarkStatistic> lstMarkStatisticsMax = lstMarkStatistics.Where(o => o.SentDate == lstMarkStatistics.Where(u => u.SchoolID == o.SchoolID).Select(u => u.SentDate).Max());
            List<MarkStatisticsOfProvince23> lsMSBD = (from s in SchoolProfileRepository.All
                                                       join p in SupervisingDeptBusiness.All on s.SupervisingDeptID equals p.SupervisingDeptID
                                                       join q in lstMarkStatistics on s.SchoolProfileID equals q.SchoolID into g
                                                       from j in g.DefaultIfEmpty()
                                                       where s.IsActive == true && (p.SupervisingDeptID == SupervisingDeptID || p.ParentID == SupervisingDeptID)
                                                                       && (ProvinceID > 0 ? s.ProvinceID == ProvinceID : true)
                                                                       && (DistrictID > 0 ? s.DistrictID == DistrictID : true)
                                                                       && (s.EducationGrade & grade) != 0
                                                                       && (TrainingTypeID > 0 ? s.TrainingTypeID == TrainingTypeID : true)
                                                       select new MarkStatisticsOfProvince23
                                                       {
                                                           SchoolID = s.SchoolProfileID,
                                                           SchoolName = s.SchoolName,
                                                           M00 = j.MarkLevel00.HasValue ? j.MarkLevel00.Value : 0,
                                                           M03 = j.MarkLevel01.HasValue ? j.MarkLevel01.Value : 0,
                                                           M05 = j.MarkLevel02.HasValue ? j.MarkLevel02.Value : 0,
                                                           M08 = j.MarkLevel03.HasValue ? j.MarkLevel03.Value : 0,
                                                           M10 = j.MarkLevel04.HasValue ? j.MarkLevel04.Value : 0,
                                                           M13 = j.MarkLevel05.HasValue ? j.MarkLevel05.Value : 0,
                                                           M15 = j.MarkLevel06.HasValue ? j.MarkLevel06.Value : 0,
                                                           M18 = j.MarkLevel07.HasValue ? j.MarkLevel07.Value : 0,
                                                           M20 = j.MarkLevel08.HasValue ? j.MarkLevel08.Value : 0,
                                                           M23 = j.MarkLevel09.HasValue ? j.MarkLevel09.Value : 0,
                                                           M25 = j.MarkLevel10.HasValue ? j.MarkLevel10.Value : 0,
                                                           M28 = j.MarkLevel11.HasValue ? j.MarkLevel11.Value : 0,
                                                           M30 = j.MarkLevel12.HasValue ? j.MarkLevel12.Value : 0,
                                                           M33 = j.MarkLevel13.HasValue ? j.MarkLevel13.Value : 0,
                                                           M35 = j.MarkLevel14.HasValue ? j.MarkLevel14.Value : 0,
                                                           M38 = j.MarkLevel15.HasValue ? j.MarkLevel15.Value : 0,
                                                           M40 = j.MarkLevel16.HasValue ? j.MarkLevel16.Value : 0,
                                                           M43 = j.MarkLevel17.HasValue ? j.MarkLevel17.Value : 0,
                                                           M45 = j.MarkLevel18.HasValue ? j.MarkLevel18.Value : 0,
                                                           M48 = j.MarkLevel19.HasValue ? j.MarkLevel19.Value : 0,
                                                           M50 = j.MarkLevel20.HasValue ? j.MarkLevel20.Value : 0,
                                                           M53 = j.MarkLevel21.HasValue ? j.MarkLevel21.Value : 0,
                                                           M55 = j.MarkLevel22.HasValue ? j.MarkLevel22.Value : 0,
                                                           M58 = j.MarkLevel23.HasValue ? j.MarkLevel23.Value : 0,
                                                           M60 = j.MarkLevel24.HasValue ? j.MarkLevel24.Value : 0,
                                                           M63 = j.MarkLevel25.HasValue ? j.MarkLevel25.Value : 0,
                                                           M65 = j.MarkLevel26.HasValue ? j.MarkLevel26.Value : 0,
                                                           M68 = j.MarkLevel27.HasValue ? j.MarkLevel27.Value : 0,
                                                           M70 = j.MarkLevel28.HasValue ? j.MarkLevel28.Value : 0,
                                                           M73 = j.MarkLevel29.HasValue ? j.MarkLevel29.Value : 0,
                                                           M75 = j.MarkLevel30.HasValue ? j.MarkLevel30.Value : 0,
                                                           M78 = j.MarkLevel31.HasValue ? j.MarkLevel31.Value : 0,
                                                           M80 = j.MarkLevel32.HasValue ? j.MarkLevel32.Value : 0,
                                                           M83 = j.MarkLevel33.HasValue ? j.MarkLevel33.Value : 0,
                                                           M85 = j.MarkLevel34.HasValue ? j.MarkLevel34.Value : 0,
                                                           M88 = j.MarkLevel35.HasValue ? j.MarkLevel35.Value : 0,
                                                           M90 = j.MarkLevel36.HasValue ? j.MarkLevel36.Value : 0,
                                                           M93 = j.MarkLevel37.HasValue ? j.MarkLevel37.Value : 0,
                                                           M95 = j.MarkLevel38.HasValue ? j.MarkLevel38.Value : 0,
                                                           M98 = j.MarkLevel39.HasValue ? j.MarkLevel39.Value : 0,
                                                           M100 = j.MarkLevel40.HasValue ? j.MarkLevel40.Value : 0
                                                       }).OrderBy(o => o.SchoolName).ToList();
            foreach (var msbdBO in lsMSBD)
            {

                msbdBO.TotalTest = msbdBO.M00 + msbdBO.M03 + msbdBO.M05 + msbdBO.M08 + msbdBO.M10 +
                                    msbdBO.M13 + msbdBO.M15 + msbdBO.M18 + msbdBO.M20 +
                                    msbdBO.M23 + msbdBO.M25 + msbdBO.M28 + msbdBO.M30 +
                                    msbdBO.M33 + msbdBO.M35 + msbdBO.M38 + msbdBO.M40 +
                                    msbdBO.M43 + msbdBO.M45 + msbdBO.M48 + msbdBO.M50 +
                                    msbdBO.M53 + msbdBO.M55 + msbdBO.M58 + msbdBO.M60 +
                                    msbdBO.M63 + msbdBO.M65 + msbdBO.M68 + msbdBO.M70 +
                                    msbdBO.M73 + msbdBO.M75 + msbdBO.M78 + msbdBO.M80 +
                                    msbdBO.M83 + msbdBO.M85 + msbdBO.M88 + msbdBO.M90 +
                                    msbdBO.M93 + msbdBO.M95 + msbdBO.M98 + msbdBO.M100
                                    ;
                msbdBO.TotalBelowAverage = msbdBO.M00 + msbdBO.M03 + msbdBO.M05 + msbdBO.M08 + msbdBO.M10 +
                                           msbdBO.M13 + msbdBO.M15 + msbdBO.M18 + msbdBO.M20 +
                                           msbdBO.M23 + msbdBO.M25 + msbdBO.M28 + msbdBO.M30 +
                                           msbdBO.M33 + msbdBO.M35 + msbdBO.M38 + msbdBO.M40 +
                                           msbdBO.M43 + msbdBO.M45 + msbdBO.M48;
                msbdBO.TotalAboveAverage = msbdBO.TotalTest - msbdBO.TotalBelowAverage;
                msbdBO.PercentAboveAverage = (msbdBO.TotalTest != 0) ? Math.Round(((double)msbdBO.TotalAboveAverage / (double)msbdBO.TotalTest) * 100, 2, MidpointRounding.AwayFromZero) : 0;
                msbdBO.PercentBelowAverage = (msbdBO.TotalTest != 0) ? Math.Round(((double)msbdBO.TotalBelowAverage / (double)msbdBO.TotalTest) * 100, 2, MidpointRounding.AwayFromZero) : 0;

            }
            lsMSBD = lsMSBD.OrderBy(o => o.SchoolName).ToList();
            return lsMSBD;
        }
        //trangdd
        public List<MarkStatisticsOfProvince23> SearchBySupervisingDept23(IDictionary<string, object> dic)
        {
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int? Semester = Utils.GetInt(dic, "Semester");
            bool SentToSupervisor = Utils.GetBool(dic, "SentToSupervisor");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? SubCommitteeID = Utils.GetInt(dic, "SubCommitteeID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int? DistrictID = Utils.GetInt(dic, "DistrictID");
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int grade = Utils.GradeToBinary(AppliedLevel.Value);
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            IQueryable<MarkStatistic> lstMarkStatistics = this.MarkStatisticRepository.All.Where(
                o => o.ReportCode == ReportCode
                    && o.Year == Year && o.Semester == Semester
                    //&& o.SentToSupervisor == SentToSupervisor
                    && o.SubjectID == SubjectID);
            /*if (SubCommitteeID.Value != 0)
            {
                lstMarkStatistics = lstMarkStatistics.Where(o => (o.SubCommitteeID == SubCommitteeID));
            }*/
            if (EducationLevelID != 0)
            {
                lstMarkStatistics = lstMarkStatistics.Where(o => (o.EducationLevelID == EducationLevelID));
            }
            //IQueryable<MarkStatistic> lstMarkStatisticsMax = lstMarkStatistics.Where(o => o.SentDate == lstMarkStatistics.Where(u => u.SchoolID == o.SchoolID).Select(u => u.SentDate).Max());
            List<MarkStatisticsOfProvince23> lsMSBD = (from s in SchoolProfileRepository.All
                                                       join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                                       join q in lstMarkStatistics on s.SchoolProfileID equals q.SchoolID into g
                                                       from j in g.DefaultIfEmpty()
                                                       where s.IsActive == true
                                                                       && (ProvinceID > 0 ? s.ProvinceID == ProvinceID : true)
                                                                       && (SupervisingDeptID > 0 ? (sd.SupervisingDeptID == SupervisingDeptID || sd.ParentID == SupervisingDeptID) : true)
                                                                       && (s.EducationGrade & grade) != 0
                                                                       && (TrainingTypeID > 0 ? s.TrainingTypeID == TrainingTypeID : true)
                                                       select new MarkStatisticsOfProvince23
                                                       {
                                                           SchoolID = s.SchoolProfileID,
                                                           SchoolName = s.SchoolName,
                                                           M00 = j.MarkLevel00.HasValue ? j.MarkLevel00.Value : 0,
                                                           M03 = j.MarkLevel01.HasValue ? j.MarkLevel01.Value : 0,
                                                           M05 = j.MarkLevel02.HasValue ? j.MarkLevel02.Value : 0,
                                                           M08 = j.MarkLevel03.HasValue ? j.MarkLevel03.Value : 0,
                                                           M10 = j.MarkLevel04.HasValue ? j.MarkLevel04.Value : 0,
                                                           M13 = j.MarkLevel05.HasValue ? j.MarkLevel05.Value : 0,
                                                           M15 = j.MarkLevel06.HasValue ? j.MarkLevel06.Value : 0,
                                                           M18 = j.MarkLevel07.HasValue ? j.MarkLevel07.Value : 0,
                                                           M20 = j.MarkLevel08.HasValue ? j.MarkLevel08.Value : 0,
                                                           M23 = j.MarkLevel09.HasValue ? j.MarkLevel09.Value : 0,
                                                           M25 = j.MarkLevel10.HasValue ? j.MarkLevel10.Value : 0,
                                                           M28 = j.MarkLevel11.HasValue ? j.MarkLevel11.Value : 0,
                                                           M30 = j.MarkLevel12.HasValue ? j.MarkLevel12.Value : 0,
                                                           M33 = j.MarkLevel13.HasValue ? j.MarkLevel13.Value : 0,
                                                           M35 = j.MarkLevel14.HasValue ? j.MarkLevel14.Value : 0,
                                                           M38 = j.MarkLevel15.HasValue ? j.MarkLevel15.Value : 0,
                                                           M40 = j.MarkLevel16.HasValue ? j.MarkLevel16.Value : 0,
                                                           M43 = j.MarkLevel17.HasValue ? j.MarkLevel17.Value : 0,
                                                           M45 = j.MarkLevel18.HasValue ? j.MarkLevel18.Value : 0,
                                                           M48 = j.MarkLevel19.HasValue ? j.MarkLevel19.Value : 0,
                                                           M50 = j.MarkLevel20.HasValue ? j.MarkLevel20.Value : 0,
                                                           M53 = j.MarkLevel21.HasValue ? j.MarkLevel21.Value : 0,
                                                           M55 = j.MarkLevel22.HasValue ? j.MarkLevel22.Value : 0,
                                                           M58 = j.MarkLevel23.HasValue ? j.MarkLevel23.Value : 0,
                                                           M60 = j.MarkLevel24.HasValue ? j.MarkLevel24.Value : 0,
                                                           M63 = j.MarkLevel25.HasValue ? j.MarkLevel25.Value : 0,
                                                           M65 = j.MarkLevel26.HasValue ? j.MarkLevel26.Value : 0,
                                                           M68 = j.MarkLevel27.HasValue ? j.MarkLevel27.Value : 0,
                                                           M70 = j.MarkLevel28.HasValue ? j.MarkLevel28.Value : 0,
                                                           M73 = j.MarkLevel29.HasValue ? j.MarkLevel29.Value : 0,
                                                           M75 = j.MarkLevel30.HasValue ? j.MarkLevel30.Value : 0,
                                                           M78 = j.MarkLevel31.HasValue ? j.MarkLevel31.Value : 0,
                                                           M80 = j.MarkLevel32.HasValue ? j.MarkLevel32.Value : 0,
                                                           M83 = j.MarkLevel33.HasValue ? j.MarkLevel33.Value : 0,
                                                           M85 = j.MarkLevel34.HasValue ? j.MarkLevel34.Value : 0,
                                                           M88 = j.MarkLevel35.HasValue ? j.MarkLevel35.Value : 0,
                                                           M90 = j.MarkLevel36.HasValue ? j.MarkLevel36.Value : 0,
                                                           M93 = j.MarkLevel37.HasValue ? j.MarkLevel37.Value : 0,
                                                           M95 = j.MarkLevel38.HasValue ? j.MarkLevel38.Value : 0,
                                                           M98 = j.MarkLevel39.HasValue ? j.MarkLevel39.Value : 0,
                                                           M100 = j.MarkLevel40.HasValue ? j.MarkLevel40.Value : 0
                                                       }).OrderBy(o => o.SchoolName).ToList();
            foreach (var msbdBO in lsMSBD)
            {
                msbdBO.TotalTest = msbdBO.M00 + msbdBO.M03 + msbdBO.M05 + msbdBO.M08 + msbdBO.M10 +
                                    msbdBO.M13 + msbdBO.M15 + msbdBO.M18 + msbdBO.M20 +
                                    msbdBO.M23 + msbdBO.M25 + msbdBO.M28 + msbdBO.M30 +
                                    msbdBO.M33 + msbdBO.M35 + msbdBO.M38 + msbdBO.M40 +
                                    msbdBO.M43 + msbdBO.M45 + msbdBO.M48 + msbdBO.M50 +
                                    msbdBO.M53 + msbdBO.M55 + msbdBO.M58 + msbdBO.M60 +
                                    msbdBO.M63 + msbdBO.M65 + msbdBO.M68 + msbdBO.M70 +
                                    msbdBO.M73 + msbdBO.M75 + msbdBO.M78 + msbdBO.M80 +
                                    msbdBO.M83 + msbdBO.M85 + msbdBO.M88 + msbdBO.M90 +
                                    msbdBO.M93 + msbdBO.M95 + msbdBO.M98 + msbdBO.M100
                                    ;
                msbdBO.TotalBelowAverage = msbdBO.M00 + msbdBO.M03 + msbdBO.M05 + msbdBO.M08 + msbdBO.M10 +
                                           msbdBO.M13 + msbdBO.M15 + msbdBO.M18 + msbdBO.M20 +
                                           msbdBO.M23 + msbdBO.M25 + msbdBO.M28 + msbdBO.M30 +
                                           msbdBO.M33 + msbdBO.M35 + msbdBO.M38 + msbdBO.M40 +
                                           msbdBO.M43 + msbdBO.M45 + msbdBO.M48;
                msbdBO.TotalAboveAverage = msbdBO.TotalTest - msbdBO.TotalBelowAverage;
                msbdBO.PercentAboveAverage = msbdBO.TotalTest != 0 ? Math.Round(((double)msbdBO.TotalAboveAverage / (double)msbdBO.TotalTest) * 100, 2, MidpointRounding.AwayFromZero) : 0;
                msbdBO.PercentBelowAverage = msbdBO.TotalTest != 0 ? Math.Round(((double)msbdBO.TotalBelowAverage / (double)msbdBO.TotalTest) * 100, 2, MidpointRounding.AwayFromZero) : 0;
                msbdBO.PercentBelowAverage = msbdBO.TotalTest != 0 ? Math.Round(((double)msbdBO.TotalBelowAverage / (double)msbdBO.TotalTest) * 100, 2, MidpointRounding.AwayFromZero) : 0;
            }
            lsMSBD = lsMSBD.OrderBy(o => o.SchoolName).ToList();
            return lsMSBD;
        }
        /// <summary>
        /// Tạo thống kê điểm kiểm tra định kỳ cấp 2- Sở
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// trangdd
        public List<MarkStatisticsOfProvince23> CreateSGDPriodicMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? Year = Utils.GetInt(dic, "Year");
            int? trainningTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? semester = Utils.GetInt(dic, "Semester");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<MarkStatisticsOfProvince23> lstMarkStatisticOfProvinceSecondary = this.SearchByProvince23(dic);
            lstMarkStatisticOfProvinceSecondary = lstMarkStatisticOfProvinceSecondary.OrderBy(o => o.SchoolName).ToList();
            FileID = 0;
            if (lstMarkStatisticOfProvinceSecondary == null)
            {
                return null;
            }
            string reportCode = SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraDinhKyCap2;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            string supervisingDeptName = supervisingDept.SupervisingDeptName;
            string districtName = districtID.HasValue && districtID.Value > 0 ? DistrictBusiness.Find(districtID).DistrictName : string.Empty;
            string provinceName = supervisingDept.Province.ProvinceName;
            string trainingName;
            if (trainningTypeID == 0)
            {
                trainingName = "[Tất cả]";
            }
            else
            { trainingName = TrainingTypeBusiness.Find(trainningTypeID).Resolution; }
            string educationName;
            if (educationLevelID == 0)
            { educationName = "Khối [Tất cả]"; }
            else
            { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }

            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            string semesterName = "";
            string Mh = SubjectCatBusiness.Find(SubjectID).DisplayName;
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
            else semesterName = "Cả năm";
            string semesterAndAcademic = "Hệ " + trainingName + " - " + educationName + " - " + "Môn " + Mh + " - " + semesterName + " - Năm học " + Year + " - " + (Year + 1);

            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", dateTime);
            sheet.SetCellValue("U5", semesterAndAcademic);
            int index = 0;

            int firstRow = 8;
            string colOrder = "A";
            string colSchoolName = "B";
            string colSendDate = "C";
            string colTotalTest = "D";
            string col00 = "E";
            string col03 = "F";
            string col05 = "G";
            string col08 = "H";
            string col10 = "I";
            string col13 = "J";
            string col15 = "K";
            string col18 = "L";
            string col20 = "M";
            string col23 = "N";
            string col25 = "O";
            string col28 = "P";
            string col30 = "Q";
            string col33 = "R";
            string col35 = "S";
            string col38 = "T";
            string col40 = "U";
            string col43 = "V";
            string col45 = "W";
            string col48 = "X";
            string colTotalBelowAverage = "Y";
            string colPercentBelowAverage = "Z";
            string col50 = "AA";
            string col53 = "AB";
            string col55 = "AC";
            string col58 = "AD";
            string col60 = "AE";
            string col63 = "AF";
            string col65 = "AG";
            string col68 = "AH";
            string col70 = "AI";
            string col73 = "AJ";
            string col75 = "AK";
            string col78 = "AL";
            string col80 = "AM";
            string col83 = "AN";
            string col85 = "AO";
            string col88 = "AP";
            string col90 = "AQ";
            string col93 = "AR";
            string col95 = "AS";
            string col98 = "AT";
            string col100 = "AU";
            string colTotalAboveAverage = "AV";
            string colPercentAboveAverage = "AW";
            IVTRange range = sheet.GetRange("A8", "AW8");
            foreach (var item in lstMarkStatisticOfProvinceSecondary)
            {

                int currentRow = firstRow + index;
                index++;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);

                //Fill dữ liệu
                sheet.SetCellValue(colOrder + currentRow, index);
                sheet.SetCellValue(colSchoolName + currentRow, item.SchoolName);
                sheet.SetCellValue(colSendDate + currentRow, item.SentDate.HasValue ? item.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                sheet.SetCellValue(colTotalTest + currentRow, item.TotalTest);
                sheet.SetCellValue(col00 + currentRow, item.M00);
                sheet.SetCellValue(col03 + currentRow, item.M03);
                sheet.SetCellValue(col05 + currentRow, item.M05);
                sheet.SetCellValue(col08 + currentRow, item.M08);

                sheet.SetCellValue(col10 + currentRow, item.M10);
                sheet.SetCellValue(col13 + currentRow, item.M13);
                sheet.SetCellValue(col15 + currentRow, item.M15);
                sheet.SetCellValue(col18 + currentRow, item.M18);

                sheet.SetCellValue(col20 + currentRow, item.M20);
                sheet.SetCellValue(col23 + currentRow, item.M23);
                sheet.SetCellValue(col25 + currentRow, item.M25);
                sheet.SetCellValue(col28 + currentRow, item.M28);

                sheet.SetCellValue(col30 + currentRow, item.M30);
                sheet.SetCellValue(col33 + currentRow, item.M33);
                sheet.SetCellValue(col35 + currentRow, item.M35);
                sheet.SetCellValue(col38 + currentRow, item.M38);

                sheet.SetCellValue(col40 + currentRow, item.M40);
                sheet.SetCellValue(col43 + currentRow, item.M43);
                sheet.SetCellValue(col45 + currentRow, item.M45);
                sheet.SetCellValue(col48 + currentRow, item.M48);


                sheet.SetCellValue(colTotalBelowAverage + currentRow, item.TotalBelowAverage);
                sheet.SetCellValue(colPercentBelowAverage + currentRow, item.PercentBelowAverage);
                sheet.SetCellValue(col50 + currentRow, item.M50);
                sheet.SetCellValue(col53 + currentRow, item.M53);
                sheet.SetCellValue(col55 + currentRow, item.M55);
                sheet.SetCellValue(col58 + currentRow, item.M58);

                sheet.SetCellValue(col60 + currentRow, item.M60);
                sheet.SetCellValue(col63 + currentRow, item.M63);
                sheet.SetCellValue(col65 + currentRow, item.M65);
                sheet.SetCellValue(col68 + currentRow, item.M68);

                sheet.SetCellValue(col70 + currentRow, item.M70);
                sheet.SetCellValue(col73 + currentRow, item.M73);
                sheet.SetCellValue(col75 + currentRow, item.M75);
                sheet.SetCellValue(col78 + currentRow, item.M78);

                sheet.SetCellValue(col80 + currentRow, item.M80);
                sheet.SetCellValue(col83 + currentRow, item.M83);
                sheet.SetCellValue(col85 + currentRow, item.M85);
                sheet.SetCellValue(col88 + currentRow, item.M88);

                sheet.SetCellValue(col90 + currentRow, item.M90);
                sheet.SetCellValue(col93 + currentRow, item.M93);
                sheet.SetCellValue(col95 + currentRow, item.M95);
                sheet.SetCellValue(col98 + currentRow, item.M98);
                sheet.SetCellValue(col100 + currentRow, item.M100);
                sheet.SetCellValue(colTotalAboveAverage + currentRow, item.TotalAboveAverage);
                sheet.SetCellValue(colPercentAboveAverage + currentRow, item.PercentAboveAverage);
            }
            sheet.FitAllColumnsOnOnePage = true;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(oBook.ToStream());
            pr.SentToSupervisor = true;

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);
            outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + districtName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + trainingName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            return lstMarkStatisticOfProvinceSecondary.ToList();
        }
        /// <summary>
        /// //Tạo thống kê điểm kiểm tra học kỳ cấp 2 - Sở
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>  
        /// trangdd
        public List<MarkStatisticsOfProvince23> CreateSGDSemesterMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? Year = Utils.GetInt(dic, "Year");
            int? trainningTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? semester = Utils.GetInt(dic, "Semester");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<MarkStatisticsOfProvince23> lstMarkStatisticOfProvinceSecondary = this.SearchByProvince23(dic);
            lstMarkStatisticOfProvinceSecondary = lstMarkStatisticOfProvinceSecondary.OrderBy(o => o.SchoolName).ToList();
            FileID = 0;
            if (lstMarkStatisticOfProvinceSecondary == null)
            {
                return null;
            }
            string reportCode = SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraHocKy;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            string supervisingDeptName = supervisingDept.SupervisingDeptName;
            string districtName = districtID.HasValue && districtID.Value > 0 ? DistrictBusiness.Find(districtID).DistrictName : string.Empty;
            string provinceName = supervisingDept.Province.ProvinceName;
            string trainingName;
            if (trainningTypeID == 0)
            {
                trainingName = "[Tất cả]";
            }
            else
            { trainingName = TrainingTypeBusiness.Find(trainningTypeID).Resolution; }
            string educationName;
            if (educationLevelID == 0)
            { educationName = "Khối [Tất cả]"; }
            else
            { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }

            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            string semesterName = "";
            string Mh = SubjectCatBusiness.Find(SubjectID).DisplayName;
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
            else semesterName = "Cả năm";
            string semesterAndAcademic = "Hệ " + trainingName + " - " + educationName + " - " + "Môn " + Mh + " - " + semesterName + " - Năm học " + Year + " - " + (Year + 1);
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", dateTime);
            sheet.SetCellValue("U5", semesterAndAcademic);
            int index = 0;

            int firstRow = 8;
            string colOrder = "A";
            string colSchoolName = "B";
            string colSendDate = "C";
            string colTotalTest = "D";
            string col00 = "E";
            string col03 = "F";
            string col05 = "G";
            string col08 = "H";
            string col10 = "I";
            string col13 = "J";
            string col15 = "K";
            string col18 = "L";
            string col20 = "M";
            string col23 = "N";
            string col25 = "O";
            string col28 = "P";
            string col30 = "Q";
            string col33 = "R";
            string col35 = "S";
            string col38 = "T";
            string col40 = "U";
            string col43 = "V";
            string col45 = "W";
            string col48 = "X";
            string colTotalBelowAverage = "Y";
            string colPercentBelowAverage = "Z";
            string col50 = "AA";
            string col53 = "AB";
            string col55 = "AC";
            string col58 = "AD";
            string col60 = "AE";
            string col63 = "AF";
            string col65 = "AG";
            string col68 = "AH";
            string col70 = "AI";
            string col73 = "AJ";
            string col75 = "AK";
            string col78 = "AL";
            string col80 = "AM";
            string col83 = "AN";
            string col85 = "AO";
            string col88 = "AP";
            string col90 = "AQ";
            string col93 = "AR";
            string col95 = "AS";
            string col98 = "AT";
            string col100 = "AU";
            string colTotalAboveAverage = "AV";
            string colPercentAboveAverage = "AW";
            IVTRange range = sheet.GetRange("A8", "AW8");
            foreach (var item in lstMarkStatisticOfProvinceSecondary)
            {
                int currentRow = firstRow + index;
                index++;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue(colOrder + currentRow, index);
                sheet.SetCellValue(colSchoolName + currentRow, item.SchoolName);
                sheet.SetCellValue(colSendDate + currentRow, item.SentDate.HasValue ? item.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                sheet.SetCellValue(colTotalTest + currentRow, item.TotalTest);
                sheet.SetCellValue(col00 + currentRow, item.M00);
                sheet.SetCellValue(col03 + currentRow, item.M03);
                sheet.SetCellValue(col05 + currentRow, item.M05);
                sheet.SetCellValue(col08 + currentRow, item.M08);

                sheet.SetCellValue(col10 + currentRow, item.M10);
                sheet.SetCellValue(col13 + currentRow, item.M13);
                sheet.SetCellValue(col15 + currentRow, item.M15);
                sheet.SetCellValue(col18 + currentRow, item.M18);

                sheet.SetCellValue(col20 + currentRow, item.M20);
                sheet.SetCellValue(col23 + currentRow, item.M23);
                sheet.SetCellValue(col25 + currentRow, item.M25);
                sheet.SetCellValue(col28 + currentRow, item.M28);

                sheet.SetCellValue(col30 + currentRow, item.M30);
                sheet.SetCellValue(col33 + currentRow, item.M33);
                sheet.SetCellValue(col35 + currentRow, item.M35);
                sheet.SetCellValue(col38 + currentRow, item.M38);

                sheet.SetCellValue(col40 + currentRow, item.M40);
                sheet.SetCellValue(col43 + currentRow, item.M43);
                sheet.SetCellValue(col45 + currentRow, item.M45);
                sheet.SetCellValue(col48 + currentRow, item.M48);


                sheet.SetCellValue(colTotalBelowAverage + currentRow, item.TotalBelowAverage);
                sheet.SetCellValue(colPercentBelowAverage + currentRow, item.PercentBelowAverage);
                sheet.SetCellValue(col50 + currentRow, item.M50);
                sheet.SetCellValue(col53 + currentRow, item.M53);
                sheet.SetCellValue(col55 + currentRow, item.M55);
                sheet.SetCellValue(col58 + currentRow, item.M58);

                sheet.SetCellValue(col60 + currentRow, item.M60);
                sheet.SetCellValue(col63 + currentRow, item.M63);
                sheet.SetCellValue(col65 + currentRow, item.M65);
                sheet.SetCellValue(col68 + currentRow, item.M68);

                sheet.SetCellValue(col70 + currentRow, item.M70);
                sheet.SetCellValue(col73 + currentRow, item.M73);
                sheet.SetCellValue(col75 + currentRow, item.M75);
                sheet.SetCellValue(col78 + currentRow, item.M78);

                sheet.SetCellValue(col80 + currentRow, item.M80);
                sheet.SetCellValue(col83 + currentRow, item.M83);
                sheet.SetCellValue(col85 + currentRow, item.M85);
                sheet.SetCellValue(col88 + currentRow, item.M88);

                sheet.SetCellValue(col90 + currentRow, item.M90);
                sheet.SetCellValue(col93 + currentRow, item.M93);
                sheet.SetCellValue(col95 + currentRow, item.M95);
                sheet.SetCellValue(col98 + currentRow, item.M98);
                sheet.SetCellValue(col100 + currentRow, item.M100);
                sheet.SetCellValue(colTotalAboveAverage + currentRow, item.TotalAboveAverage);
                sheet.SetCellValue(colPercentAboveAverage + currentRow, item.PercentAboveAverage);
            }
            sheet.FitAllColumnsOnOnePage = true;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(oBook.ToStream());
            pr.SentToSupervisor = true;

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);
            outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + districtName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + trainingName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            return lstMarkStatisticOfProvinceSecondary.ToList();
        }
        /// <summary>
        /// Tạo thống kê điểm kiểm tra định kỳ cấp 2- Phòng
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// trangdd
        public List<MarkStatisticsOfProvince23> CreatePGDPriodicMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? trainningTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? semester = Utils.GetInt(dic, "Semester");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            int? Year = Utils.GetInt(dic, "Year");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<MarkStatisticsOfProvince23> lstMarkStatisticsOfSchoolSecondary = this.SearchBySupervisingDept23(dic);
            lstMarkStatisticsOfSchoolSecondary = lstMarkStatisticsOfSchoolSecondary.OrderBy(o => o.SchoolName).ToList();
            FileID = 0;
            if (lstMarkStatisticsOfSchoolSecondary == null)
            {
                return null;
            }
            string reportCode = SystemParamsInFile.REPORT_PGD_ThongKeDiemKiemTraDinhKyCap2;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
            string districtName = districtID.HasValue && districtID.Value > 0 ? DistrictBusiness.Find(districtID).DistrictName : string.Empty;
            string provinceName = supervisingDept.Province.ProvinceName;
            string trainingName;
            if (trainningTypeID == 0)
            {
                trainingName = "[Tất cả]";
            }
            else
            { trainingName = TrainingTypeBusiness.Find(trainningTypeID).Resolution; }
            string educationName;
            if (educationLevelID == 0)
            { educationName = "Khối [Tất cả]"; }
            else
            { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }

            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            string semesterName = "";
            string Mh = "";
            if (SubjectCatBusiness.Find(SubjectID) != null)
            {
                Mh = SubjectCatBusiness.Find(SubjectID).DisplayName;
            }

            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
            else semesterName = "Cả năm";
            string semesterAndAcademic = "Hệ " + trainingName + " - " + educationName + " - " + "Môn " + Mh + " - " + semesterName + " - Năm học " + Year + " - " + (Year + 1);
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", dateTime);
            sheet.SetCellValue("U5", semesterAndAcademic);
            int index = 0;

            int firstRow = 8;
            string colOrder = "A";
            string colSchoolName = "B";
            string colSendDate = "C";
            string colTotalTest = "D";
            string col00 = "E";
            string col03 = "F";
            string col05 = "G";
            string col08 = "H";
            string col10 = "I";
            string col13 = "J";
            string col15 = "K";
            string col18 = "L";
            string col20 = "M";
            string col23 = "N";
            string col25 = "O";
            string col28 = "P";
            string col30 = "Q";
            string col33 = "R";
            string col35 = "S";
            string col38 = "T";
            string col40 = "U";
            string col43 = "V";
            string col45 = "W";
            string col48 = "X";
            string colTotalBelowAverage = "Y";
            string colPercentBelowAverage = "Z";
            string col50 = "AA";
            string col53 = "AB";
            string col55 = "AC";
            string col58 = "AD";
            string col60 = "AE";
            string col63 = "AF";
            string col65 = "AG";
            string col68 = "AH";
            string col70 = "AI";
            string col73 = "AJ";
            string col75 = "AK";
            string col78 = "AL";
            string col80 = "AM";
            string col83 = "AN";
            string col85 = "AO";
            string col88 = "AP";
            string col90 = "AQ";
            string col93 = "AR";
            string col95 = "AS";
            string col98 = "AT";
            string col100 = "AU";
            string colTotalAboveAverage = "AV";
            string colPercentAboveAverage = "AW";
            IVTRange range = sheet.GetRange("A8", "AW8");
            foreach (var item in lstMarkStatisticsOfSchoolSecondary)
            {
                int currentRow = firstRow + index;
                index++;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue(colOrder + currentRow, index);
                sheet.SetCellValue(colSchoolName + currentRow, item.SchoolName);
                sheet.SetCellValue(colSendDate + currentRow, item.SentDate.HasValue ? item.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                sheet.SetCellValue(colTotalTest + currentRow, item.TotalTest);
                sheet.SetCellValue(col00 + currentRow, item.M00);
                sheet.SetCellValue(col03 + currentRow, item.M03);
                sheet.SetCellValue(col05 + currentRow, item.M05);
                sheet.SetCellValue(col08 + currentRow, item.M08);

                sheet.SetCellValue(col10 + currentRow, item.M10);
                sheet.SetCellValue(col13 + currentRow, item.M13);
                sheet.SetCellValue(col15 + currentRow, item.M15);
                sheet.SetCellValue(col18 + currentRow, item.M18);

                sheet.SetCellValue(col20 + currentRow, item.M20);
                sheet.SetCellValue(col23 + currentRow, item.M23);
                sheet.SetCellValue(col25 + currentRow, item.M25);
                sheet.SetCellValue(col28 + currentRow, item.M28);

                sheet.SetCellValue(col30 + currentRow, item.M30);
                sheet.SetCellValue(col33 + currentRow, item.M33);
                sheet.SetCellValue(col35 + currentRow, item.M35);
                sheet.SetCellValue(col38 + currentRow, item.M38);

                sheet.SetCellValue(col40 + currentRow, item.M40);
                sheet.SetCellValue(col43 + currentRow, item.M43);
                sheet.SetCellValue(col45 + currentRow, item.M45);
                sheet.SetCellValue(col48 + currentRow, item.M48);


                sheet.SetCellValue(colTotalBelowAverage + currentRow, item.TotalBelowAverage);
                sheet.SetCellValue(colPercentBelowAverage + currentRow, item.PercentBelowAverage);
                sheet.SetCellValue(col50 + currentRow, item.M50);
                sheet.SetCellValue(col53 + currentRow, item.M53);
                sheet.SetCellValue(col55 + currentRow, item.M55);
                sheet.SetCellValue(col58 + currentRow, item.M58);

                sheet.SetCellValue(col60 + currentRow, item.M60);
                sheet.SetCellValue(col63 + currentRow, item.M63);
                sheet.SetCellValue(col65 + currentRow, item.M65);
                sheet.SetCellValue(col68 + currentRow, item.M68);

                sheet.SetCellValue(col70 + currentRow, item.M70);
                sheet.SetCellValue(col73 + currentRow, item.M73);
                sheet.SetCellValue(col75 + currentRow, item.M75);
                sheet.SetCellValue(col78 + currentRow, item.M78);

                sheet.SetCellValue(col80 + currentRow, item.M80);
                sheet.SetCellValue(col83 + currentRow, item.M83);
                sheet.SetCellValue(col85 + currentRow, item.M85);
                sheet.SetCellValue(col88 + currentRow, item.M88);

                sheet.SetCellValue(col90 + currentRow, item.M90);
                sheet.SetCellValue(col93 + currentRow, item.M93);
                sheet.SetCellValue(col95 + currentRow, item.M95);
                sheet.SetCellValue(col98 + currentRow, item.M98);
                sheet.SetCellValue(col100 + currentRow, item.M100);
                sheet.SetCellValue(colTotalAboveAverage + currentRow, item.TotalAboveAverage);
                sheet.SetCellValue(colPercentAboveAverage + currentRow, item.PercentAboveAverage);
            }
            sheet.FitAllColumnsOnOnePage = true;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(oBook.ToStream());
            pr.SentToSupervisor = true;

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);

            outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + districtName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + trainingName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            return lstMarkStatisticsOfSchoolSecondary.ToList();
        }
        /// <summary>
        /// Tạo thống kê điểm kiểm tra học kỳ cấp 2 - Phòng
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// trangdd
        public List<MarkStatisticsOfProvince23> CreatePGDSemesterMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? trainningTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? semester = Utils.GetInt(dic, "Semester");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            int? Year = Utils.GetInt(dic, "Year");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<MarkStatisticsOfProvince23> lstMarkStatisticsOfSchoolSecondary = this.SearchBySupervisingDept23(dic);
            lstMarkStatisticsOfSchoolSecondary = lstMarkStatisticsOfSchoolSecondary.OrderBy(o => o.SchoolName).ToList();
            FileID = 0;
            if (lstMarkStatisticsOfSchoolSecondary == null)
            {
                return null;
            }
            string reportCode = SystemParamsInFile.REPORT_PGD_ThongKeDiemKiemTraHocKy;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
            string districtName = districtID.HasValue && districtID.Value > 0 ? DistrictBusiness.Find(districtID).DistrictName : string.Empty;
            string provinceName = supervisingDept.Province.ProvinceName;
            string trainingName;
            if (trainningTypeID == 0)
            {
                trainingName = "[Tất cả]";
            }
            else
            { trainingName = TrainingTypeBusiness.Find(trainningTypeID).Resolution; }
            string educationName;
            if (educationLevelID == 0)
            { educationName = "Khối [Tất cả]"; }
            else
            { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }

            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string semesterName = "";
            string Mh = SubjectCatBusiness.Find(SubjectID).DisplayName;
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
            else semesterName = "Cả năm";
            string semesterAndAcademic = "Hệ " + trainingName + " - " + educationName + " - " + "Môn " + Mh + " - " + semesterName + " - Năm học " + Year + " - " + (Year + 1);
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", dateTime);
            sheet.SetCellValue("U5", semesterAndAcademic);
            int index = 0;

            int firstRow = 8;

            string colOrder = "A";
            string colSchoolName = "B";
            string colSendDate = "C";
            string colTotalTest = "D";
            string col00 = "E";
            string col03 = "F";
            string col05 = "G";
            string col08 = "H";
            string col10 = "I";
            string col13 = "J";
            string col15 = "K";
            string col18 = "L";
            string col20 = "M";
            string col23 = "N";
            string col25 = "O";
            string col28 = "P";
            string col30 = "Q";
            string col33 = "R";
            string col35 = "S";
            string col38 = "T";
            string col40 = "U";
            string col43 = "V";
            string col45 = "W";
            string col48 = "X";
            string colTotalBelowAverage = "Y";
            string colPercentBelowAverage = "Z";
            string col50 = "AA";
            string col53 = "AB";
            string col55 = "AC";
            string col58 = "AD";
            string col60 = "AE";
            string col63 = "AF";
            string col65 = "AG";
            string col68 = "AH";
            string col70 = "AI";
            string col73 = "AJ";
            string col75 = "AK";
            string col78 = "AL";
            string col80 = "AM";
            string col83 = "AN";
            string col85 = "AO";
            string col88 = "AP";
            string col90 = "AQ";
            string col93 = "AR";
            string col95 = "AS";
            string col98 = "AT";
            string col100 = "AU";
            string colTotalAboveAverage = "AV";
            string colPercentAboveAverage = "AW";
            IVTRange range = sheet.GetRange("A8", "AW8");
            foreach (var item in lstMarkStatisticsOfSchoolSecondary)
            {
                int currentRow = firstRow + index;
                index++;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue(colOrder + currentRow, index);
                sheet.SetCellValue(colSchoolName + currentRow, item.SchoolName);
                sheet.SetCellValue(colSendDate + currentRow, item.SentDate.HasValue ? item.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                sheet.SetCellValue(colTotalTest + currentRow, item.TotalTest);
                sheet.SetCellValue(col00 + currentRow, item.M00);
                sheet.SetCellValue(col03 + currentRow, item.M03);
                sheet.SetCellValue(col05 + currentRow, item.M05);
                sheet.SetCellValue(col08 + currentRow, item.M08);

                sheet.SetCellValue(col10 + currentRow, item.M10);
                sheet.SetCellValue(col13 + currentRow, item.M13);
                sheet.SetCellValue(col15 + currentRow, item.M15);
                sheet.SetCellValue(col18 + currentRow, item.M18);

                sheet.SetCellValue(col20 + currentRow, item.M20);
                sheet.SetCellValue(col23 + currentRow, item.M23);
                sheet.SetCellValue(col25 + currentRow, item.M25);
                sheet.SetCellValue(col28 + currentRow, item.M28);

                sheet.SetCellValue(col30 + currentRow, item.M30);
                sheet.SetCellValue(col33 + currentRow, item.M33);
                sheet.SetCellValue(col35 + currentRow, item.M35);
                sheet.SetCellValue(col38 + currentRow, item.M38);

                sheet.SetCellValue(col40 + currentRow, item.M40);
                sheet.SetCellValue(col43 + currentRow, item.M43);
                sheet.SetCellValue(col45 + currentRow, item.M45);
                sheet.SetCellValue(col48 + currentRow, item.M48);


                sheet.SetCellValue(colTotalBelowAverage + currentRow, item.TotalBelowAverage);
                sheet.SetCellValue(colPercentBelowAverage + currentRow, item.PercentBelowAverage);
                sheet.SetCellValue(col50 + currentRow, item.M50);
                sheet.SetCellValue(col53 + currentRow, item.M53);
                sheet.SetCellValue(col55 + currentRow, item.M55);
                sheet.SetCellValue(col58 + currentRow, item.M58);

                sheet.SetCellValue(col60 + currentRow, item.M60);
                sheet.SetCellValue(col63 + currentRow, item.M63);
                sheet.SetCellValue(col65 + currentRow, item.M65);
                sheet.SetCellValue(col68 + currentRow, item.M68);

                sheet.SetCellValue(col70 + currentRow, item.M70);
                sheet.SetCellValue(col73 + currentRow, item.M73);
                sheet.SetCellValue(col75 + currentRow, item.M75);
                sheet.SetCellValue(col78 + currentRow, item.M78);

                sheet.SetCellValue(col80 + currentRow, item.M80);
                sheet.SetCellValue(col83 + currentRow, item.M83);
                sheet.SetCellValue(col85 + currentRow, item.M85);
                sheet.SetCellValue(col88 + currentRow, item.M88);

                sheet.SetCellValue(col90 + currentRow, item.M90);
                sheet.SetCellValue(col93 + currentRow, item.M93);
                sheet.SetCellValue(col95 + currentRow, item.M95);
                sheet.SetCellValue(col98 + currentRow, item.M98);
                sheet.SetCellValue(col100 + currentRow, item.M100);
                sheet.SetCellValue(colTotalAboveAverage + currentRow, item.TotalAboveAverage);
                sheet.SetCellValue(colPercentAboveAverage + currentRow, item.PercentAboveAverage);
            }
            sheet.FitAllColumnsOnOnePage = true;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(oBook.ToStream());
            pr.SentToSupervisor = true;

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);

            outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + districtName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + trainingName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            return lstMarkStatisticsOfSchoolSecondary.ToList();
        }


        /// <summary>
        /// SearchSubjectHasReport
        /// </summary>
        /// <param name="ReportCode">The report code.</param>
        /// <param name="Year">The year.</param>
        /// <param name="Semester">The semester.</param>
        /// <param name="SentToSupervisor">if set to <c>true</c> [sent to supervisor].</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="SubCommitteeID">The sub committee ID.</param>
        /// <param name="TrainingTypeID">The training type ID.</param>
        /// <param name="SupervisingDeptID">The supervising dept ID.</param>
        /// <param name="ProvinceID">The province ID.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 03/12/2012   3:02 PM
        /// </remarks>
        public List<SubjectCat> SearchSubjectHasReport(string ReportCode, int Year
          , int? Semester, bool SentToSupervisor, int EducationLevelID, int? SubCommitteeID
          , int TrainingTypeID, int? SupervisingDeptID, int? ProvinceID)
        {
            //MarkStatistics join SubjectCat join SchoolProfile
            IQueryable<MarkStatistic> iqMS = MarkStatisticRepository.All;
            if (ReportCode != null && ReportCode.Trim().Length > 0)
            {
                iqMS = iqMS.Where(o => o.ReportCode.ToUpper() == ReportCode.ToUpper());
            }
            if (Year != 0)
            {
                iqMS = iqMS.Where(o => o.Year == Year);
            }
            if (Semester.HasValue)
            {
                iqMS = iqMS.Where(o => o.Semester == Semester.Value);
            }

            //if(SentToSupervisor)
            /*{
                iqMS = iqMS.Where(o => o.SentToSupervisor == SentToSupervisor);
            }*/
            if (EducationLevelID != 0)
            {
                iqMS = iqMS.Where(o => o.EducationLevelID == EducationLevelID);
            }
            /*if (SubCommitteeID.HasValue && SubCommitteeID != 0)
            {
                iqMS = iqMS.Where(o => o.SubCommitteeID == SubCommitteeID.Value);
            }*/
            /*if (TrainingTypeID != 0)
            {
                iqMS = iqMS.Where(o => o.SchoolProfile.TrainingTypeID == TrainingTypeID);
            }*/
            /*if (SupervisingDeptID.HasValue && SupervisingDeptID != 0)
            {
                iqMS = iqMS.Where(o => o.SchoolProfile.SupervisingDeptID == SupervisingDeptID.Value);
            }
            if (ProvinceID.HasValue && ProvinceID != 0)
            {
                iqMS = iqMS.Where(o => o.SchoolProfile.ProvinceID == ProvinceID.Value);
            }*/

            List<int> lsSC = iqMS.Select(o => o.SubjectID).Distinct().ToList();
            IQueryable<SubjectCat> iqSC = SubjectCatBusiness.All.Where(o => lsSC.Contains(o.SubjectCatID)).OrderBy(o => o.OrderInSubject);
            if (iqSC.Count() == 0)
            {
                return null;
            }
            return iqSC.ToList();
        }

        /// <summary>
        /// GetHashKeyForMarkStatistics
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 03/12/2012   3:02 PM
        /// </remarks>
        public string GetHashKeyForMarkStatistics(MarkReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = entity.Year;
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["Semester"] = entity.Semester;
            dic["TrainingTypeID"] = entity.TrainingTypeID;
            dic["SubcommitteeID"] = entity.SubcommitteeID;
            dic["SubjectID"] = entity.SubjectID;
            dic["ProvinceID"] = entity.ProvinceID;
            dic["DistrictID"] = entity.DistrictID;
            return ReportUtils.GetHashKey(dic);
        }

        /// <summary>
        /// CreateSGDPriodicMarkStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 03/12/2012   3:03 PM sdfsdg
        /// </remarks>
        public List<MarkStatisticsByDistrictBO> CreateSGDPriodicMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            List<MarkStatisticsByDistrictBO> lsMSBDBO = SearchByProvince(SearchInfo);
            lsMSBDBO = lsMSBDBO.OrderBy(o => o.DistrictName).ToList();
            if (lsMSBDBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("SGD_TH_TongHopDiemKiemTraDinhKy");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A10", "AD10");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "AD10");
            sheet.Name = "TH_TKDiemDinhKy";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP ĐIỂM KIỂM TRA ĐỊNH KỲ TIỂU HỌC - " + thisSubject.SubjectName.ToUpper();
            if (Semester == 0)
            {
                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            }
            string semesterAndYear = ReportUtils.ConvertSemesterForReportName(Semester) + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            #region Tao khung
            for (int j = 0; j < lsMSBDBO.Count(); j++)
            {
                sheet.CopyPasteSameSize(templateRange, "A" + (10 + j).ToString());
                MarkStatisticsByDistrictBO item = lsMSBDBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["DistrictName"] = item.DistrictName;
                innerDic["TotalSchool"] = item.TotalSchool;
                innerDic["STT"] = j + 1;
                innerDic["TotalTest"] = item.TotalTest;
                innerDic["TotalBelowAverage"] = item.TotalBelowAverage;
                innerDic["PercentBelowAverage"] = Math.Round(100 * item.PercentBelowAverage, 2, MidpointRounding.AwayFromZero);
                innerDic["TotalAboveAverage"] = item.TotalAboveAverage;
                innerDic["PercentAboveAverage"] = Math.Round(100 * item.PercentAboveAverage, 2, MidpointRounding.AwayFromZero);
                for (int i = 0; i <= 20; i++)
                {
                    string keyInDic = "";
                    decimal tempInt = i * 5;
                    if (i < 2)
                    {
                        keyInDic += "0" + tempInt.ToString();
                    }
                    else
                    {
                        keyInDic += tempInt;
                    }
                    innerDic[keyInDic] = item.MarkInfo[keyInDic];
                }
                //add
                insideDic.Add(innerDic);
            }
            #endregion
            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "SGD_TH_TongHopDiemKiemTraDinhKy";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //SGD_TH_TongHopDiemKiemTraDinhKy_[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }

            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID = 0
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",0},
                {"SupervisingDeptID",SupervisingDeptID}

            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSBDBO;
        }

        /// <summary>
        /// CreatePGDPriodicMarkStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 05/12/2012   2:56 PM
        /// </remarks>
        public List<MarkStatisticsOfSchoolBO> CreatePGDPriodicMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");

            List<MarkStatisticsOfSchoolBO> lsMSOSBO = SearchBySupervisingDept(SearchInfo);
            if (lsMSOSBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("PGD_TH_TongHopDiemKiemTraDinhKy");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;


            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A10", "AC10");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "AC10");
            sheet.Name = "TH_TKDiemDinhKy";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP ĐIỂM KIỂM TRA ĐỊNH KỲ TIỂU HỌC - " + thisSubject.SubjectName.ToUpper();
            string strSemester = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "Học kỳ " + Semester;
            }
            else
            {
                strSemester = "Cả năm";
            }
            string semesterAndYear = strSemester + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSOSBO.Count(); j++)
            {
                // Lay style cho rows
                sheet.CopyPasteSameSize(templateRange, "A" + (10 + j).ToString());
                MarkStatisticsOfSchoolBO item = lsMSOSBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["School"] = item.SchoolName;
                innerDic["TotalTest"] = item.TotalTest;
                innerDic["TotalBelowAverage"] = item.TotalBelowAverage;
                innerDic["PercentBelowAverage"] = Math.Round(100 * item.PercentBelowAverage, 2, MidpointRounding.AwayFromZero);
                innerDic["TotalAboveAverage"] = item.TotalAboveAverage;
                innerDic["PercentAboveAverage"] = Math.Round(100 * item.PercentAboveAverage, 2, MidpointRounding.AwayFromZero);
                innerDic["STT"] = j + 1;
                for (int i = 0; i <= 20; i++)
                {
                    string keyInDic = "";
                    decimal tempInt = i * 5;
                    if (i < 2)
                    {
                        keyInDic += "0" + tempInt.ToString();
                    }
                    else
                    {
                        keyInDic += tempInt;
                    }
                    innerDic[keyInDic] = item.MarkInfo[keyInDic];
                }

                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "PGD_TH_TongHopDiemKiemTraDinhKy";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //PGD_TH_TongHopDiemKiemTraDinhKy_[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }
            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",DistrictID},
                 {"SupervisingDeptID",SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSOSBO;
        }


        /// <summary>
        /// SearchByProvince
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 03/12/2012 
        /// </remarks>
        public List<MarkStatisticsByDistrictBO> SearchByProvince(IDictionary<string, object> SearchInfo)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");

            IQueryable<District> iqDistrict = DistrictBusiness.Search(new Dictionary<string, object>()
            {
                {"ProvinceID",ProvinceID}
            });
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            IQueryable<MarkStatistic> iqMS = MarkStatisticRepository.All;
            if (ReportCode != null && ReportCode.Trim().Length > 0)
            {
                iqMS = iqMS.Where(o => o.ReportCode.ToUpper().CompareTo(ReportCode.ToUpper()) == 0);
            }
            if (Year != 0)
            {
                iqMS = iqMS.Where(o => o.Year == Year);
            }
            if (Semester != 0)
            {
                iqMS = iqMS.Where(o => o.Semester == Semester);
            }
            //if (SentToSupervisor.HasValue)
            //{
            //    iqMS = iqMS.Where(o => o.SentToSupervisor == SentToSupervisor);
            //}
            if (EducationLevelID != 0)
            {
                iqMS = iqMS.Where(o => o.EducationLevelID == EducationLevelID);
            }

            //if (TrainingTypeID != 0)
            //{
            //    iqMS = iqMS.Where(o => o.SchoolProfile.TrainingTypeID == TrainingTypeID);
            //}

            //if (ProvinceID != 0)
            //{
            //    iqMS = iqMS.Where(o => o.SchoolProfile.ProvinceID == ProvinceID);
            //}
            //if (SupervisingDeptID != 0)
            //{
            //    iqMS = iqMS.Where(o => (o.SchoolProfile.SupervisingDeptID == SupervisingDeptID || o.SchoolProfile.SupervisingDept.ParentID == SupervisingDeptID) && o.SchoolProfile.IsActive == true);
            //}
            if (SubjectID != 0)
            {
                iqMS = iqMS.Where(o => o.SubjectID == SubjectID);
            }
            // Lay du lieu theo ngay gui gan nhan
            //var iqMSMax = iqMS.Where(u => u.SentDate == iqMS.Where(v => v.SchoolID == u.SchoolID).Select(v => v.SentDate).Max());
            var listMs = (from d in iqDistrict
                          join m in iqMS on d.DistrictID equals m.DistrictID into i
                          from j in i.DefaultIfEmpty()
                          group j by new
                          {
                              d.DistrictID,
                              d.DistrictName
                          } into g
                          orderby g.Key.DistrictName
                          select new
                          {
                              g.Key.DistrictID,
                              g.Key.DistrictName,
                              MarkLevel00 = g.Sum(o => o.MarkLevel00.HasValue ? o.MarkLevel00.Value : 0),
                              MarkLevel01 = g.Sum(o => o.MarkLevel01.HasValue ? o.MarkLevel01.Value : 0),
                              MarkLevel02 = g.Sum(o => o.MarkLevel02.HasValue ? o.MarkLevel02.Value : 0),
                              MarkLevel03 = g.Sum(o => o.MarkLevel03.HasValue ? o.MarkLevel03.Value : 0),
                              MarkLevel04 = g.Sum(o => o.MarkLevel04.HasValue ? o.MarkLevel04.Value : 0),
                              MarkLevel05 = g.Sum(o => o.MarkLevel05.HasValue ? o.MarkLevel05.Value : 0),
                              MarkLevel06 = g.Sum(o => o.MarkLevel06.HasValue ? o.MarkLevel06.Value : 0),
                              MarkLevel07 = g.Sum(o => o.MarkLevel07.HasValue ? o.MarkLevel07.Value : 0),
                              MarkLevel08 = g.Sum(o => o.MarkLevel08.HasValue ? o.MarkLevel08.Value : 0),
                              MarkLevel09 = g.Sum(o => o.MarkLevel09.HasValue ? o.MarkLevel09.Value : 0),
                              MarkLevel10 = g.Sum(o => o.MarkLevel10.HasValue ? o.MarkLevel10.Value : 0),
                              MarkLevel11 = g.Sum(o => o.MarkLevel11.HasValue ? o.MarkLevel11.Value : 0),
                              MarkLevel12 = g.Sum(o => o.MarkLevel12.HasValue ? o.MarkLevel12.Value : 0),
                              MarkLevel13 = g.Sum(o => o.MarkLevel13.HasValue ? o.MarkLevel13.Value : 0),
                              MarkLevel14 = g.Sum(o => o.MarkLevel14.HasValue ? o.MarkLevel14.Value : 0),
                              MarkLevel15 = g.Sum(o => o.MarkLevel15.HasValue ? o.MarkLevel15.Value : 0),
                              MarkLevel16 = g.Sum(o => o.MarkLevel16.HasValue ? o.MarkLevel16.Value : 0),
                              MarkLevel17 = g.Sum(o => o.MarkLevel17.HasValue ? o.MarkLevel17.Value : 0),
                              MarkLevel18 = g.Sum(o => o.MarkLevel18.HasValue ? o.MarkLevel18.Value : 0),
                              MarkLevel19 = g.Sum(o => o.MarkLevel19.HasValue ? o.MarkLevel19.Value : 0),
                              MarkLevel20 = g.Sum(o => o.MarkLevel20.HasValue ? o.MarkLevel20.Value : 0),
                              TotalBelowAverage = g.Sum(o => o.BelowAverage.HasValue ? o.BelowAverage.Value : 0),
                              TotalAboveAverage = g.Sum(o => o.OnAverage.HasValue ? o.OnAverage.Value : 0),
                              MarkTotal = g.Sum(o => o.MarkTotal.HasValue ? o.MarkTotal.Value : 0),
                              TotalSchool = g.Count()
                          }).ToList();

            List<MarkStatisticsByDistrictBO> lsMSBD = new List<MarkStatisticsByDistrictBO>();
            foreach (var districtObj in listMs)
            {
                MarkStatisticsByDistrictBO msbdBO = new MarkStatisticsByDistrictBO();
                msbdBO.DistrictID = districtObj.DistrictID;
                msbdBO.DistrictName = districtObj.DistrictName;
                msbdBO.TotalSchool = districtObj.TotalSchool;
                msbdBO.TotalTest = districtObj.MarkTotal;
                msbdBO.TotalAboveAverage = districtObj.TotalAboveAverage;
                msbdBO.TotalBelowAverage = districtObj.TotalBelowAverage;
                for (int i = 0; i <= 20; i++)
                {
                    string keyInsideQuery = "MarkLevel";
                    if (i < 10)
                    {
                        keyInsideQuery += "0" + i;
                    }
                    else
                    {
                        keyInsideQuery += i;
                    }
                    int? sumMarkLevel = (int?)districtObj.GetType().GetProperty(keyInsideQuery).GetValue(districtObj, null);
                    sumMarkLevel = sumMarkLevel.HasValue ? sumMarkLevel : 0;
                    string keyInDic = "";
                    {
                        decimal tempInt = i * 5;
                        if (i < 2)
                        {
                            keyInDic += "0" + tempInt.ToString();
                        }
                        else
                        {
                            keyInDic += tempInt;
                        }
                    }
                    msbdBO.MarkInfo[keyInDic] = sumMarkLevel.Value;
                }
                msbdBO.PercentAboveAverage = (msbdBO.TotalTest != 0) ? (double)msbdBO.TotalAboveAverage / msbdBO.TotalTest : 0;
                msbdBO.PercentBelowAverage = (msbdBO.TotalTest != 0) ? (double)msbdBO.TotalBelowAverage / msbdBO.TotalTest : 0;
                lsMSBD.Add(msbdBO);
            }
            return lsMSBD;
        }

        /// <summary>
        /// CreateSGDPriodicMarkStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 05/12/2012   2:44 PM
        /// </remarks>
        public List<MarkStatisticsOfSchoolBO> SearchBySupervisingDept(IDictionary<string, object> SearchInfo)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int grade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            IQueryable<MarkStatistic> iqMS = MarkStatisticRepository.All;
            if (ReportCode != null && ReportCode.Trim().Length > 0)
            {
                iqMS = iqMS.Where(o => o.ReportCode.ToUpper().CompareTo(ReportCode.ToUpper()) == 0);
            }
            if (Year != 0)
            {
                iqMS = iqMS.Where(o => o.Year == Year);
            }
            if (Semester != 0)
            {
                iqMS = iqMS.Where(o => o.Semester == Semester);
            }
            //if (SentToSupervisor.HasValue)
            //{
            //    iqMS = iqMS.Where(o => o.SentToSupervisor == SentToSupervisor);
            //}
            if (EducationLevelID != 0)
            {
                iqMS = iqMS.Where(o => o.EducationLevelID == EducationLevelID);
            }

            if (SubjectID != 0)
            {
                iqMS = iqMS.Where(o => o.SubjectID == SubjectID);
            }
            // Lay du lieu theo ngay gui gan nhan
            //var iqMSMax = iqMS.Where(u => u.SentDate == iqMS.Where(v => v.SchoolID == u.SchoolID).Select(v => v.SentDate).Max());

            var lsSP = (from s in SchoolProfileRepository.All
                        join t in SupervisingDeptBusiness.All on s.SupervisingDeptID equals t.SupervisingDeptID
                        join q in iqMS on s.SchoolProfileID equals q.SchoolID into g
                        from j in g.DefaultIfEmpty()
                        where s.IsActive == true
                                        && (ProvinceID > 0 ? s.ProvinceID == ProvinceID : true)
                                        && (SupervisingDeptID > 0 ? (t.SupervisingDeptID == SupervisingDeptID || t.ParentID == SupervisingDeptID) : true)
                                        && (s.EducationGrade & grade) != 0
                                        && (TrainingTypeID > 0 ? s.TrainingTypeID == TrainingTypeID : true)
                        orderby s.SchoolName
                        select new
                        {
                            s.SchoolProfileID,
                            s.SchoolName,
                            MarkLevel00 = j.MarkLevel00.HasValue ? j.MarkLevel00.Value : 0,
                            MarkLevel01 = j.MarkLevel01.HasValue ? j.MarkLevel01.Value : 0,
                            MarkLevel02 = j.MarkLevel02.HasValue ? j.MarkLevel02.Value : 0,
                            MarkLevel03 = j.MarkLevel03.HasValue ? j.MarkLevel03.Value : 0,
                            MarkLevel04 = j.MarkLevel04.HasValue ? j.MarkLevel04.Value : 0,
                            MarkLevel05 = j.MarkLevel05.HasValue ? j.MarkLevel05.Value : 0,
                            MarkLevel06 = j.MarkLevel06.HasValue ? j.MarkLevel06.Value : 0,
                            MarkLevel07 = j.MarkLevel07.HasValue ? j.MarkLevel07.Value : 0,
                            MarkLevel08 = j.MarkLevel08.HasValue ? j.MarkLevel08.Value : 0,
                            MarkLevel09 = j.MarkLevel09.HasValue ? j.MarkLevel09.Value : 0,
                            MarkLevel10 = j.MarkLevel10.HasValue ? j.MarkLevel10.Value : 0,
                            MarkLevel11 = j.MarkLevel11.HasValue ? j.MarkLevel11.Value : 0,
                            MarkLevel12 = j.MarkLevel12.HasValue ? j.MarkLevel12.Value : 0,
                            MarkLevel13 = j.MarkLevel13.HasValue ? j.MarkLevel13.Value : 0,
                            MarkLevel14 = j.MarkLevel14.HasValue ? j.MarkLevel14.Value : 0,
                            MarkLevel15 = j.MarkLevel15.HasValue ? j.MarkLevel15.Value : 0,
                            MarkLevel16 = j.MarkLevel16.HasValue ? j.MarkLevel16.Value : 0,
                            MarkLevel17 = j.MarkLevel17.HasValue ? j.MarkLevel17.Value : 0,
                            MarkLevel18 = j.MarkLevel18.HasValue ? j.MarkLevel18.Value : 0,
                            MarkLevel19 = j.MarkLevel19.HasValue ? j.MarkLevel19.Value : 0,
                            MarkLevel20 = j.MarkLevel20.HasValue ? j.MarkLevel20.Value : 0,
                            TotalBelowAverage = j.BelowAverage.HasValue ? j.BelowAverage.Value : 0,
                            TotalAboveAverage = j.OnAverage.HasValue ? j.OnAverage.Value : 0,
                            MarkTotal = j.MarkTotal.HasValue ? j.MarkTotal.Value : 0
                        }).ToList();
            List<MarkStatisticsOfSchoolBO> lsMSOS = new List<MarkStatisticsOfSchoolBO>();
            foreach (var schoolObj in lsSP)
            {
                MarkStatisticsOfSchoolBO msbdBO = new MarkStatisticsOfSchoolBO();
                msbdBO.SchoolID = schoolObj.SchoolProfileID;
                msbdBO.SchoolName = schoolObj.SchoolName;
                msbdBO.TotalTest = schoolObj.MarkTotal;
                msbdBO.TotalAboveAverage = schoolObj.TotalAboveAverage;
                msbdBO.TotalBelowAverage = schoolObj.TotalBelowAverage;
                for (int i = 0; i <= 20; i++)
                {
                    string keyInsideQuery = "MarkLevel";
                    if (i < 10)
                    {
                        keyInsideQuery += "0" + i;
                    }
                    else
                    {
                        keyInsideQuery += i;
                    }
                    int? sumMarkLevel = (int?)schoolObj.GetType().GetProperty(keyInsideQuery).GetValue(schoolObj, null);
                    sumMarkLevel = sumMarkLevel.HasValue ? sumMarkLevel : 0;

                    string keyInDic = "";
                    decimal tempInt = i * 5;
                    if (i < 2)
                    {
                        keyInDic += "0" + tempInt.ToString();
                    }
                    else
                    {
                        keyInDic += tempInt;
                    }
                    msbdBO.MarkInfo[keyInDic] = sumMarkLevel.Value;
                }
                msbdBO.PercentAboveAverage = (msbdBO.TotalTest != 0) ? (double)msbdBO.TotalAboveAverage / msbdBO.TotalTest : 0;
                msbdBO.PercentBelowAverage = (msbdBO.TotalTest != 0) ? (double)msbdBO.TotalBelowAverage / msbdBO.TotalTest : 0;
                lsMSOS.Add(msbdBO);
            }
            return lsMSOS;
        }


        /// <summary>
        /// SumMark
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="lsTempMarkStatistic">The ls temp mark statistic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 03/12/2012   4:33 PM
        /// </remarks>
        public int? SumMark(int index, IQueryable<MarkStatistic> lsTempMarkStatistic)
        {
            int? result = 0;
            switch (index)
            {
                case 0:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel00);
                    break;

                case 1:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel01);
                    break;

                case 2:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel02);
                    break;

                case 3:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel03);
                    break;

                case 4:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel04);
                    break;

                case 5:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel05);
                    break;

                case 6:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel06);
                    break;

                case 7:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel07);
                    break;

                case 8:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel08);
                    break;

                case 9:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel09);
                    break;

                case 10:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel10);
                    break;

                case 11:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel11);
                    break;

                case 12:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel12);
                    break;

                case 13:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel13);
                    break;

                case 14:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel14);
                    break;

                case 15:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel15);
                    break;

                case 16:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel16);
                    break;

                case 17:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel17);
                    break;

                case 18:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel18);
                    break;

                case 19:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel19);
                    break;

                case 20:
                    result = lsTempMarkStatistic.Sum(o => o.MarkLevel20);
                    break;

                default:
                    result = 0;
                    break;
            }
            return result;
        }

        public ProcessedReport GetMarkStatistics(MarkReportBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraDinhKyCap2;
            string inputParameterHashKey = GetHashKeyForMarkStatistics(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// CreateSGDSemesterMarkStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/12/2012   10:40 AM
        /// </remarks>
        public List<MarkStatisticsByDistrictBO> CreateSGDSemesterMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");

            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            List<MarkStatisticsByDistrictBO> lsMSBDBO = SearchByProvince(SearchInfo);
            lsMSBDBO = lsMSBDBO.OrderBy(o => o.DistrictName).ToList();
            if (lsMSBDBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("SGD_TH_TongHopDiemKiemTraHocKy");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A10", "AD10");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "AD10");
            sheet.Name = "TH_TKDiemHocKy";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP ĐIỂM KIỂM TRA HỌC KỲ TIỂU HỌC - " + thisSubject.SubjectName.ToUpper();
            if (Semester == 0)
            {
                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            }
            string semesterAndYear = ReportUtils.ConvertSemesterForReportName(Semester) + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSBDBO.Count(); j++)
            {
                //Tao style
                sheet.CopyPasteSameSize(templateRange, "A" + (j + 10).ToString());
                MarkStatisticsByDistrictBO item = lsMSBDBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["DistrictName"] = item.DistrictName;
                innerDic["TotalSchool"] = item.TotalSchool;
                innerDic["STT"] = j + 1;
                innerDic["TotalTest"] = item.TotalTest;
                innerDic["TotalBelowAverage"] = item.TotalBelowAverage;
                innerDic["PercentBelowAverage"] = Math.Round(100 * item.PercentBelowAverage, 2, MidpointRounding.AwayFromZero);
                innerDic["TotalAboveAverage"] = item.TotalAboveAverage;
                innerDic["PercentAboveAverage"] = Math.Round(100 * item.PercentAboveAverage, 2, MidpointRounding.AwayFromZero);
                for (int i = 0; i <= 20; i++)
                {
                    string keyInDic = "";
                    decimal tempInt = i * 5;
                    if (i < 2)
                    {
                        keyInDic += "0" + tempInt.ToString();
                    }
                    else
                    {
                        keyInDic += tempInt;
                    }
                    innerDic[keyInDic] = item.MarkInfo[keyInDic];
                }

                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "SGD_TH_TongHopDiemKiemTraHocKy";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //SGD_TH_TongHopDiemKiemTra_[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }

            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID = 0
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",0},
                {"SupervisingDeptID",SupervisingDeptID}

            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSBDBO;
        }

        /// <summary>
        /// CreatePGDSemesterMarkStatisticsPrimary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 07/12/2012   10:40 AM
        /// </remarks>
        public List<MarkStatisticsOfSchoolBO> CreatePGDSemesterMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");

            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");

            List<MarkStatisticsOfSchoolBO> lsMSOSBO = SearchBySupervisingDept(SearchInfo);
            if (lsMSOSBO == null)
            {
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("PGD_TH_TongHopDiemKiemTraHocKy");
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;


            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetTemplate = oBook.GetSheet(1);
            IVTRange templateRange = sheetTemplate.GetRange("A10", "AC10");
            IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemplate, "AC10");
            sheet.Name = "TH_TKDiemHocKy";

            //to do:fill data to sheet
            Dictionary<string, object> HK1MarkDic = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();

            SupervisingDept thisSD = SupervisingDeptBusiness.Find(SupervisingDeptID);
            SubjectCat thisSubject = SubjectCatBusiness.Find(SubjectID);
            Province thisProvince = ProvinceBusiness.Find(ProvinceID);
            string titleAndSubjectName = "TỔNG HỢP ĐIỂM KIỂM TRA HỌC KỲ TIỂU HỌC - " + thisSubject.SubjectName.ToUpper();
            string strSemester = "";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "Học kỳ " + Semester;
            }
            else
            {
                strSemester = "Cả năm";
            }
            string semesterAndYear = strSemester + " năm học " + Year.ToString() + "-" + (Year + 1).ToString();
            string datetime = "ngày {0} tháng {1} năm {2}";
            string provinceAndDate = thisProvince.ProvinceName + "," + string.Format(datetime, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            for (int j = 0; j < lsMSOSBO.Count(); j++)
            {
                // Tao style
                sheet.CopyPasteSameSize(templateRange, "A" + (j + 10).ToString());
                MarkStatisticsOfSchoolBO item = lsMSOSBO[j];
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic["School"] = item.SchoolName;
                innerDic["TotalTest"] = item.TotalTest;
                innerDic["TotalBelowAverage"] = item.TotalBelowAverage;
                innerDic["PercentBelowAverage"] = string.Format("{0:P2}", item.PercentBelowAverage);
                innerDic["TotalAboveAverage"] = item.TotalAboveAverage;
                innerDic["PercentAboveAverage"] = string.Format("{0:P2}", item.PercentAboveAverage);
                innerDic["STT"] = j + 1;
                for (int i = 0; i <= 20; i++)
                {
                    string keyInDic = "";
                    decimal tempInt = i * 5;
                    if (i < 2)
                    {
                        keyInDic += "0" + tempInt.ToString();
                    }
                    else
                    {
                        keyInDic += tempInt;
                    }
                    innerDic[keyInDic] = item.MarkInfo[keyInDic];
                }

                //add
                insideDic.Add(innerDic);
            }

            HK1MarkDic.Add("Rows", insideDic);
            HK1MarkDic.Add("TitleAndSubjectName", titleAndSubjectName);
            HK1MarkDic.Add("SupervisingDeptName", thisSD.SupervisingDeptName);
            HK1MarkDic.Add("SemesterAndYear", semesterAndYear);
            HK1MarkDic.Add("ProvinceNameAndDate", provinceAndDate);
            sheet.FillVariableValue(HK1MarkDic);
            sheet.FitAllColumnsOnOnePage = true;
            sheetTemplate.Delete();

            Stream Data = oBook.ToStream();

            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = "PGD_TH_TongHopDiemKiemTraHocKy";
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;

            //PGD_TH_TongHopDiemKiemTra_[EducationLevel]_[Semester]_[Subject]
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string EducationLevel = "Tất cả";
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelBusiness.Find(EducationLevelID);
                EducationLevel = el.Resolution;
            }
            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            string SubjectCat = sc.SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevel);
            outputNamePattern = outputNamePattern.Replace("[Subject]", SubjectCat);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //insert vào bảng ProcessedReportParameter các bản ghi tương ứng key –
            //value: Year, Semester, EducationLevelID, TrainingTypeID, SubcommitteeID = 0, SubjectID, ProvinceID, DistrictID
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID",EducationLevelID},
                {"TrainingTypeID",TrainingTypeID},
                {"SubcommitteeID",0},
                {"SubjectID",SubjectID},
                {"ProvinceID",ProvinceID},
                {"DistrictID",DistrictID},
                 {"SupervisingDeptID",SupervisingDeptID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return lsMSOSBO;
        }

        /// <summary>
        /// CreateSGDPriodicMarkStatisticsTertiary
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <param name="FileID">The file ID.</param>
        /// <returns>
        /// List{MarkStatisticsOfProvince23}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/11/2012</date>
        public List<MarkStatisticsOfProvince23> CreateSGDPriodicMarkStatisticsTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {

            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? Year = Utils.GetInt(dic, "Year");
            int? trainningTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? semester = Utils.GetInt(dic, "Semester");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            List<MarkStatisticsOfProvince23> lstMarkStatisticOfProvinceSecondary = this.SearchByProvince23(dic);
            lstMarkStatisticOfProvinceSecondary = lstMarkStatisticOfProvinceSecondary.OrderBy(o => o.SchoolName).ToList();
            FileID = 0;
            if (lstMarkStatisticOfProvinceSecondary == null)
            {
                return null;
            }
            string reportCode = SystemParamsInFile.SGD_THPT_THONGKEDIEMKIEMTRADINHKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            string supervisingDeptName = supervisingDept.SupervisingDeptName;
            string districtName = districtID.HasValue && districtID.Value > 0 ? DistrictBusiness.Find(districtID).DistrictName : string.Empty;
            string provinceName = supervisingDept.Province.ProvinceName;
            string trainingName;
            if (trainningTypeID == 0)
            {
                trainingName = "[Tất cả]";
            }
            else
            { trainingName = TrainingTypeBusiness.Find(trainningTypeID).Resolution; }
            string educationName;
            if (educationLevelID == 0)
            { educationName = "Khối [Tất cả]"; }
            else
            { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }

            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            string semesterName = "";

            string Mh = "";
            if (SubjectCatBusiness.Find(SubjectID) != null)
            {
                Mh = SubjectCatBusiness.Find(SubjectID).DisplayName;
            }
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
            else semesterName = "Cả năm";
            string semesterAndAcademic = "Hệ " + trainingName + " - " + educationName + " - " + "Môn " + Mh + " - " + semesterName + " - Năm học " + Year + " - " + (Year + 1);

            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", dateTime);
            sheet.SetCellValue("F5", semesterAndAcademic);
            int index = 0;

            int firstRow = 8;
            string colOrder = "A";
            string colSchoolName = "B";
            string colSendDate = "C";
            string colTotalTest = "D";
            string col00 = "E";
            string col03 = "F";
            string col05 = "G";
            string col08 = "H";
            string col10 = "I";
            string col13 = "J";
            string col15 = "K";
            string col18 = "L";
            string col20 = "M";
            string col23 = "N";
            string col25 = "O";
            string col28 = "P";
            string col30 = "Q";
            string col33 = "R";
            string col35 = "S";
            string col38 = "T";
            string col40 = "U";
            string col43 = "V";
            string col45 = "W";
            string col48 = "X";
            string colTotalBelowAverage = "Y";
            string colPercentBelowAverage = "Z";
            string col50 = "AA";
            string col53 = "AB";
            string col55 = "AC";
            string col58 = "AD";
            string col60 = "AE";
            string col63 = "AF";
            string col65 = "AG";
            string col68 = "AH";
            string col70 = "AI";
            string col73 = "AJ";
            string col75 = "AK";
            string col78 = "AL";
            string col80 = "AM";
            string col83 = "AN";
            string col85 = "AO";
            string col88 = "AP";
            string col90 = "AQ";
            string col93 = "AR";
            string col95 = "AS";
            string col98 = "AT";
            string col100 = "AU";
            string colTotalAboveAverage = "AV";
            string colPercentAboveAverage = "AW";
            IVTRange range = sheet.GetRange("A8", "AW8");
            foreach (var item in lstMarkStatisticOfProvinceSecondary)
            {

                int currentRow = firstRow + index;
                index++;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue(colOrder + currentRow, index);
                sheet.SetCellValue(colSchoolName + currentRow, item.SchoolName);
                sheet.SetCellValue(colSendDate + currentRow, item.SentDate.HasValue ? item.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                sheet.SetCellValue(colTotalTest + currentRow, item.TotalTest);
                sheet.SetCellValue(col00 + currentRow, item.M00);
                sheet.SetCellValue(col03 + currentRow, item.M03);
                sheet.SetCellValue(col05 + currentRow, item.M05);
                sheet.SetCellValue(col08 + currentRow, item.M08);

                sheet.SetCellValue(col10 + currentRow, item.M10);
                sheet.SetCellValue(col13 + currentRow, item.M13);
                sheet.SetCellValue(col15 + currentRow, item.M15);
                sheet.SetCellValue(col18 + currentRow, item.M18);

                sheet.SetCellValue(col20 + currentRow, item.M20);
                sheet.SetCellValue(col23 + currentRow, item.M23);
                sheet.SetCellValue(col25 + currentRow, item.M25);
                sheet.SetCellValue(col28 + currentRow, item.M28);

                sheet.SetCellValue(col30 + currentRow, item.M30);
                sheet.SetCellValue(col33 + currentRow, item.M33);
                sheet.SetCellValue(col35 + currentRow, item.M35);
                sheet.SetCellValue(col38 + currentRow, item.M38);

                sheet.SetCellValue(col40 + currentRow, item.M40);
                sheet.SetCellValue(col43 + currentRow, item.M43);
                sheet.SetCellValue(col45 + currentRow, item.M45);
                sheet.SetCellValue(col48 + currentRow, item.M48);


                sheet.SetCellValue(colTotalBelowAverage + currentRow, item.TotalBelowAverage);
                sheet.SetCellValue(colPercentBelowAverage + currentRow, item.PercentBelowAverage);
                sheet.SetCellValue(col50 + currentRow, item.M50);
                sheet.SetCellValue(col53 + currentRow, item.M53);
                sheet.SetCellValue(col55 + currentRow, item.M55);
                sheet.SetCellValue(col58 + currentRow, item.M58);

                sheet.SetCellValue(col60 + currentRow, item.M60);
                sheet.SetCellValue(col63 + currentRow, item.M63);
                sheet.SetCellValue(col65 + currentRow, item.M65);
                sheet.SetCellValue(col68 + currentRow, item.M68);

                sheet.SetCellValue(col70 + currentRow, item.M70);
                sheet.SetCellValue(col73 + currentRow, item.M73);
                sheet.SetCellValue(col75 + currentRow, item.M75);
                sheet.SetCellValue(col78 + currentRow, item.M78);

                sheet.SetCellValue(col80 + currentRow, item.M80);
                sheet.SetCellValue(col83 + currentRow, item.M83);
                sheet.SetCellValue(col85 + currentRow, item.M85);
                sheet.SetCellValue(col88 + currentRow, item.M88);

                sheet.SetCellValue(col90 + currentRow, item.M90);
                sheet.SetCellValue(col93 + currentRow, item.M93);
                sheet.SetCellValue(col95 + currentRow, item.M95);
                sheet.SetCellValue(col98 + currentRow, item.M98);
                sheet.SetCellValue(col100 + currentRow, item.M100);
                sheet.SetCellValue(colTotalAboveAverage + currentRow, item.TotalAboveAverage);
                sheet.SetCellValue(colPercentAboveAverage + currentRow, item.PercentAboveAverage);
            }
            sheet.FitAllColumnsOnOnePage = true;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(oBook.ToStream());
            pr.SentToSupervisor = true;

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);
            outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + districtName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + trainingName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            return lstMarkStatisticOfProvinceSecondary.ToList();



        }

        /// <summary>
        /// CreateSGDSemesterMarkStatisticsTertiary
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <param name="FileID">The file ID.</param>
        /// <returns>
        /// List{MarkStatisticsOfProvince23}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/11/2012</date>
        public List<MarkStatisticsOfProvince23> CreateSGDSemesterMarkStatisticsTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID)
        {
            int? SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int? Year = Utils.GetInt(dic, "Year");
            int? trainningTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? semester = Utils.GetInt(dic, "Semester");
            int? SubjectID = Utils.GetInt(dic, "SubjectID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            List<MarkStatisticsOfProvince23> lstMarkStatisticOfProvinceSecondary = this.SearchByProvince23(dic);
            lstMarkStatisticOfProvinceSecondary = lstMarkStatisticOfProvinceSecondary.OrderBy(o => o.SchoolName).ToList();
            FileID = 0;
            if (lstMarkStatisticOfProvinceSecondary == null)
            {
                return null;
            }
            string reportCode = SystemParamsInFile.SGD_THPT_THONGKEDIEMKIEMTRAHOCKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            string supervisingDeptName = supervisingDept.SupervisingDeptName;
            string districtName = districtID.HasValue && districtID.Value > 0 ? DistrictBusiness.Find(districtID).DistrictName : string.Empty;
            string provinceName = supervisingDept.Province.ProvinceName;
            string trainingName;
            if (trainningTypeID == 0)
            {
                trainingName = "[Tất cả]";
            }
            else
            { trainingName = TrainingTypeBusiness.Find(trainningTypeID).Resolution; }
            string educationName;
            if (educationLevelID == 0)
            { educationName = "Khối [Tất cả]"; }
            else
            { educationName = EducationLevelBusiness.Find(educationLevelID).Resolution; }

            string dateTime = "Ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            string semesterName = "";
            string Mh = SubjectCatBusiness.Find(SubjectID).DisplayName;
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semesterName = "Học kỳ I";
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semesterName = "Học kỳ II";
            else semesterName = "Cả năm";
            string semesterAndAcademic = "Hệ " + trainingName + " - " + educationName + " - " + "Môn " + Mh + " - " + semesterName + " - Năm học " + Year + " - " + (Year + 1);
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", dateTime);
            sheet.SetCellValue("F5", semesterAndAcademic);
            int index = 0;

            int firstRow = 8;
            string colOrder = "A";
            string colSchoolName = "B";
            string colSendDate = "C";
            string colTotalTest = "D";
            string col00 = "E";
            string col03 = "F";
            string col05 = "G";
            string col08 = "H";
            string col10 = "I";
            string col13 = "J";
            string col15 = "K";
            string col18 = "L";
            string col20 = "M";
            string col23 = "N";
            string col25 = "O";
            string col28 = "P";
            string col30 = "Q";
            string col33 = "R";
            string col35 = "S";
            string col38 = "T";
            string col40 = "U";
            string col43 = "V";
            string col45 = "W";
            string col48 = "X";
            string colTotalBelowAverage = "Y";
            string colPercentBelowAverage = "Z";
            string col50 = "AA";
            string col53 = "AB";
            string col55 = "AC";
            string col58 = "AD";
            string col60 = "AE";
            string col63 = "AF";
            string col65 = "AG";
            string col68 = "AH";
            string col70 = "AI";
            string col73 = "AJ";
            string col75 = "AK";
            string col78 = "AL";
            string col80 = "AM";
            string col83 = "AN";
            string col85 = "AO";
            string col88 = "AP";
            string col90 = "AQ";
            string col93 = "AR";
            string col95 = "AS";
            string col98 = "AT";
            string col100 = "AU";
            string colTotalAboveAverage = "AV";
            string colPercentAboveAverage = "AW";
            IVTRange range = sheet.GetRange("A8", "AW8");
            foreach (var item in lstMarkStatisticOfProvinceSecondary)
            {
                int currentRow = firstRow + index;
                index++;
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, currentRow);
                //Fill dữ liệu
                sheet.SetCellValue(colOrder + currentRow, index);
                sheet.SetCellValue(colSchoolName + currentRow, item.SchoolName);
                sheet.SetCellValue(colSendDate + currentRow, item.SentDate.HasValue ? item.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                sheet.SetCellValue(colTotalTest + currentRow, item.TotalTest);
                sheet.SetCellValue(col00 + currentRow, item.M00);
                sheet.SetCellValue(col03 + currentRow, item.M03);
                sheet.SetCellValue(col05 + currentRow, item.M05);
                sheet.SetCellValue(col08 + currentRow, item.M08);

                sheet.SetCellValue(col10 + currentRow, item.M10);
                sheet.SetCellValue(col13 + currentRow, item.M13);
                sheet.SetCellValue(col15 + currentRow, item.M15);
                sheet.SetCellValue(col18 + currentRow, item.M18);

                sheet.SetCellValue(col20 + currentRow, item.M20);
                sheet.SetCellValue(col23 + currentRow, item.M23);
                sheet.SetCellValue(col25 + currentRow, item.M25);
                sheet.SetCellValue(col28 + currentRow, item.M28);

                sheet.SetCellValue(col30 + currentRow, item.M30);
                sheet.SetCellValue(col33 + currentRow, item.M33);
                sheet.SetCellValue(col35 + currentRow, item.M35);
                sheet.SetCellValue(col38 + currentRow, item.M38);

                sheet.SetCellValue(col40 + currentRow, item.M40);
                sheet.SetCellValue(col43 + currentRow, item.M43);
                sheet.SetCellValue(col45 + currentRow, item.M45);
                sheet.SetCellValue(col48 + currentRow, item.M48);


                sheet.SetCellValue(colTotalBelowAverage + currentRow, item.TotalBelowAverage);
                sheet.SetCellValue(colPercentBelowAverage + currentRow, item.PercentBelowAverage);
                sheet.SetCellValue(col50 + currentRow, item.M50);
                sheet.SetCellValue(col53 + currentRow, item.M53);
                sheet.SetCellValue(col55 + currentRow, item.M55);
                sheet.SetCellValue(col58 + currentRow, item.M58);

                sheet.SetCellValue(col60 + currentRow, item.M60);
                sheet.SetCellValue(col63 + currentRow, item.M63);
                sheet.SetCellValue(col65 + currentRow, item.M65);
                sheet.SetCellValue(col68 + currentRow, item.M68);

                sheet.SetCellValue(col70 + currentRow, item.M70);
                sheet.SetCellValue(col73 + currentRow, item.M73);
                sheet.SetCellValue(col75 + currentRow, item.M75);
                sheet.SetCellValue(col78 + currentRow, item.M78);

                sheet.SetCellValue(col80 + currentRow, item.M80);
                sheet.SetCellValue(col83 + currentRow, item.M83);
                sheet.SetCellValue(col85 + currentRow, item.M85);
                sheet.SetCellValue(col88 + currentRow, item.M88);

                sheet.SetCellValue(col90 + currentRow, item.M90);
                sheet.SetCellValue(col93 + currentRow, item.M93);
                sheet.SetCellValue(col95 + currentRow, item.M95);
                sheet.SetCellValue(col98 + currentRow, item.M98);
                sheet.SetCellValue(col100 + currentRow, item.M100);
                sheet.SetCellValue(colTotalAboveAverage + currentRow, item.TotalAboveAverage);
                sheet.SetCellValue(colPercentAboveAverage + currentRow, item.PercentAboveAverage);
            }
            sheet.FitAllColumnsOnOnePage = true;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(oBook.ToStream());
            pr.SentToSupervisor = true;

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string hk = ReportUtils.ConvertSemesterForReportName(semester.Value);
            outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + districtName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + trainingName : string.Empty));
            outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
            outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
            outputNamePattern = outputNamePattern.Replace("[Subject]", Mh);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            FileID = pr.ProcessedReportID;
            return lstMarkStatisticOfProvinceSecondary.ToList();
        }

        public IQueryable<MarkStatistic> Search(IDictionary<string, object> SearchInfo)
        {

            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            IQueryable<MarkStatistic> lstMarkStatistic = MarkStatisticRepository.All;

            if (SchoolID != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (Year != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.Year == Year);
            }
            if (ReportCode.Length != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.ReportCode.ToLower().Contains(ReportCode.ToLower()));
            }
            if (Semester != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.Semester == Semester);
            }
            if (SubjectID != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.SubjectID == SubjectID);
            }
            if (EducationLevelID != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.EducationLevelID == EducationLevelID);
            }
            //if (SubCommitteeID != 0)
            //{
            //    lstMarkStatistic = lstMarkStatistic.Where(o => o.SubCommitteeID == SubCommitteeID);
            //}
            if (AppliedLevel != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.AppliedLevel == AppliedLevel);
            }
            //if (SentToSupervisor != false)
            //{
            //    lstMarkStatistic = lstMarkStatistic.Where(o => o.SentToSupervisor == SentToSupervisor);
            //}
            if (ProvinceID != 0)
            {
                lstMarkStatistic = lstMarkStatistic.Where(o => o.ProvinceID == ProvinceID);
            }

            IQueryable<MarkStatistic> lstMarkStatistic1 = (from o in lstMarkStatistic
                                                           select o);

            return lstMarkStatistic1;

        }

        public void CreateMarkStatistic(IDictionary<string, object> SearchInfo)
        {

            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int last2digitProvince = UtilsBusiness.GetPartionId(provinceId, 64);
            int districtId = Utils.GetInt(SearchInfo, "DistrictID");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            bool isFemale = Utils.GetBool(SearchInfo, "IsFemale");
            bool isEthnic = Utils.GetBool(SearchInfo, "IsEthnic");
            bool isFemaleAndEthnic = Utils.GetBool(SearchInfo, "IsFemaleAndEthnic");
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            string sUnitId = unitId.ToString();
            //Lay danh sach truong can tong hop
            IQueryable<AcademicYearBO> iqSchool = from ac in AcademicYearBusiness.AllNoTracking
                                                  join sp in SchoolProfileBusiness.AllNoTracking on ac.SchoolID equals sp.SchoolProfileID
                                                  //join su in SupervisingDeptBusiness.AllNoTracking on sp.SupervisingDeptID equals su.SupervisingDeptID
                                                  where sp.ProvinceID == provinceId
                                                        && ac.Year == year
                                                        && sp.IsActive == true
                                                        && ac.IsActive == true
                                                  //&& (su.SupervisingDeptID == unitId || su.TraversalPath.Contains(sUnitId))
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = ac.AcademicYearID,
                                                      SchoolID = ac.SchoolID,
                                                      Year = ac.Year,
                                                      School = sp
                                                  };
            if (districtId > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == districtId);
            }
            List<int> lstEducationGrade = UtilsBusiness.GetEducationGrade(appliedLevel);
            iqSchool = iqSchool.Where(sp => lstEducationGrade.Contains(sp.School.EducationGrade));

            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }
            //Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.Last2DigitNumberProvince == last2digitProvince
                                                              && m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.AppliedLevel == appliedLevel
                                                              && m.EducationLevelID == educationLevelId
                                                              && m.Semester == semester
                                                              && m.SubjectID == subjectID
                                                              && m.MarkType == markType
                                                              && m.ReportCode == ReportCode
                                                              select m;
            if (superVisingDeptId > 0)//Xoa du lieu bao cao phong
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtId && m.SuperVisingDeptID == 1);
            }
            else
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SuperVisingDeptID == 0);
            }
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);
            if (isFemale)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE);
            }
            if (isEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.ETHNIC);
            }
            if (isFemaleAndEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC);
            }
            iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => lstAdd.Contains(m.CriteriaReportID));
            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();
            if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
            {
                if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                {
                    try
                    {
                        context.Configuration.AutoDetectChangesEnabled = false;
                        context.Configuration.ValidateOnSaveEnabled = false;
                        if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                        {
                            MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                            MarkTypeBusiness.Save();
                        }
                    }
                    catch (Exception ex)
                    {
                        
                        LogExtensions.ErrorExt(logger, DateTime.Now, "CreateMarkStatistic", "null", ex);
                    }
                    finally
                    {
                        context.Configuration.AutoDetectChangesEnabled = true;
                        context.Configuration.ValidateOnSaveEnabled = true;
                    }
                }
            }
            string smarkType = "";

            int numTask = UtilsBusiness.GetNumTask;

            int isCommenting = 0;
            //Lay kieu mon khi bao cao la thong ke hoc luc mon
            if (markType == SystemParamsInFile.AdditionReport.HLM)
            {
                var objProvinceSubject = (from ps in ProvinceSubjectBusiness.All
                                          join py in AcademicYearOfProvinceBusiness.All on ps.AcademicYearOfProvinceID equals py.AcademicYearOfProvinceID
                                          where ps.ProvinceID == provinceId
                                          && ps.EducationLevelID == educationLevelId
                                          && ps.SubjectID == subjectID
                                          && py.Year == year
                                          select ps).FirstOrDefault();
                if (objProvinceSubject == null)
                {
                    SubjectCat objSubject = SubjectCatBusiness.Find(subjectID);
                    isCommenting = objSubject.IsCommenting;
                }
                else
                {
                    isCommenting = objProvinceSubject.IsCommenting;
                }
            }
            else
            {
                //Lay cac con diem dinh ky, hoc ky
                if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    if (markType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_DINH_KY)
                    {
                        smarkType = "3,4";
                    }
                    else
                    {
                        smarkType = "4";
                    }
                }
                else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (markType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_DINH_KY)
                    {
                        smarkType = "8,11";
                    }
                    else
                    {
                        smarkType = "11";
                    }
                }
            }
            //Tong hop bao cao diem dinh ky, hoc ky
            Task[] arrTask = new Task[numTask];
            for (int z = 0; z < numTask; z++)
            {
                int currentIndex = z;
                arrTask[z] = Task.Factory.StartNew(() =>
                {
                    #region run task
                    List<AcademicYearBO> lstReportRunTask = lstSchool.Where(p => p.SchoolID % numTask == currentIndex).ToList();
                    if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                    {
                        for (int j = 0; j < lstReportRunTask.Count; j++)
                        {
                            AcademicYearBO objAcademicYearTask = lstReportRunTask[j];
                            if (objAcademicYearTask == null)
                            {
                                continue;
                            }
                            for (int add = 0; add < lstAdd.Count; add++)
                            {
                                InsertMarkStatistc(provinceId, objAcademicYearTask.School.DistrictID.Value, superVisingDeptId, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, objAcademicYearTask.Year,
                                appliedLevel, semester, educationLevelId, subjectID, markType, ReportCode, lstAdd[add], isCommenting, smarkType);
                            }
                        }
                    }
                    #endregion
                });
            }
            Task.WaitAll(arrTask);

        }

        private void InsertMarkStatistc(int provinceId, int districtId, int supervisingDeptId, int schoolId, int academicYearId, int year, int appliedLevel,
                                         int semester, int educationLevelId, int subjectId, int markType, string reportCode, int criteriaReportId, int commenting, string smarkType)
        {
            SMASEntities contenxt1 = new SMASEntities();

            if (markType <= SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                #region "Tong hop du lieu diem dinh ky, hoc ky"
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_INSERT_MARKSTATISTIC",
                                        Connection = conn,
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                    command.Parameters.Add("P_DISTRICT_ID", districtId);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                    command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                    command.Parameters.Add("P_YEAR", year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                    command.Parameters.Add("P_EDUCATION_LEVEL_ID", educationLevelId);
                                    command.Parameters.Add("P_SEMESTER_ID", semester);
                                    command.Parameters.Add("P_SUBJECT_ID", subjectId);
                                    command.Parameters.Add("P_REPORT_CODE", reportCode);
                                    command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                    command.Parameters.Add("P_CRITERIA_REPORT_ID", criteriaReportId);
                                    command.Parameters.Add("P_MARK_TYPES", smarkType);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception ex)
                                {
                                    
                                    LogExtensions.ErrorExt(logger, DateTime.Now, "InsertMarkStatistc", "null", ex);
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region "Tong hop HLM"
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_INSERT_MARKSTATISTIC_AVG",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                    command.Parameters.Add("P_DISTRICT_ID", districtId);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                    command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                    command.Parameters.Add("P_YEAR", year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                    command.Parameters.Add("P_EDUCATION_LEVEL_ID", educationLevelId);
                                    command.Parameters.Add("P_SEMESTER_ID", semester);
                                    command.Parameters.Add("P_SUBJECT_ID", subjectId);
                                    command.Parameters.Add("P_REPORT_CODE", reportCode);
                                    command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                    command.Parameters.Add("P_CRITERIA_REPORT_ID", criteriaReportId);
                                    command.Parameters.Add("P_COMMENTING", commenting);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception ex)
                                {
                                    
                                    string para = "null";
                                    LogExtensions.ErrorExt(logger, DateTime.Now, "InsertMarkStatistc", para, ex);
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
                #endregion
            }

        }

        #region "Xuat excel diem kiem tra dinh ky,hoc ky, HLM"

        public System.IO.Stream ExportMarkStatistic(IDictionary<string, object> SearchInfo)
        {
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int reportType = Utils.GetInt(SearchInfo, "ReportType");
            bool isMale = Utils.GetBool(SearchInfo, "IsFemale");
            bool isEthnic = Utils.GetBool(SearchInfo, "IsEthnic");
            bool isMaleAndEthnic = Utils.GetBool(SearchInfo, "IsFeMaleAndEthnic");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);
            if (isMale)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE);
            }
            if (isEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.ETHNIC);
            }
            if (isMaleAndEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC);
            }

            //ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //string templatePath = ReportUtils.GetTemplatePath(reportDef);
            string templatePath = String.Format("{0}/Phong_So/{1}.xls", SystemParamsInFile.TEMPLATE_FOLDER, reportCode);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            List<MarkStatisticBO> lstMark = GetDataMarkStatistic(SearchInfo, lstAdd);

            Stream excel = null;
            if (reportType <= SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                excel = ExportMarkPeriodAndSemester(oBook, lstMark, lstAdd, SearchInfo);
            }
            else
            {
                excel = ExportMarkAverage(oBook, lstMark, lstAdd, SearchInfo);
            }
            return excel;
        }
        private List<MarkStatisticBO> GetDataMarkStatistic(IDictionary<string, object> SearchInfo, List<int> lstAdd)
        {
            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int last2digitProvince = UtilsBusiness.GetPartionId(provinceId, 64);
            int districtId = Utils.GetInt(SearchInfo, "DistrictID");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            string sUnitId = unitId.ToString();
            List<int> lstLevel = UtilsBusiness.GetEducationGrade(appliedLevel);
            //Lay du lieu tong hop
            IQueryable<MarkStatistic> iqMarkStatistic = from m in MarkStatisticBusiness.AllNoTracking
                                                        where m.Last2DigitNumberProvince == last2digitProvince
                                                         && m.ProvinceID == provinceId
                                                         && m.Year == year
                                                         && m.AppliedLevel == appliedLevel
                                                         && m.EducationLevelID == educationLevelId
                                                         && m.Semester == semester
                                                         && m.SubjectID == subjectID
                                                         && m.MarkType == markType
                                                         && m.ReportCode == ReportCode
                                                         && m.SuperVisingDeptID == superVisingDeptId
                                                         && lstAdd.Contains(m.CriteriaReportID)
                                                        select m;
            if (districtId > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.DistrictID == districtId);
            }
            List<MarkStatistic> lstMarkStatistic = iqMarkStatistic.ToList();

            IQueryable<SchoolProfileBO> iqlstSchool = from sp in SchoolProfileBusiness.AllNoTracking
                                                      join d in DistrictBusiness.AllNoTracking on sp.DistrictID equals d.DistrictID
                                                      where d.ProvinceID == provinceId
                                                       && lstLevel.Contains(sp.EducationGrade)
                                                       && sp.IsActive
                                                      select new SchoolProfileBO
                                                      {
                                                          DistrictCode = d.DistrictCode,
                                                          DistrictName = d.DistrictName,
                                                          DistrictID = d.DistrictID,
                                                          SchoolCode = sp.SchoolCode,
                                                          SchoolProfileID = sp.SchoolProfileID,
                                                          SchoolName = sp.SchoolName
                                                      };
            if (districtId > 0)
            {
                iqlstSchool = iqlstSchool.Where(sp => sp.DistrictID == districtId);
            }
            List<SchoolProfileBO> lstSchool = iqlstSchool.ToList();

            List<MarkStatisticBO> lstMark = new List<MarkStatisticBO>();

            if (lstSchool == null || lstSchool.Count == 0)
            {
                return lstMark;
            }
            SchoolProfileBO objSchoolProfileBO;
            MarkStatistic objMarkStatistic;
            MarkStatisticBO objMarkStatisticBO;
            for (int i = 0; i < lstSchool.Count; i++)
            {
                objSchoolProfileBO = lstSchool[i];
                for (int j = 0; j < lstAdd.Count; j++)
                {
                    objMarkStatistic = lstMarkStatistic.FirstOrDefault(c => c.SchoolID == objSchoolProfileBO.SchoolProfileID &&
                                                                       c.CriteriaReportID == lstAdd[j]);
                    objMarkStatisticBO = new MarkStatisticBO
                    {
                        AcademicYearID = (objMarkStatistic == null) ? 0 : objMarkStatistic.AcademicYearID,
                        AppliedLevel = (objMarkStatistic == null) ? 0 : objMarkStatistic.AppliedLevel,
                        BelowAverage = (objMarkStatistic == null) ? 0 : objMarkStatistic.BelowAverage,
                        CriteriaReportID = (objMarkStatistic == null) ? lstAdd[j] : objMarkStatistic.CriteriaReportID,
                        MarkLevel00 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel00,
                        MarkLevel01 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel01,
                        MarkLevel02 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel02,
                        MarkLevel03 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel03,
                        MarkLevel04 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel04,
                        MarkLevel05 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel05,
                        MarkLevel06 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel06,
                        MarkLevel07 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel07,
                        MarkLevel08 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel08,
                        MarkLevel09 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel09,
                        MarkLevel10 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel10,
                        MarkLevel11 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel11,
                        MarkLevel12 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel12,
                        MarkLevel13 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel13,
                        MarkLevel14 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel14,
                        MarkLevel15 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel15,
                        MarkLevel16 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel16,
                        MarkLevel17 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel17,
                        MarkLevel18 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel18,
                        MarkLevel19 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel19,
                        MarkLevel20 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel20,
                        MarkLevel21 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel21,
                        MarkLevel22 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel22,
                        MarkLevel23 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel23,
                        MarkLevel24 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel24,
                        MarkLevel25 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel25,
                        MarkLevel26 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel26,
                        MarkLevel27 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel27,
                        MarkLevel28 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel28,
                        MarkLevel29 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel29,
                        MarkLevel30 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel30,
                        MarkLevel31 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel31,
                        MarkLevel32 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel32,
                        MarkLevel33 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel33,
                        MarkLevel34 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel34,
                        MarkLevel35 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel35,
                        MarkLevel36 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel36,
                        MarkLevel37 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel37,
                        MarkLevel38 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel38,
                        MarkLevel39 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel39,
                        MarkLevel40 = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkLevel40,
                        MarkTotal = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkTotal,
                        MarkType = (objMarkStatistic == null) ? 0 : objMarkStatistic.MarkType,
                        OnAverage = (objMarkStatistic == null) ? 0 : objMarkStatistic.OnAverage,
                        ProcessedDate = (objMarkStatistic == null) ? null : objMarkStatistic.ProcessedDate,
                        PupilTotal = (objMarkStatistic == null) ? 0 : objMarkStatistic.PupilTotal,
                        ReportCode = (objMarkStatistic == null) ? "" : objMarkStatistic.ReportCode,
                        SchoolCode = objSchoolProfileBO.SchoolCode,
                        SchoolID = objSchoolProfileBO.SchoolProfileID,
                        SchoolName = objSchoolProfileBO.SchoolName,
                        DistrictCode = objSchoolProfileBO.DistrictCode,
                        DistrictName = objSchoolProfileBO.DistrictName,
                        DistrictID = objSchoolProfileBO.DistrictID.Value
                    };
                    lstMark.Add(objMarkStatisticBO);
                }
            }
            return lstMark;
        }

        /// <summary>
        /// Dien du lieu vao file excel cho bao cao thong ke diem kiem tra dinh ky, hoc ky
        /// </summary>
        /// <param name="oBook"></param>
        /// <param name="lstMark">Danh sach diem</param>
        /// <param name="lstAdd">Tieu chi bo sung: 0: binh thuong, 1: hs nu, 2: hs dan toc, 3: hs nu dan toc</param>
        /// <param name="SearchInfo">Cac tieu chi tim kiem</param>
        /// <returns></returns>
        private Stream ExportMarkPeriodAndSemester(IVTWorkbook oBook, List<MarkStatisticBO> lstMark, List<int> lstAdd, IDictionary<string, object> SearchInfo)
        {
            int StartRow = 10;
            //Id don vi quan ly
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");

            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(unitId);
            SubjectCat objSubject = SubjectCatBusiness.Find(subjectID);
            IVTWorksheet sheet = null;
            String Title = String.Format("THỐNG KÊ ĐIỂM KIỂM TRA {0} CÁC TRƯỜNG {1}", markType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_DINH_KY ? "ĐỊNH KỲ" : "HỌC KỲ", appliedLevel == 2 ? "TRUNG HỌC CƠ SỞ" : "TRUNG HỌC PHỔ THÔNG");
            String Subtitle = "";

            List<MarkStatisticBO> lstMarkExport = new List<MarkStatisticBO>();
            List<int> lstDistrict = new List<int>();
            MarkStatisticBO objMark;
            int addition = 0;
            int districtId;
            int row = 0;
            int orderId = 1;
            int rowDistrict = 9;
            List<int> lstSumProvince;

            for (int i = 0; i < lstAdd.Count; i++)
            {

                addition = lstAdd[i];
                if (addition == SystemParamsInFile.AdditionReport.NORMAL)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                    sheet.Name = "TatCa";
                    Subtitle = String.Format("Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                }
                else if (addition == SystemParamsInFile.AdditionReport.FEMALE)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(2));
                    sheet.Name = "HS_Nu";
                    Subtitle = String.Format("Học sinh nữ - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                }
                else if (addition == SystemParamsInFile.AdditionReport.ETHNIC)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(3));
                    sheet.Name = "HS_DanToc";
                    Subtitle = String.Format("Học sinh dân tộc - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                }
                else if (lstAdd[i] == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(4));
                    sheet.Name = "HS_NuDanToc";
                    Subtitle = String.Format("Học sinh nữ dân tộc - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                }
                if (superVisingDeptId == 0)
                {
                    #region "Dien du lieu bao cao so"
                    lstSumProvince = new List<int>();
                    lstDistrict = lstMark.Where(c => c.CriteriaReportID == addition).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                    StartRow = 10;
                    row = StartRow;
                    rowDistrict = 9;
                    orderId = 1;
                    lstSumProvince.Add(rowDistrict);
                    for (int d = 0; d < lstDistrict.Count; d++)
                    {
                        districtId = lstDistrict[d];
                        if (d > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A9", "AW9"), rowDistrict);
                        }
                        lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                        for (int j = 0; j < lstMarkExport.Count; j++)
                        {
                            objMark = lstMarkExport[j];
                            if (j == 0)
                            {
                                sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                            }
                            if (j > 0 || (j == 0 && d > 0))
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A10", "AW10"), row);
                            }
                            FillMarkToExcel(sheet, objMark, row, orderId);
                            (sheet.GetRange(row, 1, row, 49)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                            row = row + 1;
                            orderId = orderId + 1;
                        }

                        if (row > StartRow)
                        {
                            SetFormulaDistrict(sheet, rowDistrict + 1, row - 1, 4, 49);
                        }
                        if (d > 0)
                        {
                            lstSumProvince.Add(rowDistrict);
                        }
                        rowDistrict = row;
                        row = row + 1;

                    }
                    SetFormulaProvince(sheet, 8, lstSumProvince, 4, 49);
                    #endregion
                }
                else
                {
                    #region Dien du lieu theo quan/Huyen
                    StartRow = 9;
                    row = StartRow;
                    orderId = 1;
                    lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition).OrderBy(m => m.SchoolCode).ToList();
                    for (int j = 0; j < lstMarkExport.Count; j++)
                    {
                        objMark = lstMarkExport[j];
                        if (j > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A9", "AW9"), row);
                        }
                        FillMarkToExcel(sheet, objMark, row, orderId);
                        (sheet.GetRange(row, 1, row, 49)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                        row = row + 1;
                        orderId = orderId + 1;
                    }
                    if (row > StartRow)
                    {
                        SetFormulaDistrict(sheet, StartRow, row - 1, 4, 49);
                    }
                    #endregion

                }
                sheet.SetCellValue(2, 1, supervisingDept.SupervisingDeptName.ToUpper());
                sheet.FitAllColumnsOnOnePage = true;
                sheet.SetCellValue(4, 10, Title);
                sheet.SetCellValue(5, 10, Subtitle);
            }
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Dien du lieu vao file excel cho bao cao thong ke HLM
        /// </summary>
        /// <param name="oBook"></param>
        /// <param name="lstMark">Danh sach diem</param>
        /// <param name="lstAdd">Tieu chi bo sung: 0: binh thuong, 1: hs nu, 2: hs dan toc, 3: hs nu dan toc</param>
        /// <param name="SearchInfo">Cac tieu chi tim kiem</param>
        /// <returns></returns>
        private Stream ExportMarkAverage(IVTWorkbook oBook, List<MarkStatisticBO> lstMark, List<int> lstAdd, IDictionary<string, object> SearchInfo)
        {
            int StartRow = 11;
            //Id don vi quan ly
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");

            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(unitId);
            SubjectCat objSubject = SubjectCatBusiness.Find(subjectID);
            IVTWorksheet sheet = null;
            String Subtitle = "";
            int isCommenting = 0;
            //Lay kieu mon khi bao cao la thong ke hoc luc mon
            if (markType == SystemParamsInFile.AdditionReport.HLM)
            {
                var objProvinceSubject = (from ps in ProvinceSubjectBusiness.All
                                          join py in AcademicYearOfProvinceBusiness.All on ps.AcademicYearOfProvinceID equals py.AcademicYearOfProvinceID
                                          where ps.ProvinceID == provinceId
                                          && ps.EducationLevelID == educationLevelId
                                          && ps.SubjectID == subjectID
                                          && py.Year == year
                                          select ps).FirstOrDefault();
                isCommenting = objProvinceSubject == null ? objSubject.IsCommenting : objProvinceSubject.IsCommenting;
            }


            List<MarkStatisticBO> lstMarkExport = new List<MarkStatisticBO>();
            List<int> lstDistrict = new List<int>();
            MarkStatisticBO objMark;
            int addition = 0;
            int districtId;
            int row = 0;
            int orderId = 1;
            int rowDistrict = 10;
            List<int> lstSumProvince;
            string semesterName = (semester == 1) ? "I" : semester == 2 ? "II" : "Cả năm";
            for (int i = 0; i < lstAdd.Count; i++)
            {
                addition = lstAdd[i];
                if (addition == SystemParamsInFile.AdditionReport.NORMAL)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                    sheet.Name = "TatCa";
                    Subtitle = String.Format("Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semesterName, year, year + 1);
                }
                else if (addition == SystemParamsInFile.AdditionReport.FEMALE)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(2));
                    sheet.Name = "HS_Nu";
                    Subtitle = String.Format("Học sinh nữ - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semesterName, year, year + 1);
                }
                else if (addition == SystemParamsInFile.AdditionReport.ETHNIC)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(3));
                    sheet.Name = "HS_DanToc";
                    Subtitle = String.Format("Học sinh dân tộc - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semesterName, year, year + 1);
                }
                else if (lstAdd[i] == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                {
                    sheet = oBook.CopySheetToLast(oBook.GetSheet(4));
                    sheet.Name = "HS_NuDanToc";
                    Subtitle = String.Format("Học sinh nữ dân tộc - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semesterName, year, year + 1);
                }
                if (superVisingDeptId == 0)
                {
                    #region "Dien du lieu bao cao so"
                    lstSumProvince = new List<int>();
                    lstDistrict = lstMark.Where(c => c.CriteriaReportID == addition).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                    StartRow = 11;
                    row = StartRow;
                    rowDistrict = 10;
                    orderId = 1;
                    lstSumProvince.Add(rowDistrict);
                    for (int d = 0; d < lstDistrict.Count; d++)
                    {
                        districtId = lstDistrict[d];
                        if (d > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A10", "U10"), rowDistrict);
                        }
                        lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                        for (int j = 0; j < lstMarkExport.Count; j++)
                        {
                            objMark = lstMarkExport[j];
                            if (j == 0)
                            {
                                sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                            }
                            if (j > 0 || (j == 0 && d > 0))
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A11", "U11"), row);
                            }
                            FillMarkToExcelAverage(sheet, objMark, row, orderId, isCommenting);
                            row = row + 1;
                            orderId = orderId + 1;
                        }
                        if (row > StartRow)
                        {
                            SetFormulaDistrictMarkAverage(sheet, rowDistrict + 1, row - 1, 4, 21);
                        }
                        if (d > 0)
                        {
                            lstSumProvince.Add(rowDistrict);
                        }
                        rowDistrict = row;
                        row = row + 1;
                    }
                    SetFormulaProvinceMarkAverage(sheet, 9, lstSumProvince, 4, 21);
                    #endregion
                }
                else
                {
                    #region Dien du lieu theo quan/Huyen
                    StartRow = 10;
                    row = StartRow;
                    orderId = 1;
                    lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition).OrderBy(m => m.SchoolCode).ToList();
                    for (int j = 0; j < lstMarkExport.Count; j++)
                    {
                        objMark = lstMarkExport[j];
                        if (j > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A10", "U10"), row);
                        }
                        FillMarkToExcelAverage(sheet, objMark, row, orderId, isCommenting);
                        (sheet.GetRange(row, 1, row, 20)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                        row = row + 1;
                        orderId = orderId + 1;
                    }
                    if (row > StartRow)
                    {
                        SetFormulaDistrictMarkAverage(sheet, StartRow, row - 1, 4, 21);
                    }
                    #endregion

                }
                sheet.SetCellValue(2, 1, supervisingDept.SupervisingDeptName.ToUpper());
                sheet.FitAllColumnsOnOnePage = true;
                sheet.SetCellValue(6, 1, Subtitle);
            }
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Dien diem vao file excel
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="objMark"></param>
        /// <param name="row"></param>
        /// <param name="OrderId"></param>
        private void FillMarkToExcel(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int OrderId)
        {
            sheet.SetCellValue(row, 1, OrderId);
            sheet.SetCellValue(row, 2, objMark.DistrictName);
            (sheet.GetRange(row, 2, row, 2)).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.SetCellValue(row, 3, objMark.SchoolName);
            sheet.SetCellValue(row, 4, objMark.MarkTotal > 0 ? objMark.MarkTotal : 0);
            sheet.SetCellValue(row, 5, objMark.MarkLevel00 > 0 ? objMark.MarkLevel00 : 0);
            sheet.SetCellValue(row, 6, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
            sheet.SetCellValue(row, 7, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
            sheet.SetCellValue(row, 8, objMark.MarkLevel03 > 0 ? objMark.MarkLevel03 : 0);
            sheet.SetCellValue(row, 9, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
            sheet.SetCellValue(row, 10, objMark.MarkLevel05 > 0 ? objMark.MarkLevel05 : 0);
            sheet.SetCellValue(row, 11, objMark.MarkLevel06 > 0 ? objMark.MarkLevel06 : 0);
            sheet.SetCellValue(row, 12, objMark.MarkLevel07 > 0 ? objMark.MarkLevel07 : 0);
            sheet.SetCellValue(row, 13, objMark.MarkLevel08 > 0 ? objMark.MarkLevel08 : 0);
            sheet.SetCellValue(row, 14, objMark.MarkLevel09 > 0 ? objMark.MarkLevel09 : 0);
            sheet.SetCellValue(row, 15, objMark.MarkLevel10 > 0 ? objMark.MarkLevel10 : 0);
            sheet.SetCellValue(row, 16, objMark.MarkLevel11 > 0 ? objMark.MarkLevel11 : 0);
            sheet.SetCellValue(row, 17, objMark.MarkLevel12 > 0 ? objMark.MarkLevel12 : 0);
            sheet.SetCellValue(row, 18, objMark.MarkLevel13 > 0 ? objMark.MarkLevel13 : 0);
            sheet.SetCellValue(row, 19, objMark.MarkLevel14 > 0 ? objMark.MarkLevel14 : 0);
            sheet.SetCellValue(row, 20, objMark.MarkLevel15 > 0 ? objMark.MarkLevel15 : 0);
            sheet.SetCellValue(row, 21, objMark.MarkLevel16 > 0 ? objMark.MarkLevel16 : 0);
            sheet.SetCellValue(row, 22, objMark.MarkLevel17 > 0 ? objMark.MarkLevel17 : 0);
            sheet.SetCellValue(row, 23, objMark.MarkLevel18 > 0 ? objMark.MarkLevel18 : 0);
            sheet.SetCellValue(row, 24, objMark.MarkLevel19 > 0 ? objMark.MarkLevel19 : 0);
            //Duoi trung binh
            sheet.SetCellValue(row, 25, objMark.BelowAverage > 0 ? objMark.BelowAverage : 0);

            //sheet.SetCellValue(row, 26, (objMark.PupilTotal > 0 && objMark.BelowAverage > 0) ? objMark.BelowAverage / objMark.PupilTotal : 0);

            sheet.SetCellValue(row, 27, objMark.MarkLevel20 > 0 ? objMark.MarkLevel20 : 0);
            sheet.SetCellValue(row, 28, objMark.MarkLevel21 > 0 ? objMark.MarkLevel21 : 0);
            sheet.SetCellValue(row, 29, objMark.MarkLevel22 > 0 ? objMark.MarkLevel22 : 0);
            sheet.SetCellValue(row, 30, objMark.MarkLevel23 > 0 ? objMark.MarkLevel23 : 0);
            sheet.SetCellValue(row, 31, objMark.MarkLevel24 > 0 ? objMark.MarkLevel24 : 0);
            sheet.SetCellValue(row, 32, objMark.MarkLevel25 > 0 ? objMark.MarkLevel25 : 0);
            sheet.SetCellValue(row, 33, objMark.MarkLevel26 > 0 ? objMark.MarkLevel26 : 0);
            sheet.SetCellValue(row, 34, objMark.MarkLevel27 > 0 ? objMark.MarkLevel27 : 0);
            sheet.SetCellValue(row, 35, objMark.MarkLevel28 > 0 ? objMark.MarkLevel28 : 0);
            sheet.SetCellValue(row, 36, objMark.MarkLevel29 > 0 ? objMark.MarkLevel29 : 0);
            sheet.SetCellValue(row, 37, objMark.MarkLevel30 > 0 ? objMark.MarkLevel30 : 0);
            sheet.SetCellValue(row, 38, objMark.MarkLevel31 > 0 ? objMark.MarkLevel31 : 0);
            sheet.SetCellValue(row, 39, objMark.MarkLevel32 > 0 ? objMark.MarkLevel32 : 0);
            sheet.SetCellValue(row, 40, objMark.MarkLevel33 > 0 ? objMark.MarkLevel33 : 0);
            sheet.SetCellValue(row, 41, objMark.MarkLevel34 > 0 ? objMark.MarkLevel34 : 0);
            sheet.SetCellValue(row, 42, objMark.MarkLevel35 > 0 ? objMark.MarkLevel35 : 0);
            sheet.SetCellValue(row, 43, objMark.MarkLevel36 > 0 ? objMark.MarkLevel36 : 0);
            sheet.SetCellValue(row, 44, objMark.MarkLevel37 > 0 ? objMark.MarkLevel37 : 0);
            sheet.SetCellValue(row, 45, objMark.MarkLevel38 > 0 ? objMark.MarkLevel38 : 0);
            sheet.SetCellValue(row, 46, objMark.MarkLevel39 > 0 ? objMark.MarkLevel39 : 0);
            sheet.SetCellValue(row, 47, objMark.MarkLevel40 > 0 ? objMark.MarkLevel40 : 0);
            //Tren trung binh
            sheet.SetCellValue(row, 48, objMark.OnAverage > 0 ? objMark.OnAverage : 0);
            //sheet.SetCellValue(row, 26, (objMark.PupilTotal > 0 && objMark.OnAverage > 0) ? objMark.OnAverage / objMark.PupilTotal : 0);
        }

        /// <summary>
        /// Dien diem vao file excel cho bao cao HLM
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="objMark"></param>
        /// <param name="row"></param>
        /// <param name="OrderId"></param>
        /// <param name="isCommenting"></param>
        private void FillMarkToExcelAverage(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int orderId, int isCommenting)
        {
            if (isCommenting == 0)
            {
                sheet.SetCellValue(row, 1, orderId);
                sheet.SetCellValue(row, 2, objMark.DistrictName);
                sheet.SetCellValue(row, 3, objMark.SchoolName);
                sheet.SetCellValue(row, 4, objMark.PupilTotal > 0 ? objMark.PupilTotal : 0);
                sheet.SetCellValue(row, 5, objMark.MarkTotal > 0 ? objMark.MarkTotal : 0);
                sheet.SetCellValue(row, 6, objMark.MarkLevel00 > 0 ? objMark.MarkLevel00 : 0);
                sheet.SetCellValue(row, 8, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                sheet.SetCellValue(row, 10, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
                sheet.SetCellValue(row, 12, objMark.MarkLevel03 > 0 ? objMark.MarkLevel03 : 0);
                sheet.SetCellValue(row, 14, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
                sheet.SetCellValue(row, 16, string.Empty);
                sheet.SetCellValue(row, 18, string.Empty);
                sheet.SetCellValue(row, 20, objMark.OnAverage > 0 ? objMark.OnAverage : 0);
                (sheet.GetRange(row, 1, row, 21)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            }
            else
            {
                sheet.SetCellValue(row, 1, orderId);
                sheet.SetCellValue(row, 2, objMark.DistrictName);
                sheet.SetCellValue(row, 3, objMark.SchoolName);
                sheet.SetCellValue(row, 4, objMark.PupilTotal > 0 ? objMark.PupilTotal : 0);
                sheet.SetCellValue(row, 5, objMark.MarkTotal > 0 ? objMark.MarkTotal : 0);
                sheet.SetCellValue(row, 16, objMark.MarkLevel00 > 0 ? objMark.MarkLevel00 : 0);
                sheet.SetCellValue(row, 18, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                (sheet.GetRange(row, 1, row, 21)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            }
        }

        /// <summary>
        /// Dien cong thuc tinh tong so cho cot toan quan
        /// </summary>
        /// <param name="sheet">Sheet can dien</param>
        /// <param name="fromRow">Hang bat dau cua so lieu thong tin truong theo quan/huyen</param>
        /// <param name="toRow">Hang ket thuc cua so lieu truong theo quan/huyen</param>
        private void SetFormulaDistrict(IVTWorksheet sheet, int fromRow, int toRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string sumValue;

            for (int i = fromColumn; i < toColumn; i++)
            {
                //Cot % diem duoi TB khong phai dien cong thuc
                if (i == 26)
                {
                    continue;
                }
                fromCell = new VTVector(fromRow, i);
                toCell = new VTVector(toRow, i);
                sumValue = String.Format("=SUM({0}:{1})", fromCell.ToString(), toCell.ToString());
                sheet.SetFormulaValue(fromRow - 1, i, sumValue);
            }
        }

        /// <summary>
        /// Dien cong thuc tinh tong so cho cot toan tinh
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="fromRow"></param>
        /// <param name="lsttoRow"></param>
        private void SetFormulaProvince(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            int toRow = 0;
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                //Cot % diem duoi TB khong phai dien cong thuc
                if (i == 26)
                {
                    continue;
                }
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }
                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    sheet.SetFormulaValue(fromRow, i, sumValue);

                }
            }
        }


        /// <summary>
        /// Dien cong thuc cho bao cao HLM
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="fromRow"></param>
        /// <param name="toRow"></param>
        /// <param name="fromColumn"></param>
        /// <param name="toColumn"></param>
        private void SetFormulaDistrictMarkAverage(IVTWorksheet sheet, int fromRow, int toRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string sumValue;

            for (int i = fromColumn; i < toColumn; i++)
            {
                //Cot % diem khong phai dien cong thuc
                if (i % 2 == 1 && i > 5)
                {
                    continue;
                }
                fromCell = new VTVector(fromRow, i);
                toCell = new VTVector(toRow, i);
                sumValue = String.Format("=SUM({0}:{1})", fromCell.ToString(), toCell.ToString());
                sheet.SetFormulaValue(fromRow - 1, i, sumValue);
            }
        }

        /// <summary>
        /// Dien cong thuc tinh tong toan tinh/TP cho bao cao HLM
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="fromRow"></param>
        /// <param name="lsttoRow"></param>
        /// <param name="fromColumn"></param>
        /// <param name="toColumn"></param>
        private void SetFormulaProvinceMarkAverage(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            int toRow = 0;
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                //Cot % diem duoi TB khong phai dien cong thuc
                if (i % 2 == 1 && i > 5)
                {
                    continue;
                }
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }
                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    sheet.SetFormulaValue(fromRow, i, sumValue);

                }
            }
        }
        #endregion
        #region Tong hop thong ke tieu hoc
        public void CreatePrimaryStatistic(IDictionary<string, object> SearchInfo)
        {

            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int last2digitProvince = UtilsBusiness.GetPartionId(provinceId, 64);
            int districtId = Utils.GetInt(SearchInfo, "DistrictID");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            bool isFemale = Utils.GetBool(SearchInfo, "IsFemale");
            bool isEthnic = Utils.GetBool(SearchInfo, "IsEthnic");
            bool isFemaleEthnic = Utils.GetBool(SearchInfo, "IsFemaleEthnic");
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            string sUnitId = unitId.ToString();
            int reportTypeId = Utils.GetInt(SearchInfo, "ReportTypeID");
            //Lay danh sach truong can tong hop
            IQueryable<AcademicYearBO> iqSchool = from sp in SchoolProfileBusiness.AllNoTracking
                                                  join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID into des
                                                  from x in des.DefaultIfEmpty()
                                                      //join su in SupervisingDeptBusiness.All on sp.SupervisingDeptID equals su.SupervisingDeptID
                                                  where sp.ProvinceID == provinceId
                                                        && sp.IsActive == true
                                                        && sp.IsActiveSMAS == true
                                                  //&& (su.SupervisingDeptID == unitId || su.TraversalPath.Contains(sUnitId))
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = x != null ? x.AcademicYearID : -1,
                                                      SchoolID = sp.SchoolProfileID,
                                                      Year = year,
                                                      School = sp
                                                  };

            if (districtId > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == districtId);
            }
            int[] numbers = new int[7] { 1, 3, 7, 9, 17, 25, 31 };
            iqSchool = iqSchool.Where(sp => numbers.Contains(sp.School.EducationGrade));

            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }

            //Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.Last2DigitNumberProvince == last2digitProvince
                                                              && m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.AppliedLevel == appliedLevel
                                                              && m.EducationLevelID == educationLevelId
                                                              && m.Semester == semester
                                                              && m.SubjectID == subjectID
                                                              && m.MarkType == markType
                                                              && m.ReportCode == ReportCode
                                                              select m;
            if (superVisingDeptId > 0)//Xoa du lieu bao cao phong
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SuperVisingDeptID == 1);
            }
            else
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SuperVisingDeptID == 0);
            }
            if (districtId > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtId);
            }
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);
            if (isFemale)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE);
            }
            if (isEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.ETHNIC);
            }
            if (isFemaleEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC);
            }
            iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => lstAdd.Contains(m.CriteriaReportID));
            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                {
                    MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                    MarkTypeBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "CreatePrimaryStatistic", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
            int numTask = UtilsBusiness.GetNumTask;


            //Tong hop bao cao diem kiem tra cuoi ky
            Task[] arrTask = new Task[numTask];
            for (int z = 0; z < numTask; z++)
            {
                int currentIndex = z;
                arrTask[z] = Task.Factory.StartNew(() =>
                {
                    #region run task
                    List<AcademicYearBO> lstReportRunTask = lstSchool.Where(p => p.SchoolID % numTask == currentIndex).ToList();
                    if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                    {
                        for (int j = 0; j < lstReportRunTask.Count; j++)
                        {
                            AcademicYearBO objAcademicYearTask = lstReportRunTask[j];
                            if (objAcademicYearTask == null)
                            {
                                continue;
                            }
                            for (int add = 0; add < lstAdd.Count; add++)
                            {
                                InsertPrimaryStatistc(provinceId, objAcademicYearTask.School.DistrictID.Value, superVisingDeptId, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, objAcademicYearTask.Year,
                                appliedLevel, semester, educationLevelId, subjectID, markType, ReportCode, lstAdd[add], reportTypeId);
                            }
                        }
                    }
                    #endregion
                });
            }
            Task.WaitAll(arrTask);
        }
        private void InsertPrimaryStatistc(int provinceId, int districtId, int supervisingDeptId, int schoolId, int academicYearId, int year, int appliedLevel,
                                         int semester, int educationLevelId, int subjectId, int markType, string reportCode, int criteriaReportId, int reportTypeId)
        {
            SMASEntities contenxt1 = new SMASEntities();

            if (markType == 1)
            {
                #region "Tong hop diem kiem tra cuoi ky"
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_INSERT_PRIMARY_STATISTIC",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                    command.Parameters.Add("P_DISTRICT_ID", districtId);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                    command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                    command.Parameters.Add("P_YEAR", year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                    command.Parameters.Add("P_EDUCATION_LEVEL_ID", educationLevelId);
                                    command.Parameters.Add("P_SEMESTER_ID", semester);
                                    command.Parameters.Add("P_SUBJECT_ID", subjectId);
                                    command.Parameters.Add("P_REPORT_CODE", reportCode);
                                    command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                    command.Parameters.Add("P_CRITERIA_REPORT_ID", criteriaReportId);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
                #endregion
            }
            else if (markType == 2)
            {
                #region Tong hop mon hoc va HDGD
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_INSERT_SAE_STATISTIC",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                    command.Parameters.Add("P_DISTRICT_ID", districtId);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                    command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                    command.Parameters.Add("P_YEAR", year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                    command.Parameters.Add("P_EDUCATION_LEVEL_ID", educationLevelId);
                                    command.Parameters.Add("P_SEMESTER_ID", semester);
                                    command.Parameters.Add("P_SUBJECT_ID", subjectId);
                                    command.Parameters.Add("P_REPORT_CODE", reportCode);
                                    command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                    command.Parameters.Add("P_CRITERIA_REPORT_ID", criteriaReportId);
                                    command.Parameters.Add("P_REPORT_TYPE", reportTypeId);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
                #endregion
            }
            else if (markType == 3)
            {
                #region Tong hop nang luc pham chat
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_INSERT_CAPQUA_STATISTIC",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                    command.Parameters.Add("P_DISTRICT_ID", districtId);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                    command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                    command.Parameters.Add("P_YEAR", year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                    command.Parameters.Add("P_EDUCATION_LEVEL_ID", educationLevelId);
                                    command.Parameters.Add("P_SEMESTER_ID", semester);
                                    command.Parameters.Add("P_SUBJECT_ID", subjectId);
                                    command.Parameters.Add("P_REPORT_CODE", reportCode);
                                    command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                    command.Parameters.Add("P_CRITERIA_REPORT_ID", criteriaReportId);
                                    command.Parameters.Add("P_REPORT_TYPE", reportTypeId);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
                #endregion
            }
            else if (markType == 4)
            {
                #region Tong hop ket qua cuoi nam
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_INSERT_SEE_STATISTIC",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                    command.Parameters.Add("P_DISTRICT_ID", districtId);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                    command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                    command.Parameters.Add("P_YEAR", year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                    command.Parameters.Add("P_EDUCATION_LEVEL_ID", educationLevelId);
                                    command.Parameters.Add("P_SEMESTER_ID", 4);
                                    command.Parameters.Add("P_REPORT_CODE", reportCode);
                                    command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                    command.Parameters.Add("P_CRITERIA_REPORT_ID", criteriaReportId);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
                #endregion
            }

        }
        public System.IO.Stream ExportPrimaryStatistic(IDictionary<string, object> SearchInfo)
        {
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int reportType = Utils.GetInt(SearchInfo, "ReportType");
            bool isFemale = Utils.GetBool(SearchInfo, "IsFemale");
            bool isEthnic = Utils.GetBool(SearchInfo, "IsEthnic");
            bool isFemaleEthnic = Utils.GetBool(SearchInfo, "IsFemaleEthnic");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);
            if (isFemale)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE);
            }
            if (isEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.ETHNIC);
            }
            if (isFemaleEthnic)
            {
                lstAdd.Add(SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC);
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            List<MarkStatisticBO> lstMark = GetDataMarkStatisticForPrimary(SearchInfo, lstAdd);

            Stream excel = null;

            excel = ExportPrimaryStatisticStream(oBook, lstMark, lstAdd, SearchInfo);

            return excel;
        }
        private List<MarkStatisticBO> GetDataMarkStatisticForPrimary(IDictionary<string, object> SearchInfo, List<int> lstAdd)
        {
            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int last2digitProvince = UtilsBusiness.GetPartionId(provinceId, 64);
            int districtId = Utils.GetInt(SearchInfo, "DistrictID");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int markType = Utils.GetInt(SearchInfo, "ReportType");
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            string sUnitId = unitId.ToString();

            //Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatisticBO> iqMarkStatistic = (from m in MarkStatisticBusiness.AllNoTracking
                                                           join sp in SchoolProfileBusiness.AllNoTracking on m.SchoolID equals sp.SchoolProfileID
                                                           join ay in AcademicYearBusiness.AllNoTracking on m.AcademicYearID equals ay.AcademicYearID
                                                           join d in DistrictBusiness.AllNoTracking on sp.DistrictID equals d.DistrictID
                                                           where m.Last2DigitNumberProvince == last2digitProvince
                                                           && m.ProvinceID == provinceId
                                                           && m.Year == year
                                                           && m.AppliedLevel == appliedLevel
                                                           && m.EducationLevelID == educationLevelId
                                                           && m.Semester == semester
                                                           && m.MarkType == markType
                                                           && m.ReportCode == ReportCode
                                                           && d.ProvinceID == provinceId
                                                           && (!ay.IsActive.HasValue || (ay.IsActive.HasValue && ay.IsActive.Value))
                                                           //&& (su.SupervisingDeptID == unitId || su.TraversalPath.Contains(sUnitId))
                                                           select new MarkStatisticBO
                                                           {
                                                               AcademicYearID = m.AcademicYearID,
                                                               AppliedLevel = m.AppliedLevel,
                                                               BelowAverage = m.BelowAverage,
                                                               CriteriaReportID = m.CriteriaReportID,
                                                               DistrictName = d.DistrictName,
                                                               DistrictID = d.DistrictID,
                                                               EducationLevelID = m.EducationLevelID,
                                                               Last2DigitNumberProvince = m.Last2DigitNumberProvince,
                                                               MarkLevel00 = m.MarkLevel00,
                                                               MarkLevel01 = m.MarkLevel01,
                                                               MarkLevel02 = m.MarkLevel02,
                                                               MarkLevel03 = m.MarkLevel03,
                                                               MarkLevel04 = m.MarkLevel04,
                                                               MarkLevel05 = m.MarkLevel05,
                                                               MarkLevel06 = m.MarkLevel06,
                                                               MarkLevel07 = m.MarkLevel07,
                                                               MarkLevel08 = m.MarkLevel08,
                                                               MarkLevel09 = m.MarkLevel09,
                                                               MarkLevel10 = m.MarkLevel10,
                                                               MarkLevel11 = m.MarkLevel11,
                                                               MarkLevel12 = m.MarkLevel12,
                                                               MarkLevel13 = m.MarkLevel13,
                                                               MarkLevel14 = m.MarkLevel14,
                                                               MarkLevel15 = m.MarkLevel15,
                                                               MarkLevel16 = m.MarkLevel16,
                                                               MarkLevel17 = m.MarkLevel17,
                                                               MarkLevel18 = m.MarkLevel18,
                                                               MarkLevel19 = m.MarkLevel19,
                                                               MarkLevel20 = m.MarkLevel20,
                                                               MarkLevel21 = m.MarkLevel21,
                                                               MarkLevel22 = m.MarkLevel22,
                                                               MarkLevel23 = m.MarkLevel23,
                                                               MarkLevel24 = m.MarkLevel24,
                                                               MarkLevel25 = m.MarkLevel25,
                                                               MarkLevel26 = m.MarkLevel26,
                                                               MarkLevel27 = m.MarkLevel27,
                                                               MarkLevel28 = m.MarkLevel28,
                                                               MarkLevel29 = m.MarkLevel29,
                                                               MarkLevel30 = m.MarkLevel30,
                                                               MarkLevel31 = m.MarkLevel31,
                                                               MarkLevel32 = m.MarkLevel32,
                                                               MarkLevel33 = m.MarkLevel33,
                                                               MarkLevel34 = m.MarkLevel34,
                                                               MarkLevel35 = m.MarkLevel35,
                                                               MarkLevel36 = m.MarkLevel36,
                                                               MarkLevel37 = m.MarkLevel37,
                                                               MarkLevel38 = m.MarkLevel38,
                                                               MarkLevel39 = m.MarkLevel39,
                                                               MarkLevel40 = m.MarkLevel40,
                                                               MarkTotal = m.MarkTotal,
                                                               MarkType = m.MarkType,
                                                               OnAverage = m.OnAverage,
                                                               ProvinceID = m.ProvinceID,
                                                               ProcessedDate = m.ProcessedDate,
                                                               PupilTotal = m.PupilTotal,
                                                               ReportCode = m.ReportCode,
                                                               SchoolCode = sp.SchoolCode,
                                                               SchoolID = m.SchoolID,
                                                               SchoolName = sp.SchoolName,
                                                               Semester = m.Semester,
                                                               SubjectID = m.SubjectID,
                                                               SuperVisingDeptID = m.SuperVisingDeptID,
                                                               Year = m.Year

                                                           });
            if (superVisingDeptId > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SuperVisingDeptID == 1);
            }
            else
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SuperVisingDeptID == 0);
            }
            if (districtId > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.DistrictID == districtId);
            }
            if (subjectID != 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SubjectID == subjectID);
            }
            iqMarkStatistic = iqMarkStatistic.Where(m => lstAdd.Contains(m.CriteriaReportID));

            List<MarkStatisticBO> lstMark = iqMarkStatistic.Distinct().ToList();
            return lstMark;
        }
        private Stream ExportPrimaryStatisticStream(IVTWorkbook oBook, List<MarkStatisticBO> lstMark, List<int> lstAdd, IDictionary<string, object> SearchInfo)
        {
            int StartRow = 10;
            //Id don vi quan ly
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int subjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int educationLevelId = Utils.GetInt(SearchInfo, "EducationLevelID");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            int reportType = Utils.GetInt(SearchInfo, "ReportType");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");

            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(unitId);
            SubjectCat objSubject = SubjectCatBusiness.Find(subjectID);
            IVTWorksheet sheet = null;
            //String Title = String.Format("TỔNG HỢP ĐIỂM KIỂM TRA ĐIỀU KIỆN CUỐI KỲ - {0}", objSubject.DisplayName.ToUpper());
            //String Subtitle = "";

            List<MarkStatisticBO> lstMarkExport = new List<MarkStatisticBO>();
            List<int> lstDistrict = new List<int>();
            MarkStatisticBO objMark;
            int addition = 0;
            int districtId;
            int row = 0;
            int orderId = 1;
            int rowDistrict = 9;
            List<int> lstSumProvince;

            SubjectCat sc = SubjectCatBusiness.Find(subjectID);
            string title = string.Empty;
            string title2 = string.Empty;
            #region Thong ke diem kiem tra dinh ky
            if (reportType == 1)
            {
                title = "TỔNG HỢP ĐIỂM KIỂM TRA ĐIỀU KIỆN CUỐI KỲ MÔN " + sc.DisplayName.ToUpper() + " {0}";
                title2 = (semester == 1 ? "HKI " : "HKII ") + "NĂM HỌC " + year + "-" + (year + 1);
                for (int i = 0; i < lstAdd.Count; i++)
                {

                    addition = lstAdd[i];
                    if (addition == SystemParamsInFile.AdditionReport.NORMAL)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "TatCa";
                        sheet.SetCellValue("A6", String.Format(title, String.Empty));
                        sheet.SetCellValue("A7", title2);
                        //Subtitle = String.Format("Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.FEMALE)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(2));
                        sheet.Name = "HS_Nu";
                        sheet.SetCellValue("A6", String.Format(title, "HỌC SINH NỮ"));
                        sheet.SetCellValue("A7", title2);
                        //Subtitle = String.Format("Học sinh nữ - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(3));
                        sheet.Name = "HS_DanToc";
                        sheet.SetCellValue("A6", String.Format(title, "HỌC SINH DÂN TỘC"));
                        sheet.SetCellValue("A7", title2);
                        //Subtitle = String.Format("Học sinh dân tộc - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                    }
                    else if (lstAdd[i] == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(4));
                        sheet.Name = "HS_NuDanToc";
                        sheet.SetCellValue("A6", String.Format(title, "HỌC SINH NỮ DÂN TỘC"));
                        sheet.SetCellValue("A7", title2);
                        //Subtitle = String.Format("Học sinh nữ dân tộc - Khối {0} - Môn {1} - Học kỳ {2} - năm học {3} - {4} ", educationLevelId, objSubject.DisplayName, semester, year, year + 1);
                    }

                    if (superVisingDeptId == 0)
                    {
                        #region "Dien du lieu bao cao so"
                        lstSumProvince = new List<int>();
                        lstDistrict = lstMark.Where(c => c.CriteriaReportID == addition).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                        StartRow = 12;
                        row = StartRow;
                        rowDistrict = 11;
                        orderId = 1;
                        lstSumProvince.Add(rowDistrict);
                        for (int d = 0; d < lstDistrict.Count; d++)
                        {
                            districtId = lstDistrict[d];
                            if (d > 0)
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A11", "AC11"), rowDistrict);
                            }
                            lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                            for (int j = 0; j < lstMarkExport.Count; j++)
                            {
                                objMark = lstMarkExport[j];
                                if (j == 0)
                                {
                                    sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                                }
                                if (j > 0 || (j == 0 && d > 0))
                                {
                                    sheet.CopyPasteSameRowHeigh(sheet.GetRange("A12", "AC12"), row);
                                }
                                FillPrimaryMarkToExcel(sheet, objMark, row, orderId);
                                (sheet.GetRange(row, 1, row, 29)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                                row = row + 1;
                                orderId = orderId + 1;
                            }

                            if (row > StartRow)
                            {
                                SetFormulaDistrictPrimary(sheet, rowDistrict + 1, row - 1, 4, 29);
                            }
                            if (d > 0)
                            {
                                lstSumProvince.Add(rowDistrict);
                            }
                            rowDistrict = row;
                            row = row + 1;

                        }
                        SetFormulaProvincePrimary(sheet, 10, lstSumProvince, 4, 29);
                        #endregion
                    }
                    else
                    {
                        #region Dien du lieu theo quan/Huyen
                        StartRow = 11;
                        row = StartRow;
                        orderId = 1;
                        lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition).OrderBy(m => m.SchoolCode).ToList();
                        for (int j = 0; j < lstMarkExport.Count; j++)
                        {
                            objMark = lstMarkExport[j];
                            if (j > 0)
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A11", "AC11"), row);
                            }
                            FillPrimaryMarkToExcel(sheet, objMark, row, orderId);
                            (sheet.GetRange(row, 1, row, 29)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                            row = row + 1;
                            orderId = orderId + 1;
                        }
                        if (row > StartRow)
                        {
                            SetFormulaDistrictPrimary(sheet, StartRow, row - 1, 4, 29);
                        }
                        #endregion

                    }
                    sheet.SetCellValue(3, 1, supervisingDept.SupervisingDeptName.ToUpper());
                    SupervisingDept parrent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                    if (parrent != null)
                    {
                        sheet.SetCellValue(2, 1, parrent.SupervisingDeptName.ToUpper());
                    }
                    string provinceName = supervisingDept.Province.ProvinceName;
                    DateTime date = DateTime.Now;
                    string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
                    string provinceAndDate = provinceName + ", " + day;
                    sheet.SetCellValue(4, 22, provinceAndDate);
                    sheet.FitAllColumnsOnOnePage = true;
                }
                oBook.GetSheet(1).Delete();
                oBook.GetSheet(1).Delete();
                oBook.GetSheet(1).Delete();
                oBook.GetSheet(1).Delete();
            }
            #endregion
            #region Thong ke mon hoc va HDGD
            else if (reportType == 2)
            {
                title2 = "{0}" + " MÔN " + sc.DisplayName.ToUpper() + (markType == 1 ? " GIỮA HỌC KỲ I " : markType == 2 ? " GIỮA HỌC KỲ II " : markType == 3 ? " CUỐI HỌC KỲ I " : " CUỐI HỌC KỲ II ") + "NĂM HỌC " + year + "-" + (year + 1);
                for (int i = 0; i < lstAdd.Count; i++)
                {
                    addition = lstAdd[i];
                    if (addition == SystemParamsInFile.AdditionReport.NORMAL)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "TatCa";
                        sheet.SetCellValue("J7", String.Format(title2, String.Empty));
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.FEMALE)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_Nu";
                        sheet.SetCellValue("J7", String.Format(title2, "HỌC SINH NỮ"));
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_DanToc";
                        sheet.SetCellValue("J7", String.Format(title2, "HỌC SINH DÂN TỘC"));
                    }
                    else if (lstAdd[i] == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_NuDanToc";
                        sheet.SetCellValue("J7", String.Format(title2, "HỌC SINH NỮ DÂN TỘC"));
                    }
                    //Fill du lieu
                    #region fill du lieu mon hoc va HDGD
                    lstSumProvince = new List<int>();
                    lstDistrict = lstMark.Where(c => c.CriteriaReportID == addition).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                    StartRow = 14;
                    row = StartRow;
                    rowDistrict = 13;
                    orderId = 1;
                    lstSumProvince.Add(rowDistrict);
                    for (int d = 0; d < lstDistrict.Count; d++)
                    {
                        districtId = lstDistrict[d];
                        if (d > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A999", "AE999"), rowDistrict);
                        }
                        lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                        for (int j = 0; j < lstMarkExport.Count; j++)
                        {
                            objMark = lstMarkExport[j];
                            if (j == 0)
                            {
                                sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                            }
                            if (j > 0 || (j == 0 && d > 0))
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A1000", "AE1000"), row);
                            }
                            FillSAEToExcel(sheet, objMark, row, orderId);
                            sheet.GetRange(row, 1, row, 31).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                            SetFormulaToSchool(sheet, row, 5, 32);
                            row = row + 1;
                            orderId = orderId + 1;
                        }

                        if (row > StartRow)
                        {
                            SetFormulaDistrictSAE(sheet, rowDistrict + 1, rowDistrict + lstMarkExport.Count, 3, 32);//fill tong so 
                        }
                        if (d > 0)
                        {
                            lstSumProvince.Add(rowDistrict);
                        }
                        rowDistrict = row;
                        row = row + 1;
                    }
                    if (superVisingDeptId == 1)//Tai khoan khong phai so
                    {
                        sheet.SetRowHeight(12, 0);
                    }
                    else
                    {
                        SetFormulaTotal(sheet, 12, lstSumProvince, 3, 32);//fill cot toan so
                    }

                    sheet.DeleteRow(999);
                    sheet.DeleteRow(999);
                    sheet.SetCellValue(3, 1, supervisingDept.SupervisingDeptName.ToUpper());
                    SupervisingDept parrent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                    if (parrent != null)
                    {
                        sheet.SetCellValue(2, 1, parrent.SupervisingDeptName.ToUpper());
                    }
                    string provinceName = supervisingDept.Province.ProvinceName;
                    DateTime date = DateTime.Now;
                    string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
                    string provinceAndDate = provinceName + ", " + day;
                    sheet.SetCellValue("J4", provinceAndDate);
                    sheet.FitAllColumnsOnOnePage = true;
                    #endregion
                }

            }
            #endregion
            #region Thong ke nang luc pham chat
            else if (reportType == 3)
            {
                title2 = "{0}" + (markType == 1 ? " GIỮA HỌC KỲ I " : markType == 2 ? " GIỮA HỌC KỲ II " : markType == 3 ? " CUỐI HỌC KỲ I " : " CUỐI HỌC KỲ II ") + "NĂM HỌC " + year + "-" + (year + 1);
                for (int i = 0; i < lstAdd.Count; i++)
                {
                    addition = lstAdd[i];
                    if (addition == SystemParamsInFile.AdditionReport.NORMAL)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "TatCa";
                        sheet.SetCellValue("J7", String.Format(title2, String.Empty));
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.FEMALE)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_Nu";
                        sheet.SetCellValue("J7", String.Format(title2, "HỌC SINH NỮ"));
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_DanToc";
                        sheet.SetCellValue("J7", String.Format(title2, "HỌC SINH DÂN TỘC"));
                    }
                    else if (lstAdd[i] == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_NuDanToc";
                        sheet.SetCellValue("J7", String.Format(title2, "HỌC SINH NỮ DÂN TỘC"));
                    }
                    #region fill du lieu nang luc pham chat
                    lstSumProvince = new List<int>();
                    lstDistrict = lstMark.Where(c => c.CriteriaReportID == addition).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                    StartRow = 14;
                    row = StartRow;
                    rowDistrict = 13;
                    orderId = 1;
                    lstSumProvince.Add(rowDistrict);
                    for (int d = 0; d < lstDistrict.Count; d++)
                    {
                        districtId = lstDistrict[d];
                        if (d > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A999", "AS999"), rowDistrict);
                        }
                        lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                        for (int j = 0; j < lstMarkExport.Count; j++)
                        {
                            objMark = lstMarkExport[j];
                            if (j == 0)
                            {
                                sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                            }
                            if (j > 0 || (j == 0 && d > 0))
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A1000", "AS1000"), row);
                            }
                            FillCAPQUAToExcel(sheet, objMark, row, orderId);
                            sheet.GetRange(row, 1, row, 45).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                            SetFormulaToSchool(sheet, row, 5, 46);
                            row = row + 1;
                            orderId = orderId + 1;
                        }

                        if (row > StartRow)
                        {
                            SetFormulaDistrictSAE(sheet, rowDistrict + 1, rowDistrict + lstMarkExport.Count, 3, 46);//fill tong so 
                        }
                        if (d > 0)
                        {
                            lstSumProvince.Add(rowDistrict);
                        }
                        rowDistrict = row;
                        row = row + 1;
                    }
                    if (superVisingDeptId == 1)//Tai khoan khong phai so
                    {
                        sheet.SetRowHeight(12, 0);
                    }
                    else
                    {
                        SetFormulaTotal(sheet, 12, lstSumProvince, 3, 46);//fill cot toan so
                    }
                    sheet.DeleteRow(999);
                    sheet.DeleteRow(999);
                    sheet.SetCellValue(3, 1, supervisingDept.SupervisingDeptName.ToUpper());
                    SupervisingDept parrent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                    if (parrent != null)
                    {
                        sheet.SetCellValue(2, 1, parrent.SupervisingDeptName.ToUpper());
                    }
                    string provinceName = supervisingDept.Province.ProvinceName;
                    DateTime date = DateTime.Now;
                    string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
                    string provinceAndDate = provinceName + ", " + day;
                    sheet.SetCellValue("J4", provinceAndDate);
                    sheet.FitAllColumnsOnOnePage = true;
                    #endregion
                }
            }
            #endregion
            #region Thong ke ket qua cuoi nam
            else if (reportType == 4)
            {
                title2 = "{0}" + " KHỐI " + educationLevelId + " NĂM HỌC " + year + "-" + (year + 1);
                for (int i = 0; i < lstAdd.Count; i++)
                {
                    addition = lstAdd[i];
                    if (addition == SystemParamsInFile.AdditionReport.NORMAL)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "TatCa";
                        sheet.SetCellValue("B7", String.Format(title2, String.Empty));
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.FEMALE)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_Nu";
                        sheet.SetCellValue("B7", String.Format(title2, "HỌC SINH NỮ"));
                    }
                    else if (addition == SystemParamsInFile.AdditionReport.ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_DanToc";
                        sheet.SetCellValue("B7", String.Format(title2, "HỌC SINH DÂN TỘC"));
                    }
                    else if (lstAdd[i] == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                    {
                        sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                        sheet.Name = "HS_NuDanToc";
                        sheet.SetCellValue("B7", String.Format(title2, "HỌC SINH NỮ DÂN TỘC"));
                    }
                    #region Fill ket qua cuoi nam
                    lstSumProvince = new List<int>();
                    lstDistrict = lstMark.Where(c => c.CriteriaReportID == addition).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                    StartRow = 13;
                    row = StartRow;
                    rowDistrict = 12;
                    orderId = 1;
                    lstSumProvince.Add(rowDistrict);
                    for (int d = 0; d < lstDistrict.Count; d++)
                    {
                        districtId = lstDistrict[d];
                        if (d > 0)
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A999", "I999"), rowDistrict);
                        }
                        lstMarkExport = lstMark.Where(m => m.CriteriaReportID == addition && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                        for (int j = 0; j < lstMarkExport.Count; j++)
                        {
                            objMark = lstMarkExport[j];
                            if (j == 0)
                            {
                                sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                            }
                            if (j > 0 || (j == 0 && d > 0))
                            {
                                sheet.CopyPasteSameRowHeigh(sheet.GetRange("A1000", "I1000"), row);
                            }
                            FillEVALUATIONToExcel(sheet, objMark, row, orderId);
                            sheet.GetRange(row, 1, row, 9).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                            SetFormulaToSchool(sheet, row, 5, 10);
                            row = row + 1;
                            orderId = orderId + 1;
                        }

                        if (row > StartRow)
                        {
                            SetFormulaDistrictSAE(sheet, rowDistrict + 1, rowDistrict + lstMarkExport.Count, 3, 10);//fill tong so 
                        }
                        if (d > 0)
                        {
                            lstSumProvince.Add(rowDistrict);
                        }
                        rowDistrict = row;
                        row = row + 1;
                    }
                    if (superVisingDeptId == 1)//Tai khoan khong phai so
                    {
                        sheet.SetRowHeight(12, 0);
                    }
                    else
                    {
                        SetFormulaTotal(sheet, 11, lstSumProvince, 3, 10);//fill cot toan so
                    }
                    sheet.DeleteRow(999);
                    sheet.DeleteRow(999);
                    sheet.SetCellValue(3, 1, supervisingDept.SupervisingDeptName.ToUpper());
                    SupervisingDept parrent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                    if (parrent != null)
                    {
                        sheet.SetCellValue(2, 1, parrent.SupervisingDeptName.ToUpper());
                    }
                    string provinceName = supervisingDept.Province.ProvinceName;
                    DateTime date = DateTime.Now;
                    string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
                    string provinceAndDate = provinceName + ", " + day;
                    sheet.SetCellValue("D4", provinceAndDate);
                    sheet.FitAllColumnsOnOnePage = true;
                    #endregion
                }
            }
            #endregion
            oBook.GetSheet(1).Delete();
            return oBook.ToStream();
        }
        private void FillPrimaryMarkToExcel(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int OrderId)
        {
            sheet.SetCellValue(row, 1, OrderId);
            sheet.SetCellValue(row, 2, objMark.DistrictName);
            (sheet.GetRange(row, 2, row, 2)).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.SetCellValue(row, 3, objMark.SchoolName);
            sheet.SetCellValue(row, 4, objMark.MarkTotal.HasValue ? objMark.MarkTotal : 0);
            sheet.SetCellValue(row, 5, objMark.MarkLevel00.HasValue ? objMark.MarkLevel00 : 0);
            sheet.SetCellValue(row, 6, objMark.MarkLevel01.HasValue ? objMark.MarkLevel01 : 0);
            sheet.SetCellValue(row, 7, objMark.MarkLevel02.HasValue ? objMark.MarkLevel02 : 0);
            sheet.SetCellValue(row, 8, objMark.MarkLevel03.HasValue ? objMark.MarkLevel03 : 0);
            sheet.SetCellValue(row, 9, objMark.MarkLevel04.HasValue ? objMark.MarkLevel04 : 0);
            sheet.SetCellValue(row, 10, objMark.MarkLevel05.HasValue ? objMark.MarkLevel05 : 0);
            sheet.SetCellValue(row, 11, objMark.MarkLevel06.HasValue ? objMark.MarkLevel06 : 0);
            sheet.SetCellValue(row, 12, objMark.MarkLevel07.HasValue ? objMark.MarkLevel07 : 0);
            sheet.SetCellValue(row, 13, objMark.MarkLevel08.HasValue ? objMark.MarkLevel08 : 0);
            sheet.SetCellValue(row, 14, objMark.MarkLevel09.HasValue ? objMark.MarkLevel09 : 0);
            //Duoi trung binh
            sheet.SetCellValue(row, 15, objMark.BelowAverage.HasValue ? objMark.BelowAverage : 0);

            sheet.SetCellValue(row, 17, objMark.MarkLevel10.HasValue ? objMark.MarkLevel10 : 0);
            sheet.SetCellValue(row, 18, objMark.MarkLevel11.HasValue ? objMark.MarkLevel11 : 0);
            sheet.SetCellValue(row, 19, objMark.MarkLevel12.HasValue ? objMark.MarkLevel12 : 0);
            sheet.SetCellValue(row, 20, objMark.MarkLevel13.HasValue ? objMark.MarkLevel13 : 0);
            sheet.SetCellValue(row, 21, objMark.MarkLevel14.HasValue ? objMark.MarkLevel14 : 0);
            sheet.SetCellValue(row, 22, objMark.MarkLevel15.HasValue ? objMark.MarkLevel15 : 0);
            sheet.SetCellValue(row, 23, objMark.MarkLevel16.HasValue ? objMark.MarkLevel16 : 0);
            sheet.SetCellValue(row, 24, objMark.MarkLevel17.HasValue ? objMark.MarkLevel17 : 0);
            sheet.SetCellValue(row, 25, objMark.MarkLevel18.HasValue ? objMark.MarkLevel18 : 0);
            sheet.SetCellValue(row, 26, objMark.MarkLevel19.HasValue ? objMark.MarkLevel19 : 0);
            sheet.SetCellValue(row, 27, objMark.MarkLevel20.HasValue ? objMark.MarkLevel20 : 0);
            //Tren trung binh
            sheet.SetCellValue(row, 28, objMark.OnAverage.HasValue ? objMark.OnAverage : 0);
        }
        private void SetFormulaDistrictPrimary(IVTWorksheet sheet, int fromRow, int toRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string sumValue;

            for (int i = fromColumn; i < toColumn; i++)
            {
                //Cot % diem duoi TB khong phai dien cong thuc
                if (i == 16)
                {
                    continue;
                }
                fromCell = new VTVector(fromRow, i);
                toCell = new VTVector(toRow, i);
                sumValue = String.Format("=SUM({0}:{1})", fromCell.ToString(), toCell.ToString());
                sheet.SetFormulaValue(fromRow - 1, i, sumValue);
            }
        }
        private void SetFormulaToSchool(IVTWorksheet sheet, int startRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string formular;

            for (int i = fromColumn; i < toColumn; i += 2)
            {
                fromCell = new VTVector(startRow, 3);
                toCell = new VTVector(startRow, i - 1);
                formular = String.Format("=IF({0}>0,ROUND({1}*100/{2},2),0)", fromCell.ToString(), toCell.ToString(), fromCell.ToString());
                sheet.SetFormulaValue(startRow, i, formular);
            }
        }
        private void SetFormulaDistrictSAE(IVTWorksheet sheet, int fromRow, int toRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string sumValue;
            string formularPercent = "";
            VTVector cellTotal;
            VTVector cellVal;
            cellTotal = new VTVector(fromRow - 1, fromColumn);
            int pos = 4;
            for (int i = fromColumn; i < toColumn; i++)
            {
                fromCell = new VTVector(fromRow, i);
                toCell = new VTVector(toRow, i);
                sumValue = String.Format("=SUM({0}:{1})", fromCell.ToString(), toCell.ToString());
                if (i == fromColumn)
                {
                    sheet.SetFormulaValue(fromRow - 1, i, sumValue);
                }
                else
                {
                    if (i % 2 == 0)
                    {
                        sheet.SetFormulaValue(fromRow - 1, i, sumValue);
                    }
                    else
                    {
                        cellVal = new VTVector(fromRow - 1, pos);
                        formularPercent = String.Format("=IF({0}>0,ROUND({1}*100/{2},2),0)", cellTotal.ToString(), cellVal.ToString(), cellTotal.ToString());
                        sheet.SetFormulaValue(fromRow - 1, i, formularPercent);
                        pos += 2;
                    }
                }
            }
        }
        private void FillSAEToExcel(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int OrderId)
        {
            string formular = string.Empty;
            sheet.SetCellValue(row, 1, OrderId);
            sheet.SetCellValue(row, 2, objMark.SchoolName);
            sheet.SetCellValue(row, 3, objMark.PupilTotal);
            sheet.SetCellValue(row, 4, objMark.MarkLevel11);
            sheet.SetCellValue(row, 6, objMark.MarkLevel12);
            sheet.SetCellValue(row, 8, objMark.MarkLevel13);
            sheet.SetCellValue(row, 10, objMark.MarkLevel10);
            sheet.SetCellValue(row, 12, objMark.MarkLevel09);
            sheet.SetCellValue(row, 14, objMark.MarkLevel08);
            sheet.SetCellValue(row, 16, objMark.MarkLevel07);
            sheet.SetCellValue(row, 18, objMark.MarkLevel06);
            sheet.SetCellValue(row, 20, objMark.MarkLevel05);
            sheet.SetCellValue(row, 22, objMark.MarkLevel04);
            sheet.SetCellValue(row, 24, objMark.MarkLevel03);
            sheet.SetCellValue(row, 26, objMark.MarkLevel02);
            sheet.SetCellValue(row, 28, objMark.MarkLevel01);
            sheet.SetCellValue(row, 30, objMark.BelowAverage);
        }
        private void FillCAPQUAToExcel(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int OrderId)
        {
            string formular = string.Empty;
            sheet.SetCellValue(row, 1, OrderId);
            sheet.SetCellValue(row, 2, objMark.SchoolName);
            sheet.SetCellValue(row, 3, objMark.PupilTotal);
            sheet.SetCellValue(row, 4, objMark.MarkLevel01);
            sheet.SetCellValue(row, 6, objMark.MarkLevel02);
            sheet.SetCellValue(row, 8, objMark.MarkLevel03);
            sheet.SetCellValue(row, 10, objMark.MarkLevel04);
            sheet.SetCellValue(row, 12, objMark.MarkLevel05);
            sheet.SetCellValue(row, 14, objMark.MarkLevel06);
            sheet.SetCellValue(row, 16, objMark.MarkLevel07);
            sheet.SetCellValue(row, 18, objMark.MarkLevel08);
            sheet.SetCellValue(row, 20, objMark.MarkLevel09);
            sheet.SetCellValue(row, 22, objMark.MarkLevel10);
            sheet.SetCellValue(row, 24, objMark.MarkLevel11);
            sheet.SetCellValue(row, 26, objMark.MarkLevel12);
            sheet.SetCellValue(row, 28, objMark.MarkLevel13);
            sheet.SetCellValue(row, 30, objMark.MarkLevel14);
            sheet.SetCellValue(row, 32, objMark.MarkLevel15);
            sheet.SetCellValue(row, 34, objMark.MarkLevel16);
            sheet.SetCellValue(row, 36, objMark.MarkLevel17);
            sheet.SetCellValue(row, 38, objMark.MarkLevel18);
            sheet.SetCellValue(row, 40, objMark.MarkLevel19);
            sheet.SetCellValue(row, 42, objMark.MarkLevel20);
            sheet.SetCellValue(row, 44, objMark.MarkLevel21);
        }
        private void FillEVALUATIONToExcel(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int OrderId)
        {
            string formular = string.Empty;
            sheet.SetCellValue(row, 1, OrderId);
            sheet.SetCellValue(row, 2, objMark.SchoolName);
            sheet.SetCellValue(row, 3, objMark.PupilTotal);
            sheet.SetCellValue(row, 4, objMark.MarkLevel01);
            sheet.SetCellValue(row, 6, objMark.MarkLevel02);
            sheet.SetCellValue(row, 8, objMark.MarkLevel03);
        }

        /// <summary>
        /// Dien cong thuc tinh tong so cho cot toan tinh
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="fromRow"></param>
        /// <param name="lsttoRow"></param>
        private void SetFormulaProvincePrimary(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            int toRow = 0;
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                //Cot % diem duoi TB khong phai dien cong thuc
                if (i == 16)
                {
                    continue;
                }
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }
                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    sheet.SetFormulaValue(fromRow, i, sumValue);

                }
            }
        }
        private void SetFormulaTotal(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            string formularPercent = "";
            int toRow = 0;
            int pos = 4;
            VTVector cellTotal;
            VTVector cellVal;
            cellTotal = new VTVector(fromRow, fromColumn);
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }
                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    if (i == fromColumn)
                    {
                        sheet.SetFormulaValue(fromRow, i, sumValue);
                    }
                    else
                    {
                        if (i % 2 == 0)
                        {
                            sheet.SetFormulaValue(fromRow, i, sumValue);
                        }
                        else
                        {
                            cellVal = new VTVector(fromRow, pos);
                            formularPercent = String.Format("=IF({0}>0,ROUND({1}*100/{2},2),0)", cellTotal, cellVal, cellTotal);
                            sheet.SetFormulaValue(fromRow, i, formularPercent);
                            pos += 2;
                        }
                    }
                }
            }
        }
        #endregion
        #region Tong hop thong ke tin hinh nhap diem cho SGD
        //INSERT DU LIEU VAO BANG MARK_STATISTIC
        public void InsertMarkStatisticBySupervisingDept(IDictionary<string, object> dic, List<SubjectCatBO> lstSubjectCatBO)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Year = Utils.GetInt(dic, "Year");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int UnitID = Utils.GetInt(dic, "UnitID");
            string ReportCode = string.Empty;
            SubjectCatBO objSC = new SubjectCatBO();
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                ReportCode = "So_TH_Thongketinhhinhnhaplieu";
            }
            else
            {
                ReportCode = "So_THCS_Thongketinhhinhnhaplieu";
            }
            IQueryable<AcademicYearBO> iqSchool = from sp in SchoolProfileBusiness.AllNoTracking
                                                  join ay in AcademicYearBusiness.All.Where(o => o.Year == Year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID into des
                                                  from x in des.DefaultIfEmpty()
                                                  where sp.ProvinceID == ProvinceID
                                                        && sp.IsActive == true
                                                        && sp.IsActiveSMAS == true
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = x != null ? x.AcademicYearID : -1,
                                                      SchoolID = sp.SchoolProfileID,
                                                      Year = Year,
                                                      School = sp
                                                  };
            if (SchoolID > 0)
            {
                iqSchool = iqSchool.Where(p => p.SchoolID == SchoolID);
            }
            if (DistrictID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == DistrictID);
            }

            int[] numbers = null;

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                numbers = new int[7] { 1, 3, 7, 9, 17, 25, 31 };
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                numbers = new int[4] { 2, 3, 6, 7 };
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                numbers = new int[3] { 4, 6, 7 };
            }
            iqSchool = iqSchool.Where(sp => numbers.Contains(sp.School.EducationGrade));

            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }

            //Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.ProvinceID == ProvinceID
                                                              && m.Year == Year
                                                              && m.AppliedLevel == AppliedLevelID
                                                              && m.Semester == SemesterID
                                                              && m.ReportCode == ReportCode
                                                              select m;
            if (SupervisingDeptID > 0)//Xoa du lieu bao cao phong
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SuperVisingDeptID == 1);
            }
            else
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SuperVisingDeptID == 0);
            }
            if (DistrictID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == DistrictID);
            }
            if (SchoolID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SchoolID == SchoolID);
            }
            if (SubjectID != 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.SubjectID == SubjectID);
            }
            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                {
                    MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                    MarkStatisticBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertMarkStatisticBySupervisingDept", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
            int numTask = UtilsBusiness.GetNumTask;
            //Tong hop bao cao diem kiem tra cuoi ky
            Task[] arrTask = new Task[numTask];
            for (int z = 0; z < numTask; z++)
            {
                int currentIndex = z;
                arrTask[z] = Task.Factory.StartNew(() =>
                {
                    #region run task
                    List<AcademicYearBO> lstReportRunTask = lstSchool.Where(p => p.SchoolID % numTask == currentIndex).ToList();
                    if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                    {
                        for (int j = 0; j < lstReportRunTask.Count; j++)
                        {
                            AcademicYearBO objAcademicYearTask = lstReportRunTask[j];
                            if (objAcademicYearTask == null)
                            {
                                continue;
                            }
                            for (int k = 0; k < lstSubjectCatBO.Count; k++)
                            {
                                objSC = lstSubjectCatBO[k];
                                if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY || AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                                {

                                    this.RunStoredSupervisingDeptReport(ProvinceID, objAcademicYearTask.School.DistrictID.Value, SupervisingDeptID, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, Year, AppliedLevelID, SemesterID, objSC.SubjectCatID, ReportCode, 1, objSC.IsCommenting.Value);
                                }
                                else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                                {

                                    if (objSC.SubjectCatID > 0)
                                    {
                                        this.RunStoredSupervisingDeptReport(ProvinceID, objAcademicYearTask.School.DistrictID.Value, SupervisingDeptID, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, Year, AppliedLevelID, SemesterID, objSC.SubjectCatID, ReportCode, 2, 0);
                                    }
                                    else if (objSC.SubjectCatID == -1)
                                    {
                                        this.RunStoredSupervisingDeptReport(ProvinceID, objAcademicYearTask.School.DistrictID.Value, SupervisingDeptID, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, Year, AppliedLevelID, SemesterID, objSC.SubjectCatID, ReportCode, 3, 0);
                                    }
                                    else if (objSC.SubjectCatID == -2)
                                    {
                                        this.RunStoredSupervisingDeptReport(ProvinceID, objAcademicYearTask.School.DistrictID.Value, SupervisingDeptID, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, Year, AppliedLevelID, SemesterID, objSC.SubjectCatID, ReportCode, 4, 0);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                });
            }
            Task.WaitAll(arrTask);
        }

        //FILL DU LIEU RA FIL EXCEL LAY DU LIEU TU BANG MARK_STATISTIC
        public Stream ExportSupervisingDeptMarkInputSituation(IDictionary<string, object> dic, List<SubjectCatBO> lstSubjectCatBO)
        {
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Year = Utils.GetInt(dic, "Year");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int UnitID = Utils.GetInt(dic, "UnitID");
            string SupervisingDeptName = Utils.GetString(dic, "SupervisingDeptName");
            string YearName = Utils.GetString(dic, "YearName");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            SubjectCatBO objSC = new SubjectCatBO();
            string templatePath;
            List<MarkStatisticBO> lstMarkStatistic = new List<MarkStatisticBO>();
            string ReportCode = string.Empty;
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                #region Cap 1

                templatePath = String.Format("{0}/Phong_So/{1}.xls", SystemParamsInFile.TEMPLATE_FOLDER, "SGD_TH_TinhHinhNhapDL_HKII");
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Sheet1 = oBook.GetSheet(1);//Mon tinh diem
                IVTWorksheet Sheet2 = oBook.GetSheet(2);//Mon NX
                IVTWorksheet Sheet3 = oBook.GetSheet(3);//Nang luc
                IVTWorksheet Sheet4 = oBook.GetSheet(4);//Pham chat
                IVTWorksheet SheetCopy = oBook.GetSheet(5);//sheet dung de copy
                IVTWorksheet sheet = null;
                ReportCode = "So_TH_Thongketinhhinhnhaplieu";
                dic.Add("ReportCode", ReportCode);
                lstMarkStatistic = this.GetListMarkStatistic(dic);
                List<MarkStatisticBO> lstMarktmp = null;
                for (int i = 0; i < lstSubjectCatBO.Count; i++)
                {
                    objSC = lstSubjectCatBO[i];
                    if (objSC.IsCommenting == 0)//Mon tinh diem
                    {
                        sheet = oBook.CopySheetToLast(Sheet1);
                    }
                    else if (objSC.IsCommenting == 1)//Mon nhan xet
                    {
                        sheet = oBook.CopySheetToLast(Sheet2);
                    }
                    else if (objSC.SubjectCatID == -1)//Nang luc
                    {
                        sheet = oBook.CopySheetToLast(Sheet3);
                    }
                    else if (objSC.SubjectCatID == -2)//Pham chat
                    {
                        sheet = oBook.CopySheetToLast(Sheet4);
                    }
                    lstMarktmp = lstMarkStatistic.Where(p => p.SubjectID == objSC.SubjectCatID).ToList();
                    //Fill thong tin chung
                    sheet.SetCellValue("A2", SupervisingDeptName.ToUpper());
                    string semesterName = SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HỌC KỲ I" : "HỌC KỲ II";
                    string title = string.Format("BẢNG THỐNG KÊ TÌNH HÌNH NHẬP DỮ LIỆU {0}, NĂM HỌC {1}", semesterName, YearName);
                    sheet.SetCellValue("A4", title);
                    sheet.SetCellValue("A5", (objSC.SubjectCatID > 0 ? "MÔN " : "") + objSC.DisplayName.ToUpper());
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.SetCellValue("E7", "Giữa học kỳ I");
                        sheet.SetCellValue("G7", "Cuối học kỳ I");
                    }
                    else
                    {
                        sheet.SetCellValue("E7", "Giữa học kỳ II");
                        sheet.SetCellValue("G7", "Cuối học kỳ II");
                    }
                    this.ExportMarkInputSituationPrimary(sheet, SheetCopy, lstMarktmp, objSC);
                    sheet.Name = Utils.StripVNSignAndSpace(objSC.DisplayName);
                }
                Sheet1.Delete();
                Sheet2.Delete();
                Sheet3.Delete();
                Sheet4.Delete();
                SheetCopy.Delete();
                return oBook.ToStream();
                #endregion
            }
            else
            {
                templatePath = String.Format("{0}/Phong_So/{1}.xls", SystemParamsInFile.TEMPLATE_FOLDER, "SGD_THCS_TKTinhHinhNhapDiem_HKI");
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet currentSheet = oBook.GetSheet(1);
                IVTWorksheet sheet2 = oBook.GetSheet(2);
                ReportCode = "So_THCS_Thongketinhhinhnhaplieu";
                dic.Add("ReportCode", ReportCode);
                lstMarkStatistic = this.GetListMarkStatistic(dic);
                List<MarkStatisticBO> lstMarktmp = null;
                for (int i = 0; i < lstSubjectCatBO.Count; i++)
                {
                    objSC = lstSubjectCatBO[i];
                    lstMarktmp = lstMarkStatistic.Where(p => p.SubjectID == objSC.SubjectCatID).ToList();
                    IVTWorksheet sheet = oBook.CopySheetToLast(currentSheet);
                    sheet.SetCellValue("A2", SupervisingDeptName.ToUpper());
                    string semesterName = SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HỌC KỲ I" : "HỌC KỲ II";
                    string title = string.Format("BẢNG THỐNG KÊ TÌNH HÌNH NHẬP DỮ LIỆU {0}, NĂM HỌC {1}", semesterName, YearName);
                    sheet.SetCellValue("A4", title);
                    sheet.SetCellValue("A5", (objSC.SubjectCatID > 0 ? "MÔN " : "") + objSC.DisplayName.ToUpper());
                    this.ExportMarkInputSituationSecondaryAndTertiary(sheet, sheet2, lstMarktmp, AppliedLevelID);
                    sheet.Name = Utils.StripVNSignAndSpace(objSC.DisplayName);
                }
                currentSheet.Delete();
                sheet2.Delete();
                return oBook.ToStream();
            }

        }
        private void ExportMarkInputSituationSecondaryAndTertiary(IVTWorksheet sheet, IVTWorksheet sheet2, List<MarkStatisticBO> lstMarkStatistic, int AppliedLevelID)
        {
            List<int> lstSumProvince = new List<int>();
            int rowDistrict = 11;
            int startRow = 12;
            lstSumProvince.Add(rowDistrict);
            List<int> lstDistrict = lstMarkStatistic.OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
            List<int> lstSchoolID = new List<int>();
            int DistrictID = 0;
            int SchoolID = 0;
            List<MarkStatisticBO> lstMarkExport = new List<MarkStatisticBO>();
            MarkStatisticBO objMarkExport = null;
            bool isFillDistrict = true;
            string percent = string.Empty;
            decimal val = 0;
            int educationFor = 0;
            int educationForEnd = 0;
            int rowCopyStart = 0;
            int rowCopyEnd = 0;
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                educationFor = GlobalConstants.EDUCATION_LEVEL_ID6;
                educationForEnd = GlobalConstants.EDUCATION_LEVEL_ID10;
                rowCopyStart = 5;
                rowCopyEnd = 8;
            }
            else
            {
                educationFor = GlobalConstants.EDUCATION_LEVEL_ID10;
                educationForEnd = GlobalConstants.EDUCATION_LEVEL_ID13;
                rowCopyStart = 13;
                rowCopyEnd = 15;
            }
            for (int i = 0; i < lstDistrict.Count; i++)
            {
                DistrictID = lstDistrict[i];
                lstSchoolID = lstMarkStatistic.Where(p => p.DistrictID == DistrictID).OrderBy(p => p.SchoolName).Select(p => p.SchoolID).Distinct().ToList();
                if (i > 0)
                {
                    sheet.CopyPasteSameRowHeigh(sheet2.GetRange("A4", "L4"), rowDistrict);
                }
                for (int j = 0; j < lstSchoolID.Count; j++)
                {
                    isFillDistrict = true;
                    SchoolID = lstSchoolID[j];
                    lstMarkExport = lstMarkStatistic.Where(m => m.DistrictID == DistrictID && m.SchoolID == SchoolID).OrderBy(m => m.EducationLevelID).ToList();
                    sheet.CopyPasteSameRowHeigh(sheet2.GetRange("A" + rowCopyStart, "L" + rowCopyEnd), startRow);
                    sheet.SetCellValue(startRow, 1, (j + 1));
                    for (int k = educationFor; k < educationForEnd; k++)//for qua cac khoi
                    {
                        objMarkExport = lstMarkExport.Where(p => p.EducationLevelID == k).FirstOrDefault();
                        if (objMarkExport != null)
                        {
                            if (k == educationFor)
                            {
                                sheet.SetCellValue(startRow, 2, objMarkExport.SchoolName);
                                if (isFillDistrict)
                                {
                                    sheet.SetCellValue(rowDistrict, 1, objMarkExport.DistrictName);
                                    isFillDistrict = false;
                                }
                            }
                            sheet.SetCellValue(startRow, 3, "Khối " + k);
                            sheet.SetCellValue(startRow, 4, objMarkExport.PupilTotal);
                            sheet.SetCellValue(startRow, 5, objMarkExport.MarkLevel01.HasValue ? objMarkExport.MarkLevel01 : 0);
                            val = objMarkExport.PupilTotal > 0 ? (decimal)((decimal)(objMarkExport.MarkLevel02.HasValue ? objMarkExport.MarkLevel02.Value : 0) / objMarkExport.PupilTotal) : 0;
                            percent = Math.Round(val, 2).ToString();
                            sheet.SetCellValue(startRow, 6, percent);
                            sheet.SetCellValue(startRow, 7, objMarkExport.MarkLevel03.HasValue ? objMarkExport.MarkLevel03 : 0);
                            val = objMarkExport.PupilTotal > 0 ? (decimal)((decimal)(objMarkExport.MarkLevel04.HasValue ? objMarkExport.MarkLevel04.Value : 0) / objMarkExport.PupilTotal) : 0;
                            percent = Math.Round(val, 2).ToString();
                            sheet.SetCellValue(startRow, 8, percent);
                            sheet.SetCellValue(startRow, 9, objMarkExport.MarkLevel05.HasValue ? objMarkExport.MarkLevel05 : 0);
                            val = objMarkExport.PupilTotal > 0 ? (decimal)((decimal)(objMarkExport.MarkLevel06.HasValue ? objMarkExport.MarkLevel06.Value : 0) / objMarkExport.PupilTotal) : 0;
                            percent = Math.Round(val, 2).ToString();
                            sheet.SetCellValue(startRow, 10, percent);
                            sheet.SetCellValue(startRow, 11, objMarkExport.MarkLevel08.HasValue && objMarkExport.MarkLevel08.Value > 0 ? 1 : 0);
                            val = objMarkExport.PupilTotal > 0 ? (decimal)((decimal)(objMarkExport.MarkLevel08.HasValue ? objMarkExport.MarkLevel08.Value : 0) / objMarkExport.PupilTotal) : 0;
                            percent = Math.Round(val, 2).ToString();
                            sheet.SetCellValue(startRow, 12, percent);
                            startRow++;
                        }
                    }
                }
                if (i > 0)
                {
                    lstSumProvince.Add(rowDistrict);
                }
                rowDistrict = startRow;
                startRow = startRow + 1;
            }
            string note = "";
            sheet.SetCellValue(startRow, 2, "Ghi chú:");
            sheet.GetRange(startRow, 2, startRow, 2).SetFontStyle(true, null, false, null, false, false);
            note = "- Số con điểm đã nhập (*): Số con điểm lớn nhất đã nhập cho 1 học sinh tương ứng với loại điểm.";
            sheet.SetCellValue(startRow + 1, 2, note);
            note = "- Số con điểm TB đã nhập (**): Tổng các con điểm đã nhập cho tất cả các học sinh / Tổng Số học sinh";
            sheet.SetCellValue(startRow + 2, 2, note);
            sheet.GetRange(startRow, 2, startRow + 2, 2).SetFontName("Times New Roman", 11);
        }
        private void ExportMarkInputSituationPrimary(IVTWorksheet sheet, IVTWorksheet sheetCopy, List<MarkStatisticBO> lstMarkStatistic, SubjectCatBO objSC)
        {
            int rowDistrict = 10;
            List<int> lstSumProvince = new List<int>();
            int startRow = 11;
            int row = startRow;
            lstSumProvince.Add(rowDistrict);
            List<int> lstDistrict = lstMarkStatistic.OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
            List<int> lstSchoolID = new List<int>();
            int DistrictID = 0;
            int SchoolID = 0;
            List<MarkStatisticBO> lstMarkExport = new List<MarkStatisticBO>();
            MarkStatisticBO objMarkExport = null;
            bool isFillDistrict = true;
            for (int i = 0; i < lstDistrict.Count; i++)
            {
                DistrictID = lstDistrict[i];
                #region Mon tinh diem
                if (objSC.IsCommenting == 0)
                {
                    lstSchoolID = lstMarkStatistic.Where(p => p.DistrictID == DistrictID).OrderBy(p => p.SchoolName).Select(p => p.SchoolID).Distinct().ToList();
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("A3", "I3"), rowDistrict);
                    }
                    for (int j = 0; j < lstSchoolID.Count; j++)
                    {
                        isFillDistrict = true;
                        SchoolID = lstSchoolID[j];
                        lstMarkExport = lstMarkStatistic.Where(m => m.DistrictID == DistrictID && m.SchoolID == SchoolID).OrderBy(m => m.EducationLevelID).ToList();
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("A4", "I8"), startRow);
                        sheet.SetCellValue(startRow, 1, (j + 1));
                        for (int k = 1; k < 6; k++)//for qua cac khoi
                        {
                            objMarkExport = lstMarkExport.Where(p => p.EducationLevelID == k).FirstOrDefault();
                            if (objMarkExport != null)
                            {
                                if (k == 1)
                                {
                                    sheet.SetCellValue(startRow, 2, objMarkExport.SchoolName);
                                    if (isFillDistrict)
                                    {
                                        sheet.SetCellValue(rowDistrict, 1, objMarkExport.DistrictName);
                                        isFillDistrict = false;
                                    }
                                }
                                sheet.SetCellValue(startRow, 3, "Khối " + k);
                                sheet.SetCellValue(startRow, 4, objMarkExport.PupilTotal);
                                sheet.SetCellValue(startRow, 5, objMarkExport.MarkLevel01.HasValue ? objMarkExport.MarkLevel01 : 0);
                                if (k > 3 && (objSC.Abbreviation == "Tt" || objSC.Abbreviation == "TV"))
                                {
                                    sheet.SetCellValue(startRow, 6, objMarkExport.MarkLevel02.HasValue ? objMarkExport.MarkLevel02 : 0);
                                }
                                sheet.SetCellValue(startRow, 7, objMarkExport.MarkLevel03.HasValue ? objMarkExport.MarkLevel03 : 0);
                                sheet.SetCellValue(startRow, 8, objMarkExport.MarkLevel04.HasValue ? objMarkExport.MarkLevel04 : 0);
                                sheet.SetCellValue(startRow, 9, objMarkExport.MarkLevel05.HasValue ? objMarkExport.MarkLevel05 : 0);
                                startRow++;
                            }
                        }
                    }
                    if (i > 0)
                    {
                        lstSumProvince.Add(rowDistrict);
                    }
                    //fill formular district
                    this.SetFormulaDistrictSupervisingDept(sheet, rowDistrict + 1, rowDistrict + lstSchoolID.Count * 5, 4, 10);
                    rowDistrict = startRow;
                    startRow = startRow + 1;
                    if (i == lstDistrict.Count - 1)
                    {
                        //Fill tong so
                        this.SetFormulaTotalSupervisingDept(sheet, 9, lstSumProvince, 4, 10);//fill cot toan so
                    }
                }
                #endregion
                #region Mon nhan xet
                else if (objSC.IsCommenting == 1)
                {
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("A12", "H12"), rowDistrict);
                    }
                    lstSchoolID = lstMarkStatistic.Where(p => p.DistrictID == DistrictID).OrderBy(p => p.SchoolName).Select(p => p.SchoolID).Distinct().ToList();
                    for (int j = 0; j < lstSchoolID.Count; j++)
                    {
                        SchoolID = lstSchoolID[j];
                        isFillDistrict = true;
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("A13", "H17"), startRow);
                        sheet.SetCellValue(startRow, 1, (j + 1));
                        lstMarkExport = lstMarkStatistic.Where(m => m.DistrictID == DistrictID && m.SchoolID == SchoolID).OrderBy(m => m.EducationLevelID).ToList();
                        for (int k = 1; k < 6; k++)//for qua cac khoi
                        {
                            objMarkExport = lstMarkExport.Where(p => p.EducationLevelID == k).FirstOrDefault();
                            if (objMarkExport != null)
                            {
                                if (k == 1)
                                {
                                    sheet.SetCellValue(startRow, 2, objMarkExport.SchoolName);
                                    if (isFillDistrict)
                                    {
                                        sheet.SetCellValue(rowDistrict, 1, objMarkExport.DistrictName);
                                        isFillDistrict = false;
                                    }
                                }
                                sheet.SetCellValue(startRow, 3, "Khối " + k);
                                sheet.SetCellValue(startRow, 4, objMarkExport.PupilTotal);
                                sheet.SetCellValue(startRow, 5, objMarkExport.MarkLevel01.HasValue ? objMarkExport.MarkLevel01 : 0);
                                sheet.SetCellValue(startRow, 6, objMarkExport.MarkLevel03.HasValue ? objMarkExport.MarkLevel03 : 0);
                                sheet.SetCellValue(startRow, 7, objMarkExport.MarkLevel04.HasValue ? objMarkExport.MarkLevel04 : 0);
                                sheet.SetCellValue(startRow, 8, objMarkExport.MarkLevel05.HasValue ? objMarkExport.MarkLevel05 : 0);
                                startRow++;
                            }
                        }
                    }
                    if (i > 0)
                    {
                        lstSumProvince.Add(rowDistrict);
                    }
                    //fill formular district
                    this.SetFormulaDistrictSupervisingDept(sheet, rowDistrict + 1, rowDistrict + lstSchoolID.Count * 5, 4, 9);
                    rowDistrict = startRow;
                    startRow = startRow + 1;
                    if (i == lstDistrict.Count - 1)
                    {
                        //Fill tong so
                        this.SetFormulaTotalSupervisingDept(sheet, 9, lstSumProvince, 4, 9);//fill cot toan so
                    }
                }
                #endregion
                #region Nang luc
                else if (objSC.SubjectCatID == -1)
                {
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("K3", "U3"), rowDistrict, 1);
                    }
                    lstSchoolID = lstMarkStatistic.Where(p => p.DistrictID == DistrictID).OrderBy(p => p.SchoolName).Select(p => p.SchoolID).Distinct().ToList();
                    for (int j = 0; j < lstSchoolID.Count; j++)
                    {
                        isFillDistrict = true;
                        SchoolID = lstSchoolID[j];
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("K4", "U8"), startRow, 1);
                        sheet.SetCellValue(startRow, 1, (j + 1));
                        lstMarkExport = lstMarkStatistic.Where(m => m.DistrictID == DistrictID && m.SchoolID == SchoolID).OrderBy(m => m.EducationLevelID).ToList();
                        for (int k = 1; k < 6; k++)//for qua cac khoi
                        {
                            objMarkExport = lstMarkExport.Where(p => p.EducationLevelID == k).FirstOrDefault();
                            if (objMarkExport != null)
                            {
                                if (k == 1)
                                {
                                    sheet.SetCellValue(startRow, 2, objMarkExport.SchoolName);
                                    if (isFillDistrict)
                                    {
                                        sheet.SetCellValue(rowDistrict, 1, objMarkExport.DistrictName);
                                        isFillDistrict = false;
                                    }
                                }
                                sheet.SetCellValue(startRow, 3, "Khối " + k);
                                sheet.SetCellValue(startRow, 4, objMarkExport.PupilTotal);
                                sheet.SetCellValue(startRow, 5, objMarkExport.MarkLevel01.HasValue ? objMarkExport.MarkLevel01 : 0);
                                sheet.SetCellValue(startRow, 6, objMarkExport.MarkLevel02.HasValue ? objMarkExport.MarkLevel02 : 0);
                                sheet.SetCellValue(startRow, 7, objMarkExport.MarkLevel03.HasValue ? objMarkExport.MarkLevel03 : 0);
                                sheet.SetCellValue(startRow, 8, objMarkExport.MarkLevel04.HasValue ? objMarkExport.MarkLevel04 : 0);
                                sheet.SetCellValue(startRow, 9, objMarkExport.MarkLevel05.HasValue ? objMarkExport.MarkLevel05 : 0);
                                sheet.SetCellValue(startRow, 10, objMarkExport.MarkLevel06.HasValue ? objMarkExport.MarkLevel06 : 0);
                                sheet.SetCellValue(startRow, 11, objMarkExport.MarkLevel07.HasValue ? objMarkExport.MarkLevel07 : 0);
                                startRow++;
                            }
                        }
                    }
                    if (i > 0)
                    {
                        lstSumProvince.Add(rowDistrict);
                    }
                    //fill formular district
                    this.SetFormulaDistrictSupervisingDept(sheet, rowDistrict + 1, rowDistrict + lstSchoolID.Count * 5, 4, 12);
                    rowDistrict = startRow;
                    startRow = startRow + 1;
                    if (i == lstDistrict.Count - 1)
                    {
                        //Fill tong so
                        this.SetFormulaTotalSupervisingDept(sheet, 9, lstSumProvince, 4, 12);//fill cot toan so
                    }
                }
                #endregion
                #region Pham chat
                else if (objSC.SubjectCatID == -2)
                {
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("K12", "W12"), rowDistrict, 1);
                    }
                    lstSchoolID = lstMarkStatistic.Where(p => p.DistrictID == DistrictID).OrderBy(p => p.SchoolName).Select(p => p.SchoolID).Distinct().ToList();
                    for (int j = 0; j < lstSchoolID.Count; j++)
                    {
                        isFillDistrict = true;
                        SchoolID = lstSchoolID[j];
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("K13", "W17"), startRow, 1);
                        sheet.SetCellValue(startRow, 1, (j + 1));
                        lstMarkExport = lstMarkStatistic.Where(m => m.DistrictID == DistrictID && m.SchoolID == SchoolID).OrderBy(m => m.EducationLevelID).ToList();
                        for (int k = 1; k < 6; k++)//for qua cac khoi
                        {

                            objMarkExport = lstMarkExport.Where(p => p.EducationLevelID == k).FirstOrDefault();
                            if (objMarkExport != null)
                            {
                                if (k == 1)
                                {
                                    sheet.SetCellValue(startRow, 2, objMarkExport.SchoolName);
                                    if (isFillDistrict)
                                    {
                                        sheet.SetCellValue(rowDistrict, 1, objMarkExport.DistrictName);
                                        isFillDistrict = false;
                                    }
                                }
                                sheet.SetCellValue(startRow, 3, "Khối " + k);
                                sheet.SetCellValue(startRow, 4, objMarkExport.PupilTotal);
                                sheet.SetCellValue(startRow, 5, objMarkExport.MarkLevel01.HasValue ? objMarkExport.MarkLevel01 : 0);
                                sheet.SetCellValue(startRow, 6, objMarkExport.MarkLevel02.HasValue ? objMarkExport.MarkLevel02 : 0);
                                sheet.SetCellValue(startRow, 7, objMarkExport.MarkLevel03.HasValue ? objMarkExport.MarkLevel03 : 0);
                                sheet.SetCellValue(startRow, 8, objMarkExport.MarkLevel04.HasValue ? objMarkExport.MarkLevel04 : 0);
                                sheet.SetCellValue(startRow, 9, objMarkExport.MarkLevel05.HasValue ? objMarkExport.MarkLevel05 : 0);
                                sheet.SetCellValue(startRow, 10, objMarkExport.MarkLevel06.HasValue ? objMarkExport.MarkLevel06 : 0);
                                sheet.SetCellValue(startRow, 11, objMarkExport.MarkLevel07.HasValue ? objMarkExport.MarkLevel07 : 0);
                                sheet.SetCellValue(startRow, 12, objMarkExport.MarkLevel08.HasValue ? objMarkExport.MarkLevel08 : 0);
                                sheet.SetCellValue(startRow, 13, objMarkExport.MarkLevel09.HasValue ? objMarkExport.MarkLevel09 : 0);
                                startRow++;
                            }
                        }
                    }
                    if (i > 0)
                    {
                        lstSumProvince.Add(rowDistrict);
                    }
                    //fill formular district
                    this.SetFormulaDistrictSupervisingDept(sheet, rowDistrict + 1, rowDistrict + lstSchoolID.Count * 5, 4, 14);
                    rowDistrict = startRow;
                    startRow = startRow + 1;
                    if (i == lstDistrict.Count - 1)
                    {
                        //Fill tong so
                        this.SetFormulaTotalSupervisingDept(sheet, 9, lstSumProvince, 4, 14);//fill cot toan so
                    }
                }
                #endregion
            }
        }
        private void RunStoredSupervisingDeptReport(int ProvinceID, int DistrictID, int SupervisingDeptID, int SchoolID, int AcademicYearID, int Year,
            int AppliedLevelID, int SemesterID, int SubjectID, string ReportCode, int TypeRun, int IsCommenting)
        {
            SMASEntities contenxt1 = new SMASEntities();
            #region cap 2 3
            if (TypeRun == 1)
            {
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand();
                                    if (IsCommenting == 0)
                                    {
                                        command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_SUPERVISING_REPORT_ST",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                    }
                                    else
                                    {
                                        command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_SUPERVISING_REPORT_ST_JUDGE",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                    }
                                    command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                    command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", SupervisingDeptID);
                                    command.Parameters.Add("P_SCHOOL_ID", SchoolID);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", AcademicYearID);
                                    command.Parameters.Add("P_YEAR", Year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", AppliedLevelID);
                                    command.Parameters.Add("P_SEMESTER_ID", SemesterID);
                                    command.Parameters.Add("P_SUBJECT_ID", SubjectID);
                                    command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            #endregion
            #region MH va HDGD cap 1
            else if (TypeRun == 2)
            {
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_SUPERVISING_REPORT_SAE_P",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                    command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", SupervisingDeptID);
                                    command.Parameters.Add("P_SCHOOL_ID", SchoolID);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", AcademicYearID);
                                    command.Parameters.Add("P_YEAR", Year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", AppliedLevelID);
                                    command.Parameters.Add("P_SEMESTER_ID", SemesterID);
                                    command.Parameters.Add("P_SUBJECT_ID", SubjectID);
                                    command.Parameters.Add("P_MARK_TYPE_ID", 2);//MH va HDGD
                                    command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            #endregion
            #region Nang luc cap 1
            else if (TypeRun == 3)
            {
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_SUPERVISING_REPORT_CAP_P",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                    command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", SupervisingDeptID);
                                    command.Parameters.Add("P_SCHOOL_ID", SchoolID);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", AcademicYearID);
                                    command.Parameters.Add("P_YEAR", Year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", AppliedLevelID);
                                    command.Parameters.Add("P_SEMESTER_ID", SemesterID);
                                    command.Parameters.Add("P_SUBJECT_ID", -1);//Nang luc
                                    command.Parameters.Add("P_MARK_TYPE_ID", 3);
                                    command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            #endregion
            #region Pham chat cap 1
            else if (TypeRun == 4)
            {
                using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
                {
                    try
                    {
                        if (conn != null)
                        {
                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (OracleTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    OracleCommand command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "SP_SUPERVISING_REPORT_QUA_P",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                    command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                    command.Parameters.Add("P_SUPER_VISING_DEPT_ID", SupervisingDeptID);
                                    command.Parameters.Add("P_SCHOOL_ID", SchoolID);
                                    command.Parameters.Add("P_ACADEMIC_YEAR_ID", AcademicYearID);
                                    command.Parameters.Add("P_YEAR", Year);
                                    command.Parameters.Add("P_APPLIED_LEVEL", AppliedLevelID);
                                    command.Parameters.Add("P_SEMESTER_ID", SemesterID);
                                    command.Parameters.Add("P_SUBJECT_ID", -2);//Pham chat
                                    command.Parameters.Add("P_MARK_TYPE_ID", 3);
                                    command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                    command.ExecuteNonQuery();
                                    tran.Commit();
                                }
                                catch (Exception)
                                {
                                    tran.Rollback();
                                }
                                finally
                                {
                                    tran.Dispose();
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            #endregion
        }
        private List<MarkStatisticBO> GetListMarkStatistic(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int Year = Utils.GetInt(dic, "Year");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int UnitID = Utils.GetInt(dic, "UnitID");
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int educationLevelId = Utils.GetInt(dic, "EducationLevelId");
            int paritionId = UtilsBusiness.GetPartionProvince(ProvinceID);

            DateTime? fromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? toDate = Utils.GetDateTime(dic, "ToDate");
            IQueryable<MarkStatisticBO> iqMarkStatistic = (from m in MarkStatisticBusiness.AllNoTracking
                                                           join sp in SchoolProfileBusiness.AllNoTracking on m.SchoolID equals sp.SchoolProfileID
                                                           join ay in AcademicYearBusiness.AllNoTracking on m.AcademicYearID equals ay.AcademicYearID
                                                           join d in DistrictBusiness.AllNoTracking on sp.DistrictID equals d.DistrictID
                                                           where m.ProvinceID == ProvinceID
                                                           && m.Year == Year
                                                           //&& m.AppliedLevel == AppliedLevelID
                                                           && m.Semester == SemesterID
                                                           && m.ReportCode == ReportCode
                                                           && d.ProvinceID == ProvinceID
                                                           && m.Last2DigitNumberProvince == paritionId
                                                           && (ay.IsActive.HasValue && ay.IsActive.Value)
                                                           && sp.IsActive == true
                                                           select new MarkStatisticBO
                                                           {
                                                               AcademicYearID = m.AcademicYearID,
                                                               AppliedLevel = m.AppliedLevel,
                                                               BelowAverage = m.BelowAverage,
                                                               CriteriaReportID = m.CriteriaReportID,
                                                               DistrictName = d.DistrictName,
                                                               DistrictID = d.DistrictID,
                                                               EducationLevelID = m.EducationLevelID,
                                                               Last2DigitNumberProvince = m.Last2DigitNumberProvince,
                                                               MarkLevel00 = m.MarkLevel00,
                                                               MarkLevel01 = m.MarkLevel01,
                                                               MarkLevel02 = m.MarkLevel02,
                                                               MarkLevel03 = m.MarkLevel03,
                                                               MarkLevel04 = m.MarkLevel04,
                                                               MarkLevel05 = m.MarkLevel05,
                                                               MarkLevel06 = m.MarkLevel06,
                                                               MarkLevel07 = m.MarkLevel07,
                                                               MarkLevel08 = m.MarkLevel08,
                                                               MarkLevel09 = m.MarkLevel09,
                                                               MarkLevel10 = m.MarkLevel10,
                                                               MarkLevel11 = m.MarkLevel11,
                                                               MarkLevel12 = m.MarkLevel12,
                                                               MarkLevel13 = m.MarkLevel13,
                                                               MarkLevel14 = m.MarkLevel14,
                                                               MarkLevel15 = m.MarkLevel15,
                                                               MarkLevel16 = m.MarkLevel16,
                                                               MarkLevel17 = m.MarkLevel17,
                                                               MarkLevel18 = m.MarkLevel18,
                                                               MarkLevel19 = m.MarkLevel19,
                                                               MarkLevel20 = m.MarkLevel20,
                                                               MarkLevel21 = m.MarkLevel21,
                                                               MarkLevel22 = m.MarkLevel22,
                                                               MarkLevel23 = m.MarkLevel23,
                                                               MarkLevel24 = m.MarkLevel24,
                                                               MarkLevel25 = m.MarkLevel25,
                                                               MarkLevel26 = m.MarkLevel26,
                                                               MarkLevel27 = m.MarkLevel27,
                                                               MarkLevel28 = m.MarkLevel28,
                                                               MarkLevel29 = m.MarkLevel29,
                                                               MarkLevel30 = m.MarkLevel30,
                                                               MarkLevel31 = m.MarkLevel31,
                                                               MarkLevel32 = m.MarkLevel32,
                                                               MarkLevel33 = m.MarkLevel33,
                                                               MarkLevel34 = m.MarkLevel34,
                                                               MarkLevel35 = m.MarkLevel35,
                                                               MarkLevel36 = m.MarkLevel36,
                                                               MarkLevel37 = m.MarkLevel37,
                                                               MarkLevel38 = m.MarkLevel38,
                                                               MarkLevel39 = m.MarkLevel39,
                                                               MarkLevel40 = m.MarkLevel40,
                                                               MarkTotal = m.MarkTotal,
                                                               MarkType = m.MarkType,
                                                               OnAverage = m.OnAverage,
                                                               ProvinceID = m.ProvinceID,
                                                               ProcessedDate = m.ProcessedDate,
                                                               PupilTotal = m.PupilTotal,
                                                               ReportCode = m.ReportCode,
                                                               SchoolCode = sp.SchoolCode,
                                                               SchoolID = m.SchoolID,
                                                               SchoolName = sp.SchoolName,
                                                               Semester = m.Semester,
                                                               SubjectID = m.SubjectID,
                                                               SuperVisingDeptID = m.SuperVisingDeptID,
                                                               Year = m.Year,
                                                               SubjectCat = m.SubjectCat
                                                           });
            if (SupervisingDeptID > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SuperVisingDeptID == 1);
            }
            else
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SuperVisingDeptID == 0);
            }
            if (DistrictID > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.DistrictID == DistrictID);
            }
            if (SubjectID != 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SubjectID == SubjectID);
            }
            if (SchoolID > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.SchoolID == SchoolID);
            }
            if (AppliedLevelID > 0)
            {
                List<int> lstEducationGrade = new List<int>();
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(AppliedLevelID));
                iqMarkStatistic = iqMarkStatistic.Where(m =>lstEducationGrade.Contains(m.AppliedLevel));
            }
            else
            {
                List<int> lstEducationGrade = new List<int>();
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_PRIMARY));
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_SECONDARY));
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_TERTIARY));
                iqMarkStatistic = iqMarkStatistic.Where(m => lstEducationGrade.Contains(m.AppliedLevel));
            }

            if (educationLevelId > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.EducationLevelID == educationLevelId);
            }

            if (fromDate.HasValue && toDate.HasValue)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate);
            }
            return iqMarkStatistic.Distinct().ToList();
        }
        private void SetFormulaTotalSupervisingDept(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            int toRow = 0;
            VTVector cellTotal;
            cellTotal = new VTVector(fromRow, fromColumn);
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }

                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    sheet.SetFormulaValue(fromRow, i, sumValue);
                }
            }
        }
        private void SetFormulaDistrictSupervisingDept(IVTWorksheet sheet, int fromRow, int toRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string sumValue;
            for (int i = fromColumn; i < toColumn; i++)
            {
                fromCell = new VTVector(fromRow, i);
                toCell = new VTVector(toRow, i);
                sumValue = String.Format("=SUM({0}:{1})", fromCell.ToString(), toCell.ToString());
                sheet.SetFormulaValue(fromRow - 1, i, sumValue);
            }
        }
        #endregion


        public void InsertReportVariablePupilBySuperVising(IDictionary<string, object> dic)
        {
            int? appliedLevelID = Utils.GetInt(dic, "AppliedLevel");
            int? monthID = Utils.GetInt(dic, "MonthID");
            int? year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool showSheetFemale = Utils.GetBool(dic, "SheetPupilFemale");
            bool showSheetEthenic = Utils.GetBool(dic, "SheetPupilEthnic");
            bool showSheetFemaleEthenic = Utils.GetBool(dic, "SheetPupilFemaleEthnic");



            List<int> lstReportType = new List<int>();
            lstReportType.Add(SystemParamsInFile.AdditionReport.NORMAL);
            if (showSheetFemale)
            {
                lstReportType.Add(SystemParamsInFile.AdditionReport.FEMALE);
            }
            if (showSheetEthenic)
            {
                lstReportType.Add(SystemParamsInFile.AdditionReport.ETHNIC);
            }

            if (showSheetFemaleEthenic)
            {
                lstReportType.Add(SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC);
            }


            Province objProvince = ProvinceBusiness.Find(provinceId);



            IQueryable<AcademicYearBO> iqSchool = from ac in AcademicYearBusiness.AllNoTracking
                                                  join sp in SchoolProfileBusiness.AllNoTracking on ac.SchoolID equals sp.SchoolProfileID

                                                  where sp.ProvinceID == provinceId
                                                        && ac.Year == year
                                                        && sp.IsActive == true
                                                        && ac.IsActive == true
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = ac.AcademicYearID,
                                                      SchoolID = ac.SchoolID,
                                                      Year = ac.Year,
                                                      FirstSemesterStartDate = ac.FirstSemesterStartDate,
                                                      School = sp,
                                                      EducationGrade = sp.EducationGrade
                                                  };
            if (districtID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == districtID);
            }
            List<int> lstEducationGrade = new List<int>();
            if (appliedLevelID.HasValue && appliedLevelID.Value > 0)
            {
                lstEducationGrade = UtilsBusiness.GetEducationGrade(appliedLevelID.Value);
                iqSchool = iqSchool.Where(sp => lstEducationGrade.Contains(sp.School.EducationGrade));
            }
            else
            {
                                
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_PRIMARY));
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_SECONDARY));
                lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_TERTIARY));
                iqSchool = iqSchool.Where(sp => lstEducationGrade.Contains(sp.School.EducationGrade));
            }


            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }



            #region lay ngay bao cao
            DateTime currenDate = DateTime.Now;
            int _month = 0;
            int _year = 0;
            if (monthID.HasValue && monthID.Value > 0)
            {
                string _monthID = monthID.ToString();
                int _length = _monthID.Length;
                string _strMonth = string.Empty;
                string _strYear = string.Empty;

                if (_length == 5)
                    _strMonth = _monthID.Substring(4, 1).ToString();
                else
                    _strMonth = _monthID.Substring(4, 2).ToString();

                _strYear = _monthID.Substring(0, 4).ToString();

                Int32.TryParse(_strMonth, out _month);
                Int32.TryParse(_strYear, out _year);
            }

            int _lastMonth = 0;
            int _lastYear = 0;
            if (_month == 1)
            {
                _lastYear = _year - 1;
                _lastMonth = 12;
            }
            else
            {
                _lastYear = _year;
                _lastMonth = _month - 1;
            }

            DateTime valueMinDate = DateTime.Now;
            DateTime valueMaxDate = DateTime.Now;
            DateTime valueMinLastMonth = new DateTime(_lastYear, _lastMonth, 1);
            DateTime valueMaxLastMonth = valueMinLastMonth.AddMonths(1).AddDays(-1);
            DateTime valueOfMonthSelected = new DateTime(_year, _month, 1);
            DateTime valueOfCurrent = new DateTime(currenDate.Year, currenDate.Month, 1);

            if (valueOfMonthSelected == valueOfCurrent)
            {
                valueMinDate = new DateTime(currenDate.Year, currenDate.Month, 1);
                valueMaxDate = DateTime.Now;
            }
            else
            {
                valueMinDate = new DateTime(_year, _month, 1);
                valueMaxDate = valueMinDate.AddMonths(1).AddDays(-1);
            }
            #endregion

            int partitionId = UtilsBusiness.GetPartionProvince(provinceId);
            var iqMark = MarkStatisticBusiness.All.Where(x => x.ReportCode == SystemParamsInFile.AdditionReport.SGD_TKBienDongHS
                                                            && lstReportType.Contains(x.CriteriaReportID)
                                                            && x.ProvinceID == provinceId
                                                            && x.Last2DigitNumberProvince == partitionId
                                                            && x.Year == year);
            if (districtID > 0)
            {
                iqMark = iqMark.Where(s => s.DistrictID == districtID);
            }

            //if (appliedLevelID > 0)
            //{
            //    iqMark = iqMark.Where(s => s.AppliedLevel == appliedLevelID);
            //}
            if (monthID > 0)
            {
                iqMark = iqMark.Where(s => s.EducationLevelID == monthID);

            }

            List<MarkStatistic> lstMark = iqMark.ToList();
            if (lstMark.Count() > 0)
            {
                SetAutoDetectChangesEnabled(false);
                MarkStatisticBusiness.DeleteAll(lstMark);
                MarkStatisticBusiness.Save();
                SetAutoDetectChangesEnabled(true);
            }


            #region

            List<Task> taskMark = new List<Task>();
            foreach (int educationGrade in lstEducationGrade)
            {
                var lstSchoolLevel = lstSchool.Where(e=>e.EducationGrade== educationGrade).ToList();
                if(lstSchoolLevel==null || lstSchoolLevel.Count == 0)
                {
                    continue;
                }
                foreach (var item in lstSchoolLevel)
                {

                    for (int i = 0; i < lstReportType.Count; i++)
                    {
                        int reportTypeId = lstReportType[i];
                        taskMark.Add(Task.Run(() =>
                        {

                            InsertReport(item, provinceId, educationGrade, SupervisingDeptID, monthID ?? 0,
                                    valueMinDate, valueMaxDate, valueMinLastMonth, valueMaxLastMonth,
                                    SystemParamsInFile.AdditionReport.SGD_TKBienDongHS, reportTypeId);
                        }));
                    }

                }
            }
            

            if (taskMark.Count > 0)
            {
                Task.WaitAll(taskMark.ToArray());
            }

            #endregion
        }

        private void InsertReport(AcademicYearBO academicYearBO, int provinceId, int appliedLevelId, int supervisingDeptId, int monthId,
            DateTime minMonth, DateTime maxMonth, DateTime minLastMonth, DateTime maxLastMonth, string reportCode, int reportType
            )
        {

            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    #region
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {

                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "SP_INSERT_VARIABLE_PUPIL",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };

                                command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                command.Parameters.Add("P_SCHOOL_ID", academicYearBO.SchoolID);
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearBO.AcademicYearID);
                                command.Parameters.Add("P_APPLIED_LEVEL_ID", appliedLevelId);
                                command.Parameters.Add("P_DISTRICT_ID", academicYearBO.School.DistrictID);
                                command.Parameters.Add("P_YEAR", academicYearBO.Year);
                                command.Parameters.Add("P_MONTH_ID", monthId);
                                command.Parameters.Add("P_REPORT_CODE", reportCode);
                                command.Parameters.Add("P_REPORT_TYPE", reportType);
                                command.Parameters.Add("P_FIRST_SEMESTER_START_DATE", academicYearBO.FirstSemesterStartDate.Value);
                                command.Parameters.Add("P_FIRST_CURRENT_DATE", minMonth);
                                command.Parameters.Add("P_END_CURRENT_DATE", maxMonth);
                                command.Parameters.Add("P_FIRST_LAST_MONTH", minLastMonth);
                                command.Parameters.Add("P_END_LAST_MONTH", maxLastMonth);
                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.Message, ex);
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public Stream ExportVariablePupilBySuperVising(IDictionary<string, object> dic)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.SGD_TKBienDongHS_112017 + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheetTemp = oBook.GetSheet(1);


            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? appliedLevelID = Utils.GetInt(dic, "AppliedLevel");
            int? monthID = Utils.GetInt(dic, "MonthID");
            int? year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool showSheetFemale = Utils.GetBool(dic, "SheetPupilFemale");
            bool showSheetEthenic = Utils.GetBool(dic, "SheetPupilEthnic");
            bool showSheetFemaleEthenic = Utils.GetBool(dic, "SheetPupilFemaleEthnic");

            List<int> lstReportType = new List<int>();
            lstReportType.Add(SystemParamsInFile.AdditionReport.NORMAL);
            if (showSheetFemale)
            {
                lstReportType.Add(SystemParamsInFile.AdditionReport.FEMALE);
            }
            if (showSheetEthenic)
            {
                lstReportType.Add(SystemParamsInFile.AdditionReport.ETHNIC);
            }

            if (showSheetFemaleEthenic)
            {
                lstReportType.Add(SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC);
            }


            Province objProvince = ProvinceBusiness.Find(provinceId);



            Dictionary<string, object> dicMark = new Dictionary<string, object>();

            dicMark.Add("Year", year);
            dicMark.Add("DistrictID", districtID);
            dicMark.Add("AppliedLevel", appliedLevelID);
            dicMark.Add("ProvinceID", provinceId);
            dicMark.Add("SupervisingDeptID", SupervisingDeptID);
            dicMark.Add("ReportCode", SystemParamsInFile.AdditionReport.SGD_TKBienDongHS);
            dicMark.Add("EducationLevelId", monthID);
            List<MarkStatisticBO> lstMark = GetListMarkStatistic(dicMark);

            #region lay thong tin ngay bao cao
            DateTime currenDate = DateTime.Now;
            int _month = 0;
            int _year = 0;
            if (monthID.HasValue && monthID.Value > 0)
            {
                string _monthID = monthID.ToString();
                int _length = _monthID.Length;
                string _strMonth = string.Empty;
                string _strYear = string.Empty;

                if (_length == 5)
                    _strMonth = _monthID.Substring(4, 1).ToString();
                else
                    _strMonth = _monthID.Substring(4, 2).ToString();

                _strYear = _monthID.Substring(0, 4).ToString();

                Int32.TryParse(_strMonth, out _month);
                Int32.TryParse(_strYear, out _year);
            }

            int _lastMonth = 0;
            int _lastYear = 0;
            if (_month == 1)
            {
                _lastYear = _year - 1;
                _lastMonth = 12;
            }
            else
            {
                _lastYear = _year;
                _lastMonth = _month - 1;
            }

            DateTime valueMinDate = DateTime.Now;
            DateTime valueMaxDate = DateTime.Now;
            DateTime valueMinLastMonth = new DateTime(_lastYear, _lastMonth, 1);
            DateTime valueMaxLastMonth = valueMinLastMonth.AddMonths(1).AddDays(-1);
            DateTime valueOfMonthSelected = new DateTime(_year, _month, 1);
            DateTime valueOfCurrent = new DateTime(currenDate.Year, currenDate.Month, 1);

            if (valueOfMonthSelected == valueOfCurrent)
            {
                valueMinDate = new DateTime(currenDate.Year, currenDate.Month, 1);
                valueMaxDate = DateTime.Now;
            }
            else
            {
                valueMinDate = new DateTime(_year, _month, 1);
                valueMaxDate = valueMinDate.AddMonths(1).AddDays(-1);
            }


            string _strDate = string.Format("[Province], ngày {0} tháng {1} năm {2}", currenDate.Day, currenDate.Month, currenDate.Year);

            if (objProvince != null)
            {
                _strDate = _strDate.Replace("[Province]", objProvince.ProvinceName).ToString();
            }
            else
            {
                _strDate = _strDate.Replace("[Province]", ".........").ToString();
            }
            #endregion

            for (int i = 0; i < lstReportType.Count; i++)
            {

                int reportTypeId = lstReportType[i];

                IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemp);

                sheet.SetCellValue("A3", superVisingDeptName);
                sheet.SetCellValue("E4", _strDate);
                /*if (SupervisingDeptID >0)
                {
                    sheet.SetCellValue("A2", "SỞ GIÁO DỤC & VÀO TẠO");
                }*/

                string title = "";
                if (reportTypeId == SystemParamsInFile.AdditionReport.NORMAL)
                {
                    title = string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH THÁNG {0}/{1}", _month, _year);
                    sheet.Name = "Tatca";
                }
                else if (reportTypeId == SystemParamsInFile.AdditionReport.FEMALE)
                {
                    title = string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ THÁNG {0}/{1}", _month, _year);
                    sheet.Name = "Nu";
                }
                else if (reportTypeId == SystemParamsInFile.AdditionReport.ETHNIC)
                {
                    title = string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH DÂN TỘC THÁNG {0}/{1}", _month, _year);
                    sheet.Name = "DT";
                }
                else if (reportTypeId == SystemParamsInFile.AdditionReport.FEMALE_AND_ETHNIC)
                {
                    title = string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH DÂN TỘC THÁNG {0}/{1}", _month, _year);
                    sheet.Name = "Nu_DT";

                }
                sheet.SetCellValue("E6", title);
                sheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
                sheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                sheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                sheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                sheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));

                #region "Dien du lieu baocao"
                List<int> lstSumProvince = lstSumProvince = new List<int>();
                List<int> lstDistrict = lstMark.Where(c => c.CriteriaReportID == reportTypeId).OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                int orderId = 1;
                int rowDistrict = 11;
                int startRow = 12;
                int row = startRow;
                lstSumProvince.Add(rowDistrict);

                for (int d = 0; d < lstDistrict.Count; d++)
                {
                    orderId = 1;
                    int districtId = lstDistrict[d];
                    if (d > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheet.GetRange("A11", "U11"), rowDistrict);
                    }
                    List<MarkStatisticBO> lstMarkExport = lstMark.Where(m => m.CriteriaReportID == reportTypeId && m.DistrictID == districtId).OrderBy(m => m.SchoolCode).ToList();
                    for (int j = 0; j < lstMarkExport.Count; j++)
                    {
                        MarkStatisticBO objMark = lstMarkExport[j];
                        if (j == 0)
                        {
                            sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                        }
                        if (j > 0 || (j == 0 && d > 0))
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange("A12", "U12"), row);
                        }

                        sheet.SetCellValue(row, 1, orderId);
                        sheet.SetCellValue(row, 2, objMark.SchoolName);
                        sheet.SetCellValue(row, 3, objMark.MarkTotal > 0 ? objMark.MarkTotal : 0);
                        sheet.SetCellValue(row, 4, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                        sheet.SetCellValue(row, 5, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
                        sheet.SetCellValue(row, 6, objMark.MarkLevel03 > 0 ? objMark.MarkLevel03 : 0);
                        sheet.SetCellValue(row, 7, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
                        sheet.SetCellValue(row, 8, objMark.MarkLevel05 > 0 ? objMark.MarkLevel05 : 0);
                        sheet.SetCellValue(row, 9, objMark.MarkLevel06 > 0 ? objMark.MarkLevel06 : 0);
                        sheet.SetCellValue(row, 10, objMark.MarkLevel07 > 0 ? objMark.MarkLevel07 : 0);
                        sheet.SetCellValue(row, 11, objMark.MarkLevel08 > 0 ? objMark.MarkLevel08 : 0);
                        sheet.SetCellValue(row, 12, objMark.MarkLevel09 > 0 ? objMark.MarkLevel09 : 0);
                        sheet.SetCellValue(row, 13, objMark.MarkLevel10 > 0 ? objMark.MarkLevel10 : 0);
                        sheet.SetCellValue(row, 14, objMark.MarkLevel11 > 0 ? objMark.MarkLevel11 : 0);
                        sheet.SetCellValue(row, 15, objMark.MarkLevel12 > 0 ? objMark.MarkLevel12 : 0);
                        sheet.SetCellValue(row, 16, objMark.MarkLevel13 > 0 ? objMark.MarkLevel13 : 0);
                        sheet.SetCellValue(row, 17, objMark.MarkLevel14 > 0 ? objMark.MarkLevel14 : 0);
                        sheet.SetCellValue(row, 18, objMark.MarkLevel15 > 0 ? objMark.MarkLevel15 : 0);
                        sheet.SetCellValue(row, 19, objMark.MarkLevel16 > 0 ? objMark.MarkLevel16 : 0);
                        sheet.SetCellValue(row, 20, objMark.MarkLevel17 > 0 ? objMark.MarkLevel17 : 0);
                        sheet.SetCellValue(row, 21, objMark.MarkLevel18 > 0 ? objMark.MarkLevel18 : 0);
                        sheet.SetCellValue(row, 22, objMark.MarkLevel19 > 0 ? objMark.MarkLevel19 : 0);

                        (sheet.GetRange(row, 1, row, 22)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                        row = row + 1;
                        orderId = orderId + 1;
                    }

                    if (row > startRow)
                    {
                        SetFormulaDistrict(sheet, rowDistrict + 1, row - 1, 3, 22);
                    }
                    if (d > 0)
                    {
                        lstSumProvince.Add(rowDistrict);
                    }
                    rowDistrict = row;
                    row = row + 1;

                }
                SetFormulaProvince(sheet, 10, lstSumProvince, 3, 22);
                #endregion



            }

            oBook.GetSheet(1).Delete();

            return oBook.ToStream();
        }


        /// <summary>
        /// Tong hop bao cao hoc sinh nghi hoc
        /// </summary>
        /// <param name="dic"></param>
        public void InsertReportAbsencePupilForSuperVising(IDictionary<string, object> dic)
        {

            int year = Utils.GetInt(dic, "Year");
            int reportType = Utils.GetInt(dic, "ReportType");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            List<int> lstGrade = Utils.GetIntList(dic, "lstGrade");
            int supervisingDeptId = Utils.GetInt(dic, "SupervisingDeptID");
            //string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");


            string reporCode = reportType == 1 ? SystemParamsInFile.DSHSNghiHoc_PhongSo : SystemParamsInFile.TKHSNghiHoc_PhongSo;
            #region // Get data

            IQueryable<AcademicYearBO> iqSchool = (from sp in SchoolProfileBusiness.AllNoTracking
                                                   join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID
                                                   where sp.ProvinceID == provinceId
                                                         && sp.IsActive == true
                                                         && sp.IsActiveSMAS == true
                                                   select new AcademicYearBO
                                                   {
                                                       AcademicYearID = ay.AcademicYearID,
                                                       SchoolID = sp.SchoolProfileID,
                                                       Year = year,
                                                       School = sp,
                                                       DistrictID = sp.DistrictID,
                                                       SuperVisingID = sp.SupervisingDeptID,
                                                       EducationGrade = sp.EducationGrade
                                                   });
            if (districtID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.DistrictID == districtID);
            }
            if (lstGrade != null && lstGrade.Count > 0)
            {
                List<int> lstEducationGrade = new List<int>();
                for (int i = 0; i < lstGrade.Count; i++)
                {
                    lstEducationGrade.AddRange(UtilsBusiness.GetEducationGrade(lstGrade[i]));
                }
                iqSchool = iqSchool.Where(s => lstEducationGrade.Contains(s.EducationGrade));
            }

            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }

            int partitionId = UtilsBusiness.GetPartionProvince(provinceId);
            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.ReportCode == reporCode
                                                              && m.Last2DigitNumberProvince == partitionId
                                                              && m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate
                                                              select m;
            if (districtID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtID);
            }

            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();

            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                {
                    MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                    MarkStatisticBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertReportAbsencePupilForSuperVising", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

            #endregion

            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);
            int sumDay = startDay + distantOfDay;

            #region
            List<Task> taskMark = new List<Task>();
            foreach (var item in lstSchool)
            {
                taskMark.Add(Task.Run(() =>
                {
                    this.InsertReportAbsencePupilForSup(supervisingDeptId, item.AcademicYearID, provinceId,
                            item.DistrictID, item.SchoolID, year, fromDate, toDate, reportType, reporCode);

                }));


            }
            if (taskMark.Count > 0)
            {
                Task.WaitAll(taskMark.ToArray());
            }
            #endregion
        }

        private void InsertReportAbsencePupilForSup(int supervisingDeptID, int academicYearId,
                                                    int provinceId, int? districtId,
                                                    int schoolId, int year, DateTime fromDate, DateTime toDate, int reportType, string reportCode)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {

                    #region
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "SP_INSERT_ABSENCE_REPORT",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };

                                command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                command.Parameters.Add("P_DISTRICT_ID", districtId);
                                command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                command.Parameters.Add("P_YEAR", year);
                                command.Parameters.Add("P_FROM_DATE", fromDate);
                                command.Parameters.Add("P_TO_DATE", toDate);
                                command.Parameters.Add("P_REPORT_CODE", reportCode);

                                tran.Commit();
                            }//end try
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Xuat bao cao thong ke hoc sinh nghi hoc
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream ReportStatisticsPupilAbsenceBySup(IDictionary<string, object> dic)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.TKHSNghiHoc_PhongSo + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);

            //int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int reportType = Utils.GetInt(dic, "ReportType");
            int year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetMN = Utils.GetBool(dic, "SheetMN");
            bool SheetTH = Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = Utils.GetBool(dic, "SheetTHPT");


            string[] lstAlphabet = { "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ" };
            // process date
            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);

            int rowTitle = 8;
            int columnTitle = 4;
            DateTime fromDateTemp = fromDate;
            DateTime DateTemp = fromDate;
            IVTRange rang = firstSheet.GetRange("D8", "E1200");
            #region fill title các tháng
            for (int i = 0; i < distantOfDay; i++)
            {
                if (SheetTH)
                {
                    firstSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    firstSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    firstSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    firstSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    firstSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                if (SheetTHCS)
                {
                    secondSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    secondSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    secondSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    secondSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    secondSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                if (SheetTHPT)
                {
                    thirdSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    thirdSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    thirdSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    thirdSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    thirdSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                if (SheetMN)
                {
                    fourthSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    fourthSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    fourthSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    fourthSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    fourthSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                fromDateTemp = fromDateTemp.AddDays(1);
                columnTitle = columnTitle + 2;
            }

            if (SheetTH)
            {
                firstSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                firstSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                firstSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                firstSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                firstSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            if (SheetTHCS)
            {
                secondSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                secondSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                secondSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                secondSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                secondSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            if (SheetTHPT)
            {
                thirdSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                thirdSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                thirdSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                thirdSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                thirdSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            if (SheetMN)
            {
                fourthSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                fourthSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                fourthSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                fourthSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                fourthSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            #endregion

            #region // Get data
            Province objProvince = ProvinceBusiness.Find(provinceId);
            List<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId).ToList();
            if (districtID.HasValue && districtID.Value > 0)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value).ToList();
            }
            //List<int> lstDistrictID = lstDistrict.Select(x => x.DistrictID).ToList();

            List<AcademicYearBO> lstSchoolAll = (from sp in SchoolProfileBusiness.AllNoTracking
                                                 join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID
                                                 where sp.ProvinceID == provinceId
                                                       && sp.IsActive == true
                                                       && sp.IsActiveSMAS == true
                                                 select new AcademicYearBO
                                                 {
                                                     AcademicYearID = ay.AcademicYearID,
                                                     SchoolID = sp.SchoolProfileID,
                                                     Year = year,
                                                     School = sp,
                                                     DistrictID = sp.DistrictID,
                                                     SuperVisingID = sp.SupervisingDeptID,
                                                     EducationGrade = sp.EducationGrade,
                                                     SchoolName = sp.SchoolName,
                                                 }).ToList();
            if (districtID > 0)
            {
                lstSchoolAll = lstSchoolAll.Where(sp => sp.School.DistrictID == districtID).ToList();
            }


            //List<MarkStatistic> lstMark = MarkStatisticBusiness.All.Where(x => (x.ReportCode == "SheetStatisticPupilAbsence")
            //                                                                && x.ProvinceID == provinceId
            //                                                                && x.Year == year).ToList();

            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.ReportCode == "SheetStatisticPupilAbsence"
                                                              && m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate
                                                              select m;
            if (districtID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtID);
            }

            List<MarkStatistic> lstMark = iQMarkStatisticDelete.ToList();

            #endregion

            #region // Fill tieu de
            if (SheetTH)
            {
                firstSheet.SetCellValue("A3", superVisingDeptName);
                firstSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }

            if (SheetTHCS)
            {
                secondSheet.SetCellValue("A3", superVisingDeptName);
                secondSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }

            if (SheetTHPT)
            {
                thirdSheet.SetCellValue("A3", superVisingDeptName);
                thirdSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }

            if (SheetMN)
            {
                fourthSheet.SetCellValue("A3", superVisingDeptName);
                fourthSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }
            #endregion

            #region các biến
            int startRowTH = 11;
            int startRowTHCS = 11;
            int startRowTHPT = 11;
            int startRowMN = 11;
            int startColumn = 3;
            int sttTH, sttTHCS, sttTHPT, sttMN;

            int countSchoolTH = 0;
            int countSchoolTHCS = 0;
            int countSchoolTHPT = 0;
            int countSchoolMN = 0;
            int lastColumn = 3;
            MarkStatistic objMarkTH = null;
            MarkStatistic objMarkTHDate = null;
            MarkStatistic objMarkTHCS = null;
            MarkStatistic objMarkTHCSDate = null;
            MarkStatistic objMarkTHPT = null;
            MarkStatistic objMarkTHPTDate = null;
            MarkStatistic objMarkMN = null;
            MarkStatistic objMarkMNDate = null;
            List<MarkStatistic> lstMSTH = null;
            List<MarkStatistic> lstMSTHCS = null;
            List<MarkStatistic> lstMSTHPT = null;
            List<MarkStatistic> lstMSMN = null;
            List<MarkStatistic> lstMSTHTemp = null;
            List<MarkStatistic> lstMSTHCSTemp = null;
            List<MarkStatistic> lstMSTHPTTemp = null;
            List<MarkStatistic> lstMSMNTemp = null;
            List<int> lstDistrictTH = new List<int>();
            List<int> lstDistrictTHCS = new List<int>();
            List<int> lstDistrictTHPT = new List<int>();
            List<int> lstDistrictMN = new List<int>();
            string stringTHDistrictRow = "=";
            string stringTHCSDistrictRow = "=";
            string stringTHPTDistrictRow = "=";
            string stringMNDistrictRow = "=";
            string stringTHDistrictSumRow = "=";
            string stringTHCSDistrictSumRow = "=";
            string stringTHPTDistrictSumRow = "=";
            string stringMNDistrictSumRow = "=";
            #endregion

            #region // Fill nội dung
            foreach (var itemDis in lstDistrict)
            {
                sttTH = sttTHCS = sttTHPT = sttMN = 1;
                countSchoolTH = 0;
                countSchoolTHCS = 0;
                countSchoolTHPT = 0;
                countSchoolMN = 0;
                stringTHDistrictRow = "=";
                stringTHCSDistrictRow = "=";
                stringTHPTDistrictRow = "=";
                stringMNDistrictRow = "=";

                // Lấy số lượng trường trong tỉnh theo từng cấp
                #region
                if (SheetTH)
                {
                    lstMSTH = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel37 != -1).ToList();
                    countSchoolTH = lstMSTH.Select(x => x.SchoolID).Distinct().Count();
                }

                if (SheetTHCS)
                {
                    lstMSTHCS = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel38 != -1).ToList();
                    countSchoolTHCS = lstMSTHCS.Select(x => x.SchoolID).Distinct().Count();
                }

                if (SheetTHPT)
                {
                    lstMSTHPT = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel39 != -1).ToList();
                    countSchoolTHPT = lstMSTHPT.Select(x => x.SchoolID).Distinct().Count();
                }

                if (SheetMN)
                {
                    lstMSMN = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel40 != -1).ToList();
                    countSchoolMN = lstMSMN.Select(x => x.SchoolID).Distinct().Count();
                }
                #endregion

                var lstSchoolProfile = lstSchoolAll.Where(x => x.DistrictID == itemDis.DistrictID);
                foreach (var itemSchool in lstSchoolProfile)
                {
                    startColumn = 3;
                    DateTemp = fromDate;
                    lstMSTHTemp = new List<MarkStatistic>();
                    lstMSTHCSTemp = new List<MarkStatistic>();
                    lstMSTHPTTemp = new List<MarkStatistic>();
                    lstMSMNTemp = new List<MarkStatistic>();
                    #region fill STT, tên trường, tổng HS của các trường
                    if (SheetTH)
                    {
                        lstMSTHTemp = lstMSTH.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel37 != -1).ToList();
                        objMarkTH = lstMSTHTemp.FirstOrDefault();
                        if (objMarkTH != null)
                        {
                            startRowTH++;
                            firstSheet.SetCellValue(("A" + startRowTH), sttTH);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            firstSheet.SetCellValue(("B" + startRowTH), itemSchool.SchoolName);
                            firstSheet.SetCellValue(startRowTH, startColumn, objMarkTH.MarkLevel37);
                            sttTH++;
                        }
                    }

                    if (SheetTHCS)
                    {
                        lstMSTHCSTemp = lstMSTHCS.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel38 != -1).ToList();
                        objMarkTHCS = lstMSTHCSTemp.FirstOrDefault();
                        if (objMarkTHCS != null)
                        {
                            startRowTHCS++;
                            secondSheet.SetCellValue(("A" + startRowTHCS), sttTHCS);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            secondSheet.SetCellValue(("B" + startRowTHCS), itemSchool.SchoolName);
                            secondSheet.SetCellValue(startRowTHCS, startColumn, objMarkTHCS.MarkLevel38);
                            sttTHCS++;
                        }
                    }

                    if (SheetTHPT)
                    {
                        lstMSTHPTTemp = lstMSTHPT.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel39 != -1).ToList();
                        objMarkTHPT = lstMSTHPTTemp.FirstOrDefault();
                        if (objMarkTHPT != null)
                        {
                            startRowTHPT++;
                            thirdSheet.SetCellValue(("A" + startRowTHPT), sttTHPT);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            thirdSheet.SetCellValue(("B" + startRowTHPT), itemSchool.SchoolName);
                            thirdSheet.SetCellValue(startRowTHPT, startColumn, objMarkTHPT.MarkLevel39);
                            sttTHPT++;
                        }
                    }

                    if (SheetMN)
                    {
                        lstMSMNTemp = lstMSMN.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel40 != -1).ToList();
                        objMarkMN = lstMSMNTemp.FirstOrDefault();
                        if (objMarkMN != null)
                        {
                            startRowMN++;
                            fourthSheet.SetCellValue(("A" + startRowMN), sttMN);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            fourthSheet.SetCellValue(("B" + startRowMN), itemSchool.SchoolName);
                            fourthSheet.SetCellValue(startRowMN, startColumn, objMarkMN.MarkLevel40);
                            sttMN++;
                        }
                    }
                    #endregion
                    startColumn++;

                    #region fill từng tháng của trường
                    for (int i = 0; i <= distantOfDay; i++)
                    {
                        #region cột tổng của trường
                        if (i == distantOfDay && i != 0)
                        {
                            if (SheetTH && objMarkTH != null)
                            {
                                firstSheet.SetCellValue(startRowTH, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                                firstSheet.SetCellValue(startRowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTH + "/IF(C" + startRowTH + "<=0,1,C" + startRowTH + "),3)*100");//"=(" + lstAlphabet[startColumn] + startRowTH + "/C" + startRowTH + ")/5%");
                            }
                            if (SheetTHCS && objMarkTHCS != null)
                            {
                                secondSheet.SetCellValue(startRowTHCS, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                                secondSheet.SetCellValue(startRowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHCS + "/IF(C" + startRowTHCS + "<=0,1,C" + startRowTHCS + "),3)*100");
                            }
                            if (SheetTHPT && objMarkTHPT != null)
                            {
                                thirdSheet.SetCellValue(startRowTHPT, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                                thirdSheet.SetCellValue(startRowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHPT + "/IF(C" + startRowTHPT + "<=0,1,C" + startRowTHPT + "),3)*100");
                            }
                            if (SheetMN && objMarkMN != null)
                            {
                                fourthSheet.SetCellValue(startRowMN, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                                fourthSheet.SetCellValue(startRowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowMN + "/IF(C" + startRowMN + "<=0,1,C" + startRowMN + "),3)*100");
                            }
                            lastColumn = startColumn;
                            break;
                        }
                        #endregion

                        if (SheetTH && objMarkTH != null)
                        {
                            stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + startRowTH + "+";
                            objMarkTHDate = lstMSTHTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            firstSheet.SetCellValue(startRowTH, startColumn, objMarkTHDate == null ? 0 : objMarkTHDate.MarkLevel01);
                            firstSheet.SetCellValue(startRowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTH + "/IF(C" + startRowTH + "<=0,1,C" + startRowTH + "),3)*100");//"=" + lstAlphabet[startColumn] + startRowTH + "/" + "C" + startRowTH + "%");
                        }
                        if (SheetTHCS && objMarkTHCS != null)
                        {
                            stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + startRowTHCS + "+";
                            objMarkTHCSDate = lstMSTHCSTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            secondSheet.SetCellValue(startRowTHCS, startColumn, objMarkTHCSDate == null ? 0 : objMarkTHCSDate.MarkLevel02);
                            secondSheet.SetCellValue(startRowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHCS + "/IF(C" + startRowTHCS + "<=0,1,C" + startRowTHCS + "),3)*100");//"=" + lstAlphabet[startColumn] + startRowTHCS + "/" + "C" + startRowTHCS + "%");
                        }
                        if (SheetTHPT && objMarkTHPT != null)
                        {
                            stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + startRowTHPT + "+";
                            objMarkTHPTDate = lstMSTHPTTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            thirdSheet.SetCellValue(startRowTHPT, startColumn, objMarkTHPTDate == null ? 0 : objMarkTHPTDate.MarkLevel03);
                            thirdSheet.SetCellValue(startRowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHPT + "/IF(C" + startRowTHPT + "<=0,1,C" + startRowTHPT + "),3)*100");
                        }
                        if (SheetMN && objMarkMN != null)
                        {
                            stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + startRowMN + "+";
                            objMarkMNDate = lstMSMNTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            fourthSheet.SetCellValue(startRowMN, startColumn, objMarkMNDate == null ? 0 : objMarkMNDate.MarkLevel04);
                            fourthSheet.SetCellValue(startRowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowMN + "/IF(C" + startRowMN + "<=0,1,C" + startRowMN + "),3)*100");
                        }

                        DateTemp = DateTemp.AddDays(1);
                        startColumn = startColumn + 2;
                    }
                    #endregion
                }

                #region fill quận/huyện
                int rowTH = startRowTH - countSchoolTH;
                int rowTHCS = startRowTHCS - countSchoolTHCS;
                int rowTHPT = startRowTHPT - countSchoolTHPT;
                int rowMN = startRowMN - countSchoolMN;
                if (SheetTH)
                {
                    firstSheet.SetCellValue(("A" + rowTH), itemDis.DistrictName);
                    lstDistrictTH.Add(rowTH);
                    firstSheet.GetRange(rowTH, 1, rowTH, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    firstSheet.GetRange(rowTH, 1, rowTH, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    firstSheet.MergeRow(rowTH, 'A', 'B');
                    firstSheet.GetRange(rowTH, 1, rowTH, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    firstSheet.SetRowHeight(rowTH, 24.75);
                }
                if (SheetTHCS)
                {
                    secondSheet.SetCellValue(("A" + rowTHCS), itemDis.DistrictName);
                    lstDistrictTHCS.Add(rowTHCS);
                    secondSheet.GetRange(rowTHCS, 1, rowTHCS, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    secondSheet.GetRange(rowTHCS, 1, rowTHCS, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    secondSheet.MergeRow(rowTHCS, 'A', 'B');
                    secondSheet.GetRange(rowTHCS, 1, rowTHCS, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    secondSheet.SetRowHeight(rowTHCS, 24.75);
                }
                if (SheetTHPT)
                {
                    thirdSheet.SetCellValue(("A" + rowTHPT), itemDis.DistrictName);
                    lstDistrictTHPT.Add(rowTHPT);
                    thirdSheet.GetRange(rowTHPT, 1, rowTHPT, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    thirdSheet.GetRange(rowTHPT, 1, rowTHPT, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    thirdSheet.MergeRow(rowTHPT, 'A', 'B');
                    thirdSheet.GetRange(rowTHPT, 1, rowTHPT, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    thirdSheet.SetRowHeight(rowTHPT, 24.75);
                }
                if (SheetMN)
                {
                    fourthSheet.SetCellValue(("A" + rowMN), itemDis.DistrictName);
                    lstDistrictMN.Add(rowMN);
                    fourthSheet.GetRange(rowMN, 1, rowMN, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    fourthSheet.GetRange(rowMN, 1, rowMN, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    fourthSheet.MergeRow(rowMN, 'A', 'B');
                    fourthSheet.GetRange(rowMN, 1, rowMN, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    fourthSheet.SetRowHeight(rowMN, 24.75);
                }

                startColumn = 3;
                if (countSchoolTH == 0)
                {
                    firstSheet.SetCellValue(rowTH, startColumn, 0);
                }
                else
                {
                    firstSheet.SetCellValue(rowTH, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTH == 0 ? rowTH : (rowTH + 1)) + ":" + lstAlphabet[startColumn] + (rowTH + countSchoolTH) + ")");
                }
                if (countSchoolTHCS == 0)
                {
                    secondSheet.SetCellValue(rowTHCS, startColumn, 0);
                }
                else
                {
                    secondSheet.SetCellValue(rowTHCS, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHCS == 0 ? rowTHCS : (rowTHCS + 1)) + ":" + lstAlphabet[startColumn] + (rowTHCS + countSchoolTHCS) + ")");
                }
                if (countSchoolTHPT == 0)
                {
                    thirdSheet.SetCellValue(rowTHPT, startColumn, 0);
                }
                else
                {
                    thirdSheet.SetCellValue(rowTHPT, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHPT == 0 ? rowTHPT : (rowTHPT + 1)) + ":" + lstAlphabet[startColumn] + (rowTHPT + countSchoolTHPT) + ")");
                }
                if (countSchoolMN == 0)
                {
                    fourthSheet.SetCellValue(rowMN, startColumn, 0);
                }
                else
                {
                    fourthSheet.SetCellValue(rowMN, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolMN == 0 ? rowMN : (rowMN + 1)) + ":" + lstAlphabet[startColumn] + (rowMN + countSchoolMN) + ")");
                }
                startColumn++;
                stringTHDistrictRow = "=";
                stringTHCSDistrictRow = "=";
                stringTHPTDistrictRow = "=";
                stringMNDistrictRow = "=";
                for (int i = 0; i <= distantOfDay; i++)
                {
                    #region fill cột tổng
                    if (i == distantOfDay && i != 0)
                    {
                        if (SheetTH)
                        {
                            firstSheet.SetCellValue(rowTH, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                            firstSheet.SetCellValue(rowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTH + "/IF(C" + rowTH + "<=0,1,C" + rowTH + "),3)*100");
                        }
                        if (SheetTHCS)
                        {
                            secondSheet.SetCellValue(rowTHCS, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                            secondSheet.SetCellValue(rowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHCS + "/IF(C" + rowTHCS + "<=0,1,C" + rowTHCS + "),3)*100");
                        }
                        if (SheetTHPT)
                        {
                            thirdSheet.SetCellValue(rowTHPT, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                            thirdSheet.SetCellValue(rowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHPT + "/IF(C" + rowTHPT + "<=0,1,C" + rowTHPT + "),3)*100");
                        }
                        if (SheetMN)
                        {
                            fourthSheet.SetCellValue(rowMN, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                            fourthSheet.SetCellValue(rowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowMN + "/IF(C" + rowMN + "<=0,1,C" + rowMN + "),3)*100");
                        }
                        break;
                    }
                    #endregion

                    if (SheetTH)
                    {
                        stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + rowTH + "+";
                        if (countSchoolTH == 0)
                        {
                            firstSheet.SetCellValue(rowTH, startColumn, 0);
                        }
                        else
                        {
                            firstSheet.SetCellValue(rowTH, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTH == 0 ? rowTH : (rowTH + 1)) + ":" + lstAlphabet[startColumn] + (rowTH + countSchoolTH) + ")");
                        }
                        firstSheet.SetCellValue(rowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTH + "/IF(C" + rowTH + "<=0,1,C" + rowTH + "),3)*100");
                    }
                    if (SheetTHCS)
                    {
                        stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + rowTHCS + "+";
                        if (countSchoolTHCS == 0)
                        {
                            secondSheet.SetCellValue(rowTHCS, startColumn, 0);
                        }
                        else
                        {
                            secondSheet.SetCellValue(rowTHCS, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHCS == 0 ? rowTHCS : (rowTHCS + 1)) + ":" + lstAlphabet[startColumn] + (rowTHCS + countSchoolTHCS) + ")");
                        }
                        secondSheet.SetCellValue(rowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHCS + "/IF(C" + rowTHCS + "<=0,1,C" + rowTHCS + "),3)*100");
                    }
                    if (SheetTHPT)
                    {
                        stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + rowTHPT + "+";
                        if (countSchoolTHPT == 0)
                        {
                            thirdSheet.SetCellValue(rowTHPT, startColumn, 0);
                        }
                        else
                        {
                            thirdSheet.SetCellValue(rowTHPT, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHPT == 0 ? rowTHPT : (rowTHPT + 1)) + ":" + lstAlphabet[startColumn] + (rowTHPT + countSchoolTHPT) + ")");
                        }
                        thirdSheet.SetCellValue(rowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHPT + "/IF(C" + rowTHPT + "<=0,1,C" + rowTHPT + "),3)*100");
                    }
                    if (SheetMN)
                    {
                        stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + rowMN + "+";
                        if (countSchoolMN == 0)
                        {
                            fourthSheet.SetCellValue(rowMN, startColumn, 0);
                        }
                        else
                        {
                            fourthSheet.SetCellValue(rowMN, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolMN == 0 ? rowMN : (rowMN + 1)) + ":" + lstAlphabet[startColumn] + (rowMN + countSchoolMN) + ")");
                        }
                        fourthSheet.SetCellValue(rowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowMN + "/IF(C" + rowMN + "<=0,1,C" + rowMN + "),3)*100");
                    }
                    startColumn = startColumn + 2;
                }
                #endregion
                startRowTH++;
                startRowTHCS++;
                startRowTHPT++;
                startRowMN++;
            }
            firstSheet.GetRange(11, 1, startRowTH - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            secondSheet.GetRange(11, 1, startRowTHCS - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            thirdSheet.GetRange(11, 1, startRowTHPT - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            fourthSheet.GetRange(11, 1, startRowMN - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            #region fill toàn tỉnh
            stringTHDistrictRow = "=";
            stringTHCSDistrictRow = "=";
            stringTHPTDistrictRow = "=";
            stringMNDistrictRow = "=";
            startColumn = 3;

            #region cột tổng học sinh của toàn tỉnh
            for (int j = 0; j < lstDistrictTH.Count; j++)
            {
                stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + lstDistrictTH[j] + "+";
            }
            firstSheet.SetCellValue(10, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
            //firstSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");

            for (int j = 0; j < lstDistrictTHCS.Count; j++)
            {
                stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + lstDistrictTHCS[j] + "+";
            }
            secondSheet.SetCellValue(10, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
            //secondSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");

            for (int j = 0; j < lstDistrictTHPT.Count; j++)
            {
                stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + lstDistrictTHPT[j] + "+";
            }
            thirdSheet.SetCellValue(10, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
            //thirdSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");

            for (int j = 0; j < lstDistrictMN.Count; j++)
            {
                stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + lstDistrictMN[j] + "+";
            }
            fourthSheet.SetCellValue(10, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
            //fourthSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");
            startColumn++;
            #endregion

            #region Các tháng của toàn tỉnh
            for (int i = 0; i < distantOfDay; i++)
            {
                stringTHDistrictRow = "=";
                stringTHCSDistrictRow = "=";
                stringTHPTDistrictRow = "=";
                stringMNDistrictRow = "=";
                for (int j = 0; j < lstDistrictTH.Count; j++)
                { //=D11+D19
                    stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + lstDistrictTH[j] + "+";
                }
                if (SheetTH)
                {
                    stringTHDistrictSumRow = stringTHDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                    firstSheet.SetCellValue(10, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                    firstSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");//"=" + lstAlphabet[startColumn] +"10/C10%");
                }

                for (int j = 0; j < lstDistrictTHCS.Count; j++)
                { //=D11+D19
                    stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + lstDistrictTHCS[j] + "+";
                }
                if (SheetTHCS)
                {
                    stringTHCSDistrictSumRow = stringTHCSDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                    secondSheet.SetCellValue(10, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                    secondSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }

                for (int j = 0; j < lstDistrictTHPT.Count; j++)
                { //=D11+D19
                    stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + lstDistrictTHPT[j] + "+";
                }
                if (SheetTHPT)
                {
                    stringTHPTDistrictSumRow = stringTHPTDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                    thirdSheet.SetCellValue(10, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                    thirdSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }

                for (int j = 0; j < lstDistrictMN.Count; j++)
                {
                    stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + lstDistrictMN[j] + "+";
                }
                if (SheetMN)
                {
                    stringMNDistrictSumRow = stringMNDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                    fourthSheet.SetCellValue(10, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                    fourthSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }

                startColumn = startColumn + 2;
            }
            #endregion

            #region cột tổng của toàn tỉnh
            if (SheetTH)
            {
                firstSheet.SetCellValue(10, lastColumn, stringTHDistrictSumRow.Substring(0, stringTHDistrictSumRow.Length - 1));
                firstSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
            }
            if (SheetTHCS)
            {
                secondSheet.SetCellValue(10, lastColumn, stringTHCSDistrictSumRow.Substring(0, stringTHCSDistrictSumRow.Length - 1));
                secondSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
            }
            if (SheetTHPT)
            {
                thirdSheet.SetCellValue(10, lastColumn, stringTHPTDistrictSumRow.Substring(0, stringTHPTDistrictSumRow.Length - 1));
                thirdSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
            }
            if (SheetMN)
            {
                fourthSheet.SetCellValue(10, lastColumn, stringMNDistrictSumRow.Substring(0, stringMNDistrictSumRow.Length - 1));
                fourthSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
            }
            #endregion
            #endregion


            if (districtID != 0)
            {
                firstSheet.DeleteRow(10);
                secondSheet.DeleteRow(10);
                thirdSheet.DeleteRow(10);
                fourthSheet.DeleteRow(10);
            }

            if (SheetTH)
            {
                firstSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                firstSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                firstSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                firstSheet.Delete();
            }
            if (SheetTHCS)
            {
                secondSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                secondSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                secondSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                secondSheet.Delete();
            }
            if (SheetTHPT)
            {
                thirdSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                thirdSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                thirdSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                thirdSheet.Delete();
            }
            if (SheetMN)
            {
                fourthSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                fourthSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                fourthSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                fourthSheet.Delete();
            }
            #endregion



            return oBook.ToStream();
        }

        /// <summary>
        /// Xuat bao cao thong ke hoc sinh nghi hoc
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream ReportStatisticsPupilAbsence(IDictionary<string, object> dic)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.TKHSNghiHoc_PhongSo + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheetTemp = oBook.GetSheet(1);


            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? appliedLevelID = Utils.GetInt(dic, "AppliedLevel");
            int? monthID = Utils.GetInt(dic, "MonthID");
            int? year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetMN = Utils.GetBool(dic, "SheetMN");
            bool SheetTH = Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = Utils.GetBool(dic, "SheetTHPT");
            List<int> lstAppliedLevel = new List<int>();

            if (SheetMN)//NT, MG
            {
                lstAppliedLevel.Add(GlobalConstants.APPLIED_LEVEL_CRECHE);
            }
            if (SheetTH)//TH
            {
                lstAppliedLevel.Add(GlobalConstants.APPLIED_LEVEL_PRIMARY);
            }
            if (SheetTHCS)//THCS
            {
                lstAppliedLevel.Add(GlobalConstants.APPLIED_LEVEL_SECONDARY);
            }
            if (SheetTHPT)//THPT
            {
                lstAppliedLevel.Add(GlobalConstants.APPLIED_LEVEL_TERTIARY);
            }

            lstAppliedLevel = lstAppliedLevel.OrderBy(s => s).ToList();
            // process date
            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);

            int rowTitle = 8;
            int columnTitle = 4;
            int startColumnDay = 4;
            DateTime fromDateTemp = fromDate;
            DateTime DateTemp = fromDate;
            IVTRange rang = sheetTemp.GetRange("D8", "E10");

            //Tao cac cot cho sheet temp
            for (int i = 0; i < distantOfDay + 1; i++)
            {
                if (i > 0)
                {
                    sheetTemp.CopyPasteSameSize(rang, rowTitle, columnTitle);
                }
                sheetTemp.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                sheetTemp.SetCellValue(rowTitle, columnTitle, i >= distantOfDay ? "Tổng" : fromDateTemp.ToShortDateString());
                sheetTemp.SetCellValue(rowTitle + 1, columnTitle, "SL");
                sheetTemp.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                fromDateTemp = fromDateTemp.AddDays(1);
                columnTitle = columnTitle + 2;
            }


            int endColumn = columnTitle + distantOfDay + 1;

            Province objProvince = ProvinceBusiness.Find(provinceId);


            Dictionary<string, object> dicMark = new Dictionary<string, object>();

            dicMark.Add("Year", year);
            dicMark.Add("DistrictID", districtID);
            //dicMark.Add("AppliedLevel", appliedLevelID);
            dicMark.Add("ProvinceID", provinceId);
            dicMark.Add("SupervisingDeptID", SupervisingDeptID);
            dicMark.Add("ReportCode", SystemParamsInFile.TKHSNghiHoc_PhongSo);
            dicMark.Add("FromDate", fromDate);
            dicMark.Add("ToDate", toDate);
            List<MarkStatisticBO> lstMark = GetListMarkStatistic(dicMark);

            //Dien du lieu
            for (int i = 0; i < lstAppliedLevel.Count; i++)
            {
                int appliedLevelId = lstAppliedLevel[i];

                List<int> lstEducationGrade = UtilsBusiness.GetEducationGrade(appliedLevelId);

                IVTWorksheet sheet = oBook.CopySheetToLast(sheetTemp);

                string title = "";
                if (appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_CRECHE || appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
                {
                    title = "THỐNG KÊ HỌC SINH VẮNG CẤP MN";
                    sheet.Name = "MN";
                }
                else if (appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                {
                    title = "THỐNG KÊ HỌC SINH VẮNG CẤP TH";
                    sheet.Name = "TH";
                }
                else if (appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    title = "THỐNG KÊ HỌC SINH VẮNG CẤP THCS";
                    sheet.Name = "THCS";
                }
                else if (appliedLevelId == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    title = "THỐNG KÊ HỌC SINH VẮNG CẤP THPT";
                    sheet.Name = "THPT";

                }
                sheet.SetCellValue("A5", title);


                #region "Dien du lieu baocao"
                List<int> lstSumProvince = lstSumProvince = new List<int>();
                List<int> lstDistrict = lstMark.OrderBy(d => d.DistrictID).Select(m => m.DistrictID).Distinct().ToList();
                int orderId = 1;
                int rowDistrict = 11;
                int startRow = 12;
                int row = startRow;
                lstSumProvince.Add(rowDistrict);

                for (int d = 0; d < lstDistrict.Count; d++)
                {
                    orderId = 1;
                    int districtId = lstDistrict[d];

                    if (d > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheet.GetRange(11, 1, 11, endColumn), rowDistrict);
                    }
                    var lstSchool = (from s in lstMark
                                     where s.DistrictID == districtId
                                     && lstEducationGrade.Contains(s.EducationGrade)
                                     select new
                                     {
                                         SchoolCode = s.SchoolCode,
                                         SchoolName = s.SchoolName,
                                         SchoolId = s.SchoolID,
                                         DistrictName = s.DistrictName
                                     }).Distinct().ToList();

                    for (int j = 0; j < lstSchool.Count; j++)
                    {
                        var objMark = lstSchool[j];
                        if (j == 0)
                        {
                            sheet.SetCellValue(rowDistrict, 2, objMark.DistrictName);
                        }
                        if (j > 0 || (j == 0 && d > 0))
                        {
                            sheet.CopyPasteSameRowHeigh(sheet.GetRange(12, 1, 12, endColumn), row);
                        }

                        sheet.SetCellValue(row, 1, orderId);
                        sheet.SetCellValue(row, 2, objMark.SchoolName);

                        var lstAbs = lstMark.Where(s => s.SchoolID == objMark.SchoolId).ToList();
                        DateTime absDate = fromDate.Date;
                        startColumnDay = 4;
                        for (int dd = 0; dd < distantOfDay; dd++)
                        {
                            var objAbs = lstMark.FirstOrDefault(s => s.ProcessedDate.Value.Date == absDate);
                            int toalPupil = 0;
                            int totalabs = 0;
                            if (appliedLevelId == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                            {
                                toalPupil = objAbs.MarkLevel05 ?? 0;
                                totalabs = objAbs.MarkLevel01 ?? 0;
                            }
                            else if (appliedLevelId == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                            {
                                toalPupil = objAbs.MarkLevel06 ?? 0;
                                totalabs = objAbs.MarkLevel02 ?? 0;
                            }
                            else if (appliedLevelId == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                            {
                                toalPupil = objAbs.MarkLevel07 ?? 0;
                                totalabs = objAbs.MarkLevel03 ?? 0;
                            }
                            else if (appliedLevelId == GlobalConstants.APPLIED_LEVEL_CRECHE)
                            {
                                toalPupil = objAbs.MarkLevel08 ?? 0;
                                totalabs = objAbs.MarkLevel04 ?? 0;
                            }
                            if (d == 0)
                            {
                                sheet.SetCellValue(row, 3, toalPupil);
                            }
                            sheet.SetCellValue(row, startColumnDay, totalabs);
                            absDate = absDate.AddDays(1);

                            startColumnDay += 2;
                        }



                        /*sheet.SetCellValue(row, 4, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                        sheet.SetCellValue(row, 5, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
                        sheet.SetCellValue(row, 6, objMark.MarkLevel03 > 0 ? objMark.MarkLevel03 : 0);
                        sheet.SetCellValue(row, 7, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
                        sheet.SetCellValue(row, 8, objMark.MarkLevel05 > 0 ? objMark.MarkLevel05 : 0);
                        sheet.SetCellValue(row, 9, objMark.MarkLevel06 > 0 ? objMark.MarkLevel06 : 0);
                        sheet.SetCellValue(row, 10, objMark.MarkLevel07 > 0 ? objMark.MarkLevel07 : 0);
                        sheet.SetCellValue(row, 11, objMark.MarkLevel08 > 0 ? objMark.MarkLevel08 : 0);
                        sheet.SetCellValue(row, 12, objMark.MarkLevel09 > 0 ? objMark.MarkLevel09 : 0);
                        sheet.SetCellValue(row, 13, objMark.MarkLevel10 > 0 ? objMark.MarkLevel10 : 0);
                        sheet.SetCellValue(row, 14, objMark.MarkLevel11 > 0 ? objMark.MarkLevel11 : 0);
                        sheet.SetCellValue(row, 15, objMark.MarkLevel12 > 0 ? objMark.MarkLevel12 : 0);
                        sheet.SetCellValue(row, 16, objMark.MarkLevel13 > 0 ? objMark.MarkLevel13 : 0);
                        sheet.SetCellValue(row, 17, objMark.MarkLevel14 > 0 ? objMark.MarkLevel14 : 0);
                        sheet.SetCellValue(row, 18, objMark.MarkLevel15 > 0 ? objMark.MarkLevel15 : 0);
                        sheet.SetCellValue(row, 19, objMark.MarkLevel16 > 0 ? objMark.MarkLevel16 : 0);
                        sheet.SetCellValue(row, 20, objMark.MarkLevel17 > 0 ? objMark.MarkLevel17 : 0);
                        sheet.SetCellValue(row, 21, objMark.MarkLevel18 > 0 ? objMark.MarkLevel18 : 0);
                        sheet.SetCellValue(row, 22, objMark.MarkLevel19 > 0 ? objMark.MarkLevel19 : 0);
                        */
                        (sheet.GetRange(row, 1, row, 22)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                        row = row + 1;
                        orderId = orderId + 1;
                    }

                    if (row > startRow)
                    {
                        //SetFormulaDistrict(sheet, rowDistrict + 1, row - 1, 3, 22);
                    }
                    if (d > 0)
                    {
                        lstSumProvince.Add(rowDistrict);
                    }
                    rowDistrict = row;
                    row = row + 1;

                }
                //SetFormulaProvince(sheet, 10, lstSumProvince, 3, endColumn);
                #endregion



            }

            oBook.GetSheet(1).Delete();

            return oBook.ToStream();
        }

 #region Report phan he QLHD
        public List<DetailCostOfSalesReport> GetListDetailCostOfSalesReport(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int ChannelType = Utils.GetInt(dic, "ChannelTypeID");
            int Month = Utils.GetInt(dic, "Month");
            int Year = Utils.GetInt(dic, "Year");
            string UserName = Utils.GetString(dic, "UserName");
            string ReportCode = "ReportCostOfSales";
            int ReportType = Utils.GetInt(dic, "ReportType");

            DateTime datetimeNow = new DateTime(Year, Month, 1);

            DateTime FromDate = datetimeNow;
            int endDate = datetimeNow.EndOfMonth().Day;
            DateTime ToDate = new DateTime(Year, Month, endDate);
            this.InsertCostOfSalesReport(ProvinceID, DistrictID, ChannelType, Year, Month, FromDate, ToDate, ReportCode, UserName);
            //sau khi insert vao table statisticCM thi lay ra tra ve list
            IQueryable<StatisticCM> iQMarkStatisticDelete = (from m in StatisticCMBusiness.All
                                                             where m.ReportCode == ReportCode
                                                             && EntityFunctions.TruncateTime(m.FromDate) >= EntityFunctions.TruncateTime(FromDate)
                                                             && EntityFunctions.TruncateTime(m.ToDate) <= EntityFunctions.TruncateTime(ToDate)
                                                             && m.MonthID == Month
                                                             && (m.Year == Year || m.Year == (Year - 1))
                                                             select m);
            if (ProvinceID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(p => p.ProvinceID == ProvinceID);
            }
            if (DistrictID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == DistrictID);
            }
            if (ChannelType > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(p => p.ReportCriteria02 == ChannelType);
            }
            List<DetailCostOfSalesReport> lstResult = new List<DetailCostOfSalesReport>();
            if (ReportType == 1)//Bao cao tong hop doanh thu
            {
                lstResult = (from iq in iQMarkStatisticDelete
                             select new
                             {
                                 ProvinceName = iq.ProvinceName,
                                 DistrictName = iq.DistrictName,
                                 ChannelTypeName = iq.Data00,
                                 CollaboratorCode = iq.Data01,
                                 CollaboratorName = iq.Data02,
                                 UnitID = iq.SchoolID,
                                 NumOfInternalSubscriber = iq.Data18.HasValue ? iq.Data18.Value : 0,
                                 NumOfExternalSubscriber = iq.Data19.HasValue ? iq.Data19.Value : 0,
                                 TotalOfInternalRevenue = iq.Data20.HasValue ? iq.Data20.Value : 0,
                                 TotalOfExternalRevenue = iq.Data21.HasValue ? iq.Data21.Value : 0,
                             }).GroupBy(p => new
                             {
                                 p.ProvinceName,
                                 p.DistrictName,
                                 p.ChannelTypeName,
                                 p.CollaboratorCode,
                                 p.CollaboratorName,
                             }).Select(p => new DetailCostOfSalesReport
                             {
                                 ProvinceName = p.Key.ProvinceName,
                                 DistrictName = p.Key.DistrictName,
                                 ChannelTypeName = p.Key.ChannelTypeName,
                                 CollaboratorCode = p.Key.CollaboratorCode,
                                 CollaboratorName = p.Key.CollaboratorName,
                                 TotalCustommer = p.Count(c => c.UnitID > 0),
                                 NumOfInternalSubscriber = p.Sum(c => c.NumOfInternalSubscriber),
                                 NumOfExternalSubscriber = p.Sum(c => c.NumOfExternalSubscriber),
                                 TotalOfInternalRevenue = p.Sum(c => c.TotalOfInternalRevenue),
                                 TotalOfExternalRevenue = p.Sum(c => c.TotalOfExternalRevenue)
                             }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName)
                         .ThenBy(p => p.CollaboratorName).ToList();
            }
            else//Bao cao chi tiet doanh thu
            {
                lstResult = (from iq in iQMarkStatisticDelete
                             select new DetailCostOfSalesReport
                             {
                                 ProvinceName = iq.ProvinceName,
                                 DistrictName = iq.DistrictName,
                                 ChannelTypeName = iq.ReportCriteria02 == 1 ? "CTV" : iq.ReportCriteria02 == 2 ? "Đại lý" : "",
                                 CollaboratorCode = iq.Data01,
                                 CollaboratorName = iq.Data02,
                                 ServiceCode = iq.Data03,
                                 CustomerName = iq.UnitName,
                                 CustomerUserName = iq.UserName,
                                 InterRatio = iq.Data16.HasValue ? iq.Data16.Value : 0,
                                 ExterRatio = iq.Data17.HasValue ? iq.Data17.Value : 0,
                                 NumOfInternalSubscriber = iq.Data18.HasValue ? iq.Data18.Value : 0,
                                 NumOfExternalSubscriber = iq.Data19.HasValue ? iq.Data19.Value : 0,
                                 TotalOfInternalRevenue = iq.Data20.HasValue ? iq.Data20.Value : 0,
                                 TotalOfExternalRevenue = iq.Data21.HasValue ? iq.Data21.Value : 0,
                             }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName)
                         .ThenBy(p => p.CollaboratorName).ToList();
            }


            return lstResult;
        }
        private void InsertCostOfSalesReport(int ProvinceID, int DistrictID, int ChannelTypeID, int YearID, int MonthID, DateTime FromDate, DateTime ToDate, string ReportCode, string UserName)
        {
            SMAS.Models.Models.SMASEntities contenxt1 = new SMAS.Models.Models.SMASEntities();

            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "BCChiTiet_DoanhThu",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };
                                command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                command.Parameters.Add("P_CHANEL_TYPE_ID", ChannelTypeID);
                                command.Parameters.Add("P_YEAR", YearID);
                                command.Parameters.Add("P_MONTH_ID", MonthID);
                                command.Parameters.Add("P_FROMDATE", FromDate);
                                command.Parameters.Add("P_TODATE", ToDate);
                                command.Parameters.Add("P_CREATED_USERNAME", UserName);
                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public List<CurrentAssignCollaboratorReportBO> getCurrentAssignCollaboratorReport(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            short UnitTypeID = Utils.GetShort(dic, "UnitTypeID");
            short Status = Utils.GetShort(dic, "Status");
            List<CurrentAssignCollaboratorReportBO> lstResult = (from ma in CBManagingAscriptionBusiness.All
                                                                 join cb in CBCollaboratorBusiness.All on ma.CBID equals cb.CBID
                                                                 join ct in CBChannelTypeBusiness.All on cb.ChannelTypeID equals ct.ChannelTypeID
                                                                 join sc in SchoolProfileBusiness.All on ma.SchoolID equals sc.SchoolProfileID into sc_g
                                                                 from sch in sc_g.DefaultIfEmpty()
                                                                 join sd in SupervisingDeptBusiness.All on ma.SuperVisingDeptID equals sd.SupervisingDeptID into sd_g
                                                                 from sde in sd_g.DefaultIfEmpty()
                                                                 where
                                                                 (UnitTypeID == 0 || ((UnitTypeID == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && ma.Type == UnitTypeID) || (UnitTypeID > GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && ma.Type >= UnitTypeID)))
                                                                     &&
                                                                     (ProvinceID == 0 ||
                                                                     ((ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sch.ProvinceID == ProvinceID)
                                                                     || (ma.Type > GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sde.ProvinceID == ProvinceID)))
                                                                     &&
                                                                     (DistrictID == 0 ||
                                                                     ((ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sch.DistrictID == DistrictID)
                                                                     || (ma.Type > GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sde.DistrictID == DistrictID)))
                                                                     && (Status == -1 || ma.Status == Status)
                                                                 select new CurrentAssignCollaboratorReportBO()
                                                                 {
                                                                     TypeID = ma.Type,
                                                                     ProvinceName = ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.Province.ProvinceName : sde.Province.ProvinceName,
                                                                     DistrictName = ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.District.DistrictName : sde.District.DistrictName,
                                                                     CommuneName = "",
                                                                     CustomerName = ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.SchoolName : sde.SupervisingDeptName,
                                                                     //CustomerUserName = ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ?
                                                                     //    sch.SchoolUserName : sde.SupervisingDeptUserName,
                                                                     AdminID = ma.Type == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.AdminID : sde.AdminID,
                                                                     CurrentAssignStatus = ma.Status == GlobalConstants.COLLABORATOR_STATUS_ASSIGNED ? "Đã gán" : "Đã bỏ gán",
                                                                     CollaboratorCode = cb.CBCode,
                                                                     CollaboratorName = cb.FullName,
                                                                     ChannelTypeName = ct.ChannelTypeName,
                                                                     AssignedDate = ma.AssignDate,
                                                                     EndAssignedDate = ma.EndDate
                                                                 })
                .OrderBy(o => o.ProvinceName).ThenBy(o => o.DistrictName).ThenBy(o => o.CustomerName)
                .ToList();
            List<int> lstAdminID = lstResult.Where(p => p.AdminID.HasValue).Select(p => p.AdminID.Value).Distinct().ToList();
            IDictionary<int, string> lstUserName = SchoolProfileBusiness.GetListUserNameByListUserID(lstAdminID);
            lstResult.ForEach(p =>
            {
                if (lstUserName.Where(u => u.Key == p.AdminID).Count() > 0)
                {
                    p.CustomerUserName = lstUserName[p.AdminID.Value];
                }
            });

            return lstResult;
        }

        public List<DetailDeferredPaymentReportBO> getDetailDeferredPaymentReport(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string UserName = Utils.GetString(dic, "UserName");
            string ReportCode = "DeferredPaymentReport";
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate", DateTime.Now);
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate", DateTime.Now);
            List<DetailDeferredPaymentReportBO> lstResult = new List<DetailDeferredPaymentReportBO>();
            this.InsertDeferredPaymentReport(ProvinceID, DistrictID, FromDate.Value, ToDate.Value, ReportCode, UserName);
            IQueryable<StatisticCM> iQMarkStatisticDelete = (from m in StatisticCMBusiness.All
                                                             where m.ReportCode == ReportCode
                                                             && EntityFunctions.TruncateTime(m.FromDate) >= EntityFunctions.TruncateTime(FromDate)
                                                             && EntityFunctions.TruncateTime(m.ToDate) <= EntityFunctions.TruncateTime(ToDate)
                                                             select m);
            if (ProvinceID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(p => p.ProvinceID == ProvinceID);
            }
            if (DistrictID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == DistrictID);
            }
            lstResult = (from iq in iQMarkStatisticDelete
                         select new DetailDeferredPaymentReportBO
                         {
                             ProvinceName = iq.ProvinceName,
                             DistrictName = iq.DistrictName,

                             SchoolName = iq.UnitName,
                             SchoolUserName = iq.UserName,
                             ServicePackage = iq.Data00,
                             FromDate = iq.Data01,
                             ToDate = iq.Data02,
                             NumOfParentUnpaid = (int)iq.Data16,
                             NumOfParentOverdue = (int)iq.Data17,
                             SumMoneyUnpaid = iq.Data18,
                             SumMoneyPaidOnTime = iq.Data19,
                             SumMoneyPaidOverdue = iq.Data20,
                             NumOfInSMSOverdueSubscriber = (int)iq.Data21,
                             NumOfExSMSOverdueSubscriber = (int)iq.Data22
                         }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName)
                         .ThenBy(p => p.SchoolName).ToList();

            return lstResult;
        }
        private void InsertDeferredPaymentReport(int ProvinceID, int DistrictID, DateTime FromDate, DateTime ToDate, string ReportCode, string UserName)
        {
            SMAS.Models.Models.SMASEntities contenxt1 = new SMAS.Models.Models.SMASEntities();

            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "BCChiTiet_TraCham",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };
                                command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                command.Parameters.Add("P_CREATED_USERNAME", UserName);
                                command.Parameters.Add("P_FROMDATE", FromDate);
                                command.Parameters.Add("P_TODATE", ToDate);

                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public List<DetailSMSEDUReportBO> getDetailSMSEDUReport(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string UserName = Utils.GetString(dic, "UserName");
            string ReportCode = "SMSEDUReport";
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate", DateTime.Now);
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate", DateTime.Now);
            List<DetailSMSEDUReportBO> lstResult = new List<DetailSMSEDUReportBO>();
            this.InsertSMSEDUReport(ProvinceID, DistrictID, ReportCode, UserName, FromDate.Value, ToDate.Value, 1);
            IQueryable<StatisticCM> iQMarkStatisticDelete = (from m in StatisticCMBusiness.All
                                                             where m.ReportCode == ReportCode
                                                             && EntityFunctions.TruncateTime(m.FromDate) >= EntityFunctions.TruncateTime(FromDate)
                                                             && EntityFunctions.TruncateTime(m.ToDate) <= EntityFunctions.TruncateTime(ToDate)
                                                             select m);
            if (ProvinceID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(p => p.ProvinceID == ProvinceID);
            }
            if (DistrictID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == DistrictID);
            }
            lstResult = (from o in iQMarkStatisticDelete
                         select new DetailSMSEDUReportBO
                         {
                             ProvinceName = o.ProvinceName,
                             DistrictName = o.DistrictName,

                             SchoolName = o.UnitName,
                             SchoolUserName = o.UserName,
                             NumOfInternalSubscriber = (int)o.Data16,
                             NumOfExternalSubscriber = (int)o.Data17,
                             NumOfSubscriberActive = (int)o.Data21,
                             NumOfSubscriberDeactive = (int)o.Data18,
                             SumMoneyTopUp = o.Data22,
                             SumMoneyConsumer = o.Data23,
                             SumAllPackage = o.Data24,
                             SumSMSTeacherInternal = (int)o.Data25,
                             SumSMSTeacherExternal = (int)o.Data26,
                             SumSMSParentInternal = (int)o.Data27,
                             SumSMSParentExternal = (int)o.Data28,
                             SumTeacherReceiveSMS = (int)o.Data29,
                             SumParentReceiveSMS = (int)o.Data30
                         }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName)
                         .ThenBy(p => p.SchoolName).ToList();
            return lstResult;
        }

        public List<SubscriberStatisticsPackageReportBO> getSubscriberStatisticsPackageReport(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string UserName = Utils.GetString(dic, "UserName");
            string ReportCode = "SubscriberStatisticsReport";
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate", DateTime.Now);
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate", DateTime.Now);
            this.InsertSMSEDUReport(ProvinceID, DistrictID, ReportCode, UserName, FromDate.Value, ToDate.Value, 2);
            List<SubscriberStatisticsPackageReportBO> lstResult = new List<SubscriberStatisticsPackageReportBO>();
            IQueryable<StatisticCM> iQMarkStatisticDelete = (from m in StatisticCMBusiness.All
                                                             where m.ReportCode == ReportCode
                                                             && EntityFunctions.TruncateTime(m.FromDate) >= EntityFunctions.TruncateTime(FromDate)
                                                             && EntityFunctions.TruncateTime(m.ToDate) <= EntityFunctions.TruncateTime(ToDate)
                                                             && m.CreatedUserName == UserName
                                                             select m);
            if (ProvinceID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(p => p.ProvinceID == ProvinceID);
            }
            if (DistrictID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == DistrictID);
            }
            lstResult = (from o in iQMarkStatisticDelete
                         select new SubscriberStatisticsPackageReportBO
                         {
                             ProvinceName = o.ProvinceName,
                             DistrictName = o.DistrictName,
                             SchoolName = o.UnitName,
                             SchoolUserName = o.UserName,
                             ServicePackage = o.Data00,
                             NumOfSubscriberRegOnlySem1 = (int)o.Data16,
                             NumOfSubscriberRegOnlySem2 = (int)o.Data17,
                             NumOfSubscriberRegOnlySem3 = (int)o.Data18
                         }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName)
                         .ThenBy(p => p.SchoolName).ToList();
            return lstResult;
        }

        private void InsertSMSEDUReport(int ProvinceID, int DistrictID, string ReportCode, string UserName, DateTime FromDate, DateTime ToDate, int TypeReport)
        {
            SMAS.Models.Models.SMASEntities contenxt1 = new SMAS.Models.Models.SMASEntities();

            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand();
                                if (TypeReport == 1)
                                {
                                    command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "BCChiTiet_SMSEDU",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                    command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                    command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                    command.Parameters.Add("P_CREATED_USERNAME", UserName);
                                    command.Parameters.Add("P_FROMDATE", FromDate);
                                    command.Parameters.Add("P_TODATE", ToDate);
                                }
                                else
                                {
                                    command = new OracleCommand
                                    {
                                        CommandType = CommandType.StoredProcedure,
                                        CommandText = "BCThongke_TheoGoiCuoc",
                                        Connection = conn,
                                        CommandTimeout = 0
                                    };
                                    command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                    command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                    command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                    command.Parameters.Add("P_YEAR", FromDate.Year);
                                    command.Parameters.Add("P_FROMDATE", FromDate);
                                    command.Parameters.Add("P_TODATE", ToDate);
                                    command.Parameters.Add("P_CREATED_USERNAME", UserName);
                                }

                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Lay bao cao Chi tiet Hop dong SMAS 
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<DetailContractReportBO> getDetailContractReport(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic,"ProvinceID");
            int DistrictID = Utils.GetInt(dic,"DistrictID");
            DateTime? FromDate = Utils.GetDateTime(dic,"FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic,"ToDate");

            DateTime OnlyDateFrom = new DateTime(FromDate.Value.Year, FromDate.Value.Month, FromDate.Value.Day, 0, 0, 0);
            DateTime OnlyDateTo = new DateTime(ToDate.Value.Year, ToDate.Value.Month, ToDate.Value.Day, 23, 59, 59);
            List<DetailContractReportBO> lstResult = (from ct in CBContractBusiness.All
                                                      join rm in CBRoamingModeBusiness.All on ct.RoamingModeID equals rm.RoamingModeID
                                                      join sc in SchoolProfileBusiness.All on ct.SchoolID equals sc.SchoolProfileID
                                                          into sc_g
                                                      from sch in sc_g.DefaultIfEmpty()
                                                      join sd in SupervisingDeptBusiness.All on ct.SuperVisingDeptID equals sd.SupervisingDeptID
                                                          into sd_g
                                                      from sde in sd_g.DefaultIfEmpty()
                                                      where
                                                        ct.CreatedTime >= OnlyDateFrom && ct.CreatedTime <= OnlyDateTo
                                                       &&
                                                       (ProvinceID == 0 ||
                                                       ((ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sch.ProvinceID == ProvinceID)
                                                       || (ct.ContractType > GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sde.ProvinceID == ProvinceID)))
                                                       &&
                                                       (DistrictID == 0 ||
                                                       ((ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sch.DistrictID == DistrictID)
                                                       || (ct.ContractType > GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL && sde.DistrictID == DistrictID)))
                                                       && ct.Status == GlobalConstants.CONTRACT_STATUS_ACTIVE // Trang thai da dau noi
                                                      select new DetailContractReportBO()
                                                      {
                                                          ProvinceName = ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.Province.ProvinceName : sde.Province.ProvinceName,
                                                          DistrictName = ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.District.DistrictName : sde.District.DistrictName,
                                                          CustomerName = ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ?
                                                              sch.SchoolName : sde.SupervisingDeptName,
                                                          Address = ct.TradingAddress,
                                                          UnitID = ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ? sch.AdminID : sde.AdminID,
                                                          //CustomerUserName = ct.ContractType == GlobalConstants.CONTRACT_TYPE_WITH_SCHOOL ?
                                                          //    sch.SchoolUserName : sde.SupervisingDeptUserName,
                                                          CreatedDate = ct.CreatedTime,
                                                          RoamingMode = rm.RoamingModeName,
                                                          MainRoamingCode = ct.MainRoamingCode,
                                                          ExtraRoamingCode = ct.ExtraRoaningCode
                                                      })
                    .OrderBy(o => o.ProvinceName).ThenBy(o => o.DistrictName).ThenBy(o => o.CustomerName)
                    .ToList();
            List<int> lstUserID = lstResult.Where(p => p.UnitID.HasValue).Select(p => p.UnitID.Value).Distinct().ToList();
            IDictionary<int, string> lstUserNameByUserID = SchoolProfileBusiness.GetListUserNameByListUserID(lstUserID);
            lstResult.ForEach(p =>
            {
                if (lstUserNameByUserID.Where(u=>u.Key == p.UnitID).Count() > 0)
                {
                    p.CustomerUserName = lstUserNameByUserID[p.UnitID.Value];
                }
            });
            return lstResult;
        }
        #endregion
    }
}
