﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using System.Drawing;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class TeachingScheduleBusiness
    {
        public IQueryable<TeachingSchedule> Search(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int SectionID = Utils.GetInt(dic, "SectionID");
            int partitionID = UtilsBusiness.GetPartionId(SchoolID);
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstSectionID = Utils.GetIntList(dic, "lstSectionID");
            int SchoolWeekID = Utils.GetInt(dic, "SchoolWeekID");
            List<int> lstSchoolWeekID = Utils.GetIntList(dic, "lstSchoolWeekID");
            List<string> lstFromTo = Utils.GetStringList(dic, "lstFromTo");
            string StrFromDate = Utils.GetString(dic, "FromDate");
            List<string> lstFromDate = Utils.GetStringList(dic, "lstFromDate");

            IQueryable<TeachingSchedule> query = TeachingScheduleBusiness.All;

            if (lstFromDate.Count() > 0)
            {
                DateTime fromDate = Convert.ToDateTime(lstFromDate[0]);
                DateTime toDate = Convert.ToDateTime(lstFromDate[lstFromDate.Count - 1]).AddDays(6);

                query = query.Where(p => p.ScheduleDate >= fromDate && p.ScheduleDate <= toDate);
            }

            if (lstFromTo.Count() > 0 && lstFromTo.Count == 2)
            {
                DateTime fromDate = Convert.ToDateTime(lstFromTo[0]);
                DateTime toDate = Convert.ToDateTime(lstFromTo[1]);

                query = query.Where(p => p.ScheduleDate >= fromDate && p.ScheduleDate <= toDate);
            }

            if (!string.IsNullOrEmpty(StrFromDate))
            {
                DateTime fromDate = Convert.ToDateTime(StrFromDate);
                query = query.Where(p => p.ScheduleDate == fromDate);
            }


            if (AcademicYearID > 0)
            {
                query = query.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (SchoolID > 0)
            {
                query = query.Where(p => p.SchoolID == SchoolID && p.Last2DigitNumberSchool == partitionID);
            }
            if (SchoolWeekID != 0)
            {
                query = query.Where(p => p.SchoolWeekID == SchoolWeekID);
            }
            else if (lstSchoolWeekID.Count > 0)
            {
                query = query.Where(p => lstSchoolWeekID.Contains(p.SchoolWeekID));
            }
            if (SubjectID > 0)
            {
                query = query.Where(p => p.SubjectID == SubjectID);
            }
            if (TeacherID > 0)
            {
                query = query.Where(p => p.TeacherID == TeacherID);
            }
            if (SectionID > 0)
            {
                query = query.Where(p => p.SectionID == SectionID);
            }
            if (lstSectionID.Count > 0)
            {
                query = query.Where(p => lstSectionID.Contains(p.SectionID));
            }
            if (ClassID > 0)
            {
                query = query.Where(p => p.ClassID == ClassID);
            }
            return query;
        }
        public void InsertOrUpdate(TeachingSchedule objTeachingSchedule, IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int partitionID = UtilsBusiness.GetPartionId(SchoolID);
            int SectionID = objTeachingSchedule.SectionID;
            int SubjectOrder = objTeachingSchedule.SubjectOrder;
            DateTime? ScheduDate = Utils.GetDateTime(dic, "ScheduDate");
            /*TeachingSchedule objTS = (from t in TeachingScheduleBusiness.All
                                      where t.AcademicYearID == AcademicYearID
                                      && t.SchoolID == SchoolID
                                      && t.SectionID == SectionID
                                      && t.SubjectOrder == SubjectOrder
                                      && t.Last2DigitNumberSchool == partitionID
                                      && t.ScheduleDate == ScheduDate
                                      && t.TeacherID == TeacherID
                                     // && t.SchoolWeekID == objTeachingSchedule.SchoolWeekID
                                      select t).FirstOrDefault();*/
            TeachingSchedule objTS = TeachingScheduleBusiness.Find(objTeachingSchedule.TeachingScheduleID);

            if (objTS != null)//Update
            {
                // danh sách cần update khi check Nghỉ dạy
                List<TeachingSchedule> lstTSNeedUpdate = new List<TeachingSchedule>();
                //List<int> lstOrderPPTC = new List<int>();
                if (((objTS.Status == 0 && objTeachingSchedule.Status == 2)
                          || (objTS.Status == 2 && objTeachingSchedule.Status == 0)))
                {
                    IDictionary<string, object> dicUpdate = new Dictionary<string, object>();
                    dicUpdate.Add("SchoolID", SchoolID);
                    dicUpdate.Add("AcademicYearID", AcademicYearID);
                    dicUpdate.Add("ClassID", objTS.ClassID);
                    dicUpdate.Add("SubjectID", objTeachingSchedule.SubjectID);
                    dicUpdate.Add("TeacherID", TeacherID);

                    lstTSNeedUpdate = TeachingScheduleBusiness.Search(dicUpdate)
                                    .Where(x => x.TeachingScheduleOrder >= objTeachingSchedule.TeachingScheduleOrder
                                             && x.Status == 0)
                                    .ToList();

                    if (objTeachingSchedule.AssignSubjectID.HasValue)
                    {
                        lstTSNeedUpdate = lstTSNeedUpdate.Where(x => x.AssignSubjectID == objTeachingSchedule.AssignSubjectID).ToList();
                    }
                    else
                    {
                        var lstAssignSubject = AssignSubjectConfigBusiness.Search(dicUpdate);
                        if (lstAssignSubject.Count() > 0)
                        {
                            lstTSNeedUpdate = lstTSNeedUpdate.Where(x => !lstAssignSubject.Any(o => o.AssignSubjectConfigID == x.AssignSubjectID)).ToList();
                        }                     
                    }

                    lstTSNeedUpdate = lstTSNeedUpdate.OrderBy(x => x.TeachingScheduleOrder).ToList();
                    // lstOrderPPTC = lstTSNeedUpdate.Select(x => x.TeachingScheduleOrder).ToList();
                }

                if (objTS.Status == 0 && objTeachingSchedule.Status == 2)
                { // tick chọn Nghĩ dạy
                    objTS.Status = objTeachingSchedule.Status;
                    // Đẩy lùi về 1 tiết PPCT
                    lstTSNeedUpdate = lstTSNeedUpdate.Where(x => x.TeachingScheduleOrder > objTeachingSchedule.TeachingScheduleOrder).ToList();
                    if (lstTSNeedUpdate.Count() > 0)
                    {
                        int order = 0;
                        foreach (var item in lstTSNeedUpdate)
                        {
                            order = item.TeachingScheduleOrder;
                            item.TeachingScheduleOrder = order - 1;
                            TeachingScheduleBusiness.Update(item);
                        }

                    }
                }
                else if (objTS.Status == 2 && objTeachingSchedule.Status == 0)
                { // Bỏ tick nghĩ dạy                 
                    objTS.Status = objTeachingSchedule.Status;
                    // Đẩy tăng lên 1 tiết PPCT
                    if (lstTSNeedUpdate.Count() > 0)
                    {
                        lstTSNeedUpdate = lstTSNeedUpdate.Where(x => x.TeachingScheduleID != objTeachingSchedule.TeachingScheduleID).ToList();
                        int order = 0;
                        foreach (var item in lstTSNeedUpdate)
                        {
                            order = item.TeachingScheduleOrder;
                            item.TeachingScheduleOrder = order + 1;
                            TeachingScheduleBusiness.Update(item);
                        }

                    }
                }
                else if (objTS.Status == 0 && objTeachingSchedule.Status == 0)
                {// bình thường
                    objTS.UpdatedTime = DateTime.Now;
                    objTS.SubjectID = objTeachingSchedule.SubjectID;
                    objTS.AssignSubjectID = objTeachingSchedule.AssignSubjectID;
                    objTS.ClassID = objTeachingSchedule.ClassID;
                    objTS.TeachingScheduleOrder = objTeachingSchedule.TeachingScheduleOrder;
                    objTS.TeachingUtensil = objTeachingSchedule.TeachingUtensil;
                    objTS.Quantity = objTeachingSchedule.Quantity;
                    objTS.InRoom = objTeachingSchedule.InRoom;
                    objTS.IsSelfMade = objTeachingSchedule.IsSelfMade;
                    objTS.Status = objTeachingSchedule.Status;
                }

                TeachingScheduleBusiness.Update(objTS);
            }
            else//Insert
            {
                // lên lịch dạy bù
                if (objTeachingSchedule.Status == 1)
                {
                    List<TeachingSchedule> lstTSNeedUpdate = new List<TeachingSchedule>();

                    IDictionary<string, object> dicUpdate = new Dictionary<string, object>();
                    dicUpdate.Add("SchoolID", SchoolID);
                    dicUpdate.Add("AcademicYearID", AcademicYearID);
                    dicUpdate.Add("ClassID", objTeachingSchedule.ClassID);
                    dicUpdate.Add("SubjectID", objTeachingSchedule.SubjectID);
                    dicUpdate.Add("TeacherID", TeacherID);

                    lstTSNeedUpdate = TeachingScheduleBusiness.Search(dicUpdate)
                                    .Where(x => x.TeachingScheduleOrder >= objTeachingSchedule.TeachingScheduleOrder
                                                && x.Status == 0)
                                    .ToList();

                    if (objTeachingSchedule.AssignSubjectID.HasValue)
                    {
                        lstTSNeedUpdate = lstTSNeedUpdate.Where(x => x.AssignSubjectID == objTeachingSchedule.AssignSubjectID).ToList();
                    }
                    else
                    {
                        var lstAssignSubject = AssignSubjectConfigBusiness.Search(dicUpdate);
                        if (lstAssignSubject.Count() > 0)
                        {
                            lstTSNeedUpdate = lstTSNeedUpdate.Where(x => !lstAssignSubject.Any(o => o.AssignSubjectConfigID == x.AssignSubjectID)).ToList();
                        }                       
                    }

                    lstTSNeedUpdate = lstTSNeedUpdate.OrderBy(x => x.TeachingScheduleOrder).ToList();

                    if (lstTSNeedUpdate.Count() > 0)
                    {
                        int order = 0;
                        foreach (var item in lstTSNeedUpdate)
                        {
                            order = item.TeachingScheduleOrder;
                            item.TeachingScheduleOrder = order + 1;
                            TeachingScheduleBusiness.Update(item);
                        }
                    }

                    objTeachingSchedule.CreatedTime = DateTime.Now;
                    TeachingScheduleBusiness.Insert(objTeachingSchedule);

                }// thêm 1 tiết bình thường
                else
                {
                    objTeachingSchedule.CreatedTime = DateTime.Now;
                    TeachingScheduleBusiness.Insert(objTeachingSchedule);
                }


            }
            TeachingScheduleBusiness.Save();
        }
        public void InsertOrUpdateList(List<TeachingSchedule> lstResult, IDictionary<string, object> dic)
        {
            List<TeachingSchedule> Query = this.Search(dic).Where(x => x.Status == 0).ToList();
            TeachingSchedule objDB = null;
            TeachingSchedule objResult = null;
            for (int i = 0; i < lstResult.Count; i++)
            {
                objResult = lstResult[i];
                objDB = Query.Where(p => p.ScheduleDate == objResult.ScheduleDate
                               && p.SectionID == objResult.SectionID
                               && p.SubjectOrder == objResult.SubjectOrder)
                               .FirstOrDefault();
                if (objDB != null)
                {
                    objDB.AssignSubjectID = objResult.AssignSubjectID;
                    objDB.SubjectID = objResult.SubjectID;
                    objDB.ClassID = objResult.ClassID;
                    objDB.TeachingScheduleOrder = objResult.TeachingScheduleOrder;
                    objDB.TeachingUtensil = objResult.TeachingUtensil;
                    objDB.Quantity = objResult.Quantity;
                    objDB.InRoom = objResult.InRoom;
                    objDB.IsSelfMade = objResult.IsSelfMade;
                    objDB.UpdatedTime = DateTime.Now;

                    TeachingScheduleBusiness.Update(objDB);
                }
                else
                {
                    objResult.CreatedTime = DateTime.Now;
                    TeachingScheduleBusiness.Insert(objResult);
                }
            }

            TeachingScheduleBusiness.Save();
        }

        public void Delete(TeachingSchedule obj)
        {
            if (obj.Status == 1)
            {
                List<TeachingSchedule> lstTSNeedUpdate = new List<TeachingSchedule>();
                IDictionary<string, object> dicUpdate = new Dictionary<string, object>();
                dicUpdate.Add("SchoolID", obj.SchoolID);
                dicUpdate.Add("AcademicYearID", obj.AcademicYearID);
                dicUpdate.Add("ClassID", obj.ClassID);
                dicUpdate.Add("SubjectID", obj.SubjectID);
                dicUpdate.Add("TeacherID", obj.TeacherID);

                lstTSNeedUpdate = TeachingScheduleBusiness.Search(dicUpdate)
                                .Where(x => x.TeachingScheduleOrder > obj.TeachingScheduleOrder
                                    && x.Status != 2).ToList();
                if (!obj.AssignSubjectID.HasValue)
                {
                    var lstAssignSubject = AssignSubjectConfigBusiness.Search(dicUpdate);
                    if (lstAssignSubject.Count() > 0)
                    {
                        lstTSNeedUpdate = lstTSNeedUpdate.Where(x => !lstAssignSubject.Any(o => o.AssignSubjectConfigID == x.AssignSubjectID)).ToList();
                    }              
                }
                else
                {
                    lstTSNeedUpdate = lstTSNeedUpdate.Where(x => x.AssignSubjectID == obj.AssignSubjectID).ToList();
                }

                lstTSNeedUpdate = lstTSNeedUpdate.OrderBy(x => x.TeachingScheduleOrder).ToList();

                if (lstTSNeedUpdate.Count() > 0)
                {
                    int order = 0;
                    foreach (var item in lstTSNeedUpdate)
                    {
                        order = item.TeachingScheduleOrder;
                        item.TeachingScheduleOrder = order - 1;
                        TeachingScheduleBusiness.Update(item);
                    }
                }
            }

            TeachingScheduleBusiness.Delete(obj.TeachingScheduleID);
            TeachingScheduleBusiness.Save();
        }

        public ProcessedReport InsertProcessReport(IDictionary<string, object> dicReport, Stream excel, string reportCode)
        {
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int TeacherID = Utils.GetInt(dicReport, "TeacherID");
            string FromDate = Utils.GetString(dicReport, "FromDate");
            string TeacherName = Utils.GetString(dicReport, "TeacherName");

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, TeacherID, FromDate);
            pr.ReportData = ReportUtils.Compress(excel);

            //Tạo tên file        
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[TenGV]", TeacherName);
            outputNamePattern = outputNamePattern.Replace("[NgayBD]", FromDate);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"TeacherID", TeacherID},
                {"FromDate", FromDate},
                {"TeacherName", TeacherName}
            };

            ProcessedReportParameterRepository.Insert(dicInsert, pr);

            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        private string GetHashKey(int AcademicYearID, int SchoolID, int TeacherID, string FromDate)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"TeacherID", TeacherID},
                {"FromDate", FromDate}
            };
            return ReportUtils.GetHashKey(dic);
        }

    }
}