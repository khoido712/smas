/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System;
using System.Data.SqlClient;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ClassSubjectBusiness
    {
        #region Them moi

        /// <summary>
        /// QuangLM sua lai
        /// Bo cac doan code validate ngoai ngu
        /// Xu ly cach validate du lieu cho hop ly
        /// </summary>
        /// <param name="ListClassSubject"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        public void Insert(List<ClassSubject> ListClassSubject, int SchoolID, int AcademicYearID, int Grade)
        {

            if (ListClassSubject == null || ListClassSubject.Count == 0)
            {
                return;
            }
            // Kiem tra lop hoc va mon hoc co ton tai trong he thong hay khong
            List<int> listClassID = ListClassSubject.Select(o => o.ClassID).Distinct().ToList();
            List<ClassProfile> listClass = ClassProfileBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID
                && o.AcademicYearID == AcademicYearID && listClassID.Contains(o.ClassProfileID) && o.IsActive == true).ToList();
            if (listClass.Count != listClassID.Count)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Mon hoc co ton tai trong he thong
            List<int> listSubjectID = ListClassSubject.Select(o => o.SubjectID).Distinct().ToList();
            int countSubject = SubjectCatBusiness.AllNoTracking.Where(o => listSubjectID.Contains(o.SubjectCatID)).Count();
            if (countSubject != listSubjectID.Count)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            // Lay thong tin mon hoc khai bao cho truong
            List<SchoolSubject> ListSchoolSubject = SchoolSubjectBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID
                && o.AcademicYearID == AcademicYearID && o.EducationLevel.Grade == Grade).ToList();
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (aca == null || !AcademicYearBusiness.IsCurrentYear(aca))
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }
            foreach (ClassSubject ClassSubject in ListClassSubject)
            {
                ClassProfile cp = listClass.Where(o => o.ClassProfileID == ClassSubject.ClassID).First();
                // Kiem tra phai khai bao mon hoc trong truong truoc khi khai bao cho lop hoc                
                List<SchoolSubject> ListSchoolSubjectBySubject = ListSchoolSubject.Where(o => o.SubjectID == ClassSubject.SubjectID
                    && o.EducationLevelID == cp.EducationLevelID).ToList();
                if (ListSchoolSubjectBySubject == null || ListSchoolSubjectBySubject.Count == 0)
                {
                    throw new BusinessException("ClassSubject_Err_SchoolSubject");
                }

                // Kiem tra ngay bat dau va ngay ket thuc phai thuoc nam hoc
                if (ClassSubject.ClassID != 0 && aca != null)
                {
                    Utils.ValidateAfterDate(aca.SecondSemesterEndDate,
                        ClassSubject.StartDate, "AcademicYear_Label_SecondSemesterEndDate", "Common_Label_StartDate");
                    Utils.ValidateAfterDate(ClassSubject.StartDate,
                        aca.FirstSemesterStartDate, "AcademicYear_Label_FirstSemesterStartDate", "Common_Label_StartDate");
                    Utils.ValidateAfterDate(aca.SecondSemesterEndDate,
                        ClassSubject.EndDate, "AcademicYear_Label_SecondSemesterEndDate", "Common_Label_EndDate");
                    Utils.ValidateAfterDate(ClassSubject.EndDate,
                        aca.FirstSemesterStartDate, "AcademicYear_Label_FirstSemesterStartDate", "Common_Label_EndDate");
                }

                // Kiem tra so tiet hoc cua moi hoc ky phai la so nguyen duong
                ClassSubject.SectionPerWeekFirstSemester = ClassSubject.SectionPerWeekFirstSemester.HasValue ? ClassSubject.SectionPerWeekFirstSemester.Value : 0;
                ClassSubject.SectionPerWeekSecondSemester = ClassSubject.SectionPerWeekSecondSemester.HasValue ? ClassSubject.SectionPerWeekSecondSemester.Value : 0;
                if (ClassSubject.SectionPerWeekFirstSemester < 0)
                {
                    throw new BusinessException("ClassSubject_Err_SectionPerWeekFirstSemester");
                }
                if (ClassSubject.SectionPerWeekSecondSemester < 0)
                {
                    throw new BusinessException("ClassSubject_Err_SectionPerWeekSecondSemester");
                }
            }

            // Du lieu hop le, thuc hien them moi vao CSDL
            foreach (ClassSubject ClassSubject in ListClassSubject)
            {
                this.Insert(ClassSubject);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        #endregion Them moi

        #region Cap nhat

        /// <summary>
        /// QuangLM sua lai
        /// Bo cac doan code validate ngoai ngu
        /// Xu ly cach validate du lieu cho hop ly
        /// </summary>
        /// <param name="ListClassSubject"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        public void Update(List<ClassSubject> ListClassSubject, int SchoolID, int AcademicYearID, int Grade)
        {
            if (ListClassSubject == null || ListClassSubject.Count == 0)
            {
                return;
            }

            // Kiem tra lop hoc va mon hoc co ton tai trong he thong hay khong
            List<int> listClassID = ListClassSubject.Select(o => o.ClassID).Distinct().ToList();
            List<ClassProfile> listClass = ClassProfileBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID
                && o.AcademicYearID == AcademicYearID && listClassID.Contains(o.ClassProfileID) && o.IsActive == true).ToList();
            if (listClass.Count != listClassID.Count)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Mon hoc co ton tai trong he thong
            List<int> listSubjectID = ListClassSubject.Select(o => o.SubjectID).Distinct().ToList();
            int countSubject = SubjectCatBusiness.AllNoTracking.Where(o => listSubjectID.Contains(o.SubjectCatID)).Count();
            if (countSubject != listSubjectID.Count)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            // Lay thong tin mon hoc khai bao cho truong
            List<SchoolSubject> ListSchoolSubject = SchoolSubjectBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID
                && o.AcademicYearID == AcademicYearID && o.EducationLevel.Grade == Grade).ToList();

            // Kiem tra nam hoc
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (aca == null || !AcademicYearBusiness.IsCurrentYear(aca))
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            foreach (ClassSubject ClassSubject in ListClassSubject)
            {
                // Kiem tra thoi gian nam hoc
                if (aca != null)
                {
                    // Kiem tra ngay bat dau va ngay ket thuc phai thuoc nam hoc
                    Utils.ValidateAfterDate(aca.SecondSemesterEndDate,
                        ClassSubject.StartDate, "AcademicYear_Label_SecondSemesterEndDate", "Common_Label_StartDate");
                    Utils.ValidateAfterDate(ClassSubject.StartDate,
                        aca.FirstSemesterStartDate, "AcademicYear_Label_FirstSemesterStartDate", "Common_Label_StartDate");
                    Utils.ValidateAfterDate(aca.SecondSemesterEndDate,
                        ClassSubject.EndDate, "AcademicYear_Label_SecondSemesterEndDate", "Common_Label_EndDate");
                    Utils.ValidateAfterDate(ClassSubject.EndDate,
                        aca.FirstSemesterStartDate, "AcademicYear_Label_FirstSemesterStartDate", "Common_Label_EndDate");
                }

                ClassProfile cp = listClass.Where(o => o.ClassProfileID == ClassSubject.ClassID).First();
                // Kiem tra phai khai bao mon hoc trong truong truoc khi khai bao cho lop hoc                
                List<SchoolSubject> ListSchoolSubjectBySubject = ListSchoolSubject.Where(o => o.SubjectID == ClassSubject.SubjectID
                    && o.EducationLevelID == cp.EducationLevelID).ToList();
                if (ListSchoolSubjectBySubject == null || ListSchoolSubjectBySubject.Count == 0)
                {
                    throw new BusinessException("ClassSubject_Err_SchoolSubject");
                }

                // Kiem tra so tiet hoc cua moi hoc ky phai la so nguyen duong
                ClassSubject.SectionPerWeekFirstSemester = ClassSubject.SectionPerWeekFirstSemester.HasValue ? ClassSubject.SectionPerWeekFirstSemester.Value : (int)0;
                ClassSubject.SectionPerWeekSecondSemester = ClassSubject.SectionPerWeekSecondSemester.HasValue ? ClassSubject.SectionPerWeekSecondSemester.Value : (int)0;
                if (ClassSubject.SectionPerWeekFirstSemester < 0)
                {
                    throw new BusinessException("ClassSubject_Err_SectionPerWeekFirstSemester");
                }
                if (ClassSubject.SectionPerWeekSecondSemester < 0)
                {
                    throw new BusinessException("ClassSubject_Err_SectionPerWeekSecondSemester");
                }

            }

            // Du lieu hop le, thuc hien them moi vao CSDL
            foreach (ClassSubject ClassSubject in ListClassSubject)
            {
                this.Update(ClassSubject);
            }

            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        #endregion Cap nhat

        #region Tim kiem

        /// <summary>
        /// Tim kiem mon hoc trong lop.
        /// Khong goi truc tiep ham search ma hoi qua SearchBySchool de an partition
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassSubject> Search(IDictionary<string, object> SearchInfo)
        {
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int? IsCommenting = Utils.GetNullableInt(SearchInfo, "IsCommenting");

            bool? IsExemptible = Utils.GetNullableBool(SearchInfo, "IsExemptible");
            bool IsVNEN = Utils.GetBool(SearchInfo, "IsVNEN", false);

            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            bool? IsApprenticeShipSubject = Utils.GetNullableBool(SearchInfo, "IsApprenticeShipSubject");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int Grade = Utils.GetInt(SearchInfo, "Grade");
            int? Semester = Utils.GetNullableInt(SearchInfo, "Semester");
            List<int> LstClassID = Utils.GetIntList(SearchInfo, "ListClassID");
            List<int> lstSubjectID = Utils.GetIntList(SearchInfo, "lstSubjectID");
            List<int> lstAppliedType = Utils.GetIntList(SearchInfo, "lstAppliedType");


            IQueryable<ClassSubject> lsClassSubject = this.ClassSubjectRepository.All.Include("SubjectCat").Where(o => o.SubjectCat.IsActive == true);
            IQueryable<ClassProfile> iqClass = ClassProfileBusiness.AllNoTracking.Where(c => (!c.IsActive.HasValue || c.IsActive == true));

            if (AcademicYearID != 0)
            {
                iqClass = iqClass.Where(x => x.AcademicYearID == AcademicYearID);
            }

            if (ClassID > 0)
            {
                iqClass = iqClass.Where(cp => cp.ClassProfileID == ClassID);

            }

            if (Grade != 0)
            {
                iqClass = iqClass.Where(o => o.EducationLevel.Grade == Grade);
            }

            lsClassSubject = from cs in lsClassSubject
                             join cp in iqClass on cs.ClassID equals cp.ClassProfileID
                             select cs;

            if (IsCommenting.HasValue)
            {
                if (IsCommenting == 0)
                {
                    List<int> temp = new List<int>() { 0, 2 };

                    //Nếu truyền vào = 0 thì tìm kiếm theo IsCommenting = 0 hoặc =2
                    lsClassSubject = lsClassSubject.Where(x => temp.Contains(x.IsCommenting.Value));
                }
                else
                {
                    lsClassSubject = lsClassSubject.Where(x => x.IsCommenting == IsCommenting.Value);
                }
            }
            if (EducationLevelID != 0)
            {
                lsClassSubject = lsClassSubject.Where(x => x.ClassProfile.EducationLevelID == EducationLevelID);
            }

            // Anhvd - Bo sung tim kiem theo danh sach ID truyen vao
            if (LstClassID.Count > 0)
            {
                lsClassSubject = lsClassSubject.Where(x => LstClassID.Contains(x.ClassID));
            }
            // End 20140922
            if (lstSubjectID.Count > 0)
            {
                lsClassSubject = lsClassSubject.Where(x => lstSubjectID.Contains(x.SubjectID));
            }

            if (SchoolID != 0)
            {
                //Chiendd1: Bo sung where theo partition
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                lsClassSubject = lsClassSubject.Where(x => x.Last2digitNumberSchool == partitionId);
            }
            if (SubjectID != 0)
            {
                lsClassSubject = lsClassSubject.Where(x => x.SubjectID == SubjectID);
            }

            if (IsExemptible.HasValue)
            {
                lsClassSubject = lsClassSubject.Where(x => x.SubjectCat.IsExemptible == IsExemptible.Value);
            }
            if (IsApprenticeShipSubject.HasValue)
            {
                lsClassSubject = lsClassSubject.Where(x => x.SubjectCat.IsApprenticeshipSubject == IsApprenticeShipSubject.Value);
            }
            if (AppliedLevel != 0)
            {
                lsClassSubject = lsClassSubject.Where(x => x.SubjectCat.AppliedLevel == AppliedLevel);
            }

            if (Semester.HasValue)
            {
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    lsClassSubject = lsClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    lsClassSubject = lsClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0);
                }
                else lsClassSubject = lsClassSubject.Where(o => o.SectionPerWeekSecondSemester > 0);
            }
            if (lstAppliedType != null && lstAppliedType.Count > 0)
            {
                lsClassSubject = lsClassSubject.Where(o => o.AppliedType.HasValue && !lstAppliedType.Contains(o.AppliedType.Value));
            }
            if (IsVNEN)
            {
                lsClassSubject = lsClassSubject.Where(o => (o.IsSubjectVNEN == false || o.IsSubjectVNEN == null));
            }
            return lsClassSubject;
        }

        public IQueryable<ClassSubject> SearchByList(IDictionary<string, object> SearchInfo)
        {

            List<int> lstSchoolID = Utils.GetIntList(SearchInfo, "lstSchoolID");
            List<int> lstAcademicYearID = Utils.GetIntList(SearchInfo, "lstAcademicYearID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            List<int> LstClassID = Utils.GetIntList(SearchInfo, "lstClassID");
            
            IQueryable<ClassSubject> lsClassSubject = this.ClassSubjectRepository.All.Include("SubjectCat").Where(o => o.SubjectCat.IsActive == true);
            IQueryable<ClassProfile> iqClass = ClassProfileBusiness.AllNoTracking.Where(c => (!c.IsActive.HasValue || c.IsActive == true));

            if (lstAcademicYearID.Count > 0)
            {
                iqClass = iqClass.Where(x => lstAcademicYearID.Contains(x.AcademicYearID));
            }

            if (AppliedLevel != 0)
            {
                iqClass = iqClass.Where(o => o.EducationLevel.Grade == AppliedLevel);
            }

            lsClassSubject = from cs in lsClassSubject
                             join cp in iqClass on cs.ClassID equals cp.ClassProfileID
                             select cs;

            // Anhvd - Bo sung tim kiem theo danh sach ID truyen vao
            if (LstClassID.Count > 0)
            {
                lsClassSubject = lsClassSubject.Where(x => LstClassID.Contains(x.ClassID));
            }
           
            if (lstSchoolID.Count != 0)
            {
                List<int> lstPartition = new List<int>();
                //Chiendd1: Bo sung where theo partition
                foreach (var item in lstSchoolID) 
                {
                    lstPartition.Add(UtilsBusiness.GetPartionId(item));
                }
                lsClassSubject = lsClassSubject.Where(x => lstPartition.Contains(x.Last2digitNumberSchool));
            }
            
            if (AppliedLevel != 0)
            {
                lsClassSubject = lsClassSubject.Where(x => x.SubjectCat.AppliedLevel == AppliedLevel);
            }

            return lsClassSubject;
        }

        #endregion Tim kiem

        #region Xoa du lieu

        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="ListClassSubject"></param>
        public void Delete(List<ClassSubject> ListClassSubject, int AcademicYearID)
        {

            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
            int Year = objAcademicYear.Year;
            //string MarkRecordHistoryTableName = typeof(MarkRecordHistory).Name;
            //string JudgeRecordHistoryTableName = typeof(JudgeRecordHistory).Name;
            //string SummedUpRecordHistoryTableName = typeof(SummedUpRecordHistory).Name;

            IQueryable<ObjectBO> iqMark = null;
            IQueryable<ObjectBO> iqJudRecord = null;
            IQueryable<ObjectBO> idSummedUp = null;
            IQueryable<ObjectBO> iqMarkHis = null;
            IQueryable<ObjectBO> iqJudRecordHis = null;
            IQueryable<ObjectBO> idSummedUpHis = null;
            int count = 0;
            #region check validate
            if (isMovedHistory)
            {
                iqMarkHis = (from m in MarkRecordHistoryBusiness.All
                             join poc in PupilOfClassBusiness.All on new { m.PupilID, m.ClassID } equals new { poc.PupilID, poc.ClassID }
                             where m.SchoolID == objAcademicYear.SchoolID
                             && m.AcademicYearID == objAcademicYear.AcademicYearID
                             && m.Last2digitNumberSchool == (objAcademicYear.SchoolID % 100)
                             select new ObjectBO
                             {
                                 PupilID = m.PupilID,
                                 ClassID = m.ClassID,
                                 SubjectID = m.SubjectID,
                                 Status = poc.Status
                             });
                iqJudRecordHis = (from j in JudgeRecordHistoryBusiness.All
                                  join poc in PupilOfClassBusiness.All on new { j.PupilID, j.ClassID } equals new { poc.PupilID, poc.ClassID }
                                  where j.SchoolID == objAcademicYear.SchoolID
                                  && j.AcademicYearID == objAcademicYear.AcademicYearID
                                  && j.Last2digitNumberSchool == (objAcademicYear.SchoolID % 100)
                                  select new ObjectBO
                                  {
                                      PupilID = j.PupilID,
                                      ClassID = j.ClassID,
                                      SubjectID = j.SubjectID,
                                      Status = poc.Status
                                  });
                idSummedUpHis = (from s in SummedUpRecordHistoryBusiness.All
                                 join poc in PupilOfClassBusiness.All on new { s.PupilID, s.ClassID } equals new { poc.PupilID, poc.ClassID }
                                 where s.SchoolID == objAcademicYear.SchoolID
                                 && s.AcademicYearID == objAcademicYear.AcademicYearID
                                 && s.Last2digitNumberSchool == (objAcademicYear.SchoolID % 100)
                                 select new ObjectBO
                                 {
                                     PupilID = s.PupilID,
                                     ClassID = s.ClassID,
                                     SubjectID = s.SubjectID,
                                     Status = poc.Status
                                 });
            }
            else
            {
                iqMark = (from m in MarkRecordBusiness.All
                          join poc in PupilOfClassBusiness.All on new { m.PupilID, m.ClassID } equals new { poc.PupilID, poc.ClassID }
                          where m.SchoolID == objAcademicYear.SchoolID
                          && m.AcademicYearID == objAcademicYear.AcademicYearID
                          && m.Last2digitNumberSchool == (objAcademicYear.SchoolID % 100)
                          select new ObjectBO
                          {
                              PupilID = m.PupilID,
                              ClassID = m.ClassID,
                              SubjectID = m.SubjectID,
                              Status = poc.Status
                          });
                iqJudRecord = (from j in JudgeRecordBusiness.All
                               join poc in PupilOfClassBusiness.All on new { j.PupilID, j.ClassID } equals new { poc.PupilID, poc.ClassID }
                               where j.SchoolID == objAcademicYear.SchoolID
                               && j.AcademicYearID == objAcademicYear.AcademicYearID
                               && j.Last2digitNumberSchool == (objAcademicYear.SchoolID % 100)
                               select new ObjectBO
                               {
                                   PupilID = j.PupilID,
                                   ClassID = j.ClassID,
                                   SubjectID = j.SubjectID,
                                   Status = poc.Status
                               });
                idSummedUp = (from s in SummedUpRecordBusiness.All
                              join poc in PupilOfClassBusiness.All on new { s.PupilID, s.ClassID } equals new { poc.PupilID, poc.ClassID }
                              where s.SchoolID == objAcademicYear.SchoolID
                              && s.AcademicYearID == objAcademicYear.AcademicYearID
                              && s.Last2digitNumberSchool == (objAcademicYear.SchoolID % 100)
                              select new ObjectBO
                              {
                                  PupilID = s.PupilID,
                                  ClassID = s.ClassID,
                                  SubjectID = s.SubjectID,
                                  Status = poc.Status
                              });
            }



            foreach (ClassSubject subject in ListClassSubject)
            {
                // Kiem tra da ton tai trong CSDL
                CheckAvailable(subject.ClassSubjectID, "ClassSubject_Label_AllTitle", false);

                if (isMovedHistory) // Neu la cac nam truoc thi kiem tra trong cac bang diem History
                {
                    count = iqMarkHis.Where(p => p.ClassID == subject.ClassID && p.SubjectID == subject.SubjectID && p.Status == 1).Count();
                    if (count > 0)
                    {
                        throw new BusinessException("Common_Validate_Using", new List<object> { "ClassSubject_Label_AllTitle" });
                    }

                    count = iqJudRecordHis.Where(p => p.ClassID == subject.ClassID && p.SubjectID == subject.SubjectID && p.Status == 1).Count();
                    if (count > 0)
                    {
                        throw new BusinessException("Common_Validate_Using", new List<object> { "ClassSubject_Label_AllTitle" });
                    }

                    count = idSummedUpHis.Where(p => p.ClassID == subject.ClassID && p.SubjectID == subject.SubjectID && p.Status == 1).Count();
                    if (count > 0)
                    {
                        throw new BusinessException("Common_Validate_Using", new List<object> { "ClassSubject_Label_AllTitle" });
                    }
                }
                else // Neu la nam hien tai thi kiem tra trong cac bang diem binh thuong
                {
                    count = iqMark.Where(p => p.ClassID == subject.ClassID && p.SubjectID == subject.SubjectID && p.Status == 1).Count();
                    if (count > 0)
                    {
                        throw new BusinessException("Common_Validate_Using", new List<object> { "ClassSubject_Label_AllTitle" });
                    }

                    count = iqJudRecord.Where(p => p.ClassID == subject.ClassID && p.SubjectID == subject.SubjectID && p.Status == 1).Count();
                    if (count > 0)
                    {
                        throw new BusinessException("Common_Validate_Using", new List<object> { "ClassSubject_Label_AllTitle" });
                    }

                    count = idSummedUp.Where(p => p.ClassID == subject.ClassID && p.SubjectID == subject.SubjectID && p.Status == 1).Count();
                    if (count > 0)
                    {
                        throw new BusinessException("Common_Validate_Using", new List<object> { "ClassSubject_Label_AllTitle" });
                    }
                }
                //markrecord,jugderecord,sumuprecord

                // Kiem tra xem mon hoc co thuoc nam hoc nay hay khong
                if (subject.ClassProfile == null || subject.ClassProfile.AcademicYearID != AcademicYearID)
                {
                    throw new BusinessException("ClassSubject_Label_AcademicYearID_Err");
                }
            }
            #endregion
            // Khong co loi thi thuc hien xoa toan bo du lieu trong danh sach nay
            base.DeleteAll(ListClassSubject);
        }

        #endregion Xoa du lieu

        #region Tim kiem mon hoc trong lop hoc theo truong

        /// <summary>
        /// Tim kiem mon hoc cua lop theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassSubject> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        #endregion Tim kiem mon hoc trong lop hoc theo truong

        #region Tim kiem mon hoc trong lop hoc theo lop

        /// <summary>
        /// Tim kiem mon hoc cua lop theo lop
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassSubject> SearchByClass(int ClassID, IDictionary<string, object> SearchInfo = null)
        {
            if (ClassID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo.Add("ClassID", ClassID);
            //Chiendd1: 07/07/2015 Doi thanh tim kiem trong SearchBySchool de an partition
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            if (SchoolID == 0)
            {
                ClassProfile objClass = ClassProfileBusiness.Find(ClassID);
                SchoolID = objClass.SchoolID;
            }
            return this.SearchBySchool(SchoolID, SearchInfo);
        }

        #endregion Tim kiem mon hoc trong lop hoc theo lop

        /// <summary>
        /// Kiem tra xem thong tin ve mon ngoai ngu trong 1 lop co hop le hay khong
        /// Khong su dung
        /// </summary>
        /// <param name="ListClassSubject"></param>
        /// <returns></returns>
        private void ValidateForeignLanguageInClass(List<ClassSubject> ListClassSubject)
        {
            var lsSubjectID = ListClassSubject.Select(o => o.SubjectID).ToList();
            var lsSubjectCat = SubjectCatBusiness.All.Where(o => lsSubjectID.Contains(o.SubjectCatID)).ToList();

            // Khong duoc co 2 mon ngoai ngu 2
            int countFL2 = ListClassSubject.Where(x => lsSubjectCat.FirstOrDefault(o => o.SubjectCatID == x.SubjectID).IsForeignLanguage &&
                (x.IsSecondForeignLanguage.HasValue && x.IsSecondForeignLanguage.Value)).Count();
            if (countFL2 > 1)
            {
                throw new BusinessException("ClassSubject_Err_OnlyOneSecondLanguage");
            }

            // Tong so mon ngoai ngu khong duoc vuot qua 2
            //int countAllFL = lsSubjectCat.Where(x => x.IsForeignLanguage).Count();
            //if (countAllFL > 2)
            //{
            //    throw new BusinessException("ClassSubject_Err_MaxCountFL");
            //}
            //if (countAllFL == 2)
            //{
            //    if (countFL2 == 0)
            //    {
            //        throw new BusinessException("ClassSubject_Err_MinCountFL2");
            //    }
            //}

            // Phai chon ngoai ngu 1 moi duoc chon ngoai ngu 2
            if (countFL2 > 0)
            {
                int countFL1 = ListClassSubject.Where(x => lsSubjectCat.FirstOrDefault(o => o.SubjectCatID == x.SubjectID).IsForeignLanguage &&
                (!x.IsSecondForeignLanguage.HasValue || !x.IsSecondForeignLanguage.Value)).Count();
                if (countFL1 == 0)
                {
                    throw new BusinessException("ClassProfile_Err_FL1NextFL2");
                }
            }
        }

        /// <summary>
        /// Tru hai list voi nhau
        /// Khong su dung.
        /// </summary>
        /// <param name="ListData"></param>
        /// <param name="ListSub"></param>
        /// <returns></returns>
        private List<ClassSubject> SubList(List<ClassSubject> ListData, List<ClassSubject> ListSub)
        {
            if (ListData == null || ListSub == null)
            {
                return ListData;
            }
            List<ClassSubject> ListResult = new List<ClassSubject>();
            foreach (ClassSubject ClassSubject in ListData)
            {
                if (ListSub.Any(x => x.ClassSubjectID == ClassSubject.ClassSubjectID))
                {
                    continue;
                }
                ListResult.Add(ClassSubject);
            }
            return ListResult;
        }

        #region Tìm kiếm các môn tính điểm trong trường

        /// <summary>
        /// Tìm kiếm các môn tính điểm trong trường. 
        /// Khong su dung
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        public List<SubjectCat> SearchMarkedSubjectInSchool(int SchoolID, int AcademicYearID, int EducationLevelID)
        {
            if ((SchoolID == 0) || (AcademicYearID == 0))
            {
                return null;
            }
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            List<int> lsClassID = ClassProfileRepository.All.Where(o => (o.EducationLevelID == EducationLevelID) && o.IsActive == true).Select(o => o.ClassProfileID).ToList();
            List<long> lsSubjectCatID = new List<long>();
            foreach (int classID in lsClassID)
            {
                List<long> ListSubjectCatID = ClassSubjectRepository.All.Where(o => o.ClassID == classID && o.Last2digitNumberSchool == partitionId).Select(o => o.ClassSubjectID).ToList();
                lsSubjectCatID = lsSubjectCatID.Union(ListSubjectCatID).ToList();
            }

            IQueryable<SubjectCat> lsSubjectCat = SubjectCatRepository.All.Where(o => lsSubjectCatID.Contains(o.SubjectCatID));
            return lsSubjectCat.ToList();
        }

        #endregion Tìm kiếm các môn tính điểm trong trường

        #region Lấy ra danh sách các lớp tham gia học môn học xác định

        /// <summary>
        /// Lấy ra danh sách các lớp tham gia học môn học xác định
        /// Khong su dung
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public List<ClassSubject> GetListClassBySubject(IDictionary<string, object> SearchInfo)
        {
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");

            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int SemesterID = Utils.GetInt(SearchInfo, "SemesterID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");

            if ((SubjectID == 0) || (SchoolID == 0) || (SemesterID == 0) || (AcademicYearID == 0))
            {
                return new List<ClassSubject>();
            }

            return Search(SearchInfo).ToList();
        }

        #endregion Lấy ra danh sách các lớp tham gia học môn học xác định

        #region Lấy ra danh sách môn học theo khối

        /// <summary>
        /// Lấy ra danh sách môn học theo khối
        /// Khong su dung
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassSubject> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null)
        {
            if (AcademicYearID == 0)
            {
                return null;
            }
            SearchInfo["AcademicYearID"] = AcademicYearID;
            return Search(SearchInfo);
        }

        #endregion Lấy ra danh sách môn học theo khối

        #region Lấy ra danh sách môn học theo khối

        /// <summary>
        /// Lấy ra danh sách môn học theo khối. Khong su dung
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<SubjectCat> SearchSubjectByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null)
        {
            if (AcademicYearID == 0)
            {
                return null;
            }
            SearchInfo["AcademicYearID"] = AcademicYearID;
            IQueryable<ClassSubject> lsClassSubject = Search(SearchInfo);
            List<long> lsClassID = lsClassSubject.Select(o => o.ClassSubjectID).ToList();
            List<long> lsSubjectCatID = new List<long>();
            foreach (int classID in lsClassID)
            {
                List<long> ListSubjectCatID = ClassSubjectRepository.All.Where(o => (o.ClassID == classID)).Select(o => o.ClassSubjectID).ToList();
                lsSubjectCatID = lsSubjectCatID.Union(ListSubjectCatID).ToList();
            }

            IQueryable<SubjectCat> lsSubjectCat = SubjectCatRepository.All.Where(o => lsSubjectCatID.Contains(o.SubjectCatID))
                .Where(o => o.IsActive == true);
            return lsSubjectCat;
        }

        #endregion Lấy ra danh sách môn học theo khối

        #region Lay danh sach mon hoc cua mot user
        public List<ClassSubject> GetListSubjectBySubjectTeacher(int UserAccountID, int AcademicYearID,
            int SchoolID, int Semester, int ClassID, bool isTeacherCanView)
        {
            bool isSchoolAdmin = UserAccountBusiness.IsSchoolAdmin(UserAccountID);
            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(UserAccountID, ClassID);
            //BGH            
            bool isBGH = UtilsBusiness.IsBGH(UserAccountID);
            // Neu la admin truong thi tra lai danh sach toan bo mon hoc cua truong
            if (isSchoolAdmin || isTeacherCanView || isHeadTeacher || isBGH)
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["ClassID"] = ClassID;
                dicClassSubject["Semester"] = Semester;
                dicClassSubject["AcademicYearID"] = AcademicYearID;
                return this.SearchBySchool(SchoolID, dicClassSubject)
                    .ToList();
            }
            UserAccount ua = UserAccountBusiness.Find(UserAccountID);
            if (ua == null || !ua.EmployeeID.HasValue)
            {
                return new List<ClassSubject>();
            }
            int TeacherID = ua.EmployeeID.Value;
            // Kiem tra giao vien bo mon
            IQueryable<ClassSupervisorAssignment> lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.AcademicYearID == AcademicYearID &&
                                                            o.SchoolID == SchoolID &&
                                                            (o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER
                                                            || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER));
            if (lstCsa.Count() > 0)
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["ClassID"] = ClassID;
                dicClassSubject["Semester"] = Semester;
                dicClassSubject["AcademicYearID"] = AcademicYearID;
                return this.SearchBySchool(SchoolID, dicClassSubject).ToList();
            }
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            // Kiem tra giao vien giang day mon
            IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All.Where(
                 o => (
                     o.TeacherID == TeacherID &&
                     o.AcademicYearID == AcademicYearID &&
                     o.SchoolID == SchoolID &&
                     o.ClassID == ClassID &&
                     o.Semester == Semester &&
                     o.IsActive &&
                     o.SubjectCat.IsActive
                     && o.Last2digitNumberSchool == partitionId
                 )
             );

            /*List<ClassSubject> listClassSubject = lsTeachingAssignment
                .Select(o => o.SubjectCat)
                .SelectMany(u => u.ClassSubjects.Where(v => v.ClassID == ClassID)).ToList();*/
            IQueryable<ClassSubject> lstAllClassSubject = from ta in lsTeachingAssignment
                                                          join cs in ClassSubjectBusiness.All.AsNoTracking() on ta.SubjectID equals cs.SubjectID
                                                          where cs.Last2digitNumberSchool == partitionId && cs.ClassID == ClassID
                                                          select cs;
            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekFirstSemester > 0);
            }
            else
            {
                lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekSecondSemester > 0);
            }
            return lstAllClassSubject.ToList();
        }


        /// <summary>
        /// Lay danh sach mon hoc giao vien duoc phan cong giang day
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public List<SubjectCatBO> GetListSubjectbyTeaching(int UserAccountID, int AcademicYearID, int SchoolID, int Semester, int ClassID)
        {
            bool isSchoolAdmin = UserAccountBusiness.IsSchoolAdmin(UserAccountID);

            // Neu la admin truong thi tra lai danh sach toan bo mon hoc cua truong
            IQueryable<SubjectCat> iqSubjectCat = SubjectCatBusiness.AllNoTracking.Where(s => s.IsActive);

            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["ClassID"] = ClassID;
            dicClassSubject["Semester"] = Semester;
            dicClassSubject["AcademicYearID"] = AcademicYearID;
            IQueryable<ClassSubject> iqClassSubject = this.SearchBySchool(SchoolID, dicClassSubject);

            IQueryable<SubjectCatBO> iqClassSubjectAdmin = (from cs in iqClassSubject
                                                            join sub in iqSubjectCat on cs.SubjectID equals sub.SubjectCatID
                                                            select new SubjectCatBO
                                                            {
                                                                SubjectCatID = sub.SubjectCatID,
                                                                DisplayName = sub.DisplayName,
                                                                IsCommenting = cs.IsCommenting,
                                                                OrderInSubject = sub.OrderInSubject,
                                                                SubjectIdInCrease = cs.SubjectIDIncrease
                                                            });

            if (isSchoolAdmin)
            {

                return iqClassSubjectAdmin.OrderBy(s => s.OrderInSubject).ThenBy(s => s.DisplayName).ToList();


            }
            UserAccount ua = UserAccountBusiness.Find(UserAccountID);
            if (ua == null || !ua.EmployeeID.HasValue)
            {
                return new List<SubjectCatBO>();
            }
            int TeacherID = ua.EmployeeID.Value;
            // Lay danh sach giao vien co quyen giao vien bo mon
            IQueryable<ClassSupervisorAssignment> lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.AcademicYearID == AcademicYearID &&
                                                            o.SchoolID == SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);
            if (lstCsa.Count() > 0)
            {

                return iqClassSubjectAdmin.OrderBy(s => s.OrderInSubject).ThenBy(s => s.DisplayName).ToList();
            }
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            // Kiem tra giao vien giang day mon
            IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All.Where(
                 o => (
                     o.TeacherID == TeacherID &&
                     o.AcademicYearID == AcademicYearID &&
                     o.SchoolID == SchoolID &&
                     o.ClassID == ClassID &&
                     o.Semester == Semester &&
                     o.IsActive &&
                     o.SubjectCat.IsActive
                     && o.Last2digitNumberSchool == partitionId
                 )
             );


            IQueryable<ClassSubject> lstAllClassSubject = from ta in lsTeachingAssignment
                                                          join cs in iqClassSubject on ta.SubjectID equals cs.SubjectID
                                                          select cs;
            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekFirstSemester > 0);
            }
            else
            {
                lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekSecondSemester > 0);
            }



            List<SubjectCatBO> lstClassSubject = (from cs in lstAllClassSubject
                                                  join sub in iqSubjectCat on cs.SubjectID equals sub.SubjectCatID
                                                  orderby sub.OrderInSubject, sub.DisplayName
                                                  select new SubjectCatBO
                                                  {
                                                      SubjectCatID = sub.SubjectCatID,
                                                      DisplayName = sub.DisplayName,
                                                      IsCommenting = cs.IsCommenting,
                                                      OrderInSubject = sub.OrderInSubject,
                                                      SubjectIdInCrease = cs.SubjectIDIncrease
                                                  }).Distinct().ToList();
            return lstClassSubject;
        }
        #endregion



        public void AppliedForEducationLevelOrSchool(int SchoolID, int AcademicYearID, int AppliedLevel, int EducationLevelID, List<ClassSubject> ListData)
        {
            //Validate dữ liệu chung
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            //Kiểm tra trường học có tồn tại không
            if (school == null || !school.IsActive)
            {
                throw new Exception("Common_Validate_NotCompatible");
            }
            //Kiểm tra trường học có học cấp tương ứng không
            if ((school.EducationGrade % Int32.Parse(Math.Pow(2, AppliedLevel - 1).ToString())) / 2 == 0)
            {
                throw new Exception("Common_Validate_NotCompatible");
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            //Kiểm tra năm học có tồn tại và đúng của trường tương ứng không
            if (academicYear == null || academicYear.SchoolID != SchoolID)
            {
                throw new Exception("Common_Validate_NotCompatible");
            }

            if (EducationLevelID != 0)
            {
                //Kiểm tra khối học có tồn tại và đúng cấp tương ứng không
                EducationLevel educationLevel = EducationLevelBusiness.Find(EducationLevelID);
                if (educationLevel == null || educationLevel.Grade != AppliedLevel)
                {
                    throw new Exception("Common_Validate_NotCompatible");
                }
            }

            //Kiểm tra dữ liệu trong ListData
            foreach (ClassSubject data in ListData)
            {
                // Kiem tra số tiết học / kỳ
                data.SectionPerWeekFirstSemester = data.SectionPerWeekFirstSemester.GetValueOrDefault();
                data.SectionPerWeekSecondSemester = data.SectionPerWeekSecondSemester.GetValueOrDefault();
                if (data.SectionPerWeekFirstSemester < 0)
                {
                    throw new BusinessException("ClassSubject_Err_SectionPerWeekFirstSemester");
                }
                if (data.SectionPerWeekSecondSemester < 0)
                {
                    throw new BusinessException("ClassSubject_Err_SectionPerWeekSecondSemester");
                }
                if (data.SectionPerWeekFirstSemester == 0 && data.SectionPerWeekSecondSemester == 0)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //Kiểm tra hệ số của môn
                data.IsSpecializedSubject = data.IsSpecializedSubject.GetValueOrDefault();
                if (!data.IsSpecializedSubject.Value)
                {
                    data.FirstSemesterCoefficient = data.FirstSemesterCoefficient.GetValueOrDefault();
                    data.SecondSemesterCoefficient = data.SecondSemesterCoefficient.GetValueOrDefault();
                    if (data.FirstSemesterCoefficient < SystemParamsInFile.SUBJECT_COEFFICIENT_MIN
                        || data.FirstSemesterCoefficient > SystemParamsInFile.SUBJECT_COEFFICIENT_MAX)
                    {
                        throw new BusinessException("ClassSubject_Err_FirstSemesterCoefficient");
                    }
                    if (data.SecondSemesterCoefficient < SystemParamsInFile.SUBJECT_COEFFICIENT_MIN
                        || data.SecondSemesterCoefficient > SystemParamsInFile.SUBJECT_COEFFICIENT_MAX)
                    {
                        throw new BusinessException("ClassSubject_Err_SecondSemesterCoefficient");
                    }
                }
                else
                {
                    data.FirstSemesterCoefficient = SystemParamsInFile.SUBJECT_FOR_SPECIAL;
                    data.SecondSemesterCoefficient = SystemParamsInFile.SUBJECT_FOR_SPECIAL;
                }
            }



        }

        public List<ClassSubject> GetListSubjectBySubjectTeacher(int UserAccountID, int AcademicYearID, int SchoolID, int Semester, int ClassID, int TyOfTeacher)
        {
            bool isSchoolAdmin = UserAccountBusiness.IsSchoolAdmin(UserAccountID);
            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(UserAccountID, ClassID);
            // Neu la admin truong thi tra lai danh sach toan bo mon hoc cua truong
            //TypeOfTeacher=2: GVCN
            if (isSchoolAdmin || (isHeadTeacher && TyOfTeacher == 2))
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["ClassID"] = ClassID;
                dicClassSubject["Semester"] = Semester;
                dicClassSubject["AcademicYearID"] = AcademicYearID;
                return this.SearchBySchool(SchoolID, dicClassSubject)
                    .Where(c => c.SubjectCat.IsActive == true)
                    .OrderBy(c => c.SubjectCat.OrderInSubject)
                    .ThenBy(c => c.SubjectCat.DisplayName)
                    .ToList();
            }
            UserAccount ua = UserAccountBusiness.Find(UserAccountID);
            if (ua == null || !ua.EmployeeID.HasValue)
            {
                return new List<ClassSubject>();
            }
            int TeacherID = ua.EmployeeID.Value;
            // Kiem tra giao vien bo mon
            IQueryable<ClassSupervisorAssignment> lstCsa = null;

            if (TyOfTeacher == 1)
            {
                lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.AcademicYearID == AcademicYearID &&
                                                            o.SchoolID == SchoolID &&
                                                            (o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER));
            }
            else if (TyOfTeacher == 2)
            {
                lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.AcademicYearID == AcademicYearID &&
                                                            o.SchoolID == SchoolID &&
                                                            (o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER));
            }
            if (lstCsa != null && lstCsa.Count() > 0)
            {
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["ClassID"] = ClassID;
                dicClassSubject["Semester"] = Semester;
                dicClassSubject["AcademicYearID"] = AcademicYearID;
                return this.SearchBySchool(SchoolID, dicClassSubject)
                     .Where(c => c.SubjectCat.IsActive == true)
                    .OrderBy(c => c.SubjectCat.OrderInSubject)
                    .ThenBy(c => c.SubjectCat.DisplayName)
                    .ToList();
            }
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            // Kiem tra giao vien giang day mon
            IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All.Where(
                 o => (
                     o.TeacherID == TeacherID &&
                     o.AcademicYearID == AcademicYearID &&
                     o.SchoolID == SchoolID &&
                     o.ClassID == ClassID &&
                     o.Semester == Semester &&
                     o.IsActive &&
                     o.SubjectCat.IsActive == true
                     && o.Last2digitNumberSchool == partitionId
                 )
             );
            List<ClassSubject> listClassSubject = lsTeachingAssignment
                .Select(o => o.SubjectCat)
                .SelectMany(u => u.ClassSubjects.Where(v => v.ClassID == ClassID && v.Last2digitNumberSchool == partitionId))
                    .OrderBy(c => c.SubjectCat.OrderInSubject)
                    .ThenBy(c => c.SubjectCat.DisplayName)
                    .ToList();
            return listClassSubject;
        }

        public bool InsertClassSubjectbySchoolSubject(int schoolId, List<SchoolSubject> lstSchoolSubject, Dictionary<string, object> dic)
        {
            try
            {
                if (lstSchoolSubject == null || lstSchoolSubject.Count == 0)
                {
                    return false;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                //Danh sach mon hoc
                List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(schoolId, dic).ToList();
                //Danh sach lop
                List<int> lstClass = ClassProfileBusiness.SearchBySchool(schoolId, dic).Select(o => o.ClassProfileID).ToList();
                if (lstClass != null && lstClass.Count > 0)
                {
                    lstClass = lstClass.Distinct().ToList();
                }
                List<int> lstsubClass = new List<int>();
                ClassSubject cs;
                foreach (int clas in lstClass)
                {
                    lstsubClass = lstClassSubject.Where(o => o.ClassID == clas).Select(o => o.SubjectID).ToList();
                    foreach (SchoolSubject ss in lstSchoolSubject)
                    {
                        if (lstsubClass.Contains(ss.SubjectID))
                        {
                            continue;
                        }
                        cs = new ClassSubject();
                        cs.SubjectID = ss.SubjectID;
                        cs.ClassID = clas;
                        cs.FirstSemesterCoefficient = ss.FirstSemesterCoefficient;
                        cs.IsCommenting = ss.IsCommenting;
                        cs.AppliedType = ss.AppliedType;
                        cs.SecondSemesterCoefficient = ss.SecondSemesterCoefficient;
                        cs.SectionPerWeekFirstSemester = ss.SectionPerWeekFirstSemester;
                        cs.SectionPerWeekSecondSemester = ss.SectionPerWeekSecondSemester;
                        cs.StartDate = ss.StartDate;
                        cs.Last2digitNumberSchool = partitionId;
                        ClassSubjectBusiness.Insert(cs);
                    }
                }
                ClassSubjectBusiness.Save();
                return true;
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("schoolId={0}", schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertClassSubjectbySchoolSubject", paramList, ex);
                return false;
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        #region Khai bao mon tang cuong
        /// <summary>
        /// anhnph1: 29/07/2015. Cap nhat mon hoc tang cuong cho mon o cot Subject_Is_Increase
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="dicSubjectSaveIncrease"></param>
        public void UpdateSubjectIncreaseByClass(int AcademicYearID, int SchoolID, int semesterId, IDictionary<string, object> dicSearch, IDictionary<int, int> dicSubjectSaveIncrease)
        {
            //lấy ra các môn tính điểm
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSearch).Where(p => p.IsCommenting == 0).ToList();

            ClassSubject objClassSubject = null;
            ClassSubject objChek_Sub = null; // kiem tra mon hoc da dc tang cuong cho mon nao chua
            List<ThreadMark> lstThreadMark = new List<ThreadMark>();
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            foreach (var objdic in dicSubjectSaveIncrease)
            {
                if (objdic.Value != 0)
                {
                    objChek_Sub = lstClassSubject.Where(p => p.SubjectIDIncrease == objdic.Key).FirstOrDefault();
                    objClassSubject = lstClassSubject.Where(p => p.SubjectID == objdic.Value).FirstOrDefault();
                    if (objChek_Sub != null) //thi cap nhap lai tang cuong cho mon 
                    {
                        objChek_Sub.SubjectIDIncrease = 0;
                    }
                    if (objClassSubject != null)
                    {
                        objClassSubject.SubjectIDIncrease = objdic.Key;
                    }

                    if (objClassSubject.SubjectIDIncrease.HasValue && objClassSubject.SubjectIDIncrease > 0)
                    {
                        AddThreadMark(SchoolID, partitionId, AcademicYearID, objClassSubject.ClassID, objClassSubject.SubjectID, (short)semesterId, lstThreadMark);
                    }
                    this.ClassSubjectBusiness.Update(objClassSubject);
                }
                else
                {
                    objClassSubject = lstClassSubject.Where(p => p.SubjectIDIncrease == objdic.Key).FirstOrDefault();
                    if (objClassSubject != null)
                    {
                        objClassSubject.SubjectIDIncrease = objdic.Value;
                        this.ClassSubjectBusiness.Update(objClassSubject);
                        AddThreadMark(SchoolID, partitionId, AcademicYearID, objClassSubject.ClassID, objClassSubject.SubjectID, (short)semesterId, lstThreadMark);
                    }
                }
            }

            this.Save();

            //Luu thong tin vao ThreadMark de tinh lai diem TBM hoc
            if (lstThreadMark != null && lstThreadMark.Count > 0)
            {
                ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);
            }
        }

        /// <summary>
        /// anhnph1: 29/07/2015. Cap nhat mon hoc tang cuong ap dung toan khoi 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="lsClassOfEducationLevelID"></param>
        /// <param name="dicSearchByEducationLevel"></param>
        /// <param name="dicSubjectSaveIncrease"></param>
        public void UpdateSubjectIncreaseByEducationLevel(int AcademicYearID, int SchoolID, int semesterId, List<int> lsClassOfEducationLevelID, int classCurrent,
                IDictionary<string, object> dicSearchByEducationLevel, IDictionary<int, int> dicSubjectSaveIncrease)
        {
            //lấy ra các môn tính điểm cua khối
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSearchByEducationLevel).Where(p => p.IsCommenting == 0).ToList();
            List<ClassSubject> lstSubjectByClass = null;
            ClassSubject objClassSubject = null;
            ClassSubject objChek_Sub = null; // kiem tra mon hoc da dc tang cuong cho mon nao chua
            List<int> lsSubject = null;
            List<ThreadMark> lstThreadMark = new List<ThreadMark>();
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            int subjectId = 0;
            //duyệt qua danh sách các lớp của khối           
            for (int i = 0; i < lsClassOfEducationLevelID.Count; i++)
            {
                lstSubjectByClass = lstClassSubject.Where(p => p.ClassID == lsClassOfEducationLevelID[i]).ToList(); //danh sach mon cua mot lop
                if (lsClassOfEducationLevelID[i] != classCurrent)
                {
                    lstSubjectByClass.Select(c => { c.SubjectIDIncrease = 0; return c; }).ToList();
                }
                //ghi chu: (Không thực hiện lưu với các lớp không học một trong 2 môn: tăng cường/được tăng cường).
                lsSubject = lstSubjectByClass.Select(p => p.SubjectID).ToList();
                if (lstSubjectByClass.Count != 0 && lstSubjectByClass != null)
                {
                    foreach (var objdic in dicSubjectSaveIncrease)
                    {
                        if ((objdic.Key != 0 && objdic.Value != 0) && (!lsSubject.Contains(objdic.Key) || !lsSubject.Contains(objdic.Value)))
                        {
                            continue;
                        }
                        if (objdic.Value != 0)
                        {
                            objClassSubject = lstSubjectByClass.Where(p => p.SubjectID == objdic.Value).FirstOrDefault();
                            objChek_Sub = lstSubjectByClass.Where(p => p.SubjectIDIncrease == objdic.Key).FirstOrDefault();
                            if (objChek_Sub != null) //thi cap nhap lai tang cuong cho mon 
                            {
                                objChek_Sub.SubjectIDIncrease = 0;
                                this.ClassSubjectBusiness.Update(objClassSubject);
                                subjectId = (objClassSubject.SubjectIDIncrease != null && objClassSubject.SubjectIDIncrease > 0) ? objClassSubject.SubjectIDIncrease.Value : objClassSubject.SubjectID;
                                AddThreadMark(SchoolID, partitionId, AcademicYearID, objClassSubject.ClassID, subjectId, (short)semesterId, lstThreadMark);
                            }
                            if (objClassSubject != null)
                            {
                                objClassSubject.SubjectIDIncrease = objdic.Key;
                                this.ClassSubjectBusiness.Update(objClassSubject);
                                subjectId = (objClassSubject.SubjectIDIncrease != null && objClassSubject.SubjectIDIncrease > 0) ? objClassSubject.SubjectIDIncrease.Value : objClassSubject.SubjectID;
                                AddThreadMark(SchoolID, partitionId, AcademicYearID, objClassSubject.ClassID, subjectId, (short)semesterId, lstThreadMark);
                            }
                        }
                        else
                        {
                            objClassSubject = lstSubjectByClass.Where(p => p.SubjectIDIncrease == objdic.Key).FirstOrDefault();
                            if (objClassSubject != null)
                            {
                                objClassSubject.SubjectIDIncrease = objdic.Value;
                                this.ClassSubjectBusiness.Update(objClassSubject);
                                subjectId = (objClassSubject.SubjectIDIncrease != null && objClassSubject.SubjectIDIncrease > 0) ? objClassSubject.SubjectIDIncrease.Value : objClassSubject.SubjectID;
                                AddThreadMark(SchoolID, partitionId, AcademicYearID, objClassSubject.ClassID, subjectId, (short)semesterId, lstThreadMark);
                            }
                        }
                    }
                }
            }
            //Luu thong tin vao ThreadMark de tinh lai diem TBM hoc
            if (lstThreadMark != null && lstThreadMark.Count > 0)
            {
                ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);
            }
            this.Save();
        }

        public void UpdateSubjectIncreaseBySchool(int AcademicYearID, int SchoolID, int semesterId, List<int> lstEducationLevel, int classCurrent, IDictionary<int, int> dicSubjectSaveIncrease, IDictionary<string, object> dicSearchByEducationLevel)
        {
            List<int> lstClassOfEducationLevelID = null;
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                lstClassOfEducationLevelID = GetClassFromEducationLevel(SchoolID, AcademicYearID, lstEducationLevel[i]).Select(p => p.ClassProfileID).ToList();
                this.UpdateSubjectIncreaseByEducationLevel(AcademicYearID, SchoolID, semesterId, lstClassOfEducationLevelID, classCurrent, dicSearchByEducationLevel, dicSubjectSaveIncrease);
            }
        }

        private List<ClassProfile> GetClassFromEducationLevel(int SchoolID, int AcademicYearID, int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",AcademicYearID}
                };

            List<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(SchoolID, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            return lsCP;
        }
        #endregion

        #region Lay danh sach lop theo mon hoc, theo khoi
        public List<ClassProfileTempBO> GetListClassBySubjectId(int academicYearID, int schoolId, int subjectID, int educationLevelId, int teacherId, bool admin, int semester,
            int appliedLevel, bool isAdminSchoolRole, bool isViewAll, bool isEmployeeManager, int userAccountID, int classID, bool? isVNEN = false)
        {
            List<ClassProfileTempBO> listClassProfile = new List<ClassProfileTempBO>();

            bool isBGH = UtilsBusiness.IsBGH(userAccountID);
            bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission_BM_HELP(userAccountID);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            if (admin || isBGH)
            {
                listClassProfile = (from cb in ClassSubjectBusiness.All
                                    join cp in ClassProfileBusiness.All on cb.ClassID equals cp.ClassProfileID
                                    where cp.AcademicYearID == academicYearID
                                    && cb.Last2digitNumberSchool == partitionId
                                    && cb.SubjectID == subjectID
                                    && cp.EducationLevelID == educationLevelId
                                    && (
                                          (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && cb.SectionPerWeekFirstSemester > 0)
                                       || (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && cb.SectionPerWeekSecondSemester > 0)
                                       )
                                    && (isVNEN.Value == true && (!cb.IsSubjectVNEN.HasValue || (cb.IsSubjectVNEN.HasValue && cb.IsSubjectVNEN.Value == false)))
                                    && cp.IsActive.Value
                                    group cb by new { cb.ClassID, cp.DisplayName, cp.EducationLevelID, cp.OrderNumber } into g
                                    select new ClassProfileTempBO
                                    {
                                        ClassProfileID = g.Key.ClassID,
                                        DisplayName = g.Key.DisplayName,
                                        EducationLevelID = g.Key.EducationLevelID,
                                        OrderNumber = g.Key.OrderNumber
                                    }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.DisplayName).ToList();
            }
            else
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearID;
                dic["AppliedLevel"] = appliedLevel;
                IQueryable<ClassProfile> iClass = ClassProfileBusiness.SearchBySchool(schoolId, dic);
                IQueryable<ClassSubject> iClassSubject = GetListSubjectBySubjectTeacher(userAccountID, academicYearID, schoolId, semester, isGVBM);
                listClassProfile = (from cp in iClass
                                    join cs in iClassSubject on cp.ClassProfileID equals cs.ClassID
                                    where cs.SubjectID == subjectID
                                    && (isVNEN.Value == true && (!cs.IsSubjectVNEN.HasValue || (cs.IsSubjectVNEN.HasValue && cs.IsSubjectVNEN.Value == false)))
                                    select new ClassProfileTempBO
                                    {
                                        ClassProfileID = cp.ClassProfileID,
                                        DisplayName = cp.DisplayName,
                                        EducationLevelID = cp.EducationLevelID,
                                        OrderNumber = cp.OrderNumber
                                    }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.DisplayName).ToList();

            }
            return listClassProfile;
        }

        private IQueryable<ClassSubject> GetListSubjectBySubjectTeacher(int UserAccountID, int AcademicYearID, int SchoolID, int Semester, bool isGVBM)
        {
            //Kiem tra quyen giao vu
            UserAccount ua = UserAccountBusiness.Find(UserAccountID);
            if (ua == null || !ua.EmployeeID.HasValue)
            {
                return null;
            }
            int TeacherID = ua.EmployeeID.Value;
            // Kiem tra giao vu
            IQueryable<ClassSupervisorAssignment> lstCsa = ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.AcademicYearID == AcademicYearID &&
                                                            o.SchoolID == SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);
            int countClassSupervisorAssignment = lstCsa.Count();
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["Semester"] = Semester;
            dicClassSubject["AcademicYearID"] = AcademicYearID;
            if (countClassSupervisorAssignment > 0 && !isGVBM)
            {
                IQueryable<ClassSubject> iClassSubject = (from cbs in this.SearchBySchool(SchoolID, dicClassSubject)
                                                          join csa in lstCsa on cbs.ClassID equals csa.ClassID
                                                          select cbs);
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    iClassSubject = iClassSubject.Where(cs => cs.SectionPerWeekFirstSemester > 0);
                }
                else
                {
                    iClassSubject = iClassSubject.Where(cs => cs.SectionPerWeekSecondSemester > 0);
                }
                return iClassSubject;
            }
            else if (countClassSupervisorAssignment > 0 && isGVBM)
            {
                IQueryable<ClassSubject> iClassSubject = (from cbs in this.SearchBySchool(SchoolID, dicClassSubject)
                                                          join csa in lstCsa on cbs.ClassID equals csa.ClassID
                                                          select cbs);
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    iClassSubject = iClassSubject.Where(cs => cs.SectionPerWeekFirstSemester > 0);
                }
                else
                {
                    iClassSubject = iClassSubject.Where(cs => cs.SectionPerWeekSecondSemester > 0);
                }

                //Giao vien bo mon
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                // Kiem tra giao vien giang day mon
                IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All.Where(
                     o => (
                         o.TeacherID == TeacherID &&
                         o.AcademicYearID == AcademicYearID &&
                         o.SchoolID == SchoolID &&
                         o.Semester == Semester &&
                         o.IsActive &&
                         o.SubjectCat.IsActive
                         && o.Last2digitNumberSchool == partitionId
                     )
                 );
                IQueryable<ClassSubject> lstAllClassSubject = from ta in lsTeachingAssignment
                                                              join cs in ClassSubjectBusiness.All.AsNoTracking() on ta.SubjectID equals cs.SubjectID
                                                              where cs.Last2digitNumberSchool == partitionId && ta.ClassID == cs.ClassID
                                                              select cs;
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekFirstSemester > 0);
                }
                else
                {
                    lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekSecondSemester > 0);
                }
                lstAllClassSubject = lstAllClassSubject.Union(iClassSubject);
                return lstAllClassSubject;
            }
            else
            {
                //Giao vien bo mon
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                // Kiem tra giao vien giang day mon
                IQueryable<TeachingAssignment> lsTeachingAssignment = TeachingAssignmentBusiness.All.Where(
                     o => (
                         o.TeacherID == TeacherID &&
                         o.AcademicYearID == AcademicYearID &&
                         o.SchoolID == SchoolID &&
                         o.Semester == Semester &&
                         o.IsActive &&
                         o.SubjectCat.IsActive
                         && o.Last2digitNumberSchool == partitionId
                     )
                 );
                IQueryable<ClassSubject> lstAllClassSubject = from ta in lsTeachingAssignment
                                                              join cs in ClassSubjectBusiness.All.AsNoTracking() on ta.SubjectID equals cs.SubjectID
                                                              where cs.Last2digitNumberSchool == partitionId && ta.ClassID == cs.ClassID
                                                              select cs;
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekFirstSemester > 0);
                }
                else
                {
                    lstAllClassSubject = lstAllClassSubject.Where(cs => cs.SectionPerWeekSecondSemester > 0);
                }
                return lstAllClassSubject;
            }
        }
        #endregion

        #region Lay danh sach lop theo linh vuc, theo khoi
        public List<ClassProfileTempBO> GetListClassByDeclareEvationGroupId(
            int academicYearID, int schoolId, int declareEvationGroupId, int educationLevelId, bool admin,
            int appliedLevel, int userAccountID, int classID)
        {
            List<ClassProfileTempBO> listClassProfile = new List<ClassProfileTempBO>();

            bool isBGH = UtilsBusiness.IsBGH(userAccountID);
            bool isGVBM = UtilsBusiness.HasSubjectTeacherPermission_BM_HELP(userAccountID);

            if (admin || isBGH)
            {
                List<DeclareEvaluationGroup> lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.All.Where(x => x.SchoolID == schoolId
                                                                                                                  && x.EducationLevelID == educationLevelId
                                                                                                                  && x.AcademicYearID == academicYearID
                                                                                                                  && x.DeclareEvaluationGroupID == declareEvationGroupId)
                                                                                                                  .ToList();
                List<ClassProfile> lstClassProfile = ClassProfileBusiness.All.Where(x => x.SchoolID == schoolId &&
                                                                                       x.EducationLevelID == educationLevelId &&
                                                                                       x.AcademicYearID == academicYearID).ToList();
                List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(x => x.EducationLevelID == educationLevelId).ToList();


                listClassProfile = (from lstCP in lstClassProfile
                                    join lstEDU in lstEducationLevel
                                    on lstCP.EducationLevelID equals lstEDU.EducationLevelID
                                    join lstDEG in lstDeclareEvaluationGroup on lstEDU.EducationLevelID equals lstDEG.EducationLevelID
                                    where lstCP.IsActive == true
                                    group new { lstCP, lstDEG } by new { lstCP.ClassProfileID, lstCP.DisplayName, lstDEG.EducationLevelID } into g
                                    select new ClassProfileTempBO
                                    {
                                        ClassProfileID = g.Key.ClassProfileID,
                                        DisplayName = g.Key.DisplayName,
                                        EducationLevelID = g.Key.EducationLevelID,
                                    }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.DisplayName).ToList();
            }
            else// giao vien
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearID;
                dic["AppliedLevel"] = appliedLevel;
                IQueryable<ClassProfile> iClass = ClassProfileBusiness.SearchBySchool(schoolId, dic);
                List<DeclareEvaluationGroup> lstlstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.GetListEvaluationByEvaluationTeacher(academicYearID, schoolId, appliedLevel, educationLevelId);
                lstlstDeclareEvaluationGroup = lstlstDeclareEvaluationGroup.Where(x => x.DeclareEvaluationGroupID == declareEvationGroupId).ToList();
                List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(x => x.EducationLevelID == educationLevelId).ToList();

                listClassProfile = (from lstCP in iClass
                                    join lstEDU in lstEducationLevel
                                    on lstCP.EducationLevelID equals lstEDU.EducationLevelID
                                    join lstDEG in lstlstDeclareEvaluationGroup on lstEDU.EducationLevelID equals lstDEG.EducationLevelID
                                    where lstCP.IsActive == true
                                    select new ClassProfileTempBO
                                    {
                                        ClassProfileID = lstCP.ClassProfileID,
                                        DisplayName = lstCP.DisplayName,
                                        EducationLevelID = lstCP.EducationLevelID,
                                        OrderNumber = lstCP.OrderNumber
                                    }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.DisplayName).ToList();
            }
            return listClassProfile;
        }
        #endregion

        /// <summary>
        /// Thuc hien luu vao bang threadMark khi co thay doi thong tin mon hoc
        /// </summary>
        /// <param name="lstClassSubject"></param>
        /// <param name="academicYearId"></param>
        /// <param name="schoolId"></param>
        /// <param name="currentSemester"></param>
        public void InsertThreadMark(List<ClassSubject> lstClassSubject, int academicYearId, int schoolId, int currentSemester)
        {
            if (lstClassSubject == null || lstClassSubject.Count == 0)
            {
                return;
            }
            //Chiendd1: 06/01/2016. Chi luu vao thong tin lop hoc, mon hoc va hoc sinh trong tool se lay de giam bot du lieu day vao ThreadMark
            List<int> lstClass = lstClassSubject.Select(c => c.ClassID).Distinct().ToList();
            int partitionid = UtilsBusiness.GetPartionId(schoolId);
            List<ThreadMark> lstThreadMark = new List<ThreadMark>();
            short semester = (short)currentSemester;
            int classId = 0;
            for (int i = 0; i < lstClass.Count; i++)
            {
                classId = lstClass[i];
                AddThreadMark(schoolId, partitionid, academicYearId, classId, 0, semester, lstThreadMark);
            }

            if (lstThreadMark != null && lstThreadMark.Count > 0)
            {
                ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);
            }

        }
        /// <summary>
        /// Dua du lieu vao doi tuong lstThreadMark de thuc hien luu vao ThreadMark
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="mode3"></param>
        /// <param name="academicYearId"></param>
        /// <param name="classId"></param>
        /// <param name="subjectId"></param>
        /// <param name="semester"></param>
        /// <param name="lstThreadMark"></param>
        private void AddThreadMark(int schoolId, int mode3, int academicYearId, int classId, int subjectId, short semester, List<ThreadMark> lstThreadMark)
        {
            //Bo where theo subjectId, tool chi thuc hien viec tinh tong ket khong tinh TBM
            if (lstThreadMark != null && lstThreadMark.Any(t => t.ClassID == classId && t.Semester == semester))
            {
                return;
            }
            ThreadMark objThreadMark1 = new ThreadMark
            {
                AcademicYearID = academicYearId,
                ClassID = classId,
                CreatedDate = DateTime.Now,
                LastDigitSchoolID = mode3,
                NumberScan = 0,
                PeriodID = 0,
                PupilID = 0,
                SchoolID = schoolId,
                Semester = 1,
                Status = 1,
                StatusResult = 0,
                SubjectID = 0,
                Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE
            };
            lstThreadMark.Add(objThreadMark1);
            ThreadMark objThreadMark2 = new ThreadMark
            {
                AcademicYearID = academicYearId,
                ClassID = classId,
                CreatedDate = DateTime.Now,
                LastDigitSchoolID = mode3,
                NumberScan = 0,
                PeriodID = 0,
                PupilID = 0,
                SchoolID = schoolId,
                Semester = 2,
                Status = 1,
                StatusResult = 0,
                SubjectID = 0,
                Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE
            };
            lstThreadMark.Add(objThreadMark2);
        }

        public List<ClassSubject> GetListSubjectByClass(int academicYearId, int classId, List<int> listSujectId)
        {
            List<ClassSubject> listResult = ClassSubjectBusiness.All.Where(p => listSujectId.Contains(p.SubjectID) && p.ClassID == classId).ToList();
            return listResult;
        }

        public void SaveMinMarkNumber(List<int> lstClassId, List<ClassSubject> lstModel, int? schoolId, int? academicYearId, int? appliedLevel)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolId;
                dic["AcademicYearID"] = academicYearId;
                dic["AppliedLevel"] = appliedLevel;
                dic["ListClassID"] = lstClassId;

                List<ClassSubject> lstClassSubjectAll = ClassSubjectBusiness.SearchBySchool(schoolId.Value, dic).ToList();

                ClassSubject model;
                List<ClassSubject> lstClassSubject;
                ClassSubject cs;
                for (int i = 0; i < lstModel.Count; i++)
                {
                    model = lstModel[i];
                    lstClassSubject = lstClassSubjectAll.Where(o => o.SubjectID == model.SubjectID).ToList();
                    for (int j = 0; j < lstClassSubject.Count; j++)
                    {
                        cs = lstClassSubject[j];

                        if (cs.SectionPerWeekFirstSemester > 0)
                        {
                            cs.MMinFirstSemester = model.MMinFirstSemester;
                            cs.PMinFirstSemester = model.PMinFirstSemester;
                            cs.VMinFirstSemester = model.VMinFirstSemester;
                        }

                        if (cs.SectionPerWeekSecondSemester > 0)
                        {
                            cs.MMinSecondSemester = model.MMinSecondSemester;
                            cs.PMinSecondSemester = model.PMinSecondSemester;
                            cs.VMinSecondSemester = model.VMinSecondSemester;
                        }
                        ClassSubjectBusiness.Update(cs);
                    }
                }

                ClassSubjectBusiness.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("AcademicYearID={0}, schoolId={1}, AppliedLevel={2}", academicYearId, schoolId, appliedLevel);
                LogExtensions.ErrorExt(logger, DateTime.Now, "SaveMinMarkNumber", paramList, ex);

            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }


        /// <summary>
        /// Lay danh sach lop giao vien co quyen bo mon theo mon hoc
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="schoolId"></param>
        /// <param name="subjectID"></param>
        /// <param name="teacherId"></param>
        /// <param name="schoolAdmin"></param>
        /// <param name="semester"></param>        
        /// <param name="isVNEN"></param>
        /// <returns></returns>
        public List<ClassProfileTempBO> GetListClassByTeachingAndSubjectId(int academicYearID, int schoolId, int subjectID, int teacherId, bool schoolAdmin, int semester, bool? isVNEN = false)
        {
            List<ClassProfileTempBO> listClassProfile = new List<ClassProfileTempBO>();
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.AllNoTracking.Where(s => s.Last2digitNumberSchool == partitionId
                                                                && s.SubjectID == subjectID);
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekFirstSemester > 0);
            }
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                iqClassSubject = iqClassSubject.Where(s => s.SectionPerWeekSecondSemester > 0);
            }



            IQueryable<ClassProfile> iqClass = ClassProfileBusiness.AllNoTracking.Where(c => c.AcademicYearID == academicYearID && c.IsActive == true);

            //Neu isVNEN=false thi khong lay cac lop VNEN nguoc lai thi lay tat ca
            if (isVNEN == false)
            {
                iqClassSubject = iqClassSubject.Where(s => s.IsSubjectVNEN != true);
                iqClass = iqClass.Where(c => c.IsVnenClass != true);
            }


            if (schoolAdmin)
            {

                IQueryable<ClassProfileTempBO> iqClassAdmin = from cb in iqClassSubject
                                                              join cp in iqClass on cb.ClassID equals cp.ClassProfileID
                                                              group cb by new { cb.ClassID, cp.DisplayName, cp.EducationLevelID, cp.OrderNumber } into g
                                                              select new ClassProfileTempBO
                                                              {
                                                                  ClassProfileID = g.Key.ClassID,
                                                                  DisplayName = g.Key.DisplayName,
                                                                  EducationLevelID = g.Key.EducationLevelID,
                                                                  OrderNumber = g.Key.OrderNumber
                                                              };


                listClassProfile = iqClassAdmin.ToList();

            }
            else //tai khoan giao vien
            {


                // Lay danh sach cac lop giao vien co quyen giao vien bo mon
                IQueryable<ClassSupervisorAssignment> iqClassSuperAssign = ClassSupervisorAssignmentBusiness.All.Where(
                                                                o => o.TeacherID == teacherId &&
                                                                o.AcademicYearID == academicYearID &&
                                                                o.SchoolID == schoolId &&
                                                                o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);

                IQueryable<ClassProfileTempBO> iqClassBO = from cp in iqClass
                                                           join csa in iqClassSuperAssign on cp.ClassProfileID equals csa.ClassID
                                                           select new ClassProfileTempBO
                                                           {
                                                               ClassProfileID = cp.ClassProfileID,
                                                               DisplayName = cp.DisplayName,
                                                               EducationLevelID = cp.EducationLevelID,
                                                               OrderNumber = cp.OrderNumber
                                                           };


                listClassProfile = iqClassBO.ToList();



                //Danh sach lop giao vien duoc phan cong giang day
                IQueryable<TeachingAssignment> iqTeachingAssignment = TeachingAssignmentBusiness.All.Where(o =>
                     o.TeacherID == teacherId &&
                     o.AcademicYearID == academicYearID &&
                     o.SchoolID == schoolId &&
                     o.SubjectID == subjectID &&
                     o.Semester == semester &&
                     o.IsActive
                     && o.Last2digitNumberSchool == partitionId
                 );

                IQueryable<ClassProfileTempBO> iqClassTeachingAssingnment = from cp in iqClass
                                                                            join cs in iqClassSubject on cp.ClassProfileID equals cs.ClassID
                                                                            join ta in iqTeachingAssignment on cp.ClassProfileID equals ta.ClassID
                                                                            where cs.SubjectID == ta.SubjectID
                                                                            select new ClassProfileTempBO
                                                                            {
                                                                                ClassProfileID = cp.ClassProfileID,
                                                                                DisplayName = cp.DisplayName,
                                                                                EducationLevelID = cp.EducationLevelID,
                                                                                OrderNumber = cp.OrderNumber
                                                                            };
                if (listClassProfile != null && listClassProfile.Count > 0)
                {
                    //Bo cac lop da co trong list
                    List<ClassProfileTempBO> lstClassAssignment = iqClassTeachingAssingnment.ToList();
                    for (int i = 0; i < lstClassAssignment.Count; i++)
                    {
                        ClassProfileTempBO classProfileTemp = lstClassAssignment[i];
                        if (listClassProfile.Exists(s => s.ClassProfileID == classProfileTemp.ClassProfileID))
                        {
                            continue;
                        }
                        listClassProfile.Add(classProfileTemp);
                    }

                    listClassProfile = listClassProfile.ToList();
                }
                else
                {
                    listClassProfile = iqClassTeachingAssingnment.Distinct().ToList();
                }


            }

            return (listClassProfile != null && listClassProfile.Count > 0) ?
                    listClassProfile.OrderBy(c => c.EducationLevelID).ThenBy(c => c.OrderNumber).ThenBy(c => c.DisplayName).ToList() : null;
        }
    }
    public class ObjectBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public int Status { get; set; }
    }
}
