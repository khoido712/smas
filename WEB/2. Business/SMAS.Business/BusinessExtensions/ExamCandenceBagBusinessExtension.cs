/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ExamCandenceBagBusiness
    {

       
        public IQueryable<ExamCandenceBag> Search(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examCandenceBagID = Utils.GetLong(search, "ExamCandenceBagID");
            List<long> lstExamGroupID = Utils.GetLongList(search, "lstExamGroupID");

            IQueryable<ExamCandenceBag> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examCandenceBagID != 0)
            {
                query = query.Where(o => o.ExamCandenceBagID == examCandenceBagID);
            }
            if (lstExamGroupID.Count > 0)
            {
                query = query.Where(o => lstExamGroupID.Contains(o.ExamGroupID));
            }

            return query;
        }

        /// <summary>
        /// Lay cac tui phach co chua thi sinh trong cac phong
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="examinationsId"></param>
        /// <param name="examGroupId"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        public List<ExamCandenceBag> GetExamCandenceBagByRoom(long examinationsId, long examGroupId, List<long> lstExamRoomId)
        {
            List<ExamCandenceBag> query = (from ecb in ExamCandenceBagRepository.All
                                             join edb in ExamDetachableBagRepository.All on ecb.ExamCandenceBagID equals edb.ExamCandenceBagID
                                             join ep in ExamPupilRepository.All on edb.ExamPupilID equals ep.ExamPupilID
                                             where ecb.ExaminationsID == examinationsId
                                             && edb.ExaminationsID == examinationsId
                                             && ep.ExaminationsID == examinationsId
                                             && ecb.ExamGroupID == examGroupId
                                             && edb.ExamGroupID == examGroupId
                                             && ep.ExamGroupID == examGroupId
                                             && (ep.ExamRoomID.HasValue ? lstExamRoomId.Contains(ep.ExamRoomID.Value) : false)
                                             select ecb).Distinct().ToList();
            return query;

        }
    }
}