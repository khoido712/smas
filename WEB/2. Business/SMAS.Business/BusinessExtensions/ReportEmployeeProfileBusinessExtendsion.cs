﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;


namespace SMAS.Business.Business
{
    public partial class ReportEmployeeProfileBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào
        public string GetHashKey(ReportEmployeeProfileBO reportEmployeeProfile)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["SchoolID"] = reportEmployeeProfile.SchoolID;
            Dictionary["ReportDate"] = reportEmployeeProfile.ReportDate;
            Dictionary["FacultyID"] = reportEmployeeProfile.FacultyID;
            Dictionary["AppliedLevel"] = reportEmployeeProfile.AppliedLevel;
            Dictionary["Compare"] = reportEmployeeProfile.Compare;
            Dictionary["SeniorYear"] = reportEmployeeProfile.SeniorYear;
            return ReportUtils.GetHashKey(Dictionary);
        }
        #endregion
        #region Lưu lại thông tin thống kê danh sách cán bộ
        public ProcessedReport InsertReportEmployeeList(ReportEmployeeProfileBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_DanhSachCanBo
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo danh sách cán bộ được cập nhật mới nhất
        public ProcessedReport GetReportEmployeeList(ReportEmployeeProfileBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeList(ReportEmployeeProfileBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BCGV;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int lastCol = 10;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "J" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = entity.ReportDate;//DateTime.Now;//

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange midRange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);
            IVTRange createUser = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol); // Khoang nguoi dung tao bao cao
            //Lấy dữ liệu
            //Them 1 ngay do ReportDate chi lay kieu Date so sanh voi DateTime
            DateTime rpDate = entity.ReportDate.AddDays(1);
            var iqEmloyee = from ehs in EmployeeHistoryStatusBusiness.All
                            
                            join emp in EmployeeBusiness.All on ehs.EmployeeID equals emp.EmployeeID
                            join tof in SchoolFacultyBusiness.All on emp.SchoolFacultyID equals tof.SchoolFacultyID
                            where emp.IsActive == true &&
                            ehs.SchoolID == entity.SchoolID
                            && tof.SchoolID == entity.SchoolID
                            //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.SchoolFacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate < rpDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                            //& ehs.Employee.AppliedLevel == entity.AppliedLevel
                            select emp;
            
            if(iqEmloyee.Count() > 0)
            {
                //iqEmloyee = iqEmloyee.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                List<object> listData = new List<object>();
                List<Employee> lstEmployeeTemp = iqEmloyee.ToList();
                List<Employee> lstEmployee = lstEmployeeTemp.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++ )
                {
                    if (i == lstEmployee.Count - 1)
                    {
                        dataSheet.CopyPasteSameSize(lastRange, firstRow + i, 1);
                    }
                    else 
                    {
                        dataSheet.CopyPasteSameSize(midRange, firstRow + i, 1);
                    }
                    dicData = new Dictionary<string, object>();
                    // STT
                    dicData["Order"] = i + 1;
                    // Ho ten
                    dicData["FullName"] = lstEmployee[i].FullName;
                    // Ma can bo
                    dicData["EmployeeCode"] = lstEmployee[i].EmployeeCode;
                    // Ngay sinh
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : null;
                    // Gioi tinh
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    // To bo mon
                    SchoolFaculty faculty = lstEmployee[i].SchoolFaculty;
                    dicData["FacultyName"] = faculty != null ? faculty.FacultyName : string.Empty;
                    // Dia chi
                    dicData["Address"] = lstEmployee[i].PermanentResidentalAddress;
                    // Loai cong viec
                    dicData["WorkType"] = lstEmployee[i].WorkType != null ? lstEmployee[i].WorkType.Resolution : string.Empty;
                    // Loai hop dong
                    dicData["ContractType"] = lstEmployee[i].ContractType != null ? lstEmployee[i].ContractType.Resolution : string.Empty;
                    // Tring do dao tao
                    dicData["TrainingLevel"] = lstEmployee[i].TrainingLevel != null ? lstEmployee[i].TrainingLevel.Resolution : string.Empty;
                    
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng người lập báo cáo
            dataSheet.CopyPasteSameSize(createUser, firstRow + iqEmloyee.Count() + 2, 1);
            dataSheet.Name = "DSCB";
            dataSheet.FitToPage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lưu lại thông tin thống kê danh sách cán bộ hợp đồng
        public ProcessedReport InsertReportEmployeeContract(ReportEmployeeProfileBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_DSCanBoHopDong
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion
        #region Lấy báo cáo danh sách cán bộ hợp đồngđược cập nhật mới nhất
        public ProcessedReport GetReportEmployeeContract(ReportEmployeeProfileBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ hợp đồng
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeContract(ReportEmployeeProfileBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int lastCol = 9;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 3, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = entity.ReportDate; //DateTime.Now;//

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange midRange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);
            IVTRange createUser = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            //Lấy dữ liệu
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Quá trình đào tạo của tất cả cán bộ
            var lsEmployeeQualification = EmployeeQualificationBusiness.SearchBySchool(entity.SchoolID, dic);
            DateTime rpDate = entity.ReportDate.AddDays(1);
            var iqEmloyee = from ehs in EmployeeHistoryStatusBusiness.All
                            join emps in EmployeeBusiness.All on ehs.EmployeeID equals emps.EmployeeID
                            join tof in SchoolFacultyBusiness.All on emps.SchoolFacultyID equals tof.SchoolFacultyID
                            join con in ContractTypeBusiness.All on emps.ContractTypeID equals con.ContractTypeID
                            where emps.IsActive == true &&
                            ehs.SchoolID == entity.SchoolID
                            //&& ehs.Employee.ContractType.Type == SystemParamsInFile.CONTRACT_TYPE_NOSTAFF
                            && (con.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_NOSTAFF_NAME || con.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_NOSTAFF_68)
                            && tof.SchoolID == entity.SchoolID
                            //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.SchoolFacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate < rpDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                            //&& ehs.Employee.AppliedLevel == entity.AppliedLevel
                            select emps;
            if (iqEmloyee.Count() > 0)
            {
                List<Employee> lstEmployeeTemp = iqEmloyee.ToList();
                List<object> listData = new List<object>();
                List<Employee> lstEmployee = lstEmployeeTemp.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                int teacherID = 0;
                EmployeeQualification employeeQualification = new EmployeeQualification();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    if (i == lstEmployee.Count - 1)
                    {
                        dataSheet.CopyPasteSameSize(lastRange, firstRow + i, 1);
                    }
                    else
                    {
                        dataSheet.CopyPasteSameSize(midRange, firstRow + i, 1);
                    }
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : null ;
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    SchoolFaculty faculty = lstEmployee[i].SchoolFaculty;
                    dicData["FacultyName"] = faculty != null ? faculty.FacultyName : string.Empty;
                    dicData["GraduationLevel"] = lstEmployee[i].TrainingLevel != null ? lstEmployee[i].TrainingLevel.Resolution : "";
                    dicData["Speciality"] = lstEmployee[i].SpecialityCat != null ? lstEmployee[i].SpecialityCat.Resolution : "";
                    teacherID = lstEmployee[i].EmployeeID;
                    employeeQualification = lsEmployeeQualification.Where(o => o.EmployeeID == teacherID).OrderBy(o => o.FromDate).FirstOrDefault();
                    dicData["FromDate"] = lstEmployee[i].StartingDate != null ? lstEmployee[i].StartingDate.Value.ToShortDateString() : null;
                    if (employeeQualification != null)
                    {
                        dicData["QualifiedAt"] = employeeQualification.QualifiedAt;
                        
                    }
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng người lập báo cáo
            dataSheet.CopyPasteSameSize(createUser, firstRow + iqEmloyee.Count() + 2, 1);
            dataSheet.Name = "DS CB HDBC";
            dataSheet.FitToPage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lưu lại thông tin thống kê danh sách cán bộ biên chế
        public ProcessedReport InsertReportEmployeePayroll(ReportEmployeeProfileBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_DSCanBoBienChe
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo danh sách cán bộ biên chế được cập nhật mới nhất
        public ProcessedReport GetReportEmployeePayroll(ReportEmployeeProfileBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ biên chế
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeePayroll(ReportEmployeeProfileBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int lastCol = 9;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 4, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = entity.ReportDate; //DateTime.Now;//entity.ReportDate;

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange midRange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);
            IVTRange createUser = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastCol);
            //Lấy dữ liệu
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Quá trình đào tạo của tất cả cán bộ
            var lsEmployeeQualification = EmployeeQualificationBusiness.SearchBySchool(entity.SchoolID, dic);
            DateTime rpDate = entity.ReportDate.AddDays(1);
            var iqEmloyee = from ehs in EmployeeHistoryStatusBusiness.All
                            join emp in EmployeeBusiness.All on ehs.EmployeeID equals emp.EmployeeID
                            join tof in SchoolFacultyBusiness.All on emp.SchoolFacultyID equals tof.SchoolFacultyID
                            join con in ContractTypeBusiness.All on emp.ContractTypeID equals con.ContractTypeID
                            where emp.IsActive == true &&
                            ehs.SchoolID == entity.SchoolID
                            //&& ehs.Employee.ContractType.Type == SystemParamsInFile.CONTRACT_TYPE_STAFF
                            && con.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_STAFF_NAME
                            && tof.SchoolID == entity.SchoolID
                            //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.SchoolFacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate < rpDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                            //&& ehs.Employee.AppliedLevel == entity.AppliedLevel
                            select emp;
            if (iqEmloyee.Count() > 0)
            {
                List<Employee> lstEmployeeTemp = iqEmloyee.ToList();
                List<object> listData = new List<object>();
                List<Employee> lstEmployee = lstEmployeeTemp.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                int teacherID = 0;
                EmployeeQualification employeeQualification = new EmployeeQualification();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    if (i == lstEmployee.Count - 1)
                    {
                        dataSheet.CopyPasteSameSize(lastRange, firstRow + i, 1);
                    }
                    else
                    {
                        dataSheet.CopyPasteSameSize(midRange, firstRow + i, 1);
                    }
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : null;
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    SchoolFaculty faculty = lstEmployee[i].SchoolFaculty;
                    dicData["FacultyName"] = faculty != null ? faculty.FacultyName : string.Empty;
                    dicData["GraduationLevel"] = lstEmployee[i].TrainingLevel != null ? lstEmployee[i].TrainingLevel.Resolution : "";
                    dicData["Speciality"] = lstEmployee[i].SpecialityCat != null ? lstEmployee[i].SpecialityCat.Resolution : "";
                    dicData["FromDate"] = lstEmployee[i].JoinedDate != null ? lstEmployee[i].JoinedDate.Value.ToShortDateString() : null;
                    teacherID = lstEmployee[i].EmployeeID;
                    employeeQualification = lsEmployeeQualification.Where(o => o.EmployeeID == teacherID).OrderBy(o => o.FromDate).FirstOrDefault();
                    if (employeeQualification != null)
                    {
                        dicData["QualifiedAt"] = employeeQualification.QualifiedAt;
                    }
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng người lập báo cáo
            dataSheet.CopyPasteSameSize(createUser, firstRow + iqEmloyee.Count() + 2, 1);
            dataSheet.Name = "DS CB HDBC";
            dataSheet.FitToPage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lưu lại thông tin thống kê danh sách cán bộ theo thâm niên
        public ProcessedReport InsertReportEmployeeSenior(ReportEmployeeProfileBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_DSCanBoTheoThamNien
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
                {"Compare", entity.Compare},
                {"SeniorYear", entity.SeniorYear},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo danh sách cán bộ theo thâm niên được cập nhật mới nhất
        public ProcessedReport GetReportEmployeeSenior(ReportEmployeeProfileBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ theo thâm niên
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeSenior(ReportEmployeeProfileBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 11;
            int lastCol = 9;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 3, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string compare = entity.Compare == 1 ? ">=" : entity.Compare == 2 ? "<=" : "=";
            DateTime reportDate = entity.ReportDate; //DateTime.Now;
            string date = entity.ReportDate.ToShortDateString();
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Compare", compare},
                    {"Date", date},
                    {"SeniorYear", entity.SeniorYear}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange midRange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);
            IVTRange createUser = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            //Lấy dữ liệu
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Quá trình đào tạo của tất cả cán bộ
            var lsEmployeeQualification = EmployeeQualificationBusiness.SearchBySchool(entity.SchoolID, dic);
            DateTime rpDate = entity.ReportDate.AddDays(1);
            var iqEmloyee = from ehs in EmployeeHistoryStatusBusiness.All
                            //join tof in TeacherOfFacultyRepository.All on ehs.EmployeeID equals tof.TeacherID
                            join emp in EmployeeBusiness.All on ehs.EmployeeID equals emp.EmployeeID
                            join tof in SchoolFacultyBusiness.All on emp.SchoolFacultyID equals tof.SchoolFacultyID
                            join con in ContractTypeBusiness.All on emp.ContractTypeID equals con.ContractTypeID
                            where emp.IsActive == true &&
                            ehs.SchoolID == entity.SchoolID
                            && tof.SchoolID == entity.SchoolID
                            && con.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_STAFF_NAME
                            //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.SchoolFacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate < rpDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                            //&& ehs.Employee.AppliedLevel == entity.AppliedLevel
                            select ehs.Employee;
            DateTime compareDate = entity.ReportDate.AddYears(-entity.SeniorYear.Value);
            
            //Do StartingDate luu ca gio, phut, giay nen khi so sanh can +1 ngay
            DateTime compareDate1 = compareDate.AddDays(1);
            if (entity.Compare == 1)
            {
                iqEmloyee = iqEmloyee.Where(o => o.JoinedDate != null && o.JoinedDate < compareDate1);
            }
            else if (entity.Compare == 2)
            {
                iqEmloyee = iqEmloyee.Where(o => o.JoinedDate != null && o.JoinedDate >= compareDate);
            }
            else
            {
                string temp = "01/01/" + (entity.ReportDate.Date.Year - entity.SeniorYear) + "";
                DateTime tmpDate = Convert.ToDateTime(temp);
                iqEmloyee = iqEmloyee.Where(o => o.JoinedDate != null && o.JoinedDate < compareDate && o.JoinedDate > tmpDate);
            }

            if (iqEmloyee.Count() > 0)
            {
                List<object> listData = new List<object>();
                List<Employee> lstEmployee = iqEmloyee.ToList();
                lstEmployee = lstEmployee.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                EmployeeQualification employeeQualification = new EmployeeQualification();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    if (i == lstEmployee.Count - 1)
                    {
                        dataSheet.CopyPasteSameSize(lastRange, firstRow + i, 1);
                    }
                    else
                    {
                        dataSheet.CopyPasteSameSize(midRange, firstRow + i, 1);
                    }
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : null;
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    dicData["BirthPlace"] = lstEmployee[i].HomeTown;
                    dicData["GraduationLevel"] = lstEmployee[i].TrainingLevel != null ? lstEmployee[i].TrainingLevel.Resolution : "";
                    dicData["Speciality"] = lstEmployee[i].SpecialityCat != null ? lstEmployee[i].SpecialityCat.Resolution : "";
                    dicData["StartingDate"] = lstEmployee[i].StartingDate != null ? lstEmployee[i].StartingDate.Value.ToShortDateString() : null;
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng người lập báo cáo
            dataSheet.CopyPasteSameSize(createUser, firstRow + iqEmloyee.Count() + 2, 1);
            dataSheet.Name = "Danh Sach Can Bo";
            dataSheet.FitToPage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Báo cáo Danh sach GVCN
        private string GetHashKeyHeadTeacher(IDictionary<string,object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }
        public ProcessedReport InsertHeadTeacherProcessReport(IDictionary<string,object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_GVCN;
            int EducationLevelID = Utils.GetInt(dic,"EducationLevelID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyHeadTeacher(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_DanhSachCanBo
            string EducationLevelName = EducationLevelID != 0 ? "_Khoi_" + EducationLevelID : "";
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationLevelName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public ProcessedReport GetReportHeadTeacherProcess(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_GVCN;
            string inputParameterHashKey = this.GetHashKeyHeadTeacher(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public Stream CreateHeadTeacherReport(IDictionary<string,object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_GVCN;
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            //fill thong tin chung
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, AppliedLevelID).ToUpper());
            sheet.SetCellValue("A3", objSP.SchoolName.ToUpper());
            sheet.SetCellValue("A6", "NĂM HỌC " + objAy.DisplayTitle);
            //lay du lieu
            IQueryable<HeadTeacherReport> iquery = (from cp in ClassProfileBusiness.All
                                                    join e in EmployeeBusiness.All on cp.HeadTeacherID equals e.EmployeeID into l
                                                    from l1 in l.DefaultIfEmpty()
                                                    where cp.SchoolID == SchoolID
                                                    && cp.AcademicYearID == AcademicYearID
                                                    && cp.IsActive.HasValue && cp.IsActive.Value
                                                    select new HeadTeacherReport
                                                    {
                                                        ClassID = cp.ClassProfileID,
                                                        ClassName = cp.DisplayName,
                                                        EducationLevelID = cp.EducationLevelID,
                                                        OrderID = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                        HeadTeacherName = l1.FullName,
                                                        Mobile = l1.Mobile
                                                    }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderID).ThenBy(p => p.ClassName);
            if (EducationLevelID != 0)
            {
                iquery = iquery.Where(p => p.EducationLevelID == EducationLevelID);
            }
            else
            {
                if (AppliedLevelID == 1)
                {
                    iquery = iquery.Where(p => p.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID && p.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID6);
                }
                else if (AppliedLevelID == 2)
                {
                    iquery = iquery.Where(p => p.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID5 && p.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID10);
                }
                else
                {
                    iquery = iquery.Where(p => p.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID9 && p.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID13);
                }
            }
            List<int> lstClassID = iquery.Select(p => p.ClassID).Distinct().ToList();
            //lay danh sach hoc sinh cua tat ca cac lop
            IDictionary<string, object> dicPOC = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"AppliedLevelID",AppliedLevelID},
                {"ListClassID",lstClassID}
            };
            IQueryable<PupilOfClass> iqueryPOC = PupilOfClassBusiness.Search(dicPOC).Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING);
            List<HeadTeacherReport> lstResult = iquery.ToList();
            HeadTeacherReport objResult = null;
            int startRow = 9;
            int count = 0;
            for (int i = 0; i < lstResult.Count; i++)
            {
                objResult = lstResult[i];
                count = iqueryPOC.Where(p => p.ClassID == objResult.ClassID).Count();
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objResult.ClassName);
                sheet.SetCellValue(startRow, 3, count);
                sheet.SetCellValue(startRow, 4, objResult.HeadTeacherName);
                sheet.SetCellValue(startRow, 5, objResult.Mobile);
                startRow++;
            }
            if (lstResult.Count > 0)
            {
                sheet.GetRange(9, 1, startRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            return oBook.ToStream();
        }
        private class HeadTeacherReport
        {
            public int ClassID { get; set; }
            public string ClassName { get; set; }
            public string HeadTeacherName { get; set; }
            public string Mobile { get; set; }
            public int EducationLevelID { get; set; }
            public int? OrderID { get; set; }
        }
        #endregion
    }
}
