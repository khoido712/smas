﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Configuration;

namespace SMAS.Business.Business
{
    public partial class VMarkRecordBusiness
    {
        /// <summary>
        /// Quanglm
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<VMarkRecord> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.SearchMarkRecord(dic);
        }


        /// <summary>
        /// Quanglm
        /// Tim kiem theo thong tin chi tiet
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<VMarkRecord> SearchMarkRecord(IDictionary<string, object> dic)
        {
            int MarkRecordID = Utils.GetInt(dic, "MarkRecordID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int PupilFileID = Utils.GetInt(dic, "PupilID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int Semester = Utils.GetInt(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            double Mark = Utils.GetDouble(dic, "Mark", -1);
            double ReTestMark = Utils.GetDouble(dic, "ReTestMark", -1);
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            // Tim kiem
            IQueryable<PupilProfile> IqPupilProfile = PupilProfileBusiness.AllNoTracking;
            IQueryable<VMarkRecord> lsMarkRecord = VMarkRecordBusiness.AllNoTracking.Where(o => IqPupilProfile.Any(p => p.PupilProfileID == o.PupilID && p.IsActive));
            // AnhVD 20131217 - Search with partition column
            if (SchoolID > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Last2digitNumberSchool == SchoolID % 100));
            }

            // End 
            if (AcademicYearID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.AcademicYearID == AcademicYearID));
                //AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                var aca = this.AcademicYearBusiness.All
                    .Where(p => p.AcademicYearID == AcademicYearID)
                    .Select(p => new { AcademicYearID = p.AcademicYearID, SchoolID = p.SchoolID, Year = p.Year })
                    .FirstOrDefault();
                if (aca == null) throw new BusinessException("Năm học không tồn tại");
                int? createAcaYear = aca.Year;
                lsMarkRecord = lsMarkRecord.Where(o => (o.CreatedAcademicYear == createAcaYear));
                // Phong khi khong tim kiem theo dk school_id
                if (SchoolID <= 0)
                {
                    lsMarkRecord = lsMarkRecord.Where(o => (o.Last2digitNumberSchool == aca.SchoolID % 100));
                }
            }
            if (MarkRecordID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkRecordID == MarkRecordID));
            }
            if (SubjectID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (PupilFileID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.PupilID == PupilFileID));
            }
            if (SchoolID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => lstPupilID.Any(v => v == o.PupilID));
            }
            if (MarkTypeID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (Semester != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Semester == Semester));
            }
            //quanglm sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        lsMarkRecord = lsMarkRecord.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (ClassID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.ClassID == ClassID));
            }
            if (AppliedLevel != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c=>c.IsActive==true);
                lsMarkRecord = lsMarkRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevel.Grade == AppliedLevel));
            }
            if (Mark != -1)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Mark == (decimal)Mark));
            }
            if (ReTestMark != -1)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Mark == (decimal)ReTestMark));
            }
            if (CreatedDate.HasValue)
            {
                //Nen chuyen trong DB loi thanh kieu DATE thoi de query
                lsMarkRecord = lsMarkRecord.Where(o => o.CreatedDate.HasValue && o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                lsMarkRecord = lsMarkRecord.Where(o => o.ModifiedDate.HasValue && o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (Title != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Title == Title));
            }
            if (MarkTitle != "")
            {
                IQueryable<MarkType> IqMarkType = MarkTypeBusiness.All;
                lsMarkRecord = lsMarkRecord.Where(o => IqMarkType.Any(m => m.MarkTypeID == o.MarkTypeID && m.Title == MarkTitle));
            }
            if (strMarkType != null && strMarkType != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c=>c.IsActive==true);
                lsMarkRecord = lsMarkRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID));
            }
            return lsMarkRecord;
        }

        public IQueryable<MarkRecordBO> SearchMarkHistoryOrNoHistory(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            if (SchoolID == 0)
            {
                return null;
            }
            int Year = Utils.GetInt(dic, "Year");
            int MarkRecordID = Utils.GetInt(dic, "MarkRecordID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int PupilFileID = Utils.GetInt(dic, "PupilID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int Semester = Utils.GetInt(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            decimal Mark = Utils.GetDecimal(dic, "Mark", -1);
            decimal ReTestMark = Utils.GetDecimal(dic, "ReTestMark", -1);
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            // Tim kiem
            IQueryable<PupilProfile> IqPupilProfile = PupilProfileBusiness.AllNoTracking;

            IQueryable<MarkRecordBO> lsMarkRecord;
            lsMarkRecord = from mr in VMarkRecordBusiness.AllNoTracking
                           join pp in IqPupilProfile on mr.PupilID equals pp.PupilProfileID
                           where pp.IsActive && mr.Last2digitNumberSchool == partitionId
                           select new MarkRecordBO
                           {
                               Last2digitNumberSchool = mr.Last2digitNumberSchool,
                               AcademicYearID = mr.AcademicYearID,
                               CreatedAcademicYear = mr.CreatedAcademicYear,
                               MarkRecordID = mr.MarkRecordID,
                               SubjectID = mr.SubjectID,
                               PupilID = mr.PupilID,
                               SchoolID = mr.SchoolID,
                               MarkTypeID = mr.MarkTypeID,
                               Semester = mr.Semester,
                               Title = mr.Title,
                               ClassID = mr.ClassID,
                               Mark = mr.Mark,
                               CreatedDate = mr.CreatedDate,
                               ModifiedDate = mr.ModifiedDate,
                           };

            if (AcademicYearID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.AcademicYearID == AcademicYearID));
                /*AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                if (aca == null) throw new BusinessException("Năm học không tồn tại");
                int? createAcaYear = aca.Year;
                lsMarkRecord = lsMarkRecord.Where(o => (o.CreatedAcademicYear == createAcaYear));*/
            }
            if (Year > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => o.CreatedAcademicYear == Year);
            }
            if (MarkRecordID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkRecordID == MarkRecordID));
            }
            if (SubjectID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (PupilFileID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.PupilID == PupilFileID));
            }
            if (SchoolID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => lstPupilID.Any(v => v == o.PupilID));
            }
            if (MarkTypeID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (Semester != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Semester == Semester));
            }
            //quanglm sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        lsMarkRecord = lsMarkRecord.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (ClassID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.ClassID == ClassID));
            }
            if (AppliedLevel != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c=>c.IsActive==true);
                if (AcademicYearID > 0)
                {
                    IqClassProfile = IqClassProfile.Where(c => c.AcademicYearID == AcademicYearID);
                }
                lsMarkRecord = lsMarkRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevel.Grade == AppliedLevel));
            }
            if (Mark != -1)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Mark == Mark));
            }
            if (ReTestMark != -1)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Mark == ReTestMark));
            }
            if (CreatedDate.HasValue)
            {
                //Nen chuyen trong DB loi thanh kieu DATE thoi de query
                lsMarkRecord = lsMarkRecord.Where(o => o.CreatedDate.HasValue && o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                lsMarkRecord = lsMarkRecord.Where(o => o.ModifiedDate.HasValue && o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (Title != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Title == Title));
            }
            if (MarkTitle != "")
            {
                IQueryable<MarkType> IqMarkType = MarkTypeBusiness.All;
                lsMarkRecord = lsMarkRecord.Where(o => IqMarkType.Any(m => m.MarkTypeID == o.MarkTypeID && m.Title == MarkTitle));
            }
            if (strMarkType != null && strMarkType != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c=>c.IsActive==true);
                lsMarkRecord = lsMarkRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID));
            }
            return lsMarkRecord;
        }
    }
}