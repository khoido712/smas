/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ClassSupervisorAssignmentBusiness
    {
        /// <summary>
        /// Tìm kiếm Phân công giáo vụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ClassID">ID lớp</param>
        /// <param name="TeacherID">ID giáo viên</param>
        /// <param name="FullName">Tên đầy đủ</param>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="SchoolFacultyID">ID tổ bộ môn</param>
        /// <param name="EducationLevel">Khối lớp</param>
        /// <param name="AcademicYearID">ID năm học</param>
        /// <returns>Danh sách phân công giáo vụ</returns>      
        public IQueryable<ClassSupervisorAssignment> Search(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            string FullName = Utils.GetString(dic, "FullName");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
            int EducationLevel = Utils.GetShort(dic, "EducationLevelID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PermissionLevel = Utils.GetByte(dic, "PermissionLevel");
            IQueryable<ClassSupervisorAssignment> lsClassSupervisorAssignment = ClassSupervisorAssignmentRepository.All;
            if (ClassID != 0)
            {
                lsClassSupervisorAssignment = from csa in lsClassSupervisorAssignment
                                              join cp in ClassProfileBusiness.All on csa.ClassID equals cp.ClassProfileID
                                              where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                              && csa.ClassID == ClassID
                                              select csa;
                    //lsClassSupervisorAssignment.Where(o => o.ClassProfile.ClassProfileID == ClassID);
            }
            if (TeacherID != 0)
            {
                lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.TeacherID == TeacherID);
            }
            if (FullName.Trim().Length != 0)
            { lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.Employee.FullName.Contains(FullName.ToLower())); }
            if (SchoolID != 0)
            {
                lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.SchoolID == SchoolID);
            }
            if (SchoolFacultyID != 0)
            {
                lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.Employee.SchoolFacultyID == SchoolFacultyID);
            }
            if (EducationLevel != 0)
            {
                lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.ClassProfile.EducationLevelID == EducationLevel);
            }
            if (AcademicYearID != 0)
            {
                lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if(PermissionLevel != 0)
            {
                lsClassSupervisorAssignment = lsClassSupervisorAssignment.Where(o => o.PermissionLevel == PermissionLevel);
            }
            return lsClassSupervisorAssignment;

        }

        

        /// <summary>
        /// Lưu thông tin phân công giáo vụ cho toàn khối
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="TeacherID"></param>
        /// <param name="ClassAndPermission"></param>
        public void UpdateForAllEducationLevel(int SchoolID,
            int AppliedLevel, int AcademicYearID, int EducationLevelID,
            int TeacherID, Dictionary<int, List<int>> ClassAndPermission)
        {
            if (ClassAndPermission == null)
            {
                ClassAndPermission = new Dictionary<int, List<int>>();
            }

            // Kiểm tra sự tồn tại của SchoolID
            this.SchoolProfileBusiness.CheckAvailable(SchoolID, "ClassSupervisorAssignment_Label_School");

            // Kiểm tra sự tồn tại AcademicYearID
            this.AcademicYearBusiness.CheckAvailable(AcademicYearID, "ClassSupervisorAssignment_Label_AcademicYear");

            // Kiểm tra tính hợp lệ của dữ liệu 
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ của dữ liệu 
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["Grade"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra sự tồn tại của TeacherID
            this.EmployeeBusiness.CheckAvailable(TeacherID, "ClassSupervisorAssignment_Label_Teacher");

            // Kiểm tra tính hợp lệ của dữ liệu 
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = TeacherID;
            SearchInfo["SchoolID"] = SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ của dữ liệu 
            int[] PERMISSIONS = new int[] { 
                SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER,
                SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER,
                SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING
            };
            foreach (int ClassID in ClassAndPermission.Keys)
            {
                ClassProfile Class = this.ClassProfileBusiness.Find(ClassID);
                if (Class.AcademicYearID != AcademicYearID || Class.EducationLevelID != EducationLevelID)
                {
                    throw new BusinessException("Common_Validate_DataNotCompatible");
                }

                List<int> ListPermission = ClassAndPermission[ClassID];
                if (ListPermission == null)
                {
                    continue;
                }
                foreach (int permission in ListPermission)
                {
                    if (permission == 0)
                    {
                        continue;
                    }

                    if (!PERMISSIONS.Contains(permission))
                    {
                        throw new BusinessException("Common_Validate_DataNotCompatible");
                    }
                }
            }

            // Kiểm tra trạng thái giáo viên phải đang làm việc
            Employee Teacher = this.EmployeeBusiness.Find(TeacherID);
            if (Teacher == null || Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("ClassSupervisorAssignment_Validate_TeacherNotWorking");
            }
            
            // Xóa dữ liệu cũ
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["TeacherID"] = TeacherID;
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AcademicYearID"] = AcademicYearID;

            List<ClassSupervisorAssignment> ListToDel = this.SearchBySchool(SchoolID, SearchInfo).ToList();
            this.DeleteAll(ListToDel);

            // Thêm dữ liệu mới
            // Kiểm tra tính hợp lệ của dữ liệu 
            foreach (int ClassID in ClassAndPermission.Keys)
            {
                List<int> ListPermission = ClassAndPermission[ClassID];
                if (ListPermission == null)
                {
                    continue;
                }
                foreach (int permission in ListPermission)
                {
                    if (permission == 0)
                    {
                        continue;
                    }

                    if (!PERMISSIONS.Contains(permission))
                    {
                        throw new BusinessException("Common_Validate_DataNotCompatible");
                    }
                    ClassSupervisorAssignment entity = new ClassSupervisorAssignment();
                    entity.AcademicYearID = AcademicYearID;
                    entity.AssignedDate = DateTime.Now;
                    entity.ClassID = ClassID;
                    entity.PermissionLevel = (int)permission;
                    entity.SchoolID = SchoolID;
                    entity.TeacherID = TeacherID;

                    this.Insert(entity);
                }
            }
        }

        /// <summary>
        /// Tìm kiếm theo trường
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassSupervisorAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        /// <summary>
        /// Xóa thông tin chủ nhiệm khi xóa GV
        /// </summary>
        /// <param name="SearchInfo"></param>
        public void DeleteByTeacherID(IDictionary<string, object> SearchInfo)
        {
            var LstClassSupAss = this.Search(SearchInfo).Select(o => o.ClassSupervisorAssignmentID).ToList();
            int Count = 0;
            if (LstClassSupAss != null && (Count = LstClassSupAss.Count) > 0)
            {
                for (int i = 0; i < Count; i++)
                {
                    this.Delete(LstClassSupAss[i]);
                }
                this.ClassSupervisorAssignmentBusiness.Save();
            }
        }
    }
}
