/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class ServicePackageDetailBusiness
    {
        /// <summary>
        /// lay lay sach goi cuoc voi chi tiet so tin
        /// </summary>
        /// <returns></returns>
        public List<ServicePackageBO> GetListMaxSMSSP(int year)
        {
            return (from spd in this.All
                    group spd by new { spd.SERVICE_PACKAGE_ID } into g
                    join spdd in this.ServicePackageDeclareBusiness.All on g.Key.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                    where spdd.YEAR == year
                    select new ServicePackageBO
                    {
                        ServicePackageID = g.Key.SERVICE_PACKAGE_ID,
                        IsLimit = spdd.IS_LIMIT == null || spdd.IS_LIMIT == false ? false : true,
                        MaxSMS = g.Sum(p => p.MAX_SMS)
                    }).ToList();
        }
    }
}
