﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class EmployeeHistoryStatusBusiness
    {
        public override EmployeeHistoryStatus Insert(EmployeeHistoryStatus EmployeeHistoryStatus)
        {
            if (!EmployeeHistoryStatus.SchoolID.HasValue && !EmployeeHistoryStatus.SupervisingDeptID.HasValue)
                throw new BusinessException("SchoolID and SupervisingDept cannot both null");
            //Check Ton tai truong hoc neu SchoolID khac NULL
            if (EmployeeHistoryStatus.SchoolID.HasValue) this.SchoolBusiness.CheckAvailable(EmployeeHistoryStatus.SchoolID.Value, "SchoolProfile_Label_SchoolProfileID");

            //Check ton tai don vi quan ly neu SupervisingDeptID khac NULL
            if (EmployeeHistoryStatus.SupervisingDeptID.HasValue) this.SchoolBusiness.CheckAvailable(EmployeeHistoryStatus.SupervisingDeptID.Value, "EmployeeHistoryStatus_Label_EmployeeHistoryStatusID");

            return base.Insert(EmployeeHistoryStatus);
        }

        public override EmployeeHistoryStatus Update(EmployeeHistoryStatus EmployeeHistoryStatus)
        {
            this.CheckAvailable(EmployeeHistoryStatus.EmployeeHistoryStatusID, "EmployeeHistoryStatus_Label_EmployeeHistoryStatusID");

            if (!EmployeeHistoryStatus.SchoolID.HasValue && !EmployeeHistoryStatus.SupervisingDeptID.HasValue)
                throw new BusinessException("SchoolID and SupervisingDeptID cannot both null");

            //Check Ton tai truong hoc neu SchoolID khac NULL
            if (EmployeeHistoryStatus.SchoolID.HasValue) this.SchoolBusiness.CheckAvailable(EmployeeHistoryStatus.SchoolID.Value, "SchoolProfile_Label_SchoolProfileID");

            //Check ton tai don vi quan ly neu SupervisingDeptID khac NULL
            if (EmployeeHistoryStatus.SupervisingDeptID.HasValue) this.SchoolBusiness.CheckAvailable(EmployeeHistoryStatus.SupervisingDeptID.Value, "EmployeeHistoryStatus_Label_EmployeeHistoryStatusID");

            return base.Update(EmployeeHistoryStatus);
        }

        public void Delete(int EmployeeHistoryStatusID)
        {
            this.CheckAvailable(EmployeeHistoryStatusID, "EmployeeHistoryStatus_Label_EmployeeHistoryStatusID", true);

            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "EmployeeHistoryStatus", EmployeeHistoryStatusID, "EmployeeHistoryStatus_Label_EmployeeHistoryStatusID");

            base.Delete(EmployeeHistoryStatusID, true);

        }

        public IQueryable<EmployeeHistoryStatus> Search(IDictionary<string, object> dic)
        {
            int? employeeHistoryStatusID = (int?)dic["EmployeeHistoryStatusID"];
            int? employeeID = (int?)dic["EmployeeID"];
            int? schoolID = (int?)dic["SchoolID"];
            int? supervisingDeptID = (int?)dic["SupervisingDeptID"];

            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = this.EmployeeHistoryStatusRepository.All;

            if (employeeHistoryStatusID.HasValue && employeeHistoryStatusID.Value > 0) 
                lsEmployeeHistoryStatus = lsEmployeeHistoryStatus.Where(u => u.EmployeeHistoryStatusID == employeeHistoryStatusID.Value);

            if (employeeID.HasValue && employeeID.Value > 0)
                lsEmployeeHistoryStatus = lsEmployeeHistoryStatus.Where(u => u.EmployeeID == employeeID.Value);

            if (schoolID.HasValue && schoolID.Value > 0)
                lsEmployeeHistoryStatus = lsEmployeeHistoryStatus.Where(u => u.SchoolID == schoolID.Value);

            if (supervisingDeptID.HasValue && supervisingDeptID.Value > 0)
                lsEmployeeHistoryStatus = lsEmployeeHistoryStatus.Where(u => u.SupervisingDeptID == supervisingDeptID.Value);

            return lsEmployeeHistoryStatus;
        }
    }
}
