/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;

namespace SMAS.Business.Business
{
    public partial class ExaminationRoomBusiness
    {
        private void Validate(int SchoolID, int AppliedLevel, ExaminationRoom Entity)
        {
            //ExaminationID: PK(Examination)
            ExaminationBusiness.CheckAvailable(Entity.ExaminationID, "ExaminationRoom_Label_ExaminationID");
            //ExaminationID, SchoolID: not compatible (Examination)
            IDictionary<string, object> SearchInfo1 = new Dictionary<string, object>();
            SearchInfo1["SchoolID"] = SchoolID;
            SearchInfo1["ExaminationID"] = Entity.ExaminationID;
            bool compatible1 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo1);
            if (!compatible1)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ExaminationID, AppliedLevel: not compatible (Examination)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["AppliedLevel"] = AppliedLevel;
            SearchInfo2["ExaminationID"] = Entity.ExaminationID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            if (Entity.ExaminationSubjectID != 0)
            {
                //ExaminationSubjectID: PK(ExaminationSubject)
                ExaminationSubjectBusiness.CheckAvailable(Entity.ExaminationSubjectID, "ExaminationRoom_Label_ExaminationSubjectID");
                //ExaminationSubjectID, ExaminationID: not compatible(ExaminationSubject)
                IDictionary<string, object> SearchInfo3 = new Dictionary<string, object>();
                SearchInfo3["ExaminationID"] = Entity.ExaminationID;
                SearchInfo3["ExaminationSubjectID"] = Entity.ExaminationSubjectID;
                bool compatible3 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "ExaminationSubject", SearchInfo3);
                if (!compatible3)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            //ExaminationSubjectID, RoomTitle: duplicate (ExaminationRoom)
            if (Entity.ExaminationSubjectID != 0)
            {
                if (ExaminationRoomBusiness.All.Where(o => o.ExaminationID == Entity.ExaminationID && o.RoomTitle.ToUpper() == Entity.RoomTitle.ToUpper() && o.ExaminationSubjectID == Entity.ExaminationSubjectID && o.EducationLevelID == Entity.EducationLevelID && o.ExaminationRoomID != Entity.ExaminationRoomID).Count() > 0)
                {
                    throw new BusinessException("Common_Validate_DuplicateRoom");
                }
            }
            else
            {
                if (ExaminationRoomBusiness.All.Where(o => o.ExaminationID == Entity.ExaminationID && o.RoomTitle.ToUpper() == Entity.RoomTitle.ToUpper() && o.EducationLevelID == Entity.EducationLevelID && o.ExaminationRoomID != Entity.ExaminationRoomID).Count() > 0)
                {
                    throw new BusinessException("Common_Validate_DuplicateRoom");
                }
            }
            //RoomTitle: require, maxlength(100)
            Utils.ValidateRequire(Entity.RoomTitle, "ExaminationRoom_Label_RoomTitle");
            Utils.ValidateMaxLength(Entity.RoomTitle, 100, "ExaminationRoom_Label_RoomTitle");
            //0 < NumberOfSeat < 1000
            Utils.ValidateRange((double)Entity.NumberOfSeat, 1, 999, "ExaminationRoom_Label_NumberOfSeat");
            //Description: maxlength(300)
            Utils.ValidateMaxLength(Entity.Description, 300, "ExaminationRoom_Label_Description");
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            Examination Exam = ExaminationBusiness.Find(Entity.ExaminationID);
            if (Exam != null)
            {
                if (!AcademicYearBusiness.IsCurrentYear(Exam.AcademicYearID))
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }
            }
            //Examination(ExaminationID).CurrentStage (lấy trong CSDL) chỉ được nhận <  EXAMINATION_STAGE_MARK_COMPLETED (5)
            Utils.ValidateCompareWithNumber(Exam.CurrentStage, SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED);


        }

        #region search
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{ExaminationRoom}
        /// </returns>
        /// <author>hath</author>
        /// <date>11/13/2012</date>
        private IQueryable<ExaminationRoom> Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ExaminationRoom> lsExaminationRoom = ExaminationRoomRepository.All;
            //ExaminationRoomID: default = 0;0   All
            int ExaminationRoomID = Utils.GetInt(SearchInfo, "ExaminationRoomID");
            //ExaminationID: default =0;0   All
            int ExaminationID = Utils.GetInt(SearchInfo, "ExaminationID");
            //ExaminationSubjectID: default = 0; 0   All
            int ExaminationSubjectID = Utils.GetInt(SearchInfo, "ExaminationSubjectID");
            // List ExamSubjectID  - default count = 0 -> Get All
            List<int> LstExaminationSubjectID = Utils.GetIntList(SearchInfo, "LstExaminationSubjectID");
            //SchoolID: default =0; 0 All
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            //AcademicYearID: default =0; 0 All
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            //EducationLevelID: default =0; 0 All
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            //AppliedLevel: default =0; 0 All; tìm kiếm theo EducationLevel(EducationLevelID).Grade
            int AppliedLevel = Utils.GetByte(SearchInfo, "AppliedLevel");
            //RoomTitle: default = “”; “”   All
            string RoomTitle = Utils.GetString(SearchInfo, "RoomTitle");
            //NumberOfSeat: default = -1;-1   All
            int NumberOfSeat = Utils.GetInt(SearchInfo, "NumberOfSeat", -1);
            //Description: default = “”; “”   All
            string Description = Utils.GetString(SearchInfo, "Description");

            if (ExaminationRoomID != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.ExaminationRoomID == ExaminationRoomID);
            }

            if (ExaminationID != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.ExaminationID == ExaminationID);
            }

            if (ExaminationSubjectID != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.ExaminationSubjectID == ExaminationSubjectID);
            }

            // Lay theo List 
            if (LstExaminationSubjectID.Count > 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => LstExaminationSubjectID.Contains(o.ExaminationSubjectID));
            }

            if (SchoolID != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.Examination.SchoolID == SchoolID);
            }

            if (AcademicYearID != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.Examination.AcademicYearID == AcademicYearID);
            }

            if (EducationLevelID != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.EducationLevelID == EducationLevelID);
            }

            if (AppliedLevel != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.Examination.AppliedLevel == AppliedLevel);
            }
            if (NumberOfSeat != -1)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.NumberOfSeat == NumberOfSeat);
            }

            if (RoomTitle.Trim().Length != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.RoomTitle.ToLower().Contains(RoomTitle.Trim().ToLower()));
            }

            if (Description.Trim().Length != 0)
            {
                lsExaminationRoom = lsExaminationRoom.Where(o => o.Description.ToLower().Contains(Description.Trim().ToLower()));
            }

            return lsExaminationRoom;

        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="Dictionary">The dictionary.</param>
        /// <returns>
        /// IQueryable{ExaminationRoom}
        /// </returns>
        /// <author>hath</author>
        /// <date>11/13/2012</date>
        public IQueryable<ExaminationRoom> SearchBySchool(int SchoolID, IDictionary<string, object> Dictionary = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                Dictionary["SchoolID"] = SchoolID;
                return Search(Dictionary);
            }


        }

        #endregion

        #region AutoAssign

        /// <summary>
        /// AutoAssign
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="ExaminationID">The examination ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ExaminationSubjectID">The examination subject ID.</param>
        /// <param name="Type">The type.</param>
        /// <param name="NumberOf">The number of.</param>
        /// <author>hath</author>
        /// <date>12/14/2012</date>
        public void AutoAssign(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, int ExaminationSubjectID, int Type, int NumberOf)
        {
            Examination Exam = ExaminationBusiness.Find(ExaminationID);
            // SchoolID: PK(SchoolProfile)
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolID", true);
            //ExaminationID: PK(Examination)
            ExaminationBusiness.CheckAvailable(ExaminationID, "ExaminationRoom_Label_ExaminationID");
            //SchoolID, ExaminationID: not compatible(Examination)
            IDictionary<string, object> SearchInfo1 = new Dictionary<string, object>();
            SearchInfo1["SchoolID"] = SchoolID;
            SearchInfo1["ExaminationID"] = ExaminationID;
            bool compatible1 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo1);
            if (!compatible1)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AppliedLevel(Grade), EducationLevelID: not compatible(EducationLevel)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["Grade"] = Exam.AppliedLevel;
            SearchInfo2["EducationLevelID"] = EducationLevelID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Type: in (1,2,3)
            Utils.ValidateRange((double)Type, 1, 3, "ExaminationRoom_Label_NumberOfSeat");
            //- Nếu Examination(ExaminationID).UsingSeparateList = 1:
            if (Exam.UsingSeparateList == 1)
            {
                //   + ExaminationSubjectID: PK(ExaminationSubject)
                ExaminationSubjectBusiness.CheckAvailable(ExaminationSubjectID, "ExaminationRoom_Label_ExaminationSubjectID");

                //   + ExaminationSubjectID, ExaminationID: not compatible(ExaminationSubject)
                IDictionary<string, object> SearchInfo3 = new Dictionary<string, object>();
                SearchInfo3["ExaminationID"] = ExaminationID;
                SearchInfo3["ExaminationSubjectID"] = ExaminationSubjectID;
                bool compatible3 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "ExaminationSubject", SearchInfo3);
                if (!compatible3)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (Exam != null)
            {
                if (!AcademicYearBusiness.IsCurrentYear(Exam.AcademicYearID))
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }
            }
            //Examination(ExaminationID).CurrentStage (lấy trong CSDL) chỉ được nhận <  EXAMINATION_STAGE_MARK_COMPLETED (5)
            Utils.ValidateCompareWithNumber(Exam.CurrentStage, SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED);
            //Nếu Type = 1: 0 < NumberOf  < 1000
            if (Type == 1)
            {
                Utils.ValidateRange((double)NumberOf, 1, 999, "ExaminationRoom_Label_NumberOfSeat");
            }
            //Nếu Type = 2: 0 < NumberOf  < 100
            if (Type == 2)
            {
                Utils.ValidateRange((double)NumberOf, 1, 999, "ExaminationRoom_Label_NumberOfSeat");
            }

            //Lấy danh sách thông tin thí sinh ListCandidate :
            string sql = "UPDATE candidate set room_id = null where examination_id = " + ExaminationID
                + " AND education_level_id = " + EducationLevelID;

            if (Exam != null && Exam.UsingSeparateList == 1)
            {
                sql += " AND subject_id = " + ExaminationSubjectID;
            }            
            IDictionary<string, object> SearchCandidate = new Dictionary<string, object>();
            SearchCandidate["ExaminationID"] = ExaminationID;
            SearchCandidate["EducationLevelID"] = EducationLevelID;
            if (Exam != null && Exam.UsingSeparateList == 1)
            {
                sql += " AND subject_id = " + ExaminationSubjectID;
                SearchCandidate["ExaminationSubjectID"] = ExaminationSubjectID;
            }
            ObjectContext objectContext = ((IObjectContextAdapter)this.context).ObjectContext;
            objectContext.ExecuteStoreCommand(sql);
            context.Configuration.AutoDetectChangesEnabled = false;            

            //Nếu Type = 1,2: Xoá dữ liệu ExaminationRoomBusiness.SearchBySchool(SchoolID, Dictionnary)
            // + Dictionnary[“ExaminationID”] =  ExaminationID
            // + Dictionnary[“EducationLevelID”] =  EducationLevelID
            // + Dictionnary[“ExaminationSubjectID”] =  ExaminationSubjectID (Thêm điều kiện này khi Examination(ExaminationID).UsingSeparateList = 1)
            if (Type == 1 || Type == 2)
            {

                IQueryable<ExaminationRoom> lsExaminationRoom = this.SearchBySchool(SchoolID, SearchCandidate);
                if (lsExaminationRoom.Count() > 0)
                {
                    List<ExaminationRoom> lstExaminationRoom = lsExaminationRoom.ToList();
                    List<int> lstRoomID = lstExaminationRoom.Select(o => o.ExaminationRoomID).ToList();

                    //Xóa phân công giám thị
                    List<InvigilatorAssignment> lstInvigilatorAssign = InvigilatorAssignmentBusiness.SearchBySchool(SchoolID, SearchCandidate)
                                                                       .Where(o => lstRoomID.Contains(o.RoomID)).ToList();
                    if (lstInvigilatorAssign.Count > 0)
                    {
                        InvigilatorAssignmentBusiness.DeleteAll(lstInvigilatorAssign);
                    }
                    //Xóa học sinh vi phạm
                    List<ViolatedCandidate> lstViolatedCandidate = ViolatedCandidateBusiness.SearchBySchool(SchoolID, SearchCandidate)
                                                                    .Where(o => lstRoomID.Contains(o.RoomID.Value)).ToList();
                    if (lstViolatedCandidate.Count > 0)
                    {
                        ViolatedCandidateBusiness.DeleteAll(lstViolatedCandidate);
                    }
                    //Xóa giám thị vi phạm
                    List<ViolatedInvigilator> lstViolatedInvigilator = ViolatedInvigilatorBusiness.SearchBySchool(SchoolID, SearchCandidate)
                                                                      .Where(o => lstRoomID.Contains(o.RoomID.Value)).ToList();

                    if (lstViolatedInvigilator.Count > 0)
                    {
                        ViolatedInvigilatorBusiness.DeleteAll(lstViolatedInvigilator);
                    }

                    //Xóa kết quả đánh phách
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ExaminationID"] = ExaminationID;
                    dic["EducationLevelID"] = EducationLevelID;

                    List<int> ListExaminationSubjectID = SearchBySchool(SchoolID, dic).Select(o => o.ExaminationSubjectID).ToList();

                    List<DetachableHeadBag> lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(SchoolID, dic).Where(o => ListExaminationSubjectID.Contains(o.ExaminationSubjectID)).ToList();

                    if (lstDetachableHeadBag.Count > 0)
                    {
                        List<int> lstDetachableHeadBagID = lstDetachableHeadBag.Select(o => o.DetachableHeadBagID).ToList();
                        List<DetachableHeadMapping> lstDetachableHeadMapping = DetachableHeadMappingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                                               .Where(o => lstDetachableHeadBagID.Contains(o.DetachableHeadBagID)).ToList();

                        if (lstDetachableHeadMapping.Count > 0)
                        {
                            DetachableHeadMappingBusiness.DeleteAll(lstDetachableHeadMapping);
                        }
                        DetachableHeadBagBusiness.DeleteAll(lstDetachableHeadBag);
                    }

                    this.DeleteAll(lstExaminationRoom);
                    this.Save();
                    /*foreach (ExaminationRoom er in lstExaminationRoom)
                    {
                        CheckAvailable(er.ExaminationRoomID, "ExaminationRoom_Label_ExaminationRoomID");
                        //CheckConstraints(GlobalConstants.EXAM_SCHEMA, "ExaminationRoom", er.ExaminationRoomID, "ExaminationRoom_Label_ExaminationRoomID");
                        this.Delete(er.ExaminationRoomID);
                        
                    }*/
                }

            }



            //Các bước ở lập trình:
            //- Tạo danh sách môn thi ListExaminationSubject
            //    + Nếu  Examination(ExaminationID).UsingSeparateList = 1: ListExaminationSubject chứa phần tử ExaminationSubjectID
            //    + Nếu  Examination(ExaminationID).UsingSeparateList = 0: ListExaminationSubject = select ExaminationSubjectID from  ExaminationSubjectBusiness.SearchBySchool(SchoolID, Dictionary)
            //    Dictionnary[“ExaminationID”] =  ExaminationID
            //    Dictionnary[“EducationLevelID”] =  EducationLevelID
            List<int> ListExaminationSubject = new List<int>();
            IDictionary<string, object> SearchExamSubject = new Dictionary<string, object>();
            SearchExamSubject["ExaminationID"] = ExaminationID;
            SearchExamSubject["EducationLevelID"] = EducationLevelID;
            if (Exam.UsingSeparateList == 1)
            {

                ListExaminationSubject.Add(ExaminationSubjectID);

            }
            if (Exam.UsingSeparateList == 0)
            {
                List<int> lsExaminationSubjectID = ExaminationSubjectBusiness.SearchBySchool(SchoolID, SearchExamSubject)
                    .Select(o => o.ExaminationSubjectID).Distinct().ToList();
                if (lsExaminationSubjectID.Count() > 0)
                {
                    ListExaminationSubject.AddRange(lsExaminationSubjectID);
                }
            }
            // Lay tat cac danh sach thi sinh cua ky thi
            IQueryable<Candidate> qCandidate = CandidateBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"ExaminationID", ExaminationID},
                {"EducationLevelID", EducationLevelID}
            });
            var lstCanExam = (from c in qCandidate
                              join pp in PupilProfileBusiness.All on c.PupilID equals pp.PupilProfileID
                              select new { Candidate = c, FullName = pp.FullName, EthnicCode = pp.Ethnic!= null ? pp.Ethnic.EthnicCode : null })
                              .ToList();
            List<ExaminationRoom> listExamRoom = this.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"ExaminationID", ExaminationID},
                {"EducationLevelID", EducationLevelID}
            }).ToList();
            foreach (int item in ListExaminationSubject)
            {
                List<Candidate> lstCan = lstCanExam
                    .Where(o => o.Candidate.SubjectID == item)
                    .OrderBy(o => o.Candidate.NamedListNumber)
                    .ThenBy(o=>o.Candidate.NamedListCode)
                    .ThenBy(o => o.FullName.getOrderingName(o.EthnicCode))
                    .Select(o => o.Candidate)
                    .ToList();
                int NumberOfRoom = 0;

                //Lưu ý: 
                //•	Cách đặt tên phòng thi: Phòng thi số [Số thứ tự phòng thi được tạo].
                //•	Số chỗ ngồi: mặc định là số thí sinh lớn nhất được chia/phòng thi.
                //Số thí sinh của phòng: là số thí sinh được xếp vào phòng.
                if (Type == 1)
                {
                    //   + Lấy danh sách phòng thi ListRoom:
                    //    Nếu Type = 1, tính số phòng tối thiểu để có thể chia ListCD.Count() vào mà mỗi phòng tối đa là NumberOf thí sinh.
                    //=> NumberOfRoom = Match.Ceil(ListCD.Count() / NumberOf). Tạo tưng ứng các phòng có số lượng như trên, lấy NumberOfSeat = NumberOf
                    Utils.ValidateRange((double)NumberOf, 0, 1000, "ExaminationRoom_Label_NumberOfSeat");
                    NumberOfRoom = (lstCan.Count % NumberOf == 0) ? lstCan.Count / NumberOf : lstCan.Count / NumberOf + 1;
                    int rowCommit = 0;
                    for (int i = 0; i < NumberOfRoom; i++)
                    {
                        ExaminationRoom ExamRoom1 = new ExaminationRoom();
                        ExamRoom1.RoomTitle = SystemParamsInFile.EXAM_ROOM_NAME + (i + 1).ToString();
                        ExamRoom1.NumberOfSeat = NumberOf;
                        ExamRoom1.ExaminationID = ExaminationID;
                        ExamRoom1.ExaminationSubjectID = item;
                        ExamRoom1.EducationLevelID = EducationLevelID;
                        //this.Insert(ExamRoom1);
                        for (int j = 0; j < NumberOf; j++)
                        {
                            if (j + i * NumberOf >= lstCan.Count)
                            {
                                break;
                            }
                            lstCan[j + i * NumberOf].ExaminationRoom = ExamRoom1;
                            CandidateBusiness.Update(lstCan[j + i * NumberOf]);
                        }
                        rowCommit++;
                        if (rowCommit == 200)
                        {
                            this.Save();
                            rowCommit = 0;
                        }
                        listExamRoom.Add(ExamRoom1);
                    }
                    //this.Save();

                    //Xep thi sinh vào phòng
                }
                if (Type == 2)
                {
                    Utils.ValidateRange((double)NumberOf, 0, 100, "ExaminationRoom_Label_NumberOfSeat");
                    //    Nếu Type = 2, 
                    //tính số lượng chỗ cho phòng NumberOfSeat = = Match.Ceil(ListCD.Count() / NumberOf). Tạo tưng ứng các phòng.
                    for (int i = 0; i < NumberOf; i++)
                    {
                        ExaminationRoom ExamRoom2 = new ExaminationRoom();
                        ExamRoom2.RoomTitle = SystemParamsInFile.EXAM_ROOM_NAME + (i + 1).ToString();
                        ExamRoom2.NumberOfSeat = (lstCan.Count % NumberOf == 0) ? lstCan.Count / NumberOf : lstCan.Count / NumberOf + 1; //(int)Math.Ceiling((double)lstCan.Count / (double)NumberOf);
                        ExamRoom2.ExaminationID = ExaminationID;
                        ExamRoom2.ExaminationSubjectID = item;
                        ExamRoom2.EducationLevelID = EducationLevelID;
                        //this.Insert(ExamRoom2);
                        listExamRoom.Add(ExamRoom2);
                    }
                    //this.Save();
                }
                //Thuat toan chia phong voi trương hop 2 và 3 giống nhau.
                if (Type == 2 || Type == 3)
                {
                    /*IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                    Dictionary["ExaminationID"] = ExaminationID;
                    Dictionary["EducationLevelID"] = EducationLevelID;
                    Dictionary["ExaminationSubjectID"] = item;
                    IQueryable<ExaminationRoom> LsER = this.SearchBySchool(SchoolID, Dictionary);*/

                    List<ExaminationRoom> lstER = listExamRoom.Where(o => o.ExaminationSubjectID == item).ToList();
                    int roomIndex = 0;
                    int canIndex = 0;

                    int sumPupil = lstCan.Count;
                    int sumSeat = lstER.Sum(u => u.NumberOfSeat).Value;
                    int pupilleft = sumPupil;
                    int seatleft = sumSeat;
                    int pupilToRoom = 0;
                    int sodu = 0;
                    int val = 0;
                    int rowCommit = 0;
                    while (canIndex < sumPupil && roomIndex < lstER.Count)
                    {
                        int cnt = 0;
                        val = (pupilleft * lstER[roomIndex].NumberOfSeat.Value) / seatleft;
                        sodu = (pupilleft * lstER[roomIndex].NumberOfSeat.Value) % seatleft;
                        pupilToRoom = (sodu == 0) ? val : val + 1;
                        while ((cnt < pupilToRoom && canIndex < sumPupil) && cnt < lstER[roomIndex].NumberOfSeat)
                        {
                            //lstCan[canIndex].RoomID = lstER[roomIndex].ExaminationRoomID;
                            lstCan[canIndex].ExaminationRoom = lstER[roomIndex];
                            CandidateBusiness.Update(lstCan[canIndex]);
                            rowCommit++;
                            if (rowCommit == 200)
                            {
                                this.Save();
                                rowCommit = 0;
                            }
                            cnt++;
                            canIndex++;
                        }
                        pupilleft -= cnt;
                        seatleft -= lstER[roomIndex].NumberOfSeat.Value;
                        roomIndex++;
                    }
                }

            }
            context.Configuration.AutoDetectChangesEnabled = true;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="ExaminationRoom">The examination room.</param>
        /// <author>hath</author>
        /// <date>11/13/2012</date>
        public void Insert(int SchoolID, int AppliedLevel, ExaminationRoom ExaminationRoom)
        {
            Validate(SchoolID, AppliedLevel, ExaminationRoom);


            //Nếu Examination(ExaminationID).UsingSeparateList = 1:
            //  + Thực hiện insert vào bảng ExaminationRoom

            Examination Exam = ExaminationBusiness.Find(ExaminationRoom.ExaminationID);


            if (Exam.UsingSeparateList == 1)
            {
                base.Insert(ExaminationRoom);
            }
            if (Exam.UsingSeparateList == 0)
            {
                //Nếu Examination(ExaminationID).UsingSeparateList = 0:
                //  + Lấy danh sách môn thi: ExaminationSubjectBusiness.SearchBySchool(SchoolID, Dictionary) ListSubject
                //       - Dictionary[“ExaminationID”] = ExaminationID
                //       - Dictionary[“EducationLevelID”] = EducationLevelID
                //  + Với mỗi ExaminationSubjectID ListSubject, tạo 1 đối tượng ExaminationRoom lấy các thông tin từ entity sang,
                //riêng ExaminationSubjectID  = ExaminationSubjectID ở trên rồi thực hiện  insert vào bảng ExaminationRoom 
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["ExaminationID"] = ExaminationRoom.ExaminationID;
                Dictionary["EducationLevelID"] = ExaminationRoom.EducationLevelID;
                IQueryable<ExaminationSubject> lsExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, Dictionary);
                if (lsExaminationSubject.Count() > 0)
                {
                    List<ExaminationSubject> lstExaminationSubject = lsExaminationSubject.ToList();
                    foreach (ExaminationSubject es in lstExaminationSubject)
                    {
                        ExaminationRoom er = new ExaminationRoom();
                        er.ExaminationSubjectID = es.ExaminationSubjectID;
                        er.Description = ExaminationRoom.Description;
                        er.EducationLevelID = ExaminationRoom.EducationLevelID;
                        er.ExaminationID = ExaminationRoom.ExaminationID;
                        er.NumberOfSeat = ExaminationRoom.NumberOfSeat;
                        er.RoomTitle = ExaminationRoom.RoomTitle;
                        base.Insert(er);
                    }
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="ExaminationRoom">The examination room.</param>
        /// <author>hath</author>
        /// <date>11/13/2012</date>
        public void Update(int SchoolID, int AppliedLevel, ExaminationRoom ExaminationRoom)
        {

            //ExaminationRoomID: PK(ExaminationRoom)
            CheckAvailable(ExaminationRoom.ExaminationRoomID, "ExaminationRoom_Label_ExaminationRoomID");
            //Phòng thi đã hết chỗ - Kiểm tra số lượng bản ghi trong Candidate  c với c.RoomID = ExaminationRoomID với NumberOfSeat
            string roomTitle = ExaminationRoomBusiness.Find(ExaminationRoom.ExaminationRoomID).RoomTitle;
            if (ExaminationRoom.Candidates.Count() > ExaminationRoom.NumberOfSeat)
            {
                throw new BusinessException("Common_Validate_NumberOfSeat");
            }




            //Nếu Examination(ExaminationID).UsingSeparateList = 1:
            //  + Thực hiện update vào bảng ExaminationRoom
            //Nếu Examination(ExaminationID).UsingSeparateList = 0:
            //  + Lấy danh sách phòng thi: ExaminationRoomBusiness.SearchBySchool(SchoolID, Dictionary) ListRoom
            //       - Dictionary[“ExaminationID”] = ExaminationID
            //       - Dictionary[“EducationLevelID”] = EducationLevelID
            //  + Với mỗi ExaminationRoom trong  ListRoom cập nhất lại các thông tin:
            //       - RoomTitle =  Entity.RoomTitle
            //       - NumberOfSeat = Entity.NumberOfSeat
            //       - Description = Entity.Description
            //     Rồi thực hiện update ExaminationRoom
            Validate(SchoolID, AppliedLevel, ExaminationRoom);
            Examination Exam = ExaminationBusiness.Find(ExaminationRoom.ExaminationID);
            if (Exam.UsingSeparateList == 1)
            {
                base.Update(ExaminationRoom);
            }
            if (Exam.UsingSeparateList == 0)
            {
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["ExaminationID"] = ExaminationRoom.ExaminationID;
                Dictionary["EducationLevelID"] = ExaminationRoom.EducationLevelID;
                Dictionary["RoomTitle"] = roomTitle;
                IQueryable<ExaminationRoom> lsExaminationRoom = this.SearchBySchool(SchoolID, Dictionary);
                if (lsExaminationRoom.Count() > 0)
                {
                    List<ExaminationRoom> lstExaminationRoom = lsExaminationRoom.ToList();
                    foreach (ExaminationRoom er in lstExaminationRoom)
                    {
                        er.RoomTitle = ExaminationRoom.RoomTitle;
                        er.NumberOfSeat = ExaminationRoom.NumberOfSeat;
                        er.Description = ExaminationRoom.Description;
                        base.Update(er);
                    }

                }

            }
        }



        #endregion

        #region Delete
        public void Delete(int SchoolID, int AppliedLevel, int ExaminationRoomID)
        {
            //ExaminationRoomID: PK(ExaminationRoom)
            this.CheckAvailable(ExaminationRoomID, "ExaminationRoom_Label_ExaminationRoomID");
            //this.CheckConstraints(GlobalConstants.EXAM_SCHEMA, "ExaminationRoom", ExaminationRoomID, "ExaminationRoom_Label_ExaminationRoomID");
            //Tìm kiếm các phòng thi cần xoá ListExaminationRoomID = GetListRoomHasSameEducationLevel(ExaminationRoomID)
            List<int> ListExaminationRoomID = GetListRoomHasSameEducationLevel(ExaminationRoomID);
            //Xóa phân công giám thị
            List<InvigilatorAssignment> lstInvigilatorAssign = InvigilatorAssignmentBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                               .Where(o => ListExaminationRoomID.Contains(o.RoomID)).ToList();
            if (lstInvigilatorAssign.Count > 0)
            {
                InvigilatorAssignmentBusiness.DeleteAll(lstInvigilatorAssign);
            }
            //Xóa học sinh vi phạm
            List<ViolatedCandidate> lstViolatedCandidate = ViolatedCandidateBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                            .Where(o => ListExaminationRoomID.Contains(o.RoomID.Value)).ToList();
            if (lstViolatedCandidate.Count > 0)
            {
                ViolatedCandidateBusiness.DeleteAll(lstViolatedCandidate);
            }
            //Xóa giám thị vi phạm
            List<ViolatedInvigilator> lstViolatedInvigilator = ViolatedInvigilatorBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                              .Where(o => ListExaminationRoomID.Contains(o.RoomID.Value)).ToList();

            if (lstViolatedInvigilator.Count > 0)
            {
                ViolatedInvigilatorBusiness.DeleteAll(lstViolatedInvigilator);
            }

            //Xóa kết quả đánh phách
            ExaminationRoom examRoom = ExaminationRoomBusiness.Find(ExaminationRoomID);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationID"] = examRoom.ExaminationID;
            dic["EducationLevelID"] = examRoom.EducationLevelID;

            List<int> ListExaminationSubject = SearchBySchool(SchoolID, dic).Select(o => o.ExaminationSubjectID).ToList();

            List<DetachableHeadBag> lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(SchoolID, dic).Where(o => ListExaminationSubject.Contains(o.ExaminationSubjectID)).ToList();

            if (lstDetachableHeadBag.Count > 0)
            {
                List<int> lstDetachableHeadBagID = lstDetachableHeadBag.Select(o => o.DetachableHeadBagID).ToList();
                List<DetachableHeadMapping> lstDetachableHeadMapping = DetachableHeadMappingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                                       .Where(o => lstDetachableHeadBagID.Contains(o.DetachableHeadBagID)).ToList();

                if (lstDetachableHeadMapping.Count > 0)
                {
                    DetachableHeadMappingBusiness.DeleteAll(lstDetachableHeadMapping);
                }
                DetachableHeadBagBusiness.DeleteAll(lstDetachableHeadBag);
            }

            //Thực hiện update RoomID = NULL cho các bản ghi trong Candidate có RoomID trong ListExaminationRoomID
            foreach (int item in ListExaminationRoomID)
            {
                IQueryable<Candidate> lsca = CandidateBusiness.All.Where(o => o.RoomID == item);
                if (lsca.Count() > 0)
                {
                    List<Candidate> lstCandidate = lsca.ToList();
                    foreach (Candidate ca in lstCandidate)
                    {
                        ca.RoomID = null;
                        CandidateBusiness.Update(ca);
                        //CandidateBusiness.Save();
                    }
                }
                base.Delete(item);
                //base.Save();
            }
            //Thực hiện xoá các bản ghi tương ứng ListExaminationRoomID trong bảng ExaminationRoom


        }

        #endregion

        #region GetListRoomHasSameEducationLevel
        public List<int> GetListRoomHasSameEducationLevel(int examinationRoomID)
        {
            ExaminationRoom er = this.Find(examinationRoomID);
            Examination e = ExaminationBusiness.Find(er.ExaminationID);
            List<int> listExaminationRoomID = new List<int>();
            if (e.UsingSeparateList == 1)
            {
                listExaminationRoomID.Add(examinationRoomID);
            }
            else
            {
                // Tạo list ExaminationRoom
                IDictionary<string, object> SearchExaminationRoom = new Dictionary<string, object>();
                SearchExaminationRoom["ExaminationID"] = er.ExaminationID;
                SearchExaminationRoom["EducationLevelID"] = er.EducationLevelID;
                SearchExaminationRoom["RoomTitle"] = er.RoomTitle;
                IQueryable<ExaminationRoom> lstExaminationRoom = this.SearchBySchool((int)e.SchoolID, SearchExaminationRoom);
                if (lstExaminationRoom != null)
                {
                    var lstID = from lst in lstExaminationRoom
                                select lst.ExaminationRoomID;
                    var ListID = lstID.ToList();
                    if (ListID.Count > 0)
                    {
                        for (int i = 0; i < ListID.Count; i++)
                        {
                            listExaminationRoomID.Add(ListID[i]);
                        }
                    }
                }
            }
            return listExaminationRoomID;
        }
        #endregion

        //Chia đều thí sinh vào các phòng thi
        //lstCandidateID: danh sách thí sinh cần chia
        //lstExamRoom: danh sách phòng thi để chia vào
        /*lstCandidateID, lstExamRoom đều lấy theo một môn bất kì nếu kì thi có danh sách thí sinh theo từng môn giống nhau nên phải update cả theo các
        môn khác nếu kì thi có tính chất này */
        public void AverageAssign(int SchoolID, int AppliedLevel, List<int> lstCandidateID, List<ExaminationRoom> lstExamRoom)
        {

            int sumPupil = lstCandidateID.Count;
            List<int> lstExamRoomID = lstExamRoom.Select(o => o.ExaminationRoomID).ToList();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            int totalPupilInRoom = CandidateBusiness.SearchBySchool(SchoolID, dic).Where(o => lstExamRoomID.Contains(o.RoomID.Value)).Count();
            sumPupil += totalPupilInRoom;
            int totalSeat = lstExamRoom.Sum(o => o.NumberOfSeat.Value);
            if (sumPupil > totalSeat)
            {
                throw new BusinessException("CandidateRoom_Validate_SeatLeafNotEnough");
            }

            int pupilLeft = sumPupil;
            int SeatLeft = totalSeat;
            int cntPupilInRoom = 0;
            int cntPupilToRoom = 0;
            int canIndex = 0;
            int val = 0;
            int du = 0;
            foreach (ExaminationRoom examRoom in lstExamRoom)
            {
                dic["RoomID"] = examRoom.ExaminationRoomID;
                cntPupilInRoom = CandidateBusiness.SearchBySchool(SchoolID, dic).Count();
                val = (pupilLeft * examRoom.NumberOfSeat.Value) / SeatLeft;
                du = (pupilLeft * examRoom.NumberOfSeat.Value) % SeatLeft;
                cntPupilToRoom = (du == 0) ? val : val + 1;
                int index = 0;
                while (index < cntPupilToRoom - cntPupilInRoom && canIndex < lstCandidateID.Count)
                {
                    CandidateBusiness.UpdateRoom(SchoolID, AppliedLevel, lstCandidateID[canIndex], examRoom.ExaminationRoomID);
                    canIndex++;
                    index++;
                }
                pupilLeft -= cntPupilToRoom > cntPupilInRoom ? cntPupilToRoom : cntPupilInRoom;
                SeatLeft -= examRoom.NumberOfSeat.Value;
            }
        }
    }
}
