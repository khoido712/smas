/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class CandidateAbsenceBusiness
    {
        public List<Candidate> InsertAll(int SchoolID, int AppliedLevel, bool HasReason, string ReasonDetail, List<int> ListCandidateID)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            foreach (int candidateID in ListCandidateID)
            {
                CandidateBusiness.CheckAvailable(candidateID, "Candidate_Label_CandidateID");

                Candidate candidate = CandidateBusiness.Find(candidateID);
                //Kiem tra tuong thich Examination
                bool examinationCompatible = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , candidate.ExaminationID },
                                                {"SchoolID", SchoolID}
                                            }, null);
                if (!examinationCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Kiem tra tuong thich EducationLevel
                bool educationLevelCompatible = new EducationLevelRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                                , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , candidate.EducationLevelID},
                                                {"Grade", AppliedLevel }
                                            }, null);
                if (!educationLevelCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (candidate.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                    throw new BusinessException("CandidateAbsence_Validate_CurrentStage");

                //if (!AcademicYearBusiness.IsCurrentYear(candidate.Examination.AcademicYearID))
                //    throw new BusinessException("Common_IsCurrentYear_Err");
            }
            if (HasReason)
            {
                Utils.ValidateMaxLength(ReasonDetail, 160, "Common_Validate_MaxLength");
            }

            #endregion

            List<Candidate> lstCandidate = new List<Candidate>();
            foreach (int candidateID in ListCandidateID)
            {
                Candidate candidate = CandidateBusiness.Find(candidateID);

                candidate.HasReason = HasReason;
                candidate.ReasonDetail = HasReason ? ReasonDetail : string.Empty;

                CandidateBusiness.Update(candidate);
                lstCandidate.Add(candidate);
            }
            return lstCandidate;
        }

        public Candidate Update(int CandidateID, int SchoolID, int AppliedLevel, bool HasReason, string ReasonDetail)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            CandidateBusiness.CheckAvailable(CandidateID, "Candidate_Label_CandidateID");

            Candidate candidate = CandidateBusiness.Find(CandidateID);
            //Kiem tra tuong thich Examination
            bool examinationCompatible = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , candidate.ExaminationID },
                                                {"SchoolID", SchoolID}
                                            }, null);
            if (!examinationCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra tuong thich EducationLevel
            bool educationLevelCompatible = new EducationLevelRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                            , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , candidate.EducationLevelID},
                                                {"Grade", AppliedLevel }
                                            }, null);
            if (!educationLevelCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (candidate.Examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                throw new BusinessException("Examination_Validate_CurrentStage");

            //if (!AcademicYearBusiness.IsCurrentYear(candidate.Examination.AcademicYearID))
            //    throw new BusinessException("Common_IsCurrentYear_Err");

            if (HasReason)
            {
                Utils.ValidateMaxLength(ReasonDetail, 160, "Common_Validate_MaxLength");
            }

            if (candidate.HasReason == null)
                throw new BusinessException("CandidateAbsence_Validate_NotAbsence");

            #endregion

            candidate.HasReason = HasReason;
            candidate.ReasonDetail = HasReason ? ReasonDetail : string.Empty;

            CandidateBusiness.Update(candidate);
            return candidate;
        }

        public void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListCandidateID)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");
            foreach (int CandidateID in ListCandidateID)
            {
                CandidateBusiness.CheckAvailable(CandidateID, "");

                Candidate candidate = CandidateBusiness.Find(CandidateID);
                bool examinationCompatible = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , candidate.ExaminationID },
                                                {"SchoolID", SchoolID}
                                            }, null);
                if (!examinationCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Kiem tra tuong thich EducationLevel
                bool educationLevelCompatible = new EducationLevelRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                                , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , candidate.EducationLevelID },
                                                {"Grade", AppliedLevel }
                                            }, null);
                if (!educationLevelCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");


                if (candidate.Examination.CurrentStage != SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                    throw new BusinessException("CandidateAbsence_Validate_CurrentStage");

                //if (!AcademicYearBusiness.IsCurrentYear(candidate.Examination.AcademicYearID))
                //    throw new BusinessException("Common_IsCurrentYear_Err");
            }
            #endregion

            foreach (int CandidateID in ListCandidateID)
            {
                Candidate candidate = CandidateBusiness.Find(CandidateID);
                candidate.HasReason = null;
                candidate.ReasonDetail = string.Empty;
                CandidateBusiness.Update(candidate);
            }
        }

        public IQueryable<Candidate> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0 || dic == null) return null;
            dic.Add("IsAbsence", true);
            return CandidateBusiness.SearchBySchool(SchoolID, dic);
        }

        public Stream CreateCandidateAbsenceReport(Dictionary<string, object> dic, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THI_THISINHVANGTHI);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? educationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            int? examinationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");
            List<Candidate> listCandidate = SearchBySchool(schoolID, dic).ToList();

            IVTWorksheet templateSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet, "J15");

            #region tao khung
            IVTRange topRow = templateSheet.GetRange("A17", "J17");
            IVTRange midRow = templateSheet.GetRange("A18", "J18");
            IVTRange botRow = templateSheet.GetRange("A19", "J19");
            

            for (int i = 0; i < listCandidate.Count - 5; i++)
            {
                if (i % 5 == 0) sheet.CopyPaste(topRow, i + 16, 1, true);
                else if (i % 5 == 4) sheet.CopyPaste(botRow, i + 16, 1, true);
                else sheet.CopyPaste(midRow, i + 16, 1, true);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examination exam = ExaminationBusiness.Find(examinationID);
            AcademicYear acaYear = AcademicYearBusiness.Find(academicYearID);

            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["ExaminationName"] = examinationID > 0 && exam != null ? exam.Title : "Tất cả";
            dicVarable["AcademicYear"] = acaYear != null ? (acaYear.Year + " - " + (acaYear.Year + 1)) : "Tất cả";

            int index = 1;
            List<object> listData = new List<object>();
            listCandidate.ForEach(u =>
            {
                listData.Add(new
                {
                    Index = index++,
                    NamedListCode = u.NamedListCode,
                    FullName = u.PupilProfile.FullName,
                    BirthDate = u.PupilProfile.BirthDate.ToString("dd/MM/yyyy"),
                    Genre = u.PupilProfile.Genre,
                    GenreTitle = u.PupilProfile.Genre == SystemParamsInFile.GENRE_FEMALE ? "Nữ" : "Nam",
                    EducationLevelResolution = u.EducationLevel.Resolution,
                    SubjectName = u.ExaminationSubject.SubjectCat.SubjectName,
                    RoomTitle = u.ExaminationRoom.RoomTitle,
                    HasReason = u.HasReason.HasValue && u.HasReason.Value ? "Có" : "Không",
                    ReasonDetail = u.ReasonDetail
                });
            });
            dicVarable["Rows"] = listData;
            if (listData.Count == 0)
            {
                sheet.DeleteRow(11);
            }
            sheet.FillVariableValue(dicVarable);
            templateSheet.Delete();
            #endregion

            #region reportName

            string examTitle = exam != null ? ReportUtils.StripVNSign(exam.Title) : "TatCa";
            string educationLevel = educationLevelID.HasValue && educationLevelID.Value > 0 ? ReportUtils.StripVNSign(EducationLevelBusiness.Find(educationLevelID.Value).Resolution) : "TatCa";
            string subjectName = examinationSubjectID.HasValue && examinationSubjectID.Value > 0 ? ReportUtils.StripVNSign(ExaminationSubjectBusiness.Find(examinationSubjectID.Value).SubjectCat.SubjectName) : "TatCa";

            reportName = reportName.Replace("Examination", examTitle)
                            .Replace("EducationLevel", educationLevel)
                            .Replace("Subject", subjectName);

            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }
    }
}
