﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    /// <summary>
    /// lương nhân viên
    /// <author>dungnt</author>
    /// <date>06/09/2012</date>
    /// </summary>
    public partial class EmployeeSalaryBusiness
    {

        #region private member variable
        private const int SALARYRESOLUTION_MAXLENGTH = 50;//Quyết định xếp lương phải nhỏ hơn 50 ký tự
        private const int DESCRIPTION_MAXLENGTH = 500;  //Ghi chú không được nhập quá 500 ký tự  
        private const int APPLIEDDATE_SUBTRACT_BIRTHDATE = 17; //Ngày xếp lương phải lớn hơn ngày sinh + 17 
        private const int SALARYAMOUNT_RANGE_MAX = 100;
        private const int SALARYAMOUNT_RANGE_MIN = 0;
        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới lương nhân viên
        /// <author>dungnt</author>
        /// <date>06/09/2012</date>
        /// </summary>
        /// <param name="insertEmployeeSalary">Đối tượng Insert</param>
        /// <returns></returns>
        public override EmployeeSalary Insert(EmployeeSalary insertEmployeeSalary)
        {
            Validate(insertEmployeeSalary);
            //Thực hiện Insert dữ liệu vào bảng EmployeeSalary
            return base.Insert(insertEmployeeSalary);

        }
        #endregion

        #region Update
        /// <summary>
        /// Cập nhật lương nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="updateEmployeeSalary">Đối tượng Update</param>
        /// <returns></returns>
        public override EmployeeSalary Update(EmployeeSalary updateEmployeeSalary)
        {
            //Kiểm tra EmployeeSalaryID có tồn tại hay không?
             CheckAvailable(updateEmployeeSalary.EmployeeSalaryID, "EmployeeSalary_Label_EmployeeSalaryID");

          // EmployeeSalaryID, SchoolID: not compatible(EmployeeSalary)
             {
                 bool EmployeeSalaryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeSalary",
                   new Dictionary<string, object>()
                {
                    {"SchoolID",updateEmployeeSalary.SchoolID},
                    {"EmployeeSalaryID",updateEmployeeSalary.EmployeeSalaryID}
                }, null);
                 if (!EmployeeSalaryCompatible)
                 {
                     throw new BusinessException("Common_Validate_NotCompatible");
                 }
             }

             //EmployeeSalaryID, SupervisingDeptID: not compatible(EmployeeSalary
             if (updateEmployeeSalary.SupervisingDeptID.HasValue)
             {
                 bool EmployeeSalaryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeSalary",
                    new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",updateEmployeeSalary.SupervisingDeptID},
                    {"EmployeeSalaryID",updateEmployeeSalary.EmployeeSalaryID}
                }, null);
                 if (!EmployeeSalaryCompatible)
                 {
                     throw new BusinessException("Common_Validate_NotCompatible");
                 }
             }
             Validate(updateEmployeeSalary);
            return base.Update(updateEmployeeSalary);

        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa lương nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="id">ID lương nhân viên</param>
        public void Delete(int SchoolOrSupervisingDeptID, int EmployeeSalaryID)
        {
           // Bản ghi này đã bị xóa hoặc không tồn tại trong cơ sở dữ liệu. Kiểm tra EmployeeSalaryID != NULL
            CheckAvailable(EmployeeSalaryID, "EmployeeSalary_Label_EmployeeSalaryID");
            EmployeeSalary temp = EmployeeSalaryRepository.Find(EmployeeSalaryID);

            Employee checkEmployee = EmployeeRepository.All.Where(o => (o.EmployeeID == temp.EmployeeID)).FirstOrDefault();
            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SCHOOL_TEACHER: 
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {

                //EmployeeID, SchoolOrSupervisingDeptID: not compatible(Employee)
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolOrSupervisingDeptID},
                    {"EmployeeID",checkEmployee.EmployeeID}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //EmployeeSalaryID, SchoolOrSupervisingDeptID(SchoolID): not compatible(EmployeeSalary)
                bool EmployeeSalaryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeSalary",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolOrSupervisingDeptID},
                    {"EmployeeSalaryID",EmployeeSalaryID}
                }, null);
                if (!EmployeeSalaryCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }



            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {

                //SupervisingDeptID, EmployeeID: not compatible(Employee)
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SchoolOrSupervisingDeptID},
                    {"EmployeeID",checkEmployee.EmployeeID}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //+ + EmployeeSalaryID, SchoolOrSupervisingDeptID(SupervisingDeptID): not compatible(EmployeeSalary)
                bool EmployeeSalaryCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeSalary",
                new Dictionary<string, object>()
                {
                    {"SupervisingDeptID",SchoolOrSupervisingDeptID},
                    {"EmployeeSalaryID",EmployeeSalaryID}
                }, null);
                if (!EmployeeSalaryCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            base.Delete(EmployeeSalaryID,false);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm lương nhân viên
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="dic">danh sách tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<EmployeeSalary> Search(IDictionary<string, object> dic)
        {
            //-	EmployeeID: default  = 0; 0 => Tìm kiếm All
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");

            //-	EmployeeScaleID: defaut = 0; 0 => Tìm kiếm All 
            int EmployeeScaleID = Utils.GetInt(dic, "EmployeeScaleID");

            //-	SalaryLevelID: default = 0; 0 => Tìm kiếm All 
            int SalaryLevelID = Utils.GetInt(dic, "SalaryLevelID");

            //-	AppliedDate: default = “ ”; “ ” => Tìm kiếm All
            DateTime? AppliedDate = Utils.GetDateTime(dic, "AppliedDate");

            //-	SalaryResolution: default = “ ” => Tìm kiếm All
            string SalaryResolution = Utils.GetString(dic, "SalaryResolution");

            //-	SchoolID: default = 0 ; 0 => Tìm kiếm All 
            int SchoolID = Utils.GetInt(dic, "SchoolID");

            //-	SupervisingDeptID = 0; 0 => Tìm kiếm All 
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");

            IQueryable<EmployeeSalary> lsEmployeeSalary = this.EmployeeSalaryRepository.All;

            if (EmployeeID != 0)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o=>(o.EmployeeID==EmployeeID));
            }

            if (EmployeeScaleID != 0)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o => (o.EmployeeScaleID == EmployeeScaleID));
            }

            if (SalaryLevelID != 0)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o => (o.SalaryLevelID == SalaryLevelID));
            }

            if (AppliedDate.HasValue)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o => (o.AppliedDate == AppliedDate));
            }

            if (SalaryResolution.Trim().Length!=0)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o => (o.SalaryResolution.ToUpper().Contains(SalaryResolution.ToUpper())));
            }

            if (SchoolID != 0)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o => (o.SchoolID == SchoolID));
            }

            if (SupervisingDeptID != 0)
            {
                lsEmployeeSalary = lsEmployeeSalary.Where(o => (o.SupervisingDeptID == SupervisingDeptID));
            }

            return lsEmployeeSalary;

        }

        /// <summary>
        /// Tìm kiếm thông tin lương cán bộ
        /// <Author>dungnt</Author>
        /// <DateTime>06/09/2012</DateTime>
        /// </summary>
        /// <param name="dic">danh sách tham số</param>
        /// <param name="EmployeeID">Id nhân viên</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<EmployeeSalary> SearchByEmployee(int EmployeeID,IDictionary<string, object> dic=null)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            //Nếu  EmployeeID = 0 -> trả về NULL
            if (EmployeeID == 0)
            {
                return null;
            }
            else
            {
                //Nếu EmployeeID <> 0 -> trả về Search (SearchInfo) với bổ sung  SearchInfo [“EmployeeID”] = EmployeeID
                dic["EmployeeID"] = EmployeeID;
                return Search(dic);
            }
        }

        public IQueryable<EmployeeSalary> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            //Nếu  SchoolID = 0 -> trả về NULL
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                //Nếu EmployeeID <> 0 -> trả về Search (SearchInfo) với bổ sung  SearchInfo [“EmployeeID”] = EmployeeID
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }
        public IQueryable<EmployeeSalary> SearchBySupervisingDept(int SupervisingDeptID, IDictionary<string, object> dic = null)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            //Nếu  EmployeeID = 0 -> trả về NULL
            if (SupervisingDeptID == 0)
            {
                return null;
            }
            else
            {
                //Nếu EmployeeID <> 0 -> trả về Search (SearchInfo) với bổ sung  SearchInfo [“EmployeeID”] = EmployeeID
                dic["SupervisingDeptID"] = SupervisingDeptID;
                return Search(dic);
            }
        }
        #endregion

        #region validate
        public void Validate(EmployeeSalary insertEmployeeSalary)
        {
            if (insertEmployeeSalary.SalaryAmount > 100)
            {
                //Lương thực tế phải có giá trị từ 0 đến 100.
                throw new BusinessException("EmployeeSalary_Validate_SalaryAmount");
            }
            //Nhân viên không tồn tại - Kiểm tra EmployeeID 
            EmployeeBusiness.CheckAvailable(insertEmployeeSalary.EmployeeID, "Employee_Label_EmployeeID");

            //Ngạch lương không tồn tại hoặc đã bị xóa – Kiểm tra EmployScaleID trong bảng EmployeeScale với isActive = 1
            EmployeeScaleBusiness.CheckAvailable(insertEmployeeSalary.EmployeeScaleID, "EmployScale_Label_EmployScaleID");

            //Bậc lương không tồn tại hoặc đã bị xóa – Kiểm tra SalaryLevelID trong bảng SalaryLevel với isActive = 1
            SalaryLevelBusiness.CheckAvailable(insertEmployeeSalary.SalaryLevelID, "SalaryLevel_Label_SalaryLevelID");


            Dictionary<string, object> dicSalary = new Dictionary<string, object>();
            if (insertEmployeeSalary.SalaryLevelID > 0)
            {
                dicSalary.Add("SalaryLevelID", insertEmployeeSalary.SalaryLevelID);
            }
            bool SalaryLevelCompatible = SalaryLevelRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "SalaryLevel", dicSalary, null);
                if (!SalaryLevelCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }


            if (insertEmployeeSalary.SalaryAmount > 0)
            {
                Utils.ValidateRange((int)insertEmployeeSalary.SalaryAmount, SALARYAMOUNT_RANGE_MIN, SALARYAMOUNT_RANGE_MAX, "Salary_Label_SalaryAmount");        
            }

            //Ghi chú không được nhập quá 500 ký tự  - Kiểm tra Description
            Utils.ValidateMaxLength(insertEmployeeSalary.Description, DESCRIPTION_MAXLENGTH, "EmployeeSalary_Label_Description");


            //Ngày xếp lương phải lớn hơn ngày sinh + 17 năm và nhỏ hơn ngày hiện tại – Kiểm tra AppliedDate
            Employee checkEmployee = EmployeeRepository.All.Where(o => (o.EmployeeID == insertEmployeeSalary.EmployeeID)).FirstOrDefault();
            if (checkEmployee.BirthDate != null)
            {
                Utils.ValidateAfterDate(insertEmployeeSalary.AppliedDate.Value, checkEmployee.BirthDate.Value, APPLIEDDATE_SUBTRACT_BIRTHDATE, "EmployeeSalary_Label_AppliedDate", "Employee_Label_BirthDate");
            }
            //Utils.ValidateAfterDate(DateTime.Now, insertEmployeeSalary.AppliedDate.Value, "Employee_Label_Date_Now", "EmployeeSalary_Label_AppliedDate");
            Utils.ValidatePastDate(insertEmployeeSalary.AppliedDate.Value, "EmployeeSalary_Label_AppliedDate");
            
            //Nếu có ngày vào nghề thì ngày xếp lương phải lớn hơn hoặc bằng ngày vào nghề - Kiểm tra AppliedDate, StartingDate (Trong bảng Employee)
            if (checkEmployee.StartingDate.HasValue)
            {
                Utils.ValidateAfterDate(insertEmployeeSalary.AppliedDate.Value, checkEmployee.StartingDate.Value, "EmployeeSalary_Label_AppliedDate", "Employee_Label_StartingDate");
            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SCHOOL_TEACHER: 
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER)
            {
                //Kiểm tra sự tồn tại của SchoolID truyền vào
                SchoolProfileBusiness.CheckAvailable(insertEmployeeSalary.SchoolID, "SchoolProfile_Label_SchoolID");

                //EmployeeID, SchoolID: not compatible(Employee)
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",insertEmployeeSalary.SchoolID},
                    {"EmployeeID",insertEmployeeSalary.EmployeeID}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //Nếu Employee(EmployeeID).EMPLOYEE_TYPE = EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF
            if (checkEmployee.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
            {
                //+ SupervisingDeptID: require
                //Phòng ban không tồn tại – Kiểm tra SupervisingDeptID
                SupervisingDeptBusiness.CheckAvailable(insertEmployeeSalary.SupervisingDeptID, "SchoolProfile_Label_SupervisingDeptID");

                //SupervisingDeptID, SchoolID: not compatible(Employee)
                bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"EmployeeID",insertEmployeeSalary.EmployeeID},
                    {"SupervisingDeptID",insertEmployeeSalary.SupervisingDeptID}
                }, null);
                if (!EmployeeCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

        }
        #endregion

        public void InsertOrUpdateSalary(IDictionary<string,object> dic,EmployeeSalary objES)
        {
            int SchoolID = Utils.GetInt(dic,"SchoolID");
            int EmployeeID = Utils.GetInt(dic,"EmployeeID");
            DateTime? AppliedDate = Utils.GetDateTime(dic,"AppliedDate");
            IQueryable<EmployeeSalary> iquery = (from es in EmployeeSalaryBusiness.All
                                                 where es.SchoolID == SchoolID
                                                 && es.EmployeeID == EmployeeID
                                                 && EntityFunctions.TruncateTime(es.AppliedDate) == EntityFunctions.TruncateTime(AppliedDate)
                                                 select es);
                                                
            if (iquery.Count() == 0)
            {
                EmployeeSalaryBusiness.Insert(objES);
            }
            else
            {
                EmployeeSalary objUpdate = iquery.FirstOrDefault();
                objUpdate.EmployeeScaleID = objES.EmployeeScaleID;
                objUpdate.SalaryLevelID = objES.SalaryLevelID;
                objUpdate.SalaryAmount = objES.SalaryAmount;
                objUpdate.Coefficient = objES.Coefficient;
                EmployeeSalaryBusiness.Update(objUpdate);
            }
            EmployeeSalaryBusiness.Save();
        }
        
    }
}