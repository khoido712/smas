﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Lỗi vi phạm
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class FaultCriteriaBusiness
    {
        #region insert
        /// <summary>
        /// Thêm mới Lỗi vi phạm
        /// <author>phuongh</author>
        /// <date>01/05/2013</date>
        /// </summary>
        /// <param name="Entity">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override FaultCriteria Insert(FaultCriteria Entity)
        {
            ValidationMetadata.ValidateObject(Entity);
            //check trùng tên
            if (this.FaultCriteriaRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "FaultCriteria", new Dictionary<string, object> {
                {"Resolution", Entity.Resolution},
                {"IsActive", true},
                {"SchoolID", Entity.SchoolID}
            }))
            {
                throw new BusinessException("Common_Validate_Duplicate", "FaultCriteria_Label_Resolution");
            }

            if (Entity.PenalizedMark < 0 || Entity.PenalizedMark > 10)
            {
                throw new BusinessException("Common_Validate_NotInRange", "FaultCriteria_Label_PenalizedMark", 0, 10);
            }

            FaultGroup fg = FaultGroupBusiness.Find(Entity.GroupID);
            if (Entity.SchoolID != fg.SchoolID)
            {
                throw new BusinessException("Common_Error_NotCompatible");
            }
            if (!string.IsNullOrWhiteSpace(Entity.Absence))
            {
                if (this.FaultCriteriaRepository.All.Where(x => x.SchoolID == Entity.SchoolID
                    && x.Absence != null && x.Absence.Trim() == Entity.Absence.Trim() && x.IsActive == true).Count() > 0)
                {
                    throw new BusinessException("FaultCriteria_Error_AlreadyHasAbsence");
                }
            }
            Entity.IsActive = true;
            return base.Insert(Entity);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Lỗi vi phạm
        /// <author>phuongh</author>
        /// <date>01/05/2013</date>
        /// </summary>
        /// <param name="Entity">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override FaultCriteria Update(FaultCriteria Entity)
        {
            this.CheckAvailable(Entity.FaultCriteriaID, "FaultCriteria_Label_FaultCriteriaID");

            //check trùng tên
            if (this.FaultCriteriaRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "FaultCriteria", new Dictionary<string, object> {
                {"Resolution", Entity.Resolution},
                {"IsActive", true},
                {"SchoolID", Entity.SchoolID}
            }, new Dictionary<string, object> {
                {"FaultCriteriaID", Entity.FaultCriteriaID}
            }))
            {
                throw new BusinessException("Common_Validate_Duplicate", "FaultCriteria_Label_Resolution");
            }

            if (Entity.PenalizedMark < 0 || Entity.PenalizedMark > 10)
            {
                throw new BusinessException("Common_Validate_NotInRange", "FaultCriteria_Label_PenalizedMark", 0, 10);
            }

            FaultGroup fg = FaultGroupBusiness.Find(Entity.GroupID);
            if (Entity.SchoolID != fg.SchoolID)
            {
                throw new BusinessException("Common_Error_NotCompatible");
            }
            FaultCriteria fc = this.Find(Entity.FaultCriteriaID);
            if (fc.SchoolID != Entity.SchoolID)
            {
                throw new BusinessException("Common_Error_NotCompatible");
            }
            if (!string.IsNullOrWhiteSpace(Entity.Absence))
            {
                if (this.FaultCriteriaRepository.All.Where(x => x.SchoolID == Entity.SchoolID
                    && x.Absence != null && x.Absence.Trim() == Entity.Absence.Trim()
                    && x.FaultCriteriaID != Entity.FaultCriteriaID && x.IsActive == true).Count() > 0)
                {
                    throw new BusinessException("FaultCriteria_Error_AlreadyHasAbsence");
                }
            }
            Entity.IsActive = true;
            return base.Update(Entity);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Lỗi vi phạm
        /// <author>phuongh1</author>
        /// <date>01/05/2013</date>
        /// </summary>
        /// <param name="FaultCriteriaID">ID Lỗi vi phạm</param>
        public void Delete(int SchoolID, int FaultCriteriaID)
        {
            //check avai
            this.CheckAvailable(FaultCriteriaID, "FaultCriteria_Label_FaultCriteriaID");
            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "FaultCriteria", FaultCriteriaID, "FaultCriteria_Label_FaultCriteriaID");

            FaultCriteria fc = this.Find(FaultCriteriaID);
            if (fc.SchoolID != SchoolID)
            {
                throw new Exception("Common_Error_NotCompatible");
            }

            base.Delete(FaultCriteriaID, true);
        }
        #endregion

        /// <summary>
        /// Tìm kiếm Lỗi vi phạm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<FaultCriteria> Search(IDictionary<string, object> dic = null)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            string resolution = Utils.GetString(dic, "Resolution");
            string description = Utils.GetString(dic, "Description");
            int FaultGroupID = Utils.GetInt(dic, "FaultGroupID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            string absence = Utils.GetString(dic, "Absence");
            IQueryable<FaultCriteria> lsFaultCriteria = this.FaultCriteriaRepository.All;

            //var query = from a in lsFaultCriteria
            //            where a.FaultGroup.SchoolID == SchoolID
            //            select a;

            if (isActive.HasValue)
            {
                lsFaultCriteria = lsFaultCriteria.Where(em => (em.IsActive == isActive));

            }

            //Hieund 04/10/2012 Bo Search theo SchoolID
            //if (SchoolID != 0)
            //{
            //    lsFaultCriteria = lsFaultCriteria.Where(em => (em.FaultGroup.SchoolID == SchoolID));
            //}

            if (resolution.Trim().Length != 0)
            {
                lsFaultCriteria = lsFaultCriteria.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (FaultGroupID != 0)
            {
                lsFaultCriteria = lsFaultCriteria.Where(em => (em.GroupID == FaultGroupID));
            }

            if (description.Trim().Length != 0)
            {
                lsFaultCriteria = lsFaultCriteria.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

            //Hieund 19/10/2010 : HieuND them tim kiem theo truong absence  
            if (!string.IsNullOrEmpty(absence))
                lsFaultCriteria = lsFaultCriteria.Where(em => em.Absence.Equals(absence.Trim()));


            if (SchoolID != 0) lsFaultCriteria = lsFaultCriteria.Where(em => em.SchoolID == SchoolID);
            return lsFaultCriteria;
        }

        public IQueryable<FaultCriteria> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (dic == null) dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
    }
}