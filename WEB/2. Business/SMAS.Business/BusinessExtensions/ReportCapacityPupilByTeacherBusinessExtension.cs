﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportCapacityPupilByTeacherBusiness : GenericBussiness<ProcessedReport>, IReportCapacityPupilByTeacherBusiness
    {
        IEmployeeHistoryStatusRepository EmployeeHistoryStatusRepository;
        ITeacherOfFacultyRepository TeacherOfFacultyRepository;
        IEmployeeSalaryRepository EmployeeSalaryRepository;

        public ReportCapacityPupilByTeacherBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(context);
            this.TeacherOfFacultyRepository = new TeacherOfFacultyRepository(context);
            this.EmployeeSalaryRepository = new EmployeeSalaryRepository(context);
        }

        #region Tạo báo cáo học lực HS theo giáo viên
        public Stream CreateReportCapacityPupilByTeacher(int SchoolID, int AcademicYearID, int AppliedLevel, int semester, int SubjectID, List<int> lstTeacher)
        {
            //GV_[SchoolLevel]_BCHLMTheoGV_[Semester]_[Subject]
            //BCHocLucHSTheoGiaoVienCap1
            //BCHocLucHSTheoGiaoVienCap23
            string reportCode = "";
            #region Cấp 1
            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_HOC_LUC_HOC_SINH_THEO_GIAO_VIEN_CAP1;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                int firstRow = 10;
                int defaultNumbeOfColumn = 7;
                int firstDynamicCol = VTVector.dic['E'];
                int defaultLastCol = VTVector.dic['R'];
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 4, firstDynamicCol + 1);

                //Tạo template chuẩn
                IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "R" + (firstRow - 1));
                tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 4, firstDynamicCol - 1), "A" + firstRow);

                //Lấy danh sách các cột động
                List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                    AppliedLevel,
                    SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON,
                    SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                    SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_ALL);

                //Tạo các cột động
                Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
                for (int i = 0; i < listSC.Count; i++)
                {
                    StatisticsConfig sc = listSC[i];
                    tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                    tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                    dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
                    tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
                }


                int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;


                //Tính các công thức 
                string formulaPercent = "=IF(cellRealNumberOfPupil=0;0;ROUND(cellTotal/cellRealNumberOfPupil;4)*100)";

                for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
                {
                    for (int i = firstDynamicCol; i <= lastCol; i += 2)
                    {
                        string cellRealNumberOfPupil = "D" + rowIndex;
                        string cellTotal = new VTVector(rowIndex, i).ToString();
                        tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                            Replace("cellTotal", cellTotal).Replace("cellRealNumberOfPupil", cellRealNumberOfPupil));
                    }
                }

                //Fill dữ liệu chung cho template

                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
                SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
                Province province = school.Province;

                string schoolName = school.SchoolName.ToUpper();
                string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, AppliedLevel).ToUpper();
                string academicYearTitle = academicYear.DisplayTitle;
                string subjectName = subject.DisplayName.ToUpper();
                string provinceName = (school.District != null ? school.District.DistrictName : "");
                DateTime reportDate = DateTime.Now;
                string Semester = SMASConvert.ConvertSemester(semester).ToUpper();

                Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"AcademicYear", academicYearTitle},
                    {"Subject", subjectName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", Semester}
                };

                tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

                //Tạo sheet Fill dữ liệu
                int lastColumn = Math.Max(lastCol, defaultLastCol);
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                    new VTVector(firstRow + 1, lastColumn).ToString());

                //Tạo vùng cho từng khối
                IVTRange summarizeRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
                IVTRange topRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
                IVTRange midRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
                IVTRange lastRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);

                //fill dữ liệu cho các khối
                //Lấy thông tin cho báo cáo
                //Danh sách khối
                IQueryable<EducationLevel> lsEducationLevel = EducationLevelBusiness.GetByGrade(AppliedLevel);
                //Danh sách lớp
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                if (semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    dic["Semester"] = semester;
                }
                else {
                    dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
                dic["AppliedLevel"] = AppliedLevel;
                dic["SubjectID"] = SubjectID;
                //int ASSIGNED = 2; //Đã phân công giảng dạy
                //dic["Type"] = ASSIGNED;
                IQueryable<TeachingAssignment> iquTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dic);
                //Danh sách điểm
                dic = new Dictionary<string,object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["AppliedLevel"] = AppliedLevel;
                dic["SubjectID"] = SubjectID;

                List<VSummedUpRecord> lsSummedUpRecord = new List<VSummedUpRecord>();
                IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", AppliedLevel},
                                                {"AcademicYearID", AcademicYearID}}).AddCriteriaSemester(AcademicYearBusiness.Find(AcademicYearID), semester);
                IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    dic["Semester"] = semester;
                    lsSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => lsPupilOfClassSemester.Any(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID) && o.PeriodID == null).ToList();
                }
                else
                {
                    IQueryable<VSummedUpRecord> lstSummedUpRecord;
                    if (AppliedLevel == 1)
                    {
                        lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => lsPupilOfClassSemester.Any(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID) && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                    }
                    else
                    {
                        lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => lsPupilOfClassSemester.Any(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID) && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                    }
                    IQueryable<VSummedUpRecord> lstSummedUp = lstSummedUpRecord.Where(u => u.Semester == lstSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester));
                    IQueryable<VSummedUpRecord> lstSum = lstSummedUp.Where(u => u.SummedUpRecordID == lstSummedUp.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.SummedUpRecordID));
                    lsSummedUpRecord = lstSum.ToList();

                     foreach (VSummedUpRecord item in lsSummedUpRecord)
                     {
                         if (item.ReTestJudgement != null)
                         {
                             item.JudgementResult = item.ReTestJudgement;
                         }
                         if (item.ReTestMark != null)
                         {
                             item.SummedUpMark = item.ReTestMark;
                         }
                     }
                }


                List<int> listBeginRow = new List<int>();
                int beginRow = firstRow + 2;
                int endRow;
                List<TeachingAssignmentBO> lstClassProfile = (from p in iquTeachingAssignment
                                                              join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                              where q.IsActive.Value
                                                              select new TeachingAssignmentBO
                                                              {
                                                                  EducationLevelID = q.EducationLevelID,
                                                                  TeacherID = p.TeacherID,
                                                                  ClassID = p.ClassID,
                                                                  ClassOrderNumber = q.OrderNumber,
                                                                  ClassName = q.DisplayName
                                                              }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ToList();
                IQueryable<ClassProfile> iqClassProfile = iquTeachingAssignment.Select(o => o.ClassProfile).Distinct().AsQueryable();
                List<int> lstClassProfileID = iqClassProfile.Select(p=>p.ClassProfileID).Distinct().ToList();
                IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"Semester",semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0},
                    {"ListClassID",lstClassProfileID}
                };
                List<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(iqClassProfile, dicCATP).ToList();
                List<Employee> lstEmployee = EmployeeBusiness.All.Where(o => lstTeacher.Contains(o.EmployeeID)).ToList();
                List<ClassInfoBO> lstClassInfo = new List<ClassInfoBO>();
                List<VSummedUpRecord> lsSummedUpRecord_Class = new List<VSummedUpRecord>();
                int teacherID = 0;
                List<TeachingAssignmentBO> lsClass = null;
                Employee teacher = null;
                TeachingAssignmentBO cp = null;
                ClassInfoBO classInfo = null;
                for (int i = 0; i < lstTeacher.Count; i++)
                {
			        teacherID = lstTeacher[i];
                    teacher = lstEmployee.Where(o => o.EmployeeID == teacherID).FirstOrDefault();
                    //Danh sách lớp giáo viên được PCGD
                    var iquTeachingAssignment1 = iquTeachingAssignment.Where(o => o.TeacherID == teacherID);
                    lsClass = lstClassProfile.Where(o => o.TeacherID == teacherID).Distinct().ToList();
                    lstClassInfo = iqClassInfoBO.Where(o => lsClass.Select(u => u.ClassID).Contains(o.ClassID)).ToList();

                    //Nếu giáo viên chưa được PCGD
                    if (lsClass == null || lsClass.Count() == 0)
                    {
                        //Fill luôn cột tổng số
                        dataSheet.CopyPasteSameSize(summarizeRange, "A" + beginRow);
                        foreach (StatisticsConfig sc in listSC)
                        {
                            dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                        }
                        dataSheet.SetCellValue("A" + beginRow, i + 1);
                        dataSheet.SetCellValue("B" + beginRow, teacher.FullName);
                        dataSheet.SetCellValue("C" + beginRow, "");
                        dataSheet.SetCellValue("D" + beginRow, 0);
                    }
                    else
                    {
                        endRow = beginRow + lsClass.Count - 1;
                        //Tạo dữ liệu cần fill
                        List<object> listData = new List<object>();
                        List<ClassInfoBO> ListClass = iqClassInfoBO.ToList();
                        for (int j = 0; j < lsClass.Count; j++)
                        {
			                cp = lsClass[j];
                            classInfo = lstClassInfo.Where(o => o.ClassID == cp.ClassID).FirstOrDefault();
                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["Order"] = i + 1;
                            dicData["FullName"] = teacher.FullName;
                            dicData["ClassName"] = cp.ClassName;
                            dicData["RealNumberOfPupil"] = classInfo.ActualNumOfPupil;
                            lsSummedUpRecord_Class = lsSummedUpRecord.Where(o => o.ClassID == cp.ClassID).ToList();
                            foreach (StatisticsConfig sc in listSC)
                            {
                                if (sc.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                                {
                                    int numberOf = (from mr in lsSummedUpRecord_Class
                                                    where 
                                                    mr.SummedUpMark < sc.MaxValue
                                                    && mr.SummedUpMark >= sc.MinValue
                                                    select mr.SummedUpRecordID).Count();
                                    dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                                }
                                else if (sc.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE)
                                {
                                    int numberOf = (from mr in lsSummedUpRecord_Class
                                                    where  mr.JudgementResult == sc.EqualValue
                                                    select mr.SummedUpRecordID).Count();
                                    dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                                }

                            }
                            listData.Add(dicData);
                        }
                        //Tạo khung dữ liệu danh sách các lớp
                        for (int k = beginRow; k <= endRow; k++)
                        {
                            IVTRange copyRange;
                            if (k == beginRow)
                            {
                                copyRange = topRange;
                            }
                            else
                            {
                                copyRange = midRange;
                            }
                            dataSheet.CopyPasteSameSize(copyRange, "A" + k);
                        }
                        //Fill dữ liệu
                        dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });

                        //Fill dữ liệu dòng tổng từng giáo viên


                        endRow = beginRow + lsClass.Count();
                        //merge row STT và Giáo viên
                        IVTRange rangeOrder = dataSheet.GetRange(beginRow, 1, endRow, 1);
                        rangeOrder.Merge();
                        IVTRange rangeTeacher = dataSheet.GetRange(beginRow, 2, endRow, 2);
                        rangeTeacher.Merge();
                        dataSheet.CopyPasteSameSize(lastRange, "A" + endRow);
                        string fomula = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int col = dicStatisticsConfigToColumn[sc];
                            dataSheet.SetFormulaValue(endRow,
                                col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                        }
                        //TS
                        dataSheet.SetFormulaValue("C" + endRow, "TS");
                        //Công thức cột SS
                        dataSheet.SetFormulaValue("D" + endRow, fomula.Replace("#", "D"));
                    }
                    //Tính vị trí tiếp theo
                    beginRow = beginRow + lsClass.Count() + 1;
                }

                //Xoá sheet template
                firstSheet.Delete();
                tempSheet.Delete();
                dataSheet.Name = "Trung_Binh_Mon";
                dataSheet.FitToPage = true;
                return oBook.ToStream();
            } 
            #endregion
            #region Cấp 2, 3
            else
            {
                //Danh sach lop hoc mon khong VNEN
                List<int> listClassIdVNEN = (from a in ClassSubjectBusiness.All
                                     join b in PupilOfClassBusiness.All on a.ClassID equals b.ClassID
                                     where b.AcademicYearID == AcademicYearID
                                     && a.SubjectID == SubjectID
                                     && ((a.IsSubjectVNEN.HasValue && a.IsSubjectVNEN.Value == false) || !a.IsSubjectVNEN.HasValue )                                    
                                     select a.ClassID).Distinct().ToList();                                    

                #region Khởi tạo template
                reportCode = SystemParamsInFile.REPORT_HOC_LUC_HOC_SINH_THEO_GIAO_VIEN_CAP23;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                int firstRow = 10;
                int defaultNumbeOfColumn = 7;
                int firstDynamicCol = VTVector.dic['F'];
                int defaultLastCol = VTVector.dic['U'];
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 4, firstDynamicCol + 1);

                //Tạo template chuẩn
                IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "U" + (firstRow - 1));
                tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 4, firstDynamicCol - 1), "A" + firstRow);

                //Lấy danh sách các cột động
                List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                    AppliedLevel,
                    SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON,
                    SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                    SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_ALL);

                //Tạo các cột động
                Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
                for (int i = 0; i < listSC.Count; i++)
                {
                    StatisticsConfig sc = listSC[i];
                    tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                    tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                    dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
                    tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
                }
                //Copy cột TB trở lên
                tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, firstDynamicCol + 2 * defaultNumbeOfColumn,
                    firstRow + 4, firstDynamicCol + 2 * defaultNumbeOfColumn + 1), firstRow, firstDynamicCol + listSC.Count * 2);

                int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;



                //Tính các công thức 
                string formulaPercent = "=IF(cellRealNumberOfPupil=0;0;ROUND(cellTotal/cellRealNumberOfPupil;4)*100)";
                var lstAboveAve = (from sc in listSC
                                   where
                                       sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE
                                       && sc.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK //Tren TB chi lay mon tinh diem
                                   select VTVector.ColumnIntToString(dicStatisticsConfigToColumn[sc])).ToArray();
                string formulaAverageTotal = "=SUM(" + string.Join("#;", lstAboveAve) + "#)";

                for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
                {
                    for (int i = firstDynamicCol; i <= lastCol; i += 2)
                    {
                        string cellRealNumberOfPupil = "E" + rowIndex;
                        string cellTotal = new VTVector(rowIndex, i).ToString();
                        tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                            Replace("cellTotal", cellTotal).Replace("cellRealNumberOfPupil", cellRealNumberOfPupil));
                    }
                    tempSheet.SetFormulaValue(rowIndex, lastCol - 1, formulaAverageTotal.Replace("#", rowIndex.ToString()));
                } 
                #endregion

                #region Fill dữ liệu chung cho template
                //Fill dữ liệu chung cho template

                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
                SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
                Province province = school.Province;

                string schoolName = school.SchoolName.ToUpper();
                string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, AppliedLevel).ToUpper();
                string academicYearTitle = academicYear.DisplayTitle;
                string subjectName = subject.DisplayName.ToUpper();
                string provinceName = (school.District != null ? school.District.DistrictName : "");
                DateTime reportDate = DateTime.Now;
                string Semester = SMASConvert.ConvertSemester(semester).ToUpper();

                Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"AcademicYear", academicYearTitle},
                    {"Subject", subjectName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", Semester}
                };

                tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);
                
                #endregion
                //Tạo sheet Fill dữ liệu
                int lastColumn = Math.Max(lastCol, defaultLastCol);
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                    new VTVector(firstRow + 1, lastColumn).ToString());

                //Tạo vùng cho từng khối
                IVTRange summarizeRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
                IVTRange topRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
                IVTRange midRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
                IVTRange lastRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);

                //fill dữ liệu cho các khối
                //Lấy thông tin cho báo cáo
                //Danh sách khối
                IQueryable<EducationLevel> lsEducationLevel = EducationLevelBusiness.GetByGrade(AppliedLevel);
                //Danh sách lớp
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["AppliedLevel"] = AppliedLevel;
                if (semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    dic["Semester"] = semester;
                }
                else
                {
                    dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
                
                //Them thong tin loai bo lop vnen
                dic["RemoveVNEN"] = true;
                dic["ListClassIDVNEN"] = listClassIdVNEN;

                //int ASSIGNED = 2; //Đã phân công giảng dạy
                //dic["Type"] = ASSIGNED;
                IQueryable<TeachingAssignment> iquTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dic);

                //Danh sách điểm
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["AppliedLevel"] = AppliedLevel;
                List<VSummedUpRecord> lsSummedUpRecord = new List<VSummedUpRecord>();
                VSummedUpRecord objVSUR = null;
                IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", AppliedLevel},
                                                {"AcademicYearID", AcademicYearID}}).AddCriteriaSemester(AcademicYearBusiness.Find(AcademicYearID), semester);
                lsPupilOfClassSemester1 = lsPupilOfClassSemester1.Where(p => listClassIdVNEN.Contains(p.ClassID));
                IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
                if (semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    dic["SubjectID"] = SubjectID;
                    dic["Semester"] = semester;
                    //lsSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(u => lsPupilOfClassSemester.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID) && u.PeriodID == null).ToList();
                    lsSummedUpRecord = (from sur in VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic)
                                        join poc in lsPupilOfClassSemester on new { sur.PupilID, sur.ClassID } equals new { poc.PupilID, poc.ClassID }
                                        where sur.PeriodID == null
                                        select sur).ToList();
                }
                else
                {
                    dic["SubjectID"] = SubjectID;
                    //IQueryable<VSummedUpRecord> lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => lsPupilOfClassSemester.Any(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID) && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.PeriodID == null);
                    List<VSummedUpRecord> lstSummedUpRecord = (from sur in VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic)
                                                                     join poc in lsPupilOfClassSemester on new { sur.PupilID, sur.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                                     where sur.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                     && sur.PeriodID == null
                                                               select sur).ToList();
                    //lsSummedUpRecord = lstSummedUpRecord.Where(o => o.Semester == lstSummedUpRecord.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Max(u => u.Semester)).ToList();
                    int maxSemesterID = 0;
                    for (int i = 0; i < lstSummedUpRecord.Count; i++)
                    {
                        objVSUR = lstSummedUpRecord[i];
                        maxSemesterID = lstSummedUpRecord.Where(p => p.PupilID == objVSUR.PupilID && p.ClassID == objVSUR.ClassID).Max(p=>p.Semester.Value);
                        if (objVSUR.Semester == maxSemesterID)
                    {
                            if (objVSUR.ReTestJudgement != null)
                        {
                                objVSUR.JudgementResult = objVSUR.ReTestJudgement;
                        }
                            if (objVSUR.ReTestMark != null)
                        {
                                objVSUR.SummedUpMark = objVSUR.ReTestMark;
                            }
                            lsSummedUpRecord.Add(objVSUR);
                        }
                    }
                }
                List<int> listBeginRow = new List<int>();
                int beginRow = firstRow + 2;
                int endRow;
                List<TeachingAssignmentBO> lstClassProfile = (from p in iquTeachingAssignment
                                                              join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                              where q.IsActive.Value
                                                              select new TeachingAssignmentBO
                                                                  {
                                                                      EducationLevelID = q.EducationLevelID,
                                                                      TeacherID = p.TeacherID,
                                                                      ClassID = p.ClassID,
                                                                      ClassOrderNumber = q.OrderNumber,
                                                                      ClassName = q.DisplayName
                                                                  }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ToList();
                IQueryable<ClassProfile> iqClassProfile = iquTeachingAssignment.Select(o => o.ClassProfile).Distinct().AsQueryable();
                IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"Semester",semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0}
                };
                List<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(iqClassProfile, dicCATP).ToList();
                List<Employee> lstEmployee = EmployeeBusiness.All.Where(o => lstTeacher.Contains(o.EmployeeID)).ToList();
                List<ClassInfoBO> lstClassInfo = new List<ClassInfoBO>();
                List<VSummedUpRecord> lsSummedUpRecord_Class = new List<VSummedUpRecord>();
                int teacherID = 0;
                List<int> lstClassID = new List<int>();
                TeachingAssignmentBO objTA = null;
                List<TeachingAssignmentBO> lsClass = null;
                Employee teacher = null;
                for (int i = 0; i < lstTeacher.Count; i++)
                {
                    teacherID = lstTeacher[i];
                    teacher = lstEmployee.Where(o => o.EmployeeID == teacherID).FirstOrDefault();
                    //Danh sách lớp giáo viên được PCGD
                    var iquTeachingAssignment1 = iquTeachingAssignment.Where(o => o.TeacherID == teacherID);
                    lsClass = lstClassProfile.Where(o => o.TeacherID == teacherID).Distinct().ToList();
                    lstClassID = lsClass.Select(p => p.ClassID.Value).Distinct().ToList();
                    lstClassInfo = iqClassInfoBO.Where(o => lstClassID.Contains(o.ClassID)).ToList();
                    //Nếu giáo viên chưa được PCGD
                    if (lsClass == null || lsClass.Count() == 0)
                    {
                        //Fill luôn cột tổng số
                        dataSheet.CopyPasteSameSize(summarizeRange, "A" + beginRow);
                        foreach (StatisticsConfig sc in listSC)
                        {
                            dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                        }
                        dataSheet.SetCellValue("A" + beginRow, i + 1);
                        dataSheet.SetCellValue("B" + beginRow, teacher != null ? teacher.FullName : "");
                        dataSheet.SetCellValue("C" + beginRow, "");
                        dataSheet.SetCellValue("D" + beginRow, 0);
                    }
                    else
                    {
                        endRow = beginRow + lsClass.Count - 1;
                        //Tạo dữ liệu cần fill
                        List<object> listData = new List<object>();
                        ClassInfoBO classInfo = null;
                        for (int j = 0; j < lsClass.Count; j++)
                        {                      
                            objTA = lsClass[j];
                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            classInfo = lstClassInfo.Where(o => o.ClassID == objTA.ClassID).FirstOrDefault();
                            dicData["Order"] = i + 1;
                            dicData["FullName"] = teacher != null ? teacher.FullName : "";
                            dicData["ClassName"] = objTA.ClassName;
                            dicData["NumberOfPupil"] = classInfo != null ? classInfo.NumOfPupil : 0;
                            dicData["RealNumberOfPupil"] = classInfo != null ? classInfo.ActualNumOfPupil : 0;
                            lsSummedUpRecord_Class = lsSummedUpRecord.Where(o => o.ClassID == objTA.ClassID).ToList();
                            foreach (StatisticsConfig sc in listSC)
                            {
                                if (sc.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                                {
                                    int numberOf = (from mr in lsSummedUpRecord_Class
                                                    where mr.SummedUpMark < sc.MaxValue
                                                    && mr.SummedUpMark >= sc.MinValue
                                                    //&& lstPupilInClass.Contains(mr.PupilID)
                                                    //&& (mr.PupilProfile.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || mr.PupilProfile.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                    select mr.SummedUpRecordID).Count();
                                    dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                                }
                                else if (sc.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE)
                                {
                                    int numberOf = (from mr in lsSummedUpRecord_Class
                                                    where  mr.JudgementResult == sc.EqualValue
                                                    //&& lstPupilInClass.Contains(mr.PupilID)
                                                    //&& (mr.PupilProfile.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || mr.PupilProfile.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                    select mr.SummedUpRecordID).Count();
                                    dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                                }
                            }
                            listData.Add(dicData);
                        }
                        //Tạo khung dữ liệu danh sách các lớp
                        for (int k = beginRow; k <= endRow; k++)
                        {
                            IVTRange copyRange;
                            if (k == beginRow)
                            {
                                copyRange = topRange;
                            }
                            else
                            {
                                copyRange = midRange;
                            }
                            dataSheet.CopyPasteSameSize(copyRange, "A" + k);
                        }
                        //Fill dữ liệu
                        dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });

                        //Fill dữ liệu dòng tổng từng giáo viên


                        endRow = beginRow + lsClass.Count();
                        //merge row STT và Giáo viên
                        IVTRange rangeOrder = dataSheet.GetRange(beginRow, 1, endRow, 1);
                        rangeOrder.Merge();
                        IVTRange rangeTeacher = dataSheet.GetRange(beginRow, 2, endRow, 2);
                        rangeTeacher.MergeLeft();
                        dataSheet.CopyPasteSameSize(lastRange, "A" + endRow);
                        string fomula = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int col = dicStatisticsConfigToColumn[sc];
                            dataSheet.SetFormulaValue(endRow,
                                col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                        }
                        //TS
                        dataSheet.SetFormulaValue("C" + endRow, "TS");
                        //Công thức cột SS
                        dataSheet.SetFormulaValue("D" + endRow, fomula.Replace("#", "D"));
                        dataSheet.SetFormulaValue("E" + endRow, fomula.Replace("#", "E"));
                    }
                    //Tính vị trí tiếp theo
                    beginRow = beginRow + lsClass.Count() + 1;
                }

                //Xoá sheet template
                firstSheet.Delete();
                tempSheet.Delete();
                dataSheet.Name = "Trung_Binh_Mon";
                dataSheet.FitToPage = true;

                return oBook.ToStream();
            } 
            #endregion
        }
        #endregion
    }
}
