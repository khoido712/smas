﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;
using System.Drawing;
using System.Globalization;

namespace SMAS.Business.Business
{
    public partial class ReportPupilSynthesisBusiness
    {

        public int startCol = 2;
        const int rowHeaderStart = 3;
        const int rowHeaderEnd = 9;


        /// <summary>
        /// ExportPupilSynthesis
        /// </summary>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="Semester">The semester.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <param name="rptCheckBox">The RPT check box.</param>
        /// <returns>
        /// Stream
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>11/28/2012</Date>
        public Stream ExportPupilSynthesis(int UserAccountID, int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, IDictionary<String, object> dic,bool isSuperViSingDept,bool isSubSuperVisingDept, out string FileName)
        {
            #region Khoi tao file excel
            string reportCode = "";
            int modSchoolID = SchoolID % 100;
            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BANGDIEMTHCAP1;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_BANGDIEMTHCAP23;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet emptySheet = oBook.GetSheet(2);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);

            //GV_[SchoolLevel]_BangDiemTHDynamic_[EducationLevel/Class]_[Semester]

            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel);
            string educationLevelOrClass = EducationLevelID == 0 ? "Tất cả" : ClassID == 0 ? EducationLevelRepository.Find(EducationLevelID).Resolution : ClassProfileBusiness.Find(ClassID).DisplayName;
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(schoolLevel));
            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            #endregion

            #region Tiêu đề template
            //Change header

            String title = "BẢNG ĐIỂM TỔNG HỢP ";
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                title += "HỌC KỲ I ";
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                title += "HỌC KỲ II ";
            else
            {
                title += "CẢ NĂM ";
            }
            if (EducationLevelID != 0)
            {
                EducationLevel el = EducationLevelRepository.Find(EducationLevelID);
                if (el != null)
                {
                    title += el.Resolution.ToUpper() + " ";
                }
            }
            if (ClassID > 0)
            {
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                if (cp != null)
                {
                    title += "LỚP " + cp.DisplayName.ToUpper() + " ";
                }
            }

            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
            title += "NĂM HỌC " + ac.DisplayTitle.ToUpper();
            sheet.SetCellValue("A2", title);
            #endregion

            #region Lấy dữ liệu vào cho thông tin học sinh

            //Lay du lieu tu dic cho cac thong tin hoc sinh
            bool chkLevel = CheckSelected("chkLevel", dic);
            bool chkBirthDate = CheckSelected("chkBirthDate", dic);
            bool chkGenre = CheckSelected("chkGenre", dic);
            bool chkBirthPlace = CheckSelected("chkBirthPlace", dic);
            bool chkAddress = CheckSelected("chkAddress", dic);
            bool chkEthnic = CheckSelected("chkEthnic", dic);
            bool chkReligion = CheckSelected("chkReligion", dic);
            bool chkPolicyTarget = CheckSelected("chkPolicyTarget", dic);
            bool chkIsYoungPioneerMember = CheckSelected("chkIsYoungPioneerMember", dic);
            bool chkIsYouthLeageMember = CheckSelected("chkIsYouthLeageMember", dic);
            bool chkIsCommunistPartyMember = CheckSelected("chkIsCommunistPartyMember", dic);
            bool chkPhone = CheckSelected("chkPhone", dic);
            bool chkFatherFullName = CheckSelected("chkFatherFullName", dic);
            bool chkMotherFullName = CheckSelected("chkMotherFullName", dic);
            bool chkStatus = CheckSelected("chkStatus", dic);
            //bổ sung các checkbox mới
            bool chkPriority = CheckSelected("chkPriority", dic);
            bool chkArea = CheckSelected("chkArea", dic); // Lay gia tri cua checkbox vung
            bool chkProvince = CheckSelected("chkProvince", dic); // Lay gia tri cua checkbox tinh thanh
            bool chkDistrict = CheckSelected("chkDistrict", dic); // Lay gia tri cua checkbox quan huyen
            bool chkCommnune = CheckSelected("chkCommnune", dic); // Lay gia tri cua checkbox xa phuong
            bool chkVillage = CheckSelected("chkVillage", dic); // Lay gia tri cua checkbox thon xom
            bool chkDisabled = CheckSelected("chkDisabled", dic);
            bool chkForeignLanguageType = CheckSelected("chkForeignLanguageType", dic);
            bool chkMobile = CheckSelected("chkMobile", dic);
            bool chkFatherBirthDate = CheckSelected("chkFatherBirthDate", dic);
            bool chkFatherJob = CheckSelected("chkFatherJob", dic);
            bool chkFatherMobile = CheckSelected("chkFatherMobile", dic);
            bool chkMotherBirthDate = CheckSelected("chkMotherBirthDate", dic);
            bool chkMotherJob = CheckSelected("chkMotherJob", dic);
            bool chkMotherMobile = CheckSelected("chkMotherMobile", dic);
            bool chkSponsoFullName = CheckSelected("chkSponsoFullName", dic);
            bool chkSponsoBirthDate = CheckSelected("chkSponsoBirthDate", dic);
            bool chkSponsoJob = CheckSelected("chkSponsoJob", dic);
            bool chkSponsoMobile = CheckSelected("chkSponsoMobile", dic);

            #endregion

            #region Copy cot o phan thong tin hoc sinh
            int P_Class = 0, P_FullName = 0, P_PupilCode = 0, P_Level = 0, P_BirthDate = 0, P_Genre = 0, P_BirthPlace = 0, P_Address = 0, P_Ethnic = 0, P_Religion = 0, P_PolicyTarget = 0, P_IsYoungPioneerMember = 0, P_IsYouthLeageMember = 0, P_IsCommunistPartyMember = 0, P_Phone = 0, P_FatherFullName = 0, P_MotherFullName = 0, P_Status = 0;
            int P_Priority = 0, P_Area = 0, P_Province = 0, P_District = 0, P_Commnune = 0, P_Village = 0, P_Disabled = 0, P_ForeignLanguageType = 0, P_Mobile = 0, P_FatherBirthDate = 0, P_FatherJob = 0, P_FatherMobile = 0, P_MotherBirthDate = 0, P_MotherJob = 0, P_MotherMobile = 0, P_SponsoFullName = 0, P_SponsoBirthDate = 0, P_SponsoJob = 0, P_SponsoMobile = 0;
            if (chkLevel)
            {
                P_Level = startCol;
                copyCol(firstSheet, sheet, 2, startCol);
            }
            P_Class = startCol;
            copyCol(firstSheet, sheet, 3, startCol);
            P_PupilCode = startCol;
            copyCol(firstSheet, sheet, 4, startCol);
            P_FullName = startCol;
            copyCol(firstSheet, sheet, 5, startCol);

            if (chkBirthDate)
            {
                P_BirthDate = startCol;
                copyCol(firstSheet, sheet, 6, startCol);
            }

            if (chkGenre)
            {
                P_Genre = startCol;
                copyCol(firstSheet, sheet, 7, startCol);
            }

            if (chkBirthPlace)
            {
                P_BirthPlace = startCol;
                copyCol(firstSheet, sheet, 8, startCol);
            }

            if (chkAddress)
            {
                P_Address = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
            }

            if (chkEthnic)
            {
                P_Ethnic = startCol;
                copyCol(firstSheet, sheet, 10, startCol);
            }

            if (chkReligion)
            {
                P_Religion = startCol;
                copyCol(firstSheet, sheet, 11, startCol);
            }

            if (chkPolicyTarget)
            {
                P_PolicyTarget = startCol;
                copyCol(firstSheet, sheet, 12, startCol);
            }

            if (chkPriority)
            {
                P_Priority = startCol;
                copyCol(firstSheet, sheet, 13, startCol);
            }


            if (chkIsYoungPioneerMember)
            {
                P_IsYoungPioneerMember = startCol;
                copyCol(firstSheet, sheet, 14, startCol);
            }

            if (chkIsYouthLeageMember)
            {
                P_IsYouthLeageMember = startCol;
                copyCol(firstSheet, sheet, 15, startCol);
            }

            if (chkIsCommunistPartyMember)
            {
                P_IsCommunistPartyMember = startCol;
                copyCol(firstSheet, sheet, 16, startCol);
            }

            if (chkArea)
            {
                P_Area = startCol;
                copyCol(firstSheet, sheet, 17, startCol);
            }

            if (chkProvince)
            {
                P_Province = startCol;
                copyCol(firstSheet, sheet, 18, startCol);
            }

            if (chkDistrict)
            {
                P_District = startCol;
                copyCol(firstSheet, sheet, 19, startCol);
            }

            if (chkCommnune)
            {
                P_Commnune = startCol;
                copyCol(firstSheet, sheet, 20, startCol);
            }

            if (chkVillage)
            {
                P_Village = startCol;
                copyCol(firstSheet, sheet, 21, startCol);
            }

            if (chkDisabled)
            {
                P_Disabled = startCol;
                copyCol(firstSheet, sheet, 22, startCol);
            }

            if (chkForeignLanguageType)
            {
                P_ForeignLanguageType = startCol;
                copyCol(firstSheet, sheet, 23, startCol);
            }

            if (chkPhone)
            {
                P_Phone = startCol;
                copyCol(firstSheet, sheet, 24, startCol);
            }

            if (chkMobile)
            {
                P_Mobile = startCol;
                copyCol(firstSheet, sheet, 25, startCol);
            }

            if (chkFatherFullName)
            {
                P_FatherFullName = startCol;
                copyCol(firstSheet, sheet, 26, startCol);
            }

            if (chkFatherBirthDate)
            {
                P_FatherBirthDate = startCol;
                copyCol(firstSheet, sheet, 27, startCol);
            }

            if (chkFatherJob)
            {
                P_FatherJob = startCol;
                copyCol(firstSheet, sheet, 28, startCol);
            }

            if (chkFatherMobile)
            {
                P_FatherMobile = startCol;
                copyCol(firstSheet, sheet, 29, startCol);
            }

            if (chkMotherFullName)
            {
                P_MotherFullName = startCol;
                copyCol(firstSheet, sheet, 30, startCol);
            }

            if (chkMotherBirthDate)
            {
                P_MotherBirthDate = startCol;
                copyCol(firstSheet, sheet, 31, startCol);
            }

            if (chkMotherJob)
            {
                P_MotherJob = startCol;
                copyCol(firstSheet, sheet, 32, startCol);
            }

            if (chkMotherMobile)
            {
                P_MotherMobile = startCol;
                copyCol(firstSheet, sheet, 33, startCol);
            }

            if (chkSponsoFullName)
            {
                P_SponsoFullName = startCol;
                copyCol(firstSheet, sheet, 34, startCol);
            }

            if (chkSponsoBirthDate)
            {
                P_SponsoBirthDate = startCol;
                copyCol(firstSheet, sheet, 35, startCol);
            }

            if (chkSponsoJob)
            {
                P_SponsoJob = startCol;
                copyCol(firstSheet, sheet, 36, startCol);
            }

            if (chkSponsoMobile)
            {
                P_SponsoMobile = startCol;
                copyCol(firstSheet, sheet, 37, startCol);
            }

            if (chkStatus)
            {
                P_Status = startCol;
                copyCol(firstSheet, sheet, 38, startCol);
            }
            #endregion

            #region Xoa phan dang sau de bat dau phan mon hoc
            IVTRange range = emptySheet.GetRange(rowHeaderStart, startCol, rowHeaderEnd, 80);
            IVTRange rangePropertyNN1 = firstSheet.GetRange(rowHeaderStart, 65, rowHeaderEnd, 65);
            IVTRange rangePropertyNN2 = firstSheet.GetRange(rowHeaderStart, 66, rowHeaderEnd, 66);
            IVTRange rangePropertyNPT = firstSheet.GetRange(rowHeaderStart, 67, rowHeaderEnd, 67);
            IVTRange rangePropertyLG = firstSheet.GetRange(rowHeaderStart, 68, rowHeaderEnd, 68);
            IVTRange rangePropertyIT = firstSheet.GetRange(rowHeaderStart, 70, rowHeaderEnd, 70);
            sheet.CopyPasteSameSize(range, rowHeaderStart, startCol);
            #endregion

            #region Bat dau mon hoc
            int columnSubjectDefault = 39;
            //bien xac dinh cot dau tien cua mon hoc
            int subjectStartCol = startCol;
            //lay danh sach lop hoc theo schoolID
            IDictionary<string, object> search = new Dictionary<string, object>();

            search["AcademicYearID"] = AcademicYearID;
            search["EducationLevelID"] = EducationLevelID;
            search["AppliedLevel"] = AppliedLevel;
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, search).ToList();
            List<SubjectCat> listSubject = listClassSubject.Select(u => u.SubjectCat).Distinct()
                                                  .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            foreach (SubjectCat ClassSubject in listSubject)
            {
                bool chkSubjects = CheckSelected("sbj_" + ClassSubject.SubjectCatID.ToString(), dic);
                //kiem tra xem cot mon hoc x co duoc cho xuat hien ko
                if (chkSubjects)
                {
                    //copy template cot
                    copyCol(firstSheet, sheet, columnSubjectDefault, startCol);
                    //dien ten mon hoc
                    sheet.SetCellValue(4, startCol - 1, ClassSubject.DisplayName);
                }

            }
            //vi tri cua cot mon hoc cuoi cung
            int subjectEndCol = startCol - 1;

            bool rdoAverageMark = CheckSelected("rdoAverageMark", dic);
            bool rdoSemesterMark = CheckSelected("rdoSemesterMark", dic);

            String titleSubject = "ĐIỂM TRUNG BÌNH CÁC MÔN HỌC";
            if (rdoSemesterMark)
            {
                titleSubject = "ĐIỂM HỌC KỲ CÁC MÔN HỌC";
            }
            //merver cot tile subject
            if (subjectStartCol <= subjectEndCol)
            {
                sheet.SetCellValue(3, subjectStartCol, titleSubject);
                //tien hanh merger
                IVTRange rangeSubjectTitle = sheet.GetRange(3, subjectStartCol, 3, subjectEndCol);
                rangeSubjectTitle.Merge();

            }

            #endregion

            // Ket thuc phan mon hoc

            // 2 cot tbcm va diem thi

            // RdoAverageMark

            // Frame ket qua hoc tap ren luyen
            #region Ket qua hoc tap ren luyen
            bool chkSubjectAverage = CheckSelected("chkSubjectAverage", dic);
            if (AppliedLevel == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                chkSubjectAverage = false;
            bool chkCapacity = CheckSelected("chkCapacity", dic);
            bool chkConduct = CheckSelected("chkConduct", dic);
            bool chkEmulation = CheckSelected("chkEmulation", dic);
            bool chkAbsence = CheckSelected("chkAbsence", dic);
            bool chkRank = CheckSelected("chkRank", dic);
            bool chkStudyingJudgement = CheckSelected("chkStudyingJudgement", dic);
            int P_chkSubjectAverage = 0, P_chkCapacity = 0, P_chkConduct = 0, P_chkEmulation = 0, P_chkAbsence = 0, P_chkRank = 0, P_chkStudyingJudgement = 0;

            if (chkSubjectAverage)
            {
                P_chkSubjectAverage = startCol;
                copyCol(firstSheet, sheet, 57, startCol);
            }
            if (chkCapacity)
            {
                P_chkCapacity = startCol;
                copyCol(firstSheet, sheet, 58, startCol);
                if (AppliedLevel == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    sheet.SetCellValue(rowHeaderStart, P_chkCapacity, "XLGD");
            }
            if (chkConduct)
            {
                P_chkConduct = startCol;
                copyCol(firstSheet, sheet, 59, startCol);
            }
            if (chkEmulation)
            {
                P_chkEmulation = startCol;
                copyCol(firstSheet, sheet, 62, startCol);
            }
            if (chkAbsence)
            {
                P_chkAbsence = startCol;
                copyCol(firstSheet, sheet, 60, startCol);
                copyCol(firstSheet, sheet, 61, startCol);
            }
            if (chkRank)
            {
                P_chkRank = startCol;
                copyCol(firstSheet, sheet, 63, startCol);
            }
            if (chkStudyingJudgement)
            {
                P_chkStudyingJudgement = startCol;
                copyCol(firstSheet, sheet, 64, startCol);
            }
            #endregion

            #region Phần thuộc tính lớp học
            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["EducationLevelID"] = EducationLevelID;
            search["ClassID"] = ClassID;
            //DANH SACH THUOC TINH CUA LOP
            IEnumerable<PropertyOfClass> listPropertyOfClass = PropertyOfClassBusiness.SearchBySchool(SchoolID, search);
            //IEnumerable<ClassPropertyCat> listClassPropertyCat = listPropertyOfClass.Select(u => u.ClassPropertyCat).Distinct();
            List<ClassPropertyCat> listClassPropertyCat = ClassPropertyCatBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", AppliedLevel } }).ToList();
            int startPropertyClassCol = startCol;

            //Them cot NN1, NN2, NghePT
            if (CheckSelected("pro_-1", dic))
            {
                sheet.CopyPasteSameSize(rangePropertyNN1, rowHeaderStart, startCol); // Copy cot Ngoai ngu 1
                startCol++;
            }
            if (CheckSelected("pro_-2", dic))
            {
                sheet.CopyPasteSameSize(rangePropertyNN2, rowHeaderStart, startCol); // Copy cot Ngoai ngu 2
                startCol++;
            }
            if (CheckSelected("pro_-3", dic))
            {
                sheet.CopyPasteSameSize(rangePropertyNPT, rowHeaderStart, startCol); // Copy cot Nghe pho thong
                startCol++;
            }
            if (CheckSelected("pro_-4", dic))
            {
                sheet.CopyPasteSameSize(rangePropertyLG, rowHeaderStart, startCol); // Copy cot Lop ghep
                startCol++;
            }
            if (CheckSelected("pro_-5", dic))
            {
                sheet.CopyPasteSameSize(rangePropertyIT, rowHeaderStart, startCol); // Copy cot Lop tin hoc
                startCol++;
            }
            foreach (ClassPropertyCat PropertyOfClass in listClassPropertyCat)
            {
                bool chkPropertyOfClases = CheckSelected("pro_" + PropertyOfClass.ClassPropertyCatID.ToString(), dic);
                if (chkPropertyOfClases)
                {
                    //copy template cot
                    copyCol(firstSheet, sheet, 65, startCol);
                    //dien ten thuoc tinh
                    sheet.SetCellValue(3, startCol - 1, PropertyOfClass.Resolution);
                }
            }
            int endPropertyClassCol = startCol - 1;
            #endregion

            // Ket phan thuoc tinh lop hoc
            int endColTable;

            if (endPropertyClassCol >= startPropertyClassCol)
                endColTable = endPropertyClassCol;
            else
                endColTable = startCol;

            #region Merger tieu de sheet
            //tien hanh merger title sheet
            IVTRange rangeShheetTitle = sheet.GetRange(2, 1, 2, 10);
            rangeShheetTitle.Merge();
            #endregion

            #region Lay danh sach lop hoc va khoi tao gia tri
            //lay danh sach lop hoc
            IQueryable<ClassProfile> listClassProfile = ClassProfileBusiness.getClassByAccountRole(AcademicYearID, SchoolID, EducationLevelID, ClassID,
                                                                        UserAccountID, isSuperViSingDept,isSubSuperVisingDept);
            
            List<ClassProfile> lstClassProfile = listClassProfile.ToList();
            int countPupil = 0;
            int indexPupil = 0;
            int startRow = 5;
            int PupilID;

            IVTRange rangeEmpty = sheet.GetRange(5, 1, 9, endColTable);
            emptySheet.CopyPasteSameSize(rangeEmpty, 20, 1);
            IVTRange RangeContent = emptySheet.GetRange(20, 1, 24, endColTable);
            IVTRange RangeContentTop = emptySheet.GetRange(20, 1, 20, endColTable);
            IVTRange RangeContentBot = emptySheet.GetRange(24, 1, 24, endColTable);
            IVTRange RangeContentMid = emptySheet.GetRange(21, 1, 21, endColTable);
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);

            // Tim ngay bat dau va ket thuc hoc ky hien tai
            DateTime? startDateOfSemester;
            DateTime? dueDateOfSemester;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                startDateOfSemester = acaYear.FirstSemesterStartDate;
                dueDateOfSemester = acaYear.FirstSemesterEndDate;
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                startDateOfSemester = acaYear.SecondSemesterStartDate;
                dueDateOfSemester = acaYear.SecondSemesterEndDate;
            }
            else
            {
                startDateOfSemester = acaYear.FirstSemesterStartDate;
                dueDateOfSemester = acaYear.SecondSemesterEndDate;
            }


            //-- Ket thuc Tim ngay bat dau va ket thuc hoc ky hien tai

            #endregion

            foreach (ClassProfile cp in lstClassProfile)
            {
                #region Lay danh sach hoc sinh
                //lay danh sach hoc sinh trong lop
                search = new Dictionary<string, object> 
                {
                    {"AcademicYearID", AcademicYearID},
                    {"EducationLevelID", EducationLevelID},
                    {"ClassID", cp.ClassProfileID},
                    {"Check", "Checked"},
                    {"AppliedLevel", AppliedLevel}
                };
                //22:09/2014: Bo sung sap xep theo STT lop hoc, ho va HS
                IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, search)
                                                                                    .OrderBy(c => c.ClassProfile.OrderNumber)
                                                                                    .ThenBy(c => c.ClassProfile.DisplayName)
                                                                                    .ThenBy(o => o.OrderInClass)
                                                                                    .ThenBy(o => o.PupilProfile.Name)
                                                                                    .ThenBy(c => c.PupilProfile.FullName);
                List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                //Danh sach hoc sinh da xet dieu kien chuyen truong, chuyen lop, thoi hoc trong ky
                List<PupilOfClass> lstPupilOfClassBO = listPupilOfClass.AddCriteriaSemester(acaYear, Semester).ToList();
                int totalPupilInClass = listPupilOfClass.Count();
                #endregion

                #region Copy dong

                //Xac dinh so hoc sinh roi tao cac dong cho hs

                for (int i = 0; i < totalPupilInClass; i++)
                {
                    // Neu la dong cuoi thi copy dong cuoi cung truoc
                    if (i == totalPupilInClass - 1)
                    {
                        sheet.CopyPasteSameSize(RangeContentBot, i + countPupil + 5, 1);
                        continue;
                    }
                    if ((i + countPupil + 1) % 5 == 1)
                    {
                        sheet.CopyPasteSameSize(RangeContentTop, i + countPupil + 5, 1);
                    }
                    else if ((i + countPupil + 1) % 5 == 0)
                    {
                        sheet.CopyPasteSameSize(RangeContentBot, i + countPupil + 5, 1);
                    }
                    else
                    {
                        sheet.CopyPasteSameSize(RangeContentMid, i + countPupil + 5, 1);
                    }
                }
                countPupil += totalPupilInClass;
                #endregion

                #region Lay thuoc tinh lop
                bool check = true;
                String language1 = "";
                String language2 = "";
                String apperenshipName = "";
                //var listClassSubjectForClass = listClassSubject.Where(u=>u.ClassID==PupilOfClass.ClassID);

                if (cp.FirstForeignLanguageID != null)
                {
                    language1 = SubjectCatBusiness.Find(cp.FirstForeignLanguageID) != null ? SubjectCatBusiness.Find(cp.FirstForeignLanguageID).DisplayName : "";
                }
                if (cp.SecondForeignLanguageID != null)
                {
                    language2 = SubjectCatBusiness.Find(cp.SecondForeignLanguageID) != null ? SubjectCatBusiness.Find(cp.SecondForeignLanguageID).DisplayName : "";
                }
                if (cp.ApprenticeshipGroupID != null)
                {
                    apperenshipName = ApprenticeshipGroupBusiness.Find(cp.ApprenticeshipGroupID) != null ? ApprenticeshipGroupBusiness.Find(cp.ApprenticeshipGroupID).GroupName : "";
                }
                #endregion

                #region Thuoc tinh lop hoc
                //do du lieu phan thuoc tinh lop hoc
                int startPropertyContentCol = startPropertyClassCol;

                if (CheckSelected("pro_-1", dic))
                {
                    for (int j = 0; j < totalPupilInClass; j++)
                    {
                        sheet.SetCellValue(startRow + j, startPropertyContentCol, language1);
                    }
                    startPropertyContentCol++;
                }
                if (CheckSelected("pro_-2", dic))
                {
                    for (int j = 0; j < totalPupilInClass; j++)
                    {
                        sheet.SetCellValue(startRow + j, startPropertyContentCol, language2);
                    }
                    startPropertyContentCol++;
                }
                if (CheckSelected("pro_-3", dic))
                {
                    for (int j = 0; j < totalPupilInClass; j++)
                    {
                        sheet.SetCellValue(startRow + j, startPropertyContentCol, apperenshipName);
                    }
                    startPropertyContentCol++;
                }
                if (CheckSelected("pro_-4", dic))
                {
                    if (cp.IsCombinedClass)
                    {
                        for (int j = 0; j < totalPupilInClass; j++)
                        {
                            sheet.SetCellValue(startRow + j, startPropertyContentCol, "x");
                        }
                    }
                    startPropertyContentCol++;
                }
                if (CheckSelected("pro_-5", dic))
                {
                    if (cp.IsITClass == true)
                    {
                        for (int j = 0; j < totalPupilInClass; j++)
                        {
                            sheet.SetCellValue(startRow + j, startPropertyContentCol, "x");
                        }
                    }
                    startPropertyContentCol++;
                }
                //danh sach thuoc tinh
                foreach (ClassPropertyCat PropertyOfClass in listClassPropertyCat)
                {
                    //kiwm tra thuoc tinh co duoc chon hay khong
                    bool chkPropertyOfClases = CheckSelected("pro_" + PropertyOfClass.ClassPropertyCatID.ToString(), dic);
                    if (chkPropertyOfClases) // Neu thuoc tinh duoc check chon
                    {
                        //kiem tra ten thuoc tinh
                        String nameProperty = PropertyOfClass.Resolution;
                        var listClassPropertyForClass = listPropertyOfClass.Where(u => u.ClassID == cp.ClassProfileID && u.ClassPropertyCatID == PropertyOfClass.ClassPropertyCatID);
                        if (listClassPropertyForClass != null && listClassPropertyForClass.Count() > 0)
                        {
                            //danh dau x vao thuoc tinh do
                            for (int j = 0; j < totalPupilInClass; j++)
                            {
                                sheet.SetCellValue(startRow + j, startPropertyContentCol, "x");
                            }
                        }
                        //vi tri cot o danh sach thuoc tinh tang 1
                        startPropertyContentCol++;
                    }
                }
                #endregion

                if (listPupilOfClass != null && listPupilOfClass.Count() > 0)
                {
                    lstPupilOfClass = listPupilOfClass.ToList();
                    #region Lay du lieu

                    Dictionary<string, object> Search = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", AcademicYearID},
                        {"Semester", Semester},
                        {"ClassID", cp.ClassProfileID}
                    };

                    // Tim buoi hoc chinh cua lop
                    int? cacBuoiHocCuaLop = cp.Section;
                    /*int buoiHocChinhCuaLop;
                    if (!cacBuoiHocCuaLop.HasValue) // Neu truong buoi hoc khong co gia tri thi lay mac dinh la buoi sang
                    {
                        buoiHocChinhCuaLop = 1; // buoi sang
                    }
                    else
                    {
                        if ((cacBuoiHocCuaLop.Value & 1) == 1) // Buoi hoc chinh duoc uu tien lay theo thu tu sang chieu toi
                        {
                            buoiHocChinhCuaLop = 1;
                        }
                        else if ((cacBuoiHocCuaLop.Value & 2) == 2)
                        {
                            buoiHocChinhCuaLop = 2;
                        }
                        else if ((cacBuoiHocCuaLop.Value & 4) == 4)
                        {
                            buoiHocChinhCuaLop = 3;
                        }
                        else
                        {
                            buoiHocChinhCuaLop = 1;
                        }
                    }*/
                    //-- Ket thuc tim buoi hoc chinh cua lop

                    var lstPupilRanking = (from p in VPupilRankingBusiness.SearchBySchool(SchoolID, Search).Where(o => o.PeriodID == null)
                                           join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                           from j1 in g1.DefaultIfEmpty()
                                           join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                           from j2 in g2.DefaultIfEmpty()
                                           join s in StudyingJudgementBusiness.All on p.StudyingJudgementID equals s.StudyingJudgementID into g3
                                           from j3 in g3.DefaultIfEmpty()
                                           select new PupilRankingBO
                                           {
                                               AverageMark = p.AverageMark,
                                               SchoolID = p.SchoolID,
                                               AcademicYearID = p.AcademicYearID,
                                               ClassID = p.ClassID,
                                               PupilID = p.PupilID,
                                               Semester = p.Semester,
                                               CapacityLevelID = p.CapacityLevelID,
                                               ConductLevelID = p.ConductLevelID,
                                               ConductLevelName = j2.Resolution,
                                               CapacityLevel = j1.CapacityLevel1,
                                               StudyingJudgementID = p.StudyingJudgementID,
                                               StudyingJudgementResolution = j3.Resolution,
                                               Rank = p.Rank,
                                               //TotalAbsentDaysWithPermission = this.PupilAbsenceBusiness.All.Count(pa=>pa.PupilID == p.PupilID && pa.ClassID == p.ClassID && pa.SchoolID == p.SchoolID && pa.Last2digitNumberSchool == p.Last2digitNumberSchool
                                               //    && System.Data.Objects.EntityFunctions.TruncateTime(pa.AbsentDate) >= startDateOfSemester
                                               //    && System.Data.Objects.EntityFunctions.TruncateTime(pa.AbsentDate) <= dueDateOfSemester
                                               //    && pa.Section == buoiHocChinhCuaLop
                                               //    && pa.IsAccepted == true) ,
                                               TotalAbsentDaysWithPermission = p.TotalAbsentDaysWithPermission,
                                               //TotalAbsentDaysWithoutPermission = this.PupilAbsenceBusiness.All.Count(pa => pa.PupilID == p.PupilID && pa.ClassID == p.ClassID && pa.SchoolID == p.SchoolID && pa.Last2digitNumberSchool == p.Last2digitNumberSchool
                                               //    && System.Data.Objects.EntityFunctions.TruncateTime(pa.AbsentDate) >= startDateOfSemester
                                               //    && System.Data.Objects.EntityFunctions.TruncateTime(pa.AbsentDate) <= dueDateOfSemester
                                               //    && pa.Section == buoiHocChinhCuaLop
                                               //    && pa.IsAccepted == false)
                                               TotalAbsentDaysWithoutPermission = p.TotalAbsentDaysWithoutPermission
                                           }).ToList();
                    if ((AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        // Lay du lieu ren luyen lai va tong ket sau thi lai
                        var listRetestPr = (from p in VPupilRankingBusiness.All.Where(o => o.SchoolID == SchoolID && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == ac.Year
                            && o.ClassID == cp.ClassProfileID
                           && o.AcademicYearID == AcademicYearID && o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                            join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                            from j1 in g1.DefaultIfEmpty()
                                            join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                            from j2 in g2.DefaultIfEmpty()
                                            join s in StudyingJudgementBusiness.All on p.StudyingJudgementID equals s.StudyingJudgementID into g3
                                            from j3 in g3.DefaultIfEmpty()
                                            select new PupilRankingBO
                                            {
                                                AverageMark = p.AverageMark,
                                                SchoolID = p.SchoolID,
                                                AcademicYearID = p.AcademicYearID,
                                                ClassID = p.ClassID,
                                                PupilID = p.PupilID,
                                                Semester = p.Semester,
                                                CapacityLevelID = p.CapacityLevelID,
                                                ConductLevelID = p.ConductLevelID,
                                                ConductLevelName = j2.Resolution,
                                                CapacityLevel = j1.CapacityLevel1,
                                                StudyingJudgementID = p.StudyingJudgementID,
                                                StudyingJudgementResolution = j3.Resolution,
                                                Rank = p.Rank,
                                                TotalAbsentDaysWithPermission = p.TotalAbsentDaysWithPermission,
                                                TotalAbsentDaysWithoutPermission = p.TotalAbsentDaysWithoutPermission
                                            }).ToList();

                        if (listRetestPr != null && listRetestPr.Count > 0)
                        {
                            lstPupilRanking = (from p in lstPupilRanking
                                               join r in listRetestPr on p.PupilID equals r.PupilID into i
                                               from g in i.DefaultIfEmpty()
                                               select new PupilRankingBO
                                               {
                                                   AverageMark = (g != null && g.AverageMark.HasValue) ? g.AverageMark : p.AverageMark,
                                                   SchoolID = p.SchoolID,
                                                   AcademicYearID = p.AcademicYearID,
                                                   ClassID = p.ClassID,
                                                   PupilID = p.PupilID,
                                                   Semester = p.Semester,
                                                   CapacityLevelID = (g != null && g.CapacityLevelID != null) ? g.CapacityLevelID : p.CapacityLevelID,
                                                   CapacityLevel = (g != null && g.CapacityLevel != null) ? g.CapacityLevel : p.CapacityLevel,
                                                   ConductLevelID = (g != null && g.ConductLevelID != null) ? g.ConductLevelID : p.ConductLevelID,
                                                   ConductLevelName = (g != null && g.ConductLevelID != null) ? g.ConductLevelName : p.ConductLevelName,
                                                   StudyingJudgementID = (g != null && g.StudyingJudgementID != null) ? g.StudyingJudgementID : p.StudyingJudgementID,
                                                   StudyingJudgementResolution = (g != null && g.StudyingJudgementID != null) ? g.StudyingJudgementResolution : p.StudyingJudgementResolution,
                                                   Rank = (g != null && g.Rank.HasValue) ? g.Rank : p.Rank,
                                                   TotalAbsentDaysWithPermission = p.TotalAbsentDaysWithPermission,
                                                   TotalAbsentDaysWithoutPermission = p.TotalAbsentDaysWithoutPermission
                                               }).ToList();
                        }
                    }

                    List<PupilEmulation> lstPupilEmulation = this.PupilEmulationBusiness.SearchBySchool(SchoolID, Search).ToList();
                    Search = new Dictionary<string, object> 
                        {
                            {"AcademicYearID", AcademicYearID},
                            {"Semester", Semester},
                            {"ClassID", cp.ClassProfileID}
                        };

                    //+ Danh sách điểm trung bình môn: SummedUpRecordBusiness.SearchBySchool(SchoolID, Searchtionary)

                    var iqAllyearSummedUp = this.VSummedUpRecordBusiness.SearchBySchool(SchoolID, Search).Where(o => o.PeriodID == null)
                        .Select(o => new
                        {
                            SchoolID = o.SchoolID,
                            AcademicYearID = o.AcademicYearID,
                            ClassID = o.ClassID,
                            SubjectID = o.SubjectID,
                            PupilID = o.PupilID,
                            Semester = o.Semester,
                            JudgementResult = o.JudgementResult,
                            SummedUpMark = o.SummedUpMark
                        });
                    if ((AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        // Lay du lieu thi lai cua hoc sinh
                        var iqRetestSummedUp = VSummedUpRecordBusiness.All.Where(o => o.SchoolID == SchoolID && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == ac.Year
                            && o.AcademicYearID == AcademicYearID && o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                        if (ClassID > 0)
                        {
                            iqRetestSummedUp = iqRetestSummedUp.Where(o => o.ClassID == ClassID);
                        }
                        iqAllyearSummedUp = (from u in iqAllyearSummedUp
                                             join r in iqRetestSummedUp on new { u.PupilID, u.SubjectID } equals new { r.PupilID, r.SubjectID } into i
                                             from g in i.DefaultIfEmpty()
                                             select new
                                             {
                                                 SchoolID = u.SchoolID,
                                                 AcademicYearID = u.AcademicYearID,
                                                 ClassID = u.ClassID,
                                                 SubjectID = u.SubjectID,
                                                 PupilID = u.PupilID,
                                                 Semester = u.Semester,
                                                 JudgementResult = (g != null && g.ReTestJudgement != null) ? g.ReTestJudgement : u.JudgementResult,
                                                 SummedUpMark = (g != null && g.ReTestMark.HasValue) ? g.ReTestMark : u.SummedUpMark
                                             });
                    }
                    var lstSummedUpRecord = iqAllyearSummedUp.ToList();


                    // Neu la hoc ky 2 doi voi cap 1 va ca nam doi voi cap 2, 3 thi xet them truong hop thi lai

                    //Diem trung binh mon cua tung mon hoc tinh diem
                    Search = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", AcademicYearID},
                        {"Semester", Semester},
                        {"ClassID", cp.ClassProfileID},
                        {"Year", acaYear.Year}
                    };
                    string markTitle = "HK, CK1, CN";
                    //+ Danh sách điểm trung bình môn: SummedUpRecordBusiness.SearchBySchool(SchoolID, Searchtionary)

                    List<VMarkRecord> lstMarkRecord = rdoSemesterMark ?
                        this.VMarkRecordBusiness.SearchBySchool(SchoolID, Search).Where(o => markTitle.Contains(o.Title)).ToList() : new List<VMarkRecord>();

                    //Diem trung binh mon cua tung mon hoc nhan xet
                    Search = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", AcademicYearID},
                        {"Semester", Semester},
                        {"ClassID", cp.ClassProfileID},
                        {"Year", acaYear.Year}
                    };

                    //+ Danh sách điểm trung bình môn: SummedUpRecordBusiness.SearchBySchool(SchoolID, Searchtionary)

                    List<VJudgeRecord> lstJudgeRecord = rdoSemesterMark ?
                        this.VJudgeRecordBusiness.SearchBySchool(SchoolID, Search).Where(o => markTitle.Contains(o.Title)).ToList() : new List<VJudgeRecord>(); ;

                    #endregion

                    #region foreach tung hoc sinh
                    if (lstPupilOfClass.Count > 0 && lstPupilOfClass != null)
                    {
                        lstPupilOfClass = lstPupilOfClass.OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilProfile.Name) + " " + p.PupilProfile.FullName).ToList();
                    }
                    foreach (PupilOfClass PupilOfClass in lstPupilOfClass)
                    {

                        #region Thong tin hoc sinh
                        //Tran thai dang hoc hoac da tot nghiep

                        if (PupilOfClass.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED && PupilOfClass.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        {
                            sheet.GetRange(startRow, 1, startRow, endColTable).SetFontColour(Color.Red);
                            sheet.GetRange(startRow, 1, startRow, endColTable).SetFontStyle(false, Color.Red, false, null, true, false);
                        }
                        PupilID = PupilOfClass.PupilID;
                        indexPupil++;
                        //cot 1 stt
                        sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                        //cot 2 khoi
                        if (chkLevel)
                        {
                            sheet.SetCellValue(startRow, P_Level, PupilOfClass.ClassProfile.EducationLevel.Resolution);
                        }
                        //cot 3 lop
                        sheet.SetCellValue(startRow, P_Class, PupilOfClass.ClassProfile.DisplayName);
                        //cot 4 Ma hoc sinh
                        sheet.SetCellValue(startRow, P_PupilCode, PupilOfClass.PupilProfile.PupilCode);
                        //cot 5 Ho va ten
                        sheet.SetCellValue(startRow, P_FullName, PupilOfClass.PupilProfile.FullName);
                        //cot 6 ngay sinh
                        if (chkBirthDate && PupilOfClass.PupilProfile.BirthDate != null)
                            sheet.SetCellValue(startRow, P_BirthDate, String.Format("{0:dd/MM/yyyy}", PupilOfClass.PupilProfile.BirthDate));
                        //cot 7 gioi tinh
                        if (chkGenre)
                            sheet.SetCellValue(startRow, P_Genre, PupilOfClass.PupilProfile.Genre == 1 ? "Nam" : "Nữ");
                        //cot 8 noi sinh
                        if (chkBirthPlace)
                            sheet.SetCellValue(startRow, P_BirthPlace, PupilOfClass.PupilProfile.BirthPlace);
                        //cot 9 dia chi
                        if (chkAddress)
                            sheet.SetCellValue(startRow, P_Address, PupilOfClass.PupilProfile.PermanentResidentalAddress);
                        //cot dan toc
                        if (chkEthnic && PupilOfClass.PupilProfile.Ethnic != null)
                            sheet.SetCellValue(startRow, P_Ethnic, PupilOfClass.PupilProfile.Ethnic.EthnicName);
                        //cot ton giao
                        if (chkReligion && PupilOfClass.PupilProfile.Religion != null)
                            sheet.SetCellValue(startRow, P_Religion, PupilOfClass.PupilProfile.Religion.Resolution);
                        //cot doi tuong chinh sach
                        if (chkPolicyTarget && PupilOfClass.PupilProfile.PolicyTarget != null)
                            sheet.SetCellValue(startRow, P_PolicyTarget, PupilOfClass.PupilProfile.PolicyTarget.Resolution);
                        //cot doi tuong ưu tiên
                        if (chkPriority && PupilOfClass.PupilProfile.PriorityType != null)
                            sheet.SetCellValue(startRow, P_Priority, PupilOfClass.PupilProfile.PriorityType.Resolution);
                        //cot doi vien
                        if (chkIsYoungPioneerMember)
                            sheet.SetCellValue(startRow, P_IsYoungPioneerMember, PupilOfClass.PupilProfile.IsYoungPioneerMember == true ? "x" : "");
                        //cot doan vien
                        if (chkIsYouthLeageMember)
                            sheet.SetCellValue(startRow, P_IsYouthLeageMember, PupilOfClass.PupilProfile.IsYouthLeageMember == true ? "x" : "");
                        //cot dang vien
                        if (chkIsCommunistPartyMember)
                            sheet.SetCellValue(startRow, P_IsCommunistPartyMember, PupilOfClass.PupilProfile.IsCommunistPartyMember == true ? "x" : "");

                        //cot khu vực
                        if (chkArea && PupilOfClass.PupilProfile.Area != null)
                            sheet.SetCellValue(startRow, P_Area, PupilOfClass.PupilProfile.Area.AreaName);

                        //cot tinh thành
                        if (chkProvince && PupilOfClass.PupilProfile.Province != null)
                            sheet.SetCellValue(startRow, P_Province, PupilOfClass.PupilProfile.Province.ProvinceName);

                        //cot quan huyen
                        if (chkDistrict && PupilOfClass.PupilProfile.District != null)
                            sheet.SetCellValue(startRow, P_District, PupilOfClass.PupilProfile.District.DistrictName);


                        //cot Xa phuong
                        if (chkCommnune && PupilOfClass.PupilProfile.Commune != null)
                            sheet.SetCellValue(startRow, P_Commnune, PupilOfClass.PupilProfile.Commune.CommuneName);

                        //cot Thon xom
                        if (chkCommnune && PupilOfClass.PupilProfile.Village != null)
                            sheet.SetCellValue(startRow, P_Village, PupilOfClass.PupilProfile.Village.VillageName);

                        //cot khuyet tat
                        if (chkDisabled && PupilOfClass.PupilProfile.DisabledType != null)
                            sheet.SetCellValue(startRow, P_Disabled, PupilOfClass.PupilProfile.DisabledType.Resolution);

                        //cot he hoc ngoai ngu
                        if (chkForeignLanguageType)
                            sheet.SetCellValue(startRow, P_ForeignLanguageType, PupilOfClass.PupilProfile.ForeignLanguageTraining == 1 ? "Hệ 3 năm" : PupilOfClass.PupilProfile.ForeignLanguageTraining == 2 ? "Hệ 7 năm" : "");

                        //cot dien thoai
                        if (chkPhone)
                            sheet.SetCellValue(startRow, P_Phone, PupilOfClass.PupilProfile.Telephone);
                        //cot dien thoai di dong
                        if (chkMobile)
                            sheet.SetCellValue(startRow, P_Mobile, PupilOfClass.PupilProfile.Mobile);
                        //cot ho ten cha
                        if (chkFatherFullName)
                            sheet.SetCellValue(startRow, P_FatherFullName, PupilOfClass.PupilProfile.FatherFullName);

                        //cot năm sinh
                        if (chkFatherBirthDate && PupilOfClass.PupilProfile.FatherBirthDate != null)
                            sheet.SetCellValue(startRow, P_FatherBirthDate, PupilOfClass.PupilProfile.FatherBirthDate.Value.Year);

                        //cot nghe nghiep
                        if (chkFatherJob)
                            sheet.SetCellValue(startRow, P_FatherJob, PupilOfClass.PupilProfile.FatherJob);

                        //cot so dien thoai
                        if (chkFatherMobile)
                            sheet.SetCellValue(startRow, P_FatherMobile, PupilOfClass.PupilProfile.FatherMobile);

                        //cot ho ten me
                        if (chkMotherFullName)
                            sheet.SetCellValue(startRow, P_MotherFullName, PupilOfClass.PupilProfile.MotherFullName);

                        //cot năm sinh
                        if (chkMotherBirthDate && PupilOfClass.PupilProfile.MotherBirthDate != null)
                            sheet.SetCellValue(startRow, P_MotherBirthDate, PupilOfClass.PupilProfile.MotherBirthDate.Value.Year);

                        //cot nghe nghiep
                        if (chkMotherJob)
                            sheet.SetCellValue(startRow, P_MotherJob, PupilOfClass.PupilProfile.MotherJob);

                        //cot so dien thoai
                        if (chkMotherMobile)
                            sheet.SetCellValue(startRow, P_MotherMobile, PupilOfClass.PupilProfile.MotherMobile);

                        //cot ho ten me
                        if (chkSponsoFullName)
                            sheet.SetCellValue(startRow, P_SponsoFullName, PupilOfClass.PupilProfile.SponsorFullName);

                        //cot năm sinh
                        if (chkSponsoBirthDate && PupilOfClass.PupilProfile.SponsorBirthDate != null)
                            sheet.SetCellValue(startRow, P_SponsoBirthDate, PupilOfClass.PupilProfile.SponsorBirthDate.Value.Year);

                        //cot nghe nghiep
                        if (chkSponsoJob)
                            sheet.SetCellValue(startRow, P_SponsoJob, PupilOfClass.PupilProfile.SponsorJob);

                        //cot so dien thoai
                        if (chkSponsoMobile)
                            sheet.SetCellValue(startRow, P_SponsoMobile, PupilOfClass.PupilProfile.SponsorMobile);

                        //cot trang thai
                        if (chkStatus)
                        {
                            String status = "";
                            switch (PupilOfClass.Status)
                            {
                                case 1: status = "Đang học"; break;
                                case 2: status = "Đã tốt nghiệp"; break;
                                case 3: status = "Đã chuyển trường"; break;
                                case 4: status = "Đã thôi học"; break;
                                case 5: status = "Đã chuyển lớp"; break;
                                default: status = "Không xác định"; break;
                            }

                            sheet.SetCellValue(startRow, P_Status, status);
                        }
                        #endregion

                        //thong tin diem mon hoc du lieu chuan bi
                        #region Thong tin diem

                        check = lstPupilOfClassBO.Where(o => o.PupilID == PupilOfClass.PupilID && o.ClassID == PupilOfClass.ClassID).Count() > 0;

                        int indexSubject = subjectStartCol;

                        List<ClassSubject> lstClassSubjectClass = listClassSubject.Where(o => o.ClassID == cp.ClassProfileID).ToList();
                        foreach (SubjectCat ClassSubject in listSubject)
                        {
                            bool chkSubjects = CheckSelected("sbj_" + ClassSubject.SubjectCatID.ToString(), dic);
                            //kiem tra xem cot mon hoc x co duoc cho xuat hien ko
                            if (chkSubjects)
                            {
                                //kiem tra xem lay diem tong ket TBM hay diem hoc ky
                                string mark = "";
                                int isCommenting = -1;
                                List<ClassSubject> lsClassSubject = lstClassSubjectClass.Where(o => o.SubjectID == ClassSubject.SubjectCatID).ToList();
                                ClassSubject cs = new ClassSubject();
                                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                {
                                    cs = lsClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0).FirstOrDefault();
                                }
                                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                                {
                                    cs = lsClassSubject.Where(o => o.SectionPerWeekSecondSemester > 0).FirstOrDefault();
                                }
                                else
                                {
                                    cs = lsClassSubject.FirstOrDefault();
                                }
                                if (cs != null && cs.IsCommenting != null)
                                {
                                    isCommenting = cs.IsCommenting.Value;
                                }
                                if (rdoSemesterMark) //&& (PupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || PupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING))
                                {
                                    //lay diem hoc ky
                                    //kiem tra  xem mon la mon nhan xet hay tinh diem
                                    if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK || isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                                    {
                                        //neu mon tinh diem
                                        VMarkRecord markCal = lstMarkRecord.Where(u => u.SubjectID == ClassSubject.SubjectCatID && u.PupilID == PupilOfClass.PupilID).FirstOrDefault();
                                        if (markCal != null)
                                            mark = markCal.Mark.ToString("0.0", CultureInfo.CreateSpecificCulture("da-DK"));
                                    }
                                    if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        //neu mon nhan xet
                                        //neu cap 2,3 thi lay tu JudgeRecord
                                        if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                        {
                                            VJudgeRecord markCal = lstJudgeRecord.Where(u => u.SubjectID == ClassSubject.SubjectCatID && u.PupilID == PupilOfClass.PupilID).FirstOrDefault();
                                            if (markCal != null)
                                                mark = markCal.Judgement;
                                        }
                                        else if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)//neu cap 1 lay tu SummedUpRecord
                                        {
                                            var markCal = lstSummedUpRecord.Where(u => u.SubjectID == ClassSubject.SubjectCatID && u.PupilID == PupilOfClass.PupilID).FirstOrDefault();
                                            if (markCal != null)
                                                mark = markCal.JudgementResult;
                                        }

                                    }
                                }
                                else //if (PupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || PupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                {
                                    //lay diem TBM
                                    if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK || isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                                    {
                                        //neu mon tinh diem
                                        var markCal = lstSummedUpRecord.Where(u => u.SubjectID == ClassSubject.SubjectCatID && u.PupilID == PupilOfClass.PupilID).FirstOrDefault();
                                        if (markCal != null)
                                            mark = markCal.SummedUpMark.HasValue ? markCal.SummedUpMark.Value.ToString("0.0", CultureInfo.CreateSpecificCulture("da-DK")) : string.Empty;
                                    }
                                    if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        //neu mon nhan xet
                                        var markCal = lstSummedUpRecord.Where(u => u.SubjectID == ClassSubject.SubjectCatID && u.PupilID == PupilOfClass.PupilID).FirstOrDefault();
                                        if (markCal != null)
                                            mark = markCal.JudgementResult;
                                    }
                                }
                                //in diem ra shell
                                //Neu hoc sinh chuyen truong, chuyen lop, thoi hoc trong ky thi ko hien thi diem

                                if (check)
                                {
                                    sheet.SetCellValue(startRow, indexSubject, mark);
                                }
                                //tang thu tu cot  len 1
                                indexSubject++;
                                //lay diem mon 

                            }

                        }

                        //diem trung binh cac mon
                        var SubjectAverage = lstPupilRanking.Where(u => u.PupilID == PupilID);

                        if (SubjectAverage != null && SubjectAverage.Count() > 0)
                        {
                            var PupilRanking = SubjectAverage.FirstOrDefault();
                            if (PupilRanking.AverageMark != null && chkSubjectAverage && check)
                                sheet.SetCellValue(startRow, P_chkSubjectAverage, PupilRanking.AverageMark.Value.ToString("0.0", CultureInfo.CreateSpecificCulture("da-DK")));

                            //xep loai hoc luc
                            if (PupilRanking.CapacityLevel != null && chkCapacity && check)
                                sheet.SetCellValue(startRow, P_chkCapacity, PupilRanking.CapacityLevel);
                            //xep loai hanh kiem
                            if (PupilRanking.ConductLevelName != null && chkConduct && check)
                                sheet.SetCellValue(startRow, P_chkConduct, PupilRanking.ConductLevelName);
                            // Cot Diem danh co phep
                            if (PupilRanking.TotalAbsentDaysWithPermission != null && chkAbsence && check)
                                sheet.SetCellValue(startRow, P_chkAbsence, PupilRanking.TotalAbsentDaysWithPermission);
                            // Cot diem danh khong co phep
                            if (PupilRanking.TotalAbsentDaysWithoutPermission != null && chkAbsence && check)
                                sheet.SetCellValue(startRow, P_chkAbsence + 1, PupilRanking.TotalAbsentDaysWithoutPermission);
                            //xep hang
                            if (PupilRanking.Rank != null && chkRank && check)
                                sheet.SetCellValue(startRow, P_chkRank, PupilRanking.Rank.Value.ToString("F0", CultureInfo.InvariantCulture));
                            //thuoc dien
                            if (PupilRanking.StudyingJudgementResolution != null && chkStudyingJudgement && check)
                            {
                                
                                //sheet.SetCellValue(startRow, P_chkStudyingJudgement, PupilRanking.StudyingJudgementResolution);
                                sheet.SetCellValue(startRow, P_chkStudyingJudgement, UtilsBusiness.GetStudyingJudgementResolution(PupilRanking,AppliedLevel));
                            }
                        }
                        var Emulation = lstPupilEmulation.Where(u => u.PupilID == PupilID);
                        if (Emulation != null && Emulation.Count() > 0)
                        {
                            var EmulationObject = Emulation.FirstOrDefault();
                            //danh hieu
                            if (EmulationObject.HonourAchivementType != null && chkEmulation && check)
                                sheet.SetCellValue(startRow, P_chkEmulation, UtilsBusiness.ConvertCapacity(EmulationObject.HonourAchivementType.Resolution));
                        }
                        #endregion

                        //vi tri dong tang 1
                        startRow++;
                    }
                    #endregion

                }

                // Xoa cac cot thua cuoi cung khong su dung
                int numberOfColumnDelete = endColTable + 1;
                for (int i = endColTable + 1; i <= 75; i++)
                {
                    sheet.DeleteColumn(numberOfColumnDelete);
                }
            }

            //--------- Do du lieu

            // Gan lai row
            firstSheet.Delete();
            emptySheet.Delete();

            return oBook.ToStream();
        }

        
        /// <summary>
        /// CheckSelected
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="dic">The dic.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>11/28/2012</Date>
        public bool CheckSelected(String key, IDictionary<string, object> dic)
        {
            if (!dic.ContainsKey(key))
            {
                return false;
            }
            else
            {
                if (dic[key].ToString().Equals("on") || dic[key].ToString().Equals("True"))
                {
                    return true;
                }

            }
            return false;
        }

        public void copyCol(IVTWorksheet sheetOld, IVTWorksheet sheet, int col, int newPostioncol)
        {
            IVTRange range = sheetOld.GetRange(rowHeaderStart, col, rowHeaderEnd, col);

            sheet.CopyPasteSameRowHeigh(range, rowHeaderStart, newPostioncol);

            startCol++;
        }

        
    }
}
