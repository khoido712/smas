﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
namespace SMAS.Business.Business
{
    public partial class DeclareEvaluationGroupBusiness
    {
        #region Lay danh sach linh vuc cua mot user
        public List<DeclareEvaluationGroup> GetListEvaluationByEvaluationTeacher(
            int AcademicYearID,
            int SchoolID,
            int AppliedLevel,
            int EducationLevelID)
        {
            // tìm lĩnh vực theo trường
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["AcademicYearID"] = AcademicYearID;
            dicClassSubject["AppliedLevel"] = AppliedLevel;
            dicClassSubject["EducationLevel"] = EducationLevelID;

            return this.SearchBySchool(SchoolID, dicClassSubject).ToList();

        }
        #endregion


        #region Tim kiem linh vuc trong lop hoc theo truong

        /// <summary>
        /// Tim kiem linh vuc cua lop theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DeclareEvaluationGroup> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        #endregion Tim kiem linh vuc trong lop hoc theo truong

        #region Tim kiem
        /// <summary>
        /// Tim kiem linh vuc trong lop.
        /// Khong goi truc tiep ham search ma hoi qua SearchBySchool de an partition
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DeclareEvaluationGroup> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevel");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");

            IQueryable<DeclareEvaluationGroup> lstDeclareEvaluationGroup = this.DeclareEvaluationGroupRepository.All;
            IQueryable<EvaluationDevelopmentGroup> lstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.All.Where(x=>x.IsActive);

            if (SchoolID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (AppliedLevel != 0)
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.AppliedLevel == AppliedLevel);
            }
            if (EducationLevelID != 0)
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.EducationLevelID == EducationLevelID);
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EducationLevelID == EducationLevelID);
            }
            if (ProvinceID != 0)
            {
                var tempt = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID.HasValue && x.ProvinceID == ProvinceID);
                if (tempt.Count() > 0)
                {
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID.HasValue && x.ProvinceID == ProvinceID);
                }
                else
                {
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => !x.ProvinceID.HasValue || x.ProvinceID == -1);
                }
            }

            lstDeclareEvaluationGroup = from lstDEG in lstDeclareEvaluationGroup
                                        join lstEDG in lstEvaluationDevelopmentGroup
                                        on lstDEG.EvaluationGroupID equals lstEDG.EvaluationDevelopmentGroupID
                                        select lstDEG;    

            return lstDeclareEvaluationGroup;
        }

        #region tim theo EducationLevel
        public IQueryable<DeclareEvaluationGroup> SearchByClass(int ClassID, IDictionary<string, object> SearchInfo = null)
        {
            if (ClassID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            //SearchInfo.Add("ClassID", ClassID);
            //Chiendd1: 07/07/2015 Doi thanh tim kiem trong SearchBySchool de an partition
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            //if (SchoolID == 0)
            //{
            //    ClassProfile objClass = ClassProfileBusiness.Find(ClassID);
            //    SchoolID = objClass.SchoolID;
            //}
            return SearchBySchoolEvalation(SchoolID, SearchInfo);
        }

        #endregion
		
		public IQueryable<DeclareEvaluationGroup> SearchBySchoolEvalation(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.SearchEva(SearchInfo);
        }

		public IQueryable<DeclareEvaluationGroup> SearchEva(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            //bool? IsApprenticeShipSubject = Utils.GetNullableBool(SearchInfo, "IsApprenticeShipSubject");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            //int Grade = Utils.GetInt(SearchInfo, "Grade");
            int? Semester = Utils.GetNullableInt(SearchInfo, "Semester");
            List<int> LstClassID = Utils.GetIntList(SearchInfo, "ListClassID");
            List<int> lstSubjectID = Utils.GetIntList(SearchInfo, "lstSubjectID");
            List<int> lstAppliedType = Utils.GetIntList(SearchInfo, "lstAppliedType");

            IQueryable<DeclareEvaluationGroup> lsClassSubject = this.DeclareEvaluationGroupRepository.All;

            //bang can truy xuat den
            IQueryable<EvaluationDevelopmentGroup> iqClass = EvaluationDevelopmentGroupBusiness.All;

            if (AcademicYearID != 0)
            {
                lsClassSubject = lsClassSubject.Where(x => x.AcademicYearID == AcademicYearID && x.EducationLevelID == EducationLevelID && x.SchoolID == SchoolID);
            }

            if (AppliedLevel != 0)
            {
                iqClass = iqClass.Where(x => x.AppliedLevel == AppliedLevel);
            }
            // xem xet can lay bang nao
            lsClassSubject = from cs in lsClassSubject
                             join cp in iqClass on cs.EvaluationGroupID equals cp.EvaluationDevelopmentGroupID
                             select cs;

            return lsClassSubject;
        }
		
        public void Insert(List<DeclareEvaluationGroup> ListDeclareEvaluationGroup,Dictionary<string,object> dic, int Securiry)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            if (Securiry == 0)
            {
            List<DeclareEvaluationGroup> lstDelete = this.All.Where(p => p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID
                                                      && p.EducationLevelID == EducationLevelID).ToList();
               
            this.DeleteAll(lstDelete);
            for (int i = 0; i < ListDeclareEvaluationGroup.Count; i++)
            {
                this.Insert(ListDeclareEvaluationGroup[i]);
            }
            }
            else 
            {
                for (int i = 0; i < ListDeclareEvaluationGroup.Count; i++)
                {
                    this.Insert(ListDeclareEvaluationGroup[i]);
                }
                this.DeleteAll(ListDeclareEvaluationGroup);
            }
        }

        public void InsertAll(List<DeclareEvaluationGroup> ListDeclareEvaluationGroup, Dictionary<string, object> dic, int educationLevelID)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");


            List<DeclareEvaluationGroup> lstDelete = this.All.Where(p => p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID
                                                      && p.EducationLevelID == educationLevelID).ToList();

                this.DeleteAll(lstDelete);
            for (int i = 0; i < ListDeclareEvaluationGroup.Count; i++)
            {
                this.Insert(ListDeclareEvaluationGroup[i]);
            }
        }

        public void Delete(List<DeclareEvaluationGroup> DeclareEvaluationGroup)
        {
            base.DeleteAll(DeclareEvaluationGroup);
        }

        #endregion
        public List<DeclareEvaluationGroupBO> GetListDeclareEvaluationGroupByLevel(int schoolID, int academicYearID, int educationLevel)
        {
            return (from d in this.repository.All
                    join g in this.EvaluationDevelopmentGroupRepository.All on d.EvaluationGroupID equals g.EvaluationDevelopmentGroupID
                    where d.SchoolID == schoolID
                    && d.AcademicYearID == academicYearID
                    && d.EducationLevelID == educationLevel
                    orderby g.OrderID
                    select new DeclareEvaluationGroupBO
                    {
                        DeclareEvaluationGroupID = d.DeclareEvaluationGroupID,
                        EvaluationDevelopmentGroupName = g.EvaluationDevelopmentGroupName,
                        EvaluationGroupID = g.EvaluationDevelopmentGroupID
                    }).ToList();
        }

        public IQueryable<DeclareEvaluationGroup> GetListBySchoolAndAcademicYear(Dictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            IQueryable<DeclareEvaluationGroup> query = DeclareEvaluationGroupBusiness.All;

            if (schoolID != 0)
                query = query.Where(x => x.SchoolID == schoolID);
            if (academicYearID != 0)
                query = query.Where(x => x.AcademicYearID == academicYearID);
            if (educationLevelID != 0)
                query = query.Where(x => x.EducationLevelID == educationLevelID);

            return query;
        }

        public bool? UpdateDEG(List<DeclareEvaluationGroup> lstUpdate) 
        {
            bool? status = lstUpdate.Select(x => x.IsLocked).FirstOrDefault() == null ? true : lstUpdate.Select(x => x.IsLocked).FirstOrDefault();
            foreach (var item in lstUpdate) 
            {
                if (item.IsLocked == null)
                    item.IsLocked = false;
                else
                    item.IsLocked = !item.IsLocked;
                this.Update(item);
                status = item.IsLocked;
            }
            this.Save();
            return status;
        }

    }
}
