/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  NAMTA
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class EnergyDistributionBusiness
    {
        #region validate
        public void Validate(EnergyDistribution entity)
        {
            if (entity.EnergyDemandFrom >= 100 || entity.EnergyDemandFrom < 0)
            {
                throw new BusinessException("EnergyDistribution_Require_EnergyDemandFrom");
            }

            if (entity.EnergyDemandTo >= 100 || entity.EnergyDemandTo < 0)
            {
                throw new BusinessException("EnergyDistribution_Require_EnergyDemandTo");
            }

            if (entity.EnergyDemandFrom >= entity.EnergyDemandTo)
            {
                throw new BusinessException("EnergyDistribution_EnergyDemandFrom_EnergyDemandTo");
            }

            if (!AcademicYearBusiness.IsCurrentYear(entity.AcademicYearID))
            {
                throw new BusinessException("EnergyDistribution_Error_AcademicYearID");
            }
        }
        #endregion

        #region Insert
        public void Insert(List<EnergyDistribution> lstEnergyDistribution, int SchoolID)
        {
            IQueryable<EnergyDistribution> lstEnergy = this.All.Where(o => o.SchoolID == SchoolID);
            if (lstEnergy.Count() > 0 && lstEnergy != null)
            {
                this.DeleteAll(lstEnergy.ToList());
            }
            EnergyDistribution item = null;
            for (int i = 0,size = lstEnergyDistribution.Count; i < size; i++)
            {
                item = lstEnergyDistribution[i];
            //}
            //foreach (EnergyDistribution item in lstEnergyDistribution)
            //{
                Validate(item);
                if (item.EnergyDemandFrom+ item.EnergyDemandTo < 100)
                {
                    base.Insert(item);
                }
                else
                {
                    throw new BusinessException("EnergyDistribution_Error_SumEnergyDemand");
                }
            }
        }
        #endregion
        #region Search
        public IQueryable<EnergyDistribution> Search(IDictionary<string, object> dic)
        {
            IQueryable<EnergyDistribution> lstEnergyDistribution = this.All;
            String EnergyDemandFromString = "";
            if (dic.ContainsKey("EnergyDemandFrom"))
            {
                EnergyDemandFromString = dic["EnergyDemandFrom"].ToString();
            }
            int EnergyDemandFrom =-1;

            bool kt = Int32.TryParse(EnergyDemandFromString.ToString(),out EnergyDemandFrom);

            int EnergyDemandTo = -1;

            String EnergyDemandToString = "";
            if (dic.ContainsKey("EnergyDemandTo"))
            {
                EnergyDemandToString = dic["EnergyDemandTo"].ToString();
            }

            kt = Int32.TryParse(EnergyDemandTo.ToString(), out EnergyDemandFrom);
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EatingGroupID = Utils.GetInt(dic, "EatingGroupID");
            int MealID = Utils.GetInt(dic, "MealID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            Boolean? IsActive = Utils.GetBool(dic, "IsActive");

            if (EnergyDemandFrom != -1)
            {
                lstEnergyDistribution = lstEnergyDistribution.Where(o => o.EnergyDemandFrom == EnergyDemandFrom);
            }

            if (EnergyDemandTo != -1)
            {
                lstEnergyDistribution = lstEnergyDistribution.Where(o => o.EnergyDemandTo == EnergyDemandTo);
            }

            if (SchoolID != 0)
            {
                lstEnergyDistribution = lstEnergyDistribution.Where(o => o.SchoolID == SchoolID);
            }

            if (EatingGroupID != 0)
            {
                lstEnergyDistribution = lstEnergyDistribution.Where(o => o.EatingGroupID == EatingGroupID);
            }

            if (MealID != 0)
            {
                lstEnergyDistribution = lstEnergyDistribution.Where(o => o.MealID == MealID);
            }

            if (AcademicYearID != 0)
            {
                lstEnergyDistribution = lstEnergyDistribution.Where(o => o.AcademicYearID == AcademicYearID);
            }

            return lstEnergyDistribution;
        }
        #endregion
        #region SearchBySchool
        public IQueryable<EnergyDistribution> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }

        }
        #endregion

    }
}