﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Nhóm công việc
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class WorkGroupTypeBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion   



        #region insert
        /// <summary>
        /// Thêm mới Nhóm công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertWorkGroupType">doi tuong can them moi</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override WorkGroupType Insert(WorkGroupType insertWorkGroupType)
        {


            //resolution rong
            Utils.ValidateRequire(insertWorkGroupType.Resolution, "WorkGroupType_Label_Resolution");

            Utils.ValidateMaxLength(insertWorkGroupType.Resolution, RESOLUTION_MAX_LENGTH, "WorkGroupType_Label_Resolution");
            Utils.ValidateMaxLength(insertWorkGroupType.Description, DESCRIPTION_MAX_LENGTH, "WorkGroupType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertWorkGroupType.Resolution, GlobalConstants.LIST_SCHEMA, "WorkGroupType", "Resolution", true, 0, "WorkGroupType_Label_Resolution");
            //this.CheckDuplicate(insertWorkGroupType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "WorkGroupType", "AppliedLevel", true, 0, "WorkGroupType_Label_ShoolID");
           // this.CheckDuplicateCouple(insertWorkGroupType.Resolution, insertWorkGroupType.SchoolID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "WorkGroupType", "Resolution", "SchoolID", true, 0, "WorkGroupType_Label_Resolution");
            //kiem tra range

           

            insertWorkGroupType.IsActive = true;

           return base.Insert(insertWorkGroupType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Nhóm công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateWorkGroupType">doi tuong can sua</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override WorkGroupType Update(WorkGroupType updateWorkGroupType)
        {

            //check avai
            new WorkGroupTypeBusiness(null).CheckAvailable(updateWorkGroupType.WorkGroupTypeID, "WorkGroupType_Label_WorkGroupTypeID");

            //resolution rong
            Utils.ValidateRequire(updateWorkGroupType.Resolution, "WorkGroupType_Label_Resolution");

            Utils.ValidateMaxLength(updateWorkGroupType.Resolution, RESOLUTION_MAX_LENGTH, "WorkGroupType_Label_Resolution");
            Utils.ValidateMaxLength(updateWorkGroupType.Description, DESCRIPTION_MAX_LENGTH, "WorkGroupType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
             this.CheckDuplicate(updateWorkGroupType.Resolution, GlobalConstants.LIST_SCHEMA, "WorkGroupType", "Resolution", true, updateWorkGroupType.WorkGroupTypeID, "WorkGroupType_Label_Resolution");

            //this.CheckDuplicateCouple(updateWorkGroupType.Resolution, updateWorkGroupType.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "WorkGroupType", "Resolution", "SchoolID", true, updateWorkGroupType.WorkGroupTypeID, "WorkGroupType_Label_Resolution");

            // this.CheckDuplicate(updateWorkGroupType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "WorkGroupType", "AppliedLevel", true, 0, "WorkGroupType_Label_ShoolID");

            //kiem tra range

         

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu WorkGroupTypeID ->vao csdl lay WorkGroupType tuong ung roi 
            //so sanh voi updateWorkGroupType.SchoolID


            //.................



            updateWorkGroupType.IsActive = true;

          return  base.Update(updateWorkGroupType);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Nhóm công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="WorkGroupTypeId">ID Nhóm công việc</param>
        public void Delete(int WorkGroupTypeId)
        {
            //check avai
            new WorkGroupTypeBusiness(null).CheckAvailable(WorkGroupTypeId, "WorkGroupType_Label_WorkGroupTypeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "WorkGroupType",WorkGroupTypeId, "WorkGroupType_Label_WorkGroupTypeIDFailed");

            base.Delete(WorkGroupTypeId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Nhóm công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>list doi tuong tim thay</returns>
        public IQueryable<WorkGroupType> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");

            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<WorkGroupType> lsWorkGroupType = this.WorkGroupTypeRepository.All;

             if (isActive.HasValue)
            {
            lsWorkGroupType = lsWorkGroupType.Where(em => (em.IsActive == isActive));
            }

            

            if (resolution.Trim().Length != 0)
            {

                lsWorkGroupType = lsWorkGroupType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (description.Trim().Length != 0)
            {

                lsWorkGroupType = lsWorkGroupType.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

        
            return lsWorkGroupType;
        }
        #endregion
    }
}