﻿using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using System.IO;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ReportPreliminarySendSGDBusiness
    {
        public string GetHashKey(ReportPreliminaryBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"Semester", entity.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        public Stream ExcelCreateReportPreliminarySendSGD(ReportPreliminaryBO entity)
        {
            //Lấy đường dẫn báo cáo
            IVTWorkbook oBook;
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                string reportCode = SystemParamsInFile.REPORT_SO_KET_GUI_SOGD;
                ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
                //Khởi tạo
                oBook = VTExport.OpenWorkbook(templatePath);
                //Fill dữ liệu vào báo cáo
                //Lấy ra sheet đầu tiên
                IVTWorksheet oSheetPL1 = oBook.GetSheet(1);
                IVTWorksheet oSheetPL2 = oBook.GetSheet(2);
                IVTWorksheet oSheetPL3 = oBook.GetSheet(3);

                //Điền các thông tin tiêu đề báo cáo
                AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
                SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
                string yearTitle = academicYear.Year.ToString() + " - " + (academicYear.Year + 1).ToString();

                //Lấy list khối học theo cấp học 
                List<EducationLevel> lstEdu = EducationLevelBusiness.Search(new Dictionary<string, object>() { { "Grade", entity.AppliedLevel }, { "IsActive", true } }).ToList();
                //Khởi tạo dic 
                Dictionary<string, object> SearchInfo = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", entity.AppliedLevel},
                {"Last2digitNumberSchool", entity.SchoolID % 100},
                {"CreatedAcademicYear", academicYear.Year}
            };

                IQueryable<VPupilRanking> listQRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, SearchInfo);
                IQueryable<VPupilRanking> iqPupilRanking = null;
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    iqPupilRanking = listQRanking.Where(o => o.Semester == entity.Semester);
                }
                else
                {
                    listQRanking = listQRanking.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                    iqPupilRanking = listQRanking.Where(u => u.Semester == listQRanking.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester));
                }

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = entity.AcademicYearID;
                dic["SchoolID"] = entity.SchoolID;
                dic["CheckWithClass"] = "CheckWithClass";
                dic["AppliedLevel"] = entity.AppliedLevel;
                IQueryable<PupilOfClass> lstPocTemp = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dic);
                //IQueryable<PupilOfClass> lstPocMaxAssignDate_Temp = lstPocTemp.AddCriteriaSemester(academicYear, entity.Semester);
                IQueryable<PupilOfClass> lstPocMaxAssignDate = lstPocTemp.Where(o => o.AssignedDate == lstPocTemp.Where(u => u.PupilID == o.PupilID).Select(u => u.AssignedDate).Max());
                IQueryable<PupilOfClass> lstPOC = lstPocTemp.AddCriteriaSemester(academicYear, entity.Semester);
                //Lấy ra hạnh kiểm học sinh
                List<PupilRankingBO> lstPr = (from p in lstPOC
                                              join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                              join s in PupilProfileBusiness.All on p.PupilID equals s.PupilProfileID
                                              join sp in SchoolProfileBusiness.All on p.SchoolID equals sp.SchoolProfileID
                                              join r in iqPupilRanking on new { p.PupilID, p.ClassID } equals new { r.PupilID, r.ClassID }
                                                   into g1
                                              from j1 in g1.DefaultIfEmpty()
                                              where q.IsActive.Value
                                              select new PupilRankingBO
                                              {
                                                  PupilID = p.PupilID,
                                                  ClassID = p.ClassID,
                                                  EducationLevelID = q.EducationLevelID,
                                                  PupilRankingID = j1.PupilRankingID,
                                                  ConductLevelID = j1.ConductLevelID,
                                                  Genre = s.Genre,
                                                  EthnicID = s.EthnicID,
                                                  EthnicName = s.Ethnic.EthnicName,
                                                  IsDisabled = s.IsDisabled,
                                                  SchoolTypeID = sp.SchoolTypeID,
                                                  CapacityLevelID = j1.CapacityLevelID,
                                                  IsVNEN = q.IsVnenClass
                                              }).ToList();
                string semester = "";
                if (entity.Semester == 1)
                {
                    semester = "HỌC KỲ I";
                }
                #region Tạo Sheet 1a - 1d
                string reportTitle = "XẾP LOẠI HẠNH KIỂM HỌC SINH " + semester + " NĂM HỌC " + yearTitle;
                IVTWorksheet sheetPL1 = oBook.CopySheetToLast(oSheetPL1, "L5");
                //Điền dữ liệu sheet PL1
                sheetPL1.SetCellValue("A2", reportTitle);
                //Lấy mẫu dòng
                IVTRange rangeTemplatePL1 = oSheetPL1.GetRange("A6", "J6");
                //Hàng tổng cộng
                IVTRange rangeSumPL1 = oSheetPL1.GetRange("A10", "J10");
                int startRow = 6;
                int firstRow_HK = 6;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL1.CopyPasteSameSize(rangeTemplatePL1, startRow, 1);
                    sheetPL1.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL1.SetCellValue(startRow, 2, lstPrEdu.Count());
                    sheetPL1.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL1.CopyPasteSameSize(rangeSumPL1, startRow, 1);
                //Tạo công thức
                sheetPL1.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 8; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL1.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                startRow += 2;
                //Xếp loại hạnh kiểm học sinh nữ
                string reportTitle_Female = "XẾP LOẠI HẠNH KIỂM HỌC SINH NỮ " + semester + " NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Female = oSheetPL1.GetRange("A12", "L15");
                IVTRange rangeTemplate_Female = oSheetPL1.GetRange("A16", "J16");
                IVTRange rangeSumPL1_Female = oSheetPL1.GetRange("A20", "J20");

                sheetPL1.CopyPasteSameSize(rangeTitle_Female, startRow, 1);
                sheetPL1.SetCellValue(startRow, 1, reportTitle_Female);
                startRow += 4;
                int firstRow_HK_Female = startRow;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL1.CopyPasteSameSize(rangeTemplate_Female, startRow, 1);
                    sheetPL1.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL1.SetCellValue(startRow, 2, lstPrEdu.Count());
                    sheetPL1.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL1.CopyPasteSameSize(rangeSumPL1_Female, startRow, 1);
                //Tạo công thức
                sheetPL1.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK_Female.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 8; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL1.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK_Female.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                startRow += 2;
                //Xếp lọai hạnh kiểm học sinh hòa nhập, khuyết tật
                string reportTitle_Disabled = semester + " " + "NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Disabled = oSheetPL1.GetRange("A22", "L26");
                IVTRange rangeTemplate_Disabled = oSheetPL1.GetRange("A27", "J27");
                IVTRange rangeSumPL1_Disabled = oSheetPL1.GetRange("A31", "J31");

                sheetPL1.CopyPasteSameSize(rangeTitle_Disabled, startRow, 1);
                startRow++;
                sheetPL1.SetCellValue(startRow, 2, reportTitle_Disabled);
                startRow += 4;
                int firstRow_HK_Disabled = startRow;
                //Loại Trường khuyết tật
                int schoolTypeID = SchoolTypeBusiness.All.Where(o => o.Resolution.ToLower().Contains("khuyết tật")).FirstOrDefault().SchoolTypeID;
                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.IsDisabled == true && o.SchoolTypeID != schoolTypeID && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL1.CopyPasteSameSize(rangeTemplate_Disabled, startRow, 1);
                    sheetPL1.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL1.SetCellValue(startRow, 2, lstPrEdu.Count());
                    sheetPL1.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL1.CopyPasteSameSize(rangeSumPL1_Disabled, startRow, 1);
                //Tạo công thức
                sheetPL1.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK_Disabled.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 8; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL1.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK_Disabled.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                startRow += 2;

                //Xếp loại hạnh kiểm học sinh dân tộc thiểu số
                string reportTitle_Ethnic = semester + " " + "NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Ethnic = oSheetPL1.GetRange("A33", "L37");
                IVTRange rangeTemplate_Ethnic = oSheetPL1.GetRange("A38", "J38");
                IVTRange rangeSumPL1_Ethnic = oSheetPL1.GetRange("A42", "J42");

                sheetPL1.CopyPasteSameSize(rangeTitle_Ethnic, startRow, 1);
                startRow++;
                sheetPL1.SetCellValue(startRow, 1, reportTitle_Ethnic);
                startRow += 4;
                int firstRow_HK_Ethnic = startRow;
                Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("kinh")).FirstOrDefault();
                Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("nước ngoài")).FirstOrDefault();
                int? EthnicID_Kinh = 0;
                int? EthnicID_ForeignPeople = 0;
                if (Ethnic_Kinh != null)
                {
                    EthnicID_Kinh = Ethnic_Kinh.EthnicID;
                }
                if (Ethnic_ForeignPeople != null)
                {
                    EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
                }
                //id người nước ngoài

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL1.CopyPasteSameSize(rangeTemplate_Ethnic, startRow, 1);
                    sheetPL1.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL1.SetCellValue(startRow, 2, lstPrEdu.Count());
                    sheetPL1.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).Count());
                    sheetPL1.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL1.CopyPasteSameSize(rangeSumPL1_Ethnic, startRow, 1);
                //Tạo công thức
                sheetPL1.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK_Ethnic.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 8; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL1.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK_Ethnic.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                oSheetPL1.Delete();
                sheetPL1.Name = "PL 1a-1d";
                #endregion
                #region Tạo dữ liệu cho phụ lục 2a - 2d
                IVTWorksheet sheetPL2 = oBook.CopySheetToLast(oSheetPL2, "L5");
                string reportTitle_HL = "XẾP LOẠI HỌC LỰC HỌC SINH " + semester + " NĂM HỌC " + yearTitle;
                //Điền dữ liệu sheet PL1
                sheetPL2.SetCellValue("A2", reportTitle_HL);
                //Lấy mẫu dòng
                IVTRange rangeTemplatePL2 = oSheetPL2.GetRange("A6", "L6");
                //Hàng tổng cộng
                IVTRange rangeSumPL2 = oSheetPL2.GetRange("A10", "L10");
                int startRow_PL2 = 6;
                int firstRow_HK_PL2 = 6;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL2.CopyPasteSameSize(rangeTemplatePL2, startRow_PL2, 1);
                    sheetPL2.SetCellValue(startRow_PL2, 1, edu.Resolution);
                    //sheetPL2.SetCellValue(startRow_PL2, 2, lstPrEdu.Count());
                    sheetPL2.SetCellValue(startRow_PL2, 3, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 5, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 7, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 9, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 11, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                    startRow_PL2++;
                }
                //Tạo hàng tính tổng
                sheetPL2.CopyPasteSameSize(rangeSumPL2, startRow_PL2, 1);
                //Tạo công thức
                sheetPL2.SetCellValue(startRow_PL2, 2, "=SUM(B" + firstRow_HK_PL2.ToString() + ":B" + (startRow_PL2 - 1).ToString() + ")");
                for (int j = 0; j < 10; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL2.SetCellValue(startRow_PL2, j + 3, "=SUM(" + alphabet + firstRow_HK_PL2.ToString() + ":" + alphabet + (startRow_PL2 - 1).ToString() + ")");
                }
                startRow_PL2 += 2;
                //Xếp loại hạnh kiểm học sinh nữ
                string reportTitle_Female_PL2 = "XẾP LOẠI HỌC LỰC HỌC SINH NỮ " + semester + " NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Female_PL2 = oSheetPL2.GetRange("A12", "L15");
                IVTRange rangeTemplate_Female_PL2 = oSheetPL2.GetRange("A16", "L16");
                IVTRange rangeSumPL1_Female_PL2 = oSheetPL2.GetRange("A20", "L20");

                sheetPL2.CopyPasteSameSize(rangeTitle_Female_PL2, startRow_PL2, 1);
                sheetPL2.SetCellValue(startRow_PL2, 1, reportTitle_Female_PL2);
                startRow_PL2 += 4;
                int firstRow_HK_Female_HL = startRow_PL2;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL2.CopyPasteSameSize(rangeTemplate_Female_PL2, startRow_PL2, 1);
                    sheetPL2.SetCellValue(startRow_PL2, 1, edu.Resolution);
                    //sheetPL2.SetCellValue(startRow_PL2, 2, lstPrEdu.Count());
                    sheetPL2.SetCellValue(startRow_PL2, 3, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 5, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 7, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 9, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 11, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                    startRow_PL2++;
                }
                //Tạo hàng tính tổng
                sheetPL2.CopyPasteSameSize(rangeSumPL1_Female_PL2, startRow_PL2, 1);
                //Tạo công thức
                sheetPL2.SetCellValue(startRow_PL2, 2, "=SUM(B" + firstRow_HK_Female_HL.ToString() + ":B" + (startRow_PL2 - 1).ToString() + ")");
                for (int j = 0; j < 10; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL2.SetCellValue(startRow_PL2, j + 3, "=SUM(" + alphabet + firstRow_HK_Female_HL.ToString() + ":" + alphabet + (startRow_PL2 - 1).ToString() + ")");
                }
                startRow_PL2 += 2;
                //Xếp lọai hạnh kiểm học sinh hòa nhập, khuyết tật
                string reportTitle_Disabled_PL2 = semester + " " + "NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Disabled_PL2 = oSheetPL2.GetRange("A22", "L26");
                IVTRange rangeTemplate_Disabled_PL2 = oSheetPL2.GetRange("A27", "L27");
                IVTRange rangeSumPL1_Disabled_PL2 = oSheetPL2.GetRange("A31", "L31");

                sheetPL2.CopyPasteSameSize(rangeTitle_Disabled_PL2, startRow_PL2, 1);
                startRow_PL2++;
                sheetPL2.SetCellValue(startRow_PL2, 1, reportTitle_Disabled_PL2);
                startRow_PL2 += 4;
                int firstRow_HK_Disabled_PL2 = startRow_PL2;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.IsDisabled == true && o.SchoolTypeID != schoolTypeID && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL2.CopyPasteSameSize(rangeTemplate_Disabled_PL2, startRow_PL2, 1);
                    sheetPL2.SetCellValue(startRow_PL2, 1, edu.Resolution);
                    //sheetPL2.SetCellValue(startRow_PL2, 2, lstPrEdu.Count());
                    sheetPL2.SetCellValue(startRow_PL2, 3, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 5, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 7, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 9, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 11, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                    startRow_PL2++;
                }
                //Tạo hàng tính tổng
                sheetPL2.CopyPasteSameSize(rangeSumPL1_Disabled_PL2, startRow_PL2, 1);
                //Tạo công thức
                sheetPL2.SetCellValue(startRow_PL2, 2, "=SUM(B" + firstRow_HK_Disabled_PL2.ToString() + ":B" + (startRow_PL2 - 1).ToString() + ")");
                for (int j = 0; j < 10; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL2.SetCellValue(startRow_PL2, j + 3, "=SUM(" + alphabet + firstRow_HK_Disabled_PL2.ToString() + ":" + alphabet + (startRow_PL2 - 1).ToString() + ")");
                }
                startRow_PL2 += 2;

                //Xếp loại hạnh kiểm học sinh dân tộc thiểu số
                string reportTitle_Ethnic_PL2 = semester + " " + "NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Ethnic_PL2 = oSheetPL2.GetRange("A33", "L37");
                IVTRange rangeTemplate_Ethnic_PL2 = oSheetPL2.GetRange("A38", "L38");
                IVTRange rangeSumPL1_Ethnic_PL2 = oSheetPL2.GetRange("A42", "L42");

                sheetPL2.CopyPasteSameSize(rangeTitle_Ethnic_PL2, startRow_PL2, 1);
                startRow_PL2++;
                sheetPL2.SetCellValue(startRow_PL2, 1, reportTitle_Ethnic);
                startRow_PL2 += 4;
                int firstRow_HK_Ethnic_PL2 = startRow_PL2;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople && (!o.IsVNEN.HasValue || (o.IsVNEN.HasValue && o.IsVNEN.Value == false)));
                    sheetPL2.CopyPasteSameSize(rangeTemplate_Ethnic_PL2, startRow_PL2, 1);
                    sheetPL2.SetCellValue(startRow_PL2, 1, edu.Resolution);
                    //sheetPL2.SetCellValue(startRow_PL2, 2, lstPrEdu.Count());
                    sheetPL2.SetCellValue(startRow_PL2, 3, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 5, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 7, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 9, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                    sheetPL2.SetCellValue(startRow_PL2, 11, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                    startRow_PL2++;
                }
                //Tạo hàng tính tổng
                sheetPL2.CopyPasteSameSize(rangeSumPL1_Ethnic_PL2, startRow_PL2, 1);
                //Tạo công thức
                sheetPL2.SetCellValue(startRow_PL2, 2, "=SUM(B" + firstRow_HK_Ethnic_PL2.ToString() + ":B" + (startRow_PL2 - 1).ToString() + ")");
                for (int j = 0; j < 10; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL2.SetCellValue(startRow_PL2, j + 3, "=SUM(" + alphabet + firstRow_HK_Ethnic_PL2.ToString() + ":" + alphabet + (startRow_PL2 - 1).ToString() + ")");
                }
                oSheetPL2.Delete();
                sheetPL2.Name = "PL 2a - 2d";
                #endregion
                #region Tạo Sheet PL 3a-3b
                //Lấy dữ liệu học sinh trong lớp
                //IDictionary<string, object> dicPoc = new Dictionary<string, object>();
                //dicPoc["AcademicYearID"] = entity.AcademicYearID;
                //dicPoc["SchoolID"] = entity.SchoolID;
                //dicPoc["Check"] = "Check";
                //dicPoc["AppliedLevel"] = entity.AppliedLevel;
                //IQueryable<PupilOfClass> lstPoc_Temp = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPoc);

                var iqPoc = (from p in lstPOC
                            join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                            join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                            where q.IsActive.Value
                            select new {
                                PupilID = p.PupilID, 
                                ClassID = p.ClassID, 
                                EducationLevelID = q.EducationLevelID,
                                Genre = r.Genre
                            }).ToList();
                List<PupilPleSendSoGDBO> iqResult = (from p in lstPocMaxAssignDate
                                                     join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                     join s in ClassProfileBusiness.All.Where(o=>o.IsActive==true) on p.ClassID equals s.ClassProfileID
                                                     join pos in PupilOfSchoolBusiness.All.Where(o => o.SchoolID == entity.SchoolID) on p.PupilID equals pos.PupilID
                                                     join r in PupilLeavingOffBusiness.All.Where(o => o.SchoolID == entity.SchoolID && o.AcademicYearID == entity.AcademicYearID) on  p.PupilID equals  r.PupilID  into g1
                                                     from j1 in g1.DefaultIfEmpty()
                                                     join t in SchoolMovementBusiness.All.Where(o => o.SchoolID == entity.SchoolID && o.AcademicYearID == entity.AcademicYearID) on p.PupilID equals t.PupilID  into g2
                                                     from j2 in g2.DefaultIfEmpty()
                                                     select new PupilPleSendSoGDBO
                                                     {
                                                         PupilID = p.PupilID,
                                                         EducationLevelID = s.EducationLevelID,
                                                         PupilLeavingOffID = j1.PupilLeavingOffID,
                                                         LeavingReasonID = j1.LeavingReasonID,
                                                         LeavingDate = j1.LeavingDate,
                                                         LeavingReasonName = j1.LeavingReason.Resolution,
                                                         SchoolMovementID = j2.SchoolMovementID,
                                                         MovedDate = j2.MovedDate,
                                                         EnrolmentType = pos.EnrolmentType,
                                                         Genre = q.Genre,
                                                         EnrolmentDate = pos.EnrolmentDate
                                                     }).ToList();
                List<PupilPleSendSoGDBO> lst_LeavingOff = iqResult.Where(o => o.LeavingReasonID.HasValue).ToList();
                List<PupilPleSendSoGDBO> lst_School_Moved = iqResult.Where(o => o.SchoolMovementID.HasValue).ToList();
                List<PupilPleSendSoGDBO> lst_Enrolment_Type = new List<PupilPleSendSoGDBO>();
                DateTime firstStartSemester = academicYear.FirstSemesterStartDate.Value.Date;
                DateTime firstEndSemester = academicYear.FirstSemesterEndDate.Value.Date;
                DateTime secondStartSemester = academicYear.SecondSemesterStartDate.Value.Date;
                DateTime secondEndSemester = academicYear.SecondSemesterEndDate.Value.Date;

                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    lst_LeavingOff = lst_LeavingOff.Where(o => o.LeavingDate.HasValue && o.LeavingDate >= firstStartSemester && o.LeavingDate <= firstEndSemester).ToList();
                    lst_School_Moved = lst_School_Moved.Where(o => o.MovedDate.HasValue && o.MovedDate >= firstStartSemester && o.MovedDate <= firstEndSemester).ToList();
                    lst_Enrolment_Type = iqResult.Where(o => o.EnrolmentDate.HasValue && o.EnrolmentDate >= firstStartSemester && o.EnrolmentDate <= firstEndSemester && o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).ToList();
                }
                else
                {
                    lst_LeavingOff = lst_LeavingOff.Where(o => o.LeavingDate.HasValue && o.LeavingDate >= firstStartSemester && o.LeavingDate <= secondEndSemester).ToList();
                    lst_School_Moved = lst_School_Moved.Where(o => o.MovedDate.HasValue && o.MovedDate >= firstStartSemester && o.MovedDate <= secondEndSemester).ToList();
                    lst_Enrolment_Type = iqResult.Where(o => o.EnrolmentDate.HasValue && o.EnrolmentDate >= firstStartSemester && o.EnrolmentDate <= secondEndSemester && o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).ToList();
                }

                IVTWorksheet sheetPL3 = oBook.CopySheetToLast(oSheetPL3, "J5");
                string title_PL3 = "TÌNH HÌNH HỌC SINH THÔI HỌC " + semester + " NĂM HỌC " + yearTitle;
                string title_end_Year = "Số HS cuối " + semester + " năm học " + yearTitle;
                sheetPL3.SetCellValue("A2", title_PL3);
                sheetPL3.SetCellValue("C4", title_end_Year);
                int startRow_PL3 = 6;
                int firstRow_PL3 = 6;
                //phân tích nguyên nhân thôi học
                //Confrim với anh chiến fix cứng id của nguyên nhân
                //IQueryable<LeavingReason> lstLeavingReason = LeavingReasonBusiness.All;
                //LeavingReason reason_capa_weak = lstLeavingReason.Where(o => o.Resolution.ToLower().Contains("học lực yếu")).FirstOrDefault();
                int reason_id_capa_weak = 4;
                //if (reason_capa_weak != null)
                //{
                //    reason_id_capa_weak = reason_capa_weak.LeavingReasonID;
                //}
                //LeavingReason reason_dif = lstLeavingReason.Where(o => o.Resolution.ToLower().Contains("hoàn cảnh khó khăn")).FirstOrDefault();
                int reason_id_dif = 1;
                //if (reason_dif != null)
                //{
                //    reason_id_dif = reason_dif.LeavingReasonID;
                //}
                //LeavingReason reason_far_house = lstLeavingReason.Where(o => o.Resolution.ToLower().Contains("xa nhà")).FirstOrDefault();
                int reason_id_far_house = 2;
                //if (reason_far_house != null)
                //{
                //    reason_id_far_house = reason_far_house.LeavingReasonID;
                //}
                IVTRange range_Edu = oSheetPL3.GetRange("A6", "J6");
                IVTRange range_Edu_Sum = oSheetPL3.GetRange("A10", "J10");
                foreach (var edu in lstEdu)
                {
                    var iqResultEdu = iqPoc.Where(o => o.EducationLevelID == edu.EducationLevelID);
                    var lst_LeavingOffEdu = lst_LeavingOff.Where(o => o.EducationLevelID == edu.EducationLevelID);
                    var lst_School_MovedEdu = lst_School_Moved.Where(o => o.EducationLevelID == edu.EducationLevelID);
                    int number_reason_id_capa_weak = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_capa_weak).Count();
                    int number_reason_id_dif = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_dif).Count();
                    int number_reason_id_far_house = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_far_house).Count();
                    sheetPL3.CopyPasteSameSize(range_Edu, startRow_PL3, 1);

                    sheetPL3.SetCellValue(startRow_PL3, 1, edu.Resolution);
                    //sheetPL3.SetCellValue(startRow_PL3, 2, iqResultEdu.Count() + lst_School_MovedEdu.Count() + lst_LeavingOff.Count() - iqResult.Where(o => o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).Count());
                    sheetPL3.SetCellValue(startRow_PL3, 3, iqResultEdu.Count());
                    sheetPL3.SetCellValue(startRow_PL3, 4, lst_Enrolment_Type.Where(o => o.EducationLevelID == edu.EducationLevelID).Count());
                    sheetPL3.SetCellValue(startRow_PL3, 5, lst_School_MovedEdu.Count());
                    sheetPL3.SetCellValue(startRow_PL3, 6, lst_LeavingOffEdu.Count());
                    sheetPL3.SetCellValue(startRow_PL3, 7, number_reason_id_capa_weak);
                    sheetPL3.SetCellValue(startRow_PL3, 8, number_reason_id_dif);
                    sheetPL3.SetCellValue(startRow_PL3, 9, number_reason_id_far_house);
                    sheetPL3.SetCellValue(startRow_PL3, 10, lst_LeavingOffEdu.Count() - number_reason_id_capa_weak - number_reason_id_dif - number_reason_id_far_house);
                    startRow_PL3++;
                }
                //Tạo dòng tổng
                sheetPL3.CopyPasteSameSize(range_Edu_Sum, startRow_PL3, 1);
                for (int j = 0; j < 9; j++)
                {
                    string alphabet = GetExcelColumnName(j + 2);
                    sheetPL3.SetCellValue(startRow_PL3, j + 2, "=SUM(" + alphabet + firstRow_PL3.ToString() + ":" + alphabet + (startRow_PL3 - 1).ToString() + ")");
                }


                startRow_PL3 += 6;
                //tạo dòng mẫu nữ học sinh
                IVTRange range_Female = oSheetPL3.GetRange("A16", "J19");
                sheetPL3.CopyPasteSameSize(range_Female, startRow_PL3, 1);
                string title_female = "TÌNH HÌNH HỌC SINH NỮ THÔI HỌC " + semester + " NĂM HỌC " + yearTitle;
                string title_end_year_female = "";
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    title_end_year_female = "Số HS NỮ cuối " + semester + " (B)";
                }
                else
                {
                    title_end_year_female = "Số HS NỮ cuối năm (B)";
                }

                sheetPL3.SetCellValue(startRow_PL3, 1, title_female);
                startRow_PL3 += 2;
                sheetPL3.SetCellValue(startRow_PL3, 3, title_end_year_female);
                startRow_PL3 += 2;

                //Dòng mẫu
                IVTRange range_PL3_Female = oSheetPL3.GetRange("A20", "J20");
                IVTRange range_PL3_Sum_Femlate = oSheetPL3.GetRange("A24", "J24");
                int first_Row_PL3_Female = startRow_PL3;
                foreach (var edu in lstEdu)
                {
                    var iqResultEdu = iqPoc.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    var lst_LeavingOffEdu = lst_LeavingOff.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    var lst_School_MovedEdu = lst_School_Moved.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    int number_reason_id_capa_weak = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_capa_weak).Count();
                    int number_reason_id_dif = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_dif).Count();
                    int number_reason_id_far_house = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_far_house).Count();
                    sheetPL3.CopyPasteSameSize(range_PL3_Female, startRow_PL3, 1);
                    sheetPL3.SetCellValue(startRow_PL3, 1, edu.Resolution);
                    //sheetPL3.SetCellValue(startRow_PL3, 2, iqResultEdu.Count() + lst_School_MovedEdu.Count() + lst_LeavingOff.Count() - iqResult.Where(o => o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).Count());
                    sheetPL3.SetCellValue(startRow_PL3, 3, iqResultEdu.Count());
                    sheetPL3.SetCellValue(startRow_PL3, 4, lst_Enrolment_Type.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE).Count());
                    sheetPL3.SetCellValue(startRow_PL3, 5, lst_School_MovedEdu.Count());
                    sheetPL3.SetCellValue(startRow_PL3, 6, lst_LeavingOffEdu.Count());
                    sheetPL3.SetCellValue(startRow_PL3, 7, number_reason_id_capa_weak);
                    sheetPL3.SetCellValue(startRow_PL3, 8, number_reason_id_dif);
                    sheetPL3.SetCellValue(startRow_PL3, 9, number_reason_id_far_house);
                    sheetPL3.SetCellValue(startRow_PL3, 10, lst_LeavingOffEdu.Count() - number_reason_id_capa_weak - number_reason_id_dif - number_reason_id_far_house);
                    startRow_PL3++;
                }
                //Tạo dòng tổng
                sheetPL3.CopyPasteSameSize(range_PL3_Sum_Femlate, startRow_PL3, 1);
                for (int j = 0; j < 9; j++)
                {
                    string alphabet = GetExcelColumnName(j + 2);
                    sheetPL3.SetCellValue(startRow_PL3, j + 2, "=SUM(" + alphabet + first_Row_PL3_Female.ToString() + ":" + alphabet + (startRow_PL3 - 1).ToString() + ")");
                }

                oSheetPL3.Delete();
                sheetPL3.Name = "PL 3a-3b";

                #endregion
            }
            else
            {
                string reportCode = SystemParamsInFile.REPORT_SO_KET_GUI_SOGD_THPT;
                ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
                //Khởi tạo
                oBook = VTExport.OpenWorkbook(templatePath);
                //Lấy ra sheet đầu tiên
                IVTWorksheet oSheetPL1 = oBook.GetSheet(1);
                IVTWorksheet oSheetPL2 = oBook.GetSheet(2);
                IVTWorksheet oSheetPL3 = oBook.GetSheet(3);
                IVTWorksheet oSheetPL4 = oBook.GetSheet(4);

                //Điền các thông tin tiêu đề báo cáo
                AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
                SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
                string yearTitle = academicYear.Year.ToString() + " - " + (academicYear.Year + 1).ToString();

                //Lấy list khối học theo cấp học 
                List<EducationLevel> lstEdu = EducationLevelBusiness.Search(new Dictionary<string, object>() { { "Grade", entity.AppliedLevel }, { "IsActive", true } }).ToList();
                //Khởi tạo dic 
                Dictionary<string, object> SearchInfo = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", entity.AppliedLevel},
                {"Last2digitNumberSchool", entity.SchoolID % 100},
                {"CreatedAcademicYear", academicYear.Year}
            };

                IQueryable<VPupilRanking> listQRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, SearchInfo);
                IQueryable<VPupilRanking> iqPupilRanking = null;
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    iqPupilRanking = listQRanking.Where(o => o.Semester == entity.Semester);
                }
                else
                {
                    listQRanking = listQRanking.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                    iqPupilRanking = listQRanking.Where(u => u.Semester == listQRanking.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester));
                }
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = entity.AcademicYearID;
                dic["SchoolID"] = entity.SchoolID;
                dic["CheckWithClass"] = "CheckWithClass";
                dic["AppliedLevel"] = entity.AppliedLevel;
                IQueryable<PupilOfClass> lstPocTemp = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dic);
                //IQueryable<PupilOfClass> lstPocMaxAssignDate_Temp = lstPocTemp.AddCriteriaSemester(academicYear, entity.Semester);
                IQueryable<PupilOfClass> lstPocMaxAssignDate = lstPocTemp.Where(o => o.AssignedDate == lstPocTemp.Where(u => u.PupilID == o.PupilID).Select(u => u.AssignedDate).Max());                   
                IQueryable<PupilOfClass> lstPOC = lstPocTemp.AddCriteriaSemester(academicYear, entity.Semester);
                //Lấy ra hạnh kiểm học sinh
                IQueryable<PupilRankingBO> lstPr_Temp = (from p in lstPOC
                                              join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                              join sub in SubCommitteeBusiness.All on q.SubCommitteeID equals sub.SubCommitteeID into g2
                                              from j2 in g2.DefaultIfEmpty()
                                              join s in PupilProfileBusiness.All on p.PupilID equals s.PupilProfileID
                                              join sp in SchoolProfileBusiness.All on p.SchoolID equals sp.SchoolProfileID
                                              join r in iqPupilRanking on new { p.PupilID, p.ClassID }
                                                  equals new { r.PupilID, r.ClassID } into g1
                                              from j1 in g1.DefaultIfEmpty()
                                              where q.IsActive.Value
                                              select new PupilRankingBO
                                              {
                                                  PupilID = p.PupilID,
                                                  ClassID = p.ClassID,
                                                  EducationLevelID = q.EducationLevelID,
                                                  PupilRankingID = j1.PupilRankingID,
                                                  ConductLevelID = j1.ConductLevelID,
                                                  Genre = s.Genre,
                                                  EthnicID = s.EthnicID,
                                                  EthnicName = s.Ethnic.EthnicName,
                                                  IsDisabled = s.IsDisabled,
                                                  SchoolTypeID = sp.SchoolTypeID,
                                                  CapacityLevelID = j1.CapacityLevelID,
                                                  SubCommiteeID = j2.SubCommitteeID

                                              });
                List<PupilRankingBO> lstPr = lstPr_Temp.ToList();
                string semester = "";
                if (entity.Semester == 1)
                {
                    semester = "HỌC KỲ I";
                }
                #region Tạo Sheet 1a
                IVTWorksheet sheetPL1 = oBook.CopySheetToLast(oSheetPL1);
                string title_PL1a = "XẾP LOẠI HẠNH KIỂM HỌC SINH " + semester + " NĂM HỌC " + yearTitle;
                sheetPL1.SetCellValue("A2", title_PL1a);
                //Dòng bắt đầu
                int startRow_PL1a = 6;
                List<int> lstSub = new List<int>();
                SubCommittee sub_KHTN = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("tn") || o.Resolution.ToLower().Contains("tự nhiên")) && o.IsActive == true).FirstOrDefault();
                int sub_id_KHTN = 0;
                if(sub_KHTN != null)
                {
                    sub_id_KHTN = sub_KHTN.SubCommitteeID;
                }
                lstSub.Add(sub_id_KHTN);
                SubCommittee sub_KHXHNV = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("xh") || o.Resolution.ToLower().Contains("xã hội")) && o.IsActive == true).FirstOrDefault();
                int sub_id_KHXHNV= 0;
                if (sub_KHTN != null)
                {
                    sub_id_KHXHNV = sub_KHXHNV.SubCommitteeID;
                }
                lstSub.Add(sub_id_KHXHNV);
               
               
                SubCommittee sub_CBA = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("cba") || o.Resolution.ToLower().Contains("cơ bản a")) && o.IsActive == true).FirstOrDefault();
                int sub_id_CBA = 0;
                if (sub_CBA != null)
                {
                    sub_id_CBA = sub_CBA.SubCommitteeID;
                }
                lstSub.Add(sub_id_CBA);
                SubCommittee sub_CBB = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("cbb") || o.Resolution.ToLower().Contains("cơ bản b")) && o.IsActive == true).FirstOrDefault();
                int sub_id_CBB = 0;
                if (sub_CBB != null)
                {
                    sub_id_CBB = sub_CBB.SubCommitteeID;
                }
                lstSub.Add(sub_id_CBB);
                SubCommittee sub_CBC = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("cbc") || o.Resolution.ToLower().Contains("cơ bản c")) && o.IsActive == true).FirstOrDefault();
                int sub_id_CBC = 0;
                if (sub_CBC != null)
                {
                    sub_id_CBC = sub_CBC.SubCommitteeID;
                }
                lstSub.Add(sub_id_CBC);
                SubCommittee sub_CBD = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("cbd") || o.Resolution.ToLower().Contains("cơ bản d")) && o.IsActive == true).FirstOrDefault();
                int sub_id_CBD = 0;
                if (sub_KHTN != null)
                {
                    sub_id_CBD = sub_CBD.SubCommitteeID;
                }
                lstSub.Add(sub_id_CBD);
                SubCommittee sub_CB = SubCommitteeBusiness.All.Where(o => (o.Description.ToLower().Contains("cb") || o.Resolution.ToLower().Contains("cơ bản")) && o.IsActive == true).FirstOrDefault();
                int sub_id_CB = 0;
                if (sub_CB != null)
                {
                    sub_id_CB = sub_CB.SubCommitteeID;
                }
                lstSub.Add(sub_id_CB);
                //Lấy xếp loại hạnh kiểm
                var lstPupilRank = (from p in lstPr_Temp
                                   group p by new { p.EducationLevelID, p.ConductLevelID, p.SubCommiteeID, p.Genre } into grp
                                   select new {
                                       EducationlevelID = grp.Key.EducationLevelID,
                                       CondutLevelID = grp.Key.ConductLevelID,
                                       SubCommiteeID = grp.Key.SubCommiteeID,
                                       Genre = grp.Key.Genre,
                                       SumBySubCommitee = grp.Count()
                                   }).ToList();

                                   
                foreach (var edu in lstEdu)
                {
                    var lstPupilRank_Edu_Male = lstPupilRank.Where(o => o.EducationlevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_MALE);
                    var lstPupilRank_Edu_FeMale = lstPupilRank.Where(o => o.EducationlevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    var lstPr_Male = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_MALE);
                    var lstPr_FeMale = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    foreach(var sub in lstSub)
                    {
                        var n_sub_male = lstPr_Male.Where(o => o.SubCommiteeID == sub).Count();
                        var n_sub_female = lstPr_FeMale.Where(o => o.SubCommiteeID == sub).Count();
                        //sheetPL1.SetCellValue(firstRow_PL1a + 8, 2, "Tổng cộng " + edu.Resolution);
                        var good_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).FirstOrDefault();
                        int n_good_male = 0;
                        if (good_male != null)
                        {
                            n_good_male = good_male.SumBySubCommitee;
                        }
                        var fair_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).FirstOrDefault();
                        int n_fair_male = 0;
                        if (fair_male != null)
                        {
                            n_fair_male = fair_male.SumBySubCommitee;
                        }
                        var normal_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).FirstOrDefault();
                        int n_normal_male = 0;
                        if (normal_male != null)
                        {
                            n_normal_male = normal_male.SumBySubCommitee;
                        }
                        var weak_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).FirstOrDefault();
                        int n_weak_male= 0;
                        if (weak_male != null)
                        {
                            n_weak_male = weak_male.SumBySubCommitee;
                        }

                        var good_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).FirstOrDefault();
                        int n_good_female = 0;
                        if(good_female != null)
                        {
                            n_good_female = good_female.SumBySubCommitee;
                        }
                        var fair_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).FirstOrDefault();
                        int n_fair_female = 0;
                        if(fair_female != null)
                        {
                            n_fair_female = fair_female.SumBySubCommitee;
                        }
                        var normal_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).FirstOrDefault();
                        int n_normal_female = 0;
                        if(normal_female != null)
                        {
                            n_normal_female = normal_female.SumBySubCommitee;
                        }
                        var weak_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CondutLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).FirstOrDefault();
                        int n_weak_female = 0;
                        if(weak_female != null)
                        {
                            n_weak_female = weak_female.SumBySubCommitee;
                        }
                        //Nam
                        sheetPL1.SetCellValue(startRow_PL1a, 4, n_sub_male);
                        sheetPL1.SetCellValue(startRow_PL1a, 6, n_good_male);
                        sheetPL1.SetCellValue(startRow_PL1a, 8, n_fair_male);
                        sheetPL1.SetCellValue(startRow_PL1a, 10, n_normal_male);
                        sheetPL1.SetCellValue(startRow_PL1a, 12, n_weak_male);
                        //Nữ
                        sheetPL1.SetCellValue(startRow_PL1a, 5, n_sub_female);
                        sheetPL1.SetCellValue(startRow_PL1a, 7, n_good_female);
                        sheetPL1.SetCellValue(startRow_PL1a, 9, n_fair_female);
                        sheetPL1.SetCellValue(startRow_PL1a, 11, n_normal_female);
                        sheetPL1.SetCellValue(startRow_PL1a, 13, n_weak_female);
                        startRow_PL1a++;
                    }
                    startRow_PL1a += 2;
                }
                oSheetPL1.Delete();
                sheetPL1.Name = "PL 1a";
                #endregion
                #region Tạo Sheet 1b
                IVTWorksheet sheetPL2 = oBook.CopySheetToLast(oSheetPL2);
                string title_PL1b = "XẾP LOẠI HỌC LỰC HỌC SINH " + semester + " NĂM HỌC " + yearTitle;
                sheetPL2.SetCellValue("A2", title_PL1b);
                //Dòng bắt đầu
                int startRow_PL1b = 6;
                var lstPupilRank_HL = (from p in lstPr_Temp
                                    group p by new { p.EducationLevelID, p.CapacityLevelID, p.SubCommiteeID, p.Genre } into grp
                                    select new
                                    {
                                        EducationlevelID = grp.Key.EducationLevelID,
                                        CapacityLevelID = grp.Key.CapacityLevelID,
                                        SubCommiteeID = grp.Key.SubCommiteeID,
                                        Genre = grp.Key.Genre,
                                        SumBySubCommitee = grp.Count()
                                    }).ToList();
                foreach (var edu in lstEdu)
                {
                    var lstPupilRank_Edu_Male = lstPupilRank_HL.Where(o => o.EducationlevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_MALE);
                    var lstPupilRank_Edu_FeMale = lstPupilRank_HL.Where(o => o.EducationlevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    var lstPr_Male = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_MALE);
                    var lstPr_FeMale = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    foreach (var sub in lstSub)
                    {
                        var n_sub_male = lstPr_Male.Where(o => o.SubCommiteeID == sub).Count();
                        var n_sub_female = lstPr_FeMale.Where(o => o.SubCommiteeID == sub).Count();
                        //sheetPL1.SetCellValue(firstRow_PL1a + 8, 2, "Tổng cộng " + edu.Resolution);
                        var excellent_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).FirstOrDefault();
                        int n_excellent_male = 0;
                        if (excellent_male != null)
                        {
                            n_excellent_male = excellent_male.SumBySubCommitee;
                        }
                        var good_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).FirstOrDefault();
                        int n_good_male = 0;
                        if (good_male != null)
                        {
                            n_good_male = good_male.SumBySubCommitee;
                        }
                        var fair_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).FirstOrDefault();
                        int n_fair_male = 0;
                        if (fair_male != null)
                        {
                            n_fair_male = fair_male.SumBySubCommitee;
                        }
                        var normal_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).FirstOrDefault();
                        int n_normal_male = 0;
                        if (normal_male != null)
                        {
                            n_normal_male = normal_male.SumBySubCommitee;
                        }
                        var weak_male = lstPupilRank_Edu_Male.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).FirstOrDefault();
                        int n_weak_male = 0;
                        if (weak_male != null)
                        {
                            n_weak_male = weak_male.SumBySubCommitee;
                        }
                        var excellent_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).FirstOrDefault();
                        int n_excellent_female = 0;
                        if (excellent_female != null)
                        {
                            n_excellent_female = excellent_female.SumBySubCommitee;
                        }
                        var good_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).FirstOrDefault();
                        int n_good_female = 0;
                        if (good_female != null)
                        {
                            n_good_female = good_female.SumBySubCommitee;
                        }
                        var fair_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).FirstOrDefault();
                        int n_fair_female = 0;
                        if (fair_female != null)
                        {
                            n_fair_female = fair_female.SumBySubCommitee;
                        }
                        var normal_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).FirstOrDefault();
                        int n_normal_female = 0;
                        if (normal_female != null)
                        {
                            n_normal_female = normal_female.SumBySubCommitee;
                        }
                        var weak_female = lstPupilRank_Edu_FeMale.Where(o => o.SubCommiteeID == sub && o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).FirstOrDefault();
                        int n_weak_female = 0;
                        if (weak_female != null)
                        {
                            n_weak_female = weak_female.SumBySubCommitee;
                        }
                        
                        //Nam
                        sheetPL2.SetCellValue(startRow_PL1b, 4, n_sub_male);
                        sheetPL2.SetCellValue(startRow_PL1b, 6, n_excellent_male);
                        sheetPL2.SetCellValue(startRow_PL1b, 8, n_good_male);
                        sheetPL2.SetCellValue(startRow_PL1b, 10, n_fair_male);
                        sheetPL2.SetCellValue(startRow_PL1b, 12, n_normal_male);
                        sheetPL2.SetCellValue(startRow_PL1b, 14, n_weak_male);

                        //Nữ
                        sheetPL2.SetCellValue(startRow_PL1b, 5, n_sub_female);
                        sheetPL2.SetCellValue(startRow_PL1b, 7, n_excellent_female);
                        sheetPL2.SetCellValue(startRow_PL1b, 9, n_good_female);
                        sheetPL2.SetCellValue(startRow_PL1b, 11, n_fair_female);
                        sheetPL2.SetCellValue(startRow_PL1b, 13, n_normal_female);
                        sheetPL2.SetCellValue(startRow_PL1b, 15, n_weak_female);
                        startRow_PL1b++;
                    }
                    startRow_PL1b += 2;
                }
                oSheetPL2.Delete();
                sheetPL2.Name = "PL 1b";

                #endregion
                #region Tạo Sheet 1c - 1f
                string reportTitle = "XẾP LOẠI HẠNH KIỂM HỌC SINH HÒA NHẬP, KHUYẾT TẬT " + semester + " NĂM HỌC " + yearTitle;
                IVTWorksheet sheetPL3 = oBook.CopySheetToLast(oSheetPL3, "L5");
                //Điền dữ liệu sheet PL1
                sheetPL3.SetCellValue("A2", reportTitle);
                //Lấy mẫu dòng
                IVTRange rangeTemplatePL3 = oSheetPL3.GetRange("A6", "J6");
                //Hàng tổng cộng
                IVTRange rangeSumPL3 = oSheetPL3.GetRange("A9", "J9");
                int startRow = 6;
                int firstRow_HK = 6;

                int schoolTypeID = SchoolTypeBusiness.All.Where(o => o.Resolution.ToLower().Contains("khuyết tật")).FirstOrDefault().SchoolTypeID;
                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.IsDisabled == true && o.SchoolTypeID != schoolTypeID);
                    sheetPL3.CopyPasteSameSize(rangeTemplatePL3, startRow, 1);
                    sheetPL3.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL1.SetCellValue(startRow, 2, lstPrEdu.Count());
                    sheetPL3.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).Count());
                    sheetPL3.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).Count());
                    sheetPL3.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).Count());
                    sheetPL3.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL3.CopyPasteSameSize(rangeSumPL3, startRow, 1);
                //Tạo công thức
                sheetPL3.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 8; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL3.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                startRow += 3;
                //Xếp loại hạnh kiểm học sinh nữ
                string reportTitle_Female = "XẾP LOẠI HẠNH KIỂM HỌC SINH DÂN TỘC THIỂU SỐ " + semester + " NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Female = oSheetPL3.GetRange("A12", "L15");
                IVTRange rangeTemplate_Ethnic = oSheetPL3.GetRange("A16", "J16");
                IVTRange rangeSumPL3_Ethnic = oSheetPL3.GetRange("A19", "J19");

                sheetPL3.CopyPasteSameSize(rangeTitle_Female, startRow, 1);
                sheetPL3.SetCellValue(startRow, 1, reportTitle_Female);
                startRow += 4;
                int firstRow_HK_Ethnic = startRow;
                Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("kinh")).FirstOrDefault();
                Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("nước ngoài")).FirstOrDefault();
                int? EthnicID_Kinh = 0;
                int? EthnicID_ForeignPeople = 0;
                if (Ethnic_Kinh != null)
                {
                    EthnicID_Kinh = Ethnic_Kinh.EthnicID;
                }
                if (Ethnic_ForeignPeople != null)
                {
                    EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
                }
                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople);
                    sheetPL3.CopyPasteSameSize(rangeTemplate_Ethnic, startRow, 1);
                    sheetPL3.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL1.SetCellValue(startRow, 2, lstPrEdu.Count());
                    sheetPL3.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).Count());
                    sheetPL3.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).Count());
                    sheetPL3.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).Count());
                    sheetPL3.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL3.CopyPasteSameSize(rangeSumPL3_Ethnic, startRow, 1);
                //Tạo công thức
                sheetPL3.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK_Ethnic.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 8; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL3.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK_Ethnic.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                startRow += 3;

                string reportTitle_Disabled_PL3 = "XẾP LOẠI HỌC LỰC HỌC SINH HÒA NHẬP, KHUYẾT TẬT "+ semester + " " + "NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Disabled_PL3 = oSheetPL3.GetRange("A22", "L25");
                IVTRange rangeTemplate_Disabled_PL3 = oSheetPL3.GetRange("A26", "L26");
                IVTRange rangeSumPL3_Disabled_PL3 = oSheetPL3.GetRange("A29", "L29");

                sheetPL3.CopyPasteSameSize(rangeTitle_Disabled_PL3, startRow, 1);
                sheetPL3.SetCellValue(startRow, 1, reportTitle_Disabled_PL3);
                startRow += 4;
                int firstRow_HK_Disabled_PL3 = startRow;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.IsDisabled == true && o.SchoolTypeID != schoolTypeID);
                    sheetPL3.CopyPasteSameSize(rangeTemplate_Disabled_PL3, startRow, 1);
                    sheetPL3.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL2.SetCellValue(startRow_PL2, 2, lstPrEdu.Count());
                    sheetPL3.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                    sheetPL3.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                    sheetPL3.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                    sheetPL3.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                    sheetPL3.SetCellValue(startRow, 11, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL3.CopyPasteSameSize(rangeSumPL3_Disabled_PL3, startRow, 1);
                //Tạo công thức
                sheetPL3.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK_Disabled_PL3.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 10; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL3.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK_Disabled_PL3.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                startRow += 3;

                //Xếp loại hạnh kiểm học sinh dân tộc thiểu số
                string reportTitle_Ethnic_PL3 = "XẾP LOẠI HỌC LỰC HỌC SINH DÂN TỘC THIỂU SỐ " + semester + " " + "NĂM HỌC " + yearTitle;
                IVTRange rangeTitle_Ethnic_PL3 = oSheetPL3.GetRange("A32", "L35");
                IVTRange rangeTemplate_Ethnic_PL3 = oSheetPL3.GetRange("A36", "L36");
                IVTRange rangeSumPL3_Ethnic_PL3 = oSheetPL3.GetRange("A39", "L39");

                sheetPL3.CopyPasteSameSize(rangeTitle_Ethnic_PL3, startRow, 1);
                sheetPL3.SetCellValue(startRow, 1, reportTitle_Ethnic_PL3);
                startRow += 4;
                int firstRow_HK_Ethnic_PL3 = startRow;

                foreach (var edu in lstEdu)
                {
                    var lstPrEdu = lstPr.Where(o => o.EducationLevelID == edu.EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople);
                    sheetPL3.CopyPasteSameSize(rangeTemplate_Ethnic_PL3, startRow, 1);
                    sheetPL3.SetCellValue(startRow, 1, edu.Resolution);
                    //sheetPL2.SetCellValue(startRow_PL2, 2, lstPrEdu.Count());
                    sheetPL3.SetCellValue(startRow, 3, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                    sheetPL3.SetCellValue(startRow, 5, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                    sheetPL3.SetCellValue(startRow, 7, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                    sheetPL3.SetCellValue(startRow, 9, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                    sheetPL3.SetCellValue(startRow, 11, lstPrEdu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                    startRow++;
                }
                //Tạo hàng tính tổng
                sheetPL3.CopyPasteSameSize(rangeSumPL3_Ethnic_PL3, startRow, 1);
                //Tạo công thức
                sheetPL3.SetCellValue(startRow, 2, "=SUM(B" + firstRow_HK_Ethnic_PL3.ToString() + ":B" + (startRow - 1).ToString() + ")");
                for (int j = 0; j < 10; j += 2)
                {
                    string alphabet = GetExcelColumnName(j + 3);
                    sheetPL3.SetCellValue(startRow, j + 3, "=SUM(" + alphabet + firstRow_HK_Ethnic_PL3.ToString() + ":" + alphabet + (startRow - 1).ToString() + ")");
                }
                oSheetPL3.Delete();
                sheetPL3.Name = "PL 1c-1f";
                #endregion
                #region Tạo Sheet PL 3a-3b
                //Lấy dữ liệu học sinh trong lớp
                //IDictionary<string, object> dicPoc = new Dictionary<string, object>();
                //dicPoc["AcademicYearID"] = entity.AcademicYearID;
                //dicPoc["SchoolID"] = entity.SchoolID;
                //dicPoc["Check"] = "Check";
                //dicPoc["AppliedLevel"] = entity.AppliedLevel;
                //IQueryable<PupilOfClass> lstPoc_Temp = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPoc);
                var iqPoc = (from p in lstPocTemp.AddCriteriaSemester(academicYear, entity.Semester)
                                           join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                           join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                           where q.IsActive.Value
                                           select new
                                           {
                                               PupilID = p.PupilID,
                                               ClassID = p.ClassID,
                                               EducationLevelID = q.EducationLevelID,
                                               Genre = r.Genre
                                           }).ToList();
                List<PupilPleSendSoGDBO> iqResult = (from p in lstPocMaxAssignDate
                                                     join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                     join s in ClassProfileBusiness.All on p.ClassID equals s.ClassProfileID
                                                     join pos in PupilOfSchoolBusiness.All.Where(o => o.SchoolID == entity.SchoolID) on p.PupilID equals pos.PupilID
                                                     join r in PupilLeavingOffBusiness.All.Where(o => o.SchoolID == entity.SchoolID && o.AcademicYearID == entity.AcademicYearID) on p.PupilID equals  r.PupilID  into g1
                                                     from j1 in g1.DefaultIfEmpty()
                                                     join t in SchoolMovementBusiness.All.Where(o => o.SchoolID == entity.SchoolID && o.AcademicYearID == entity.AcademicYearID) on p.PupilID  equals t.PupilID  into g2
                                                     from j2 in g2.DefaultIfEmpty()
                                                     where s.IsActive.Value
                                                     select new PupilPleSendSoGDBO
                                                     {
                                                         PupilID = p.PupilID,
                                                         ClassID = p.ClassID,
                                                         EducationLevelID = s.EducationLevelID,
                                                         PupilLeavingOffID = j1.PupilLeavingOffID,
                                                         LeavingReasonID = j1.LeavingReasonID,
                                                         LeavingDate = j1.LeavingDate,
                                                         LeavingReasonName = j1.LeavingReason.Resolution,
                                                         SchoolMovementID = j2.SchoolMovementID,
                                                         MovedDate = j2.MovedDate,
                                                         EnrolmentType = pos.EnrolmentType,
                                                         Genre = q.Genre,
                                                         EnrolmentDate = pos.EnrolmentDate
                                                     }).ToList();
                List<PupilPleSendSoGDBO> lst_LeavingOff = iqResult.Where(o => o.LeavingReasonID.HasValue).ToList();
                List<PupilPleSendSoGDBO> lst_School_Moved = iqResult.Where(o => o.SchoolMovementID.HasValue).ToList();
                List<PupilPleSendSoGDBO> lst_Enrolment_Type = new List<PupilPleSendSoGDBO>();
                DateTime firstStartSemester = academicYear.FirstSemesterStartDate.Value.Date;
                DateTime firstEndSemester = academicYear.FirstSemesterEndDate.Value.Date;
                DateTime secondStartSemester = academicYear.SecondSemesterStartDate.Value.Date;
                DateTime secondEndSemester = academicYear.SecondSemesterEndDate.Value.Date;

                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    lst_LeavingOff = lst_LeavingOff.Where(o => o.LeavingDate.HasValue && o.LeavingDate >= firstStartSemester && o.LeavingDate <= firstEndSemester).ToList();
                    lst_School_Moved = lst_School_Moved.Where(o => o.MovedDate.HasValue && o.MovedDate >= firstStartSemester && o.MovedDate <= firstEndSemester).ToList();
                    lst_Enrolment_Type = iqResult.Where(o => o.EnrolmentDate.HasValue && o.EnrolmentDate >= firstStartSemester && o.EnrolmentDate <= firstEndSemester &&  o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).ToList();
                }
                else
                {
                    lst_LeavingOff = lst_LeavingOff.Where(o => o.LeavingDate.HasValue && o.LeavingDate >= firstStartSemester && o.LeavingDate <= secondEndSemester).ToList();
                    lst_School_Moved = lst_School_Moved.Where(o => o.MovedDate.HasValue && o.MovedDate >= firstStartSemester && o.MovedDate <= secondEndSemester).ToList();
                    lst_Enrolment_Type = iqResult.Where(o => o.EnrolmentDate.HasValue && o.EnrolmentDate >= firstStartSemester && o.EnrolmentDate <= secondEndSemester && o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).ToList();
                }

                IVTWorksheet sheetPL4 = oBook.CopySheetToLast(oSheetPL4, "J5");
                string title_PL4 = "TÌNH HÌNH HỌC SINH THÔI HỌC " + semester + " NĂM HỌC " + yearTitle;
                string title_end_Year = "Số HS cuối " + semester + " năm học " + yearTitle + " (B)";
                string title_start_Year = "Tổng số HS đầu năm học " + yearTitle + " (A)";
                sheetPL4.SetCellValue("A2", title_PL4);
                sheetPL4.SetCellValue("C4", title_end_Year);
                sheetPL4.SetCellValue("B4", title_start_Year);
                int startRow_PL4 = 6;
                int firstRow_PL4 = 6;
                //phân tích nguyên nhân thôi học
               // IQueryable<LeavingReason> lstLeavingReason = LeavingReasonBusiness.All;
                //LeavingReason reason_capa_weak = lstLeavingReason.Where(o => o.Resolution.ToLower().Contains("học lực yếu")).FirstOrDefault();
                int reason_id_capa_weak = 4;
                //if (reason_capa_weak != null)
                //{
                //    reason_id_capa_weak = reason_capa_weak.LeavingReasonID;
                //}
                //LeavingReason reason_dif = lstLeavingReason.Where(o => o.Resolution.ToLower().Contains("hoàn cảnh khó khăn")).FirstOrDefault();
                int reason_id_dif = 1;
                //if (reason_dif != null)
                //{
                //    reason_id_dif = reason_dif.LeavingReasonID;
                //}
                //LeavingReason reason_far_house = lstLeavingReason.Where(o => o.Resolution.ToLower().Contains("xa nhà")).FirstOrDefault();
                int reason_id_far_house = 2;
                //if (reason_far_house != null)
                //{
                //    reason_id_far_house = reason_far_house.LeavingReasonID;
                //}
                IVTRange range_Edu = oSheetPL4.GetRange("A6", "J6");
                IVTRange range_Edu_Sum = oSheetPL4.GetRange("A9", "J9");
                foreach (var edu in lstEdu)
                {
                    var iqResultEdu = iqPoc.Where(o => o.EducationLevelID == edu.EducationLevelID);
                    var lst_LeavingOffEdu = lst_LeavingOff.Where(o => o.EducationLevelID == edu.EducationLevelID);
                    var lst_School_MovedEdu = lst_School_Moved.Where(o => o.EducationLevelID == edu.EducationLevelID);
                    int number_reason_id_capa_weak = lst_LeavingOff.Where(o => o.LeavingReasonID.HasValue && o.EducationLevelID == edu.EducationLevelID && o.LeavingReasonID == reason_id_capa_weak).Count();
                    int number_reason_id_dif = lst_LeavingOff.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_dif && o.EducationLevelID == edu.EducationLevelID).Count();
                    int number_reason_id_far_house = lst_LeavingOff.Where(o => o.EducationLevelID == edu.EducationLevelID && o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_far_house).Count();
                    sheetPL4.CopyPasteSameSize(range_Edu, startRow_PL4, 1);

                    sheetPL4.SetCellValue(startRow_PL4, 1, edu.Resolution);
                    //sheetPL4.SetCellValue(startRow_PL3, 2, iqResultEdu.Count() + lst_School_MovedEdu.Count() + lst_LeavingOff.Count() - iqResult.Where(o => o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).Count());
                    sheetPL4.SetCellValue(startRow_PL4, 3, iqResultEdu.Count());
                    sheetPL4.SetCellValue(startRow_PL4, 4, lst_Enrolment_Type.Where(o => o.EducationLevelID == edu.EducationLevelID).Count());
                    sheetPL4.SetCellValue(startRow_PL4, 5, lst_School_MovedEdu.Count());
                    sheetPL4.SetCellValue(startRow_PL4, 6, lst_LeavingOffEdu.Count());
                    sheetPL4.SetCellValue(startRow_PL4, 7, number_reason_id_capa_weak);
                    sheetPL4.SetCellValue(startRow_PL4, 8, number_reason_id_dif);
                    sheetPL4.SetCellValue(startRow_PL4, 9, number_reason_id_far_house);
                    sheetPL4.SetCellValue(startRow_PL4, 10, lst_LeavingOffEdu.Count() - number_reason_id_capa_weak - number_reason_id_dif - number_reason_id_far_house);
                    startRow_PL4++;
                }
                //Tạo dòng tổng
                sheetPL4.CopyPasteSameSize(range_Edu_Sum, startRow_PL4, 1);
                for (int j = 0; j < 9; j++)
                {
                    string alphabet = GetExcelColumnName(j + 2);
                    sheetPL4.SetCellValue(startRow_PL4, j + 2, "=SUM(" + alphabet + firstRow_PL4.ToString() + ":" + alphabet + (startRow_PL4 - 1).ToString() + ")");
                }


                startRow_PL4 += 5;
                //tạo dòng mẫu nữ học sinh
                IVTRange range_Female = oSheetPL4.GetRange("A14", "J17");
                sheetPL4.CopyPasteSameSize(range_Female, startRow_PL4, 1);
                string title_female = "TÌNH HÌNH HỌC SINH NỮ THÔI HỌC " + semester + " NĂM HỌC " + yearTitle;
                string title_end_year_female = "";
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    title_end_year_female = "Số HS NỮ cuối " + semester + " năm học " + yearTitle + " (B)";
                }
                else
                {
                    title_end_year_female = "Số HS NỮ cuối năm " + yearTitle + " (B)";
                }
                string title_start_year_female = "Tổng số HS NỮ đầu năm học " + yearTitle + " (A)";
                sheetPL4.SetCellValue(startRow_PL4, 1, title_female);
                startRow_PL4 += 2;
                sheetPL4.SetCellValue(startRow_PL4, 3, title_end_year_female);
                sheetPL4.SetCellValue(startRow_PL4, 2, title_start_year_female);
                startRow_PL4 += 2;

                //Dòng mẫu
                IVTRange range_PL4_Female = oSheetPL4.GetRange("A18", "J18");
                IVTRange range_PL4_Sum_Femlate = oSheetPL4.GetRange("A21", "J21");
                int first_Row_PL4_Female = startRow_PL4;
                foreach (var edu in lstEdu)
                {
                    var iqResultEdu = iqPoc.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    var lst_LeavingOffEdu = lst_LeavingOff.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    var lst_School_MovedEdu = lst_School_Moved.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                    int number_reason_id_capa_weak = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_capa_weak).Count();
                    int number_reason_id_dif = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_dif).Count();
                    int number_reason_id_far_house = lst_LeavingOffEdu.Where(o => o.LeavingReasonID.HasValue && o.LeavingReasonID == reason_id_far_house).Count();
                    sheetPL4.CopyPasteSameSize(range_PL4_Female, startRow_PL4, 1);
                    sheetPL4.SetCellValue(startRow_PL4, 1, edu.Resolution);
                    //sheetPL4.SetCellValue(startRow_PL3, 2, iqResultEdu.Count() + lst_School_MovedEdu.Count() + lst_LeavingOff.Count() - iqResult.Where(o => o.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL).Count());
                    sheetPL4.SetCellValue(startRow_PL4, 3, iqResultEdu.Count());
                    sheetPL4.SetCellValue(startRow_PL4, 4, lst_Enrolment_Type.Where(o => o.EducationLevelID == edu.EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE).Count());
                    sheetPL4.SetCellValue(startRow_PL4, 5, lst_School_MovedEdu.Count());
                    sheetPL4.SetCellValue(startRow_PL4, 6, lst_LeavingOffEdu.Count());
                    sheetPL4.SetCellValue(startRow_PL4, 7, number_reason_id_capa_weak);
                    sheetPL4.SetCellValue(startRow_PL4, 8, number_reason_id_dif);
                    sheetPL4.SetCellValue(startRow_PL4, 9, number_reason_id_far_house);
                    sheetPL4.SetCellValue(startRow_PL4, 10, lst_LeavingOffEdu.Count() - number_reason_id_capa_weak - number_reason_id_dif - number_reason_id_far_house);
                    startRow_PL4++;
                }
                //Tạo dòng tổng
                sheetPL4.CopyPasteSameSize(range_PL4_Sum_Femlate, startRow_PL4, 1);
                for (int j = 0; j < 9; j++)
                {
                    string alphabet = GetExcelColumnName(j + 2);
                    sheetPL4.SetCellValue(startRow_PL4, j + 2, "=SUM(" + alphabet + first_Row_PL4_Female.ToString() + ":" + alphabet + (startRow_PL4 - 1).ToString() + ")");
                }

                oSheetPL4.Delete();
                sheetPL4.Name = "PL 2a-2b";

                #endregion
            }
            return oBook.ToStream();
        }
        public ProcessedReport ExcelInsertReportPreliminarySendSGD(ReportPreliminaryBO entity, Stream data)
        {
            //Tạo và insert vào bảng ProcessedReport
            string reportCode = "";
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                reportCode = SystemParamsInFile.REPORT_SO_KET_GUI_SOGD;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_SO_KET_GUI_SOGD_THPT;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);
            //Tạo tên file GV_[SchoolLevel]_BC_PhanCongGiangDay_[EducationLevel/Class]_[Semester]
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            //Insert vào bảng ProcessedReportParameter
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            //Insert vào bảng ProcessedReport
            ProcessedReportBusiness.Insert(pr);
            ProcessedReportBusiness.Save();
            return pr;
        }
        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
    }
}
