/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class QuickHintBusiness
    {
        public List<QuickHint> GetListQuickHintByUserAccount(int userAccountId, string functionCode)
        {
            return this.repository.All.Where(o => o.UserAccountID == userAccountId && o.FunctionCode == functionCode).ToList();
        }
        public void InsertOrUpdate(IDictionary<string, object> dic, List<QuickHint> lstQuickHint,bool isImport = false)
        {
            int TypeID = Utils.GetInt(dic, "TypeID");
            string FunctionCode = Utils.GetString(dic, "FunctionCode");
            int UserAccountID = Utils.GetInt(dic, "UserAccountID");
            List<QuickHint> lstQuickHintDB = QuickHintBusiness.All.Where(p => p.FunctionCode.Equals(FunctionCode) && !p.IsSystemHint && p.UserAccountID == UserAccountID && p.TypeID == TypeID).ToList();
            QuickHint objQH = null;
            List<QuickHint> lstInsert = new List<QuickHint>();
            QuickHint objInsert = null;
            for (int i = 0; i < lstQuickHint.Count; i++)
            {
                objInsert = lstQuickHint[i];
                if (isImport)
                {
                    objQH = lstQuickHintDB.Where(p => p.HintCode.Equals(objInsert.HintCode)).FirstOrDefault();
                    if (objQH != null)
                    {
                        objQH.Content = objInsert.Content;
                    }
                    else
                    {
                        lstInsert.Add(objInsert);
                    }
                }
                else
                {
                    objQH = lstQuickHintDB.Where(p => p.QuickHintID == objInsert.QuickHintID).FirstOrDefault();
                    if (objQH != null)
                    {
                        objQH.HintCode = objInsert.HintCode;
                        objQH.Content = objInsert.Content;
                    }
                    else
                    {
                        lstInsert.Add(objInsert);
                    }
                }
                
                
            }
            if (lstInsert.Count > 0)
            {
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    objInsert = lstInsert[i];
                    QuickHintBusiness.Insert(objInsert);
                }
            }
            QuickHintBusiness.Save();
        }
        public void DeleteQuickHint(IDictionary<string, object> dic)
        {
            int TypeID = Utils.GetInt(dic, "TypeID");
            string FunctionCode = Utils.GetString(dic, "FunctionCode");
            List<int> lstQuickHintID = Utils.GetIntList(dic,"lstQuicHintID");
            List<QuickHint> lstQuickHintDB = this.repository.All.Where(p => p.FunctionCode.Equals(FunctionCode) && !p.IsSystemHint && lstQuickHintID.Contains(p.QuickHintID) && p.TypeID == TypeID).ToList();
            QuickHintBusiness.DeleteAll(lstQuickHintDB);
            QuickHintBusiness.Save();
        }
    }
}
