﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class FlowSituationBusiness
    {
        public ReportDefinition GetReportDefinitionOfClassMovement(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }

        public ReportDefinition GetReportDefinitionOfSchoolMovement(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_TRUONG_A4;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }

        public ReportDefinition GetReportDefinitionOfMovementAcceptance(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_DEN_A4;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }

        public ReportDefinition GetReportDefinitionOfPupilLeavingOff(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_THOI_HOC_A4;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }

        public ReportDefinition GetReportDefinitionOfPolicyTarget(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_DIEN_CHINH_SACH_A4;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }

        public string GetHashKeyForPeriod(FlowSituationBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        //HỌc sinh chuyển lớp
        public ProcessedReport ExcelGetClassMovement(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4;
            string inputParameterHashKey = GetHashKeyForPeriod(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport ExcelInsertClassMovement(FlowSituationBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForPeriod(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);

            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);


            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);


            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},

                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream ExcelCreateClassMovement(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            //Tạo dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string semester = SMASConvert.ConvertSemester(entity.Semester).ToUpper();
            DateTime reportDate = DateTime.Now;
            string provinceName = (school.District != null ? school.District.DistrictName : "");

            int firstRow = 9;
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "H" + firstRow);
            sheet.Name = "TKHSChuyenLop";
            //Template Dòng đầu tiên
            IVTRange topRow = firstSheet.GetRange("A10", "H10");
            //Template dòng giữa
            IVTRange middleRow = firstSheet.GetRange("A11", "H11");
            //Template dòng cuối
            IVTRange bottomRow = firstSheet.GetRange("A12", "H12");
            //Template để mergeRow người lập báo cáo
            IVTRange mergeRow = firstSheet.GetRange("G15", "H15");
            int currentRow = 0;

            List<object> list = new List<object>();
            int order = 1;
            Dictionary<string, object> dic = new Dictionary<string, object>
                {
                    {"SchoolName", schoolName},
                    {"SupervisingDeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            AcademicYear aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            DateTime fromDate = ((entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) || (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)) ? aca.FirstSemesterStartDate.Value : aca.SecondSemesterStartDate.Value;
            DateTime toDate = entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? aca.FirstSemesterEndDate.Value : aca.SecondSemesterEndDate.Value;

            //Chiendd: 22/09/2014: Sua lai cau query
            //Danh sách học sinh chuyển lớp

            List<ClassMovementBO> lstEnd = (from p in ClassMovementBusiness.All
                                            join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                            join r in ClassProfileBusiness.All on p.ClassProfile.ClassProfileID equals r.ClassProfileID
                                            join s in ClassProfileBusiness.All on p.ClassProfile1.ClassProfileID equals s.ClassProfileID
                                            where p.SchoolID == entity.SchoolID && p.AcademicYearID == entity.AcademicYearID
                                            && p.MovedDate >= fromDate && p.MovedDate <= toDate && q.IsActive
                                                                                        && p.EducationLevel.Grade == entity.AppliedLevel
                                                                                        && r.IsActive.Value
                                                                                        && s.IsActive == true
                                            select new ClassMovementBO
                                            {
                                                ClassMovementID = p.ClassMovementID,
                                                oldClass = r.DisplayName,
                                                newClass = s.DisplayName,
                                                MovedDate = p.MovedDate,
                                                Reason = p.Reason,
                                                FullName = q.FullName,
                                                BirthDate = q.BirthDate,
                                                Genre = q.Genre,
                                                EducationLevelID = p.EducationLevelID,
                                                ClassOrder = (s.OrderNumber.HasValue) ? s.OrderNumber.Value : 0,
                                                Name = q.Name
                                            }).OrderBy(c => c.EducationLevelID)
                                            .ThenBy(c => c.ClassOrder)
                                            .ThenBy(c => c.newClass)
                                            .ThenBy(c => c.Name)
                                            .ThenBy(c => c.FullName)
                                            .ToList();

            foreach (ClassMovementBO classMovement in lstEnd)
            {
                currentRow = 9 + order;
                if (order == 1 && lstEnd.Count() == 1)
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }
                else if (order == 1)
                {
                    sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                }
                else if (order == lstEnd.Count())
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }
                else
                {
                    sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                }
                //IDictionary<string, object> dic1 = new Dictionary<string, object>();
                //dic1["ClassProfileID"] = classMovement.FromClassID;

                //IDictionary<string, object> dic2 = new Dictionary<string, object>();
                //dic2["ClassProfileID"] = classMovement.ToClassID;

                //ClassProfile oldClass = ClassProfileBusiness.Find(classMovement.FromClassID);
                //ClassProfile newClass = ClassProfileBusiness.Find(classMovement.ToClassID);
                list.Add(new Dictionary<string, object>
                        {
                            {"Order", order++},
                            {"PupilName", classMovement.FullName},
                            {"BirthDate", String.Format("{0:dd/MM/yyyy}", classMovement.BirthDate)},
                            {"Genre", SMASConvert.ConvertGenre(classMovement.Genre)},
                            {"OldClass", classMovement.oldClass != null ? classMovement.oldClass : ""},
                            {"NewClass",  classMovement.newClass != null ? classMovement.newClass : ""},
                            {"MovedDate",String.Format("{0:dd/MM/yyyy}", classMovement.MovedDate)},
                            {"Reason", classMovement.Reason}
                        });
            }

            dic.Add("list", list);
            //Fill dữ liệu
            sheet.FillVariableValue(dic);
            sheet.CopyPasteSameSize(mergeRow, 11 + order, 7);
            sheet.SetCellValue(11 + order, 7, "Người lập báo cáo");
            sheet.FitSheetOnOnePage = true;
            firstSheet.Delete();
            return oBook.ToStream();
        }

        //HỌc sinh chuyển trường
        public ProcessedReport ExcelGetSchoolMovement(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_TRUONG_A4;
            string inputParameterHashKey = GetHashKeyForPeriod(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport ExcelInsertSchoolMovement(FlowSituationBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_TRUONG_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForPeriod(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);

            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);


            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);


            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},

                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream ExcelCreateSchoolMovement(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_TRUONG_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 9;
            //Lấy sheet TKHS chuyển trường
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "G" + firstRow);
            sheet.Name = "TKHSChuyenTruong";
            //Template dòng trên cùng
            IVTRange topRow = firstSheet.GetRange("A10", "G10");
            //dòng dưới cùng của mẫu báo cáo
            IVTRange bottomRow = firstSheet.GetRange("A11", "G11");
            //Dòng để merge
            IVTRange mergeRow = firstSheet.GetRange("F13", "G13");

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string semester = SMASConvert.ConvertSemester(entity.Semester).ToUpper();
            DateTime reportDate = DateTime.Now;
            List<object> list = new List<object>();
            int order = 1;
            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                {"SupervisingDeptName", supervisingDeptName},
                {"SchoolName", schoolName},
                {"ProvinceName", provinceName},
                {"ReportDate", reportDate},
                {"Semester", semester}
            };

            //Danh sách học sinh chuyển trường
            //List<SchoolMovement> listSchoolMovement = SchoolMovementBusiness.GetListSchoolMovement(entity.AcademicYearID, entity.SchoolID, entity.Semester);

            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = entity.AcademicYearID;
            search["Semester"] = entity.Semester;
            search["AppliedLevel"] = entity.AppliedLevel;

            ///Chiendd:22/09/2014 Bo sung order STT
            IQueryable<SchoolMovement> lsSchoolMovement = SchoolMovementBusiness.SearchBySchool(entity.SchoolID, search);
            IDictionary<string, object> dicLeaving = new Dictionary<string, object>();
            dicLeaving["AcademicYearID"] = entity.AcademicYearID;
            dicLeaving["SchoolID"] = entity.SchoolID;
            dicLeaving["Semester"] = entity.Semester;
            dicLeaving["Check"] = "Check";
            IQueryable<PupilOfClass> iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicLeaving);
            if (lsSchoolMovement != null && lsSchoolMovement.Count() > 0)
            {
                IDictionary<string, object> dic1 = new Dictionary<string, object>();
                var listSchoolMovement = (from sm in lsSchoolMovement
                                          join cp in iqPupilOfClass on new { sm.PupilID, sm.ClassID } equals new { cp.PupilID, cp.ClassID }
                                          join cl in ClassProfileBusiness.All on sm.ClassID equals cl.ClassProfileID
                                          join pp in PupilProfileBusiness.All on sm.PupilID equals pp.PupilProfileID
                                          where cl.IsActive.Value
                                          select new
                                          {
                                              PupilID = sm.PupilID,
                                              EducationLevelID = cl.EducationLevelID,
                                              ClassID = sm.ClassID,
                                              ClassName = cl.DisplayName,
                                              OrderNumber = cl.OrderNumber,
                                              BirthDate = pp.BirthDate,
                                              Genre = pp.Genre,
                                              MovedDate = sm.MovedDate,
                                              OrderInClass = cp.OrderInClass,
                                              Name = pp.Name,
                                              FullName = pp.FullName,
                                              Description = sm.Description
                                          }).ToList();
                listSchoolMovement = listSchoolMovement.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                foreach (var schoolMovement in listSchoolMovement)
                {
                    int currentRow = firstRow + order;
                    if (order == listSchoolMovement.Count())
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object>
                    {
                        {"Order", order++},
                        {"PupilName", schoolMovement.FullName},
                        {"BirthDate", String.Format("{0:dd/MM/yyyy}", schoolMovement.BirthDate)},
                        {"Genre", SMASConvert.ConvertGenre(schoolMovement.Genre)},
                        {"Class", schoolMovement.ClassName},
                        {"MovedDate", String.Format("{0:dd/MM/yyyy}", schoolMovement.MovedDate)},
                        {"Description",schoolMovement.Description}
                    });

                }
            }
            dic.Add("list", list);
            //Fill dữ liệu
            sheet.FillVariableValue(dic);
            //VTVector lastRow = new VTVector(countRow + 2, 0);
            sheet.CopyPasteSameSize(mergeRow, firstRow + order, 6);
            sheet.SetCellValue(firstRow + order, 6, "Người lập báo cáo");
            sheet.FitSheetOnOnePage = true;
            firstSheet.Delete();
            return oBook.ToStream();

        }
        #region
        //HỌc sinh chuyển đến
        /// <summary>
        /// Lấy danh sách học sinh chuyển đến được cập nhật mới nhất
        /// Author: Tamhm1
        /// Date: 21/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport ExcelGetMovementAcceptance(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_DEN_A4;
            string inputParameterHaskKey = GetHashKeyForPeriod(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHaskKey);
        }
        /// <summary>
        /// Lưu lại thông tin thống kê học sinh chuyển đến
        /// Author: Tamhm1
        /// Date: 21/11/2012
        /// </summary>
        /// <param name="flowSituationBO"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport ExcelInsertMovementAcceptance(FlowSituationBO flowSituationBO, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_DEN_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport processedReport = new ProcessedReport();
            processedReport.ReportCode = reportCode;
            processedReport.ProcessedDate = DateTime.Now;
            processedReport.InputParameterHashKey = GetHashKeyForPeriod(flowSituationBO);
            processedReport.ReportData = ReportUtils.Compress(data);

            //Tạo tên file 
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(flowSituationBO.AppliedLevel);

            string semester = ReportUtils.ConvertSemesterForReportName(flowSituationBO.Semester);


            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);


            processedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", flowSituationBO.AcademicYearID},
                {"SchoolID", flowSituationBO.SchoolID},
                {"AppliedLevel", flowSituationBO.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, processedReport);
            ProcessedReportRepository.Insert(processedReport);
            ProcessedReportRepository.Save();
            return processedReport;
        }

        public Stream ExcelCreateMovementAcceptance(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_DEN_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 9;
            //Lấy sheet hoc sinh chuyen den
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "H" + firstRow);
            sheet.Name = "TKHSChuyenDen";

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            sheet.SetCellValue("A3", school.SchoolName.ToUpper());
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper());
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string date = provinceName + ", " + " ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            sheet.SetCellValue("F4", date);
            sheet.SetCellValue("D7", SMASConvert.ConvertSemester(entity.Semester).ToUpper());

            VTVector cellOrderNumber = new VTVector("A10");
            VTVector cellFullName = new VTVector("B10");
            VTVector cellBirthDate = new VTVector("C10");
            VTVector cellGenre = new VTVector("D10");
            VTVector cellSchool = new VTVector("E10");
            VTVector cellClass = new VTVector("F10");
            VTVector cellMovedDate = new VTVector("G10");
            VTVector cellNote = new VTVector("H10");
            int countRow = 0;
            IVTRange topRow = firstSheet.GetRange("A10", "H10");
            IVTRange middleRow = firstSheet.GetRange("A11", "H11");
            IVTRange bottomRow = firstSheet.GetRange("A12", "H12");
            IVTRange mergeRow = firstSheet.GetRange("G15", "H15");
            //List<MovementAcceptance> listMovementAcceptance = MovementAcceptanceBusiness.GetListMovementAcceptance(entity.AcademicYearID, entity.SchoolID, entity.Semester);
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = entity.AcademicYearID;
            search["AppliedLevel"] = entity.AppliedLevel;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                search["Semester"] = entity.Semester;
            }

            AcademicYear objAcademicYear = AcademicYearBusiness.Find(entity.AcademicYearID);

            IQueryable<MovementAcceptance> iqMovementAcceptance = MovementAcceptanceBusiness.SearchBySchool(entity.SchoolID, search);
            //lay danh sach nhung hoc sinh co trang thai chuyen den tu truong khac
            IQueryable<PupilOfSchool> iqMA1 = (from pf in PupilProfileBusiness.All
                                               join pos in PupilOfSchoolBusiness.All on pf.PupilProfileID equals pos.PupilID
                                               join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where pos.SchoolID == entity.SchoolID
                                               && poc.SchoolID == entity.SchoolID
                                               && poc.AcademicYearID == entity.AcademicYearID
                                               && pf.IsActive
                                               && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                               && cp.IsActive.HasValue && cp.IsActive.Value
                                               && pos.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL
                                               select pos);
            //IQueryable<MovementAcceptanceBO> lsMovementAcceptance = iqMovementAcceptance.Union(iqMA1);
            //Lay danh sach hoc sinh dang hoc hoac da tot nghiep

            //Chi lay cac ngay chuyen den trong hoc ky
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqMA1 = iqMA1.Where(s => s.EnrolmentDate >= objAcademicYear.FirstSemesterStartDate && s.EnrolmentDate <= objAcademicYear.FirstSemesterEndDate);
            }
            else if(entity.Semester==GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                iqMA1 = iqMA1.Where(s => s.EnrolmentDate >= objAcademicYear.SecondSemesterStartDate && s.EnrolmentDate <= objAcademicYear.SecondSemesterEndDate);
            }
            else
            {
                iqMA1 = iqMA1.Where(s => s.EnrolmentDate >= objAcademicYear.FirstSemesterStartDate && s.EnrolmentDate <= objAcademicYear.SecondSemesterEndDate);
            }

            IDictionary<string, object> dicPOC = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel}
            };
            IQueryable<PupilOfClass> iqPupilOfClass = PupilOfClassBusiness.Search(dicPOC);
            List<int> listValidStatus = new List<int> {
                SystemParamsInFile.PUPIL_STATUS_STUDYING,
                SystemParamsInFile.PUPIL_STATUS_GRADUATED
            };
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqPupilOfClass = iqPupilOfClass.Where(x => x.AssignedDate <= objAcademicYear.FirstSemesterEndDate
                && (listValidStatus.Contains(x.Status)
                || x.EndDate > objAcademicYear.FirstSemesterEndDate));
            }
            else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                iqPupilOfClass = iqPupilOfClass.Where(p => p.AssignedDate >= objAcademicYear.SecondSemesterStartDate && p.AssignedDate <= objAcademicYear.SecondSemesterEndDate);
            }
            else
            {
                iqPupilOfClass = iqPupilOfClass.Where(p => p.AssignedDate <= objAcademicYear.SecondSemesterEndDate);
            }

            List<MovementAcceptanceBO> listMovementAcceptance1 = (from sm in iqMovementAcceptance
                                                                  join cp in iqPupilOfClass on sm.PupilID equals cp.PupilID
                                                                  join cl in ClassProfileBusiness.All on sm.ClassID equals cl.ClassProfileID
                                                                  join pp in PupilProfileBusiness.All on sm.PupilID equals pp.PupilProfileID
                                                                  join sp in SchoolProfileBusiness.All on sm.MovedFromSchoolID equals sp.SchoolProfileID
                                                                  where cl.IsActive.Value
                                                                  && pp.IsActive
                                                                  && cl.AcademicYearID == objAcademicYear.AcademicYearID
                                                                  select new MovementAcceptanceBO
                                                                  {
                                                                      PupilID = sm.PupilID,
                                                                      ClassID = sm.ClassID,
                                                                      EducationLeveID = cl.EducationLevelID,
                                                                      OrderNumber = cl.OrderNumber,
                                                                      DisplayName = cl.DisplayName,
                                                                      Name = pp.Name,
                                                                      FullName = pp.FullName,
                                                                      OrderInClass = cp.OrderInClass,
                                                                      Genre = pp.Genre,
                                                                      BirthDate = pp.BirthDate,
                                                                      MovedFromSchoolID = sm.MovedFromSchoolID,
                                                                      SchoolName = sp.SchoolName,
                                                                      MovedDate = sm.MovedDate,
                                                                      Description = sm.Description
                                                                  }).ToList();
            List<int> lstMID = listMovementAcceptance1.Select(p => p.PupilID).Distinct().ToList();

            List<MovementAcceptanceBO> listMovementAcceptance2 = (from sm in iqMA1
                                                                  join cp in iqPupilOfClass on sm.PupilID equals cp.PupilID
                                                                  join cl in ClassProfileBusiness.All on cp.ClassID equals cl.ClassProfileID
                                                                  join pp in PupilProfileBusiness.All on sm.PupilID equals pp.PupilProfileID
                                                                  join sp in SchoolProfileBusiness.All on sm.SchoolID equals sp.SchoolProfileID
                                                                  where cl.IsActive.Value
                                                                  && !lstMID.Contains(sm.PupilID)
                                                                  && cl.AcademicYearID == objAcademicYear.AcademicYearID
                                                                  select new MovementAcceptanceBO
                                                                  {
                                                                      PupilID = sm.PupilID,
                                                                      ClassID = cl.ClassProfileID,
                                                                      EducationLeveID = cl.EducationLevelID,
                                                                      OrderNumber = cl.OrderNumber,
                                                                      DisplayName = cl.DisplayName,
                                                                      Name = pp.Name,
                                                                      FullName = pp.FullName,
                                                                      OrderInClass = cp.OrderInClass,
                                                                      Genre = pp.Genre,
                                                                      BirthDate = pp.BirthDate,
                                                                      MovedFromSchoolID = sm.SchoolID,
                                                                      MovedDate = sm.EnrolmentDate,
                                                                  }).ToList();

            List<MovementAcceptanceBO> lstresult = listMovementAcceptance1.Union(listMovementAcceptance2).ToList();
            lstresult = lstresult.OrderBy(o => o.EducationLeveID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            foreach (MovementAcceptanceBO movementAcceptance in lstresult)
            {
                int currentRow = 10 + countRow;
                if (countRow == 0 && lstresult.Count == 1)
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }
                else if (countRow == 0)
                {
                    sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                }
                else if ((countRow + 1) == lstresult.Count)
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }
                else
                {
                    sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                }
                VTVector vtIndex = new VTVector(countRow, 0);
                sheet.SetCellValue(cellOrderNumber + vtIndex, (countRow + 1));
                sheet.SetCellValue(cellFullName + vtIndex, movementAcceptance.FullName);
                sheet.SetCellValue(cellBirthDate + vtIndex, String.Format("{0:dd/MM/yyyy}", movementAcceptance.BirthDate));
                sheet.SetCellValue(cellGenre + vtIndex, SMASConvert.ConvertGenre(movementAcceptance.Genre));
                sheet.SetCellValue(cellSchool + vtIndex, movementAcceptance.SchoolName);
                sheet.SetCellValue(cellClass + vtIndex, movementAcceptance.DisplayName);
                sheet.SetCellValue(cellMovedDate + vtIndex, String.Format("{0:dd/MM/yyyy}", movementAcceptance.MovedDate));
                sheet.SetCellValue(cellNote + vtIndex, movementAcceptance.Description);
                countRow++;
            }
            VTVector lastRow = new VTVector(countRow + 2, 0);
            sheet.CopyPasteSameSize(mergeRow, 10 + countRow + 2, 7);
            sheet.SetCellValue(cellMovedDate + lastRow, "Người lập báo cáo");
            sheet.FitSheetOnOnePage = true;
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
        #region Học sinh thôi học
        /// <summary>
        /// Lấy ra danh sách học sinh thôi học được cập nhật mới nhất
        /// Author: Tamhm1
        /// Date: 21/11/2012
        /// </summary>
        /// <param name="flowSituationBO"></param>
        /// <returns></returns>
        public ProcessedReport ExcelGetPupilLeavingOff(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_THOI_HOC_A4;
            string inputParameterHaskKey = GetHashKeyForPeriod(flowSituationBO);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHaskKey);
        }
        /// <summary>
        /// Author: Tamhm1
        /// Date: 21/12/2012
        /// Lưu thông tin danh sách học sinh thôi học
        /// </summary>
        /// <param name="flowSituationBO"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport ExcelInsertPupilLeavingOff(FlowSituationBO flowSituationBO, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_THOI_HOC_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForPeriod(flowSituationBO);
            pr.ReportData = ReportUtils.Compress(data);
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(flowSituationBO.AppliedLevel);

            string semester = ReportUtils.ConvertSemesterForReportName(flowSituationBO.Semester);


            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);


            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", flowSituationBO.AcademicYearID},
                {"SchoolID", flowSituationBO.SchoolID},
                {"AppliedLevel", flowSituationBO.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        /// <summary>
        /// Lưu lại thông tin thống kê học sinh thôi học
        /// Author: Tamhm1
        /// Date: 21/11/2012
        /// </summary>
        /// <param name="flowSituationBO"></param>
        /// <returns></returns>
        public Stream ExcelCreatePupilLeavingOff(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_THOI_HOC_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 9;
            //Lấy sheet hoc sinh thôi học
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "G" + firstRow);
            sheet.Name = "TKHSThoiHoc";

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            sheet.SetCellValue("A3", school.SchoolName.ToUpper());
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper());
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string date = provinceName + ", " + " ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            sheet.SetCellValue("D4", date);
            sheet.SetCellValue("D7", SMASConvert.ConvertSemester(entity.Semester).ToUpper());

            VTVector cellOrderNumber = new VTVector("A10");
            VTVector cellFullName = new VTVector("B10");
            VTVector cellBirthDate = new VTVector("C10");
            VTVector cellGenre = new VTVector("D10");
            VTVector cellClass = new VTVector("E10");
            VTVector cellDate = new VTVector("F10");
            VTVector cellNote = new VTVector("G10");

            int countRow = 0;
            IVTRange topRow = firstSheet.GetRange("A10", "G10");
            IVTRange middleRow = firstSheet.GetRange("A11", "G11");
            IVTRange bottomRow = firstSheet.GetRange("A14", "G14");

            //List<PupilLeavingOff> lstPupilLeavingOff = PupilLeavingOffBusiness.GetListPupilLeavingOff(entity.AcademicYearID, entity.SchoolID, entity.Semester);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = entity.AcademicYearID;
            search["AppliedLevel"] = entity.AppliedLevel;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                search["Semester"] = entity.Semester;
            }
            IQueryable<PupilLeavingOff> lsPupilLeavingOff = PupilLeavingOffBusiness.SearchBySchool(entity.SchoolID, search);

            if (lsPupilLeavingOff != null && lsPupilLeavingOff.Count() > 0)
            {
                //List<PupilLeavingOff> lstPupilLeavingOff = lsPupilLeavingOff.OrderBy(s => s.LeavingDate).ToList();
                IDictionary<string, object> dicLeaving = new Dictionary<string, object>();
                dicLeaving["AcademicYearID"] = entity.AcademicYearID;
                dicLeaving["SchoolID"] = entity.SchoolID;
                dicLeaving["Semester"] = entity.Semester;
                dicLeaving["Check"] = "Check";
                IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicLeaving);
                var lstPupilLeavingOff = (from sm in lsPupilLeavingOff
                                          join cp in lstPupilOfClass on new { sm.PupilID, sm.ClassID } equals new { cp.PupilID, cp.ClassID }
                                          join cl in ClassProfileBusiness.All on cp.ClassID equals cl.ClassProfileID
                                          join pp in PupilProfileBusiness.All on sm.PupilID equals pp.PupilProfileID
                                          where cl.IsActive.Value
                                          select new
                                          {
                                              PupilID = sm.PupilID,
                                              EducationLevelID = cl.EducationLevelID,
                                              ClassID = sm.ClassID,
                                              ClassName = cl.DisplayName,
                                              OrderNumber = cl.OrderNumber,
                                              BirthDate = pp.BirthDate,
                                              Genre = pp.Genre,
                                              LeavingDate = sm.LeavingDate,
                                              OrderInClass = cp.OrderInClass,
                                              Name = pp.Name,
                                              FullName = pp.FullName,
                                              Description = sm.Description
                                          }).ToList();
                lstPupilLeavingOff = lstPupilLeavingOff.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                foreach (var pupilLeavingOff in lstPupilLeavingOff)
                {
                    int currentRow = countRow + 10;
                    if (countRow == 0 && lstPupilLeavingOff.Count() == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else if (countRow == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    else if ((countRow + 1) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else if ((countRow + 1) == lstPupilLeavingOff.Count())
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    VTVector vtIndex = new VTVector(countRow, 0);
                    sheet.SetCellValue(cellOrderNumber + vtIndex, (countRow + 1));
                    sheet.SetCellValue(cellFullName + vtIndex, pupilLeavingOff.FullName);
                    sheet.SetCellValue(cellBirthDate + vtIndex, String.Format("{0:dd/MM/yyyy}", pupilLeavingOff.BirthDate));
                    sheet.SetCellValue(cellGenre + vtIndex, SMASConvert.ConvertGenre(pupilLeavingOff.Genre));
                    sheet.SetCellValue(cellClass + vtIndex, pupilLeavingOff.ClassName);
                    sheet.SetCellValue(cellDate + vtIndex, pupilLeavingOff.LeavingDate);
                    sheet.SetCellValue(cellNote + vtIndex, pupilLeavingOff.Description);
                    countRow++;
                }
                sheet.FitSheetOnOnePage = true;
            }
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
        #region
        public string GetHaskKeyForPolicyTarget(FlowSituationBO flowSituationBO)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = flowSituationBO.AcademicYearID;
            dic["SchoolID"] = flowSituationBO.SchoolID;
            dic["AppliedLevel"] = flowSituationBO.AppliedLevel;
            return ReportUtils.GetHashKey(dic);
        }
        /// <summary>
        /// Lấy danh sách học sinh diện chính sách mới nhất được cập nhật
        /// Tamhm1
        /// 21/11/2012
        /// </summary>
        /// <param name="flowSituationBO"></param>
        /// <returns></returns>
        public ProcessedReport ExcelGetPolicyTarget(FlowSituationBO flowSituationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_DIEN_CHINH_SACH_A4;
            string inputParameterHashKey = GetHaskKeyForPolicyTarget(flowSituationBO);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// Lưu lại thông tin học sinh diện chính sách
        /// Author: Tamhm1
        /// Date: 21/11/2012
        /// </summary>
        /// <param name="flowSituationBO"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport ExcelInsertPolicyTarget(FlowSituationBO flowSituationBO, Stream data)
        {
            ReportDefinition reportDef = new ReportDefinition();
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_DIEN_CHINH_SACH_A4;
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.InputParameterHashKey = GetHaskKeyForPolicyTarget(flowSituationBO);
            pr.ProcessedDate = DateTime.Now;
            pr.ReportData = ReportUtils.Compress(data);
            reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(flowSituationBO.AppliedLevel);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", flowSituationBO.AcademicYearID},
                {"SchoolID", flowSituationBO.SchoolID},
                {"AppliedLevel", flowSituationBO.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream ExcelCreatePolicyTarget(FlowSituationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_HOC_SINH_DIEN_CHINH_SACH_A4;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet hoc sinh dien chính sách
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "F8");
            sheet.Name = "TKHSChinhSach";

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            sheet.SetCellValue("A3", school.SchoolName.ToUpper());
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper());

            VTVector cellClassName = new VTVector("A9");
            VTVector cellOrderNumber = new VTVector("A10");
            VTVector cellFullName = new VTVector("B10");
            VTVector cellBirthDate = new VTVector("C10");
            VTVector cellPriorityType = new VTVector("D10");
            VTVector cellPolicyTarget = new VTVector("E10");
            VTVector cellNote = new VTVector("F10");


            int countRow = 0;
            IVTRange topRow = firstSheet.GetRange("A9", "F9");
            IVTRange bottomRow = firstSheet.GetRange("A10", "F10");

            IQueryable<PupilProfile> lsPupilProfile = PupilProfileBusiness.GetListPupilPolicyTarget(entity.AcademicYearID, entity.SchoolID);
            lsPupilProfile = lsPupilProfile.Where(o => o.ClassProfile.EducationLevel.Grade == entity.AppliedLevel);
            if (lsPupilProfile != null && lsPupilProfile.Count() > 0)
            {

                //Chiendd: 22/09/2014: Lay danh sach hoc sinh dang hoc hoac da tot nghiep. fix bug 16272
                AcademicYear objAcademicYear = AcademicYearBusiness.All.FirstOrDefault(a => a.AcademicYearID == entity.AcademicYearID);
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = entity.AcademicYearID;
                SearchInfo["SchoolID"] = entity.SchoolID;
                SearchInfo["Check"] = "Check";
                IQueryable<PupilOfClass> iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, SearchInfo).AddCriteriaSemester(objAcademicYear, SystemParamsInFile.SEMESTER_OF_YEAR_ALL);

                ClassProfile classProfile = new ClassProfile();
                //thuc hien lenh join
                List<PupilProfileBO> result = (from p in lsPupilProfile
                                               join q in iqPupilOfClass on new { PupilID = p.PupilProfileID, ClassID = p.CurrentClassID } equals new { q.PupilID, q.ClassID }
                                               join r in ClassProfileBusiness.All on q.ClassID equals r.ClassProfileID
                                               where r.IsActive.Value
                                               select new PupilProfileBO
                                               {
                                                   PupilProfileID = p.PupilProfileID,
                                                   EducationLevelID = r.EducationLevelID,
                                                   CurrentClassID = p.CurrentClassID,
                                                   ClassName = r.DisplayName,
                                                   ClassOrderNumber = r.OrderNumber,
                                                   FullName = p.FullName,
                                                   Name = p.Name,
                                                   BirthDate = p.BirthDate,
                                                   PriorityTypeName = p.PriorityType.Resolution,
                                                   PolicyTargetName = p.PolicyTarget.Resolution,
                                                   OrderInClass = q.OrderInClass
                                               }).ToList();
                var lstClassProfile = result.Select(o => new { o.EducationLevelID, o.CurrentClassID, o.ClassOrderNumber, o.ClassName }).Distinct().ToList();
                lstClassProfile = lstClassProfile.OrderBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ToList();
                List<PupilProfileBO> resultClass = new List<PupilProfileBO>();
                for (int i = 0; i < lstClassProfile.Count(); i++)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    int myInt = lstClassProfile[i].CurrentClassID;
                    classProfile = ClassProfileBusiness.Find(myInt);
                    string className = "Lớp: ";
                    if (classProfile != null)
                    {
                        className += classProfile.DisplayName.ToUpper();
                    }

                    resultClass = result.Where(o => o.CurrentClassID == myInt).ToList();
                    resultClass = resultClass.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                    int currentClassRow = 9 + countRow;
                    sheet.CopyPasteSameRowHeigh(topRow, currentClassRow);
                    sheet.SetCellValue(currentClassRow, 1, className);
                    int rowRecordOfClass = 1;
                    if (resultClass != null && resultClass.Count > 0)
                    {
                        foreach (PupilProfileBO pupilProfileBo in resultClass)
                        {
                            int currentRow = 10 + countRow;

                            sheet.CopyPaste(bottomRow, currentRow, 1, true);

                            VTVector vtIndex = new VTVector(countRow, 0);

                            sheet.SetCellValue(cellOrderNumber + vtIndex, rowRecordOfClass);
                            sheet.SetCellValue(cellFullName + vtIndex, pupilProfileBo.FullName);
                            sheet.SetCellValue(cellBirthDate + vtIndex, String.Format("{0:dd/MM/yyyy}", pupilProfileBo.BirthDate));
                            sheet.SetCellValue(cellPriorityType + vtIndex, pupilProfileBo.PriorityTypeName);
                            sheet.SetCellValue(cellPolicyTarget + vtIndex, pupilProfileBo.PolicyTargetName);

                            countRow++;
                            rowRecordOfClass++;
                        }
                    }
                    countRow++;

                }
                sheet.FitSheetOnOnePage = true;
            }
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}


