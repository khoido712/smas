﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class DishInspectionBusiness
    {

        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        private void Validate(DishInspection entity)
        {
            //EatingGroupID: FK(EatingGroup)
            if (entity.EatingGroupID != 0)
            {
                EatingGroupBusiness.CheckAvailable(entity.EatingGroupID, "EatingGroup_Label_EatingGroupID", true);
            }
            
            //MenuType: in (1, 2)
            if (entity.MenuType != 1 && entity.MenuType != 2)
            {
                throw new BusinessException("DishInspection_Validate_MenuType");
            }
            //DailyMenuCode: require
            if (entity.DailyMenu != null && entity.DailyMenu.DailyMenuCode == null)
            {
                throw new BusinessException("DishInspection_Validate_DailyMenuCodeRequire");
            }
            //NumberOfChildren: maxlength(4)
            if (entity.NumberOfChildren.ToString().Length > 4)
            {
                throw new BusinessException("DishInspection_Validate_NumberOfChildrenMaxLength");
            }
            //SchoolID, DailyMenuID: not compatible(DailyMenu)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["DailyMenuID"] = entity.DailyMenuID;
            SearchInfo["SchoolID"] = entity.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DailyMenu", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //SchoolID, EatingGroupID: not compatible(EatingGroup)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["EatingGroupID"] = entity.EatingGroupID;
            SearchInfo2["SchoolID"] = entity.SchoolID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EatingGroup", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //MenuType, DailyMenuCode: not compatible(DailyMenu)
            DailyMenu dailyMenu = DailyMenuRepository.Find(entity.DailyMenuID);
            IDictionary<string, object> SearchInfo3 = new Dictionary<string, object>();
            SearchInfo3["MenuType"] = entity.MenuType;
            SearchInfo3["DailyMenuCode"] = dailyMenu.DailyMenuCode;
            bool compatible3 = this.repository.ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DailyMenu", SearchInfo3);
            if (!compatible3)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //InspectedDate: require
            Utils.ValidateRequire(entity.InspectedDate, "DishInspection_Label_InspectedDate");
            // InspectedDate <= Date.Now
            Utils.ValidatePastDateNow(entity.InspectedDate, "DishInspection_Label_InspectDate");
            //SchoolID, InspectedDate, EatingGroupID, MenuType: Duplicate
            // Kiểm trùng dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = entity.SchoolID;
            SearchInfo["InspectedDate"] = entity.InspectedDate;
            SearchInfo["EatingGroupID"] = entity.EatingGroupID;
            SearchInfo["MenuType"] = entity.MenuType;

            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["DishInspectionID"] = entity.DishInspectionID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DishInspection", SearchInfo, ExceptInfo);
            if (!compatible)
            {
                List<object> Params = new List<object>();
                Params.Add("DishInspection_Label");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }

        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns>
        /// IQueryable{DishInspection}
        /// </returns>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        private IQueryable<DishInspection> Search(IDictionary<string, object> dic)
        {

            int SchoolID = Utils.GetInt(dic, "SchoolID");
            DateTime? InspectedDate = Utils.GetDateTime(dic, "InspectedDate");
            int MenuType = Utils.GetInt(dic, "MenuType");
            int DailyMenuID = Utils.GetInt(dic, "DailyMenuID");
            int EatingGroupID = Utils.GetInt(dic, "EatingGroupID");

            IQueryable<DishInspection> lstDishInspection = DishInspectionRepository.All;

            if (SchoolID != 0)
            {
                lstDishInspection = lstDishInspection.Where(o => (o.SchoolID == SchoolID));
            }
            if (InspectedDate != null)
            {
                lstDishInspection = lstDishInspection.Where(o => (o.InspectedDate == InspectedDate));
            }
            if (MenuType != 0)
            {
                lstDishInspection = lstDishInspection.Where(o => (o.MenuType == MenuType));
            }
            if (DailyMenuID != 0)
            {
                lstDishInspection = lstDishInspection.Where(o => (o.DailyMenuID == DailyMenuID));
            }
            if (EatingGroupID != 0)
            {
                lstDishInspection = lstDishInspection.Where(o => (o.EatingGroupID == EatingGroupID));
            }
            return lstDishInspection;
        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{DishInspection}
        /// </returns>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        public IQueryable<DishInspection> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        /// <summary>
        /// CheckDishInspection
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        public bool CheckDishInspection(int SchoolID, DateTime InspectedDate, int dailyMenu)
        {
            IQueryable<DishInspection> lstDishInspection = this.All.Where(o => o.SchoolID == SchoolID && o.InspectedDate == InspectedDate && o.DailyMenuID == dailyMenu);
            if (lstDishInspection.Count() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dishInspection">The dish inspection.</param>
        /// <returns>
        /// DishInspection
        /// </returns>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        public override DishInspection Insert(DishInspection dishInspection)
        {
            //Validate
            Validate(dishInspection);
            //Insert
            return base.Insert(dishInspection);
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dishInspection">The dish inspection.</param>
        /// <returns>
        /// DishInspection
        /// </returns>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        public override DishInspection Update(DishInspection dishInspection)
        {
            //Validate
            Validate(dishInspection);
            //Update
            return base.Update(dishInspection);
        }

        /// <summary>
        /// GetInspectedDate
        /// </summary>
        /// <returns>
        /// IQueryable{DishInspection}
        /// </returns>
        /// <author>phuongtd5</author>
        /// <date>11/12/2012</date>
        public IQueryable<DishInspection> GetInspectedDate(int SchoolID, DateTime FromDate, DateTime ToDate)
        {
            IQueryable<DishInspection> lstDishInspection = this.All.Where(o => o.SchoolID == SchoolID && o.InspectedDate >= FromDate && o.InspectedDate <= ToDate);
            return lstDishInspection;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <author>phuongtd5</author>
        /// <date>11/13/2012</date>
        public void Delete(int SchoolID, DateTime InspectedDate)
        {
            IQueryable<DishInspection> lstDishInspection = this.All.Where(o => o.SchoolID == SchoolID && o.InspectedDate == InspectedDate);
            if (lstDishInspection.Count() != 0)
            {
                this.DeleteAll(lstDishInspection.ToList());
            }
        }

        /// <summary>
        /// Xóa đối tượng bằng cách update IsActive = 1.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="LogicallyDelete"></param>
        public void Delete(int id, bool LogicallyDelete)
        {
            base.Delete(id, LogicallyDelete);
        }

        /// <summary>
        /// GetListFood
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <returns>
        /// Int32
        /// </returns>
        /// <date>11/13/2012</date>
        /// <author>
        /// Quanglm1
        /// </author>
        public IQueryable<DishInspectionBO> GetListFood(int SchoolID, DateTime InspectedDate)
        {
            IQueryable<DishInspection> QueryDishInspection = DishInspectionRepository.All;
            IQueryable<DailyFoodInspection> QueryDailyFoodInspection = DailyFoodInspectionRepository.All;
            IQueryable<EatingGroup> QueryEatingGroup = EatingGroupRepository.All;
            IQueryable<FoodCat> QueryFoodCat = FoodCatRepository.All;
            var sql = from di in QueryDishInspection
                      join dfi in QueryDailyFoodInspection on di.DishInspectionID equals dfi.DishInspectionID
                      join eg in QueryEatingGroup on di.EatingGroupID equals eg.EatingGroupID
                      join f in QueryFoodCat on dfi.FoodID equals f.FoodID
                      where (di.SchoolID == SchoolID && di.InspectedDate.Year == InspectedDate.Year
                      && di.InspectedDate.Month <= InspectedDate.Month
                      && di.InspectedDate.Day <= InspectedDate.Day)
                      select new DishInspectionBO
                      {
                          DishInspectionID = di.DishInspectionID,
                          InspectedDate = di.InspectedDate,
                          MenuType = di.MenuType,
                          DailyCostOfChildren = di.DailyCostOfChildren,
                          Expense = di.Expense,
                          EatingGroupID = di.EatingGroupID,
                          SchoolIDInspection = di.SchoolID,
                          DailyMenuID = di.DailyMenuID,
                          IsActiveInspection = di.IsActive,
                          NumberOfChildren = di.NumberOfChildren,
                          Weight = dfi.Weight,
                          PricePerOnce = dfi.PricePerOnce,
                          EatingGroupName = eg.EatingGroupName,
                          FoodID = f.FoodID,
                          FoodName = f.FoodName,
                          FoodGroupID = f.FoodGroupID.Value
                      };
            return sql;
        }



        /// <summary>
        /// GetListOtherService
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <returns>
        /// IQueryable{DishInspectionBO}
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/14/2012</date>
        public IQueryable<DishInspectionBO> GetListOtherService(int SchoolID, DateTime InspectedDate)
        {
            IQueryable<DishInspection> QueryDishInspection = DishInspectionRepository.All;
            IQueryable<DailyOtherServiceInspection> QueryDailyOtherServiceInspection = DailyOtherServiceInspectionRepository.All;
            IQueryable<OtherService> QueryOtherService = OtherServiceRepository.All;

            var sql = from di in QueryDishInspection
                      join dosi in QueryDailyOtherServiceInspection on di.DishInspectionID equals dosi.DishInspectionID
                      join os in QueryOtherService on dosi.OtherServiceID equals os.OtherServiceID
                      where di.SchoolID == SchoolID 
                      && di.InspectedDate.Year == InspectedDate.Year
                      && di.InspectedDate.Month == InspectedDate.Month
                      && di.InspectedDate.Day == InspectedDate.Day
                      select new DishInspectionBO
                      {
                          DishInspectionID = di.DishInspectionID,
                          InspectedDate = di.InspectedDate,
                          MenuType = di.MenuType,
                          DailyCostOfChildren = di.DailyCostOfChildren,
                          Expense = di.Expense,
                          EatingGroupID = di.EatingGroupID,
                          SchoolIDInspection = di.SchoolID,
                          DailyMenuID = di.DailyMenuID,
                          IsActiveInspection = di.IsActive,
                          NumberOfChildren = di.NumberOfChildren,
                          OtherServiceID = os.OtherServiceID,
                          OtherServiceName = os.OtherServiceName,
                          Note = os.Note,
                          PriceService = os.Price,
                          SchoolIDService = os.SchoolID,
                          EffectDate = os.EffectDate,
                          IsActiveService = os.IsActive,
                          Price = dosi.Price
                      };

            return sql;
        }

        /// <summary>
        /// GetNumberOfChildren
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <returns>
        /// Int32
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/14/2012</date>
        public int GetNumberOfChildren(int SchoolID, DateTime InspectedDate)
        {
            IQueryable<DishInspection> QueryDishInspection = DishInspectionRepository.All
                .Where(o => o.SchoolID == SchoolID)
                .Where(o => o.InspectedDate.Year == InspectedDate.Year)
                .Where(o => o.InspectedDate.Month == InspectedDate.Month)
                .Where(o => o.InspectedDate.Day == InspectedDate.Day);
            if (QueryDishInspection.Count() > 0)
            {
                return QueryDishInspection.Sum(o => o != null ? o.NumberOfChildren : 0);
            }
            return 0;
        }

        #region Tạo báo cáo tiền tạm chi theo thực đơn
        /// <summary>
        /// CreateReportDishInspectionCost
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="InspectedDate">The inspected date.</param>
        /// <param name="AcademicYearID">The AcademicYearID</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>PhuongTD</author>
        /// <date>12/12/2012</date>
        public Stream CreateReportDishInspectionCost(int SchoolID, int AcademicYearID, DateTime date)
        {

            string reportCode = SystemParamsInFile.REPORT_TIEN_AN_TAM_CHI_THEO_THUC_DON;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int lastRowHeader = 8;
            int defaultLastColumn = 9; // "I"
            int firstRow = 12;
            int firstRowService = 17;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo header cho template
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + lastRowHeader);
            #region //Fill dữ liệu header cho template
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Date", date.ToShortDateString()}
                };
            tempSheet.GetRange(1, 1, lastRowHeader, defaultLastColumn).FillVariableValue(dicGeneralInfo);
            #endregion
            //Tạo sheet Fill dữ liệu copy phần header của sheet1 sang
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(lastRowHeader, defaultLastColumn).ToString());

            //Tính số trẻ
            int NumberOfChildren = GetNumberOfChildren(SchoolID, date);
            //Lấy danh sách thực phẩm
            IQueryable<DishInspectionBO> lsFood = GetListFood(SchoolID, date); 
            //Lấy danh sách nhóm ăn
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            IQueryable<EatingGroupBO> lsEatingGroup = EatingGroupBusiness.SearchBySchool(SchoolID, SearchInfo);
            #region //Bắt đầu fill phần thực phẩm
            dataSheet.CopyPasteSameSize(firstSheet.GetRange("A9", "I9"), lastRowHeader + 1, 1);
            if (lsFood != null && lsFood.Count() > 0)
            {
                List<int> lstFoodID = lsFood.Select(o => o.FoodID).Distinct().ToList();
                int indexFood = 0;
                int startDynamicCol = 6;
                int lastCol = startDynamicCol + lsEatingGroup.Count() * 4 - 1;
                int endRow = firstRow + lstFoodID.Count;
                List<object> listData = new List<object>();
                string formular = "";
                IVTRange kGRange = firstSheet.GetRange("F12", "F12");
                IVTRange totalMoney = firstSheet.GetRange("G12", "G12");
                IVTRange kGRowRange = firstSheet.GetRange("F13", "F13");
                IVTRange totalRowMoney = firstSheet.GetRange("G13", "G13");
                IVTRange eatingGroupRange = firstSheet.GetRange("F11", "G11");
                IVTRange menuType = firstSheet.GetRange("F10", "G10");

                foreach(int foodID in lstFoodID)
                {
                    
                    int countColumn = 0;
                    Dictionary<string, object> dicData = new Dictionary<string, object>();
                    indexFood++;
                    tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol), firstRow + indexFood, 1);
                    //dataSheet.CopyPasteSameSize(range, firstRow + i, 1);
                    dicData = new Dictionary<string, object>();
                    IQueryable<DishInspectionBO> lsDishInspection = lsFood.Where(o => o.FoodID == foodID);
                    DishInspectionBO foodCat = lsDishInspection.FirstOrDefault();
                    dicData["Order"] = indexFood;
                    dicData["FoodName"] = foodCat.FoodName;
                    dicData["WeightKG"] = foodCat.Weight * NumberOfChildren / 1000;
                    dicData["Price"] = foodCat.PricePerOnce;
                    if (lsEatingGroup != null && lsEatingGroup.Count() > 0)
                    {
                        List<EatingGroupBO> ListEatingGroup = lsEatingGroup.ToList();
                        //Thực đơn thường
                        foreach (EatingGroupBO eatingGroup in ListEatingGroup)
                        {
                            //tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
                            tempSheet.CopyPasteSameSize(kGRowRange, firstRow + indexFood, startDynamicCol + countColumn * 2);
                            tempSheet.SetCellValue(firstRow + indexFood, startDynamicCol + countColumn * 2, "${list[].Get(Normal" + eatingGroup.EatingGroupID + ")}");
                            formular = "=D" + (firstRow + indexFood) + "*" + VTVector.ColumnIntToString(startDynamicCol + countColumn * 2) + (firstRow + indexFood);
                            tempSheet.CopyPasteSameSize(totalRowMoney, firstRow + indexFood, startDynamicCol + countColumn * 2 + 1);
                            tempSheet.SetFormulaValue(firstRow + indexFood, startDynamicCol + countColumn * 2 + 1, formular);
                            DishInspectionBO dishInspection = lsDishInspection.Where(o => o.EatingGroupID == eatingGroup.EatingGroupID && o.MenuType == SystemParamsInFile.MENU_TYPE_NORMAL).FirstOrDefault();
                            dicData["Normal" + eatingGroup.EatingGroupID.ToString()] = dishInspection != null ? dishInspection.Weight * NumberOfChildren / 1000 : 0;
                            //FillHeader
                            dataSheet.CopyPasteSameSize(kGRange, 12, startDynamicCol + countColumn * 2);
                            dataSheet.CopyPasteSameSize(totalMoney, 12, startDynamicCol + countColumn * 2 + 1);
                            dataSheet.CopyPasteSameSize(eatingGroupRange, 11, startDynamicCol + countColumn * 2);
                            dataSheet.SetCellValue(11, startDynamicCol + countColumn * 2, eatingGroup.EatingGroupName);
                            dataSheet.CopyPaste(menuType, 10, startDynamicCol + countColumn * 2);
                            countColumn++;
                        }
                        dataSheet.GetRange(10, startDynamicCol, 10, startDynamicCol + ListEatingGroup.Count * 2 - 1).Merge();
                        dataSheet.SetCellValue(10, startDynamicCol, "Thực đơn thường");
                        //Thực đơn bổ sung
                        foreach (EatingGroupBO eatingGroup in ListEatingGroup)
                        {
                            //tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
                            tempSheet.CopyPasteSameSize(kGRowRange, firstRow + indexFood, startDynamicCol + countColumn * 2);
                            tempSheet.SetCellValue(firstRow + indexFood, startDynamicCol + countColumn * 2, "${list[].Get(Additional" + eatingGroup.EatingGroupID + ")}");
                            formular = "=D" + (firstRow + indexFood) + "*" + VTVector.ColumnIntToString(startDynamicCol + countColumn * 2) + (firstRow + indexFood);
                            tempSheet.CopyPasteSameSize(totalRowMoney, firstRow + indexFood, startDynamicCol + countColumn * 2 + 1);
                            tempSheet.SetFormulaValue(firstRow + indexFood, startDynamicCol + countColumn * 2 + 1, formular);
                            DishInspectionBO dishInspection = lsDishInspection.Where(o => o.EatingGroupID == eatingGroup.EatingGroupID && o.MenuType == SystemParamsInFile.MENU_TYPE_ADDITIONAL).FirstOrDefault();
                            dicData["Additional" + eatingGroup.EatingGroupID.ToString()] = dishInspection != null ? dishInspection.Weight * NumberOfChildren / 1000 : 0;
                            //FillHeader
                            dataSheet.CopyPasteSameSize(kGRange, 12, startDynamicCol + countColumn * 2);
                            dataSheet.CopyPasteSameSize(totalMoney, 12, startDynamicCol + countColumn * 2 + 1);
                            dataSheet.CopyPasteSameSize(eatingGroupRange, 11, startDynamicCol + countColumn * 2);
                            dataSheet.SetCellValue(11, startDynamicCol + countColumn * 2, eatingGroup.EatingGroupName);
                            dataSheet.CopyPaste(menuType, 10, startDynamicCol + countColumn * 2);
                            countColumn++;
                        }
                        dataSheet.GetRange(10, startDynamicCol + ListEatingGroup.Count * 2, 10, startDynamicCol + ListEatingGroup.Count * 4 - 1).Merge();
                        dataSheet.SetCellValue(10, startDynamicCol + ListEatingGroup.Count * 2, "Thực đơn bổ sung");
                    }
                    listData.Add(dicData);
                    dataSheet.CopyPasteSameSize(tempSheet.GetRange(firstRow + indexFood, 1, firstRow + indexFood, lastCol), firstRow + indexFood, 1);
                }
                dataSheet.GetRange(firstRow + 1, 1, firstRow + lstFoodID.Count + 1, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                IVTRange staticCol = firstSheet.GetRange(firstRow - 2, 1, firstRow, startDynamicCol - 1);
                dataSheet.CopyPasteSameSize(staticCol, firstRow - 2, 1);
                //Fill dòng tổng
                IVTRange sumRange = firstSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);
                dataSheet.CopyPasteSameSize(sumRange, endRow + 1, 1);
                //Tính tổng cột thành tiền
                formular = "=SUM(E" + (firstRow + 1) + ":E" + endRow + ")";
                dataSheet.SetFormulaValue("E" + (endRow + 1), formular);
                for (int i = startDynamicCol; i < lastCol; i+=2)
                {
                    formular = "=SUM(" + VTVector.ColumnIntToString(i + 1) + (firstRow + 1) + ":" + VTVector.ColumnIntToString(i + 1) + endRow + ")";
                    dataSheet.SetFormulaValue(VTVector.ColumnIntToString(i + 1) + (endRow + 1), formular);
                }
                firstRowService = endRow + 4;
            }
            #endregion
            //Danh sách dịch vụ
            List<DishInspectionBO> ListOtherService = GetListOtherService(SchoolID, date).ToList();
            IVTRange spenInfo = firstSheet.GetRange("A17", "D21");
            IVTRange serviceList = firstSheet.GetRange("A22", "D22");
            IVTRange nutritionNorm = firstSheet.GetRange("A23", "D25");
            dataSheet.CopyPasteSameSize(spenInfo, firstRowService, 1);
            int countService = 0;
            List<object> listService = new List<object>();
            string formularService = "";
            foreach (DishInspectionBO dishInspection in ListOtherService)
            {
                Dictionary<string, object> serviceData = new Dictionary<string, object>();
                countService++;
                dataSheet.CopyPasteSameSize(serviceList, firstRowService + countService + 4, 1);
                formularService = "=C" + (firstRowService + countService + 4) + "*B" + (firstRowService + countService + 4);
                dataSheet.SetFormulaValue(firstRowService + countService + 4, 4, formularService);
                serviceData["ServiceName"] = dishInspection.OtherServiceName;
                serviceData["Number"] = dishInspection.NumberOfChildren;
                serviceData["Price"] = dishInspection.PriceService;
                listService.Add(serviceData);
            }
            dataSheet.GetRange(firstRowService, 1, firstRowService + 4 + ListOtherService.Count, 4).FillVariableValue(new Dictionary<string, object>
                    {
                        {"listServices",listService}
                    });
            dataSheet.CopyPasteSameSize(nutritionNorm, firstRowService + 5 + ListOtherService.Count, 1);
            //Tổng tiền dịch vụ
            if (ListOtherService.Count != 0)
            {
                formularService = "=SUM(D" + (firstRowService + 5) + ":D" + (firstRowService + 4 + ListOtherService.Count) + ")";
                dataSheet.SetFormulaValue(firstRowService + 4, 4, formularService);
            }
            else {
                dataSheet.SetCellValue(firstRowService + 4, 4, 0);
            }
            //Tiền thực phẩm
            formularService = "=E" + (firstRowService - 3);
            dataSheet.SetFormulaValue(firstRowService + 3, 4, formularService);
            //Định mức trong ngày
            int DailyPrice = DailyDishCostBusiness.MaxDailyDishCost(date, SchoolID);
            dataSheet.SetCellValue(firstRowService + 6 + ListOtherService.Count, 2, NumberOfChildren);
            dataSheet.SetCellValue(firstRowService + 6 + ListOtherService.Count, 3, DailyPrice);
            formularService = "=C" + (firstRowService + 6 + ListOtherService.Count) + "*B" + (firstRowService + 6 + ListOtherService.Count);
            dataSheet.SetFormulaValue(firstRowService + 6 + ListOtherService.Count, 4, formularService);
            //Tiền chi trong ngày
            formularService = "=D" + (firstRowService + 3) + "+D" + (firstRowService + 4);
            dataSheet.SetFormulaValue(firstRowService + 2, 4, formularService);
            //Chênh lệch
            formularService = "=D" + (firstRowService + 6 + ListOtherService.Count) + "-D" + (firstRowService + 2);
            dataSheet.SetFormulaValue(firstRowService + 7 + ListOtherService.Count, 4, formularService);
            firstSheet.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }
        #endregion

        #region Xuất excel bảng điều tra
        public Stream CreateReportDishInspection(int SchoolID, int AcademicYearID, int DishInspectionID)
        {
            string reportCode = SystemParamsInFile.REPORT_BANG_DIEU_TRA;
            string formular = "";
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //formular đưa alt enter vào trong cell  =A1&CHAR(10)&A2
            #region Điền giá trị Sheet1 vào sheet tổng hợp
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            DishInspection dishInspection = DishInspectionBusiness.Find(DishInspectionID);
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Date", dishInspection.InspectedDate.ToShortDateString()},
                    {"DailyMenuCode", dishInspection.DailyMenu.DailyMenuCode},
                    {"EatingGroupName", dishInspection.EatingGroup.EatingGroupName},
                    {"NumberOfChildren", dishInspection.NumberOfChildren}
                };
            firstSheet.FillVariableValue(dicGeneralInfo);

            //Danh sách chi tiết thực đơn ngày
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["DailyMenuID"] = dishInspection.DailyMenuID;
            IQueryable<DailyMenuDetail> lsDailyMenuDetail =  DailyMenuDetailBusiness.SearchBySchool(SchoolID, dic);
            //Danh sách bữa ăn
            IQueryable<MealCat> lsMeal =  MealCatBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>());
            List<MealCat> lstMeal = new List<MealCat>();
            //Fill Sheet1 vào sheet tổng

            IVTWorksheet sheetData = oBook.CopySheetToLast(firstSheet, "G13");
            IVTRange sheet1_MealName = firstSheet.GetRange("A15", "A15");
            IVTRange sheet1_DishName = firstSheet.GetRange("A16", "A16");
            int sheet1_firstRow = 15;
            if (lsMeal != null && lsMeal.Count() > 0)
            {
                lstMeal = lsMeal.ToList();
            }
            int numberMeal = 0;
            int sheet1_lastRow = 0;
            int maxDish = 0;
            foreach (MealCat meal in lstMeal)
            {
                IQueryable<DailyMenuDetail> lsDailyMenuDetailOfMeal = lsDailyMenuDetail.Where(o => o.MealID == meal.MealCatID);
                if (lsDailyMenuDetailOfMeal != null && lsDailyMenuDetailOfMeal.Count() > 0)
                {
                    if (maxDish < lsDailyMenuDetailOfMeal.Count())
                    {
                        maxDish = lsDailyMenuDetailOfMeal.Count();
                    }
                }
            }
            foreach(MealCat meal in lstMeal)
            {
                numberMeal++;
                sheetData.CopyPasteSameSize(sheet1_MealName, sheet1_firstRow, numberMeal);
                sheetData.SetCellValue(sheet1_firstRow, numberMeal, meal.MealName);
                int numberDish = 0;
                IQueryable<DailyMenuDetail> lsDailyMenuDetailOfMeal = lsDailyMenuDetail.Where(o => o.MealID == meal.MealCatID);
                string concatMealName = "";
                if (lsDailyMenuDetailOfMeal != null && lsDailyMenuDetailOfMeal.Count() > 0)
                {
                    foreach (DailyMenuDetail dailyMenuDetail in lsDailyMenuDetailOfMeal.ToList())
                    {
                        numberDish++;
                        sheetData.CopyPasteSameSize(sheet1_DishName, sheet1_firstRow + numberDish, numberMeal);
                        concatMealName += dailyMenuDetail.DishName + "\r\n";
                    }
                    sheetData.GetRange(sheet1_firstRow + 1, numberMeal, sheet1_firstRow + maxDish, numberMeal).Merge();
                    sheetData.SetCellValue(sheet1_firstRow + 1, numberMeal, concatMealName.Remove(concatMealName.Length - 2));
                }
            }
            sheet1_lastRow = sheet1_firstRow + maxDish;

            #endregion


            #region Điền dữ liệu vào sheet5
            List<int> lstFoodID = new List<int>();
            List<decimal> lstWeight = new List<decimal>();
            IVTWorksheet sheet5 = oBook.GetSheet(5);
            dic = new Dictionary<string, object>();
            dic["DishInspectionID"] = DishInspectionID;
            IQueryable<DailyFoodInspection> lsDailyFoodInspection =  DailyFoodInspectionBusiness.SearchBySchool(SchoolID, dic);
            if (lsDailyFoodInspection != null && lsDailyFoodInspection.Count() > 0)
            {
                IQueryable<FoodGroup> lsFoodGroup = lsDailyFoodInspection.Select(o => o.FoodGroup).Distinct();
                if (lsFoodGroup != null && lsFoodGroup.Count() > 0)
                {
                    foreach (FoodGroup foodGroup in lsFoodGroup.ToList())
                    {
                        IQueryable<DailyFoodInspection> lsDailyFoodInspectionByGroup = lsDailyFoodInspection.Where(o => o.FoodGroupID == foodGroup.FoodGroupID);
                        if (lsDailyFoodInspectionByGroup != null && lsDailyFoodInspectionByGroup.Count() > 0)
                        {
                            foreach (DailyFoodInspection dailyFoodInspection in lsDailyFoodInspectionByGroup.ToList())
                            {
                                lstFoodID.Add(dailyFoodInspection.FoodID);
                                lstWeight.Add(dailyFoodInspection.Weight);
                            }
                        }
                    }
                }
            }
            #endregion

            #region Điền dữ liệu vào sheet3
            IVTWorksheet sheet3 = oBook.GetSheet(3);
            MineralOfDailyMenuBO mineralOfDailyMenu = FoodCatBusiness.CaculatorContent(lstFoodID, lstWeight, dishInspection.EatingGroupID, SchoolID, AcademicYearID);
            
            #endregion

            #region Điền dữ liệu vào sheet 4
            IVTWorksheet sheet4 = oBook.GetSheet(4);
            IDictionary<string, object> dic_nn = new Dictionary<string, object>();
            dic_nn["AcademicYearID"] = AcademicYearID;
            dic_nn["EatingGroupID"] = dishInspection.EatingGroupID;
            NutritionalNorm nn = NutritionalNormBusiness.SearchBySchool(SchoolID, dic_nn).Where(o => o.EffectDate <= DateTime.Now).OrderByDescending(o => o.EffectDate).FirstOrDefault();
            if (lstFoodID.Count != 0 && nn != null)
            {
                Dictionary<string, object> sheet4_dic = new Dictionary<string, object> 
                {
                    {"PercentProteinAnimal", mineralOfDailyMenu.ProteinAnimal / (mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) * 100},
                    {"PercentProteinAnimalDM", nn.AnimalProteinFrom + "-" + nn.AnimalProteinTo}, 
                    {"PercentProteinPlant", mineralOfDailyMenu.ProteinPlant / (mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) * 100}, 
                    {"PercentProteinPlantDM", nn.PlantProteinFrom + "-" + nn.PlantProteinTo}, 
                    {"PercentFatAnimal", mineralOfDailyMenu.FatAnimal / (mineralOfDailyMenu.FatAnimal + mineralOfDailyMenu.FatPlant) * 100}, 
                    {"PercentFatAnimalDM", nn.AnimalFatFrom + "-" + nn.AnimalFatTo}, 
                    {"PercentFatPlant", mineralOfDailyMenu.FatPlant / (mineralOfDailyMenu.FatAnimal + mineralOfDailyMenu.FatPlant) * 100}, 
                    {"PercentFatPlantDM", nn.PlantFatFrom + "-" + nn.PlantFatTo},
                    {"PercentProtein", Math.Round((mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) * 4 / mineralOfDailyMenu.Calo * 100, 2)},
                    {"PercentFat", Math.Round((mineralOfDailyMenu.FatPlant + mineralOfDailyMenu.FatAnimal) * 4 / mineralOfDailyMenu.Calo * 100, 2)},
                    {"PercentSugar", Math.Round(mineralOfDailyMenu.Sugar * 4 / mineralOfDailyMenu.Calo * 100, 2)},
                    {"PercentProteinDM", nn.ProteinFrom + "-" + nn.ProteinTo},
                    {"PercentFatDM", nn.FatFrom + "-" + nn.FatTo},
                    {"PercentSugarDM", nn.SugarFrom + "-" + nn.SugarTo}
                };

                sheet4.FillVariableValue(sheet4_dic);
            }
            #endregion


            #region Điền dữ liệu vào sheet 2
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            IVTRange sheet2_header = sheet2.GetRange("A6", "C8");
            IVTRange sheet2_row = sheet2.GetRange("A9", "C9");
            #endregion

            #region Điền dữ liệu vào sheet6

            IVTWorksheet sheet6 = oBook.GetSheet(6);
            List<DishInspectionBO> ListOtherService = GetListOtherService(SchoolID, dishInspection.InspectedDate).ToList();
            
            #endregion

            #region Nếu đã nhập thực phẩm thì điền dữ liệu các sheet sau
            if (lstFoodID.Count > 0)
            {
                int sheet2_firstRow = sheet1_lastRow + 2;
                sheetData.CopyPasteSameSize(sheet2_header, sheet2_firstRow, 1);
                List<Object> lstMealData = new List<object>();
                List<RateByMealBO> lstRateByMeal = FoodCatBusiness.CaculatorRateByMeal(dishInspection.DailyMenuID, SchoolID, AcademicYearID, dishInspection.EatingGroupID);
                if (lstRateByMeal != null && lstRateByMeal.Count > 0)
                {
                    int countMeal = 0;
                    foreach (RateByMealBO rateByMeal in lstRateByMeal)
                    {
                        countMeal++;
                        sheetData.CopyPasteSameSize(sheet2_row, sheet2_firstRow + 2 + countMeal, 1);
                        Dictionary<string, object> mealData = new Dictionary<string, object>();
                        mealData["MealName"] = rateByMeal.MealName;
                        mealData["Percent"] = rateByMeal.Rate;
                        mealData["Norm"] = rateByMeal.EnergyDemandFrom + "-" + rateByMeal.EnergyDemandTo;
                        lstMealData.Add(mealData);
                    }
                }
                sheetData.GetRange("A" + (sheet2_firstRow + 3), "C" + (sheet2_firstRow + 3 + lstRateByMeal.Count)).FillVariableValue(new Dictionary<string, object> { { "listMeal", lstMealData } });
                int sheet2_lastRow = sheet2_firstRow + 3 + lstRateByMeal.Count;
            

                #region Điền dữ liệu sheet 3 vào sheet tổng hợp
                int sheet3_firstRow = sheet2_lastRow + 1;
                IVTRange sheet3_PLG = sheet3.GetRange("A6", "C13");
                sheetData.CopyPasteSameSize(sheet3_PLG, sheet3_firstRow, 1);
                Dictionary<string, object> dicNutrition1 = new Dictionary<string, object>
            {
                {"ProteinAnimal", mineralOfDailyMenu.ProteinAnimal}, 
                {"ProteinAnimalDM", Math.Round(mineralOfDailyMenu.ProteinAnimalMin, 2) + "-" + Math.Round(mineralOfDailyMenu.ProteinAnimalMax, 2)},
                {"ProteinPlant", mineralOfDailyMenu.ProteinPlant},
                {"ProteinPlantDM", Math.Round(mineralOfDailyMenu.ProteinPlantMin, 2) + "-" + Math.Round(mineralOfDailyMenu.ProteinPlantMax, 2)},
                {"FatAnimal", mineralOfDailyMenu.FatAnimal}, 
                {"FatAnimalDM", Math.Round(mineralOfDailyMenu.FatAnimalMin, 2) + "-" + Math.Round(mineralOfDailyMenu.FatAnimalMax, 2)},
                {"FatPlant", mineralOfDailyMenu.FatPlant}, 
                {"FatPlantDM", Math.Round(mineralOfDailyMenu.FatPlantMin, 2) + "-" + Math.Round(mineralOfDailyMenu.FatPlantMax, 2)},
                {"Sugar", mineralOfDailyMenu.Sugar}, 
                {"SugarDM", Math.Round(mineralOfDailyMenu.SugarMin, 2) + "-" + Math.Round(mineralOfDailyMenu.SugarMax, 2)}
            };
                sheetData.GetRange("A" + sheet3_firstRow, "C" + (sheet3_firstRow + 7)).FillVariableValue(dicNutrition1);

                Dictionary<string, object> dicNutrition2 = new Dictionary<string, object>
            {
                {"Calo", mineralOfDailyMenu.Calo}, 
                {"CaloDM", Math.Round(mineralOfDailyMenu.CaloMin, 2) + "-" + Math.Round(mineralOfDailyMenu.CaloMax, 2)}
            };

                List<Object> sheet3_ListData = new List<object>();
                List<FoodMineralMenuBO> lstFoodMineralMenu = mineralOfDailyMenu.lstFoodMineral;
                IVTRange sheet3_Nutrition = sheet3.GetRange("A15", "C15");

                if (lstFoodMineralMenu != null && lstFoodMineralMenu.Count > 0)
                {
                    int countMineral = 0;
                    foreach (FoodMineralMenuBO foodMineralMenu in lstFoodMineralMenu)
                    {
                        countMineral++;
                        sheetData.CopyPasteSameSize(sheet3_Nutrition, sheet3_firstRow + 7 + countMineral, 1);
                        Dictionary<string, object> sheet3_data = new Dictionary<string, object>();
                        sheet3_data["MineralName"] = foodMineralMenu.MineralName;
                        sheet3_data["Content"] = foodMineralMenu.Value;
                        sheet3_data["Norm"] = Math.Round(foodMineralMenu.ValueFrom, 2) + "-" + Math.Round(foodMineralMenu.ValueTo, 2);
                        sheet3_ListData.Add(sheet3_data);
                    }
                }
                sheetData.GetRange("A" + (sheet3_firstRow + 8), "C" + (sheet3_firstRow + 8 + lstFoodMineralMenu.Count)).FillVariableValue(new Dictionary<string, object> { { "listNutrition", sheet3_ListData } });
                IVTRange sheet3_Calo = sheet3.GetRange("A14", "C14");
                sheetData.CopyPasteSameSize(sheet3_Calo, sheet3_firstRow + 8 + lstFoodMineralMenu.Count, 1);
                sheetData.GetRange("A" + (sheet3_firstRow + 8 + lstFoodMineralMenu.Count), "C" + (sheet3_firstRow + 8 + lstFoodMineralMenu.Count)).FillVariableValue(dicNutrition2);

                int sheet3_lastRow = sheet3_firstRow + 9 + lstFoodMineralMenu.Count;
                #endregion

                #region Copy sheet4 vào sheet tổng
                int sheet4_firstRow = sheet3_lastRow + 1;
                IVTRange sheet4_all = sheet4.GetRange("A6", "F11");
                sheetData.CopyPasteSameSize(sheet4_all, sheet4_firstRow, 1);
                int sheet4_lastRow = sheet4_firstRow + 6;
                #endregion

                #region Điền dữ liệu sheet5 vào sheet tổng
                int sheet5_firstRow = sheet4_lastRow + 1;
                int sheet5_lastRow = sheet5_firstRow + 3;
                IVTRange sheet5_Header = sheet5.GetRange("A6", "G8");
                IVTRange sheet5_FoodGroup = sheet5.GetRange("A9", "G9");
                IVTRange sheet5_Food = sheet5.GetRange("A10", "G10");
                IVTRange sheet_Sum = sheet5.GetRange("A11", "G11");
                sheetData.CopyPasteSameSize(sheet5_Header, sheet5_firstRow, 1);
                sheet5_firstRow += 2;
                if (lsDailyFoodInspection != null && lsDailyFoodInspection.Count() > 0)
                {
                    IQueryable<FoodGroup> lsFoodGroup = lsDailyFoodInspection.Select(o => o.FoodGroup).Distinct();
                    if (lsFoodGroup != null && lsFoodGroup.Count() > 0)
                    {
                        int countFoodGroup = 0;
                        int countFood = 0;
                        foreach (FoodGroup foodGroup in lsFoodGroup.ToList())
                        {
                            countFoodGroup++;
                            sheetData.CopyPasteSameSize(sheet5_FoodGroup, sheet5_firstRow + countFoodGroup + countFood, 1);
                            sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 1, "Thực phẩm xuất " + foodGroup.FoodGroupName);
                            IQueryable<DailyFoodInspection> lsDailyFoodInspectionByGroup = lsDailyFoodInspection.Where(o => o.FoodGroupID == foodGroup.FoodGroupID);
                            if (lsDailyFoodInspectionByGroup != null && lsDailyFoodInspectionByGroup.Count() > 0)
                            {
                                foreach (DailyFoodInspection dailyFoodInspection in lsDailyFoodInspectionByGroup.ToList())
                                {
                                    lstFoodID.Add(dailyFoodInspection.FoodID);
                                    lstWeight.Add(dailyFoodInspection.Weight);
                                    countFood++;
                                    sheetData.CopyPasteSameSize(sheet5_Food, sheet5_firstRow + countFoodGroup + countFood, 1);
                                    sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 1, countFood);
                                    sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 2, dailyFoodInspection.FoodCat.FoodName);
                                    sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 3, dailyFoodInspection.Weight * dishInspection.NumberOfChildren / 1000);
                                    sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 4, dailyFoodInspection.Weight);
                                    sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 5, dailyFoodInspection.Weight * dishInspection.NumberOfChildren / 1000 * dailyFoodInspection.FoodCat.DiscardedRate / 100);
                                    sheetData.SetCellValue(sheet5_firstRow + countFoodGroup + countFood, 6, dailyFoodInspection.FoodCat.Price);
                                    formular = "=F" + (sheet5_firstRow + countFoodGroup + countFood) + "*C" + (sheet5_firstRow + countFoodGroup + countFood);
                                    sheetData.SetFormulaValue(sheet5_firstRow + countFoodGroup + countFood, 7, formular);
                                }
                            }
                        }
                        sheet5_lastRow = sheet5_firstRow + countFoodGroup + countFood + 1;
                        sheetData.CopyPasteSameSize(sheet_Sum, sheet5_lastRow, 1);
                        formular = "=SUM(G" + (sheet5_firstRow + 1) + ":G" + (sheet5_lastRow - 1) + ")";
                        sheetData.SetFormulaValue(sheet5_lastRow, 7, formular);
                        sheet5_lastRow++;
                    }
                }
                #endregion

                #region Điền dữ liệu sheet6 vào sheet tổng
                int sheet6_firstRow = sheet5_lastRow + 1;
                IVTRange sheet6_header = sheet6.GetRange("A6", "D11");
                IVTRange sheet6_row = sheet6.GetRange("A12", "D12");
                IVTRange sheet6_footer = sheet6.GetRange("A13", "D15");

                sheetData.CopyPasteSameSize(sheet6_header, sheet6_firstRow, 1);
                List<Object> sheet6_listData = new List<object>();
                if (ListOtherService != null && ListOtherService.Count > 0)
                {
                    int countService = 0;
                    foreach (DishInspectionBO otherService in ListOtherService)
                    {
                        countService++;
                        sheetData.CopyPasteSameSize(sheet6_row, sheet6_firstRow + 5 + countService, 1);
                        Dictionary<string, object> sheet6_data = new Dictionary<string, object>();
                        sheet6_data["ServiceName"] = otherService.OtherServiceName;
                        sheet6_data["Number"] = otherService.NumberOfChildren;
                        sheet6_data["Price"] = otherService.PriceService;
                        sheet6_listData.Add(sheet6_data);
                        formular = "=C" + (sheet6_firstRow + 5 + countService) + "*B" + (sheet6_firstRow + 5 + countService);
                        sheetData.SetFormulaValue(sheet6_firstRow + 5 + countService, 4, formular);
                    }
                    sheetData.GetRange("A" + (sheet6_firstRow + 5), "D" + (sheet6_firstRow + 5 + ListOtherService.Count)).FillVariableValue(new Dictionary<string, object> { { "listServices", sheet6_listData } });
                }
                sheetData.CopyPasteSameSize(sheet6_footer, sheet6_firstRow + 6 + ListOtherService.Count, 1);

                //Tổng tiền dịch vụ
                if (ListOtherService.Count != 0)
                {
                    formular = "=SUM(D" + (sheet6_firstRow + 6) + ":D" + (sheet6_firstRow + 5 + ListOtherService.Count) + ")";
                    sheetData.SetFormulaValue(sheet6_firstRow + 5, 4, formular);
                }
                else
                {
                    sheetData.SetCellValue(sheet6_firstRow + 5, 4, 0);
                }
                //Tiền thực phẩm
                formular = "=G" + (sheet5_lastRow - 1);
                sheetData.SetFormulaValue(sheet6_firstRow + 4, 4, formular);
                //Tiền thu
                int DailyPrice = DailyDishCostBusiness.MaxDailyDishCost(dishInspection.InspectedDate, SchoolID);
                sheetData.SetCellValue(sheet6_firstRow + 7 + ListOtherService.Count, 2, dishInspection.NumberOfChildren);
                sheetData.SetCellValue(sheet6_firstRow + 7 + ListOtherService.Count, 3, DailyPrice);
                formular = "=C" + (sheet6_firstRow + 7 + ListOtherService.Count) + "*B" + (sheet6_firstRow + 7 + ListOtherService.Count);
                sheetData.SetFormulaValue(sheet6_firstRow + 7 + ListOtherService.Count, 4, formular);
                //Tiền chi trong ngày
                formular = "=D" + (sheet6_firstRow + 4) + "+D" + (sheet6_firstRow + 5);
                sheetData.SetFormulaValue(sheet6_firstRow + 3, 4, formular);
                //Chênh lệch
                formular = "=D" + (sheet6_firstRow + 7 + ListOtherService.Count) + "-D" + (sheet6_firstRow + 3);
                sheetData.SetFormulaValue(sheet6_firstRow + 8 + ListOtherService.Count, 4, formular);
                #endregion
            }
            #endregion
            firstSheet.Delete();
            sheet2.Delete();
            sheet3.Delete();
            sheet4.Delete();
            sheet5.Delete();
            sheet6.Delete();    
            return oBook.ToStream();
        }
        #endregion
    }
}