﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class ViolatedCandidateBusiness
    {
        public List<ViolatedCandidate> InsertAll(int SchoolID, int AppliedLevel, string ViolationDetail, int ExamViolationTypeID, List<int> ListCandidateID)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            foreach (int candidateID in ListCandidateID)
            {
                CandidateBusiness.CheckAvailable(candidateID, "Candidate_Label_CandidateID");

                Candidate candidate = CandidateBusiness.Find(candidateID);

                //Kiem tra tuong thich Candidate
                bool candidateCompatible = new CandidateRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , candidate.ExaminationID },
                                                {"SchoolID", SchoolID}
                                            }, null);
                if (!candidateCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //Kiem tra tuong thich EducationLevel
                bool educationLevelCompatible = new CandidateRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                                , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , candidate.EducationLevelID},
                                                {"Grade", AppliedLevel}
                                            }, null);
                if (!educationLevelCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                Examination examination = ExaminationBusiness.Find(candidate.ExaminationID);

                if (examination == null || examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                    throw new BusinessException("ViolatedCandidate_Validate_CurrentStage");

                if (!AcademicYearBusiness.IsCurrentYear(candidate.Examination.AcademicYearID))
                    throw new BusinessException("Common_IsCurrentYear_Err");
            }

            Utils.ValidateMaxLength(ViolationDetail, 160, "Common_Validate_MaxLength");

            ExamViolationTypeBusiness.CheckAvailable(ExamViolationTypeID, "ExamViolationType_Label_ExamViolationTypeID");

            ExamViolationType examViolationType = ExamViolationTypeBusiness.Find(ExamViolationTypeID);

            if (!examViolationType.SchoolID.HasValue || examViolationType.SchoolID.Value != SchoolID)
                throw new BusinessException("ViolatedCandidate_Validate_Invalid");

            if (!examViolationType.AppliedForCandidate.HasValue || !examViolationType.AppliedForCandidate.Value)
                throw new BusinessException("ViolatedCandidate_Validate_Invalid");
            #endregion

            List<ViolatedCandidate> lstViolatedCandidate = new List<ViolatedCandidate>();
            foreach (int candidateID in ListCandidateID)
            {
                Candidate candidate = CandidateBusiness.Find(candidateID);
                ViolatedCandidate vc = new ViolatedCandidate();
                vc.CandidateID = candidateID;
                vc.EducationLevelID = candidate.EducationLevelID;
                vc.ExaminationID = candidate.ExaminationID;
                vc.ExamViolationTypeID = ExamViolationTypeID;
                vc.NameListCode = candidate.NamedListCode;
                vc.PupilCode = candidate.PupilProfile.PupilCode;
                vc.PupilID = candidate.PupilID;
                vc.RoomID = candidate.RoomID;
                vc.SubjectID = candidate.SubjectID;
                vc.ViolationDetail = ViolationDetail;
                this.Insert(vc);
                lstViolatedCandidate.Add(vc);
            }
            return lstViolatedCandidate;
        }

        public ViolatedCandidate Update(int ViolatedCandidateID, int SchoolID, int AppliedLevel, string ViolationDetail, int ExamViolationTypeID)
        {
            #region Validation
            SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");

            this.CheckAvailable(ViolatedCandidateID, "ViolatedCandidate_Label_ViolatedCandidateID");

            ViolatedCandidate violatedCandidate = this.Find(ViolatedCandidateID);


            //Kiem tra tuong thich Candidate
            bool examinationCompatible = ViolatedCandidateRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , violatedCandidate.ExaminationID},
                                                {"SchoolID", SchoolID}
                                            }, null);
            if (!examinationCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra tuong thich EducationLevel
            bool educationLevelCompatible = new CandidateRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel"
                                            , new Dictionary<string, object>()
                                            {
                                                {"EducationLevelID" , violatedCandidate.EducationLevelID},
                                                {"Grade", AppliedLevel}
                                            }, null);
            if (!educationLevelCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            Utils.ValidateMaxLength(ViolationDetail, 160, "Common_Validate_MaxLength");

            ExamViolationTypeBusiness.CheckAvailable(ExamViolationTypeID, "ExamViolationType_Label_ExamViolationTypeID");

            ExamViolationType examViolationType = ExamViolationTypeBusiness.Find(ExamViolationTypeID);

            if (!examViolationType.SchoolID.HasValue || examViolationType.SchoolID.Value != SchoolID)
                throw new BusinessException("ViolatedCandidate_Validate_Invalid");

            if (!examViolationType.AppliedForCandidate.HasValue || !examViolationType.AppliedForCandidate.Value)
                throw new BusinessException("ViolatedCandidate_Validate_Invalid");

            Examination examination = ExaminationBusiness.Find(violatedCandidate.ExaminationID);

            if (examination == null || examination.CurrentStage != (int)SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                throw new BusinessException("ViolatedCandidate_Validate_CurrentStage");

            if (!AcademicYearBusiness.IsCurrentYear(examination.AcademicYearID))
                throw new BusinessException("Common_IsCurrentYear_Err");
            #endregion

            violatedCandidate.ExamViolationTypeID = ExamViolationTypeID;
            violatedCandidate.ViolationDetail = ViolationDetail;

            return violatedCandidate;
        }

        public void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListViolatedCandidateID)
        {
            #region
            foreach (int ViolatedCandidateID in ListViolatedCandidateID)
            {
                this.CheckAvailable(ViolatedCandidateID, "ViolatedCandidate_Label_ViolatedCandidateID");

                ViolatedCandidate violatedCandidate = this.Find(ViolatedCandidateID);

                //Kiem tra tuong thich Candidate
                bool examinationCompatible = ViolatedCandidateRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ExaminationID" , violatedCandidate.ExaminationID},
                                                {"SchoolID", SchoolID}
                                            }, null);
                if (!examinationCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (violatedCandidate.Examination.CurrentStage != SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED)
                    throw new BusinessException("ViolatedCandidate_Validate_CurrentStage");

                if (!AcademicYearBusiness.IsCurrentYear(violatedCandidate.Examination.AcademicYearID))
                    throw new BusinessException("Common_IsCurrentYear_Err");
            }
            #endregion

            foreach (int ViolatedCandidateID in ListViolatedCandidateID)
            {
                this.Delete(ViolatedCandidateID);
            }
        }

        private IQueryable<ViolatedCandidate> Search(IDictionary<string, object> dic)
        {
            int? ViolatedCandidateID = Utils.GetNullableInt(dic, "ViolatedCandidateID");
            int? ExaminationID = Utils.GetNullableInt(dic, "ExaminationID");
            int? CandidateID = Utils.GetNullableInt(dic, "CandidateID");
            int? ExaminationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");
            int? EducationLevelID = Utils.GetNullableShort(dic, "EducationLevelID");
            int? PupilID = Utils.GetNullableInt(dic, "PupilID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string PupilName = Utils.GetString(dic, "PupilName");
            string NamedListCode = Utils.GetString(dic, "NamedListCode");
            int? RoomID = Utils.GetNullableInt(dic, "RoomID");
            int? ExamViolationTypeID = Utils.GetNullableShort(dic, "ExamViolationTypeID");
            string ViolationDetail = Utils.GetString(dic, "ViolationDetail");
            int? AcademicYearID = Utils.GetNullableInt(dic, "AcademicYearID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? AppliedLevel = Utils.GetNullableInt(dic, "AppliedLevel");

            var listViolatedCandidate = this.All;

            if (ViolatedCandidateID.HasValue && ViolatedCandidateID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.ViolatedCandidateID == ViolatedCandidateID);
            if (ExaminationID.HasValue && ExaminationID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.ExaminationID == ExaminationID);
            if (CandidateID.HasValue && CandidateID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.CandidateID == CandidateID);
            if (ExaminationSubjectID.HasValue && ExaminationSubjectID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.SubjectID == ExaminationSubjectID);
            if (EducationLevelID.HasValue && EducationLevelID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.EducationLevelID == EducationLevelID);
            if (PupilID.HasValue && PupilID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.PupilID == PupilID);
            if (!string.IsNullOrEmpty(PupilCode))
                listViolatedCandidate = listViolatedCandidate.Where(u => u.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            if (!string.IsNullOrEmpty(PupilName))
                listViolatedCandidate = listViolatedCandidate.Where(u => u.PupilProfile.FullName.ToLower().Contains(PupilName.ToLower()));
            if (!string.IsNullOrEmpty(NamedListCode))
                listViolatedCandidate = listViolatedCandidate.Where(u => u.NameListCode.ToLower().Contains(NamedListCode.ToLower()));
            if (RoomID.HasValue && RoomID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.RoomID == RoomID);
            if (ExamViolationTypeID.HasValue && ExamViolationTypeID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.ExamViolationTypeID == ExamViolationTypeID);
            if (!string.IsNullOrEmpty(ViolationDetail))
                listViolatedCandidate = listViolatedCandidate.Where(u => u.ViolationDetail.ToLower().Contains(ViolationDetail.ToLower()));
            if (AcademicYearID.HasValue && AcademicYearID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.Examination.AcademicYearID == AcademicYearID);
            if (SchoolID.HasValue && SchoolID.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.Examination.SchoolID == SchoolID);
            if (AppliedLevel.HasValue && AppliedLevel.Value > 0)
                listViolatedCandidate = listViolatedCandidate.Where(u => u.EducationLevel.Grade == AppliedLevel);

            return listViolatedCandidate;
        }

        public IQueryable<ViolatedCandidate> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0 || dic == null) return null;
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        public Stream CreateViolatedCandidateReport(IDictionary<string, object> dic, out string reportName)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.THI_THISINHVPQC);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? educationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            int? examinationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");
            List<ViolatedCandidate> listViolatedCandidate = Search(dic)
                .ToList().OrderBy(o => o.Candidate.NamedListNumber).ThenBy(o => o.Candidate.PupilProfile.Name)
                .ThenBy(o => o.Candidate.PupilProfile.FullName)
                .ToList(); ;

            IVTWorksheet templateSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet, "J10");

            #region tao khung
            IVTRange topRow = templateSheet.GetRange("A11", "J11");
            IVTRange midRow = templateSheet.GetRange("A12", "J12");
            IVTRange botRow = templateSheet.GetRange("A13", "J13");

            for (int i = 0; i < listViolatedCandidate.Count; i++)
            {
                if (i % 5 == 0) sheet.CopyPaste(topRow, i + 11, 1);
                else if (i % 5 == 4 || i == listViolatedCandidate.Count - 1) sheet.CopyPaste(botRow, i + 11, 1);
                else sheet.CopyPaste(midRow, i + 11, 1);
            }
            #endregion

            #region do du lieu vao report
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examination exam = ExaminationBusiness.Find(examinationID);
            AcademicYear acaYear = AcademicYearBusiness.Find(academicYearID);

            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName.ToUpper();
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["ProvinceDate"] = school.Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            dicVarable["ExaminationName"] = examinationID > 0 && exam != null ? exam.Title : "Tất cả";
            dicVarable["AcademicYear"] = acaYear != null ? (acaYear.Year + " - " + (acaYear.Year + 1)) : "Tất cả";

            int index = 1;
            List<object> listData = new List<object>();            
            listViolatedCandidate.ForEach(u => {
                Dictionary<string, object> obj = new Dictionary<string, object>();
                obj["Index"] = index++;
                obj["NameListCode"] = u.NameListCode;
                obj["FullName"] = u.Candidate.PupilProfile.FullName;
                obj["BirthDate"] = u.PupilProfile.BirthDate.ToString("dd/MM/yyyy");
                obj["GenreTitle"] = u.PupilProfile.Genre == SystemParamsInFile.GENRE_FEMALE ? "Nữ" : "Nam";
                obj["EducationLevelResolution"] = u.EducationLevel.Resolution;
                obj["SubjectName"] = u.ExaminationSubject.SubjectCat.SubjectName;
                obj["RoomTitle"] = u.ExaminationRoom.RoomTitle;
                obj["ExamViolationTypeResolution"] = u.ExamViolationType.Resolution;
                obj["ViolationDetail"] = u.ViolationDetail;
                listData.Add(obj);
            });
            if (listData.Count == 0)
                throw new BusinessException("ViolatedCandidate_Validate_NotExistPupil");
            dicVarable["list"] = listData;
            sheet.FillVariableValue(dicVarable);
            templateSheet.Delete();
            #endregion

            //Xoa dong template neu list rong
            //if (listData.Count == 0)
            //    sheet.DeleteRow(11);

            #region reportName

            string examTitle = exam != null ? ReportUtils.StripVNSign(exam.Title) : "TatCa";
            string educationLevel = educationLevelID.HasValue && educationLevelID.Value > 0 ? ReportUtils.StripVNSign(EducationLevelBusiness.Find(educationLevelID.Value).Resolution) : "TatCa";
            string subjectName = examinationSubjectID.HasValue && examinationSubjectID.Value > 0 ? ReportUtils.StripVNSign(ExaminationSubjectBusiness.Find(examinationSubjectID.Value).SubjectCat.SubjectName) : "TatCa";

            reportName = reportName.Replace("Examination", examTitle)
                            .Replace("EducationLevel", educationLevel)
                            .Replace("Subject", subjectName);

            reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
            #endregion

            return oBook.ToStream();
        }
    }
}
