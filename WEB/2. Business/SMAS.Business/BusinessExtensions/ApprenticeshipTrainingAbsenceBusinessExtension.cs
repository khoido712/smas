/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{




    public partial class ApprenticeshipTrainingAbsenceBusiness
    {
        #region Validate

        public void Validate(ApprenticeshipTrainingAbsence apprenticeshiptrainingabsence)
        {
            ValidationMetadata.ValidateObject(apprenticeshiptrainingabsence);

            //Tháng phải nằm trong khoảng từ 1 đến 12. Kiểm tra trường Month
            if (apprenticeshiptrainingabsence.AbsentDate.Month < 1 || apprenticeshiptrainingabsence.AbsentDate.Month > 12)
            {
                throw new BusinessException("Common_Validate_NotMouth");
            }
            //Giá trị buổi học phải nằm trong các giá trị

            if (apprenticeshiptrainingabsence.SectionInDay != 1 && apprenticeshiptrainingabsence.SectionInDay != 2 && apprenticeshiptrainingabsence.SectionInDay != 4)
            {
                throw new BusinessException("Common_Validate_NotSectionInDay");
            }
            //Năm học đã hoàn thành không thể khai báo thời gian học nghề. Kiểm tra AcademicYear nếu đã tồn tại AcademicYear với Year + 1 thì không cho phép tác động đến hệ thống
            //dữ liệu lỗi 
            //AcademicYear academicyear = AcademicYearRepository.Find(apprenticeshiptrainingabsence.AcademicYearID);

            //List<AcademicYear> academicyearcheck = AcademicYearRepository.All.Where(o => (o.SchoolID == apprenticeshiptrainingabsence.SchoolID)).ToList();
            //foreach (var y in academicyearcheck)
            //{
            //    if (y.Year > academicyear.Year)
            //    {
            //        throw new BusinessException("Common_Validate_EndSchoolYear");
            //    }
            //}

            ////Học kỳ phải thuộc 3 giá trị Học kỳ:
            //if (apprenticeshiptrainingabsence.Semester != GlobalConstants.SEMESTER_OF_YEAR_FIRST || apprenticeshiptrainingabsence.Semester != GlobalConstants.SEMESTER_OF_YEAR_SECOND || apprenticeshiptrainingabsence != null)
            //{
            //    throw new BusinessException("Common_Validate_NotSemester");
            //}

            PupilProfile pupilprofile = PupilProfileRepository.Find(apprenticeshiptrainingabsence.PupilID);
            if (pupilprofile.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_NotStudy");
            }

        }
        #endregion

        #region InsertApprenticeshipTrainingAbsence
        public ApprenticeshipTrainingAbsence InsertApprenticeshipTrainingAbsence(ApprenticeshipTrainingAbsence Insert)
        {
            // Validate giữ liệu
            Validate(Insert);
            // Insert
            return base.Insert(Insert);
        }


        #endregion

        #region UpdateApprenticeshipTrainingAbsence
        public ApprenticeshipTrainingAbsence UpdateApprenticeshipTrainingAbsence(ApprenticeshipTrainingAbsence Update)
        {
            // Validate giữ liệu
            Validate(Update);
            // Update
            return base.Update(Update);
        }


        #endregion

        #region DeleteApprenticeshipTrainingAbsence

        public void DeleteApprenticeshipTrainingAbsence(int ApprenticeshipTrainingAbsenceID)
        {
            // kiểm tra xem dữ liệu định xóa có tồn tại không
            bool checkDelete = new ApprenticeshipTrainingAbsenceRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ApprenticeshipTrainingAbsence",
           new Dictionary<string, object>()
                {
                    {"ApprenticeshipTrainingAbsenceID",ApprenticeshipTrainingAbsenceID}
                }, null);
            if (!checkDelete)
            {
                throw new BusinessException("Common_Delete_Exception");
            }

            else
            {
                base.Delete(ApprenticeshipTrainingAbsenceID);

            }

        }

        #endregion

        #region SearchApprenticeshipTrainingAbsence

        private IQueryable<ApprenticeshipTrainingAbsence> Search(IDictionary<string, object> SearchInfo)
        {
            int ApprenticeshipTrainingAbsenceID = Utils.GetInt(SearchInfo, "ApprenticeshipTrainingAbsenceID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int ApprenticeShipClassID = Utils.GetInt(SearchInfo, "ApprenticeShipClassID");
            int EducationLevelID = Utils.GetShort(SearchInfo, "EducationLevelID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            string PupilCode = Utils.GetString(SearchInfo, "PupilCode");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            DateTime? AbsentDate = Utils.GetDateTime(SearchInfo, "AbsentDate");
            DateTime? FromDate = Utils.GetDateTime(SearchInfo, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(SearchInfo, "ToDate");
            int SectionInDay = Utils.GetInt(SearchInfo, "SectionInDay");
            bool IsAccepted = Utils.GetBool(SearchInfo, "IsAccepted");

            IQueryable<ApprenticeshipTrainingAbsence> lsApprenticeshipTrainingAbsence = this.ApprenticeshipTrainingAbsenceRepository.All;

            if (ClassID != 0)
            {
                lsApprenticeshipTrainingAbsence = from at in lsApprenticeshipTrainingAbsence
                                                  join cp in ClassProfileBusiness.All on at.ClassID equals cp.ClassProfileID
                                                  where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                  && at.ClassID == ClassID
                                                  select at;
            }
            if (ApprenticeshipTrainingAbsenceID != 0)
            {
                lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (o.ApprenticeshipTrainingAbsenceID == ApprenticeshipTrainingAbsenceID));
            }

            if (ApprenticeShipClassID != 0)
            {
                lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (o.ApprenticeshipClassID == ApprenticeShipClassID));
            }

            if (PupilID != 0)
            {
                lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (o.PupilID == PupilID));
            }

            if (FromDate != null && ToDate != null)
            {
                lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (FromDate <= o.AbsentDate) && (o.AbsentDate <= ToDate));
            }

            if (EducationLevelID != 0)
            {
                lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (o.EducationLevelID.Value == EducationLevelID));
            }

            //if (IsAccepted != null)
            //{
            //    lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (o.IsAccepted == IsAccepted));
            //}

            if (SectionInDay == 1 || SectionInDay == 2 || SectionInDay == 4)
            {
                lsApprenticeshipTrainingAbsence = lsApprenticeshipTrainingAbsence.Where(o => (o.SectionInDay == SectionInDay));
            }

            return lsApprenticeshipTrainingAbsence;

        }

        #endregion

        #region SearchBySchool


        public IQueryable<ApprenticeshipTrainingAbsence> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);

            }

        }


        #endregion


        #region GetHashKey

        public string GetHashKey(ApprenticeshipTrainingAbsence ApprenticeshipTrainingAbsence)
        {


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = ApprenticeshipTrainingAbsence.AcademicYearID;
            dic["EducationLevelID"] = ApprenticeshipTrainingAbsence.EducationLevelID;
            dic["ClassID"] = ApprenticeshipTrainingAbsence.ClassID;
            dic["SchoolID"] = ApprenticeshipTrainingAbsence.SchoolID;
            return ReportUtils.GetHashKey(dic);
        }

        #endregion


        #region CreatExcelApprenticeshipTrainingAbsence

        public int CreatExcelApprenticeshipTrainingAbsence(string InputParameterHashKey, IDictionary<string, object> SearchInfo)
        {
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int ApprenticeshipClassID = Utils.GetInt(SearchInfo, "ApprenticeShipClassID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            DateTime? FromDate = Utils.GetDateTime(SearchInfo, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(SearchInfo, "ToDate");
            int Month = Utils.GetInt(SearchInfo, "Month");
            int SectionInDay = Utils.GetInt(SearchInfo, "SectionInDay");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int Year = this.AcademicYearBusiness.Find(AcademicYearID).Year;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DIEM_DANH_LOP_NGHE);
            string AcademicYear = this.AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper();
            string SchoolName = this.SchoolProfileBusiness.Find(SchoolID).SchoolName.ToUpper();
            string SubjectName = this.ApprenticeshipClassBusiness.All.Where(o => o.ApprenticeshipClassID == ApprenticeshipClassID).FirstOrDefault().ApprenticeshipSubject.SubjectName.ToUpper();
            string ClassName = this.ApprenticeshipClassBusiness.All.Where(o => o.ApprenticeshipClassID == ApprenticeshipClassID).FirstOrDefault().ClassName.ToUpper();
            string Suppervising = this.SchoolProfileBusiness.All.Where(o => o.SchoolProfileID == SchoolID).FirstOrDefault().SupervisingDept.SupervisingDeptName.ToUpper();
            string Province = this.SchoolProfileBusiness.Find(SchoolID).Province.ProvinceName;
            string SemesterName = "";
            string SectionName = "";

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";

            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                Year = Year + 1;
                SemesterName = "HỌC KỲ II";
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }

            if (SectionInDay == SystemParamsInFile.SECTION_MORNING)
            {
                SectionName = "BUỔI SÁNG";

            }
            if (SectionInDay == SystemParamsInFile.SECTION_AFTERNOON)
            {
                SectionName = "BUỔI CHIỀU";
            }
            if (SectionInDay == SystemParamsInFile.SECTION_EVENING)
            {
                SectionName = "BUỔI TỐI";
            }


            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
          

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            //Fill dữ liệu chung
            
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A5", "ĐIỂM DANH NGHỀ THÁNG " + Month + " " + AcademicYear + " - " + ClassName + " - " + SubjectName + " - " + SectionName);



            // lấy ra list Pupil
            IDictionary<string, object> SearchInfoPupil = new Dictionary<string, object>();
            SearchInfoPupil["AcademicYearID"] = AcademicYearID;
            SearchInfoPupil["ApprenticeshipClassID"] = ApprenticeshipClassID;
            SearchInfoPupil["EducationLevelID"] = EducationLevelID;

            List<ApprenticeshipTraining> lstPupil = this.ApprenticeshipTrainingBusiness.SearchBySchool(SchoolID, SearchInfoPupil).ToList();

            // lấy  lstApprenticeshipAbsence

            IDictionary<string, object> SearchInfoAbsence = new Dictionary<string, object>();
            SearchInfoAbsence["AcademicYearID"] = AcademicYearID;
            SearchInfoAbsence["EducationLevelID"] = EducationLevelID;
            SearchInfoAbsence["FromDate"] = FromDate;
            SearchInfoAbsence["ToDate"] = ToDate;
            SearchInfoAbsence["SectionInDay"] = SectionInDay;
            SearchInfoAbsence["ApprenticeshipClassID"] = ApprenticeshipClassID;

            List<ApprenticeshipTrainingAbsence> lstApprenticeshipAbsence = this.ApprenticeshipTrainingAbsenceBusiness.SearchBySchool(SchoolID, SearchInfoAbsence).ToList();

            // Map dữ liệu tương ứng @@
            List<ApprenticeshipTrainingAbsenceBO> listAbsenceViewModel = new List<ApprenticeshipTrainingAbsenceBO>();
            Dictionary<int, List<ApprenticeshipTrainingAbsenceBO>> Dic_listAbsence = new Dictionary<int, List<ApprenticeshipTrainingAbsenceBO>>();
            foreach (ApprenticeshipTraining item in lstPupil)
            {
                ApprenticeshipTrainingAbsenceBO objAbsence = new ApprenticeshipTrainingAbsenceBO();
                objAbsence.PupilID = item.PupilID;
                objAbsence.PupilCode = item.PupilCode;
                objAbsence.FullName = item.PupilProfile.FullName;
                objAbsence.ApprenticeshipClassID = ApprenticeshipClassID;
                List<ApprenticeshipTrainingAbsence> lstTrainingAbsence = lstApprenticeshipAbsence.Where(o => o.PupilID == item.PupilID).ToList();
                int sumP = 0;
                int sumK = 0;
                if (lstTrainingAbsence.Count() != 0)
                {
                    List<ApprenticeshipTrainingAbsenceBO> listAbsenceIsAcceped = new List<ApprenticeshipTrainingAbsenceBO>();
                    foreach (var itemAbsence in lstTrainingAbsence)
                    {
                        ApprenticeshipTrainingAbsenceBO AbsenceIsAcceped = new ApprenticeshipTrainingAbsenceBO();
                        AbsenceIsAcceped.AbsentDate = itemAbsence.AbsentDate;
                        AbsenceIsAcceped.IsAccepted = itemAbsence.IsAccepted;
                        if (itemAbsence.IsAccepted == false)
                        {
                            sumK++;
                        }
                        if (itemAbsence.IsAccepted == true)
                        {
                            sumP++;
                        }
                        listAbsenceIsAcceped.Add(AbsenceIsAcceped);
                    }
                    if (!Dic_listAbsence.Keys.Contains(item.PupilID))
                        Dic_listAbsence.Add(item.PupilID, listAbsenceIsAcceped);
                }
                objAbsence.SumP = sumP;
                objAbsence.SumK = sumK;
                listAbsenceViewModel.Add(objAbsence);

            }



            // lấy ra danh listScheduleDetail
            IDictionary<string, object> SearchInfoSchedule = new Dictionary<string, object>();
            SearchInfoSchedule["AcademicYearID"] = AcademicYearID;
            SearchInfoSchedule["ApprenticeshipClassID"] = ApprenticeshipClassID;
            SearchInfoSchedule["Month"] = Month;
            SearchInfoSchedule["SectionInDay"] = SectionInDay;
            ApprenticeshipTrainingSchedule objApprenticeshipSchedule = this.ApprenticeshipTrainingScheduleBusiness.SearchBySchool(SchoolID, SearchInfoSchedule).FirstOrDefault();

            Dictionary<int, List<int>> Dic_listScheduleDetail = new Dictionary<int, List<int>>();
            foreach (var itemViewModel in listAbsenceViewModel)
            {
                List<int> listScheduleDetail = new List<int>();
                if (objApprenticeshipSchedule != null)
                {
                    for (int i = 0; i < objApprenticeshipSchedule.ScheduleDetail.Length; i++)
                    {
                        if (objApprenticeshipSchedule.ScheduleDetail.Substring(i, 1) == "K")
                            listScheduleDetail.Add(0);
                        else
                            listScheduleDetail.Add(i+1);
                    }
                }
                else
                {
                    
                        listScheduleDetail.Add(0);
              
                }

                if (!Dic_listScheduleDetail.Keys.Contains(itemViewModel.PupilID))
                {
                    Dic_listScheduleDetail.Add(itemViewModel.PupilID, listScheduleDetail);
                }
            }


            //Sua HeaderTeamplate
            int headerStartCol = 4;
            int headerRowDay = 6;
            int headerRowDayOfWeek = 7;

            List<int> lstDetail = Dic_listScheduleDetail.FirstOrDefault().Value;
            IVTRange rangeTitle = firstSheet.GetRange("D6","D6");
            int h = 0;
            foreach (var itemDetail in lstDetail)
            {
               
                if (itemDetail != 0)
                {
                    h++;
                    DateTime Date = new DateTime(Year, Month, itemDetail);
                    string DayofWeek = Date.DayOfWeek.ToString();
                    string dayofweek = "";
                    if (DayofWeek == "Monday") { dayofweek = "T2"; }
                    if (DayofWeek == "Tuesday") { dayofweek = "T3"; }
                    if (DayofWeek == "Wednesday") { dayofweek = "T4"; }
                    if (DayofWeek == "Thursday") { dayofweek = "T5"; }
                    if (DayofWeek == "Friday") { dayofweek = "T6"; }
                    if (DayofWeek == "Saturday") { dayofweek = "T7"; }
                    if (DayofWeek == "Sunday") { dayofweek = "CN"; }
                    sheet.CopyPasteSameSize(rangeTitle,6,4+h);
                    sheet.SetCellValue(headerRowDay, headerStartCol, itemDetail);
                    sheet.CopyPasteSameSize(rangeTitle, 7, 4 + h);
                    sheet.SetCellValue(headerRowDayOfWeek, headerStartCol, dayofweek);
                    headerStartCol++;
                   
                }
           
            }

            //Cac cot con lai
            int colK = headerStartCol;
            sheet.CopyPasteSameSize(rangeTitle, 7, 4 + h);
            sheet.SetCellValue(7, colK, "K");
            int colP = colK + 1;
            sheet.CopyPasteSameSize(rangeTitle, 7, 5 + h);
            sheet.SetCellValue(7, colP, "P");
            int colTS = colP + 1;
            sheet.CopyPasteSameSize(rangeTitle, 7, 6 + h);
            sheet.SetCellValue(7, colTS, "TS");
            sheet.CopyPasteSameSize(rangeTitle, 6, 5 + h);
            sheet.CopyPasteSameSize(rangeTitle, 6, 6 + h);
            sheet.SetCellValue(6, colK, "Tổng số buổi nghỉ");
            sheet.MergeRow(6, colK, colK+2);


            string strColLast = convertNumberToString(colK-1);
            string strColK = convertNumberToString(colK);
            string strColP = convertNumberToString(colP);
            string strColTS = convertNumberToString(colTS); 

            int  startrow = 8;

            IVTRange rangeAbsenb = firstSheet.GetRange("D8", "D8");
            IVTRange range = firstSheet.GetRange(startrow, 1, startrow, colTS);
            IVTRange rangetotal = firstSheet.GetRange("E8", "E8");
            for (int j = 0; j < listAbsenceViewModel.Count; j++)
            {
                sheet.CopyPasteSameRowHeigh(range, startrow);
                sheet.SetCellValue(startrow, 1, j + 1);
                //Tên học sinh
                sheet.SetCellValue(startrow, 2, listAbsenceViewModel[j].FullName);
                sheet.SetCellValue(startrow, 3, listAbsenceViewModel[j].PupilCode);

               List<ApprenticeshipTrainingAbsenceBO> AppAbsence = Dic_listAbsence.Where(o => o.Key == listAbsenceViewModel[j].PupilID).FirstOrDefault().Value;               
               int startCol = 4;
               int g = 3;
                   foreach (var itemDetail in lstDetail)
                   {
                       if (itemDetail != 0)
                       {
                           bool check = false;
                           if (AppAbsence != null)
                           {
                               foreach (var itemAbsenb in AppAbsence)
                               {
                                   if (itemDetail == itemAbsenb.AbsentDate.Day)
                                   {
                                       g++;
                                       check = true;
                                       sheet.CopyPasteSameSize(rangeAbsenb, startrow, g);
                                       sheet.SetCellValue(startrow, startCol, itemAbsenb.IsAccepted == true ? "P" : "K");

                                   }

                               }
                           }
                           

                           if (check == false)
                           {
                               g++;
                               sheet.CopyPasteSameSize(rangeAbsenb, startrow, g);
                           }

                           startCol++;
                       }

                   }
               
               
               char D = 'D';
               int d = (int)D;

                char last = (char)(d + startCol - 5);

                char sumK = (char)(d + startCol - 4);
                char sumP = (char)(d + startCol - 3);

                string strCol = convertNumberToString(startCol);

                string tempK = "=COUNTIF(D" + startrow + ":" + strColLast +startrow.ToString()+ ",'K')";
                string tempP = "=COUNTIF(D" + startrow + ":" + strColLast +startrow.ToString()+ ",'P')";
                
                sheet.CopyPasteSameSize(rangetotal, startrow, startCol);
                sheet.SetCellValue(startrow, startCol, tempK.Replace('\'', '\"').Replace(",", ";")); startCol++;
                sheet.CopyPasteSameSize(rangetotal, startrow, startCol);
                sheet.SetCellValue(startrow, startCol, tempP.Replace('\'', '\"').Replace(",", ";")); startCol++;
                sheet.CopyPasteSameSize(rangetotal, startrow, startCol);
                sheet.SetCellValue(startrow, startCol, "=SUM(" + strColK + (startrow).ToString() + ":" + strColP + (startrow).ToString() + ")");

                startrow++;
            }

            //Xoá sheet template
            firstSheet.Delete();

                Stream Data = oBook.ToStream();
                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = SystemParamsInFile.REPORT_DIEM_DANH_LOP_NGHE;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = InputParameterHashKey;
                pr.ReportData = ReportUtils.Compress(Data);
                pr.SentToSupervisor = true;


                //Tạo tên file HS_THPT_BangDiemDanhNghe_[CLass]_[Semester]_[Subject]
                string outputNamePattern = reportDef.OutputNamePattern;

                string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                outputNamePattern = outputNamePattern.Replace("[CLass]", ReportUtils.StripVNSign(ClassName));
                outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(SubjectName));
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID", EducationLevelID},
                {"SchoolID",SchoolID}
            };
                ProcessedReportParameterRepository.Insert(dic, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                int FileID = pr.ProcessedReportID;

                return FileID;
         

        
        }

        #endregion

        public string convertNumberToString(int column)
        {
            string value = "";
            char firstCh = 'A';
            int index = 1;
            char ch = 'A';
            while (index != column)
            {
               
                if (ch > 'Z')
                {
                    value = firstCh.ToString();
                    firstCh++;
                    ch = 'A';
                }
                index++;
                ch++;
            }
            return value + (char)(ch) ;
        }
    }

}
