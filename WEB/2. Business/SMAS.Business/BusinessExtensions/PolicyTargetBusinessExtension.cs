/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class PolicyTargetBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int PROVINCECODE_MAX_LENGTH = 10; //Độ dài mã tỉnh thành phố
        private const int PROVINCENAME_MAX_LENGTH = 50; //Độ dài tên tỉnh thành phố
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn

        #endregion

        #region Search

        /// <summary>
        /// Tìm kiếm đối tượng/Diện chính sách
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution"> Diễn giải đối tượng / diện chính sách </param>
        /// <param name="Description"> Mô tả</param>
        /// <param name="IsActive"> Biến kích hoạt</param>
        /// <returns>IQueryable<PolicyTarget></returns>
        public IQueryable<PolicyTarget> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<PolicyTarget> lsPolicyTarget = PolicyTargetRepository.All;

            if (IsActive.HasValue)
               lsPolicyTarget= lsPolicyTarget.Where(pol => pol.IsActive == IsActive);
            if (Resolution.Trim().Length != 0)
                lsPolicyTarget = lsPolicyTarget.Where(pol => pol.Resolution.Contains(Resolution.ToLower()));
            if (Description.Trim().Length != 0)
               lsPolicyTarget=  lsPolicyTarget.Where(pol => pol.Description.Contains(Description.ToLower()));

            return lsPolicyTarget;

        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa đối tượng/Diện chính sách
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="PolicyTargetID"> ID đối tượng / diện chính sách</param>
        public void Delete(int PolicyTargetID )
        {
            //Bạn chưa chọn diện chính sách cần xóa hoặc diện chính sách bạn chọn đã bị xóa khỏi hệ thống
            new PolicyTargetBusiness(null).CheckAvailable((int)PolicyTargetID, "TeacherGrade_Label_GradeResolution", true);

            //Không thể xóa diện chính sách đang sử dụng
            new PolicyTargetBusiness(null).CheckConstraintsWithIsActive(GlobalConstants.LIST_SCHEMA, "PolicyTarget", PolicyTargetID, "TeacherGrade_Label_GradeResolutionFailed");
            base.Delete(PolicyTargetID, true);
        }
        #endregion


        #region Insert

        /// <summary>
        /// Thêm đối tượng/Diện chính sách
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="policyTarget">Đối tượng Đối tượng /Diện chính sách</param>
        /// <returns>Đối tượng Đối tượng /Diện chính sách</returns>
        public override PolicyTarget Insert(PolicyTarget policyTarget)
        {
            //Tên diện chính sách không được để trống 
            Utils.ValidateRequire(policyTarget.Resolution, "PolicyTarget_Label_Resolution");
            //Độ dài trường diện chính sách không được vượt quá 50 
            Utils.ValidateMaxLength(policyTarget.Resolution, RESOLUTION_MAX_LENGTH, "PolicyTarget_Label_Resolution");
            //Tên diện chính sách đã tồn tại 
            new PolicyTargetBusiness(null).CheckDuplicate(policyTarget.Resolution, GlobalConstants.LIST_SCHEMA, "PolicyTarget", "Resolution", true, policyTarget.PolicyTargetID, "PolicyTarget_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(policyTarget.Description, DESCRIPTION_MAX_LENGTH, "Description");


            //Insert
            return base.Insert(policyTarget);

        }
        #endregion

        #region Update

        /// <summary>
        /// Sửa đối tượng/Diện chính sách
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="policyTarget"> Đối tượng Đối tượng /Diện chính sách</param>
        /// <returns>Đối tượng Đối tượng /Diện chính sách</returns>
        public override PolicyTarget Update(PolicyTarget policyTarget)
        {
            //Bạn chưa chọn diện chính sách cần sửa hoặc diện chính sách bạn chọn đã bị xóa khỏi hệ thống
            new PolicyTargetBusiness(null).CheckAvailable((int)policyTarget.PolicyTargetID, "TeacherGrade_Label_GradeResolution", true);
            //Tên diện chính sách không được để trống 
            Utils.ValidateRequire(policyTarget.Resolution, "PolicyTarget_Label_Resolution");
            //Độ dài trường diện chính sách không được vượt quá 50 
            Utils.ValidateMaxLength(policyTarget.Resolution, RESOLUTION_MAX_LENGTH, "PolicyTarget_Label_Resolution");
            //Tên diện chính sách đã tồn tại 
            new PolicyTargetBusiness(null).CheckDuplicate(policyTarget.Resolution, GlobalConstants.LIST_SCHEMA, "PolicyTarget", "Resolution", true, policyTarget.PolicyTargetID, "PolicyTarget_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(policyTarget.Description, DESCRIPTION_MAX_LENGTH, "Description");



            return base.Update(policyTarget);
        }
        #endregion
    }
}
