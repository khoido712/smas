﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;


namespace SMAS.Business.Business
{
    public partial class ConcurrentWorkAssignmentBusiness
    {

        private const int MOVETO_MAX_LENGTH = 200;   //Đơn vị mới chuyển đến
        private const int RESOLUTION_MAX_LENGTH = 500;   //Độ dài trường Số văn bản / quyết định thay đổi công tác / nghỉ hưu
        private const int DESCRIPTION_MAX_LENGTH = 200; //Độ dài ghi chú
        private const int EMPLOYEESTATUS = 1; // Trạng thái làm việc

        private void Validate(ConcurrentWorkAssignment Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkTypeID"] = Entity.ConcurrentWorkTypeID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ConcurrentWorkType", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }


            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }


        }

        /// <summary>
        /// Tìm kiếm công việc kiêm nhiệm
        /// </summary>
        /// <param name="teacherID">ID cán bộ</param>
        /// <param name="FullName">Tên đầy đủ</param>
        /// <param name="FacultyID">ID tổ bộ môn</param>
        /// <param name="ConcurrentWorkTypeID">ID loại hình công việc kiêm nhiệm</param>
        /// <param name="EmployeeCode">Mã giáo viên</param>
        /// <param name="AcademicYearID">ID năm học</param>
        /// <param name="SchoolID">ID trường</param>
        /// <returns>
        /// Danh sách công việc kiêm nhiệm
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public IQueryable<ConcurrentWorkAssignment> Search(IDictionary<string, object> dic)
        {
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            string FullName = Utils.GetString(dic, "FullName");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int ConcurrentWorkTypeID = Utils.GetInt(dic, "ConcurrentWorkTypeID");
            string EmployeeCode = Utils.GetString(dic, "EmployeeCode");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");


            //IQueryable<ConcurrentWorkAssignment> lsConcurrentWorkAssignment = ConcurrentWorkAssignmentRepository.All;
            IQueryable<ConcurrentWorkAssignment> lsConcurrentWorkAssignment = from p in ConcurrentWorkAssignmentRepository.All
                                                                              join q in EmployeeHistoryStatusBusiness.All on p.TeacherID equals q.EmployeeID
                                                                              where q.SchoolID == SchoolID && q.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                                              //&& q.FromDate == (from v in EmployeeHistoryStatusBusiness.All.Where(o => o.SchoolID == SchoolID)
                                                                              //                  where v.EmployeeID == p.TeacherID
                                                                              //                  select v.FromDate).Max()
                                                                              select p;




            if (TeacherID != 0)
            {
                lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.TeacherID == TeacherID);
            }
            if (FullName.Trim().Length != 0)
            { lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.Employee.FullName.Contains(FullName.ToLower())); }
            if (FacultyID != 0)
            {
                lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.FacultyID == FacultyID);
            }
            if (ConcurrentWorkTypeID != 0)
            {
                lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.ConcurrentWorkTypeID == ConcurrentWorkTypeID);
            }
            if (EmployeeCode.Trim().Length != 0)
            { lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.Employee.EmployeeCode.Contains(EmployeeCode.ToLower())); }
            if (AcademicYearID != 0)
            {
                lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.SchoolID == SchoolID);
            }

            if (IsActive != null)
            {
                lsConcurrentWorkAssignment = lsConcurrentWorkAssignment.Where(o => o.IsActive == IsActive);
            }

            return lsConcurrentWorkAssignment;
        }


        /// <summary>
        /// Xóa công việc kiêm nhiệm
        /// </summary>
        /// <param name="ConcurrentWorkAssignmentID">Đối tượng ConcurrentWorkAssignmentID</param>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public void Delete(int SchoolID, int ConcurrentWorkAssignmentID)
        {
            //Dữ liệu không tồn tại
            this.CheckAvailable(ConcurrentWorkAssignmentID, "ConcurrentWorkAssignment_Label_ConcurrentWorkAssignmentID");

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkAssignmentID"] = ConcurrentWorkAssignmentID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra có ràng buộc dữ liệu không
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkAssignmentID"] = ConcurrentWorkAssignmentID;
            SearchInfo["IsActive"] = true;
            bool hasConstraint = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkReplacement", SearchInfo);
            if (hasConstraint)
            {
                List<object> Params = new List<object>();
                Params.Add("ConcurrentWorkAssignment_Label");
                throw new BusinessException("Common_Validate_Using", Params);
            }

            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(this.Find(ConcurrentWorkAssignmentID).AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            base.Delete(ConcurrentWorkAssignmentID, true);
        }


        /// <summary>
        /// Sửa công việc kiêm nhiệm
        /// </summary>
        /// <param name="concurrentWorkAssignment">Đối tượng ConcurrentWorkAssignment</param>
        /// <returns>
        /// Đối tượng ConcurrentWorkAssignment
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public override ConcurrentWorkAssignment Update(ConcurrentWorkAssignment concurrentWorkAssignment)
        {
            this.Validate(concurrentWorkAssignment);
            //ton tai
            this.CheckAvailable(concurrentWorkAssignment.ConcurrentWorkAssignmentID, "ConcurrentWorkAssignment_Label_ConcurrentWorkAssignmentID");

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkAssignmentID"] = concurrentWorkAssignment.ConcurrentWorkAssignmentID;
            SearchInfo["SchoolID"] = concurrentWorkAssignment.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkAssignmentID"] = concurrentWorkAssignment.ConcurrentWorkAssignmentID;
            SearchInfo["AcademicYearID"] = concurrentWorkAssignment.AcademicYearID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = concurrentWorkAssignment.TeacherID;
            SearchInfo["SchoolID"] = concurrentWorkAssignment.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["TeacherID"] = concurrentWorkAssignment.TeacherID;
            SearchInfo["FacultyID"] = concurrentWorkAssignment.FacultyID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeacherOfFaculty", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //Cán bộ có trạng thái Đang làm việc thuộc tổ bộ môn được chọn – Kiểm tra trạng thái EmployeeStatus = 1 (Đang làm việc) trong bảng Employee
            if (concurrentWorkAssignment.Employee.EmploymentStatus != EMPLOYEESTATUS)
            {
                throw new BusinessException("TeachingAssignment_Validate_TeacherNotWorking");
            }

            return base.Update(concurrentWorkAssignment);
        }

        /// <summary>
        /// Thêm công việc kiêm nhiệm
        /// </summary>
        /// <param name="concurrentWorkAssignment">Đối tượng ConcurrentWorkAssignment</param>
        /// <returns>
        /// Đối tượng ConcurrentWorkAssignment
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public void Insert(ConcurrentWorkAssignment concurrentWorkAssignment, List<int> ListTeacherID)
        {
            this.Validate(concurrentWorkAssignment);

            foreach (int TeacherID in ListTeacherID)
            {
                //Kiểm tra sự tồn tại của TeacherID trong bảng Employee
                EmployeeBusiness.CheckAvailable(TeacherID, "ConcurrentWorkAssignment_Label_TeacherID", true);

                // Kiểm tra tính hợp lệ dữ liệu
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EmployeeID"] = TeacherID;
                SearchInfo["SchoolID"] = concurrentWorkAssignment.SchoolID;
                bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiểm tra tính hợp lệ dữ liệu
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["TeacherID"] = TeacherID;
                SearchInfo["FacultyID"] = concurrentWorkAssignment.FacultyID;
                compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeacherOfFaculty", SearchInfo);
                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }


                // Kiểm tra trạng thái giáo viên phải đang làm việc
                Employee Teacher = null;
                if (Teacher == null)
                {
                    Teacher = this.EmployeeBusiness.Find(TeacherID);
                }
                if (Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                {
                    throw new BusinessException("ConcurrentWorkAssignment_Validate_TeacherNotWorking");
                }

                //
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["TeacherID"] = TeacherID;
                SearchInfo["SchoolID"] = concurrentWorkAssignment.SchoolID;
                SearchInfo["AcademicYearID"] = concurrentWorkAssignment.AcademicYearID;
                SearchInfo["ConcurrentWorkTypeID"] = concurrentWorkAssignment.ConcurrentWorkTypeID;
                SearchInfo["IsActive"] = true;
                bool isExist = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment", SearchInfo);
                if (!isExist)
                {
                    ConcurrentWorkAssignment newObj = new ConcurrentWorkAssignment();
                    newObj.AcademicYearID = concurrentWorkAssignment.AcademicYearID;
                    newObj.ConcurrentWorkTypeID = concurrentWorkAssignment.ConcurrentWorkTypeID;
                    newObj.FacultyID = concurrentWorkAssignment.FacultyID;
                    newObj.SchoolID = concurrentWorkAssignment.SchoolID;
                    newObj.TeacherID = TeacherID;
                    newObj.IsActive = true;
                    base.Insert(newObj);
                }


            }

        }


        public IQueryable<ConcurrentWorkAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;
                return this.Search(dic);
            }
        }

        /// <summary>
        /// Xóa phân công việc (chỉ hủy kích hoạt, ko xóa hẳn khỏi DB)
        /// </summary>
        /// <author>AnhVD9 20140924</author>
        /// <param name="SchoolID"></param>
        /// <param name="TeacherID"></param>
        public void Delete(Dictionary<string, object> SearchInfo)
        {
            var LstWork = this.Search(SearchInfo).ToList();
            int Count = 0;
            if (LstWork != null && (Count = LstWork.Count) > 0)
            {
                ConcurrentWorkAssignment entity;
                for (int i = 0; i < Count; i++)
                {
                    entity = LstWork[i];
                    base.Delete(entity.ConcurrentWorkAssignmentID, true); // Deactive cả các năm học cũ - Ko xóa
                }
                this.ConcurrentWorkAssignmentBusiness.Save();
            }
        }
    }
}