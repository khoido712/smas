﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class PupilGraduationBusiness
    {

        #region InsertPupilGraduation
        /// <summary>
        /// Thêm mới thông tin bằng tốt nghiệp của học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <edit>hieund 20/02/2013</edit>
        /// <param name="ethnic">Đối tượng PupilGraduation</param>
        /// <returns>Đối tượng PupilGraduation</returns>
        public void InsertPupilGraduation(List<PupilGraduation> listPupilGraduation)
        {
            if (listPupilGraduation == null || listPupilGraduation.Count == 0)
            {
                return;
            }
            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            PupilGraduation pg = listPupilGraduation.First();
            int schoolID = pg.SchoolID;
            int academicYearID = pg.AcademicYearID;
            int graduationLevel = pg.GraduationLevel;
            List<int> listPupilID = listPupilGraduation.Select(o => o.PupilID).Distinct().ToList();
            
            // Lay danh sach hoc sinh de Xep loai tot nghiep dua tren danh sach ID hoc sinh
            List<PupilProfile> listPupilProfile = (from pp in PupilProfileBusiness.All
                                                  join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                                  where poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && listPupilID.Contains(pp.PupilProfileID)
                                                  select pp).ToList();

            if (listPupilProfile.Count != listPupilID.Count
                && listPupilProfile.Any(o => o.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING && o.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_GRADUATED))
            {
                throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");
            }
            // Xoa du lieu cu
            var existedData = this.Search(new Dictionary<string, object>() { { "SchoolID", schoolID }, { "AcademicYearID", academicYearID }, { "GraduationLevel", graduationLevel } })
                .Where(o => listPupilID.Contains(o.PupilID))
                .ToList();
            this.DeleteAll(existedData);
            // Lay du lieu hoc sinh cua lop hoc
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object>(){
            { "AcademicYearID", academicYearID }
            }).Where(o => listPupilID.Contains(o.PupilID)).ToList();
            // Lay danh sach hoc sinh chua co dien, thiet lap lai cho cac hoc sinh do la "Du dieu kien du thi tot nghiep"
            List<PupilRanking> listPupilRanking = PupilRankingBusiness.SearchBySchool(schoolID, new Dictionary<string, object>()
            {
                {"AcademicYearID", academicYearID},{"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_ALL},
            }).Where(o => listPupilID.Contains(o.PupilID) && !o.StudyingJudgementID.HasValue).ToList();
            foreach (PupilGraduation pupil in listPupilGraduation)
            {
                ValidationMetadata.ValidateObject(pupil);
                if (pupil.GraduationLevel == SystemParamsInFile.EDUCATION_GRADE_SECONDARY && pupil.Status.HasValue && pupil.Status.Value == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT)
                {
                    //-	Cập nhật lại trạng thái học sinh trong bảng PupilOfClass với Status = PUPIL_STATUS_GRADUATED (2): Đã tốt nghiệp
                    //-	Trạng thái tốt nghiệp trong bảng PupilGraduation là 5 Đã tốt nghiệp
                    PupilOfClass pupilOfClass = listPupilOfClass.Where(o => o.PupilID == pupil.PupilID && o.ClassID == pupil.ClassID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).SingleOrDefault();
                    if (pupilOfClass != null)
                    {
                        pupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                        PupilOfClassBusiness.Update(pupilOfClass);

                        PupilProfile pupilProfile = listPupilProfile.Find(o => o.PupilProfileID == pupil.PupilID);
                        pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                        PupilProfileBusiness.Update(pupilProfile);
                    }
                }

                base.Insert(pupil);
                // Thiet lap lai dien cho HS
                PupilRanking pr = listPupilRanking.FirstOrDefault(o => o.PupilID == pupil.PupilID);
                //if (pr != null && !pr.StudyingJudgementID.HasValue)
                //{
                //    pr.StudyingJudgementID = SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION;
                //    PupilRankingBusiness.Update(pr);
                //}
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }
        #endregion

        #region UpdatePupilGraduation
        /// <summary>
        /// Update
        /// <edit>hieund : 20/02/2013</edit>
        /// </summary>
        /// <param name="listPupilGraduation"></param>
        public void UpdatePupilGraduation(List<PupilGraduation> listPupilGraduation)
        {
            PupilGraduation pg = listPupilGraduation.First();
            int schoolID = pg.SchoolID;
            int academicYearID = pg.AcademicYearID;
            int graduationLevel = pg.GraduationLevel;
            List<int> listPupilID = listPupilGraduation.Select(o => o.PupilID).Distinct().ToList();
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.SearchBySchool(schoolID).Where(o => listPupilID.Contains(o.PupilProfileID)).ToList();
            if (listPupilProfile.Count != listPupilID.Count || listPupilProfile.Any(o => o.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING
                && o.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_GRADUATED))
            {
                throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");
            }
            // Lay du lieu hoc sinh cua lop hoc
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object>(){
            { "AcademicYearID", academicYearID }
            }).Where(o => listPupilID.Contains(o.PupilID)).ToList();
            foreach (PupilGraduation pupil in listPupilGraduation)
            {
                ValidationMetadata.ValidateObject(pupil);
                //Lay hoc sinh                
                PupilProfile pupilProfile = listPupilProfile.Find(o => o.PupilProfileID == pupil.PupilID);
                if (pupil.Status == SystemParamsInFile.GRADUATION_STATUS_GRADUATED)
                {
                    PupilOfClass pupilOfClass = listPupilOfClass.Where(o => o.PupilID == pupil.PupilID && o.ClassID == pupil.ClassID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).SingleOrDefault();
                    if (pupilOfClass != null)
                    {
                        pupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                        PupilOfClassBusiness.Update(pupilOfClass);

                        pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                        PupilProfileBusiness.Update(pupilProfile);
                    }
                }

                // Nếu là học sinh cấp 2 chưa ở trạng thái chưa đủ điều kiệp xét duyệt hoặc đã tốt nghiệp
                // Cấp 3 ở trạng tháng chưa tốt nghiệp thì cập nhật lại trạng thái cho học sinh là đang học
                if ((pupil.GraduationLevel == SystemParamsInFile.EDUCATION_GRADE_SECONDARY
                    && pupil.Status != SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT && pupil.Status != SystemParamsInFile.GRADUATION_STATUS_GRADUATED) ||
                    (pupil.GraduationLevel == SystemParamsInFile.EDUCATION_GRADE_TERTIARY && pupil.Status != SystemParamsInFile.GRADUATION_STATUS_GRADUATED))
                {
                    PupilOfClass pupilOfClass = listPupilOfClass.Where(o => o.PupilID == pupil.PupilID && o.ClassID == pupil.ClassID && o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED).SingleOrDefault();
                    if (pupilOfClass != null)
                    {
                        pupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                        PupilOfClassBusiness.Update(pupilOfClass);

                        pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                        PupilProfileBusiness.Update(pupilProfile);
                    }
                }

                base.Update(pupil);
            }
        }
        #endregion

        #region DeletePupilGraduation
        /// <summary>
        /// Xoá thông tin bằng tốt nghiệp của học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <edit>hieund : 20/02/2013</edit>
        /// <param name="ethnic">Đối tượng PupilGraduation</param>
        /// <returns>Đối tượng PupilGraduation</returns>
        public void DeletePupilGraduation(int UserID, PupilGraduation PupilGraduation)
        {
            base.Delete(PupilGraduation.PupilGraduationID);

            PupilOfClass pupilOfClass = PupilOfClassBusiness.All.Where(o => o.PupilID == PupilGraduation.PupilID
                                                                            && o.ClassProfile.EducationLevel.Grade == PupilGraduation.GraduationLevel
                                                                            && o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                                .SingleOrDefault();
            if (pupilOfClass != null)
            {
                pupilOfClass.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
                PupilOfClassBusiness.UpdatePupilOfClass(UserID, pupilOfClass);

                PupilProfile pupilProfile = pupilOfClass.PupilProfile;
                pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                PupilProfileBusiness.Update(pupilProfile);
            }
        }
        #endregion

        #region DeletePupilGraduation
        /// <summary>
        /// Xoá thông tin bằng tốt nghiệp của học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <edit>hieund : 20/02/2013</edit>
        /// <param name="ethnic">Đối tượng PupilGraduation</param>
        /// <returns>Đối tượng PupilGraduation</returns>
        public void DeletePupilGraduation(List<PupilGraduation> listPupilGraduation)
        {
            if (listPupilGraduation == null || listPupilGraduation.Count == 0)
            {
                return;
            }
            PupilGraduation pg = listPupilGraduation.First();
            int schoolID = pg.SchoolID;
            int academicYearID = pg.AcademicYearID;
            int graduationLevel = pg.GraduationLevel;
            // Danh sach hoc sinh
            List<int> listPupilID = listPupilGraduation.Select(o => o.PupilID).Distinct().ToList();
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.SearchBySchool(schoolID).Where(o => listPupilID.Contains(o.PupilProfileID)).ToList();
            if (listPupilProfile.Count != listPupilID.Count || listPupilProfile.Any(o => o.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING
                && o.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_GRADUATED))
            {
                throw new BusinessException("PupilProfile_Validate_NotExistOrStudying");
            }
            // Xoa du lieu
            this.DeleteAll(listPupilGraduation);
            // Lay du lieu hoc sinh cua lop hoc
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object>(){
            { "AcademicYearID", academicYearID }
            }).Where(o => listPupilID.Contains(o.PupilID)).ToList();


            foreach (PupilGraduation pupil in listPupilGraduation)
            {
                //-	Cập nhật lại trạng thái học sinh trong bảng PupilOfClass với Status = PUPIL_STATUS_GRADUATED (2): Đã tốt nghiệp
                //-	Trạng thái tốt nghiệp trong bảng PupilGraduation là 5 Đã tốt nghiệp
                PupilOfClass pupilOfClass = listPupilOfClass.Where(o => o.PupilID == pupil.PupilID && o.ClassID == pupil.ClassID && o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED).SingleOrDefault();
                if (pupilOfClass != null)
                {
                    pupilOfClass.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                    PupilOfClassBusiness.Update(pupilOfClass);

                    PupilProfile pupilProfile = listPupilProfile.Find(o => o.PupilProfileID == pupil.PupilID);
                    pupilProfile.ProfileStatus = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                    PupilProfileBusiness.Update(pupilProfile);

                }
            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin bằng tốt nghiệp của học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilGraduation</param>
        /// <returns>Đối tượng PupilGraduation</returns>
        private IQueryable<PupilGraduation> Search(IDictionary<string, object> dic)
        {
            int PupilGraduationID = Utils.GetInt(dic, "PupilGraduationID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Year = Utils.GetInt(dic, "Year");
            int GraduationLevel = Utils.GetInt(dic, "GraduationLevel");
            int GraduationGrade = Utils.GetInt(dic, "GraduationGrade");
            string IssuedNumber = Utils.GetString(dic, "IssuedNumber");
            string RecordNumber = Utils.GetString(dic, "RecordNumber");
            DateTime? IssuedDate = Utils.GetDateTime(dic, "IssuedDate");
            DateTime? ReceivedDate = Utils.GetDateTime(dic, "ReceivedDate");
            int IsReceived = Utils.GetInt(dic, "IsReceived");
            string PriorityReason = Utils.GetString(dic, "PriorityReason");
            string Description = Utils.GetString(dic, "Description");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");

            //IQueryable<PupilGraduation> lsPupilGraduation = this.PupilGraduationRepository.All;

            Dictionary<string, object> dicPoc = new Dictionary<string, object>();
            dicPoc["ClassID"] = ClassID;
            dicPoc["PupilID"] = PupilID;
            dicPoc["AcademicYearID"] = AcademicYearID;
            dicPoc["PupilCode"] = PupilCode;
            dicPoc["FullName"] = FullName;
            dicPoc["Status"] = (new int[] { SystemParamsInFile.PUPIL_STATUS_GRADUATED, SystemParamsInFile.PUPIL_STATUS_STUDYING }).ToList();
            dicPoc["Check"] = "Check";

            var query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicPoc)
                        join pg in this.All on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { pg.SchoolID, pg.AcademicYearID, pg.ClassID, pg.PupilID }
                        select new { poc, pg };

            if (ClassID > 0)
                query = query.OrderBy(u => u.poc.OrderInClass).ThenBy(u => u.poc.PupilProfile.Name);
            else
                query = query.OrderBy(u => u.poc.PupilProfile.Name);

            IQueryable<PupilGraduation> lsPupilGraduation = query.Select(u => u.pg);

            // bắt đầu tìm kiếm
            if (PupilGraduationID != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.PupilGraduationID == PupilGraduationID));
            }

            //if (PupilID != 0)
            //{
            //    lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.PupilID == PupilID));
            //}

            //if (!string.IsNullOrEmpty(PupilCode))
            //{
            //    lsPupilGraduation = lsPupilGraduation.Where(pg => pg.PupilProfile.PupilCode.Contains(PupilCode));
            //}

            //if (!string.IsNullOrEmpty(FullName))
            //{
            //    lsPupilGraduation = lsPupilGraduation.Where(pg => pg.PupilProfile.FullName.Contains(FullName));
            //}

            //if (ClassID != 0)
            //{
            //    lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.ClassID == ClassID));
            //}

            //if (SchoolID != 0)
            //{
            //    lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.SchoolID == SchoolID));
            //}

            //if (AcademicYearID != 0)
            //{
            //    lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.AcademicYearID == AcademicYearID));
            //}

            if (Year != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.Year == Year));
            }

            if (GraduationLevel != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.GraduationLevel == GraduationLevel));
            }

            if (GraduationGrade != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pg => (pg.GraduationGrade == GraduationGrade));

            }
            if (IssuedNumber.Length != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.IssuedNumber == IssuedNumber));
            }
            if (RecordNumber.Length != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.RecordNumber == RecordNumber));
            }
            if (IssuedDate != null)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.IssuedDate == IssuedDate));
            }
            if (ReceivedDate != null)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.ReceivedDate == ReceivedDate));
            }
            if (IsReceived != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.IsReceived == IsReceived));
            }
            if (IssuedNumber.Length != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.IssuedNumber == IssuedNumber));
            }
            if (PriorityReason.Length != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.PriorityReason == PriorityReason));
            }
            if (Description.Length != 0)
            {
                lsPupilGraduation = lsPupilGraduation.Where(pu => (pu.Description == Description));
            }

            return lsPupilGraduation;

        }
        #endregion

        #region SearchBySchool
        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilGraduation</param>
        /// <returns>Đối tượng PupilGraduation</returns>
        public IQueryable<PupilGraduation> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }
        #endregion

        public List<PupilGraduationBO> GetApprovePupil(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> pocDic)
        {

            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
            int year = ac.Year;

            int? ClassID = Utils.GetNullableInt(pocDic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(pocDic, "lstPupilID");
            int modSchoolID = SchoolID % 100;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            DateTime? FirstSemesterStartDate = aca.FirstSemesterStartDate;
            DateTime? FirstSemesterEndDate = aca.FirstSemesterEndDate;
            DateTime? SecondSemesterStartDate = aca.SecondSemesterStartDate;
            DateTime? SecondSemesterEndDate = aca.SecondSemesterEndDate;

            var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, pocDic).Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                       join pg in PupilGraduationBusiness.All.Where(u => u.GraduationLevel == AppliedLevel)
                               on new { poc.PupilID, poc.ClassID, poc.AcademicYearID }
                               equals new { pg.PupilID, pg.ClassID, pg.AcademicYearID } into g1
                       join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && !u.PeriodID.HasValue && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                               on new { poc.PupilID, poc.ClassID, poc.AcademicYearID }
                               equals new { pr.PupilID, pr.ClassID, pr.AcademicYearID } into g2
                       from j1 in g1.DefaultIfEmpty()
                       from j2 in g2.DefaultIfEmpty()
                       join ca in CapacityLevelBusiness.All on j2.CapacityLevelID equals ca.CapacityLevelID into g3
                       from j3 in g3.DefaultIfEmpty()
                       join co in ConductLevelBusiness.All on j2.ConductLevelID equals co.ConductLevelID into g4
                       from j4 in g4.DefaultIfEmpty()
                       select new PupilGraduationBO
                       {
                           AcademicYearID = AcademicYearID,
                           BirthDate = pp.BirthDate,
                           CapacityLevel = j3.CapacityLevel1,
                           CapacityLevelID = j2.CapacityLevelID,
                           ClassID = poc.ClassID,
                           ClassName = poc.ClassProfile.DisplayName,
                           ConductLevel = j4.Resolution,
                           ConductLevelID = j2.ConductLevelID,
                           SeperateKey = poc.ClassProfile.SeperateKey,
                           //2017-04-24: viethd31 fix loi khong where buoi hoc chinh
                           lstAbsence = pp.PupilAbsences.Where(u => u.AcademicYearID == AcademicYearID && u.ClassID == poc.ClassID
                          && ((u.AbsentDate >= FirstSemesterStartDate && u.AbsentDate <= FirstSemesterEndDate)
                                                                       || (u.AbsentDate >= SecondSemesterStartDate && u.AbsentDate <= SecondSemesterEndDate))),
                           //AbsentDays = pp.PupilAbsences.Where(u => u.AcademicYearID == AcademicYearID && u.ClassID == poc.ClassID).Count(),
                           Description = j1.Description,
                           ExemptionResolution = j1.Exemption,
                           FullName = pp.FullName,
                           Genre = pp.Genre,
                           GraduationGrade = j1.GraduationGrade,
                           GraduationLevel = j1.GraduationLevel,
                           IsReceived = j1.IsReceived,
                           IssuedDate = j1.IssuedDate,
                           IssuedNumber = j1.IssuedNumber,
                           PriorityReason = j1.PriorityReason,
                           PupilCode = pp.PupilCode,
                           PupilGraduationID = j1.PupilGraduationID,
                           PupilID = poc.PupilID,
                           ReceivedDate = j1.ReceivedDate,
                           RecordNumber = j1.RecordNumber,
                           SchoolID = SchoolID,
                           GraduationStatus = j1.Status,
                           Notification = j1.Description,
                           Year = year,
                           AverageMark = j2.AverageMark,
                           PupilRankingID = j2.PupilRankingID,
                           Name = pp.Name,
                           OrderInClass = poc.OrderInClass,
                           BirthPlace = pp.BirthPlace,
                           EthnicID = pp.Ethnic.EthnicID,
                           EthnicName = pp.Ethnic.EthnicName,
                           PupilLearningType = pp.PupilLearningType,
                           ItCertificatePoint = pp.ITQualificationLevel.EncouragePoint,
                           LanguageCertificatePoint = pp.ForeignLanguageGrade.EncouragePoint
                       };
           
            List<PupilGraduationBO> lstResult;
            if (lstPupilID.Count > 0)
            {
                data = data.Where(p => lstPupilID.Contains(p.PupilID));
            }

            if (ClassID.HasValue)
                lstResult = data.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ToList();
            else
                lstResult = data.OrderBy(u => u.Name).ToList();
            

            lstResult.ForEach(o => { o.AbsentDays = o.lstAbsence.Where(u => UtilsBusiness.GetListSectionID(o.SeperateKey.GetValueOrDefault()).Contains(u.Section)).Count(); o.lstAbsence = null; });

            return lstResult;
        }

        public List<PupilGraduationBO> GetGraduationPupil(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> pocDic)
        {
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
            int year = ac.Year;
            int modSchoolID = SchoolID % 100;

            int? ClassID = Utils.GetNullableInt(pocDic, "ClassID");
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            DateTime? FirstSemesterStartDate = aca.FirstSemesterStartDate;
            DateTime? FirstSemesterEndDate = aca.FirstSemesterEndDate;
            DateTime? SecondSemesterStartDate = aca.SecondSemesterStartDate;
            DateTime? SecondSemesterEndDate = aca.SecondSemesterEndDate;

            var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, pocDic).Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                       join pg in PupilGraduationBusiness.All.Where(u => u.GraduationLevel == AppliedLevel)
                               on new { poc.PupilID, poc.ClassID, poc.AcademicYearID }
                               equals new { pg.PupilID, pg.ClassID, pg.AcademicYearID } into g1
                       join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && !u.PeriodID.HasValue && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                               on new { poc.PupilID, poc.ClassID, poc.AcademicYearID }
                               equals new { pr.PupilID, pr.ClassID, pr.AcademicYearID } into g2
                       join ap in ApprenticeshipTrainingBusiness.All on poc.PupilID equals ap.PupilID into g3
                       from j1 in g1.DefaultIfEmpty()
                       from j2 in g2.DefaultIfEmpty()
                       from j3 in g3.DefaultIfEmpty()
                       join ca in CapacityLevelBusiness.All on j2.CapacityLevelID equals ca.CapacityLevelID into g4
                       from j4 in g4.DefaultIfEmpty()
                       join co in ConductLevelBusiness.All on j2.ConductLevelID equals co.ConductLevelID into g5
                       from j5 in g5.DefaultIfEmpty()
                       select new PupilGraduationBO
                       {
                           AcademicYearID = AcademicYearID,
                           BirthDate = pp.BirthDate,
                           CapacityLevel = j4.CapacityLevel1,
                           CapacityLevelID = j2.CapacityLevelID,
                           ClassID = poc.ClassID,
                           ClassName = poc.ClassProfile.DisplayName,
                           ConductLevel = j5.Resolution,
                           ConductLevelID = j2.ConductLevelID,
                           SeperateKey = poc.ClassProfile.SeperateKey,
                           //2017-04-24: viethd31 fix loi khong where buoi hoc chinh
                           lstAbsence = pp.PupilAbsences.Where(u => u.AcademicYearID == AcademicYearID && u.ClassID == poc.ClassID
                          && ((u.AbsentDate >= FirstSemesterStartDate && u.AbsentDate <= FirstSemesterEndDate)
                                                                       || (u.AbsentDate >= SecondSemesterStartDate && u.AbsentDate <= SecondSemesterEndDate))),
                           //AbsentDays = pp.PupilAbsences.Where(u => u.AcademicYearID == AcademicYearID && u.ClassID == poc.ClassID).Count(),

                           Description = j1.Description,
                           ExemptionResolution = j1.Exemption,
                           FullName = pp.FullName,
                           Genre = pp.Genre,
                           GraduationGrade = j1.GraduationGrade,
                           GraduationLevel = j1.GraduationLevel,
                           IsReceived = j1.IsReceived,
                           IssuedDate = j1.IssuedDate,
                           IssuedNumber = j1.IssuedNumber,
                           PriorityReason = j1.PriorityReason,
                           PupilCode = pp.PupilCode,
                           PupilGraduationID = j1.PupilGraduationID,
                           PupilID = poc.PupilID,
                           ReceivedDate = j1.ReceivedDate,
                           RecordNumber = j1.RecordNumber,
                           SchoolID = SchoolID,
                           GraduationStatus = j1.Status,
                           Notification = j1.Description,
                           Year = year,
                           AverageMark = j2.AverageMark,
                           PupilRankingID = j2.PupilRankingID,
                           ApprenticeshipRank = j3.Ranking,
                           PraisePoints = pp.PupilPraises.Where(u => u.EducationLevel.Grade == AppliedLevel).Sum(u => u.PraiseType.GraduationMark),
                           Name = pp.Name,
                           OrderInClass = poc.OrderInClass,
                           BirthPlace = pp.BirthPlace,
                           PupilLearningType = pp.PupilLearningType,
                           ItCertificatePoint = pp.ITQualificationLevel.EncouragePoint,
                           LanguageCertificatePoint = pp.ForeignLanguageGrade.EncouragePoint
                       };

            List<PupilGraduationBO> lstResult;
            if (ClassID.HasValue)
                lstResult = data.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ToList();
            else
                lstResult = data.OrderBy(u => u.Name).ToList();

            lstResult.ForEach(o => { o.AbsentDays = o.lstAbsence.Where(u => UtilsBusiness.GetListSectionID(o.SeperateKey.GetValueOrDefault()).Contains(u.Section)).Count(); o.lstAbsence = null; });

            return lstResult;
        }


        public List<PupilGraduationBO> SearchPupilGraduation(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> pocDic)
        {
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
            int year = ac.Year;
            int modSchoolID = SchoolID % 100;

            int? ClassID = Utils.GetNullableInt(pocDic, "ClassID");
            List<int> lstGraduationGrade = new List<int>(){ 1, 2, 3, 4};
            var data = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, pocDic).Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                       join pg in PupilGraduationBusiness.All.Where(u => u.GraduationLevel == AppliedLevel && (lstGraduationGrade.Contains(u.GraduationGrade))
                           && (u.Status.HasValue && (u.Status.Value == 1 || u.Status.Value == 5)))
                               on new { poc.PupilID, poc.ClassID, poc.AcademicYearID }
                               equals new { pg.PupilID, pg.ClassID, pg.AcademicYearID } into g1
                       join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && !u.PeriodID.HasValue && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                               on new { poc.PupilID, poc.ClassID, poc.AcademicYearID }
                               equals new { pr.PupilID, pr.ClassID, pr.AcademicYearID } into g2
                       join ap in ApprenticeshipTrainingBusiness.All on poc.PupilID equals ap.PupilID into g3
                       from j1 in g1.DefaultIfEmpty()
                       from j2 in g2.DefaultIfEmpty()
                       from j3 in g3.DefaultIfEmpty()
                       join ca in CapacityLevelBusiness.All on j2.CapacityLevelID equals ca.CapacityLevelID into g4
                       from j4 in g4.DefaultIfEmpty()
                       join co in ConductLevelBusiness.All on j2.ConductLevelID equals co.ConductLevelID into g5
                       from j5 in g5.DefaultIfEmpty()
                       select new PupilGraduationBO
                       {               
                           PupilID =  poc.PupilID,
                           ClassName = poc.ClassProfile.DisplayName,                        
                           FullName = pp.FullName,
                           Genre = pp.Genre,
                           GraduationGrade = j1.GraduationGrade,                       
                           PupilCode = pp.PupilCode,
                           GraduationStatus = j1.Status,                       
                           Name = pp.Name,
                           OrderInClass = poc.OrderInClass,
                           BirthPlace = pp.BirthPlace,
                           BirthDate = pp.BirthDate,
                           TempResidentalAddress = pp.TempResidentalAddress,
                           PermanentResidentalAddress = pp.PermanentResidentalAddress
                       };

            data = data.Where(x => x.GraduationGrade.HasValue);

            List<PupilGraduationBO> lstResult;
            if (ClassID.HasValue)
                lstResult = data.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ToList();
            else
                lstResult = data.OrderBy(u => u.Name).ToList();

            return lstResult;
        }
    }
}
