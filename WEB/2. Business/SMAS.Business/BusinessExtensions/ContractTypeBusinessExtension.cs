/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Loại hợp đồng
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class ContractTypeBusiness
    {
        #region insert
        /// <summary>
        /// Thêm mới Loại hợp đồng
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertContractType">doi tuong can them moi</param>
        /// <returns></returns>
        public override ContractType Insert(ContractType insertContractType)
        {
            ValidationMetadata.ValidateObject(insertContractType);

            //resolution rong
            //Utils.ValidateRequire(insertContractType.Resolution, "ContractType_Label_Resolution");

            //kiem tra resolution va description
            //Utils.ValidateMaxLength(insertContractType.Resolution, 100, "ContractType_Label_Resolution");
            //Utils.ValidateMaxLength(insertContractType.Description, 400, "ContractType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertContractType.Resolution, GlobalConstants.LIST_SCHEMA, "ContractType", "Resolution", true, 0, "ContractType_Label_Resolution");
            //this.CheckDuplicate(insertContractType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "ContractType", "AppliedLevel", true, 0, "ContractType_Label_ShoolID");
            // this.CheckDuplicateCouple(insertContractType.Resolution, insertContractType.SchoolID.ToString(),
            //GlobalConstants.LIST_SCHEMA, "ContractType", "Resolution", "SchoolID", true, 0, "ContractType_Label_Resolution");
            //kiem tra range



            insertContractType.IsActive = true;

            return base.Insert(insertContractType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Loại hợp đồng
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateContractType">doi tuong can sua</param>
        /// <returns></returns>
        public override ContractType Update(ContractType updateContractType)
        {
            //check avai
            new ContractTypeBusiness(null).CheckAvailable(updateContractType.ContractTypeID, "ContractType_Label_ContractTypeID");
            ValidationMetadata.ValidateObject(updateContractType);

            //resolution rong
            //Utils.ValidateRequire(updateContractType.Resolution, "ContractType_Label_Resolution");
            //Utils.ValidateMaxLength(updateContractType.Resolution, RESOLUTION_MAX_LENGTH, "ContractType_Label_Resolution");
            //Utils.ValidateMaxLength(updateContractType.Description, DESCRIPTION_MAX_LENGTH, "ContractType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(updateContractType.Resolution, GlobalConstants.LIST_SCHEMA, "ContractType", "Resolution", true, updateContractType.ContractTypeID, "ContractType_Label_Resolution");

            //this.CheckDuplicateCouple(updateContractType.Resolution, updateContractType.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "ContractType", "Resolution", "SchoolID", true, updateContractType.ContractTypeID, "ContractType_Label_Resolution");

            // this.CheckDuplicate(updateContractType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "ContractType", "AppliedLevel", true, updateContractType.ContractTypeID, "ContractType_Label_ShoolID");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu ContractTypeID ->vao csdl lay ContractType tuong ung roi 
            //so sanh voi updateContractType.SchoolID
            //.................
            updateContractType.IsActive = true;
            return base.Update(updateContractType);
        }
        #endregion

        #region Delete
        /// <summary>
        ///Xóa Loại hợp đồng
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="ContractTypeId">id doi tuong can xoa</param>
        public void Delete(int ContractTypeId)
        {
            //check avai
            new ContractTypeBusiness(null).CheckAvailable(ContractTypeId, "ContractType_Label_ContractTypeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "ContractType", ContractTypeId, "ContractType_Label_ContractTypeIDFailed");

            base.Delete(ContractTypeId, true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Loại hợp đồng
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<ContractType> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic, "Resolution");
            string description = Utils.GetString(dic, "Description");
            int? Type = Utils.GetNullableByte(dic, "Type");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<ContractType> lsContractType = this.ContractTypeRepository.All;

            if (isActive.HasValue)
            {
                lsContractType = lsContractType.Where(em => (em.IsActive == isActive));
            }

            if (Type.HasValue)
            {
                lsContractType = lsContractType.Where(em => (em.Type == Type));
            }

            if (resolution.Trim().Length != 0)
            {
                lsContractType = lsContractType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (description.Trim().Length != 0)
            {
                lsContractType = lsContractType.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }
            return lsContractType;
        }
        #endregion
    }
}
