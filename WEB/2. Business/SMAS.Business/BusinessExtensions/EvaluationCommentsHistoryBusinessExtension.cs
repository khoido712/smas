﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;
using SMAS.VTUtils.Log;


namespace SMAS.Business.Business
{
    public partial class EvaluationCommentsHistoryBusiness
    {
        private static string CommentStr = "Nhận xét tháng: ";
        private static string LOG_NXCK = "Nhận xét CK: ";
        /// <summary>
        /// Lay danh gia nhan xet cua hoc sinh trong lop tu bang lich su
        /// </summary>
        /// <param name="schoolID">ID truong</param>
        /// <param name="academicYearID">ID nam hoc</param>
        /// <param name="classID">id lop hoc</param>
        /// <param name="semesterID">hoc ky</param>
        /// <param name="subjectID">Tieu chi danh gia: Neu la HDGD thi la subjectId</param>
        /// <param name="TypeOfTeacher">Loai giao vien: 1: GVBM, 2:GVBM</param>
        /// <param name="evaluationID">Mat danh gia</param>
        /// <returns></returns>
        public List<EvaluationCommentsBO> GetEvaluationCommentsHistoryByClass(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID)
        {
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            //List<long> pupilIDList = pupilOfClassList.Select(p => Convert.ToInt64(p.PupilID)).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationCommentsHistory> listEvaluationComments = new List<EvaluationCommentsHistory>();
            if (TypeOfTeacher == 1)//GVBM
            {
                listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearID
                                           && ev.ClassID == classID
                                           && ev.SemesterID == semesterID
                                           && (ev.EvaluationCriteriaID == subjectID) // combobox tieu chi danh gia 
                                           && ev.TypeOfTeacher == TypeOfTeacher
                                           && ev.EvaluationID == evaluationID //tab mat danh gia                                                                
                                          select ev).ToList();
            }
            else //GVCN
            {
                listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearID
                                           && ev.ClassID == classID
                                           && ev.SemesterID == semesterID
                                           && ev.TypeOfTeacher == TypeOfTeacher
                                           && ev.EvaluationID == evaluationID //tab mat danh gia                                                                
                                          select ev).ToList();
            }
            //Danh gia nhan xet cuoi ky
            List<SummedEvaluationHistory> listSummedEvaluation = (from ev in SummedEvaluationHistoryBusiness.All
                                                                  where
                                                                  ev.LastDigitSchoolID == partitionId
                                                               && ev.AcademicYearID == academicYearID
                                                               && ev.ClassID == classID
                                                               && ev.SemesterID == semesterID
                                                               //&& (ev.EvaluationCriteriaID == subjectID) // combobox tieu chi danh gia                                                             
                                                               && ev.EvaluationID == evaluationID
                                                                  select ev).ToList();
            //List Danh gia cua hoc sinh theo hoc ky
            List<SummedEndingEvaluation> listSummedEndingEvaluation = (from sn in SummedEndingEvaluationBusiness.All
                                                                       where
                                                                       sn.LastDigitSchoolID == partitionId
                                                                       && sn.AcademicYearID == academicYearID
                                                                       && sn.ClassID == classID
                                                                       && sn.SemesterID == semesterID
                                                                       && sn.EvaluationID == evaluationID
                                                                       select sn).ToList();
            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
            EvaluationCommentsBO evaluationBO = null;
            EvaluationCommentsHistory ObjTemp;
            SummedEndingEvaluation summedEndingObj = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            SummedEvaluationHistory objSE;
            DateTime? MaxUpdateTime = null;
            if (listEvaluationComments.Count > 0 || listSummedEvaluation.Count > 0 || listSummedEndingEvaluation.Count > 0)
            {
                List<EvaluationCommentsHistory> lsttmpEC = listEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM1M6) || !string.IsNullOrEmpty(p.CommentsM2M7)
                                                    || !string.IsNullOrEmpty(p.CommentsM3M8) || !string.IsNullOrEmpty(p.CommentsM4M9) || !string.IsNullOrEmpty(p.CommentsM5M10)).ToList();
                List<SummedEvaluationHistory> lstSE = listSummedEvaluation.Where(p => !string.IsNullOrEmpty(p.EndingComments) || p.PeriodicEndingMark.HasValue || !string.IsNullOrEmpty(p.EndingEvaluation)).ToList();
                List<SummedEndingEvaluation> lstSEE = listSummedEndingEvaluation.Where(p => !string.IsNullOrEmpty(p.EndingEvaluation)).ToList();
                if (lsttmpEC.Count > 0 || lstSE.Count > 0 || lstSEE.Count > 0)
                {
                    //MaxUpdateTime = lsttmpEC.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime)).Union(lstSE.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime))).Max();
                    MaxUpdateTime = lsttmpEC.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime))
                                        .Union(lstSE.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime)))
                                        .Union(lstSEE.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime))).Max();
                }
            }
            listSummedEvaluation = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    ObjTemp = listEvaluationComments.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    evaluationBO = new EvaluationCommentsBO();
                    evaluationBO.FullName = pocObj.PupilFullName;
                    evaluationBO.Name = pocObj.Name;
                    evaluationBO.PupilID = pocObj.PupilID;
                    evaluationBO.Status = pocObj.Status;
                    evaluationBO.PupilCode = pocObj.PupilCode;
                    evaluationBO.Birthday = pocObj.Birthday;
                    evaluationBO.OrderInClass = pocObj.OrderInClass ?? 0;
                    if (ObjTemp != null)
                    {
                        evaluationBO.EvaluationCommentsID = ObjTemp.EvaluationCommentsID;
                        evaluationBO.Comment1 = ObjTemp.CommentsM1M6;
                        evaluationBO.Comment2 = ObjTemp.CommentsM2M7;
                        evaluationBO.Comment3 = ObjTemp.CommentsM3M8;
                        evaluationBO.Comment4 = ObjTemp.CommentsM4M9;
                        evaluationBO.Comment5 = ObjTemp.CommentsM5M10;
                        evaluationBO.EvaluationCriteriaID = ObjTemp.EvaluationCriteriaID;
                    }
                    else
                    {
                        evaluationBO.EvaluationCommentsID = 0;
                        evaluationBO.Comment1 = string.Empty;
                        evaluationBO.Comment2 = string.Empty;
                        evaluationBO.Comment3 = string.Empty;
                        evaluationBO.Comment4 = string.Empty;
                        evaluationBO.Comment5 = string.Empty;
                        evaluationBO.CommentEnding = string.Empty;
                        evaluationBO.PiriodicEndingMark = null;
                        evaluationBO.EvaluationEnding = string.Empty;
                        evaluationBO.RetestMark = null;
                        evaluationBO.EvaluationReTraining = string.Empty;
                        evaluationBO.EvaluationCommentsID = 0;
                    }

                    objSE = listSummedEvaluation.Where(s => s.PupilID == pupilID).FirstOrDefault();
                    if (objSE != null)
                    {
                        evaluationBO.CommentEnding = objSE.EndingComments;
                        evaluationBO.PiriodicEndingMark = objSE.PeriodicEndingMark;
                        evaluationBO.RetestMark = objSE.RetestMark;
                        evaluationBO.SummedEvaluationID = objSE.SummedEvaluationID;
                        evaluationBO.CreateTime = objSE.CreateTime;
                        evaluationBO.UpdateTime = objSE.UpdateTime;
                        if (evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                        {
                            evaluationBO.EvaluationEnding = objSE.EndingEvaluation;
                        }
                    }
                    //Neu la nang luc pham chat thi lay ket qua tu danh gia cua GVCN
                    if (evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || evaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                    {
                        summedEndingObj = listSummedEndingEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        if (summedEndingObj != null)
                        {
                            evaluationBO.SummedEndingEvaluationID = summedEndingObj.SummedEndingEvaluationID;
                            evaluationBO.EvaluationEnding = summedEndingObj.EndingEvaluation;
                            evaluationBO.EvaluationReTraining = summedEndingObj.EvaluationReTraining;
                        }
                    }
                    evaluationBO.MaxUpdateTime = MaxUpdateTime;
                    listResult.Add(evaluationBO);
                }
            }
            return listResult;
        }

        public void InsertOrUpdateHistory(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, ref List<ActionAuditDataBO> lstActionAuditData)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                #region Tao du lieu ghi log
                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                ActionAuditDataBO objAc = null;
                List<int> lstPupilID = listInsertOrUpdate.Select(p => p.PupilID).Distinct().ToList();
                bool isChange = false;

                List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                               join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where poc.AcademicYearID == academicYearID
                                               && poc.SchoolID == schoolID
                                               && poc.ClassID == classID
                                               && lstPupilID.Contains(poc.PupilID)
                                               && cp.IsActive.Value
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   PupilCode = pf.PupilCode,
                                                   ClassID = poc.ClassID,
                                                   ClassName = cp.DisplayName,
                                                   PupilFullName = pf.FullName
                                               }).ToList();
                PupilOfClassBO objPOC = new PupilOfClassBO();

                SubjectCat objSC = SubjectCatBusiness.Find(evaluationCriteriaID);
                string evaluationName = evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                        evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
                #endregion
                AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearID);
                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"ClassID",classID}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";

                int partitionNumber = UtilsBusiness.GetPartionId(schoolID);
                List<EvaluationCommentsHistory> evaluationCommentsList = EvaluationCommentsHistoryBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber
                                                                              && p.AcademicYearID == academicYearID
                                                                              && p.ClassID == classID
                                                                              && p.SemesterID == semesterID
                                                                              && p.EvaluationID == evaluationID
                                                                              && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                                                                              && p.TypeOfTeacher == typeOfTeacher).ToList();

                List<EvaluationCommentsHistory> insertList = new List<EvaluationCommentsHistory>();
                List<EvaluationCommentsHistory> updateList = new List<EvaluationCommentsHistory>();
                EvaluationCommentsHistory checkInlist = null;
                EvaluationComments evaluationCommentsObj;
                string strMonth;
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T5", "");

                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    isChange = false;
                    evaluationCommentsObj = listInsertOrUpdate[i];
                    long pupilID = evaluationCommentsObj.PupilID;
                    strMonth = string.Empty;
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();

                    objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();

                    objectIDStrtmp.Append(pupilID);
                    descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                    paramsStrtmp.Append(MonthID + ", " + schoolID + ", " + academicYearID + ", " + classID + ", " + educationLevelID + ", " + semesterID + ", " + evaluationID + ", " + evaluationCriteriaID + ", " + typeOfTeacher);
                    userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                    userDescriptionsStrtmp.Append("Cập nhật đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                    userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + MonthID + "/Học kỳ " + semesterID + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");

                    if (checkInlist == null)
                    {
                        evaluationCommentsObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                        if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                                isChange = true;
                            }
                        }
                        if (isChange)
                        {
                            evaluationCommentsObj.CreateTime = DateTime.Now;
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            insertList.Add(ConvertToHistory(evaluationCommentsObj));
                        }
                    }
                    else
                    {

                        if ((MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6) && !lockT1
                            && checkInlist.CommentsM1M6 != evaluationCommentsObj.CommentsM1M6)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM1M6))
                            {
                                if (!checkInlist.CommentsM1M6.Equals(evaluationCommentsObj.CommentsM1M6))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_1) ? "T1," : "T6,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM1M6).Append("; ");
                                    checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_1) ? "T1," : "T6,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM1M6).Append("; ");
                                checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7) && !lockT2)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM2M7))
                            {
                                if (!checkInlist.CommentsM2M7.Equals(evaluationCommentsObj.CommentsM2M7))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_2) ? "T2," : "T7,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM2M7).Append("; ");
                                    checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_2) ? "T2," : "T7,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM2M7).Append("; ");
                                checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8) && !lockT3)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM3M8))
                            {
                                if (!checkInlist.CommentsM3M8.Equals(evaluationCommentsObj.CommentsM3M8))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_3) ? "T3," : "T8,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM3M8).Append("; ");
                                    checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_3) ? "T3," : "T8,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM3M8).Append("; ");
                                checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9) && !lockT4)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM4M9))
                            {
                                if (!checkInlist.CommentsM4M9.Equals(evaluationCommentsObj.CommentsM4M9))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_4) ? "T4," : "T9,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM4M9).Append("; ");
                                    checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_4) ? "T4," : "T9,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM4M9).Append("; ");
                                checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10) && !lockT5)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM5M10))
                            {
                                if (!checkInlist.CommentsM5M10.Equals(evaluationCommentsObj.CommentsM5M10))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_5) ? "T5," : "T10,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM5M10).Append("; ");
                                    checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_5) ? "T5," : "T10,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM5M10).Append("; ");
                                checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                            }
                        }
                        if (isChange)
                        {
                            checkInlist.UpdateTime = DateTime.Now;
                            checkInlist.NoteUpdate = strMonth + "," + DateTime.Now.ToString() + "|";
                            updateList.Add(checkInlist);
                        }
                    }
                    objAc = new ActionAuditDataBO();
                    objAc.PupilID = pupilID;
                    objAc.ClassID = classID;
                    objAc.EvaluationID = evaluationID;
                    objAc.SubjectID = evaluationCriteriaID;
                    objAc.ObjID = objectIDStrtmp;
                    objAc.Parameter = paramsStrtmp;
                    objAc.OldData = oldObjectStrtmp;
                    objAc.NewData = newObjectStrtmp;
                    objAc.UserAction = userActionsStrtmp;
                    objAc.UserFunction = userFuntionsStrtmp;
                    objAc.UserDescription = userDescriptionsStrtmp;
                    objAc.Description = descriptionStrtmp;
                    objAc.isInsertLog = isChange;
                    lstActionAuditData.Add(objAc);


                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                }

                //Kiem tra nam hoc dang thuc hien da chuyen du lieu lich su
                //Neu da chuyen du lieu lich su thi thuc hien them moi, cap nhat vao bang lich su

                //IDictionary<string, object> columnMap = ColumnMappings();
                if (insertList != null && insertList.Count > 0)
                {
                    for (int i = 0; i < insertList.Count; i++)
                    {
                        this.Insert(insertList[i]);
                    }
                }
                if (updateList != null && updateList.Count > 0)
                {
                    for (int i = 0; i < updateList.Count; i++)
                    {
                        this.Update(updateList[i]);
                    }
                }
                this.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        public void BulkDeleteEvaluationCommentsHistory(List<long> IDList, IDictionary<string, object> dic, bool isRetest, ref List<ActionAuditDataBO> lstActionAuditData)
        {
            // Fix lỗi ATTT
            // if (IDList == null && IDList.Count == 0)
            if (IDList == null || !IDList.Any())
            {
                return;
            }
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int classId = Utils.GetInt(dic["ClassId"]);
            int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            int evaluationId = Utils.GetInt(dic["EvaluationId"]);
            int evaluationCriteriaID = Utils.GetInt(dic["EvaluationCriteriaID"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int monthId = Utils.GetInt(dic["MonthId"]);
            string LockMarkTitle = Utils.GetString(dic["LockMarkTitle"]);
            AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearId);
            int pupilID = 0;
            bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T1", "");
            bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T2", "");
            bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T3", "");
            bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T4", "");
            bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T5", "");
            bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "CK", "");

            #region Tao du lieu ghi log
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            ActionAuditDataBO objAc = null;

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == academicYearId
                                           && poc.SchoolID == schoolId
                                           && poc.ClassID == classId
                                           && IDList.Contains(poc.PupilID)
                                           && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();

            SubjectCat objSC = SubjectCatBusiness.Find(evaluationCriteriaID);
            string evaluationName = evaluationId == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                    evaluationId == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
            #endregion

            //Lay danh sach trong CSDL de co du lieu chinh xac can xoa.
            // Xoa du lieu thang
            bool isChange = false;
            if (monthId < GlobalConstants.MonthID_11)
            {
                List<EvaluationCommentsHistory> lstECDelete = EvaluationCommentsHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionId
                             && p.AcademicYearID == academicYearId
                             && p.ClassID == classId
                             && p.SemesterID == semesterId
                             && p.EvaluationID == evaluationId
                             && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                             && p.TypeOfTeacher == typeOfTeacher
                             && IDList.Contains(p.PupilID)).ToList();


                EvaluationCommentsHistory objEC = null;
                for (int i = 0; i < lstECDelete.Count; i++)
                {
                    isChange = false;
                    objEC = lstECDelete[i];
                    pupilID = objEC.PupilID;
                    objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    objectIDStrtmp.Append(pupilID);
                    descriptionStrtmp.Append("Xóa nhận xét sổ TD CLGD GVBM");
                    paramsStrtmp.Append(pupilID);
                    userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                    userDescriptionsStrtmp.Append("Xóa nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                    userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + monthId + "/Học kỳ " + semesterId + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");

                    if ((monthId == GlobalConstants.MonthID_1 || monthId == GlobalConstants.MonthID_6) && !lockT1 && !string.IsNullOrEmpty(objEC.CommentsM1M6))
                    {
                        oldObjectStrtmp.Append(CommentStr + objEC.CommentsM1M6).Append("; ");
                        objEC.CommentsM1M6 = "";
                        isChange = true;
                    }
                    else if ((monthId == GlobalConstants.MonthID_2 || monthId == GlobalConstants.MonthID_7) && !lockT2 && !string.IsNullOrEmpty(objEC.CommentsM2M7))
                    {
                        oldObjectStrtmp.Append(CommentStr + objEC.CommentsM2M7).Append("; ");
                        objEC.CommentsM2M7 = "";
                        isChange = true;
                    }
                    else if ((monthId == GlobalConstants.MonthID_3 || monthId == GlobalConstants.MonthID_8) && !lockT3 && !string.IsNullOrEmpty(objEC.CommentsM3M8))
                    {
                        oldObjectStrtmp.Append(CommentStr + objEC.CommentsM3M8).Append("; ");
                        objEC.CommentsM3M8 = "";
                        isChange = true;
                    }
                    else if ((monthId == GlobalConstants.MonthID_4 || monthId == GlobalConstants.MonthID_9) && !lockT4 && !string.IsNullOrEmpty(objEC.CommentsM4M9))
                    {
                        oldObjectStrtmp.Append(CommentStr + objEC.CommentsM4M9).Append("; ");
                        objEC.CommentsM4M9 = "";
                        isChange = true;
                    }
                    else if ((monthId == GlobalConstants.MonthID_5 || monthId == GlobalConstants.MonthID_10) && !lockT5 && !string.IsNullOrEmpty(objEC.CommentsM5M10))
                    {
                        oldObjectStrtmp.Append(CommentStr + objEC.CommentsM5M10).Append("; ");
                        objEC.CommentsM5M10 = "";
                        isChange = true;
                    }

                    if (isChange)
                    {
                        objAc = new ActionAuditDataBO();
                        objAc.PupilID = pupilID;
                        objAc.ClassID = classId;
                        objAc.EvaluationID = evaluationId;
                        objAc.SubjectID = evaluationCriteriaID;
                        objAc.ObjID = objectIDStrtmp;
                        objAc.Parameter = paramsStrtmp;
                        objAc.OldData = oldObjectStrtmp;
                        objAc.NewData = newObjectStrtmp;
                        objAc.UserAction = userActionsStrtmp;
                        objAc.UserFunction = userFuntionsStrtmp;
                        objAc.UserDescription = userDescriptionsStrtmp;
                        objAc.Description = descriptionStrtmp;
                        lstActionAuditData.Add(objAc);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    this.Update(objEC);
                }
                this.Save();
            }
            // Cap nhat du lieu danh gia nhat xet cuoi ky doi voi tab
            //Thuc hien xoa du lieu cuoi ky
            if (monthId == GlobalConstants.MonthID_11 && evaluationId == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
            {
                List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.All
                                                            .Where(p => p.LastDigitSchoolID == partitionId
                                                             && p.AcademicYearID == academicYearId
                                                             && p.ClassID == classId
                                                             && p.SemesterID == semesterId
                                                             && p.EvaluationID == evaluationId
                                                             && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                                                             && IDList.Contains(p.PupilID)).ToList();
                SummedEvaluationHistory objSU = null;

                if (listSummedEvaluation != null && listSummedEvaluation.Count > 0)
                {
                    for (int i = 0; i < listSummedEvaluation.Count; i++)
                    {
                        isChange = false;
                        objSU = listSummedEvaluation[i];
                        pupilID = objSU.PupilID;
                        objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        objectIDStrtmp.Append(pupilID);
                        descriptionStrtmp.Append("Xóa nhận xét sổ TD CLGD GVBM");
                        paramsStrtmp.Append(pupilID);
                        userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                        userDescriptionsStrtmp.Append("Xóa nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                        userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + monthId + "/Học kỳ " + semesterId + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");
                        if (!isRetest)
                        {
                            if (!lockCK)
                            {
                                if (!string.IsNullOrEmpty(objSU.EndingComments))
                                {
                                    oldObjectStrtmp.Append(LOG_NXCK + objSU.EndingComments).Append("; ");
                                    isChange = true;
                                }
                                if (objSU.PeriodicEndingMark.HasValue)
                                {
                                    oldObjectStrtmp.Append("KTĐK CK: " + objSU.PeriodicEndingMark).Append("; ");
                                    isChange = true;
                                }
                                if (!string.IsNullOrEmpty(objSU.EndingEvaluation))
                                {
                                    oldObjectStrtmp.Append("Đánh giá: " + objSU.EndingEvaluation).Append("; ");
                                    isChange = true;
                                }

                                objSU.PeriodicEndingMark = null;
                                objSU.EndingComments = "";
                                objSU.UpdateTime = DateTime.Now;
                                objSU.EndingEvaluation = "";
                            }
                        }
                        else
                        {
                            objSU.RetestMark = null;
                            objSU.EvaluationReTraining = null;
                        }
                        if (isChange)
                        {
                            objAc = new ActionAuditDataBO();
                            objAc.PupilID = pupilID;
                            objAc.ClassID = classId;
                            objAc.EvaluationID = evaluationId;
                            objAc.SubjectID = evaluationCriteriaID;
                            objAc.ObjID = objectIDStrtmp;
                            objAc.Parameter = paramsStrtmp;
                            objAc.OldData = oldObjectStrtmp;
                            objAc.NewData = newObjectStrtmp;
                            objAc.UserAction = userActionsStrtmp;
                            objAc.UserFunction = userFuntionsStrtmp;
                            objAc.UserDescription = userDescriptionsStrtmp;
                            objAc.Description = descriptionStrtmp;
                            lstActionAuditData.Add(objAc);
                        }
                        objectIDStrtmp = new StringBuilder();
                        descriptionStrtmp = new StringBuilder();
                        paramsStrtmp = new StringBuilder();
                        oldObjectStrtmp = new StringBuilder();
                        newObjectStrtmp = new StringBuilder();
                        userFuntionsStrtmp = new StringBuilder();
                        userActionsStrtmp = new StringBuilder();
                        userDescriptionsStrtmp = new StringBuilder();
                        SummedEvaluationHistoryBusiness.Update(objSU);
                    }
                    SummedEvaluationHistoryBusiness.Save();
                }
            }
        }

        /// <summary>
        /// Insert hoac update nhan xet cap 1
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="insertOrUpdateObj"></param>
        public void InsertOrUpdateEvaluationCommentsByPupilID(int MonthID, EvaluationComments insertOrUpdateObj, ref ActionAuditDataBO objAc)
        {

            long pupilID = 0;
            int typeOfTeacher = 0;
            int evaluationCriteriaID = 0;
            int evaluationID = 0;
            int semesterID = 0;
            int educationLevelID = 0;
            int schoolID = 0;
            int academicYearID = 0;
            int classID = 0;
            int partitionNumber = 0;
            string strMonth = string.Empty;
            if (insertOrUpdateObj != null)
            {
                pupilID = insertOrUpdateObj.PupilID;
                typeOfTeacher = insertOrUpdateObj.TypeOfTeacher;
                evaluationCriteriaID = insertOrUpdateObj.EvaluationCriteriaID;
                evaluationID = insertOrUpdateObj.EvaluationID;
                semesterID = insertOrUpdateObj.SemesterID;
                educationLevelID = insertOrUpdateObj.EducationLevelID;
                schoolID = insertOrUpdateObj.SchoolID;
                academicYearID = insertOrUpdateObj.AcademicYearID;
                classID = insertOrUpdateObj.ClassID;
                partitionNumber = schoolID % GlobalConstants.PARTITION;
            }
            AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearID);
            EvaluationCommentsHistory evaluationCommentsObj = EvaluationCommentsHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber
                             && p.AcademicYearID == academicYearID
                             && p.ClassID == classID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID
                             && p.EvaluationCriteriaID == evaluationCriteriaID
                             && p.TypeOfTeacher == typeOfTeacher
                             && p.PupilID == pupilID).FirstOrDefault();

            #region Tao du lieu ghi log
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            bool isChange = false;

            SubjectCat objSC = SubjectCatBusiness.Find(evaluationCriteriaID);
            PupilProfile objPF = PupilProfileBusiness.Find(pupilID);
            ClassProfile objCP = ClassProfileBusiness.Find(classID);
            string evaluationName = evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                    evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
            #endregion
            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
            paramsStrtmp.Append(MonthID + ", " + schoolID + ", " + academicYearID + ", " + classID + ", " + educationLevelID + ", " + semesterID + ", " + evaluationID + ", " + evaluationCriteriaID + ", " + typeOfTeacher);
            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
            userDescriptionsStrtmp.Append("Cập nhật đánh giá " + evaluationName + " HS " + objPF.FullName + ", " + "mã " + objPF.PupilCode + ", ");
            userDescriptionsStrtmp.Append("Lớp ").Append(objCP.DisplayName + "/" + objSC.DisplayName + "/" + "Tháng " + MonthID + "/Học kỳ " + semesterID + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");
            if (evaluationCommentsObj != null)
            {
                //update thong tin
                if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                {
                    if (evaluationCommentsObj.CommentsM1M6 != insertOrUpdateObj.CommentsM1M6)
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM1M6).Append("; ");
                        oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                        isChange = true;
                        evaluationCommentsObj.CommentsM1M6 = insertOrUpdateObj.CommentsM1M6;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                {
                    if (evaluationCommentsObj.CommentsM2M7 != insertOrUpdateObj.CommentsM2M7)
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM2M7).Append("; ");
                        oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                        isChange = true;
                        evaluationCommentsObj.CommentsM2M7 = insertOrUpdateObj.CommentsM2M7;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                {
                    if (evaluationCommentsObj.CommentsM3M8 != insertOrUpdateObj.CommentsM3M8)
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM3M8).Append("; ");
                        oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                        isChange = true;
                        evaluationCommentsObj.CommentsM3M8 = insertOrUpdateObj.CommentsM3M8;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                {
                    if (evaluationCommentsObj.CommentsM4M9 != insertOrUpdateObj.CommentsM4M9)
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM4M9).Append("; ");
                        oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                        isChange = true;
                        evaluationCommentsObj.CommentsM4M9 = insertOrUpdateObj.CommentsM4M9;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                {
                    if (evaluationCommentsObj.CommentsM5M10 != insertOrUpdateObj.CommentsM5M10)
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM5M10).Append("; ");
                        oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                        isChange = true;
                        evaluationCommentsObj.CommentsM5M10 = insertOrUpdateObj.CommentsM5M10;
                    }
                }
                EvaluationCommentsHistoryBusiness.Update(evaluationCommentsObj);
            }
            else
            {
                insertOrUpdateObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                {
                    if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM1M6))
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM1M6).Append("; ");
                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                        isChange = true;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                {
                    if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM2M7))
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM2M7).Append("; ");
                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                        isChange = true;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                {
                    if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM3M8))
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM3M8).Append("; ");
                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                        isChange = true;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                {
                    if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM4M9))
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM4M9).Append("; ");
                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                        isChange = true;
                    }
                }
                else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                {
                    if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM5M10))
                    {
                        newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM5M10).Append("; ");
                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                        isChange = true;
                    }
                }
                if (isChange)
                {
                    insertOrUpdateObj.UpdateTime = DateTime.Now;
                    EvaluationCommentsHistoryBusiness.Insert(ConvertToHistory(insertOrUpdateObj));
                }
            }
            if (isChange)
            {
                string tmpOld = string.Empty;
                string tmpNew = string.Empty;
                tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                oldObjectStrtmp.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                tmpNew = newObjectStrtmp.Length > 0 ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) : "";
                newObjectStrtmp.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                objAc.PupilID = pupilID;
                objAc.ClassID = classID;
                objAc.EvaluationID = evaluationID;
                objAc.SubjectID = evaluationCriteriaID;
                objAc.ObjID = objectIDStrtmp;
                objAc.Parameter = paramsStrtmp;
                objAc.OldData = oldObjectStrtmp;
                objAc.NewData = newObjectStrtmp;
                objAc.UserAction = userActionsStrtmp;
                objAc.UserFunction = userFuntionsStrtmp;
                objAc.UserDescription = userDescriptionsStrtmp;
                objAc.Description = descriptionStrtmp;
                objAc.isInsertLog = isChange;
            }
            EvaluationCommentsHistoryBusiness.Save();
        }

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="insertOrUpdateObj"></param>
        public List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationCommentsByPupilIDForHeadTeacher(int MonthID, EvaluationComments insertOrUpdateObj)
        {

            long pupilID = 0;
            int typeOfTeacher = 0;
            long evaluationCriteriaID = 0;
            long evaluationID = 0;
            int semesterID = 0;
            int educationLevelID = 0;
            int schoolID = 0;
            int academicYearID = 0;
            long classID = 0;
            int partitionNumber = 0;

            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

            if (insertOrUpdateObj != null)
            {
                pupilID = insertOrUpdateObj.PupilID;
                typeOfTeacher = insertOrUpdateObj.TypeOfTeacher;
                evaluationCriteriaID = insertOrUpdateObj.EvaluationCriteriaID;
                evaluationID = insertOrUpdateObj.EvaluationID;
                semesterID = insertOrUpdateObj.SemesterID;
                educationLevelID = insertOrUpdateObj.EducationLevelID;
                schoolID = insertOrUpdateObj.SchoolID;
                academicYearID = insertOrUpdateObj.AcademicYearID;
                classID = insertOrUpdateObj.ClassID;
                partitionNumber = UtilsBusiness.GetPartionId(schoolID);
            }
            //
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            if (objAcademicYear == null)
            {
                return lstActionAuditBO;
            }
            //Nau la du lieu da chuyen vao lich su thi thuc hien cap nhat vao bang du lieu lich su
            //bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
            //if (isMovedHistory)
            //{
            //    EvaluationCommentsHistoryBusiness.InsertOrUpdateEvaluationCommentsByPupilID(MonthID, insertOrUpdateObj);
            //    return lstActionAuditBO;
            //}

            //Lay danh sach lop cua truong
            ClassProfile cp = ClassProfileBusiness.Find(classID);

            //danh sach hoc sinh cua lop
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.All.Where
                (o => o.AcademicYearID == academicYearID && o.ClassID == classID).ToList();

            //Lay mon hoc
            List<SubjectCat> lstSuject = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                ).ToList();

            //Lay tieu chi
            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();
            EvaluationCommentsHistory evaluationCommentsObj = EvaluationCommentsHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber

                             && p.ClassID == classID
                             && p.AcademicYearID == academicYearID
                             && p.EducationLevelID == educationLevelID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID
                             && p.EvaluationCriteriaID == evaluationCriteriaID
                             && p.TypeOfTeacher == typeOfTeacher
                             && p.PupilID == pupilID).FirstOrDefault();
            if (evaluationCommentsObj != null)
            {
                //update thong tin
                if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                {
                    evaluationCommentsObj.CommentsM1M6 = insertOrUpdateObj.CommentsM1M6;
                }
                else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                {
                    evaluationCommentsObj.CommentsM2M7 = insertOrUpdateObj.CommentsM2M7;
                }
                else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                {
                    evaluationCommentsObj.CommentsM3M8 = insertOrUpdateObj.CommentsM3M8;
                }
                else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                {
                    evaluationCommentsObj.CommentsM4M9 = insertOrUpdateObj.CommentsM4M9;
                }
                else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                {
                    evaluationCommentsObj.CommentsM5M10 = insertOrUpdateObj.CommentsM5M10;
                }

                evaluationCommentsObj.UpdateTime = DateTime.Now;

                if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6)) evaluationCommentsObj.CommentsM1M6 = null;
                if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7)) evaluationCommentsObj.CommentsM2M7 = null;
                if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8)) evaluationCommentsObj.CommentsM3M8 = null;
                if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9)) evaluationCommentsObj.CommentsM4M9 = null;
                if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10)) evaluationCommentsObj.CommentsM5M10 = null;

                if (evaluationCommentsObj.CommentsM1M6 != this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM1M6")
                                    || evaluationCommentsObj.CommentsM2M7 != this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM2M7")
                                    || evaluationCommentsObj.CommentsM3M8 != this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM3M8")
                                    || evaluationCommentsObj.CommentsM4M9 != this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM4M9")
                                    || evaluationCommentsObj.CommentsM5M10 != this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM5M10"))
                {

                    EvaluationCommentsHistoryBusiness.Update(evaluationCommentsObj);

                    //Tao action audit
                    EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                    aab.AcademicYear = objAcademicYear.DisplayTitle;
                    aab.ClassName = cp.DisplayName;
                    aab.Month = MonthID;
                    aab.ObjectId = evaluationCommentsObj.PupilID;

                    string oldVal = "";
                    string newVal = "";
                    if (MonthID == 1 || MonthID == 6)
                    {
                        newVal = evaluationCommentsObj.CommentsM1M6;
                        oldVal = this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM1M6");
                    }
                    else if (MonthID == 2 || MonthID == 7)
                    {
                        newVal = evaluationCommentsObj.CommentsM2M7;
                        oldVal = this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM2M7");
                    }
                    else if (MonthID == 3 || MonthID == 8)
                    {
                        newVal = evaluationCommentsObj.CommentsM3M8;
                        oldVal = this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM3M8");
                    }
                    else if (MonthID == 4 || MonthID == 9)
                    {
                        newVal = evaluationCommentsObj.CommentsM4M9;
                        oldVal = this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM4M9");
                    }
                    else if (MonthID == 5 || MonthID == 10)
                    {
                        newVal = evaluationCommentsObj.CommentsM5M10;
                        oldVal = this.context.Entry<EvaluationCommentsHistory>(evaluationCommentsObj).OriginalValues.GetValue<string>("CommentsM5M10");
                    }
                    aab.NewObjectValue.Add(CRITERIA_NXT, newVal);
                    aab.OldObjectValue.Add(CRITERIA_NXT, oldVal);

                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == evaluationCommentsObj.PupilID).FirstOrDefault();
                    aab.PupilCode = poc.PupilProfile.PupilCode;
                    aab.PupilName = poc.PupilProfile.FullName;
                    aab.Semester = semesterID;
                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == evaluationCommentsObj.EvaluationCriteriaID).FirstOrDefault();
                    if (sc != null)
                    {
                        aab.SubjectName = sc.DisplayName;
                    }
                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == evaluationCommentsObj.EvaluationCriteriaID);
                    if (ec != null)
                    {
                        aab.EvaluationCriteriaName = ec.CriteriaName;
                    }
                    aab.EvaluationID = evaluationID;

                    lstActionAuditBO.Add(aab);

                }
            }
            else
            {
                long id = EvaluationCommentsBusiness.GetNextSeq<long>();
                insertOrUpdateObj.EvaluationCommentsID = id;
                insertOrUpdateObj.CreateTime = DateTime.Now;

                if (!String.IsNullOrEmpty(insertOrUpdateObj.CommentsM1M6)
                                   || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM2M7)
                                   || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM3M8)
                                   || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM4M9)
                                   || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM5M10))
                {
                    EvaluationCommentsHistoryBusiness.Insert(ConvertToHistory(insertOrUpdateObj));

                    //Tao action audit
                    EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                    aab.AcademicYear = objAcademicYear.DisplayTitle;
                    aab.ClassName = cp.DisplayName;
                    aab.Month = MonthID;
                    aab.ObjectId = insertOrUpdateObj.PupilID;
                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == insertOrUpdateObj.PupilID).FirstOrDefault();
                    aab.PupilCode = poc.PupilProfile.PupilCode;
                    aab.PupilName = poc.PupilProfile.FullName;
                    aab.Semester = semesterID;
                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == insertOrUpdateObj.EvaluationCriteriaID).FirstOrDefault();
                    if (sc != null)
                    {
                        aab.SubjectName = sc.DisplayName;
                    }
                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == insertOrUpdateObj.EvaluationCriteriaID);
                    if (ec != null)
                    {
                        aab.EvaluationCriteriaName = ec.CriteriaName;
                    }
                    aab.EvaluationID = evaluationID;

                    string val = "";
                    if (MonthID == 1 || MonthID == 6)
                    {
                        val = insertOrUpdateObj.CommentsM1M6;
                    }
                    else if (MonthID == 2 || MonthID == 7)
                    {
                        val = insertOrUpdateObj.CommentsM2M7;
                    }
                    else if (MonthID == 3 || MonthID == 8)
                    {
                        val = insertOrUpdateObj.CommentsM3M8;
                    }
                    else if (MonthID == 4 || MonthID == 9)
                    {
                        val = insertOrUpdateObj.CommentsM4M9;
                    }
                    else if (MonthID == 5 || MonthID == 10)
                    {
                        val = insertOrUpdateObj.CommentsM5M10;
                    }
                    aab.NewObjectValue.Add(CRITERIA_NXT, val);
                    aab.OldObjectValue.Add(CRITERIA_NXT, string.Empty);



                    lstActionAuditBO.Add(aab);
                }


            }
            EvaluationCommentsHistoryBusiness.Save();

            return lstActionAuditBO;
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("EvaluationCommentsID", "EVALUATION_COMMENTS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("TypeOfTeacher", "TYPE_OF_TEACHER");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("CommentsM1M6", "COMMENTS_M1_M6");
            columnMap.Add("CommentsM2M7", "COMMENTS_M2_M7");
            columnMap.Add("CommentsM3M8", "COMMENTS_M3_M8");
            columnMap.Add("CommentsM4M9", "COMMENTS_M4_M9");
            columnMap.Add("CommentsM5M10", "COMMENTS_M5_M10");
            columnMap.Add("CommentsEnding", "COMMENTS_ENDING");
            columnMap.Add("NoteInsert", "NOTE_INSERT");
            columnMap.Add("NoteUpdate", "NOTE_UPDATE");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");

            return columnMap;
        }

        /// <summary>
        /// Chuyen du lieu tu EvaluationComments sang EvaluationCommentsHistory
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        private EvaluationCommentsHistory ConvertToHistory(EvaluationComments objA)
        {
            if (objA == null)
            {
                return new EvaluationCommentsHistory();
            }
            EvaluationCommentsHistory objB = new EvaluationCommentsHistory
            {
                EvaluationCommentsID = objA.EvaluationCommentsID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                // CommentsEnding = objA.CommentsEnding,
                CommentsM1M6 = objA.CommentsM1M6,
                CommentsM2M7 = objA.CommentsM2M7,
                CommentsM3M8 = objA.CommentsM3M8,
                CommentsM4M9 = objA.CommentsM4M9,
                CommentsM5M10 = objA.CommentsM5M10,
                CreateTime = objA.CreateTime,
                EducationLevelID = objA.EducationLevelID,
                //  EndingEvaluation = objA.EndingEvaluation,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                //  EvaluationReTraining = objA.EvaluationReTraining,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                NoteInsert = objA.NoteInsert,
                NoteUpdate = objA.NoteUpdate,
                // PeriodicEndingMark = objA.PeriodicEndingMark,
                PupilID = objA.PupilID,
                // RetestMark = objA.RetestMark,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                TypeOfTeacher = objA.TypeOfTeacher,
                UpdateTime = objA.UpdateTime
            };
            return objB;
        }

        /// <summary>
        /// Chuyen du lieu 1 list EvaluationComments sang list EvaluationCommentsHistory
        /// </summary>
        /// <param name="lstA"></param>
        /// <returns></returns>
        private List<EvaluationCommentsHistory> ConvertListtoHistory(List<EvaluationComments> lstA)
        {
            List<EvaluationCommentsHistory> lstECHistory = lstA.ConvertAll(e => ConvertToHistory(e));
            return lstECHistory;
        }

        public List<EvaluationCommentActionAuditBO> DeleteEvaluationCommentsHistory(List<long> idList, List<long> idListSummedEnding, int schoolID, int academicYearID, int semesterID, int classID, int evaluationCriteriaID, int typeOfTeacher, int educationLevelID, int evaluationID, int monthID)
        {
            int partitionNumber = schoolID % GlobalConstants.PARTITION;
            string strCheckLockMark = this.CheckLockMark(classID, schoolID, academicYearID);
            List<EvaluationCommentsHistory> DeleteList = new List<EvaluationCommentsHistory>();

            //Lay danh sach lop cua truong
            ClassProfile cp = ClassProfileBusiness.Find(classID);

            //danh sach hoc sinh cua lop
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.All.Where
                (o => o.AcademicYearID == academicYearID && o.ClassID == classID).ToList();

            //Lay mon hoc
            List<SubjectCat> lstSuject = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                ).ToList();
            //Lay tieu chi
            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

            if (idList != null && idList.Count > 0)
            {
                if (monthID != GlobalConstants.MonthID_11)
                {
                    List<EvaluationCommentsHistory> evaluationCommentsList = EvaluationCommentsHistoryBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionNumber
                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                                 && p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 //&& p.EvaluationCriteriaID == evaluationCriteriaID
                                 && p.TypeOfTeacher == typeOfTeacher).ToList();
                    for (int i = 0; i < idList.Count; i++)
                    {
                        // kiem tra ID lay vao co dung cua lop duoc chon khong
                        long evaluationCommentsID = idList[i];
                        EvaluationCommentsHistory evaluationCommentsObj = evaluationCommentsList.Where(p => p.EvaluationCommentsID == evaluationCommentsID).FirstOrDefault();
                        if (evaluationCommentsObj != null)
                        {
                            if ((!strCheckLockMark.Contains("HK1") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) || (!strCheckLockMark.Contains("HK2") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            {
                                if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                {
                                    if (!strCheckLockMark.Contains("T1") || !strCheckLockMark.Contains("T6"))
                                    {
                                        evaluationCommentsObj.CommentsM1M6 = null;
                                    }
                                }
                                else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                {
                                    if (!strCheckLockMark.Contains("T2") || !strCheckLockMark.Contains("T7"))
                                    {
                                        evaluationCommentsObj.CommentsM2M7 = null;
                                    }
                                }
                                else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                {
                                    if (!strCheckLockMark.Contains("T3") || !strCheckLockMark.Contains("T8"))
                                    {
                                        evaluationCommentsObj.CommentsM3M8 = null;
                                    }
                                }
                                else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                {
                                    if (!strCheckLockMark.Contains("T4") || !strCheckLockMark.Contains("T9"))
                                    {
                                        evaluationCommentsObj.CommentsM4M9 = null;
                                    }
                                }
                                else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                {
                                    if (!strCheckLockMark.Contains("T5") || !strCheckLockMark.Contains("T10"))
                                    {
                                        evaluationCommentsObj.CommentsM5M10 = null;
                                    }
                                }
                            }
                            evaluationCommentsObj.UpdateTime = DateTime.Now;
                            DeleteList.Add(evaluationCommentsObj);
                        }
                    }

                    if (DeleteList != null && DeleteList.Count > 0)
                    {
                        for (int j = 0; j < DeleteList.Count; j++)
                        {
                            EvaluationCommentsHistory objDelete = DeleteList[j];
                            EvaluationCommentsHistoryBusiness.Update(objDelete);

                            //tao action audit
                            if (objDelete.CommentsM1M6 != this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM1M6")
                                    || objDelete.CommentsM2M7 != this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM2M7")
                                    || objDelete.CommentsM3M8 != this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM3M8")
                                    || objDelete.CommentsM4M9 != this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM4M9")
                                    || objDelete.CommentsM5M10 != this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM5M10"))
                            {
                                EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_DELETE);
                                aab.AcademicYear = aca.DisplayTitle;
                                aab.ClassName = cp.DisplayName;
                                aab.Month = monthID;
                                aab.ObjectId = objDelete.PupilID;

                                string oldVal = "";
                                if (monthID == 1 || monthID == 6)
                                {
                                    oldVal = this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM1M6");
                                }
                                else if (monthID == 2 || monthID == 7)
                                {
                                    oldVal = this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM2M7");
                                }
                                else if (monthID == 3 || monthID == 8)
                                {
                                    oldVal = this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM3M8");
                                }
                                else if (monthID == 4 || monthID == 9)
                                {
                                    oldVal = this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM4M9");
                                }
                                else if (monthID == 5 || monthID == 10)
                                {
                                    oldVal = this.context.Entry<EvaluationCommentsHistory>(objDelete).OriginalValues.GetValue<string>("CommentsM5M10");
                                }
                                aab.NewObjectValue.Add(CRITERIA_NXT, String.Empty);
                                aab.OldObjectValue.Add(CRITERIA_NXT, oldVal);

                                PupilOfClass poc = lstPoc.Where(o => o.PupilID == objDelete.PupilID).FirstOrDefault();
                                aab.PupilCode = poc.PupilProfile.PupilCode;
                                aab.PupilName = poc.PupilProfile.FullName;
                                aab.Semester = semesterID;
                                SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == objDelete.EvaluationCriteriaID).FirstOrDefault();
                                if (sc != null)
                                {
                                    aab.SubjectName = sc.DisplayName;
                                }
                                EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == objDelete.EvaluationCriteriaID);
                                if (ec != null)
                                {
                                    aab.EvaluationCriteriaName = ec.CriteriaName;
                                }
                                aab.EvaluationID = evaluationID;

                                lstActionAuditBO.Add(aab);
                            }
                        }
                        EvaluationCommentsHistoryBusiness.Save();
                    }
                }
                else
                {
                    List<long> summedEvaluationIDList = new List<long>();
                    List<SummedEvaluationHistory> summedEvaluationList = SummedEvaluationHistoryBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionNumber

                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                                 && p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && p.EvaluationCriteriaID == evaluationCriteriaID).ToList();
                    for (int i = 0; i < idList.Count; i++)
                    {
                        // kiem tra ID lay vao co dung cua lop duoc chon khong
                        long summedEvaluationID = idList[i];
                        SummedEvaluationHistory evaluationCommentsObj = summedEvaluationList.Where(p => p.SummedEvaluationID == summedEvaluationID).FirstOrDefault();
                        if (evaluationCommentsObj != null)
                        {
                            if ((!strCheckLockMark.Contains("HK1") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) || (!strCheckLockMark.Contains("HK2") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            {
                                if ((!strCheckLockMark.Contains("CK1") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) || (!strCheckLockMark.Contains("CK2") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                                {
                                    summedEvaluationIDList.Add(evaluationCommentsObj.SummedEvaluationID);
                                }
                            }
                        }
                    }

                    if (summedEvaluationIDList != null && summedEvaluationIDList.Count > 0)
                    {
                        for (int j = 0; j < summedEvaluationIDList.Count; j++)
                        {
                            SummedEvaluationHistoryBusiness.Delete(summedEvaluationIDList[j]);
                            SummedEvaluationHistory se = summedEvaluationList.FirstOrDefault(o => o.SummedEvaluationID == summedEvaluationIDList[j]);

                            if (!String.IsNullOrEmpty(se.EndingEvaluation)
                                   || !String.IsNullOrEmpty(se.EndingComments)
                                   || se.PeriodicEndingMark.HasValue)
                            {
                                //them action audit
                                //Kiem tra da co action audit hay chua
                                EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == se.PupilID).FirstOrDefault();
                                if (aab == null)
                                {
                                    aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_DELETE);
                                    lstActionAuditBO.Add(aab);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = monthID;
                                    aab.ObjectId = se.PupilID;
                                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == se.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == se.EvaluationCriteriaID).FirstOrDefault();
                                    if (sc != null)
                                    {
                                        aab.SubjectName = sc.DisplayName;
                                    }
                                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == se.EvaluationCriteriaID);
                                    if (ec != null)
                                    {
                                        aab.EvaluationCriteriaName = ec.CriteriaName;
                                    }
                                    aab.EvaluationID = evaluationID;

                                }
                                if (se.PeriodicEndingMark != null)
                                {
                                    aab.OldObjectValue.Add(CRITERIA_KTDKCK, se.PeriodicEndingMark.Value.ToString());
                                    aab.NewObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                }

                                if (!String.IsNullOrEmpty(se.EndingEvaluation))
                                {
                                    aab.OldObjectValue.Add(CRITERIA_DG, se.EndingEvaluation);
                                    aab.NewObjectValue.Add(CRITERIA_DG, string.Empty);
                                }

                                if (!string.IsNullOrEmpty(se.EndingComments))
                                {
                                    aab.OldObjectValue.Add(CRITERIA_NXCK, se.EndingComments);
                                    aab.NewObjectValue.Add(CRITERIA_NXCK, string.Empty);
                                }
                            }
                        }
                        SummedEvaluationHistoryBusiness.Save();
                    }
                }
            }

            if (idListSummedEnding != null && idListSummedEnding.Count > 0)
            {
                List<long> summedEndingList = new List<long>();
                List<SummedEndingEvaluation> summedEndingEvaluationList = SummedEndingEvaluationBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber

                             && p.ClassID == classID
                             && p.AcademicYearID == academicYearID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID).ToList();
                for (int i = 0; i < idListSummedEnding.Count; i++)
                {
                    long summedEndingID = idListSummedEnding[i];
                    SummedEndingEvaluation summedEndingOBj = summedEndingEvaluationList.Where(p => p.SummedEndingEvaluationID == summedEndingID).FirstOrDefault();
                    if (summedEndingOBj != null)
                    {
                        if ((!strCheckLockMark.Contains("HK1") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) || (!strCheckLockMark.Contains("HK2") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                        {
                            if ((!strCheckLockMark.Contains("CK1") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) || (!strCheckLockMark.Contains("CK2") && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            {
                                summedEndingList.Add(summedEndingOBj.SummedEndingEvaluationID);
                            }
                        }
                    }
                }

                if (summedEndingList != null && summedEndingList.Count > 0)
                {
                    for (int j = 0; j < summedEndingList.Count; j++)
                    {
                        SummedEndingEvaluationBusiness.Delete(summedEndingList[j]);
                        SummedEndingEvaluation see = summedEndingEvaluationList.FirstOrDefault(o => o.SummedEndingEvaluationID == summedEndingList[j]);

                        if (!string.IsNullOrEmpty(see.EndingEvaluation))
                        {
                            //them action audit
                            //Kiem tra da co action audit hay chua
                            EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == see.PupilID).FirstOrDefault();
                            if (aab == null)
                            {
                                aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_DELETE);
                                lstActionAuditBO.Add(aab);
                                aab.AcademicYear = aca.DisplayTitle;
                                aab.ClassName = cp.DisplayName;
                                aab.Month = monthID;
                                aab.ObjectId = see.PupilID;
                                PupilOfClass poc = lstPoc.Where(o => o.PupilID == see.PupilID).FirstOrDefault();
                                aab.PupilCode = poc.PupilProfile.PupilCode;
                                aab.PupilName = poc.PupilProfile.FullName;
                                aab.Semester = semesterID;
                                aab.SubjectName = String.Empty;
                                aab.EvaluationCriteriaName = String.Empty;
                                aab.EvaluationID = evaluationID;

                            }
                            if (!String.IsNullOrEmpty(see.EndingEvaluation))
                            {
                                aab.OldObjectValue.Add(CRITERIA_DG, see.EndingEvaluation);
                                aab.NewObjectValue.Add(CRITERIA_DG, string.Empty);
                            }
                        }
                    }
                }
                SummedEndingEvaluationBusiness.Save();
            }

            return lstActionAuditBO;
        }

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID">Id cac tieu chi (tab dang chon)</param>
        /// <param name="evaluationCriteriaID">ID các mặt đánh giá (nếu là tab 1 thì là ID môn học) </param>
        /// <param name="typeOfTeacher">1:GVBM, 2: GVCN</param>
        /// <param name="listInsertOrUpdate"></param>

        public List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationCommentsHistory(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationCommentsHistory> listInsertOrUpdate, List<SummedEvaluationHistory> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding)
        {
            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int partitionNumber = schoolID % GlobalConstants.PARTITION;
                using (TransactionScope scope = new TransactionScope())
                {

                    //Lay danh sach lop cua truong
                    ClassProfile cp = ClassProfileBusiness.Find(classID);

                    //danh sach hoc sinh cua lop
                    List<PupilOfClass> lstPoc = PupilOfClassBusiness.All.Where
                        (o => o.AcademicYearID == academicYearID && o.ClassID == classID).ToList();

                    //Lay nam hoc
                    AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

                    //Lay mon hoc
                    List<SubjectCat> lstSuject = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                        ).ToList();
                    //Lay tieu chi
                    List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();

                    #region Them nhan xet cho thang
                    if (listInsertOrUpdate != null && listInsertOrUpdate.Count > 0)
                    {
                        List<EvaluationCommentsHistory> evaluationCommentsList = EvaluationCommentsHistoryBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionNumber

                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                                 //&& p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && p.EvaluationCriteriaID == evaluationCriteriaID
                                 && p.TypeOfTeacher == typeOfTeacher).ToList();

                        List<EvaluationCommentsHistory> InsertList = new List<EvaluationCommentsHistory>();
                        List<EvaluationCommentsHistory> UpdateList = new List<EvaluationCommentsHistory>();
                        EvaluationCommentsHistory checkInlist = null;


                        for (int i = 0; i < listInsertOrUpdate.Count; i++)
                        {
                            EvaluationCommentsHistory evaluationCommentsObj = listInsertOrUpdate[i];
                            long pupilID = evaluationCommentsObj.PupilID;
                            string strMonth = string.Empty;
                            //kiem tra pupilID da ton tai trong list da select len chua
                            checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checkInlist == null)
                            {
                                // insert thong tin moi
                                InsertList.Add(evaluationCommentsObj);
                            }
                            else
                            {
                                //update thong tin
                                if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                                {
                                    if (MonthID == GlobalConstants.MonthID_1)
                                    {
                                        strMonth += "T1,";
                                    }
                                    else
                                    {
                                        strMonth += "T6,";
                                    }
                                    checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                }
                                else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                                {
                                    if (MonthID == GlobalConstants.MonthID_2)
                                    {
                                        strMonth += "T2,";
                                    }
                                    else
                                    {
                                        strMonth += "T7,";
                                    }
                                    checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                }
                                else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                                {
                                    if (MonthID == GlobalConstants.MonthID_3)
                                    {
                                        strMonth += "T3,";
                                    }
                                    else
                                    {
                                        strMonth += "T8,";
                                    }
                                    checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                }
                                else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                                {
                                    if (MonthID == GlobalConstants.MonthID_4)
                                    {
                                        strMonth += "T4,";
                                    }
                                    else
                                    {
                                        strMonth += "T9,";
                                    }
                                    checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                }
                                else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                                {
                                    if (MonthID == GlobalConstants.MonthID_5)
                                    {
                                        strMonth += "T5,";
                                    }
                                    else
                                    {
                                        strMonth += "T10,";
                                    }
                                    checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                }
                                checkInlist.UpdateTime = DateTime.Now;
                                UpdateList.Add(checkInlist);
                            }
                        }



                        if (InsertList != null && InsertList.Count > 0)
                        {
                            foreach (EvaluationCommentsHistory insertObj in InsertList)
                            {
                                insertObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();

                                if (!String.IsNullOrEmpty(insertObj.CommentsM1M6)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM2M7)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM3M8)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM4M9)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM5M10))
                                {
                                    EvaluationCommentsHistoryBusiness.Insert(insertObj);


                                    //Tao action audit
                                    EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = MonthID;
                                    aab.ObjectId = insertObj.PupilID;
                                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == insertObj.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == insertObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (sc != null)
                                    {
                                        aab.SubjectName = sc.DisplayName;
                                    }
                                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == insertObj.EvaluationCriteriaID);
                                    if (ec != null)
                                    {
                                        aab.EvaluationCriteriaName = ec.CriteriaName;
                                    }
                                    aab.EvaluationID = evaluationID;

                                    string val = "";
                                    if (MonthID == 1 || MonthID == 6)
                                    {
                                        val = insertObj.CommentsM1M6;
                                    }
                                    else if (MonthID == 2 || MonthID == 7)
                                    {
                                        val = insertObj.CommentsM2M7;
                                    }
                                    else if (MonthID == 3 || MonthID == 8)
                                    {
                                        val = insertObj.CommentsM3M8;
                                    }
                                    else if (MonthID == 4 || MonthID == 9)
                                    {
                                        val = insertObj.CommentsM4M9;
                                    }
                                    else if (MonthID == 5 || MonthID == 10)
                                    {
                                        val = insertObj.CommentsM5M10;
                                    }
                                    aab.NewObjectValue.Add(CRITERIA_NXT, val);
                                    aab.OldObjectValue.Add(CRITERIA_NXT, string.Empty);



                                    lstActionAuditBO.Add(aab);
                                }
                            }
                        }

                        if (UpdateList != null && UpdateList.Count > 0)
                        {
                            foreach (EvaluationCommentsHistory updateObj in UpdateList)
                            {
                                if (String.IsNullOrEmpty(updateObj.CommentsM1M6)) updateObj.CommentsM1M6 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM2M7)) updateObj.CommentsM2M7 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM3M8)) updateObj.CommentsM3M8 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM4M9)) updateObj.CommentsM4M9 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM5M10)) updateObj.CommentsM5M10 = null;

                                if (updateObj.CommentsM1M6 != this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM1M6")
                                    || updateObj.CommentsM2M7 != this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM2M7")
                                    || updateObj.CommentsM3M8 != this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM3M8")
                                    || updateObj.CommentsM4M9 != this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM4M9")
                                    || updateObj.CommentsM5M10 != this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM5M10"))
                                {
                                    EvaluationCommentsHistoryBusiness.Update(updateObj);

                                    //Tao action audit
                                    EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = MonthID;
                                    aab.ObjectId = updateObj.PupilID;

                                    string oldVal = "";
                                    string newVal = "";
                                    if (MonthID == 1 || MonthID == 6)
                                    {
                                        newVal = updateObj.CommentsM1M6;
                                        oldVal = this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM1M6");
                                    }
                                    else if (MonthID == 2 || MonthID == 7)
                                    {
                                        newVal = updateObj.CommentsM2M7;
                                        oldVal = this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM2M7");
                                    }
                                    else if (MonthID == 3 || MonthID == 8)
                                    {
                                        newVal = updateObj.CommentsM3M8;
                                        oldVal = this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM3M8");
                                    }
                                    else if (MonthID == 4 || MonthID == 9)
                                    {
                                        newVal = updateObj.CommentsM4M9;
                                        oldVal = this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM4M9");
                                    }
                                    else if (MonthID == 5 || MonthID == 10)
                                    {
                                        newVal = updateObj.CommentsM5M10;
                                        oldVal = this.context.Entry<EvaluationCommentsHistory>(updateObj).OriginalValues.GetValue<string>("CommentsM5M10");
                                    }
                                    aab.NewObjectValue.Add(CRITERIA_NXT, newVal);
                                    aab.OldObjectValue.Add(CRITERIA_NXT, oldVal);

                                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == updateObj.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == updateObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (sc != null)
                                    {
                                        aab.SubjectName = sc.DisplayName;
                                    }
                                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == updateObj.EvaluationCriteriaID);
                                    if (ec != null)
                                    {
                                        aab.EvaluationCriteriaName = ec.CriteriaName;
                                    }
                                    aab.EvaluationID = evaluationID;

                                    lstActionAuditBO.Add(aab);
                                }

                            }
                        }

                        EvaluationCommentsHistoryBusiness.Save();
                    }
                    #endregion

                    #region Them du lieu nhận xét cuối kỳ vào summedEvaluation (chỉ insert tab CLGD va nhan xet cuoi ky)
                    if (listInsertOrUpdateSummed != null && listInsertOrUpdateSummed.Count > 0)
                    {
                        List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                                 && p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && p.EvaluationCriteriaID == evaluationCriteriaID).ToList();
                        List<SummedEvaluationHistory> InsertListSummed = new List<SummedEvaluationHistory>();
                        List<SummedEvaluationHistory> UpdateListSummed = new List<SummedEvaluationHistory>();
                        for (int j = 0; j < listInsertOrUpdateSummed.Count; j++)
                        {
                            SummedEvaluationHistory summedEvaluationObj = listInsertOrUpdateSummed[j];
                            int pupilID = summedEvaluationObj.PupilID;
                            SummedEvaluationHistory checklist = null;

                            //kiem tra hoc sinh da co trong table summedEvaluation chua
                            checklist = listSummedEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checklist != null)
                            {
                                checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                checklist.EndingComments = summedEvaluationObj.EndingComments;
                                checklist.UpdateTime = DateTime.Now;
                                UpdateListSummed.Add(checklist);
                            }
                            else
                            {
                                //insert
                                summedEvaluationObj.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                summedEvaluationObj.CreateTime = DateTime.Now;
                                InsertListSummed.Add(summedEvaluationObj);
                            }
                        }
                        if (InsertListSummed != null && InsertListSummed.Count > 0)
                        {
                            for (int i = 0; i < InsertListSummed.Count; i++)
                            {
                                SummedEvaluationHistory se = InsertListSummed[i];

                                if (!String.IsNullOrEmpty(se.EndingEvaluation)
                                    || !String.IsNullOrEmpty(se.EndingComments)
                                    || se.PeriodicEndingMark.HasValue)
                                {
                                    SummedEvaluationHistoryBusiness.Insert(se);

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == se.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = se.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == se.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == se.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == se.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (se.PeriodicEndingMark != null)
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, se.PeriodicEndingMark.Value.ToString());
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(se.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, se.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(se.EndingComments))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, se.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, string.Empty);

                                    }

                                }
                            }
                        }
                        if (UpdateListSummed != null && UpdateListSummed.Count > 0)
                        {
                            for (int j = 0; j < UpdateListSummed.Count; j++)
                            {
                                SummedEvaluationHistory se = UpdateListSummed[j];

                                if (String.IsNullOrEmpty(se.EndingEvaluation)) se.EndingEvaluation = null;
                                if (String.IsNullOrEmpty(se.EndingComments)) se.EndingComments = null;

                                if (se.EndingEvaluation != this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<string>("EndingEvaluation")
                                    || se.PeriodicEndingMark != this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark")
                                    || se.EndingComments != this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<string>("EndingComments"))
                                {
                                    SummedEvaluationHistoryBusiness.Update(se);

                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == se.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = se.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == se.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == se.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == se.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (se.PeriodicEndingMark != this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, se.PeriodicEndingMark.HasValue ? se.PeriodicEndingMark.Value.ToString() : String.Empty);
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").HasValue
                                            ? this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").Value.ToString() : string.Empty);
                                    }

                                    if (se.EndingEvaluation
                                        != this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, se.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }

                                    if (se.EndingComments
                                        != this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<string>("EndingComments"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, se.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, this.context.Entry<SummedEvaluationHistory>(se).OriginalValues.GetValue<string>("EndingComments"));

                                    }

                                }
                            }
                        }
                        SummedEvaluationHistoryBusiness.Save();
                    }

                    #endregion

                    #region Thêm dữ liệu Đánh giá khi chọn Tab Năng lực hoặc phẩm chất

                    SummedEndingEvaluation checkInsummedEnding = null;
                    if (listInsertOrUpdateSummedEnding != null && listInsertOrUpdateSummedEnding.Count > 0)
                    {
                        List<SummedEndingEvaluation> listSummedEnding = SummedEndingEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                             && p.ClassID == classID
                             && p.AcademicYearID == academicYearID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID).ToList();
                        List<SummedEndingEvaluation> InsertListSummedEnding = new List<SummedEndingEvaluation>();
                        List<SummedEndingEvaluation> UpdateListSummedEnding = new List<SummedEndingEvaluation>();
                        for (int k = 0; k < listInsertOrUpdateSummedEnding.Count; k++)
                        {
                            SummedEndingEvaluation summedEndingObj = listInsertOrUpdateSummedEnding[k];
                            if (summedEndingObj != null)
                            {
                                long pupilID = summedEndingObj.PupilID;
                                checkInsummedEnding = listSummedEnding.Where(p => p.PupilID == pupilID).FirstOrDefault();
                                if (checkInsummedEnding != null)
                                {
                                    //update
                                    checkInsummedEnding.EndingEvaluation = summedEndingObj.EndingEvaluation;
                                    checkInsummedEnding.UpdateTime = DateTime.Now;
                                    UpdateListSummedEnding.Add(checkInsummedEnding);
                                }
                                else
                                {
                                    //insert
                                    summedEndingObj.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>();
                                    summedEndingObj.CreateTime = DateTime.Now;
                                    InsertListSummedEnding.Add(summedEndingObj);
                                }
                            }
                        }

                        if (InsertListSummedEnding != null && InsertListSummedEnding.Count > 0)
                        {
                            for (int i = 0; i < InsertListSummedEnding.Count; i++)
                            {
                                SummedEndingEvaluation see = InsertListSummedEnding[i];

                                if (!string.IsNullOrEmpty(see.EndingEvaluation))
                                {
                                    SummedEndingEvaluationBusiness.Insert(see);

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == see.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = see.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == see.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        aab.SubjectName = string.Empty;
                                        aab.EvaluationCriteriaName = string.Empty;
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (!String.IsNullOrEmpty(see.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, see.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }
                                }
                            }
                        }

                        if (UpdateListSummedEnding != null && UpdateListSummedEnding.Count > 0)
                        {
                            for (int j = 0; j < UpdateListSummedEnding.Count; j++)
                            {
                                SummedEndingEvaluation see = UpdateListSummedEnding[j];
                                if (string.IsNullOrEmpty(see.EndingEvaluation)) see.EndingEvaluation = null;

                                if (see.EndingEvaluation != this.context.Entry<SummedEndingEvaluation>(see).OriginalValues.GetValue<string>("EndingEvaluation"))
                                {
                                    SummedEndingEvaluationBusiness.Update(see);
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == see.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = see.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == see.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        aab.SubjectName = string.Empty;
                                        aab.EvaluationCriteriaName = string.Empty;
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (see.EndingEvaluation != this.context.Entry<SummedEndingEvaluation>(see).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, see.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEndingEvaluation>(see).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }
                                }

                            }
                        }
                        SummedEndingEvaluationBusiness.Save();
                    }

                    #endregion


                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("MonthID={0}, schoolID={1}, academicYearID={2}, classID={3}, educationLevelID={4}, semesterID={5}, evaluationID={6}, evaluationCriteriaID={7}, typeOfTeacher={8}", MonthID, schoolID, academicYearID, classID, educationLevelID, semesterID, evaluationID, evaluationCriteriaID, typeOfTeacher);
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateEvaluationCommentsHistory", paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
            return lstActionAuditBO;
        }

        /// <summary>
        /// Insert import (khong phan biet thang nhan xet)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID">Id cac tieu chi (tab dang chon)</param>
        /// <param name="evaluationCriteriaID">ID các mặt đánh giá (nếu là tab 1 thì là ID môn học) </param>
        /// <param name="typeOfTeacher">1:GVBM, 2: GVCN</param>
        /// <param name="listInsertOrUpdate"></param>
        public void InsertOrUpdateEvaluationCommentsImportHistory(int monthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationCommentsHistory> listInsertOrUpdate, List<SummedEvaluationHistory> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding)
        {
            int partitionNumber = schoolID % GlobalConstants.PARTITION;
            string strLockMark = EvaluationCommentsBusiness.CheckLockMark(classID, schoolID, academicYearID);
            strLockMark = strLockMark.Replace("T10", "TH10");
            using (TransactionScope scope = new TransactionScope())
            {
                #region Them nhan xet cho thang
                if (listInsertOrUpdate != null && listInsertOrUpdate.Count > 0)
                {
                    List<EvaluationCommentsHistory> evaluationCommentsList = EvaluationCommentsHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber
                             && p.AcademicYearID == academicYearID
                             && p.ClassID == classID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID
                             //&& p.EvaluationCriteriaID == evaluationCriteriaID
                             && p.TypeOfTeacher == typeOfTeacher).ToList();

                    List<EvaluationCommentsHistory> InsertList = new List<EvaluationCommentsHistory>();
                    List<EvaluationCommentsHistory> UpdateList = new List<EvaluationCommentsHistory>();
                    EvaluationCommentsHistory checkInlist = null;
                    for (int i = 0; i < listInsertOrUpdate.Count; i++)
                    {
                        EvaluationCommentsHistory evaluationCommentsObj = listInsertOrUpdate[i];
                        long pupilID = evaluationCommentsObj.PupilID;
                        string strMonth = string.Empty;
                        //kiem tra pupilID da ton tai trong list da select len chua
                        checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        if (checkInlist == null)
                        {
                            #region insert thong tin moi
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                if (!strLockMark.Contains("HK1"))
                                {
                                    if (strLockMark.Contains("T1"))//khoa thang 1
                                    {
                                        evaluationCommentsObj.CommentsM1M6 = null;
                                    }
                                    if (strLockMark.Contains("T2"))//khoa thang 2
                                    {
                                        evaluationCommentsObj.CommentsM2M7 = null;
                                    }
                                    if (strLockMark.Contains("T3"))//khoa thang 3
                                    {
                                        evaluationCommentsObj.CommentsM3M8 = null;
                                    }
                                    if (strLockMark.Contains("T4"))//khoa thang 4
                                    {
                                        evaluationCommentsObj.CommentsM4M9 = null;
                                    }
                                    if (strLockMark.Contains("T5"))//khoa thang 5
                                    {
                                        evaluationCommentsObj.CommentsM5M10 = null;
                                    }
                                    InsertList.Add(evaluationCommentsObj);
                                }
                            }
                            else
                            {
                                if (!strLockMark.Contains("HK2"))
                                {
                                    if (strLockMark.Contains("T6"))//khoa thang 1
                                    {
                                        evaluationCommentsObj.CommentsM1M6 = null;
                                    }
                                    if (strLockMark.Contains("T7"))//khoa thang 2
                                    {
                                        evaluationCommentsObj.CommentsM2M7 = null;
                                    }
                                    if (strLockMark.Contains("T8"))//khoa thang 3
                                    {
                                        evaluationCommentsObj.CommentsM3M8 = null;
                                    }
                                    if (strLockMark.Contains("T9"))//khoa thang 4
                                    {
                                        evaluationCommentsObj.CommentsM4M9 = null;
                                    }
                                    if (strLockMark.Contains("TH10"))//khoa thang 5
                                    {
                                        evaluationCommentsObj.CommentsM5M10 = null;
                                    }
                                    InsertList.Add(evaluationCommentsObj);
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region update thong tin moi
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                if (!strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1"))// k khoa thang 1
                                    {
                                        checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                    }
                                    if (!strLockMark.Contains("T2"))//k khoa thang 2
                                    {
                                        checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                    }
                                    if (!strLockMark.Contains("T3"))//k khoa thang 3
                                    {
                                        checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                    }
                                    if (!strLockMark.Contains("T4"))//k khoa thang 4
                                    {
                                        checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                    }
                                    if (!strLockMark.Contains("T5"))//k khoa thang 5
                                    {
                                        checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                    }
                                    UpdateList.Add(checkInlist);
                                }
                            }
                            else
                            {
                                if (!strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6"))// k khoa thang 1
                                    {
                                        checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                    }
                                    if (!strLockMark.Contains("T7"))//k khoa thang 2
                                    {
                                        checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                    }
                                    if (!strLockMark.Contains("T8"))//k khoa thang 3
                                    {
                                        checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                    }
                                    if (!strLockMark.Contains("T9"))//k khoa thang 4
                                    {
                                        checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                    }
                                    if (!strLockMark.Contains("TH10"))//k khoa thang 5
                                    {
                                        checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                    }
                                    UpdateList.Add(checkInlist);
                                }
                            }

                            #endregion
                        }

                    }

                    if (InsertList != null && InsertList.Count > 0)
                    {
                        foreach (EvaluationCommentsHistory insertObj in InsertList)
                        {
                            insertObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                            insertObj.CreateTime = DateTime.Now;
                            EvaluationCommentsHistoryBusiness.Insert(insertObj);
                        }
                    }

                    if (UpdateList != null && UpdateList.Count > 0)
                    {
                        foreach (EvaluationCommentsHistory updateObj in UpdateList)
                        {
                            updateObj.CreateTime = DateTime.Now;
                            EvaluationCommentsHistoryBusiness.Update(updateObj);
                        }
                    }

                    EvaluationCommentsHistoryBusiness.Save();
                }
                #endregion

                #region Them du lieu nhận xét cuối kỳ vào summedEvaluation (chỉ insert tab CLGD)
                if (evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || evaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                {
                    if (monthID == GlobalConstants.MonthID_11)
                    {
                        if (listInsertOrUpdateSummed != null && listInsertOrUpdateSummed.Count > 0)
                        {

                            List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber
                                     && p.AcademicYearID == academicYearID
                                     && p.ClassID == classID
                                     && p.SemesterID == semesterID
                                     && p.EvaluationID == evaluationID).ToList();
                            List<SummedEvaluationHistory> InsertListSummed = new List<SummedEvaluationHistory>();
                            List<SummedEvaluationHistory> UpdateListSummed = new List<SummedEvaluationHistory>();
                            for (int j = 0; j < listInsertOrUpdateSummed.Count; j++)
                            {
                                SummedEvaluationHistory summedEvaluationObj = listInsertOrUpdateSummed[j];
                                int pupilID = summedEvaluationObj.PupilID;
                                int evaluationCriteria = summedEvaluationObj.EvaluationCriteriaID;
                                SummedEvaluationHistory checklist = listSummedEvaluation.Where(p => p.PupilID == pupilID && (p.EvaluationCriteriaID != 0 && p.EvaluationCriteriaID == evaluationCriteria)).FirstOrDefault();
                                //kiem tra hoc sinh da co trong table summedEvaluation chua                                 
                                if (checklist != null)
                                {
                                    // khong chua thong tin khoa diem CK1,CK2
                                    if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                    {
                                        if (!strLockMark.Contains("HK1"))
                                        {
                                            if (!strLockMark.Contains("CK1"))
                                            {
                                                checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                                checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                                checklist.EndingComments = summedEvaluationObj.EndingComments;
                                                UpdateListSummed.Add(checklist);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!strLockMark.Contains("HK2"))
                                        {
                                            if (!strLockMark.Contains("CK2"))
                                            {
                                                checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                                checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                                checklist.EndingComments = summedEvaluationObj.EndingComments;
                                                UpdateListSummed.Add(checklist);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //insert
                                    summedEvaluationObj.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                    if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                    {
                                        if (!strLockMark.Contains("HK1"))
                                        {
                                            if (strLockMark.Contains("CK1"))
                                            {
                                                // neu chua thong tin khoa diem thi khong insert cac cot nay
                                                summedEvaluationObj.PeriodicEndingMark = null;
                                                summedEvaluationObj.EndingEvaluation = null;
                                                summedEvaluationObj.EndingComments = null;
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                            else
                                            {
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!strLockMark.Contains("HK2"))
                                        {
                                            if (strLockMark.Contains("CK2"))
                                            {
                                                // neu chua thong tin khoa diem thi khong insert cac cot nay
                                                summedEvaluationObj.PeriodicEndingMark = null;
                                                summedEvaluationObj.EndingEvaluation = null;
                                                summedEvaluationObj.EndingComments = null;
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                            else
                                            {
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                        }
                                    }

                                }
                            }


                            //insert or update in database
                            if (InsertListSummed != null && InsertListSummed.Count > 0)
                            {
                                for (int i = 0; i < InsertListSummed.Count; i++)
                                {

                                    SummedEvaluationHistoryBusiness.Insert(InsertListSummed[i]);
                                }
                            }
                            if (UpdateListSummed != null && UpdateListSummed.Count > 0)
                            {
                                for (int j = 0; j < UpdateListSummed.Count; j++)
                                {
                                    SummedEvaluationHistoryBusiness.Update(UpdateListSummed[j]);
                                }
                            }
                            SummedEvaluationHistoryBusiness.Save();
                        }
                    }
                }

                if (evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                {
                    if (listInsertOrUpdateSummed != null && listInsertOrUpdateSummed.Count > 0)
                    {
                        List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber
                                 && p.AcademicYearID == academicYearID
                                 && p.ClassID == classID
                                 && p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && p.EvaluationCriteriaID == evaluationCriteriaID).ToList();
                        List<SummedEvaluationHistory> InsertListSummed = new List<SummedEvaluationHistory>();
                        List<SummedEvaluationHistory> UpdateListSummed = new List<SummedEvaluationHistory>();
                        for (int j = 0; j < listInsertOrUpdateSummed.Count; j++)
                        {
                            SummedEvaluationHistory summedEvaluationObj = listInsertOrUpdateSummed[j];
                            int pupilID = summedEvaluationObj.PupilID;
                            SummedEvaluationHistory checklist = null;

                            //kiem tra hoc sinh da co trong table summedEvaluation chua
                            checklist = listSummedEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checklist != null)
                            {
                                // khong chua thong tin khoa diem CK1,CK2
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (!strLockMark.Contains("CK1"))
                                        {
                                            checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                            checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                            checklist.EndingComments = summedEvaluationObj.EndingComments;
                                            UpdateListSummed.Add(checklist);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (!strLockMark.Contains("CK2"))
                                        {
                                            checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                            checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                            checklist.EndingComments = summedEvaluationObj.EndingComments;
                                            UpdateListSummed.Add(checklist);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                //insert
                                summedEvaluationObj.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (strLockMark.Contains("CK1"))
                                        {
                                            // neu chua thong tin khoa diem thi khong insert cac cot nay
                                            summedEvaluationObj.PeriodicEndingMark = null;
                                            summedEvaluationObj.EndingEvaluation = null;
                                            summedEvaluationObj.EndingComments = null;
                                            InsertListSummed.Add(summedEvaluationObj);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (strLockMark.Contains("CK2"))
                                        {
                                            // neu chua thong tin khoa diem thi khong insert cac cot nay
                                            summedEvaluationObj.PeriodicEndingMark = null;
                                            summedEvaluationObj.EndingEvaluation = null;
                                            summedEvaluationObj.EndingComments = null;
                                            InsertListSummed.Add(summedEvaluationObj);
                                        }
                                    }
                                }

                            }
                        }
                        if (InsertListSummed != null && InsertListSummed.Count > 0)
                        {
                            for (int i = 0; i < InsertListSummed.Count; i++)
                            {

                                SummedEvaluationHistoryBusiness.Insert(InsertListSummed[i]);
                            }
                        }
                        if (UpdateListSummed != null && UpdateListSummed.Count > 0)
                        {
                            for (int j = 0; j < UpdateListSummed.Count; j++)
                            {
                                SummedEvaluationHistoryBusiness.Update(UpdateListSummed[j]);
                            }
                        }
                        SummedEvaluationHistoryBusiness.Save();
                    }
                }

                #endregion

                #region Thêm dữ liệu Đánh giá khi chọn Tab Năng lực hoặc phẩm chất

                SummedEndingEvaluation checkInsummedEnding = null;
                if (listInsertOrUpdateSummedEnding != null && listInsertOrUpdateSummedEnding.Count > 0)
                {
                    List<SummedEndingEvaluation> listSummedEnding = SummedEndingEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber
                         && p.AcademicYearID == academicYearID
                         && p.ClassID == classID
                         && p.SemesterID == semesterID
                         && p.EvaluationID == evaluationID).ToList();
                    List<SummedEndingEvaluation> InsertListSummedEnding = new List<SummedEndingEvaluation>();
                    List<SummedEndingEvaluation> UpdateListSummedEnding = new List<SummedEndingEvaluation>();
                    for (int k = 0; k < listInsertOrUpdateSummedEnding.Count; k++)
                    {
                        SummedEndingEvaluation summedEndingObj = listInsertOrUpdateSummedEnding[k];
                        if (summedEndingObj != null)
                        {
                            long pupilID = summedEndingObj.PupilID;
                            checkInsummedEnding = listSummedEnding.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checkInsummedEnding != null)
                            {
                                //update
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (!strLockMark.Contains("CK1"))
                                        {
                                            checkInsummedEnding.EndingEvaluation = summedEndingObj.EndingEvaluation;
                                            UpdateListSummedEnding.Add(checkInsummedEnding);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (!strLockMark.Contains("CK2"))
                                        {
                                            checkInsummedEnding.EndingEvaluation = summedEndingObj.EndingEvaluation;
                                            UpdateListSummedEnding.Add(checkInsummedEnding);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                //insert
                                summedEndingObj.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>();
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (strLockMark.Contains("CK1"))
                                        {
                                            summedEndingObj.EndingEvaluation = null;
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                        else
                                        {
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (strLockMark.Contains("CK2"))
                                        {
                                            summedEndingObj.EndingEvaluation = null;
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                        else
                                        {
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                    }
                                }

                            }
                        }
                    }

                    if (InsertListSummedEnding != null && InsertListSummedEnding.Count > 0)
                    {
                        for (int i = 0; i < InsertListSummedEnding.Count; i++)
                        {
                            SummedEndingEvaluationBusiness.Insert(InsertListSummedEnding[i]);
                        }
                    }

                    if (UpdateListSummedEnding != null && UpdateListSummedEnding.Count > 0)
                    {
                        for (int j = 0; j < UpdateListSummedEnding.Count; j++)
                        {
                            SummedEndingEvaluationBusiness.Update(UpdateListSummedEnding[j]);
                        }
                    }
                    SummedEndingEvaluationBusiness.Save();
                }

                #endregion


                scope.Complete();
            }
        }

        public void InsertOrUpdateEducationQualityImport(IDictionary<string, object> dic, List<EvaluationComments> listInsertOrUpdate)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int schoolId = Utils.GetInt(dic["SchoolId"]);
                int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                int classId = Utils.GetInt(dic["ClassId"]);
                int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
                int semesterId = Utils.GetInt(dic["SemesterId"]);
                int evaluationId = Utils.GetInt(dic["EvaluationId"]);
                int evaluationCriteriaID = Utils.GetInt(dic["EvaluationCriteriaID"]);
                int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);

                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolId},
                {"AcademicYearID",academicYearId},
                {"ClassID",classId}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T5", "");


                List<EvaluationCommentsHistory> evaluationCommentsList = EvaluationCommentsHistoryBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionId
                             && p.AcademicYearID == academicYearId
                             && p.ClassID == classId
                             && p.SemesterID == semesterId
                             && p.EvaluationID == evaluationId
                             && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                             && p.TypeOfTeacher == typeOfTeacher).ToList();
                if (listInsertOrUpdate == null || listInsertOrUpdate.Count == 0)
                {
                    return;
                }

                List<EvaluationCommentsHistory> InsertList = new List<EvaluationCommentsHistory>();
                List<EvaluationCommentsHistory> UpdateList = new List<EvaluationCommentsHistory>();
                EvaluationCommentsHistory checkInlist = null;
                EvaluationComments evaluationCommentsObj;
                long pupilID;
                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    evaluationCommentsObj = listInsertOrUpdate[i];
                    pupilID = evaluationCommentsObj.PupilID;
                    string strMonth = string.Empty;
                    //kiem tra pupilID da ton tai trong list da select len chua
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    if (checkInlist == null)
                    {
                        // insert thong tin moi
                        if (lockT1) evaluationCommentsObj.CommentsM1M6 = "";
                        if (lockT2) evaluationCommentsObj.CommentsM2M7 = "";
                        if (lockT3) evaluationCommentsObj.CommentsM3M8 = "";
                        if (lockT4) evaluationCommentsObj.CommentsM4M9 = "";
                        if (lockT5) evaluationCommentsObj.CommentsM5M10 = "";
                        evaluationCommentsObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                        InsertList.Add(ConvertToHistory(evaluationCommentsObj));
                    }
                    else
                    {
                        //update thong tin
                        if (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            strMonth += "T1,T2,T3,T4,T5";
                        }
                        else
                        {
                            strMonth += "T6,T7,T8,T9,T10";
                        }

                        if (!lockT1)
                        {
                            checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                        }
                        if (!lockT2)
                        {
                            checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                        }
                        if (!lockT3)
                        {
                            checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                        }
                        if (!lockT4)
                        {
                            checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                        }
                        if (!lockT5)
                        {
                            checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                        }
                        checkInlist.NoteUpdate = strMonth + "," + DateTime.Now.ToString() + "|";
                        UpdateList.Add(checkInlist);
                    }
                }
                IDictionary<string, object> columnMapping = ColumnMappings();
                if (InsertList != null && InsertList.Count > 0)
                {

                    //BulkInsert(InsertList, columnMapping);
                    for (int i = 0; i < InsertList.Count; i++)
                    {
                        Insert(InsertList[i]);
                    }

                }

                if (UpdateList != null && UpdateList.Count > 0)
                {
                    //List<string> columnWhere = new List<string>();
                    //columnWhere.Add("LAST_DIGIT_SCHOOL_ID");
                    //columnWhere.Add("EVALUATION_COMMENTS_ID");
                    //BulkUpdate(UpdateList, columnMapping, columnWhere);
                    for (int i = 0; i < UpdateList.Count; i++)
                    {
                        this.Update(UpdateList[i]);
                    }
                }
                this.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateEducationQualityImport", "null", ex);

            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public List<EvaluationCommentsBO> getListEvaluationCommentsHistoryBySchoolExport(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID)
        {
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classID).ToList();

            //Danh gia nhan xet cuoi ky
            List<SummedEvaluationHistory> listSummedEvaluation = (from ev in SummedEvaluationHistoryBusiness.All
                                                                  where
                                                                   ev.LastDigitSchoolID == partitionId
                                                                   && ev.AcademicYearID == academicYearID
                                                                   && ev.ClassID == classID
                                                                   && ev.SemesterID == semesterID
                                                                   && ev.EvaluationID == evaluationID //tab mat danh gia                                                           
                                                                  select ev).ToList();
            //List Danh gia cua hoc sinh theo hoc ky
            List<SummedEndingEvaluation> listSummedEndingEvaluation = (from sn in SummedEndingEvaluationBusiness.All
                                                                       where
                                                                       sn.LastDigitSchoolID == partitionId
                                                                       && sn.AcademicYearID == academicYearID
                                                                       && sn.ClassID == classID
                                                                       && sn.SemesterID == semesterID
                                                                       && sn.EvaluationID == evaluationID
                                                                       select sn).ToList();


            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
            EvaluationCommentsBO evaluationBO = null;
            SummedEndingEvaluation summedEndingObj = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            List<SummedEvaluationHistory> summedEvaluationList;
            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    summedEvaluationList = listSummedEvaluation.Where(s => s.PupilID == pupilID).ToList();
                    summedEndingObj = listSummedEndingEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();

                    if (summedEvaluationList != null && summedEvaluationList.Count > 0)
                    {
                        for (int j = 0; j < summedEvaluationList.Count; j++)
                        {
                            evaluationBO = new EvaluationCommentsBO();
                            evaluationBO.FullName = pocObj.PupilFullName;
                            evaluationBO.Name = pocObj.Name;
                            evaluationBO.PupilID = pocObj.PupilID;
                            evaluationBO.Status = pocObj.Status;
                            evaluationBO.PupilCode = pocObj.PupilCode;
                            evaluationBO.Birthday = pocObj.Birthday;
                            evaluationBO.OrderInClass = pocObj.OrderInClass ?? 0;
                            evaluationBO.CommentEnding = summedEvaluationList[j].EndingComments;
                            evaluationBO.SummedEvaluationID = summedEvaluationList[j].SummedEvaluationID;
                            if (summedEndingObj != null)
                            {
                                evaluationBO.SummedEndingEvaluationID = summedEndingObj.SummedEndingEvaluationID;
                                evaluationBO.EvaluationEnding = summedEndingObj.EndingEvaluation;
                                evaluationBO.EvaluationReTraining = summedEndingObj.EvaluationReTraining;
                            }
                            evaluationBO.EvaluationCriteriaID = summedEvaluationList[j].EvaluationCriteriaID;
                            listResult.Add(evaluationBO);
                        }
                    }
                    else
                    {
                        evaluationBO = new EvaluationCommentsBO();
                        evaluationBO.FullName = pocObj.PupilFullName;
                        evaluationBO.Name = pocObj.Name;
                        evaluationBO.PupilID = pocObj.PupilID;
                        evaluationBO.Status = pocObj.Status;
                        evaluationBO.PupilCode = pocObj.PupilCode;
                        evaluationBO.Birthday = pocObj.Birthday;
                        evaluationBO.OrderInClass = pocObj.OrderInClass ?? 0;
                        evaluationBO.CommentEnding = string.Empty;
                        evaluationBO.SummedEvaluationID = 0;
                        evaluationBO.SummedEndingEvaluationID = 0;
                        evaluationBO.EvaluationEnding = string.Empty;
                        evaluationBO.EvaluationCriteriaID = 0;
                        evaluationBO.EvaluationReTraining = string.Empty;
                        listResult.Add(evaluationBO);
                    }
                }
            }
            return listResult;
        }

        public void InsertOrUpdateHistorybyClass(int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;

                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"ClassID",classID}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";

                int partitionNumber = UtilsBusiness.GetPartionId(schoolID);
                List<EvaluationCommentsHistory> evaluationCommentsList = EvaluationCommentsHistoryBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber
                                                                              && p.AcademicYearID == academicYearID
                                                                              && p.ClassID == classID
                                                                              && p.SemesterID == semesterID
                                                                              && p.TypeOfTeacher == typeOfTeacher).ToList();

                List<EvaluationCommentsHistory> insertList = new List<EvaluationCommentsHistory>();
                List<EvaluationCommentsHistory> updateList = new List<EvaluationCommentsHistory>();
                EvaluationCommentsHistory checkInlist = null;
                EvaluationComments evaluationCommentsObj;
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T5", "");

                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    evaluationCommentsObj = listInsertOrUpdate[i];
                    //Kiem tra du lieu cua hoc sinh theo mat danh gia, mon hoc da co dl trong DB
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == evaluationCommentsObj.PupilID &&
                                                                p.EvaluationID == evaluationCommentsObj.EvaluationID &&
                                                                p.EvaluationCriteriaID == evaluationCommentsObj.EvaluationCriteriaID).FirstOrDefault();
                    if (checkInlist == null)
                    {
                        evaluationCommentsObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                        evaluationCommentsObj.CreateTime = DateTime.Now;
                        insertList.Add(ConvertToHistory(evaluationCommentsObj));
                    }
                    else
                    {
                        //update thong tin
                        if (!lockT1)
                        {
                            checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                        }
                        if (!lockT2)
                        {
                            checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                        }
                        if (!lockT3)
                        {
                            checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                        }
                        if (!lockT4)
                        {
                            checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                        }
                        if (!lockT5)
                        {
                            checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                        }
                        checkInlist.UpdateTime = DateTime.Now;
                        updateList.Add(checkInlist);
                    }
                }

                //IDictionary<string, object> columnMap = ColumnMappings();
                if (insertList != null && insertList.Count > 0)
                {
                    //BulkInsert(insertList, columnMap);
                    for (int i = 0; i < insertList.Count; i++)
                    {
                        Insert(insertList[i]);
                    }
                }
                if (updateList != null && updateList.Count > 0)
                {
                    //List<string> columnWhere = new List<string>();
                    //columnWhere.Add("LAST_DIGIT_SCHOOL_ID");
                    //columnWhere.Add("EVALUATION_COMMENTS_ID");
                    //BulkUpdate(updateList, columnMap, columnWhere);
                    for (int i = 0; i < updateList.Count; i++)
                    {
                        this.Update(updateList[i]);
                    }
                }
                this.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("schoolID={0}, academicYearID={1}, classID={2}, educationLevelID={3}, semesterID={4}, typeOfTeacher={5}",
                                              schoolID, academicYearID, classID, educationLevelID, semesterID, typeOfTeacher
                                                );
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateHistorybyClass", paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }

        public List<EvaluationCommentsReportBO> GetAllEvaluationCommentsHistorybyClass(int classId, IDictionary<string, object> dic, List<int> lstSubject)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            bool isSemester = Utils.GetBool(dic, "isSemester");
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationCommentsHistory> listEvaluationComments = new List<EvaluationCommentsHistory>();
            if (typeOfTeacher == GlobalConstants.EVALUTION_TEACHER_SUBJECT)//GVBM
            {
                listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearId
                                           && ev.ClassID == classId
                                           && ev.SemesterID == semesterId
                                           && ev.TypeOfTeacher == typeOfTeacher
                                          && lstSubject.Contains(ev.EvaluationCriteriaID)
                                          select ev).ToList();
            }
            else if (typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER)
            {
                listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearId
                                           && ev.ClassID == classId
                                           && ev.SemesterID == semesterId
                                           && ev.TypeOfTeacher == typeOfTeacher
                                          select ev).ToList();
            }

            //Danh gia nhan xet cuoi ky
            List<SummedEvaluationHistory> listSummedEvaluation = new List<SummedEvaluationHistory>();
            //Neu import cho ca hoc ky thi moi lay du lieu cuoi ky
            if (typeOfTeacher == GlobalConstants.EVALUTION_TEACHER_SUBJECT)
            {
                listSummedEvaluation = (from ev in SummedEvaluationHistoryBusiness.All
                                        where
                                         ev.LastDigitSchoolID == partitionId
                                         && ev.AcademicYearID == academicYearId
                                         && ev.ClassID == classId
                                         && ev.SemesterID == semesterId
                                        && lstSubject.Contains(ev.EvaluationCriteriaID)
                                        select ev).ToList();
            }
            else if (typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER && isSemester)
            {
                listSummedEvaluation = (from ev in SummedEvaluationHistoryBusiness.All
                                        where
                                         ev.LastDigitSchoolID == partitionId
                                         && ev.AcademicYearID == academicYearId
                                         && ev.ClassID == classId
                                         && ev.SemesterID == semesterId
                                        select ev).ToList();
            }
            //Neu la GVCN thi lay danh gia nhat xet cuoi ky cho mat danh gia Nang luc, pham chat
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = new List<SummedEndingEvaluation>();
            if (typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER && isSemester)
            {
                lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.All.Where(c => c.LastDigitSchoolID == partitionId
                                                                                     && c.AcademicYearID == academicYearId
                                                                                     && c.ClassID == classId
                                                                                     && c.SemesterID == semesterId).ToList();
            }
            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsReportBO> listResult = new List<EvaluationCommentsReportBO>();
            EvaluationCommentsReportBO evaluationBO = null;
            EvaluationCommentsHistory objEducation;
            EvaluationCommentsHistory objCapacities;
            EvaluationCommentsHistory objQualities;
            SummedEvaluationHistory objSummedEvaluationEducation = null;
            SummedEndingEvaluation objSummedEndingEvaluation = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            if (pupilOfClassList == null || pupilOfClassList.Count == 0)
            {
                return new List<EvaluationCommentsReportBO>();
            }
            int subjectId = 0;
            List<EvaluationCommentsHistory> listEvaluationEducation = new List<EvaluationCommentsHistory>();//Mon hoc va HDGD
            List<EvaluationCommentsHistory> listEvaluationCapacities = new List<EvaluationCommentsHistory>();//Nang luc
            List<EvaluationCommentsHistory> listEvaluationQualities = new List<EvaluationCommentsHistory>();//Nang luc
            List<SummedEvaluationHistory> listSummedEvaluationbySubject = new List<SummedEvaluationHistory>();
            IDictionary<int, EvaluationComments> dicEvaluation;
            IDictionary<int, SummedEvaluation> dicSummedEvaluation;
            IDictionary<int, SummedEndingEvaluation> dicSummedEndingEvaluation;
            for (int sc = 0; sc < lstSubject.Count; sc++)
            {
                subjectId = lstSubject[sc];

                listEvaluationEducation = listEvaluationComments.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
                listEvaluationCapacities = listEvaluationComments.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).ToList();
                listEvaluationQualities = listEvaluationComments.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
                listSummedEvaluationbySubject = listSummedEvaluation.Where(e => e.EvaluationCriteriaID == subjectId).ToList();
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    evaluationBO = new EvaluationCommentsReportBO()
                    {
                        Birthday = pocObj.Birthday,
                        FullName = pocObj.PupilFullName,
                        Name = pocObj.Name,
                        OrderInClass = pocObj.OrderInClass ?? 0,
                        PupilCode = pocObj.PupilCode,
                        PupilID = pocObj.PupilID,
                        Status = pocObj.Status,
                        EvaluationCriteriaID = subjectId
                    };
                    objEducation = listEvaluationEducation.FirstOrDefault(e => e.PupilID == pupilID);
                    objCapacities = listEvaluationCapacities.FirstOrDefault(e => e.PupilID == pupilID);
                    objQualities = listEvaluationQualities.FirstOrDefault(e => e.PupilID == pupilID);
                    dicEvaluation = new Dictionary<int, EvaluationComments>();
                    dicEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, ConvertFromHistory(objEducation));
                    dicEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, ConvertFromHistory(objCapacities));
                    dicEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, ConvertFromHistory(objQualities));
                    evaluationBO.DicEvaluation = dicEvaluation;
                    //Lay nhan xet cuoi ky, 
                    objSummedEvaluationEducation = listSummedEvaluationbySubject.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY);

                    dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                    dicSummedEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, ConvertSummedFromHistory(objSummedEvaluationEducation));
                    evaluationBO.DicSummedEvaluation = dicSummedEvaluation;
                    listResult.Add(evaluationBO);
                }
            }

            //Lay du lieu nang luc, pham chat cuoi ky theo tieu chi danh gia cho GVCN, EvaluationCriteriaID=-1;
            if (isSemester && typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER)
            {

                List<EvaluationCriteria> lstEC = EvaluationCriteriaBusiness.getEvaluationCriteria();
                EvaluationCriteria objEvaluationCriteria;
                SummedEvaluationHistory objSummedEvaluationCapacities = null;
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    evaluationBO = new EvaluationCommentsReportBO()
                    {
                        Birthday = pocObj.Birthday,
                        FullName = pocObj.PupilFullName,
                        Name = pocObj.Name,
                        OrderInClass = pocObj.OrderInClass ?? 0,
                        PupilCode = pocObj.PupilCode,
                        PupilID = pocObj.PupilID,
                        Status = pocObj.Status,
                        EvaluationCriteriaID = -1
                    };
                    dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                    for (int j = 0; j < lstEC.Count; j++)
                    {
                        objEvaluationCriteria = lstEC[j];
                        objSummedEvaluationCapacities = listSummedEvaluation.FirstOrDefault(e => e.PupilID == pupilID && e.EvaluationCriteriaID == objEvaluationCriteria.EvaluationCriteriaID);
                        dicSummedEvaluation.Add(objEvaluationCriteria.EvaluationCriteriaID, ConvertSummedFromHistory(objSummedEvaluationCapacities));
                    }
                    evaluationBO.CapacitesOrQualitiesSemester = dicSummedEvaluation;

                    if (lstSummedEndingEvaluation.Count > 0)
                    {
                        dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                        //nang luc
                        objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY);
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);

                        objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY);
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);
                        evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                    }
                    else
                    {
                        dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                        //nang luc
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, new SummedEndingEvaluation());
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, new SummedEndingEvaluation());
                        evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                    }
                    listResult.Add(evaluationBO);
                }
            }
            return listResult;
        }

        public List<EvaluationCommentsReportBO> GetAllEvaluationCommentsMonthbyClassofHeadTeacher(int classId, IDictionary<string, object> dic)
        {
            
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int semesterId = Utils.GetInt(dic["SemesterID"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationCommentsHistory> listEvaluationComments = new List<EvaluationCommentsHistory>();

            listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                      where
                                      ev.LastDigitSchoolID == partitionId
                                      && ev.AcademicYearID == academicYearId
                                       && ev.ClassID == classId
                                       && ev.SemesterID == semesterId
                                       && ev.TypeOfTeacher == typeOfTeacher
                                      select ev).ToList();

            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsReportBO> listResult = new List<EvaluationCommentsReportBO>();
            EvaluationCommentsReportBO evaluationBO = null;
            EvaluationCommentsHistory objEducation;
            EvaluationCommentsHistory objCapacities;
            EvaluationCommentsHistory objQualities;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            if (pupilOfClassList == null || pupilOfClassList.Count == 0)
            {
                return new List<EvaluationCommentsReportBO>();
            }
            List<EvaluationCommentsHistory> listEvaluationEducation = listEvaluationComments.Where(s => s.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
            List<EvaluationCommentsHistory> listEvaluationCapacities = listEvaluationComments.Where(s => s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).ToList();
            List<EvaluationCommentsHistory> listEvaluationQualities = listEvaluationComments.Where(s => s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
            dic.Add("EvaluationID", GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY);
            listEvaluationEducation = this.SetCommentByGVBM(listEvaluationEducation, dic);
            dic["EvaluationID"] = GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY;
            listEvaluationCapacities = this.SetCommentByGVBM(listEvaluationCapacities, dic);
            dic["EvaluationID"] = GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY;
            listEvaluationQualities = this.SetCommentByGVBM(listEvaluationQualities, dic);
            IDictionary<int, EvaluationComments> dicEvaluation;

            for (int i = 0; i < pupilOfClassList.Count; i++)
            {
                pocObj = pupilOfClassList[i];
                pupilID = pocObj.PupilID;
                evaluationBO = new EvaluationCommentsReportBO()
                {
                    Birthday = pocObj.Birthday,
                    FullName = pocObj.PupilFullName,
                    Name = pocObj.Name,
                    OrderInClass = pocObj.OrderInClass ?? 0,
                    PupilCode = pocObj.PupilCode,
                    PupilID = pocObj.PupilID,
                    Status = pocObj.Status,
                    EvaluationCriteriaID = 0
                };
                objEducation = listEvaluationEducation.FirstOrDefault(e => e.PupilID == pupilID);
                objCapacities = listEvaluationCapacities.FirstOrDefault(e => e.PupilID == pupilID);
                objQualities = listEvaluationQualities.FirstOrDefault(e => e.PupilID == pupilID);
                dicEvaluation = new Dictionary<int, EvaluationComments>();
                dicEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, ConvertFromHistory(objEducation));
                dicEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, ConvertFromHistory(objCapacities));
                dicEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, ConvertFromHistory(objQualities));
                evaluationBO.DicEvaluation = dicEvaluation;
                listResult.Add(evaluationBO);
            }


            return listResult;
        }

        public List<EvaluationCommentsReportBO> GetAllEvaluationCommentsbyClassAndHeadTeacher(int classId, IDictionary<string, object> dic, List<int> lstSubject)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int semesterId = Utils.GetInt(dic["SemesterID"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);

            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationCommentsHistory> listEvaluationComments = new List<EvaluationCommentsHistory>();
            listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                      where
                                      ev.LastDigitSchoolID == partitionId
                                      && ev.AcademicYearID == academicYearId
                                       && ev.ClassID == classId
                                       && ev.SemesterID == semesterId
                                       && ev.TypeOfTeacher == typeOfTeacher
                                      select ev).ToList();

            List<SummedEvaluationHistory> listSummedEvaluation = (from ev in SummedEvaluationHistoryBusiness.All
                                                                  where
                                                                   ev.LastDigitSchoolID == partitionId
                                                                   && ev.AcademicYearID == academicYearId
                                                                   && ev.ClassID == classId
                                                                   && ev.SemesterID == semesterId
                                                                  select ev).ToList();

            //Neu la GVCN thi lay danh gia nhat xet cuoi ky cho mat danh gia Nang luc, pham chat
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = new List<SummedEndingEvaluation>();
            lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.All.Where(c => c.LastDigitSchoolID == partitionId
                                                                                     && c.AcademicYearID == academicYearId
                                                                                     && c.ClassID == classId
                                                                                     && c.SemesterID == semesterId).ToList();

            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsReportBO> listResult = new List<EvaluationCommentsReportBO>();
            EvaluationCommentsReportBO evaluationBO = null;
            EvaluationCommentsHistory objEducation;
            EvaluationCommentsHistory objCapacities;
            EvaluationCommentsHistory objQualities;
            SummedEvaluationHistory objSummedEvaluationEducation = null;
            SummedEndingEvaluation objSummedEndingEvaluation = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            if (pupilOfClassList == null || pupilOfClassList.Count == 0)
            {
                return new List<EvaluationCommentsReportBO>();
            }
            int subjectId = 0;
            List<EvaluationCommentsHistory> listEvaluationEducation = new List<EvaluationCommentsHistory>();//Mon hoc va HDGD
            List<EvaluationCommentsHistory> listEvaluationCapacities = new List<EvaluationCommentsHistory>();//Nang luc
            List<EvaluationCommentsHistory> listEvaluationQualities = new List<EvaluationCommentsHistory>();//Nang luc
            List<SummedEvaluationHistory> listSummedEvaluationbySubject = new List<SummedEvaluationHistory>();
            IDictionary<int, EvaluationComments> dicEvaluation;
            IDictionary<int, SummedEvaluation> dicSummedEvaluation;
            IDictionary<int, SummedEndingEvaluation> dicSummedEndingEvaluation;

            listEvaluationEducation = listEvaluationComments.Where(e => e.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
            listEvaluationCapacities = listEvaluationComments.Where(e => e.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).ToList();
            listEvaluationQualities = listEvaluationComments.Where(e => e.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
            for (int sc = 0; sc < lstSubject.Count; sc++)
            {
                subjectId = lstSubject[sc];

                listSummedEvaluationbySubject = listSummedEvaluation.Where(e => e.EvaluationCriteriaID == subjectId).ToList();
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    evaluationBO = new EvaluationCommentsReportBO()
                    {
                        Birthday = pocObj.Birthday,
                        FullName = pocObj.PupilFullName,
                        Name = pocObj.Name,
                        OrderInClass = pocObj.OrderInClass ?? 0,
                        PupilCode = pocObj.PupilCode,
                        PupilID = pocObj.PupilID,
                        Status = pocObj.Status,
                        EvaluationCriteriaID = subjectId
                    };
                    objEducation = listEvaluationEducation.FirstOrDefault(e => e.PupilID == pupilID);
                    objCapacities = listEvaluationCapacities.FirstOrDefault(e => e.PupilID == pupilID);
                    objQualities = listEvaluationQualities.FirstOrDefault(e => e.PupilID == pupilID);
                    dicEvaluation = new Dictionary<int, EvaluationComments>();
                    dicEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, ConvertFromHistory(objEducation));
                    dicEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, ConvertFromHistory(objCapacities));
                    dicEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, ConvertFromHistory(objQualities));
                    evaluationBO.DicEvaluation = dicEvaluation;
                    //Lay nhan xet cuoi ky, 
                    objSummedEvaluationEducation = listSummedEvaluationbySubject.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY);

                    dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                    dicSummedEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, ConvertSummedFromHistory(objSummedEvaluationEducation));
                    evaluationBO.DicSummedEvaluation = dicSummedEvaluation;
                    listResult.Add(evaluationBO);
                }
            }

            //Dien du lieu nang luc, pham chat cuoi ky
            List<EvaluationCriteria> lstEC = EvaluationCriteriaBusiness.getEvaluationCriteria();
            EvaluationCriteria objEvaluationCriteria;
            SummedEvaluationHistory objSummedEvaluationCapacities = null;
            for (int i = 0; i < pupilOfClassList.Count; i++)
            {
                pocObj = pupilOfClassList[i];
                pupilID = pocObj.PupilID;
                evaluationBO = new EvaluationCommentsReportBO()
                {
                    Birthday = pocObj.Birthday,
                    FullName = pocObj.PupilFullName,
                    Name = pocObj.Name,
                    OrderInClass = pocObj.OrderInClass ?? 0,
                    PupilCode = pocObj.PupilCode,
                    PupilID = pocObj.PupilID,
                    Status = pocObj.Status,
                    EvaluationCriteriaID = -1
                };
                dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                for (int j = 0; j < lstEC.Count; j++)
                {
                    objEvaluationCriteria = lstEC[j];
                    objSummedEvaluationCapacities = listSummedEvaluation.FirstOrDefault(e => e.PupilID == pupilID && e.EvaluationCriteriaID == objEvaluationCriteria.EvaluationCriteriaID);
                    dicSummedEvaluation.Add(objEvaluationCriteria.EvaluationCriteriaID, ConvertSummedFromHistory(objSummedEvaluationCapacities));
                }
                evaluationBO.CapacitesOrQualitiesSemester = dicSummedEvaluation;
                if (lstSummedEndingEvaluation.Count > 0)
                {
                    dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                    //nang luc
                    objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY);
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);

                    objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY);
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);
                    evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                }
                else
                {
                    dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                    //nang luc                    
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, new SummedEndingEvaluation());
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, new SummedEndingEvaluation());
                    evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                }
                listResult.Add(evaluationBO);
            }

            return listResult;
        }


        private EvaluationComments ConvertFromHistory(EvaluationCommentsHistory objA)
        {
            if (objA == null)
            {
                return new EvaluationComments();
            }
            EvaluationComments objB = new EvaluationComments
            {
                EvaluationCommentsID = objA.EvaluationCommentsID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                CommentsM1M6 = objA.CommentsM1M6,
                CommentsM2M7 = objA.CommentsM2M7,
                CommentsM3M8 = objA.CommentsM3M8,
                CommentsM4M9 = objA.CommentsM4M9,
                CommentsM5M10 = objA.CommentsM5M10,
                CreateTime = objA.CreateTime,
                EducationLevelID = objA.EducationLevelID,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                NoteInsert = objA.NoteInsert,
                NoteUpdate = objA.NoteUpdate,
                PupilID = objA.PupilID,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                TypeOfTeacher = objA.TypeOfTeacher,
                UpdateTime = objA.UpdateTime
            };
            return objB;
        }

        /// <summary>
        /// Chuyen du lieu tu SummedEvaluation sang SummedEvaluationHistory
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        private SummedEvaluation ConvertSummedFromHistory(SummedEvaluationHistory objA)
        {
            if (objA == null)
            {
                return new SummedEvaluation();
            }
            SummedEvaluation objB = new SummedEvaluation
            {
                SummedEvaluationID = objA.SummedEvaluationID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                CreateTime = objA.CreateTime,
                EducationLevelID = objA.EducationLevelID,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                PupilID = objA.PupilID,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                UpdateTime = objA.UpdateTime,
                EndingComments = objA.EndingComments,
                EndingEvaluation = objA.EndingEvaluation,
                EvaluationReTraining = objA.EvaluationReTraining,
                PeriodicEndingMark = objA.PeriodicEndingMark,
                RetestMark = objA.RetestMark
            };
            return objB;
        }

        public IQueryable<EvaluationCommentsHistory> getListEvaluationByListSubject(IDictionary<string, object> dic)
        {
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            List<int> listSubjectId = Utils.GetIntList(dic["listSubjectId"]);
            int evaluationId = Utils.GetInt(dic["EvaluationId"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int educationLevelId = Utils.GetInt(dic["EduationLevelId"]);
            int classId = Utils.GetInt(dic["ClassId"]);

            IQueryable<EvaluationCommentsHistory> iQuery = this.All;
            if (schoolId > 0)
            {
                iQuery = iQuery.Where(p => p.SchoolID == schoolId);
            }
            if (partitionId > 0)
            {
                iQuery = iQuery.Where(p => p.LastDigitSchoolID == partitionId);
            }
            if (academicYearId > 0)
            {
                iQuery = iQuery.Where(p => p.AcademicYearID == academicYearId);
            }
            if (semesterId > 0)
            {
                iQuery = iQuery.Where(p => p.SemesterID == semesterId);
            }
            if (evaluationId > 0)
            {
                iQuery = iQuery.Where(p => p.EvaluationID == evaluationId);
            }
            if (typeOfTeacher > 0)
            {
                iQuery = iQuery.Where(p => p.TypeOfTeacher == typeOfTeacher);
            }
            if (educationLevelId > 0)
            {
                iQuery = iQuery.Where(p => p.EducationLevelID == educationLevelId);
            }
            if (classId > 0)
            {
                iQuery = iQuery.Where(p => p.ClassID == classId);
            }
            if (listSubjectId != null && listSubjectId.Count > 0)
            {
                iQuery = iQuery.Where(p => listSubjectId.Contains(p.EvaluationCriteriaID));
            }

            return iQuery;
        }

        public string CheckLockMark(int classID, int schoolID, int academicYearID)
        {
            string strLockMark = string.Empty;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", schoolID);
            dic.Add("AcademicYearID", academicYearID);
            dic.Add("ClassID", classID);
            List<LockMarkPrimary> lockmarlList = LockMarkPrimaryBusiness.SearchLockedMark(dic).ToList();

            if (lockmarlList != null && lockmarlList.Count > 0)
            {
                return lockmarlList.Select(p => p.LockPrimary).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }

        public List<string> GetListCommentHistory(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");
            int TypeOfTeacher = Utils.GetInt(dic, "TypeOfTeacherID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int Month = Utils.GetInt(dic, "MonthID");
            List<string> lstComment = new List<string>();
            int partition = UtilsBusiness.GetPartionId(SchoolID);

            IQueryable<EvaluationCommentsHistory> lstEvaluationComments = (from e in EvaluationCommentsHistoryBusiness.All.AsNoTracking()
                                                                           where e.SchoolID == SchoolID
                                                                           && e.AcademicYearID == AcademicYearID
                                                                           && e.ClassID == ClassID
                                                                           && e.EvaluationID == EvaluationID
                                                                           && e.TypeOfTeacher == TypeOfTeacher
                                                                           && (e.SemesterID == SemesterID || SemesterID == 0)
                                                                           && e.LastDigitSchoolID == partition
                                                                           select e);
            if (Month == 1 || Month == 6)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM1M6)).Select(p => p.CommentsM1M6).Distinct().ToList();
            }
            else if (Month == 2 || Month == 7)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM2M7)).Select(p => p.CommentsM2M7).Distinct().ToList();
            }
            else if (Month == 3 || Month == 8)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM3M8)).Select(p => p.CommentsM3M8).Distinct().ToList();
            }
            else if (Month == 4 || Month == 9)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM4M9)).Select(p => p.CommentsM4M9).Distinct().ToList();
            }
            else if (Month == 5 || Month == 10)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM5M10)).Select(p => p.CommentsM5M10).Distinct().ToList();
            }
            else
            {
                List<string> ListStrTmp = new List<string>();
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM1M6)).Select(p => p.CommentsM1M6).Distinct().ToList();

                ListStrTmp = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM2M7)).Select(p => p.CommentsM2M7).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM3M8)).Select(p => p.CommentsM3M8).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM4M9)).Select(p => p.CommentsM4M9).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM5M10)).Select(p => p.CommentsM5M10).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                lstComment = lstComment.Distinct().ToList();
            }

            return lstComment.OrderBy(p => p).ToList();
        }

        public void CopyDataEvaluationHitory(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect, ref List<ActionAuditDataBO> lstActionAuditData)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
                int schoolId = Utils.GetInt(objDic["SchoolId"]);
                int academicYearId = Utils.GetInt(objDic["AcademicYearId"]);
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                int classId = Utils.GetInt(objDic["ClassId"]);
                int educationLevelId = Utils.GetInt(objDic["EduationLevelId"]);
                int evaluationID = Utils.GetInt(objDic["EvaluationId"]);
                int semeseterID = Utils.GetInt(objDic["SemesterId"]);
                int typeOfTeacher = Utils.GetInt(objDic["TypeOfTeacher"]);
                //Danh sach hoc sinh dang hoc của lop
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = Utils.GetInt(objDic["ClassId"]);
                dic["Check"] = "Check";
                dic["Status"] = Utils.GetInt(objDic["Status"]);
                List<int> listSubjectID = Utils.GetIntList(objDic["listSubjectId"]);
                AcademicYear acaObj = AcademicYearBusiness.Find(academicYearId);
                #region Tao du lieu ghi log
                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                ActionAuditDataBO objAc = null;
                bool isChange = false;

                List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolId, dic)
                                               join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where poc.AcademicYearID == academicYearId
                                               && poc.SchoolID == schoolId
                                               && poc.ClassID == classId
                                               && cp.IsActive.Value
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   PupilCode = pf.PupilCode,
                                                   ClassID = poc.ClassID,
                                                   ClassName = cp.DisplayName,
                                                   PupilFullName = pf.FullName
                                               }).ToList();
                PupilOfClassBO objPOC = null;

                List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                          where sc.IsActive
                                          && listSubjectID.Contains(sc.SubjectCatID)
                                          select sc).ToList();
                SubjectCat objSC = null;

                string evaluationName = evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                        evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
                #endregion

                IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolId, dic);
                List<int> listPupilId = listPupilOfClass.Select(p => p.PupilID).ToList();

                //Danh sach tat ca hoc sinh co trong table nhan xet
                objDic["listSubjectId"] = null;
                List<EvaluationCommentsHistory> ListEvaluationComment = new List<EvaluationCommentsHistory>();
                if (iCopyLocation > 0)
                {
                    ListEvaluationComment = this.getListEvaluationByListSubject(objDic).Where(p => listPupilId.Contains(p.PupilID)).ToList();
                }
                List<EvaluationCommentsHistory> listEvaSource = new List<EvaluationCommentsHistory>();
                EvaluationCommentsHistory evaluationCommentObj = null;

                List<EvaluationOfPupilBO> listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
                EvaluationOfPupilBO evaluationTmp = null;

                int subjectID = 0;
                int pupilID = 0;
                //Lay thong tin khoa cot diem
                string strLockMark = this.CheckLockMark(classId, schoolId, academicYearId);
                strLockMark = strLockMark.Replace("T10", "TH10");

                if (iCopyObject == 1 && iCopyLocation == 1 && month != 11)
                {
                    #region TH Ghi de nhan xet cua tat ca hoc sinh,sao chep nhan xet cua thang
                    //Danh sach nhan xet cua thang voi mon hoc duoc chon
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;
                        if (month.Value == GlobalConstants.MonthID_1 || month.Value == GlobalConstants.MonthID_6)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM1M6;
                        }
                        else if (month.Value == GlobalConstants.MonthID_2 || month.Value == GlobalConstants.MonthID_7)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM2M7;
                        }
                        else if (month.Value == GlobalConstants.MonthID_3 || month.Value == GlobalConstants.MonthID_8)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM3M8;
                        }
                        else if (month.Value == GlobalConstants.MonthID_4 || month.Value == GlobalConstants.MonthID_9)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM4M9;
                        }
                        else if (month.Value == GlobalConstants.MonthID_5 || month.Value == GlobalConstants.MonthID_10)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM5M10;
                        }
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string strEvalutionpattern = string.Empty;

                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        evaluationCommentObj = null;
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationCommentsHistory> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationCommentsHistory ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh

                            pupilID = listPupilIDEva[j];
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");

                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1") && ObjEvaluationTmp.CommentsM1M6 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2") && ObjEvaluationTmp.CommentsM2M7 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3") && ObjEvaluationTmp.CommentsM3M8 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4") && ObjEvaluationTmp.CommentsM4M9 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T5") && ObjEvaluationTmp.CommentsM5M10 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6") && ObjEvaluationTmp.CommentsM1M6 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7") && ObjEvaluationTmp.CommentsM2M7 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8") && ObjEvaluationTmp.CommentsM3M8 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9") && ObjEvaluationTmp.CommentsM4M9 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10") && ObjEvaluationTmp.CommentsM5M10 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationCommentsHistory();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = pupilID;
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;

                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                }
                                if (isChange)
                                {
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                                    EvaluationCommentsHistoryBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    EvaluationCommentsHistoryBusiness.Save();

                    #endregion
                }
                else if (iCopyObject == 1 && iCopyLocation == 2)
                {
                    #region TH Ghi de nhan xet cua tat ca hoc sinh,Sao chep nhan xet tat ca cac thang cua hoc ky

                    //Danh sach nhan xet cua thang voi mon hoc duoc chon tren giao dien
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;
                        evaluationTmp.EvaluationComment16 = evaluationCommentObj.CommentsM1M6;
                        evaluationTmp.EvaluationComment27 = evaluationCommentObj.CommentsM2M7;
                        evaluationTmp.EvaluationComment38 = evaluationCommentObj.CommentsM3M8;
                        evaluationTmp.EvaluationComment49 = evaluationCommentObj.CommentsM4M9;
                        evaluationTmp.EvaluationComment510 = evaluationCommentObj.CommentsM5M10;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string CommentsM1M6, CommentsM2M7, CommentsM3M8, CommentsM4M9, CommentsM5M10;
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        evaluationCommentObj = null;
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationCommentsHistory> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationCommentsHistory ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            CommentsM1M6 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment16).FirstOrDefault();
                            CommentsM2M7 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment27).FirstOrDefault();
                            CommentsM3M8 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment38).FirstOrDefault();
                            CommentsM4M9 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment49).FirstOrDefault();
                            CommentsM5M10 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment510).FirstOrDefault();

                            pupilID = listPupilIDEva[j];
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1") && ObjEvaluationTmp.CommentsM1M6 != CommentsM1M6)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T2") && ObjEvaluationTmp.CommentsM2M7 != CommentsM2M7)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T3") && ObjEvaluationTmp.CommentsM3M8 != CommentsM3M8)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T4") && ObjEvaluationTmp.CommentsM4M9 != CommentsM4M9)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T5") && ObjEvaluationTmp.CommentsM5M10 != CommentsM5M10)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                        isChange = true;
                                    }

                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    EvaluationCommentsHistoryBusiness.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6") && ObjEvaluationTmp.CommentsM1M6 != CommentsM1M6)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                    }
                                    if (!strLockMark.Contains("T7") && ObjEvaluationTmp.CommentsM1M6 != CommentsM2M7)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                    }
                                    if (!strLockMark.Contains("T8") && ObjEvaluationTmp.CommentsM1M6 != CommentsM3M8)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                    }
                                    if (!strLockMark.Contains("T9") && ObjEvaluationTmp.CommentsM1M6 != CommentsM4M9)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                    }
                                    if (!strLockMark.Contains("TH10") && ObjEvaluationTmp.CommentsM1M6 != CommentsM5M10)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                    }

                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    EvaluationCommentsHistoryBusiness.Update(ObjEvaluationTmp);
                                }

                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationCommentsHistory();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = pupilID;
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;

                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                if (isChange)
                                {
                                    ObjEvaluationTmp.CreateTime = DateTime.Now;
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    EvaluationCommentsHistoryBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    EvaluationCommentsHistoryBusiness.Save();

                    #endregion
                    #region Sao chep tat ca cac thang cua hoc sinh thi sao chep luon KTHK doi voi GVBM tab Mon Hoc và HDGD
                    if (evaluationID == 1 && typeOfTeacher == 1)//GVBM
                    {
                        //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                        objDic.Add("lstPupilID", listPupilId);
                        List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                        //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                        List<SummedEvaluationHistory> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                        SummedEvaluationHistory sumObj = null;
                        SummedEvaluationHistory sumObjInsert = null;
                        listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
                        string strEvalutionpattern = string.Empty;
                        for (int j = 0; j < listSummEvaluationSource.Count; j++)
                        {
                            evaluationTmp = new EvaluationOfPupilBO();
                            evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                            evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                            listEvaluationOfPupil.Add(evaluationTmp);
                        }

                        for (int i = 0; i < listSubjectID.Count; i++)
                        {
                            subjectID = listSubjectID[i];
                            List<SummedEvaluationHistory> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                            for (int j = 0; j < listPupilIDEva.Count; j++)
                            {
                                strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                                sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                                objAc = lstActionAuditData.Where(p => p.PupilID == listPupilIDEva[i]).FirstOrDefault();
                                if (sumObj != null)
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        if (sumObj.EndingComments != strEvalutionpattern)
                                        {
                                            if (objAc != null)
                                            {
                                                objAc.OldData.Append(LOG_NXCK + sumObj.EndingComments).Append("; ");
                                                objAc.NewData.Append(LOG_NXCK + strEvalutionpattern).Append("; ");
                                            }
                                            sumObj.EndingComments = strEvalutionpattern;
                                        }
                                        SummedEvaluationHistoryBusiness.Update(sumObj);
                                    }
                                }
                                else
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        sumObjInsert = new SummedEvaluationHistory();
                                        sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                        sumObjInsert.AcademicYearID = academicYearId;
                                        sumObjInsert.ClassID = classId;
                                        sumObjInsert.CreateTime = DateTime.Now;
                                        sumObjInsert.EducationLevelID = educationLevelId;
                                        sumObjInsert.EndingComments = strEvalutionpattern;
                                        sumObjInsert.EndingEvaluation = string.Empty;
                                        sumObjInsert.EvaluationCriteriaID = subjectID;
                                        sumObjInsert.EvaluationID = evaluationID;
                                        sumObjInsert.EvaluationReTraining = null;
                                        sumObjInsert.LastDigitSchoolID = partitionId;
                                        sumObjInsert.PeriodicEndingMark = null;
                                        sumObjInsert.PupilID = listPupilIDEva[j];
                                        sumObjInsert.RetestMark = null;
                                        sumObjInsert.SchoolID = schoolId;
                                        sumObjInsert.SemesterID = semeseterID;
                                        if (sumObjInsert.EndingComments != strEvalutionpattern)
                                        {
                                            if (objAc != null)
                                            {
                                                objAc.NewData.Append(LOG_NXCK + strEvalutionpattern).Append("; ");
                                            }

                                            SummedEvaluationHistoryBusiness.Insert(sumObjInsert);
                                        }
                                    }
                                }
                            }
                        }
                        SummedEvaluationHistoryBusiness.Save();
                    }
                    #endregion
                }
                else if (iCopyObject == 2 && iCopyLocation == 1 && month != 11)
                {
                    #region TH Chi cap nhat hoc sinh chua duoc nhan xet, sao chep theo thang
                    //Danh sach nhan xet cua thang voi mon hoc duoc chon
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;

                        if (month.Value == GlobalConstants.MonthID_1 || month.Value == GlobalConstants.MonthID_6)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM1M6;
                        }
                        else if (month.Value == GlobalConstants.MonthID_2 || month.Value == GlobalConstants.MonthID_7)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM2M7;
                        }
                        else if (month.Value == GlobalConstants.MonthID_3 || month.Value == GlobalConstants.MonthID_8)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM3M8;
                        }
                        else if (month.Value == GlobalConstants.MonthID_4 || month.Value == GlobalConstants.MonthID_9)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM4M9;
                        }
                        else if (month.Value == GlobalConstants.MonthID_5 || month.Value == GlobalConstants.MonthID_10)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM5M10;
                        }
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string strEvalutionpattern = string.Empty;

                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        evaluationCommentObj = null;
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationCommentsHistory> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationCommentsHistory ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDEva[j];
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    EvaluationCommentsHistoryBusiness.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    EvaluationCommentsHistoryBusiness.Update(ObjEvaluationTmp);
                                }
                                newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationCommentsHistory();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = pupilID;
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T51"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }

                                }
                                if (isChange)
                                {
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                                    EvaluationCommentsHistoryBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    EvaluationCommentsHistoryBusiness.Save();

                    #endregion
                }
                else if (iCopyObject == 2 && iCopyLocation == 2 && month != 11)
                {
                    #region TH Chi cap nhat hoc sinh cua duoc nhan xet, sao chep tat ca cac thang cua hoc ky

                    //Danh sach nhan xet cua thang voi mon hoc duoc chon tren giao dien
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;
                        evaluationTmp.EvaluationComment16 = evaluationCommentObj.CommentsM1M6;
                        evaluationTmp.EvaluationComment27 = evaluationCommentObj.CommentsM2M7;
                        evaluationTmp.EvaluationComment38 = evaluationCommentObj.CommentsM3M8;
                        evaluationTmp.EvaluationComment49 = evaluationCommentObj.CommentsM4M9;
                        evaluationTmp.EvaluationComment510 = evaluationCommentObj.CommentsM5M10;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string CommentsM1M6, CommentsM2M7, CommentsM3M8, CommentsM4M9, CommentsM5M10;
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        evaluationCommentObj = null;
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationCommentsHistory> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationCommentsHistory ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            CommentsM1M6 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment16).FirstOrDefault();
                            CommentsM2M7 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment27).FirstOrDefault();
                            CommentsM3M8 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment38).FirstOrDefault();
                            CommentsM4M9 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment49).FirstOrDefault();
                            CommentsM5M10 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment510).FirstOrDefault();
                            pupilID = listPupilIDEva[j];
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6) && !strLockMark.Contains("T1"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7) && !strLockMark.Contains("T2"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8) && !strLockMark.Contains("T3"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9) && !strLockMark.Contains("T4"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10) && !strLockMark.Contains("T5"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                    }

                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    EvaluationCommentsHistoryBusiness.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6) && !strLockMark.Contains("T6"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7) && !strLockMark.Contains("T7"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8) && !strLockMark.Contains("T8"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9) && !strLockMark.Contains("T9"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                    }
                                    if (string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10) && !strLockMark.Contains("TH10"))
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                    }

                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    EvaluationCommentsHistoryBusiness.Update(ObjEvaluationTmp);
                                }
                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationCommentsHistory();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = pupilID;
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                if (isChange)
                                {
                                    ObjEvaluationTmp.CreateTime = DateTime.Now;
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    EvaluationCommentsHistoryBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    EvaluationCommentsHistoryBusiness.Save();

                    #endregion
                    #region Sao chep tat ca cac thang cua hoc sinh thi sao chep luon KTHK doi voi GVBM tab Mon Hoc và HDGD
                    if (evaluationID == 1 && typeOfTeacher == 1)//GVBM
                    {
                        //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                        objDic.Add("lstPupilID", listPupilId);
                        List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                        //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                        List<SummedEvaluationHistory> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                        SummedEvaluationHistory sumObj = null;
                        SummedEvaluationHistory sumObjInsert = null;
                        listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
                        string strEvalutionpattern = string.Empty;
                        for (int j = 0; j < listSummEvaluationSource.Count; j++)
                        {
                            evaluationTmp = new EvaluationOfPupilBO();
                            evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                            evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                            listEvaluationOfPupil.Add(evaluationTmp);
                        }

                        for (int i = 0; i < listSubjectID.Count; i++)
                        {
                            subjectID = listSubjectID[i];
                            List<SummedEvaluationHistory> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                            for (int j = 0; j < listPupilIDEva.Count; j++)
                            {
                                strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                                sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                                objAc = lstActionAuditData.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();
                                if (sumObj != null)
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        if (!string.IsNullOrEmpty(sumObj.EndingComments)) continue;
                                        if (objAc != null)
                                        {
                                            objAc.OldData.Append(LOG_NXCK).Append("; ");
                                            objAc.NewData.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                        }
                                        sumObj.EndingComments = strEvalutionpattern;
                                        SummedEvaluationHistoryBusiness.Update(sumObj);
                                    }
                                }
                                else
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        sumObjInsert = new SummedEvaluationHistory();
                                        sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                        sumObjInsert.AcademicYearID = academicYearId;
                                        sumObjInsert.ClassID = classId;
                                        sumObjInsert.CreateTime = DateTime.Now;
                                        sumObjInsert.EducationLevelID = educationLevelId;
                                        sumObjInsert.EndingComments = strEvalutionpattern;
                                        sumObjInsert.EndingEvaluation = string.Empty;
                                        sumObjInsert.EvaluationCriteriaID = subjectID;
                                        sumObjInsert.EvaluationID = evaluationID;
                                        sumObjInsert.EvaluationReTraining = null;
                                        sumObjInsert.LastDigitSchoolID = partitionId;
                                        sumObjInsert.PeriodicEndingMark = null;
                                        sumObjInsert.PupilID = pupilID;
                                        sumObjInsert.RetestMark = null;
                                        sumObjInsert.SchoolID = schoolId;
                                        sumObjInsert.SemesterID = semeseterID;
                                        if (objAc != null)
                                        {
                                            objAc.OldData.Append(LOG_NXCK).Append("; ");
                                        }
                                        SummedEvaluationHistoryBusiness.Insert(sumObjInsert);
                                    }
                                }
                            }
                        }
                        SummedEvaluationHistoryBusiness.Save();
                    }
                    #endregion
                }
                else if (iCopyObject == 1 && (iCopyLocation == 0 || month == 11))
                {
                    #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh
                    string strEvalutionpattern = string.Empty;
                    //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                    List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                    //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                    List<SummedEvaluationHistory> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                    SummedEvaluationHistory sumObj = null;
                    SummedEvaluationHistory sumObjInsert = null;
                    for (int j = 0; j < listSummEvaluationSource.Count; j++)
                    {
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                        evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //For qua danh sach mon hoc
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];
                        //List du lieu nhan xet co trong DB
                        List<SummedEvaluationHistory> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        for (int k = 0; k < listPupilIDSum.Count; k++)
                        {
                            isChange = false;
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                            sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDSum[k];
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (sumObj != null)
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    if (sumObj.EndingComments != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(LOG_NXCK).Append(sumObj.EndingComments).Append("; ");
                                        newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                        isChange = true;
                                        sumObj.EndingComments = strEvalutionpattern;
                                    }
                                    SummedEvaluationHistoryBusiness.Update(sumObj);
                                }
                            }
                            else
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    sumObjInsert = new SummedEvaluationHistory();
                                    sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                    sumObjInsert.AcademicYearID = academicYearId;
                                    sumObjInsert.ClassID = classId;
                                    sumObjInsert.CreateTime = DateTime.Now;
                                    sumObjInsert.EducationLevelID = educationLevelId;
                                    sumObjInsert.EndingComments = strEvalutionpattern;
                                    sumObjInsert.EndingEvaluation = string.Empty;
                                    sumObjInsert.EvaluationCriteriaID = subjectID;
                                    sumObjInsert.EvaluationID = evaluationID;
                                    sumObjInsert.EvaluationReTraining = null;
                                    sumObjInsert.LastDigitSchoolID = partitionId;
                                    sumObjInsert.PeriodicEndingMark = null;
                                    sumObjInsert.PupilID = listPupilIDSum[k];
                                    sumObjInsert.RetestMark = null;
                                    sumObjInsert.SchoolID = schoolId;
                                    sumObjInsert.SemesterID = semeseterID;
                                    newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                    isChange = true;
                                    SummedEvaluationHistoryBusiness.Insert(sumObjInsert);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    SummedEvaluationHistoryBusiness.Save();
                    #endregion
                }
                else if (iCopyObject == 2 && (iCopyLocation == 0 || month == 11))
                {
                    #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh chua duoc nhan xet
                    string strEvalutionpattern = string.Empty;
                    //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                    List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                    //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                    List<SummedEvaluationHistory> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                    SummedEvaluationHistory sumObj = null;
                    SummedEvaluationHistory sumObjInsert = null;
                    for (int j = 0; j < listSummEvaluationSource.Count; j++)
                    {
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                        evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //For qua danh sach mon hoc
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];
                        //List du lieu nhan xet co trong DB
                        List<SummedEvaluationHistory> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        for (int k = 0; k < listPupilIDSum.Count; k++)
                        {
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                            sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDSum[k];
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (sumObj != null)
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    if (!string.IsNullOrEmpty(sumObj.EndingComments)) continue;
                                    oldObjectStrtmp.Append(LOG_NXCK).Append("; ");
                                    newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                    sumObj.EndingComments = strEvalutionpattern;
                                    SummedEvaluationHistoryBusiness.Update(sumObj);
                                }
                            }
                            else
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    sumObjInsert = new SummedEvaluationHistory();
                                    sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                    sumObjInsert.AcademicYearID = academicYearId;
                                    sumObjInsert.ClassID = classId;
                                    sumObjInsert.CreateTime = DateTime.Now;
                                    sumObjInsert.EducationLevelID = educationLevelId;
                                    sumObjInsert.EndingComments = strEvalutionpattern;
                                    sumObjInsert.EndingEvaluation = string.Empty;
                                    sumObjInsert.EvaluationCriteriaID = subjectID;
                                    sumObjInsert.EvaluationID = evaluationID;
                                    sumObjInsert.EvaluationReTraining = null;
                                    sumObjInsert.LastDigitSchoolID = partitionId;
                                    sumObjInsert.PeriodicEndingMark = null;
                                    sumObjInsert.PupilID = listPupilIDSum[k];
                                    sumObjInsert.RetestMark = null;
                                    sumObjInsert.SchoolID = schoolId;
                                    sumObjInsert.SemesterID = semeseterID;
                                    newObjectStrtmp.Append("NXCK: ").Append(strEvalutionpattern).Append("; ");
                                    SummedEvaluationHistoryBusiness.Insert(sumObjInsert);
                                }
                            }
                            objAc = new ActionAuditDataBO();
                            objAc.PupilID = pupilID;
                            objAc.ClassID = classId;
                            objAc.EvaluationID = evaluationID;
                            objAc.SubjectID = subjectID;
                            objAc.ObjID = objectIDStrtmp;
                            objAc.Parameter = paramsStrtmp;
                            objAc.OldData = oldObjectStrtmp;
                            objAc.NewData = newObjectStrtmp;
                            objAc.UserAction = userActionsStrtmp;
                            objAc.UserFunction = userFuntionsStrtmp;
                            objAc.UserDescription = userDescriptionsStrtmp;
                            objAc.Description = descriptionStrtmp;
                            lstActionAuditData.Add(objAc);

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    SummedEvaluationHistoryBusiness.Save();
                    #endregion
                }

            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public List<EvaluationCommentActionAuditBO> CopyDataEvaluationHistoryForHeadTeacher(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect)
        {
            int academicYearId = Utils.GetInt(objDic["AcademicYearId"]);
            AcademicYear acaObj = AcademicYearBusiness.Find(academicYearId);
            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

            int schoolId = Utils.GetInt(objDic["SchoolId"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int classId = Utils.GetInt(objDic["ClassId"]);
            int educationLevelId = Utils.GetInt(objDic["EduationLevelId"]);
            int evaluationID = Utils.GetInt(objDic["EvaluationId"]);
            int semeseterID = Utils.GetInt(objDic["SemesterId"]);
            int typeOfTeacher = Utils.GetInt(objDic["TypeOfTeacher"]);

            ClassProfile classProfile = ClassProfileBusiness.Find(classId);
            //Lay mon hoc
            List<SubjectCat> lstSuject = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                ).ToList();

            //Lay tieu chi
            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();

            //Danh sach hoc sinh dang hoc của lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = Utils.GetInt(objDic["ClassId"]);
            dic["Check"] = "Check";
            dic["Status"] = Utils.GetInt(objDic["Status"]);
            List<int> listSubjectID = Utils.GetIntList(objDic["listSubjectId"]);

            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolId, dic);
            List<int> listPupilId = listPupilOfClass.Select(p => p.PupilID).ToList();

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolId, dic)
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == academicYearId
                                           && poc.SchoolID == schoolId
                                           && poc.ClassID == classId
                                           && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName,
                                               PupilProfile = pf
                                           }).ToList();

            //Danh sach tat ca hoc sinh co trong table nhan xet
            objDic["listSubjectId"] = null;

            List<EvaluationOfPupilBO> listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
            EvaluationOfPupilBO evaluationTmp = null;

            int subjectID = 0;

            //Lay thong tin khoa cot diem
            string strLockMark = this.CheckLockMark(classId, schoolId, academicYearId);
            strLockMark = strLockMark.Replace("T10", "TH10");


            if (iCopyObject == 1 && (iCopyLocation == 0 || month == 11))
            {
                #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh
                string strEvalutionpattern = string.Empty;
                //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                objDic.Add("lstPupilID", listPupilId);
                List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                List<SummedEvaluationHistory> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                SummedEvaluationHistory sumObj = null;
                SummedEvaluationHistory sumObjInsert = null;
                for (int j = 0; j < listSummEvaluationSource.Count; j++)
                {
                    evaluationTmp = new EvaluationOfPupilBO();
                    evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                    evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                    listEvaluationOfPupil.Add(evaluationTmp);
                }

                //For qua danh sach mon hoc
                for (int i = 0; i < listSubjectID.Count; i++)
                {
                    subjectID = listSubjectID[i];
                    //List du lieu nhan xet co trong DB
                    List<SummedEvaluationHistory> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                    for (int k = 0; k < listPupilIDSum.Count; k++)
                    {
                        strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                        sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh

                        if (sumObj != null)
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                sumObj.EndingComments = strEvalutionpattern;
                                sumObj.UpdateTime = DateTime.Now;
                                SummedEvaluationHistoryBusiness.Update(sumObj);

                                //luu log
                                if (String.IsNullOrEmpty(sumObj.EndingEvaluation)) sumObj.EndingEvaluation = null;
                                if (String.IsNullOrEmpty(sumObj.EndingComments)) sumObj.EndingComments = null;

                                if (sumObj.EndingEvaluation != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation")
                                    || sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark")
                                    || sumObj.EndingComments != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                {

                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObj.PupilID && o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObj.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObj.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObj.EvaluationCriteriaID;
                                    }
                                    if (sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObj.PeriodicEndingMark.HasValue ? sumObj.PeriodicEndingMark.Value.ToString() : String.Empty);
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").HasValue
                                            ? this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").Value.ToString() : string.Empty);
                                    }

                                    if (sumObj.EndingEvaluation
                                        != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObj.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }

                                    if (sumObj.EndingComments
                                        != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObj.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingComments"));

                                    }

                                }
                            }
                        }
                        else
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                sumObjInsert = new SummedEvaluationHistory();
                                sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                sumObjInsert.AcademicYearID = academicYearId;
                                sumObjInsert.ClassID = classId;
                                sumObjInsert.CreateTime = DateTime.Now;
                                sumObjInsert.EducationLevelID = educationLevelId;
                                sumObjInsert.EndingComments = strEvalutionpattern;
                                sumObjInsert.EndingEvaluation = string.Empty;
                                sumObjInsert.EvaluationCriteriaID = subjectID;
                                sumObjInsert.EvaluationID = evaluationID;
                                sumObjInsert.EvaluationReTraining = null;
                                sumObjInsert.LastDigitSchoolID = partitionId;
                                sumObjInsert.PeriodicEndingMark = null;
                                sumObjInsert.PupilID = listPupilIDSum[k];
                                sumObjInsert.RetestMark = null;
                                sumObjInsert.SchoolID = schoolId;
                                sumObjInsert.SemesterID = semeseterID;
                                SummedEvaluationHistoryBusiness.Insert(sumObjInsert);

                                //luu log
                                if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation)
                                    || !String.IsNullOrEmpty(sumObjInsert.EndingComments)
                                    || sumObjInsert.PeriodicEndingMark.HasValue)
                                {

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObjInsert.PupilID && o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObjInsert.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObjInsert.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObjInsert.EvaluationCriteriaID;
                                    }
                                    if (sumObjInsert.PeriodicEndingMark != null)
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObjInsert.PeriodicEndingMark.Value.ToString());
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObjInsert.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(sumObjInsert.EndingComments))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObjInsert.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, string.Empty);

                                    }

                                }
                            }
                        }
                    }
                }
                SummedEvaluationBusiness.Save();
                #endregion
            }
            else if (iCopyObject == 2 && (iCopyLocation == 0 || month == 11))
            {
                #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh chua duoc nhan xet
                string strEvalutionpattern = string.Empty;
                //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                objDic.Add("lstPupilID", listPupilId);
                List<SummedEvaluationHistory> listSummedEvaluation = SummedEvaluationHistoryBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                List<SummedEvaluationHistory> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                SummedEvaluationHistory sumObj = null;
                SummedEvaluationHistory sumObjInsert = null;
                for (int j = 0; j < listSummEvaluationSource.Count; j++)
                {
                    evaluationTmp = new EvaluationOfPupilBO();
                    evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                    evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                    listEvaluationOfPupil.Add(evaluationTmp);
                }

                //For qua danh sach mon hoc
                for (int i = 0; i < listSubjectID.Count; i++)
                {
                    subjectID = listSubjectID[i];
                    //List du lieu nhan xet co trong DB
                    List<SummedEvaluationHistory> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                    for (int k = 0; k < listPupilIDSum.Count; k++)
                    {
                        strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                        sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh

                        if (sumObj != null)
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                if (!string.IsNullOrEmpty(sumObj.EndingComments)) continue;
                                sumObj.EndingComments = strEvalutionpattern;
                                sumObj.UpdateTime = DateTime.Now;
                                SummedEvaluationHistoryBusiness.Update(sumObj);

                                //luu log
                                if (String.IsNullOrEmpty(sumObj.EndingEvaluation)) sumObj.EndingEvaluation = null;
                                if (String.IsNullOrEmpty(sumObj.EndingComments)) sumObj.EndingComments = null;

                                if (sumObj.EndingEvaluation != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation")
                                    || sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark")
                                    || sumObj.EndingComments != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                {

                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObj.PupilID && o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObj.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObj.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObj.EvaluationCriteriaID;
                                    }
                                    if (sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObj.PeriodicEndingMark.HasValue ? sumObj.PeriodicEndingMark.Value.ToString() : String.Empty);
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").HasValue
                                            ? this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").Value.ToString() : string.Empty);
                                    }

                                    if (sumObj.EndingEvaluation
                                        != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObj.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }

                                    if (sumObj.EndingComments
                                        != this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObj.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, this.context.Entry<SummedEvaluationHistory>(sumObj).OriginalValues.GetValue<string>("EndingComments"));

                                    }

                                }
                            }
                        }
                        else
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                sumObjInsert = new SummedEvaluationHistory();
                                sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                sumObjInsert.AcademicYearID = academicYearId;
                                sumObjInsert.ClassID = classId;
                                sumObjInsert.CreateTime = DateTime.Now;
                                sumObjInsert.EducationLevelID = educationLevelId;
                                sumObjInsert.EndingComments = strEvalutionpattern;
                                sumObjInsert.EndingEvaluation = string.Empty;
                                sumObjInsert.EvaluationCriteriaID = subjectID;
                                sumObjInsert.EvaluationID = evaluationID;
                                sumObjInsert.EvaluationReTraining = null;
                                sumObjInsert.LastDigitSchoolID = partitionId;
                                sumObjInsert.PeriodicEndingMark = null;
                                sumObjInsert.PupilID = listPupilIDSum[k];
                                sumObjInsert.RetestMark = null;
                                sumObjInsert.SchoolID = schoolId;
                                sumObjInsert.SemesterID = semeseterID;
                                SummedEvaluationHistoryBusiness.Insert(sumObjInsert);

                                //luu log
                                if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation)
                                    || !String.IsNullOrEmpty(sumObjInsert.EndingComments)
                                    || sumObjInsert.PeriodicEndingMark.HasValue)
                                {

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObjInsert.PupilID && o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObjInsert.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObjInsert.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObjInsert.EvaluationCriteriaID;
                                    }
                                    if (sumObjInsert.PeriodicEndingMark != null)
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObjInsert.PeriodicEndingMark.Value.ToString());
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObjInsert.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(sumObjInsert.EndingComments))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObjInsert.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, string.Empty);

                                    }

                                }
                            }
                        }
                    }
                }
                SummedEvaluationHistoryBusiness.Save();
                #endregion
            }

            return lstActionAuditBO;
        }

        private List<EvaluationCommentsHistory> SetCommentByGVBM(List<EvaluationCommentsHistory> lstEvaluationComments, IDictionary<string, object> dic)
        {
            string SPACE = "\r\n";
            int evaluationID = Utils.GetInt(dic["EvaluationID"]);
            //lay ra danh sach nhan xet cua GVBM
            List<EvaluationCommentsBO> lstEV = new List<EvaluationCommentsBO>();
            lstEV = EvaluationCommentsBusiness.GetListEvaluationComments(dic);

            List<EvaluationCommentsBO> lsttmp = null;
            EvaluationCommentsBO objtmp = null;
            StringBuilder strComment1 = new StringBuilder();
            StringBuilder strComment2 = new StringBuilder();
            StringBuilder strComment3 = new StringBuilder();
            StringBuilder strComment4 = new StringBuilder();
            StringBuilder strComment5 = new StringBuilder();
            EvaluationCommentsHistory objEvaluationComments = null;
            for (int i = 0; i < lstEvaluationComments.Count; i++)
            {
                strComment1 = new StringBuilder();
                strComment2 = new StringBuilder();
                strComment3 = new StringBuilder();
                strComment4 = new StringBuilder();
                strComment5 = new StringBuilder();
                objEvaluationComments = lstEvaluationComments[i];
                lsttmp = lstEV.Where(p => p.PupilID == objEvaluationComments.PupilID).ToList();
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    objtmp = lsttmp[j];
                    if (evaluationID == 1)
                    {
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM1M6) && !string.IsNullOrEmpty(objtmp.Comment1))
                        {
                            strComment1.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment1).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM2M7) && !string.IsNullOrEmpty(objtmp.Comment2))
                        {
                            strComment2.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment2).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM3M8) && !string.IsNullOrEmpty(objtmp.Comment3))
                        {
                            strComment3.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment3).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM4M9) && !string.IsNullOrEmpty(objtmp.Comment4))
                        {
                            strComment4.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment4).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM5M10) && !string.IsNullOrEmpty(objtmp.Comment5))
                        {
                            strComment5.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment5).Append(SPACE);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM1M6) && !string.IsNullOrEmpty(objtmp.Comment1))
                        {
                            strComment1.Append(objtmp.Comment1).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM2M7) && !string.IsNullOrEmpty(objtmp.Comment2))
                        {
                            strComment2.Append(objtmp.Comment2).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM3M8) && !string.IsNullOrEmpty(objtmp.Comment3))
                        {
                            strComment3.Append(objtmp.Comment3).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM4M9) && !string.IsNullOrEmpty(objtmp.Comment4))
                        {
                            strComment4.Append(objtmp.Comment4).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM5M10) && !string.IsNullOrEmpty(objtmp.Comment5))
                        {
                            strComment5.Append(objtmp.Comment5).Append(SPACE);
                        }
                    }
                }

                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM1M6) && !string.IsNullOrEmpty(strComment1.ToString()))
                {
                    objEvaluationComments.CommentsM1M6 = strComment1.ToString().Substring(0, strComment1.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM2M7) && !string.IsNullOrEmpty(strComment2.ToString()))
                {
                    objEvaluationComments.CommentsM2M7 = strComment2.ToString().Substring(0, strComment2.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM3M8) && !string.IsNullOrEmpty(strComment3.ToString()))
                {
                    objEvaluationComments.CommentsM3M8 = strComment3.ToString().Substring(0, strComment3.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM4M9) && !string.IsNullOrEmpty(strComment4.ToString()))
                {
                    objEvaluationComments.CommentsM4M9 = strComment4.ToString().Substring(0, strComment4.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM5M10) && !string.IsNullOrEmpty(strComment5.ToString()))
                {
                    objEvaluationComments.CommentsM5M10 = strComment5.ToString().Substring(0, strComment5.ToString().Length - 2);
                }
            }

            return lstEvaluationComments;
        }

        #region action audit


        public const string CRITERIA_NXT = "Nhận xét tháng";
        public const string CRITERIA_NXCK = "Nhận xét cuối kỳ";
        public const string CRITERIA_KTDKCK = "KTĐK CK";
        public const string CRITERIA_DG = "Đánh giá";


        #endregion
    }
}
