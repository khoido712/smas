/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author quanglm
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class TrainingProgramBusiness
    {

        #region Tim kiem
        /// <summary>
        /// Tim kiem lop hoc
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<TrainingProgram> Search(IDictionary<string, object> dic)
        {
            IQueryable<TrainingProgram> lsTrainingProgram = this.TrainingProgramBusiness.All.Where(x => x.IsActive);
            return lsTrainingProgram;
        }

        #endregion
    }
}
