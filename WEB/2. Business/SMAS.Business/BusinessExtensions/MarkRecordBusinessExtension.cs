﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  trangdd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Transactions;
using System.Drawing;
using SMAS.Models.Models.CustomModels;
using System.Configuration;
using Oracle.DataAccess.Client;
using System.Data;
using SMAS.VTUtils.Log;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Text;

namespace SMAS.Business.Business
{

    /// <summary>
    /// chi tiết hồ sơ điểm môn học tính điểm
    /// <author>trangdd</author>
    /// <date>02/10/2012</date>
    /// </summary>
    public partial class MarkRecordBusiness
    {

        public int startCol = 5;
        const int rowHeaderStart = 6;
        const int rowHeaderEnd = 13;

        #region get pupil in mark record
        /// <summary>
        /// tim kiem chi tiết hồ sơ điểm môn học tính điểm
        /// <author>hath</author>
        /// <date>05/9/2012</date>
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns>
        /// List{MarkRecordObject}
        /// </returns>
        public List<MarkRecordObject> GetPupilInMarkRecord(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Semester = Utils.GetInt(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            //tien hanh tim
            IQueryable<MarkRecord> lsMarkRecord = MarkRecordRepository.All;
            if (AcademicYearID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.AcademicYearID == AcademicYearID));
            }
            if (SchoolID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (SubjectID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (Semester != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Semester == Semester));
            }
            if (ClassID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.ClassID == ClassID));
            }
            //bat dau xay dung MarkRecordObject
            List<MarkRecordObject> lsObj = new List<MarkRecordObject>();
            IQueryable<PupilProfile> lsPupilProfile = PupilProfileRepository.All.Where(o => (o.IsActive == true));
            IQueryable<MarkType> lsMarkType = MarkTypeRepository.All.Where(o => (o.IsActive == true));
            MarkRecordObject newObj = new MarkRecordObject();
            foreach (var item in lsMarkRecord)
            {
                newObj = new MarkRecordObject();
                var pp = from t in lsPupilProfile
                         where t.PupilProfileID == item.PupilID
                         select t;
                PupilProfile pupilProfile = pp.FirstOrDefault();
                newObj.FullName = pupilProfile.FullName;
                newObj.PupilCode = pupilProfile.PupilCode;
                var mt = from t in lsMarkType
                         where t.MarkTypeID == item.MarkTypeID
                         select t;
                MarkType markType = mt.FirstOrDefault();
                newObj.Title = markType.Title;
                //add to list object
                lsObj.Add(newObj);
            }
            return lsObj;
        }
        #endregion

        #region Get Mard Record Of Class (Lấy bảng điểm của lớp)
        /// <summary>
        ///Lấy bảng điểm của lớp
        /// <author>trangdd</author>
        /// <date>10/10/2012</date>
        /// <edit>hieund9 - 28/01/2013</edit>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MarkRecordBO> GetMardRecordOfClass(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Semester = Utils.GetInt(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PeriodID = Utils.GetInt(dic, "PeriodID");
            //Chiendd1: 14/06/2016, sua lai cau lenh lay trong Vmark
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            IQueryable<PupilOfClass> listPupilOfClassAll = PupilOfClassBusiness.All.Where(o => o.ClassID == ClassID && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID);
            IQueryable<PupilOfClass> listPupilOfClass = listPupilOfClassAll.Where(o => o.AssignedDate == listPupilOfClassAll.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());

            IQueryable<VMarkRecord> vMark = VMarkRecordBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID
                                                             && o.ClassID == ClassID && o.Last2digitNumberSchool == partitionId
                                                             && o.SubjectID == SubjectID);
            IQueryable<MarkRecordBO> query_CurrentYear = from poc in listPupilOfClass
                                                         join pu in PupilProfileBusiness.AllNoTracking on poc.PupilID equals pu.PupilProfileID
                                                         join mr in vMark on poc.PupilID equals mr.PupilID into PupilOfClassMarkRecord
                                                         from PupM in PupilOfClassMarkRecord.DefaultIfEmpty()
                                                         where pu.IsActive == true
                                                         orderby PupM.OrderNumber
                                                         select new MarkRecordBO
                                                         {
                                                             PupilCode = pu.PupilCode,
                                                             FullName = pu.FullName,
                                                             Name = pu.Name,
                                                             Mark = PupM.Mark,
                                                             MarkRecordID = PupM.MarkRecordID,
                                                             PupilID = poc.PupilID,
                                                             ClassID = poc.ClassID,
                                                             SchoolID = poc.SchoolID,
                                                             AcademicYearID = poc.AcademicYearID,
                                                             SubjectID = PupM.SubjectID,
                                                             MarkTypeID = PupM.MarkTypeID,
                                                             Semester = PupM.Semester,
                                                             Title = PupM.Title,
                                                             OrderNumber = PupM.OrderNumber,
                                                             Status = poc.Status,
                                                             MarkedDate = PupM.MarkedDate,
                                                             BrithDate = pu.BirthDate,
                                                             OrderInClass = poc.OrderInClass,
                                                             EndDate = poc.EndDate
                                                         };
            if (Semester > 0)
            {
                query_CurrentYear = query_CurrentYear.Where(c => c.Semester == Semester);
            }
            if (PeriodID > 0)
            {
                string str = this.GetMarkTitle(PeriodID) + ",";
                query_CurrentYear = query_CurrentYear.Where(c => str.Contains(c.Title + ","));
            }
            return query_CurrentYear;


        }

        /// <summary>
        /// lay len diem cua tat ca cac mon hoc trong lop
        /// <author>hungnd 20/01/2014</author>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MarkRecordWSBO> GetMardRecordOfAllSubjectInClass(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            int createdAcaYear = -1;
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            if (aca != null)
            {
                createdAcaYear = aca.Year;
            }
            IQueryable<PupilOfClass> listPupilOfClassAll = PupilOfClassBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID);
            if (ClassID > 0)
            {
                listPupilOfClassAll = listPupilOfClassAll.Where(p => p.ClassID == ClassID);
                lstClassID.Add(ClassID);
            }
            if (lstClassID.Count > 0)
            {
                listPupilOfClassAll = listPupilOfClassAll.Where(p => lstClassID.Contains(p.ClassID));
            }
            IQueryable<PupilOfClass> listPupilOfClass = listPupilOfClassAll.Where(o => o.AssignedDate == listPupilOfClassAll.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());

            var query = from poc in listPupilOfClass
                        join pu in PupilProfileRepository.All on poc.PupilID equals pu.PupilProfileID
                        join mr in VMarkRecordBusiness.All.Where(o => o.SchoolID == SchoolID
                            && o.AcademicYearID == AcademicYearID
                            && lstClassID.Contains(o.ClassID)
                            && o.Last2digitNumberSchool == SchoolID % 100
                            && o.CreatedAcademicYear == createdAcaYear
                            && (o.Semester == Semester || Semester == 0))
                                on poc.PupilID equals mr.PupilID
                        join s in SubjectCatBusiness.All on mr.SubjectID equals s.SubjectCatID
                        join t in MarkTypeBusiness.All on mr.MarkTypeID equals t.MarkTypeID
                        where pu.IsActive == true
                        select new MarkRecordWSBO
                        {
                            Mark = mr.Mark,
                            PupilID = poc.PupilID,
                            SubjectID = mr.SubjectID,
                            SubjectName = s.DisplayName,
                            SubjectOrderNumber = s.OrderInSubject,
                            MarkTypeID = mr.MarkTypeID,
                            MarkOrderNumber = mr.OrderNumber,
                            Title = t.Title,
                            MarkedDate = (mr.ModifiedDate == null ? mr.CreatedDate : mr.ModifiedDate), // Neu da sua lai diem lay thoi gian nhap la modifydate
                            MarkredDatePrimary = mr.MarkedDate
                        };
            return query;
        }
        #endregion

        public IEnumerable<MarkRecordNewBO> GetMardRecordOfClassNew(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Semester = Utils.GetInt(dic, "Semester");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PeriodID = Utils.GetInt(dic, "PeriodID");
            string MarkTitle = "";
            if (PeriodID != 0)
            {
                MarkTitle = this.GetMarkTitle(PeriodID) + ",";
            }
            else
            {
                MarkTitle = this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            }

            var listMarkRecordObject = this.context.PUPIL_GET_MARK_RECORD_OF_CLASS(SchoolID, AcademicYearID, ClassID, Semester, PeriodID, SubjectID, MarkTitle);
            if (listMarkRecordObject != null && listMarkRecordObject.Count() > 0)
            {
                return listMarkRecordObject.ToList();
            }


            return null;
        }


        #region search (Tìm kiếm điểm)
        private IQueryable<MarkRecord> SearchMarkRecord(IDictionary<string, object> dic)
        {
            int MarkRecordID = Utils.GetInt(dic, "MarkRecordID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int PupilFileID = Utils.GetInt(dic, "PupilID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            double Mark = Utils.GetDouble(dic, "Mark", -1);
            double ReTestMark = Utils.GetDouble(dic, "ReTestMark", -1);
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "ListSubjectID");
            List<int> lstClassID = Utils.GetIntList(dic, "ListClassID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupil = Utils.GetString(dic, "NoCheckIsActivePupil");
            IQueryable<MarkRecord> lsMarkRecord = MarkRecordBusiness.AllNoTracking;
            if (ClassID != 0)
            {
                lsMarkRecord = from mr in lsMarkRecord
                               join cp in ClassProfileBusiness.All on mr.ClassID equals cp.ClassProfileID
                               where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                               && mr.ClassID == ClassID
                               && cp.ClassProfileID == ClassID
                               select mr;
            }
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupil))
            {
                lsMarkRecord = lsMarkRecord.Where(o => o.PupilProfile.IsActive);
            }
            // AnhVD 20131217 - Search with partition column
            if (SchoolID > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Last2digitNumberSchool == SchoolID % 100));
            }

            // End 
            if (AcademicYearID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.AcademicYearID == AcademicYearID));
            }
            if (MarkRecordID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkRecordID == MarkRecordID));
            }
            if (SubjectID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (PupilFileID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.PupilID == PupilFileID));
            }
            if (SchoolID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (MarkTypeID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (Year != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.CreatedAcademicYear == Year));
            }
            if (Semester != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Semester == Semester));
            }
            //quanglm sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        lsMarkRecord = lsMarkRecord.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (AppliedLevel != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (Mark != -1)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Mark == (decimal)Mark));
            }
            if (ReTestMark != -1)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Mark == (decimal)ReTestMark));
            }
            if (CreatedDate.HasValue)
            {
                //Nen chuyen trong DB loi thanh kieu DATE thoi de query
                lsMarkRecord = lsMarkRecord.Where(o => o.CreatedDate.HasValue && o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                lsMarkRecord = lsMarkRecord.Where(o => o.ModifiedDate.HasValue && o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (Title != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.Title == Title));
            }
            if (MarkTitle != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.MarkType.Title == MarkTitle));
            }
            if (strMarkType != null && strMarkType != "")
            {
                lsMarkRecord = lsMarkRecord.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (o.ClassProfile.EducationLevelID == EducationLevelID));
            }
            if (lstSubjectID != null && lstSubjectID.Count > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (lstSubjectID.Contains(o.SubjectID)));
            }
            if (lstClassID != null && lstClassID.Count > 0)
            {
                lsMarkRecord = lsMarkRecord.Where(o => (lstClassID.Contains(o.ClassID)));
            }
            return lsMarkRecord;
        }
        #endregion

        public IQueryable<MarkRecord> GetMarkRecordToDelete(int AcademicYearID, int SchoolID, int SubjectID, int Semester, int ClassID, int? PeriodID)
        {
            //string strMarkTitle = MarkRecordBusiness.GetMarkTitle(PeriodID) // Lấy chuỗi chứa các tiêu đề các con điểm 
            //Thực hiện tìm kiếm trong bảng MarkRecord:
            //Select * from MarkRecord mr
            //where mr.AcademicYearID = AcademicYearID 
            //and mr.SchoolID = SchoolID 
            //and mr.SubjectID = SubjectID
            //and mr.Semester = Semester
            //and mr.ClassID = ClassID
            //and strMarkTitle.Containt(mr.Title)
            //and mr.PupilID = PupilID
            string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            IQueryable<MarkRecord> querry = this.All.Where(o => o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && o.SubjectID == SubjectID && o.Semester == Semester
                && o.ClassID == ClassID && strMarkTitle.Contains(o.Title));
            return querry;
        }

        #region Validate
        public void Validate(MarkRecord markRecord)
        {
            ValidationMetadata.ValidateObject(markRecord);
            SubjectCat objSubject = SubjectCatBusiness.Find(markRecord.SubjectID);
            if (objSubject == null || !objSubject.IsApprenticeshipSubject)
            {
                bool SubjectCompatible = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject",
                      new Dictionary<string, object>()
                {
                    {"ClassID",markRecord.ClassID},
                    {"SubjectID",markRecord.SubjectID}
                }, null);
                if (!SubjectCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",markRecord.SchoolID},
                    {"AcademicYearID",markRecord.AcademicYearID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, AcademicYearID: not compatible(PupilProfile)            
            bool PupilCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                     new Dictionary<string, object>()
                {
                    {"PupilProfileID",markRecord.PupilID},
                    {"CurrentAcademicYearID",markRecord.AcademicYearID}
                }, null);
            if (!PupilCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, ClassID: not compatible(PupilProfile)
            // DungVA 27/03
            // Nếu không phải là môn nghề thì check học sinh có thuộc lớp hiện tại hay không?
            if (objSubject != null && !objSubject.IsApprenticeshipSubject)
            {
                bool ClassCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                     new Dictionary<string, object>()
                {
                    {"PupilProfileID",markRecord.PupilID},
                    {"CurrentClassID",markRecord.ClassID}
                }, null);
                if (!ClassCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            // End DungVA
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(markRecord.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            ClassProfile classprofile = ClassProfileBusiness.Find(markRecord.ClassID);
            EducationLevel educationlevel = this.EducationLevelBusiness.Find(classprofile.EducationLevelID);
            //check diem
            if (markRecord.Mark != -1)
                UtilsBusiness.CheckValidateMark(educationlevel.Grade, 0, markRecord.Mark.ToString());
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING             
            IDictionary<string, object> SearchPupilOfClass = new Dictionary<string, object>();
            SearchPupilOfClass["PupilID"] = markRecord.PupilID;
            SearchPupilOfClass["ClassID"] = markRecord.ClassID;
            IQueryable<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.SearchBySchool(markRecord.SchoolID, SearchPupilOfClass);
            PupilOfClass PupilOfClass = ListPupilOfClass.OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (PupilOfClass != null)
            {
                if (!objSubject.IsApprenticeshipSubject)
                {
                    if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                    }
                }
                else
                {
                    if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING && PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                    {
                        throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                    }
                }
            }
            //ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester) = true: Không cho phép nhập điểm (Đối với môn miễn giảm)
            //Chưa có hàm ExemptedSubjectBusiness.IsExemptedSubject(PupilID, ClassID, AcademicYearID, SubjectID, Semester)
            if (ExemptedSubjectBusiness.IsExemptedSubject(markRecord.PupilID, markRecord.ClassID, markRecord.AcademicYearID, markRecord.SubjectID, markRecord.Semester.Value))
            {
                throw new BusinessException("Common_Validate_IsExemptedSubject");
            }
            //Môn học không phải là môn nhận xét: Kiểm tra trong bảng ClassSubject theo điều kiện SubjectID, ClassID và IsCommenting = 0, 2 (nếu kết quả trả về != null => là môn tính điểm)
            IDictionary<string, object> SearchClassSubject = new Dictionary<string, object>();
            SearchClassSubject["SubjectID"] = markRecord.SubjectID;
            SearchClassSubject["ClassID"] = markRecord.ClassID;
            IEnumerable<ClassSubject> ListClassSubjectFromDB = this.ClassSubjectBusiness.SearchBySchool(markRecord.SchoolID, SearchClassSubject);
            ClassSubject classSubject = ListClassSubjectFromDB.FirstOrDefault();
            if (classSubject != null && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK) && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE))
            {
                throw new BusinessException("Common_Validate_IsCommenting ");
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm điểm của học sinh
        /// </summary>
        /// <author>trangdd</author>
        /// <edit>hieund - 28/01/2013</edit>
        /// <date>02/10/2012</date>      
        public bool InsertMarkRecord(int UserID, List<MarkRecord> ListMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? PeriodID,
            List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, int SubjectID, int? EmployeeID,
            int? Year = null)
        {
            bool isDBChange = false;
            try
            {
                if (ListMarkRecord == null || ListMarkRecord.Count == 0)
                {
                    return isDBChange;
                }
                SetAutoDetectChangesEnabled(false);
                int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                var lstPupilSubject = ListMarkRecord.Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                //ID hoc sinh
                List<int> lstPupilID = ListMarkRecord.Select(u => u.PupilID).Distinct().ToList();
                var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                .Distinct();

                List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                List<string> ArrLockedMark = null;
                //neu khong phai tai khoan quan tri truong
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                if (!isSchoolAdmin)
                {
                    string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                    // Neu tat ca con diem bi khoa thi bao loi
                    if (lstLockedMarkDetail.Contains("LHK"))
                    {
                        throw new BusinessException("MarkRecord_Validate_MarkLock");
                    }

                    // Chi lay cac con diem khong bi khoa
                    ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                    ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();

                    for (int i = 0; i < ArrLockedMark.Count; i++)
                    {
                        if (strtmp.Contains(ArrLockedMark[i]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[i], "");
                        }
                    }
                }

                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                #region validate
                //Năm học không phải là năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("Common_Validate_IsCurrentYear");

                // Kiem tra quyen giao vien bo mon va kieu mon hoc
                ClassSubject classSubject = null;
                foreach (var sc in distinctSC)
                {
                    if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, sc.ClassID, sc.SubjectID, Semester))
                        throw new BusinessException("MarkRecord_Validate_InputMarkPermission");
                    // Kiem tra mon hoc la mon tinh diem
                    classSubject = ClassSubjectBusiness.SearchByClass(sc.ClassID, new Dictionary<string, object> { { "SubjectID", sc.SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();
                    if (classSubject == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK && classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                if (!isSchoolAdmin && !timeYear)
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }

                // Nam hoc dang xet khong truong ung voi truong
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra du lieu dot
                if (PeriodID.HasValue)
                {
                    // Kiem tra dot ton tai
                    PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID.Value);
                    if (period == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("MarkRecord_Label_PeriodID");
                        throw new BusinessException("Common_Validate_NotAvailable", Params);
                    }
                    // Dot va hoc ky co khop nhau
                    if (period.Semester != Semester)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }

                Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                lstClassID.ForEach(u =>
                {
                    List<PupilOfClassBO> lstPoc = null;
                    if (Year != null && Year > 0)
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive && poc.Year == Year
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    else
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    dicListPOC[u] = lstPoc.ToList();
                });
                Dictionary<int, List<ExemptedSubject>> dicListExempted = new Dictionary<int, List<ExemptedSubject>>();
                lstClassID.ForEach(u =>
                {
                    dicListExempted[u] = ExemptedSubjectBusiness.GetListExemptedSubject(u, Semester).ToList();
                });

                //bool isMarkJudge = false;
                // Lấy thông tin môn nghề
                // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                // Khi lam phan nghe thi se kiem tra lai cho phu hop
                // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                ApprenticeshipClass apprenticeshipClassSubject = null;
                // Validate mon hoc, mien giam
                foreach (var o in lstPupilSubject)
                {
                    SubjectCat objSubject = null;
                    if (apprenticeshipClassSubject != null)
                    {
                        objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                    }
                    else
                    {
                        objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                    }
                    var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                    if (poc == null
                        || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                    {
                        throw new BusinessException("Import_Validate_StudyingStatus");
                    }
                    if (poc.AcademicYearID != o.AcademicYearID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion

                #region Insert MarkRecord
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                List<MarkRecord> listMarkDb = null;
                // Kiểm tra có phải là môn nghề không?
                if (apprenticeshipClassSubject != null)
                {
                    SearchInfo["SubjectID"] = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectID;
                    SearchInfo["Semester"] = Semester;
                    listMarkDb = this.SearchMarkRecord(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID) && lstClassID.Contains(u.ClassID)).ToList();
                }
                else
                {
                    SearchInfo["ClassID"] = ClassID;
                    SearchInfo["SubjectID"] = SubjectID;
                    SearchInfo["Semester"] = Semester;
                    IQueryable<MarkRecord> listMarkForDelete = this.SearchMarkRecord(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID));
                    // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                    if (!isSchoolAdmin)
                    {
                        listMarkForDelete = listMarkForDelete.Where(v => !ArrLockedMark.Contains(v.Title));
                    }
                    if (PeriodID.HasValue)
                    {
                        listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                    }
                    listMarkDb = listMarkForDelete.ToList();
                }

                string strPupilId = string.Join(",", lstPupilID);


                //Insert diem moi
                List<MarkRecord> listMarkRecordTmp = new List<MarkRecord>();
                MarkRecord mrOfPupilObj = null;
                if (ListMarkRecord != null && ListMarkRecord.Count > 0)
                {
                    listMarkRecordTmp = ListMarkRecord.Where(p => p.Mark != -1).ToList();//Danh sach diem lay tu giao dien
                    int _pupilId = 0;
                    string _title = string.Empty;
                    decimal _mark = 0m;
                    foreach (MarkRecord itemInsert in listMarkRecordTmp)
                    {
                        _pupilId = itemInsert.PupilID;
                        _title = itemInsert.Title;
                        _mark = itemInsert.Mark;
                        //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                        //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                        mrOfPupilObj = listMarkDb.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                        if (mrOfPupilObj != null)
                        {
                            if (mrOfPupilObj.Mark == _mark)
                            {
                                itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                itemInsert.LogChange = mrOfPupilObj.LogChange;
                                itemInsert.SynchronizeID = mrOfPupilObj.SynchronizeID;

                            }
                            else
                            {
                                itemInsert.ModifiedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong

                            }
                            itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                        }
                        else
                        {
                            itemInsert.CreatedDate = DateTime.Now;
                            itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;

                        }
                        itemInsert.CreatedAcademicYear = academicYear.Year;
                    }

                    ///Xoa diem cu
                    decimal historyId = UtilsBusiness.IsMoveHistory(academicYear) ? 1 : 0;
                    //this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, historyId, PeriodID, strPupilId, strtmp);
                    //Them moi diem
                    //this.BulkInsert(listMarkRecordTmp, MarkRecordColumnMappings(), "MarkRecordID");
                    List<string> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID.ToString()).Distinct().ToList();
                    /*if (listMarkRecordTmp.Count > 0)
                    {
                        isDBChange = true;
                    }
                    if (lstMarkIdDelete.Count > 0)
                    {
                        //this.DeleteMarkRecord(academicYearID, SchoolID, ClassID, Semester, SubjectID, (int)historyId, lstMarkIdDelete);
                        isDBChange = true;
                    }*/

                    //Them moi va xoa diem
                    List<CustomType> lstParaDelete = new List<CustomType>(); //new List<string> { "P_ACADEMIC_YEAR_ID", "P_SCHOOL_ID", "P_IS_HISTORY", "P_STR_MARKRECORD_ID_DEL" };

                    CustomType objCustomerType = null;

                    objCustomerType = new CustomType { ParaName = "P_ACADEMIC_YEAR_ID", ParaType = "String", ParaValue = academicYearID.ToString(), IsListParaValue = false };
                    lstParaDelete.Add(objCustomerType);

                    objCustomerType = new CustomType { ParaName = "P_SCHOOL_ID", ParaType = "String", ParaValue = SchoolID.ToString(), IsListParaValue = false };
                    lstParaDelete.Add(objCustomerType);

                    objCustomerType = new CustomType { ParaName = "P_HISTORY", ParaType = "String", ParaValue = historyId.ToString(), IsListParaValue = false };
                    lstParaDelete.Add(objCustomerType);

                    objCustomerType = new CustomType { ParaName = "P_STR_MARKRECORD_ID_DEL", ParaType = "String", ParaValue = "", IsListParaValue = true, ListValue = lstMarkIdDelete };
                    lstParaDelete.Add(objCustomerType);
                    if (listMarkRecordTmp.Count > 0 || lstMarkIdDelete.Count > 0)
                    {
                        BulkInsertAndDelete(listMarkRecordTmp, MarkRecordColumnMappings(), "SP_DELETE_MARK_RECORD_NEW", lstParaDelete, "MarkRecordID");
                        isDBChange = true;
                    }

                }

                #endregion

                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                List<int> pupilIDs = listMarkRecordTmp.Select(p => p.PupilID).Distinct().ToList();
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = PeriodID;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                //ThreadMarkBusiness.InsertActionList(info);
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion

                #region Chay store de tinh diem TBM
                SummedMarkAllPeriodAndSemester(lstPupilID, SchoolID, AcademicYearID, Semester, classSubject, strMarkTitle, PeriodID);
                #endregion
            }
            catch (Exception ex)
            {
                
                string paraList = string.Format("SchoolID={0}, AcademicYearID={1},Grade={2},ClassID={3},SubjectID={4},EmployeeID={5}", SchoolID, AcademicYearID, Grade, ClassID, SubjectID, EmployeeID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertMarkRecord", paraList, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }

            return isDBChange;
        }

        public void InsertMarkRecordByListSubject(int UserID, List<MarkRecord> LstMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, List<int> listSubjectID, int? EmployeeID, int? PeriodID = null, int? Year = null)
        {
            try
            {
                if (LstMarkRecord == null || LstMarkRecord.Count == 0)
                {
                    return;
                }
                int SubjectID = 0;
                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<PupilOfClassBO> lstPupilOfClass = new List<PupilOfClassBO>();
                if (Year != null && Year > 0)
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.Year == Year && poc.SchoolID == SchoolID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }
                else
                {
                lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                   join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                   where pp.IsActive && poc.AcademicYearID == AcademicYearID
                                   select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }

                List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "lstSubjectID", listSubjectID }, { "SchoolID", SchoolID } }).ToList();
                IDictionary<string, object> dicEx = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"SemesterID",Semester},
                    {"ClassID",ClassID},
                    {"lstSubjectID",listSubjectID}
                };
                List<ExemptedSubject> lstEx = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicEx).ToList();
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);

                #region Lay du diem da ton tai trong DB
                List<MarkRecord> listMarkDb = new List<MarkRecord>();
                List<MarkRecord> listMarkDbBySubjectId = new List<MarkRecord>();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["Semester"] = Semester;
                SearchInfo["ListSubjectID"] = listSubjectID;
                listMarkDb = this.SearchMarkRecord(SearchInfo).ToList();
                #endregion


                for (int l = 0; l < listSubjectID.Count; l++)
                {
                    SubjectID = listSubjectID[l];
                    listMarkDbBySubjectId = listMarkDb.Where(v => v.SubjectID == SubjectID).ToList();
                    List<MarkRecord> ListMarkRecord = LstMarkRecord.Where(p => p.SubjectID == SubjectID).ToList();
                    int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                    var lstPupilSubject = ListMarkRecord.Where(p => p.SubjectID == SubjectID).Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                    List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                    //ID hoc sinh
                    List<int> lstPupilID = lstSummedUpRecord.Where(p => p.SubjectID == SubjectID).Select(u => u.PupilID).Distinct().ToList();
                    var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                    .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                    .Distinct();

                    List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                    List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                    //Lấy danh sách khóa con điểm lstLockedMarkDetail
                    List<string> ArrLockedMark = null;

                    //neu khong phai tai khoan quan tri truong

                    if (!isSchoolAdmin)
                    {
                        string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                        // Neu tat ca con diem bi khoa thi bao loi
                        if (lstLockedMarkDetail.Contains("LHK"))
                        {
                            throw new BusinessException("MarkRecord_Validate_MarkLock");
                        }

                        // Chi lay cac con diem khong bi khoa
                        ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                        ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                        for (int i = 0; i < ArrLockedMark.Count; i++)
                        {
                            if (strtmp.Contains(ArrLockedMark[i]))
                            {
                                strtmp = strtmp.Replace(ArrLockedMark[i], "");
                            }
                        }
                        // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                        listMarkDbBySubjectId = listMarkDb.Where(v => !ArrLockedMark.Contains(v.Title) && v.SubjectID == SubjectID).ToList();
                    }

                    if (PeriodID.HasValue)
                    {
                        listMarkDbBySubjectId = listMarkDb.Where(u => strMarkTitle.Contains(u.Title) && u.SubjectID == SubjectID).ToList();
                    }

                    AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                    #region validate

                    bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                                  : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                    if (!isSchoolAdmin && !timeYear)
                    {
                        throw new BusinessException("Common_IsCurrentYear_Err");
                    }

                    // Nam hoc dang xet khong truong ung voi truong
                    if (academicYear.SchoolID != SchoolID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }

                    Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                    lstClassID.ForEach(u =>
                    {
                        List<PupilOfClassBO> lstPoc = null;
                        if (Year != null && Year > 0)
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        else
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        dicListPOC[u] = lstPoc.ToList();
                    });
                    // Lấy thông tin môn nghề
                    // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                    // Khi lam phan nghe thi se kiem tra lai cho phu hop
                    // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                    ApprenticeshipClass apprenticeshipClassSubject = null;
                    // Validate mon hoc, mien giam
                    foreach (var o in lstPupilSubject)
                    {
                        SubjectCat objSubject = null;
                        if (apprenticeshipClassSubject != null)
                        {
                            objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                        }
                        else
                        {
                            objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                        }
                        //// Mien giam
                        //if (dicListExempted[o.ClassID].Any(u => u.PupilID == o.PupilID && u.SubjectID == o.SubjectID))
                        //{
                        //    throw new BusinessException("Common_Validate_IsExemptedSubject");
                        //}

                        // Trang thai hoc sinh
                        var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                        if (poc == null
                            || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                        {
                            throw new BusinessException("Import_Validate_StudyingStatus");
                        }
                        if (poc.AcademicYearID != o.AcademicYearID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                    }
                    #endregion

                    #region Insert MarkRecord
                    List<int> lstPupilExemID = lstEx.Where(p => p.SubjectID == SubjectID).Select(p => p.PupilID).Distinct().ToList();
                    List<int> lstPupiltmp = lstPupilID.Where(p => !lstPupilExemID.Contains(p)).ToList();
                    string strPupilId = string.Join(",", lstPupiltmp);


                    //Insert diem moi
                    List<MarkRecord> MarkRecordTmp = new List<MarkRecord>();
                    MarkRecord mrOfPupilObj = null;
                    if (ListMarkRecord != null && ListMarkRecord.Count > 0)
                    {
                        MarkRecordTmp = ListMarkRecord.Where(p => lstPupiltmp.Contains(p.PupilID) && p.Mark != -1).ToList();
                        int _pupilId = 0;
                        string _title = string.Empty;
                        decimal _mark = 0m;
                        foreach (MarkRecord itemInsert in MarkRecordTmp)
                        {
                            _pupilId = itemInsert.PupilID;
                            _title = itemInsert.Title;
                            _mark = itemInsert.Mark;
                            //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                            //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                            mrOfPupilObj = listMarkDbBySubjectId.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                            if (mrOfPupilObj != null)
                            {
                                if (mrOfPupilObj.Mark == _mark)
                                {
                                    itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                    itemInsert.LogChange = mrOfPupilObj.LogChange;
                                    itemInsert.SynchronizeID = mrOfPupilObj.SynchronizeID;
                                }
                                else
                                {
                                    itemInsert.ModifiedDate = DateTime.Now;
                                    itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                                }
                                itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                            }
                            else
                            {
                                itemInsert.CreatedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                            }

                            itemInsert.CreatedAcademicYear = academicYear.Year;
                        }

                        ///Xoa diem cu
                        decimal historyId = UtilsBusiness.IsMoveHistory(academicYear) ? 1 : 0;

                        /*this.BulkInsert(MarkRecordTmp, MarkRecordColumnMappings(), "MarkRecordID");
                        //List<long> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID).Distinct().ToList();
                        if (lstMarkIdDelete.Count > 0)
                        {
                            this.DeleteMarkRecord(academicYearID, SchoolID, ClassID, Semester, SubjectID, (int)historyId, lstMarkIdDelete);
                        }*/


                        //Them moi va xoa diem
                        List<string> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID.ToString()).Distinct().ToList();
                        List<CustomType> lstParaDelete = new List<CustomType>();

                        CustomType objCustomerType = null;

                        objCustomerType = new CustomType { ParaName = "P_ACADEMIC_YEAR_ID", ParaType = "String", ParaValue = academicYearID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_SCHOOL_ID", ParaType = "String", ParaValue = SchoolID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_HISTORY", ParaType = "String", ParaValue = historyId.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_STR_MARKRECORD_ID_DEL", ParaType = "String", ParaValue = "", IsListParaValue = true, ListValue = lstMarkIdDelete };
                        lstParaDelete.Add(objCustomerType);
                        if (MarkRecordTmp.Count > 0 || lstMarkIdDelete.Count > 0)
                        {
                            this.BulkInsertAndDelete(MarkRecordTmp, MarkRecordColumnMappings(), GlobalConstants.SP_DELETE_MARK_RECORD, lstParaDelete, "MarkRecordID");
                        }



                    }
                    #endregion

                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> listPupilIDs = lstPupiltmp;
                    //List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listMarkDb.Select(o => o.PupilID)).ToList();
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    //info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = listPupilIDs;
                    //ThreadMarkBusiness.InsertActionList(info);
                    ThreadMarkBusiness.BulkInsertActionList(info);
                    // End
                    #endregion

                    #region Chay store de tinh diem TBM
                    ClassSubject classSubjectObj = listClassSubject.Where(p => p.SubjectID == SubjectID).FirstOrDefault();
                    SummedMarkAllPeriodAndSemester(lstPupilID, SchoolID, AcademicYearID, Semester, classSubjectObj, strMarkTitle, PeriodID);

                    #endregion
                }
            }
            finally
            {
                // this.Save();
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void InsertMarkRecordByListClass(int UserID, List<MarkRecord> LstMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, List<int> listClassID, int SubjectID, int? EmployeeID, int? PeriodID = null, int? Year = null)
        {
            try
            {
                if (LstMarkRecord == null || LstMarkRecord.Count == 0)
                {
                    return;
                }
                int ClassID = 0;
                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "ListClassID", listClassID } }).ToList();
                List<PupilOfClassBO> lstPupilOfClass = new List<PupilOfClassBO>();
                if (Year != null && Year > 0)
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.Year == Year && poc.SchoolID == SchoolID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }
                else
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.AcademicYearID == AcademicYearID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }

                IDictionary<string, object> dicEx = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"SemesterID",Semester},
                    {"lstClassID",listClassID},
                    {"SubjectID",SubjectID}
                };
                List<ExemptedSubject> lstEx = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicEx).ToList();

                #region Lay du diem da ton tai trong DB
                List<MarkRecord> listMarkDb = new List<MarkRecord>();
                List<MarkRecord> listMarkDbByClassID = new List<MarkRecord>();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                SearchInfo["ListClassID"] = listClassID;
                SearchInfo["Semester"] = Semester;
                SearchInfo["SubjectID"] = SubjectID;
                listMarkDb = this.SearchMarkRecord(SearchInfo).ToList();
                #endregion

                for (int l = 0; l < listClassID.Count; l++)
                {
                    ClassID = listClassID[l];
                    listMarkDbByClassID = listMarkDb.Where(p => p.ClassID == ClassID).ToList();
                    this.context.Configuration.AutoDetectChangesEnabled = false;
                    this.context.Configuration.ValidateOnSaveEnabled = false;
                    List<MarkRecord> ListMarkRecord = LstMarkRecord.Where(p => p.ClassID == ClassID).ToList();
                    int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                    var lstPupilSubject = ListMarkRecord.Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                    List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                    //ID hoc sinh
                    List<int> lstPupilID = lstSummedUpRecord.Where(p => p.ClassID == ClassID).Select(u => u.PupilID).Distinct().ToList();
                    var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                    .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                    .Distinct();

                    List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                    List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                    //Lấy danh sách khóa con điểm lstLockedMarkDetail
                    List<string> ArrLockedMark = null;

                    //neu khong phai tai khoan quan tri truong
                    bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                    if (!isSchoolAdmin)
                    {
                        string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                        // Neu tat ca con diem bi khoa thi bao loi
                        if (lstLockedMarkDetail.Contains("LHK"))
                        {
                            throw new BusinessException("MarkRecord_Validate_MarkLock");
                        }

                        // Chi lay cac con diem khong bi khoa
                        ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                        ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                        for (int i = 0; i < ArrLockedMark.Count; i++)
                        {
                            if (strtmp.Contains(ArrLockedMark[i]))
                            {
                                strtmp = strtmp.Replace(ArrLockedMark[i], "");
                            }
                        }
                        // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                        listMarkDbByClassID = listMarkDbByClassID.Where(v => !ArrLockedMark.Contains(v.Title)).ToList();
                    }

                    if (PeriodID.HasValue)
                    {
                        listMarkDbByClassID = listMarkDbByClassID.Where(u => strMarkTitle.Contains(u.Title)).ToList();
                    }

                    AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                    #region validate

                    bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                                  : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                    if (!isSchoolAdmin && !timeYear)
                    {
                        throw new BusinessException("Common_IsCurrentYear_Err");
                    }

                    // Nam hoc dang xet khong truong ung voi truong
                    if (academicYear.SchoolID != SchoolID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }


                    Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                    lstClassID.ForEach(u =>
                    {
                        List<PupilOfClassBO> lstPoc = null;
                        if (Year != null && Year > 0)
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        else
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        dicListPOC[u] = lstPoc.ToList();
                    });

                    // Lấy thông tin môn nghề
                    // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                    // Khi lam phan nghe thi se kiem tra lai cho phu hop
                    // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                    ApprenticeshipClass apprenticeshipClassSubject = null;
                    // Validate mon hoc, mien giam
                    foreach (var o in lstPupilSubject)
                    {
                        SubjectCat objSubject = null;
                        if (apprenticeshipClassSubject != null)
                        {
                            objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                        }
                        else
                        {
                            objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                        }

                        // Trang thai hoc sinh
                        var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                        if (poc == null
                            || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                        {
                            throw new BusinessException("Import_Validate_StudyingStatus");
                        }
                        if (poc.AcademicYearID != o.AcademicYearID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                    }
                    #endregion

                    #region Insert MarkRecord

                    //Xoa tat ca diem
                    List<int> lstPupilExemID = lstEx.Where(p => p.ClassID == ClassID).Select(p => p.PupilID).Distinct().ToList();
                    List<int> lstPupilIDtmp = lstPupilID.Where(p => !lstPupilExemID.Contains(p)).ToList();
                    string strPupilId = string.Join(",", lstPupilIDtmp);


                    List<MarkRecord> listMarkRecordTmp = new List<MarkRecord>();
                    MarkRecord mrOfPupilObj = null;
                    //Insert diem moi
                    if (ListMarkRecord != null && ListMarkRecord.Count > 0)
                    {
                        listMarkRecordTmp = ListMarkRecord.Where(p => lstPupilIDtmp.Contains(p.PupilID) && p.Mark != -1).ToList();
                        int _pupilId = 0;
                        string _title = string.Empty;
                        decimal _mark = 0m;
                        foreach (MarkRecord itemInsert in listMarkRecordTmp)
                        {
                            _pupilId = itemInsert.PupilID;
                            _title = itemInsert.Title;
                            _mark = itemInsert.Mark;
                            //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                            //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                            mrOfPupilObj = listMarkDbByClassID.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                            if (mrOfPupilObj != null)
                            {
                                if (mrOfPupilObj.Mark == _mark)
                                {
                                    itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                    itemInsert.LogChange = mrOfPupilObj.LogChange;
                                    itemInsert.SynchronizeID = mrOfPupilObj.SynchronizeID;
                                }
                                else
                                {
                                    itemInsert.ModifiedDate = DateTime.Now;
                                    itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                                }
                                itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                            }
                            else
                            {
                                itemInsert.CreatedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;
                            }

                            itemInsert.CreatedAcademicYear = academicYear.Year;
                        }

                        decimal historyId = UtilsBusiness.IsMoveHistory(academicYear) ? 1 : 0;
                        //this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, historyId, PeriodID, strPupilId, strtmp);
                        //Them moi
                        /*this.BulkInsert(listMarkRecordTmp, MarkRecordColumnMappings(), "MarkRecordID");
                        List<long> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID).Distinct().ToList();
                        if (lstMarkIdDelete.Count > 0)
                        {
                            this.DeleteMarkRecord(academicYearID, SchoolID, ClassID, Semester, SubjectID, (int)historyId, lstMarkIdDelete);
                        }*/

                        //Them moi va xoa diem
                        List<string> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID.ToString()).Distinct().ToList();
                        List<CustomType> lstParaDelete = new List<CustomType>();

                        CustomType objCustomerType = null;

                        objCustomerType = new CustomType { ParaName = "P_ACADEMIC_YEAR_ID", ParaType = "String", ParaValue = academicYearID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_SCHOOL_ID", ParaType = "String", ParaValue = SchoolID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_HISTORY", ParaType = "String", ParaValue = historyId.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_STR_MARKRECORD_ID_DEL", ParaType = "String", ParaValue = "", IsListParaValue = true, ListValue = lstMarkIdDelete };
                        lstParaDelete.Add(objCustomerType);
                        if (listMarkRecordTmp.Count > 0 || lstMarkIdDelete.Count > 0)
                        {
                            BulkInsertAndDelete(listMarkRecordTmp, MarkRecordColumnMappings(), GlobalConstants.SP_DELETE_MARK_RECORD, lstParaDelete, "MarkRecordID");
                        }
                    }
                    #endregion

                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = lstPupilIDtmp;
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    //info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = pupilIDs;
                    //ThreadMarkBusiness.InsertActionList(info);
                    ThreadMarkBusiness.BulkInsertActionList(info);
                    // End
                    #endregion

                    #region Chay store de tinh diem TBM

                    ClassSubject classSubjectObj = listClassSubject.Where(p => p.ClassID == ClassID).FirstOrDefault();
                    SummedMarkAllPeriodAndSemester(lstPupilID, SchoolID, AcademicYearID, Semester, classSubjectObj, strMarkTitle, PeriodID);
                    #endregion
                }

            }
            finally
            {
                //this.Save();
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void InsertMarkRecordAll(int UserID, List<MarkRecord> ListMarkRecord, List<SummedUpRecord> lstSummedUpRecord, List<int> lstPupilExempted, IDictionary<string, object> dicParam)
        {
            try
            {
                int SchoolID = Utils.GetInt(dicParam, "SchoolID");
                int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
                int PeriodID = Utils.GetInt(dicParam, "PeriodID");
                int Semester = Utils.GetInt(dicParam, "SemesterID");
                int Year = Utils.GetInt(dicParam, "Year");
                int Grade = Utils.GetInt(dicParam, "Grade");
                bool isInsertAnySubject = Utils.GetBool(dicParam, "IsInsertAnySubject");
                int EducationLevelID = Utils.GetInt(dicParam, "EducationLevelID");
                int SubjectID = 0;
                int ClassID = 0;

                if (ListMarkRecord == null || ListMarkRecord.Count == 0)
                {
                    return;
                }
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                string strMarkTitle = PeriodID > 0 ? this.GetMarkTitle(PeriodID) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                var lstPupilSubject = ListMarkRecord.Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();

                //ID hoc sinh
                List<int> lstPupilID = lstSummedUpRecord.Select(u => u.PupilID).Distinct().ToList();
                var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                .Distinct();

                List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                List<int> lstClassID = distinctSC.Select(p => p.ClassID).ToList();
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                List<string> ArrLockedMark = null;

                //neu khong phai tai khoan quan tri truong
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                if (!isSchoolAdmin)
                {
                    string lstLockedMarkDetail = string.Empty;

                    if (isInsertAnySubject)
                    {
                        for (int i = 0; i < lstSubjectID.Count; i++)
                        {
                            SubjectID = lstSubjectID[i];
                            ClassID = lstClassID.FirstOrDefault();
                            lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                            // Neu tat ca con diem bi khoa thi bao loi
                            if (lstLockedMarkDetail.Contains("LHK") && lstSubjectID.Count == 1)
                            {
                                throw new BusinessException("MarkRecord_Validate_MarkLock");
                            }
                            else
                            {
                                ListMarkRecord = ListMarkRecord.Where(p => p.SubjectID != SubjectID && p.ClassID != ClassID).ToList();
                                //continue;
                            }
                            // Chi lay cac con diem khong bi khoa
                            //ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                            ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                        }
                    }
                    else
                    {
                        for (int i = 0; i < lstClassID.Count; i++)
                        {
                            ClassID = lstClassID[i];
                            SubjectID = lstSubjectID.FirstOrDefault();
                        }
                    }




                }
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                #region validate
                //Năm học không phải là năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("Common_Validate_IsCurrentYear");

                // Kiem tra quyen giao vien bo mon va kieu mon hoc
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>()
                {
                    {"AcademicYearID",academicYearID},
                    {"ListClassID",lstClassID},
                    {"lstSubjectID",lstSubjectID},
                    {"EducationLevelID",EducationLevelID}
                };
                ClassSubject classSubject = null;
                List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicClassSubject).Where(p => p.Last2digitNumberSchool == SchoolID % 100).ToList();
                foreach (var sc in distinctSC)
                {
                    if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, sc.ClassID, sc.SubjectID, Semester))
                        throw new BusinessException("MarkRecord_Validate_InputMarkPermission");
                    // Kiem tra mon hoc la mon tinh diem
                    classSubject = lstClassSubject.Where(p => p.ClassID == sc.ClassID).FirstOrDefault();
                    if (classSubject == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK && classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                if (!isSchoolAdmin && !timeYear)
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }

                // Nam hoc dang xet khong truong ung voi truong
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra du lieu dot
                if (PeriodID > 0)
                {
                    // Kiem tra dot ton tai
                    PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                    if (period == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("MarkRecord_Label_PeriodID");
                        throw new BusinessException("Common_Validate_NotAvailable", Params);
                    }
                    // Dot va hoc ky co khop nhau
                    if (period.Semester != Semester)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }

                Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                lstClassID.ForEach(u =>
                {
                    List<PupilOfClassBO> lstPoc = null;
                    if (Year > 0)
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive && poc.Year == Year
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    else
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    dicListPOC[u] = lstPoc.ToList();
                });
                Dictionary<int, List<ExemptedSubject>> dicListExempted = new Dictionary<int, List<ExemptedSubject>>();
                lstClassID.ForEach(u =>
                {
                    dicListExempted[u] = ExemptedSubjectBusiness.GetListExemptedSubject(u, Semester).ToList();
                });

                //bool isMarkJudge = false;
                // Lấy thông tin môn nghề
                // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                // Khi lam phan nghe thi se kiem tra lai cho phu hop
                // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                ApprenticeshipClass apprenticeshipClassSubject = null;
                // Validate mon hoc, mien giam
                foreach (var o in lstPupilSubject)
                {
                    SubjectCat objSubject = null;
                    if (apprenticeshipClassSubject != null)
                    {
                        objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                    }
                    else
                    {
                        objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                    }
                    // Mien giam
                    if (dicListExempted[o.ClassID].Any(u => u.PupilID == o.PupilID && u.SubjectID == o.SubjectID))
                    {
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                    }

                    // Trang thai hoc sinh
                    var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                    if (poc == null
                        || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                    {
                        throw new BusinessException("Import_Validate_StudyingStatus");
                    }
                    if (poc.AcademicYearID != o.AcademicYearID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    // Kiem tra co phai mon tinh diem ket hop nhan xet
                    //if (objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    //{
                    //    isMarkJudge = true;
                    //}
                }

                if (PeriodID > 0)
                {
                    strMarkTitle = strMarkTitle + ",";
                }
                foreach (MarkRecord markRecord in ListMarkRecord)
                {
                    ValidationMetadata.ValidateObject(markRecord);
                    if (markRecord.Mark != -1)
                    {
                        UtilsBusiness.CheckValidateMark(Grade, SystemParamsInFile.ISCOMMENTING_TYPE_MARK, markRecord.Mark.ToString());
                    }
                    // Kiem tra xem dau diem nhap len da dung voi dau diem duoc quy dinh cua he thong
                    if (!strMarkTitle.Equals(string.Empty) && !strMarkTitle.Contains(markRecord.Title + ","))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion

                #region Insert MarkRecord
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                List<MarkRecord> listMarkDb = null;
                // Kiểm tra có phải là môn nghề không?
                if (apprenticeshipClassSubject != null)
                {
                    SearchInfo["SubjectID"] = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectID;
                    SearchInfo["Semester"] = Semester;
                    listMarkDb = this.SearchMarkRecord(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID) && lstClassID.Contains(u.ClassID)).ToList();
                }
                else
                {
                    //SearchInfo["ClassID"] = ClassID;
                    //SearchInfo["SubjectID"] = SubjectID;
                    SearchInfo["Semester"] = Semester;
                    IQueryable<MarkRecord> listMarkForDelete = this.SearchMarkRecord(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID) && lstClassID.Contains(u.ClassID) && lstSubjectID.Contains(u.SubjectID));
                    // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                    if (!isSchoolAdmin)
                    {
                        listMarkForDelete = listMarkForDelete.Where(v => !ArrLockedMark.Contains(v.Title));
                    }
                    if (PeriodID > 0)
                        listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                    listMarkDb = listMarkForDelete.ToList();
                }
                // Tim kiem danh sach them, sua, xoa
                List<MarkRecord> listInsert = new List<MarkRecord>();
                List<MarkRecord> listUpdate = new List<MarkRecord>();

                if (listMarkDb.Count == 0)
                {
                    listInsert = ListMarkRecord;
                }
                else
                {
                    foreach (MarkRecord itemInsert in ListMarkRecord)
                    {
                        MarkRecord itemDb = listMarkDb.Where(o => o.PupilID == itemInsert.PupilID && o.Title == itemInsert.Title && o.SubjectID == itemInsert.SubjectID && o.ClassID == itemInsert.ClassID).FirstOrDefault();
                        if (itemDb == null)
                        {

                            listInsert.Add(itemInsert);
                        }
                        else
                        {
                            if (!itemInsert.Mark.Equals(itemDb.Mark))
                            {
                                itemDb.OldMark = itemDb.Mark;
                                itemDb.Mark = itemInsert.Mark;
                                itemDb.IsSMS = false;
                                listUpdate.Add(itemDb);
                            }
                            listMarkDb.Remove(itemDb);
                        }
                    }
                }
                if (listInsert.Count > 0)
                {
                    foreach (MarkRecord item in listInsert)
                    {
                        item.CreatedAcademicYear = academicYear.Year;
                        item.CreatedDate = DateTime.Now;
                        this.Insert(item);
                    }
                }
                if (listUpdate.Count > 0)
                {
                    foreach (MarkRecord item in listUpdate)
                    {
                        item.ModifiedDate = DateTime.Now;
                        this.Update(item);
                    }
                }
                if (listMarkDb.Count > 0)
                {
                    this.DeleteAll(listMarkDb);
                }

                #endregion

                this.Save();
                int mode3 = UtilsBusiness.GetPartionId(SchoolID, 3);
                if (isInsertAnySubject)
                {
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    //Chiendd1:17/08/2015, sua lai ham tinh TBM theo mon tang cuong
                    List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listMarkDb.Select(o => o.PupilID)).ToList();
                    IDictionary<string, object> dicclassSubject = new Dictionary<string, object>();
                    ClassSubject objClassSubject;
                    List<ThreadMark> lstThreadMarkBO = new List<ThreadMark>();

                    for (int i = 0; i < lstSubjectID.Count; i++)
                    {
                        SubjectID = lstSubjectID[i];
                        #region Save ThreadMark
                        ThreadMarkBusiness.AddThreadMark(SchoolID, academicYearID, ClassID, SubjectID, (short)Semester, GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE, PeriodID, 0, lstThreadMarkBO);
                        // End
                        #endregion
                        #region Chay store de tinh diem TBM
                        objClassSubject = lstClassSubject.FirstOrDefault(c => c.SubjectID == SubjectID);
                        SummedMarkAllPeriodAndSemester(lstPupilID, SchoolID, AcademicYearID, Semester, objClassSubject, strMarkTitle, PeriodID);

                        #endregion
                    }
                    ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMarkBO);
                }
                else
                {
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listMarkDb.Select(o => o.PupilID)).ToList();
                    List<int> lstPupilIdInClass = new List<int>();
                    ClassSubject objClassSubject;
                    List<ThreadMark> lstThreadMark = new List<ThreadMark>();

                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        ClassID = lstClassID[i];

                        #region Save ThreadMark
                        ThreadMarkBusiness.AddThreadMark(SchoolID, academicYearID, ClassID, SubjectID, (short)Semester, GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE, PeriodID, 0, lstThreadMark);
                        // End
                        #endregion
                        #region Chay store de tinh diem TBM
                        objClassSubject = lstClassSubject.FirstOrDefault(c => c.ClassID == ClassID);
                        lstPupilIdInClass = listInsert.Where(c => c.ClassID == ClassID).Select(o => o.PupilID)
                                            .Union(listUpdate.Where(c => c.ClassID == ClassID).Select(o => o.PupilID))
                                            .Union(listMarkDb.Where(c => c.ClassID == ClassID).Select(o => o.PupilID))
                                            .ToList();
                        SummedMarkAllPeriodAndSemester(lstPupilID, SchoolID, AcademicYearID, Semester, objClassSubject, strMarkTitle, PeriodID);

                        #endregion
                    }
                    if (lstThreadMark != null && lstThreadMark.Count > 0)
                    {
                        ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);
                    }
                }

            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void InsertMarkRecordTXOfPrimary(List<MarkRecord> ListMarkRecord)
        {
            if (ListMarkRecord.Count == 0) return;
            MarkRecord firstRecord = ListMarkRecord[0];
            int schoolID = firstRecord.SchoolID;
            int academicYearID = firstRecord.AcademicYearID;
            int subjectID = firstRecord.SubjectID;
            int classID = firstRecord.ClassID;
            int semester = firstRecord.Semester.Value;
            List<int> listPupilIDs = ListMarkRecord.Select(u => u.PupilID).Distinct().ToList();
            #region Validate
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID}
                }, null);
            if (!SchoolCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");
            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object> { { "SubjectID", subjectID }, { "SchoolID", schoolID } }).SingleOrDefault();
            if (classSubject == null)
                throw new BusinessException("Common_Validate_NotCompatible");
            if (classSubject != null && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK) && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE))
                throw new BusinessException("Common_Validate_IsCommenting ");
            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semester).Where(u => u.SubjectID == subjectID).ToList();

            ClassProfile classProfile = ClassProfileBusiness.Find(classID);
            // Trang thai hoc sinh
            List<PupilOfClassBO> listPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ClassID", classID }, { "Check", "Check" } })
                                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                            select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = pp.CurrentClassID, AcademicYearID = pp.CurrentAcademicYearID, Status = poc.Status }).ToList();
            //check lock mark
            string[] LockTitle = this.GetLockMarkTitlePrimary(schoolID, academicYearID, classID, subjectID, semester).Split(',');
            Dictionary<int, int> dicMonth = new Dictionary<int, int>();
            dicMonth[9] = 1;
            dicMonth[10] = 2;
            dicMonth[11] = 3;
            dicMonth[12] = 4;
            dicMonth[1] = 5;
            dicMonth[2] = 6;
            dicMonth[3] = 7;
            dicMonth[4] = 8;
            dicMonth[5] = 9;
            //Lấy từng phần tử trong lstMarkRecord
            foreach (MarkRecord MarkRecord in ListMarkRecord)
            {
                ValidationMetadata.ValidateObject(MarkRecord);

                PupilOfClassBO poc = listPOC.Where(u => u.PupilID == MarkRecord.PupilID).FirstOrDefault();
                if (poc == null || poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");

                if (poc.AcademicYearID != academicYearID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (poc.ClassID != classID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //check diem
                if (MarkRecord.Mark != -1)
                {
                    UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, 0, MarkRecord.Mark.ToString());

                    if (listExempted.Any(u => u.PupilID == MarkRecord.PupilID))
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                }

            }
            #endregion
            using (TransactionScope scope = new TransactionScope())
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearID;
                dic["ClassID"] = classID;
                dic["Semester"] = semester;
                dic["SubjectID"] = subjectID;
                List<MarkRecord> listMarkDb = MarkRecordBusiness.SearchBySchool(schoolID, dic).Where(u => listPupilIDs.Contains(u.PupilID) && (u.Title == SystemParamsInFile.TX || u.MarkedDate.HasValue)).ToList();
                // Lay diem != -1
                ListMarkRecord = ListMarkRecord.Where(u => u.Mark != -1).ToList();
                // Tim kiem danh sach them, sua, xoa
                List<MarkRecord> listInsert = new List<MarkRecord>();
                List<MarkRecord> listUpdate = new List<MarkRecord>();

                if (listMarkDb.Count == 0)
                {
                    listInsert = ListMarkRecord;
                }
                else
                {
                    foreach (MarkRecord itemInsert in ListMarkRecord)
                    {
                        MarkRecord itemDb = listMarkDb.Where(o => o.PupilID == itemInsert.PupilID && o.Title == itemInsert.Title && o.MarkedDate == itemInsert.MarkedDate)
                            .FirstOrDefault();
                        if (itemDb == null)
                        {
                            listInsert.Add(itemInsert);
                        }
                        else
                        {
                            if (!itemInsert.Mark.Equals(itemDb.Mark))
                            {
                                itemDb.OldMark = itemDb.Mark;
                                itemDb.Mark = itemInsert.Mark;
                                itemDb.IsSMS = false;
                                listUpdate.Add(itemDb);
                            }

                            listMarkDb.Remove(itemDb);
                        }
                    }
                }
                if (listInsert.Count > 0)
                {
                    foreach (MarkRecord item in listInsert)
                    {
                        bool isInsert = true;
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && item.Title.Equals("DTX"))
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (lTitle.Contains("T"))
                                {
                                    string lt = lTitle.Replace("T", "");
                                    int Month = dicMonth[item.MarkedDate.Value.Month];
                                    if (lt == Month.ToString())
                                    {
                                        // Neu co con diem bi khoa thi bo qua
                                        isInsert = false;
                                        //throw new BusinessException("Common_Validate_LockMarkTitleError");
                                    }
                                }
                            }
                        }
                        if (isInsert)
                        {
                            this.Insert(item);
                        }
                    }
                }
                if (listUpdate.Count > 0)
                {
                    foreach (MarkRecord item in listUpdate)
                    {
                        bool isUpdate = true;
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && item.Title.Equals("DTX"))
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (lTitle.Contains("T"))
                                {
                                    string lt = lTitle.Replace("T", "");
                                    int Month = dicMonth[item.MarkedDate.Value.Month];
                                    if (lt == Month.ToString())
                                    {
                                        // Neu co con diem bi khoa thi bo qua
                                        isUpdate = false;
                                        //throw new BusinessException("Common_Validate_LockMarkTitleError");
                                    }
                                }
                            }
                        }
                        if (isUpdate)
                        {
                            this.Update(item);
                        }
                    }
                }
                if (listMarkDb.Count > 0)
                {
                    var ListBreakLock = new List<MarkRecord>();
                    foreach (MarkRecord item in listMarkDb)
                    {
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && item.Title.Equals("DTX"))
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (lTitle.Contains("T"))
                                {
                                    string lt = lTitle.Replace("T", "");
                                    int Month = dicMonth[item.MarkedDate.Value.Month];
                                    if (lt == Month.ToString())
                                    {
                                        ListBreakLock.Add(item);
                                    }
                                }
                            }
                        }
                    }
                    var listDBRemove = listMarkDb.FindAll(u => !ListBreakLock.Any(a => a == u));
                    this.DeleteAll(listDBRemove);
                }
                this.Save();
                scope.Complete();
            }
        }


        #endregion

        #region Delete
        /// <summary>
        ///Xóa điểm học sinh
        /// <author>trangdd</author>
        /// <date>02/10/2012</date>
        /// </summary>
        /// <param name="MarkRecordID">ID điểm môn học</param>
        public void DeleteMarkRecord(int UserID, long MarkRecordID, int? SchoolID)
        {
            //Kiem tra MarkRecordID co ton tai
            MarkRecordBusiness.CheckAvailable((int)MarkRecordID, "MarkRecord_Label_MarkRecordID", false);

            //MarkRecordID, SchoolID: not compatible(MarkRecord)
            bool MarkRecordCompatible = new MarkRecordRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "MarkRecord",
                    new Dictionary<string, object>()
                {
                    {"MarkRecordID",MarkRecordID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!MarkRecordCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Lấy dữ liệu từ DB có MarkRecordID = MarkRecordID
            MarkRecord markRecord = MarkRecordRepository.Find(MarkRecordID);
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING     
            SubjectCat objSubjectCat = SubjectCatBusiness.Find(markRecord.SubjectID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = markRecord.PupilID;
            SearchInfo["ClassID"] = markRecord.ClassID;
            PupilOfClass PupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, SearchInfo).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (!objSubjectCat.IsApprenticeshipSubject)
            {
                if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại                
            if (!AcademicYearBusiness.IsCurrentYear(markRecord.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            if (objSubjectCat.IsApprenticeshipSubject)
            {
                base.Delete(MarkRecordID);
            }
            else if (UtilsBusiness.HasSubjectTeacherPermission(UserID, markRecord.ClassID, markRecord.SubjectID))
            {
                base.Delete(MarkRecordID);
            }
            #region Save ThreadMark
            List<int> pupilIDs = new List<int> { { markRecord.PupilID } };
            // AnhVD 20131225 - Insert into ThreadMark for Auto Process
            ThreadMarkBO info = new ThreadMarkBO();
            info.SchoolID = markRecord.SchoolID;
            info.ClassID = markRecord.ClassID;
            info.AcademicYearID = markRecord.AcademicYearID;
            info.SubjectID = markRecord.SubjectID;
            info.Semester = markRecord.Semester.Value;
            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
            info.PupilIds = pupilIDs;
            ThreadMarkBusiness.InsertActionList(info);
            ThreadMarkBusiness.Save();
            // End
            #endregion
        }

        public void DeleteMarkRecord(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (!subjectCat.IsApprenticeshipSubject)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }
            string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            string strtmp = strMarkTitle;
            bool isAdminSchool = UserAccountBusiness.IsSchoolAdmin(UserID);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["SubjectID"] = SubjectID;
            SearchInfo["Semester"] = Semester;
            if (academicYear != null)
            {
                SearchInfo["Year"] = academicYear.Year;
            }
            string MarkLockRecord = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
            List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
            IQueryable<MarkRecord> listMarkForDelete = this.SearchMarkRecord(SearchInfo).Where(u => listPupilID.Contains(u.PupilID));

            if (!isAdminSchool)
            {
                listMarkForDelete = listMarkForDelete.Where(u => !markLocks.Contains(u.Title));
                for (int i = 0; i < markLocks.Count; i++)
                {
                    if (strtmp.Contains(markLocks[i]))
                    {
                        strtmp = strtmp.Replace(markLocks[i], "");
                    }
                }
            }

            if (subjectCat.IsApprenticeshipSubject)
            {
                if (listMarkForDelete.Count() > 0)
                {
                    List<int> lstPupilID = listMarkForDelete.Select(p => p.PupilID).Distinct().ToList();
                    this.SP_DeleteMarkRecord(AcademicYearID, SchoolID, ClassID, Semester, SubjectID, 0, PeriodID, lstPupilID, strtmp);
                }
            }
            else
            {
                if (PeriodID.HasValue)
                    listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                var ListMarkDelete = listMarkForDelete.ToList();
                string lockPrimaryTitle = "";
                if (classProfile.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    lockPrimaryTitle = this.GetLockMarkTitlePrimary(SchoolID, AcademicYearID, ClassID, SubjectID, Semester);
                    string[] LockTitle = lockPrimaryTitle.Split(',');
                    Dictionary<int, int> dicMonth = new Dictionary<int, int>();
                    dicMonth[9] = 1;
                    dicMonth[10] = 2;
                    dicMonth[11] = 3;
                    dicMonth[12] = 4;
                    dicMonth[1] = 5;
                    dicMonth[2] = 6;
                    dicMonth[3] = 7;
                    dicMonth[4] = 8;
                    dicMonth[5] = 9;
                    //if education is 1 then check lock mark primary
                    var ListMarkDeleteEnd = new List<MarkRecord>();
                    foreach (MarkRecord item in ListMarkDelete)
                    {
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0)
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (item.Title.Equals("DTX"))
                                {
                                    if (lTitle.Contains("T"))
                                    {
                                        string lt = lTitle.Replace("T", "");
                                        int Month = dicMonth[item.MarkedDate.Value.Month];
                                        if (lt == Month.ToString())
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                }
                                else if (item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                {

                                    if (lTitle.Equals(SystemParamsInFile.GKI))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DGK1) || item.Title.Equals(SystemParamsInFile.VGK1) || item.Title.Equals(SystemParamsInFile.GK1))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                    else if (lTitle.Equals(SystemParamsInFile.CKI))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DCK1) || item.Title.Equals(SystemParamsInFile.VCK1) || item.Title.Equals(SystemParamsInFile.CK1))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                }
                                else if (item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                                {

                                    if (lTitle.Equals(SystemParamsInFile.GKII))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DGK2) || item.Title.Equals(SystemParamsInFile.VGK2) || item.Title.Equals(SystemParamsInFile.GK2))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                    else if (lTitle.Equals(SystemParamsInFile.CKII))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DCN) || item.Title.Equals(SystemParamsInFile.VCN) || item.Title.Equals(SystemParamsInFile.CN))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (ListMarkDeleteEnd.Count > 0)
                    {
                        ListMarkDelete = ListMarkDelete.FindAll(u => !ListMarkDeleteEnd.Contains(u));
                    }
                }
                else
                {
                    if (!isAdminSchool) ListMarkDelete = ListMarkDelete.Where(u => !markLocks.Contains(u.Title)).ToList();
                }


                if (ListMarkDelete.Count > 0)
                {
                    //this.DeleteAll(ListMarkDelete);
                    List<int> lstPupilID = ListMarkDelete.Select(p => p.PupilID).Distinct().ToList();
                    int periodId = PeriodID != null ? PeriodID.Value : 0;
                    this.SP_DeleteMarkRecord(AcademicYearID, SchoolID, ClassID, Semester, SubjectID, 0, periodId, lstPupilID, strtmp);
                }

                // Neu la cap 1 thi khong can thi insert vao bang nay
                if (classProfile.EducationLevel.Grade != SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = ListMarkDelete.Select(o => o.PupilID).ToList();
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = pupilIDs;
                    //ThreadMarkBusiness.InsertActionList(info);
                    ThreadMarkBusiness.BulkInsertActionList(info);
                    // End
                    #endregion
                }
            }
        }
        #endregion

        #region Import Mark Record (Import điểm học sinh)
        /// <summary>
        ///Import điểm học sinh
        /// <author>trangdd</author>
        /// <edit>hieund9 - 21/01/2013 Namta</edit>
        /// <date>02/10/2012</date>
        /// </summary>      
        public void ImportMarkRecord(int UserID, List<MarkRecord> lstMarkRecord)
        {
            try
            {

                if (lstMarkRecord.Count == 0)
                    return;

                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                MarkRecord firstMarkRecord = lstMarkRecord.First();
                int schoolID = firstMarkRecord.SchoolID;
                int academicYearID = firstMarkRecord.AcademicYearID;
                int semester = firstMarkRecord.Semester.Value;
                ClassProfile firstClass = ClassProfileBusiness.Find(firstMarkRecord.ClassID);
                int educationLevelID = firstClass.EducationLevelID;
                int grade = firstClass.EducationLevel.Grade;
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                #region validate
                //Năm học không phải là năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("Common_Validate_IsCurrentYear");

                bool timeYear = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                if (!UserAccountBusiness.IsSchoolAdmin(UserID) && !timeYear)
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }

                /*bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                        new Dictionary<string, object>()
                {
                    {"SchoolID", schoolID},
                    {"AcademicYearID", academicYearID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
                }*/

                // Kiem tra neu ton tai hoc sinh co trang thai khac dang hoc thi bao loi
                List<int> listPupilID = lstMarkRecord.Select(o => o.PupilID).Distinct().ToList();
                int countPupil = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "AcademicYearID", academicYearID } })
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where pp.IsActive && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                  && listPupilID.Contains(poc.PupilID)
                                  select poc).Count();

                if (listPupilID.Count != countPupil)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
                // Kiem tra neu co mon hoc va lop hoc khong duoc gan tuong ung trong ClassSubject thi bao loi
                var listClassSubjectID = lstMarkRecord.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
                List<int> listClassID = listClassSubjectID.Select(o => o.ClassID).Distinct().ToList();
                List<ClassSubject> listClassSubject = ClassSubjectBusiness.All.Where(o => listClassID.Contains(o.ClassID) && o.Last2digitNumberSchool == partitionId).ToList();
                foreach (var classSubjectID in listClassSubjectID)
                {
                    ClassSubject cs = listClassSubject.FirstOrDefault(o => o.ClassID == classSubjectID.ClassID && o.SubjectID == classSubjectID.SubjectID);
                    if (cs == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (cs != null && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                // Kiem tra neu co hoc sinh thuoc dang mien giam nhung van co du lieu thi bao loi      
                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.All.Where(o => o.SchoolID == schoolID
                                        && o.AcademicYearID == academicYearID && ((semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                        && o.FirstSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL)
                                        ||
                                        (semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        && o.SecondSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL))).ToList();

                foreach (MarkRecord markRecord in lstMarkRecord)
                {
                    // Kiem tra mien giam
                    if (lstExempted.Any(o => o.PupilID == markRecord.PupilID && o.SubjectID == markRecord.SubjectID && o.ClassID == markRecord.ClassID))
                    {
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                    }
                    ValidationMetadata.ValidateObject(markRecord);
                    if (markRecord.Mark != -1)
                    {
                        UtilsBusiness.CheckValidateMark(grade, SystemParamsInFile.ISCOMMENTING_TYPE_MARK, markRecord.Mark.ToString());
                    }
                }
                #endregion

                #region them moi ban ghi markrecord
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                // Thong tin ve cac con diem cua cap hoc
                List<MarkType> listMarkType = MarkTypeBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", grade } }).ToList();
                SemeterDeclaration semeterDeclaration = SemeterDeclarationBusiness.Search(
                new Dictionary<string, object>() {
                { "SchoolID", schoolID },
                { "AcademicYearID", academicYearID },
                { "Semester", semester }
                }
                ).FirstOrDefault();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["Semester"] = semester;
                SearchInfo["EducationLevelID"] = educationLevelID;
                // Neu chi import cho 1 lop thi chi can lay diem cua 1 lop la du
                if (!lstMarkRecord.Any(o => o.ClassID != firstMarkRecord.ClassID))
                {
                    SearchInfo["ClassID"] = firstMarkRecord.ClassID;
                }
                List<MarkRecord> MarkRecordUpdateList = this.SearchMarkRecord(SearchInfo).ToList();
                // Luu lai thong tin de cap nhat vao bang tien trinh tinh TBM tu dong
                Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();
                // Danh sach cac list diem insert de bo sung vao danh sach lay ra dem di tong ket ma khong can
                // phai truy van lai nua
                List<MarkRecord> listMarkRecordInsert = new List<MarkRecord>();
                List<MarkRecord> lstMarkUpdate = new List<MarkRecord>();
                foreach (MarkRecord MarkRecord in lstMarkRecord)
                {
                    //Tìm kiếm bản ghi
                    MarkRecord MarkRecordUpdate = MarkRecordUpdateList.Where(u => u.PupilID == MarkRecord.PupilID &&
                                                                                u.ClassID == MarkRecord.ClassID &&
                                                                                u.SubjectID == MarkRecord.SubjectID &&
                                                                                u.Title == MarkRecord.Title).FirstOrDefault();
                    if (MarkRecordUpdate != null)
                    {
                        //Nếu tìm thấy bản ghi thì cập nhật lại Mark = lstMarkRecord [i].Mark
                        if (MarkRecordUpdate.Mark != MarkRecord.Mark)
                        {
                            MarkRecordUpdate.OldMark = MarkRecordUpdate.Mark;
                            MarkRecordUpdate.Mark = MarkRecord.Mark;
                            MarkRecordUpdate.ModifiedDate = DateTime.Now;
                            lstMarkUpdate.Add(MarkRecordUpdate);
                        }
                    }
                    else
                    {
                        //Nếu không thì insert lstMarkRecord [i] vào bảng MarkRecord
                        MarkRecord.CreatedDate = DateTime.Now;
                        listMarkRecordInsert.Add(MarkRecord);
                    }
                }
                // Insert diem
                if (listMarkRecordInsert.Count > 0)
                {
                    BulkInsert(listMarkRecordInsert, MarkRecordColumnMappings(), "MarkRecordID");

                }
                if (lstMarkUpdate.Count > 0)
                {
                    for (int i = 0; i < lstMarkUpdate.Count; i++)
                    {
                        MarkRecordBusiness.Update(lstMarkUpdate[i]);
                    }
                    this.Save();
                }
                #endregion

                //Thuc hien tinh diem TBM
                #region Tinh diem TBM
                if (listMarkRecordInsert.Count == 0 && lstMarkUpdate.Count == 0)
                {
                    return;
                }
                this.SummedUpMarkAndSummary(lstMarkRecord, schoolID, academicYearID, semester);

                #endregion
            }
            catch (Exception ex)
            {
                string paramList = string.Format("UserID={0}, List<MarkRecord> lstMarkRecord", UserID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportMarkRecord", paramList, ex);

            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        #endregion

        #region Insert MarkRecord Of Primary (Lưu điểm kiểm tra thường xuyên va dinh ky của học sinh cấp 1)
        public void InsertMarkRecordOfPrimary(List<MarkRecord> lstMarkRecordOfPrimary)
        {
            if (lstMarkRecordOfPrimary.Count == 0)
            {
                return;
            }
            //Lấy từng phần tử trong lstMarkRecordOfPrimary
            foreach (MarkRecord MarkRecord in lstMarkRecordOfPrimary)
            {
                if (MarkRecord.Mark != -1)
                    Validate(MarkRecord);
            }
            // Loc diem != -1 truyen vao
            lstMarkRecordOfPrimary = lstMarkRecordOfPrimary.Where(u => u.Mark != -1).ToList();
            //this.BulkInsert(lstMarkRecordOfPrimary);
            foreach (var mr in lstMarkRecordOfPrimary)
            {
                this.Insert(mr);
            }
        }

        public void InsertMarkRecordDKOfPrimary(int User, List<MarkRecord> lstMarkRecordDKOfPrimary, List<SummedUpRecord> LstSummedUpRecord, int Semester)
        {

            if (lstMarkRecordDKOfPrimary.Count == 0 || LstSummedUpRecord.Count == 0) return;
            MarkRecord firstRecord = lstMarkRecordDKOfPrimary[0];
            int schoolID = firstRecord.SchoolID;
            int academicYearID = firstRecord.AcademicYearID;
            int subjectID = firstRecord.SubjectID;
            int classID = firstRecord.ClassID;

            #region Validate
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID}
                }, null);
            if (!SchoolCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object> { { "SubjectID", subjectID }, { "SchoolID", schoolID } }).SingleOrDefault();
            if (classSubject == null)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (classSubject != null && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK) && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE))
                throw new BusinessException("Common_Validate_IsCommenting ");

            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, Semester).Where(u => u.SubjectID == subjectID).ToList();
            ClassProfile classProfile = ClassProfileBusiness.Find(classID);
            List<PupilOfClassBO> listPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ClassID", classID }, { "Check", "Check" } })
                                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                            select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = pp.CurrentClassID, AcademicYearID = pp.CurrentAcademicYearID, Status = poc.Status }).ToList();

            string[] LockTitle = this.GetLockMarkTitlePrimary(schoolID, academicYearID, classID, subjectID, Semester).Split(',');
            //width mark read and write add title in array lockmark
            List<string> LockTitleAdd = LockTitle.ToList();
            string[] LockAddSpecial = new string[] { SystemParamsInFile.GK1, SystemParamsInFile.GK2, SystemParamsInFile.CN, SystemParamsInFile.CK1 };
            foreach (string TitleLock in LockTitle)
            {
                if (LockAddSpecial.Contains(TitleLock))
                {
                    LockTitleAdd.Add("D" + TitleLock);
                    LockTitleAdd.Add("V" + TitleLock);
                }
            }
            LockTitle = LockTitleAdd.ToArray();
            //Lấy từng phần tử trong lstMarkRecord
            foreach (MarkRecord MarkRecord in lstMarkRecordDKOfPrimary)
            {
                ValidationMetadata.ValidateObject(MarkRecord);

                PupilOfClassBO poc = listPOC.Where(u => u.PupilID == MarkRecord.PupilID).FirstOrDefault();
                if (poc == null || poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");

                if (poc.AcademicYearID != academicYearID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (poc.ClassID != classID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //check diem
                if (MarkRecord.Mark != -1)
                {
                    UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, 0, MarkRecord.Mark.ToString());

                    if (listExempted.Any(u => u.PupilID == MarkRecord.PupilID))
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                }
                //check record has lock?
                //if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !MarkRecord.Title.Equals("DTX"))
                //{
                //    foreach (string lTitle in LockTitle)
                //    {

                //        if (lTitle == MarkRecord.Title.ToString())
                //        {
                //            throw new BusinessException("Common_Validate_LockMarkTitleError");
                //        }
                //    }
                //}
            }
            #endregion

            List<int> listPupilIDs = lstMarkRecordDKOfPrimary.Select(u => u.PupilID).Distinct().ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["ClassID"] = classID;
            dic["Semester"] = Semester;
            dic["SubjectID"] = subjectID;
            List<MarkRecord> listMarkDb = MarkRecordBusiness.SearchBySchool(schoolID, dic).Where(u => listPupilIDs.Contains(u.PupilID) && u.MarkedDate == null).ToList();

            //Them moi diem dinh ky
            // Loc diem != -1 truyen vao
            lstMarkRecordDKOfPrimary = lstMarkRecordDKOfPrimary.Where(u => u.Mark != -1).ToList();
            // Tim kiem danh sach them, sua, xoa
            List<MarkRecord> listInsert = new List<MarkRecord>();
            List<MarkRecord> listUpdate = new List<MarkRecord>();

            if (listMarkDb.Count == 0)
            {
                listInsert = lstMarkRecordDKOfPrimary;
            }
            else
            {
                foreach (MarkRecord itemInsert in lstMarkRecordDKOfPrimary)
                {
                    MarkRecord itemDb = listMarkDb.Where(o => o.PupilID == itemInsert.PupilID && o.Title == itemInsert.Title).FirstOrDefault();
                    if (itemDb == null)
                    {
                        listInsert.Add(itemInsert);
                    }
                    else
                    {
                        if (!itemInsert.Mark.Equals(itemDb.Mark))
                        {
                            itemDb.OldMark = itemDb.Mark;
                            itemDb.Mark = itemInsert.Mark;
                            itemDb.IsSMS = false;
                            listUpdate.Add(itemDb);
                        }
                        listMarkDb.Remove(itemDb);
                    }
                }
            }
            if (listInsert.Count > 0)
            {
                foreach (MarkRecord item in listInsert)
                {
                    bool isInsert = true;
                    //check record has lock?
                    if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !item.Title.Equals("DTX"))
                    {
                        foreach (string lTitle in LockTitle)
                        {

                            if (lTitle == item.Title.ToString())
                            {
                                // Neu co con diem bi khoa thi bo qua
                                isInsert = false;
                                //throw new BusinessException("Common_Validate_LockMarkTitleError");
                            }
                        }
                    }
                    if (isInsert)
                    {
                        this.Insert(item);
                    }

                }
            }
            if (listUpdate.Count > 0)
            {
                foreach (MarkRecord item in listUpdate)
                {
                    bool isUpdate = true;
                    if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !item.Title.Equals("DTX"))
                    {
                        foreach (string lTitle in LockTitle)
                        {

                            if (lTitle == item.Title.ToString())
                            {
                                // Neu co con diem bi khoa thi bo qua
                                isUpdate = false;
                                //throw new BusinessException("Common_Validate_LockMarkTitleError");
                            }
                        }
                    }
                    if (isUpdate)
                    {
                        this.Update(item);
                    }

                }
            }
            int count = listMarkDb.Count;
            if (count > 0)
            {
                for (int i = count - 1; i >= 0; i--)
                {
                    MarkRecord item = listMarkDb[i];
                    if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !item.Title.Equals("DTX"))
                    {
                        foreach (string lTitle in LockTitle)
                        {

                            if (lTitle == item.Title.ToString())
                            {
                                // Nhung con diem bi khoa thi se khong xoa
                                listMarkDb.RemoveAt(i);
                            }
                        }
                    }
                }
                if (listMarkDb.Count > 0)
                {
                    this.DeleteAll(listMarkDb);
                }
            }

            // Kiem tra neu nhu co con diem hoc ky cua ky dang xet bi khoa thi khong tinh phan TBM nua
            #region Kiem tra co khoa diem hoc ky cua ky dang xet hay khong
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                if (LockTitle.Contains(SystemParamsInFile.CKI))
                {
                    return;
                }
            }
            else
            {
                if (LockTitle.Contains(SystemParamsInFile.CKII))
                {
                    return;
                }
            }
            #endregion

            // Doan nay xu ly chua toi uu lam
            // Neu cac hoc sinh ma

            //Xoa diem trung binh cua hoc sinh
            IDictionary<string, object> SearchSumUpRecord = new Dictionary<string, object>();
            SearchSumUpRecord["ClassID"] = classID;
            SearchSumUpRecord["AcademicYearID"] = academicYearID;
            SearchSumUpRecord["SubjectID"] = subjectID;
            if (classSubject.SectionPerWeekSecondSemester != 0)
                SearchSumUpRecord["Semester"] = Semester;

            List<SummedUpRecord> listSur = SummedUpRecordBusiness.SearchBySchool(schoolID, SearchSumUpRecord).Where(u => listPupilIDs.Contains(u.PupilID)).ToList();
            if (listSur.Count > 0)
                SummedUpRecordBusiness.DeleteAll(listSur);

            //Neu la hoc ky II thi xoa ca diem tong ket ca nam
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || classSubject.SecondSemesterCoefficient == 0)
            {
                IDictionary<string, object> SearchSurIII = new Dictionary<string, object>();
                SearchSurIII["ClassID"] = classID;
                SearchSurIII["AcademicYearID"] = academicYearID;
                SearchSurIII["SubjectID"] = subjectID;
                SearchSurIII["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecord> listSurIII = SummedUpRecordBusiness.SearchBySchool(schoolID, SearchSurIII).Where(u => listPupilIDs.Contains(u.PupilID)).ToList();
                if (listSurIII.Count > 0)
                    SummedUpRecordBusiness.DeleteAll(listSurIII);
            }

            //truong hop chi hoc o hoc ky I và khong hoc hoc ky 2
            //Insert diem trung binh mon
            foreach (SummedUpRecord SummedUpRecord in LstSummedUpRecord)
            {
                List<MarkRecord> lstMarkRecordOfPupil = lstMarkRecordDKOfPrimary.Where(o => o.PupilID == SummedUpRecord.PupilID).ToList();
                if (lstMarkRecordOfPupil.Count > 0)
                {
                    if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        MarkRecord mrCK1 = lstMarkRecordOfPupil.Where(o => o.Title == SystemParamsInFile.CK1).FirstOrDefault();
                        if (mrCK1 != null)
                        {
                            SummedUpRecord.SummedUpMark = mrCK1.Mark;
                            SummedUpRecord.PeriodID = null;
                            SummedUpRecord.Comment = "";
                            SummedUpRecord.SummedUpDate = DateTime.Now;
                            SummedUpRecord.Semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                            SummedUpRecord.Last2digitNumberSchool = schoolID % 100;
                            this.SummedUpRecordBusiness.Insert(SummedUpRecord);
                            if (classSubject != null)
                            {
                                //chi hoc o hoc ky I
                                if (classSubject.SectionPerWeekSecondSemester == 0)
                                {
                                    //Thuc hien insert ca nam
                                    SummedUpRecord surCN = new SummedUpRecord();
                                    surCN.PupilID = SummedUpRecord.PupilID;
                                    surCN.ClassID = SummedUpRecord.ClassID;
                                    surCN.SchoolID = SummedUpRecord.SchoolID;
                                    surCN.AcademicYearID = SummedUpRecord.AcademicYearID;
                                    surCN.SubjectID = SummedUpRecord.SubjectID;
                                    surCN.IsCommenting = SummedUpRecord.IsCommenting;
                                    surCN.CreatedAcademicYear = SummedUpRecord.CreatedAcademicYear;
                                    surCN.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    surCN.SummedUpMark = mrCK1.Mark;
                                    surCN.PeriodID = null;
                                    surCN.Comment = "";
                                    surCN.SummedUpDate = DateTime.Now;
                                    surCN.Last2digitNumberSchool = schoolID % 100;
                                    this.SummedUpRecordBusiness.Insert(surCN);
                                }
                            }
                        }
                    }
                    else
                    {
                        MarkRecord mrCN = lstMarkRecordOfPupil.Where(o => o.Title == SystemParamsInFile.CN).FirstOrDefault();
                        if (mrCN != null)
                        {
                            SummedUpRecord.SummedUpMark = mrCN.Mark;
                            SummedUpRecord.PeriodID = null;
                            SummedUpRecord.Comment = "";
                            SummedUpRecord.SummedUpDate = DateTime.Now;
                            SummedUpRecord.Semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                            this.SummedUpRecordBusiness.Insert(SummedUpRecord);

                            //Thuc hien insert ca nam
                            SummedUpRecord surCN = new SummedUpRecord();
                            surCN.PupilID = SummedUpRecord.PupilID;
                            surCN.ClassID = SummedUpRecord.ClassID;
                            surCN.SchoolID = SummedUpRecord.SchoolID;
                            surCN.AcademicYearID = SummedUpRecord.AcademicYearID;
                            surCN.SubjectID = SummedUpRecord.SubjectID;
                            surCN.IsCommenting = SummedUpRecord.IsCommenting;
                            surCN.Last2digitNumberSchool = schoolID % 100;
                            surCN.CreatedAcademicYear = SummedUpRecord.CreatedAcademicYear;
                            surCN.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                            surCN.SummedUpMark = mrCN.Mark;
                            surCN.PeriodID = null;
                            surCN.Comment = "";
                            surCN.SummedUpDate = DateTime.Now;
                            this.SummedUpRecordBusiness.Insert(surCN);
                        }
                    }
                }
            }
        }
        #endregion


        #region Search by School
        public IQueryable<MarkRecord> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.SearchMarkRecord(dic);
        }
        #endregion

        #region Update IsSMS is TRUE
        /// <summary>
        /// Anhvd
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilIds"></param>
        /// <returns></returns>
        public bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds)
        {
            IQueryable<MarkRecord> lstMark = this.SearchMarkRecord(dic);
            for (int i = 0; i < pupilIds.Count; i++)
            {
                int pupilId = pupilIds[i];
                IQueryable<MarkRecord> lstMarkPupil = lstMark.Where(o => o.PupilID == pupilId);
                foreach (var item in lstMarkPupil)
                {
                    item.IsSMS = true;
                    this.Update(item);
                }
            }
            this.Save();
            return true;
        }
        #endregion

        /// <summary>
        /// written by namdv
        /// </summary>
        /// <param name="classid"></param>
        /// <param name="semester"></param>InsertMarkRecordDKOfPrimary
        /// <param name="subjectid"></param>
        /// <returns></returns>
        public string GetLockMarkTitle(int schoolid, int academicyearid, int classid, int semester, int subjectid = 0, int periodid = 0)
        {
            List<string> lstLockMarkTitlePeriod = new List<string>();
            string strMarkType = "";
            string markTitle = "";
            if (periodid > 0)
            {
                markTitle = this.GetMarkTitle(periodid);
            }
            else
            {
                //Lay danh sach cac dot da ket thuc hoac dot da bi khoa. 
                //Ap dung cho nghiep vu diem dot da ket thuc thi khong cho nhap diem  con diem o ky
                //Cho phep giao vien nhap diem khi dot da ket thuc
                var lstPeriod = PeriodDeclarationBusiness.AllNoTracking.Where(s => s.SchoolID == schoolid
                                                                                  && s.AcademicYearID == academicyearid
                                                                                  && s.Semester == semester
                                                                                  && (s.EndDate < DateTime.Now || s.IsLock == true)).ToList();
                //Dua cac con diem cua dot vao list
                foreach (var objPeriod in lstPeriod)
                {
                    //Neu dot chua bi khoa va cho giao vien nhap diem thi khong khoa
                    if ((!objPeriod.IsLock.HasValue || objPeriod.IsLock == false) && objPeriod.IsEditMark == true)
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(objPeriod.StrInterviewMark) && objPeriod.StrInterviewMark != ",")
                    {
                        lstLockMarkTitlePeriod.AddRange(objPeriod.StrInterviewMark.Split(',').Where(s => s != ""));
                    }
                    if (!string.IsNullOrEmpty(objPeriod.StrWritingMark) && objPeriod.StrWritingMark != ",")
                    {
                        lstLockMarkTitlePeriod.AddRange(objPeriod.StrWritingMark.Split(',').Where(s => s != ""));
                    }

                    if (!string.IsNullOrEmpty(objPeriod.StrTwiceCoeffiecientMark) && objPeriod.StrTwiceCoeffiecientMark != ",")
                    {
                        lstLockMarkTitlePeriod.AddRange(objPeriod.StrTwiceCoeffiecientMark.Split(',').Where(s => s != ""));
                    }

                    if (objPeriod.ContaintSemesterMark.HasValue && objPeriod.ContaintSemesterMark.Value)
                    {
                        lstLockMarkTitlePeriod.Add(SystemParamsInFile.MARK_TYPE_HK);
                    }

                }
            }
            //Chiendd1: 01/07/2015: Bo sung where dieu kien partitionId cho bang LockedMarkDetail;
            int partitionId = UtilsBusiness.GetPartionId(schoolid);
            var iqLockMarkDetail = LockedMarkDetailBusiness.All.Where(o => o.ClassID == classid && o.Semester == semester && o.Last2digitNumberSchool == partitionId);
            if (subjectid > 0)
            {
                iqLockMarkDetail = iqLockMarkDetail.Where(s => s.SubjectID == subjectid);
            }

            var lstLockMarkDetail = iqLockMarkDetail.Select(o => new { MarkTypeID = o.MarkTypeID, MarkIndex = o.MarkIndex }).Distinct().OrderBy(o => o.MarkTypeID).ThenBy(o => o.MarkIndex).ToList();

            List<int> listMarkTypeID = lstLockMarkDetail.Where(o => o.MarkTypeID.HasValue)
                .Select(o => o.MarkTypeID.Value).Distinct().ToList();
            List<MarkType> listMarkType = MarkTypeBusiness.All.Where(o => listMarkTypeID.Contains(o.MarkTypeID)).ToList();
            List<string> lstLockMarkTitle = new List<string>();
            foreach (var item in lstLockMarkDetail)
            {
                var marktype = listMarkType.Where(o => o.MarkTypeID == item.MarkTypeID).FirstOrDefault();
                string markTitleCompare = (marktype.Title != SystemParamsInFile.MARK_TYPE_HK && marktype.Title != SystemParamsInFile.MARK_TYPE_LHK) ? marktype.Title + item.MarkIndex.ToString() : marktype.Title;
                if (marktype != null && (periodid == 0 ? true : markTitle.Contains(markTitleCompare)) && !strMarkType.Contains(markTitleCompare))
                {
                    strMarkType += "," + markTitleCompare;
                    lstLockMarkTitle.Add(markTitleCompare);
                }

            }

            if (lstLockMarkTitlePeriod.Count > 0)
            {
                for (int i = 0; i < lstLockMarkTitlePeriod.Count; i++)
                {
                    if (lstLockMarkTitle.Contains(lstLockMarkTitlePeriod[i]))
                    {
                        continue;
                    }
                    lstLockMarkTitle.Add(lstLockMarkTitlePeriod[i]);
                }
            }
            if (lstLockMarkTitle.Count > 0)
            {
                return string.Join(",", lstLockMarkTitle);
            }
            return "";
            //return strMarkType;
        }

        /// <summary>
        /// 12/04/2014
        /// QuangLM1
        /// Lay thong tin con diem bi khoa cua toan khoi
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="academicyearid"></param>
        /// <param name="educationlevelid"></param>
        /// <param name="semester"></param>
        /// <returns></returns>
        public string GetLockMarkTitleByEducation(int schoolid, int academicyearid, int educationlevelid, int semester)
        {
            //Chiendd1: 01/07/2015: Bo sung where dieu kien partitionId cho bang LockedMarkDetail;
            int partitionId = UtilsBusiness.GetPartionId(schoolid);
            string strMarkType = "";
            var lstLockMarkDetail = LockedMarkDetailBusiness.All.Where(o => o.SchoolID == schoolid && o.AcademicYearID == academicyearid
                                                                        && o.ClassProfile.EducationLevelID == educationlevelid && o.Semester == semester
                                                                        && o.Last2digitNumberSchool == partitionId)
                                                                        .Select(o => new { MarkTypeID = o.MarkTypeID, MarkIndex = o.MarkIndex })
                                                                        .Distinct().OrderBy(o => o.MarkTypeID).ThenBy(o => o.MarkIndex).ToList();

            List<int> listMarkTypeID = lstLockMarkDetail.Where(o => o.MarkTypeID.HasValue)
                .Select(o => o.MarkTypeID.Value).Distinct().ToList();
            List<MarkType> listMarkType = MarkTypeBusiness.All.Where(o => listMarkTypeID.Contains(o.MarkTypeID)).ToList();
            foreach (var item in lstLockMarkDetail)
            {
                var marktype = listMarkType.Where(o => o.MarkTypeID == item.MarkTypeID).FirstOrDefault();
                string markTitleCompare = (marktype.Title != SystemParamsInFile.MARK_TYPE_HK && marktype.Title != SystemParamsInFile.MARK_TYPE_LHK) ? marktype.Title + item.MarkIndex.ToString() : marktype.Title;
                if (marktype != null && !strMarkType.Contains(markTitleCompare))
                    strMarkType += "," + markTitleCompare;
            }
            return strMarkType;
        }

        /// <summary>
        /// written by namdv
        /// </summary>
        /// <param name="classid"></param>
        /// <param name="semester"></param>
        /// <param name="subjectid"></param>
        /// <returns></returns>
        public Dictionary<int, string> GetLockMarkTitleBySubject(int schoolid, int academicyearid, int classid, int semester, int subjectid = 0)
        {
            //Chiendd1: 01/07/2015: Bo sung where dieu kien partitionId cho bang LockedMarkDetail;
            int partitionId = UtilsBusiness.GetPartionId(schoolid);
            Dictionary<int, string> dicMarkType = new Dictionary<int, string>();
            var lstLockMarkDetail = LockedMarkDetailBusiness.All.Where(o => o.SchoolID == schoolid &&
                                                                            o.AcademicYearID == academicyearid &&
                                                                            o.ClassID == classid && o.Semester == semester
                                                                            && o.Last2digitNumberSchool == partitionId)
                                                                        .Where(o => subjectid == 0 ? true : o.SubjectID == subjectid)
                                                                         .Select(o => new { MarkTypeID = o.MarkTypeID, MarkIndex = o.MarkIndex, SubjectID = o.SubjectID })
                                                                        .Distinct().ToList();

            List<int> listMarkTypeID = lstLockMarkDetail.Where(o => o.MarkTypeID.HasValue)
                .Select(o => o.MarkTypeID.Value).Distinct().ToList();
            List<MarkType> listMarkType = MarkTypeBusiness.All.Where(o => listMarkTypeID.Contains(o.MarkTypeID)).ToList();
            foreach (var item in lstLockMarkDetail)
            {
                string strMarkType = "";
                // Neu da co thi lay ra de cong them chuoi khoa
                if (dicMarkType.ContainsKey(item.SubjectID))
                {
                    strMarkType = dicMarkType[item.SubjectID];
                }
                var marktype = listMarkType.Where(o => o.MarkTypeID == item.MarkTypeID).FirstOrDefault();
                if (marktype != null && !strMarkType.Contains(marktype.Title + item.MarkIndex.ToString()))
                    strMarkType += "," + ((marktype.Title != SystemParamsInFile.MARK_TYPE_HK && marktype.Title != SystemParamsInFile.MARK_TYPE_LHK) ? marktype.Title + item.MarkIndex.ToString() : marktype.Title);
                dicMarkType[item.SubjectID] = strMarkType;
            }
            return dicMarkType;
        }

        /// <summary>
        /// GetMarkTile
        /// </summary>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public string GetMarkTitle(int PeriodID)
        {
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
            string strMarkTitle = "";
            strMarkTitle += period.StrInterviewMark + period.StrWritingMark + period.StrTwiceCoeffiecientMark;
            //if (period.InterviewMark.Value > 0)
            //{
            //    for (int i = 0; i < period.InterviewMark.Value; i++)
            //    {
            //        strMarkTitle += "M" + (period.StartIndexOfInterviewMark + i).ToString() + ",";
            //    }
            //}
            //if (period.WritingMark.Value > 0)
            //{
            //    for (int i = 0; i < period.WritingMark.Value; i++)
            //    {
            //        strMarkTitle += "P" + (period.StartIndexOfWritingMark + i).ToString() + ",";
            //    }
            //}
            //if (period.TwiceCoeffiecientMark.Value > 0)
            //{
            //    for (int i = 0; i < period.TwiceCoeffiecientMark.Value; i++)
            //    {
            //        strMarkTitle += "V" + (period.StartIndexOfTwiceCoeffiecientMark + i).ToString() + ",";
            //    }
            //}
            if (period.ContaintSemesterMark.HasValue && period.ContaintSemesterMark.Value) // Bao gồm điểm kiểm tra học kỳ
            {
                strMarkTitle += "HK";
            }

            return strMarkTitle;
        }

        public string GetMarkTitleForSemester(int SchoolID, int AcademicYearID, int Semester)
        {
            string strMarkType = string.Empty;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["Semester"] = Semester;

            SemeterDeclaration semester = SemeterDeclarationBusiness.Search(dic).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            if (semester == null) return strMarkType;
            for (int i = 1; i <= semester.TwiceCoeffiecientMark; i++)
                strMarkType += "V" + i + ",";
            strMarkType += "HK";

            return strMarkType;
        }

        #region GetHashKey

        public string GetHashKey(MarkRecord MarkRecord)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = MarkRecord.AcademicYearID;
            dic["ApprenticeShipClassID"] = MarkRecord.ClassID;
            dic["SchoolID"] = MarkRecord.SchoolID;
            return ReportUtils.GetHashKey(dic);
        }

        #endregion

        #region CreatExcelMarkRecord

        public int CreatExcelMarkRecord(string InputParameterHashKey, IDictionary<string, object> SearchInfo)
        {
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int ApprenticeshipClassID = Utils.GetInt(SearchInfo, "ApprenticeShipClassID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");

            ApprenticeshipClass appClass = this.ApprenticeshipClassBusiness.Find(ApprenticeshipClassID);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_SO_DIEM_MON_NGHE);
            AcademicYear academicYear = this.AcademicYearBusiness.Find(AcademicYearID);
            string AcademicYear = academicYear.DisplayTitle.ToUpper();
            string SchoolName = this.SchoolProfileBusiness.Find(SchoolID).SchoolName.ToUpper();
            string SubjectName = this.ApprenticeshipClassBusiness.All.Where(o => o.ApprenticeshipClassID == ApprenticeshipClassID).FirstOrDefault().ApprenticeshipSubject.SubjectName.ToUpper();
            string ClassName = appClass.ClassName.ToUpper();
            string SemesterName = ReportUtils.ConvertSemesterForReportName(Semester);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "F7");

            //Fill dữ liệu chung
            sheet.SetCellValue("A2", SchoolName);
            sheet.SetCellValue("A4", "BẢNG ĐIỂM MÔN " + SubjectName + " " + SemesterName + "  " + ClassName);
            sheet.SetCellValue("A5", AcademicYear);

            ApprenticeshipSubject ApprenticeshipSubject = ApprenticeshipClassBusiness.Find(ApprenticeshipClassID).ApprenticeshipSubject;
            int SubjectID = ApprenticeshipSubject.SubjectID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["ApprenticeshipClassID"] = ApprenticeshipClassID;
            List<ApprenticeshipTraining> ListApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(SchoolID, dic).ToList();

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            dic["Year"] = academicYear.Year;
            List<MarkRecord> ListMarkRecord = MarkRecordBusiness.SearchBySchool(SchoolID, dic).ToList();

            List<SummedUpRecordBO> ListSummedUpRecord = new List<SummedUpRecordBO>();
            if (appClass.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["Semester"] = Semester;
                ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordCareerOfClass(AcademicYearID, SchoolID, 0, SubjectID).ToList();
            }
            else
            {
                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["Semester"] = Semester;
                ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordOfClass(AcademicYearID, SchoolID, Semester, 0, null, SubjectID).ToList();
            }

            //IQueryable<MarkType> ListMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", AppliedLevel } });
            IEnumerable<MarkType> ListMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", AppliedLevel }, { "Active", true } }).ToList();
            List<MarkType> ListMarkType1 = ListMarkType.Where(o => o.Title != "HK").OrderBy(u => u.Title).ToList(); // Danh sach cac con diem M, P, V
            List<MarkType> ListMarkType2 = ListMarkType.Where(o => o.Title == "HK").ToList(); // Con diem HK
            dic = new Dictionary<string, object>();
            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
            if (ay != null)
            {
                dic["Year"] = ay.Year;
            }
            dic["SchoolID"] = SchoolID;
            dic["Semester"] = Semester;
            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).FirstOrDefault();

            // Tạo danh sách các tiêu đề cho các cột sinh động trong excel
            int headerStartCol = 4;
            int headerRow = 7;
            int headerStartMCol = 4;
            int headerStartVCol = 4;
            int headerStartPCol = 4;
            int headerHK = 0;
            int countCol = 0;
            int VCol = 0;
            IVTRange rangeHeader = firstSheet.GetRange("E6", "E6");
            IVTRange rangeHeaderTotal = firstSheet.GetRange("F6", "F6");
            foreach (var mt in ListMarkType1)
            {
                if (mt.Title == SystemParamsInFile.MARK_TYPE_M || mt.Title == SystemParamsInFile.MARK_TYPE_P)
                {
                    if (mt.Title == SystemParamsInFile.MARK_TYPE_M)
                    {
                        int startMergeCol = headerStartCol + countCol;
                        for (int i = 1; i <= SemeterDeclaration.InterviewMark; i++)
                        {
                            countCol++;
                            headerStartMCol = headerStartCol + countCol;
                            if (i == 1)
                            {
                                sheet.CopyPasteSameSize(rangeHeader, headerRow, headerStartMCol);
                                sheet.CopyPasteSameSize(rangeHeader, 6, headerStartMCol);
                                sheet.SetCellValue(headerRow, headerStartMCol, mt.Resolution);
                            }
                            else
                            {
                                sheet.CopyPasteSameSize(rangeHeader, headerRow, headerStartMCol);
                                sheet.CopyPasteSameSize(rangeHeader, 6, headerStartMCol);
                            }

                        }
                        sheet.MergeRow(7, startMergeCol + 1, headerStartMCol);

                    }
                    else
                    {
                        int startMergeCol = headerStartCol + countCol;
                        for (int i = 1; i <= SemeterDeclaration.WritingMark; i++)
                        {
                            countCol++;
                            headerStartPCol = headerStartCol + countCol;
                            if (i == 1)
                            {
                                sheet.CopyPasteSameSize(rangeHeader, headerRow, headerStartPCol);
                                sheet.CopyPasteSameSize(rangeHeader, 6, headerStartPCol);
                                sheet.SetCellValue(headerRow, headerStartPCol, mt.Resolution);
                            }
                            else
                            {
                                sheet.CopyPasteSameSize(rangeHeader, headerRow, headerStartPCol);
                                sheet.CopyPasteSameSize(rangeHeader, 6, headerStartPCol);
                            }
                        }
                        sheet.MergeRow(7, startMergeCol + 1, headerStartPCol);
                    }
                }
                else
                {
                    VCol = headerStartCol + countCol;
                    for (int i = 1; i <= SemeterDeclaration.TwiceCoeffiecientMark; i++)
                    {
                        countCol++;
                        headerStartVCol = headerStartCol + countCol;
                        if (i == 1)
                        {
                            sheet.CopyPasteSameSize(rangeHeader, headerRow, headerStartVCol);
                            sheet.CopyPasteSameSize(rangeHeader, 6, headerStartVCol);
                            sheet.SetCellValue(headerRow, headerStartVCol, mt.Resolution);
                        }
                        else
                        {
                            sheet.CopyPasteSameSize(rangeHeader, headerRow, headerStartVCol);
                            sheet.CopyPasteSameSize(rangeHeader, 6, headerStartVCol);
                        }

                    }
                    sheet.MergeRow(7, VCol + 1, headerStartVCol);
                    //sheet.CopyPasteSameSize(rangeHeader, 6, VCol + 1);
                    sheet.SetCellValue(6, VCol + 1, "Điểm hệ số 2");
                }
                headerHK = headerStartCol + countCol + 1;
                sheet.CopyPasteSameSize(rangeHeaderTotal, 6, headerHK);
                sheet.CopyPasteSameSize(rangeHeaderTotal, 7, headerHK);
                sheet.SetCellValue(6, headerHK, "KTHK");
                sheet.MergeColumn(headerHK, 6, 7);
                headerHK++;
            }

            //sheet.CopyPasteSameSize(rangeHeader, 6, 5);
            sheet.SetCellValue(6, 5, "Điểm hệ số 1");
            sheet.CopyPasteSameSize(rangeHeaderTotal, 6, headerHK);
            sheet.CopyPasteSameSize(rangeHeaderTotal, 7, headerHK);
            sheet.SetCellValue(6, headerHK, "TBM");
            sheet.MergeColumn(headerHK, 6, 7); headerHK++;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                sheet.CopyPasteSameSize(rangeHeaderTotal, 6, headerHK);
                sheet.CopyPasteSameSize(rangeHeaderTotal, 7, headerHK);
                sheet.SetCellValue(6, headerHK, "HK I");
                sheet.MergeColumn(headerHK, 6, 7); headerHK++;
                sheet.CopyPasteSameSize(rangeHeaderTotal, 6, headerHK);
                sheet.CopyPasteSameSize(rangeHeaderTotal, 7, headerHK);
                sheet.SetCellValue(6, headerHK, "Cả năm");
                sheet.MergeColumn(headerHK, 6, 7); headerHK++;
            }
            sheet.CopyPasteSameSize(rangeHeaderTotal, 6, headerHK);
            sheet.CopyPasteSameSize(rangeHeaderTotal, 7, headerHK);
            sheet.SetCellValue(6, headerHK, "Xếp loại");
            sheet.MergeColumn(headerHK, 6, 7); headerHK++;
            sheet.MergeRow(6, 5, headerStartPCol);
            sheet.MergeRow(6, VCol + 1, headerStartVCol);



            // fill dữ liệu ra excel
            int startrow = 8;
            int index = 0;
            int startcol = 0;
            char HKI = new char();

            foreach (ApprenticeshipTraining at in ListApprenticeshipTraining)
            {
                index++;

                IVTRange range = firstSheet.GetRange("A9", "D9");
                IVTRange rangeMark = firstSheet.GetRange("E9", "E9");
                IVTRange rangeTotal = firstSheet.GetRange("F9", "F9");
                if (index % 5 == 0 || index == ListApprenticeshipTraining.Count)
                {
                    range = firstSheet.GetRange("A10", "D10");
                    rangeMark = firstSheet.GetRange("E10", "E10");
                    rangeTotal = firstSheet.GetRange("F10", "F10");
                }
                sheet.CopyPasteSameRowHeigh(range, startrow);
                sheet.SetCellValue(startrow, 1, index);
                sheet.SetCellValue(startrow, 4, at.PupilProfile.FullName);
                sheet.SetCellValue(startrow, 3, at.PupilProfile.PupilCode);

                List<MarkRecord> PupilMarkRecord = ListMarkRecord.Where(o => o.PupilID == at.PupilID).ToList();

                SummedUpRecordBO SummedUpRecordBO = null;
                // Neu la hoc ky 1 thi da lay dung diem cua hoc ky 1 trong cau truy van
                // Neu la hoc ky 2 thi lay ca diem ca nam nen phai loc theo semester de lay dung
                // diem cua hoc ky 2
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    SummedUpRecordBO = ListSummedUpRecord.Where(o => o.PupilID == at.PupilID).FirstOrDefault();
                else
                    SummedUpRecordBO = ListSummedUpRecord.Where(o => o.PupilID == at.PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();

                bool Editable = false;

                if (at.PupilProfile.ProfileStatus == SMAS.Business.Common.SystemParamsInFile.PUPIL_STATUS_STUDYING || at.PupilProfile.ProfileStatus == SMAS.Business.Common.SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                {
                    Editable = true;
                }

                //if (Editable)
                //{
                //    bool IsAcepted = ExemptedSubjectBusiness.IsExemptedSubject(at.PupilID, at.ClassID.Value, at.AcademicYearID.Value, SubjectID, (int)Semester);
                //    if (IsAcepted)
                //    {
                //        Editable = false;
                //    }
                //}

                startcol = 5;
                // điểm thành phần
                foreach (var mt in ListMarkType1)
                {
                    if (mt.Title == SystemParamsInFile.MARK_TYPE_M)
                    {
                        List<MarkRecord> PupilMarkRecordM = PupilMarkRecord.Where(o => o.Title.Contains(SystemParamsInFile.MARK_TYPE_M)).ToList();
                        for (int i = 1; i <= SemeterDeclaration.InterviewMark; i++)
                        {
                            var MarkMRecord = PupilMarkRecordM.Where(o => o.Title == "M" + i.ToString());
                            sheet.CopyPasteSameSize(rangeMark, startrow, startcol);
                            if (MarkMRecord != null && MarkMRecord.Count() > 0)
                            {
                                if (Editable != false)
                                {
                                    sheet.SetCellValue(startrow, startcol, MarkMRecord.FirstOrDefault().Mark);
                                }
                            }
                            startcol++;
                        }

                    }
                    else if (mt.Title == SystemParamsInFile.MARK_TYPE_P)
                    {
                        List<MarkRecord> PupilMarkRecordP = PupilMarkRecord.Where(o => o.Title.Contains(SystemParamsInFile.MARK_TYPE_P)).ToList();
                        for (int i = 1; i <= SemeterDeclaration.WritingMark; i++)
                        {
                            var MarkMRecord = PupilMarkRecordP.Where(o => o.Title == "P" + i.ToString());
                            sheet.CopyPasteSameSize(rangeMark, startrow, startcol);
                            if (MarkMRecord != null && MarkMRecord.Count() > 0)
                            {
                                if (Editable != false)
                                {
                                    sheet.SetCellValue(startrow, startcol, MarkMRecord.FirstOrDefault().Mark);
                                }
                            }
                            startcol++;
                        }
                    }
                    else
                    {
                        List<MarkRecord> PupilMarkRecordM = PupilMarkRecord.Where(o => o.Title.Contains(SystemParamsInFile.MARK_TYPE_V)).ToList();
                        for (int i = 1; i <= SemeterDeclaration.TwiceCoeffiecientMark; i++)
                        {
                            var MarkMRecord = PupilMarkRecordM.Where(o => o.Title == "V" + i.ToString());
                            sheet.CopyPasteSameSize(rangeMark, startrow, startcol);
                            if (MarkMRecord != null && MarkMRecord.Count() > 0)
                            {
                                if (Editable != false)
                                {
                                    sheet.SetCellValue(startrow, startcol, MarkMRecord.FirstOrDefault().Mark);
                                }
                            }
                            startcol++;
                        }
                    }
                }
                decimal HK = (decimal)-1;
                var markHK = PupilMarkRecord.Where(o => o.Title == "HK").ToList();

                if (markHK != null && markHK.Count() > 0)
                {
                    HK = markHK.FirstOrDefault().Mark;
                }
                sheet.CopyPasteSameSize(rangeMark, startrow, startcol);
                if (Editable != false && HK != -1)
                {
                    sheet.SetCellValue(startrow, startcol, HK);
                }
                startcol++;
                // điểm tổng kết

                char E = 'E';
                int e = (int)E;
                // vi trí cột cuối của hệ số 1 
                char last1 = (char)(e + SemeterDeclaration.InterviewMark + SemeterDeclaration.WritingMark - 1);
                // vị trí cột đầu của hệ số 2
                char first2 = (char)(e + SemeterDeclaration.InterviewMark + SemeterDeclaration.WritingMark);
                // vị trí cột cuối của hệ số 2
                char last2 = (char)(e + SemeterDeclaration.InterviewMark + SemeterDeclaration.WritingMark + SemeterDeclaration.TwiceCoeffiecientMark - 1);
                // vị trí cột điểm HK
                char MarkHK = (char)(e + SemeterDeclaration.InterviewMark + SemeterDeclaration.WritingMark + SemeterDeclaration.TwiceCoeffiecientMark);
                int map = e + SemeterDeclaration.InterviewMark + SemeterDeclaration.WritingMark + SemeterDeclaration.TwiceCoeffiecientMark + 1;
                // vị trí cột TBM
                char TBM = (char)(map);

                // fill cột số 2
                string temp2 = "=1*COUNTA(" + E + startrow + ":" + last1 + startrow + ")+2*COUNTA(" + first2 + startrow + ":" + last2 + startrow + ")+IF(" + MarkHK + startrow + "<>'',3,0)";
                sheet.SetCellValue(startrow, 2, temp2.Replace('\'', '\"').Replace(",", ";"));

                // fill cột TBM

                string tempTBM = "=IF(" + MarkHK + startrow + "='','',ROUND(1*(SUM(" + E + startrow + ":" + last1 + startrow + ")+2*SUM(" + first2 + startrow + ":" + last2 + startrow + ")+3*" + MarkHK + startrow + ")/IF(B" + startrow + ">0,B" + startrow + ",1),1))";
                sheet.CopyPasteSameSize(rangeTotal, startrow, startcol);
                sheet.SetCellValue(startrow, startcol, tempTBM.Replace('\'', '\"').Replace(",", ";")); startcol++;

                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    map++;
                    sheet.CopyPasteSameSize(rangeTotal, startrow, startcol);
                    if (SummedUpRecordBO != null)
                    {
                        sheet.SetCellValue(startrow, startcol, SummedUpRecordBO.SummedUpMark);
                    }
                    startcol++;
                    //vị trí cột HK I
                    HKI = (char)(map); map++;
                    // vị trí cột cả năm
                    char CN = (char)(map);
                    // fill cột cả năm
                    string tempCN = "=IF(" + HKI + startrow + "<>'',IF(" + TBM + startrow + "<>'',ROUND((" + HKI + startrow + "+2*" + TBM + startrow + ")/3,1),''),'')";
                    sheet.CopyPasteSameSize(rangeTotal, startrow, startcol);
                    sheet.SetCellValue(startrow, startcol, tempCN.Replace('\'', '\"').Replace(",", ";")); startcol++;
                }

                // vị trí cột gần cuối
                char lastcol = (char)(map);
                // fill cột xếp hạng
                string tempXH = "=IF(" + lastcol + startrow + "='','',IF(" + lastcol + startrow + ">=8,'G',IF(AND(" + lastcol + startrow + ">=6.5," + lastcol + startrow + "<8),'K',IF(AND(" + lastcol + startrow + ">=5," + lastcol + startrow + "<6.5),'TB',IF(AND(" + lastcol + startrow + "<5),'Y')))))";
                sheet.CopyPasteSameSize(rangeTotal, startrow, startcol);
                sheet.SetCellValue(startrow, startcol, tempXH.Replace('\'', '\"').Replace(",", ";"));


                startrow++;
            }


            // chỉnh width 2 cột
            sheet.SetColumnWidth('B', 0);
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                sheet.SetColumnWidth(HKI, 0);
            }




            //Xoá sheet template
            firstSheet.Delete();

            Stream Data = oBook.ToStream();
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = SystemParamsInFile.REPORT_SO_DIEM_MON_NGHE;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = InputParameterHashKey;
            pr.ReportData = ReportUtils.Compress(Data);
            pr.SentToSupervisor = true;


            //Tạo tên file HS_THPT_BangDiemNghe_[Class]_[Semester]_[Subject]
            string outputNamePattern = reportDef.OutputNamePattern;

            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(ClassName));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(SubjectName));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            pr.ReportName = ReportUtils.StripVNSign(pr.ReportName);
            IDictionary<string, object> dictionary = new Dictionary<string, object> {
                {"Semester", Semester},
                {"SchoolID",SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dictionary, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            int FileID = pr.ProcessedReportID;

            return FileID;

        }
        #endregion

        public string GetMarkSemesterTitle(int SchoolID, int AcademicYearID, int Semester)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["Semester"] = Semester;

            SemeterDeclaration semester = SemeterDeclarationBusiness.Search(dic).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            if (semester == null)
            {
                return string.Empty;
            }
            string strMarkTitle = "";
            if (semester.InterviewMark > 0)
            {
                for (int i = 0; i < semester.InterviewMark; i++)
                {
                    strMarkTitle += "M" + (1 + i).ToString() + ",";
                }
            }
            if (semester.WritingMark > 0)
            {
                for (int i = 0; i < semester.WritingMark; i++)
                {
                    strMarkTitle += "P" + (1 + i).ToString() + ",";
                }
            }
            if (semester.TwiceCoeffiecientMark > 0)
            {
                for (int i = 0; i < semester.TwiceCoeffiecientMark; i++)
                {
                    strMarkTitle += "V" + (1 + i).ToString() + ",";
                }
            }
            strMarkTitle += "HK,";
            return strMarkTitle;
        }

        public string GetMarkSemesterTitle(int SemesterID)
        {
            SemeterDeclaration semester = new SemeterDeclarationRepository(this.context).Find(SemesterID);
            string strMarkTitle = "";
            if (semester.InterviewMark > 0)
            {
                for (int i = 0; i < semester.InterviewMark; i++)
                {
                    strMarkTitle += "M" + (1 + i).ToString() + ",";
                }
            }
            if (semester.WritingMark > 0)
            {
                for (int i = 0; i < semester.WritingMark; i++)
                {
                    strMarkTitle += "P" + (1 + i).ToString() + ",";
                }
            }
            if (semester.TwiceCoeffiecientMark > 0)
            {
                for (int i = 0; i < semester.TwiceCoeffiecientMark; i++)
                {
                    strMarkTitle += "V" + (1 + i).ToString() + ",";
                }
            }
            return strMarkTitle;
        }

        public Stream ExportExcelToMarkOrJuge(IDictionary<string, object> dicParam, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null)
        {
            Stream ExcelResult = null;
            int SchoolID = Utils.GetInt(dicParam, "SchoolID");
            int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
            int ClassID = Utils.GetInt(dicParam, "ClassID");
            int SubjectID = Utils.GetInt(dicParam, "SubjectID");
            int Semester = Utils.GetInt(dicParam, "Semester");
            int AppliedLevel = Utils.GetInt(dicParam, "AppliedLevelID");
            int PeriodID = Utils.GetInt(dicParam, "PeriodID");
            int EducationLevelID = Utils.GetInt(dicParam, "EducationLevelID");

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",Semester}
            };
            ClassSubject objClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dic).FirstOrDefault();

            if (objClassSubject.IsCommenting == 0)
            {
                ExcelResult = this.ExportReport(SchoolID, AcademicYearID, Semester, ClassID, PeriodID, SubjectID, out FileName, dicDisplay, isAdmin);
            }
            else
            {
                ExcelResult = JudgeRecordBusiness.ExportReport(SchoolID, AcademicYearID, Semester, AppliedLevel, EducationLevelID, ClassID, PeriodID, SubjectID, out FileName, isAdmin);
            }

            return ExcelResult;
        }

        /// <summary>
        /// ExportReport
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="Semester">The semester.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <param name="PeriodID">The period ID.</param>
        /// <param name="SubjectID">The subject ID.</param>
        /// <param name="FileName">Name of the file.</param>
        /// <returns>
        /// Stream
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/18/2012</Date>
        public Stream ExportReport(int SchoolID, int AcademicYearID, int Semester, int ClassID, int PeriodID, int SubjectID, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null)
        {
            string reportCode = "";
            reportCode = SystemParamsInFile.BANGDIEMTHEODOT;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);


            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);
            string subject = subjectCat.SubjectName;
            string period = PeriodDeclarationBusiness.Find(PeriodID).Resolution;
            string classname = ClassProfileBusiness.Find(ClassID).DisplayName;
            bool isGDCDSubject = subjectCat.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subject));
            outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(period));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(classname));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));

            //FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            FileName = "Bang Diem Mon_" + ReportUtils.StripVNSign(subject) + "_" + ReportUtils.StripVNSign(period) + "_" + ReportUtils.StripVNSign(classname)
                + "_" + ReportUtils.StripVNSign(semester) + ".xls";
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["SubjectID"] = SubjectID;
            search["ClassID"] = ClassID;
            search["AcademicYearID"] = AcademicYearID;
            search["Semester"] = Semester;

            IEnumerable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, search);
            ClassSubject ClassSubject = listClassSubject.FirstOrDefault();

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet emptySheet = oBook.GetSheet(2);
            IVTWorksheet sheet = oBook.GetSheet(3);
            string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
            //Xoa bang thua
            for (int i = 0; i < 5; i++)
            {
                sheet.DeleteRow(13);
            }
            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheet.CopyPasteSameSize(range, "E6");

            #region Tiêu đề template
            //Change header

            String title = "BẢNG ĐIỂM MÔN ";
            title += subjectCat.DisplayName.ToUpper() + " ";

            title += period.ToUpper();
            string SemesterName = ReportUtils.ConvertSemesterForReportName(Semester);
            title += " " + SemesterName + " ";
            title += " LỚP ";
            title += classname.ToUpper();

            sheet.SetCellValue("A2", SchoolProfileBusiness.Find(SchoolID).SchoolName.ToUpper());
            //sheet.SetCellValue("A4", title);

            //sheet.SetCellValue("A5", "NĂM HỌC " + AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());



            #endregion

            //Xac dinh so dau diem
            #region xac dinh so dau diem
            string strMarkTypePeriod = this.GetMarkTitle(PeriodID) + ",";


            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];

            int cntM = 0, cntP = 0, cntV = 0;
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;
                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }
            #endregion

            int startM = 5;
            int startP = startM + cntM;
            int startV = startP + cntP;
            int k = 0;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                sheet.SetCellValue(7, startCol - 1, listM[k]);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                sheet.SetCellValue(7, startCol - 1, listP[k]);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                sheet.SetCellValue(7, startCol - 1, listV[k]);
                k++;
            }



            int P_KTHK = startCol;
            //copy cot KTHK
            if (strMarkTypePeriod.Contains("HK"))
            {
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetColumnWidth(P_KTHK, 9);
                sheet.SetCellValue(6, P_KTHK, "KTHK");
            }


            int P_TBM = startCol;
            //copy cot TBM
            copyCol(firstSheet, sheet, 10, startCol);
            sheet.MergeColumn(P_TBM, 6, 7);
            sheet.SetCellValue(6, P_TBM, "TBM");
            int P_NhanXet = P_TBM;
            //copy cot Nhan xet
            if (isGDCDSubject)
            {
                P_NhanXet = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetCellValue(6, P_NhanXet, "Nhận xét");
            }

            //Merger cot tieu de dau diem
            if (cntM > 0)
            {
                sheet.MergeRow(6, startM, startP - 1);
            }
            if (cntP > 0)
            {
                sheet.MergeRow(6, startP, startV - 1);
            }
            if (cntV > 0)
            {
                sheet.MergeRow(6, startV, startV + cntV - 1);
            }

            //set tieu de cho cac cot dau diem
            if (cntM > 0)
            {
                sheet.SetCellValue(6, startM, "Miệng");
            }
            if (cntP > 0)
            {
                sheet.SetCellValue(6, startP, "15p");
            }
            if (cntV > 0)
            {
                sheet.SetCellValue(6, startV, "1 tiết");
            }
            //dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["Check"] = "check";
            List<PupilOfClassBO> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                .Select(o => new PupilOfClassBO
                {
                    PupilID = o.PupilID,
                    PupilCode = o.PupilProfile.PupilCode,
                    PupilFullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name,
                    Status = o.Status,
                    OrderInClass = o.OrderInClass
                }).ToList().OrderBy(i => i.OrderInClass).ThenBy(i => i.Name).ThenBy(i => i.PupilFullName).ToList();

            int cntPupil = listPupilOfClass.Count();

            //Xac dinh so hoc sinh roi tao cac dong cho hs
            #region tao cac dong trang cho ds hs
            int RowEnd = 8;
            if (cntPupil > 5)
            {
                int numberGroupStudent = cntPupil / 5;
                if (cntPupil % 5 == 0)
                {
                    numberGroupStudent = numberGroupStudent - 1;
                }
                IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_NhanXet);
                for (int j = 1; j <= numberGroupStudent; j++)
                {
                    RowEnd = RowEnd + 5;
                    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                }
            }
            #endregion
            int indexPupil = 0;

            search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SchoolID"] = SchoolID;
            search["Year"] = academicYear.Year;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["AcademicYearID"] = academicYear.AcademicYearID;
            List<MarkRecordBO> listMarkRecord = VMarkRecordBusiness.SearchMarkHistoryOrNoHistory(search).Where(o => strMarkTypePeriod.Contains(o.Title + ",")).ToList();

            search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SchoolID"] = SchoolID;
            search["Year"] = academicYear.Year;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            search["AcademicYearID"] = academicYear.AcademicYearID;
            List<SummedUpRecordBO> listSummedUpRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).ToList();
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, Semester)
                .Where(o => o.SubjectID == SubjectID)
                .ToList();

            int startRow = 8;
            foreach (PupilOfClassBO Pupil in listPupilOfClass)
            {
                indexPupil++;
                sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = "";
                if (stM != stP)
                    edM = convertPosition(startP - 1, startRow);
                else
                { edM = stM; }

                string edP = "";
                if (stP != stV)
                    edP = convertPosition(startV - 1, startRow);
                else
                {
                    edP = stP;
                }
                int endV = 0;
                if (cntV != 0)
                    endV = startV + cntV - 1;
                else
                    endV = startV;

                string edV = convertPosition(endV, startRow);
                // Vi tri diem hk
                string posHK = "";
                // He so
                string cntMark = "=0";
                // TBM
                string TBM = "";
                //
                if (strMarkTypePeriod.Contains("HK"))
                {
                    posHK = convertPosition(P_TBM - 1, startRow);
                    TBM = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((0";

                    if (cntM > 0)
                    {
                        cntMark += "+1*COUNTA(" + stM + ":" + edM + ")";
                        TBM += "+1*SUM(" + stM + ":" + edM + ")";

                    }
                    if (cntP > 0)
                    {
                        cntMark += "+1*COUNTA(" + stP + ":" + edP + ")";
                        TBM += "+1*SUM(" + stP + ":" + edP + ")";

                    }
                    if (cntV > 0)
                    {
                        cntMark += "+2*COUNTA(" + stV + ":" + edV + ") ";
                        TBM += "+2*SUM(" + stV + ":" + edV + ")";

                    }

                    cntMark += "+3*COUNTA(" + posHK + ":" + posHK + ")";
                    TBM += "+" + posHK + "*3)/(" + convertPosition(2, startRow) + ");1);\"\")";
                }
                //
                else
                {
                    TBM = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((0";

                    if (cntM > 0)
                    {
                        cntMark += "+1*COUNTA(" + stM + ":" + edM + ")";
                        TBM += "+1*SUM(" + stM + ":" + edM + ")";

                    }
                    if (cntP > 0)
                    {
                        cntMark += "+1*COUNTA(" + stP + ":" + edP + ")";
                        TBM += "+1*SUM(" + stP + ":" + edP + ")";

                    }
                    if (cntV > 0)
                    {
                        cntMark += "+2*COUNTA(" + stV + ":" + edV + ") ";
                        TBM += "+2*SUM(" + stV + ":" + edV + ")";

                    }
                    //cntMark = "=1*COUNTA(" + stM + ":" + edM + ")+" + "1*COUNTA(" + stP + ":" + edP + ")+" + "2*COUNTA(" + stV + ":" + edV + ")";

                    TBM += ")" + "/(" + convertPosition(2, startRow) + ");1);\"\")";

                    //TBM = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((1*SUM(" + stP + ":" + edP + ")+1*SUM(" + stM + ":" + edM + ")+2*SUM(" + stV + ":" + edV + "))" + "/(" + convertPosition(2, startRow) + ");1);\"\")";
                }
                //string cntMark = "=1*COUNTA(" + stM + ":" + edM + ")+" + "1*COUNTA(" + stP + ":" + edP + ")+" + "2*COUNTA(" + stV + ":" + edV + ")" + "3*COUNTA(" + P_KTHK + ":" + P_KTHK + ")";
                //string cntMark = "=1*COUNTIF(" + stM + ":" + edM + ";" + "\">=0\")";
                sheet.SetFormulaValue(startRow, 2, cntMark);
                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                IVTRange rangeStyle1 = firstSheet.GetRange("D13", "D13");
                IVTRange rangeStyle2 = firstSheet.GetRange("D17", "D17");
                IVTRange rangeStyle3 = firstSheet.GetRange("D14", "D14");
                //xoa column thua neu co
                IVTRange emptyRange = firstSheet.GetRange("M6", "Q12");
                sheet.CopyPaste(emptyRange, 6, startCol);
                //end

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                }

                //Copy row style
                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    if (isGDCDSubject)
                    {
                        sheet.GetRange(startRow, 1, startRow, P_NhanXet).FillColor(Color.Yellow);
                        sheet.GetRange(startRow, 1, startRow, P_NhanXet).IsLock = true; ;
                    }
                    else
                    {
                        sheet.GetRange(startRow, 1, startRow, P_TBM).FillColor(Color.Yellow);
                        sheet.GetRange(startRow, 1, startRow, P_TBM).IsLock = true;
                    }
                }

                int col = 5;
                int j = 0;

                DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;
                DateTime? endDate = Pupil.LeavingDate;

                bool showPupilData = false;

                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                    {
                        showPupilData = true;
                    }
                }
                else
                {
                    showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                    || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                }

                List<MarkRecordBO> lstMarkOfPupil = listMarkRecord.Where(o => o.PupilID == Pupil.PupilID).ToList();
                if (showPupilData)
                {
                    for (int i = startM; i < startP; i++)
                    {
                        String titleMark = listM[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    j = 0;
                    for (int i = startP; i < startV; i++)
                    {
                        String titleMark = listP[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    j = 0;
                    for (int i = startV; i < startV + cntV; i++)
                    {
                        String titleMark = listV[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    //diem kiem tra hk
                    IEnumerable<MarkRecordBO> listMarkRecordMarkSemester = lstMarkOfPupil.Where(o => o.Title.Equals("HK"));
                    if (listMarkRecordMarkSemester != null && listMarkRecordMarkSemester.Count() > 0)
                    {
                        MarkRecordBO MarkRecord = listMarkRecordMarkSemester.FirstOrDefault();
                        sheet.SetCellValue(startRow, P_KTHK, MarkRecord.Mark);
                        //sheet.SetCellValue(startRow, 25, MarkRecord.Mark);
                        //sheet.SetCellValue(startRow, P_KTHK, "=IF(Y" + startRow + "=10, \"10\", Y" + startRow + ")");
                    }
                    if ((strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        sheet.GetRange(startRow, P_KTHK, startRow, P_KTHK).IsLock = true;
                    // Khoá các cột điểm, không cho nhập điểm trong excel đối với học sinh được miễn giảm
                    var ExemptType = listExemptedSubject.Where(p => p.PupilID == Pupil.PupilID).FirstOrDefault();

                    if (ExemptType != null)
                    {
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            sheet.GetRange(startRow, 1, startRow, listMarkType.Count() + 3).FillColor(Color.Yellow);
                            sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + 3);
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, listMarkType.Count() + 4).FillColor(Color.Yellow);
                            sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                        }
                    }

                    if (isGDCDSubject)
                    {
                        sheet.GetRange(startRow, P_NhanXet, startRow, P_NhanXet).IsLock = false;
                        SummedUpRecordBO SummedUpRecord = listSummedUpRecord.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault();
                        if (SummedUpRecord != null)
                            sheet.SetCellValue(startRow, P_NhanXet, SummedUpRecord.Comment);
                    }
                }
                startRow++;
            }
            if (isGDCDSubject)
            {
                P_TBM = P_NhanXet;
            }
            if (cntPupil <= 5)
            {
                sheet.DeleteRow(13);
                sheet.DeleteRow(13);
                int numberGroupStudenttemp = 8 + cntPupil;
                for (int i = numberGroupStudenttemp; i < 13; i++)
                {
                    sheet.DeleteRow(i);
                }
                sheet.DeleteRow(numberGroupStudenttemp);

                sheet.GetRange(8 + cntPupil - 1, 1, 8 + cntPupil - 1, P_TBM).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
            }
            else
            {
                int numberGroupStudenttemp = 8 + cntPupil;
                int cntPupilTemp = (cntPupil / 5 + 1) * 5;
                for (int i = numberGroupStudenttemp; i < cntPupilTemp + 8; i++)
                {
                    sheet.DeleteRow(i);
                }
                if (cntPupil > 10)
                {
                    sheet.DeleteRow(numberGroupStudenttemp);
                }
                sheet.DeleteRow(numberGroupStudenttemp);
                sheet.GetRange(8 + cntPupil - 1, 1, 8 + cntPupil - 1, P_TBM).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
            }
            sheet.MergeRow(4, 1, P_TBM);
            sheet.SetCellValue("A4", title);
            sheet.MergeRow(5, 1, P_TBM);
            sheet.SetCellValue("A5", "NĂM HỌC " + AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());
            sheet.SetColumnWidth(2, 0);
            oBook.GetSheet(4).Delete();
            emptySheet.Delete();
            firstSheet.Delete();
            sheet.ProtectSheet();
            decimal fromNum = 0;
            decimal toNum = 10;
            return oBook.ToStreamNumberValidationData(1, fromNum, toNum, 8, 5, 8 + cntPupil, startV + cntV);
        }

        public Stream ExportAnySubject(IDictionary<string, object> dicParam, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null)
        {
            int ClassID = Utils.GetInt(dicParam, "ClassID");
            int SchoolID = Utils.GetInt(dicParam, "SchoolID");
            int PeriodID = Utils.GetInt(dicParam, "PeriodID");
            int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
            int Semester = Utils.GetInt(dicParam, "Semester");
            string strSubject = Utils.GetString(dicParam, "ArrSubjectID");
            int typeExport = Utils.GetInt(dicParam, "TypeExport");
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            bool isNotShowPupil = objAcademicYear.IsShowPupil.HasValue && objAcademicYear.IsShowPupil.Value;

            string periodName = PeriodDeclarationBusiness.Find(PeriodID).Resolution;
            string classname = ClassProfileBusiness.Find(ClassID).DisplayName;
            List<int> lstSubjectID = strSubject.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
            int SubjectID = 0;
            string reportCode = "";
            reportCode = SystemParamsInFile.BANGDIEMTHEODOT;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            dicParam.Add("ClassName", classname);
            dicParam.Add("PeriodName", periodName);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet emptySheet = oBook.GetSheet(2);
            IVTWorksheet sheet = oBook.GetSheet(3);

            IVTWorksheet firstSheetJudge = oBook.GetSheet(4);
            IVTWorksheet sheetJudge = oBook.CopySheetToLast(firstSheetJudge, "D17");
            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheetJudge.CopyPasteSameSize(range, "E6");
            IVTWorksheet sheettmp = null;


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["Check"] = "check";
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                .Select(o => new PupilOfClassBO
                {
                    PupilID = o.PupilID,
                    PupilCode = o.PupilProfile.PupilCode,
                    PupilFullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name,
                    Status = o.Status,
                    OrderInClass = o.OrderInClass,
                    LeavingDate = o.EndDate
                });
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            List<PupilOfClassBO> listPupilOfClass = iquery.ToList().OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();

            List<ClassSubjectBO> lstClassSubjectBO = new List<ClassSubjectBO>();
            lstClassSubjectBO = (from cs in ClassSubjectBusiness.SearchBySchool(SchoolID, dic)
                                 join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                 where lstSubjectID.Contains(cs.SubjectID)
                                 select new ClassSubjectBO
                                 {
                                     ClassID = cs.ClassID,
                                     SubjectID = cs.SubjectID,
                                     SubjectName = sc.DisplayName,
                                     OrderInSubject = sc.OrderInSubject,
                                     SubjectIDCrease = cs.SubjectIDIncrease.HasValue ? cs.SubjectIDIncrease.Value : 0,
                                     DisplayName = cs.ClassProfile.DisplayName,
                                     IsCommenting = cs.IsCommenting
                                 }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            //lay ra danh sach cac mon tang cuong
            List<int> lstCreaseID = lstClassSubjectBO.Select(p => p.SubjectIDCrease).ToList();
            List<ClassSubjectBO> lstSubjectPlus = lstClassSubjectBO.Where(c => lstCreaseID.Contains(c.SubjectID)).ToList();
            string strLockMarkType = string.Empty;
            string strMarkTypePeriod = this.GetMarkTitle(PeriodID) + ",";
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SchoolID"] = SchoolID;
            search["Year"] = objAcademicYear.Year;
            search["AcademicYearID"] = objAcademicYear.AcademicYearID;
            //search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            List<MarkRecordBO> listMarkRecord = VMarkRecordBusiness.SearchMarkHistoryOrNoHistory(search).Where(o => strMarkTypePeriod.Contains(o.Title + ",")).ToList();

            search["checkWithClassMovement"] = "1";
            List<JudgeRecordBO> listJudgeRecord = VJudgeRecordBusiness.SearchJudgeRecordNoView(search).Where(u => strMarkTypePeriod.Contains(u.Title)).ToList();

            search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SchoolID"] = SchoolID;
            search["Year"] = objAcademicYear.Year;
            //search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            search["AcademicYearID"] = objAcademicYear.AcademicYearID;
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            List<SummedUpRecordBO> listSummedUpRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).ToList();
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, Semester).ToList();
            string[] cd = new string[] { GlobalConstants.SHORT_OK, GlobalConstants.SHORT_NOT_OK };
            ClassSubjectBO objCSBO = null;
            for (int i = 0; i < lstClassSubjectBO.Count; i++)
            {
                objCSBO = lstClassSubjectBO[i];
                SubjectID = objCSBO.SubjectID;
                if (objCSBO.IsCommenting == 0)
                {
                    sheettmp = oBook.CopySheetToLast(sheet);
                    sheettmp.SetCellValue("B3", typeExport);
                    this.SetDataToExportAnySubject(sheettmp, emptySheet, firstSheet, dicParam, strLockMarkType, objCSBO, listPupilOfClass, listMarkRecord, listExemptedSubject, listSummedUpRecord, lstSubjectID, lstSubjectPlus, i + 1, ref lstValidation, isAdmin);
                }
                else
                {
                    sheettmp = oBook.CopySheetToLast(sheetJudge);
                    sheettmp.SetCellValue("B3", typeExport);
                    this.SetDataToSubjectjudge(sheettmp, firstSheetJudge, objCSBO, listPupilOfClass, listJudgeRecord, listSummedUpRecord, listExemptedSubject, objAcademicYear, strLockMarkType, dicParam, i + 1, cd, ref lstValidation, isAdmin);
                }

            }
            firstSheet.Delete();
            firstSheetJudge.Delete();
            emptySheet.Delete();
            sheet.Delete();
            sheetJudge.Delete();
            string tmp = string.Empty;
            if (!Utils.StripVNSign(periodName).ToUpper().Contains("DOT"))
            {
                tmp = "Dot" + periodName;
            }
            else
            {
                tmp = periodName;
            }
            if (typeExport == 1)
            {
                SubjectID = lstSubjectID.FirstOrDefault();
                string subjectName = SubjectCatBusiness.Find(SubjectID).DisplayName;
                FileName = string.Format("BangDiemMon_{0}_{1}_{2}_{3}.xls", Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(subjectName)), Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(tmp)), Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(classname)), Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII");
            }
            else
            {
                FileName = string.Format("BangDiemMonHoc_{0}_{1}.xls", Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(classname)), Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(tmp)));
            }

            return oBook.ToStreamValidationData(lstValidation);
        }

        private void SetDataToSubjectjudge(IVTWorksheet sheet, IVTWorksheet firstSheet, ClassSubjectBO objCSBO, List<PupilOfClassBO> listPupilOfClass,
                                            List<JudgeRecordBO> listJudgeRecord, List<SummedUpRecordBO> listSummedUpRecord, List<ExemptedSubject> listExemptedSubject,
                                            AcademicYear acaYear, string strLockMarkType, IDictionary<string, object> dicParam, int sheetIndex, string[] cd, ref List<VTDataValidation> lstValidation, bool? isAdmin = null)
        {
            string PeriodName = Utils.GetString(dicParam, "PeriodName");
            string ClassName = Utils.GetString(dicParam, "ClassName");
            int SubjectID = objCSBO.SubjectID;
            int ClassID = Utils.GetInt(dicParam, "ClassID");
            int SchoolID = Utils.GetInt(dicParam, "SchoolID");
            int PeriodID = Utils.GetInt(dicParam, "PeriodID");
            int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
            int Semester = Utils.GetInt(dicParam, "Semester");
            VTDataValidation objValidation = new VTDataValidation();
            strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
            listJudgeRecord = listJudgeRecord.Where(p => p.SubjectID == SubjectID).ToList();
            listExemptedSubject = listExemptedSubject.Where(p => p.SubjectID == SubjectID).ToList();
            listSummedUpRecord = listSummedUpRecord.Where(p => p.SubjectID == SubjectID).ToList();
            #region Tiêu đề template
            //Change header
            String title = "BẢNG ĐIỂM MÔN ";
            title += objCSBO.SubjectName.ToUpper() + " ";

            string tmp = string.Empty;
            if (!Utils.StripVNSign(PeriodName).ToUpper().Contains("DOT"))
            {
                tmp = "ĐỢT " + PeriodName;
            }
            else
            {
                tmp = PeriodName;
            }

            title += tmp;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                title += " HKI ";
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                title += " HKII ";
            else
            {
                title += " CẢ NĂM ";
            }
            title += " LỚP ";
            title += objCSBO.DisplayName.ToUpper();
            sheet.SetCellValue("A2", acaYear.SchoolProfile.SchoolName);
            #endregion Tiêu đề template
            #region xac dinh so dau diem

            string strMarkTypePeriod = this.GetMarkTitle(PeriodID);

            //Lấy con điểm bị khoá
            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];

            int cntM = 0, cntP = 0, cntV = 0;
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;
                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }

            #endregion xac dinh so dau diem

            int startM = 5;
            int startP = startM + cntM;
            int startV = startP + cntP;
            int k = 0;
            int P_TBM = 0;
            startCol = 5;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                sheet.SetCellValue(7, startCol - 1, listM[k]);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                sheet.SetCellValue(7, startCol - 1, listP[k]);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                sheet.SetCellValue(7, startCol - 1, listV[k]);
                k++;
            }
            if (strMarkTypePeriod.Contains("HK"))
            {
                //coppy co hoc ki
                copyCol(firstSheet, sheet, 8, startCol);
                //sheet.SetCellValue(6, startCol-1, "HK");
            }

            //copy cot TBM
            P_TBM = startCol;
            copyCol(firstSheet, sheet, 9, startCol);
            // Cột nhận xét
            int P_Nhan_xet = startCol;
            copyCol(firstSheet, sheet, 10, startCol);
            //Merger cot tieu de dau diem
            if (startM != startP)
                sheet.MergeRow(6, startM, startP - 1);
            if (startP != startV)
                sheet.MergeRow(6, startP, startV - 1);
            if (cntV > 0)
            {
                sheet.MergeRow(6, startV, startV + cntV - 1);
            }
            //set tieu de cho cac cot dau diem
            if (cntM > 0)
            {
                sheet.SetCellValue(6, startM, "Miệng");
            }
            if (cntP > 0)
            {
                sheet.SetCellValue(6, startP, "15p");
            }
            if (cntV > 0)
            {
                sheet.SetCellValue(6, startV, "1 tiết");
            }
            int totalPupil = listPupilOfClass.Count;

            //tao cac dong trang cho ds hs
            int RowEnd = 8;
            if (totalPupil > 5)
            {
                int numberGroupStudent = totalPupil / 5;
                IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Nhan_xet);
                for (int j = 1; j <= numberGroupStudent; j++)
                {
                    RowEnd = RowEnd + 5;
                    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                }
            }

            objValidation = new VTDataValidation
            {
                Contrains = cd,
                FromColumn = startM,
                FromRow = 8,
                SheetIndex = sheetIndex,
                ToColumn = startV + cntV,
                ToRow = 7 + totalPupil,
                Type = VTValidationType.LIST
            };
            lstValidation.Add(objValidation);

            IVTRange rangeStyle1 = firstSheet.GetRange("D13", "D13");
            IVTRange rangeStyle2 = firstSheet.GetRange("D17", "D17");
            IVTRange rangeStyle3 = firstSheet.GetRange("D14", "D14");

            int indexPupil = 0;
            int startRow = 8;
            int endV = P_TBM;

            foreach (PupilOfClassBO Pupil in listPupilOfClass)
            {
                //cot stt
                indexPupil++;
                sheet.SetCellValue(startRow, 1, indexPupil);
                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = convertPosition(startP - 1, startRow);
                string edP = convertPosition(startV - 1, startRow);
                string edV = string.Empty;// = convertPosition(P_TBM - 1, startRow);
                string HK = string.Empty;// convertPosition(P_HK1 - 2, startRow);
                string cntMark = string.Empty;
                if (strMarkTypePeriod.Contains("HK"))
                {
                    edV = convertPosition(P_TBM - 2, startRow);
                    HK = convertPosition(P_TBM - 1, startRow);
                    cntMark = "=COUNTA(" + stM + ":" + HK + ")";
                }
                else
                {
                    edV = convertPosition(P_TBM - 1, startRow);
                    cntMark = "=COUNTA(" + stM + ":" + edV + ")";
                }
                sheet.SetFormulaValue(startRow, 2, cntMark);

                //ma hoc sinh
                sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                //ho va ten
                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                }

                //Copy row style
                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                    Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED || listExemptedSubject.Any(u => u.PupilID == Pupil.PupilID))
                {
                    if ((indexPupil - 1) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle1, startRow);
                    }
                    if ((indexPupil) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle2, startRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle3, startRow);
                    }
                    sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                    sheet.Lock(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                    // Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                    DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                    DateTime? endDate = Pupil.LeavingDate;

                    bool showPupilData = false;

                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                        {
                            showPupilData = true;
                        }
                    }
                    else
                    {
                        showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                    }

                    if (!showPupilData)
                    {
                        sheet.GetRange(startRow, P_TBM, startRow, P_Nhan_xet).FillColor(Color.Yellow);
                        startRow++;
                        continue;
                    }
                }

                if (listExemptedSubject.Any(u => u.PupilID == Pupil.PupilID))
                {
                    int lockCol = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 3 : 4;
                    sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + lockCol);
                }

                //Lay diem tung dau diem hoc sinh
                int col = 5;
                int j = 0;

                IEnumerable<JudgeRecordBO> listJudgeRecordPupil = listJudgeRecord.Where(o => o.PupilID == Pupil.PupilID);
                SummedUpRecordBO lstSummedUpRecord = listSummedUpRecord.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault();

                for (int i = startM; i < startP; i++)
                {
                    String titleMark = listM[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }
                j = 0;

                for (int i = startP; i < startV; i++)
                {
                    String titleMark = listP[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);

                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }

                if (strMarkTypePeriod.Contains("HK"))
                {
                    endV = P_TBM - 1;
                }
                j = 0;
                for (int i = startV; i < endV; i++)
                {
                    String titleMark = listV[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }

                // hoc ki
                if (strMarkTypePeriod.Contains("HK"))
                {
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals("HK")).OrderByDescending(u => u.CreatedDate).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);

                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                }
                // Fill giá trị TBM để so sánh
                sheet.SetCellValue(startRow, 25, lstSummedUpRecord != null ? lstSummedUpRecord.JudgementResult : string.Empty);
                //set fomular for TBM
                string TBM = string.Empty;
                bool isClassification = acaYear.IsClassification.HasValue && acaYear.IsClassification.Value;
                if (strMarkTypePeriod.Contains("HK"))
                {
                    if (isClassification)
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") * 3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0;\"Đ\";\"CĐ\");\"\")";
                    }
                    else
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                    }

                }
                else
                {
                    if (isClassification)
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + edV + ";" + "\"" + "Đ" + "\"" + ") * 3 - COUNTA(" + stM + ":" + edV + ")" + "*2 >=0;\"Đ\";\"CĐ\");\"\")";
                    }
                    else
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + edV + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + edV + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                    }

                }
                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).FillColor(Color.Yellow);

                // Fill cột ghi chú
                string note = string.Empty;
                note = "=IF(AND(Y" + startRow + "<>\"\"," + convertPosition(P_TBM, startRow) + "<>\"\")," + "IF(Y" + startRow + "<>" + convertPosition(P_TBM, startRow) + ",\"HLM trong file excel khác HLM trong hệ thống\",\"\"), \"\")";
                sheet.SetFormulaValue(startRow, P_Nhan_xet, note);
                sheet.GetRange(startRow, P_Nhan_xet, startRow, P_Nhan_xet).FillColor(Color.Yellow);
                startRow++;
            }
            sheet.SetColumnWidth(25, 0);
            sheet.MergeRow(4, 1, P_TBM);
            sheet.SetCellValue("A4", title);
            sheet.MergeRow(5, 1, P_TBM);
            sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());

            sheet.GetRange(8, 1, 8 + totalPupil - 1, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            for (int i = 0; i < totalPupil; i++)
            {
                if (i % 5 == 0)
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                }
                else
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_Nhan_xet).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                }
            }

            if (totalPupil < 5)
            {
                for (int i = 8 + totalPupil; i < 13; i++)
                {
                    sheet.DeleteRow(8 + totalPupil);
                }
            }
            sheet.GetRange(8 + totalPupil, 1, 8 + totalPupil, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

            sheet.SetColumnWidth(2, 0);
            sheet.Name = Utils.StripVNSignAndSpace(objCSBO.SubjectName);
            sheet.SetFontName("Times New Roman", 11);
            sheet.ProtectSheet();
        }

        private void SetDataToExportAnySubject(IVTWorksheet sheet, IVTWorksheet emptySheet, IVTWorksheet firstSheet, IDictionary<string, object> dic, string strLockMarkType
                                                , ClassSubjectBO objCSBO
                                                , List<PupilOfClassBO> listPupilOfClass, List<MarkRecordBO> listMarkRecord, List<ExemptedSubject> listExemptedSubject
                                                , List<SummedUpRecordBO> listSummedUpRecord, List<int> lstSubjectID, List<ClassSubjectBO> lstSubjectPlus, int sheetIndex, ref List<VTDataValidation> lstValidation
                                                , bool? isAdmin = null)
        {
            #region xac dinh so dau diem
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");
            int PeriodID = Utils.GetInt(dic, "PeriodID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> lstCreaseID = lstSubjectPlus.Select(p => p.SubjectID).ToList();
            string PeriodName = Utils.GetString(dic, "PeriodName");
            string ClassName = Utils.GetString(dic, "ClassName");
            int typeExportID = Utils.GetInt(dic, "TypeExport");
            int SubjectID = objCSBO.SubjectID;
            string strMarkTypePeriod = this.GetMarkTitle(PeriodID) + ",";
            bool isInCrease = objCSBO.SubjectIDCrease > 0;
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            List<MarkRecordBO> lstMarkRecordTmp = new List<MarkRecordBO>();
            List<ExemptedSubject> lstExemptedSubjectTmp = new List<ExemptedSubject>();
            List<SummedUpRecordBO> lstSummedUpRecordTmp = new List<SummedUpRecordBO>();

            strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
            lstMarkRecordTmp = listMarkRecord.Where(p => p.SubjectID == SubjectID).ToList();
            lstExemptedSubjectTmp = listExemptedSubject.Where(p => p.SubjectID == SubjectID).ToList();
            lstSummedUpRecordTmp = listSummedUpRecord.Where(p => p.SubjectID == SubjectID).ToList();

            //list diem mon tang cuong
            SummedUpRecordBO objSummedUpRecordInCrease = new SummedUpRecordBO();
            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheet.CopyPasteSameSize(range, "E6");

            int cntM = 0, cntP = 0, cntV = 0;
            int startM = 5;
            int startP = 0;
            int startV = 0;

            #region Tiêu đề template
            //Change header

            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);
            bool isGDCDSubject = subjectCat.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            String title = "BẢNG ĐIỂM MÔN ";
            title += subjectCat.DisplayName.ToUpper() + " ";
            string tmp = string.Empty;
            if (!Utils.StripVNSign(PeriodName).ToUpper().Contains("DOT"))
            {
                tmp = "ĐỢT " + PeriodName;
            }
            else
            {
                tmp = PeriodName;
            }

            title += tmp;
            string SemesterName = ReportUtils.ConvertSemesterForReportName(Semester);
            title += " " + SemesterName + " ";
            title += " LỚP ";
            title += ClassName.ToUpper();

            sheet.SetCellValue("A2", SchoolProfileBusiness.Find(SchoolID).SchoolName.ToUpper());
            #endregion

            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;
                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }
            #endregion

            startP = startM + cntM;
            startV = startP + cntP;
            int cntPupil = listPupilOfClass.Count();
            int k = 0;
            startCol = 5;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                sheet.SetCellValue(7, startCol - 1, listM[k]);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                sheet.SetCellValue(7, startCol - 1, listP[k]);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                sheet.SetCellValue(7, startCol - 1, listV[k]);
                k++;
            }

            bool chkHK = strMarkTypePeriod.Contains("HK");


            int P_KTHK = startCol;
            //copy cot KTHK
            if (chkHK)
            {
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetColumnWidth(P_KTHK, 9);
                sheet.SetCellValue(6, P_KTHK, "KTHK");
                sheet.GetRange(6, P_KTHK, 7, P_KTHK).Merge();
            }
            int P_TBMTC = 0;
            if (isInCrease)
            {
                P_TBMTC = startCol;
                copyCol(firstSheet, sheet, 10, startCol);
                sheet.SetCellValue(6, P_TBMTC, "TBMTC");
            }

            int P_TBM_TMP = startCol;
            //copy cot TBM
            copyCol(firstSheet, sheet, 10, startCol);
            sheet.MergeColumn(P_TBM_TMP, 6, 7);
            sheet.SetCellValue(6, P_TBM_TMP, "TBM_TMP");
            sheet.HideColumn(P_TBM_TMP);

            int P_TBM = startCol;
            copyCol(firstSheet, sheet, 10, startCol);
            sheet.MergeColumn(P_TBM, 6, 7);
            sheet.SetCellValue(6, P_TBM, "TBM");

            int P_NhanXet = 0;
            //copy cot Nhan xet
            if (isGDCDSubject)
            {
                P_NhanXet = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetCellValue(6, P_NhanXet, "Nhận xét");
            }

            //Merger cot tieu de dau diem
            if (cntM > 0)
            {
                sheet.MergeRow(6, startM, startP - 1);
            }
            if (cntP > 0)
            {
                sheet.MergeRow(6, startP, startV - 1);
            }
            if (cntV > 0)
            {
                sheet.MergeRow(6, startV, startV + cntV - 1);
            }

            //set tieu de cho cac cot dau diem
            if (cntM > 0)
            {
                sheet.SetCellValue(6, startM, "Miệng");
            }
            if (cntP > 0)
            {
                sheet.SetCellValue(6, startP, "15p");
            }
            if (cntV > 0)
            {
                sheet.SetCellValue(6, startV, "1 tiết");
            }

            #region tao cac dong trang cho ds hs
            if (cntPupil > 5)
            {
                int numberGroupStudent = cntPupil / 5;
                if (cntPupil % 5 == 0)
                {
                    numberGroupStudent = numberGroupStudent - 1;
                }
                //IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_NhanXet);
                //for (int j = 1; j <= numberGroupStudent; j++)
                //{
                //    RowEnd = RowEnd + 5;
                //    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                //}
            }
            #endregion
            int indexPupil = 0;
            int startRow = 8;
            sheet.SetCellValue("B3", typeExportID);

            VTDataValidation objValidation = new VTDataValidation
            {
                FromColumn = startM,
                FromRow = 8,
                SheetIndex = sheetIndex,
                ToColumn = startP - 1,
                ToRow = 7 + cntPupil,
                Type = VTValidationType.INTEGER,
                FromValue = "0",
                ToValue = "10",
            };
            lstValidation.Add(objValidation);

            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                objValidation = new VTDataValidation
                {
                    FromColumn = startP,
                    FromRow = 8,
                    SheetIndex = sheetIndex,
                    ToColumn = startV + cntV - 1,
                    ToRow = 7 + cntPupil,
                    Type = VTValidationType.INTEGER,
                    FromValue = "0",
                    ToValue = "10"
                };
                lstValidation.Add(objValidation);
                objValidation = new VTDataValidation
                {
                    FromValue = "0",
                    ToValue = "10",
                    SheetIndex = sheetIndex,
                    Type = VTValidationType.DECIMAL,
                    FromColumn = startV + cntV,
                    ToColumn = startV + cntV,
                    FromRow = 8,
                    ToRow = cntPupil + 7,
                    MinLength = "0",
                    MaxLength = "3",
                };
                lstValidation.Add(objValidation);
            }
            else
            {
                objValidation = new VTDataValidation
                {
                    FromColumn = startP,
                    FromRow = 8,
                    SheetIndex = sheetIndex,
                    ToColumn = startV + cntV,
                    ToRow = 7 + cntPupil,
                    Type = VTValidationType.DECIMAL,
                    FromValue = "0",
                    ToValue = "10",
                    MinLength = "1",
                    MaxLength = "3",
                };
                lstValidation.Add(objValidation);
            }
            foreach (PupilOfClassBO Pupil in listPupilOfClass)
            {
                indexPupil++;
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignLeft);
                sheet.SetCellValue(startRow, 1, indexPupil);
                sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = "";
                if (stM != stP)
                    edM = convertPosition(startP - 1, startRow);
                else
                { edM = stM; }

                string edP = "";
                if (stP != stV)
                    edP = convertPosition(startV - 1, startRow);
                else
                {
                    edP = stP;
                }
                int endV = 0;
                if (cntV != 0)
                    endV = startV + cntV - 1;
                else
                    endV = startV;

                string edV = convertPosition(endV, startRow);
                // Vi tri diem hk
                string posHK = "";
                // He so
                string cntMark = "=0";
                // TBM
                string TBM_TMP = "";
                string TBM = "";
                //
                string TBMTC = "";
                if (chkHK)
                {
                    posHK = convertPosition(P_KTHK, startRow);
                    TBM_TMP = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((0";

                    if (cntM > 0)
                    {
                        cntMark += "+1*COUNTA(" + stM + ":" + edM + ")";
                        TBM_TMP += "+1*SUM(" + stM + ":" + edM + ")";

                    }
                    if (cntP > 0)
                    {
                        cntMark += "+1*COUNTA(" + stP + ":" + edP + ")";
                        TBM_TMP += "+1*SUM(" + stP + ":" + edP + ")";

                    }
                    if (cntV > 0)
                    {
                        cntMark += "+2*COUNTA(" + stV + ":" + edV + ") ";
                        TBM_TMP += "+2*SUM(" + stV + ":" + edV + ")";

                    }

                    cntMark += "+3*COUNTA(" + posHK + ":" + posHK + ")";
                    TBM_TMP += "+" + posHK + "*3)/(" + convertPosition(2, startRow) + ");1);\"\")";
                }
                //
                else
                {
                    TBM_TMP = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((0";

                    if (cntM > 0)
                    {
                        cntMark += "+1*COUNTA(" + stM + ":" + edM + ")";
                        TBM_TMP += "+1*SUM(" + stM + ":" + edM + ")";

                    }
                    if (cntP > 0)
                    {
                        cntMark += "+1*COUNTA(" + stP + ":" + edP + ")";
                        TBM_TMP += "+1*SUM(" + stP + ":" + edP + ")";

                    }
                    if (cntV > 0)
                    {
                        cntMark += "+2*COUNTA(" + stV + ":" + edV + ") ";
                        TBM_TMP += "+2*SUM(" + stV + ":" + edV + ")";

                    }
                    TBM_TMP += ")" + "/(" + convertPosition(2, startRow) + ");1);\"\")";
                }
                //fill TBM
                sheet.SetFormulaValue(startRow, 2, cntMark);
                sheet.SetFormulaValue(startRow, P_TBM_TMP, TBM_TMP);

                if (isInCrease)
                {
                    TBM = "=IF(AND(" + convertPosition(P_TBMTC, startRow) + "<>\"\"," + convertPosition(P_TBM_TMP, startRow) + "<>\"\"),";
                    TBM += "IF(" + convertPosition(P_TBM_TMP, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(";
                    TBM += convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0)))>10,10,";
                    TBM += convertPosition(P_TBM_TMP, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(";
                    TBM += convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0))))," + convertPosition(P_TBM_TMP, startRow) + ")";
                    sheet.SetFormulaValue(startRow, P_TBM, TBM);
                }
                else
                {
                    sheet.SetFormulaValue(startRow, P_TBM, TBM_TMP);
                }
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).FillColor(Color.Yellow);
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).IsLock = true; ;
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).SetFontStyle(false, null, false, 11, false, false);

                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                IVTRange rangeStyle1 = firstSheet.GetRange("D13", "D13");
                IVTRange rangeStyle2 = firstSheet.GetRange("D17", "D17");
                IVTRange rangeStyle3 = firstSheet.GetRange("D14", "D14");
                //xoa column thua neu co
                IVTRange emptyRange = firstSheet.GetRange("M6", "Q12");
                sheet.CopyPaste(emptyRange, 6, startCol);
                //end

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    if (chkHK)
                    {
                        sheet.GetRange(startRow, startM, startRow, P_KTHK).IsLock = false;
                    }
                    else
                    {
                        sheet.GetRange(startRow, startM, startRow, endV).IsLock = false;
                    }

                }

                //Copy row style
                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    if (isGDCDSubject)
                    {
                        sheet.GetRange(startRow, 1, startRow, P_NhanXet).FillColor(Color.Yellow);
                    }
                    else
                    {
                        sheet.GetRange(startRow, 1, startRow, P_TBM).FillColor(Color.Yellow);
                        sheet.GetRange(startRow, 1, startRow, P_TBM).IsLock = true;
                    }
                }

                int col = 5;
                int j = 0;
                // Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;
                DateTime? endDate = Pupil.LeavingDate;

                bool showPupilData = false;

                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                    {
                        showPupilData = true;
                    }
                }
                else
                {
                    showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                    || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                }

                List<MarkRecordBO> lstMarkOfPupil = lstMarkRecordTmp.Where(o => o.PupilID == Pupil.PupilID).ToList();
                if (showPupilData)
                {
                    for (int i = startM; i < startP; i++)
                    {
                        String titleMark = listM[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    j = 0;
                    for (int i = startP; i < startV; i++)
                    {
                        String titleMark = listP[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    j = 0;
                    for (int i = startV; i < startV + cntV; i++)
                    {
                        String titleMark = listV[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        sheet.GetRange(startRow, col, startRow, col).SetFontStyle(false, null, false, 11, false, false);
                        col++;
                        j++;
                    }
                    //diem kiem tra hk
                    IEnumerable<MarkRecordBO> listMarkRecordMarkSemester = lstMarkOfPupil.Where(o => o.Title.Equals("HK"));
                    if (listMarkRecordMarkSemester != null && listMarkRecordMarkSemester.Count() > 0)
                    {
                        MarkRecordBO MarkRecord = listMarkRecordMarkSemester.FirstOrDefault();
                        sheet.SetCellValue(startRow, P_KTHK, MarkRecord.Mark);
                        //sheet.SetCellValue(startRow, 25, MarkRecord.Mark);
                        //sheet.SetCellValue(startRow, P_KTHK, "=IF(Y" + startRow + "=10, \"10\", Y" + startRow + ")");
                    }
                    if ((strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        sheet.GetRange(startRow, P_KTHK, startRow, P_KTHK).IsLock = true;
                    // Khoá các cột điểm, không cho nhập điểm trong excel đối với học sinh được miễn giảm
                    var ExemptType = lstExemptedSubjectTmp.Where(p => p.PupilID == Pupil.PupilID).FirstOrDefault();

                    if (ExemptType != null)
                    {
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            sheet.GetRange(startRow, 1, startRow, listMarkType.Count() + 3).FillColor(Color.Yellow);
                            sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + 3);
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, listMarkType.Count() + 4).FillColor(Color.Yellow);
                            sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                        }
                    }

                    if (isGDCDSubject)
                    {
                        sheet.GetRange(startRow, P_NhanXet, startRow, P_NhanXet).IsLock = false;
                        SummedUpRecordBO SummedUpRecord = listSummedUpRecord.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault();
                        if (SummedUpRecord != null)
                            sheet.SetCellValue(startRow, P_NhanXet, SummedUpRecord.Comment);
                    }

                    if (isInCrease)//fill TBM tang cuong
                    {
                        if (lstSubjectID.Contains(objCSBO.SubjectIDCrease))//KT neu co chon mon tang cuong de xuat excel thi fill cong thuc lay tu sheet fill ra
                        {
                            string SubjectNameTC = string.Empty;
                            string TC_Formular = "";
                            string Column_TC = convertPosition(P_TBMTC, startRow);
                            SubjectNameTC = lstSubjectPlus.Where(p => p.SubjectID == objCSBO.SubjectIDCrease).FirstOrDefault().SubjectName;
                            TC_Formular = Utils.StripVNSign(SubjectNameTC).Replace(" ", "");
                            TBMTC = "=IF(" + TC_Formular + "!" + Column_TC + "<>\"\"," + TC_Formular + "!" + Column_TC + ",\"\")";
                            sheet.SetFormulaValue(startRow, P_TBMTC, TBMTC);
                        }
                        else
                        {
                            objSummedUpRecordInCrease = listSummedUpRecord.Where(p => p.PupilID == Pupil.PupilID && p.SubjectID == objCSBO.SubjectIDCrease).FirstOrDefault();
                            sheet.SetCellValue(startRow, P_TBMTC, objSummedUpRecordInCrease != null ? objSummedUpRecordInCrease.SummedUpMark : null);
                        }
                        sheet.GetRange(startRow, P_TBMTC, startRow, P_TBMTC).FillColor(Color.Yellow);
                        sheet.GetRange(startRow, P_TBMTC, startRow, P_TBMTC).IsLock = true;
                        sheet.GetRange(startRow, P_TBMTC, startRow, P_TBMTC).SetFontStyle(false, null, false, 11, false, false);
                    }
                }
                startRow++;
            }
            if (isGDCDSubject)
            {
                P_TBM = P_NhanXet;
            }
            sheet.GetRange(8, 1, 8 + cntPupil - 1, P_TBM).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            for (int i = 0; i < cntPupil; i++)
            {
                if (i % 5 == 0)
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_TBM).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                }
                else
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_TBM).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                }
            }

            if (cntPupil < 5)
            {
                for (int i = 8 + cntPupil; i < 13; i++)
                {
                    sheet.DeleteRow(8 + cntPupil);
                }
            }
            sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_TBM).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet
            sheet.MergeRow(4, 1, P_TBM);
            sheet.GetRange(8, P_KTHK, 8 + cntPupil, P_TBM).NumberFormat("0.0");
            sheet.SetCellValue("A4", title);
            sheet.MergeRow(5, 1, P_TBM);
            sheet.SetCellValue("A5", "NĂM HỌC " + AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());
            sheet.SetColumnWidth(2, 0);
            sheet.SetFontName("Times New Roman", 11);
            sheet.Name = Utils.StripVNSignAndSpace(subjectCat.DisplayName);
            sheet.ProtectSheet();

        }

        public Stream ExportSubjectOfClass(IDictionary<string, object> dicParam, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null)
        {

            int SubjectID = Utils.GetInt(dicParam, "SubjectID");
            int SchoolID = Utils.GetInt(dicParam, "SchoolID");
            int PeriodID = Utils.GetInt(dicParam, "PeriodID");
            int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
            int Semester = Utils.GetInt(dicParam, "Semester");
            string strClassID = Utils.GetString(dicParam, "ArrClassID");
            int isCommenting = Utils.GetInt(dicParam, "isCommenting");
            int EducationLevelID = Utils.GetInt(dicParam, "EducationLevelID");
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            SubjectCat objSubjectCat = SubjectCatBusiness.Find(SubjectID);
            string SubjectName = objSubjectCat.SubjectName;
            bool isNotShowPupil = objAcademicYear.IsShowPupil.HasValue && objAcademicYear.IsShowPupil.Value;

            string periodName = PeriodDeclarationBusiness.Find(PeriodID).Resolution;
            List<int> lstClassID = strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            int ClassID = 0;
            string reportCode = "";
            reportCode = SystemParamsInFile.BANGDIEMTHEODOT;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            dicParam.Add("PeriodName", periodName);
            dicParam.Add("SubjectName", SubjectName);
            dicParam.Add("IsCommenting", objSubjectCat.IsCommenting);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet emptySheet = oBook.GetSheet(2);
            IVTWorksheet sheet = oBook.GetSheet(3);

            IVTWorksheet firstSheetJudge = oBook.GetSheet(4);
            IVTWorksheet sheetJudge = oBook.CopySheetToLast(firstSheet, "D17");

            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheetJudge.CopyPasteSameSize(range, "E6");
            IVTWorksheet sheettmp = null;


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            //dic["Check"] = "check";
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).Where(p => lstClassID.Contains(p.ClassID))
                .Select(o => new PupilOfClassBO
                {
                    PupilID = o.PupilID,
                    PupilCode = o.PupilProfile.PupilCode,
                    PupilFullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name,
                    Status = o.Status,
                    OrderInClass = o.OrderInClass,
                    ClassID = o.ClassID,
                    EndDate = o.EndDate
                });
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            List<PupilOfClassBO> listPupilOfClass = iquery.ToList().OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            string strLockMarkType = string.Empty;
            string strMarkTypePeriod = this.GetMarkTitle(PeriodID) + ",";
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["SchoolID"] = SchoolID;
            search["Year"] = objAcademicYear.Year;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["AcademicYearID"] = objAcademicYear.AcademicYearID;
            if (isAdmin.HasValue && isAdmin.Value == true)
            {
                search["EducationLevelID"] = EducationLevelID;
            }
            List<MarkRecordBO> listMarkRecord = VMarkRecordBusiness.SearchMarkHistoryOrNoHistory(search).Where(o => (strMarkTypePeriod.Contains(o.Title + ",") && lstClassID.Contains(o.ClassID.Value))).ToList();

            search["checkWithClassMovement"] = "1";
            List<JudgeRecordBO> listJudgeRecord = VJudgeRecordBusiness.SearchJudgeRecordNoView(search).Where(u => strMarkTypePeriod.Contains(u.Title)).ToList();

            search = new Dictionary<string, object>();
            search["SchoolID"] = SchoolID;
            search["Year"] = objAcademicYear.Year;
            //search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            search["lstClassID"] = lstClassID;
            search["AcademicYearID"] = objAcademicYear.AcademicYearID;
            //search["SubjectID"] = SubjectID;
            if (isAdmin.HasValue && isAdmin.Value == true)
            {
                search["EducationLevelID"] = EducationLevelID;
            }
            List<SummedUpRecordBO> listSummedUpRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).ToList();
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(SchoolID,
                                                                        new Dictionary<string, object>()
                                                                        { {"AcademicYearID",AcademicYearID},
                                                                          {"SubjectID",SubjectID},
                                                                          {"SemesterID",Semester}
                                                                        }).ToList();
            dicParam.Add("strMarkTypePeriod", strMarkTypePeriod);
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            List<MarkRecordBO> listMRtmp = new List<MarkRecordBO>();
            List<JudgeRecordBO> listJRtmp = new List<JudgeRecordBO>();
            List<SummedUpRecordBO> listSURtmp = new List<SummedUpRecordBO>();
            List<ExemptedSubject> listEStmp = new List<ExemptedSubject>();
            List<SummedUpRecordBO> listSURtmpTC = new List<SummedUpRecordBO>();

            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["EducationLevelID"] = EducationLevelID;
            search["ListClassID"] = lstClassID;
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, search).ToList();
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            string[] cd = new string[] { GlobalConstants.SHORT_OK, GlobalConstants.SHORT_NOT_OK };
            ClassSubject objClassSubject = null;
            ClassSubject objCSTC = null;
            bool isIncrease = false;

            if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
            {
                for (int i = 0; i < lstClassID.Count; i++)
                {
                    isIncrease = false;
                    sheettmp = oBook.CopySheetToLast(sheet);
                    ClassID = lstClassID[i];
                    strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
                    lstPOCtmp = listPupilOfClass.Where(p => p.ClassID == ClassID).ToList();
                    listMRtmp = listMarkRecord.Where(p => p.ClassID == ClassID).ToList();
                    listEStmp = listExemptedSubject.Where(p => p.ClassID == ClassID).ToList();
                    listSURtmp = listSummedUpRecord.Where(p => p.ClassID == ClassID && p.SubjectID == SubjectID).ToList();
                    objClassSubject = lstClassSubject.Where(p => p.ClassID == ClassID && p.SubjectID == SubjectID).FirstOrDefault();
                    if (objClassSubject != null && objClassSubject.SubjectIDIncrease > 0)
                    {
                        objCSTC = lstClassSubject.Where(p => p.ClassID == ClassID && p.SubjectID == objClassSubject.SubjectIDIncrease).FirstOrDefault();
                        if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            isIncrease = objCSTC != null && objCSTC.SectionPerWeekFirstSemester > 0;
                        }
                        else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            isIncrease = objCSTC != null && objCSTC.SectionPerWeekSecondSemester > 0;
                        }
                        listSURtmpTC = listSummedUpRecord.Where(p => p.ClassID == ClassID && p.SubjectID == objClassSubject.SubjectIDIncrease).ToList();
                    }
                    this.SetDataExportSubjectOfClass(sheettmp, emptySheet, firstSheet, dicParam, ClassID, strLockMarkType, lstPOCtmp, listMRtmp, listEStmp, listSURtmp, listSURtmpTC, objCSTC, isIncrease, i + 1, ref lstValidation, dicDisplay, isAdmin);
                }
            }
            else
            {
                for (int i = 0; i < lstClassID.Count; i++)
                {
                    sheettmp = oBook.CopySheetToLast(sheetJudge);
                    ClassID = lstClassID[i];
                    strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
                    lstPOCtmp = listPupilOfClass.Where(p => p.ClassID == ClassID).ToList();
                    listJRtmp = listJudgeRecord.Where(p => p.ClassID == ClassID).ToList();
                    listEStmp = listExemptedSubject.Where(p => p.ClassID == ClassID).ToList();
                    listSURtmp = listSummedUpRecord.Where(p => p.ClassID == ClassID).ToList();
                    this.SetDataExportSubjectOfClassJudge(sheettmp, firstSheetJudge, dicParam, ClassID, strLockMarkType, lstPOCtmp, listJRtmp, listEStmp, listSURtmp, objAcademicYear, cd, i + 1, ref lstValidation, isAdmin);
                }
            }

            firstSheet.Delete();
            firstSheetJudge.Delete();
            sheetJudge.Delete();
            emptySheet.Delete();
            sheet.Delete();
            if (lstClassID.Count == 1)
            {
                string className = ClassProfileBusiness.Find(lstClassID.FirstOrDefault()).DisplayName;
                FileName = string.Format("BangDiemMon{0}_{1}_{2}.xls", Utils.StripVNSignAndSpace(SubjectName), Utils.StripVNSignAndSpace(className), Utils.StripVNSignAndSpace(periodName));
            }
            else
            {
                FileName = string.Format("BangDiemMon{0}_Caclop_{1}.xls", Utils.StripVNSignAndSpace(SubjectName), Utils.StripVNSignAndSpace(periodName));
            }
            return oBook.ToStreamValidationData(lstValidation);
        }

        private void SetDataExportSubjectOfClass(IVTWorksheet sheet, IVTWorksheet emptySheet, IVTWorksheet firstSheet, IDictionary<string, object> dic, int ClassID, string strLockMarkType
                                                , List<PupilOfClassBO> listPupilOfClass, List<MarkRecordBO> listMarkRecord, List<ExemptedSubject> listExemptedSubject
                                                , List<SummedUpRecordBO> listSummedUpRecord, List<SummedUpRecordBO> listSummedUpRecordTC, ClassSubject objClassSubject, bool isInCrease
                                                , int sheetIndex, ref List<VTDataValidation> lstValidation, Dictionary<int, bool> dicDisplay = null
                                                , bool? isAdmin = null)
        {
            #region xac dinh so dau diem
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");
            int PeriodID = Utils.GetInt(dic, "PeriodID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            string PeriodName = Utils.GetString(dic, "PeriodName");
            int isCommenting = Utils.GetInt(dic, "IsCommenting");
            string SubjectName = Utils.GetString(dic, "SubjectName");
            string ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            string strMarkTypePeriod = this.GetMarkTitle(PeriodID) + ",";
            SummedUpRecordBO objSUR_TC = null;
            IVTRange range = emptySheet.GetRange("E6", "H17");
            sheet.CopyPasteSameSize(range, "E6");
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);

            int cntM = 0, cntP = 0, cntV = 0;
            int startM = 5;
            int startP = 0;
            int startV = 0;

            #region Tiêu đề template
            //Change header
            bool isGDCDSubject = isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            String title = "BẢNG ĐIỂM MÔN ";
            title += SubjectName.ToUpper() + " ";

            string tmp = string.Empty;
            if (!Utils.StripVNSign(PeriodName).ToUpper().Contains("DOT"))
            {
                tmp = "ĐỢT " + PeriodName;
            }
            else
            {
                tmp = PeriodName;
            }

            title += tmp;
            string SemesterName = ReportUtils.ConvertSemesterForReportName(Semester);
            title += " " + SemesterName + " ";
            title += " LỚP ";
            title += ClassName.ToUpper();

            sheet.SetCellValue("A2", SchoolProfileBusiness.Find(SchoolID).SchoolName.ToUpper());
            #endregion

            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;
                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }
            #endregion

            startP = startM + cntM;
            startV = startP + cntP;
            int cntPupil = listPupilOfClass.Count();
            int k = 0;
            startCol = 5;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                sheet.SetCellValue(7, startCol - 1, listM[k]);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                sheet.SetCellValue(7, startCol - 1, listP[k]);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                sheet.SetCellValue(7, startCol - 1, listV[k]);
                k++;
            }



            int P_KTHK = startCol;
            bool chkKTHK = strMarkTypePeriod.Contains("HK");
            //copy cot KTHK
            if (chkKTHK)
            {
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetColumnWidth(P_KTHK, 9);
                sheet.SetCellValue(6, P_KTHK, "KTHK");
                sheet.GetRange(6, P_KTHK, 7, P_KTHK).Merge();
            }
            int P_TBMTC = 0;
            if (isInCrease)
            {
                P_TBMTC = startCol;
                copyCol(firstSheet, sheet, 10, startCol);
                sheet.MergeColumn(P_TBMTC, 6, 7);
                sheet.SetCellValue(6, P_TBMTC, "TBMTC");
            }


            int P_TBM_TMP = startCol;
            //copy cot TBM
            copyCol(firstSheet, sheet, 10, startCol);
            sheet.MergeColumn(P_TBM_TMP, 6, 7);
            sheet.SetCellValue(6, P_TBM_TMP, "TBM_TMP");

            int P_TBM = startCol;
            copyCol(firstSheet, sheet, 10, startCol);
            sheet.MergeColumn(P_TBM, 6, 7);
            sheet.SetCellValue(6, P_TBM, "TBM");

            int P_NhanXet = P_TBM;
            //copy cot Nhan xet
            if (isGDCDSubject)
            {
                P_NhanXet = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
                sheet.SetCellValue(6, P_NhanXet, "Nhận xét");
            }

            //Merger cot tieu de dau diem
            if (cntM > 0)
            {
                sheet.MergeRow(6, startM, startP - 1);
            }
            if (cntP > 0)
            {
                sheet.MergeRow(6, startP, startV - 1);
            }
            if (cntV > 0)
            {
                sheet.MergeRow(6, startV, startV + cntV - 1);
            }

            //set tieu de cho cac cot dau diem
            if (cntM > 0)
            {
                sheet.SetCellValue(6, startM, "Miệng");
            }
            if (cntP > 0)
            {
                sheet.SetCellValue(6, startP, "15p");
            }
            if (cntV > 0)
            {
                sheet.SetCellValue(6, startV, "1 tiết");
            }
            VTDataValidation objValidation = new VTDataValidation
            {
                FromColumn = startM,
                FromRow = 8,
                SheetIndex = sheetIndex,
                ToColumn = startP - 1,
                ToRow = 7 + cntPupil,
                Type = VTValidationType.INTEGER,
                FromValue = "0",
                ToValue = "10",
            };
            lstValidation.Add(objValidation);

            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                objValidation = new VTDataValidation
                {
                    FromColumn = startP,
                    FromRow = 8,
                    SheetIndex = sheetIndex,
                    ToColumn = startV + cntV - 1,
                    ToRow = 7 + cntPupil,
                    Type = VTValidationType.INTEGER,
                    FromValue = "0",
                    ToValue = "10"
                };
                lstValidation.Add(objValidation);
                objValidation = new VTDataValidation
                {
                    FromValue = "0",
                    ToValue = "10",
                    SheetIndex = sheetIndex,
                    Type = VTValidationType.DECIMAL,
                    FromColumn = startV + cntV,
                    ToColumn = startV + cntV,
                    FromRow = 8,
                    ToRow = cntPupil + 7,
                    MinLength = "0",
                    MaxLength = "3",
                };
                lstValidation.Add(objValidation);
            }
            else
            {
                objValidation = new VTDataValidation
                {
                    FromColumn = startP,
                    FromRow = 8,
                    SheetIndex = sheetIndex,
                    ToColumn = startV + cntV,
                    ToRow = 7 + cntPupil,
                    Type = VTValidationType.DECIMAL,
                    FromValue = "0",
                    ToValue = "10",
                    MinLength = "1",
                    MaxLength = "3",
                };
                lstValidation.Add(objValidation);
            }
            #region tao cac dong trang cho ds hs
            int RowEnd = 8;
            if (cntPupil > 5)
            {
                int numberGroupStudent = cntPupil / 5;
                if (cntPupil % 5 == 0)
                {
                    numberGroupStudent = numberGroupStudent - 1;
                }
                IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_NhanXet);
                for (int j = 1; j <= numberGroupStudent; j++)
                {
                    RowEnd = RowEnd + 5;
                    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                }
            }
            #endregion
            int indexPupil = 0;
            int startRow = 8;
            sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID);//dung de phan biet xuat cac mon hay cac lop phuc vu luong Import
            foreach (PupilOfClassBO Pupil in listPupilOfClass)
            {
                indexPupil++;
                sheet.SetCellValue(startRow, 1, indexPupil.ToString());

                sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();
                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = "";
                if (stM != stP)
                    edM = convertPosition(startP - 1, startRow);
                else
                { edM = stM; }

                string edP = "";
                if (stP != stV)
                    edP = convertPosition(startV - 1, startRow);
                else
                {
                    edP = stP;
                }
                int endV = 0;
                if (cntV != 0)
                    endV = startV + cntV - 1;
                else
                    endV = startV;

                string edV = convertPosition(endV, startRow);
                // Vi tri diem hk
                string posHK = "";
                // He so
                string cntMark = "=0";
                // TBM
                string TBM_TMP = "";

                string TBM = "";

                if (chkKTHK)
                {
                    posHK = convertPosition(P_KTHK, startRow);
                    TBM_TMP = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((0";

                    if (cntM > 0)
                    {
                        cntMark += "+1*COUNTA(" + stM + ":" + edM + ")";
                        TBM_TMP += "+1*SUM(" + stM + ":" + edM + ")";

                    }
                    if (cntP > 0)
                    {
                        cntMark += "+1*COUNTA(" + stP + ":" + edP + ")";
                        TBM_TMP += "+1*SUM(" + stP + ":" + edP + ")";

                    }
                    if (cntV > 0)
                    {
                        cntMark += "+2*COUNTA(" + stV + ":" + edV + ") ";
                        TBM_TMP += "+2*SUM(" + stV + ":" + edV + ")";

                    }

                    cntMark += "+3*COUNTA(" + posHK + ":" + posHK + ")";
                    TBM_TMP += "+" + posHK + "*3)/(" + convertPosition(2, startRow) + ");1);\"\")";
                }
                //
                else
                {
                    TBM_TMP = "=IF(" + convertPosition(2, startRow) + ">0;ROUND((0";

                    if (cntM > 0)
                    {
                        cntMark += "+1*COUNTA(" + stM + ":" + edM + ")";
                        TBM_TMP += "+1*SUM(" + stM + ":" + edM + ")";

                    }
                    if (cntP > 0)
                    {
                        cntMark += "+1*COUNTA(" + stP + ":" + edP + ")";
                        TBM_TMP += "+1*SUM(" + stP + ":" + edP + ")";

                    }
                    if (cntV > 0)
                    {
                        cntMark += "+2*COUNTA(" + stV + ":" + edV + ") ";
                        TBM_TMP += "+2*SUM(" + stV + ":" + edV + ")";

                    }
                    TBM_TMP += ")" + "/(" + convertPosition(2, startRow) + ");1);\"\")";
                }
                sheet.SetFormulaValue(startRow, 2, cntMark);
                sheet.SetFormulaValue(startRow, P_TBM_TMP, TBM_TMP);

                if (isInCrease)
                {
                    TBM = "=IF(AND(" + convertPosition(P_TBMTC, startRow) + "<>\"\"," + convertPosition(P_TBM_TMP, startRow) + "<>\"\"),";
                    TBM += "IF(" + convertPosition(P_TBM_TMP, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(";
                    TBM += convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0)))>10,10,";
                    TBM += convertPosition(P_TBM_TMP, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(";
                    TBM += convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0))))," + convertPosition(P_TBM_TMP, startRow) + ")";
                    sheet.SetFormulaValue(startRow, P_TBM, TBM);
                }
                else
                {
                    sheet.SetFormulaValue(startRow, P_TBM, TBM_TMP);
                }
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).FillColor(Color.Yellow);
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).IsLock = true;
                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                IVTRange rangeStyle1 = firstSheet.GetRange("D13", "D13");
                IVTRange rangeStyle2 = firstSheet.GetRange("D17", "D17");
                IVTRange rangeStyle3 = firstSheet.GetRange("D14", "D14");
                //xoa column thua neu co
                IVTRange emptyRange = firstSheet.GetRange("M6", "Q12");
                sheet.CopyPaste(emptyRange, 6, startCol);
                //end

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    if (chkKTHK)
                    {
                        sheet.GetRange(startRow, startM, startRow, P_KTHK).IsLock = false;
                    }
                    else
                    {
                        sheet.GetRange(startRow, startM, startRow, endV).IsLock = false;
                    }

                }

                //Copy row style
                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    if (isGDCDSubject)
                    {
                        sheet.GetRange(startRow, 1, startRow, P_NhanXet).FillColor(Color.Yellow);
                    }
                    else
                    {
                        sheet.GetRange(startRow, 1, startRow, P_TBM).FillColor(Color.Yellow);
                    }
                }

                int col = 5;
                int j = 0;
                // Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;
                DateTime? endDate = Pupil.EndDate;

                bool showPupilData = false;

                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                    {
                        showPupilData = true;
                    }
                }
                else
                {
                    showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                    || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                }

                List<MarkRecordBO> lstMarkOfPupil = listMarkRecord.Where(o => o.PupilID == Pupil.PupilID).ToList();
                if (showPupilData)
                {
                    for (int i = startM; i < startP; i++)
                    {
                        String titleMark = listM[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    j = 0;
                    for (int i = startP; i < startV; i++)
                    {
                        String titleMark = listP[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    j = 0;
                    for (int i = startV; i < startV + cntV; i++)
                    {
                        String titleMark = listV[j];
                        IEnumerable<MarkRecordBO> listMarkRecordItem = lstMarkOfPupil.Where(o => o.Title.Equals(titleMark));
                        if (listMarkRecordItem != null && listMarkRecordItem.Count() > 0)
                        {
                            MarkRecordBO MarkRecord = listMarkRecordItem.FirstOrDefault();
                            sheet.SetCellValue(startRow, col, MarkRecord.Mark);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, col, "");
                        }
                        if ((strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        {
                            sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                        }
                        col++;
                        j++;
                    }
                    //diem kiem tra hk
                    IEnumerable<MarkRecordBO> listMarkRecordMarkSemester = lstMarkOfPupil.Where(o => o.Title.Equals("HK"));
                    if (listMarkRecordMarkSemester != null && listMarkRecordMarkSemester.Count() > 0)
                    {
                        MarkRecordBO MarkRecord = listMarkRecordMarkSemester.FirstOrDefault();
                        sheet.SetCellValue(startRow, P_KTHK, MarkRecord.Mark);
                        //sheet.SetCellValue(startRow, 25, MarkRecord.Mark);
                        //sheet.SetCellValue(startRow, P_KTHK, "=IF(Y" + startRow + "=10, \"10\", Y" + startRow + ")");
                    }
                    if ((strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        sheet.GetRange(startRow, P_KTHK, startRow, P_KTHK).IsLock = true;
                    // Khoá các cột điểm, không cho nhập điểm trong excel đối với học sinh được miễn giảm
                    var ExemptType = listExemptedSubject.Where(p => p.PupilID == Pupil.PupilID).FirstOrDefault();

                    if (ExemptType != null)
                    {
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            sheet.GetRange(startRow, 1, startRow, listMarkType.Count() + 3).FillColor(Color.Yellow);
                            sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + 3);
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, listMarkType.Count() + 4).FillColor(Color.Yellow);
                            sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                        }
                    }

                    //
                    if (isInCrease)
                    {
                        objSUR_TC = listSummedUpRecordTC.Where(p => p.PupilID == Pupil.PupilID).FirstOrDefault();
                        sheet.SetCellValue(startRow, P_TBMTC, objSUR_TC != null ? objSUR_TC.SummedUpMark : null);
                        sheet.GetRange(startRow, P_TBMTC, startRow, P_TBMTC).FillColor(Color.Yellow);
                        sheet.GetRange(startRow, P_TBMTC, startRow, P_TBMTC).IsLock = true;
                    }

                    if (isGDCDSubject)
                    {
                        sheet.GetRange(startRow, P_NhanXet, startRow, P_NhanXet).IsLock = false;
                        SummedUpRecordBO SummedUpRecord = listSummedUpRecord.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault();
                        if (SummedUpRecord != null)
                            sheet.SetCellValue(startRow, P_NhanXet, SummedUpRecord.Comment);
                    }
                }
                startRow++;
            }
            if (isGDCDSubject)
            {
                P_TBM = P_NhanXet;
            }
            sheet.GetRange(8, 1, 8 + cntPupil - 1, P_TBM).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            for (int i = 0; i < cntPupil; i++)
            {
                if (i % 5 == 0)
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_TBM).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                }
                else
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_TBM).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                }
            }

            if (cntPupil < 5)
            {
                for (int i = 8 + cntPupil; i < 13; i++)
                {
                    sheet.DeleteRow(8 + cntPupil);
                }
            }
            sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_TBM).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet
            sheet.SetColumnWidth(P_TBM_TMP, 0);
            sheet.GetRange(8, P_KTHK, 8 + cntPupil, P_TBM).NumberFormat("0.0");
            sheet.MergeRow(4, 1, P_TBM);
            sheet.SetCellValue("A4", title);
            sheet.MergeRow(5, 1, P_TBM);
            sheet.SetCellValue("A5", "NĂM HỌC " + AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());
            sheet.SetColumnWidth(2, 0);
            sheet.Name = Utils.StripVNSignAndSpace(ClassName);
            sheet.SetFontName("Times New Roman", 11);
            sheet.ProtectSheet();
        }

        private void SetDataExportSubjectOfClassJudge(IVTWorksheet sheet, IVTWorksheet firstSheet, IDictionary<string, object> dicParam, int ClassID, string strLockMarkType
                                                , List<PupilOfClassBO> listPupilOfClass, List<JudgeRecordBO> listJudgeRecord, List<ExemptedSubject> listExemptedSubject
                                                , List<SummedUpRecordBO> listSummedUpRecord, AcademicYear objAca, string[] cd, int sheetIndex, ref List<VTDataValidation> lstDataValidation
                                                , bool? isAdmin = null)
        {
            int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
            int Semester = Utils.GetInt(dicParam, "Semester");
            int PeriodID = Utils.GetInt(dicParam, "PeriodID");
            int SchoolID = Utils.GetInt(dicParam, "SchoolID");
            int SubjectID = Utils.GetInt(dicParam, "SubjectID");
            string PeriodName = Utils.GetString(dicParam, "PeriodName");
            string SubjectName = Utils.GetString(dicParam, "SubjectName");
            string ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            string strMarkTypePeriod = Utils.GetString(dicParam, "strMarkTypePeriod");
            VTDataValidation objValidation = new VTDataValidation();
            #region Tiêu đề template
            //Change header
            String title = "BẢNG ĐIỂM MÔN ";
            title += SubjectName.ToUpper() + " ";

            string tmp = string.Empty;

            if (!Utils.StripVNSignAndSpace(ClassName).ToUpper().Contains("LOP"))
            {
                tmp = "LỚP " + ClassName;
            }
            else
            {
                tmp = ClassName;
            }
            title += tmp;
            tmp = "";
            if (!Utils.StripVNSign(PeriodName).ToUpper().Contains("DOT"))
            {
                tmp = " ĐỢT " + PeriodName;
            }
            else
            {
                tmp = PeriodName;
            }
            title += tmp;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                title += " HKI ";
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                title += " HKII ";
            else
            {
                title += " CẢ NĂM ";
            }
            #endregion Tiêu đề template
            #region xac dinh so dau diem

            //Lấy con điểm bị khoá
            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];

            int cntM = 0, cntP = 0, cntV = 0;
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;
                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }

            #endregion xac dinh so dau diem
            int startM = 5;
            int startP = startM + cntM;
            int startV = startP + cntP;
            int k = 0;
            int P_TBM = 0;
            startCol = 5;
            for (int j = startM; j < startP; j++)
            {
                copyCol(firstSheet, sheet, 5, startCol);
                sheet.SetCellValue(7, startCol - 1, listM[k]);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(firstSheet, sheet, 6, startCol);
                sheet.SetCellValue(7, startCol - 1, listP[k]);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(firstSheet, sheet, 7, startCol);
                sheet.SetCellValue(7, startCol - 1, listV[k]);
                k++;
            }
            if (strMarkTypePeriod.Contains("HK"))
            {
                //coppy co hoc ki
                copyCol(firstSheet, sheet, 8, startCol);
                //sheet.SetCellValue(6, startCol-1, "HK");
            }

            //copy cot TBM
            P_TBM = startCol;
            copyCol(firstSheet, sheet, 9, startCol);
            // Cột nhận xét
            int P_Nhan_xet = startCol;
            copyCol(firstSheet, sheet, 10, startCol);
            //Merger cot tieu de dau diem
            if (startM != startP)
                sheet.MergeRow(6, startM, startP - 1);
            if (startP != startV)
                sheet.MergeRow(6, startP, startV - 1);
            sheet.MergeRow(6, startV, startV + cntV - 1);

            //set tieu de cho cac cot dau diem
            sheet.SetCellValue(6, startM, "Miệng");
            sheet.SetCellValue(6, startP, "15p");
            sheet.SetCellValue(6, startV, "1 tiết");

            int totalPupil = listPupilOfClass.Count;

            //tao cac dong trang cho ds hs
            int RowEnd = 8;
            if (totalPupil > 5)
            {
                int numberGroupStudent = totalPupil / 5;
                IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Nhan_xet);
                for (int j = 1; j <= numberGroupStudent; j++)
                {
                    RowEnd = RowEnd + 5;
                    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                }
            }

            objValidation = new VTDataValidation
            {
                Contrains = cd,
                FromColumn = startM,
                FromRow = 8,
                SheetIndex = sheetIndex,
                ToColumn = startV + cntV,
                ToRow = 7 + totalPupil,
                Type = VTValidationType.LIST
            };
            lstDataValidation.Add(objValidation);

            IVTRange rangeStyle1 = firstSheet.GetRange("D13", "D13");
            IVTRange rangeStyle2 = firstSheet.GetRange("D17", "D17");
            IVTRange rangeStyle3 = firstSheet.GetRange("D14", "D14");

            int indexPupil = 0;
            int startRow = 8;
            int endV = P_TBM;
            int cntPupil = listPupilOfClass.Count();
            sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID);//dung de phan biet xuat cac mon hay cac lop phuc vu luong Import)
            foreach (PupilOfClassBO Pupil in listPupilOfClass)
            {
                //cot stt
                indexPupil++;
                sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = convertPosition(startP - 1, startRow);
                string edP = convertPosition(startV - 1, startRow);
                string edV = string.Empty;// = convertPosition(P_TBM - 1, startRow);
                string HK = string.Empty;// convertPosition(P_HK1 - 2, startRow);
                string cntMark = string.Empty;
                if (strMarkTypePeriod.Contains("HK"))
                {
                    edV = convertPosition(P_TBM - 2, startRow);
                    HK = convertPosition(P_TBM - 1, startRow);
                    cntMark = "=COUNTA(" + stM + ":" + HK + ")";
                }
                else
                {
                    edV = convertPosition(P_TBM - 1, startRow);
                    cntMark = "=COUNTA(" + stM + ":" + edV + ")";
                }
                sheet.SetFormulaValue(startRow, 2, cntMark);

                //ma hoc sinh
                sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                //ho va ten
                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                }

                //Copy row style
                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                    Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED || listExemptedSubject.Any(u => u.PupilID == Pupil.PupilID))
                {
                    if ((indexPupil - 1) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle1, startRow);
                    }
                    if ((indexPupil) % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle2, startRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(rangeStyle3, startRow);
                    }
                    sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                    sheet.Lock(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                    // Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                    DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? objAca.FirstSemesterEndDate : objAca.SecondSemesterEndDate;
                    DateTime? endDate = Pupil.EndDate;

                    bool showPupilData = false;

                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                        {
                            showPupilData = true;
                        }
                    }
                    else
                    {
                        showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                    }
                    if (!showPupilData)
                    {
                        sheet.GetRange(startRow, P_Nhan_xet, startRow, P_Nhan_xet).FillColor(Color.Yellow);
                        startRow++;
                        continue;
                    }
                }

                if (listExemptedSubject.Any(u => u.PupilID == Pupil.PupilID))
                {
                    int lockCol = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 3 : 4;
                    sheet.LockCellExemptType(sheet, startRow, 1, startRow, listMarkType.Count() + lockCol);
                }

                //Lay diem tung dau diem hoc sinh
                int col = 5;
                int j = 0;

                IEnumerable<JudgeRecordBO> listJudgeRecordPupil = listJudgeRecord.Where(o => o.PupilID == Pupil.PupilID);
                SummedUpRecordBO lstSummedUpRecord = listSummedUpRecord.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault();

                for (int i = startM; i < startP; i++)
                {
                    String titleMark = listM[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }
                j = 0;

                for (int i = startP; i < startV; i++)
                {
                    String titleMark = listP[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);

                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }

                if (strMarkTypePeriod.Contains("HK"))
                {
                    endV = P_TBM - 1;
                }
                j = 0;
                for (int i = startV; i < endV; i++)
                {
                    String titleMark = listV[j];
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);
                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                    col++;
                    j++;
                }

                // hoc ki
                if (strMarkTypePeriod.Contains("HK"))
                {
                    JudgeRecordBO judgeItem = listJudgeRecordPupil.Where(o => o.Title.Equals("HK")).OrderByDescending(u => u.CreatedDate).FirstOrDefault();
                    sheet.SetCellValue(startRow, col, judgeItem != null ? judgeItem.Judgement : string.Empty);

                    if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    {
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                    }
                }
                // Fill giá trị TBM để so sánh
                sheet.SetCellValue(startRow, 25, lstSummedUpRecord != null ? lstSummedUpRecord.JudgementResult : string.Empty);
                //set fomular for TBM
                string TBM = string.Empty;
                bool isClassification = objAca.IsClassification.HasValue && objAca.IsClassification.Value;
                if (strMarkTypePeriod.Contains("HK"))
                {
                    if (isClassification)
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") * 3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0;\"Đ\";\"CĐ\");\"\")";
                    }
                    else
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                    }

                }
                else
                {
                    if (isClassification)
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + edV + ";" + "\"" + "Đ" + "\"" + ") * 3 - COUNTA(" + stM + ":" + edV + ")" + "*2 >=0;\"Đ\";\"CĐ\");\"\")";
                    }
                    else
                    {
                        TBM = "=IF(" + convertPosition(2, startRow) + ">0;IF(COUNTIF(" + stM + ":" + edV + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + edV + ")" + "*2/3; 0) >=0;\"Đ\";\"CĐ\");\"\")";
                    }

                }
                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                sheet.GetRange(startRow, P_TBM, startRow, P_TBM).FillColor(Color.Yellow);

                // Fill cột ghi chú
                string note = string.Empty;
                note = "=IF(AND(Y" + startRow + "<>\"\"," + convertPosition(P_TBM, startRow) + "<>\"\")," + "IF(Y" + startRow + "<>" + convertPosition(P_TBM, startRow) + ",\"HLM trong file excel khác HLM trong hệ thống\",\"\"), \"\")";
                sheet.SetFormulaValue(startRow, P_Nhan_xet, note);
                sheet.GetRange(startRow, P_Nhan_xet, startRow, P_Nhan_xet).FillColor(Color.Yellow);
                startRow++;
            }
            sheet.SetColumnWidth(25, 0);
            sheet.MergeRow(4, 1, P_TBM);
            sheet.SetCellValue("A4", title.ToUpper());
            sheet.MergeRow(5, 1, P_TBM);
            sheet.SetCellValue("A5", objAca.DisplayTitle.ToUpper());

            sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            for (int i = 0; i < cntPupil; i++)
            {
                if (i % 5 == 0)
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                }
                else
                {
                    sheet.GetRange(i + 8, 1, i + 8, P_Nhan_xet).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                }
            }

            if (cntPupil < 5)
            {
                for (int i = 8 + cntPupil; i < 13; i++)
                {
                    sheet.DeleteRow(8 + cntPupil);
                }
            }
            sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Nhan_xet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet
            sheet.SetColumnWidth(2, 0);
            sheet.Name = Utils.StripVNSignAndSpace(ClassName);
            sheet.SetFontName("Times New Roman", 11);
            sheet.ProtectSheet();
        }

        public Stream ExportReportForSemester(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, int PeriodID, int SubjectID, out string FileName, bool? isAdmin = null)
        {

            #region Lay template, tao ten file
            string reportCode = "";
            reportCode = "BangDiemCacMon_23_HKII";

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string subject = SubjectCatBusiness.Find(SubjectID).SubjectName;
            //string period = PeriodDeclarationBusiness.Find(PeriodID).Resolution;
            string classname = ClassProfileBusiness.Find(ClassID).DisplayName;

            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subject));
            //outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(period));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(classname));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));

            //FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            FileName = "Bang Diem Mon_" + ReportUtils.StripVNSign(subject) + "_" + ReportUtils.StripVNSign(classname)
                + "_" + ReportUtils.StripVNSign(semester) + ".xls";
            #endregion

            //Lay mon hoc
            SubjectCat sc = SubjectCatBusiness.Find(SubjectID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);

            #region xu ly lock template
            //Lấy sheet template
            //IVTWorksheet firstSheet = oBook.GetSheet(1);
            //IVTWorksheet emptySheet = oBook.GetSheet(2);
            //IVTWorksheet sheet = oBook.GetSheet(1);

            //firstSheet.LockSheet = true;
            //firstSheet.ProtectSheet();
            //sheet.LockSheet = true;

            ////Xoa bang thua
            //sheet.DeleteRow(13);
            //sheet.DeleteRow(14);
            //sheet.DeleteRow(15);
            //sheet.DeleteRow(16);
            //sheet.DeleteRow(17);

            //IVTRange range = emptySheet.GetRange("E6", "H17");
            //sheet.CopyPasteSameSize(range, "E6");
            #endregion

            #region Tiêu đề template
            //Change header
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidationNumber;
            IVTWorksheet patternSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(patternSheet);

            String title = "BẢNG ĐIỂM MÔN ";
            title += SubjectCatBusiness.Find(SubjectID).DisplayName.ToUpper() + "";
            title += " " + ReportUtils.ConvertSemesterForReportName(Semester) + " ";
            title += " LỚP ";
            title += ClassProfileBusiness.Find(ClassID).DisplayName.ToUpper();

            sheet.SetCellValue("A2", SchoolProfileBusiness.Find(SchoolID).SchoolName);
            sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_ONE_SUBJECT);
            //sheet.SetCellValue("A4", title);
            //sheet.SetCellValue("A5", AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());
            #endregion

            #region xac dinh so dau diem
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["Semester"] = Semester;

            SemeterDeclaration semeterDeclaration = SemeterDeclarationBusiness.Search(search).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            string strMarkTypePeriod = this.GetMarkSemesterTitle(semeterDeclaration.SemeterDeclarationID);

            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
            string[] listM = new string[listMarkType.Count()];
            string[] listP = new string[listMarkType.Count()];
            string[] listV = new string[listMarkType.Count()];

            int cntM = 0, cntP = 0, cntV = 0;
            foreach (string typeMark in listMarkType)
            {
                if (typeMark.Length < 2) continue;

                if (typeMark[0] == 'M')
                {
                    listM[cntM] = typeMark;
                    cntM++;
                }
                if (typeMark[0] == 'P')
                {
                    listP[cntP] = typeMark;
                    cntP++;
                }
                if (typeMark[0] == 'V')
                {
                    listV[cntV] = typeMark;
                    cntV++;
                }
            }
            #endregion

            #region tao column chuan cho sheetData
            int startM = 5;
            int startP = startM + cntM;
            int startV = startP + cntP;
            int k = 0;
            for (int j = startM; j < startP; j++)
            {
                copyCol(patternSheet, sheet, 5, startCol);
                k++;
            }
            k = 0;
            for (int j = startP; j < startV; j++)
            {
                copyCol(patternSheet, sheet, 6, startCol);
                k++;
            }
            k = 0;
            for (int j = startV; j < startV + cntV; j++)
            {
                copyCol(patternSheet, sheet, 7, startCol);
                k++;
            }

            int P_Nhanxet = startCol;
            int P_TBM = startCol;
            int P_KTHT = startCol;
            int P_HK1 = startCol;
            int P_Canam = startCol;
            int P_TBMTC = startCol;
            int P_TBMTmp = startCol;


            //copy cot KTHK
            P_KTHT = startCol;
            copyCol(patternSheet, sheet, 8, startCol);

            //copy cot TBMTC
            P_TBMTC = startCol;
            copyCol(patternSheet, sheet, 9, startCol);

            //copy cot TBMTMP
            P_TBMTmp = startCol;
            copyCol(patternSheet, sheet, 10, startCol);

            //copy cot TBM
            P_TBM = startCol;
            copyCol(patternSheet, sheet, 11, startCol);

            //copy cot HK
            P_HK1 = startCol;
            copyCol(patternSheet, sheet, 12, startCol);

            //copy cot Canam
            P_Canam = startCol;
            copyCol(patternSheet, sheet, 13, startCol);

            P_Nhanxet = startCol;

            if (SubjectCatBusiness.Find(SubjectID).IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
            {
                sheet.SetColumnWidth(P_Nhanxet, 0);
            }
            else
            {
                sheet.GetRange(8, P_Nhanxet, 12, P_Nhanxet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
            }


            //sheet.GetRange("a2", "a3").SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideHorizontal);
            //Merger cot tieu de dau diem
            sheet.MergeRow(6, startM, startV - 1);
            sheet.MergeRow(6, startV, startV + cntV - 1);

            //set tieu de cho cac cot dau diem
            sheet.SetCellValue(6, startM, "Điểm hệ số 1");
            sheet.SetCellValue(6, startV, "Điểm hệ số 2");

            sheet.MergeRow(7, startM, startP - 1);
            sheet.MergeRow(7, startP, startV - 1);
            sheet.MergeRow(7, startV, startV + cntV - 1);

            sheet.SetCellValue(7, startM, "Miệng");
            sheet.SetCellValue(7, startP, "Viết/15P");
            sheet.SetCellValue(7, startV, "Viết");

            #endregion

            //dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["Check"] = "Check";
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).AddPupilStatus(isNotShowPupil);
            List<PupilOfClassBO> lst = (from poc in listPupilOfClass
                                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                        join es in ExemptedSubjectBusiness.All.Where(p => p.SubjectID == SubjectID && p.AcademicYearID == AcademicYearID && p.SchoolID == SchoolID)
                                        on new { poc.PupilID, poc.ClassID } equals new { es.PupilID, es.ClassID }
                                        into g1
                                        from g2 in g1.DefaultIfEmpty()
                                            //orderby poc.PupilProfile.Name
                                        select new PupilOfClassBO
                                        {
                                            PupilOfClassID = poc.PupilOfClassID,
                                            PupilID = poc.PupilID,
                                            ClassID = poc.ClassID,
                                            SchoolID = poc.SchoolID,
                                            AcademicYearID = poc.AcademicYearID,
                                            Year = poc.Year,
                                            AssignedDate = poc.AssignedDate,
                                            OrderInClass = poc.OrderInClass,
                                            Description = poc.Description,
                                            NoConductEstimation = poc.NoConductEstimation,
                                            Status = poc.Status,
                                            EndDate = poc.EndDate,
                                            FirstSemesterExemptType = g2.FirstSemesterExemptType,
                                            SecondSemesterExemptType = g2.SecondSemesterExemptType,
                                            PupilCode = pp.PupilCode,
                                            PupilFullName = pp.FullName,
                                            Name = pp.Name,
                                            EthnicName = pp.Ethnic != null ? pp.Ethnic.EthnicName : null,
                                            EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null
                                        }).ToList().OrderBy(p => p.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
            int cntPupil = listPupilOfClass.Count();
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, SubjectID);
            objValidationNumber = new VTDataValidation
            {
                FromValue = "0",
                ToValue = "10",
                SheetIndex = 1,
                Type = VTValidationType.INTEGER,
                FromColumn = 5,
                ToColumn = startP - 1,
                FromRow = 8,
                ToRow = cntPupil + 7,
            };
            lstValidation.Add(objValidationNumber);

            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                objValidationNumber = new VTDataValidation
                {
                    FromValue = "0",
                    ToValue = "10",
                    SheetIndex = 1,
                    Type = VTValidationType.INTEGER,
                    FromColumn = startP,
                    ToColumn = startV + cntV - 1,
                    FromRow = 8,
                    ToRow = cntPupil + 7
                };
                lstValidation.Add(objValidationNumber);
                objValidationNumber = new VTDataValidation
                {
                    FromValue = "0",
                    ToValue = "10",
                    SheetIndex = 1,
                    Type = VTValidationType.DECIMAL,
                    FromColumn = P_KTHT,
                    ToColumn = P_KTHT,
                    FromRow = 8,
                    ToRow = cntPupil + 7,
                    MinLength = "0",
                    MaxLength = "3",
                };
                lstValidation.Add(objValidationNumber);
            }
            else
            {
                objValidationNumber = new VTDataValidation
                {
                    FromValue = "0",
                    ToValue = "10",
                    SheetIndex = 1,
                    Type = VTValidationType.DECIMAL,
                    FromColumn = startP,
                    ToColumn = startV + cntV,
                    FromRow = 8,
                    ToRow = cntPupil + 7,
                    MinLength = "0",
                    MaxLength = "3",
                };
                lstValidation.Add(objValidationNumber);
            }

            //Xac dinh so hoc sinh roi tao cac dong cho hs
            #region tao cac dong trang cho ds hs
            int RowEnd = 8;
            if (cntPupil > 5)
            {
                int numberGroupStudent = cntPupil / 5;
                if (cntPupil % 5 == 0)
                {
                    numberGroupStudent = numberGroupStudent - 1;
                }
                IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Nhanxet);
                for (int j = 1; j <= numberGroupStudent; j++)
                {
                    RowEnd = RowEnd + 5;
                    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                }
            }
            #endregion
            #region Lay du lieu diem

            // Kiem tra mon hoc co mon tang cuong khong
            int subjectIdIncrease = 0;
            List<int> listSubjectIdAndIncrease = new List<int>();
            ClassSubject classSubjectObj = (from cs in ClassSubjectBusiness.All
                                            join poc in PupilOfClassBusiness.All on cs.ClassID equals poc.ClassID
                                            where cs.SubjectID == SubjectID
                                            && poc.AcademicYearID == AcademicYearID
                                            && poc.ClassID == ClassID
                                            && cs.ClassID == ClassID
                                            select cs).FirstOrDefault();
            if (classSubjectObj != null && classSubjectObj.SubjectIDIncrease.HasValue)
            {
                subjectIdIncrease = classSubjectObj.SubjectIDIncrease.Value;
            }
            if (subjectIdIncrease > 0)
            {
                listSubjectIdAndIncrease.Add(SubjectID);
                listSubjectIdAndIncrease.Add(subjectIdIncrease);
            }
            //kiem tra co cau hinh so con diem toi thieu hay khong
            bool isMinSemester = false;
            int minM = 0;
            int minP = 0;
            int minV = 0;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinFirstSemester.HasValue && classSubjectObj.MMinFirstSemester.Value > 0)
                                   || classSubjectObj.PMinFirstSemester.HasValue && classSubjectObj.PMinFirstSemester.Value > 0
                                   || classSubjectObj.VMinFirstSemester.HasValue && classSubjectObj.VMinFirstSemester.Value > 0);
                minM = classSubjectObj != null && classSubjectObj.MMinFirstSemester.HasValue ? classSubjectObj.MMinFirstSemester.Value : 0;
                minP = classSubjectObj != null && classSubjectObj.PMinFirstSemester.HasValue ? classSubjectObj.PMinFirstSemester.Value : 0;
                minV = classSubjectObj != null && classSubjectObj.VMinFirstSemester.HasValue ? classSubjectObj.VMinFirstSemester.Value : 0;
            }
            else
            {
                isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinSecondSemester.HasValue && classSubjectObj.MMinSecondSemester.Value > 0)
                                   || classSubjectObj.PMinSecondSemester.HasValue && classSubjectObj.PMinSecondSemester.Value > 0
                                   || classSubjectObj.VMinSecondSemester.HasValue && classSubjectObj.VMinSecondSemester.Value > 0);
                minM = classSubjectObj != null && classSubjectObj.MMinSecondSemester.HasValue ? classSubjectObj.MMinSecondSemester.Value : 0;
                minP = classSubjectObj != null && classSubjectObj.PMinSecondSemester.HasValue ? classSubjectObj.PMinSecondSemester.Value : 0;
                minV = classSubjectObj != null && classSubjectObj.VMinSecondSemester.HasValue ? classSubjectObj.VMinSecondSemester.Value : 0;
            }

            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            search = new Dictionary<string, object>();
            search["Year"] = acaYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["Year"] = acaYear.Year;
            List<MarkRecordBO> listMarkRecord = VMarkRecordBusiness.SearchMarkHistoryOrNoHistory(search).ToList();

            search = new Dictionary<string, object>();
            search["Year"] = acaYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            search["ListSubjectID"] = listSubjectIdAndIncrease;
            // search["Semester"] = Semester;
            List<SummedUpRecordBO> listSummerupRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).Where(o => o.PeriodID == null).ToList();

            List<SummedUpRecordBO> listSummerupRecordI = new List<SummedUpRecordBO>();
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listSummerupRecordI = listSummerupRecord.Where(p => p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            }
            else
                listSummerupRecordI = listSummerupRecord;
            #endregion

            int indexPupil = 0;
            int startRow = 8;


            // DateTime endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate.Value : acaYear.SecondSemesterEndDate.Value;

            // lay thong tin mon hoc cua lop
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() {
            {"SubjectID",SubjectID},
            {"SchoolID",SchoolID}
            }
            ).ToList();
            bool isNoLearnFirst = listClassSubject.Any(o => o.SectionPerWeekFirstSemester <= 0);
            bool isNoLearnSecond = listClassSubject.Any(o => o.SectionPerWeekSecondSemester <= 0);

            sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Canam).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
            sheet.GetRange(8 + cntPupil, 1, 7 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

            //tao luoi trong phan diem
            sheet.GetRange(8, startM, 7 + cntPupil, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startP, 7 + cntPupil, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
            sheet.GetRange(8, startV, 7 + cntPupil, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);

            sheet.GetRange(8, 1, cntPupil + 8, 1).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignRight);//STT
            sheet.GetRange(8, 3, cntPupil + 8, 3).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignCenter);//ma hoc sinh
            sheet.GetRange(8, 4, cntPupil + 8, 4).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignLeft);// tem hoc sinh

            sheet.GetRange(startRow, P_TBMTC, cntPupil + 7, P_Canam).FillColor(Color.Yellow);
            sheet.GetRange(startRow, P_TBMTC, cntPupil + 7, P_Canam).IsLock = true;
            if (subjectIdIncrease <= 0)
            {
                sheet.HideColumn(P_TBMTC);
            }
            sheet.HideColumn(P_TBMTmp);

            foreach (PupilOfClassBO Pupil in lst)
            {
                //cot stt
                indexPupil++;
                sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                string stM = convertPosition(startM, startRow);
                string stP = convertPosition(startP, startRow);
                string stV = convertPosition(startV, startRow);

                string edM = convertPosition(startP - 1, startRow);
                string edP = convertPosition(startV - 1, startRow);
                string edV = convertPosition(P_KTHT - 1, startRow);

                for (int i = 0; i < cntPupil; i++)
                {
                    if (i % 5 == 0)
                    {
                        sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                        sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    }
                    else
                    {
                        sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                        sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    }
                }

                //ho va ten
                string cntMark = "=1*COUNTA(" + stM + ":" + edM + ")+" + "1*COUNTA(" + stP + ":" + edP + ")+" + "2*COUNTA(" + stV + ":" + edV + ")";
                //string cntMark = "=1*COUNTIF(" + stM + ":" + edM + ";" + "\">=0\")";
                sheet.SetFormulaValue(startRow, 2, cntMark);


                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                sheet.GetRange(startRow, 4, startRow, 4).WrapText();

                IVTRange rangeStyle1 = patternSheet.GetRange("D13", "D13");
                IVTRange rangeStyle2 = patternSheet.GetRange("D17", "D17");
                IVTRange rangeStyle3 = patternSheet.GetRange("D14", "D14");

                if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                }
                else
                {
                    sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                    sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                    sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);

                    //Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                    DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                    DateTime? endDate = Pupil.EndDate;
                    bool showPupilData = false;

                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                            && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                        {
                            showPupilData = true;
                        }
                    }
                    else
                    {
                        showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                    }

                    if (!showPupilData)
                    {
                        startRow++;
                        continue;
                    }
                }

                if ((Pupil.FirstSemesterExemptType.HasValue && Pupil.FirstSemesterExemptType.Value > 0) || (Pupil.SecondSemesterExemptType.HasValue && Pupil.SecondSemesterExemptType.Value > 0))
                {
                    sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                    sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                }

                //Lay diem tung dau diem hoc sinh
                int col = 5;
                int j = 0;
                for (int i = startM; i < startP; i++)
                {
                    MarkRecordBO markRecord = listMarkRecord.Where(p => p.PupilID == Pupil.PupilID && listM[j].Equals(p.Title)).FirstOrDefault();
                    if (markRecord != null)
                        sheet.SetCellValue(startRow, col, markRecord.Mark);
                    else
                        sheet.SetCellValue(startRow, col, string.Empty);

                    if ((strLockMarkType.Contains(listM[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                    col++;
                    j++;
                }

                j = 0;
                for (int i = startP; i < startV; i++)
                {
                    MarkRecordBO markRecord = listMarkRecord.Where(o => o.PupilID == Pupil.PupilID && listP[j].Equals(o.Title)).FirstOrDefault();
                    if (markRecord != null)
                        sheet.SetCellValue(startRow, col, markRecord.Mark);
                    else
                        sheet.SetCellValue(startRow, col, "");

                    if ((strLockMarkType.Contains(listP[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                    col++;
                    j++;
                }

                j = 0;
                for (int i = startV; i < startV + cntV; i++)
                {
                    MarkRecordBO markRecord = listMarkRecord.Where(o => o.PupilID == Pupil.PupilID && listV[j].Equals(o.Title)).FirstOrDefault();
                    if (markRecord != null)
                        sheet.SetCellValue(startRow, col, markRecord.Mark);
                    else
                        sheet.SetCellValue(startRow, col, "");

                    if ((strLockMarkType.Contains(listV[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                        sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                    col++;
                    j++;

                    if (cntV < 3)
                    {
                        sheet.SetColumnWidth(i, 8);
                    }
                }

                MarkRecordBO markRecordHK = listMarkRecord.Where(o => o.PupilID == Pupil.PupilID && "HK".Equals(o.Title)).FirstOrDefault();
                if (markRecordHK != null)
                    sheet.SetCellValue(startRow, P_KTHT, markRecordHK.Mark);
                if ((strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                    sheet.GetRange(startRow, P_KTHT, startRow, P_KTHT).IsLock = true;

                //kiem tra va hien thi diem mon tang cuong va cong thuc TBM trong excel
                if (subjectIdIncrease > 0) // mon hoc co mon tang cuong
                {
                    if (Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED || Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        //Lay TBM cua mon tang cuong
                        SummedUpRecordBO surIIncrease = listSummerupRecord.FirstOrDefault(u => u.PupilID == Pupil.PupilID && u.SubjectID == subjectIdIncrease && u.Semester == Semester);
                        if (surIIncrease != null)
                        {
                            sheet.SetCellValue(convertPosition(P_TBMTC, startRow), surIIncrease.SummedUpMark);
                        }
                    }
                }

                string TBMTmp = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(2, startRow) + "<>\"\";ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3);1);\"\");\"\")";
                sheet.SetFormulaValue(startRow, P_TBMTmp, TBMTmp);
                sheet.HideColumn(P_TBMTmp);

                //Cong thuc tinh trung binh mon neu co mon tang cuong thi fill cong thuc mon tang cuong, khong co mon tang cuong thi fill cong thuc nhu binh thuong
                string TBM = string.Empty;
                if (isMinSemester)
                {
                    if (subjectIdIncrease > 0)
                    {
                        //fill cong thuc cho mon tang cuong
                        TBM = "=IF(AND(COUNT(" + stM + ":" + edM + ") >= " + minM + ",COUNT(" + stP + ":" + edP + ") >= " + minP + ",COUNT(" + stV + ":" + edV + ") >= " + minV + "),IF(AND(" + convertPosition(P_TBMTC, startRow) + " <>\"\"," + convertPosition(P_TBMTmp, startRow) + "<>\"\"),IF(" + convertPosition(P_TBMTmp, startRow) + " +IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0)))>10,10," + convertPosition(P_TBMTmp, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0))))," + convertPosition(P_TBMTmp, startRow) + "),\"\")";
                        sheet.SetFormulaValue(startRow, P_TBM, TBM);
                    }
                    else
                    {
                        //Fill cong thuc cho mon khong tang cuong   
                        TBM = "=IF(AND(COUNT(" + stM + ":" + edM + ") >= " + minM + ",COUNT(" + stP + ":" + edP + ") >= " + minP + ",COUNT(" + stV + ":" + edV + ") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(2, startRow) + "<>\"\",ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3),1),\"\"),\"\"),\"\")";
                        sheet.SetFormulaValue(startRow, P_TBM, TBM);
                    }
                }
                else
                {
                    if (subjectIdIncrease > 0)
                    {
                        //fill cong thuc cho mon tang cuong
                        TBM = "=IF(AND(" + convertPosition(P_TBMTC, startRow) + " <>\"\"," + convertPosition(P_TBMTmp, startRow) + "<>\"\"),IF(" + convertPosition(P_TBMTmp, startRow) + " +IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0)))>10,10," + convertPosition(P_TBMTmp, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,0.3,IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,0.2,IF(" + convertPosition(P_TBMTC, startRow) + ">=5,0.1,0))))," + convertPosition(P_TBMTmp, startRow) + ")";
                        sheet.SetFormulaValue(startRow, P_TBM, TBM);
                    }
                    else
                    {
                        //Fill cong thuc cho mon khong tang cuong   
                        TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(2, startRow) + "<>\"\",ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3),1),\"\"),\"\")";
                        sheet.SetFormulaValue(startRow, P_TBM, TBM);
                    }
                }
                SummedUpRecordBO surI = listSummerupRecordI.FirstOrDefault(u => u.PupilID == Pupil.PupilID && u.SubjectID == SubjectID);
                if (surI != null)
                {
                    sheet.SetCellValue(startRow, P_HK1, surI.SummedUpMark);
                }

                string formularTBHK = "=IF(" + convertPosition(P_HK1, startRow) + "<>\"\",IF(" + convertPosition(P_TBM, startRow) + "<>\"\",ROUND((" + convertPosition(P_HK1, startRow) + "+2*" + convertPosition(P_TBM, startRow) + ")/3,1),\"\"),\"\")";


                if (Pupil.SecondSemesterExemptType != null || isNoLearnSecond)
                {
                    formularTBHK = "=" + convertPosition(P_HK1, startRow);
                }

                if (Pupil.FirstSemesterExemptType != null || isNoLearnFirst)
                {
                    formularTBHK = TBM;
                }

                sheet.SetFormulaValue(startRow, P_Canam, formularTBHK);


                startRow++;
            }

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                sheet.SetColumnWidth(P_Canam, 0);
            }

            sheet.SetColumnWidth(P_HK1, 0);
            sheet.SetColumnWidth(2, 0);

            if (cntPupil <= 10)
            {
                int totalRecordNotFill = 10 + 8; // 8 bat dau tu dong thu 8
                int totalRecordDelete = cntPupil + 8;
                for (int i = totalRecordNotFill; i >= totalRecordDelete; i--)
                {
                    sheet.DeleteRow(i);
                }
            }
            sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

            if (Semester == 1)
            {
                sheet.MergeRow(4, 1, P_TBM);
                sheet.MergeRow(5, 1, P_TBM);
                sheet.SetCellValue("A4", title);
                sheet.SetCellValue("A5", AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());
            }
            else
            {
                sheet.MergeRow(4, 1, P_Canam);
                sheet.MergeRow(5, 1, P_Canam);
                sheet.SetCellValue("A4", title);
                sheet.SetCellValue("A5", AcademicYearBusiness.Find(AcademicYearID).DisplayTitle.ToUpper());
            }

            // emptySheet.Delete();
            //firstSheet.Delete();
            sheet.Name = Utils.StripVNSignAndSpace(sc.DisplayName);
            sheet.GetRange(8, P_KTHT, 8 + cntPupil, P_TBM).NumberFormat("0.0");
            sheet.SetFontName("Times New Roman", 11);
            sheet.ProtectSheet();
            oBook.GetSheet(2).Delete();
            oBook.GetSheet(1).Delete();
            // decimal fromNum = 0;
            // decimal toNum = 10;
            //return oBook.ToStreamNumberValidationData(1, fromNum, toNum, 8, 5, 8 + cntPupil, startV + cntV);
            return oBook.ToStreamValidationData(lstValidation);
        }

        #region Xuat excel So diem theo danh sach mon hoc truyen vao

        public Stream ExportReportForSemesterByListSubjectID(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, List<int> listSubjectID, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null)
        {

            #region Lay template, tao ten file
            string reportCode = "BangDiemCacMon_23_HKII";

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string classname = ClassProfileBusiness.Find(ClassID).DisplayName;

            // outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subject));
            // outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(classname));
            // outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));

            #endregion

            int partitionId = UtilsBusiness.GetPartionId(SchoolID, 100);
            #region Danh sach mon hoc theo Id truyen vao
            List<SubjectCatBO> listSubjectCat = (from cs in ClassSubjectBusiness.All
                                                 join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                 join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                 where listSubjectID.Contains(cs.SubjectID)
                                                 && sc.AppliedLevel == AppliedLevel
                                                 && cp.AcademicYearID == AcademicYearID
                                                 && cp.EducationLevel.Grade == AppliedLevel
                                                 && cs.ClassID == ClassID
                                                 && cp.EducationLevelID == EducationLevelID
                                                 && sc.IsActive == true
                                                 && cp.IsActive.Value
                                                 && cs.Last2digitNumberSchool == partitionId
                                                 && ((Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && cs.SectionPerWeekFirstSemester > 0)
                                                     || (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && cs.SectionPerWeekSecondSemester > 0))
                                                 select new SubjectCatBO
                                                 {
                                                     SubjectCatID = cs.SubjectID,
                                                     SubjectName = sc.DisplayName,
                                                     IsCommenting = cs.IsCommenting,
                                                     DisplayName = sc.DisplayName,
                                                     SubjectIdInCrease = cs.SubjectIDIncrease,
                                                     OrderInSubject = sc.OrderInSubject
                                                 }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            //SubjectCatBusiness.All.Where(p => listSubjectID.Contains(p.SubjectCatID) && p.AppliedLevel == AppliedLevel).ToList();
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            #endregion

            #region xu ly lock template
            //Lấy sheet template
            //IVTWorksheet firstSheet = oBook.GetSheet(1);
            //IVTWorksheet emptySheet = oBook.GetSheet(2);
            //IVTWorksheet sheet = oBook.GetSheet(3);

            // firstSheet.LockSheet = true;
            //firstSheet.ProtectSheet();
            // sheet.LockSheet = true;

            //Xoa bang thua
            //sheet.DeleteRow(13);
            //sheet.DeleteRow(14);
            //sheet.DeleteRow(15);
            //sheet.DeleteRow(16);
            //sheet.DeleteRow(17);


            //IVTRange range = emptySheet.GetRange("E6", "H17");
            // sheet.CopyPasteSameSize(range, "E6");
            #endregion

            #region Tiêu đề template
            //Change header
            IVTWorksheet sheet = null;
            IVTWorksheet patternSheet = null;
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objay = AcademicYearBusiness.Find(AcademicYearID);
            string className = ClassProfileBusiness.Find(ClassID).DisplayName;
            string schoolName = objSP.SchoolName;
            string strTitleAcademicYear = AcademicYearBusiness.Find(AcademicYearID).DisplayTitle;
            #endregion

            #region Dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            // dic["Check"] = "Check";
            bool isNotShowPupil = objay.IsShowPupil.HasValue && objay.IsShowPupil.Value;
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).AddPupilStatus(isNotShowPupil);
            List<ExemptedSubject> lstExemptedSubject = ExemptedSubjectBusiness.All
                .Where(p => p.AcademicYearID == AcademicYearID && p.ClassID == ClassID
                    && listSubjectID.Contains(p.SubjectID)
                    && ((Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && p.FirstSemesterExemptType > 0)
                     || (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && p.SecondSemesterExemptType > 0))).ToList();

            List<PupilOfClassBO> lstPupilOfClasss = (from poc in listPupilOfClass
                                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                     select new PupilOfClassBO
                                                     {
                                                         PupilOfClassID = poc.PupilOfClassID,
                                                         PupilID = poc.PupilID,
                                                         ClassID = poc.ClassID,
                                                         SchoolID = poc.SchoolID,
                                                         AcademicYearID = poc.AcademicYearID,
                                                         Year = poc.Year,
                                                         AssignedDate = poc.AssignedDate,
                                                         OrderInClass = poc.OrderInClass,
                                                         Description = poc.Description,
                                                         NoConductEstimation = poc.NoConductEstimation,
                                                         Status = poc.Status,
                                                         EndDate = poc.EndDate,
                                                         PupilCode = pp.PupilCode,
                                                         PupilFullName = pp.FullName,
                                                         Name = pp.Name,
                                                         EthnicName = pp.Ethnic != null ? pp.Ethnic.EthnicName : null,
                                                         EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null,
                                                     }).ToList().OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
            int cntPupil = lstPupilOfClasss.Count();
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            #endregion

            #region Danh sach diem tinh diem
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["Semester"] = Semester;
            SemeterDeclaration semeterDeclaration = SemeterDeclarationBusiness.Search(search).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();

            search = new Dictionary<string, object>();
            search["Year"] = acaYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            //search["SubjectID"] = subjectId;
            search["Semester"] = Semester;
            search["Year"] = acaYear.Year;
            List<MarkRecordBO> listMarkRecord = VMarkRecordBusiness.SearchMarkHistoryOrNoHistory(search).ToList();
            List<MarkRecordBO> listMarkRecordBySubjectID = new List<MarkRecordBO>();

            search = new Dictionary<string, object>();
            search["Year"] = acaYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            //search["SubjectID"] = subjectId;
            //search["Semester"] = Semester;
            List<SummedUpRecordBO> listSummerupRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).Where(o => o.PeriodID == null).ToList();
            List<SummedUpRecordBO> listSummerupRecordBySubjectID = new List<SummedUpRecordBO>();

            List<SummedUpRecordBO> listSummerupRecordI = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> listSummerupRecordIBySubjectID = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> listSummerupRecordIBySubjectIDIncrease = new List<SummedUpRecordBO>();
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listSummerupRecordI = listSummerupRecord.Where(p => p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            }
            else
            {
                listSummerupRecordI = listSummerupRecord;
            }

            // lay thong tin mon hoc cua lop
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() {
            {"SchoolID",SchoolID}
            }
            ).ToList();
            List<ClassSubject> listClassSubjectBySubjectId = new List<ClassSubject>();
            ClassSubject classSubjectObj = null;
            //khai bao thong tin dang ky cau hinh so con diem toi thieu
            bool isMinSemester = false;
            int minM = 0;
            int minP = 0;
            int minV = 0;

            #endregion

            #region Danh sach diem nhan xet            
            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["ClassID"] = ClassID;
            //search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["checkWithClassMovement"] = "1";
            search["Year"] = acaYear.Year;
            List<JudgeRecordBO> listJudgeRecord = VJudgeRecordBusiness.SearchJudgeRecordNoView(search).ToList();



            #endregion


            int subjectId = 0;
            int isCommenting = 0;
            int subjectIdIncrese = 0;
            List<int> listSubjectIdIncrease = listSubjectCat.Where(p => p.SubjectIdInCrease.HasValue && p.SubjectIdInCrease.Value > 0).Select(p => p.SubjectIdInCrease.Value).ToList();

            #region Danh sach Id mon hoc gop tu 2 list cua mon duoc chon va mon tang cuong
            List<int> listSubjectIdChoiceAndIncrease = listSubjectID.Union(listSubjectIdIncrease).ToList();
            List<SubjectCat> listSubjectCatUnion = SubjectCatBusiness.All.Where(p => listSubjectIdChoiceAndIncrease.Contains(p.SubjectCatID)).ToList();
            #endregion

            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation;
            VTDataValidation objValidationNumber;
            string[] cd = new string[] { GlobalConstants.SHORT_OK, GlobalConstants.SHORT_NOT_OK };

            //BangDiemMonHoc_Tenlop_HK.xls
            FileName = "BangDiemMonHoc_" + ReportUtils.StripVNSign(classname) + "_" + semester + ".xls";

            #region for qua danh sach mon hoc
            int totalSheet = 0;
            for (int l = 0; l < listSubjectCat.Count; l++)
            {
                startCol = 5;
                subjectId = listSubjectCat[l].SubjectCatID;
                isCommenting = listSubjectCat[l].IsCommenting.Value;
                subjectIdIncrese = listSubjectCat[l].SubjectIdInCrease.HasValue ? listSubjectCat[l].SubjectIdInCrease.Value : 0;
                objValidation = new VTDataValidation();
                classSubjectObj = listClassSubject.Where(p => p.SubjectID == subjectId).FirstOrDefault();
                //lay thong tin dang ky cau hinh so con diem toi thieu
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinFirstSemester.HasValue && classSubjectObj.MMinFirstSemester.Value > 0)
                                       || classSubjectObj.PMinFirstSemester.HasValue && classSubjectObj.PMinFirstSemester.Value > 0
                                       || classSubjectObj.VMinFirstSemester.HasValue && classSubjectObj.VMinFirstSemester.Value > 0);
                    minM = classSubjectObj != null && classSubjectObj.MMinFirstSemester.HasValue ? classSubjectObj.MMinFirstSemester.Value : 0;
                    minP = classSubjectObj != null && classSubjectObj.PMinFirstSemester.HasValue ? classSubjectObj.PMinFirstSemester.Value : 0;
                    minV = classSubjectObj != null && classSubjectObj.VMinFirstSemester.HasValue ? classSubjectObj.VMinFirstSemester.Value : 0;
                }
                else
                {
                    isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinSecondSemester.HasValue && classSubjectObj.MMinSecondSemester.Value > 0)
                                       || classSubjectObj.PMinSecondSemester.HasValue && classSubjectObj.PMinSecondSemester.Value > 0
                                       || classSubjectObj.VMinSecondSemester.HasValue && classSubjectObj.VMinSecondSemester.Value > 0);
                    minM = classSubjectObj != null && classSubjectObj.MMinSecondSemester.HasValue ? classSubjectObj.MMinSecondSemester.Value : 0;
                    minP = classSubjectObj != null && classSubjectObj.PMinSecondSemester.HasValue ? classSubjectObj.PMinSecondSemester.Value : 0;
                    minV = classSubjectObj != null && classSubjectObj.VMinSecondSemester.HasValue ? classSubjectObj.VMinSecondSemester.Value : 0;
                }
                totalSheet++;
                if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                {
                    patternSheet = oBook.GetSheet(1);
                    sheet = oBook.CopySheetToLast(patternSheet);
                    sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID);
                    #region Mon tinh diem
                    String title = "BẢNG ĐIỂM MÔN ";
                    title += listSubjectCat[l].DisplayName.ToUpper() + "";
                    title += " " + ReportUtils.ConvertSemesterForReportName(Semester) + " ";
                    title += " LỚP ";
                    title += className.ToUpper();
                    sheet.SetCellValue("A2", schoolName);
                    sheet.SetCellValue("A4", title);
                    sheet.SetCellValue("A5", strTitleAcademicYear.ToUpper());

                    string sheetName = this.getSheetNameExport(listSubjectIdIncrease, subjectId, listSubjectCat[l].DisplayName);

                    //lay ten mon tang cuong
                    string subjectNameIncrease = string.Empty;
                    if (subjectIdIncrese > 0)
                    {
                        subjectNameIncrease = this.getSubjectNameIncrease(listSubjectCatUnion, subjectIdIncrese);
                    }

                    #region xac dinh so dau diem
                    string strMarkTypePeriod = this.GetMarkSemesterTitle(semeterDeclaration.SemeterDeclarationID);
                    string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
                    string[] listM = new string[listMarkType.Count()];
                    string[] listP = new string[listMarkType.Count()];
                    string[] listV = new string[listMarkType.Count()];

                    int cntM = 0, cntP = 0, cntV = 0;
                    foreach (string typeMark in listMarkType)
                    {
                        if (typeMark.Length < 2) continue;

                        if (typeMark[0] == 'M')
                        {
                            listM[cntM] = typeMark;
                            cntM++;
                        }
                        if (typeMark[0] == 'P')
                        {
                            listP[cntP] = typeMark;
                            cntP++;
                        }
                        if (typeMark[0] == 'V')
                        {
                            listV[cntV] = typeMark;
                            cntV++;
                        }
                    }
                    #endregion

                    #region tao column chuan cho sheetData
                    int startM = 5;
                    int startP = startM + cntM;
                    int startV = startP + cntP;
                    int k = 0;
                    for (int j = startM; j < startP; j++)
                    {
                        copyCol(patternSheet, sheet, 5, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startP; j < startV; j++)
                    {
                        copyCol(patternSheet, sheet, 6, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startV; j < startV + cntV; j++)
                    {
                        copyCol(patternSheet, sheet, 7, startCol);
                        k++;
                    }

                    // int P_Nhanxet = startCol;                   
                    int P_TBM = startCol;
                    int P_KTHT = startCol;
                    int P_HK1 = startCol;
                    int P_Canam = startCol;
                    int P_TBMTC = startCol;
                    int P_TBMTmp = startCol;

                    //copy cot KTHK
                    P_KTHT = startCol;
                    copyCol(patternSheet, sheet, 8, startCol);

                    //copy cot TBMTC
                    P_TBMTC = startCol;
                    copyCol(patternSheet, sheet, 9, startCol);

                    //copy cot TBMTMP
                    P_TBMTmp = startCol;
                    copyCol(patternSheet, sheet, 10, startCol);

                    //copy cot TBM
                    P_TBM = startCol;
                    copyCol(patternSheet, sheet, 11, startCol);

                    //copy cot HK
                    P_HK1 = startCol;
                    copyCol(patternSheet, sheet, 12, startCol);

                    P_Canam = startCol;
                    copyCol(patternSheet, sheet, 13, startCol);
                    ///////////////////
                    objValidationNumber = new VTDataValidation
                    {
                        FromValue = "0",
                        ToValue = "10",
                        SheetIndex = totalSheet,
                        Type = VTValidationType.INTEGER,
                        FromColumn = 5,
                        ToColumn = startP - 1,
                        FromRow = 8,
                        ToRow = lstPupilOfClasss.Count + 7
                    };
                    lstValidation.Add(objValidationNumber);

                    if (objSP.TrainingTypeID == 3)//truong GDTX
                    {
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "0",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.INTEGER,
                            FromColumn = startP,
                            ToColumn = startV + cntV - 1,
                            FromRow = 8,
                            ToRow = lstPupilOfClasss.Count + 7
                        };
                        lstValidation.Add(objValidationNumber);
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "0",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.DECIMAL,
                            FromColumn = P_KTHT,
                            ToColumn = P_KTHT,
                            FromRow = 8,
                            ToRow = cntPupil + 7,
                            MinLength = "0",
                            MaxLength = "3",
                        };
                        lstValidation.Add(objValidationNumber);
                    }
                    else
                    {
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "0",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.DECIMAL,
                            FromColumn = startP,
                            ToColumn = startV + cntV,
                            FromRow = 8,
                            ToRow = lstPupilOfClasss.Count + 7,
                            MinLength = "1",
                            MaxLength = "3",
                        };
                        lstValidation.Add(objValidationNumber);
                    }
                    //P_Nhanxet = startCol;

                    //copyCol(patternSheet, sheet, 9, startCol);

                    //if (listSubjectCat[l].IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    //{
                    //    sheet.SetColumnWidth(P_Nhanxet, 0);
                    //}
                    //else
                    //{
                    //    sheet.GetRange(8, P_Nhanxet, 12, P_Nhanxet).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    //}


                    //sheet.GetRange("a2", "a3").SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideHorizontal);
                    //Merger cot tieu de dau diem
                    sheet.MergeRow(6, startM, startV - 1);
                    sheet.MergeRow(6, startV, startV + cntV - 1);

                    //set tieu de cho cac cot dau diem
                    sheet.SetCellValue(6, startM, "Điểm hệ số 1");
                    sheet.SetCellValue(6, startV, "Điểm hệ số 2");

                    sheet.MergeRow(7, startM, startP - 1);
                    sheet.MergeRow(7, startP, startV - 1);
                    sheet.MergeRow(7, startV, startV + cntV - 1);

                    sheet.SetCellValue(7, startM, "Miệng");
                    sheet.SetCellValue(7, startP, "Viết/15P");
                    sheet.SetCellValue(7, startV, "Viết");

                    #endregion

                    string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, subjectId);

                    // Xac dinh so hoc sinh roi tao cac dong cho hs
                    #region tao cac dong trang cho ds hs
                    //int RowEnd = 8;
                    if (cntPupil > 5)
                    {
                        int numberGroupStudent = cntPupil / 5;
                        if (cntPupil % 5 == 0)
                        {
                            numberGroupStudent = numberGroupStudent - 1;
                        }
                        //IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Nhanxet);
                        //for (int j = 1; j <= numberGroupStudent; j++)
                        //{
                        //    RowEnd = RowEnd + 5;
                        //    sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                        //}
                    }
                    #endregion

                    #region Lay du lieu diem theo  mon hoc
                    listMarkRecordBySubjectID = listMarkRecord.Where(p => p.SubjectID == subjectId).ToList();
                    listSummerupRecordBySubjectID = listSummerupRecord.Where(p => p.SubjectID == subjectId && p.Semester == Semester).ToList();
                    listSummerupRecordIBySubjectID = listSummerupRecordI.Where(p => p.SubjectID == subjectId).ToList();
                    listSummerupRecordIBySubjectIDIncrease = listSummerupRecordI.Where(p => p.SubjectID == subjectIdIncrese).ToList();
                    #endregion

                    #region Fill thong diem cua hoc sinh
                    int indexPupil = 0;
                    int startRow = 8;

                    // DateTime endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate.Value : acaYear.SecondSemesterEndDate.Value;

                    //  lay thong tin mon hoc cua lop
                    listClassSubjectBySubjectId = listClassSubject.Where(p => p.SubjectID == subjectId).ToList();
                    bool isNoLearnFirst = listClassSubjectBySubjectId.Any(o => o.SectionPerWeekFirstSemester <= 0);
                    bool isNoLearnSecond = listClassSubjectBySubjectId.Any(o => o.SectionPerWeekSecondSemester <= 0);

                    sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Canam).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    //tao luoi trong phan diem

                    sheet.GetRange(8, startM, cntPupil + 7, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startP, cntPupil + 7, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startV, cntPupil + 7, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);

                    sheet.GetRange(startRow, P_TBMTC, cntPupil + 8, P_Canam).IsLock = true;
                    sheet.GetRange(8, 1, cntPupil + 8, 1).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignRight);//STT
                    sheet.GetRange(8, 3, cntPupil + 8, 3).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignCenter);//ma hoc sinh
                    sheet.GetRange(8, 4, cntPupil + 8, 4).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignLeft);// tem hoc sinh                    
                    if (subjectIdIncrese <= 0)
                    {
                        sheet.HideColumn(P_TBMTC);
                    }


                    foreach (PupilOfClassBO Pupil in lstPupilOfClasss)
                    {
                        //cot stt
                        indexPupil++;
                        sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                        sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                        sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                        string stM = convertPosition(startM, startRow);
                        string stP = convertPosition(startP, startRow);
                        string stV = convertPosition(startV, startRow);

                        string edM = convertPosition(startP - 1, startRow);
                        string edP = convertPosition(startV - 1, startRow);
                        string edV = convertPosition(P_KTHT - 1, startRow);

                        sheet.GetRange(startRow, P_TBMTC, startRow, P_Canam).FillColor(Color.Yellow);

                        for (int i = 0; i < cntPupil; i++)
                        {
                            if (i % 5 == 0)
                            {
                                sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                                sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            }
                            else
                            {
                                sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                                sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            }
                        }


                        //ho va ten
                        string cntMark = "=1*COUNTA(" + stM + ":" + edM + ")+" + "1*COUNTA(" + stP + ":" + edP + ")+" + "2*COUNTA(" + stV + ":" + edV + ")";
                        //string cntMark = "=1*COUNTIF(" + stM + ":" + edM + ";" + "\">=0\")";
                        sheet.SetFormulaValue(startRow, 2, cntMark);

                        string TBMTC = string.Empty;
                        string TBM = string.Empty;
                        string TBMIncreased = string.Empty;
                        string TBMTmp = string.Empty;

                        //kiem tra va hien thi diem mon tang cuong va cong thuc TBM trong excel
                        if (subjectIdIncrese > 0 && listSubjectID.Exists(p => p == subjectIdIncrese) && Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING) // mon hoc co mon tang cuong
                        {
                            //Fill cong thuc TBM tang cuong
                            TBMIncreased = "=IF(" + subjectNameIncrease + "!" + convertPosition(P_TBM, startRow) + "<>\"\"," + subjectNameIncrease + "!" + convertPosition(P_TBM, startRow) + ",\"\")";
                            sheet.SetFormulaValue(startRow, P_TBMTC, TBMIncreased);
                        }
                        else
                        {
                            if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                            {
                                SummedUpRecordBO surIIncrease = listSummerupRecord.FirstOrDefault(u => u.PupilID == Pupil.PupilID && u.SubjectID == subjectIdIncrese && u.Semester == Semester);
                                if (surIIncrease != null)
                                {
                                    sheet.SetCellValue(convertPosition(P_TBMTC, startRow), surIIncrease.SummedUpMark);
                                }
                            }
                        }

                        TBMTmp = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(2, startRow) + "<>\"\";ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3);1);\"\");\"\")";
                        sheet.SetFormulaValue(startRow, P_TBMTmp, TBMTmp);
                        sheet.HideColumn(P_TBMTmp);

                        if (isMinSemester)
                        {
                            if (listSubjectCat[l].SubjectIdInCrease.HasValue && listSubjectCat[l].SubjectIdInCrease.Value > 0
                                && (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED))
                            {
                                //fill cong thuc cho mon tang cuong
                                TBM = "=IF(AND(COUNT(" + stM + ":" + edM + ") >= " + minM + ",COUNT(" + stP + ":" + edP + ") >= " + minP + ",COUNT(" + stV + ":" + edV + ") >= " + minV + "),IF(AND(" + convertPosition(P_TBMTC, startRow) + " <>\"\"," + convertPosition(P_TBMTmp, startRow) + "<>\"\"),IF(" + convertPosition(P_TBMTmp, startRow) + " +IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0)))>10,10," + convertPosition(P_TBMTmp, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0))))," + convertPosition(P_TBMTmp, startRow) + "),\"\")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                            else
                            {
                                //Fill cong thuc cho mon khong tang cuong   
                                TBM = "=IF(AND(COUNT(" + stM + ":" + edM + ") >= " + minM + ",COUNT(" + stP + ":" + edP + ") >= " + minP + ",COUNT(" + stV + ":" + edV + ") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(2, startRow) + "<>\"\",ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3),1),\"\"),\"\"),\"\")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                        }
                        else
                        {
                            if (listSubjectCat[l].SubjectIdInCrease.HasValue && listSubjectCat[l].SubjectIdInCrease.Value > 0
                                && (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED))
                            {
                                //fill cong thuc cho mon tang cuong
                                TBM = "=IF(AND(" + convertPosition(P_TBMTC, startRow) + " <>\"\"," + convertPosition(P_TBMTmp, startRow) + "<>\"\"),IF(" + convertPosition(P_TBMTmp, startRow) + " +IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0)))>10,10," + convertPosition(P_TBMTmp, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0))))," + convertPosition(P_TBMTmp, startRow) + ")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                            else
                            {
                                //Fill cong thuc cho mon khong tang cuong   
                                TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(2, startRow) + "<>\"\";ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3);1);\"\");\"\")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                        }
                        sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                        sheet.GetRange(startRow, 4, startRow, 4).WrapText();

                        bool showData = true;
                        if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                            sheet.GetRange(startRow, P_TBMTC, startRow, P_Canam).IsLock = true;
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                            sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                            sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                        }

                        //Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                        DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                        DateTime? endDate = Pupil.EndDate;
                        bool showPupilData = false;

                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                            {
                                showPupilData = true;
                            }
                        }
                        else
                        {
                            showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                            || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                        }

                        if (!showPupilData)
                        {
                            startRow++;
                            continue;
                        }

                        if (lstExemptedSubject.Exists(p => p.PupilID == Pupil.PupilID && p.SubjectID == subjectId) && (lstExemptedSubject.Count(p => p.FirstSemesterExemptType > 0) > 0 || lstExemptedSubject.Count(p => p.SecondSemesterExemptType > 0) > 0))
                        {
                            sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                            sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                        }

                        if (showData)
                        {
                            //Lay diem tung dau diem hoc sinh
                            int col = 5;
                            int j = 0;
                            for (int i = startM; i < startP; i++)
                            {
                                MarkRecordBO markRecord = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals(listM[j]));
                                if (markRecord != null)
                                    sheet.SetCellValue(startRow, col, markRecord.Mark);
                                else
                                    sheet.SetCellValue(startRow, col, string.Empty);

                                if ((strLockMarkType.Contains(listM[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startP; i < startV; i++)
                            {
                                MarkRecordBO markRecord = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals(listP[j]));
                                if (markRecord != null)
                                    sheet.SetCellValue(startRow, col, markRecord.Mark);
                                else
                                    sheet.SetCellValue(startRow, col, "");

                                if ((strLockMarkType.Contains(listP[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startV; i < startV + cntV; i++)
                            {
                                MarkRecordBO markRecord = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals(listV[j]));
                                if (markRecord != null)
                                    sheet.SetCellValue(startRow, col, markRecord.Mark);
                                else
                                    sheet.SetCellValue(startRow, col, "");

                                if ((strLockMarkType.Contains(listV[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                                col++;
                                j++;

                                if (cntV < 3)
                                {
                                    sheet.SetColumnWidth(i, 8);
                                }
                            }

                            MarkRecordBO markRecordHK = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals("HK"));
                            if (markRecordHK != null)
                                sheet.SetCellValue(startRow, P_KTHT, markRecordHK.Mark);
                            if ((strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                sheet.GetRange(startRow, P_KTHT, startRow, P_KTHT).IsLock = true;

                            SummedUpRecordBO surI = listSummerupRecordIBySubjectID.FirstOrDefault(u => u.PupilID == Pupil.PupilID);
                            if (surI != null)
                                sheet.SetCellValue(startRow, P_HK1, surI.SummedUpMark);

                            string formularTBHK = "=IF(" + convertPosition(P_HK1, startRow) + "<>\"\";IF(" + convertPosition(P_TBM, startRow) + "<>\"\";ROUND((" + convertPosition(P_HK1, startRow) + "+2*" + convertPosition(P_TBM, startRow) + ")/3;1);\"\");\"\")";

                            if (Pupil.SecondSemesterExemptType != null || isNoLearnSecond)
                            {
                                formularTBHK = "=" + convertPosition(P_HK1, startRow);
                            }

                            if (Pupil.FirstSemesterExemptType != null || isNoLearnFirst)
                            {
                                formularTBHK = "=" + convertPosition(P_TBM, startRow);
                            }
                            sheet.SetFormulaValue(startRow, P_Canam, formularTBHK);
                        }

                        startRow++;
                    }

                    if (cntPupil <= 10)
                    {
                        int totalRecordNotFill = 10 + 8; // 8 bat dau tu dong thu 8
                        int totalRecordDelete = cntPupil + 8;
                        for (int i = totalRecordNotFill; i >= totalRecordDelete; i--)
                        {
                            sheet.DeleteRow(i);
                        }
                    }
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet


                    #endregion

                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.SetColumnWidth(P_Canam, 0);
                    }

                    sheet.SetColumnWidth(P_HK1, 0);
                    sheet.SetColumnWidth(2, 0);

                    if (Semester == 1)
                    {
                        sheet.MergeRow(4, 1, P_TBM);
                        sheet.MergeRow(5, 1, P_TBM);
                        sheet.SetCellValue("A4", title);
                        sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
                    }
                    else
                    {
                        sheet.MergeRow(4, 1, P_Canam);
                        sheet.MergeRow(5, 1, P_Canam);
                        sheet.SetCellValue("A4", title);
                        sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
                    }
                    sheet.Name = sheetName;
                    sheet.SetFontName("Times New Roman", 11);
                    sheet.GetRange(8, P_KTHT, 8 + cntPupil, P_Canam).NumberFormat("0.0");
                    sheet.ProtectSheet();
                    #endregion
                }
                else if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                {
                    patternSheet = oBook.GetSheet(2);
                    sheet = oBook.CopySheetToLast(patternSheet);
                    sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_LIST_SUBJECT_ID);
                    #region mon nhan xet

                    #region Tiêu đề template
                    //Change header

                    String title = "BẢNG ĐIỂM MÔN ";
                    title += listSubjectCat[l].DisplayName.ToUpper() + "";
                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        title += " HKI ";
                    else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        title += " HKII ";
                    else
                    {
                        title += " CẢ NĂM ";
                    }
                    title += " LỚP ";
                    title += classname.ToUpper();

                    sheet.SetCellValue("A2", acaYear.SchoolProfile.SchoolName);
                    sheet.SetCellValue("A4", title);
                    sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());

                    #endregion Tiêu đề template

                    //IDictionary<string, object> searchDeclation = new Dictionary<string, object>();
                    //searchDeclation["AcademicYearID"] = AcademicYearID;
                    //searchDeclation["SchoolID"] = SchoolID;
                    //searchDeclation["Semester"] = Semester;

                    #region xac dinh so dau diem

                    //SemeterDeclaration sd = SemeterDeclarationBusiness.Search(searchDeclation).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
                    string strMarkTypePeriod = string.Empty;
                    if (semeterDeclaration != null)
                    {
                        strMarkTypePeriod = MarkRecordBusiness.GetMarkSemesterTitle(semeterDeclaration.SemeterDeclarationID);
                    }

                    // Lấy con điểm bị khoá
                    string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, (int)Semester, subjectId);
                    string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
                    string[] listM = new string[listMarkType.Count()];
                    string[] listP = new string[listMarkType.Count()];
                    string[] listV = new string[listMarkType.Count()];

                    int cntM = 0, cntP = 0, cntV = 0;
                    if (listMarkType != null && listMarkType.Count() > 0)
                    {
                        foreach (string typeMark in listMarkType)
                        {
                            if (typeMark.Length < 2) continue;
                            if (typeMark[0] == 'M')
                            {
                                listM[cntM] = typeMark;
                                cntM++;
                            }
                            if (typeMark[0] == 'P')
                            {
                                listP[cntP] = typeMark;
                                cntP++;
                            }
                            if (typeMark[0] == 'V')
                            {
                                listV[cntV] = typeMark;
                                cntV++;
                            }
                        }
                    }
                    int totalMark = listMarkType.Count() - 1;
                    sheet.MergeRow(4, 1, (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 7 : 8) + totalMark);
                    sheet.MergeRow(5, 1, (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 7 : 8) + totalMark);
                    #endregion xac dinh so dau diem

                    int startM = 5;
                    int startP = startM + cntM;
                    int startV = startP + cntP;
                    int k = 0;
                    for (int j = startM; j < startP; j++)
                    {
                        copyCol(patternSheet, sheet, 5, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startP; j < startV; j++)
                    {
                        copyCol(patternSheet, sheet, 6, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startV; j < startV + cntV; j++)
                    {
                        copyCol(patternSheet, sheet, 7, startCol);
                        k++;
                    }

                    int P_TBM = startCol;
                    int P_KTHT = startCol;
                    int P_HK1 = startCol;
                    int P_Canam = startCol;

                    int P_HLM1 = startCol;
                    int P_HLM2 = startCol;
                    int P_Ghi_chu = startCol;

                    objValidation = new VTDataValidation
                    {
                        Contrains = cd,
                        FromColumn = startM,
                        FromRow = 8,
                        SheetIndex = totalSheet,
                        ToColumn = startV + cntV,
                        ToRow = 7 + lstPupilOfClasss.Count(),
                        Type = VTValidationType.LIST
                    };
                    lstValidation.Add(objValidation);

                    //copy cot KTHK
                    copyCol(patternSheet, sheet, 8, startCol);
                    sheet.SetCellValue(6, P_KTHT, "KTHK");

                    //Neu la hk 1
                    if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        // copy cot TBM HK 1
                        P_HLM1 = startCol;
                        copyCol(patternSheet, sheet, 9, startCol);
                        sheet.SetCellValue(6, P_HLM1, "HLM");
                        P_TBM = P_HLM1;
                    }
                    else
                    {
                        // copy cot TBM HK 2
                        P_HLM2 = startCol;
                        copyCol(patternSheet, sheet, 10, startCol);
                        sheet.SetCellValue(6, P_HLM2, "HLM");
                        //copy cot Canam
                        P_Canam = startCol;
                        copyCol(patternSheet, sheet, 11, startCol);
                        sheet.SetCellValue(6, P_Canam, "Cả năm");
                        P_TBM = P_HLM2;
                    }
                    // copy cot ghi chu
                    P_Ghi_chu = startCol;
                    copyCol(patternSheet, sheet, 12, startCol);
                    sheet.SetCellValue(6, P_Ghi_chu, "Ghi chú");
                    P_HK1 = P_KTHT;
                    ////copy cot HK
                    //P_HK1 = startCol;
                    //copyCol(firstSheet, sheet, 10, startCol);

                    ////copy cot Canam
                    //P_Canam = startCol;
                    //copyCol(firstSheet, sheet, 11, startCol);
                    //// copy cot ghi chu


                    //if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    //    copyCol(firstSheet, sheet, 11, startCol);

                    //Merger cot tieu de dau diem
                    sheet.MergeRow(6, startM, startV - 1);
                    sheet.MergeRow(6, startV, startV + cntV - 1);

                    //set tieu de cho cac cot dau diem
                    sheet.SetCellValue(6, startM, "Điểm hệ số 1");
                    sheet.SetCellValue(6, startV, "Điểm hệ số 2");

                    sheet.MergeRow(7, startM, startP - 1);
                    sheet.MergeRow(7, startP, startV - 1);
                    sheet.MergeRow(7, startV, startV + cntV - 1);

                    sheet.SetCellValue(7, startM, "Miệng");
                    sheet.SetCellValue(7, startP, "Viết/15P");
                    sheet.SetCellValue(7, startV, "Viết");

                    //Xac dinh so hoc sinh roi tao cac dong cho hs

                    #region tao cac dong trang cho ds hs

                    int RowEnd = 8;
                    if (cntPupil > 5)
                    {
                        int numberGroupStudent = cntPupil / 5;
                        if (cntPupil % 5 == 0)
                        {
                            numberGroupStudent = numberGroupStudent - 1;
                        }
                        IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Ghi_chu);
                        for (int j = 1; j <= numberGroupStudent; j++)
                        {
                            RowEnd = RowEnd + 5;
                            sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                        }
                    }

                    #endregion tao cac dong trang cho ds hs

                    int indexPupil = 0;
                    int startRow = 8;
                    List<int> lstPupilID = new List<int>();
                    var ExemptType = new ExemptedSubject();

                    listClassSubjectBySubjectId = listClassSubject.Where(p => p.SubjectID == subjectId).ToList();
                    bool isNoLearnFirst = listClassSubjectBySubjectId.Any(o => o.SectionPerWeekFirstSemester <= 0);
                    bool isNoLearnSecond = listClassSubjectBySubjectId.Any(o => o.SectionPerWeekSecondSemester <= 0);
                    bool isClassification = acaYear.IsClassification.HasValue && acaYear.IsClassification.Value ? true : false;

                    string TBM = string.Empty;
                    sheet.GetRange(8, 1, cntPupil + 7, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, 1, cntPupil + 7, P_Ghi_chu).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
                    sheet.GetRange(startRow, P_TBM, cntPupil + 7, P_Ghi_chu).IsLock = true;
                    sheet.GetRange(startRow, P_TBM, cntPupil + 7, P_Ghi_chu).FillColor(Color.Yellow);
                    sheet.SetColumnWidth(P_Ghi_chu, 297);
                    //tao luoi trong phan diem
                    sheet.GetRange(8, startM, cntPupil + 7, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startP, cntPupil + 7, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startV, cntPupil + 7, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);

                    for (int i = 0; i < cntPupil; i++)
                    {
                        if (i % 5 == 0)
                        {
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                        else
                        {
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                    }
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    foreach (PupilOfClassBO Pupil in lstPupilOfClasss)
                    {
                        if (!lstPupilID.Contains(Pupil.PupilID))
                        {
                            lstPupilID.Add(Pupil.PupilID);

                            //cot stt
                            indexPupil++;
                            sheet.SetCellValue(startRow, 1, indexPupil.ToString());

                            //ma hoc sinh
                            sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                            sheet.GetRange(startRow, 3, startRow, 3).WrapText();
                            string stM = convertPosition(startM, startRow);
                            string stP = convertPosition(startP, startRow);
                            string stV = convertPosition(startV, startRow);

                            string edM = convertPosition(startP - 1, startRow);
                            string edP = convertPosition(startV - 1, startRow);
                            string edV = convertPosition(P_HK1 - 3, startRow);
                            string HK = convertPosition(P_HK1, startRow);

                            if (isMinSemester)
                            {
                                //
                                if (isClassification)
                                {
                                    TBM = "=IF(AND(COUNTIF(" + stM + ":" + edM + ",\"**\") >= " + minM + ",COUNTIF(" + stP + ":" + edP + ",\"**\") >= " + minP + ",COUNTIF(" + stV + ":" + edV + ",\"**\") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\",IF(COUNTIF(" + stM + ":" + HK + "," + "\"" + "Đ" + "\"" + ")*3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0, \"Đ\", \"CĐ\"),\"CĐ\"),\"\"),\"\")";
                                }
                                else
                                {
                                    TBM = "=IF(AND(COUNTIF(" + stM + ":" + edM + ",\"**\") >= " + minM + ",COUNTIF(" + stP + ":" + edP + ",\"**\") >= " + minP + ",COUNTIF(" + stV + ":" + edV + ",\"**\") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\",IF(COUNTIF(" + stM + ":" + HK + "," + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3, 0) >=0, \"Đ\", \"CĐ\"),\"CĐ\"),\"\"),\"\")";
                                }
                            }
                            else
                            {
                                if (isClassification)
                                {
                                    TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\";IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ")*3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0; \"Đ\"; \"CĐ\");\"CĐ\");\"\")";
                                }
                                else
                                {
                                    TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\";IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0; \"Đ\"; \"CĐ\");\"CĐ\");\"\")";
                                }
                            }

                            sheet.SetFormulaValue(startRow, P_TBM, TBM);

                            // sheet.LockSheet = true;
                            // sheet.ProtectSheet();
                            sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                            sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                            //IVTRange rangeStyle1 = patternSheet.GetRange("D13", "D13");
                            //IVTRange rangeStyle2 = patternSheet.GetRange("D17", "D17");
                            //IVTRange rangeStyle3 = patternSheet.GetRange("D14", "D14");

                            if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                                || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF
                                || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED
                              )
                            {
                                sheet.GetRange(startRow, 1, startRow, P_Ghi_chu).FillColor(Color.Yellow);
                                sheet.GetRange(startRow, 1, startRow, P_Ghi_chu).IsLock = true;
                                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                                //sheet.Lock(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                            }
                            else
                            {
                                sheet.GetRange(startRow, 5, startRow, P_Ghi_chu).IsLock = false;
                                sheet.GetRange(startRow, P_Ghi_chu, startRow, P_Ghi_chu).IsLock = true;
                            }

                            //Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                            DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                            DateTime? endDate = Pupil.EndDate;
                            bool showPupilData = false;

                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                                {
                                    showPupilData = true;
                                }
                            }
                            else
                            {
                                showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                            }

                            if (!showPupilData)
                            {
                                startRow++;
                                continue;
                            }

                            if (lstExemptedSubject.Exists(p => p.PupilID == Pupil.PupilID && p.SubjectID == subjectId) && (lstExemptedSubject.Count(p => p.FirstSemesterExemptType > 0) > 0 || lstExemptedSubject.Count(p => p.SecondSemesterExemptType > 0) > 0))
                            {
                                sheet.GetRange(startRow, 1, startRow, P_Ghi_chu).FillColor(Color.Yellow);
                                sheet.GetRange(startRow, 1, startRow, P_Ghi_chu).IsLock = true;
                            }
                            var listJudgeRecordPupil = listJudgeRecord.Where(o => o.PupilID == Pupil.PupilID && o.SubjectID == subjectId);

                            //ho va ten
                            //Lay diem tung dau diem hoc sinh
                            int col = 5;
                            int j = 0;
                            for (int i = startM; i < startP; i++)
                            {
                                String titleMark = listM[j];
                                JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                                sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                                if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                {
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                                }
                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startP; i < startV; i++)
                            {
                                String titleMark = listP[j];
                                JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                                sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                                if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                {
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                                }
                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startV; i < P_TBM - 1; i++)
                            {
                                String titleMark = listV[j];
                                JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                                sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                                if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                {
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                                }
                                col++;
                                j++;
                            }

                            //diem kiem tra hk
                            JudgeRecordBO judgeRecordItemHK = listJudgeRecordPupil.Where(o => o.Title.Equals("HK")).FirstOrDefault();
                            sheet.SetCellValue(startRow, col, judgeRecordItemHK != null ? judgeRecordItemHK.Judgement : string.Empty);
                            if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                            {
                                sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                            }

                            IEnumerable<SummedUpRecordBO> listSummedUpRecord = listSummerupRecord.Where(o => o.PupilID == Pupil.PupilID && o.SubjectID == subjectId);
                            if (listSummedUpRecord != null && listSummedUpRecord.Count() > 0)
                            {
                                SummedUpRecordBO SummedUpRecord = listSummedUpRecord.FirstOrDefault();
                                sheet.SetCellValue(startRow, 40, SummedUpRecord.JudgementResult);
                            }
                            string formularTBHK = "=IF(" + convertPosition(P_TBM, startRow) + "<>\"\",IF(" + convertPosition(P_TBM, startRow) + "=\"Đ\", \"Đ\", \"CĐ\"),\"\")";
                            if (Pupil.SecondSemesterExemptType != null || isNoLearnSecond)
                            {
                                formularTBHK = "=" + convertPosition(P_HK1, startRow);
                            }

                            if (Pupil.FirstSemesterExemptType != null || isNoLearnFirst)
                            {
                                formularTBHK = "=" + convertPosition(P_TBM, startRow);
                            }
                            //string formularTBHK = "=I
                            //sheet.ProtectSheet();
                            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                sheet.SetFormulaValue(startRow, P_Canam, formularTBHK);

                            // Cot ghi chu
                            string note = string.Empty;
                            note = "=IF(AND(AN" + startRow + "<>\"\"," + convertPosition(P_TBM, startRow) + "<>\"\")," + "IF(AN" + startRow + "<>" + convertPosition(P_TBM, startRow) + ",\"HLM trong file excel khác HLM trong hệ thống\",\"\"), \"\")";
                            sheet.SetFormulaValue(startRow, P_Ghi_chu, note);

                            startRow++;
                        }
                    }
                    //sheet.SetColumnWidth(P_HK1, 0);
                    sheet.SetColumnWidth(40, 0);
                    sheet.SetColumnWidth(2, 0);
                    //sheet.DeleteRow(8 + cntPupil);
                    if (cntPupil <= 10)
                    {
                        int totalRecordNotFill = 10 + 8; // 8 bat dau tu dong thu 8
                        int totalRecordDelete = cntPupil + 8;
                        for (int i = totalRecordNotFill; i >= totalRecordDelete; i--)
                        {
                            sheet.DeleteRow(i);
                        }
                    }
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.GetRange(8 + cntPupil - 1, 1, 8 + cntPupil - 1, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                        sheet.MergeRow(4, 1, P_Canam - 1);
                        sheet.SetCellValue("A4", title);
                        sheet.MergeRow(5, 1, P_Canam - 1);
                        sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
                    }
                    else
                    {
                        sheet.GetRange(8 + cntPupil - 1, 1, 8 + cntPupil - 1, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                        sheet.MergeRow(4, 1, P_Canam);
                        sheet.SetCellValue("A4", title);
                        sheet.MergeRow(5, 1, P_Canam);
                        sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
                    }
                    sheet.Name = Utils.StripVNSignAndSpace(listSubjectCat[l].DisplayName);
                    sheet.SetFontName("Times New Roman", 11);
                    sheet.ProtectSheet();
                    #endregion
                }
            }
            #endregion
            oBook.GetSheet(2).Delete();
            oBook.GetSheet(1).Delete();
            //emptySheet.Delete();
            //firstSheet.Delete();          
            //decimal fromNum = 0;
            //decimal toNum = 10;
            // return oBook.ToStreamNumberValidationData(1, fromNum, toNum, 8, 5, 8 + cntPupil, startV + cntV);
            return oBook.ToStreamValidationData(lstValidation);
        }
        #endregion

        #region Xuat excel Danh sach diem theo mon hoc boi danh sach lop
        public Stream ExportReportForSemesterByListClassId(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, List<int> listClassID, int SubjectID, int subjectIDIncrease, int isCommenting, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null)
        {

            #region Lay template, tao ten file
            string reportCode = "BangDiemCacMon_23_HKII";

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string classname = string.Empty;

            // outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subject));
            // outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(classname));
            // outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));

            #endregion

            #region Danh sach mon hoc theo Id truyen vao
            List<ClassProfile> listClassProfile = (from cp in ClassProfileBusiness.All
                                                   where listClassID.Contains(cp.ClassProfileID)
                                                   && cp.IsActive == true
                                                   select cp).ToList();
            //SubjectCatBusiness.All.Where(p => listSubjectID.Contains(p.SubjectCatID) && p.AppliedLevel == AppliedLevel).ToList();
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            #endregion

            #region xu ly lock template
            //Lấy sheet template
            //IVTWorksheet firstSheet = oBook.GetSheet(1);
            //IVTWorksheet emptySheet = oBook.GetSheet(2);
            //IVTWorksheet sheet = oBook.GetSheet(3);

            // firstSheet.LockSheet = true;
            //firstSheet.ProtectSheet();
            // sheet.LockSheet = true;

            //Xoa bang thua
            //sheet.DeleteRow(13);
            //sheet.DeleteRow(14);
            //sheet.DeleteRow(15);
            //sheet.DeleteRow(16);
            //sheet.DeleteRow(17);


            //IVTRange range = emptySheet.GetRange("E6", "H17");
            // sheet.CopyPasteSameSize(range, "E6");
            #endregion

            #region Tiêu đề template
            //Change header
            IVTWorksheet sheet = null;
            IVTWorksheet patternSheet = null;
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            string className = string.Empty;
            string schoolName = objSP.SchoolName;
            string strTitleAcademicYear = objAy.DisplayTitle;
            #endregion

            #region Dem so hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ListClassID"] = listClassID;
            //dic["Check"] = "Check";
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).AddPupilStatus(isNotShowPupil);
            List<ExemptedSubject> lstExemptedSubject = ExemptedSubjectBusiness.All
                .Where(p => p.AcademicYearID == acaYear.AcademicYearID && listClassID.Contains(p.ClassID)
                    && p.SubjectID == SubjectID
                    && ((Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && p.FirstSemesterExemptType > 0)
                     || (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && p.SecondSemesterExemptType > 0))
                    ).ToList();

            List<PupilOfClassBO> lstPupilOfClasss = (from poc in listPupilOfClass
                                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                     select new PupilOfClassBO
                                                     {
                                                         PupilOfClassID = poc.PupilOfClassID,
                                                         PupilID = poc.PupilID,
                                                         ClassID = poc.ClassID,
                                                         SchoolID = poc.SchoolID,
                                                         AcademicYearID = poc.AcademicYearID,
                                                         Year = poc.Year,
                                                         AssignedDate = poc.AssignedDate,
                                                         OrderInClass = poc.OrderInClass,
                                                         Description = poc.Description,
                                                         NoConductEstimation = poc.NoConductEstimation,
                                                         Status = poc.Status,
                                                         EndDate = poc.EndDate,
                                                         PupilCode = pp.PupilCode,
                                                         PupilFullName = pp.FullName,
                                                         Name = pp.Name,
                                                         EthnicName = pp.Ethnic != null ? pp.Ethnic.EthnicName : null,
                                                         EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : null
                                                     }).ToList();
            List<PupilOfClassBO> lstPupilByClassID = new List<PupilOfClassBO>();

            List<int> listPupilId = new List<int>();
            if (listPupilOfClass != null && listPupilOfClass.Count() > 0)
            {
                listPupilId = listPupilOfClass.Select(p => p.PupilID).Distinct().ToList();
            }
            #endregion

            #region Danh sach diem tinh diem
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            search["Semester"] = Semester;
            SemeterDeclaration semeterDeclaration = SemeterDeclarationBusiness.Search(search).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();

            search = new Dictionary<string, object>();
            search["Year"] = acaYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            //search["ClassID"] = ClassID;
            //search["ListPupilID"] = listPupilId;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["Year"] = acaYear.Year;
            if (isAdmin.HasValue && isAdmin == true)
            {
                search["EducationLevelID"] = EducationLevelID;
            }
            List<MarkRecordBO> listMarkRecord = VMarkRecordBusiness.SearchMarkHistoryOrNoHistory(search).ToList();
            List<MarkRecordBO> listMarkRecordBySubjectID = new List<MarkRecordBO>();

            //ListSubjectID
            search = new Dictionary<string, object>();
            search["Year"] = acaYear.Year;
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            //search["ListPupilID"] = listPupilId;
            //search["SubjectID"] = SubjectID;
            //search["Semester"] = Semester;
            if (isAdmin.HasValue && isAdmin.Value == true)
            {
                search["EducationLevelID"] = EducationLevelID;
            }
            List<SummedUpRecordBO> listSummerupRecord = VSummedUpRecordBusiness.SearchSummedUpRecord(search).Where(o => o.PeriodID == null).ToList();
            List<SummedUpRecordBO> listSummerupRecordBySubjectID = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> listSummerupRecordByClassID = new List<SummedUpRecordBO>();

            List<SummedUpRecordBO> listSummerupRecordI = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> listSummerupRecordIBySubjectID = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> listSummerupRecordIByClassID = new List<SummedUpRecordBO>();
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listSummerupRecordI = listSummerupRecord.Where(p => p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            }
            else
            {
                listSummerupRecordI = listSummerupRecord;
            }

            #endregion

            #region Danh sach diem nhan xet

            search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["SchoolID"] = SchoolID;
            //search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["checkWithClassMovement"] = "1";
            search["Year"] = acaYear.Year;
            if (isAdmin.HasValue && isAdmin == true)
            {
                search["EducationLevelID"] = EducationLevelID;
            }
            List<JudgeRecordBO> listJudgeRecord = VJudgeRecordBusiness.SearchJudgeRecordNoView(search).ToList();

            #endregion

            #region Object môn hoc
            //int classFirstInList = listClassID.FirstOrDefault();
            List<ClassSubject> listclassSubject = (from cs in ClassSubjectBusiness.All
                                                   join poc in PupilOfClassBusiness.All on cs.ClassID equals poc.ClassID
                                                   where listClassID.Contains(poc.ClassID)
                                                   && poc.AcademicYearID == AcademicYearID
                                                   && cs.SubjectID == SubjectID
                                                   select cs).ToList();
            ClassSubject classSubjectObj = null;
            SubjectCat subjectCatObj = SubjectCatBusiness.All.Where(p => p.SubjectCatID == SubjectID).FirstOrDefault();
            if (listClassID != null && listClassID.Count > 1)
            {
                FileName = "BangDiemMon_" + Utils.StripVNSign(subjectCatObj.DisplayName) + "_Caclop_" + semester + ".xls";
            }
            else
            {
                FileName = "BangDiemMon_" + Utils.StripVNSign(subjectCatObj.DisplayName) + "_" + Utils.StripVNSign(listClassProfile[0].DisplayName) + "_" + semester + ".xls";
            }
            #endregion

            int classId = 0;
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation;
            #region for qua danh sach lop hoc
            int totalSheet = 0;
            string[] cd = new string[] { GlobalConstants.SHORT_OK, GlobalConstants.SHORT_NOT_OK };
            VTDataValidation objValidationNumber;
            for (int l = 0; l < listClassProfile.Count; l++)
            {
                totalSheet++;
                startCol = 5;
                classId = listClassProfile[l].ClassProfileID;
                classname = listClassProfile[l].DisplayName;

                //  lay thong tin mon hoc cua lop
                classSubjectObj = listclassSubject.Where(p => p.ClassID == classId).FirstOrDefault();
                //lay thong tin cau hinh so con diem toi thieu
                bool isMinSemester = false;
                int minM = 0;
                int minP = 0;
                int minV = 0;
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinFirstSemester.HasValue && classSubjectObj.MMinFirstSemester.Value > 0)
                                       || classSubjectObj.PMinFirstSemester.HasValue && classSubjectObj.PMinFirstSemester.Value > 0
                                       || classSubjectObj.VMinFirstSemester.HasValue && classSubjectObj.VMinFirstSemester.Value > 0);
                    minM = classSubjectObj != null && classSubjectObj.MMinFirstSemester.HasValue ? classSubjectObj.MMinFirstSemester.Value : 0;
                    minP = classSubjectObj != null && classSubjectObj.PMinFirstSemester.HasValue ? classSubjectObj.PMinFirstSemester.Value : 0;
                    minV = classSubjectObj != null && classSubjectObj.VMinFirstSemester.HasValue ? classSubjectObj.VMinFirstSemester.Value : 0;
                }
                else
                {
                    isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinSecondSemester.HasValue && classSubjectObj.MMinSecondSemester.Value > 0)
                                       || classSubjectObj.PMinSecondSemester.HasValue && classSubjectObj.PMinSecondSemester.Value > 0
                                       || classSubjectObj.VMinSecondSemester.HasValue && classSubjectObj.VMinSecondSemester.Value > 0);
                    minM = classSubjectObj != null && classSubjectObj.MMinSecondSemester.HasValue ? classSubjectObj.MMinSecondSemester.Value : 0;
                    minP = classSubjectObj != null && classSubjectObj.PMinSecondSemester.HasValue ? classSubjectObj.PMinSecondSemester.Value : 0;
                    minV = classSubjectObj != null && classSubjectObj.VMinSecondSemester.HasValue ? classSubjectObj.VMinSecondSemester.Value : 0;
                }

                if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                {
                    lstPupilByClassID = lstPupilOfClasss.Where(p => p.ClassID == classId).ToList().OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
                    System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                    patternSheet = oBook.GetSheet(1);
                    sheet = oBook.CopySheetToLast(patternSheet);
                    sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID);
                    objValidationNumber = new VTDataValidation();
                    #region Mon tinh diem
                    String title = "BẢNG ĐIỂM MÔN ";
                    title += subjectCatObj.DisplayName.ToUpper() + "";
                    title += " " + ReportUtils.ConvertSemesterForReportName(Semester) + " ";
                    title += " LỚP ";
                    title += classname.ToUpper();
                    sheet.SetCellValue("A2", schoolName);
                    sheet.SetCellValue("A4", title);
                    sheet.SetCellValue("A5", strTitleAcademicYear.ToUpper());

                    #region xac dinh so dau diem
                    string strMarkTypePeriod = this.GetMarkSemesterTitle(semeterDeclaration.SemeterDeclarationID);
                    string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
                    string[] listM = new string[listMarkType.Count()];
                    string[] listP = new string[listMarkType.Count()];
                    string[] listV = new string[listMarkType.Count()];

                    int cntM = 0, cntP = 0, cntV = 0;
                    foreach (string typeMark in listMarkType)
                    {
                        if (typeMark.Length < 2) continue;

                        if (typeMark[0] == 'M')
                        {
                            listM[cntM] = typeMark;
                            cntM++;
                        }
                        if (typeMark[0] == 'P')
                        {
                            listP[cntP] = typeMark;
                            cntP++;
                        }
                        if (typeMark[0] == 'V')
                        {
                            listV[cntV] = typeMark;
                            cntV++;
                        }
                    }
                    #endregion

                    #region tao column chuan cho sheetData
                    int startM = 5;
                    int startP = startM + cntM;
                    int startV = startP + cntP;
                    int k = 0;
                    for (int j = startM; j < startP; j++)
                    {
                        copyCol(patternSheet, sheet, 5, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startP; j < startV; j++)
                    {
                        copyCol(patternSheet, sheet, 6, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startV; j < startV + cntV; j++)
                    {
                        copyCol(patternSheet, sheet, 7, startCol);
                        k++;
                    }

                    // int P_Nhanxet = startCol;
                    int P_TBMTC = startCol;
                    int P_TBM = startCol;
                    int P_KTHT = startCol;
                    int P_HK1 = startCol;
                    int P_Canam = startCol;
                    int P_TBMTmp = startCol;

                    //copy cot KTHK
                    P_KTHT = startCol;

                    //copy cot KTHK
                    copyCol(patternSheet, sheet, 8, startCol);

                    //copy cot TBMTC
                    P_TBMTC = startCol;
                    copyCol(patternSheet, sheet, 9, startCol);

                    //copy cot TBMTmp
                    P_TBMTmp = startCol;
                    copyCol(patternSheet, sheet, 10, startCol);

                    //copy cot TBM
                    P_TBM = startCol;
                    copyCol(patternSheet, sheet, 11, startCol);

                    //copy cot HK
                    P_HK1 = startCol;
                    copyCol(patternSheet, sheet, 12, startCol);

                    P_Canam = startCol;
                    copyCol(patternSheet, sheet, 13, startCol);



                    //sheet.GetRange("a2", "a3").SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideHorizontal);
                    //Merger cot tieu de dau diem
                    sheet.MergeRow(6, startM, startV - 1);
                    sheet.MergeRow(6, startV, startV + cntV - 1);

                    //set tieu de cho cac cot dau diem
                    sheet.SetCellValue(6, startM, "Điểm hệ số 1");
                    sheet.SetCellValue(6, startV, "Điểm hệ số 2");

                    sheet.MergeRow(7, startM, startP - 1);
                    sheet.MergeRow(7, startP, startV - 1);
                    sheet.MergeRow(7, startV, startV + cntV - 1);

                    sheet.SetCellValue(7, startM, "Miệng");
                    sheet.SetCellValue(7, startP, "Viết/15P");
                    sheet.SetCellValue(7, startV, "Viết");

                    #endregion

                    string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, classId, (int)Semester, SubjectID);

                    #region Lay du lieu diem theo  mon hoc
                    listMarkRecordBySubjectID = listMarkRecord.Where(p => p.SubjectID == SubjectID && p.ClassID == classId).ToList();
                    listSummerupRecordBySubjectID = listSummerupRecord.Where(p => p.SubjectID == SubjectID && p.ClassID == classId && p.Semester == Semester).ToList();
                    listSummerupRecordIBySubjectID = listSummerupRecordI.Where(p => p.SubjectID == SubjectID && p.ClassID == classId).ToList();
                    #endregion

                    #region Fill thong diem cua hoc sinh
                    int indexPupil = 0;
                    int startRow = 8;

                    //DateTime endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate.Value : acaYear.SecondSemesterEndDate.Value;


                    bool isNoLearnFirst = (classSubjectObj != null && classSubjectObj.SectionPerWeekFirstSemester <= 0) ? true : false;
                    bool isNoLearnSecond = (classSubjectObj != null && classSubjectObj.SectionPerWeekSecondSemester <= 0) ? true : false;
                    int cntPupil = lstPupilByClassID.Count();
                    sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Canam).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    //tao luoi trong phan diem
                    sheet.GetRange(8, startM, cntPupil + 7, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startP, cntPupil + 7, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startV, cntPupil + 7, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);

                    sheet.GetRange(startRow, P_TBMTC, cntPupil + 8, P_Canam).IsLock = true;
                    sheet.GetRange(8, 1, cntPupil + 8, 1).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignRight);//STT
                    sheet.GetRange(8, 3, cntPupil + 8, 3).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignCenter);//ma hoc sinh
                    sheet.GetRange(8, 4, cntPupil + 8, 4).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignLeft);// tem hoc sinh

                    objValidationNumber = new VTDataValidation
                    {
                        FromValue = "0",
                        ToValue = "10",
                        SheetIndex = totalSheet,
                        Type = VTValidationType.INTEGER,
                        FromColumn = 5,
                        ToColumn = startP - 1,
                        FromRow = 8,
                        ToRow = lstPupilOfClasss.Count + 7
                    };
                    lstValidation.Add(objValidationNumber);

                    if (objSP.TrainingTypeID == 3)//truong GDTX
                    {
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "0",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.INTEGER,
                            FromColumn = startP,
                            ToColumn = startV + cntV - 1,
                            FromRow = 8,
                            ToRow = lstPupilByClassID.Count + 7
                        };
                        lstValidation.Add(objValidationNumber);
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "0",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.DECIMAL,
                            FromColumn = P_KTHT,
                            ToColumn = P_KTHT,
                            FromRow = 8,
                            ToRow = cntPupil + 7,
                            MinLength = "0",
                            MaxLength = "3",
                        };
                        lstValidation.Add(objValidationNumber);
                    }
                    else
                    {
                        objValidationNumber = new VTDataValidation
                        {
                            FromValue = "0",
                            ToValue = "10",
                            SheetIndex = totalSheet,
                            Type = VTValidationType.DECIMAL,
                            FromColumn = startP,
                            ToColumn = startV + cntV,
                            FromRow = 8,
                            ToRow = lstPupilByClassID.Count + 7,
                            MinLength = "1",
                            MaxLength = "3",
                        };
                        lstValidation.Add(objValidationNumber);
                    }

                    if (subjectIDIncrease <= 0)
                    {
                        sheet.HideColumn(P_TBMTC);
                    }

                    for (int i = 0; i < cntPupil; i++)
                    {
                        if (i % 5 == 0)
                        {
                            sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                            sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                        else
                        {
                            sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                            sheet.GetRange(i + 8, 1, i + 8, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                    }


                    foreach (PupilOfClassBO Pupil in lstPupilByClassID)
                    {
                        //cot stt
                        indexPupil++;
                        sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                        sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                        sheet.GetRange(startRow, 3, startRow, 3).WrapText();

                        string stM = convertPosition(startM, startRow);
                        string stP = convertPosition(startP, startRow);
                        string stV = convertPosition(startV, startRow);

                        string edM = convertPosition(startP - 1, startRow);
                        string edP = convertPosition(startV - 1, startRow);
                        string edV = convertPosition(P_KTHT - 1, startRow);

                        sheet.GetRange(startRow, P_TBMTC, startRow, P_Canam).FillColor(Color.Yellow);

                        //ho va ten
                        string cntMark = "=1*COUNTA(" + stM + ":" + edM + ")+" + "1*COUNTA(" + stP + ":" + edP + ")+" + "2*COUNTA(" + stV + ":" + edV + ")";
                        //string cntMark = "=1*COUNTIF(" + stM + ":" + edM + ";" + "\">=0\")";
                        sheet.SetFormulaValue(startRow, 2, cntMark);

                        string TBMTC = string.Empty;
                        string TBM = string.Empty;
                        string TBMTmp = string.Empty;

                        if (isMinSemester)
                        {
                            if (subjectIDIncrease > 0)
                            {
                                //Lay TBM cua mon tang cuong
                                SummedUpRecordBO surIIncrease = listSummerupRecord.FirstOrDefault(u => u.PupilID == Pupil.PupilID && u.ClassID == classId && u.SubjectID == subjectIDIncrease && u.Semester == Semester);
                                if (surIIncrease != null && (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED))
                                {
                                    sheet.SetCellValue(convertPosition(P_TBMTC, startRow), surIIncrease.SummedUpMark);
                                }
                                //fill cong thuc cho mon tang cuong
                                TBM = "=IF(AND(COUNT(" + stM + ":" + edM + ") >= " + minM + ",COUNT(" + stP + ":" + edP + ") >= " + minP + ",COUNT(" + stV + ":" + edV + ") >= " + minV + "),IF(AND(" + convertPosition(P_TBMTC, startRow) + " <>\"\"," + convertPosition(P_TBMTmp, startRow) + "<>\"\"),IF(" + convertPosition(P_TBMTmp, startRow) + " +IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0)))>10,10," + convertPosition(P_TBMTmp, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0))))," + convertPosition(P_TBMTmp, startRow) + "),\"\")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                            else
                            {
                                //Fill cong thuc cho mon khong tang cuong   
                                TBM = "=IF(AND(COUNT(" + stM + ":" + edM + ") >= " + minM + ",COUNT(" + stP + ":" + edP + ") >= " + minP + ",COUNT(" + stV + ":" + edV + ") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(2, startRow) + "<>\"\",ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3),1),\"\"),\"\"),\"\")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                        }
                        else
                        {
                            if (subjectIDIncrease > 0)
                            {
                                //Lay TBM cua mon tang cuong
                                SummedUpRecordBO surIIncrease = listSummerupRecord.FirstOrDefault(u => u.PupilID == Pupil.PupilID && u.ClassID == classId && u.SubjectID == subjectIDIncrease && u.Semester == Semester);
                                if (surIIncrease != null && (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED))
                                {
                                    sheet.SetCellValue(convertPosition(P_TBMTC, startRow), surIIncrease.SummedUpMark);
                                }
                                //fill cong thuc cho mon tang cuong
                                TBM = "=IF(AND(" + convertPosition(P_TBMTC, startRow) + " <>\"\"," + convertPosition(P_TBMTmp, startRow) + "<>\"\"),IF(" + convertPosition(P_TBMTmp, startRow) + " +IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0)))>10,10," + convertPosition(P_TBMTmp, startRow) + "+IF(" + convertPosition(P_TBMTC, startRow) + ">=8,ROUND((1/3),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=6.5,ROUND((1/5),1),IF(" + convertPosition(P_TBMTC, startRow) + ">=5,ROUND((1/7),1),0))))," + convertPosition(P_TBMTmp, startRow) + ")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                            else
                            {
                                //Fill cong thuc cho mon khong tang cuong   
                                TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(2, startRow) + "<>\"\";ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3);1);\"\");\"\")";
                                sheet.SetFormulaValue(startRow, P_TBM, TBM);
                            }
                        }


                        //Fill TBMTmp
                        TBMTmp = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(2, startRow) + "<>\"\",ROUND(((1*SUM(" + stM + ":" + edM + ")+1*SUM(" + stP + ":" + edP + ")+2*SUM(" + stV + ":" + edV + "))+" + convertPosition(P_KTHT, startRow) + "*3)/(" + convertPosition(2, startRow) + "+3),1),\"\"),\"\")";
                        sheet.SetFormulaValue(startRow, P_TBMTmp, TBMTmp);
                        sheet.HideColumn(P_TBMTmp);


                        sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                        sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                        //IVTRange rangeStyle1 = patternSheet.GetRange("D13", "D13");
                        //IVTRange rangeStyle2 = patternSheet.GetRange("D17", "D17");
                        //IVTRange rangeStyle3 = patternSheet.GetRange("D14", "D14");

                        bool showData = true;
                        if (Pupil.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            sheet.GetRange(startRow, startM, startRow, P_TBM - 1).IsLock = false;
                        }
                        else
                        {
                            // showData = (!Pupil.EndDate.HasValue || Pupil.EndDate.Value > endSemester);
                            sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                            sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                            sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                        }

                        //Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                        DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                        DateTime? endDate = Pupil.EndDate;
                        bool showPupilData = false;

                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                            {
                                showPupilData = true;
                            }
                        }
                        else
                        {
                            showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                            || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                            || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                        }

                        if (!showPupilData)
                        {
                            startRow++;
                            continue;
                        }

                        if ((lstExemptedSubject.Count(p => p.PupilID == Pupil.PupilID && p.ClassID == classId && p.FirstSemesterExemptType > 0) > 0)
                            || (lstExemptedSubject.Count(p => p.PupilID == Pupil.PupilID && p.ClassID == classId && p.SecondSemesterExemptType > 0) > 0))
                        {
                            sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                            sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                        }

                        if (showData)
                        {
                            //Lay diem tung dau diem hoc sinh
                            int col = 5;
                            int j = 0;
                            for (int i = startM; i < startP; i++)
                            {
                                MarkRecordBO markRecord = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals(listM[j]));
                                if (markRecord != null)
                                    sheet.SetCellValue(startRow, col, markRecord.Mark);
                                else
                                    sheet.SetCellValue(startRow, col, string.Empty);

                                if ((strLockMarkType.Contains(listM[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startP; i < startV; i++)
                            {
                                MarkRecordBO markRecord = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals(listP[j]));
                                if (markRecord != null)
                                    sheet.SetCellValue(startRow, col, markRecord.Mark);
                                else
                                    sheet.SetCellValue(startRow, col, "");

                                if ((strLockMarkType.Contains(listP[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startV; i < startV + cntV; i++)
                            {
                                MarkRecordBO markRecord = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals(listV[j]));
                                if (markRecord != null)
                                    sheet.SetCellValue(startRow, col, markRecord.Mark);
                                else
                                    sheet.SetCellValue(startRow, col, "");

                                if ((strLockMarkType.Contains(listV[j]) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;

                                col++;
                                j++;

                                if (cntV < 3)
                                {
                                    sheet.SetColumnWidth(i, 8);
                                }
                            }

                            MarkRecordBO markRecordHK = listMarkRecordBySubjectID.Where(o => o.PupilID == Pupil.PupilID).FirstOrDefault(o => o.Title.Equals("HK"));
                            if (markRecordHK != null)
                                sheet.SetCellValue(startRow, P_KTHT, markRecordHK.Mark);
                            if ((strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                sheet.GetRange(startRow, P_KTHT, startRow, P_KTHT).IsLock = true;

                            SummedUpRecordBO surI = listSummerupRecordIBySubjectID.FirstOrDefault(u => u.PupilID == Pupil.PupilID);
                            if (surI != null)
                                sheet.SetCellValue(startRow, P_HK1, surI.SummedUpMark);

                            string formularTBHK = "=IF(" + convertPosition(P_HK1, startRow) + "<>\"\";IF(" + convertPosition(P_TBM, startRow) + "<>\"\";ROUND((" + convertPosition(P_HK1, startRow) + "+2*" + convertPosition(P_TBM, startRow) + ")/3;1);\"\");\"\")";


                            if (Pupil.SecondSemesterExemptType != null || isNoLearnSecond)
                            {
                                formularTBHK = "=" + convertPosition(P_HK1, startRow);
                            }

                            if (Pupil.FirstSemesterExemptType != null || isNoLearnFirst)
                            {
                                formularTBHK = "=" + convertPosition(P_TBM, startRow);
                            }

                            sheet.SetFormulaValue(startRow, P_Canam, formularTBHK);
                        }
                        startRow++;
                    }
                    if (cntPupil <= 10)
                    {
                        int totalRecordNotFill = 10 + 8; // 8 bat dau tu dong thu 8
                        int totalRecordDelete = cntPupil + 8;
                        for (int i = totalRecordNotFill; i >= totalRecordDelete; i--)
                        {
                            sheet.DeleteRow(i);
                        }
                    }
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Canam).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    #endregion


                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.SetColumnWidth(P_Canam, 0);
                    }

                    sheet.SetColumnWidth(P_HK1, 0);
                    sheet.SetColumnWidth(2, 0);

                    if (Semester == 1)
                    {
                        sheet.MergeRow(4, 1, P_TBM);
                        sheet.MergeRow(5, 1, P_TBM);
                        sheet.SetCellValue("A4", title);
                        sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
                    }
                    else
                    {
                        sheet.MergeRow(4, 1, P_Canam);
                        sheet.MergeRow(5, 1, P_Canam);
                        sheet.SetCellValue("A4", title);
                        sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());
                    }
                    sheet.Name = Utils.StripVNSignAndSpace(classname);
                    sheet.ProtectSheet();
                    #endregion
                    sheet.GetRange(8, P_KTHT, 8 + cntPupil, P_Canam).NumberFormat("0.0");
                    sheet.SetFontName("Times New Roman", 11);
                    sheet.ProtectSheet();
                }
                else if (isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                {
                    lstPupilByClassID = lstPupilOfClasss.Where(p => p.ClassID == classId).ToList().OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
                    patternSheet = oBook.GetSheet(2);
                    sheet = oBook.CopySheetToLast(patternSheet);
                    sheet.SetCellValue("B3", GlobalConstants.EXPORT_MARK_RECORD_LIST_CLASS_ID);
                    #region mon nhan xet

                    #region Tiêu đề template
                    //Change header

                    String title = "BẢNG ĐIỂM MÔN ";
                    title += subjectCatObj.DisplayName.ToUpper() + "";
                    if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        title += " HKI ";
                    else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        title += " HKII ";
                    else
                    {
                        title += " CẢ NĂM ";
                    }
                    title += " LỚP ";
                    title += classname.ToUpper();

                    sheet.SetCellValue("A2", acaYear.SchoolProfile.SchoolName);
                    sheet.SetCellValue("A4", title);
                    sheet.SetCellValue("A5", acaYear.DisplayTitle.ToUpper());

                    #endregion Tiêu đề template

                    //IDictionary<string, object> searchDeclation = new Dictionary<string, object>();
                    //searchDeclation["AcademicYearID"] = AcademicYearID;
                    //searchDeclation["SchoolID"] = SchoolID;
                    //searchDeclation["Semester"] = Semester;

                    #region xac dinh so dau diem

                    //SemeterDeclaration sd = SemeterDeclarationBusiness.Search(searchDeclation).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
                    string strMarkTypePeriod = string.Empty;
                    if (semeterDeclaration != null)
                    {
                        strMarkTypePeriod = MarkRecordBusiness.GetMarkSemesterTitle(semeterDeclaration.SemeterDeclarationID);
                    }

                    // Lấy con điểm bị khoá
                    string strLockMarkType = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, classId, (int)Semester, SubjectID);
                    string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' });
                    string[] listM = new string[listMarkType.Count()];
                    string[] listP = new string[listMarkType.Count()];
                    string[] listV = new string[listMarkType.Count()];

                    int cntM = 0, cntP = 0, cntV = 0;
                    if (listMarkType != null && listMarkType.Count() > 0)
                    {
                        foreach (string typeMark in listMarkType)
                        {
                            if (typeMark.Length < 2) continue;
                            if (typeMark[0] == 'M')
                            {
                                listM[cntM] = typeMark;
                                cntM++;
                            }
                            if (typeMark[0] == 'P')
                            {
                                listP[cntP] = typeMark;
                                cntP++;
                            }
                            if (typeMark[0] == 'V')
                            {
                                listV[cntV] = typeMark;
                                cntV++;
                            }
                        }
                    }
                    int totalMark = listMarkType.Count() - 1;
                    sheet.MergeRow(4, 1, (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 7 : 8) + totalMark);
                    sheet.MergeRow(5, 1, (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? 7 : 8) + totalMark);
                    #endregion xac dinh so dau diem

                    int startM = 5;
                    int startP = startM + cntM;
                    int startV = startP + cntP;
                    int k = 0;
                    for (int j = startM; j < startP; j++)
                    {
                        copyCol(patternSheet, sheet, 5, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startP; j < startV; j++)
                    {
                        copyCol(patternSheet, sheet, 6, startCol);
                        k++;
                    }
                    k = 0;
                    for (int j = startV; j < startV + cntV; j++)
                    {
                        copyCol(patternSheet, sheet, 7, startCol);
                        k++;
                    }

                    int P_TBM = startCol;
                    int P_KTHT = startCol;
                    int P_HK1 = startCol;
                    int P_Canam = startCol;

                    int P_HLM1 = startCol;
                    int P_HLM2 = startCol;
                    int P_Ghi_chu = startCol;

                    //copy cot KTHK
                    copyCol(patternSheet, sheet, 8, startCol);
                    sheet.SetCellValue(6, P_KTHT, "KTHK");

                    //Neu la hk 1
                    if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        // copy cot TBM HK 1
                        P_HLM1 = startCol;
                        copyCol(patternSheet, sheet, 9, startCol);
                        sheet.SetCellValue(6, P_HLM1, "HLM");
                        P_TBM = P_HLM1;
                    }
                    else
                    {
                        // copy cot TBM HK 2
                        P_HLM2 = startCol;
                        copyCol(patternSheet, sheet, 10, startCol);
                        sheet.SetCellValue(6, P_HLM2, "HLM");
                        //copy cot Canam
                        P_Canam = startCol;
                        copyCol(patternSheet, sheet, 11, startCol);
                        sheet.SetCellValue(6, P_Canam, "Cả năm");
                        P_TBM = P_HLM2;
                    }
                    // copy cot ghi chu
                    P_Ghi_chu = startCol;
                    copyCol(patternSheet, sheet, 12, startCol);
                    sheet.SetCellValue(6, P_Ghi_chu, "Ghi chú");
                    P_HK1 = P_KTHT;
                    ////copy cot HK
                    //P_HK1 = startCol;
                    //copyCol(firstSheet, sheet, 10, startCol);

                    ////copy cot Canam
                    //P_Canam = startCol;
                    //copyCol(firstSheet, sheet, 11, startCol);
                    //// copy cot ghi chu


                    //if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    //    copyCol(firstSheet, sheet, 11, startCol);

                    //Merger cot tieu de dau diem
                    sheet.MergeRow(6, startM, startV - 1);
                    sheet.MergeRow(6, startV, startV + cntV - 1);

                    //set tieu de cho cac cot dau diem
                    sheet.SetCellValue(6, startM, "Điểm hệ số 1");
                    sheet.SetCellValue(6, startV, "Điểm hệ số 2");

                    sheet.MergeRow(7, startM, startP - 1);
                    sheet.MergeRow(7, startP, startV - 1);
                    sheet.MergeRow(7, startV, startV + cntV - 1);

                    sheet.SetCellValue(7, startM, "Miệng");
                    sheet.SetCellValue(7, startP, "Viết/15P");
                    sheet.SetCellValue(7, startV, "Viết");

                    //dem so hoc sinh
                    //IDictionary<string, object> dic = new Dictionary<string, object>();
                    // dic["ClassID"] = ClassID;
                    // dic["Check"] = "check";
                    // IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic);

                    // int cntPupil = listPupilOfClass.Select(o => o.PupilID).Distinct().Count();



                    //Xac dinh so hoc sinh roi tao cac dong cho hs

                    #region tao cac dong trang cho ds hs

                    //int RowEnd = 8;
                    //if (cntPupil > 5)
                    //{
                    //    int numberGroupStudent = cntPupil / 5;
                    //    if (cntPupil % 5 == 0)
                    //    {
                    //        numberGroupStudent = numberGroupStudent - 1;
                    //    }
                    //    IVTRange RangeContent = sheet.GetRange(8, 1, 12, P_Ghi_chu);
                    //    for (int j = 1; j <= numberGroupStudent; j++)
                    //    {
                    //        RowEnd = RowEnd + 5;
                    //        sheet.CopyPasteSameSize(RangeContent, RowEnd, 1);
                    //    }
                    //}

                    #endregion tao cac dong trang cho ds hs

                    int indexPupil = 0;
                    int startRow = 8;
                    List<int> lstPupilID = new List<int>();
                    var ExemptType = new ExemptedSubject();
                    bool isNoLearnFirst = (classSubjectObj != null && classSubjectObj.SectionPerWeekFirstSemester <= 0) ? true : false;
                    bool isNoLearnSecond = (classSubjectObj != null && classSubjectObj.SectionPerWeekSecondSemester <= 0) ? true : false;
                    bool isClassification = acaYear.IsClassification.HasValue && acaYear.IsClassification.Value ? true : false;

                    string TBM = string.Empty;

                    int cntPupil = lstPupilByClassID.Count();

                    objValidation = new VTDataValidation
                    {
                        Contrains = cd,
                        FromColumn = startM,
                        FromRow = 8,
                        SheetIndex = totalSheet,
                        ToColumn = startV + cntV,
                        ToRow = 7 + cntPupil,
                        Type = VTValidationType.LIST
                    };
                    lstValidation.Add(objValidation);

                    sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, 1, 8 + cntPupil - 1, P_Ghi_chu).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.InsideHorizontal);
                    sheet.GetRange(startRow, P_TBM, cntPupil + 8, P_Ghi_chu).IsLock = true;

                    //tao luoi trong phan diem

                    sheet.GetRange(8, startM, cntPupil + 7, startP - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startP, cntPupil + 7, startV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, startV, cntPupil + 7, startV + cntV - 1).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.HairLine, VTBorderIndex.InsideVertical);
                    sheet.GetRange(8, 1, cntPupil + 7, 1).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignRight);//STT
                    sheet.GetRange(8, 3, cntPupil + 7, 3).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignCenter);//ma hoc sinh
                    sheet.GetRange(8, 4, cntPupil + 7, 4).SetHAlign(VTUtils.Excel.Export.VTHAlign.xlHAlignLeft);// tem hoc sinh
                    sheet.SetColumnWidth(P_Ghi_chu, 297);

                    for (int i = 0; i < cntPupil; i++)
                    {
                        if (i % 5 == 0)
                        {
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                        else
                        {
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                            sheet.GetRange(i + 8, 1, i + 8, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        }
                    }
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    foreach (PupilOfClassBO Pupil in lstPupilByClassID)
                    {
                        if (!lstPupilID.Contains(Pupil.PupilID))
                        {
                            lstPupilID.Add(Pupil.PupilID);

                            //cot stt
                            indexPupil++;
                            sheet.SetCellValue(startRow, 1, indexPupil.ToString());

                            //ma hoc sinh
                            sheet.SetCellValue(startRow, 3, Pupil.PupilCode);
                            sheet.GetRange(startRow, 3, startRow, 3).WrapText();
                            string stM = convertPosition(startM, startRow);
                            string stP = convertPosition(startP, startRow);
                            string stV = convertPosition(startV, startRow);

                            string edM = convertPosition(startP - 1, startRow);
                            string edP = convertPosition(startV - 1, startRow);
                            string edV = convertPosition(P_HK1 - 3, startRow);
                            string HK = convertPosition(P_HK1, startRow);

                            sheet.GetRange(startRow, P_TBM, startRow, P_Ghi_chu).FillColor(Color.Yellow);

                            if (isMinSemester)
                            {
                                if (isClassification)
                                {
                                    TBM = "=IF(AND(COUNTIF(" + stM + ":" + edM + ",\"**\") >= " + minM + ",COUNTIF(" + stP + ":" + edP + ",\"**\") >= " + minP + ",COUNTIF(" + stV + ":" + edV + ",\"**\") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\",IF(COUNTIF(" + stM + ":" + HK + "," + "\"" + "Đ" + "\"" + ")*3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0, \"Đ\", \"CĐ\"),\"CĐ\"),\"\"),\"\")";
                                }
                                else
                                {
                                    TBM = "=IF(AND(COUNTIF(" + stM + ":" + edM + ",\"**\") >= " + minM + ",COUNTIF(" + stP + ":" + edP + ",\"**\") >= " + minP + ",COUNTIF(" + stV + ":" + edV + ",\"**\") >= " + minV + "),IF(" + convertPosition(P_KTHT, startRow) + "<>\"\",IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\",IF(COUNTIF(" + stM + ":" + HK + "," + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3, 0) >=0, \"Đ\", \"CĐ\"),\"CĐ\"),\"\"),\"\")";
                                }
                            }
                            else
                            {
                                if (isClassification)
                                {
                                    TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\";IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ")*3 - COUNTA(" + stM + ":" + HK + ")" + "*2 >=0; \"Đ\"; \"CĐ\");\"CĐ\");\"\")";
                                }
                                else
                                {
                                    TBM = "=IF(" + convertPosition(P_KTHT, startRow) + "<>\"\";IF(" + convertPosition(P_KTHT, startRow) + "=\"Đ\";IF(COUNTIF(" + stM + ":" + HK + ";" + "\"" + "Đ" + "\"" + ") - ROUND(COUNTA(" + stM + ":" + HK + ")" + "*2/3; 0) >=0; \"Đ\"; \"CĐ\");\"CĐ\");\"\")";
                                }
                            }

                            sheet.SetFormulaValue(startRow, P_TBM, TBM);

                            // sheet.LockSheet = true;
                            // sheet.ProtectSheet();
                            sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                            sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                            IVTRange rangeStyle1 = patternSheet.GetRange("D13", "D13");
                            IVTRange rangeStyle2 = patternSheet.GetRange("D17", "D17");
                            IVTRange rangeStyle3 = patternSheet.GetRange("D14", "D14");

                            if (Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                                || Pupil.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF
                                || Pupil.Status == GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                || Pupil.Status == GlobalConstants.PUPIL_STATUS_GRADUATED
                              )
                            {
                                sheet.GetRange(startRow, 1, startRow, P_Canam).FillColor(Color.Yellow);
                                sheet.GetRange(startRow, 1, startRow, P_Canam).IsLock = true;
                                sheet.SetCellValue(startRow, 4, Pupil.PupilFullName);
                                sheet.Lock(sheet, startRow, 1, startRow, listMarkType.Count() + 4);
                            }
                            else
                            {
                                sheet.GetRange(startRow, 5, startRow, startV + cntV).IsLock = false;
                            }

                            //Kiểm tra xem nếu học sinh có ngày chuyển trường/chuyển lớp/thôi học < ngày kết thúc học kỳ => không cho phép xuất dữ liệu điểm
                            DateTime? endSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? acaYear.FirstSemesterEndDate : acaYear.SecondSemesterEndDate;
                            DateTime? endDate = Pupil.EndDate;
                            bool showPupilData = false;

                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                    || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value))
                                {
                                    showPupilData = true;
                                }
                            }
                            else
                            {
                                showPupilData = Pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || Pupil.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                                || (Pupil.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);
                            }

                            if (!showPupilData)
                            {
                                startRow++;
                                continue;
                            }

                            if ((lstExemptedSubject.Count(p => p.PupilID == Pupil.PupilID && p.ClassID == classId && p.FirstSemesterExemptType > 0) > 0)
                           || (lstExemptedSubject.Count(p => p.PupilID == Pupil.PupilID && p.ClassID == classId && p.SecondSemesterExemptType > 0) > 0))
                            {
                                sheet.GetRange(startRow, 1, startRow, P_Ghi_chu).FillColor(Color.Yellow);
                                sheet.GetRange(startRow, 1, startRow, P_Ghi_chu).IsLock = true;
                            }


                            var listJudgeRecordPupil = listJudgeRecord.Where(o => o.PupilID == Pupil.PupilID);

                            //ho va ten
                            //Lay diem tung dau diem hoc sinh
                            int col = 5;
                            int j = 0;
                            for (int i = startM; i < startP; i++)
                            {
                                String titleMark = listM[j];
                                JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                                sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                                if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                {
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                                }
                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startP; i < startV; i++)
                            {
                                String titleMark = listP[j];
                                JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                                sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                                if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                {
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                                }
                                col++;
                                j++;
                            }

                            j = 0;
                            for (int i = startV; i < P_TBM - 1; i++)
                            {
                                String titleMark = listV[j];
                                JudgeRecordBO judgeRecordItem = listJudgeRecordPupil.Where(o => o.Title.Equals(titleMark)).FirstOrDefault();
                                sheet.SetCellValue(startRow, col, judgeRecordItem != null ? judgeRecordItem.Judgement : string.Empty);
                                if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains(titleMark) || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                                {
                                    sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                                }
                                col++;
                                j++;
                            }

                            //diem kiem tra hk
                            JudgeRecordBO judgeRecordItemHK = listJudgeRecordPupil.Where(o => o.Title.Equals("HK")).FirstOrDefault();
                            sheet.SetCellValue(startRow, col, judgeRecordItemHK != null ? judgeRecordItemHK.Judgement : string.Empty);
                            if (!string.IsNullOrWhiteSpace(strLockMarkType) && (strLockMarkType.Contains("HK") || strLockMarkType.Contains("LHK")) && isAdmin != null && isAdmin == false)
                            {
                                sheet.GetRange(startRow, col, startRow, col).IsLock = true;
                            }

                            IEnumerable<SummedUpRecordBO> listSummedUpRecord = listSummerupRecord.Where(o => o.PupilID == Pupil.PupilID);
                            if (listSummedUpRecord != null && listSummedUpRecord.Count() > 0)
                            {
                                SummedUpRecordBO SummedUpRecord = listSummedUpRecord.FirstOrDefault();
                                sheet.SetCellValue(startRow, 40, SummedUpRecord.JudgementResult);
                            }
                            string formularTBHK = "=IF(" + convertPosition(P_TBM, startRow) + "<>\"\",IF(" + convertPosition(P_TBM, startRow) + "=\"Đ\", \"Đ\", \"CĐ\"),\"\")";
                            if (Pupil.SecondSemesterExemptType != null || isNoLearnSecond)
                            {
                                formularTBHK = "=" + convertPosition(P_HK1, startRow);
                            }

                            if (Pupil.FirstSemesterExemptType != null || isNoLearnFirst)
                            {
                                formularTBHK = "=" + convertPosition(P_TBM, startRow);
                            }
                            //string formularTBHK = "=I
                            //sheet.ProtectSheet();
                            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                sheet.SetFormulaValue(startRow, P_Canam, formularTBHK);

                            // Cot ghi chu
                            string note = string.Empty;
                            note = "=IF(AND(AN" + startRow + "<>\"\"," + convertPosition(P_TBM, startRow) + "<>\"\")," + "IF(AN" + startRow + "<>" + convertPosition(P_TBM, startRow) + ",\"HLM trong file excel khác HLM trong hệ thống\",\"\"), \"\")";
                            sheet.SetFormulaValue(startRow, P_Ghi_chu, note);
                            startRow++;
                        }
                    }
                    if (cntPupil <= 10)
                    {
                        int totalRecordNotFill = 10 + 8; // 8 bat dau tu dong thu 8
                        int totalRecordDelete = cntPupil + 8;
                        for (int i = totalRecordNotFill; i >= totalRecordDelete; i--)
                        {
                            sheet.DeleteRow(i);
                        }
                    }
                    sheet.GetRange(8 + cntPupil, 1, 8 + cntPupil, P_Ghi_chu).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeTop);//ke dong cuoi cung cua sheet

                    //sheet.SetColumnWidth(P_HK1, 0);
                    sheet.SetColumnWidth(40, 0);
                    sheet.SetColumnWidth(2, 0);
                    sheet.Name = Utils.StripVNSignAndSpace(listClassProfile[l].DisplayName);
                    #endregion
                    sheet.SetFontName("Times New Roman", 11);
                    sheet.ProtectSheet();
                }
            }
            #endregion
            //emptySheet.Delete();
            //firstSheet.Delete();
            //sheet.ProtectSheet();
            //decimal fromNum = 0;
            //decimal toNum = 10;
            // return oBook.ToStreamNumberValidationData(1, fromNum, toNum, 8, 5, 8 + cntPupil, startV + cntV);
            oBook.GetSheet(2).Delete();
            oBook.GetSheet(1).Delete();
            return oBook.ToStreamValidationData(lstValidation);
        }
        #endregion

        public void copyCol(IVTWorksheet sheetOld, IVTWorksheet sheet, int col, int newPostioncol)
        {

            IVTRange range = sheetOld.GetRange(rowHeaderStart, col, rowHeaderEnd, col);

            sheet.CopyPasteSameRowHeigh(range, rowHeaderStart, newPostioncol);

            startCol++;
        }
        /// <summary>
        /// Get column name excel
        /// Hungnd 17/04/2013
        /// </summary>
        /// <param name="columnNumber"></param>
        /// <returns></returns>
        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
        /// <summary>
        /// Convert position of excel (column index, row index)
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private string convertPosition(int col, int row)
        {
            string columnName = GetExcelColumnName(col);
            return columnName + row.ToString();
        }

        public bool CheckMarkTX(string Mark)
        {
            Mark = Mark.Trim();
            Mark = Mark + " ";
            int k = 0;
            for (int i = 0; i < Mark.Length; i++)
            {
                if (Mark.Substring(i, 1) == " ")
                {
                    string Number = Mark.Substring(i - k, k);
                    int Result;
                    if (Number == "" || int.TryParse(Number, out Result) == false)
                    {
                        return false;
                    }
                    if (int.Parse(Number) >= 0 && int.Parse(Number) <= 10)
                    {

                    }
                    else
                    {
                        return false;
                    }
                    k = 0;
                }
                else
                { k = k + 1; }
            }
            return true;
        }

        public bool CheckMarkDX(string Mark)
        {
            Mark = Mark.Trim();
            int Result;
            if (int.TryParse(Mark, out Result) == false)
            {
                return false;
            }
            if (int.Parse(Mark) >= 0 && int.Parse(Mark) <= 10)
            {

            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// lấy danh sách môn học có khóa điểm
        /// Bổ sung nếu khóa điểm Học kì thì lấy toàn bộ 
        /// </summary>
        /// <modifier> AnhVD9 20141006</modifier>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="MarkTitle"></param>
        /// <returns></returns>
        public List<SubjectCat> GetListSubject(int SchoolID, int AcademicYearID, int EducationLevelID, int? ClassID, int Semester, string MarkTitle)
        {
            int classID = ClassID.HasValue ? ClassID.Value : 0;
            if (classID > 0)
            {
                return this.LockedMarkDetailBusiness.All
                            .Where(u => u.ClassProfile.EducationLevelID == EducationLevelID && u.ClassID == classID && u.SchoolID == SchoolID
                                        && u.AcademicYearID == AcademicYearID && u.Semester == Semester
                                        && u.Last2digitNumberSchool == SchoolID % 100)
                            .ToList().Where(u => (u.MarkType.Title == SystemParamsInFile.MARK_TYPE_HK && MarkTitle == SystemParamsInFile.MARK_TYPE_HK)
                                || u.MarkType.Title == SystemParamsInFile.MARK_TYPE_LHK
                            || (MarkTitle != SystemParamsInFile.MARK_TYPE_HK && (u.MarkType.Title + u.MarkIndex) == MarkTitle))
                            .Select(u => u.SubjectCat)
                            .ToList();
            }
            else
            {
                return this.LockedMarkDetailBusiness.All
                            .Where(u => u.ClassProfile.EducationLevelID == EducationLevelID && u.SchoolID == SchoolID
                                        && u.AcademicYearID == AcademicYearID && u.Semester == Semester
                                        && u.Last2digitNumberSchool == SchoolID % 100)
                            .ToList().Where(u => (u.MarkType.Title == SystemParamsInFile.MARK_TYPE_HK && MarkTitle == SystemParamsInFile.MARK_TYPE_HK)
                            || u.MarkType.Title == SystemParamsInFile.MARK_TYPE_LHK
                            || (MarkTitle != SystemParamsInFile.MARK_TYPE_HK && (u.MarkType.Title + u.MarkIndex) == MarkTitle))
                            .Select(u => u.SubjectCat)
                            .ToList();
            }
        }

        /// <summary>
        /// Lấy danh sách điểm Hs - Sử dụng trong chức năng import điểm HK
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="MarkTitle"></param>
        /// <returns></returns>
        public List<List<PupilForImportBO>> GetListPupilForImport(int UserAccountID, int SchoolID, int AcademicYearID, int EducationLevelID, int? ClassID, int Semester, string MarkTitle)
        {
            int classID = ClassID.HasValue && ClassID.Value > 0 ? ClassID.Value : 0;
            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            #region Lay du lieu hoc sinh
            // Neu la admin truong hoac cau hinh xem toan bo du lieu thi hien thi tat cac cac lop
            Dictionary<string, object> dicPoc = new Dictionary<string, object>();
            dicPoc["AcademicYearID"] = AcademicYearID;
            dicPoc["EducationLevelID"] = EducationLevelID;
            if (ClassID.HasValue && ClassID.Value > 0) dicPoc["ClassID"] = ClassID.Value;
            var pupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dicPoc);

            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            int? createdAcaYear = aca.Year;
            int lastSchoolNum = SchoolID % 100;

            // Neu khong phai quan tri truong va Ko cho xem toan truong thi chi hien thi cac lop thuoc quyen quan ly
            if (!UserAccountBusiness.IsSchoolAdmin(UserAccountID) && !aca.IsTeacherCanViewAll)
            {
                Employee employee = UserAccountBusiness.MapFromUserAccountToEmployee(UserAccountID);
                if (!(employee != null && employee.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN))
                {
                    IDictionary<string, object> dicClass = new Dictionary<string, object>();
                    dicClass.Add("AcademicYearID", AcademicYearID);
                    dicClass.Add("EducationLevelID", EducationLevelID);
                    dicClass.Add("UserAccountID", UserAccountID);
                    dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                    List<int> lstClassID = this.ClassProfileBusiness.SearchBySchool(SchoolID, dicClass).Select(o => o.ClassProfileID).ToList();
                    if (lstClassID != null && lstClassID.Count == 0)
                    {
                        lstClassID.Add(0); // Ds rỗng với classid = 0
                    }
                    pupilOfClass = pupilOfClass.Where(o => lstClassID.Contains(o.ClassID));
                }
            }
            // Mon tinh diem
            var iqMark = from poc in pupilOfClass
                         join pp in PupilProfileBusiness.AllNoTracking on poc.PupilID equals pp.PupilProfileID
                         join cp in ClassProfileBusiness.AllNoTracking.Where(o => o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                         join m in VMarkRecordBusiness.AllNoTracking.Where(u => u.Last2digitNumberSchool == lastSchoolNum && u.CreatedAcademicYear == createdAcaYear
                                                                    && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID
                                                                    && u.Semester == Semester && u.Title == MarkTitle
                                                                    && (ClassID.HasValue ? u.ClassID == ClassID.Value : true)) on new { poc.PupilID, poc.ClassID } equals new { m.PupilID, m.ClassID }
                                                                    into i
                         where (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                         from g in i.DefaultIfEmpty()
                         join ex in ExemptedSubjectBusiness.AllNoTracking.Where(o => ((Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                    && o.FirstSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL)
                                    ||
                                    (Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                    && o.SecondSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL))) on new { g.ClassID, g.PupilID, g.SubjectID } equals new { ex.ClassID, ex.PupilID, ex.SubjectID }
                         into i1
                         from g1 in i1.DefaultIfEmpty()
                         orderby cp.EducationLevelID, cp.OrderNumber ?? 0, cp.DisplayName, poc.OrderInClass ?? 0, pp.Name, pp.FullName
                         select new PupilForImportBO
                         {
                             MarkRecordID = g.MarkRecordID,
                             PupilID = poc.PupilID,
                             PupilCode = pp.PupilCode,
                             FullName = pp.FullName,
                             Name = pp.Name,
                             ClassID = poc.ClassID,
                             ClassName = cp.DisplayName,
                             SubjectID = g.SubjectID,
                             Mark = g.Mark,
                             MarkedDate = g.MarkedDate,
                             MarkTypeID = g.MarkTypeID,
                             Title = g.Title,
                             OrderNumber = g.OrderNumber,
                             Status = poc.Status,
                             EndDate = poc.EndDate,
                             IsExempted = g1.ExemptedSubjectID != null && g1.ExemptedSubjectID > 0
                         };

            var lstMark = iqMark.ToList();

            // Mon nhan xet
            var iqJudge = from poc in pupilOfClass
                          join pp in PupilProfileBusiness.AllNoTracking on poc.PupilID equals pp.PupilProfileID
                          join cp in ClassProfileBusiness.AllNoTracking.Where(o => o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                          join j in VJudgeRecordBusiness.AllNoTracking.Where(u => u.Last2digitNumberSchool == lastSchoolNum && u.CreatedAcademicYear == createdAcaYear
                                                                     && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID
                                                                     && u.Semester == Semester && u.Title == MarkTitle
                                                                     && (ClassID.HasValue ? u.ClassID == ClassID.Value : true)) on new { poc.PupilID, poc.ClassID } equals new { j.PupilID, j.ClassID }
                                                                     into i
                          where (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false))
                          from g in i.DefaultIfEmpty()
                          join ex in ExemptedSubjectBusiness.AllNoTracking.Where(o => ((Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                    && o.FirstSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL)
                                    ||
                                    (Semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                    && o.SecondSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL))) on new { g.ClassID, g.PupilID, g.SubjectID } equals new { ex.ClassID, ex.PupilID, ex.SubjectID }
                          into i1
                          from g1 in i1.DefaultIfEmpty()
                          orderby cp.EducationLevelID, cp.OrderNumber ?? 0, cp.DisplayName, poc.OrderInClass ?? 0, pp.Name, pp.FullName
                          select new PupilForImportBO
                          {
                              JudgeRecordID = g.JudgeRecordID,
                              PupilID = poc.PupilID,
                              PupilCode = pp.PupilCode,
                              FullName = pp.FullName,
                              Name = pp.Name,
                              ClassID = poc.ClassID,
                              ClassName = cp.DisplayName,
                              SubjectID = g.SubjectID,
                              Judgement = g.Judgement,
                              MarkedDate = g.MarkedDate,
                              MarkTypeID = g.MarkTypeID,
                              Title = g.Title,
                              OrderNumber = g.OrderNumber,
                              Status = poc.Status,
                              EndDate = poc.EndDate,
                              IsExempted = g1.ExemptedSubjectID != null && g1.ExemptedSubjectID > 0
                          };
            var lstJudge = iqJudge.ToList();

            List<List<PupilForImportBO>> listPupilForImport = new List<List<PupilForImportBO>>();
            listPupilForImport.Add(lstMark);
            listPupilForImport.Add(lstJudge);
            #endregion

            return listPupilForImport;
        }

        /// <summary>
        /// GetMaxMarkRecord
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public IDictionary<string, object> GetMaxMarkRecord(int AcademicYearID, int SchoolID, int Semester, int? PeriodID = null)
        {
            int MaxM = 0;
            int MaxP = 0;
            int MaxV = 0;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> MarkRecordDic = new Dictionary<string, object>();
            if (!PeriodID.HasValue)
            {
                try
                {
                    int? TempMaxM = this.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID }
                    ,{"Semester",Semester }
                    ,{"Year", academicYear.Year}
                }).Where(o => o.MarkType.Title == SystemParamsInFile.MARK_TYPE_M).Select(o => o.OrderNumber).Max();
                    MaxM = TempMaxM.HasValue ? TempMaxM.Value : 0;
                }
                catch (Exception ex)
                {
                    
                    string paramList = string.Format("AcademicYearID={0}, SchoolID={1}, Semester={2}", AcademicYearID, SchoolID, Semester);
                    LogExtensions.ErrorExt(logger, DateTime.Now, "GetMaxMarkRecord", paramList, ex);
                    MaxM = 0;
                }

                try
                {
                    int? TempMaxP = this.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID }
                    ,{"Semester",Semester }
                    ,{"Year", academicYear.Year}
                }).Where(o => o.MarkType.Title == SystemParamsInFile.MARK_TYPE_P).Select(o => o.OrderNumber).Max();
                    MaxP = TempMaxP.HasValue ? TempMaxP.Value : 0;

                }
                catch (Exception ex)
                {
                    
                    string paramList = string.Format("AcademicYearID={0}, SchoolID={1}, Semester={2}", AcademicYearID, SchoolID, Semester);
                    LogExtensions.ErrorExt(logger, DateTime.Now, "GetMaxMarkRecord", paramList, ex);
                    MaxP = 0;
                }


                try
                {
                    int? TempMaxV = this.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID }
                    ,{"Semester",Semester }
                    ,{"Year", academicYear.Year}
                }).Where(o => o.MarkType.Title == SystemParamsInFile.MARK_TYPE_V).Select(o => o.OrderNumber).Max();
                    MaxV = TempMaxV.HasValue ? TempMaxV.Value : 0;
                }
                catch (Exception ex)
                {
                    
                    string paramList = string.Format("AcademicYearID={0}, SchoolID={1}, Semester={2}", AcademicYearID, SchoolID, Semester);
                    LogExtensions.ErrorExt(logger, DateTime.Now, "GetMaxMarkRecord", paramList, ex);
                    MaxV = 0;
                }

                MarkRecordDic[SystemParamsInFile.MARK_TYPE_M] = MaxM;
                MarkRecordDic[SystemParamsInFile.MARK_TYPE_P] = MaxP;
                MarkRecordDic[SystemParamsInFile.MARK_TYPE_V] = MaxV;
            }
            else
            {
                int MaxIndexM = 0;
                int MaxIndexP = 0;
                int MaxIndexV = 0;
                PeriodDeclaration p = PeriodDeclarationBusiness.Find(PeriodID);
                if (p != null)
                {
                    MaxIndexM = p.StartIndexOfInterviewMark.Value;
                    MaxIndexP = p.StartIndexOfWritingMark.Value;
                    MaxIndexV = p.StartIndexOfTwiceCoeffiecientMark.Value;
                }
                //So con diem cua dot
                string markM = "";
                string markP = "";
                string markV = "";
                for (int i = MaxIndexM; i < p.InterviewMark.Value + MaxIndexM; i++)
                {
                    markM += "M" + i.ToString();
                }
                for (int i = MaxIndexP; i < p.WritingMark.Value + MaxIndexP; i++)
                {
                    markP += "P" + i.ToString();
                }
                for (int i = MaxIndexV; i < p.TwiceCoeffiecientMark.Value + MaxIndexV; i++)
                {
                    markV += "V" + i.ToString();
                }
                IQueryable<MarkRecord> lsMarkRecord = this.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID }
                    ,{"Semester",Semester }
                    ,{"Year", academicYear.Year}
                });

                if (lsMarkRecord.Count() > 0)
                {
                    IQueryable<MarkRecord> lsMarkRecordM = lsMarkRecord.Where(o => markM.Contains(o.Title));
                    if (lsMarkRecordM.Count() > 0)
                    {
                        MaxM = lsMarkRecordM.Select(o => o.OrderNumber).Max();
                    }
                    IQueryable<MarkRecord> lsMarkRecordP = lsMarkRecord.Where(o => markP.Contains(o.Title));
                    if (lsMarkRecordP.Count() > 0)
                    {
                        MaxP = lsMarkRecordP.Select(o => o.OrderNumber).Max();
                    }
                    IQueryable<MarkRecord> lsMarkRecordV = lsMarkRecord.Where(o => markV.Contains(o.Title));
                    if (lsMarkRecordV.Count() > 0)
                    {
                        MaxV = lsMarkRecordV.Select(o => o.OrderNumber).Max();
                    }
                }



                MarkRecordDic[SystemParamsInFile.MARK_TYPE_M] = MaxM - MaxIndexM + 1;
                MarkRecordDic[SystemParamsInFile.MARK_TYPE_P] = MaxP - MaxIndexP + 1;
                MarkRecordDic[SystemParamsInFile.MARK_TYPE_V] = MaxV - MaxIndexV + 1;
            }

            return MarkRecordDic;

        }

        /// <summary>
        /// GetCountPupilMarkRecordForClass
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <param name="SubjectID">The subject ID.</param>
        /// <param name="TitleMark">The title mark.</param>
        /// <returns>
        /// System.Int32
        /// </returns>
        ///<author>NamTA</author>
        ///<date>4/15/2013</date>
        public int GetCountPupilMarkRecordForClass(int SchoolID, int AcademicYearID, int EducationLevelID, int? ClassID, int SubjectID, string TitleMark)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            //neu khong co lop se lay danh sach hoc sinh theo khoi
            dic["ClassID"] = ClassID;
            //lay trang thai moi nhat cua hoc sinh
            dic["Check"] = "check";
            //lay danh sach hoc sinh co trang thai dang hoc
            IEnumerable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);

            //Kiem tra mon hoc

            IDictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["SubjectID"] = SubjectID;

            int IsCommenting = 0;
            IEnumerable<ClassSubject> ListClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject);

            if (ListClassSubject != null && ListClassSubject.Count() > 0)
            {
                IsCommenting = ListClassSubject.FirstOrDefault().IsCommenting.Value;
            }

            IDictionary<string, object> dicMark = new Dictionary<string, object>();
            dicMark["AcademicYearID"] = AcademicYearID;
            dicMark["EducationLevelID"] = EducationLevelID;
            dicMark["ClassID"] = ClassID;
            dicMark["Year"] = academicYear.Year;
            IEnumerable<MarkRecord> listMarkRecord = MarkRecordBusiness.SearchBySchool(SchoolID, dicMark);

            IDictionary<string, object> dicJudge = new Dictionary<string, object>();
            dicJudge["AcademicYearID"] = AcademicYearID;
            dicJudge["EducationLevelID"] = EducationLevelID;
            //neu khong co lop se lay danh sach hoc sinh theo khoi
            dicJudge["ClassID"] = ClassID;
            dicJudge["Year"] = academicYear.Year;
            IEnumerable<JudgeRecord> listJudgeRecord = JudgeRecordBusiness.SearchBySchool(SchoolID, dicJudge);

            if (IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
            {
                return listJudgeRecord.Where(o => o.Title.Contains(TitleMark) && listPupil.Where(u => u.PupilID == o.PupilID) != null ? listPupil.Where(u => u.PupilID == o.PupilID).FirstOrDefault().Status == SystemParamsInFile.PUPIL_STATUS_STUDYING : true).Count();
            }
            else
            {
                return listMarkRecord.Where(o => o.Title.Contains(TitleMark) && listPupil.Where(u => u.PupilID == o.PupilID) != null ? listPupil.Where(u => u.PupilID == o.PupilID).FirstOrDefault().Status == SystemParamsInFile.PUPIL_STATUS_STUDYING : true).Count();
            }
        }

        /// <summary>
        /// QuangLM
        /// Cap nhat lai thong tin diem cu
        /// </summary>
        /// <param name="listMarkForDelete"></param>
        /// <param name="listMarkForInsert"></param>
        private void SetOldValue(List<MarkRecord> listMarkForDelete, List<MarkRecord> listMarkForInsert)
        {
            foreach (MarkRecord itemDel in listMarkForDelete)
            {
                foreach (MarkRecord itemInsert in listMarkForInsert)
                {
                    if (itemInsert.PupilID == itemDel.PupilID && itemInsert.Title == itemDel.Title)
                    {
                        // Neu diem khac
                        if (itemInsert.Mark != itemDel.Mark)
                        {
                            itemInsert.CreatedDate = itemDel.CreatedDate;
                            itemInsert.IsSMS = false;
                            itemInsert.ModifiedDate = DateTime.Now;
                            itemInsert.OldMark = itemDel.Mark;
                            itemInsert.SynchronizeID = itemDel.SynchronizeID;
                        }
                        else
                        {
                            itemInsert.CreatedDate = itemDel.CreatedDate;
                            itemInsert.IsSMS = itemDel.IsSMS;
                            itemInsert.ModifiedDate = null;
                            itemInsert.OldMark = itemDel.OldMark;
                        }
                        break;
                    }
                }
            }
        }

        #region Webservice Calculate mark - AnhVD 20131223
        /// <summary>
        /// Tu dong tien hanh tinh diem TBM cho mon hoc
        /// Chiendd: modify 17/08/2015, sua lai luong tinh diem TBM tu dong
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        /// <returns>1:  </returns>
        public string AutoSummupRecord(int UserID, List<int> lstPupilID, int Semester, int? PeriodID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID)
        {
            //bien ket qua
            //0:Khong co ket qua,1:cap nhat thanh cong,2:co loi xay ra
            string Result = "0";
            lstPupilID = lstPupilID.Distinct().ToList();
            if (lstPupilID == null || lstPupilID.Count == 0)
            {
                return "0";
            }
            try
            {
                // Lay danh sach mon hoc theo lop
                IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
                dicClassSubject["AcademicYearID"] = AcademicYearID;
                dicClassSubject["SchoolID"] = SchoolID;
                dicClassSubject["Semester"] = Semester;
                dicClassSubject["ClassID"] = ClassID;
                ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();


                string strMarkTitle = (PeriodID.HasValue && PeriodID.Value > 0) ? GetMarkTitle(PeriodID.Value) : this.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                SummedMarkAllPeriodAndSemester(lstPupilID, SchoolID, AcademicYearID, Semester, classSubject, strMarkTitle, PeriodID);

                Result = "1";


            }
            catch (Exception ex)
            {
                Result = "2";
                
                string paramList = string.Format("AcademicYearID={0}, SchoolID={1}, Semester={2}", AcademicYearID, SchoolID, Semester);
                LogExtensions.ErrorExt(logger, DateTime.Now, "AutoSummupRecord", paramList, ex);
            }
            return Result;
        }
        #endregion
        #region GetLockMarkPrimary - Namta
        /// <summary>
        /// Namta - Function get title mark lock for primary level
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        /// <returns></returns>
        public string GetLockMarkTitlePrimary(int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int Semester)
        {

            string titlePrimary = "";

            //Type Subject Is Judge or Mark
            var classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() { { "SubjectID", SubjectID }, { "SchoolID", SchoolID } });
            if (classSubject != null && classSubject.Count() > 0)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcacdemicYearID"] = AcademicYearID;
                dic["IsCommenting"] = classSubject.FirstOrDefault().IsCommenting;
                dic["ClassID"] = ClassID;
                dic["SubjectID"] = SubjectID;

                IQueryable<LockMarkPrimary> listLockMark = LockMarkPrimaryBusiness.SearchLockedMark(dic);
                string[] Title12Semester1Mark = { SystemParamsInFile.T1, SystemParamsInFile.T2, SystemParamsInFile.T3, SystemParamsInFile.T4, SystemParamsInFile.T5, SystemParamsInFile.GKI, SystemParamsInFile.CKI };
                string[] Title12Semester2Mark = { SystemParamsInFile.T6, SystemParamsInFile.T7, SystemParamsInFile.T8, SystemParamsInFile.T9, SystemParamsInFile.GKII, SystemParamsInFile.CKII };
                string[] Title12Semester1Judge = { SystemParamsInFile.NX1, SystemParamsInFile.NX2, SystemParamsInFile.NX3, SystemParamsInFile.NX4, SystemParamsInFile.HKI };
                string[] Title345Semester1Judge = { SystemParamsInFile.NX1, SystemParamsInFile.NX2, SystemParamsInFile.NX3, SystemParamsInFile.NX4, SystemParamsInFile.NX5, SystemParamsInFile.HKI };
                if (listLockMark != null && listLockMark.Count() > 0)
                {
                    //split string to find by Semester
                    string LockTitle = listLockMark.FirstOrDefault().LockPrimary.ToString();
                    string[] LTArray = LockTitle.Split(',');
                    if ((int)dic["IsCommenting"] == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                    {
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            var LTSemester = LTArray.ToList().FindAll(u => Title12Semester1Mark.ToList().Contains(u)).ToList();
                            LockTitle = String.Join(",", LTSemester);
                        }
                        else
                        {
                            var LTSemester = LTArray.ToList().FindAll(u => Title12Semester2Mark.ToList().Contains(u)).ToList();
                            LockTitle = String.Join(",", LTSemester);
                        }
                    }
                    else
                    {
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            int EducationLevel = classSubject.FirstOrDefault().ClassProfile.EducationLevelID;
                            //if grade class is 1 or 2
                            if (EducationLevel == 1 || EducationLevel == 2)
                            {
                                var LTSemester = LTArray.ToList().FindAll(u => Title12Semester1Judge.ToList().Contains(u)).ToList();
                                LockTitle = String.Join(",", LTSemester);
                            }
                            else
                            {
                                var LTSemester = LTArray.ToList().FindAll(u => Title345Semester1Judge.ToList().Contains(u)).ToList();
                                LockTitle = String.Join(",", LTSemester);
                            }
                        }
                        else
                        {
                            LockTitle = String.Join(",", LTArray);
                        }
                    }
                    if (LockTitle.Length > 0 && LockTitle[LockTitle.Length - 1] != ',')
                    {
                        LockTitle = LockTitle + ",";
                    }
                    //return string split by ','
                    return LockTitle;
                }


            }

            return titlePrimary;
        }
        #endregion

        public void ImportFromInputMark(List<Object> insertList, List<Object> updateList)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                List<ThreadMark> lstThreadMark = new List<ThreadMark>();
                List<MarkRecord> lstMarkRecord = new List<MarkRecord>();
                List<MarkRecord> lstMarkRecordInsert = new List<MarkRecord>();
                for (int i = 0; i < insertList.Count; i++)
                {

                    Object obj = insertList[i];
                    if (obj.GetType() != typeof(MarkRecord))
                    {
                        continue;
                    }

                    MarkRecord entity = (MarkRecord)obj;
                    entity.CreatedDate = DateTime.Now;
                    lstMarkRecord.Add(entity);
                    MarkRecordBusiness.Insert(entity);
                }
                for (int i = 0; i < updateList.Count; i++)
                {
                    Object obj = updateList[i];
                    if (!obj.GetType().Name.Contains(typeof(MarkRecord).Name))
                    {
                        continue;
                    }

                    MarkRecord entity = (MarkRecord)obj;
                    entity.ModifiedDate = DateTime.Now;
                    MarkRecordBusiness.Update(entity);
                    lstMarkRecord.Add(entity);
                }

                MarkRecordBusiness.Save();

                //Thuc hien tinh TBM va luu vao ThreadMark de tong ket diem tu dong
                if (lstMarkRecord != null && lstMarkRecord.Count > 0)
                {
                    MarkRecord objMarkRecord = lstMarkRecord.FirstOrDefault();
                    this.SummedUpMarkAndSummary(lstMarkRecord, objMarkRecord.SchoolID, objMarkRecord.AcademicYearID, objMarkRecord.Semester.Value);
                }
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        private string getSheetNameExport(List<int> listSubjectIncrease, int subjectId, string subjectName)
        {
            string sheetName = subjectName;
            if (listSubjectIncrease.Exists(p => p == subjectId))
            {
                // sheetName = subjectName + "_TC";
                sheetName = subjectName;
            }
            return Utils.StripVNSignAndSpace(sheetName);
        }

        private string getSubjectNameIncrease(List<SubjectCat> listSubjectCatUnion, int subjectIdIncrease)
        {
            string name = string.Empty;
            if (listSubjectCatUnion != null && listSubjectCatUnion.Count > 0)
            {
                name = listSubjectCatUnion.Where(p => p.SubjectCatID == subjectIdIncrease).Select(p => p.SubjectName).FirstOrDefault();
            }
            // return Utils.StripVNSignAndSpace(name + "_TC");
            return Utils.StripVNSignAndSpace(name);
        }

        public Dictionary<int, string> GetLockMarkTitleBySchoolID(int schoolid, int academicyearid, int semester, int classid = 0, int subjectid = 0)
        {

            //Chiendd1: 01/07/2015: Bo sung where dieu kien partitionId cho bang LockedMarkDetail;
            int partitionId = UtilsBusiness.GetPartionId(schoolid);
            Dictionary<int, string> dicMarkType = new Dictionary<int, string>();
            var lstLockMarkDetail = LockedMarkDetailBusiness.All.Where(o => o.SchoolID == schoolid &&
                                                                            o.AcademicYearID == academicyearid
                                                                            && (o.ClassID == classid || classid == 0)
                                                                            && o.Semester == semester
                                                                            && o.Last2digitNumberSchool == partitionId)
                                                                        .Where(o => subjectid == 0 ? true : o.SubjectID == subjectid)
                                                                         .Select(o => new { MarkTypeID = o.MarkTypeID, MarkIndex = o.MarkIndex, SubjectID = o.SubjectID })
                                                                        .Distinct().ToList();

            List<int> listMarkTypeID = lstLockMarkDetail.Where(o => o.MarkTypeID.HasValue)
                .Select(o => o.MarkTypeID.Value).Distinct().ToList();
            List<MarkType> listMarkType = MarkTypeBusiness.All.Where(o => listMarkTypeID.Contains(o.MarkTypeID)).ToList();
            foreach (var item in lstLockMarkDetail)
            {
                string strMarkType = "";
                // Neu da co thi lay ra de cong them chuoi khoa
                if (dicMarkType.ContainsKey(item.SubjectID))
                {
                    strMarkType = dicMarkType[item.SubjectID];
                }
                var marktype = listMarkType.Where(o => o.MarkTypeID == item.MarkTypeID).FirstOrDefault();
                if (marktype != null && !strMarkType.Contains(marktype.Title + item.MarkIndex.ToString()))
                    strMarkType += "," + ((marktype.Title != SystemParamsInFile.MARK_TYPE_HK && marktype.Title != SystemParamsInFile.MARK_TYPE_LHK) ? marktype.Title + item.MarkIndex.ToString() : marktype.Title);
                dicMarkType[item.SubjectID] = strMarkType;
            }
            return dicMarkType;
        }

        /// <summary>
        /// Chiendd1: 17/08/2015. Thuc hien tinh diem TBM mon hoc tang cuong va khong tang cuong
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="semesterId"></param>
        /// <param name="PeriodID"></param>
        /// <param name="strMarkTitle"></param>
        /// <param name="classSubject"></param>
        private void SummedUpMark(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, int? PeriodID, String strMarkTitle, ClassSubject classSubject)
        {
            if (classSubject == null)
            {
                return;
            }
            if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK)
            {
                return;
            }
            if (lstPupilID == null || lstPupilID.Count == 0)
            {
                return;
            }
            //Tinh TBM theo mon tang cuong
            String pupilIds = UtilsBusiness.JoinPupilId(lstPupilID);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            List<int> lstPupilIDIsMin = new List<int>();
            if (PeriodID.HasValue)
            {
                lstPupilIDIsMin = lstPupilID;
            }
            else
            {
                //Tinh ra so hoc sinh chua nhap du so con diem toi thieu khi duoc cau hinh
                bool isMinSemester = false;
                int MinM = 0;
                int MinP = 0;
                int MinV = 0;
                if (semesterId == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    isMinSemester = classSubject != null && ((classSubject.MMinFirstSemester.HasValue && classSubject.MMinFirstSemester.Value > 0)
                                       || classSubject.PMinFirstSemester.HasValue && classSubject.PMinFirstSemester.Value > 0
                                       || classSubject.VMinFirstSemester.HasValue && classSubject.VMinFirstSemester.Value > 0);
                    MinM = classSubject != null && classSubject.MMinFirstSemester.HasValue ? classSubject.MMinFirstSemester.Value : 0;
                    MinP = classSubject != null && classSubject.PMinFirstSemester.HasValue ? classSubject.PMinFirstSemester.Value : 0;
                    MinV = classSubject != null && classSubject.VMinFirstSemester.HasValue ? classSubject.VMinFirstSemester.Value : 0;
                }
                else
                {
                    isMinSemester = classSubject != null && ((classSubject.MMinSecondSemester.HasValue && classSubject.MMinSecondSemester.Value > 0)
                                       || classSubject.PMinSecondSemester.HasValue && classSubject.PMinSecondSemester.Value > 0
                                       || classSubject.VMinSecondSemester.HasValue && classSubject.VMinSecondSemester.Value > 0);
                    MinM = classSubject != null && classSubject.MMinSecondSemester.HasValue ? classSubject.MMinSecondSemester.Value : 0;
                    MinP = classSubject != null && classSubject.PMinSecondSemester.HasValue ? classSubject.PMinSecondSemester.Value : 0;
                    MinV = classSubject != null && classSubject.VMinSecondSemester.HasValue ? classSubject.VMinSecondSemester.Value : 0;
                }

                if (isMinSemester)
                {
                    var lstMarkRecord = (from mr in MarkRecordBusiness.All
                                         where mr.SchoolID == schoolId
                                            && mr.Last2digitNumberSchool == partitionId
                                             && mr.AcademicYearID == academicYearId
                                             && mr.ClassID == classSubject.ClassID
                                             && mr.SubjectID == classSubject.SubjectID
                                             && mr.Semester == semesterId
                                             && lstPupilID.Contains(mr.PupilID)
                                             && (mr.Title.Contains("M") || mr.Title.Contains("P") || mr.Title.Contains("V"))
                                         group mr by new
                                         {
                                             mr.PupilID,
                                             mr.Title,
                                             mr.Mark
                                         }
                                             into g
                                         select new
                                         {
                                             pupilID = g.Key.PupilID,
                                             Title = g.Key.Title,
                                             Mark = g.Key.Mark
                                         });
                    List<int> lsttmp = lstMarkRecord.Select(p => p.pupilID).Distinct().ToList();
                    int countM = 0;
                    int countP = 0;
                    int countV = 0;
                    int pupilIDtmp = 0;
                    for (int i = 0; i < lsttmp.Count; i++)
                    {
                        pupilIDtmp = lsttmp[i];
                        countM = lstMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("M")).Count();
                        countP = lstMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("P")).Count();
                        countV = lstMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("V")).Count();
                        if (countM >= MinM && countP >= MinP && countV >= MinV)
                        {
                            lstPupilIDIsMin.Add(pupilIDtmp);
                        }
                    }
                }
                else
                {
                    lstPupilIDIsMin = lstPupilID;
                }
            }

            string pupilIdIsMin = UtilsBusiness.JoinPupilId(lstPupilIDIsMin);


            #region Store tinh TBM


            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();

            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    if (classSubject.SubjectIDIncrease.HasValue && classSubject.SubjectIDIncrease.Value > 0)
                    {

                        OracleCommand command = new OracleCommand
                        {
                            CommandType = CommandType.StoredProcedure,
                            CommandText = "PROC_CLASS_MARK_SUMMED_UP_PLUS",
                            Connection = conn,
                        };
                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                        command.Parameters.Add("P_CLASS_ID", classSubject.ClassID);
                        command.Parameters.Add("P_SUBJECT_ID", classSubject.SubjectID);
                        command.Parameters.Add("P_SEMESTER", semesterId);
                        command.Parameters.Add("P_PERIOD_ID", PeriodID);
                        command.Parameters.Add("P_MARK_TITLE", strMarkTitle);
                        command.Parameters.Add("P_SUBJECT_PLUS_ID", classSubject.SubjectIDIncrease);
                        command.Parameters.Add("P_PUPIL_IDS", pupilIds);
                        command.Parameters.Add("P_PUPIL_IDISMIN", pupilIdIsMin);
                        
                        command.ExecuteNonQuery();

                        //this.context.PROC_CLASS_MARK_SUMMED_UP_PLUS(schoolId, academicYearId, classSubject.ClassID, classSubject.SubjectID, semesterId, PeriodID, strMarkTitle, classSubject.SubjectIDIncrease, pupilIds, pupilIdIsMin);
                    }
                    else
                    {
                        OracleCommand command = new OracleCommand
                        {
                            CommandType = CommandType.StoredProcedure,
                            CommandText = "PROC_CLASS_MARK_SUMMED_UP_NEW",
                            Connection = conn
                        };
                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                        command.Parameters.Add("P_CLASS_ID", classSubject.ClassID);
                        command.Parameters.Add("P_SUBJECT_ID", classSubject.SubjectID);
                        command.Parameters.Add("P_SEMESTER", semesterId);
                        command.Parameters.Add("P_PERIOD_ID", PeriodID);
                        command.Parameters.Add("P_MARK_TITLE", strMarkTitle);
                        command.Parameters.Add("P_PUPIL_IDS", pupilIds);
                        command.Parameters.Add("P_PUPIL_IDISMIN", pupilIdIsMin);
                        command.ExecuteNonQuery();

                        //this.context.PROC_CLASS_MARK_SUMMED_UP_NEW(schoolId, academicYearId, classSubject.ClassID, classSubject.SubjectID, semesterId, PeriodID, strMarkTitle, pupilIds, pupilIdIsMin);

                    }
                }
                catch (Exception ex)
                {
                    
                    string para = string.Format("Tinh TBM schoolId={0},academicYearId={1}, ClassID={2},semesterId={3},PeriodID={4} ", schoolId, academicYearId, classSubject.ClassID, semesterId, PeriodID);
                    LogExtensions.ErrorExt(logger, DateTime.Now, "SummedUpMark", para, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }

            #endregion

        }


        /// <summary>
        /// Tinh diem TBM hoc ky va tat ca cac dot trong ky
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="semesterId"></param>
        /// <param name="classSubject"></param>
        /// <returns></returns>
        public void SummedMarkAllPeriodAndSemester(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, ClassSubject classSubject, string strMarkTitle, int? PeriodId)
        {
            List<Task> taskMark = new List<Task>();

            //Neu nhap diem theo dot. TBM cua dot
            if (PeriodId.HasValue && PeriodId.Value > 0)
            {
                taskMark.Add(Task.Run(() =>
                {
                    SummedUpMark(lstPupilID, schoolId, academicYearId, semesterId, PeriodId, strMarkTitle, classSubject);
                }));
            }
            else //Nhap diem theo ky. tinh lai TBM cac dot trong ky
            {
                //1. Lay danh sach tat cac cac dot trong hoc ky
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearId;
                dic["Semester"] = semesterId;
                List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(schoolId, dic).OrderBy(o => o.FromDate).ToList();
                if (lstPeriod != null && lstPeriod.Count > 0)
                {
                    foreach (PeriodDeclaration objPeriod in lstPeriod)
                    {
                        string strMarkTitlePeriod = this.GetMarkTitle(objPeriod.PeriodDeclarationID);
                        taskMark.Add(Task.Run(() =>
                        {
                            SummedUpMark(lstPupilID, schoolId, academicYearId, semesterId, objPeriod.PeriodDeclarationID, strMarkTitlePeriod, classSubject);
                        }));
                    }
                }


            }


            //Tinh TBM hoc ky. Nhap diem dot hay ky deu phai tinh lai TBM HK
            string strMarkTitleSemester = "";

            if (PeriodId.HasValue && PeriodId.Value > 0)
            {
                strMarkTitleSemester = this.GetMarkSemesterTitle(schoolId, academicYearId, semesterId);
            }
            else
            {
                strMarkTitleSemester = strMarkTitle;
            }

            taskMark.Add(Task.Run(() =>
            {
                SummedUpMark(lstPupilID, schoolId, academicYearId, semesterId, null, strMarkTitleSemester, classSubject);
            }));


            if (taskMark.Count > 0)
            {
                Task.WaitAll(taskMark.ToArray());
            }


        }


        private List<int> CheckValidateCreateMark(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, ClassSubject classSubject, int? periodId)
        {

            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            //Nếu là môn học được tăng cường thì tính TBM cho cả lớp
            if (classSubject.SubjectIDIncrease.HasValue && classSubject.SubjectIDIncrease > 0)
            {
                return lstPupilID;
            }
            if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK)
            {
                return lstPupilID;
            }
            //Lay thoi gian thay doi diem gan nhat cua hoc sinh. Chi tinh cho mon tinh diem
            List<PupilMarkCreate> lstMark = new List<PupilMarkCreate>();
            lstMark = (from mr in MarkRecordBusiness.All.AsNoTracking()
                       where mr.Last2digitNumberSchool == partitionId
                        && mr.AcademicYearID == academicYearId
                        && mr.SubjectID == classSubject.SubjectID
                        && mr.ClassID == classSubject.ClassID
                        && mr.Semester == semesterId
                        && lstPupilID.Contains(mr.PupilID)
                       group new { mr.CreatedDate, mr.ModifiedDate } by mr.PupilID into g
                       select new PupilMarkCreate
                       {
                           PupilId = g.Key,
                           CreateDate = g.Max(c => c.CreatedDate),
                           ModifyDate = g.Max(c => c.ModifiedDate)
                       }).ToList();
            if (lstMark == null || lstMark.Count == 0)
            {
                return lstPupilID;
            }
            //Thoi gian tinh diem TBM gan nhat cua hoc sinh
            IQueryable<SummedUpRecord> qSumup = from mr in SummedUpRecordBusiness.All.AsNoTracking()
                                                where mr.Last2digitNumberSchool == partitionId
                                                 && mr.AcademicYearID == academicYearId
                                                 && mr.SubjectID == classSubject.SubjectID
                                                 && mr.ClassID == classSubject.ClassID
                                                 && mr.Semester == semesterId
                                                 && lstPupilID.Contains(mr.PupilID)
                                                select mr;
            if (periodId.HasValue && periodId > 0)
            {
                qSumup = qSumup.Where(s => s.PeriodID == periodId);
            }
            else
            {
                qSumup = qSumup.Where(s => s.PeriodID == null || s.PeriodID == 0);
            }
            List<PupilMarkCreate> lstSumedUp = (from mr in qSumup
                                                group mr.SummedUpDate by mr.PupilID into g
                                                select new PupilMarkCreate
                                                {
                                                    PupilId = g.Key,
                                                    CreateDate = g.Max()

                                                }).ToList();
            if (lstSumedUp == null || lstSumedUp.Count == 0)
            {
                return lstPupilID;
            }
            List<int> lstPupilid = new List<int>();
            PupilMarkCreate objSummed;
            PupilMarkCreate objMark;
            for (int i = 0; i < lstMark.Count; i++)
            {
                objMark = lstMark[i];
                objSummed = lstSumedUp.FirstOrDefault(s => s.PupilId == objMark.PupilId);
                if (objSummed == null || (objMark.CreateDate >= objSummed.CreateDate || objMark.ModifyDate >= objSummed.CreateDate))
                {
                    lstPupilid.Add(objMark.PupilId);
                }
            }
            return lstPupilid;
        }

        private IDictionary<string, object> MarkRecordColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            //columnMap.Add("MarkRecordID", "MARK_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_IsByC", "M_IS_BY_C");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldMark", "OLD_MARK");
            columnMap.Add("M_MarkRecord", "M_MARK_RECORD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;
        }

        public void SP_DeleteMarkRecord(int academicYearID, int schoolID, int classID, int semesterID, int subjectID, int? isHistory, int? periodID, List<int> listPupilIdDelete, string strMarkTitle = null)
        {
            string strPupilId = string.Join(",", listPupilIdDelete);
            this.context.SP_DELETE_MARK_RECORD(academicYearID, schoolID, classID, semesterID, subjectID, isHistory, periodID, strPupilId, strMarkTitle);
        }

        public void SP_DeleteJudgeRecord(int academicYearID, int schoolID, int classID, int semesterID, int subjectID, int? isHistory, int? periodID, List<int> listPupilIdDelete, string strMarkTitle = null)
        {
            string strPupilId = string.Join(",", listPupilIdDelete);
            this.context.SP_DELETE_JUDGE_RECORD(academicYearID, schoolID, classID, semesterID, subjectID, isHistory, periodID, strPupilId, strMarkTitle);
        }

        public void SP_DeleteSummedUpRecord(int? isHistory, List<int> listPupilId, int peroid, int subjectId, int classId, int semester, int shoolID, int academicYearId)
        {
            string strPupilId = string.Join(",", listPupilId);
            this.context.SP_DELETE_SUMMED_UP_RECORD(isHistory, strPupilId, peroid, subjectId, classId, semester, shoolID, academicYearId);
        }


        public void SummedUpMarkAndSummary(List<MarkRecord> lstMark, int schoolId, int academicYearId, int semesterId)
        {
            if (lstMark == null || lstMark.Count == 0)
            {
                return;
            }
            List<ThreadMark> lstThreadMark = new List<ThreadMark>();
            ThreadMark objThreadMark = new ThreadMark();
            int classId = 0;
            int subjectId = 0;
            string strMarkTitle = this.GetMarkSemesterTitle(schoolId, academicYearId, semesterId);
            short semester = (short)semesterId;
            int threadMarkPartitionId = UtilsBusiness.GetPartionId(schoolId);
            List<int> lstClassId = lstMark.Select(m => m.ClassID).Distinct().ToList();
            ClassSubject objClassSubject;

            for (int i = 0; i < lstClassId.Count; i++)
            {
                classId = lstClassId[i];
                List<int> lstPupilSumup = lstMark.Where(p => p.ClassID == classId).Select(p => p.PupilID).Distinct().ToList();
                List<int> lstSubject = lstMark.Where(c => c.ClassID == classId).Select(s => s.SubjectID).Distinct().ToList();
                List<ClassSubject> lstclassSubject = ClassSubjectBusiness.SearchByClass(classId, new Dictionary<string, object> { { "SchoolID", schoolId } }).ToList();
                if (lstSubject == null || lstPupilSumup == null)
                {
                    continue;
                }


                for (int j = 0; j < lstSubject.Count; j++)
                {
                    subjectId = lstSubject[j];
                    objClassSubject = lstclassSubject.FirstOrDefault(s => s.SubjectID == subjectId);
                    if (objClassSubject == null)
                    {
                        continue;
                    }
                    //Khong thuc hien tinh TBM mon nhan xet
                    if (objClassSubject.IsCommenting == Common.GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        continue;
                    }


                    SummedMarkAllPeriodAndSemester(lstPupilSumup, schoolId, academicYearId, semesterId, objClassSubject, strMarkTitle, null);
                    objThreadMark = new ThreadMark
                    {
                        AcademicYearID = academicYearId,
                        ClassID = classId,
                        CreatedDate = DateTime.Now,
                        LastDigitSchoolID = threadMarkPartitionId,
                        NumberScan = 0,
                        PeriodID = 0,
                        PupilID = 0,
                        SchoolID = schoolId,
                        Semester = semester,
                        Status = 1,//TBM cho dot neu co
                        StatusResult = 0,
                        SubjectID = subjectId,
                        Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE
                    };
                    lstThreadMark.Add(objThreadMark);
                }



            }


            //Luu vao ThreadMark de tinh tong ket diem
            if (lstThreadMark != null && lstThreadMark.Count > 0)
            {
                ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);
            }
        }


        private void SpDeteleMarkRecord(int academicYearID, int schoolID, int history, string strMarkIdDelete)
        {
            OracleConnection conn = (OracleConnection)context.Database.Connection;
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            OracleCommand command = new OracleCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "SP_DELETE_MARK_RECORD_BY_ID",
                Connection = conn,
            };
            command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearID);
            command.Parameters.Add("P_SCHOOL_ID", schoolID);
            command.Parameters.Add("P_IS_HISTORY", history);
            command.Parameters.Add("P_STR_MARKRECORD_ID_DEL", strMarkIdDelete);
            command.ExecuteNonQuery();

        }

        public void ImportFromOtherSystem(List<ImportFromOtherSystemBO> lstBO, int schoolId, int academicYearId, int appliedId, int classId, int subjectId, int semester, bool isComment, int? employeeId)
        {
            // Lay so cot diem mieng, 15 phut, viet trong hoc ky
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearId;
            dic["SchoolID"] = schoolId;
            dic["Semester"] = semester;

            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration == null) return;

            int soCotDiemMieng = SemeterDeclaration.InterviewMark;
            int soCotDiem15Phut = SemeterDeclaration.WritingMark;
            int soCotDiemKiemTra = SemeterDeclaration.TwiceCoeffiecientMark;

            List<MarkType> lstMarkType = MarkTypeBusiness.All.Where(o => o.AppliedLevel == appliedId).ToList();
            AcademicYear aca = AcademicYearBusiness.Find(academicYearId);

            dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["SchoolID"] = schoolId;
            dic["AcademicYearID"] = academicYearId;
            dic["SubjectID"] = subjectId;
            dic["AppliedLevel"] = appliedId;
            dic["Semester"] = semester;
            List<SummedUpRecord> lstSur = SummedUpRecordBusiness.SearchBySchool(schoolId, dic).ToList();

            dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["SchoolID"] = schoolId;
            dic["AcademicYearID"] = academicYearId;
            dic["SubjectID"] = subjectId;
            dic["AppliedLevel"] = appliedId;
            dic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_ALL;
            List<SummedUpRecord> lstSurAll = SummedUpRecordBusiness.SearchBySchool(schoolId, dic).ToList();

            if (subjectId > 0)
            {
                #region mon tinh diem
                if (!isComment)
                {
                    dic = new Dictionary<string, object>();
                    dic["SubjectID"] = subjectId;
                    dic["AcademicYearID"] = academicYearId;
                    dic["Semester"] = semester;
                    dic["ClassID"] = classId;
                    List<MarkRecord> lstMr = MarkRecordBusiness.SearchBySchool(schoolId, dic).ToList();
                    for (int i = 0; i < lstBO.Count; i++)
                    {
                        ImportFromOtherSystemBO bo = lstBO[i];
                        List<object> lstM = bo.lstM;
                        List<object> lstP = bo.lstP;
                        List<object> lstV = bo.lstV;

                        //Lay cac record diem cua hoc sinh nay
                        List<MarkRecord> lstMrOfPupil = lstMr.Where(o => o.PupilID == bo.PupilID).ToList();

                        //Diem mieng
                        for (int j = 0; j < soCotDiemMieng; j++)
                        {
                            if (j >= lstM.Count())
                            {
                                break;
                            }
                            int? mark = (int?)lstM[j];

                            //Kiem tra co diem hay chua
                            MarkRecord mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                            //update
                            if (mr != null)
                            {
                                if (mark != null)
                                {
                                    mr.Mark = mark.Value;
                                    mr.ModifiedDate = DateTime.Now;
                                    mr.LogChange = employeeId.GetValueOrDefault();
                                    MarkRecordBusiness.Update(mr);
                                }
                                else
                                {
                                    MarkRecordBusiness.Delete(mr.MarkRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (mark != null)
                                {
                                    mr = new MarkRecord();
                                    mr.AcademicYearID = academicYearId;
                                    mr.ClassID = classId;
                                    mr.CreatedDate = DateTime.Now;
                                    mr.Last2digitNumberSchool = schoolId % 100;
                                    mr.Mark = mark.Value;
                                    mr.MarkedDate = DateTime.Now;
                                    mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                    mr.OrderNumber = j + 1;
                                    mr.PeriodID = null;
                                    mr.PupilID = bo.PupilID;
                                    mr.SchoolID = schoolId;
                                    mr.Semester = semester;
                                    mr.SubjectID = subjectId;
                                    mr.Title = "M" + (j + 1);
                                    mr.Year = aca.Year;
                                    mr.CreatedAcademicYear = aca.Year;
                                    mr.LogChange = employeeId.GetValueOrDefault();

                                    MarkRecordBusiness.Insert(mr);
                                }
                            }
                        }

                        //Diem 15P
                        for (int j = 0; j < soCotDiem15Phut; j++)
                        {
                            if (j >= lstP.Count())
                            {
                                break;
                            }
                            decimal? mark = (decimal?)lstP[j];

                            //Kiem tra co diem hay chua
                            MarkRecord mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                            //update
                            if (mr != null)
                            {
                                if (mark != null)
                                {
                                    mr.Mark = mark.Value;
                                    mr.ModifiedDate = DateTime.Now;
                                    mr.LogChange = employeeId.GetValueOrDefault();
                                    MarkRecordBusiness.Update(mr);
                                }
                                else
                                {
                                    MarkRecordBusiness.Delete(mr.MarkRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (mark != null)
                                {
                                    mr = new MarkRecord();
                                    mr.AcademicYearID = academicYearId;
                                    mr.ClassID = classId;
                                    mr.CreatedDate = DateTime.Now;
                                    mr.Last2digitNumberSchool = schoolId % 100;
                                    mr.Mark = mark.Value;
                                    mr.MarkedDate = DateTime.Now;
                                    mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                    mr.OrderNumber = j + 1;
                                    mr.PeriodID = null;
                                    mr.PupilID = bo.PupilID;
                                    mr.SchoolID = schoolId;
                                    mr.Semester = semester;
                                    mr.SubjectID = subjectId;
                                    mr.Title = "P" + (j + 1);
                                    mr.Year = aca.Year;
                                    mr.CreatedAcademicYear = aca.Year;
                                    mr.LogChange = employeeId.GetValueOrDefault();

                                    MarkRecordBusiness.Insert(mr);
                                }
                            }
                        }

                        //Diem 1T
                        for (int j = 0; j < soCotDiemKiemTra; j++)
                        {
                            if (j >= lstV.Count())
                            {
                                break;
                            }
                            decimal? mark = (decimal?)lstV[j];

                            //Kiem tra co diem hay chua
                            MarkRecord mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                            //update
                            if (mr != null)
                            {
                                if (mark != null)
                                {
                                    mr.Mark = mark.Value;
                                    mr.ModifiedDate = DateTime.Now;
                                    mr.LogChange = employeeId.GetValueOrDefault();
                                    MarkRecordBusiness.Update(mr);
                                }
                                else
                                {
                                    MarkRecordBusiness.Delete(mr.MarkRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (mark != null)
                                {
                                    mr = new MarkRecord();
                                    mr.AcademicYearID = academicYearId;
                                    mr.ClassID = classId;
                                    mr.CreatedDate = DateTime.Now;
                                    mr.Last2digitNumberSchool = schoolId % 100;
                                    mr.Mark = mark.Value;
                                    mr.MarkedDate = DateTime.Now;
                                    mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                    mr.OrderNumber = j + 1;
                                    mr.PeriodID = null;
                                    mr.PupilID = bo.PupilID;
                                    mr.SchoolID = schoolId;
                                    mr.Semester = semester;
                                    mr.SubjectID = subjectId;
                                    mr.Title = "V" + (j + 1);
                                    mr.Year = aca.Year;
                                    mr.CreatedAcademicYear = aca.Year;
                                    mr.LogChange = employeeId.GetValueOrDefault();

                                    MarkRecordBusiness.Insert(mr);
                                }
                            }
                        }

                        //Diem HK
                        //Kiem tra co diem hay chua
                        MarkRecord mrHK = lstMrOfPupil.FirstOrDefault(o => o.Title == "HK");
                        decimal? markHK = (decimal?)bo.HK;
                        //update
                        if (mrHK != null)
                        {
                            if (markHK != null)
                            {
                                mrHK.Mark = markHK.Value;
                                mrHK.ModifiedDate = DateTime.Now;
                                mrHK.LogChange = employeeId.GetValueOrDefault();
                                MarkRecordBusiness.Update(mrHK);
                            }
                            else
                            {
                                MarkRecordBusiness.Delete(mrHK.MarkRecordID);
                            }
                        }
                        //insert
                        else
                        {
                            if (markHK != null)
                            {
                                mrHK = new MarkRecord();
                                mrHK.AcademicYearID = academicYearId;
                                mrHK.ClassID = classId;
                                mrHK.CreatedDate = DateTime.Now;
                                mrHK.Last2digitNumberSchool = schoolId % 100;
                                mrHK.Mark = markHK.Value;
                                mrHK.MarkedDate = DateTime.Now;
                                mrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                mrHK.OrderNumber = 0;
                                mrHK.PeriodID = null;
                                mrHK.PupilID = bo.PupilID;
                                mrHK.SchoolID = schoolId;
                                mrHK.Semester = semester;
                                mrHK.SubjectID = subjectId;
                                mrHK.Title = "HK";
                                mrHK.Year = aca.Year;
                                mrHK.CreatedAcademicYear = aca.Year;
                                mrHK.LogChange = employeeId.GetValueOrDefault();

                                MarkRecordBusiness.Insert(mrHK);
                            }
                        }

                        //Diem TBM
                        //Kiem tra co diem hay chua
                        SummedUpRecord sur = lstSur.FirstOrDefault(o => o.PupilID == bo.PupilID);
                        decimal? markTB = (decimal?)bo.TB;
                        //update
                        if (sur != null)
                        {
                            if (markTB != null)
                            {
                                sur.SummedUpMark = markTB.Value;
                                sur.SummedUpDate = DateTime.Now;
                                SummedUpRecordBusiness.Update(sur);
                            }
                            else
                            {
                                SummedUpRecordBusiness.Delete(sur.SummedUpRecordID);
                            }
                        }
                        //insert
                        else
                        {
                            if (markTB != null)
                            {
                                sur = new SummedUpRecord();
                                sur.AcademicYearID = academicYearId;
                                sur.ClassID = classId;
                                sur.IsCommenting = isComment ? 1 : 0;
                                sur.Last2digitNumberSchool = schoolId % 100;
                                sur.PeriodID = null;
                                sur.PupilID = bo.PupilID;
                                sur.SchoolID = schoolId;
                                sur.Semester = semester;
                                sur.SubjectID = subjectId;
                                sur.SummedUpDate = DateTime.Now;
                                sur.Year = aca.Year;
                                sur.SummedUpMark = markTB.Value;
                                sur.CreatedAcademicYear = aca.Year;

                                SummedUpRecordBusiness.Insert(sur);
                            }
                        }

                        //Diem TBMCN
                        //Kiem tra co diem hay chua
                        SummedUpRecord surAll = lstSurAll.FirstOrDefault(o => o.PupilID == bo.PupilID);
                        decimal? markTBCN = (decimal?)bo.TBCN;
                        //update
                        if (surAll != null)
                        {
                            if (markTBCN != null)
                            {
                                surAll.SummedUpMark = markTBCN.Value;
                                surAll.SummedUpDate = DateTime.Now;
                                SummedUpRecordBusiness.Update(surAll);
                            }
                            else
                            {
                                SummedUpRecordBusiness.Delete(surAll.SummedUpRecordID);
                            }
                        }
                        //insert
                        else
                        {
                            if (markTBCN != null)
                            {
                                surAll = new SummedUpRecord();
                                surAll.AcademicYearID = academicYearId;
                                surAll.ClassID = classId;
                                surAll.IsCommenting = isComment ? 1 : 0;
                                surAll.Last2digitNumberSchool = schoolId % 100;
                                surAll.PeriodID = null;
                                surAll.PupilID = bo.PupilID;
                                surAll.SchoolID = schoolId;
                                surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                surAll.SubjectID = subjectId;
                                surAll.SummedUpDate = DateTime.Now;
                                surAll.Year = aca.Year;
                                surAll.SummedUpMark = markTBCN.Value;
                                surAll.CreatedAcademicYear = aca.Year;

                                SummedUpRecordBusiness.Insert(surAll);
                            }
                        }
                    }
                }
                #endregion
                #region mon nhan xet
                else
                {
                    dic = new Dictionary<string, object>();
                    dic["ClassID"] = classId;
                    dic["AcademicYearID"] = academicYearId;
                    dic["SubjectID"] = subjectId;
                    dic["Semester"] = semester;

                    List<JudgeRecord> lstJr = JudgeRecordBusiness.SearchBySchool(schoolId, dic).Where(o => o.PeriodID == null).ToList();

                    for (int i = 0; i < lstBO.Count; i++)
                    {
                        ImportFromOtherSystemBO bo = lstBO[i];
                        List<object> lstM = bo.lstM;
                        List<object> lstP = bo.lstP;
                        List<object> lstV = bo.lstV;

                        //Lay cac record diem cua hoc sinh nay
                        List<JudgeRecord> lstJrOfPupil = lstJr.Where(o => o.PupilID == bo.PupilID).ToList();

                        //Diem mieng
                        for (int j = 0; j < soCotDiemMieng; j++)
                        {
                            if (j >= lstM.Count())
                            {
                                break;
                            }
                            string mark = (string)lstM[j];

                            //Kiem tra co diem hay chua
                            JudgeRecord jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                            //update
                            if (jr != null)
                            {
                                if (!String.IsNullOrEmpty(mark))
                                {
                                    jr.Judgement = mark;
                                    jr.ModifiedDate = DateTime.Now;
                                    jr.LogChange = employeeId.GetValueOrDefault();
                                    JudgeRecordBusiness.Update(jr);
                                }
                                else
                                {
                                    JudgeRecordBusiness.Delete(jr.JudgeRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (!String.IsNullOrEmpty(mark))
                                {
                                    jr = new JudgeRecord();
                                    jr.AcademicYearID = academicYearId;
                                    jr.ClassID = classId;
                                    jr.CreatedDate = DateTime.Now;
                                    jr.Last2digitNumberSchool = schoolId % 100;
                                    jr.Judgement = mark;
                                    jr.MarkedDate = DateTime.Now;
                                    jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                    jr.OrderNumber = j + 1;
                                    jr.PeriodID = null;
                                    jr.PupilID = bo.PupilID;
                                    jr.SchoolID = schoolId;
                                    jr.Semester = semester;
                                    jr.SubjectID = subjectId;
                                    jr.Title = "M" + (j + 1);
                                    jr.Year = aca.Year;
                                    jr.CreatedAcademicYear = aca.Year;
                                    jr.LogChange = employeeId.GetValueOrDefault();

                                    JudgeRecordBusiness.Insert(jr);
                                }
                            }
                        }

                        //Diem 15P
                        for (int j = 0; j < soCotDiem15Phut; j++)
                        {
                            if (j >= lstP.Count())
                            {
                                break;
                            }
                            string mark = (string)lstP[j];

                            //Kiem tra co diem hay chua
                            JudgeRecord jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                            //update
                            if (jr != null)
                            {
                                if (!String.IsNullOrEmpty(mark))
                                {
                                    jr.Judgement = mark;
                                    jr.ModifiedDate = DateTime.Now;
                                    jr.LogChange = employeeId.GetValueOrDefault();
                                    JudgeRecordBusiness.Update(jr);
                                }
                                else
                                {
                                    JudgeRecordBusiness.Delete(jr.JudgeRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (!String.IsNullOrEmpty(mark))
                                {
                                    jr = new JudgeRecord();
                                    jr.AcademicYearID = academicYearId;
                                    jr.ClassID = classId;
                                    jr.CreatedDate = DateTime.Now;
                                    jr.Last2digitNumberSchool = schoolId % 100;
                                    jr.Judgement = mark;
                                    jr.MarkedDate = DateTime.Now;
                                    jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                    jr.OrderNumber = j + 1;
                                    jr.PeriodID = null;
                                    jr.PupilID = bo.PupilID;
                                    jr.SchoolID = schoolId;
                                    jr.Semester = semester;
                                    jr.SubjectID = subjectId;
                                    jr.Title = "P" + (j + 1);
                                    jr.Year = aca.Year;
                                    jr.CreatedAcademicYear = aca.Year;
                                    jr.LogChange = employeeId.GetValueOrDefault();

                                    JudgeRecordBusiness.Insert(jr);
                                }
                            }
                        }

                        //Diem 1T
                        for (int j = 0; j < soCotDiemKiemTra; j++)
                        {
                            if (j >= lstV.Count())
                            {
                                break;
                            }
                            string mark = (string)lstV[j];

                            //Kiem tra co diem hay chua
                            JudgeRecord jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                            //update
                            if (jr != null)
                            {
                                if (!String.IsNullOrEmpty(mark))
                                {
                                    jr.Judgement = mark;
                                    jr.ModifiedDate = DateTime.Now;
                                    jr.LogChange = employeeId.GetValueOrDefault();
                                    JudgeRecordBusiness.Update(jr);
                                }
                                else
                                {
                                    JudgeRecordBusiness.Delete(jr.JudgeRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (!String.IsNullOrEmpty(mark))
                                {
                                    jr = new JudgeRecord();
                                    jr.AcademicYearID = academicYearId;
                                    jr.ClassID = classId;
                                    jr.CreatedDate = DateTime.Now;
                                    jr.Last2digitNumberSchool = schoolId % 100;
                                    jr.Judgement = mark;
                                    jr.MarkedDate = DateTime.Now;
                                    jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                    jr.OrderNumber = j + 1;
                                    jr.PeriodID = null;
                                    jr.PupilID = bo.PupilID;
                                    jr.SchoolID = schoolId;
                                    jr.Semester = semester;
                                    jr.SubjectID = subjectId;
                                    jr.Title = "V" + (j + 1);
                                    jr.Year = aca.Year;
                                    jr.CreatedAcademicYear = aca.Year;
                                    jr.LogChange = employeeId.GetValueOrDefault();

                                    JudgeRecordBusiness.Insert(jr);
                                }
                            }
                        }

                        //Diem HK
                        //Kiem tra co diem hay chua
                        JudgeRecord jrHK = lstJrOfPupil.FirstOrDefault(o => o.Title == "HK");
                        string markHK = (string)bo.HK;
                        //update
                        if (jrHK != null)
                        {
                            if (markHK != null)
                            {
                                jrHK.Judgement = markHK;
                                jrHK.ModifiedDate = DateTime.Now;
                                jrHK.LogChange = employeeId.GetValueOrDefault();
                                JudgeRecordBusiness.Update(jrHK);
                            }
                            else
                            {
                                JudgeRecordBusiness.Delete(jrHK.JudgeRecordID);
                            }
                        }
                        //insert
                        else
                        {
                            if (markHK != null)
                            {
                                jrHK = new JudgeRecord();
                                jrHK.AcademicYearID = academicYearId;
                                jrHK.ClassID = classId;
                                jrHK.CreatedDate = DateTime.Now;
                                jrHK.Last2digitNumberSchool = schoolId % 100;
                                jrHK.Judgement = markHK;
                                jrHK.MarkedDate = DateTime.Now;
                                jrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                jrHK.OrderNumber = 0;
                                jrHK.PeriodID = null;
                                jrHK.PupilID = bo.PupilID;
                                jrHK.SchoolID = schoolId;
                                jrHK.Semester = semester;
                                jrHK.SubjectID = subjectId;
                                jrHK.Title = "HK";
                                jrHK.Year = aca.Year;
                                jrHK.CreatedAcademicYear = aca.Year;
                                jrHK.LogChange = employeeId.GetValueOrDefault();

                                JudgeRecordBusiness.Insert(jrHK);
                            }
                        }

                        //Diem TBM
                        //Kiem tra co diem hay chua
                        SummedUpRecord sur = lstSur.FirstOrDefault(o => o.PupilID == bo.PupilID);
                        string markTB = (string)bo.TB;
                        //update
                        if (sur != null)
                        {
                            if (markTB != null)
                            {
                                sur.JudgementResult = markTB;
                                sur.SummedUpDate = DateTime.Now;

                                SummedUpRecordBusiness.Update(sur);
                            }
                            else
                            {
                                SummedUpRecordBusiness.Delete(sur.SummedUpRecordID);
                            }
                        }
                        //insert
                        else
                        {
                            if (markTB != null)
                            {
                                sur = new SummedUpRecord();
                                sur.AcademicYearID = academicYearId;
                                sur.ClassID = classId;
                                sur.IsCommenting = isComment ? 1 : 0;
                                sur.Last2digitNumberSchool = schoolId % 100;
                                sur.PeriodID = null;
                                sur.PupilID = bo.PupilID;
                                sur.SchoolID = schoolId;
                                sur.Semester = semester;
                                sur.SubjectID = subjectId;
                                sur.SummedUpDate = DateTime.Now;
                                sur.Year = aca.Year;
                                sur.JudgementResult = markTB;
                                sur.CreatedAcademicYear = aca.Year;

                                SummedUpRecordBusiness.Insert(sur);
                            }
                        }

                        //Diem TBMCN
                        //Kiem tra co diem hay chua
                        SummedUpRecord surAll = lstSurAll.FirstOrDefault(o => o.PupilID == bo.PupilID);
                        string markTBCN = (string)bo.TBCN;
                        //update
                        if (surAll != null)
                        {
                            if (markTBCN != null)
                            {
                                surAll.JudgementResult = markTBCN;
                                surAll.SummedUpDate = DateTime.Now;

                                SummedUpRecordBusiness.Update(surAll);
                            }
                            else
                            {
                                SummedUpRecordBusiness.Delete(surAll.SummedUpRecordID);
                            }
                        }
                        //insert
                        else
                        {
                            if (markTBCN != null)
                            {
                                surAll = new SummedUpRecord();
                                surAll.AcademicYearID = academicYearId;
                                surAll.ClassID = classId;
                                surAll.IsCommenting = isComment ? 1 : 0;
                                surAll.Last2digitNumberSchool = schoolId % 100;
                                surAll.PeriodID = null;
                                surAll.PupilID = bo.PupilID;
                                surAll.SchoolID = schoolId;
                                surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                surAll.SubjectID = subjectId;
                                surAll.SummedUpDate = DateTime.Now;
                                surAll.Year = aca.Year;
                                surAll.JudgementResult = markTBCN;
                                surAll.CreatedAcademicYear = aca.Year;

                                SummedUpRecordBusiness.Insert(surAll);
                            }
                        }
                    }

                }
                #endregion

                this.Save();
            }
            else
            {
                var lstSubject = lstBO.Select(o => new { SubjectID = o.SubjectID, IsComment = o.IsComment }).Distinct().ToList();

                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearId;
                dic["Semester"] = semester;
                dic["ClassID"] = classId;
                List<MarkRecord> lstMrAll = MarkRecordBusiness.SearchBySchool(schoolId, dic).Where(o => o.PeriodID == null).ToList();

                dic = new Dictionary<string, object>();
                dic["ClassID"] = classId;
                dic["AcademicYearID"] = academicYearId;
                dic["Semester"] = semester;

                List<JudgeRecord> lstJrAll = JudgeRecordBusiness.SearchBySchool(schoolId, dic).Where(o => o.PeriodID == null).ToList();

                for (int h = 0; h < lstSubject.Count; h++)
                {
                    var subject = lstSubject[h];
                    int eachSubjectId = subject.SubjectID;
                    isComment = subject.IsComment;

                    List<ImportFromOtherSystemBO> lstBoSubject = lstBO.Where(o => o.SubjectID == eachSubjectId).ToList();

                    #region mon tinh diem
                    if (!isComment)
                    {
                        List<MarkRecord> lstMr = lstMrAll.Where(o => o.PeriodID == null && o.SubjectID == eachSubjectId).ToList();
                        for (int i = 0; i < lstBoSubject.Count; i++)
                        {
                            ImportFromOtherSystemBO bo = lstBoSubject[i];
                            List<object> lstM = bo.lstM;
                            List<object> lstP = bo.lstP;
                            List<object> lstV = bo.lstV;

                            //Lay cac record diem cua hoc sinh nay
                            List<MarkRecord> lstMrOfPupil = lstMr.Where(o => o.PupilID == bo.PupilID).ToList();

                            //Diem mieng
                            for (int j = 0; j < soCotDiemMieng; j++)
                            {
                                if (j >= lstM.Count())
                                {
                                    break;
                                }
                                int? mark = (int?)lstM[j];

                                //Kiem tra co diem hay chua
                                MarkRecord mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                                //update
                                if (mr != null)
                                {
                                    if (mark != null)
                                    {
                                        mr.Mark = mark.Value;
                                        mr.ModifiedDate = DateTime.Now;
                                        mr.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordBusiness.Update(mr);
                                    }
                                    else
                                    {
                                        MarkRecordBusiness.Delete(mr.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (mark != null)
                                    {
                                        mr = new MarkRecord();
                                        mr.AcademicYearID = academicYearId;
                                        mr.ClassID = classId;
                                        mr.CreatedDate = DateTime.Now;
                                        mr.Last2digitNumberSchool = schoolId % 100;
                                        mr.Mark = mark.Value;
                                        mr.MarkedDate = DateTime.Now;
                                        mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                        mr.OrderNumber = j + 1;
                                        mr.PeriodID = null;
                                        mr.PupilID = bo.PupilID;
                                        mr.SchoolID = schoolId;
                                        mr.Semester = semester;
                                        mr.SubjectID = eachSubjectId;
                                        mr.Title = "M" + (j + 1);
                                        mr.Year = aca.Year;
                                        mr.CreatedAcademicYear = aca.Year;
                                        mr.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordBusiness.Insert(mr);
                                    }
                                }
                            }

                            //Diem 15P
                            for (int j = 0; j < soCotDiem15Phut; j++)
                            {
                                if (j >= lstP.Count())
                                {
                                    break;
                                }
                                decimal? mark = (decimal?)lstP[j];

                                //Kiem tra co diem hay chua
                                MarkRecord mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                                //update
                                if (mr != null)
                                {
                                    if (mark != null)
                                    {
                                        mr.Mark = mark.Value;
                                        mr.ModifiedDate = DateTime.Now;
                                        mr.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordBusiness.Update(mr);
                                    }
                                    else
                                    {
                                        MarkRecordBusiness.Delete(mr.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (mark != null)
                                    {
                                        mr = new MarkRecord();
                                        mr.AcademicYearID = academicYearId;
                                        mr.ClassID = classId;
                                        mr.CreatedDate = DateTime.Now;
                                        mr.Last2digitNumberSchool = schoolId % 100;
                                        mr.Mark = mark.Value;
                                        mr.MarkedDate = DateTime.Now;
                                        mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                        mr.OrderNumber = j + 1;
                                        mr.PeriodID = null;
                                        mr.PupilID = bo.PupilID;
                                        mr.SchoolID = schoolId;
                                        mr.Semester = semester;
                                        mr.SubjectID = eachSubjectId;
                                        mr.Title = "P" + (j + 1);
                                        mr.Year = aca.Year;
                                        mr.CreatedAcademicYear = aca.Year;
                                        mr.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordBusiness.Insert(mr);
                                    }
                                }
                            }

                            //Diem 1T
                            for (int j = 0; j < soCotDiemKiemTra; j++)
                            {
                                if (j >= lstV.Count())
                                {
                                    break;
                                }
                                decimal? mark = (decimal?)lstV[j];

                                //Kiem tra co diem hay chua
                                MarkRecord mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                                //update
                                if (mr != null)
                                {
                                    if (mark != null)
                                    {
                                        mr.Mark = mark.Value;
                                        mr.ModifiedDate = DateTime.Now;
                                        mr.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordBusiness.Update(mr);
                                    }
                                    else
                                    {
                                        MarkRecordBusiness.Delete(mr.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (mark != null)
                                    {
                                        mr = new MarkRecord();
                                        mr.AcademicYearID = academicYearId;
                                        mr.ClassID = classId;
                                        mr.CreatedDate = DateTime.Now;
                                        mr.Last2digitNumberSchool = schoolId % 100;
                                        mr.Mark = mark.Value;
                                        mr.MarkedDate = DateTime.Now;
                                        mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                        mr.OrderNumber = j + 1;
                                        mr.PeriodID = null;
                                        mr.PupilID = bo.PupilID;
                                        mr.SchoolID = schoolId;
                                        mr.Semester = semester;
                                        mr.SubjectID = eachSubjectId;
                                        mr.Title = "V" + (j + 1);
                                        mr.Year = aca.Year;
                                        mr.CreatedAcademicYear = aca.Year;
                                        mr.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordBusiness.Insert(mr);
                                    }
                                }
                            }

                            //Diem HK
                            //Kiem tra co diem hay chua
                            MarkRecord mrHK = lstMrOfPupil.FirstOrDefault(o => o.Title == "HK");
                            decimal? markHK = (decimal?)bo.HK;
                            //update
                            if (mrHK != null)
                            {
                                if (markHK != null)
                                {
                                    mrHK.Mark = markHK.Value;
                                    mrHK.ModifiedDate = DateTime.Now;
                                    mrHK.LogChange = employeeId.GetValueOrDefault();
                                    MarkRecordBusiness.Update(mrHK);
                                }
                                else
                                {
                                    MarkRecordBusiness.Delete(mrHK.MarkRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markHK != null)
                                {
                                    mrHK = new MarkRecord();
                                    mrHK.AcademicYearID = academicYearId;
                                    mrHK.ClassID = classId;
                                    mrHK.CreatedDate = DateTime.Now;
                                    mrHK.Last2digitNumberSchool = schoolId % 100;
                                    mrHK.Mark = markHK.Value;
                                    mrHK.MarkedDate = DateTime.Now;
                                    mrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                    mrHK.OrderNumber = 0;
                                    mrHK.PeriodID = null;
                                    mrHK.PupilID = bo.PupilID;
                                    mrHK.SchoolID = schoolId;
                                    mrHK.Semester = semester;
                                    mrHK.SubjectID = eachSubjectId;
                                    mrHK.Title = "HK";
                                    mrHK.Year = aca.Year;
                                    mrHK.CreatedAcademicYear = aca.Year;
                                    mrHK.LogChange = employeeId.GetValueOrDefault();

                                    MarkRecordBusiness.Insert(mrHK);
                                }
                            }

                            //Diem TBM
                            //Kiem tra co diem hay chua
                            SummedUpRecord sur = lstSur.FirstOrDefault(o => o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                            decimal? markTB = (decimal?)bo.TB;
                            //update
                            if (sur != null)
                            {
                                if (markTB != null)
                                {
                                    sur.SummedUpMark = markTB.Value;
                                    sur.SummedUpDate = DateTime.Now;
                                    SummedUpRecordBusiness.Update(sur);
                                }
                                else
                                {
                                    SummedUpRecordBusiness.Delete(sur.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTB != null)
                                {
                                    sur = new SummedUpRecord();
                                    sur.AcademicYearID = academicYearId;
                                    sur.ClassID = classId;
                                    sur.IsCommenting = isComment ? 1 : 0;
                                    sur.Last2digitNumberSchool = schoolId % 100;
                                    sur.PeriodID = null;
                                    sur.PupilID = bo.PupilID;
                                    sur.SchoolID = schoolId;
                                    sur.Semester = semester;
                                    sur.SubjectID = eachSubjectId;
                                    sur.SummedUpDate = DateTime.Now;
                                    sur.Year = aca.Year;
                                    sur.SummedUpMark = markTB.Value;
                                    sur.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordBusiness.Insert(sur);
                                }
                            }

                            //Diem TBMCN
                            //Kiem tra co diem hay chua
                            SummedUpRecord surAll = lstSurAll.FirstOrDefault(o => o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                            decimal? markTBCN = (decimal?)bo.TBCN;
                            //update
                            if (surAll != null)
                            {
                                if (markTBCN != null)
                                {
                                    surAll.SummedUpMark = markTBCN.Value;
                                    surAll.SummedUpDate = DateTime.Now;
                                    SummedUpRecordBusiness.Update(surAll);
                                }
                                else
                                {
                                    SummedUpRecordBusiness.Delete(surAll.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTBCN != null)
                                {
                                    surAll = new SummedUpRecord();
                                    surAll.AcademicYearID = academicYearId;
                                    surAll.ClassID = classId;
                                    surAll.IsCommenting = isComment ? 1 : 0;
                                    surAll.Last2digitNumberSchool = schoolId % 100;
                                    surAll.PeriodID = null;
                                    surAll.PupilID = bo.PupilID;
                                    surAll.SchoolID = schoolId;
                                    surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    surAll.SubjectID = eachSubjectId;
                                    surAll.SummedUpDate = DateTime.Now;
                                    surAll.Year = aca.Year;
                                    surAll.SummedUpMark = markTBCN.Value;
                                    surAll.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordBusiness.Insert(surAll);
                                }
                            }
                        }
                    }
                    #endregion
                    #region mon nhan xet
                    else
                    {
                        List<JudgeRecord> lstJr = lstJrAll.Where(o => o.PeriodID == null && o.SubjectID == eachSubjectId).ToList();

                        for (int i = 0; i < lstBoSubject.Count; i++)
                        {
                            ImportFromOtherSystemBO bo = lstBoSubject[i];
                            List<object> lstM = bo.lstM;
                            List<object> lstP = bo.lstP;
                            List<object> lstV = bo.lstV;

                            //Lay cac record diem cua hoc sinh nay
                            List<JudgeRecord> lstJrOfPupil = lstJr.Where(o => o.PupilID == bo.PupilID).ToList();

                            //Diem mieng
                            for (int j = 0; j < soCotDiemMieng; j++)
                            {
                                if (j >= lstM.Count())
                                {
                                    break;
                                }
                                string mark = (string)lstM[j];

                                //Kiem tra co diem hay chua
                                JudgeRecord jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                                //update
                                if (jr != null)
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr.Judgement = mark;
                                        jr.ModifiedDate = DateTime.Now;
                                        jr.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordBusiness.Update(jr);
                                    }
                                    else
                                    {
                                        JudgeRecordBusiness.Delete(jr.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr = new JudgeRecord();
                                        jr.AcademicYearID = academicYearId;
                                        jr.ClassID = classId;
                                        jr.CreatedDate = DateTime.Now;
                                        jr.Last2digitNumberSchool = schoolId % 100;
                                        jr.Judgement = mark;
                                        jr.MarkedDate = DateTime.Now;
                                        jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                        jr.OrderNumber = j + 1;
                                        jr.PeriodID = null;
                                        jr.PupilID = bo.PupilID;
                                        jr.SchoolID = schoolId;
                                        jr.Semester = semester;
                                        jr.SubjectID = eachSubjectId;
                                        jr.Title = "M" + (j + 1);
                                        jr.Year = aca.Year;
                                        jr.CreatedAcademicYear = aca.Year;
                                        jr.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordBusiness.Insert(jr);
                                    }
                                }
                            }

                            //Diem 15P
                            for (int j = 0; j < soCotDiem15Phut; j++)
                            {
                                if (j >= lstP.Count())
                                {
                                    break;
                                }
                                string mark = (string)lstP[j];

                                //Kiem tra co diem hay chua
                                JudgeRecord jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                                //update
                                if (jr != null)
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr.Judgement = mark;
                                        jr.ModifiedDate = DateTime.Now;
                                        jr.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordBusiness.Update(jr);
                                    }
                                    else
                                    {
                                        JudgeRecordBusiness.Delete(jr.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr = new JudgeRecord();
                                        jr.AcademicYearID = academicYearId;
                                        jr.ClassID = classId;
                                        jr.CreatedDate = DateTime.Now;
                                        jr.Last2digitNumberSchool = schoolId % 100;
                                        jr.Judgement = mark;
                                        jr.MarkedDate = DateTime.Now;
                                        jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                        jr.OrderNumber = j + 1;
                                        jr.PeriodID = null;
                                        jr.PupilID = bo.PupilID;
                                        jr.SchoolID = schoolId;
                                        jr.Semester = semester;
                                        jr.SubjectID = eachSubjectId;
                                        jr.Title = "P" + (j + 1);
                                        jr.Year = aca.Year;
                                        jr.CreatedAcademicYear = aca.Year;
                                        jr.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordBusiness.Insert(jr);
                                    }
                                }
                            }

                            //Diem 1T
                            for (int j = 0; j < soCotDiemKiemTra; j++)
                            {
                                if (j >= lstV.Count())
                                {
                                    break;
                                }
                                string mark = (string)lstV[j];

                                //Kiem tra co diem hay chua
                                JudgeRecord jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                                //update
                                if (jr != null)
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr.Judgement = mark;
                                        jr.ModifiedDate = DateTime.Now;
                                        jr.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordBusiness.Update(jr);
                                    }
                                    else
                                    {
                                        JudgeRecordBusiness.Delete(jr.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr = new JudgeRecord();
                                        jr.AcademicYearID = academicYearId;
                                        jr.ClassID = classId;
                                        jr.CreatedDate = DateTime.Now;
                                        jr.Last2digitNumberSchool = schoolId % 100;
                                        jr.Judgement = mark;
                                        jr.MarkedDate = DateTime.Now;
                                        jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                        jr.OrderNumber = j + 1;
                                        jr.PeriodID = null;
                                        jr.PupilID = bo.PupilID;
                                        jr.SchoolID = schoolId;
                                        jr.Semester = semester;
                                        jr.SubjectID = eachSubjectId;
                                        jr.Title = "V" + (j + 1);
                                        jr.Year = aca.Year;
                                        jr.CreatedAcademicYear = aca.Year;
                                        jr.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordBusiness.Insert(jr);
                                    }
                                }
                            }

                            //Diem HK
                            //Kiem tra co diem hay chua
                            JudgeRecord jrHK = lstJrOfPupil.FirstOrDefault(o => o.Title == "HK");
                            string markHK = (string)bo.HK;
                            //update
                            if (jrHK != null)
                            {
                                if (markHK != null)
                                {
                                    jrHK.Judgement = markHK;
                                    jrHK.ModifiedDate = DateTime.Now;
                                    jrHK.LogChange = employeeId.GetValueOrDefault();
                                    JudgeRecordBusiness.Update(jrHK);
                                }
                                else
                                {
                                    JudgeRecordBusiness.Delete(jrHK.JudgeRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markHK != null)
                                {
                                    jrHK = new JudgeRecord();
                                    jrHK.AcademicYearID = academicYearId;
                                    jrHK.ClassID = classId;
                                    jrHK.CreatedDate = DateTime.Now;
                                    jrHK.Last2digitNumberSchool = schoolId % 100;
                                    jrHK.Judgement = markHK;
                                    jrHK.MarkedDate = DateTime.Now;
                                    jrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                    jrHK.OrderNumber = 0;
                                    jrHK.PeriodID = null;
                                    jrHK.PupilID = bo.PupilID;
                                    jrHK.SchoolID = schoolId;
                                    jrHK.Semester = semester;
                                    jrHK.SubjectID = eachSubjectId;
                                    jrHK.Title = "HK";
                                    jrHK.Year = aca.Year;
                                    jrHK.CreatedAcademicYear = aca.Year;
                                    jrHK.LogChange = employeeId.GetValueOrDefault();

                                    JudgeRecordBusiness.Insert(jrHK);
                                }
                            }

                            //Diem TBM
                            //Kiem tra co diem hay chua
                            SummedUpRecord sur = lstSur.FirstOrDefault(o => o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                            string markTB = (string)bo.TB;
                            //update
                            if (sur != null)
                            {
                                if (markTB != null)
                                {
                                    sur.JudgementResult = markTB;
                                    sur.SummedUpDate = DateTime.Now;

                                    SummedUpRecordBusiness.Update(sur);
                                }
                                else
                                {
                                    SummedUpRecordBusiness.Delete(sur.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTB != null)
                                {
                                    sur = new SummedUpRecord();
                                    sur.AcademicYearID = academicYearId;
                                    sur.ClassID = classId;
                                    sur.IsCommenting = isComment ? 1 : 0;
                                    sur.Last2digitNumberSchool = schoolId % 100;
                                    sur.PeriodID = null;
                                    sur.PupilID = bo.PupilID;
                                    sur.SchoolID = schoolId;
                                    sur.Semester = semester;
                                    sur.SubjectID = eachSubjectId;
                                    sur.SummedUpDate = DateTime.Now;
                                    sur.Year = aca.Year;
                                    sur.JudgementResult = markTB;
                                    sur.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordBusiness.Insert(sur);
                                }
                            }

                            //Diem TCN
                            //Kiem tra co diem hay chua
                            SummedUpRecord surAll = lstSurAll.FirstOrDefault(o => o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                            string markTBCN = (string)bo.TBCN;
                            //update
                            if (surAll != null)
                            {
                                if (markTBCN != null)
                                {
                                    surAll.JudgementResult = markTBCN;
                                    surAll.SummedUpDate = DateTime.Now;

                                    SummedUpRecordBusiness.Update(surAll);
                                }
                                else
                                {
                                    SummedUpRecordBusiness.Delete(surAll.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTBCN != null)
                                {
                                    surAll = new SummedUpRecord();
                                    surAll.AcademicYearID = academicYearId;
                                    surAll.ClassID = classId;
                                    surAll.IsCommenting = isComment ? 1 : 0;
                                    surAll.Last2digitNumberSchool = schoolId % 100;
                                    surAll.PeriodID = null;
                                    surAll.PupilID = bo.PupilID;
                                    surAll.SchoolID = schoolId;
                                    surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    surAll.SubjectID = eachSubjectId;
                                    surAll.SummedUpDate = DateTime.Now;
                                    surAll.Year = aca.Year;
                                    surAll.JudgementResult = markTBCN;
                                    surAll.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordBusiness.Insert(surAll);
                                }
                            }
                        }

                    }
                    #endregion
                }

                this.Save();
            }
        }

        #region SMAS EDU
        /// <summary>
        /// Cấp 1:Toán T1,2,3 GK T4,5 CK
        /// Thể dục NX1,2,3,4,5
        /// Cấp 2,3:Toán M P V KTHK 
        /// Thể Dục M P V KTHK
        /// - nghi co phep:2
        /// - nghi khong phep:1
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevel"></param>
        /// <param name="Year"></param>
        /// <param name="DateStart"></param>
        /// <param name="DateEnd"></param>
        /// <param name="CheckAbsent"></param>
        /// <returns></returns>
        public List<PupilMarkBO> GetMarkRecordByClass(int SchoolID, List<int> lstClassID, int EducationLevel,
             int AcademicYearID, DateTime DateStart, DateTime DateEnd, bool CheckAbsent, int Semester)
        {
            List<PupilMarkBO> lstPupilMarkBO = new List<PupilMarkBO>();
            try
            {
                // Kiem tra truong hoac nam hoc khong ton tai
                if (SchoolProfileBusiness.Find(SchoolID) == null)
                {
                    throw new BusinessException("Trường không tồn tại.");
                }
                if (AcademicYearBusiness.Find(AcademicYearID) == null)
                {
                    throw new BusinessException("Năm học không tồn tại.");
                }

                //lay len diem chi tiet cac mon theo datetime
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["lstClassID"] = lstClassID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["Semester"] = Semester;
                //Lay len diem cua lop
                IQueryable<MarkRecordWSBO> IQMarkRecord = MarkRecordBusiness.GetMardRecordOfAllSubjectInClass(dic);
                //Lay diem nhan xet cua lop
                IQueryable<JudgeRecordWSBO> IQJudgeRecord = JudgeRecordBusiness.GetAllJudgeRecordOfClass(dic);

                //Loc thoi gian cham diem mon tinh diem
                //Lay lai thoi gian bat dau va ket thuc cho chinh xac
                DateTime fromDate = new DateTime(DateStart.Year, DateStart.Month, DateStart.Day);
                DateTime toDate = new DateTime(DateEnd.Year, DateEnd.Month, DateEnd.Day).AddDays(1);
                bool isPrimary = EducationLevel >= 1 && EducationLevel <= 5;
                var IQFilterMarkRecordTmp = (from m in IQMarkRecord
                                             where m.MarkedDate >= fromDate
                                             && m.MarkedDate < toDate
                                             select new
                                             {
                                                 m.PupilID,
                                                 m.SubjectName,
                                                 m.SubjectOrderNumber,
                                                 m.MarkOrderNumber,
                                                 m.Mark,
                                                 m.Title,
                                                 m.MarkTypeID,
                                                 m.MarkedDate,
                                                 m.MarkredDatePrimary
                                             }).ToList();

                var IQFilterMarkRecord = (from m in IQFilterMarkRecordTmp
                                          where (isPrimary && !m.Title.Contains("VGK") && !m.Title.Contains("ĐGK")
                                                           && !m.Title.Contains("VCK") && !m.Title.Contains("ĐCK")
                                                           && !m.Title.Contains("VCN") && !m.Title.Contains("ĐCN"))
                                          || (isPrimary == false)
                                          select new
                                          {
                                              m.PupilID,
                                              m.SubjectName,
                                              m.SubjectOrderNumber,
                                              m.MarkOrderNumber,
                                              Mark = m.Mark.HasValue ? m.Mark.Value.ToString("0.#").Replace(",",".") : string.Empty,
                                              Title = !isPrimary && m.Title.Contains("M") ? "M" : !isPrimary && m.Title.Contains("P") ? "15P" : !isPrimary && m.Title.Contains("V")
                                                        ? "1T" : !isPrimary && m.Title.Contains("HK") ? "KTHK" : isPrimary && m.Title.Contains("ĐTX") && m.MarkredDatePrimary.HasValue
                                                        ? m.Title + (SetMonthWithPrimary(m.MarkredDatePrimary.Value.Month))
                                                        : isPrimary && m.Title.Contains("GK") ? "GK" : isPrimary && (m.Title.Contains("CK") || m.Title.Contains("CN")) ? "CK" : m.Title + " ",
                                              m.MarkTypeID
                                          }).ToList();

                //Loc thoi gian cham diem mon nhan xet
                var LsFilterJudgeRecordTmp = (from j in IQJudgeRecord
                                              where j.MarkedDate >= fromDate
                                              && j.MarkedDate < toDate
                                              select new
                                              {
                                                  j.PupilID,
                                                  j.SubjectID,
                                                  j.SubjectName,
                                                  j.SubjectOrderNumber,
                                                  j.MarkOrderNumber,
                                                  Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                                  j.MarkTypeID,
                                                  Title = j.Title,
                                                  EducationLevelID = j.EducationLevelID
                                              }).ToList();

                var LsFilterJudgeRecord = (from j in LsFilterJudgeRecordTmp
                                           select new
                                           {
                                               j.PupilID,
                                               j.SubjectID,
                                               j.SubjectName,
                                               j.SubjectOrderNumber,
                                               j.MarkOrderNumber,
                                               Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                               j.MarkTypeID,
                                               Title = j.Title.Contains("P") ? "15P" : j.Title.Contains("V") ? "1T" : j.Title.Contains("M") ? "M" : isPrimary ? "NX" + (Semester == 2 ? (j.EducationLevelID > 2 ? j.MarkOrderNumber + 5 : j.MarkOrderNumber + 4) : j.MarkOrderNumber) : (!isPrimary && j.Title.Contains("HK") ? "KTHK" : j.Title),
                                           }).ToList();
                //Loc nhung hoc sinh co diem mon tin diem phat sinh
                var IQMarkRecordGroup = (from p in IQFilterMarkRecord
                                         orderby p.MarkTypeID, p.SubjectOrderNumber, p.SubjectName, p.MarkOrderNumber
                                         group p by new
                                         {
                                             p.PupilID,
                                             p.SubjectName,
                                             p.SubjectOrderNumber,
                                             p.Title
                                         } into g
                                         select new
                                         {
                                             PupilID = g.Key.PupilID.Value,
                                             SubjectName = g.Key.SubjectName,
                                             SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                             OrderPrimaryID = SetOrderPrimary(isPrimary, g.Key.Title),
                                             MarkContent = (isPrimary && g.Key.Title.Contains("ĐTX")) ? (g.Key.Title.Replace("ĐTX", "T") + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                    : isPrimary && (g.Key.Title.Contains("GK") || g.Key.Title.Contains("CK")) ? (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + ", " + b))) : (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                         }).ToList();
                //Nhom diem cua cac mon tinh diem lai
                List<PupilMarkBO> IQMarkRecordResponse = (from mg in IQMarkRecordGroup
                                                          orderby mg.SubjectOrderNumber, mg.SubjectName, mg.OrderPrimaryID
                                                          group mg
                                                          by new
                                                          {
                                                              mg.PupilID,
                                                              mg.SubjectName
                                                          }
                                                              into g
                                                              select new PupilMarkBO
                                                              {
                                                                  PupilProfileID = g.Key.PupilID,
                                                                  Content = "-" + g.Key.SubjectName + "\n" + g.Select(x => x.MarkContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                                              }).ToList();

                //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                var IQMarkJudgeGroup = (from p in LsFilterJudgeRecord
                                        orderby p.MarkTypeID, p.MarkOrderNumber
                                        group p by new
                                        {
                                            p.PupilID,
                                            p.SubjectName,
                                            p.SubjectID,
                                            p.SubjectOrderNumber,
                                            Title = p.Title
                                        } into g
                                        select new
                                        {
                                            PupilID = g.Key.PupilID.Value,
                                            SubjectName = g.Key.SubjectName,
                                            SubjectID = g.Key.SubjectID,
                                            SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                            JudgementContent = g.Key.Title
                                                + ": " + g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))
                                        }).ToList();

                //Nhom diem cua cac mon nhan xet lai
                var IQMarkJudgeResponse = (from mg in IQMarkJudgeGroup
                                           orderby mg.SubjectOrderNumber, mg.SubjectName
                                           group mg
                                            by new
                                            {
                                                mg.PupilID,
                                                mg.SubjectName,
                                                mg.SubjectID
                                            }
                                               into g
                                               select new PupilMarkBO
                                               {
                                                   PupilProfileID = g.Key.PupilID,
                                                   Content = "-" + g.Key.SubjectName + "\n"
                                                            + g.Select(x => x.JudgementContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                               }).ToList();
                // loc hoc sinh trung nhau
                IQMarkJudgeResponse = (from p in IQMarkJudgeResponse
                                       group p by new { p.PupilProfileID }
                                           into g
                                           select new PupilMarkBO
                                           {
                                               PupilProfileID = g.Key.PupilProfileID,
                                               Content = g.Where(p => p.PupilProfileID == g.Key.PupilProfileID).Select(p => p.Content).Aggregate((a, b) => (a + b))
                                           }).ToList();

                IQMarkRecordResponse = (from p in IQMarkRecordResponse
                                        group p by new { p.PupilProfileID }
                                            into g
                                            select new PupilMarkBO
                                            {
                                                PupilProfileID = g.Key.PupilProfileID,
                                                Content = g.Where(p => p.PupilProfileID == g.Key.PupilProfileID).Select(p => p.Content).Aggregate((a, b) => (a + b))
                                            }).ToList();

                // Bo sung them mon
                foreach (var item in IQMarkJudgeResponse)
                {
                    var t = IQMarkRecordResponse.FirstOrDefault(o => o.PupilProfileID == item.PupilProfileID);
                    if (t != null)
                    {
                        t.Content += item.Content;
                        t.Content = t.Content.Substring(0, t.Content.Length - 1);
                    }
                    else
                    {
                        IQMarkRecordResponse.Add(item);
                    }
                }
                if (CheckAbsent)
                {
                    DateStart = DateTime.Now.Date.AddMonths(-1).AddDays(1);
                    DateEnd = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    //lay len danh sach hoc sinh nghi hoc
                    List<PupilAbsence> IQPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dic).Where(p => p.AbsentDate >= DateStart && p.AbsentDate <= DateEnd).ToList();

                    //Filter de Count so ngay nghi khong phep cua tung hoc sinh trong lop
                    var IQPupilAbsencePCount = (from p in IQPupilAbsence
                                                where p.IsAccepted == true
                                                group p by new
                                                {
                                                    p.PupilID
                                                } into g
                                                select new
                                                {
                                                    PupilProfileID = g.Key.PupilID,
                                                    Absent = g.Count() > 0 ? "\n-Nghỉ có phép: " + g.Count().ToString() : string.Empty
                                                }).ToList();
                    //Filter de Count so ngay nghi co phep cua tung hoc sinh trong lop
                    var IQPupilAbsenceKCount = (from p in IQPupilAbsence
                                                where p.IsAccepted == false
                                                group p by new
                                                {
                                                    p.PupilID
                                                } into g
                                                select new
                                                {
                                                    PupilProfileID = g.Key.PupilID,
                                                    Absent = g.Count() > 0 ? "\n-Nghỉ không phép: " + g.Count().ToString() : string.Empty
                                                }).ToList();

                    //Loc nhung hoc sinh co diem phat sinh
                    List<PupilMarkBO> IQFilterMarkRecordResponse = (from mr in IQMarkRecordResponse
                                                                    join fa in IQPupilAbsencePCount on mr.PupilProfileID equals fa.PupilProfileID into jP
                                                                    from g1 in jP.DefaultIfEmpty()
                                                                    join pa in IQPupilAbsenceKCount on mr.PupilProfileID equals pa.PupilProfileID into jK
                                                                    from g2 in jK.DefaultIfEmpty()
                                                                    select new PupilMarkBO
                                                                          {
                                                                              PupilProfileID = mr.PupilProfileID,
                                                                              Content = (mr.Content.EndsWith("\n") ? mr.Content.Substring(0, mr.Content.Length - 1) : mr.Content) +
                                                                              (g1 == null ? string.Empty : g1.Absent) +
                                                                              (g2 == null ? string.Empty : g2.Absent)
                                                                          }).Distinct().ToList();
                    return IQFilterMarkRecordResponse;
                }
                else
                {
                    return IQMarkRecordResponse;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
        }
        /// <summary>
        /// điểm của học sinh trong lớp.Content format:
        /// Cấp 1:Toan:7(học lực) Thể dục:Đ,CĐ
        /// Hạnh kiểm:Đ
        /// Xếp loại giáo dục:Khá
        /// Nghỉ có phép:1
        /// Nghỉ không phép:1
        /// Cấp 2,3:Toán:<TBM> Thể dục<HLM>
        /// Trung bình các môn:<8,0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// Danh hiệu thi đua:<xuất sắc>>
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="ClassID">ID lớp học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="Semester">học kỳ</param>
        /// <param name="EducationLevel">Cấp học (TH, THCS, THPT)</param>
        /// <returns></returns>
        public List<PupilMarkBO> GetListMarkRecordSemester(int SchoolID, List<int> lstClassID,List<int> lstPupilID, int AcademicYearID, int Semester, int grade)
        {
            try
            {
                //Validate semester
                if (Semester != GlobalConstants.SEMESTER_OF_YEAR_FIRST && Semester != GlobalConstants.SEMESTER_OF_YEAR_SECOND
                    && Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {

                    throw new BusinessException("Đợt không tồn tại");
                }
                else
                {
                    AcademicYear objAcademicYear = (from a in AcademicYearBusiness.All
                                                    where a.SchoolID == SchoolID
                                                    && a.AcademicYearID == AcademicYearID
                                                    select a).FirstOrDefault();
                    DateTime FromDateHKI = DateTime.Now;
                    DateTime ToDateHKI = DateTime.Now;
                    DateTime FromDateHKII = DateTime.Now;
                    DateTime ToDateHKII = DateTime.Now;
                    if (objAcademicYear != null)
                    {
                        FromDateHKI = objAcademicYear.FirstSemesterStartDate.Value.Date;
                        ToDateHKI = objAcademicYear.FirstSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                        FromDateHKII = objAcademicYear.SecondSemesterStartDate.Value.Date;
                        ToDateHKII = objAcademicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }
                    //Cap hoc
                    List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.Search(new Dictionary<string, object>() { { "Semester", Semester }, { "SchoolID", SchoolID }, { "ListClassID", lstClassID } })
                        .Select(o => new ClassSubjectBO
                    {
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        DisplayName = o.SubjectCat.DisplayName,
                        IsCommenting = o.IsCommenting,
                        OrderInSubject = o.SubjectCat.OrderInSubject
                    }).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                    //lay diem danh theo trong 1 thang cua hoc sinh
                    //chiendd1: bo sung parttition
                    int partitionId = SchoolID % 100;
                    IQueryable<PupilAbsence> lstPupilAbsent = (from p in PupilAbsenceBusiness.All
                                                               where lstClassID.Contains(p.ClassID)
                                                               && lstPupilID.Contains(p.PupilID)
                                                               && p.SchoolID == SchoolID
                                                               && p.AcademicYearID == AcademicYearID
                                                               && p.Last2digitNumberSchool == partitionId
                                                               && ((grade > 1 && ((Semester == 1 && p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                 || (Semester == 2 && p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII)
                                                                                 || (Semester == 3 && ((p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                                   || (p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII)))))
                                                               || (grade == 1 && ((Semester == 1 && p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                              || (Semester == 2 && ((p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                                || (p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII))))))
                                                               select p);

                    //lay diem chi tiet trung binh cua cac mon
                    List<SummedUpRecordBO> listSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordByListClassID(lstClassID,AcademicYearID, SchoolID, Semester, null).Where(p=>lstPupilID.Contains(p.PupilID)).ToList();
                    //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                    List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingByClassID(lstClassID,AcademicYearID, SchoolID, Semester, null).Where(p=>lstPupilID.Contains(p.PupilID)).ToList();
                    //lay danh hieu thi dua
                    List<PupilEmulationBO> listPupilEmulation = PupilEmulationBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() {
                                                                { "AcademicYearID", AcademicYearID } ,
                                                                { "lstClassID", lstClassID },
                                                                { "Semester", Semester }
                                                                }).Select(o => new PupilEmulationBO
                                                                {
                                                                    PupilID = o.PupilID,
                                                                    HonourAchivementTypeResolution = o.HonourAchivementType.Resolution
                                                                }).Where(p => lstPupilID.Contains(p.PupilID)).ToList();
                    List<PupilMarkBO> listMarkOfSemesterResponse = new List<PupilMarkBO>();
                    // Lay thong tin cho tung hoc sinh
                    // Thong tin diem theo hoc sinh
                    List<SummedUpRecordBO> listPupilSum = new List<SummedUpRecordBO>();
                    // Thong tin khen thuong
                    PupilEmulationBO pupilEmulationBO = new PupilEmulationBO();
                    //Object mapping vao list
                    PupilMarkBO objMarkOfSemester = new PupilMarkBO();
                    //object mapping thong tin diem trung binh mon (dung de tao content)
                    SummedUpRecordBO objSummedUpRecordBO = new SummedUpRecordBO();
                    int countAbsentP = 0;
                    int countAbsentK = 0;
                    int classID = 0;
                    foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                    {
                        int pupilID = pupilRank.PupilID;
                        classID = pupilRank.ClassID.HasValue ? pupilRank.ClassID.Value : 0;
                        // Khoi tao item
                        objMarkOfSemester = new PupilMarkBO();
                        // Lay len thong tin diem cua hoc sinh
                        listPupilSum = listSummedUpRecord.Where(o => o.ClassID == classID && o.PupilID == pupilID).ToList();
                        // Lay len thon tin khen thuong
                        pupilEmulationBO = listPupilEmulation.Find(o => o.ClassID == pupilRank.ClassID && o.PupilID == pupilID);
                        var lstClassSubjecttmp = listClassSubject.Where(p => p.ClassID == classID).ToList();
                        // Lay thong tin hoc luc hanh kiem
                        string pupilInfo = "";
                        countAbsentP = lstPupilAbsent.Where(p => p.ClassID == classID && p.PupilID == pupilID && p.IsAccepted == true).Count();
                        countAbsentK = lstPupilAbsent.Where(p => p.PupilID == pupilID && p.ClassID == classID && p.IsAccepted == false).Count();
                        // Thong diem diem theo mon
                        foreach (ClassSubjectBO cs in lstClassSubjecttmp)
                        {
                            objSummedUpRecordBO = listPupilSum.Find(o => o.SubjectID == cs.SubjectID);
                            if (objSummedUpRecordBO != null)
                            {
                                if (!objSummedUpRecordBO.SummedUpMark.HasValue && string.IsNullOrEmpty(objSummedUpRecordBO.JudgementResult))
                                {
                                    continue;
                                }
                                pupilInfo += cs.DisplayName + ": " + (cs.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? objSummedUpRecordBO.JudgementResult + "\n"
                                    : (objSummedUpRecordBO.SummedUpMark.HasValue ? (grade > 1 ? (objSummedUpRecordBO.SummedUpMark.Value == 0 ? "0" : objSummedUpRecordBO.SummedUpMark.Value == 10 ? "10" : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.0").Replace(",", "."))
                                    : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.#").Replace(",", ".")) : string.Empty)) + (grade > 1 && cs.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ? "\n" : "");
                                if (grade == 1 && objSummedUpRecordBO.SummedUpMark.HasValue)
                                {
                                    decimal SummedUpMark = objSummedUpRecordBO.SummedUpMark.Value;
                                    if (SummedUpMark > GlobalConstants.EXCELLENT_MARK)
                                    {
                                        pupilInfo += GlobalConstants.LEVEL_EXPERT + "\n";
                                    }
                                    else if (SummedUpMark > GlobalConstants.GOOD_MARK_PRIMARY)
                                    {
                                        pupilInfo += GlobalConstants.LEVEL_GOOD + "\n";
                                    }
                                    else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                                    {
                                        pupilInfo += GlobalConstants.LEVEL_NORMAL + "\n";
                                    }
                                    else
                                    {
                                        pupilInfo += GlobalConstants.LEVEL_WEAK + "\n";
                                    }
                                }
                            }
                        }
                        if (pupilInfo.EndsWith("\n"))
                        {
                            pupilInfo = pupilInfo.Substring(0, pupilInfo.Length - 1);
                        }
                        // Neu la cap 1 thi bo sung them thong tin nghi hoc
                        if (grade == 1)
                        {
                            if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                            {
                                pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                            }

                            if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                            {
                                pupilInfo += "\nXếp loại giáo dục: " + pupilRank.CapacityLevel;
                            }

                            if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                            {
                                pupilInfo += "\nDanh hiệu: " + pupilEmulationBO.HonourAchivementTypeResolution;
                            }

                            if (countAbsentP > 0)
                            {
                                pupilInfo += "\nNghỉ có phép: " + countAbsentP;
                            }

                            if (countAbsentK > 0)
                            {
                                pupilInfo += "\nNghỉ không phép: " + countAbsentK;
                            }
                        }
                        else
                        {
                            if (pupilRank.AverageMark.HasValue && pupilRank.AverageMark.Value > 0)
                            {
                                pupilInfo += "\nTrung bình các môn: " + pupilRank.AverageMark.Value.ToString("0.0").Replace(",", ".");
                            }

                            if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                            {
                                pupilInfo += "\nHọc lực: " + pupilRank.CapacityLevel;
                            }

                            if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                            {
                                pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                            }

                            if (pupilRank.Rank.HasValue && pupilRank.Rank.Value > 0)
                            {
                                pupilInfo += "\nXếp hạng: " + pupilRank.Rank.ToString();
                            }

                            if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                            {
                                pupilInfo += "\nDanh hiệu thi đua: " + pupilEmulationBO.HonourAchivementTypeResolution;
                            }

                            if (countAbsentP > 0)
                            {
                                pupilInfo += "\nNghỉ có phép: " + countAbsentP;
                            }

                            if (countAbsentK > 0)
                            {
                                pupilInfo += "\nNghỉ không phép: " + countAbsentK;
                            }

                        }
                        // Thong tin diem TBM
                        // gan thong tin chi tiet object de add
                        objMarkOfSemester.Content = pupilInfo;
                        objMarkOfSemester.PupilProfileID = pupilID;
                        // Gan vao list de tra ve
                        listMarkOfSemesterResponse.Add(objMarkOfSemester);
                    }
                    return listMarkOfSemesterResponse;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
        }
        /// <summary>
        /// lấy ds điểm đợt của học sinh trong lớp.Content format:
        /// Toán:<TBM>
        /// Thể dục:<HLM>
        /// Trung bình các môn:<8.0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="ClassID">ID lớp học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="PeriodID">ID đợt</param>
        /// <returns></returns>
        public List<PupilMarkBO> GetListMarkRecordTime(int SchoolID, int ClassID, int AcademicYearID, int PeriodID)
        {
            try
            {
                if (AcademicYearID == 0)
                {
                    throw new BusinessException("Năm học không tồn tại");
                }
                else if (PeriodID == 0)
                {
                    throw new BusinessException("Đợt không tồn tại");
                }
                else
                {
                    int Semester = 0;
                    //lay thong tin dot nam trong hoc ky nao va nam hoc nao
                    PeriodDeclaration objPeriodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
                    Semester = objPeriodDeclaration.Semester.Value;
                    AcademicYearID = objPeriodDeclaration.AcademicYearID;
                    //Lay len danh sach hoc sinh cua lop
                    List<PupilOfClassBO> IQPupilOfClass = PupilOfClassBusiness.GetPupilInClass(ClassID).ToList();
                    //lay diem chi tiet trung binh cua cac mon
                    List<SummedUpRecordBO> IQSummedUpRecord = (from s in SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, PeriodID)
                                                               join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                                               orderby sc.OrderInSubject
                                                               select s).ToList();
                    //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                    List<PupilOfClassRanking> IQPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(AcademicYearID, SchoolID, Semester, ClassID, PeriodID).ToList();
                    //Fill vao Content
                    //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                    var lstMarkJudgeGroup = (from m in IQSummedUpRecord
                                             where m.JudgementResult != null
                                             orderby m.OrderInClass
                                             group m by m.PupilID into g
                                             select new
                                             {
                                                 PupilProfileID = g.Key,
                                                 JudgeContent = (g.Select(x => (x.SubjectName + ": " + x.JudgementResult.ToString() + " "))).Aggregate((a, b) => (a + "\n" + b))
                                             }).ToList();
                    var lstMarkRecordPeriodGroup = (from m in IQSummedUpRecord
                                                    where m.SummedUpMark != null
                                                    orderby m.OrderInClass
                                                    group m by m.PupilID into g
                                                    select new
                                                    {
                                                        PupilProfileID = g.Key,
                                                        MarkContent = (g.Select(x => (x.SubjectName + ": " + (x.SummedUpMark.HasValue ? x.SummedUpMark.Value.ToString("0.0").Replace(",", ".") : x.SummedUpMark.ToString()) + " "))).Aggregate((a, b) => (a + "\n" + b))
                                                    }).ToList();
                    //Fill Diem thanh phan 
                    List<PupilMarkBO> IQMarkOfPeriodResponse = (from p in IQPupilOfClass
                                                                join j in lstMarkJudgeGroup on p.PupilID equals j.PupilProfileID into j1
                                                                from g1 in j1.DefaultIfEmpty()
                                                                join m in lstMarkRecordPeriodGroup on p.PupilID equals m.PupilProfileID into m1
                                                                from g2 in m1.DefaultIfEmpty()
                                                                select new PupilMarkBO
                                                                        {
                                                                            PupilProfileID = p.PupilID,
                                                                            Content = (g2 == null ? "" : g2.MarkContent) + (g2 != null && g2.MarkContent != "" ? "\n" : "") + (g1 == null ? "" : g1.JudgeContent)
                                                                        }).Distinct().ToList();
                    //Fill hoc luc, hanh kiem, xep hang
                    List<PupilMarkBO> IQFilterMarkOfPeriodResponse = (from mr in IQMarkOfPeriodResponse
                                                                      join pr in IQPupilRanking on mr.PupilProfileID equals pr.PupilID into g1
                                                                      from g2 in g1.DefaultIfEmpty()
                                                                      where mr.Content != ""
                                                                      select new PupilMarkBO
                                                                              {
                                                                                  PupilProfileID = mr.PupilProfileID,
                                                                                  Content = mr.Content +
                                                                                      //neu khong ton tai pupilranking
                                                                                  (g2 == null ?
                                                                                  string.Empty
                                                                                  :
                                                                                      //Neu ton tai pupilranking                                                                                  
                                                                                  (g2.AverageMark == null ? "" : "\nTrung bình các môn: " + (g2.AverageMark.HasValue ? g2.AverageMark.Value.ToString("0.0") : string.Empty)) +
                                                                                  (g2.CapacityLevel == null ? "" : "\nHọc lực:" + g2.CapacityLevel) +
                                                                                  (g2.ConductLevel == null ? "" : "\nHạnh kiểm: " + g2.ConductLevel) +
                                                                                  (g2.Rank == null ? "" : "\nXếp hạng: " + g2.Rank.ToString()))
                                                                              }).ToList();
                    return IQFilterMarkOfPeriodResponse;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
        }
        public List<PupilMarkBO> GetListMarkRecordTimeByAllClass(List<int> lstClassID,List<int> lstPupilID,int SchoolID,int AcademicYearID,int PeriodID)
        {
            try
            {
                if (AcademicYearID == 0)
                {
                    throw new BusinessException("Năm học không tồn tại");
                }
                else if (PeriodID == 0)
                {
                    throw new BusinessException("Đợt không tồn tại");
                }
                else
                {
                    int Semester = 0;
                    //lay thong tin dot nam trong hoc ky nao va nam hoc nao
                    PeriodDeclaration objPeriodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
                    Semester = objPeriodDeclaration.Semester.Value;
                    AcademicYearID = objPeriodDeclaration.AcademicYearID;
                    //Lay len danh sach hoc sinh cua lop
                    IDictionary<string, object> dicPOC = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",AcademicYearID},
                        {"SchoolID",SchoolID},
                        {"ListClassID",lstClassID},
                        {"lstPupilID",lstPupilID}
                    };
                    List<PupilOfClass> IQPupilOfClass = PupilOfClassBusiness.Search(dicPOC).ToList();
                    //lay diem chi tiet trung binh cua cac mon
                    List<SummedUpRecordBO> IQSummedUpRecord = (from s in SummedUpRecordBusiness.GetSummedUpRecordByListClassID(lstClassID, AcademicYearID, SchoolID, Semester, PeriodID)
                                                               join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                                               where lstPupilID.Contains(s.PupilID)
                                                               orderby sc.OrderInSubject
                                                               select s).ToList();
                    //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                    List<PupilOfClassRanking> IQPupilRanking = PupilRankingBusiness.GetPupilRankingByClassID(lstClassID, AcademicYearID, SchoolID, Semester, PeriodID).Where(p => lstPupilID.Contains(p.PupilID)).ToList();
                    //Fill vao Content
                    //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                    var lstMarkJudgeGroup = (from m in IQSummedUpRecord
                                             where m.JudgementResult != null
                                             orderby m.OrderInClass
                                             group m by m.PupilID into g
                                             select new
                                             {
                                                 PupilProfileID = g.Key,
                                                 JudgeContent = (g.Select(x => (x.SubjectName + ": " + x.JudgementResult.ToString() + " "))).Aggregate((a, b) => (a + "\n" + b))
                                             }).ToList();
                    var lstMarkRecordPeriodGroup = (from m in IQSummedUpRecord
                                                    where m.SummedUpMark != null
                                                    orderby m.OrderInClass
                                                    group m by m.PupilID into g
                                                    select new
                                                    {
                                                        PupilProfileID = g.Key,
                                                        MarkContent = (g.Select(x => (x.SubjectName + ": " + (x.SummedUpMark.HasValue ? x.SummedUpMark.Value.ToString("0.0").Replace(",", ".") : x.SummedUpMark.ToString()) + " "))).Aggregate((a, b) => (a + "\n" + b))
                                                    }).ToList();
                    //Fill Diem thanh phan 
                    List<PupilMarkBO> IQMarkOfPeriodResponse = (from p in IQPupilOfClass
                                                                join j in lstMarkJudgeGroup on p.PupilID equals j.PupilProfileID into j1
                                                                from g1 in j1.DefaultIfEmpty()
                                                                join m in lstMarkRecordPeriodGroup on p.PupilID equals m.PupilProfileID into m1
                                                                from g2 in m1.DefaultIfEmpty()
                                                                select new PupilMarkBO
                                                                {
                                                                    PupilProfileID = p.PupilID,
                                                                    Content = (g2 == null ? "" : g2.MarkContent) + (g2 != null && g2.MarkContent != "" ? "\n" : "") + (g1 == null ? "" : g1.JudgeContent)
                                                                }).Distinct().ToList();
                    //Fill hoc luc, hanh kiem, xep hang
                    List<PupilMarkBO> IQFilterMarkOfPeriodResponse = (from mr in IQMarkOfPeriodResponse
                                                                      join pr in IQPupilRanking on mr.PupilProfileID equals pr.PupilID into g1
                                                                      from g2 in g1.DefaultIfEmpty()
                                                                      where mr.Content != ""
                                                                      select new PupilMarkBO
                                                                      {
                                                                          PupilProfileID = mr.PupilProfileID,
                                                                          Content = mr.Content +
                                                                              //neu khong ton tai pupilranking
                                                                          (g2 == null ?
                                                                          string.Empty
                                                                          :
                                                                              //Neu ton tai pupilranking                                                                                  
                                                                          (g2.AverageMark == null ? "" : "\nTrung bình các môn: " + (g2.AverageMark.HasValue ? g2.AverageMark.Value.ToString("0.0") : string.Empty)) +
                                                                          (g2.CapacityLevel == null ? "" : "\nHọc lực:" + g2.CapacityLevel) +
                                                                          (g2.ConductLevel == null ? "" : "\nHạnh kiểm: " + g2.ConductLevel) +
                                                                          (g2.Rank == null ? "" : "\nXếp hạng: " + g2.Rank.ToString()))
                                                                      }).ToList();
                    return IQFilterMarkOfPeriodResponse;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
        }
        public List<PupilMarkBO> GetListExamMarkSemester(List<PupilProfileBO> lstPupil, int SchoolID, List<int> lstClassID, int AcademicYearID, int Semester, int grade)
        {
            try
            {
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {
                    return new List<PupilMarkBO>();
                }
                else
                {
                    #region Danh sach mon hoc cua lop
                    List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.Search(new Dictionary<string, object>() 
                        { 
                            { "Semester", Semester }, 
                            { "AcademicYearID", AcademicYearID }, 
                            { "SchoolID", SchoolID } 
                        })
                        .Where(p => lstClassID.Contains(p.ClassID))
                        .Select(o => new ClassSubjectBO
                        {
                            ClassID = o.ClassID,
                            SubjectID = o.SubjectID,
                            DisplayName = o.SubjectCat.DisplayName,
                            IsCommenting = o.IsCommenting,
                            OrderInSubject = o.SubjectCat.OrderInSubject
                        }).OrderBy(p => p.IsCommenting.HasValue ? p.IsCommenting.Value : 0).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                    #endregion
                    PupilProfileBO objPF = null;
                    List<int> lstPupilID = lstPupil.Select(p=>p.PupilProfileID).Distinct().ToList();
                    List<PupilMarkBO> LstExamMarkSemseter = new List<PupilMarkBO>();
                    PupilMarkBO examMarkSemesterObj = null;
                    int PupilID = 0;
                    StringBuilder Content = null;
                    ClassSubjectBO classSubjectObj = null;
                    if (grade != GlobalConstants.PRIMARY_EDUCATION_SCHOOL)
                    {
                        #region Lay diem cap 2,3
                        List<int> listMarkSubjectID = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK).Select(p => p.SubjectID).Distinct().ToList();
                        List<int> listJudgeSubjectID = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE).Select(p => p.SubjectID).Distinct().ToList();

                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = AcademicYearID;
                        dic["Semester"] = Semester;
                        dic["Title"] = GlobalConstants.MARK_TYPE_HK;
                        dic["ListPupilID"] = lstPupilID;

                        //Lay diem thi theo mon tinh diem
                        List<VMarkRecord> lstMark = VMarkRecordBusiness.SearchBySchool(SchoolID, dic).Where(m => listMarkSubjectID.Contains(m.SubjectID) && lstClassID.Contains(m.ClassID)).ToList();
                        //Lay diem mon nhan xet
                        List<VJudgeRecord> lstJudge = VJudgeRecordBusiness.SearchBySchool(SchoolID, dic).Where(m => listJudgeSubjectID.Contains(m.SubjectID) && lstClassID.Contains(m.ClassID)).ToList();

                        List<VMarkRecord> lstMarkRecordWithPupil = null;
                        List<VJudgeRecord> lstJudgeRecordWithPupil = null;
                        List<VMarkRecord> lstMarkRecordWithSubject = null;
                        List<VJudgeRecord> lstJudgeRecordWithSubject = null;

                        for (int k = 0, num = lstPupil.Count; k < num; k++)
                        {
                            objPF = lstPupil[k];
                            PupilID = objPF.PupilProfileID;
                            examMarkSemesterObj = new PupilMarkBO();
                            Content = new StringBuilder();
                            examMarkSemesterObj.PupilProfileID = PupilID;
                            lstMarkRecordWithPupil = lstMark.Where(p => p.PupilID == PupilID).ToList();
                            lstJudgeRecordWithPupil = lstJudge.Where(p => p.PupilID == PupilID).ToList();
                            var lstCS = listClassSubject.Where(p => p.ClassID == objPF.CurrentClassID).ToList();
                            for (int i = 0; i < lstCS.Count; i++)
                            {
                                classSubjectObj = lstCS[i];

                                lstMarkRecordWithSubject = lstMarkRecordWithPupil.Where(p => p.SubjectID == classSubjectObj.SubjectID).ToList();
                                if (lstMarkRecordWithSubject != null && lstMarkRecordWithSubject.Count > 0)
                                {
                                    foreach (var item in lstMarkRecordWithSubject)
                                        Content.Append(classSubjectObj.DisplayName).Append(":").Append(string.Format("{0:0.#}", item.Mark)).Append("\n");
                                }

                                lstJudgeRecordWithSubject = lstJudgeRecordWithPupil.Where(p => p.SubjectID == classSubjectObj.SubjectID).ToList();
                                if (lstJudgeRecordWithSubject != null && lstJudgeRecordWithSubject.Count > 0)
                                {
                                    foreach (var item in lstJudgeRecordWithSubject)
                                        Content.Append(classSubjectObj.DisplayName).Append(":").Append(item.Judgement).Append("\n");
                                }
                            }
                            if (Content.Length > 0) examMarkSemesterObj.Content = Content.ToString();
                            LstExamMarkSemseter.Add(examMarkSemesterObj);
                        }

                        return LstExamMarkSemseter;
                        #endregion
                    }
                    else
                    {
                        #region lay diem cap 1
                        // chi lay nhung mon tinh diem
                        SummedEvaluation summedEvaluationObj = null;
                        int partition = UtilsBusiness.GetPartionId(SchoolID);
                        listClassSubject = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK).ToList();
                        List<int> listSubjectID = listClassSubject.Select(p => p.SubjectID).ToList();

                        List<SummedEvaluation> lstSummedEvaluation = (from m in SummedEvaluationBusiness.All
                                                                      where m.LastDigitSchoolID == partition
                                                                       && m.AcademicYearID == AcademicYearID
                                                                       && m.SchoolID == SchoolID
                                                                       && lstClassID.Contains(m.ClassID)
                                                                       && m.SemesterID == Semester
                                                                       && listSubjectID.Contains(m.EvaluationCriteriaID)
                                                                       && lstPupilID.Contains(m.PupilID)
                                                                      select m).ToList();
                        List<SummedEvaluation> lstSummedEvaluationPupil = null;
                        List<SummedEvaluation> lstSummedEvaluationSub = null;
                        for (int k = 0, num = lstPupil.Count; k < num; k++)
                        {
                            objPF = lstPupil[k];
                            PupilID = objPF.PupilProfileID;
                            lstSummedEvaluationPupil = lstSummedEvaluation.Where(p => p.PupilID == PupilID).ToList();
                            examMarkSemesterObj = new PupilMarkBO();
                            Content = new StringBuilder();
                            examMarkSemesterObj.PupilProfileID = PupilID;
                            var lstCS = listClassSubject.Where(p => p.ClassID == objPF.CurrentClassID).ToList();
                            for (int i = 0; i < lstCS.Count; i++)
                            {
                                classSubjectObj = lstCS[i];
                                if (lstSummedEvaluationPupil != null && lstSummedEvaluationPupil.Count > 0)
                                {
                                    lstSummedEvaluationSub = lstSummedEvaluationPupil.Where(p => p.EvaluationCriteriaID == classSubjectObj.SubjectID).ToList();
                                    if (lstSummedEvaluationSub != null && lstSummedEvaluationSub.Count > 0)
                                    {
                                        foreach (var item in lstSummedEvaluationSub)
                                            Content.Append(classSubjectObj.DisplayName).Append(":").Append(string.Format("{0:0.#}", summedEvaluationObj.PeriodicEndingMark)).Append("\n");
                                    }
                                }
                            }
                            if (Content.Length > 0) examMarkSemesterObj.Content = Content.ToString();
                            LstExamMarkSemseter.Add(examMarkSemesterObj);
                        }
                        return LstExamMarkSemseter;

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilMarkBO>();
            }
        }
        private int SetMonthWithPrimary(int month)
        {
            int Item = 0;
            if (month == 1)
            {
                Item = 5;
            }
            else if (month == 2)
            {
                Item = 6;
            }
            else if (month == 3)
            {
                Item = 7;
            }
            else if (month == 4)
            {
                Item = 8;
            }
            else if (month == 5)
            {
                Item = 9;
            }
            else if (month == 9)
            {
                Item = 1;
            }
            else if (month == 10)
            {
                Item = 2;
            }
            else if (month == 11)
            {
                Item = 3;
            }
            else if (month == 12)
            {
                Item = 4;
            }
            return Item;
        }
        private int SetOrderPrimary(bool isPrimary, string title)
        {
            int orderID = 0;
            if (isPrimary)
            {
                if (title.Contains("ĐTX1") || title.Contains("ĐTX6"))
                {
                    orderID = 1;
                }

                if (title.Contains("ĐTX2") || title.Contains("ĐTX7"))
                {
                    orderID = 2;
                }

                if (title.Contains("ĐTX3"))
                {
                    orderID = 3;
                }

                if (title.Contains("GK"))
                {
                    orderID = 4;
                }

                if (title.Contains("ĐTX4") || title.Contains("ĐTX8"))
                {
                    orderID = 5;
                }

                if (title.Contains("ĐTX5") || title.Contains("ĐTX9"))
                {
                    orderID = 5;
                }

                if (title.Contains("CK"))
                {
                    orderID = 6;
                }
            }

            return orderID;
        }
        #endregion
    }
}
