/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  trangdd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class PupilPraiseBusiness
    {
        #region Validate
        /// <summary>     
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      
        private void Validate(PupilPraise PupilPraise)
        {
            ValidationMetadata.ValidateObject(PupilPraise);
   
            SchoolProfileBusiness.CheckAvailable(PupilPraise.SchoolID, "PupilPraise_Label_SchoolID", true);
            PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilPraise.PupilID);

            if (PupilProfile.CurrentSchoolID != PupilPraise.SchoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            if (PupilProfile.CurrentClassID != PupilPraise.ClassID)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying1");
            }

            if (PupilProfile.CurrentAcademicYearID != PupilPraise.AcademicYearID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            ClassProfile ClassProfile = ClassProfileBusiness.Find(PupilPraise.ClassID);

            if (ClassProfile.EducationLevelID != PupilPraise.EducationLevelID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //PupilID, PraiseTypeID, PraiseDate: duplicate
            bool Praiseduplicate = PupilPraiseRepository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilPraise",
                new Dictionary<string, object>()
                {
                    {"PupilID",PupilPraise.PupilID},
                    {"PraiseTypeID",PupilPraise.PraiseTypeID},
                    {"PraiseDate",PupilPraise.PraiseDate}
                },
                  new Dictionary<string, object>()
                 {
                    {"PupilPraiseID",PupilPraise.PupilPraiseID},                    
                });
            if (Praiseduplicate)
            {
                throw new BusinessException("Common_Validate_NotDuplicate_PupilPraise");
            }

            //LeavingDate <= DateTime.Now            
            Utils.ValidatePastDate(PupilPraise.PraiseDate, "PupilPraise_Label_PraiseDate");

            //AcademicYear(AcademicYearID).FirstSemesterStartDate < PraiseDate < AcademicYear(AcademicYearID).SecondSemesterEndDate. (Dựa vào AcademicYearID xác định SemesterStartDate, SemesterEndDate)
            AcademicYear AcademicYear = AcademicYearBusiness.Find(PupilPraise.AcademicYearID);
            if (AcademicYear.FirstSemesterStartDate > PupilPraise.PraiseDate || PupilPraise.PraiseDate > AcademicYear.SecondSemesterEndDate)
            {
                List<object> Params = new List<object>();
                Params.Add("PupilPraise_Label_PraiseDate");
                Params.Add(AcademicYear.FirstSemesterStartDate);
                Params.Add(AcademicYear.SecondSemesterEndDate);
                throw new BusinessException("PupilPraise_PraiseDate_Not_InAcademicYear_Time", Params);
            }

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = PupilPraise.PupilID;
            SearchInfo["AcademicYearID"] = PupilPraise.AcademicYearID;
            SearchInfo["ClassID"] = PupilPraise.ClassID;
            SearchInfo["Check"] = "Check";
            PupilOfClass pupilOfClass = PupilOfClassBusiness.SearchBySchool(PupilPraise.SchoolID, SearchInfo).FirstOrDefault();
            if (pupilOfClass != null && pupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying1");
            }

        }
        #endregion
        #region InsertPupilPraise
        /// <summary>
        /// Thêm mới thông tin khen thưởng học sinh
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      

        public void InsertPupilPraise(int UserID, PupilPraise PupilPraise)
        {
            Validate(PupilPraise); 
            this.Insert(PupilPraise);
        }
        #endregion
        #region UpdatePupilPraise
        /// <summary>
        /// Cập nhật thông tin khen thưởng học sinh
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      

        public void UpdatePupilPraise(int UserID, PupilPraise PupilPraise)
        {
            Validate(PupilPraise);
            this.Update(PupilPraise);
        }
        #endregion

        #region DeletePupilPraise
        /// <summary>
        /// Xóa thông tin khen thưởng học sinh
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      

        public void DeletePupilPraise(int UserID, int PupilPraiseID, int SchoolID)
        {
           
            new PupilPraiseBusiness(null).CheckAvailable((int)PupilPraiseID, "PupilPraise_Label_PupilPraiseID", false);

            PupilPraise PupilPraise = PupilPraiseRepository.Find(PupilPraiseID);
            PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilPraise.PupilID);
          
            if (PupilProfile.CurrentSchoolID != SchoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        
            if (!AcademicYearBusiness.IsCurrentYear(PupilPraise.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = PupilPraise.PupilID;
            SearchInfo["AcademicYearID"] = PupilPraise.AcademicYearID;
            SearchInfo["ClassID"] = PupilPraise.ClassID;
            SearchInfo["Check"] = "Check";
            PupilOfClass pupilOfClass = PupilOfClassBusiness.SearchBySchool(PupilPraise.SchoolID, SearchInfo).FirstOrDefault();
            //if (PupilProfile.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING)
            if(pupilOfClass.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying1");
            }

            this.Delete(PupilPraiseID);

        }
        #endregion

        #region Search
        private IQueryable<PupilPraise> Search(IDictionary<string, object> SearchInfo)
        {

            int PupilPraiseID = Utils.GetInt(SearchInfo, "PupilPraiseID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int EducationLevelID = Utils.GetShort(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int PraiseTypeID = Utils.GetShort(SearchInfo, "PraiseTypeID");
            DateTime? PraiseDate = Utils.GetDateTime(SearchInfo, "PraiseDate");
            DateTime? FromPraiseDate = Utils.GetDateTime(SearchInfo, "FromPraiseDate");
            DateTime? ToPraiseDate = Utils.GetDateTime(SearchInfo, "ToPraiseDate");
            int PraiseLevel = (int)Utils.GetInt(SearchInfo, "PraiseLevel");
            bool? IsRecordedInSchoolReport = Utils.GetNullableBool(SearchInfo, "IsRecordedInSchoolReport");
            string Description = Utils.GetString(SearchInfo, "Description");
            string PupilCode = Utils.GetString(SearchInfo, "PupilCode");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            DateTime? Brithday = Utils.GetDateTime(SearchInfo, "Bridthday");
            IQueryable<PupilPraise> Query = from p in PupilPraiseRepository.All
                                            join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                            where q.IsActive == true
                                            select p;


            if (ClassID != 0)
            {
                Query = from pp in Query
                        join cp in ClassProfileBusiness.All on pp.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && pp.ClassID == ClassID
                        select pp;
            }
            if (PupilPraiseID != 0)
            {
                Query = Query.Where(o => o.PupilPraiseID == PupilPraiseID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }
            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (PraiseTypeID != 0)
            {
                Query = Query.Where(o => o.PraiseTypeID == PraiseTypeID);
            }
            if (PraiseDate.HasValue)
            {
                Query = Query.Where(o => o.PraiseDate == PraiseDate);
            }
            if (FromPraiseDate.HasValue)
            {
                Query = Query.Where(o => o.PraiseDate >= FromPraiseDate);
            }
            if (ToPraiseDate.HasValue)
            {
                Query = Query.Where(o => o.PraiseDate <= ToPraiseDate);
            }
            if (PraiseLevel != 0)
            {
                Query = Query.Where(o => o.PraiseLevel == PraiseLevel);
            }
            if (IsRecordedInSchoolReport.HasValue)
            {
                Query = Query.Where(o => o.IsRecordedInSchoolReport == IsRecordedInSchoolReport);
            }
            //if (Description.Trim().Length != 0)
            //{
            //    Query = Query.Where(o => o.Description.Contains(Description.ToLower()));
            //}
            if (PupilCode.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            }
            if (FullName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
            Query = Query.OrderBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);
            return Query;
        }
        #endregion
        #region Search by School
        public IQueryable<PupilPraise> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }
        #endregion
    }
}
