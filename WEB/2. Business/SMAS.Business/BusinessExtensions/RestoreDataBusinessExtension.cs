﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;

namespace SMAS.Business.Business
{
    public partial class RestoreDataBusiness
    {
        public string RestoreData(Guid restoreDataId)
        {
            RESTORE_DATA objRestore = RestoreDataBusiness.Find(restoreDataId);
            if (objRestore == null)
            {
                return "Không tìm thấy dữ liệu phục hồi";
            }

            int partitionId = UtilsBusiness.GetPartionId(objRestore.SCHOOL_ID, 100);
            List<RESTORE_DATA_DETAIL> lstRestoreDetail = RestoreDataDetailBusiness.All
                                                    .Where(s => s.RESTORE_DATA_ID == objRestore.RESTORE_DATA_ID
                                                           && s.SCHOOL_ID == objRestore.SCHOOL_ID
                                                           && s.ACADEMIC_YEAR_ID == objRestore.ACADEMIC_YEAR_ID
                                                           && s.LAST_2DIGIT_NUMBER_SCHOOL == partitionId).OrderBy(s => s.ORDER_ID).ToList();
            if (lstRestoreDetail == null || lstRestoreDetail.Count == 0)
            {
                return "Không tìm thấy dữ liệu phục hồi";
            }

            List<RestoreSqlBO> lstRetVal = RestoreData(objRestore, lstRestoreDetail);
            var objResSql = lstRetVal.FirstOrDefault(s => s.Error);
            if (objResSql != null && objRestore.RESTORE_DATA_TYPE_ID == RestoreDataConstant.RESTORE_DATA_TYPE_SCHOOL)
            {
                return "Tài khoản trường đã tồn tại. Vui lòng kiểm tra lại thông tin trường";
            }
            if (objResSql != null && objRestore.RESTORE_DATA_TYPE_ID == RestoreDataConstant.RESTORE_DATA_TYPE_YEAR)
            {
                return "Năm học đã tồn tại. Vui lòng kiểm tra lại thông tin năm học";
            }


            List<string> lstSql = lstRetVal.Where(s =>!string.IsNullOrEmpty(s.Sql)).OrderBy(s => s.OrderId).Select(s => s.Sql).ToList();
            bool retVal = RestoreDataBusiness.ExcuteSql(lstSql);
            if (retVal)
            {
                //Phuc hoi thong tin hoc sinh
                if (objRestore.RESTORE_DATA_TYPE_ID == RestoreDataConstant.RESTORE_DATA_TYPE_CLASS ||
               objRestore.RESTORE_DATA_TYPE_ID == RestoreDataConstant.RESTORE_DATA_TYPE_PUPIL ||
               objRestore.RESTORE_DATA_TYPE_ID == RestoreDataConstant.RESTORE_DATA_TYPE_YEAR)
                {
                    List<PupilOfClass> lstPoc = lstRetVal.Where(s => s.PupilOfass != null).Select(s => s.PupilOfass).ToList();
                    if (lstPoc != null && lstPoc.Count > 0)
                    {
                        PupilOfClassBusiness.BulkInsert(lstPoc, ColumnMapping.Instance.PupilOfClass(), "");
                    }
                }

                //phuc hoi thong tin diem
                if (objRestore.RESTORE_DATA_TYPE_ID == RestoreDataConstant.RESTORE_DATA_TYPE_MARK)
                {
                    List<MarkRecord> lstMark = lstRetVal.Where(s => s.MarkRecord != null).Select(s => s.MarkRecord).ToList();
                    if (lstMark != null && lstMark.Count > 0)
                    {
                        MarkRecordBusiness.BulkInsert(lstMark, ColumnMapping.Instance.MarkRecord(), "");
                    }

                    List<JudgeRecord> lstJudgeMark = lstRetVal.Where(s => s.JudgeRecord != null).Select(s => s.JudgeRecord).ToList();
                    if (lstJudgeMark != null && lstJudgeMark.Count > 0)
                    {
                        JudgeRecordBusiness.BulkInsert(lstJudgeMark, ColumnMapping.Instance.JudgeRecord(), "");
                    }

                    //Diem tong ket

                    List<SummedUpRecord> lstSummed = lstRetVal.Where(s => s.SummedUpRecord != null).Select(s => s.SummedUpRecord).ToList();
                    if (lstSummed != null && lstSummed.Count > 0)
                    {
                        SummedUpRecordBusiness.BulkInsert(lstSummed, ColumnMapping.Instance.SummedUpRecord(), "");
                    }

                    //so tay giao vien thang
                    List<TeacherNoteBookMonth> lstTeacherNoteBookMonth = lstRetVal.Where(s => s.TeacherNoteBookMonth != null).Select(s => s.TeacherNoteBookMonth).ToList();
                    if (lstTeacherNoteBookMonth != null && lstTeacherNoteBookMonth.Count > 0)
                    {
                        TeacherNoteBookMonthBusiness.BulkInsert(lstTeacherNoteBookMonth, ColumnMapping.Instance.TeacherNoteBookMonth(), "");

                    }

                    //so tay giao vien hoc ky
                    List<TeacherNoteBookSemester> lstTeacherNoteBookSemester = lstRetVal.Where(s => s.TeacherNoteBookSemester != null).Select(s => s.TeacherNoteBookSemester).ToList();
                    if (lstTeacherNoteBookSemester != null && lstTeacherNoteBookSemester.Count > 0)
                    {
                        TeacherNoteBookSemesterBusiness.BulkInsert(lstTeacherNoteBookSemester, ColumnMapping.Instance.TeacherNoteBookSemester(), "");
                    }

                    //So danh gia hoc sinh tieu hoc
                    List<RatedCommentPupil> lstRatedCommentPupil = lstRetVal.Where(s => s.RatedCommentPupil != null).Select(s => s.RatedCommentPupil).ToList();
                    if (lstRatedCommentPupil != null && lstRatedCommentPupil.Count > 0)
                    {
                        RatedCommentPupilBusiness.BulkInsert(lstRatedCommentPupil, ColumnMapping.Instance.RatedCommentPupil(), "");
                    }

                }
                objRestore.RESTORED_DATE = DateTime.Now;
                objRestore.RESTORED_STATUS = 1;
                RestoreDataBusiness.Update(objRestore);
                RestoreDataBusiness.Save();

            }
            return "";
        }

        private List<RestoreSqlBO> RestoreData(RESTORE_DATA objRestore, List<RESTORE_DATA_DETAIL> lstRestoreDetail)
        {
            RESTORE_DATA_DETAIL objRestoreDetail;
            List<RestoreSqlBO> lstSql = new List<RestoreSqlBO>();
            List<RestoreDataDetailBO> lstData = new List<RestoreDataDetailBO>();
            RestoreDataDetailBO objData;

            #region Chuyen du lieu vao Object de xu ly
            for (int i = 0; i < lstRestoreDetail.Count; i++)
            {
                objRestoreDetail = lstRestoreDetail[i];
                objData = new RestoreDataDetailBO
                {
                    ACADEMIC_YEAR_ID = objRestoreDetail.ACADEMIC_YEAR_ID,
                    CREATED_DATE = objRestoreDetail.CREATED_DATE,
                    END_DATE = objRestoreDetail.END_DATE,
                    IS_VALIDATE = objRestoreDetail.IS_VALIDATE,
                    KEY_VALUE = 0,
                    LAST_2DIGIT_NUMBER_SCHOOL = objRestoreDetail.LAST_2DIGIT_NUMBER_SCHOOL,
                    REPLACE_CODE = "",
                    ORDER_ID = objRestoreDetail.ORDER_ID,
                    RESTORE_DATA_DETAIL_ID = objRestoreDetail.RESTORE_DATA_DETAIL_ID,
                    RESTORE_DATA_ID = objRestoreDetail.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRestoreDetail.RESTORE_DATA_TYPE_ID,
                    TABLE_NAME = objRestoreDetail.TABLE_NAME,
                    //VALIDATE_CODE = objResUser.UserName,
                    SCHOOL_ID = objRestoreDetail.SCHOOL_ID,
                    SQL_DELETE = objRestoreDetail.SQL_DELETE,
                    //SQL_RESTORE = objResUser.SqlUndo,
                    SQL_UNDO = objRestoreDetail.SQL_UNDO,
                    //USER_ID = objResUser.UserId
                };

                #region chuyen cau lenh tu json sang object


                switch (objRestoreDetail.TABLE_NAME)
                {
                    case RestoreDataConstant.TABLE_USERS:
                        {
                            #region 1. Thong tin username
                            RestoreUserBO objResUser = JsonConvert.DeserializeObject<RestoreUserBO>(objRestoreDetail.SQL_UNDO);
                            if (objResUser == null)
                            {
                                continue;
                            }
                            objData.VALIDATE_CODE = objResUser.UserName;
                            objData.SQL_UNDO = objResUser.SqlUndo;
                            objData.USER_ID = objResUser.UserId;
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_USER_ACCOUNT:
                        {
                            #region 2. User Account


                            objData.VALIDATE_CODE = "";
                            objData.SQL_UNDO = objRestoreDetail.SQL_UNDO;
                            objData.USER_ID = "";
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_SCHOOL_PROFILE:
                        {
                            #region 3. thong tin truong
                            RestoreSchoolProfileBO objSchool = JsonConvert.DeserializeObject<RestoreSchoolProfileBO>(objRestoreDetail.SQL_UNDO);
                            if (objSchool == null)
                            {
                                continue;
                            }

                            objData.VALIDATE_CODE = objSchool.SchoolCode;
                            objData.SQL_UNDO = objSchool.SqlUndo;
                            objData.USER_ID = "";
                            objData.KEY_VALUE = objSchool.SchoolProfileId;
                            objData.REPLACE_CODE = objSchool.ReplaceSchoolCode;
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_PUPIL_PROFILE:
                        {
                            #region 4. Thong tin hoc sinh
                            if (!string.IsNullOrEmpty(objRestoreDetail.SQL_UNDO))
                            {
                                RestorePupilProfileBO objPupilFile = JsonConvert.DeserializeObject<RestorePupilProfileBO>(objRestoreDetail.SQL_UNDO);
                                if (objPupilFile != null && objPupilFile.PupilId>0)
                                {
                                    objData.VALIDATE_CODE = objPupilFile.PulpilCode;
                                    objData.REPLACE_CODE = string.Format("{0}_{1}", objPupilFile.PulpilCode, objPupilFile.PupilId);
                                    objData.KEY_VALUE = objPupilFile.PupilId;
                                    objData.SQL_UNDO = objPupilFile.SqlUndo;
                                }
                            }



                            ///Lay ra thong tin hoc sinh. lop hoc
                            if (!string.IsNullOrEmpty(objRestoreDetail.SQL_DELETE))
                            {
                                RestorePupilOfClassBO objPupilOfClass = JsonConvert.DeserializeObject<RestorePupilOfClassBO>(objRestoreDetail.SQL_DELETE);
                                if (objPupilOfClass != null)
                                {
                                    objData.PupilOfClass = new PupilOfClass
                                    {
                                        AcademicYearID = objPupilOfClass.AcademicYearID,
                                        AssignedDate = objPupilOfClass.AssignedDate,
                                        CreatedAcademicYear = objPupilOfClass.CreatedAcademicYear,
                                        ClassID = objPupilOfClass.ClassID,
                                        Description = objPupilOfClass.Description,
                                        EndDate = objPupilOfClass.EndDate,
                                        IsRegisterContract = objPupilOfClass.IsRegisterContract,
                                        Last2digitNumberSchool = objPupilOfClass.Last2digitNumberSchool,
                                        MSourcedb = objPupilOfClass.MSourcedb,
                                        M_OldID = objPupilOfClass.M_OldID,
                                        M_ProvinceID = objPupilOfClass.M_ProvinceID,
                                        NoConductEstimation = objPupilOfClass.NoConductEstimation,
                                        OrderInClass = objPupilOfClass.OrderInClass,
                                        PupilID = objPupilOfClass.PupilID,
                                        PupilOfClassID = objPupilOfClass.PupilOfClassID,
                                        SchoolID = objPupilOfClass.SchoolID,
                                        Status = objPupilOfClass.Status,
                                        SynchronizeID = objPupilOfClass.SynchronizeID,
                                        Year = objPupilOfClass.Year
                                    };
                                }
                            }
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_CLASS_PROFILE:
                        {
                            #region 5. Thong tin lop hoc
                            RestoreClassProfileBO objClass = JsonConvert.DeserializeObject<RestoreClassProfileBO>(objRestoreDetail.SQL_UNDO);
                            if (objClass == null)
                            {
                                continue;
                            }
                            objData.VALIDATE_CODE = objClass.ClassName;
                            objData.KEY_VALUE = objClass.ClassId;
                            objData.REPLACE_CODE = string.Format("{0}_{1}", objClass.ClassName, objClass.ClassId);
                            objData.SQL_UNDO = objClass.SqlUndo;
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_ACADEMIC_YEAR:
                        {
                            #region 6. Nam hoc

                            objData.VALIDATE_CODE = "";
                            objData.SQL_UNDO = objRestoreDetail.SQL_UNDO;
                            objData.USER_ID = "";
                            objData.KEY_VALUE = objRestoreDetail.ACADEMIC_YEAR_ID;
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_MARK_RECORD:
                        {
                            #region 7. Thong tin diem mon tinh diem
                            BakMarkRecordBO objMark = JsonConvert.DeserializeObject<BakMarkRecordBO>(objRestoreDetail.SQL_UNDO);
                            if (objMark == null)
                            {
                                continue;
                            }

                            objData.MarkRecord = new MarkRecord
                            {
                                AcademicYearID = objMark.AcademicYearID,
                                ClassID = objMark.ClassID,
                                CreatedDate = objMark.CreatedDate,
                                CreatedAcademicYear = objMark.CreatedAcademicYear,
                                IsOldData = objMark.IsOldData,
                                IsSMS = false,
                                LogChange = objMark.LogChange,
                                Last2digitNumberSchool = objMark.Last2digitNumberSchool,
                                Mark = objMark.Mark,
                                MarkRecordID = objMark.MarkRecordID,
                                MarkedDate = objMark.MarkedDate,
                                MarkTypeID = objMark.MarkTypeID,
                                ModifiedDate = objMark.ModifiedDate,
                                MSourcedb = objMark.MSourcedb,
                                M_MarkRecord = objMark.M_MarkRecord,
                                M_OldID = objMark.M_OldID,
                                OldMark = objMark.OldMark,
                                OrderNumber = objMark.OrderNumber,
                                PeriodID = objMark.PeriodID,
                                PupilID = objMark.PupilID,
                                SchoolID = objMark.SchoolID,
                                Semester = objMark.Semester,
                                SubjectID = objMark.SubjectID,
                                SynchronizeID = objMark.SynchronizeID,
                                Title = objMark.Title,
                                Year = objMark.Year
                            };

                            ///gia tri de xoa diem cua hoc sinh hien tai
                            if (!string.IsNullOrEmpty(objRestoreDetail.SQL_DELETE))
                            {
                                KeyDelMarkBO objDelExistsMark = JsonConvert.DeserializeObject<KeyDelMarkBO>(objRestoreDetail.SQL_DELETE);
                                if (objDelExistsMark != null)
                                {
                                    //DELETE MARK_RECORD WHERE ACADEMIC_YEAR_ID={0} and SCHOOL_ID={1} AND LAST_2DIGIT_NUMBER_SCHOOL={2} AND PUPIL_ID={3} AND CLASS_ID={4} AND SUBJECT_ID={5} AND MARK_TYPE_ID={6} AND SEMESTER={7}
                                    objData.SQL_UNDO = string.Format(RestoreDataConstant.SQL_DELETE_EXISTS_MARK,
                                            objDelExistsMark.AcademicYearID,
                                            objDelExistsMark.SchoolID,
                                            UtilsBusiness.GetPartionId(objDelExistsMark.SchoolID),
                                            objDelExistsMark.PupilID,
                                            objDelExistsMark.ClassID,
                                            objDelExistsMark.SubjectID,
                                            objDelExistsMark.MarkTypeID,
                                            objDelExistsMark.Semester.Value
                                            );
                                }
                            }

                            lstData.Add(objData);
                            #endregion
                            break;
                        }

                    case RestoreDataConstant.TABLE_JUDGE_RECORD:
                        {

                            #region 8. thong tin diem mon nhan xet

                            BakJudgeRecordBO objMark = JsonConvert.DeserializeObject<BakJudgeRecordBO>(objRestoreDetail.SQL_UNDO);
                            if (objMark == null)
                            {
                                continue;
                            }

                            objData.JudgeRecord = new JudgeRecord
                            {
                                AcademicYearID = objMark.AcademicYearID,
                                ClassID = objMark.ClassID,
                                CreatedDate = objMark.CreatedDate,
                                CreatedAcademicYear = objMark.CreatedAcademicYear,
                                IsOldData = objMark.IsOldData,
                                IsSMS = false,
                                LogChange = objMark.LogChange,
                                Last2digitNumberSchool = objMark.Last2digitNumberSchool,
                                Judgement = objMark.Judgement,
                                MarkedDate = objMark.MarkedDate,
                                MarkTypeID = objMark.MarkTypeID,
                                ModifiedDate = objMark.ModifiedDate,
                                MSourcedb = objMark.MSourcedb,
                                M_OldID = objMark.M_OldID,
                                OrderNumber = objMark.OrderNumber,
                                PeriodID = objMark.PeriodID,
                                PupilID = objMark.PupilID,
                                SchoolID = objMark.SchoolID,
                                Semester = objMark.Semester,
                                SubjectID = objMark.SubjectID,
                                SynchronizeID = objMark.SynchronizeID,
                                Title = objMark.Title,
                                Year = objMark.Year,
                                JudgeRecordID = objMark.JudgeRecordID,
                                ReTestJudgement = objMark.ReTestJudgement,
                                OldJudgement = objMark.OldJudgement
                            };

                            ///gia tri de xoa diem cua hoc sinh hien tai
                            if (!string.IsNullOrEmpty(objRestoreDetail.SQL_DELETE))
                            {
                                KeyDelMarkBO objDelExistsMark = JsonConvert.DeserializeObject<KeyDelMarkBO>(objRestoreDetail.SQL_DELETE);
                                if (objDelExistsMark != null)
                                {
                                    objData.SQL_UNDO = string.Format(RestoreDataConstant.SQL_DELETE_EXISTS_JUDGE_MARK,
                                            objDelExistsMark.AcademicYearID,
                                            objDelExistsMark.SchoolID,
                                            UtilsBusiness.GetPartionId(objDelExistsMark.SchoolID),
                                            objDelExistsMark.PupilID,
                                            objDelExistsMark.ClassID,
                                            objDelExistsMark.SubjectID,
                                            objDelExistsMark.MarkTypeID,
                                            objDelExistsMark.Semester.Value
                                            );
                                }
                            }

                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_SUMMED_UP_RECORD:
                        {

                            #region 9. Thong tin diem TBM

                            BakSummedUpBO objMark = JsonConvert.DeserializeObject<BakSummedUpBO>(objRestoreDetail.SQL_UNDO);
                            if (objMark == null)
                            {
                                continue;
                            }

                            objData.SummedUpRecord = new SummedUpRecord
                            {
                                AcademicYearID = objMark.AcademicYearID,
                                ClassID = objMark.ClassID,
                                CreatedAcademicYear = objMark.CreatedAcademicYear,
                                IsOldData = objMark.IsOldData,
                                Last2digitNumberSchool = objMark.Last2digitNumberSchool,
                                MSourcedb = objMark.MSourcedb,
                                M_OldID = objMark.M_OldID,
                                PeriodID = objMark.PeriodID,
                                PupilID = objMark.PupilID,
                                SchoolID = objMark.SchoolID,
                                Semester = objMark.Semester,
                                SubjectID = objMark.SubjectID,
                                SynchronizeID = objMark.SynchronizeID,
                                Year = objMark.Year,
                                IsCommenting = objMark.IsCommenting,
                                JudgementResult = objMark.JudgementResult,
                                Comment = objMark.Comment,
                                ReTestJudgement = objMark.ReTestJudgement,
                                ReTestMark = objMark.ReTestMark,
                                SummedUpDate = objMark.SummedUpDate,
                                SummedUpRecordID = objMark.SummedUpRecordID,
                                SummedUpMark = objMark.SummedUpMark
                            };

                            ///gia tri de xoa diem cua hoc sinh hien tai
                            if (!string.IsNullOrEmpty(objRestoreDetail.SQL_DELETE))
                            {
                                KeyDelMarkBO objDelExistsMark = JsonConvert.DeserializeObject<KeyDelMarkBO>(objRestoreDetail.SQL_DELETE);
                                if (objDelExistsMark != null)
                                {
                                    objData.SQL_UNDO = string.Format(RestoreDataConstant.SQL_DELETE_EXISTS_SUMMED_MARK,
                                        objDelExistsMark.PupilID,
                                        objDelExistsMark.ClassID,
                                            objDelExistsMark.SchoolID,
                                            objDelExistsMark.AcademicYearID,
                                            objDelExistsMark.SubjectID,
                                            objDelExistsMark.Semester.Value,
                                            objDelExistsMark.PeriodID ?? 0,
                                            UtilsBusiness.GetPartionId(objDelExistsMark.SchoolID)
                                            );
                                }
                            }
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_MONTH:
                        {
                            #region 10. So tay giao vien thang
                            TeacherNoteBookMonth objMark = JsonConvert.DeserializeObject<TeacherNoteBookMonth>(objRestoreDetail.SQL_UNDO);
                            if (objMark == null)
                            {
                                continue;
                            }
                            objData.TeacherNoteBookMonth = objMark;
                            objData.SQL_UNDO = string.Format(RestoreDataConstant.SQL_DELETE_TEACHER_NOTEBOOK_MONTH,
                                                            objMark.AcademicYearID,
                                                            objMark.SchoolID,
                                                            objMark.PartitionID,
                                                            objMark.PupilID,
                                                            objMark.ClassID,
                                                            objMark.SubjectID,
                                                            objMark.MonthID);
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_SEMESTER:
                        {
                            #region 11. So tay giao vien hoc ky
                            TeacherNoteBookSemester objMark = JsonConvert.DeserializeObject<TeacherNoteBookSemester>(objRestoreDetail.SQL_UNDO);
                            if (objMark == null)
                            {
                                continue;
                            }
                            objData.TeacherNoteBookSemester = objMark;
                            objData.SQL_UNDO = string.Format(RestoreDataConstant.SQL_DELETE_TEACHER_NOTEBOOK_SEMESTER,
                                                            objMark.AcademicYearID,
                                                            objMark.SchoolID,
                                                            objMark.PartitionID,
                                                            objMark.PupilID,
                                                            objMark.ClassID,
                                                            objMark.SubjectID,
                                                            objMark.SemesterID);
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_RATED_COMMENT_PUPIL:
                        {
                            #region 12. So tay giao vien hoc ky
                            RatedCommentPupil objRatedComment = JsonConvert.DeserializeObject<RatedCommentPupil>(objRestoreDetail.SQL_UNDO);
                            if (objRatedComment == null)
                            {
                                continue;
                            }
                            objData.RatedCommentPupil = objRatedComment;
                            objData.SQL_UNDO = string.Format(RestoreDataConstant.SQL_DELETE_RATED_COMMENT_PUPIL,
                                                            objRatedComment.AcademicYearID,
                                                            objRatedComment.SchoolID,
                                                            objRatedComment.ClassID,
                                                            objRatedComment.SubjectID,
                                                            objRatedComment.PupilID,
                                                            objRatedComment.SemesterID,
                                                            objRatedComment.EvaluationID,
                                                            objRatedComment.LastDigitSchoolID
                                                            );
                            lstData.Add(objData);
                            #endregion
                            break;
                        }
                }

                #endregion
            }
            #endregion
            if (lstData == null || lstData.Count == 0)
            {
                return new List<RestoreSqlBO>();
            }

            List<string> lstTable = lstData.Select(s => s.TABLE_NAME).Distinct().ToList();
            for (int i = 0; i < lstTable.Count; i++)
            {
                string tableName = lstTable[i];
                #region Chuyen cau lenh sang SQL
                switch (tableName)
                {
                    case RestoreDataConstant.TABLE_USERS:
                        {
                            #region 1.1 Tao lenh phuc hoi tai khoan
                            List<RestoreDataDetailBO> lstResUser = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstResUser == null || lstResUser.Count == 0)
                            {
                                continue;
                            }
                            //1. Lay danh sach user trong DB de kiem tra
                            List<string> lstUser = lstResUser.Select(s => s.VALIDATE_CODE).ToList();

                            String strUser = String.Join(",", lstUser);
                            List<aspnet_Users> lstValidateUser = aspnet_UsersBusiness.All.Where(s => strUser.Contains(s.UserName)).ToList();

                            for (int j = 0; j < lstResUser.Count; j++)
                            {
                                objData = lstResUser[j];
                                bool errMsg = false;
                                if (objData.IS_VALIDATE > 0 && lstValidateUser.Any(s => s.UserName == objData.VALIDATE_CODE))
                                {
                                    errMsg = true;
                                }
                                if (!string.IsNullOrEmpty(objData.USER_ID) && (!string.IsNullOrEmpty(objData.VALIDATE_CODE)))
                                {
                                    RestoreSqlBO objSql = new RestoreSqlBO
                                    {
                                        Error = errMsg,
                                        OrderId = 1,
                                        Sql = string.Format(objData.SQL_UNDO, objData.VALIDATE_CODE, objData.VALIDATE_CODE, objData.USER_ID, objData.VALIDATE_CODE)
                                    };
                                    lstSql.Add(objSql);
                                }
                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_USER_ACCOUNT:
                        {
                            #region 1.2 Phuc hoi user account
                            List<RestoreDataDetailBO> lstAccount = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstAccount == null || lstAccount.Count == 0)
                            {
                                continue;
                            }
                            for (int j = 0; j < lstAccount.Count; j++)
                            {
                                objData = lstAccount[j];
                                RestoreSqlBO objSql = new RestoreSqlBO
                                {
                                    Error = false,
                                    OrderId = 2,
                                    Sql = objData.SQL_UNDO
                                };
                                lstSql.Add(objSql);
                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_SCHOOL_PROFILE:
                        {
                            #region 1.3 Phuc hoi truong
                            RestoreDataDetailBO objResSchool = lstData.FirstOrDefault(s => s.TABLE_NAME == tableName);
                            if (objResSchool == null)
                            {
                                continue;
                            }

                            //Neu ma da ton tai thi thay bang ma tu sinh
                            SchoolProfile school = SchoolProfileBusiness.All.FirstOrDefault(s => s.SchoolCode == objResSchool.VALIDATE_CODE && s.IsActive);
                            if (school != null)
                            {
                                objResSchool.REPLACE_CODE = string.Format("{0}_{1}", objResSchool.VALIDATE_CODE, objResSchool.SCHOOL_ID);
                            }
                            else
                            {
                                objResSchool.REPLACE_CODE = objResSchool.VALIDATE_CODE;
                            }

                            RestoreSqlBO objSql = new RestoreSqlBO
                            {
                                Error = false,
                                OrderId = 3,
                                Sql = string.Format(RestoreDataConstant.SQL_UNDO_SCHOOL, objResSchool.REPLACE_CODE, objResSchool.KEY_VALUE)
                            };
                            lstSql.Add(objSql);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_PUPIL_PROFILE:
                        {
                            #region 1.4 Phuc hoi hoc sinh
                            List<RestoreDataDetailBO> lstPupil = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstPupil == null || lstPupil.Count == 0)
                            {
                                continue;
                            }
                            objData = lstPupil.FirstOrDefault();
                            List<string> lstPupilCode = lstPupil.Where(s => !string.IsNullOrEmpty(s.VALIDATE_CODE)).Select(s => s.VALIDATE_CODE).ToList();
                            List<PupilProfile> lstCheckPupil = new List<PupilProfile>();
                            if (lstPupilCode.Count > 0)
                            {
                                lstCheckPupil = PupilProfileBusiness.All.Where(s => s.CurrentSchoolID == objData.SCHOOL_ID && s.IsActive && lstPupilCode.Contains(s.PupilCode) && s.PupilProfileID != objData.KEY_VALUE).ToList();
                            }
                                
                            RestoreDataDetailBO objResPupil;
                            for (int j = 0; j < lstPupil.Count; j++)
                            {
                                objResPupil = lstPupil[j];

                                if (objResPupil == null)
                                {
                                    continue;
                                }
                                RestoreSqlBO objSql = new RestoreSqlBO();
                                objSql.OrderId = 2;
                                objSql.Error = false;
                                objSql.PupilOfass = objResPupil.PupilOfClass;
                                if (!string.IsNullOrEmpty(objResPupil.VALIDATE_CODE))
                                {
                                    if (lstCheckPupil.Any(s => s.PupilCode == objResPupil.VALIDATE_CODE))
                                    {
                                        objSql.Sql = string.Format(objResPupil.SQL_UNDO, objResPupil.REPLACE_CODE, objResPupil.KEY_VALUE);
                                    }
                                    else
                                    {
                                        objSql.Sql = string.Format(objResPupil.SQL_UNDO, objResPupil.VALIDATE_CODE, objResPupil.KEY_VALUE);
                                    }
                                }

                                lstSql.Add(objSql);

                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_CLASS_PROFILE:
                        {
                            #region 1.5 Phuc hoi lop hoc
                            List<RestoreDataDetailBO> lstClass = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstClass == null || lstClass.Count == 0)
                            {
                                continue;
                            }
                            objData = lstClass.FirstOrDefault();
                            List<string> lstClassName = lstClass.Select(s => s.VALIDATE_CODE).ToList();
                            List<ClassProfile> lstCheckClass = ClassProfileBusiness.All.Where(s => s.AcademicYearID == objData.ACADEMIC_YEAR_ID && s.IsActive == true && lstClassName.Contains(s.DisplayName)).ToList();
                            RestoreDataDetailBO objResClass;
                            for (int j = 0; j < lstClass.Count; j++)
                            {
                                objResClass = lstClass[j];

                                if (objResClass == null)
                                {
                                    continue;
                                }
                                RestoreSqlBO objSql = new RestoreSqlBO();
                                objSql.OrderId = 3;
                                objSql.Error = false;
                                if (lstCheckClass.Any(s => s.DisplayName == objResClass.VALIDATE_CODE))
                                {
                                    objSql.Sql = string.Format(objResClass.SQL_UNDO, objResClass.REPLACE_CODE, objResClass.KEY_VALUE);
                                }
                                else
                                {
                                    objSql.Sql = string.Format(objResClass.SQL_UNDO, objResClass.VALIDATE_CODE, objResClass.KEY_VALUE);
                                }
                                lstSql.Add(objSql);
                            }
                            #endregion
                            break;

                        }
                    case RestoreDataConstant.TABLE_ACADEMIC_YEAR:
                        {
                            #region 1.6 Phuc hoi nam hoc
                            RestoreDataDetailBO objResYear = lstData.FirstOrDefault(s => s.TABLE_NAME == tableName);
                            if (objResYear == null)
                            {
                                continue;
                            }

                            AcademicYear objResAcademicYear = AcademicYearBusiness.Find(objResYear.ACADEMIC_YEAR_ID);
                            bool errMsg = false;
                            string sqlUndo = "";
                            //neu nam hoc da khai bao
                            bool exists = AcademicYearBusiness.All.Any(s => s.SchoolID == objResAcademicYear.SchoolID && s.Year == objResAcademicYear.Year && s.IsActive == true);
                            if (exists)
                            {
                                errMsg = true;
                            }
                            else
                            {
                                //kiem tra cac gia tri ngay thang cua nam hoc khac có thoi gian long voi nam hoc bi xoa
                                //Neu ngay ket thuc nam hoc truoc>ngay bat dau nam hoc bi xoa 
                                //Hoac thoi gian ket thuc nam hoc xoa >ngay bat dau nam hoc sau do
                                List<AcademicYear> lstYear = AcademicYearBusiness.All.Where(s => s.SchoolID == objResAcademicYear.SchoolID && s.IsActive == true
                                                                                                && s.AcademicYearID != objResAcademicYear.AcademicYearID
                                                                                                && ((s.SecondSemesterEndDate >= objResAcademicYear.FirstSemesterStartDate && s.Year < objResAcademicYear.Year)
                                                                                                    || ((s.FirstSemesterStartDate <= objResAcademicYear.SecondSemesterEndDate && s.Year > objResAcademicYear.Year)))
                                                                                                ).ToList();
                                AcademicYear objChangeAcademic;

                                if (lstYear != null && lstYear.Count > 0)
                                {
                                    for (int y = 0; y < lstYear.Count; y++)
                                    {
                                        objChangeAcademic = lstYear[y];
                                        if (objChangeAcademic.SecondSemesterEndDate >= objResAcademicYear.FirstSemesterStartDate && objChangeAcademic.Year < objResAcademicYear.Year)
                                        {
                                            //Thay doi ngay bat dau nam hoc bi xoa: Lay ngay ket thuc nam hoc truoc +1 ngay
                                            objResAcademicYear.FirstSemesterStartDate = objChangeAcademic.SecondSemesterEndDate.Value.AddDays(1);
                                        }
                                        else if (objChangeAcademic.FirstSemesterStartDate <= objResAcademicYear.SecondSemesterEndDate)
                                        {
                                            //Thay doi ngay ket thuc nam hoc bi xoa: lay ngay bat dau nam hoc sau-1 ngay
                                            objResAcademicYear.SecondSemesterEndDate = objChangeAcademic.FirstSemesterStartDate.Value.AddDays(-1);
                                        }
                                    }

                                    sqlUndo = string.Format(RestoreDataConstant.SQL_UNDO_ACADEMIC_YEAR_CHANGETIME, objResAcademicYear.FirstSemesterEndDate.Value.ToString("dd/MM/yyyy"), objResAcademicYear.SecondSemesterEndDate.Value.ToString("dd/MM/yyyy"), objResAcademicYear.AcademicYearID);
                                }



                            }

                            RestoreSqlBO objSql = new RestoreSqlBO
                            {
                                Error = errMsg,
                                OrderId = 5,
                                //Sql = objResYear.SQL_UNDO
                            };
                            objSql.Sql = string.IsNullOrEmpty(sqlUndo) ? objResYear.SQL_UNDO : sqlUndo;
                            lstSql.Add(objSql);
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_MARK_RECORD:
                        {
                            #region 1.7 Phuc hoi diem
                            List<RestoreDataDetailBO> lstMark = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstMark == null || lstMark.Count == 0)
                            {
                                continue;
                            }
                            RestoreDataDetailBO objResMark;
                            for (int j = 0; j < lstMark.Count; j++)
                            {
                                objResMark = lstMark[j];

                                if (objResMark == null)
                                {
                                    continue;
                                }
                                RestoreSqlBO objSql = new RestoreSqlBO();
                                objSql.OrderId = 2;
                                objSql.Error = false;
                                objSql.MarkRecord = objResMark.MarkRecord;
                                objSql.Sql = objResMark.SQL_UNDO;
                                lstSql.Add(objSql);

                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_JUDGE_RECORD:
                        {
                            #region 1.8 Phuc hoi diem nhan xet
                            List<RestoreDataDetailBO> lstMark = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstMark == null || lstMark.Count == 0)
                            {
                                continue;
                            }
                            RestoreDataDetailBO objResMark;
                            for (int j = 0; j < lstMark.Count; j++)
                            {
                                objResMark = lstMark[j];

                                if (objResMark == null)
                                {
                                    continue;
                                }
                                RestoreSqlBO objSql = new RestoreSqlBO();
                                objSql.OrderId = 2;
                                objSql.Error = false;
                                objSql.JudgeRecord = objResMark.JudgeRecord;
                                objSql.Sql = objResMark.SQL_UNDO;
                                lstSql.Add(objSql);

                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_SUMMED_UP_RECORD:
                        {
                            #region 1.9 Phuc hoi diem TBM
                            List<RestoreDataDetailBO> lstMark = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstMark == null || lstMark.Count == 0)
                            {
                                continue;
                            }
                            RestoreDataDetailBO objResMark;
                            for (int j = 0; j < lstMark.Count; j++)
                            {
                                objResMark = lstMark[j];

                                if (objResMark == null)
                                {
                                    continue;
                                }
                                RestoreSqlBO objSql = new RestoreSqlBO();
                                objSql.OrderId = 2;
                                objSql.Error = false;
                                objSql.SummedUpRecord = objResMark.SummedUpRecord;
                                objSql.Sql = objResMark.SQL_UNDO;
                                lstSql.Add(objSql);

                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_MONTH:
                        {
                            #region 1.10 Phuc hoi so tay giao vien thang
                            List<RestoreDataDetailBO> lstMark = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstMark == null || lstMark.Count == 0)
                            {
                                continue;
                            }
                            RestoreDataDetailBO objResMark;
                            for (int j = 0; j < lstMark.Count; j++)
                            {
                                objResMark = lstMark[j];

                                if (objResMark == null)
                                {
                                    continue;
                                }
                                RestoreSqlBO objSql = new RestoreSqlBO();
                                objSql.OrderId = 1;
                                objSql.Error = false;
                                objSql.TeacherNoteBookMonth = objResMark.TeacherNoteBookMonth;
                                objSql.Sql = objResMark.SQL_UNDO;
                                lstSql.Add(objSql);
                            }
                            #endregion
                            break;
                        }
                    case RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_SEMESTER:
                        {
                            #region 1.11 Phuc hoi so tay giao vien hoc ky
                            List<RestoreDataDetailBO> lstMark = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstMark == null || lstMark.Count == 0)
                            {
                                continue;
                            }
                            RestoreDataDetailBO objResMark;
                            RestoreSqlBO objSql;
                            for (int j = 0; j < lstMark.Count; j++)
                            {
                                objResMark = lstMark[j];

                                if (objResMark == null)
                                {
                                    continue;
                                }
                                objSql = new RestoreSqlBO();
                                objSql.OrderId = 1;
                                objSql.Error = false;
                                objSql.TeacherNoteBookSemester = objResMark.TeacherNoteBookSemester;
                                objSql.Sql = objResMark.SQL_UNDO;
                                lstSql.Add(objSql);
                            }
                            #endregion
                            break;
                        }

                    case RestoreDataConstant.TABLE_RATED_COMMENT_PUPIL:
                        {
                            #region 1.12 Phuc hoi so danh gia hoc sinh Cap 1
                            List<RestoreDataDetailBO> lstMark = lstData.Where(s => s.TABLE_NAME == tableName).ToList();
                            if (lstMark == null || lstMark.Count == 0)
                            {
                                continue;
                            }
                            RestoreDataDetailBO objResMark;
                            RestoreSqlBO objSql;
                            for (int j = 0; j < lstMark.Count; j++)
                            {
                                objResMark = lstMark[j];

                                if (objResMark == null)
                                {
                                    continue;
                                }
                                objSql = new RestoreSqlBO();
                                objSql.OrderId = 1;
                                objSql.Error = false;
                                objSql.RatedCommentPupil = objResMark.RatedCommentPupil;
                                objSql.Sql = objResMark.SQL_UNDO;
                                lstSql.Add(objSql);
                            }
                            #endregion
                            break;
                        }
                }
                #endregion
            }

            return lstSql;
        }

    }
}
