﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL areaPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    public partial class EvaluationRewardBusiness
    {
        public IQueryable<EvaluationReward> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstRewardID = Utils.GetIntList(dic, "lstRewardID");
            List<long> lstEvaluationRewardID = Utils.GetLongList(dic, "lstEvaluationRewardID");
            IQueryable<EvaluationReward> iquery = EvaluationRewardBusiness.All;
            if (SchoolID != 0)
            {
                int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
                iquery = iquery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartitionID);
            }
            if (AcademicYearID != 0)
            {
                iquery = iquery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iquery = iquery.Where(p => p.ClassID == ClassID);
            }
            if (SemesterID != 0)
            {
                iquery = iquery.Where(p => p.SemesterID == SemesterID);
            }
            if (PupilID != 0)
            {
                iquery = iquery.Where(p => p.PupilID == PupilID);
            }
            if (lstClassID.Count > 0)
            {
                iquery = iquery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstPupilID.Count > 0)
            {
                iquery = iquery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (lstRewardID.Count > 0)
            {
                iquery = iquery.Where(p => lstRewardID.Contains(p.RewardID.Value));
            }
            if (lstEvaluationRewardID.Count > 0)
            {
                iquery = iquery.Where(p => lstEvaluationRewardID.Contains(p.EvaluationRewardID));
            }
            return iquery;
        }
        public ProcessedReport GetEvaluationRewardRepord(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT22;
            string inputParameterHashKey = ReportUtils.GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public void InsertEvaluationReward(List<EvaluationReward> lstEvaluationReward, IDictionary<string, object> dic)
        {
            List<EvaluationReward> lstDB = this.Search(dic).ToList();
            EvaluationRewardBusiness.DeleteAll(lstDB);
            EvaluationReward objInsert = null;
            for (int i = 0; i < lstEvaluationReward.Count; i++)
            {
                objInsert = lstEvaluationReward[i];
                objInsert.EvaluationRewardID = this.GetNextSeq<long>();
                objInsert.CreateTime = DateTime.Now;
                objInsert.UpdateTime = DateTime.Now;
                EvaluationRewardBusiness.Insert(objInsert);
            }
            EvaluationRewardBusiness.Save();
        }
        public void DeleteEvaluationReward(IDictionary<string, object> dic)
        {
            List<EvaluationReward> lstDB = this.Search(dic).ToList();
            EvaluationRewardBusiness.DeleteAll(lstDB);
            EvaluationRewardBusiness.Save();
        }
        public ProcessedReport InsertEvaluationRewarReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT22;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            string ClassName = string.Empty;
            string SemesterName = SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";
            if (ClassID > 0)
            {
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                ClassName = Utils.StripVNSignAndSpace(objCP.DisplayName);
            }
            else
            {
                ClassName = "Khoi" + EducationLevelID;
            }
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ClassName).Replace("[Semester]", SemesterName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public Stream CreateEvaluationRewardReport(IDictionary<string,object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT22;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            string SemesterName = SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? " Học kỳ I " : " Học kỳ II ";
            string ClassName = string.Empty;
            if (ClassID > 0)
            {
                ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
                ClassName = objCP.DisplayName;
            }
            else
            {
                ClassName = "Khối " + EducationLevelID;
            }
            //fill thong tin chung
            sheet.SetCellValue("A2", objSP.SchoolName.ToUpper());
            string Title = "DANH SÁCH KHEN THƯỞNG " + ClassName + SemesterName + "Năm học " + objAy.DisplayTitle;
            sheet.SetCellValue("A4", Title.ToUpper());
            //lay danh sach hoc sinh
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.Search(dic)
                                          .Select(p => new PupilOfClassBO
                                          {
                                              PupilID = p.PupilID,
                                              ClassID = p.ClassID,
                                              ClassName = p.ClassProfile.DisplayName,
                                              OrderInClass = p.OrderInClass,
                                              PupilFullName = p.PupilProfile.FullName,
                                              ClassOrder = p.ClassProfile.OrderNumber,
                                              EducationLevelID = p.ClassProfile.EducationLevelID,
                                              Status = p.Status
                                          }).ToList();
            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();
            List<int> lstCLassID = lstPOC.Select(p => p.ClassID).Distinct().ToList();
            //lay danh sach khen thuong cua hoc sinh trong 2 bang SummedEndingEvaluation + EvaluationRward
            if (lstCLassID.Count == 0)
            {
                lstCLassID.Add(ClassID);
            }
            IDictionary<string, object> dicSearchReward = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstCLassID},
                {"lstPupilID",lstPupilID},
                {"isReward",true}
            };
            List<SummedEndingEvaluation> lstSEE = SummedEndingEvaluationBusiness.GetSummedEndingEvaluation(dicSearchReward).Where(p => p.Reward.HasValue).ToList();
            SummedEndingEvaluation objSEE = null;

            dicSearchReward = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstCLassID},
                {"lstPupilID",lstPupilID},
                {"SemesterID",SemesterID}
            };
            List<EvaluationReward> lstER = this.Search(dicSearchReward).Where(p=>p.RewardID.HasValue).ToList();
            EvaluationReward objEvaluationReward = null;

            List<EvaluationRewardReportBO> lstERBO = new List<EvaluationRewardReportBO>();
            EvaluationRewardReportBO objERBO = null;
            for (int i = 0; i < lstSEE.Count; i++)
            {
                objSEE = lstSEE[i];
                objERBO = new EvaluationRewardReportBO();
                objERBO.PupilID = objSEE.PupilID;
                objERBO.ClassID = objSEE.ClassID;
                objERBO.RewardName = "Khen thưởng cuối năm";
                objERBO.Content = "Học sinh xuất sắc";
                lstERBO.Add(objERBO);
            }

            for (int i = 0; i < lstER.Count; i++)
            {
                objEvaluationReward = lstER[i];
                objERBO = new EvaluationRewardReportBO();
                objERBO.PupilID = objEvaluationReward.PupilID;
                objERBO.ClassID = objEvaluationReward.ClassID;
                if (objEvaluationReward.RewardID == 1)
                {
                    objERBO.RewardName = "Khen thưởng đột xuất";
                    objERBO.Content = objEvaluationReward.Content;
                }
                else
                {
                    objERBO.RewardName = "Khen thưởng cuối năm";
                    objERBO.Content = "Học sinh có thành tích vượt trội: " + objEvaluationReward.Content;
                }
                lstERBO.Add(objERBO);
            }

            var lstERBOtmp = (from erbo in lstERBO
                              join poc in lstPOC on new { erbo.ClassID, erbo.PupilID } equals new { poc.ClassID, poc.PupilID }
                              select new
                              {
                                  ClassID = erbo.ClassID,
                                  ClassName = poc.ClassName,
                                  PupilID = poc.PupilID,
                                  FullName = poc.PupilFullName,
                                  Status = poc.Status,
                                  ClassOrderID = poc.ClassOrder,
                                  PupilOrderID = poc.OrderInClass,
                                  RewardName = erbo.RewardName,
                                  Content = erbo.Content
                              }).OrderBy(p=>p.ClassOrderID).ThenBy(p=>p.ClassName)
                                        .ThenBy(p=>p.PupilOrderID).ThenBy(p=>p.FullName).ToList();

            int startRow = 7;
            for (int i = 0; i < lstERBOtmp.Count; i++)
            {
                var objERBOtmp = lstERBOtmp[i];
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objERBOtmp.ClassName);
                sheet.SetCellValue(startRow, 3, objERBOtmp.FullName);
                sheet.SetCellValue(startRow, 4, objERBOtmp.RewardName);
                sheet.SetCellValue(startRow, 5, objERBOtmp.Content);
                if (objERBOtmp.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                startRow++;
            }
            sheet.GetRange(7, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.Name = Utils.StripVNSignAndSpace(ClassName);
            return oBook.ToStream();
        }
        private class EvaluationRewardReportBO
        {
            public int PupilID { get; set; }
            public int ClassID { get; set; }
            public string RewardName { get; set; }
            public string Content { get; set; }
        }
    }
}
