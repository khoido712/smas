/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    /// <summary>
    /// MonthlyBalanceBusiness
    /// </summary>
    /// <author>Quanglm</author>
    /// <date>11/15/2012</date>
    public partial class MonthlyBalanceBusiness
    {

        /// <summary>
        /// GetMonthylyBalance
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">To date.</param>
        /// <returns>
        /// Int32
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/15/2012</date>
        public int GetMonthylyBalance(int SchoolID, DateTime FromDate, DateTime ToDate)
        {
            MonthlyBalance MonthlyBalance = this.MonthlyBalanceRepository.All
                .Where(o => o.SchoolID == SchoolID)
                .Where(o => o.BalanceMonth >= FromDate)
                .Where(o => o.BalanceMonth <= ToDate).FirstOrDefault();
            if (MonthlyBalance == null)
            {
                return 0;
            }
            return MonthlyBalance.Balance;
        }
    }
}