﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  trangdd
* @version $Revision: $
*/

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;

using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{

    public partial class MarkRecordHistoryBusiness
    {

        public void InsertMarkRecordHistoryAll(int UserID, List<MarkRecordHistory> ListMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, List<int> lstPupilExempted, IDictionary<string, object> dicParam)
        {
            try
            {
                int SchoolID = Utils.GetInt(dicParam, "SchoolID");
                int AcademicYearID = Utils.GetInt(dicParam, "AcademicYearID");
                int PeriodID = Utils.GetInt(dicParam, "PeriodID");
                int Semester = Utils.GetInt(dicParam, "SemesterID");
                int Year = Utils.GetInt(dicParam, "Year");
                int Grade = Utils.GetInt(dicParam, "Grade");
                bool isInsertAnySubject = Utils.GetBool(dicParam, "IsInsertAnySubject");
                int SubjectID = 0;
                int ClassID = 0;

                if (ListMarkRecord == null || ListMarkRecord.Count == 0)
                {
                    return;
                }
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                string strMarkTitle = this.GetMarkTitle(PeriodID);
                var lstPupilSubject = ListMarkRecord.Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                //ID hoc sinh
                List<int> lstPupilID = lstSummedUpRecord.Select(u => u.PupilID).Distinct().ToList();
                var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                .Distinct();

                List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                List<string> ArrLockedMark = null;

                //neu khong phai tai khoan quan tri truong
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                if (!isSchoolAdmin)
                {
                    string lstLockedMarkDetail = string.Empty;

                    if (isInsertAnySubject)
                    {
                        for (int i = 0; i < lstSubjectID.Count; i++)
                        {
                            SubjectID = lstSubjectID[i];
                            ClassID = lstClassID.FirstOrDefault();
                            lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                            // Neu tat ca con diem bi khoa thi bao loi
                            if (lstLockedMarkDetail.Contains("LHK") && lstSubjectID.Count == 1)
                            {
                                throw new BusinessException("MarkRecord_Validate_MarkLock");
                            }
                            else
                            {
                                ListMarkRecord = ListMarkRecord.Where(p => p.SubjectID != SubjectID && p.ClassID != ClassID).ToList();
                                continue;
                            }
                            // Chi lay cac con diem khong bi khoa
                            ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                            ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                        }
                    }
                    else
                    {
                        for (int i = 0; i < lstClassID.Count; i++)
                        {
                            ClassID = lstClassID[i];
                            SubjectID = lstSubjectID.FirstOrDefault();
                        }
                    }




                }
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                #region validate
                //Năm học không phải là năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("Common_Validate_IsCurrentYear");

                // Kiem tra quyen giao vien bo mon va kieu mon hoc
                ClassSubject classSubject = null;
                foreach (var sc in distinctSC)
                {
                    if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, sc.ClassID, sc.SubjectID, Semester))
                        throw new BusinessException("MarkRecord_Validate_InputMarkPermission");
                    // Kiem tra mon hoc la mon tinh diem
                    classSubject = ClassSubjectBusiness.SearchByClass(sc.ClassID, new Dictionary<string, object> { { "SubjectID", sc.SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();
                    if (classSubject == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK && classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                if (!isSchoolAdmin && !timeYear)
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }

                // Nam hoc dang xet khong truong ung voi truong
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra du lieu dot
                if (PeriodID > 0)
                {
                    // Kiem tra dot ton tai
                    PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
                    if (period == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("MarkRecord_Label_PeriodID");
                        throw new BusinessException("Common_Validate_NotAvailable", Params);
                    }
                    // Dot va hoc ky co khop nhau
                    if (period.Semester != Semester)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }

                Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                lstClassID.ForEach(u =>
                {
                    List<PupilOfClassBO> lstPoc = null;
                    if (Year > 0)
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive && poc.Year == Year
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    else
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    dicListPOC[u] = lstPoc.ToList();
                });
                Dictionary<int, List<ExemptedSubject>> dicListExempted = new Dictionary<int, List<ExemptedSubject>>();
                lstClassID.ForEach(u =>
                {
                    dicListExempted[u] = ExemptedSubjectBusiness.GetListExemptedSubject(u, Semester).ToList();
                });

                //bool isMarkJudge = false;
                // Lấy thông tin môn nghề
                // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                // Khi lam phan nghe thi se kiem tra lai cho phu hop
                // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                ApprenticeshipClass apprenticeshipClassSubject = null;
                // Validate mon hoc, mien giam
                foreach (var o in lstPupilSubject)
                {
                    SubjectCat objSubject = null;
                    if (apprenticeshipClassSubject != null)
                    {
                        objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                    }
                    else
                    {
                        objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                    }
                    // Mien giam
                    if (dicListExempted[o.ClassID].Any(u => u.PupilID == o.PupilID && u.SubjectID == o.SubjectID))
                    {
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                    }

                    // Trang thai hoc sinh
                    var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                    if (poc == null
                        || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                    {
                        throw new BusinessException("Import_Validate_StudyingStatus");
                    }
                    if (poc.AcademicYearID != o.AcademicYearID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    // Kiem tra co phai mon tinh diem ket hop nhan xet
                    //if (objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    //{
                    //    isMarkJudge = true;
                    //}
                }

                if (PeriodID > 0)
                {
                    strMarkTitle = strMarkTitle + ",";
                }
                foreach (MarkRecordHistory markRecord in ListMarkRecord)
                {
                    ValidationMetadata.ValidateObject(markRecord);
                    if (markRecord.Mark != -1)
                    {
                        UtilsBusiness.CheckValidateMark(Grade, SystemParamsInFile.ISCOMMENTING_TYPE_MARK, markRecord.Mark.ToString());
                    }
                    // Kiem tra xem dau diem nhap len da dung voi dau diem duoc quy dinh cua he thong
                    if (!strMarkTitle.Equals(string.Empty) && !strMarkTitle.Contains(markRecord.Title + ","))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion

                #region Insert MarkRecord
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                List<MarkRecordHistory> listMarkDb = null;
                // Kiểm tra có phải là môn nghề không?
                if (apprenticeshipClassSubject != null)
                {
                    SearchInfo["SubjectID"] = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectID;
                    SearchInfo["Semester"] = Semester;
                    listMarkDb = this.SearchMarkRecordHistory(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID) && lstClassID.Contains(u.ClassID)).ToList();
                }
                else
                {
                    //SearchInfo["ClassID"] = ClassID;
                    //SearchInfo["SubjectID"] = SubjectID;
                    SearchInfo["Semester"] = Semester;
                    IQueryable<MarkRecordHistory> listMarkForDelete = this.SearchMarkRecordHistory(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID) && lstClassID.Contains(u.ClassID) && lstSubjectID.Contains(u.SubjectID));
                    // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                    if (!isSchoolAdmin)
                    {
                        listMarkForDelete = listMarkForDelete.Where(v => !ArrLockedMark.Contains(v.Title));
                    }
                    if (PeriodID > 0)
                        listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                    listMarkDb = listMarkForDelete.ToList();
                }
                // Tim kiem danh sach them, sua, xoa
                List<MarkRecordHistory> listInsert = new List<MarkRecordHistory>();
                List<MarkRecordHistory> listUpdate = new List<MarkRecordHistory>();

                if (listMarkDb.Count == 0)
                {
                    listInsert = ListMarkRecord;
                }
                else
                {
                    foreach (MarkRecordHistory itemInsert in ListMarkRecord)
                    {
                        MarkRecordHistory itemDb = listMarkDb.Where(o => o.PupilID == itemInsert.PupilID && o.Title == itemInsert.Title && o.SubjectID == itemInsert.SubjectID && o.ClassID == itemInsert.ClassID).FirstOrDefault();
                        if (itemDb == null)
                        {
                            listInsert.Add(itemInsert);
                        }
                        else
                        {
                            if (!itemInsert.Mark.Equals(itemDb.Mark))
                            {
                                itemDb.OldMark = itemDb.Mark;
                                itemDb.Mark = itemInsert.Mark;
                                itemDb.IsSMS = false;
                                listUpdate.Add(itemDb);
                            }
                            listMarkDb.Remove(itemDb);
                        }
                    }
                }
                if (listInsert.Count > 0)
                {
                    foreach (MarkRecordHistory item in listInsert)
                    {
                        item.CreatedAcademicYear = academicYear.Year;
                        item.CreatedDate = DateTime.Now;
                        this.Insert(item);
                    }
                }
                if (listUpdate.Count > 0)
                {
                    foreach (MarkRecordHistory item in listUpdate)
                    {
                        item.ModifiedDate = DateTime.Now;
                        this.Update(item);
                    }
                }
                if (listMarkDb.Count > 0)
                {
                    this.DeleteAll(listMarkDb);
                }

                #endregion

                this.Save();

                if (isInsertAnySubject)
                {
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    //Chiendd1:17/08/2015, sua lai ham tinh TBM theo mon tang cuong
                    List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listMarkDb.Select(o => o.PupilID)).ToList();
                    IDictionary<string, object> dicclassSubject = new Dictionary<string, object>();
                    ClassID = lstClassID.FirstOrDefault();
                    dicclassSubject.Add("SchoolID", SchoolID);
                    List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, dicclassSubject).ToList();
                    ClassSubject objClassSubject;
                    for (int i = 0; i < lstSubjectID.Count; i++)
                    {
                        SubjectID = lstSubjectID[i];
                        #region Save ThreadMark
                        ThreadMarkBO info = new ThreadMarkBO();
                        info.SchoolID = SchoolID;
                        info.ClassID = ClassID;
                        info.AcademicYearID = AcademicYearID;
                        info.SubjectID = SubjectID;
                        info.Semester = Semester;
                        info.PeriodID = PeriodID;
                        info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                        info.PupilIds = pupilIDs;
                        ThreadMarkBusiness.InsertActionList(info);
                        // End
                        #endregion

                        #region Chay store de tinh diem TBM
                        objClassSubject = lstClassSubject.FirstOrDefault(c => c.SubjectID == SubjectID);
                        SummedUpMarkHis(lstPupilID, SchoolID, academicYearID, Semester, PeriodID, strMarkTitle, objClassSubject);
                        #endregion
                    }
                }
                else
                {
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listMarkDb.Select(o => o.PupilID)).ToList();
                    List<int> lstPupilIdInClass = new List<int>();
                    IDictionary<string, object> dicclassSubject = new Dictionary<string, object>();
                    dicclassSubject.Add("SchoolID", SchoolID);
                    SubjectID = lstSubjectID.FirstOrDefault();
                    List<ClassSubject> lstClassSubject = ClassSubjectBusiness.All.Where(c => c.Last2digitNumberSchool == SchoolID % 100 && c.SubjectID == SubjectID && lstClassID.Contains(c.ClassID)).ToList();
                    ClassSubject objClassSubject;

                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        ClassID = lstClassID[i];

                        #region Save ThreadMark
                        ThreadMarkBO info = new ThreadMarkBO();
                        info.SchoolID = SchoolID;
                        info.ClassID = ClassID;
                        info.AcademicYearID = AcademicYearID;
                        info.SubjectID = SubjectID;
                        info.Semester = Semester;
                        info.PeriodID = PeriodID;
                        info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                        info.PupilIds = pupilIDs;
                        ThreadMarkBusiness.InsertActionList(info);
                        // End
                        #endregion
                        #region Chay store de tinh diem TBM
                        objClassSubject = lstClassSubject.FirstOrDefault(c => c.ClassID == ClassID);
                        lstPupilIdInClass = listInsert.Where(c => c.ClassID == ClassID).Select(o => o.PupilID)
                                            .Union(listUpdate.Where(c => c.ClassID == ClassID).Select(o => o.PupilID))
                                            .Union(listMarkDb.Where(c => c.ClassID == ClassID).Select(o => o.PupilID))
                                            .ToList();
                        SummedUpMarkHis(lstPupilID, SchoolID, academicYearID, Semester, PeriodID, strMarkTitle, objClassSubject);
                        #endregion
                    }
                }

            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public bool InsertMarkRecordHistory(int UserID, List<MarkRecordHistory> ListMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, int SubjectID, int? EmployeeID, int? Year = null)
        {
            bool isDBChange = false;
            try
            {
                if (ListMarkRecord == null || ListMarkRecord.Count == 0)
                {
                    return isDBChange;
                }
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                var lstPupilSubject = ListMarkRecord.Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                //ID hoc sinh
                List<int> lstPupilID = lstSummedUpRecord.Select(u => u.PupilID).Distinct().ToList();
                var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                .Distinct();

                List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                //Lấy danh sách khóa con điểm lstLockedMarkDetail
                List<string> ArrLockedMark = null;

                //neu khong phai tai khoan quan tri truong
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                if (!isSchoolAdmin)
                {
                    string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                    // Neu tat ca con diem bi khoa thi bao loi
                    if (lstLockedMarkDetail.Contains("LHK"))
                    {
                        throw new BusinessException("MarkRecord_Validate_MarkLock");
                    }

                    // Chi lay cac con diem khong bi khoa
                    ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                    ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();

                    for (int i = 0; i < ArrLockedMark.Count; i++)
                    {
                        if (strtmp.Contains(ArrLockedMark[i]))
                        {
                            strtmp = strtmp.Replace(ArrLockedMark[i], "");
                        }
                    }
                }
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                #region validate
                //Năm học không phải là năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("Common_Validate_IsCurrentYear");

                // Kiem tra quyen giao vien bo mon va kieu mon hoc
                ClassSubject classSubject = null;
                foreach (var sc in distinctSC)
                {
                    if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, sc.ClassID, sc.SubjectID, Semester))
                        throw new BusinessException("MarkRecord_Validate_InputMarkPermission");
                    // Kiem tra mon hoc la mon tinh diem
                    classSubject = ClassSubjectBusiness.SearchByClass(sc.ClassID, new Dictionary<string, object> { { "SubjectID", sc.SubjectID }, { "SchoolID", SchoolID } }).FirstOrDefault();
                    if (classSubject == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK && classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                              ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                              : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                if (!isSchoolAdmin && !timeYear)
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }

                // Nam hoc dang xet khong truong ung voi truong
                if (academicYear.SchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra du lieu dot
                if (PeriodID.HasValue)
                {
                    // Kiem tra dot ton tai
                    PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID.Value);
                    if (period == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("MarkRecord_Label_PeriodID");
                        throw new BusinessException("Common_Validate_NotAvailable", Params);
                    }
                    // Dot va hoc ky co khop nhau
                    if (period.Semester != Semester)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }

                Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                lstClassID.ForEach(u =>
                {
                    List<PupilOfClassBO> lstPoc = null;
                    if (Year != null && Year > 0)
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive && poc.Year == Year
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    else
                    {
                        lstPoc = (from poc in PupilOfClassBusiness.AllNoTracking
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where poc.ClassID == u && pp.IsActive
                                  select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                    }
                    dicListPOC[u] = lstPoc.ToList();
                });
                Dictionary<int, List<ExemptedSubject>> dicListExempted = new Dictionary<int, List<ExemptedSubject>>();
                lstClassID.ForEach(u =>
                {
                    dicListExempted[u] = ExemptedSubjectBusiness.GetListExemptedSubject(u, Semester).ToList();
                });

                //bool isMarkJudge = false;
                // Lấy thông tin môn nghề
                // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                // Khi lam phan nghe thi se kiem tra lai cho phu hop
                // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                ApprenticeshipClass apprenticeshipClassSubject = null;
                // Validate mon hoc, mien giam
                foreach (var o in lstPupilSubject)
                {
                    SubjectCat objSubject = null;
                    if (apprenticeshipClassSubject != null)
                    {
                        objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                    }
                    else
                    {
                        objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                    }
                    var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                    if (poc == null
                        || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                    {
                        throw new BusinessException("Import_Validate_StudyingStatus");
                    }
                    if (poc.AcademicYearID != o.AcademicYearID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion

                #region Insert MarkRecord
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                List<MarkRecordHistory> listMarkDb = null;
                // Kiểm tra có phải là môn nghề không?
                if (apprenticeshipClassSubject != null)
                {
                    SearchInfo["SubjectID"] = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectID;
                    SearchInfo["Semester"] = Semester;
                    listMarkDb = this.SearchMarkRecordHistory(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID) && lstClassID.Contains(u.ClassID)).ToList();
                }
                else
                {
                    SearchInfo["ClassID"] = ClassID;
                    SearchInfo["SubjectID"] = SubjectID;
                    SearchInfo["Semester"] = Semester;
                    IQueryable<MarkRecordHistory> listMarkForDelete = this.SearchMarkRecordHistory(SearchInfo).Where(u => lstPupilID.Contains(u.PupilID));
                    // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                    if (!isSchoolAdmin)
                    {
                        listMarkForDelete = listMarkForDelete.Where(v => !ArrLockedMark.Contains(v.Title));
                    }
                    if (PeriodID.HasValue)
                    {
                        listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                    }
                    listMarkDb = listMarkForDelete.ToList();
                }

                string strPupilId = string.Join(",", lstPupilID);
                /*if (!UtilsBusiness.IsMoveHistory(academicYear.Year))
                {
                    this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, 0, PeriodID, strPupilId, strtmp);
                }
                else
                {
                    this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, 1, PeriodID, strPupilId, strtmp);
                }*/

                //Insert diem moi
                List<MarkRecordHistory> listMarkRecordTmp = new List<MarkRecordHistory>();
                MarkRecordHistory mrOfPupilObj = null;
                if (ListMarkRecord != null && ListMarkRecord.Count > 0)
                {
                    listMarkRecordTmp = ListMarkRecord.Where(p => p.Mark != -1).ToList();//Danh sach diem lay tu giao dien
                    int _pupilId = 0;
                    string _title = string.Empty;
                    decimal _mark = 0m;
                    foreach (MarkRecordHistory itemInsert in listMarkRecordTmp)
                    {
                        _pupilId = itemInsert.PupilID;
                        _title = itemInsert.Title;
                        _mark = itemInsert.Mark;
                        //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                        //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                        mrOfPupilObj = listMarkDb.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                        if (mrOfPupilObj != null)
                        {
                            if (mrOfPupilObj.Mark == _mark)
                            {
                                itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                            }
                            else
                            {
                                itemInsert.ModifiedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                            }
                            itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                        }
                        else
                        {
                            itemInsert.CreatedDate = DateTime.Now;
                            itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                        }

                        itemInsert.CreatedAcademicYear = academicYear.Year;
                    }

                    ///Xoa diem cu
                    //this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, historyId, PeriodID, strPupilId, strtmp);
                    /*this.BulkInsert(listMarkRecordTmp, MarkRecordColumnMappings(), "MarkRecordID");
                    if (listMarkRecordTmp.Count > 0)
                    {
                        isDBChange = true;
                    }
                    List<long> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID).Distinct().ToList();
                    if (lstMarkIdDelete.Count > 0)
                    {
                        MarkRecordBusiness.DeleteMarkRecord(academicYearID, SchoolID, ClassID, Semester, SubjectID, 1, lstMarkIdDelete);
                        isDBChange = true;
                    }*/

                    //Them moi va xoa diem
                    List<string> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID.ToString()).Distinct().ToList();
                    List<CustomType> lstParaDelete = new List<CustomType>();

                    CustomType objCustomerType = null;

                    objCustomerType = new CustomType { ParaName = "P_ACADEMIC_YEAR_ID", ParaType = "String", ParaValue = academicYearID.ToString(), IsListParaValue = false };
                    lstParaDelete.Add(objCustomerType);

                    objCustomerType = new CustomType { ParaName = "P_SCHOOL_ID", ParaType = "String", ParaValue = SchoolID.ToString(), IsListParaValue = false };
                    lstParaDelete.Add(objCustomerType);

                    objCustomerType = new CustomType { ParaName = "P_HISTORY", ParaType = "String", ParaValue = "1", IsListParaValue = false };
                    lstParaDelete.Add(objCustomerType);

                    objCustomerType = new CustomType { ParaName = "P_STR_MARKRECORD_ID_DEL", ParaType = "String", ParaValue = "", IsListParaValue = true, ListValue = lstMarkIdDelete };
                    lstParaDelete.Add(objCustomerType);
                    if (listMarkRecordTmp.Count > 0 || lstMarkIdDelete.Count > 0)
                    {
                        BulkInsertAndDelete(listMarkRecordTmp, MarkRecordColumnMappings(), GlobalConstants.SP_DELETE_MARK_RECORD, lstParaDelete, "MarkRecordID");
                        isDBChange = true;
                    }
                }

                #endregion

                #region Save ThreadMark
                // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                List<int> pupilIDs = listMarkRecordTmp.Select(p => p.PupilID).Distinct().ToList();
                ThreadMarkBO info = new ThreadMarkBO();
                info.SchoolID = SchoolID;
                info.ClassID = ClassID;
                info.AcademicYearID = AcademicYearID;
                info.SubjectID = SubjectID;
                info.Semester = Semester;
                info.PeriodID = PeriodID;
                info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                info.PupilIds = pupilIDs;
                //ThreadMarkBusiness.InsertActionList(info);
                ThreadMarkBusiness.BulkInsertActionList(info);
                // End
                #endregion

                #region Chay store de tinh diem TBM
                SummedUpMarkHis(lstPupilID, SchoolID, academicYearID, Semester, PeriodID, strMarkTitle, classSubject);
                #endregion
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }

            return isDBChange;
        }

        public void InsertMarkRecordHistoryBySubjectID(int UserID, List<MarkRecordHistory> LstMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, List<int> listSubjectID, int? EmployeeID, int? Year = null)
        {
            try
            {
                if (LstMarkRecord == null || LstMarkRecord.Count == 0)
                {
                    return;
                }
                int SubjectID = 0;
                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<PupilOfClassBO> lstPupilOfClass = new List<PupilOfClassBO>();
                if (Year != null && Year > 0)
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.Year == Year && poc.SchoolID == SchoolID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }
                else
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.AcademicYearID == AcademicYearID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }

                List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object> { { "lstSubjectID", listSubjectID }, { "SchoolID", SchoolID } }).ToList();
                IDictionary<string, object> dicEx = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"SemesterID",Semester},
                    {"ClassID",ClassID},
                    {"lstSubjectID",listSubjectID}
                };
                List<ExemptedSubject> lstEx = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicEx).ToList();
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);

                #region Lay du diem da ton tai trong DB
                List<MarkRecordHistory> listMarkDb = new List<MarkRecordHistory>();
                List<MarkRecordHistory> listMarkDbBySubjectId = new List<MarkRecordHistory>();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["Semester"] = Semester;
                SearchInfo["ListSubjectID"] = listSubjectID;
                listMarkDb = this.SearchMarkRecordHistory(SearchInfo).ToList();
                #endregion


                for (int l = 0; l < listSubjectID.Count; l++)
                {
                    SubjectID = listSubjectID[l];
                    listMarkDbBySubjectId = listMarkDb.Where(v => v.SubjectID == SubjectID).ToList();
                    List<MarkRecordHistory> ListMarkRecord = LstMarkRecord.Where(p => p.SubjectID == SubjectID).ToList();
                    int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                    var lstPupilSubject = ListMarkRecord.Where(p => p.SubjectID == SubjectID).Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                    List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                    //ID hoc sinh
                    List<int> lstPupilID = lstSummedUpRecord.Where(p => p.SubjectID == SubjectID).Select(u => u.PupilID).Distinct().ToList();
                    var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                    .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                    .Distinct();

                    List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                    List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                    //Lấy danh sách khóa con điểm lstLockedMarkDetail
                    List<string> ArrLockedMark = null;

                    //neu khong phai tai khoan quan tri truong

                    if (!isSchoolAdmin)
                    {
                        string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                        // Neu tat ca con diem bi khoa thi bao loi
                        if (lstLockedMarkDetail.Contains("LHK"))
                        {
                            throw new BusinessException("MarkRecord_Validate_MarkLock");
                        }

                        // Chi lay cac con diem khong bi khoa
                        ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                        ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                        for (int i = 0; i < ArrLockedMark.Count; i++)
                        {
                            if (strtmp.Contains(ArrLockedMark[i]))
                            {
                                strtmp = strtmp.Replace(ArrLockedMark[i], "");
                            }
                        }
                        // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                        listMarkDbBySubjectId = listMarkDb.Where(v => !ArrLockedMark.Contains(v.Title) && v.SubjectID == SubjectID).ToList();
                    }

                    if (PeriodID.HasValue)
                    {
                        listMarkDbBySubjectId = listMarkDb.Where(u => strMarkTitle.Contains(u.Title) && u.SubjectID == SubjectID).ToList();
                    }

                    AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                    #region validate

                    bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                                  : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                    if (!isSchoolAdmin && !timeYear)
                    {
                        throw new BusinessException("Common_IsCurrentYear_Err");
                    }

                    // Nam hoc dang xet khong truong ung voi truong
                    if (academicYear.SchoolID != SchoolID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }

                    Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                    lstClassID.ForEach(u =>
                    {
                        List<PupilOfClassBO> lstPoc = null;
                        if (Year != null && Year > 0)
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        else
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        dicListPOC[u] = lstPoc.ToList();
                    });
                    // Lấy thông tin môn nghề
                    // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                    // Khi lam phan nghe thi se kiem tra lai cho phu hop
                    // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                    ApprenticeshipClass apprenticeshipClassSubject = null;
                    // Validate mon hoc, mien giam
                    foreach (var o in lstPupilSubject)
                    {
                        SubjectCat objSubject = null;
                        if (apprenticeshipClassSubject != null)
                        {
                            objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                        }
                        else
                        {
                            objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                        }
                        //// Mien giam
                        // if (dicListExempted[o.ClassID].Any(u => u.PupilID == o.PupilID && u.SubjectID == o.SubjectID))
                        // {
                        //    throw new BusinessException("Common_Validate_IsExemptedSubject");
                        //}

                        // Trang thai hoc sinh
                        var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                        if (poc == null
                            || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                        {
                            throw new BusinessException("Import_Validate_StudyingStatus");
                        }
                        if (poc.AcademicYearID != o.AcademicYearID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                    }
                    #endregion
                    #region Insert MarkRecord
                    List<int> lstPupilExemID = lstEx.Where(p => p.SubjectID == SubjectID).Select(p => p.PupilID).Distinct().ToList();
                    List<int> lstPupiltmp = lstPupilID.Where(p => !lstPupilExemID.Contains(p)).ToList();
                    string strPupilId = string.Join(",", lstPupiltmp);

                    /*if (!UtilsBusiness.IsMoveHistory(academicYear.Year))
                    {
                        this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, 0, PeriodID, strPupilId, strtmp);
                    }
                    else
                    {
                        this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, 1, PeriodID, strPupilId, strtmp);
                    }*/

                    //Insert diem moi
                    List<MarkRecordHistory> MarkRecordTmp = new List<MarkRecordHistory>();
                    MarkRecordHistory mrOfPupilObj = null;
                    if (ListMarkRecord != null && ListMarkRecord.Count > 0)
                    {
                        MarkRecordTmp = ListMarkRecord.Where(p => lstPupiltmp.Contains(p.PupilID) && p.Mark != -1).ToList();
                        int _pupilId = 0;
                        string _title = string.Empty;
                        decimal _mark = 0m;
                        foreach (MarkRecordHistory itemInsert in MarkRecordTmp)
                        {
                            _pupilId = itemInsert.PupilID;
                            _title = itemInsert.Title;
                            _mark = itemInsert.Mark;
                            //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                            //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                            mrOfPupilObj = listMarkDbBySubjectId.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                            if (mrOfPupilObj != null)
                            {
                                if (mrOfPupilObj.Mark == _mark)
                                {
                                    itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                }
                                else
                                {
                                    itemInsert.ModifiedDate = DateTime.Now;
                                    itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                                }
                                itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                            }
                            else
                            {
                                itemInsert.CreatedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                            }

                            itemInsert.CreatedAcademicYear = academicYear.Year;
                        }

                        ///Xoa diem cu

                        /*this.BulkInsert(MarkRecordTmp, MarkRecordColumnMappings(), "MarkRecordID");

                        List<long> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID).Distinct().ToList();
                        if (lstMarkIdDelete.Count > 0)
                        {
                            MarkRecordBusiness.DeleteMarkRecord(academicYearID, SchoolID, ClassID, Semester, SubjectID, 1, lstMarkIdDelete);
                        }*/

                        //Them moi va xoa diem
                        List<string> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID.ToString()).Distinct().ToList();
                        List<CustomType> lstParaDelete = new List<CustomType>();

                        CustomType objCustomerType = null;

                        objCustomerType = new CustomType { ParaName = "P_ACADEMIC_YEAR_ID", ParaType = "String", ParaValue = academicYearID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_SCHOOL_ID", ParaType = "String", ParaValue = SchoolID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_HISTORY", ParaType = "String", ParaValue = "1", IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_STR_MARKRECORD_ID_DEL", ParaType = "String", ParaValue = "", IsListParaValue = true, ListValue = lstMarkIdDelete };
                        lstParaDelete.Add(objCustomerType);
                        if (MarkRecordTmp.Count > 0 || lstMarkIdDelete.Count > 0)
                        {
                            BulkInsertAndDelete(MarkRecordTmp, MarkRecordColumnMappings(), GlobalConstants.SP_DELETE_MARK_RECORD, lstParaDelete, "MarkRecordID");
                        }



                    }
                    #endregion
                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> listPupilIDs = lstPupiltmp;
                    //List<int> pupilIDs = listInsert.Select(o => o.PupilID).Union(listUpdate.Select(o => o.PupilID)).Union(listMarkDb.Select(o => o.PupilID)).ToList();
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    //info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = listPupilIDs;
                    //ThreadMarkBusiness.InsertActionList(info);
                    ThreadMarkBusiness.BulkInsertActionList(info);
                    // End
                    #endregion
                    #region Chay store de tinh diem TBM
                    ClassSubject classSubjectObj = listClassSubject.Where(p => p.SubjectID == SubjectID).FirstOrDefault();
                    SummedUpMarkHis(lstPupiltmp, SchoolID, academicYearID, Semester, PeriodID, strMarkTitle, classSubjectObj);
                    #endregion
                }
            }
            finally
            {
                // this.Save();
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void InsertMarkRecordHistoryByClassID(int UserID, List<MarkRecordHistory> LstMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, List<int> listClassID, int SubjectID, int? EmployeeID, int? Year = null)
        {
            try
            {
                if (LstMarkRecord == null || LstMarkRecord.Count == 0)
                {
                    return;
                }
                int ClassID = 0;
                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
                string strtmp = strMarkTitle;
                List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "SubjectID", SubjectID }, { "ListClassID", listClassID } }).ToList();
                List<PupilOfClassBO> lstPupilOfClass = new List<PupilOfClassBO>();
                if (Year != null && Year > 0)
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.Year == Year && poc.SchoolID == SchoolID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }
                else
                {
                    lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                       where pp.IsActive && poc.AcademicYearID == AcademicYearID
                                       select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                }

                IDictionary<string, object> dicEx = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"SemesterID",Semester},
                    {"lstClassID",listClassID},
                    {"SubjectID",SubjectID}
                };
                List<ExemptedSubject> lstEx = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicEx).ToList();

                #region Lay du diem da ton tai trong DB
                List<MarkRecordHistory> listMarkDb = new List<MarkRecordHistory>();
                List<MarkRecordHistory> listMarkDbByClassID = new List<MarkRecordHistory>();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["NoCheckIsActivePupil"] = "NoCheckIsActivePupil";
                SearchInfo["ListClassID"] = listClassID;
                SearchInfo["Semester"] = Semester;
                SearchInfo["SubjectID"] = SubjectID;
                listMarkDb = this.SearchMarkRecordHistory(SearchInfo).ToList();
                #endregion

                for (int l = 0; l < listClassID.Count; l++)
                {
                    ClassID = listClassID[l];
                    listMarkDbByClassID = listMarkDb.Where(p => p.ClassID == ClassID).ToList();
                    this.context.Configuration.AutoDetectChangesEnabled = false;
                    this.context.Configuration.ValidateOnSaveEnabled = false;
                    List<MarkRecordHistory> ListMarkRecord = LstMarkRecord.Where(p => p.ClassID == ClassID).ToList();
                    int academicYearID = AcademicYearID > 0 ? AcademicYearID : ListMarkRecord.Select(u => u.AcademicYearID).FirstOrDefault();
                    var lstPupilSubject = ListMarkRecord.Select(u => new { u.PupilID, u.SubjectID, u.ClassID, u.AcademicYearID }).Distinct().ToList();
                    List<int> lstClassID = lstPupilSubject.Select(u => u.ClassID).Distinct().ToList();
                    //ID hoc sinh
                    List<int> lstPupilID = lstSummedUpRecord.Where(p => p.ClassID == ClassID).Select(u => u.PupilID).Distinct().ToList();
                    var distinctSC = lstPupilSubject.Select(u => new { u.ClassID, u.SubjectID })
                                                    .Union(lstSummedUpRecord.Select(u => new { u.ClassID, u.SubjectID }))
                                                    .Distinct();

                    List<int> lstSubjectID = distinctSC.Select(u => u.SubjectID).Distinct().ToList();
                    List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(u => u.IsActive && lstSubjectID.Contains(u.SubjectCatID)).ToList();
                    //Lấy danh sách khóa con điểm lstLockedMarkDetail
                    List<string> ArrLockedMark = null;

                    //neu khong phai tai khoan quan tri truong
                    bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(UserID);
                    if (!isSchoolAdmin)
                    {
                        string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(SchoolID, academicYearID, ClassID, Semester, SubjectID);
                        // Neu tat ca con diem bi khoa thi bao loi
                        if (lstLockedMarkDetail.Contains("LHK"))
                        {
                            throw new BusinessException("MarkRecord_Validate_MarkLock");
                        }

                        // Chi lay cac con diem khong bi khoa
                        ArrLockedMark = lstLockedMarkDetail.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
                        ListMarkRecord = ListMarkRecord.Where(o => !ArrLockedMark.Contains(o.Title)).ToList();
                        for (int i = 0; i < ArrLockedMark.Count; i++)
                        {
                            if (strtmp.Contains(ArrLockedMark[i]))
                            {
                                strtmp = strtmp.Replace(ArrLockedMark[i], "");
                            }
                        }
                        // Kiem tra them quyen cua giao vien xem co duoc thao tac voi con diem bi khoa hay khong?
                        listMarkDbByClassID = listMarkDbByClassID.Where(v => !ArrLockedMark.Contains(v.Title)).ToList();
                    }

                    if (PeriodID.HasValue)
                    {
                        listMarkDbByClassID = listMarkDbByClassID.Where(u => strMarkTitle.Contains(u.Title)).ToList();
                    }

                    AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                    #region validate

                    bool timeYear = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (academicYear != null && academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate)
                                                                                  : (academicYear != null && academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate);
                    if (!isSchoolAdmin && !timeYear)
                    {
                        throw new BusinessException("Common_IsCurrentYear_Err");
                    }

                    // Nam hoc dang xet khong truong ung voi truong
                    if (academicYear.SchoolID != SchoolID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }


                    Dictionary<int, List<PupilOfClassBO>> dicListPOC = new Dictionary<int, List<PupilOfClassBO>>();
                    lstClassID.ForEach(u =>
                    {
                        List<PupilOfClassBO> lstPoc = null;
                        if (Year != null && Year > 0)
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        else
                        {
                            lstPoc = (from poc in lstPupilOfClass
                                      where poc.ClassID == u
                                      select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status }).ToList();
                        }
                        dicListPOC[u] = lstPoc.ToList();
                    });

                    // Lấy thông tin môn nghề
                    // Phan nghe chua ap dung nen bo qua viec kiem tra mon nghe
                    // Khi lam phan nghe thi se kiem tra lai cho phu hop
                    // ApprenticeshipClass apprenticeshipClassSubject = ApprenticeshipClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ApprenticeshipSubjectID", SubjectID }, { "ApprenticeshipClassID", ClassID } }).SingleOrDefault();
                    ApprenticeshipClass apprenticeshipClassSubject = null;
                    // Validate mon hoc, mien giam
                    foreach (var o in lstPupilSubject)
                    {
                        SubjectCat objSubject = null;
                        if (apprenticeshipClassSubject != null)
                        {
                            objSubject = apprenticeshipClassSubject.ApprenticeshipSubject.SubjectCat;
                        }
                        else
                        {
                            objSubject = lstSubjectCat.SingleOrDefault(u => u.SubjectCatID == o.SubjectID);
                        }

                        // Trang thai hoc sinh
                        var poc = dicListPOC[o.ClassID].Where(u => u.PupilID == o.PupilID && u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderByDescending(v => v.AssignedDate.HasValue).ThenByDescending(v => v.PupilOfClassID).FirstOrDefault();
                        if (poc == null
                            || (!objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            || (objSubject.IsApprenticeshipSubject && poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS))
                        {
                            throw new BusinessException("Import_Validate_StudyingStatus");
                        }
                        if (poc.AcademicYearID != o.AcademicYearID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        if (objSubject != null && !objSubject.IsApprenticeshipSubject && poc.ClassID != o.ClassID)
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                    }
                    #endregion

                    #region Insert MarkRecord

                    //Xoa tat ca diem
                    List<int> lstPupilExemID = lstEx.Where(p => p.ClassID == ClassID).Select(p => p.PupilID).Distinct().ToList();
                    List<int> lstPupilIDtmp = lstPupilID.Where(p => !lstPupilExemID.Contains(p)).ToList();
                    string strPupilId = string.Join(",", lstPupilIDtmp);
                    /*if (!UtilsBusiness.IsMoveHistory(academicYear.Year))
                    {
                        this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, 0, PeriodID, strPupilId, strtmp);
                    }
                    else
                    {
                        this.context.SP_DELETE_MARK_RECORD(academicYearID, SchoolID, ClassID, Semester, SubjectID, 1, PeriodID, strPupilId, strtmp);
                    }*/

                    List<MarkRecordHistory> listMarkRecordTmp = new List<MarkRecordHistory>();
                    MarkRecordHistory mrOfPupilObj = null;
                    //Insert diem moi
                    if (ListMarkRecord != null && ListMarkRecord.Count > 0)
                    {
                        listMarkRecordTmp = ListMarkRecord.Where(p => lstPupilIDtmp.Contains(p.PupilID) && p.Mark != -1).ToList();
                        int _pupilId = 0;
                        string _title = string.Empty;
                        decimal _mark = 0m;
                        foreach (MarkRecordHistory itemInsert in listMarkRecordTmp)
                        {
                            _pupilId = itemInsert.PupilID;
                            _title = itemInsert.Title;
                            _mark = itemInsert.Mark;
                            //kiểm tra điểm của học sinh đó tồn tại nhưng không bằng nhau thì lấy createDate và cập nhật modifiDate
                            //Kiem tra neu diem bang nhau thì chỉ lấy createDate
                            mrOfPupilObj = listMarkDbByClassID.Where(p => p.PupilID == _pupilId && p.Title == _title).FirstOrDefault();
                            if (mrOfPupilObj != null)
                            {
                                if (mrOfPupilObj.Mark == _mark)
                                {
                                    itemInsert.ModifiedDate = mrOfPupilObj.ModifiedDate;
                                }
                                else
                                {
                                    itemInsert.ModifiedDate = DateTime.Now;
                                    itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                                }
                                itemInsert.CreatedDate = mrOfPupilObj.CreatedDate;
                            }
                            else
                            {
                                itemInsert.CreatedDate = DateTime.Now;
                                itemInsert.LogChange = EmployeeID.HasValue ? EmployeeID : 0;//id = 1 quan tri truong
                            }

                            itemInsert.CreatedAcademicYear = academicYear.Year;
                        }

                        ///Xoa diem cu
                        /*this.BulkInsert(listMarkRecordTmp, MarkRecordColumnMappings(), "MarkRecordID");
                        List<long> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID).Distinct().ToList();
                        if (lstMarkIdDelete.Count > 0)
                        {
                            MarkRecordBusiness.DeleteMarkRecord(academicYearID, SchoolID, ClassID, Semester, SubjectID, 1, lstMarkIdDelete);
                        }*/


                        //Them moi va xoa diem
                        List<string> lstMarkIdDelete = ListMarkRecord.Where(p => p.MarkRecordID > 0).Select(m => m.MarkRecordID.ToString()).Distinct().ToList();
                        List<CustomType> lstParaDelete = new List<CustomType>();

                        CustomType objCustomerType = null;

                        objCustomerType = new CustomType { ParaName = "P_ACADEMIC_YEAR_ID", ParaType = "String", ParaValue = academicYearID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_SCHOOL_ID", ParaType = "String", ParaValue = SchoolID.ToString(), IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_HISTORY", ParaType = "String", ParaValue = "1", IsListParaValue = false };
                        lstParaDelete.Add(objCustomerType);

                        objCustomerType = new CustomType { ParaName = "P_STR_MARKRECORD_ID_DEL", ParaType = "String", ParaValue = "", IsListParaValue = true, ListValue = lstMarkIdDelete };
                        lstParaDelete.Add(objCustomerType);
                        if (listMarkRecordTmp.Count > 0 || lstMarkIdDelete.Count > 0)
                        {
                            BulkInsertAndDelete(listMarkRecordTmp, MarkRecordColumnMappings(), GlobalConstants.SP_DELETE_MARK_RECORD, lstParaDelete, "MarkRecordID");
                        }

                    }
                    #endregion

                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = lstPupilIDtmp;
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    //info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = pupilIDs;
                    //ThreadMarkBusiness.InsertActionList(info);
                    ThreadMarkBusiness.BulkInsertActionList(info);
                    // End
                    #endregion

                    #region Chay store de tinh diem TBM
                    ClassSubject classSubjectObj = listClassSubject.Where(p => p.ClassID == ClassID).FirstOrDefault();
                    SummedUpMarkHis(lstPupilIDtmp, SchoolID, academicYearID, Semester, PeriodID, strMarkTitle, classSubjectObj);
                    #endregion
                }
            }
            finally
            {
                //this.Save();
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void DeleteMarkRecordHistory(int UserID, long MarkRecordHistoryID, int? SchoolID)
        {
            //Kiem tra MarkRecordID co ton tai
            MarkRecordBusiness.CheckAvailable((int)MarkRecordHistoryID, "MarkRecord_Label_MarkRecordID", false);

            //MarkRecordID, SchoolID: not compatible(MarkRecord)
            bool MarkRecordHistoryCompatible = new MarkRecordHistoryRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "MarkRecordHistory",
                    new Dictionary<string, object>()
                {
                    {"MarkRecordID",MarkRecordHistoryID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!MarkRecordHistoryCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Lấy dữ liệu từ DB có MarkRecordID = MarkRecordID
            MarkRecordHistory markRecordHistory = MarkRecordHistoryRepository.Find(MarkRecordHistoryID);
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING     
            SubjectCat objSubjectCat = SubjectCatBusiness.Find(markRecordHistory.SubjectID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = markRecordHistory.PupilID;
            SearchInfo["ClassID"] = markRecordHistory.ClassID;
            PupilOfClass PupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, SearchInfo).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (!objSubjectCat.IsApprenticeshipSubject)
            {
                if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại                
            if (!AcademicYearBusiness.IsCurrentYear(markRecordHistory.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            if (objSubjectCat.IsApprenticeshipSubject)
            {
                base.Delete(MarkRecordHistoryID);
            }
            else if (UtilsBusiness.HasSubjectTeacherPermission(UserID, markRecordHistory.ClassID, markRecordHistory.SubjectID))
            {
                base.Delete(MarkRecordHistoryID);
            }
            #region Save ThreadMark
            List<int> pupilIDs = new List<int> { { markRecordHistory.PupilID } };
            // AnhVD 20131225 - Insert into ThreadMark for Auto Process
            ThreadMarkBO info = new ThreadMarkBO();
            info.SchoolID = markRecordHistory.SchoolID;
            info.ClassID = markRecordHistory.ClassID;
            info.AcademicYearID = markRecordHistory.AcademicYearID;
            info.SubjectID = markRecordHistory.SubjectID;
            info.Semester = markRecordHistory.Semester.Value;
            info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
            info.PupilIds = pupilIDs;
            ThreadMarkBusiness.InsertActionList(info);
            ThreadMarkBusiness.Save();
            // End
            #endregion
        }

        public void DeleteMarkRecordHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (!subjectCat.IsApprenticeshipSubject)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            bool isAdminSchool = UserAccountBusiness.IsSchoolAdmin(UserID);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : string.Empty;
            string strtmp = strMarkTitle;

            if (subjectCat.IsApprenticeshipSubject)
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["SubjectID"] = SubjectID;
                SearchInfo["Semester"] = Semester;
                if (academicYear != null)
                {
                    SearchInfo["Year"] = academicYear.Year;
                }

                string MarkLockRecord = this.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                List<MarkRecordHistory> listMarkForDelete = this.SearchMarkRecordHistory(SearchInfo).Where(u => listPupilID.Contains(u.PupilID)).ToList();
                if (!isAdminSchool)
                {
                    listMarkForDelete = listMarkForDelete.Where(u => !markLocks.Contains(u.Title)).ToList();
                    for (int i = 0; i < markLocks.Count; i++)
                    {
                        if (strtmp.Contains(markLocks[i]))
                        {
                            strtmp = strtmp.Replace(markLocks[i], "");
                        }
                    }
                }

                if (listMarkForDelete.Count > 0)
                {
                    //this.DeleteAll(listMarkForDelete);
                    List<int> lstPupilID = listMarkForDelete.Select(p => p.PupilID).Distinct().ToList();
                    int periodId = PeriodID != null ? PeriodID.Value : 0;
                    MarkRecordBusiness.SP_DeleteMarkRecord(AcademicYearID, SchoolID, ClassID, Semester, SubjectID, 1, periodId, lstPupilID, strtmp);
                }
            }
            else
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["SubjectID"] = SubjectID;
                SearchInfo["Semester"] = Semester;
                if (academicYear != null)
                {
                    SearchInfo["Year"] = academicYear.Year;
                }
                IQueryable<MarkRecordHistory> listMarkForDelete = this.SearchMarkRecordHistory(SearchInfo).Where(u => listPupilID.Contains(u.PupilID));
                if (PeriodID.HasValue)
                    listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                var ListMarkDelete = listMarkForDelete.ToList();

                string MarkLockRecord = this.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                if (!isAdminSchool)
                {
                    ListMarkDelete = ListMarkDelete.Where(u => !markLocks.Contains(u.Title)).ToList();
                    for (int i = 0; i < markLocks.Count; i++)
                    {
                        if (strtmp.Contains(markLocks[i]))
                        {
                            strtmp = strtmp.Replace(markLocks[i], "");
                        }
                    }
                }


                if (ListMarkDelete.Count > 0)
                {
                    //this.DeleteAll(ListMarkDelete);
                    List<int> lstPupilID = ListMarkDelete.Select(p => p.PupilID).Distinct().ToList();
                    int periodId = PeriodID != null ? PeriodID.Value : 0;
                    MarkRecordBusiness.SP_DeleteMarkRecord(AcademicYearID, SchoolID, ClassID, Semester, SubjectID, 1, periodId, lstPupilID, strtmp);
                }

                // Neu la cap 1 thi khong can thi insert vao bang nay
                if (classProfile.EducationLevel.Grade != SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = ListMarkDelete.Select(o => o.PupilID).ToList();
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = pupilIDs;
                    //ThreadMarkBusiness.InsertActionList(info);
                    ThreadMarkBusiness.BulkInsertActionList(info);
                    // End
                    #endregion
                }
            }
        }

        private string GetMarkTitle(int PeriodID)
        {
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(PeriodID);
            string strMarkTitle = "";
            strMarkTitle += period.StrInterviewMark + period.StrWritingMark + period.StrTwiceCoeffiecientMark;
            if (period.ContaintSemesterMark.HasValue && period.ContaintSemesterMark.Value) // Bao gồm điểm kiểm tra học kỳ
            {
                strMarkTitle += "HK";
            }
            return strMarkTitle;
        }

        public IQueryable<MarkRecordHistory> SearchMarkRecordHistory(IDictionary<string, object> dic)
        {
            int MarkRecordID = Utils.GetInt(dic, "MarkRecordID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int PupilFileID = Utils.GetInt(dic, "PupilID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            double Mark = Utils.GetDouble(dic, "Mark", -1);
            double ReTestMark = Utils.GetDouble(dic, "ReTestMark", -1);
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupil = Utils.GetString(dic, "NoCheckIsActivePupil");
            IQueryable<MarkRecordHistory> lsMarkRecordHistory = MarkRecordHistoryRepository.All;
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupil))
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => o.PupilProfile.IsActive);
            }
            // AnhVD 20131217 - Search with partition column
            if (SchoolID > 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Last2digitNumberSchool == SchoolID % 100));
            }

            // End 
            if (AcademicYearID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.AcademicYearID == AcademicYearID));
            }
            if (MarkRecordID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.MarkRecordID == MarkRecordID));
            }
            if (SubjectID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.SubjectID == SubjectID));
            }
            if (PupilFileID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.PupilID == PupilFileID));
            }
            if (SchoolID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.SchoolID == SchoolID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (MarkTypeID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (Year != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.CreatedAcademicYear == Year));
            }
            if (Semester != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Semester == Semester));
            }
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (ClassID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.ClassID == ClassID));
            }
            if (AppliedLevel != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (Mark != -1)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Mark == (decimal)Mark));
            }
            if (ReTestMark != -1)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Mark == (decimal)ReTestMark));
            }
            if (CreatedDate.HasValue)
            {
                //Nen chuyen trong DB loi thanh kieu DATE thoi de query
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => o.CreatedDate.HasValue && o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => o.ModifiedDate.HasValue && o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (Title != "")
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Title == Title));
            }
            if (MarkTitle != "")
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.MarkType.Title == MarkTitle));
            }
            if (strMarkType != null && strMarkType != "")
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.ClassProfile.EducationLevelID == EducationLevelID));
            }
            return lsMarkRecordHistory;
        }

        private string GetLockMarkTitle(int schoolid, int academicyearid, int classid, int semester, int subjectid = 0, int periodid = 0)
        {
            string strMarkType = "";
            string markTitle = "";
            if (periodid > 0)
            {
                markTitle = this.GetMarkTitle(periodid);
            }
            //Chiendd1: Bo sung where partitionId cho bang LockedMarkDertail
            int partitionId = UtilsBusiness.GetPartionId(schoolid);
            var lstLockMarkDetail = LockedMarkDetailBusiness.All.Where(o => o.ClassID == classid && o.Semester == semester && o.Last2digitNumberSchool == partitionId)
                .Where(o => subjectid == 0 || o.SubjectID == subjectid)
                 .Select(o => new { MarkTypeID = o.MarkTypeID, MarkIndex = o.MarkIndex })
                 .Distinct().OrderBy(o => o.MarkTypeID).ThenBy(o => o.MarkIndex).ToList();

            List<int> listMarkTypeID = lstLockMarkDetail.Where(o => o.MarkTypeID.HasValue)
                .Select(o => o.MarkTypeID.Value).Distinct().ToList();
            List<MarkType> listMarkType = MarkTypeBusiness.All.Where(o => listMarkTypeID.Contains(o.MarkTypeID)).ToList();
            foreach (var item in lstLockMarkDetail)
            {
                var marktype = listMarkType.Where(o => o.MarkTypeID == item.MarkTypeID).FirstOrDefault();
                string markTitleCompare = (marktype.Title != SystemParamsInFile.MARK_TYPE_HK && marktype.Title != SystemParamsInFile.MARK_TYPE_LHK) ? marktype.Title + item.MarkIndex.ToString() : marktype.Title;
                if (marktype != null && (periodid == 0 ? true : markTitle.Contains(markTitleCompare)) && !strMarkType.Contains(markTitleCompare))
                    strMarkType += "," + markTitleCompare;
            }
            return strMarkType;
        }

        /// //////////////////////////////////////// INsert Mark DK History///////////////////////
        public void InsertMarkRecordDKOfPrimaryHistory(int User, List<MarkRecordHistory> lstMarkRecordDKOfPrimaryHistory, List<SummedUpRecordHistory> LstSummedUpRecordHistory, int Semester)
        {

            if (lstMarkRecordDKOfPrimaryHistory.Count == 0 || LstSummedUpRecordHistory.Count == 0) return;
            MarkRecordHistory firstRecord = lstMarkRecordDKOfPrimaryHistory[0];
            int schoolID = firstRecord.SchoolID;
            int academicYearID = firstRecord.AcademicYearID;
            int subjectID = firstRecord.SubjectID;
            int classID = firstRecord.ClassID;

            #region Validate
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID}
                }, null);
            if (!SchoolCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object> { { "SubjectID", subjectID }, { "SchoolID", schoolID } }).SingleOrDefault();
            if (classSubject == null)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (classSubject != null && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK) && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE))
                throw new BusinessException("Common_Validate_IsCommenting ");

            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, Semester).Where(u => u.SubjectID == subjectID).ToList();
            ClassProfile classProfile = ClassProfileBusiness.Find(classID);
            List<PupilOfClassBO> listPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ClassID", classID }, { "Check", "Check" } })
                                                //join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                            select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status })
                                            .ToList();

            string[] LockTitle = MarkRecordBusiness.GetLockMarkTitlePrimary(schoolID, academicYearID, classID, subjectID, Semester).Split(',');
            //width mark read and write add title in array lockmark
            List<string> LockTitleAdd = LockTitle.ToList();
            string[] LockAddSpecial = new string[] { SystemParamsInFile.GK1, SystemParamsInFile.GK2, SystemParamsInFile.CN, SystemParamsInFile.CK1 };
            foreach (string TitleLock in LockTitle)
            {
                if (LockAddSpecial.Contains(TitleLock))
                {
                    LockTitleAdd.Add("D" + TitleLock);
                    LockTitleAdd.Add("V" + TitleLock);
                }
            }
            LockTitle = LockTitleAdd.ToArray();
            //Lấy từng phần tử trong lstMarkRecord
            foreach (MarkRecordHistory markRecordHistory in lstMarkRecordDKOfPrimaryHistory)
            {
                ValidationMetadata.ValidateObject(markRecordHistory);

                PupilOfClassBO poc = listPOC.Where(u => u.PupilID == markRecordHistory.PupilID).FirstOrDefault();
                if (poc == null || poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");

                if (poc.AcademicYearID != academicYearID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (poc.ClassID != classID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //check diem
                if (markRecordHistory.Mark != -1)
                {
                    UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, 0, markRecordHistory.Mark.ToString());

                    if (listExempted.Any(u => u.PupilID == markRecordHistory.PupilID))
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                }
                //check record has lock?
                //if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !MarkRecord.Title.Equals("DTX"))
                //{
                //    foreach (string lTitle in LockTitle)
                //    {

                //        if (lTitle == MarkRecord.Title.ToString())
                //        {
                //            throw new BusinessException("Common_Validate_LockMarkTitleError");
                //        }
                //    }
                //}
            }
            #endregion

            List<int> listPupilIDs = lstMarkRecordDKOfPrimaryHistory.Select(u => u.PupilID).Distinct().ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["ClassID"] = classID;
            dic["Semester"] = Semester;
            dic["SubjectID"] = subjectID;
            dic["SchoolID"] = schoolID;
            List<MarkRecordHistory> listMarkDb = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(dic).Where(u => listPupilIDs.Contains(u.PupilID) && u.MarkedDate == null).ToList();

            //Them moi diem dinh ky
            // Loc diem != -1 truyen vao
            lstMarkRecordDKOfPrimaryHistory = lstMarkRecordDKOfPrimaryHistory.Where(u => u.Mark != -1).ToList();
            // Tim kiem danh sach them, sua, xoa
            List<MarkRecordHistory> listInsert = new List<MarkRecordHistory>();
            List<MarkRecordHistory> listUpdate = new List<MarkRecordHistory>();

            if (listMarkDb.Count == 0)
            {
                listInsert = lstMarkRecordDKOfPrimaryHistory;
            }
            else
            {
                foreach (MarkRecordHistory itemInsert in lstMarkRecordDKOfPrimaryHistory)
                {
                    MarkRecordHistory itemDb = listMarkDb.Where(o => o.PupilID == itemInsert.PupilID && o.Title == itemInsert.Title).FirstOrDefault();
                    if (itemDb == null)
                    {
                        listInsert.Add(itemInsert);
                    }
                    else
                    {
                        if (!itemInsert.Mark.Equals(itemDb.Mark))
                        {
                            itemDb.OldMark = itemDb.Mark;
                            itemDb.Mark = itemInsert.Mark;
                            itemDb.IsSMS = false;
                            listUpdate.Add(itemDb);
                        }
                        listMarkDb.Remove(itemDb);
                    }
                }
            }
            if (listInsert.Count > 0)
            {
                foreach (MarkRecordHistory item in listInsert)
                {
                    bool isInsert = true;
                    //check record has lock?
                    if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !item.Title.Equals("DTX"))
                    {
                        foreach (string lTitle in LockTitle)
                        {

                            if (lTitle == item.Title.ToString())
                            {
                                // Neu co con diem bi khoa thi bo qua
                                isInsert = false;
                                //throw new BusinessException("Common_Validate_LockMarkTitleError");
                            }
                        }
                    }
                    if (isInsert)
                    {
                        // this.Insert(item);
                        item.CreatedDate = DateTime.Now;
                        MarkRecordHistoryBusiness.Insert(item);
                    }

                }
            }
            if (listUpdate.Count > 0)
            {
                foreach (MarkRecordHistory item in listUpdate)
                {
                    bool isUpdate = true;
                    if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !item.Title.Equals("DTX"))
                    {
                        foreach (string lTitle in LockTitle)
                        {

                            if (lTitle == item.Title.ToString())
                            {
                                // Neu co con diem bi khoa thi bo qua
                                isUpdate = false;
                                //throw new BusinessException("Common_Validate_LockMarkTitleError");
                            }
                        }
                    }
                    if (isUpdate)
                    {
                        //this.Update(item);
                        item.ModifiedDate = DateTime.Now;
                        MarkRecordHistoryBusiness.Update(item);
                    }

                }
            }
            int count = listMarkDb.Count;
            if (count > 0)
            {
                for (int i = count - 1; i >= 0; i--)
                {
                    MarkRecordHistory item = listMarkDb[i];
                    if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && !item.Title.Equals("DTX"))
                    {
                        foreach (string lTitle in LockTitle)
                        {

                            if (lTitle == item.Title.ToString())
                            {
                                // Nhung con diem bi khoa thi se khong xoa
                                listMarkDb.RemoveAt(i);
                            }
                        }
                    }
                }
                if (listMarkDb.Count > 0)
                {
                    //this.DeleteAll(listMarkDb);
                    MarkRecordHistoryBusiness.DeleteAll(listMarkDb);
                }
            }

            // Kiem tra neu nhu co con diem hoc ky cua ky dang xet bi khoa thi khong tinh phan TBM nua
            #region Kiem tra co khoa diem hoc ky cua ky dang xet hay khong
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                if (LockTitle.Contains(SystemParamsInFile.CKI))
                {
                    return;
                }
            }
            else
            {
                if (LockTitle.Contains(SystemParamsInFile.CKII))
                {
                    return;
                }
            }
            #endregion

            // Doan nay xu ly chua toi uu lam
            // Neu cac hoc sinh ma

            //Xoa diem trung binh cua hoc sinh
            IDictionary<string, object> SearchSumUpRecord = new Dictionary<string, object>();
            SearchSumUpRecord["ClassID"] = classID;
            SearchSumUpRecord["AcademicYearID"] = academicYearID;
            SearchSumUpRecord["SubjectID"] = subjectID;
            SearchSumUpRecord["SchoolID"] = schoolID;
            if (classSubject.SectionPerWeekSecondSemester != 0)
                SearchSumUpRecord["Semester"] = Semester;

            List<SummedUpRecordHistory> listSur = SummedUpRecordHistoryBusiness.SearchSummedUpHistory(SearchSumUpRecord).Where(u => listPupilIDs.Contains(u.PupilID)).ToList();
            if (listSur.Count > 0)
                SummedUpRecordHistoryBusiness.DeleteAll(listSur);

            //Neu la hoc ky II thi xoa ca diem tong ket ca nam
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || classSubject.SecondSemesterCoefficient == 0)
            {
                IDictionary<string, object> SearchSurIII = new Dictionary<string, object>();
                SearchSurIII["ClassID"] = classID;
                SearchSurIII["AcademicYearID"] = academicYearID;
                SearchSurIII["SubjectID"] = subjectID;
                SearchSurIII["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecordHistory> listSurIII = SummedUpRecordHistoryBusiness.SearchSummedUpHistory(SearchSurIII).Where(u => listPupilIDs.Contains(u.PupilID)).ToList();
                if (listSurIII.Count > 0)
                    SummedUpRecordHistoryBusiness.DeleteAll(listSurIII);
            }

            //truong hop chi hoc o hoc ky I và khong hoc hoc ky 2
            //Insert diem trung binh mon
            foreach (SummedUpRecordHistory SummedUpRecord in LstSummedUpRecordHistory)
            {
                List<MarkRecordHistory> lstMarkRecordOfPupil = lstMarkRecordDKOfPrimaryHistory.Where(o => o.PupilID == SummedUpRecord.PupilID).ToList();
                if (lstMarkRecordOfPupil.Count > 0)
                {
                    if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        MarkRecordHistory mrCK1 = lstMarkRecordOfPupil.Where(o => o.Title == SystemParamsInFile.CK1).FirstOrDefault();
                        if (mrCK1 != null)
                        {
                            SummedUpRecord.SummedUpMark = mrCK1.Mark;
                            SummedUpRecord.PeriodID = null;
                            SummedUpRecord.Comment = "";
                            SummedUpRecord.SummedUpDate = DateTime.Now;
                            SummedUpRecord.Semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                            SummedUpRecord.Last2digitNumberSchool = schoolID % 100;

                            SummedUpRecordHistoryBusiness.Insert(SummedUpRecord);

                            if (classSubject != null)
                            {
                                //chi hoc o hoc ky I
                                if (classSubject.SectionPerWeekSecondSemester == 0)
                                {
                                    //Thuc hien insert ca nam
                                    SummedUpRecordHistory surCN = new SummedUpRecordHistory();
                                    surCN.PupilID = SummedUpRecord.PupilID;
                                    surCN.ClassID = SummedUpRecord.ClassID;
                                    surCN.SchoolID = SummedUpRecord.SchoolID;
                                    surCN.AcademicYearID = SummedUpRecord.AcademicYearID;
                                    surCN.SubjectID = SummedUpRecord.SubjectID;
                                    surCN.IsCommenting = SummedUpRecord.IsCommenting;
                                    surCN.CreatedAcademicYear = SummedUpRecord.CreatedAcademicYear;
                                    surCN.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    surCN.SummedUpMark = mrCK1.Mark;
                                    surCN.PeriodID = null;
                                    surCN.Comment = "";
                                    surCN.SummedUpDate = DateTime.Now;
                                    surCN.Last2digitNumberSchool = schoolID % 100;
                                    //this.SummedUpRecordBusiness.Insert(surCN);
                                    SummedUpRecordHistoryBusiness.Insert(surCN);
                                }
                            }
                        }
                    }
                    else
                    {
                        MarkRecordHistory mrCN = lstMarkRecordOfPupil.Where(o => o.Title == SystemParamsInFile.CN).FirstOrDefault();
                        if (mrCN != null)
                        {
                            SummedUpRecord.SummedUpMark = mrCN.Mark;
                            SummedUpRecord.PeriodID = null;
                            SummedUpRecord.Comment = "";
                            SummedUpRecord.SummedUpDate = DateTime.Now;
                            SummedUpRecord.Semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                            // this.SummedUpRecordBusiness.Insert(SummedUpRecord);
                            SummedUpRecordHistoryBusiness.Insert(SummedUpRecord);

                            //Thuc hien insert ca nam
                            SummedUpRecordHistory surCN = new SummedUpRecordHistory();
                            surCN.PupilID = SummedUpRecord.PupilID;
                            surCN.ClassID = SummedUpRecord.ClassID;
                            surCN.SchoolID = SummedUpRecord.SchoolID;
                            surCN.AcademicYearID = SummedUpRecord.AcademicYearID;
                            surCN.SubjectID = SummedUpRecord.SubjectID;
                            surCN.IsCommenting = SummedUpRecord.IsCommenting;
                            surCN.Last2digitNumberSchool = schoolID % 100;
                            surCN.CreatedAcademicYear = SummedUpRecord.CreatedAcademicYear;
                            surCN.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                            surCN.SummedUpMark = mrCN.Mark;
                            surCN.PeriodID = null;
                            surCN.Comment = "";
                            surCN.SummedUpDate = DateTime.Now;
                            // this.SummedUpRecordBusiness.Insert(surCN);
                            SummedUpRecordHistoryBusiness.Insert(surCN);
                        }
                    }
                }
            }
        }


        ///////////////////////////////////Insert Mark In Mark Record History////////////////////////////////////////////////
        public void InsertMarkRecordTXOfPrimaryHistory(List<MarkRecordHistory> ListMarkRecordHistory)
        {
            if (ListMarkRecordHistory.Count == 0) return;
            MarkRecordHistory firstRecord = ListMarkRecordHistory[0];
            int schoolID = firstRecord.SchoolID;
            int academicYearID = firstRecord.AcademicYearID;
            int subjectID = firstRecord.SubjectID;
            int classID = firstRecord.ClassID;
            int semester = firstRecord.Semester.Value;
            List<int> listPupilIDs = ListMarkRecordHistory.Select(u => u.PupilID).Distinct().ToList();
            #region Validate
            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(academicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                    new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID}
                }, null);
            if (!SchoolCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");
            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object> { { "SubjectID", subjectID } }).SingleOrDefault();
            if (classSubject == null)
                throw new BusinessException("Common_Validate_NotCompatible");
            if (classSubject != null && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK) && (classSubject.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE))
                throw new BusinessException("Common_Validate_IsCommenting ");
            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semester).Where(u => u.SubjectID == subjectID).ToList();

            ClassProfile classProfile = ClassProfileBusiness.Find(classID);
            // Trang thai hoc sinh
            List<PupilOfClassBO> listPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ClassID", classID }, { "Check", "Check" } })
                                                //join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                            select new PupilOfClassBO { PupilID = poc.PupilID, ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, Status = poc.Status })
                                            .ToList();
            //check lock mark
            string[] LockTitle = MarkRecordBusiness.GetLockMarkTitlePrimary(schoolID, academicYearID, classID, subjectID, semester).Split(',');
            Dictionary<int, int> dicMonth = new Dictionary<int, int>();
            dicMonth[9] = 1;
            dicMonth[10] = 2;
            dicMonth[11] = 3;
            dicMonth[12] = 4;
            dicMonth[1] = 5;
            dicMonth[2] = 6;
            dicMonth[3] = 7;
            dicMonth[4] = 8;
            dicMonth[5] = 9;
            //Lấy từng phần tử trong ListMarkRecordHistory
            foreach (MarkRecordHistory MarkRecord in ListMarkRecordHistory)
            {
                ValidationMetadata.ValidateObject(MarkRecord);

                PupilOfClassBO poc = listPOC.Where(u => u.PupilID == MarkRecord.PupilID).FirstOrDefault();
                if (poc == null || poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");

                if (poc.AcademicYearID != academicYearID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                if (poc.ClassID != classID)
                    throw new BusinessException("Common_Validate_NotCompatible");

                //check diem
                if (MarkRecord.Mark != -1)
                {
                    UtilsBusiness.CheckValidateMark(classProfile.EducationLevel.Grade, 0, MarkRecord.Mark.ToString());

                    if (listExempted.Any(u => u.PupilID == MarkRecord.PupilID))
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                }

            }
            #endregion
            using (TransactionScope scope = new TransactionScope())
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearID;
                dic["ClassID"] = classID;
                dic["Semester"] = semester;
                dic["SubjectID"] = subjectID;
                dic["SchoolID"] = schoolID;
                List<MarkRecordHistory> listMarkDb = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(dic).Where(u => listPupilIDs.Contains(u.PupilID) && (u.Title == SystemParamsInFile.TX || u.MarkedDate.HasValue)).ToList();
                // Lay diem != -1
                ListMarkRecordHistory = ListMarkRecordHistory.Where(u => u.Mark != -1).ToList();
                // Tim kiem danh sach them, sua, xoa
                List<MarkRecordHistory> listInsert = new List<MarkRecordHistory>();
                List<MarkRecordHistory> listUpdate = new List<MarkRecordHistory>();

                if (listMarkDb.Count == 0)
                {
                    listInsert = ListMarkRecordHistory;
                }
                else
                {
                    foreach (MarkRecordHistory itemInsert in ListMarkRecordHistory)
                    {
                        MarkRecordHistory itemDb = listMarkDb.Where(o => o.PupilID == itemInsert.PupilID && o.Title == itemInsert.Title && o.MarkedDate == itemInsert.MarkedDate)
                            .FirstOrDefault();
                        if (itemDb == null)
                        {
                            listInsert.Add(itemInsert);
                        }
                        else
                        {
                            if (!itemInsert.Mark.Equals(itemDb.Mark))
                            {
                                itemDb.OldMark = itemDb.Mark;
                                itemDb.Mark = itemInsert.Mark;
                                itemDb.IsSMS = false;
                                listUpdate.Add(itemDb);
                            }

                            listMarkDb.Remove(itemDb);
                        }
                    }
                }
                if (listInsert.Count > 0)
                {
                    foreach (MarkRecordHistory item in listInsert)
                    {
                        bool isInsert = true;
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && item.Title.Equals("DTX"))
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (lTitle.Contains("T"))
                                {
                                    string lt = lTitle.Replace("T", "");
                                    int Month = dicMonth[item.MarkedDate.Value.Month];
                                    if (lt == Month.ToString())
                                    {
                                        // Neu co con diem bi khoa thi bo qua
                                        isInsert = false;
                                        //throw new BusinessException("Common_Validate_LockMarkTitleError");
                                    }
                                }
                            }
                        }
                        if (isInsert)
                        {
                            MarkRecordHistoryBusiness.Insert(item);
                        }
                    }
                }
                if (listUpdate.Count > 0)
                {
                    foreach (MarkRecordHistory item in listUpdate)
                    {
                        bool isUpdate = true;
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && item.Title.Equals("DTX"))
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (lTitle.Contains("T"))
                                {
                                    string lt = lTitle.Replace("T", "");
                                    int Month = dicMonth[item.MarkedDate.Value.Month];
                                    if (lt == Month.ToString())
                                    {
                                        // Neu co con diem bi khoa thi bo qua
                                        isUpdate = false;
                                        //throw new BusinessException("Common_Validate_LockMarkTitleError");
                                    }
                                }
                            }
                        }
                        if (isUpdate)
                        {
                            MarkRecordHistoryBusiness.Update(item);
                        }
                    }
                }
                if (listMarkDb.Count > 0)
                {
                    var ListBreakLock = new List<MarkRecordHistory>();
                    foreach (MarkRecordHistory item in listMarkDb)
                    {
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0 && item.Title.Equals("DTX"))
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (lTitle.Contains("T"))
                                {
                                    string lt = lTitle.Replace("T", "");
                                    int Month = dicMonth[item.MarkedDate.Value.Month];
                                    if (lt == Month.ToString())
                                    {
                                        ListBreakLock.Add(item);
                                    }
                                }
                            }
                        }
                    }
                    var listDBRemove = listMarkDb.FindAll(u => !ListBreakLock.Any(a => a == u));
                    //this.DeleteAll(listDBRemove);
                    MarkRecordHistoryBusiness.DeleteAll(listDBRemove);
                }
                MarkRecordHistoryBusiness.Save();
                scope.Complete();
            }
        }

        #region search (Tìm kiếm điểm history)
        public IQueryable<MarkRecordHistory> SearchMarkRecordPrimaryHistory(IDictionary<string, object> dic)
        {
            int MarkRecordID = Utils.GetInt(dic, "MarkRecordID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int PupilFileID = Utils.GetInt(dic, "PupilID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            double Mark = Utils.GetDouble(dic, "Mark", -1);
            double ReTestMark = Utils.GetDouble(dic, "ReTestMark", -1);
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupil = Utils.GetString(dic, "NoCheckIsActivePupil");
            IQueryable<MarkRecordHistory> lsMarkRecordHistory = MarkRecordHistoryBusiness.All;
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupil))
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => o.PupilProfile.IsActive);
            }
            // AnhVD 20131217 - Search with partition column
            if (SchoolID > 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Last2digitNumberSchool == SchoolID % 100));
            }

            // End 
            if (AcademicYearID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.AcademicYearID == AcademicYearID));
            }
            if (MarkRecordID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.MarkRecordID == MarkRecordID));
            }
            if (SubjectID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.SubjectID == SubjectID));
            }
            if (PupilFileID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.PupilID == PupilFileID));
            }
            if (SchoolID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.SchoolID == SchoolID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (MarkTypeID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (Year != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.CreatedAcademicYear == Year));
            }
            if (Semester != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Semester == Semester));
            }
            //quanglm sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }
            if (ClassID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.ClassID == ClassID));
            }
            if (AppliedLevel != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (Mark != -1)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Mark == (decimal)Mark));
            }
            if (ReTestMark != -1)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Mark == (decimal)ReTestMark));
            }
            if (CreatedDate.HasValue)
            {
                //Nen chuyen trong DB loi thanh kieu DATE thoi de query
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => o.CreatedDate.HasValue && o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => o.ModifiedDate.HasValue && o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (Title != "")
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.Title == Title));
            }
            if (MarkTitle != "")
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.MarkType.Title == MarkTitle));
            }
            if (strMarkType != null && strMarkType != "")
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                lsMarkRecordHistory = lsMarkRecordHistory.Where(o => (o.ClassProfile.EducationLevelID == EducationLevelID));
            }
            return lsMarkRecordHistory;
        }
        #endregion

        public void DeleteMarkRecordPrimaryHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (!subjectCat.IsApprenticeshipSubject)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            bool isAdminSchool = UserAccountBusiness.IsSchoolAdmin(UserID);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            if (subjectCat.IsApprenticeshipSubject)
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["SubjectID"] = SubjectID;
                SearchInfo["Semester"] = Semester;
                if (academicYear != null)
                {
                    SearchInfo["Year"] = academicYear.Year;
                }

                string MarkLockRecord = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                List<MarkRecordHistory> listMarkForDelete = SearchMarkRecordPrimaryHistory(SearchInfo).Where(u => listPupilID.Contains(u.PupilID)).ToList();
                if (!isAdminSchool) listMarkForDelete = listMarkForDelete.Where(u => !markLocks.Contains(u.Title)).ToList();

                if (listMarkForDelete.Count > 0)
                {
                    this.DeleteAll(listMarkForDelete);
                }
            }
            else
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = SchoolID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["SubjectID"] = SubjectID;
                SearchInfo["Semester"] = Semester;
                SearchInfo["SchoolID"] = SchoolID;
                if (academicYear != null)
                {
                    SearchInfo["Year"] = academicYear.Year;
                }
                IQueryable<MarkRecordHistory> listMarkForDelete = SearchMarkRecordPrimaryHistory(SearchInfo).Where(u => listPupilID.Contains(u.PupilID));

                string strMarkTitle = PeriodID.HasValue ? this.GetMarkTitle(PeriodID.Value) : string.Empty;
                if (PeriodID.HasValue)
                    listMarkForDelete = listMarkForDelete.Where(u => strMarkTitle.Contains(u.Title));
                var ListMarkDelete = listMarkForDelete.ToList();
                string lockPrimaryTitle = "";
                if (classProfile.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    lockPrimaryTitle = MarkRecordBusiness.GetLockMarkTitlePrimary(SchoolID, AcademicYearID, ClassID, SubjectID, Semester);
                    string[] LockTitle = lockPrimaryTitle.Split(',');
                    Dictionary<int, int> dicMonth = new Dictionary<int, int>();
                    dicMonth[9] = 1;
                    dicMonth[10] = 2;
                    dicMonth[11] = 3;
                    dicMonth[12] = 4;
                    dicMonth[1] = 5;
                    dicMonth[2] = 6;
                    dicMonth[3] = 7;
                    dicMonth[4] = 8;
                    dicMonth[5] = 9;
                    //if education is 1 then check lock mark primary
                    var ListMarkDeleteEnd = new List<MarkRecordHistory>();
                    foreach (MarkRecordHistory item in ListMarkDelete)
                    {
                        if (LockTitle.Count() > 0 && LockTitle[0].Length > 0)
                        {

                            foreach (string lTitle in LockTitle)
                            {
                                if (item.Title.Equals("DTX"))
                                {
                                    if (lTitle.Contains("T"))
                                    {
                                        string lt = lTitle.Replace("T", "");
                                        int Month = dicMonth[item.MarkedDate.Value.Month];
                                        if (lt == Month.ToString())
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                }
                                else if (item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                {

                                    if (lTitle.Equals(SystemParamsInFile.GKI))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DGK1) || item.Title.Equals(SystemParamsInFile.VGK1) || item.Title.Equals(SystemParamsInFile.GK1))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                    else if (lTitle.Equals(SystemParamsInFile.CKI))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DCK1) || item.Title.Equals(SystemParamsInFile.VCK1) || item.Title.Equals(SystemParamsInFile.CK1))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                }
                                else if (item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                                {

                                    if (lTitle.Equals(SystemParamsInFile.GKII))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DGK2) || item.Title.Equals(SystemParamsInFile.VGK2) || item.Title.Equals(SystemParamsInFile.GK2))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                    else if (lTitle.Equals(SystemParamsInFile.CKII))
                                    {
                                        if (item.Title.Equals(SystemParamsInFile.DCN) || item.Title.Equals(SystemParamsInFile.VCN) || item.Title.Equals(SystemParamsInFile.CN))
                                        {
                                            ListMarkDeleteEnd.Add(item);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (ListMarkDeleteEnd.Count > 0)
                    {
                        ListMarkDelete = ListMarkDelete.FindAll(u => !ListMarkDeleteEnd.Contains(u));
                    }
                }
                else
                {
                    string MarkLockRecord = MarkRecordBusiness.GetLockMarkTitle(SchoolID, AcademicYearID, ClassID, Semester, SubjectID);
                    List<string> markLocks = MarkLockRecord.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();
                    if (!isAdminSchool) ListMarkDelete = ListMarkDelete.Where(u => !markLocks.Contains(u.Title)).ToList();
                }

                if (ListMarkDelete.Count > 0)
                    this.DeleteAll(ListMarkDelete);

                // Neu la cap 1 thi khong can thi insert vao bang nay
                if (classProfile.EducationLevel.Grade != SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    #region Save ThreadMark
                    // AnhVD 20131225 - Insert into ThreadMark for Auto Process
                    List<int> pupilIDs = ListMarkDelete.Select(o => o.PupilID).ToList();
                    ThreadMarkBO info = new ThreadMarkBO();
                    info.SchoolID = SchoolID;
                    info.ClassID = ClassID;
                    info.AcademicYearID = AcademicYearID;
                    info.SubjectID = SubjectID;
                    info.Semester = Semester;
                    info.PeriodID = PeriodID;
                    info.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                    info.PupilIds = pupilIDs;
                    ThreadMarkBusiness.InsertActionList(info);
                    // End
                    #endregion
                }
            }
        }

        #region Import Mark Record History (Import điểm học sinh trong các năm học trước)
        /// <summary>
        ///Import điểm học sinh năm học trước
        /// <author>trangdd</author>
        /// <edit>anhvd9 - 11/09/2014</edit>
        /// <date>02/10/2012</date>
        /// </summary>      
        public void ImportMarkRecord(int UserID, List<MarkRecordHistory> lstMarkRecordHistory)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstMarkRecordHistory.Count == 0) return;
                MarkRecordHistory firstMarkRecord = lstMarkRecordHistory.First();
                int schoolID = firstMarkRecord.SchoolID;
                int academicYearID = firstMarkRecord.AcademicYearID;
                int semester = firstMarkRecord.Semester.Value;
                ClassProfile firstClass = ClassProfileBusiness.Find(firstMarkRecord.ClassID);
                int educationLevelID = firstClass.EducationLevelID;
                int grade = firstClass.EducationLevel.Grade;
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                #region validate
                bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                            new Dictionary<string, object>()
                    {
                    {"SchoolID", schoolID},
                    {"AcademicYearID", academicYearID}
                    }, null);
                if (!SchoolCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                // Kiem tra neu ton tai hoc sinh co trang thai khac dang hoc thi bao loi
                List<int> listPupilID = lstMarkRecordHistory.Select(o => o.PupilID).Distinct().ToList();
                int countPupil = (from poc in PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "AcademicYearID", academicYearID } })
                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                  where pp.IsActive && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                  && listPupilID.Contains(poc.PupilID)
                                  select poc).Count();

                if (listPupilID.Count != countPupil)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }
                // Kiem tra neu co mon hoc va lop hoc khong duoc gan tuong ung trong ClassSubject thi bao loi
                var listClassSubjectID = lstMarkRecordHistory.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
                List<int> listClassID = listClassSubjectID.Select(o => o.ClassID).Distinct().ToList();
                List<ClassSubject> listClassSubject = ClassSubjectBusiness.All.Where(o => listClassID.Contains(o.ClassID) && o.Last2digitNumberSchool == partitionId).ToList();
                foreach (var classSubjectID in listClassSubjectID)
                {
                    ClassSubject cs = listClassSubject.FirstOrDefault(o => o.ClassID == classSubjectID.ClassID && o.SubjectID == classSubjectID.SubjectID);
                    if (cs == null)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (cs != null && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK && cs.IsCommenting != GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        throw new BusinessException("Common_Validate_IsCommenting");
                    }
                }

                // Kiem tra neu co hoc sinh thuoc dang mien giam nhung van co du lieu thi bao loi      
                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.All.Where(o => o.SchoolID == schoolID
                                        && o.AcademicYearID == academicYearID && ((semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                        && o.FirstSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL)
                                        ||
                                        (semester == SMAS.Business.Common.SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        && o.SecondSemesterExemptType == SMAS.Business.Common.SystemParamsInFile.EXEMPT_TYPE_ALL))).ToList();

                foreach (MarkRecordHistory markRecord in lstMarkRecordHistory)
                {
                    // Kiem tra mien giam
                    if (lstExempted.Any(o => o.PupilID == markRecord.PupilID && o.SubjectID == markRecord.SubjectID && o.ClassID == markRecord.ClassID))
                    {
                        throw new BusinessException("Common_Validate_IsExemptedSubject");
                    }
                    ValidationMetadata.ValidateObject(markRecord);
                    if (markRecord.Mark != -1)
                    {
                        UtilsBusiness.CheckValidateMark(grade, SystemParamsInFile.ISCOMMENTING_TYPE_MARK, markRecord.Mark.ToString());
                    }
                }
                #endregion

                #region them moi ban ghi markrecord
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                // Thong tin ve cac con diem cua cap hoc
                List<MarkType> listMarkType = MarkTypeBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", grade } }).ToList();
                SemeterDeclaration semeterDeclaration = SemeterDeclarationBusiness.Search(
                new Dictionary<string, object>() {
                { "SchoolID", schoolID },
                { "AcademicYearID", academicYearID },
                { "Semester", semester }
                    }
                ).FirstOrDefault();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["AcademicYearID"] = academicYearID;
                SearchInfo["Semester"] = semester;
                SearchInfo["EducationLevelID"] = educationLevelID;
                // Neu chi import cho 1 lop thi chi can lay diem cua 1 lop la du
                if (!lstMarkRecordHistory.Any(o => o.ClassID != firstMarkRecord.ClassID))
                {
                    SearchInfo["ClassID"] = firstMarkRecord.ClassID;
                }
                List<MarkRecordHistory> lstMarkRecordHistoryUpdate = this.SearchMarkRecordHistory(SearchInfo).ToList();
                // Luu lai thong tin de cap nhat vao bang tien trinh tinh TBM tu dong
                Dictionary<string, List<int>> dicPupilThreadMark = new Dictionary<string, List<int>>();
                // Danh sach cac list diem insert de bo sung vao danh sach lay ra dem di tong ket ma khong can
                // phai truy van lai nua
                List<MarkRecordHistory> listMarkRecordInsert = new List<MarkRecordHistory>();
                foreach (MarkRecordHistory MarkRecordHistory in lstMarkRecordHistory)
                {
                    //Tìm kiếm bản ghi
                    MarkRecordHistory MarkRecordUpdate = lstMarkRecordHistoryUpdate.Where(u => u.PupilID == MarkRecordHistory.PupilID &&
                                                                                u.ClassID == MarkRecordHistory.ClassID &&
                                                                                u.SubjectID == MarkRecordHistory.SubjectID &&
                                                                                u.Title == MarkRecordHistory.Title).FirstOrDefault();
                    bool isUpdate = false;
                    if (MarkRecordUpdate != null)
                    {
                        //Nếu tìm thấy bản ghi thì cập nhật lại Mark = lstMarkRecord [i].Mark
                        if (MarkRecordUpdate.Mark != MarkRecordHistory.Mark)
                        {
                            MarkRecordUpdate.OldMark = MarkRecordUpdate.Mark;
                            MarkRecordUpdate.Mark = MarkRecordHistory.Mark;
                            MarkRecordUpdate.ModifiedDate = DateTime.Now;
                            this.Update(MarkRecordUpdate);
                            isUpdate = true;
                        }
                    }
                    else
                    {
                        //Nếu không thì insert lstMarkRecord [i] vào bảng MarkRecord
                        MarkRecordHistory.CreatedDate = DateTime.Now;
                        listMarkRecordInsert.Add(MarkRecordHistory);
                    }
                    // Neu co cap nhat CSDL thi luu lai de tinh diem TBM tu dong
                    if (isUpdate)
                    {
                        string key = MarkRecordHistory.ClassID + "_" + MarkRecordHistory.SubjectID;
                        if (dicPupilThreadMark.ContainsKey(key))
                        {
                            listPupilID = dicPupilThreadMark[key];
                            if (!listPupilID.Contains(MarkRecordHistory.PupilID))
                            {
                                listPupilID.Add(MarkRecordHistory.PupilID);
                                dicPupilThreadMark[key] = listPupilID;
                            }
                        }
                        else
                        {
                            dicPupilThreadMark[key] = new List<int>() { MarkRecordHistory.PupilID };
                        }
                    }
                }
                // Insert diem
                if (listMarkRecordInsert.Count > 0)
                {
                    BulkInsert(listMarkRecordInsert, MarkRecordColumnMappings(), "MarkRecordHistoryId");
                }

                #endregion
                this.Save();
            }
            catch (Exception ex)
            {
                string paraList = string.Format("UserID={0}, List<MarkRecordHistory> lstMarkRecordHistory", UserID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportMarkRecord", paraList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        #endregion
        public void SummedUpMarkHis(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, int? PeriodID, String strMarkTitle, ClassSubject classSubject)
        {
            if (classSubject == null)
            {
                return;
            }
            //Tinh TBM theo mon tang cuong
            String pupilIds = UtilsBusiness.JoinPupilId(lstPupilID);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            //Tinh ra so hoc sinh chua nhap du so con diem toi thieu khi duoc cau hinh
            bool isMinSemester = false;
            int MinM = 0;
            int MinP = 0;
            int MinV = 0;
            if (semesterId == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                isMinSemester = classSubject != null && ((classSubject.MMinFirstSemester.HasValue && classSubject.MMinFirstSemester.Value > 0)
                                   || classSubject.PMinFirstSemester.HasValue && classSubject.PMinFirstSemester.Value > 0
                                   || classSubject.VMinFirstSemester.HasValue && classSubject.VMinFirstSemester.Value > 0);
                MinM = classSubject != null && classSubject.MMinFirstSemester.HasValue ? classSubject.MMinFirstSemester.Value : 0;
                MinP = classSubject != null && classSubject.PMinFirstSemester.HasValue ? classSubject.PMinFirstSemester.Value : 0;
                MinV = classSubject != null && classSubject.VMinFirstSemester.HasValue ? classSubject.VMinFirstSemester.Value : 0;
            }
            else
            {
                isMinSemester = classSubject != null && ((classSubject.MMinSecondSemester.HasValue && classSubject.MMinSecondSemester.Value > 0)
                                   || classSubject.PMinSecondSemester.HasValue && classSubject.PMinSecondSemester.Value > 0
                                   || classSubject.VMinSecondSemester.HasValue && classSubject.VMinSecondSemester.Value > 0);
                MinM = classSubject != null && classSubject.MMinSecondSemester.HasValue ? classSubject.MMinSecondSemester.Value : 0;
                MinP = classSubject != null && classSubject.PMinSecondSemester.HasValue ? classSubject.PMinSecondSemester.Value : 0;
                MinV = classSubject != null && classSubject.VMinSecondSemester.HasValue ? classSubject.VMinSecondSemester.Value : 0;
            }
            List<int> lstPupilIDIsMin = new List<int>();
            if (isMinSemester)
            {
                var lstMarkRecord = (from mr in MarkRecordBusiness.All
                                     where mr.SchoolID == schoolId
                                         && mr.AcademicYearID == academicYearId
                                         && mr.Last2digitNumberSchool == partitionId
                                         && mr.ClassID == classSubject.ClassID
                                         && mr.SubjectID == classSubject.SubjectID
                                         && lstPupilID.Contains(mr.PupilID)
                                         && (mr.Title.Contains("M") || mr.Title.Contains("P") || mr.Title.Contains("V"))
                                     group mr by new
                                     {
                                         mr.PupilID,
                                         mr.Title,
                                         mr.Mark
                                     }
                                         into g
                                     select new
                                     {
                                         pupilID = g.Key.PupilID,
                                         Title = g.Key.Title,
                                         Mark = g.Key.Mark
                                     });
                List<int> lsttmp = lstMarkRecord.Select(p => p.pupilID).Distinct().ToList();
                int countM = 0;
                int countP = 0;
                int countV = 0;
                int pupilIDtmp = 0;
                for (int i = 0; i < lsttmp.Count; i++)
                {
                    pupilIDtmp = lsttmp[i];
                    countM = lstMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("M")).Count();
                    countP = lstMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("P")).Count();
                    countV = lstMarkRecord.Where(p => p.pupilID == pupilIDtmp && p.Title.Contains("V")).Count();
                    if (countM >= MinM && countP >= MinP && countV >= MinV)
                    {
                        lstPupilIDIsMin.Add(pupilIDtmp);
                    }
                }
            }
            else
            {
                lstPupilIDIsMin = lstPupilID;
            }
            String pupilIdIsMin = UtilsBusiness.JoinPupilId(lstPupilIDIsMin);

            if (classSubject.SubjectIDIncrease.HasValue && classSubject.SubjectIDIncrease.Value > 0)
            {
                this.context.PROC_CLASS_MARK_SUMMED_UP_HIS1(schoolId, academicYearId, classSubject.ClassID, classSubject.SubjectID, semesterId, PeriodID, strMarkTitle, classSubject.SubjectIDIncrease, pupilIds, pupilIdIsMin);
            }
            else
            {
                this.context.PROC_CLASS_MARK_SUMMED_UP_HISN(schoolId, academicYearId, classSubject.ClassID, classSubject.SubjectID, semesterId, PeriodID, strMarkTitle, pupilIds, pupilIdIsMin);
            }
        }

        private IDictionary<string, object> MarkRecordColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            //columnMap.Add("MarkRecordID", "MARK_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_IsByC", "M_IS_BY_C");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldMark", "OLD_MARK");
            columnMap.Add("M_MarkRecord", "M_MARK_RECORD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;
        }

        public void ImportFromOtherSystem(List<ImportFromOtherSystemBO> lstBO, int schoolId, int academicYearId, int appliedId, int classId, int subjectId, int semester, bool isComment, int? employeeId)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                // Lay so cot diem mieng, 15 phut, viet trong hoc ky
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearId;
                dic["SchoolID"] = schoolId;
                dic["Semester"] = semester;

                SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dic)
                    .Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                if (SemeterDeclaration == null) throw new BusinessException("Validate_School_NotMark");

                int soCotDiemMieng = SemeterDeclaration.InterviewMark;
                int soCotDiem15Phut = SemeterDeclaration.WritingMark;
                int soCotDiemKiemTra = SemeterDeclaration.TwiceCoeffiecientMark;

                List<MarkType> lstMarkType = MarkTypeBusiness.All.Where(o => o.AppliedLevel == appliedId).ToList();
                AcademicYear aca = AcademicYearBusiness.Find(academicYearId);

                dic = new Dictionary<string, object>();
                dic["ClassID"] = classId;
                dic["SchoolID"] = schoolId;
                dic["AcademicYearID"] = academicYearId;
                dic["SubjectID"] = subjectId;
                dic["AppliedLevel"] = appliedId;
                dic["Semester"] = semester;
                List<SummedUpRecordHistory> lstSur = SummedUpRecordHistoryBusiness.SearchBySchool(schoolId, dic)
                    .ToList();

                dic = new Dictionary<string, object>();
                dic["ClassID"] = classId;
                dic["SchoolID"] = schoolId;
                dic["AcademicYearID"] = academicYearId;
                dic["SubjectID"] = subjectId;
                dic["AppliedLevel"] = appliedId;
                dic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecordHistory> lstSurAll =
                    SummedUpRecordHistoryBusiness.SearchBySchool(schoolId, dic).ToList();


                if (subjectId > 0)
                {
                    #region mon tinh diem

                    if (!isComment)
                    {
                        dic = new Dictionary<string, object>();
                        dic["SubjectID"] = subjectId;
                        dic["AcademicYearID"] = academicYearId;
                        dic["Semester"] = semester;
                        dic["ClassID"] = classId;
                        dic["SchoolID"] = schoolId;
                        List<MarkRecordHistory> lstMr = MarkRecordHistoryBusiness.SearchMarkRecordHistory(dic)
                            .Where(o => o.PeriodID == null).ToList();
                        for (int i = 0; i < lstBO.Count; i++)
                        {
                            ImportFromOtherSystemBO bo = lstBO[i];
                            List<object> lstM = bo.lstM;
                            List<object> lstP = bo.lstP;
                            List<object> lstV = bo.lstV;

                            //Lay cac record diem cua hoc sinh nay
                            List<MarkRecordHistory> lstMrOfPupil = lstMr.Where(o => o.PupilID == bo.PupilID).ToList();

                            //Diem mieng
                            for (int j = 0; j < soCotDiemMieng; j++)
                            {
                                if (j >= lstM.Count())
                                {
                                    break;
                                }
                                int? mark = (int?)lstM[j];

                                //Kiem tra co diem hay chua
                                MarkRecordHistory mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                                //update
                                if (mr != null)
                                {
                                    if (mark != null)
                                    {
                                        mr.Mark = mark.Value;
                                        mr.ModifiedDate = DateTime.Now;
                                        mr.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordHistoryBusiness.Update(mr);
                                    }
                                    else
                                    {
                                        MarkRecordHistoryBusiness.Delete(mr.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (mark != null)
                                    {
                                        mr = new MarkRecordHistory();
                                        mr.AcademicYearID = academicYearId;
                                        mr.ClassID = classId;
                                        mr.CreatedDate = DateTime.Now;
                                        mr.Last2digitNumberSchool = schoolId % 100;
                                        mr.Mark = mark.Value;
                                        mr.MarkedDate = DateTime.Now;
                                        mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                        mr.OrderNumber = j + 1;
                                        mr.PeriodID = null;
                                        mr.PupilID = bo.PupilID;
                                        mr.SchoolID = schoolId;
                                        mr.Semester = semester;
                                        mr.SubjectID = subjectId;
                                        mr.Title = "M" + (j + 1);
                                        mr.Year = aca.Year;
                                        mr.CreatedAcademicYear = aca.Year;
                                        mr.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordHistoryBusiness.Insert(mr);
                                    }
                                }
                            }

                            //Diem 15P
                            for (int j = 0; j < soCotDiem15Phut; j++)
                            {
                                if (j >= lstP.Count())
                                {
                                    break;
                                }
                                decimal? mark = (decimal?)lstP[j];

                                //Kiem tra co diem hay chua
                                MarkRecordHistory mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                                //update
                                if (mr != null)
                                {
                                    if (mark != null)
                                    {
                                        mr.Mark = mark.Value;
                                        mr.ModifiedDate = DateTime.Now;
                                        mr.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordHistoryBusiness.Update(mr);
                                    }
                                    else
                                    {
                                        MarkRecordHistoryBusiness.Delete(mr.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (mark != null)
                                    {
                                        mr = new MarkRecordHistory();
                                        mr.AcademicYearID = academicYearId;
                                        mr.ClassID = classId;
                                        mr.CreatedDate = DateTime.Now;
                                        mr.Last2digitNumberSchool = schoolId % 100;
                                        mr.Mark = mark.Value;
                                        mr.MarkedDate = DateTime.Now;
                                        mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                        mr.OrderNumber = j + 1;
                                        mr.PeriodID = null;
                                        mr.PupilID = bo.PupilID;
                                        mr.SchoolID = schoolId;
                                        mr.Semester = semester;
                                        mr.SubjectID = subjectId;
                                        mr.Title = "P" + (j + 1);
                                        mr.Year = aca.Year;
                                        mr.CreatedAcademicYear = aca.Year;
                                        mr.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordHistoryBusiness.Insert(mr);
                                    }
                                }
                            }

                            //Diem 1T
                            for (int j = 0; j < soCotDiemKiemTra; j++)
                            {
                                if (j >= lstV.Count())
                                {
                                    break;
                                }
                                decimal? mark = (decimal?)lstV[j];

                                //Kiem tra co diem hay chua
                                MarkRecordHistory mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                                //update
                                if (mr != null)
                                {
                                    if (mark != null)
                                    {
                                        mr.Mark = mark.Value;
                                        mr.ModifiedDate = DateTime.Now;
                                        mr.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordHistoryBusiness.Update(mr);
                                    }
                                    else
                                    {
                                        MarkRecordHistoryBusiness.Delete(mr.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (mark != null)
                                    {
                                        mr = new MarkRecordHistory();
                                        mr.AcademicYearID = academicYearId;
                                        mr.ClassID = classId;
                                        mr.CreatedDate = DateTime.Now;
                                        mr.Last2digitNumberSchool = schoolId % 100;
                                        mr.Mark = mark.Value;
                                        mr.MarkedDate = DateTime.Now;
                                        mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                        mr.OrderNumber = j + 1;
                                        mr.PeriodID = null;
                                        mr.PupilID = bo.PupilID;
                                        mr.SchoolID = schoolId;
                                        mr.Semester = semester;
                                        mr.SubjectID = subjectId;
                                        mr.Title = "V" + (j + 1);
                                        mr.Year = aca.Year;
                                        mr.CreatedAcademicYear = aca.Year;
                                        mr.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordHistoryBusiness.Insert(mr);
                                    }
                                }
                            }

                            //Diem HK
                            //Kiem tra co diem hay chua
                            MarkRecordHistory mrHK = lstMrOfPupil.FirstOrDefault(o => o.Title == "HK");
                            decimal? markHK = (decimal?)bo.HK;
                            //update
                            if (mrHK != null)
                            {
                                if (markHK != null)
                                {
                                    mrHK.Mark = markHK.Value;
                                    mrHK.ModifiedDate = DateTime.Now;
                                    mrHK.LogChange = employeeId.GetValueOrDefault();
                                    MarkRecordHistoryBusiness.Update(mrHK);
                                }
                                else
                                {
                                    MarkRecordHistoryBusiness.Delete(mrHK.MarkRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markHK != null)
                                {
                                    mrHK = new MarkRecordHistory();
                                    mrHK.AcademicYearID = academicYearId;
                                    mrHK.ClassID = classId;
                                    mrHK.CreatedDate = DateTime.Now;
                                    mrHK.Last2digitNumberSchool = schoolId % 100;
                                    mrHK.Mark = markHK.Value;
                                    mrHK.MarkedDate = DateTime.Now;
                                    mrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                    mrHK.OrderNumber = 0;
                                    mrHK.PeriodID = null;
                                    mrHK.PupilID = bo.PupilID;
                                    mrHK.SchoolID = schoolId;
                                    mrHK.Semester = semester;
                                    mrHK.SubjectID = subjectId;
                                    mrHK.Title = "HK";
                                    mrHK.Year = aca.Year;
                                    mrHK.CreatedAcademicYear = aca.Year;
                                    mrHK.LogChange = employeeId.GetValueOrDefault();

                                    MarkRecordHistoryBusiness.Insert(mrHK);
                                }
                            }

                            //Diem TBM
                            //Kiem tra co diem hay chua
                            SummedUpRecordHistory sur = lstSur.FirstOrDefault(o => o.PupilID == bo.PupilID);
                            decimal? markTB = (decimal?)bo.TB;
                            //update
                            if (sur != null)
                            {
                                if (markTB != null)
                                {
                                    sur.SummedUpMark = markTB.Value;
                                    sur.SummedUpDate = DateTime.Now;
                                    SummedUpRecordHistoryBusiness.Update(sur);
                                }
                                else
                                {
                                    SummedUpRecordHistoryBusiness.Delete(sur.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTB != null)
                                {
                                    sur = new SummedUpRecordHistory();
                                    sur.AcademicYearID = academicYearId;
                                    sur.ClassID = classId;
                                    sur.IsCommenting = isComment ? 1 : 0;
                                    sur.Last2digitNumberSchool = schoolId % 100;
                                    sur.PeriodID = null;
                                    sur.PupilID = bo.PupilID;
                                    sur.SchoolID = schoolId;
                                    sur.Semester = semester;
                                    sur.SubjectID = subjectId;
                                    sur.SummedUpDate = DateTime.Now;
                                    sur.Year = aca.Year;
                                    sur.SummedUpMark = markTB.Value;
                                    sur.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordHistoryBusiness.Insert(sur);
                                }
                            }


                            //Diem TBMCN
                            //Kiem tra co diem hay chua
                            SummedUpRecordHistory surAll = lstSurAll.FirstOrDefault(o => o.PupilID == bo.PupilID);
                            decimal? markTBCN = (decimal?)bo.TBCN;
                            //update
                            if (surAll != null)
                            {
                                if (markTBCN != null)
                                {
                                    surAll.SummedUpMark = markTBCN.Value;
                                    surAll.SummedUpDate = DateTime.Now;
                                    SummedUpRecordHistoryBusiness.Update(surAll);
                                }
                                else
                                {
                                    SummedUpRecordHistoryBusiness.Delete(surAll.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTBCN != null)
                                {
                                    surAll = new SummedUpRecordHistory();
                                    surAll.AcademicYearID = academicYearId;
                                    surAll.ClassID = classId;
                                    surAll.IsCommenting = isComment ? 1 : 0;
                                    surAll.Last2digitNumberSchool = schoolId % 100;
                                    surAll.PeriodID = null;
                                    surAll.PupilID = bo.PupilID;
                                    surAll.SchoolID = schoolId;
                                    surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    surAll.SubjectID = subjectId;
                                    surAll.SummedUpDate = DateTime.Now;
                                    surAll.Year = aca.Year;
                                    surAll.SummedUpMark = markTBCN.Value;
                                    surAll.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordHistoryBusiness.Insert(surAll);
                                }
                            }
                        }
                    }

                    #endregion

                    #region mon nhan xet

                    else
                    {
                        dic = new Dictionary<string, object>();
                        dic["ClassID"] = classId;
                        dic["AcademicYearID"] = academicYearId;
                        dic["SubjectID"] = subjectId;
                        dic["Semester"] = semester;
                        dic["SchoolID"] = schoolId;

                        List<JudgeRecordHistory> lstJr = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(dic)
                            .Where(o => o.PeriodID == null).ToList();

                        for (int i = 0; i < lstBO.Count; i++)
                        {
                            ImportFromOtherSystemBO bo = lstBO[i];
                            List<object> lstM = bo.lstM;
                            List<object> lstP = bo.lstP;
                            List<object> lstV = bo.lstV;

                            //Lay cac record diem cua hoc sinh nay
                            List<JudgeRecordHistory> lstJrOfPupil = lstJr.Where(o => o.PupilID == bo.PupilID).ToList();

                            //Diem mieng
                            for (int j = 0; j < soCotDiemMieng; j++)
                            {
                                if (j >= lstM.Count())
                                {
                                    break;
                                }
                                string mark = (string)lstM[j];

                                //Kiem tra co diem hay chua
                                JudgeRecordHistory jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                                //update
                                if (jr != null)
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr.Judgement = mark;
                                        jr.ModifiedDate = DateTime.Now;
                                        jr.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordHistoryBusiness.Update(jr);
                                    }
                                    else
                                    {
                                        JudgeRecordHistoryBusiness.Delete(jr.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr = new JudgeRecordHistory();
                                        jr.AcademicYearID = academicYearId;
                                        jr.ClassID = classId;
                                        jr.CreatedDate = DateTime.Now;
                                        jr.Last2digitNumberSchool = schoolId % 100;
                                        jr.Judgement = mark;
                                        jr.MarkedDate = DateTime.Now;
                                        jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                        jr.OrderNumber = j + 1;
                                        jr.PeriodID = null;
                                        jr.PupilID = bo.PupilID;
                                        jr.SchoolID = schoolId;
                                        jr.Semester = semester;
                                        jr.SubjectID = subjectId;
                                        jr.Title = "M" + (j + 1);
                                        jr.Year = aca.Year;
                                        jr.CreatedAcademicYear = aca.Year;
                                        jr.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordHistoryBusiness.Insert(jr);
                                    }
                                }
                            }

                            //Diem 15P
                            for (int j = 0; j < soCotDiem15Phut; j++)
                            {
                                if (j >= lstP.Count())
                                {
                                    break;
                                }
                                string mark = (string)lstP[j];

                                //Kiem tra co diem hay chua
                                JudgeRecordHistory jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                                //update
                                if (jr != null)
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr.Judgement = mark;
                                        jr.ModifiedDate = DateTime.Now;
                                        jr.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordHistoryBusiness.Update(jr);
                                    }
                                    else
                                    {
                                        JudgeRecordHistoryBusiness.Delete(jr.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr = new JudgeRecordHistory();
                                        jr.AcademicYearID = academicYearId;
                                        jr.ClassID = classId;
                                        jr.CreatedDate = DateTime.Now;
                                        jr.Last2digitNumberSchool = schoolId % 100;
                                        jr.Judgement = mark;
                                        jr.MarkedDate = DateTime.Now;
                                        jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                        jr.OrderNumber = j + 1;
                                        jr.PeriodID = null;
                                        jr.PupilID = bo.PupilID;
                                        jr.SchoolID = schoolId;
                                        jr.Semester = semester;
                                        jr.SubjectID = subjectId;
                                        jr.Title = "P" + (j + 1);
                                        jr.Year = aca.Year;
                                        jr.CreatedAcademicYear = aca.Year;
                                        jr.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordHistoryBusiness.Insert(jr);
                                    }
                                }
                            }

                            //Diem 1T
                            for (int j = 0; j < soCotDiemKiemTra; j++)
                            {
                                if (j >= lstV.Count())
                                {
                                    break;
                                }
                                string mark = (string)lstV[j];

                                //Kiem tra co diem hay chua
                                JudgeRecordHistory jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                                //update
                                if (jr != null)
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr.Judgement = mark;
                                        jr.ModifiedDate = DateTime.Now;
                                        jr.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordHistoryBusiness.Update(jr);
                                    }
                                    else
                                    {
                                        JudgeRecordHistoryBusiness.Delete(jr.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (!String.IsNullOrEmpty(mark))
                                    {
                                        jr = new JudgeRecordHistory();
                                        jr.AcademicYearID = academicYearId;
                                        jr.ClassID = classId;
                                        jr.CreatedDate = DateTime.Now;
                                        jr.Last2digitNumberSchool = schoolId % 100;
                                        jr.Judgement = mark;
                                        jr.MarkedDate = DateTime.Now;
                                        jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                        jr.OrderNumber = j + 1;
                                        jr.PeriodID = null;
                                        jr.PupilID = bo.PupilID;
                                        jr.SchoolID = schoolId;
                                        jr.Semester = semester;
                                        jr.SubjectID = subjectId;
                                        jr.Title = "V" + (j + 1);
                                        jr.Year = aca.Year;
                                        jr.CreatedAcademicYear = aca.Year;
                                        jr.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordHistoryBusiness.Insert(jr);
                                    }
                                }
                            }

                            //Diem HK
                            //Kiem tra co diem hay chua
                            JudgeRecordHistory jrHK = lstJrOfPupil.FirstOrDefault(o => o.Title == "HK");
                            string markHK = (string)bo.HK;
                            //update
                            if (jrHK != null)
                            {
                                if (markHK != null)
                                {
                                    jrHK.Judgement = markHK;
                                    jrHK.ModifiedDate = DateTime.Now;
                                    jrHK.LogChange = employeeId.GetValueOrDefault();
                                    JudgeRecordHistoryBusiness.Update(jrHK);
                                }
                                else
                                {
                                    JudgeRecordHistoryBusiness.Delete(jrHK.JudgeRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markHK != null)
                                {
                                    jrHK = new JudgeRecordHistory();
                                    jrHK.AcademicYearID = academicYearId;
                                    jrHK.ClassID = classId;
                                    jrHK.CreatedDate = DateTime.Now;
                                    jrHK.Last2digitNumberSchool = schoolId % 100;
                                    jrHK.Judgement = markHK;
                                    jrHK.MarkedDate = DateTime.Now;
                                    jrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                    jrHK.OrderNumber = 0;
                                    jrHK.PeriodID = null;
                                    jrHK.PupilID = bo.PupilID;
                                    jrHK.SchoolID = schoolId;
                                    jrHK.Semester = semester;
                                    jrHK.SubjectID = subjectId;
                                    jrHK.Title = "HK";
                                    jrHK.Year = aca.Year;
                                    jrHK.CreatedAcademicYear = aca.Year;
                                    jrHK.LogChange = employeeId.GetValueOrDefault();

                                    JudgeRecordHistoryBusiness.Insert(jrHK);
                                }
                            }

                            //Diem TBM
                            //Kiem tra co diem hay chua
                            SummedUpRecordHistory sur = lstSur.FirstOrDefault(o => o.PupilID == bo.PupilID);
                            string markTB = (string)bo.TB;
                            //update
                            if (sur != null)
                            {
                                if (markTB != null)
                                {
                                    sur.JudgementResult = markTB;
                                    sur.SummedUpDate = DateTime.Now;
                                    SummedUpRecordHistoryBusiness.Update(sur);
                                }
                                else
                                {
                                    SummedUpRecordHistoryBusiness.Delete(sur.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTB != null)
                                {
                                    sur = new SummedUpRecordHistory();
                                    sur.AcademicYearID = academicYearId;
                                    sur.ClassID = classId;
                                    sur.IsCommenting = isComment ? 1 : 0;
                                    sur.Last2digitNumberSchool = schoolId % 100;
                                    sur.PeriodID = null;
                                    sur.PupilID = bo.PupilID;
                                    sur.SchoolID = schoolId;
                                    sur.Semester = semester;
                                    sur.SubjectID = subjectId;
                                    sur.SummedUpDate = DateTime.Now;
                                    sur.Year = aca.Year;
                                    sur.JudgementResult = markTB;
                                    sur.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordHistoryBusiness.Insert(sur);
                                }
                            }

                            //Diem TBMCN
                            //Kiem tra co diem hay chua
                            SummedUpRecordHistory surAll = lstSurAll.FirstOrDefault(o => o.PupilID == bo.PupilID);
                            string markTBCN = (string)bo.TBCN;
                            //update
                            if (surAll != null)
                            {
                                if (markTBCN != null)
                                {
                                    surAll.JudgementResult = markTBCN;
                                    surAll.SummedUpDate = DateTime.Now;

                                    SummedUpRecordHistoryBusiness.Update(surAll);
                                }
                                else
                                {
                                    SummedUpRecordHistoryBusiness.Delete(surAll.SummedUpRecordID);
                                }
                            }
                            //insert
                            else
                            {
                                if (markTBCN != null)
                                {
                                    surAll = new SummedUpRecordHistory();
                                    surAll.AcademicYearID = academicYearId;
                                    surAll.ClassID = classId;
                                    surAll.IsCommenting = isComment ? 1 : 0;
                                    surAll.Last2digitNumberSchool = schoolId % 100;
                                    surAll.PeriodID = null;
                                    surAll.PupilID = bo.PupilID;
                                    surAll.SchoolID = schoolId;
                                    surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    surAll.SubjectID = subjectId;
                                    surAll.SummedUpDate = DateTime.Now;
                                    surAll.Year = aca.Year;
                                    surAll.JudgementResult = markTBCN;
                                    surAll.CreatedAcademicYear = aca.Year;

                                    SummedUpRecordHistoryBusiness.Insert(surAll);
                                }
                            }
                        }

                    }

                    #endregion

                    this.Save();
                }
                else
                {
                    var lstSubject = lstBO.Select(o => new { SubjectID = o.SubjectID, IsComment = o.IsComment })
                        .Distinct().ToList();

                    dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = academicYearId;
                    dic["Semester"] = semester;
                    dic["ClassID"] = classId;
                    dic["SchoolID"] = schoolId;
                    List<MarkRecordHistory> lstMrAll = MarkRecordHistoryBusiness.SearchMarkRecordHistory(dic)
                        .Where(o => o.PeriodID == null).ToList();

                    dic = new Dictionary<string, object>();
                    dic["ClassID"] = classId;
                    dic["AcademicYearID"] = academicYearId;
                    dic["Semester"] = semester;
                    dic["SchoolID"] = schoolId;

                    List<JudgeRecordHistory> lstJrAll = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(dic)
                        .Where(o => o.PeriodID == null).ToList();

                    for (int h = 0; h < lstSubject.Count; h++)
                    {
                        var subject = lstSubject[h];
                        int eachSubjectId = subject.SubjectID;
                        isComment = subject.IsComment;

                        List<ImportFromOtherSystemBO> lstBoSubject =
                            lstBO.Where(o => o.SubjectID == eachSubjectId).ToList();

                        #region mon tinh diem

                        if (!isComment)
                        {
                            List<MarkRecordHistory> lstMr = lstMrAll
                                .Where(o => o.PeriodID == null && o.SubjectID == eachSubjectId).ToList();
                            for (int i = 0; i < lstBoSubject.Count; i++)
                            {
                                ImportFromOtherSystemBO bo = lstBoSubject[i];
                                List<object> lstM = bo.lstM;
                                List<object> lstP = bo.lstP;
                                List<object> lstV = bo.lstV;

                                //Lay cac record diem cua hoc sinh nay
                                List<MarkRecordHistory> lstMrOfPupil =
                                    lstMr.Where(o => o.PupilID == bo.PupilID).ToList();

                                //Diem mieng
                                for (int j = 0; j < soCotDiemMieng; j++)
                                {
                                    if (j >= lstM.Count())
                                    {
                                        break;
                                    }
                                    int? mark = (int?)lstM[j];

                                    //Kiem tra co diem hay chua
                                    MarkRecordHistory mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                                    //update
                                    if (mr != null)
                                    {
                                        if (mark != null)
                                        {
                                            mr.Mark = mark.Value;
                                            mr.ModifiedDate = DateTime.Now;
                                            mr.LogChange = employeeId.GetValueOrDefault();
                                            MarkRecordHistoryBusiness.Update(mr);
                                        }
                                        else
                                        {
                                            MarkRecordHistoryBusiness.Delete(mr.MarkRecordID);
                                        }
                                    }
                                    //insert
                                    else
                                    {
                                        if (mark != null)
                                        {
                                            mr = new MarkRecordHistory();
                                            mr.AcademicYearID = academicYearId;
                                            mr.ClassID = classId;
                                            mr.CreatedDate = DateTime.Now;
                                            mr.Last2digitNumberSchool = schoolId % 100;
                                            mr.Mark = mark.Value;
                                            mr.MarkedDate = DateTime.Now;
                                            mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                            mr.OrderNumber = j + 1;
                                            mr.PeriodID = null;
                                            mr.PupilID = bo.PupilID;
                                            mr.SchoolID = schoolId;
                                            mr.Semester = semester;
                                            mr.SubjectID = eachSubjectId;
                                            mr.Title = "M" + (j + 1);
                                            mr.Year = aca.Year;
                                            mr.CreatedAcademicYear = aca.Year;
                                            mr.LogChange = employeeId.GetValueOrDefault();

                                            MarkRecordHistoryBusiness.Insert(mr);
                                        }
                                    }
                                }

                                //Diem 15P
                                for (int j = 0; j < soCotDiem15Phut; j++)
                                {
                                    if (j >= lstP.Count())
                                    {
                                        break;
                                    }
                                    decimal? mark = (decimal?)lstP[j];

                                    //Kiem tra co diem hay chua
                                    MarkRecordHistory mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                                    //update
                                    if (mr != null)
                                    {
                                        if (mark != null)
                                        {
                                            mr.Mark = mark.Value;
                                            mr.ModifiedDate = DateTime.Now;
                                            mr.LogChange = employeeId.GetValueOrDefault();
                                            MarkRecordHistoryBusiness.Update(mr);
                                        }
                                        else
                                        {
                                            MarkRecordHistoryBusiness.Delete(mr.MarkRecordID);
                                        }
                                    }
                                    //insert
                                    else
                                    {
                                        if (mark != null)
                                        {
                                            mr = new MarkRecordHistory();
                                            mr.AcademicYearID = academicYearId;
                                            mr.ClassID = classId;
                                            mr.CreatedDate = DateTime.Now;
                                            mr.Last2digitNumberSchool = schoolId % 100;
                                            mr.Mark = mark.Value;
                                            mr.MarkedDate = DateTime.Now;
                                            mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                            mr.OrderNumber = j + 1;
                                            mr.PeriodID = null;
                                            mr.PupilID = bo.PupilID;
                                            mr.SchoolID = schoolId;
                                            mr.Semester = semester;
                                            mr.SubjectID = eachSubjectId;
                                            mr.Title = "P" + (j + 1);
                                            mr.Year = aca.Year;
                                            mr.CreatedAcademicYear = aca.Year;
                                            mr.LogChange = employeeId.GetValueOrDefault();

                                            MarkRecordHistoryBusiness.Insert(mr);
                                        }
                                    }
                                }

                                //Diem 1T
                                for (int j = 0; j < soCotDiemKiemTra; j++)
                                {
                                    if (j >= lstV.Count())
                                    {
                                        break;
                                    }
                                    decimal? mark = (decimal?)lstV[j];

                                    //Kiem tra co diem hay chua
                                    MarkRecordHistory mr = lstMrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                                    //update
                                    if (mr != null)
                                    {
                                        if (mark != null)
                                        {
                                            mr.Mark = mark.Value;
                                            mr.ModifiedDate = DateTime.Now;
                                            mr.LogChange = employeeId.GetValueOrDefault();
                                            MarkRecordHistoryBusiness.Update(mr);
                                        }
                                        else
                                        {
                                            MarkRecordHistoryBusiness.Delete(mr.MarkRecordID);
                                        }
                                    }
                                    //insert
                                    else
                                    {
                                        if (mark != null)
                                        {
                                            mr = new MarkRecordHistory();
                                            mr.AcademicYearID = academicYearId;
                                            mr.ClassID = classId;
                                            mr.CreatedDate = DateTime.Now;
                                            mr.Last2digitNumberSchool = schoolId % 100;
                                            mr.Mark = mark.Value;
                                            mr.MarkedDate = DateTime.Now;
                                            mr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                            mr.OrderNumber = j + 1;
                                            mr.PeriodID = null;
                                            mr.PupilID = bo.PupilID;
                                            mr.SchoolID = schoolId;
                                            mr.Semester = semester;
                                            mr.SubjectID = eachSubjectId;
                                            mr.Title = "V" + (j + 1);
                                            mr.Year = aca.Year;
                                            mr.CreatedAcademicYear = aca.Year;
                                            mr.LogChange = employeeId.GetValueOrDefault();

                                            MarkRecordHistoryBusiness.Insert(mr);
                                        }
                                    }
                                }

                                //Diem HK
                                //Kiem tra co diem hay chua
                                MarkRecordHistory mrHK = lstMrOfPupil.FirstOrDefault(o => o.Title == "HK");
                                decimal? markHK = (decimal?)bo.HK;
                                //update
                                if (mrHK != null)
                                {
                                    if (markHK != null)
                                    {
                                        mrHK.Mark = markHK.Value;
                                        mrHK.ModifiedDate = DateTime.Now;
                                        mrHK.LogChange = employeeId.GetValueOrDefault();
                                        MarkRecordHistoryBusiness.Update(mrHK);
                                    }
                                    else
                                    {
                                        MarkRecordHistoryBusiness.Delete(mrHK.MarkRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (markHK != null)
                                    {
                                        mrHK = new MarkRecordHistory();
                                        mrHK.AcademicYearID = academicYearId;
                                        mrHK.ClassID = classId;
                                        mrHK.CreatedDate = DateTime.Now;
                                        mrHK.Last2digitNumberSchool = schoolId % 100;
                                        mrHK.Mark = markHK.Value;
                                        mrHK.MarkedDate = DateTime.Now;
                                        mrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                        mrHK.OrderNumber = 0;
                                        mrHK.PeriodID = null;
                                        mrHK.PupilID = bo.PupilID;
                                        mrHK.SchoolID = schoolId;
                                        mrHK.Semester = semester;
                                        mrHK.SubjectID = eachSubjectId;
                                        mrHK.Title = "HK";
                                        mrHK.Year = aca.Year;
                                        mrHK.CreatedAcademicYear = aca.Year;
                                        mrHK.LogChange = employeeId.GetValueOrDefault();

                                        MarkRecordHistoryBusiness.Insert(mrHK);
                                    }
                                }

                                //Diem TBM
                                //Kiem tra co diem hay chua
                                SummedUpRecordHistory sur = lstSur.FirstOrDefault(o =>
                                    o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                                decimal? markTB = (decimal?)bo.TB;
                                //update
                                if (sur != null)
                                {
                                    if (markTB != null)
                                    {
                                        sur.SummedUpMark = markTB.Value;
                                        sur.SummedUpDate = DateTime.Now;
                                        SummedUpRecordHistoryBusiness.Update(sur);
                                    }
                                    else
                                    {
                                        SummedUpRecordHistoryBusiness.Delete(sur.SummedUpRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (markTB != null)
                                    {
                                        sur = new SummedUpRecordHistory();
                                        sur.AcademicYearID = academicYearId;
                                        sur.ClassID = classId;
                                        sur.IsCommenting = isComment ? 1 : 0;
                                        sur.Last2digitNumberSchool = schoolId % 100;
                                        sur.PeriodID = null;
                                        sur.PupilID = bo.PupilID;
                                        sur.SchoolID = schoolId;
                                        sur.Semester = semester;
                                        sur.SubjectID = eachSubjectId;
                                        sur.SummedUpDate = DateTime.Now;
                                        sur.Year = aca.Year;
                                        sur.SummedUpMark = markTB.Value;
                                        sur.CreatedAcademicYear = aca.Year;

                                        SummedUpRecordHistoryBusiness.Insert(sur);
                                    }
                                }

                                //Diem TBMCN
                                //Kiem tra co diem hay chua
                                SummedUpRecordHistory surAll = lstSurAll.FirstOrDefault(o =>
                                    o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                                decimal? markTBCN = (decimal?)bo.TBCN;
                                //update
                                if (surAll != null)
                                {
                                    if (markTBCN != null)
                                    {
                                        surAll.SummedUpMark = markTBCN.Value;
                                        surAll.SummedUpDate = DateTime.Now;
                                        SummedUpRecordHistoryBusiness.Update(surAll);
                                    }
                                    else
                                    {
                                        SummedUpRecordHistoryBusiness.Delete(surAll.SummedUpRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (markTBCN != null)
                                    {
                                        surAll = new SummedUpRecordHistory();
                                        surAll.AcademicYearID = academicYearId;
                                        surAll.ClassID = classId;
                                        surAll.IsCommenting = isComment ? 1 : 0;
                                        surAll.Last2digitNumberSchool = schoolId % 100;
                                        surAll.PeriodID = null;
                                        surAll.PupilID = bo.PupilID;
                                        surAll.SchoolID = schoolId;
                                        surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                        surAll.SubjectID = eachSubjectId;
                                        surAll.SummedUpDate = DateTime.Now;
                                        surAll.Year = aca.Year;
                                        surAll.SummedUpMark = markTBCN.Value;
                                        surAll.CreatedAcademicYear = aca.Year;

                                        SummedUpRecordHistoryBusiness.Insert(surAll);
                                    }
                                }
                            }
                        }

                        #endregion

                        #region mon nhan xet

                        else
                        {
                            List<JudgeRecordHistory> lstJr = lstJrAll
                                .Where(o => o.PeriodID == null && o.SubjectID == eachSubjectId).ToList();

                            for (int i = 0; i < lstBoSubject.Count; i++)
                            {
                                ImportFromOtherSystemBO bo = lstBoSubject[i];
                                List<object> lstM = bo.lstM;
                                List<object> lstP = bo.lstP;
                                List<object> lstV = bo.lstV;

                                //Lay cac record diem cua hoc sinh nay
                                List<JudgeRecordHistory> lstJrOfPupil =
                                    lstJr.Where(o => o.PupilID == bo.PupilID).ToList();

                                //Diem mieng
                                for (int j = 0; j < soCotDiemMieng; j++)
                                {
                                    if (j >= lstM.Count())
                                    {
                                        break;
                                    }
                                    string mark = (string)lstM[j];

                                    //Kiem tra co diem hay chua
                                    JudgeRecordHistory jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "M" + (j + 1));

                                    //update
                                    if (jr != null)
                                    {
                                        if (!String.IsNullOrEmpty(mark))
                                        {
                                            jr.Judgement = mark;
                                            jr.ModifiedDate = DateTime.Now;
                                            jr.LogChange = employeeId.GetValueOrDefault();
                                            JudgeRecordHistoryBusiness.Update(jr);
                                        }
                                        else
                                        {
                                            JudgeRecordHistoryBusiness.Delete(jr.JudgeRecordID);
                                        }
                                    }
                                    //insert
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(mark))
                                        {
                                            jr = new JudgeRecordHistory();
                                            jr.AcademicYearID = academicYearId;
                                            jr.ClassID = classId;
                                            jr.CreatedDate = DateTime.Now;
                                            jr.Last2digitNumberSchool = schoolId % 100;
                                            jr.Judgement = mark;
                                            jr.MarkedDate = DateTime.Now;
                                            jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "M").MarkTypeID;
                                            jr.OrderNumber = j + 1;
                                            jr.PeriodID = null;
                                            jr.PupilID = bo.PupilID;
                                            jr.SchoolID = schoolId;
                                            jr.Semester = semester;
                                            jr.SubjectID = eachSubjectId;
                                            jr.Title = "M" + (j + 1);
                                            jr.Year = aca.Year;
                                            jr.CreatedAcademicYear = aca.Year;
                                            jr.LogChange = employeeId.GetValueOrDefault();

                                            JudgeRecordHistoryBusiness.Insert(jr);
                                        }
                                    }
                                }

                                //Diem 15P
                                for (int j = 0; j < soCotDiem15Phut; j++)
                                {
                                    if (j >= lstP.Count())
                                    {
                                        break;
                                    }
                                    string mark = (string)lstP[j];

                                    //Kiem tra co diem hay chua
                                    JudgeRecordHistory jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "P" + (j + 1));

                                    //update
                                    if (jr != null)
                                    {
                                        if (!String.IsNullOrEmpty(mark))
                                        {
                                            jr.Judgement = mark;
                                            jr.ModifiedDate = DateTime.Now;
                                            jr.LogChange = employeeId.GetValueOrDefault();
                                            JudgeRecordHistoryBusiness.Update(jr);
                                        }
                                        else
                                        {
                                            JudgeRecordHistoryBusiness.Delete(jr.JudgeRecordID);
                                        }
                                    }
                                    //insert
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(mark))
                                        {
                                            jr = new JudgeRecordHistory();
                                            jr.AcademicYearID = academicYearId;
                                            jr.ClassID = classId;
                                            jr.CreatedDate = DateTime.Now;
                                            jr.Last2digitNumberSchool = schoolId % 100;
                                            jr.Judgement = mark;
                                            jr.MarkedDate = DateTime.Now;
                                            jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "P").MarkTypeID;
                                            jr.OrderNumber = j + 1;
                                            jr.PeriodID = null;
                                            jr.PupilID = bo.PupilID;
                                            jr.SchoolID = schoolId;
                                            jr.Semester = semester;
                                            jr.SubjectID = eachSubjectId;
                                            jr.Title = "P" + (j + 1);
                                            jr.Year = aca.Year;
                                            jr.CreatedAcademicYear = aca.Year;
                                            jr.LogChange = employeeId.GetValueOrDefault();

                                            JudgeRecordHistoryBusiness.Insert(jr);
                                        }
                                    }
                                }

                                //Diem 1T
                                for (int j = 0; j < soCotDiemKiemTra; j++)
                                {
                                    if (j >= lstV.Count())
                                    {
                                        break;
                                    }
                                    string mark = (string)lstV[j];

                                    //Kiem tra co diem hay chua
                                    JudgeRecordHistory jr = lstJrOfPupil.FirstOrDefault(o => o.Title == "V" + (j + 1));

                                    //update
                                    if (jr != null)
                                    {
                                        if (!String.IsNullOrEmpty(mark))
                                        {
                                            jr.Judgement = mark;
                                            jr.ModifiedDate = DateTime.Now;
                                            jr.LogChange = employeeId.GetValueOrDefault();
                                            JudgeRecordHistoryBusiness.Update(jr);
                                        }
                                        else
                                        {
                                            JudgeRecordHistoryBusiness.Delete(jr.JudgeRecordID);
                                        }
                                    }
                                    //insert
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(mark))
                                        {
                                            jr = new JudgeRecordHistory();
                                            jr.AcademicYearID = academicYearId;
                                            jr.ClassID = classId;
                                            jr.CreatedDate = DateTime.Now;
                                            jr.Last2digitNumberSchool = schoolId % 100;
                                            jr.Judgement = mark;
                                            jr.MarkedDate = DateTime.Now;
                                            jr.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "V").MarkTypeID;
                                            jr.OrderNumber = j + 1;
                                            jr.PeriodID = null;
                                            jr.PupilID = bo.PupilID;
                                            jr.SchoolID = schoolId;
                                            jr.Semester = semester;
                                            jr.SubjectID = eachSubjectId;
                                            jr.Title = "V" + (j + 1);
                                            jr.Year = aca.Year;
                                            jr.CreatedAcademicYear = aca.Year;
                                            jr.LogChange = employeeId.GetValueOrDefault();

                                            JudgeRecordHistoryBusiness.Insert(jr);
                                        }
                                    }
                                }

                                //Diem HK
                                //Kiem tra co diem hay chua
                                JudgeRecordHistory jrHK = lstJrOfPupil.FirstOrDefault(o => o.Title == "HK");
                                string markHK = (string)bo.HK;
                                //update
                                if (jrHK != null)
                                {
                                    if (markHK != null)
                                    {
                                        jrHK.Judgement = markHK;
                                        jrHK.ModifiedDate = DateTime.Now;
                                        jrHK.LogChange = employeeId.GetValueOrDefault();
                                        JudgeRecordHistoryBusiness.Update(jrHK);
                                    }
                                    else
                                    {
                                        JudgeRecordHistoryBusiness.Delete(jrHK.JudgeRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (markHK != null)
                                    {
                                        jrHK = new JudgeRecordHistory();
                                        jrHK.AcademicYearID = academicYearId;
                                        jrHK.ClassID = classId;
                                        jrHK.CreatedDate = DateTime.Now;
                                        jrHK.Last2digitNumberSchool = schoolId % 100;
                                        jrHK.Judgement = markHK;
                                        jrHK.MarkedDate = DateTime.Now;
                                        jrHK.MarkTypeID = lstMarkType.FirstOrDefault(o => o.Title == "HK").MarkTypeID;
                                        jrHK.OrderNumber = 0;
                                        jrHK.PeriodID = null;
                                        jrHK.PupilID = bo.PupilID;
                                        jrHK.SchoolID = schoolId;
                                        jrHK.Semester = semester;
                                        jrHK.SubjectID = eachSubjectId;
                                        jrHK.Title = "HK";
                                        jrHK.Year = aca.Year;
                                        jrHK.CreatedAcademicYear = aca.Year;
                                        jrHK.LogChange = employeeId.GetValueOrDefault();

                                        JudgeRecordHistoryBusiness.Insert(jrHK);
                                    }
                                }

                                //Diem TBM
                                //Kiem tra co diem hay chua
                                SummedUpRecordHistory sur = lstSur.FirstOrDefault(o =>
                                    o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                                string markTB = (string)bo.TB;
                                //update
                                if (sur != null)
                                {
                                    if (markTB != null)
                                    {
                                        sur.JudgementResult = markTB;
                                        sur.SummedUpDate = DateTime.Now;
                                        SummedUpRecordHistoryBusiness.Update(sur);
                                    }
                                    else
                                    {
                                        SummedUpRecordHistoryBusiness.Delete(sur.SummedUpRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (markTB != null)
                                    {
                                        sur = new SummedUpRecordHistory();
                                        sur.AcademicYearID = academicYearId;
                                        sur.ClassID = classId;
                                        sur.IsCommenting = isComment ? 1 : 0;
                                        sur.Last2digitNumberSchool = schoolId % 100;
                                        sur.PeriodID = null;
                                        sur.PupilID = bo.PupilID;
                                        sur.SchoolID = schoolId;
                                        sur.Semester = semester;
                                        sur.SubjectID = eachSubjectId;
                                        sur.SummedUpDate = DateTime.Now;
                                        sur.Year = aca.Year;
                                        sur.JudgementResult = markTB;
                                        sur.CreatedAcademicYear = aca.Year;

                                        SummedUpRecordHistoryBusiness.Insert(sur);
                                    }
                                }

                                //Diem TCN
                                //Kiem tra co diem hay chua
                                SummedUpRecordHistory surAll = lstSurAll.FirstOrDefault(o =>
                                    o.PupilID == bo.PupilID && o.SubjectID == eachSubjectId);
                                string markTBCN = (string)bo.TBCN;
                                //update
                                if (surAll != null)
                                {
                                    if (markTBCN != null)
                                    {
                                        surAll.JudgementResult = markTBCN;
                                        surAll.SummedUpDate = DateTime.Now;

                                        SummedUpRecordHistoryBusiness.Update(surAll);
                                    }
                                    else
                                    {
                                        SummedUpRecordHistoryBusiness.Delete(surAll.SummedUpRecordID);
                                    }
                                }
                                //insert
                                else
                                {
                                    if (markTBCN != null)
                                    {
                                        surAll = new SummedUpRecordHistory();
                                        surAll.AcademicYearID = academicYearId;
                                        surAll.ClassID = classId;
                                        surAll.IsCommenting = isComment ? 1 : 0;
                                        surAll.Last2digitNumberSchool = schoolId % 100;
                                        surAll.PeriodID = null;
                                        surAll.PupilID = bo.PupilID;
                                        surAll.SchoolID = schoolId;
                                        surAll.Semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                        surAll.SubjectID = eachSubjectId;
                                        surAll.SummedUpDate = DateTime.Now;
                                        surAll.Year = aca.Year;
                                        surAll.JudgementResult = markTBCN;
                                        surAll.CreatedAcademicYear = aca.Year;

                                        SummedUpRecordHistoryBusiness.Insert(surAll);
                                    }
                                }
                            }

                        }

                        #endregion
                    }
                    this.Save();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }

        }
    }
}
