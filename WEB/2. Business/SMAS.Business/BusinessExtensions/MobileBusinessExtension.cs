﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Business.Common.Extension;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp;

namespace SMAS.Business.Business
{
    public partial class MobileBusiness
    {
        public Conversation GetListConversation(int schoolId, int employeeId, string content, int page, int pageSize)
        {
            try
            {
                Conversation c = new Conversation();
                DateTime toDate = DateTime.Now.Date;
                DateTime fromDate = toDate.AddMonths(-6);

                List<ConversationDetail> lstConversationDetail = (from h in this.SMSHistoryBusiness.All
                    .Where(o => o.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID && o.SCHOOL_ID == schoolId
                    && o.RECEIVER_ID == employeeId && o.CREATE_DATE >= fromDate && o.CREATE_DATE <= toDate).OrderByDescending(o => o.CREATE_DATE).ToList()
                                                                  group h by h.SENDER_ID into g
                                                                  select new ConversationDetail
                                                                  {
                                                                      SenderId = g.Key,
                                                                      Content = g.FirstOrDefault().SMS_CONTENT,
                                                                      SendDate = g.FirstOrDefault().CREATE_DATE.ToString("HH'h'mm dd/MM/yyyy"),
                                                                      SenderName = g.FirstOrDefault().SENDER_NAME
                                                                  }).ToList();

                if (!string.IsNullOrEmpty(content))
                {
                    lstConversationDetail = lstConversationDetail.Where(o => o.Content.ToUpper().Contains(content.ToUpper())).ToList();
                }
                c.TotalRecord = lstConversationDetail.Count;
                c.ListConversation = lstConversationDetail.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                return c;
            }
            catch
            {
                return null;
            }
        }

        public List<ContactGroupBO> GetListContactGroup(int schoolID, int role, int employeeId)
        {
            try
            {
                List<SMS_TEACHER_CONTACT_GROUP> lst = new List<SMS_TEACHER_CONTACT_GROUP>();

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["schoolID"] = schoolID;
                //Admin
                if (role == 1)
                {
                    lst = this.ContactGroupBusiness.Search(dic).ToList();
                }
                //BGH
                else if (role == 3)
                {
                    dic["employeeID"] = employeeId;
                    dic["isPrincipal"] = true;
                    lst = this.ContactGroupBusiness.Search(dic).ToList();

                    //Lay luon cac nhom duoc phep nhan tin
                    List<SMS_TEACHER_CONTACT_GROUP> lstCgOfSchool = this.ContactGroupBusiness.All.Where(o => o.SCHOOL_ID == schoolID).ToList();
                    List<SMS_TEACHER_CONTACT_GROUP> lstCg = lstCgOfSchool.Where(o => (o.IS_ALLOW_SCHOOL_BOARD_SEND == true) || GetIDsFromString(o.ASSIGN_TEACHERS_ID).Contains(employeeId)).ToList();

                    lst = lst.Union(lstCg).ToList();
                }
                //GV
                else
                {
                    dic["employeeID"] = employeeId;
                    dic["isPrincipal"] = false;
                    var query = this.ContactGroupBusiness.Search(dic);
                    lst = query.ToList();

                    List<SMS_TEACHER_CONTACT_GROUP> lstCgOfSchool = this.ContactGroupBusiness.All.Where(o => o.SCHOOL_ID == schoolID).ToList();
                    List<SMS_TEACHER_CONTACT_GROUP> lstCg = lstCgOfSchool.Where(o => GetIDsFromString(o.ASSIGN_TEACHERS_ID).Contains(employeeId)).ToList();

                    lst = lst.Union(lstCg).OrderByDescending(a => a.IS_DEFAULT).ThenBy(a => a.CONTACT_GROUP_NAME).ToList();
                }

                List<int> lstContactGroupId = lst.Select(o => o.CONTACT_GROUP_ID).ToList();
                List<SMS_TEACHER_CONTACT> lstTeacherContact = this.TeacherContactBusiness.All.Where(o => o.IS_ACTIVE && lstContactGroupId.Contains(o.CONTACT_GROUP_ID)).ToList();
                return lst.Select(o => new ContactGroupBO
                {
                    ContactGroupId = o.CONTACT_GROUP_ID,
                    ContactGroupName = o.CONTACT_GROUP_NAME,
                    ListTeacherId = lstTeacherContact.Where(u => u.CONTACT_GROUP_ID == o.CONTACT_GROUP_ID).Select(u => u.TEACHER_ID).ToList()

                }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SendSMSSORespone SendSMS(string mobile, string content, int historyTypeID)
        {
            SendSMSSORespone obj = new SendSMSSORespone();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["Mobile"] = mobile;
            dic["Content"] = content;
            dic["TypeID"] = historyTypeID;
            try
            {
                this.MTBusiness.SendSMS(dic);
                obj.Result = true;
                obj.ValidateCode = 1;
            }
            catch (Exception ex)
            {
                obj.Result = false;
                obj.ValidateCode = 0;
            }
            return obj;
        }

        public MessageOfConversation GetListConversationDetail(int schoolId, int employeeId, int senderId, int page, int pageSize)
        {
            try
            {
                MessageOfConversation c = new MessageOfConversation();
                DateTime toDate = DateTime.Now.Date;
                DateTime fromDate = toDate.AddMonths(-6);

                List<MessageOfConversationDetail> lstConversationDetail = (from h in this.SMSHistoryBusiness.All
                    .Where(o => o.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID
                        && o.SCHOOL_ID == schoolId
                    && o.RECEIVER_ID == employeeId
                    && o.SENDER_ID == senderId
                   ).OrderByDescending(o => o.CREATE_DATE).ToList()

                    select new MessageOfConversationDetail
                    {
                        SenderId = h.SENDER_ID,
                        Content = h.SMS_CONTENT,
                        SendDate = h.CREATE_DATE.ToString("HH'h'mm dd/MM/yyyy"),
                        SenderName = h.SENDER_NAME,
                        MessageId = h.HISTORY_ID,
                    }).ToList();
                c.TotalRecord = lstConversationDetail.Count;
                c.ListMessage = lstConversationDetail.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                return c;
            }
            catch
            {
                return null;
            }
        }

        public MessageOfConversation GetMessagesOfGroup(int schoolId, int contactGroupId, int page, int pageSize)
        {
            try
            {
                MessageOfConversation c = new MessageOfConversation();
                DateTime fromDate = DateTime.Now.Date;
                DateTime toDate = fromDate.AddMonths(6);

                List<MessageOfConversationDetail> lstConversationDetail = (from h in this.SMSHistoryBusiness.All
                    .Where(o => o.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID
                        && o.SCHOOL_ID == schoolId
                    && o.CONTACT_GROUP_ID == contactGroupId
                   ).OrderByDescending(o => o.CREATE_DATE).ToList()

                        select new
                        {
                            SenderId = h.SENDER_ID,
                            Content = h.SMS_CONTENT,
                            SendDate = h.CREATE_DATE.ToString("HH'h'mm dd/MM/yyyy"),
                            SenderName = h.SENDER_NAME,


                        }).Distinct().Select(o => new MessageOfConversationDetail
                        {
                            SenderId = o.SenderId,
                            Content = o.Content,
                            SendDate = o.SendDate,
                            SenderName = o.SenderName,


                        }).ToList();
                c.TotalRecord = lstConversationDetail.Count;
                c.ListMessage = lstConversationDetail.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                return c;
            }
            catch
            {
                return null;
            }
        }

        public Result SendSMSToGroup(int schoolId, int academicYearId, int contactGroupId, int role, int teacherId, string content, int grade, string schoolName)
        {
            Result r = new Result();

            //viethd4: kiem tra co nam trong danh sach gui tin cho nhom
            bool isAllow = false;
            SMS_TEACHER_CONTACT_GROUP cg = this.ContactGroupBusiness.Find(contactGroupId);

            //Neu la nhom toan truong
            if (cg.IS_DEFAULT == true) isAllow = true;

            //admin hoac BGH
            if (role == 1 || (role == 3 && cg.IS_ALLOW_SCHOOL_BOARD_SEND == true))
            {
                isAllow = true;
            }



            //Neu la thanh vien cua nhom
            if (cg.IS_ALLOW_MEMBER_SEND == true)
            {
                //Lay danh sach thanh vien cua nhom
                SMS_TEACHER_CONTACT tc = this.TeacherContactBusiness.All.Where(o => o.IS_ACTIVE == true && o.CONTACT_GROUP_ID == contactGroupId && o.TEACHER_ID == teacherId).FirstOrDefault();
                if (tc != null)
                {
                    isAllow = true;
                }
            }

            if (GetIDsFromString(cg.ASSIGN_TEACHERS_ID).Contains(teacherId)) isAllow = true;

            if (!isAllow)
            {
                r.ErrorCode = "error";
                r.Message = "Tài khoản không có quyền gửi tin cho nhóm này";

                return r;
            }

            //Gửi tin nhan den giao vien
            var counter = 0;

            #region Short content
            string shortContent = content.Trim();
            #endregion

            //Gui tin nhan den GV
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["contactGroupID"] = contactGroupId;
            //Nếu là admin trường thì TeacherID = 0
            dic["senderID"] = teacherId;

            //
            bool isSendSignMessage = false;
            //if (cfg != null) isSendSignMessage = cfg.IsAllowSendUnicodeMessage && model.AllowSignMessage;
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["schoolID"] = schoolId;
            dictionary["isLock"] = false;
            if (cg.IS_DEFAULT == true)
            {
                dictionary["isGetDefault"] = true;
            }
            else
            {
                dictionary["contactGroupID"] = contactGroupId;
            }
            dic["lstReceiverID"] = new List<int>();

            dic["schoolID"] = schoolId;
            dic["isSignMsg"] = isSendSignMessage;
            dic["content"] = content;
            dic["shortContent"] = shortContent;
            dic["academicYearID"] = academicYearId;
            dic["type"] = GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER;
            dic["typeHistory"] = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TEACHER_ID;
            dic["isPrincipal"] = role == 3;
            dic["isAdmin"] = role == 1;

            //Lay chuong trinh khuyen mai
            SchoolProfile sp = this.SchoolProfileBusiness.Find(schoolId);
            PromotionProgramBO objPromotion = this.PromotionBusiness.GetPromotionProgramByProvinceId(sp.ProvinceID.GetValueOrDefault(), schoolId);

            dic["Promotion"] = objPromotion;

            var aca = AcademicYearBusiness.Find(academicYearId);
            AcademicYearBO academicYeaBO = null;
            if (aca != null)
            {
                academicYeaBO = new AcademicYearBO()
                {
                    AcademicYearID = aca.AcademicYearID,
                    FirstSemesterStartDate = aca.FirstSemesterStartDate,
                    FirstSemesterEndDate = aca.FirstSemesterEndDate,
                    SecondSemesterStartDate = aca.SecondSemesterStartDate,
                    SecondSemesterEndDate = aca.SecondSemesterEndDate,
                    Year = aca.Year,
                    SchoolID = aca.SchoolID
                };
            }
            else
            {
                academicYeaBO = new AcademicYearBO();
            }

            int semester = 0;
            DateTime dateTimeNow = DateTime.Now;
            AcademicYearBO academicYearBO = academicYeaBO;
            if (academicYearBO != null)
            {
                if (academicYearBO.FirstSemesterStartDate.Value.Date <= dateTimeNow && academicYearBO.SecondSemesterStartDate.Value.Date.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(59) >= dateTimeNow)
                {
                    semester = 1;
                }
                // Anhvd9 - Cho phep nhan tin toi 31.8 neu nam hoc cu da ket thuc
                if (academicYearBO.SecondSemesterStartDate.Value.Date <= dateTimeNow && new DateTime(academicYearBO.SecondSemesterEndDate.Value.Year, GlobalConstantsEdu.MONTH_END_ALLOW_SEND_SMS, GlobalConstantsEdu.DAYOFMONTH_END_ALLOW_SEND_SMS, 23, 59, 59) >= dateTimeNow)
                {
                    semester = 2;
                }
            }

            ResultBO result = this.SMSHistoryBusiness.SendSMSExternal(dic, academicYearId, semester, grade, schoolName);
            if (result.isError)
            {
                r.ErrorCode = "error";
                r.Message = result.ErrorMsg;

                return r;
            }

            counter = result.NumSendSuccess;
            string desLog = string.Format("Gửi tin nhắn thành công cho {0} GV. ", counter);

            //Nếu giao dịch gửi tin gv có thay đổi tài khoản chính mới ghi log
            if (result.TransAmount > 0)
            {
                desLog += ("Ngày giao dịch: " + DateTime.Now + ". Loại GD: Giao dịch thanh toán. Số lượng " + result.TotalSMSSentInMainBalance + " SMS. Số tiền: " + result.TransAmount + "VNĐ. Dịch vụ: Tin nhắn GV");
            }
            //WriteLogInfo(desLog, null, string.Empty, string.Empty, null, string.Empty);
            //return Json(new { Message = string.Format(Res.Get("Send_SMS_Group_SMS_Success_Msg"), counter), Type = "success", Balance = result.Balance, NumOfPromotionInternalSMS = result.NumOfPromotionInternalSMS });

            r.Message = string.Format("Gửi tin nhắn thành công cho {0} GV. ", counter);

            return r;
        }

        public List<SendSMSToParentBO> GetMessageList(int schoolId, int academicYearId, int year, int classId, int educationLevel, int appliedLevel, int semester, int type, int option, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearId);

                SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);

                SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolId);
                bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX");
                string prefix = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX, false);
                List<int> lstPupilIDHasSMS = new List<int>();

                List<PupilProfileBO> lstPupil = this.PupilProfileBusiness.GetListPupilformatString(schoolId, academicYearId, educationLevel, classId, appliedLevel);
                List<SendSMSToParentBO> lstMessage = new List<SendSMSToParentBO>();
                List<PupilProfileBO> lstPupilContract = SMSParentContractBusiness.getSMSParentContract(schoolId, year, semester, lstPupil).ToList();
                //trao doi
                if (type == 1)
                {
                    lstMessage = this.Communicate(schoolId, lstPupilContract, prefix);
                }
                //Diem
                else if (type == 2)
                {
                    lstMessage = this.GetMarkByType(schoolId, academicYearId, semester, lstPupilContract, classId, educationLevel, prefix, fromDate.Value, toDate.Value);
                }
                //Diem dot
                else if (type == 3)
                {
                    lstMessage = this.GetMarkRecordTime(schoolId, academicYearId, year, lstPupilContract, classId, educationLevel, option, prefix);
                }
                //Chuyen can
                else if (type == 4)
                {
                    lstMessage = this.GetTraningInfoExpend(schoolId, academicYearId, lstPupilContract, classId, educationLevel, prefix, fromDate.Value, toDate.Value);
                }
                //Nhan xet thang VNEN
                else if (type == 5)
                {
                    lstMessage = this.GetCommentVNENSMSType(schoolId, academicYearId, lstPupilContract, classId, option, semester);
                }
                //KQGK
                else if (type == 6)
                {
                    lstMessage = this.GetTT22Result(schoolId, academicYearId, lstPupilContract, classId, option, 1);
                }
                //KQCK
                else if (type == 7)
                {
                    lstMessage = this.GetTT22Result(schoolId, academicYearId, lstPupilContract, classId, option, 2);
                }
                //Diem thi hoc ky
                else if (type == 8)
                {
                    lstMessage = this.GetExamMarkSemester(schoolId, academicYearId, year, lstPupilContract, classId, educationLevel, option, prefix);
                }
                //Ket qua hoc ky
                else if (type == 9)
                {
                    lstMessage = this.GetSemesterResult(schoolId, academicYearId, year, lstPupilContract, classId, educationLevel, option, prefix);
                }
                //lich thi
                else if (type == 10)
                {
                    lstMessage = this.GetInfoExamSchedule(schoolId, academicYearId, semester, lstPupilContract, classId, educationLevel, option, prefix);
                }
                //Ket qua ky thi
                else if (type == 11)
                {
                    lstMessage = this.GetExamResult(schoolId, academicYearId, semester, lstPupilContract, classId, educationLevel, option, prefix);
                }
                //TKB
                else if (type == 12)
                {
                    lstMessage = this.GetCalendarOfPupil(schoolId, academicYearId, year, semester, lstPupilContract, classId, educationLevel, prefix);
                }

                return lstMessage;
            }
            catch
            {
                return null;
            }
        }

        public MSendSMSResult SendSMSToParent(MSendSmsRequest data)
        {
            string TypeCode = string.Empty;

            switch (data.TypeID)
            {
                case 1:
                    TypeCode = "TNTD";
                    break;
                case 2:
                    break;
                case 3:
                    TypeCode = "TNDD";
                    break;
                case 4:
                    TypeCode = "TNRLMR";
                    break;
                case 5:
                    TypeCode = "NXTVNEN";
                    break;
                case 6:
                    TypeCode = "TT22KQGK";
                    break;
                case 7:
                    TypeCode = "TT22KQCK";
                    break;
                case 8:
                    TypeCode = "TT22KQCK";
                    break;
                case 9:
                    TypeCode = "TNHKI";
                    break;
                case 10:
                    TypeCode = "TNTBLT";
                    break;
                case 11:
                    TypeCode = "TNTBKQT";
                    break;
                case 12:
                    TypeCode = "TNTKB";
                    break;
            }

            SMS_TYPE SMSType;
            SMSType = this.SMSTypeBusiness.All.FirstOrDefault(o => o.TYPE_CODE.Trim() == TypeCode);

            if (SMSType == null) return new MSendSMSResult(false, 0, "Bản tin chưa được kích hoạt, vui lòng liên hệ quản trị trường.");

            TypeCode = SMSType.TYPE_CODE.Trim();
            data.TypeID = SMSType.SMS_TYPE_ID;


            try
            {
                ResultBO result = new ResultBO();
                bool res;
                data.ShortContents = new List<string>();

                if (String.Compare(TypeCode, GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TYPECODE) == 0)
                {
                    return CreateSMSTraoDoi(data, out res);
                }
                else
                {
                    //check prefix va tao shortContent doi voi cac loai ban tin # trao doi
                    // Prefix có dạng [Truong] [Tên_trường] [Nội_dung]
                    SchoolProfile sp = SchoolProfileBusiness.Find(data.SchoolID);

                    SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(data.SchoolID);
                    bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX");
                    string prefixTemplate = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX, false);
                    string content = string.Empty;
                    string shortContent = string.Empty;

                    List<string> lstContent = new List<string>();
                    List<string> lstShortContent = new List<string>();

                    for (int i = 0, size = data.Contents.Count; i < size; i++)
                    {
                        content = data.Contents[i];
                        if (!content.StripVNSign().StartsWith(prefixTemplate.StripVNSign()))
                        {
                            //Dieu chinh lai template noi dung tin nhan theo mẫu [Tên_trường] [Noi dung SMS]
                            data.Contents[i] = String.Format("{0}{1}", prefixTemplate, content);
                            content = data.Contents[i];
                        }
                        shortContent = content.Remove(0, prefixTemplate.Length);
                        data.ShortContents.Add(shortContent);
                    }
                    // Apply các loại bản tin # trao đổi
                    result = SendData(data.SchoolID, data.AcademicYearID, data.EmployeeID, data.Grade, data.SchoolName, data.Role == 3, data.Role == 1, data.PupilIDs, data.ClassID, string.Empty, data.TypeID, data.Contents, data.ShortContents, 0, false);

                }

                if (result.isError)
                {
                    return new MSendSMSResult(false, 0, result.ErrorMsg);
                }
                return new MSendSMSResult(true, result.NumSendSuccess, string.Format("Gửi thành công đến {0} phụ huynh học sinh", result.NumSendSuccess));
            }
            catch
            {
                return new MSendSMSResult(false, 0, "Có lỗi trong quá trình gửi tin");
            }
        }

        private List<SendSMSToParentBO> Communicate(int schoolId, List<PupilProfileBO> lstPupil, string prefix)
        {
            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            SendSMSToParentBO model = null;
            int STT = 1;
            PupilProfileBO objPupilProfileBO = null;

            for (int i = 0, iSize = lstPupil.Count; i < iSize; i++)
            {
                objPupilProfileBO = lstPupil[i];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.PupilName = objPupilProfileBO.FullName;
                model.NoNewMessage = true;
                model.isCheck = false;
                model.IsSent = true;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = string.Empty;
                model.Content = string.Empty;
                //model = countSMSCommunicate(model, IsSignMsg);
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.EducationLevelID = objPupilProfileBO.EducationLevelID;

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetMarkByType(int schoolID, int academicYearID, int semester, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, string prefix, DateTime fromDate, DateTime toDate)
        {
            DateTime dateStart = fromDate;
            DateTime dateEnd = toDate;
            // Format tin nhan
            string SMSFormat = string.Empty;
            string SMSContent = string.Empty;

            if (DateTime.Compare(dateStart, dateEnd) == 0)
            {
                SMSFormat = String.Format("{0}thông báo điểm ngày {1} của em ", prefix, dateStart.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMM));
            }
            else
            {
                SMSFormat = String.Format("{0}thông báo điểm từ {1}-{2} của em ", prefix, dateStart.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMM), dateEnd.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMM));
            }

            List<PupilMarkBO> lstMarkRecordByClass = PupilProfileBusiness.GetMarkRecordByClass(schoolID, classID, educationLevelID, academicYearID, dateStart, dateEnd, false, semester);

            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            SendSMSToParentBO model = null;
            int STT = 1;
            int pupilProfileID = 0;

            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);

            PupilProfileBO objPupilProfileBO = null;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                pupilProfileID = objPupilProfileBO.PupilProfileID;
                model.PupilFileID = pupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                if (lstMarkRecordByClass != null) SMSContent = lstMarkRecordByClass.Where(p => p.PupilProfileID == pupilProfileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false, GlobalConstantsEdu.COMMON_COLON_SPACE);


                lstPupilRet.Add(model);
            }

            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetMarkRecordTime(int schoolID, int academicYearID, int year, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, int periodID, string prefix)
        {
            string semesterName = String.Empty;

            String SMSFormat = String.Empty;
            List<MarkConfigByTimeBO> lstTimeMarkList = this.GetTimeMarkBySemester(academicYearID, 0);
            MarkConfigByTimeBO timeMark = lstTimeMarkList.Where(t => t.MarkConfigByTimeID == periodID).FirstOrDefault();
            if (timeMark == null)
                if (lstTimeMarkList.Count > 0) timeMark = lstTimeMarkList.FirstOrDefault();

            if (timeMark != null)
            {
                // Format tin nhan
                if (timeMark.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) semesterName = "HKI";
                else semesterName = "HKII";
                SMSFormat = String.Format("{0}thông báo kết quả {1}-{2} của em ", prefix, timeMark.TimeName, semesterName);
            }

            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            PupilProfileBO objPupilProfileBO = null;
            SendSMSToParentBO model = null;
            List<int> lstClassID = lstPupil.Select(o => o.CurrentClassID).ToList();

            List<PupilMarkBO> lstMarkRecordTimeList;

            lstMarkRecordTimeList = this.PupilProfileBusiness.GetListMarkOfPeriod(schoolID, classID, academicYearID, periodID);


            lstMarkRecordTimeList = lstMarkRecordTimeList.Distinct().ToList();

            int STT = 1;
            string SMSContent = string.Empty;
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                if (lstMarkRecordTimeList != null) SMSContent = lstMarkRecordTimeList.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetTraningInfoExpend(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, string prefix, DateTime fromDate, DateTime toDate)
        {
            DateTime dateStart = fromDate;
            DateTime dateEnd = toDate;
            string SMSFormat = string.Empty;

            if (dateStart.Date == dateEnd.Date)
                SMSFormat = String.Format("{0}thông báo chuyên cần ngày {1} của em ", prefix, dateStart.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMM));
            else SMSFormat = String.Format("{0}thông báo chuyên cần từ ngày {1}-{2} của em ", prefix, dateStart.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMM), dateEnd.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMM));

            List<PupilMarkBO> lstTraningInfo = lstTraningInfo = this.PupilProfileBusiness.GetListTraningInfoByTime(schoolID, classID, academicYearID, dateStart.Date, dateEnd.Date);

            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            SendSMSToParentBO model = null;
            int STT = 1;

            PupilProfileBO objPupilProfileBO = null;
            string SMSContent = string.Empty;
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.IsSent = true;
                if (lstTraningInfo != null) SMSContent = lstTraningInfo.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false, "\n");

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetCommentVNENSMSType(int schoolID, int academicYearId, List<PupilProfileBO> lstPupil, int classID, int monthID, int semester)
        {
            List<long> listPupilID = lstPupil.Select(p => (long)p.PupilProfileID).ToList();

            List<int> lstClassID = lstPupil.Select(o => o.CurrentClassID).Distinct().ToList();

            List<EvaluationCommentsBO> listResult;

            listResult = this.PupilProfileBusiness.GetCommentsVNEN(listPupilID, schoolID, academicYearId, classID, semester, monthID);


            listResult = listResult.Distinct().ToList();
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolID);
            bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX");
            string prefixName = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX, false);

            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            SendSMSToParentBO model = null;
            int STT = 1;
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;
            string SMSFormat = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.isCheck = false;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = prefixName;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                if (listResult != null) SMSContent = listResult.Where(p => p.PupilID == model.PupilFileID).Select(p => p.EvaluationMonth).FirstOrDefault();

                SMSFormat = string.Format("{0}thông báo nhận xét tháng {1} của em ", prefixName, monthID);


                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetTT22Result(int schoolId, int academicYearId, List<PupilProfileBO> lstPupil, int ClassID, int Semester, int type)
        {

            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolId);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
            bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX");
            string prefixName = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX, false);

            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;

            SendSMSToParentBO model = null;
            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            List<long> listPupilID = lstPupil.Select(p => (long)p.PupilProfileID).ToList();
            List<RatedCommentPupilBO> lstRatedCommentPuipil = this.PupilProfileBusiness.GetListRatedCommentPupil(listPupilID, schoolId, academicYearId, ClassID, type, Semester);
            int index = 1;
            string SMSFormat = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                if (type == 1)
                {
                    SMSFormat = string.Format("{0}thông báo kết quả giữa học kỳ {1} của em ", prefixName, Semester);
                }
                else if (type == 2)
                {
                    SMSFormat = string.Format("{0}thông báo kết quả cuối {1} của em ", prefixName, (Semester == 2 ? "năm học" : "học kỳ 1"));
                }

                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = index++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.isCheck = false;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = prefixName;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                if (lstRatedCommentPuipil != null) SMSContent = lstRatedCommentPuipil.Where(p => p.PupilID == model.PupilFileID).Select(p => p.SMSContent).FirstOrDefault();

                model = countSMSBreakLine(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }

            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetExamMarkSemester(int schoolID, int academicYearID, int year, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, int semester, string prefix)
        {
            // set dot instead of comma
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = GlobalConstantsEdu.SYNTAX_DOT;
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            List<int> lstPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();
            var lstMark = this.PupilProfileBusiness.GetListExamMarkSemester(lstPupilID, schoolID, classID, academicYearID, semester);

            string semesterName = String.Empty;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) semesterName = "HKI";
                else semesterName = "HKII";
            }
            String SMSFormat = String.Format("{0}thông báo kết quả thi {1} của em ", prefix, semesterName);


            int STT = 1;
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;
            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            SendSMSToParentBO model = null;
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                objPupilProfileBO = lstPupil[k];

                if (lstMark != null) SMSContent = lstMark.Where(p => p.PupilID == model.PupilFileID).Select(p => p.MarkContent).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false, "\n");

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetSemesterResult(int schoolID, int academicYearID, int year, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, int semester, string prefix)
        {
            // set dot instead of comma
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = GlobalConstantsEdu.SYNTAX_DOT;
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            string semesterName = String.Empty;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || semester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
            {
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    semesterName = "HKI";
                }
                else
                {
                    semesterName = "HKII";
                }
            }
            else if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL) semesterName = "cả năm";

            String SMSFormat = String.Format("{0}thông báo kết quả {1} của em ", prefix, semesterName);

            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            SendSMSToParentBO model = null;
            int STT = 1;
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;

            List<PupilMarkBO> lstMarkRecordSemesterList = this.PupilProfileBusiness.GetListMarkRecordSemester(schoolID, classID, academicYearID, semester);
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                if (lstMarkRecordSemesterList != null) SMSContent = lstMarkRecordSemesterList.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetInfoExamSchedule(int schoolID, int academicYearID, int semester, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, int examID, string prefix)
        {
            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            PupilProfileBO objPupilProfileBO = null;
            SendSMSToParentBO model = null;
            List<long> listPupilID = lstPupil.Select(p => (long)p.PupilProfileID).ToList();
            List<ExamSubjectBO> listResult = this.PupilProfileBusiness.getExamScheduleInfo(listPupilID, schoolID, academicYearID, classID, semester, examID);
            ExamSubjectBO examSubjectBO = null;

            int STT = 1;
            string SMSFormat = String.Empty;
            string SMSContent = String.Empty;

            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSFormat = String.Empty;
                SMSContent = String.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                examSubjectBO = listResult.FirstOrDefault(p => p.PupilID == model.PupilFileID);
                if (examSubjectBO != null && !string.IsNullOrEmpty(examSubjectBO.ScheduleContent))
                {
                    SMSFormat = String.Format("{0}thông báo lịch thi {1} ", prefix, examSubjectBO.ExaminationsName);
                    SMSContent = examSubjectBO.ScheduleContent;
                }
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetExamResult(int schoolID, int academicYearID, int semester, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, int examID, string prefix)
        {
            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            PupilProfileBO objPupilProfileBO = null;
            SendSMSToParentBO model = null;
            List<long> listPupilID = lstPupil.Select(p => (long)p.PupilProfileID).ToList();
            List<ExamMarkPupilBO> listResult = this.PupilProfileBusiness.getExamResultInfo(listPupilID, schoolID, academicYearID, classID, semester, examID);

            int STT = 1;
            string SMSFormat = string.Empty;
            string SMSContent = string.Empty;
            ExamMarkPupilBO examMarkPupilBO = null;
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSFormat = string.Empty;
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.isCheck = false;
                model.ClassID = classID;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.IsSent = true;
                examMarkPupilBO = listResult.FirstOrDefault(p => p.PupilID == model.PupilFileID);
                if (examMarkPupilBO != null && !string.IsNullOrEmpty(examMarkPupilBO.ExamResult))
                {
                    SMSFormat = String.Format("{0}thông báo kết quả kỳ thi {1} em ", prefix, examMarkPupilBO.ExaminationName);
                    SMSContent = examMarkPupilBO.ExamResult;
                }
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }

        private List<SendSMSToParentBO> GetCalendarOfPupil(int schoolID, int academicYearID, int year, int semester, List<PupilProfileBO> lstPupil, int classID, int educationLevelID, string prefix)
        {
            string SMSFormat = String.Format("{0}thông báo thời khóa biểu của em ", prefix);
            List<SendSMSToParentBO> lstPupilRet = new List<SendSMSToParentBO>();
            int STT = 1;
            SendSMSToParentBO model = null;
            PupilProfileBO objPupilProfileBO = null;
            string SMSContent = string.Empty;

            string CalendarOfPupil = this.PupilProfileBusiness.GetListCalendarOfPupil(academicYearID, classID, semester);
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentBO();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.PupilName = objPupilProfileBO.FullName;
                model.isCheck = false;
                model.ClassID = classID;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.Content = SMSFormat;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                SMSContent = CalendarOfPupil;
                model.IsSent = true;
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), false);

                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }


        private List<int> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<int> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return listID;
        }

        private string GetPrefix(string prefix, string schoolName, bool vocationType, bool hasColon = true)
        {
            string result;
            if (prefix == null)
            {

                if (vocationType)
                {
                    if (!schoolName.StripVNSign().ToUpper().StartsWith("TRUNG TAM "))
                    {
                        schoolName = "Trung tâm " + schoolName;
                    }
                    else
                    {
                        schoolName = "Trung tâm " + schoolName.Substring("Trung tam ".Length, schoolName.Length - "Trung tam ".Length);
                    }
                }
                else
                {
                    if (!schoolName.StripVNSign().ToUpper().StartsWith("TRUONG "))
                    {
                        schoolName = "Trường " + schoolName;
                    }
                    else
                    {
                        schoolName = "Trường " + schoolName.Substring("Truong ".Length, schoolName.Length - "Truong ".Length);
                    }
                }

                result = schoolName + GlobalConstantsEdu.COMMON_COLON_SPACE;
                //AnhVD9 - 20151026 Bỏ dấu : đối với các bản tin # Trao đổi
                if (!hasColon) result = schoolName + GlobalConstants.SPACE;
            }
            else
            {
                if (prefix != string.Empty)
                {
                    result = prefix + GlobalConstantsEdu.COMMON_COLON_SPACE;
                    //AnhVD9 - 20151026 Bỏ dấu : đối với các bản tin # Trao đổi
                    if (!hasColon) result = prefix + GlobalConstants.SPACE;
                }
                else
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        private SendSMSToParentBO countSMS(SendSMSToParentBO model, string content, string formatSMS, string pupilName, bool isSignMsg = false, string SEPARATOR = GlobalConstantsEdu.COMMON_COLON_SPACE, bool returnNoSMSMSg = true)
        {
            if (!string.IsNullOrEmpty(content))
            {
                model.isNewMessage = true;
                content = String.Format("{0}{1}{2}{3}", formatSMS, pupilName, SEPARATOR, content);
                int numberChac = content.Length;
                int numSMS = 0;
                if (isSignMsg)
                {
                    model.Content = content;
                    if (numberChac <= GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT)
                    {
                        numSMS = 1;
                        //model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                    }
                    else
                    {
                        numSMS = (int)Math.Ceiling((double)numberChac / GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                        //model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                    }
                }
                else
                {
                    model.Content = content.StripVNSign();
                    numSMS = (int)Math.Ceiling((double)numberChac / GlobalConstantsEdu.COMMON_SMS_COUNT);
                    //model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }

                model.NumSMS = numSMS;
            }
            else
            {
                if (returnNoSMSMSg)
                {
                    model.Content = "Không có tin mới.";
                }
            }
            return model;
        }

        private SendSMSToParentBO countSMSBreakLine(SendSMSToParentBO model, string content, string formatSMS, string pupilName, bool isSignMsg = false, string SEPARATOR = GlobalConstantsEdu.COMMON_COLON_SPACE)
        {
            if (!string.IsNullOrEmpty(content))
            {
                model.isNewMessage = true;
                content = String.Format("{0}{1}{2}{3}", formatSMS, pupilName, SEPARATOR + "\n", content);
                int numberChac = content.Length;
                int numSMS = 0;
                if (isSignMsg)
                {
                    model.Content = content;
                    if (numberChac <= GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT) { }
                    //model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                    else
                    {
                        numSMS = (int)Math.Ceiling((double)numberChac / GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                        //model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                    }
                }
                else
                {
                    model.Content = content.StripVNSign();
                    numSMS = (int)Math.Ceiling((double)numberChac / GlobalConstantsEdu.COMMON_SMS_COUNT);
                    //model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }
            }
            else
            {
                model.Content = "Không có tin mới.";
            }
            return model;
        }

        private ResultBO SendData(int schoolId, int academicYearId, int employeeID, int grade, string schoolName, bool rolePrincipal, bool schoolAdmin, List<int> lstReceiverID, int ClassID, string Content, int TypeID, List<string> lstContent, List<string> lstShortContent, int? EducationLevelID = 0, bool isSignMsg = false,
            bool isTimerUsed = false, string timerConfigName = "", DateTime sendTime = default(DateTime), bool sendAllSchool = false, bool isAllClass = false, bool isCustomType = false)
        {
            //Tin nhan SMS          
            string shortContent = string.Empty;

            SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolId);
            bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX");
            string prefixNoColon = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX, false);
            string prefixWithColon = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX);

            #region get short content
            if (lstContent.Count == 0)
            {
                string brandName = this.GetBrandName(isGDTX);
                if (Content.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
                {
                    shortContent = Content.Remove(0, prefixWithColon.Length);
                }
                else if (Content.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
                {
                    shortContent = Content.Remove(0, prefixNoColon.Length);
                }
                else if (Content.StripVNSign().StartsWith(prefixWithColon.Trim().StripVNSign())) // Format có dấu : nhưng ko có khoảng trắng
                {
                    shortContent = Content.Remove(0, prefixWithColon.Trim().Length);
                }
                else if (Content.StripVNSign().StartsWith(prefixNoColon.Trim().StripVNSign())) // Format ko có dấu : và ko có khoẳng trắng
                {
                    shortContent = Content.Remove(0, prefixNoColon.Trim().Length);
                }
                //else if (Content.StripVNSign().StartsWith(brandName.StripVNSign()))
                //{
                //    shortContent = Content.Remove(0, brandName.Length);
                //}
                else
                {
                    shortContent = Content;
                }
            }
            #endregion

            Dictionary<string, object> dic = new Dictionary<string, object>();
            //Nếu là admin trường thì EmployeeID = 0
            dic["senderID"] = employeeID;
            dic["lstReceiverID"] = lstReceiverID;
            dic["schoolID"] = schoolId;
            dic["classID"] = ClassID;
            dic["content"] = Content.Trim();
            dic["isSignMsg"] = isSignMsg;
            dic["shortContent"] = shortContent;
            dic["academicYearID"] = academicYearId;
            dic["type"] = GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT;
            dic["typeHistory"] = TypeID;
            dic["isPrincipal"] = rolePrincipal;
            dic["isAdmin"] = schoolAdmin;
            dic["lstContent"] = lstContent;
            dic["lstShortContent"] = lstShortContent;
            dic["iEducationLevelID"] = EducationLevelID;
            dic["isTimerUsed"] = isTimerUsed;
            dic["sendTime"] = sendTime;
            dic["timerConfigName"] = timerConfigName;
            dic["BoolSendAllSchool"] = sendAllSchool;
            dic["IsAllClass"] = isAllClass;
            dic["IsCustomType"] = isCustomType;

            var aca = AcademicYearBusiness.Find(academicYearId);
            AcademicYearBO academicYeaBO = null;
            if (aca != null)
            {
                academicYeaBO = new AcademicYearBO()
                {
                    AcademicYearID = aca.AcademicYearID,
                    FirstSemesterStartDate = aca.FirstSemesterStartDate,
                    FirstSemesterEndDate = aca.FirstSemesterEndDate,
                    SecondSemesterStartDate = aca.SecondSemesterStartDate,
                    SecondSemesterEndDate = aca.SecondSemesterEndDate,
                    Year = aca.Year,
                    SchoolID = aca.SchoolID
                };
            }
            else
            {
                academicYeaBO = new AcademicYearBO();
            }

            int semester = 0;
            DateTime dateTimeNow = DateTime.Now;
            AcademicYearBO academicYearBO = academicYeaBO;
            if (academicYearBO != null)
            {
                if (academicYearBO.FirstSemesterStartDate.Value.Date <= dateTimeNow && academicYearBO.SecondSemesterStartDate.Value.Date.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(59) >= dateTimeNow)
                {
                    semester = 1;
                }
                // Anhvd9 - Cho phep nhan tin toi 31.8 neu nam hoc cu da ket thuc
                if (academicYearBO.SecondSemesterStartDate.Value.Date <= dateTimeNow && new DateTime(academicYearBO.SecondSemesterEndDate.Value.Year, GlobalConstantsEdu.MONTH_END_ALLOW_SEND_SMS, GlobalConstantsEdu.DAYOFMONTH_END_ALLOW_SEND_SMS, 23, 59, 59) >= dateTimeNow)
                {
                    semester = 2;
                }
            }

            return this.SMSHistoryBusiness.SendSMSExternal(dic, academicYearId, semester, grade, schoolName);
        }

        private MSendSMSResult CreateSMSTraoDoi(MSendSmsRequest data, out bool success, bool isSendImport = false)
        {
            success = false;

            int EducatinlevelID = data.EducationlevelID;
            int counter = 0;
            bool isSignMsg = false;
            SMS_SCHOOL_CONFIG cfg = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(data.SchoolID);
            if (cfg != null) isSignMsg = cfg.IS_ALLOW_SEND_UNICODE && false;

            DateTime sendTime = default(DateTime);

            #region Xử lý Prefix
            string content = data.Contents.FirstOrDefault(); // Nội dung trao đổi
            // AnhVD9 20141104 - Tin nhan SMS bat dau voi: BRAND_NAME
            SchoolProfile sp = SchoolProfileBusiness.Find(data.SchoolID);
            SMS_SCHOOL_CONFIG sc = this.SchoolConfigBusiness.GetSchoolConfigBySchoolID(data.SchoolID);
            bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX");
            string prefixNoColon = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX, false);
            string prefixWithColon = this.GetPrefix(sc != null ? sc.PRE_FIX : null, sp.SchoolName, isGDTX);
            string brandName = this.GetBrandName(isGDTX);
            //if (!content.StripVNSign().StartsWith(brandName.StripVNSign()))
            //{
            //    //Dieu chinh lai template noi dung tin nhan theo mẫu [Tên trường]: [Noi dung SMS]
            //    content = String.Format(SendSMSToParentConstants.SMSFORMAT_SEND, brandName, content);
            //}

            string shortContent = string.Empty;
            if (content.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
            {
                shortContent = content.Remove(0, prefixWithColon.Length);
            }
            else if (content.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
            {
                shortContent = content.Remove(0, prefixNoColon.Length);
            }
            else if (content.StripVNSign().StartsWith(prefixWithColon.StripVNSign().Trim())) // Format có dấu : nhưng ko có khoảng trắng
            {
                shortContent = content.Remove(0, prefixWithColon.Trim().Length);
            }
            else if (content.StripVNSign().StartsWith(prefixNoColon.StripVNSign().Trim())) // Format ko có dấu : và ko có khoẳng trắng
            {
                shortContent = content.Remove(0, prefixNoColon.Trim().Length);
            }
            //else if (content.StripVNSign().StartsWith(brandName.StripVNSign()))
            //{
            //    shortContent = content.Remove(0, brandName.Length);
            //}
            else
            {
                shortContent = content;
            }
            #endregion
            #region toan lop

            #region Kiểm tra nội dung tin
            //check prefix va tao shortContent truong truong hop gui toan lop voi noi dung tin nhan khac nhau tung hoc sinh
            // Prefix có dạng [TRUONG]\_/  
            for (int i = 0, size = data.Contents.Count; i < size; i++)
            {
                content = data.Contents[i];
                //if (!content.StripVNSign().StartsWith(brandName.StripVNSign()))
                //{
                //    //Dieu chinh lai template noi dung tin nhan theo mẫu [Tên trường]: [Noi dung SMS]
                //    data.Contents[i] = String.Format(SendSMSToParentConstants.SMSFORMAT_SEND, brandName, content);
                //    content = data.Contents[i];
                //}

                #region format ShortContent
                if (content.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
                {
                    shortContent = content.Remove(0, prefixWithColon.Length);
                }
                else if (content.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
                {
                    shortContent = content.Remove(0, prefixNoColon.Length);
                }
                else if (content.StripVNSign().StartsWith(prefixWithColon.Trim().StripVNSign())) // Format có dấu : nhưng ko có khoảng trắng
                {
                    shortContent = content.Remove(0, prefixWithColon.Trim().Length);
                }
                else if (content.StripVNSign().StartsWith(prefixNoColon.Trim().StripVNSign())) // Format ko có dấu : và ko có khoẳng trắng
                {
                    shortContent = content.Remove(0, prefixNoColon.Trim().Length);
                }
                //else if (content.StripVNSign().StartsWith(brandName.StripVNSign()))
                //{
                //    shortContent = content.Remove(0, brandName.Length);
                //}
                else
                {
                    shortContent = content;
                }
                #endregion

                data.ShortContents.Add(shortContent);
            }
            #endregion

            ResultBO result = SendData(data.SchoolID, data.AcademicYearID, data.EmployeeID, data.Grade, data.SchoolName, data.Role == 3, data.Role == 1, data.PupilIDs, data.ClassID, string.Empty, GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID, data.Contents, data.ShortContents, 0, isSignMsg,
                false, string.Empty, sendTime);
            if (result.isError)
            {
                return new MSendSMSResult(false, 0, result.ErrorMsg);
            }
            counter += result.NumSendSuccess;

            #endregion

            success = true;
            return new MSendSMSResult(true, counter, string.Format("Gửi thành công đến {0} phụ huynh học sinh", counter));

        }

        public List<MarkConfigByTimeBO> GetTimeMarkBySemester(int academicYearID, int semester)
        {
            IDictionary<string, object> SearchDic = new Dictionary<string, object>();
            SearchDic["AcademicYearID"] = academicYearID;
            //Neu truyen vao hoc ky = 0 thi lay tat ca cac dot cua nam
            if (semester != 0)
            {
                SearchDic["Semester"] = semester;
            }
            List<MarkConfigByTimeBO> listPeriod = PeriodDeclarationBusiness.Search(SearchDic).Select(o => new MarkConfigByTimeBO
            {
                FromDate = o.FromDate.HasValue ? o.FromDate.Value : DateTime.Now,
                MarkConfigByTimeID = o.PeriodDeclarationID,
                SemesterID = o.Semester.HasValue ? o.Semester.Value : 0,
                TimeName = o.Resolution,
                ToDate = o.EndDate.HasValue ? o.EndDate.Value : DateTime.Now,
                Year =  o.Year.HasValue ? o.Year.Value : 0
            }).ToList();

            return listPeriod;
        }

        public string GetBrandName(bool VocationType)
        {
            string brandName = "";
            if (VocationType) brandName = "Trung tâm ";
            else brandName = "Trường ";
            return brandName;
        }

        #region Chat group or 1 & 1

        private string FirebaseDBStantdartUrl = "https://smasplus.firebaseio.com/";

        public ChatMessageBO InsertChatMessageData(int ChatGroupID, int UserAccountSendID, string ContentMessage)
        {
            var chatMessageObj = new ChatMessageBO();
            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                    DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second,
                                    DateTime.Now.Millisecond);
            chatMessageObj.ChatGroupID = ChatGroupID;
            chatMessageObj.UserAccountSendID = UserAccountSendID;
            chatMessageObj.SendDate = date;
            try
            {
                var url = FirebaseDBStantdartUrl + ChatGroupID + ".json";
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(new ChatMessageBO
                {
                    ChatGroupID = ChatGroupID,
                    UserAccountSendID = UserAccountSendID,
                    ContentMessage = ContentMessage,
                    SendDate = date
                });

                var request = WebRequest.CreateHttp(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                var buffer = Encoding.UTF8.GetBytes(json);
                request.ContentLength = buffer.Length;
                request.GetRequestStream().Write(buffer, 0, buffer.Length);
                //var response = request.GetResponse();
                //json = (new StreamReader(response.GetResponseStream())).ReadToEnd();
                chatMessageObj.ContentMessage = ContentMessage;
            }
            catch (Exception)
            {
                chatMessageObj.ContentMessage = "Can't send message!";
            }
            
            return chatMessageObj;
        }

        public List<ChatMessageBO> GetListChatMessage(int ChatGroupID, int UserAccountSendID, int PageIndex, int PageSize)
        {
            /*var url = FirebaseDBStantdartUrl + ChatGroupID + ".json?print=pretty";
            //var url = FirebaseDBStantdartUrl + ChatGroupID + "/" + UserAccountSendID + ".json?print=pretty";
            var ListChatMessage = new List<ChatMessageBO>();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (Stream requestStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, Encoding.UTF8);
                    var readerData = reader.ReadToEnd();
                    dynamic dataConvert = JsonConvert.DeserializeObject<dynamic>(readerData);
                    if (dataConvert != null)
                    {
                        foreach (var itemConvert in dataConvert)
                        {
                            if (!string.IsNullOrEmpty(itemConvert.ToString()) && itemConvert != null && ((JProperty)itemConvert) != null)
                            {
                                ListChatMessage.Add(JsonConvert.DeserializeObject<ChatMessageBO>(((JProperty)itemConvert).Value.ToString()));
                            }
                        }
                    }
                }
                ListChatMessage = ListChatMessage.OrderByDescending(x => x.SendDate).Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();

            }
            catch (Exception)
            {
                ListChatMessage.Add(new ChatMessageBO()
                {
                    ChatGroupID = 0,
                    ContentMessage = "Can't get message!"
                });
            }
            
            return ListChatMessage;
            */
            return new List<ChatMessageBO>();
        }

        public bool DeleteChatMessageInGroupByID(int GroupID, List<string> listMessID)
        {
            bool result = true;
            var url = string.Empty;
            HttpWebRequest request = null;
            WebResponse response = null;            
            try
            {
                foreach (var mess in listMessID)
                {
                    url = FirebaseDBStantdartUrl + GroupID + "/" + mess + ".json";
                    request = WebRequest.CreateHttp(url);
                    request.Method = "DELETE";
                    request.ContentType = "application/json";
                    response = request.GetResponse();
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        #endregion
        

    }
}
