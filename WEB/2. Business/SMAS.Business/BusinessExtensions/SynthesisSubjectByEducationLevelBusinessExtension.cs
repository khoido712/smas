﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class SynthesisSubjectByEducationLevelBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// minhh - 28/11/2012
        ///Lấy mảng băm cho các tham số đầu vào 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(SynthesisSubjectByEducationLevel entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel},
                {"StatisticLevelReportID", entity.StatisticLevelReportID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin thống kê xep loai mon hoc theo khoi
        /// <summary>
        /// minhh - 28/11/2012
        /// Lưu lại thông tin 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertSynthesisSubjectByEducationLevel(SynthesisSubjectByEducationLevel entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_CAC_MON_THEO_KHOI;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"StatisticLevelReportID", entity.StatisticLevelReportID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy bảng thống kê xep loai mon hoc được cập nhật mới nhất
        /// <summary>
        /// <author>minhh</author>
        /// Lấy bảng thống kê xếp hạng mon hoc được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport GetSynthesisSubjectByEducationLevel(SynthesisSubjectByEducationLevel entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_CAC_MON_THEO_KHOI;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin thống kê xep loai mon hoc
        /// <summary>
        /// <author>minhh</author>
        /// Lưu lại thông tin xếp hạng mon hoc theo khoi
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateSynthesisSubjectByEducationLevel(SynthesisSubjectByEducationLevel entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_CAC_MON_THEO_KHOI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "S10");

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);

            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);

            string semester = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semester = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semester = "HỌC KỲ II";
            string semesterAndAcademic = semester + " - NĂM HỌC: " + academicYear.DisplayTitle;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                semesterAndAcademic = "NĂM HỌC: " + academicYear.DisplayTitle;
            }
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.SetCellValue("I4", provinceAndDate);
            sheet.SetCellValue("A7", semesterAndAcademic);
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            int startRow = 11;


            var lstSubject = SchoolSubjectBusiness
                                                .SearchByAcademicYear(entity.AcademicYearID, new Dictionary<string, object> { })
                                                .Where(u => u.SubjectCat.AppliedLevel == entity.AppliedLevel)
                                                .Select(u => new { u.SubjectID, u.SubjectCat.DisplayName, u.SubjectCat.OrderInSubject, u.IsCommenting })
                                                .Distinct()
                                                .OrderBy(o => o.OrderInSubject)
                                                .ThenBy(o => o.DisplayName)
                                                .ToList();
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClassBO> lstQPoc;
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                          join cs in ClassSubjectBusiness.SearchBySchool(entity.SchoolID) on p.ClassID equals cs.ClassID
                          join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                          join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                          where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          && cs.SectionPerWeekFirstSemester.HasValue && cs.SectionPerWeekFirstSemester > 0
                          && r.IsActive.Value
                          select new PupilOfClassBO
                          {
                              PupilID = p.PupilID,
                              ClassID = p.ClassID,
                              EducationLevelID = r.EducationLevelID,
                              Genre = q.Genre,
                              EthnicID = q.EthnicID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject
                          };
            }
            else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                          join cs in ClassSubjectBusiness.SearchBySchool(entity.SchoolID) on p.ClassID equals cs.ClassID
                          join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                          join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                          where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          && cs.SectionPerWeekSecondSemester.HasValue && cs.SectionPerWeekSecondSemester > 0
                          && r.IsActive.Value
                          select new PupilOfClassBO
                          {
                              PupilID = p.PupilID,
                              ClassID = p.ClassID,
                              EducationLevelID = r.EducationLevelID,
                              Genre = q.Genre,
                              EthnicID = q.EthnicID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject
                          };
            }
            else
            {
                lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                          join cs in ClassSubjectBusiness.SearchBySchool(entity.SchoolID) on p.ClassID equals cs.ClassID
                          join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                          join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                          where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          && r.IsActive.Value
                          select new PupilOfClassBO
                          {
                              PupilID = p.PupilID,
                              ClassID = p.ClassID,
                              EducationLevelID = r.EducationLevelID,
                              Genre = q.Genre,
                              EthnicID = q.EthnicID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject
                          };
            }

            //loc ra nhung hoc sinh khong dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",Aca.Year}
            };



            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, 0, 0);

            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID", null},
                {"AppliedLevel",entity.AppliedLevel}
            };

            List<SummedUpRecordBO> tmpSummedUp = new List<SummedUpRecordBO>();
            int partitionID = UtilsBusiness.GetPartionId(entity.SchoolID);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                IQueryable<SummedUpRecordBO> lstSummedUptmp = (from sur in VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic)
                                                               join q in ClassProfileBusiness.All on sur.ClassID equals q.ClassProfileID
                                                               join r in PupilProfileBusiness.All on sur.PupilID equals r.PupilProfileID
                                                               where sur.Last2digitNumberSchool == partitionID
                                                               && sur.PeriodID == null
                                                               && sur.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                               && q.IsActive.Value
                                                               select new SummedUpRecordBO
                                                               {
                                                                   PupilID = sur.PupilID,
                                                                   ClassID = sur.ClassID,
                                                                   SchoolID = sur.SchoolID,
                                                                   EducationLevelID = q.EducationLevelID,
                                                                   SubjectID = sur.SubjectID,
                                                                   Genre = r.Genre,
                                                                   Semester = sur.Semester,
                                                                   EthnicID = r.EthnicID,
                                                                   SummedUpMark = sur.ReTestMark != null ? sur.ReTestMark : sur.SummedUpMark,
                                                                   JudgementResult = sur.ReTestJudgement != null ? sur.ReTestJudgement : sur.JudgementResult
                                                               });
                tmpSummedUp = lstSummedUptmp.Where(o => o.Semester == lstSummedUptmp.Where(v => v.PupilID == o.PupilID && v.ClassID == o.ClassID && v.SubjectID == o.SubjectID).Max(v => v.Semester)).ToList();
            }
            else
            {
                dic["Semester"] = entity.Semester;
                tmpSummedUp = (from sur in VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dic)
                               join q in ClassProfileBusiness.All on sur.ClassID equals q.ClassProfileID
                               join r in PupilProfileBusiness.All on sur.PupilID equals r.PupilProfileID
                               where sur.Last2digitNumberSchool == partitionID
                               && sur.PeriodID == null
                               && q.IsActive.Value
                               select new SummedUpRecordBO
                               {
                                   PupilID = sur.PupilID,
                                   ClassID = sur.ClassID,
                                   SchoolID = sur.SchoolID,
                                   EducationLevelID = q.EducationLevelID,
                                   SubjectID = sur.SubjectID,
                                   Genre = r.Genre,
                                   EthnicID = r.EthnicID,
                                   SummedUpMark = sur.ReTestMark != null ? sur.ReTestMark : sur.SummedUpMark,
                                   JudgementResult = sur.ReTestJudgement != null ? sur.ReTestJudgement : sur.JudgementResult
                               }).ToList();
            }

            List<SummedUpRecordBO> lstSummedUp = (from u in tmpSummedUp
                                                  join l in lstPOC on u.ClassID equals l.ClassID
                                                  where u.PupilID == l.PupilID
                                                  && u.SubjectID == l.SubjectID
                                                  select u).ToList();
            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstSummedUp = lstSummedUp.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            List<SummedUpRecordBO> lstSummedUp_Female = lstSummedUp.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            List<SummedUpRecordBO> lstSummedUp_Ethnic = lstSummedUp.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            List<SummedUpRecordBO> lstSummedUp_FemaleEthnic = lstSummedUp.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();

            // Lay danh sach hoc sinh theo khoi
            var listCountCurentPupilByEducation = lstPOC.GroupBy(o => new { o.EducationLevelID, o.SubjectID }).Select(o => new { EducationLevelID = o.Key.EducationLevelID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByEducation_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.EducationLevelID, o.SubjectID }).Select(o => new { EducationLevelID = o.Key.EducationLevelID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByEducation_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).GroupBy(o => new { o.EducationLevelID, o.SubjectID }).Select(o => new { EducationLevelID = o.Key.EducationLevelID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByEducation_FemaleEthnic = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).GroupBy(o => new { o.EducationLevelID, o.SubjectID }).Select(o => new { EducationLevelID = o.Key.EducationLevelID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            List<StatisticLevelConfig> lstStatisticLevelConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", entity.StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            int numberConfig = lstStatisticLevelConfig.Count();
            List<SummedUpRecordBO> lstSummedUpSubject = new List<SummedUpRecordBO>();
            List<decimal?> lstSummedUpEduBySubject = new List<decimal?>();
            List<string> lstSummedUpEduBySubjectNX = new List<string>();
            int SL = 0;
            int col = 0;
            int lastColumn = (numberConfig + 2) * 2 + 3;
            sheet.Name = "XLMH";
            IVTRange range = firstSheet.GetRange(12, 1, 12, lastColumn);
            IVTRange bRangeGroup = firstSheet.GetRange(14, 1, 14, lastColumn);
            IVTRange eRangeGroup = firstSheet.GetRange(16, 1, 16, lastColumn);
            IVTRange range_level = firstSheet.GetRange("D9", "E10");
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_level, 9, 4 + col);
                sheet.SetCellValue(9, 4 + col, lstStatisticLevelConfig[i].Title);
                col += 2;
            }
            sheet.CopyPasteSameSize(range_level, 9, 4 + col);
            sheet.SetCellValue(9, 4 + col, SystemParamsInFile.JUDGE_MARK_D);
            col += 2;
            sheet.CopyPasteSameSize(range_level, 9, 4 + col);
            sheet.SetCellValue(9, 4 + col, SystemParamsInFile.JUDGE_MARK_CD);
            int passPupil = 0;
            int failPupil = 0;
            int countAllPupilOfEducation = 0;
            //HoanTV5 start 2014/12/25
            // Thay đổi luồng nghiệp vụ nếu môn không được khai báo cho khối sẽ không thống kê
            IQueryable<SchoolSubject> IqSchoolSubject = (from s in SchoolSubjectBusiness.All
                                                         where s.SchoolID == entity.SchoolID
                                                             && s.AcademicYearID == entity.AcademicYearID
                                                             && (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY && s.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID1 && s.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID7
                                                                 || entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY && s.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID5 && s.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID10
                                                                 || entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY && s.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID9 && s.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID13)
                                                         select s);

            int count = 0;
            int educationLevelID = 0;
            int countEdu = 0;
            #region THCM
            for (int p = 0; p < lstSubject.Count; p++)
            {

                bool hasMarkSubjectAll = false;
                bool hasJudgeSubjectAll = false;
                countEdu = 0;
                //Lay ra row chua mon hoc va lop hoc mon do
                var ClassSubject = lstSubject[p];
                int startmin = startRow;
                lstSummedUpSubject = lstSummedUp.Where(o => o.SubjectID == ClassSubject.SubjectID).ToList();
                //Duyet theo tung khoi
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {

                    educationLevelID = lstEducationLevel[i].EducationLevelID;
                    count = IqSchoolSubject.Where(s => s.EducationLevelID == educationLevelID && s.SubjectID == ClassSubject.SubjectID).Count();
                    if (count == 0)//nếu môn không được khai báo cho khối thì không fill dữ liệu
                    {
                        continue;
                    }
                    countEdu++;
                    col = 0;
                    countAllPupilOfEducation = 0;
                    //Copy row style
                    if (i == 0)
                        sheet.CopyPasteSameRowHeigh(bRangeGroup, startRow);
                    else
                        sheet.CopyPasteSameRowHeigh(range, startRow);

                    //Ten khoi
                    sheet.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);
                    var objCountPupil = listCountCurentPupilByEducation.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID).FirstOrDefault();
                    countAllPupilOfEducation += objCountPupil != null ? objCountPupil.TotalPupil : 0;
                    sheet.SetCellValue(startRow, 3, countAllPupilOfEducation);
                    if (ClassSubject != null && ClassSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        hasMarkSubjectAll = true;
                        lstSummedUpEduBySubject = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID).Select(o => o.SummedUpMark).ToList();
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o <= slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o < slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    SL = lstSummedUpEduBySubject.Where(o => o == slc.MaxValue).Count();
                                    sheet.SetCellValue(startRow, 4 + col, SL);//E
                                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                }
                            }
                            col += 2;
                        }
                    }
                    else
                    {
                        hasJudgeSubjectAll = true;
                        col = numberConfig * 2;
                        passPupil = 0;
                        failPupil = 0;
                        lstSummedUpEduBySubjectNX = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID && o.JudgementResult != null && o.JudgementResult != "").Select(o => o.JudgementResult).ToList();
                        if (lstSummedUpEduBySubjectNX.Count > 0)
                        {
                            //Dien hanh kiem D
                            passPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                            //Dien hanh kiem CD
                            failPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                        }
                        sheet.SetCellValue(startRow, col + 4, passPupil);//P
                        sheet.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                        col += 2;
                        sheet.SetCellValue(startRow, col + 4, failPupil);//P
                        sheet.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                    }
                    startRow++;

                }//end for education

                sheet.CopyPasteSameRowHeigh(eRangeGroup, startRow);
                sheet.MergeColumn(1, startRow - countEdu, startRow);
                sheet.SetCellValue(startRow - countEdu, 1, lstSubject[p].DisplayName);

                sheet.SetCellValue(startRow, 2, "TS");
                sheet.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
                col = 0;
                if (hasMarkSubjectAll)
                {
                    for (int m = 0; m < numberConfig; m++)
                    {
                        sheet.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllGoodPupil);
                        col += 2;
                    }
                }
                if (hasJudgeSubjectAll)
                {
                    col = numberConfig * 2;
                    sheet.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    col += 2;
                    sheet.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                }
                startRow++;
            }//end for listSubject
            sheet.FitAllColumnsOnOnePage = true;
            sheet.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startRow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            sheet.SetFontName("Times New Roman", 0);
            #endregion


            if (entity.FemaleChecked)
            {
                #region Tạo sheet học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(sheet);
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC, HỌC SINH NỮ");
                startRow = 11;
                for (int p = 0; p < lstSubject.Count; p++)
                {

                    bool hasMarkSubjectAll = false;
                    bool hasJudgeSubjectAll = false;
                    countEdu = 0;

                    //Lay ra row chua mon hoc va lop hoc mon do
                    var ClassSubject = lstSubject[p];
                    int startmin = startRow;
                    lstSummedUpSubject = lstSummedUp_Female.Where(o => o.SubjectID == ClassSubject.SubjectID).ToList();
                    //Duyet theo tung khoi
                    for (int i = 0; i < lstEducationLevel.Count; i++)
                    {
                        educationLevelID = lstEducationLevel[i].EducationLevelID;
                        count = IqSchoolSubject.Where(s => s.EducationLevelID == educationLevelID && s.SubjectID == ClassSubject.SubjectID).Count();
                        if (count == 0)//nếu môn không được khai báo cho khối thì không fill dữ liệu
                        {
                            continue;
                        }
                        countEdu++;
                        col = 0;
                        countAllPupilOfEducation = 0;
                        //Copy row style
                        if (i == 0)
                            sheet_Female.CopyPasteSameRowHeigh(bRangeGroup, startRow);
                        else
                            sheet_Female.CopyPasteSameRowHeigh(range, startRow);

                        //Ten khoi
                        sheet_Female.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);
                        var objCountPupil = listCountCurentPupilByEducation_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID).FirstOrDefault();
                        countAllPupilOfEducation += objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        sheet_Female.SetCellValue(startRow, 3, countAllPupilOfEducation);
                        if (ClassSubject != null && ClassSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            hasMarkSubjectAll = true;
                            lstSummedUpEduBySubject = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).Select(o => o.SummedUpMark).ToList();
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                col += 2;
                            }
                        }
                        else
                        {
                            hasJudgeSubjectAll = true;
                            col = numberConfig * 2;
                            passPupil = 0;
                            failPupil = 0;
                            lstSummedUpEduBySubjectNX = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID && o.JudgementResult != null && o.JudgementResult != "").Select(o => o.JudgementResult).ToList();
                            if (lstSummedUpEduBySubjectNX.Count > 0)
                            {
                                //Dien hanh kiem D
                                passPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                //Dien hanh kiem CD
                                failPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                            }
                            sheet_Female.SetCellValue(startRow, col + 4, passPupil);//P
                            sheet_Female.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                            col += 2;
                            sheet_Female.SetCellValue(startRow, col + 4, failPupil);//P
                            sheet_Female.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                        }
                        startRow++;

                    }//end for education

                    sheet_Female.CopyPasteSameRowHeigh(eRangeGroup, startRow);
                    sheet_Female.MergeColumn(1, startRow - countEdu, startRow);
                    sheet_Female.SetCellValue(startRow - countEdu, 1, lstSubject[p].DisplayName);

                    sheet_Female.SetCellValue(startRow, 2, "TS");
                    sheet_Female.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
                    col = 0;
                    if (hasMarkSubjectAll)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_Female.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                            sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllGoodPupil);
                            col += 2;
                        }
                    }
                    if (hasJudgeSubjectAll)
                    {
                        col = numberConfig * 2;
                        sheet_Female.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                        col += 2;
                        sheet_Female.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    }
                    startRow++;
                }//end for listSubject
                sheet_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startRow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Female.FitAllColumnsOnOnePage = true;
                sheet_Female.SetFontName("Times New Roman", 0);
                #endregion
            }
            if (entity.EthnicChecked)
            {
                #region Tạo sheet học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(sheet);
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC, HỌC SINH DÂN TỘC");
                startRow = 11;
                for (int p = 0; p < lstSubject.Count; p++)
                {

                    bool hasMarkSubjectAll = false;
                    bool hasJudgeSubjectAll = false;
                    countEdu = 0;

                    //Lay ra row chua mon hoc va lop hoc mon do
                    var ClassSubject = lstSubject[p];
                    int startmin = startRow;
                    lstSummedUpSubject = lstSummedUp_Ethnic.Where(o => o.SubjectID == ClassSubject.SubjectID).ToList();
                    //Duyet theo tung khoi
                    for (int i = 0; i < lstEducationLevel.Count; i++)
                    {
                        educationLevelID = lstEducationLevel[i].EducationLevelID;
                        count = IqSchoolSubject.Where(s => s.EducationLevelID == educationLevelID && s.SubjectID == ClassSubject.SubjectID).Count();
                        if (count == 0)//nếu môn không được khai báo cho khối thì không fill dữ liệu
                        {
                            continue;
                        }
                        countEdu++;
                        col = 0;
                        countAllPupilOfEducation = 0;
                        //Copy row style
                        if (i == 0)
                            sheet_Ethnic.CopyPasteSameRowHeigh(bRangeGroup, startRow);
                        else
                            sheet_Ethnic.CopyPasteSameRowHeigh(range, startRow);

                        //Ten khoi
                        sheet_Ethnic.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);
                        var objCountPupil = listCountCurentPupilByEducation_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID).FirstOrDefault();
                        countAllPupilOfEducation += objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        sheet_Ethnic.SetCellValue(startRow, 3, countAllPupilOfEducation);
                        if (ClassSubject != null && ClassSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            hasMarkSubjectAll = true;
                            lstSummedUpEduBySubject = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).Select(o => o.SummedUpMark).ToList();
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                col += 2;
                            }
                        }
                        else
                        {
                            hasJudgeSubjectAll = true;
                            col = numberConfig * 2;
                            passPupil = 0;
                            failPupil = 0;
                            lstSummedUpEduBySubjectNX = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID && o.JudgementResult != null && o.JudgementResult != "").Select(o => o.JudgementResult).ToList();
                            if (lstSummedUpEduBySubjectNX.Count > 0)
                            {
                                //Dien hanh kiem D
                                passPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                //Dien hanh kiem CD
                                failPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                            }
                            sheet_Ethnic.SetCellValue(startRow, col + 4, passPupil);//P
                            sheet_Ethnic.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                            col += 2;
                            sheet_Ethnic.SetCellValue(startRow, col + 4, failPupil);//P
                            sheet_Ethnic.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                        }
                        startRow++;

                    }//end for education

                    sheet_Ethnic.CopyPasteSameRowHeigh(eRangeGroup, startRow);
                    sheet_Ethnic.MergeColumn(1, startRow - countEdu, startRow);
                    sheet_Ethnic.SetCellValue(startRow - countEdu, 1, lstSubject[p].DisplayName);

                    sheet_Ethnic.SetCellValue(startRow, 2, "TS");
                    sheet_Ethnic.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
                    col = 0;
                    if (hasMarkSubjectAll)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_Ethnic.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                            sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllGoodPupil);
                            col += 2;
                        }
                    }
                    if (hasJudgeSubjectAll)
                    {
                        col = numberConfig * 2;
                        sheet_Ethnic.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                        col += 2;
                        sheet_Ethnic.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    }
                    startRow++;
                }//end for listSubject
                sheet_Ethnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startRow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                sheet_Ethnic.SetFontName("Times New Roman", 0);
                #endregion
            }
            if (entity.FemaleEthnicChecked)
            {
                #region Tạo sheet học sinh nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(sheet);
                sheet_FemaleEthnic.Name = "HS_NU_DT";
                sheet_FemaleEthnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC, HỌC SINH NỮ DÂN TỘC");
                startRow = 11;
                for (int p = 0; p < lstSubject.Count; p++)
                {

                    bool hasMarkSubjectAll = false;
                    bool hasJudgeSubjectAll = false;
                    countEdu = 0;

                    //Lay ra row chua mon hoc va lop hoc mon do
                    var ClassSubject = lstSubject[p];
                    int startmin = startRow;
                    lstSummedUpSubject = lstSummedUp_FemaleEthnic.Where(o => o.SubjectID == ClassSubject.SubjectID).ToList();
                    //Duyet theo tung khoi
                    for (int i = 0; i < lstEducationLevel.Count; i++)
                    {
                        educationLevelID = lstEducationLevel[i].EducationLevelID;
                        count = IqSchoolSubject.Where(s => s.EducationLevelID == educationLevelID && s.SubjectID == ClassSubject.SubjectID).Count();
                        if (count == 0)//nếu môn không được khai báo cho khối thì không fill dữ liệu
                        {
                            continue;
                        }
                        countEdu++;
                        col = 0;
                        countAllPupilOfEducation = 0;
                        //Copy row style
                        if (i == 0)
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(bRangeGroup, startRow);
                        else
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(range, startRow);

                        //Ten khoi
                        sheet_FemaleEthnic.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);
                        var objCountPupil = listCountCurentPupilByEducation_FemaleEthnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID).FirstOrDefault();
                        countAllPupilOfEducation += objCountPupil != null ? objCountPupil.TotalPupil : 0;
                        sheet_FemaleEthnic.SetCellValue(startRow, 3, countAllPupilOfEducation);
                        if (ClassSubject != null && ClassSubject.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            hasMarkSubjectAll = true;
                            lstSummedUpEduBySubject = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).Select(o => o.SummedUpMark).ToList();
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o >= slc.MinValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o > slc.MinValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o <= slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o < slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        SL = lstSummedUpEduBySubject.Where(o => o == slc.MaxValue).Count();
                                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(5 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                                    }
                                }
                                col += 2;
                            }
                        }
                        else
                        {
                            hasJudgeSubjectAll = true;
                            col = numberConfig * 2;
                            passPupil = 0;
                            failPupil = 0;
                            lstSummedUpEduBySubjectNX = lstSummedUpSubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == ClassSubject.SubjectID && o.JudgementResult != null && o.JudgementResult != "").Select(o => o.JudgementResult).ToList();
                            if (lstSummedUpEduBySubjectNX.Count > 0)
                            {
                                //Dien hanh kiem D
                                passPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();

                                //Dien hanh kiem CD
                                failPupil = lstSummedUpEduBySubjectNX.Where(o => o.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();

                            }
                            sheet_FemaleEthnic.SetCellValue(startRow, col + 4, passPupil);//P
                            sheet_FemaleEthnic.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                            col += 2;
                            sheet_FemaleEthnic.SetCellValue(startRow, col + 4, failPupil);//P
                            sheet_FemaleEthnic.SetCellValue(startRow, col + 5, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(col + 4) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllPassPupil);
                        }
                        startRow++;

                    }//end for education

                    sheet_FemaleEthnic.CopyPasteSameRowHeigh(eRangeGroup, startRow);
                    sheet_FemaleEthnic.MergeColumn(1, startRow - countEdu, startRow);
                    sheet_FemaleEthnic.SetCellValue(startRow - countEdu, 1, lstSubject[p].DisplayName);

                    sheet_FemaleEthnic.SetCellValue(startRow, 2, "TS");
                    sheet_FemaleEthnic.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
                    col = 0;
                    if (hasMarkSubjectAll)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                            sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");//percentAllGoodPupil);
                            col += 2;
                        }
                    }
                    if (hasJudgeSubjectAll)
                    {
                        col = numberConfig * 2;
                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                        col += 2;
                        sheet_FemaleEthnic.SetCellValue(startRow, 4 + col, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + col) + startmin.ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + col) + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startRow, 5 + col, "=IF(C" + startRow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + col) + startRow + "/" + "C" + startRow + "*100,2),0)");
                    }
                    startRow++;
                }//end for listSubject
                sheet_FemaleEthnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startRow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                sheet_FemaleEthnic.SetFontName("Times New Roman", 0);
                #endregion
            }
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}