﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class ServicePackageBusiness
    {
        public const int TYPE = 30;
        /// <summary>
        /// Lay toan bo goi cuoc
        /// </summary>
        /// <returns></returns>
        public List<ServicePackageBO> GetAllPackage()
        {
            return (from s in this.All
                    where s.IS_ACTIVE == true
                    select new ServicePackageBO
                    {
                        ServicePackageID = s.SERVICE_PACKAGE_ID,
                        ServiceCode = s.SERVICE_CODE,
                        IsActive = s.IS_ACTIVE,
                    }).ToList();
        }

        public ServicePackageBO SearchDetailPackage(string ServiceCode, int year)
        {
            ServicePackageBO objDetail = new ServicePackageBO();
            List<SMS_SERVICE_PACKAGE> listService = this.All.ToList();
            List<SMS_SERVICE_PACKAGE_DETAIL> lstServcieDetail = this.ServicePackageDetailBusiness.All.ToList();
            List<ServicePackageBO> lstSMS = this.ServicePackageDetailBusiness.GetListMaxSMSSP(year);

            SMS_SERVICE_PACKAGE objService = listService.Where(p => p.SERVICE_CODE.ToUpper().Equals(ServiceCode.ToUpper())).FirstOrDefault();



            if (objService != null)
            {
                //Lay ra khai bao cua nam gan nhat
                SMS_SERVICE_PACKAGE_DECLARE lastDetail = this.ServicePackageDeclareBusiness.All
                    .Where(o => o.SERVICE_PACKAGE_ID == objService.SERVICE_PACKAGE_ID && o.STATUS && (year == 0 || o.YEAR == year)).OrderByDescending(o => o.YEAR).FirstOrDefault();

                objDetail.ServicePackageID = objService.SERVICE_PACKAGE_ID;
                objDetail.ServiceCode = objService.SERVICE_CODE;
                objDetail.Description = objService.DESCRIPTION;
                objDetail.ViettelPrice = objService.VIETTEL_PRICE;
                objDetail.OtherPrice = objService.OTHER_PRICE;
                objDetail.PriceForCustomer = objService.PRICE_FOR_CUSTOMER;
                objDetail.IsActive = objService.IS_ACTIVE;
                objDetail.InternalSharingRatio = objService.INTERNAL_SHARING_RATIO;
                objDetail.ExternalSharingRatio = objService.EXTERNAL_SHARING_RATIO;
                objDetail.IsFreePackage = objService.IS_FREE_PACKAGE;

                if (lastDetail != null)
                {
                    objDetail.Semester = lastDetail.SEMESTER;
                    objDetail.EffectiveDays = lastDetail.EFFECTIVE_DAYS;
                    objDetail.IsForOnlyExternal = lastDetail.IS_EXTERNAL;
                    objDetail.IsForOnlyInternal = lastDetail.IS_INTERNAL;
                    objDetail.IsExtraPackage = lastDetail.IS_EXTRA_PACKAGE;
                    objDetail.IsWholeYearPackage = lastDetail.IS_WHOLE_YEAR_PACKAGE;
                    objDetail.ApplyType = lastDetail.APPLY_TYPE;
                }

                SMS_SERVICE_PACKAGE_DETAIL objSerDetail = lstServcieDetail.Where(p => p.SERVICE_PACKAGE_ID == objService.SERVICE_PACKAGE_ID && p.SERVICE_PACKAGE_TYPE == TYPE).FirstOrDefault();
                if (objSerDetail != null)
                {
                    objDetail.ServicePackageDetailID = objSerDetail.SERVICE_PACKAGE_DETAIL_ID;
                    objDetail.Type = objSerDetail.SERVICE_PACKAGE_TYPE;
                    objDetail.MaxSMS = objSerDetail.MAX_SMS;
                }
                else
                {
                    objDetail.MaxSMS = lstSMS.Where(p => p.ServicePackageID == objService.SERVICE_PACKAGE_ID).FirstOrDefault().MaxSMS;
                }
            }
            return objDetail;
        }

        /// <summary>
        /// get service package by condition
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <returns></returns>
        public IQueryable<SMS_SERVICE_PACKAGE> Search(Dictionary<string, object> dic)
        {
            string serviceCode = Utils.GetString(dic, "strServiceCode").ToLowerInvariant();
            IQueryable<SMS_SERVICE_PACKAGE> servicePackage = this.All;

            if (!string.IsNullOrEmpty(serviceCode)) servicePackage = servicePackage.Where(p => p.SERVICE_CODE.ToUpper() == serviceCode.ToUpper());

            return servicePackage;
        }

        /// <summary>
        /// get all service package
        /// </summary>
        /// <returns></returns>
        public List<ServicePackageBO> GetGridServicePackage(int? AcademicYearID, string ServiceCode, int? ApplyType)
        {

            //lay ra danh sach cac goi cuoc co thoi han va goi cuoc da duoc kich hoat 
            List<ServicePackageBO> lstServicePac = (from sp in this.All
                                                    join spd in this.ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                    where sp.IS_ACTIVE == true && spd.STATUS == true
                                                    select new ServicePackageBO
                                                    {
                                                        ServicePackageID = sp.SERVICE_PACKAGE_ID,
                                                        ServicePackageDeclareDetailID = spd.SERVICE_PACKAGE_DECLARE_ID,
                                                        ServiceCode = sp.SERVICE_CODE,
                                                        Description = sp.DESCRIPTION,
                                                        Createdtime = sp.CREATED_TIME,
                                                        ViettelPrice = sp.VIETTEL_PRICE,
                                                        OtherPrice = sp.OTHER_PRICE,
                                                        PriceForCustomer = sp.PRICE_FOR_CUSTOMER,
                                                        IsActive = sp.IS_ACTIVE,
                                                        Year = spd.YEAR,
                                                        FromDate = spd.FROM_DATE,
                                                        ToDate = spd.TO_DATE,
                                                        Semester = spd.SEMESTER,
                                                        EffectiveDays = spd.EFFECTIVE_DAYS,
                                                        ApplyType = spd.APPLY_TYPE
                                                    }).ToList();


            if (AcademicYearID.HasValue)
            {
                lstServicePac = lstServicePac.Where(o => o.Year == AcademicYearID).ToList();
            }

            if (ServiceCode != "")
            {
                lstServicePac = lstServicePac.Where(o => o.ServiceCode.ToUpper().Contains(ServiceCode.ToUpper())).ToList();

            }

            if (ApplyType.HasValue && ApplyType != 0)
            {
                lstServicePac = lstServicePac.Where(o => o.ApplyType == ApplyType).ToList();
            }

            return lstServicePac;
        }

        public bool InsertServicePackageDetail(ServicePackageBO servicePackageUpdate)
        {
            SMS_SERVICE_PACKAGE currentEntity = this.All.FirstOrDefault(p => p.SERVICE_CODE.Equals(servicePackageUpdate.ServiceCode));
            // Kiem tra da co goi cuoc thi khong them nua 
            if (this.ServicePackageDeclareBusiness.All.Any(p => p.SERVICE_PACKAGE_ID == currentEntity.SERVICE_PACKAGE_ID
                    && p.YEAR == servicePackageUpdate.Year && p.STATUS == true))
            {
                return false;
            }
            else
            {
                //Neu la ap dung cho tinh thi xoa het danh sach ap dung
                if (servicePackageUpdate.ApplyType == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE)
                {
                    List<SMS_SERVICE_PACKAGE_UNIT> lstApplied = this.ServicePackageSchoolDetailBusiness.All.Where(o => o.SERVICE_PACKAGE_ID == currentEntity.SERVICE_PACKAGE_ID).ToList();
                    this.ServicePackageSchoolDetailBusiness.DeleteAll(lstApplied);
                }

                //Insert lai danh sach tinh ap dung
                SMS_SERVICE_PACKAGE_UNIT spsd;
                for (int i = 0; i < servicePackageUpdate.ListAppliedProvince.Count; i++)
                {
                    spsd = new SMS_SERVICE_PACKAGE_UNIT();
                    spsd.SERVICE_PACKAGE_ID = currentEntity.SERVICE_PACKAGE_ID;
                    spsd.PACKAGE_UNIT_TYPE = GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE;
                    spsd.UNIT_ID = servicePackageUpdate.ListAppliedProvince[i];

                    this.ServicePackageSchoolDetailBusiness.Insert(spsd);
                }

                SMS_SERVICE_PACKAGE_DECLARE declareDetail = new SMS_SERVICE_PACKAGE_DECLARE();
                declareDetail.SERVICE_PACKAGE_ID = currentEntity.SERVICE_PACKAGE_ID;
                declareDetail.YEAR = servicePackageUpdate.Year;
                declareDetail.SEMESTER = Convert.ToByte(servicePackageUpdate.Semester);
                declareDetail.FROM_DATE = servicePackageUpdate.FromDate;
                declareDetail.TO_DATE = servicePackageUpdate.ToDate;
                declareDetail.EFFECTIVE_DAYS = servicePackageUpdate.EffectiveDays;
                declareDetail.DESCRIPTION = servicePackageUpdate.ServiceCode;
                declareDetail.STATUS = true;
                declareDetail.IS_EXTERNAL = servicePackageUpdate.IsForOnlyExternal.GetValueOrDefault();
                declareDetail.IS_INTERNAL = servicePackageUpdate.IsForOnlyInternal.GetValueOrDefault();
                declareDetail.IS_WHOLE_YEAR_PACKAGE = servicePackageUpdate.IsWholeYearPackage.GetValueOrDefault();
                declareDetail.IS_EXTRA_PACKAGE = servicePackageUpdate.IsExtraPackage.GetValueOrDefault();
                declareDetail.IS_LIMIT = servicePackageUpdate.IsLimit.GetValueOrDefault();
                declareDetail.APPLY_TYPE = servicePackageUpdate.ApplyType;
                declareDetail.CREATED_TIME = DateTime.Now;
                this.ServicePackageDeclareBusiness.Insert(declareDetail);
            }

            this.Save();
            return true;
        }

        /// <summary>
        /// Them moi goi cuoc
        /// </summary>
        /// <param name="servicePacObj"></param>
        /// <returns></returns>
        public bool InsertServicePackage(ServicePackageBO servicePacObj)
        {
            if (this.All.Where(p => p.IS_ACTIVE == true && p.SERVICE_CODE.ToUpper().Equals(servicePacObj.ServiceCode.ToUpper())).Count() > 0)
            {
                return false;
            }
            else
            {
                // Them goi cuoc
                SMS_SERVICE_PACKAGE objSer = new SMS_SERVICE_PACKAGE();
                objSer.SERVICE_CODE = servicePacObj.ServiceCode;
                objSer.DESCRIPTION = servicePacObj.Description;
                objSer.VIETTEL_PRICE = (int?)servicePacObj.ViettelPrice;
                objSer.OTHER_PRICE = (int?)servicePacObj.OtherPrice;
                objSer.PRICE_FOR_CUSTOMER = (int)servicePacObj.PriceForCustomer;
                objSer.IS_ACTIVE = servicePacObj.IsActive.GetValueOrDefault();
                objSer.INTERNAL_SHARING_RATIO = servicePacObj.InternalSharingRatio;
                objSer.EXTERNAL_SHARING_RATIO = servicePacObj.ExternalSharingRatio;
                objSer.CREATED_TIME = DateTime.Now;
                objSer.IS_FREE_PACKAGE = servicePacObj.IsFreePackage.GetValueOrDefault();
                this.Insert(objSer);
                this.Save();

                // Them chi tiet
                SMS_SERVICE_PACKAGE_DETAIL objSerDetail = new SMS_SERVICE_PACKAGE_DETAIL();
                objSerDetail.SERVICE_PACKAGE_ID = objSer.SERVICE_PACKAGE_ID;
                objSerDetail.MAX_SMS = servicePacObj.MaxSMS;
                objSerDetail.SERVICE_PACKAGE_TYPE = servicePacObj.Type;
                objSerDetail.CREATED_TIME = DateTime.Now;
                this.ServicePackageDetailBusiness.Insert(objSerDetail);
                this.ServicePackageDetailBusiness.Save();
                return true;
            }
        }

        /// <summary>
        /// Cap nhat goi cuoc
        /// </summary>
        /// <param name="updatePackage"></param>
        /// <returns></returns>
        public bool UpdateServicePackage(ServicePackageBO updatePackage)
        {
            //Neu ton tai goi cuoc khac cung ServiCode thi ko cap nhat
            if (this.All.Where(p => p.SERVICE_PACKAGE_ID != updatePackage.ServicePackageID &&
                                    p.SERVICE_CODE.ToUpper().Equals(updatePackage.ServiceCode.ToUpper())).Count() > 0)
            {
                return false;
            }
            else
            {
                SMS_SERVICE_PACKAGE objSer = this.Find(updatePackage.ServicePackageID);
                if (objSer != null)
                {
                    objSer.SERVICE_CODE = updatePackage.ServiceCode;
                    objSer.DESCRIPTION = updatePackage.Description;
                    objSer.VIETTEL_PRICE = (int?)updatePackage.ViettelPrice;
                    objSer.OTHER_PRICE = (int?)updatePackage.OtherPrice;
                    objSer.PRICE_FOR_CUSTOMER = (int)updatePackage.PriceForCustomer;
                    objSer.INTERNAL_SHARING_RATIO = updatePackage.InternalSharingRatio;
                    objSer.EXTERNAL_SHARING_RATIO = updatePackage.ExternalSharingRatio;
                    objSer.IS_FREE_PACKAGE = updatePackage.IsFreePackage.GetValueOrDefault();
                    objSer.UPDATED_TIME = DateTime.Now;

                    SMS_SERVICE_PACKAGE_DETAIL objSerDetail = this.ServicePackageDetailBusiness
                                    .Find(updatePackage.ServicePackageDetailID);
                    if (objSerDetail != null)
                    {
                        objSerDetail.MAX_SMS = updatePackage.MaxSMS;
                        objSerDetail.UPDATED_TIME = DateTime.Now;
                    }
                    this.Save();
                }
                return true;
            }
        }

        /// <summary>
        /// Cap nhat Thong tin goi cuoc va Chi tiet goi cuoc
        /// </summary>
        /// <param name="servicePacObj"></param>
        /// <returns></returns>
        public bool UpdateServicePackageDetail(ServicePackageBO servicePackageUpdate)
        {
            SMS_SERVICE_PACKAGE currentServicePackage = this.Find(servicePackageUpdate.ServicePackageID);
            // Kiem tra da co goi cuoc thi khong them nua 
            if (this.ServicePackageDeclareBusiness.All.Where(p => p.YEAR == servicePackageUpdate.Year
                                                && p.SERVICE_PACKAGE_ID == servicePackageUpdate.ServicePackageID
                                                && p.SEMESTER == servicePackageUpdate.Semester
                                                && p.FROM_DATE == servicePackageUpdate.FromDate
                                                && p.TO_DATE == servicePackageUpdate.ToDate
                                                && p.SERVICE_PACKAGE_DECLARE_ID != servicePackageUpdate.ServicePackageDeclareDetailID).Count() > 0)
            {
                return false;
            }
            else
            {
                //Neu la ap dung cho tinh thi xoa het danh sach ap dung
                if (servicePackageUpdate.ApplyType == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE)
                {
                    List<SMS_SERVICE_PACKAGE_UNIT> lstApplied = this.ServicePackageSchoolDetailBusiness.All.Where(o => o.SERVICE_PACKAGE_ID == currentServicePackage.SERVICE_PACKAGE_ID).ToList();
                    this.ServicePackageSchoolDetailBusiness.DeleteAll(lstApplied);
                }

                //Insert lai danh sach tinh ap dung
                SMS_SERVICE_PACKAGE_UNIT spsd;
                for (int i = 0; i < servicePackageUpdate.ListAppliedProvince.Count; i++)
                {
                    spsd = new SMS_SERVICE_PACKAGE_UNIT();
                    spsd.SERVICE_PACKAGE_ID = currentServicePackage.SERVICE_PACKAGE_ID;
                    spsd.PACKAGE_UNIT_TYPE = GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE;
                    spsd.UNIT_ID = servicePackageUpdate.ListAppliedProvince[i];

                    this.ServicePackageSchoolDetailBusiness.Insert(spsd);
                }

                SMS_SERVICE_PACKAGE_DECLARE objDeclareDetail = this.ServicePackageDeclareBusiness
                        .Find(servicePackageUpdate.ServicePackageDeclareDetailID);
                if (objDeclareDetail != null)
                {
                    // Cap nhat chi tiet
                    objDeclareDetail.SERVICE_PACKAGE_ID = currentServicePackage.SERVICE_PACKAGE_ID;
                    objDeclareDetail.YEAR = servicePackageUpdate.Year;
                    objDeclareDetail.SEMESTER = Convert.ToByte(servicePackageUpdate.Semester);
                    objDeclareDetail.FROM_DATE = servicePackageUpdate.FromDate;
                    objDeclareDetail.TO_DATE = servicePackageUpdate.ToDate;
                    objDeclareDetail.EFFECTIVE_DAYS = servicePackageUpdate.EffectiveDays;
                    objDeclareDetail.DESCRIPTION = servicePackageUpdate.ServiceCode;
                    objDeclareDetail.IS_EXTRA_PACKAGE = servicePackageUpdate.IsExtraPackage.GetValueOrDefault();
                    objDeclareDetail.IS_LIMIT = servicePackageUpdate.IsLimit.GetValueOrDefault();
                    objDeclareDetail.IS_EXTERNAL = servicePackageUpdate.IsForOnlyExternal.GetValueOrDefault();
                    objDeclareDetail.IS_INTERNAL = servicePackageUpdate.IsForOnlyInternal.GetValueOrDefault();
                    objDeclareDetail.IS_WHOLE_YEAR_PACKAGE = servicePackageUpdate.IsWholeYearPackage.GetValueOrDefault();
                    objDeclareDetail.APPLY_TYPE = servicePackageUpdate.ApplyType;

                    objDeclareDetail.UPDATED_TIME = DateTime.Now;
                }
            }

            this.Save();
            return true;
        }

        /// <summary>
        /// Search detail service package
        /// </summary>
        /// <param name="ServicePackageID"></param>
        /// <returns></returns>
        public ServicePackageBO SearchDetail(int ServicePackageID, int? ServicePackageDeclareDetailID)
        {
            SMS_SERVICE_PACKAGE chkServicePac = this.Find(ServicePackageID);
            if (chkServicePac != null)
            {
                ServicePackageBO objServicePackage = new ServicePackageBO();
                objServicePackage.ServicePackageID = chkServicePac.SERVICE_PACKAGE_ID;
                objServicePackage.ServiceCode = chkServicePac.SERVICE_CODE;
                objServicePackage.Description = chkServicePac.DESCRIPTION;
                objServicePackage.IsActive = chkServicePac.IS_ACTIVE;
                SMS_SERVICE_PACKAGE_DECLARE chkPacDetail = null;

                chkPacDetail = this.ServicePackageDeclareBusiness.All.Where(p => p.SERVICE_PACKAGE_DECLARE_ID == ServicePackageDeclareDetailID
                    && p.STATUS == true && p.SERVICE_PACKAGE_ID == ServicePackageID).FirstOrDefault();
                if (chkPacDetail != null)
                {
                    objServicePackage.ServicePackageDeclareDetailID = chkPacDetail.SERVICE_PACKAGE_DECLARE_ID;
                    objServicePackage.Year = chkPacDetail.YEAR;
                    objServicePackage.Semester = chkPacDetail.SEMESTER;
                    objServicePackage.FromDate = chkPacDetail.FROM_DATE;
                    objServicePackage.ToDate = chkPacDetail.TO_DATE;
                    objServicePackage.EffectiveDays = chkPacDetail.EFFECTIVE_DAYS;
                    objServicePackage.Status = chkPacDetail.STATUS;
                    objServicePackage.ApplyType = chkPacDetail.APPLY_TYPE.GetValueOrDefault();
                    objServicePackage.IsWholeYearPackage = chkPacDetail.IS_WHOLE_YEAR_PACKAGE;
                    objServicePackage.IsForOnlyExternal = chkPacDetail.IS_EXTERNAL;
                    objServicePackage.IsForOnlyInternal = chkPacDetail.IS_INTERNAL;
                    objServicePackage.IsExtraPackage = chkPacDetail.IS_EXTRA_PACKAGE;
                }

                return objServicePackage;
            }

            return new ServicePackageBO();
        }

        /// <summary>
        /// Change IsActive ServicePackage
        /// </summary>
        /// <param name="ServicePackageID"></param>
        /// <returns></returns>
        public bool DeactiveServicePackage(int ServicePackageID, int? ServicePackageDeclareDetailID)
        {
            // Neu dang co HD thi khong huy dc
            if (this.SMSParentContractDetailBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == ServicePackageID).Count() > 0)
            {
                return false;
            }

            SMS_SERVICE_PACKAGE objServicePackage = this.Find(ServicePackageID);
            
            if (ServicePackageDeclareDetailID.HasValue)
            {
                SMS_SERVICE_PACKAGE_DECLARE objDeclareDetail = this.ServicePackageDeclareBusiness.Find(ServicePackageDeclareDetailID);
                if (objDeclareDetail != null)
                {
                    objDeclareDetail.STATUS = false;
                    objDeclareDetail.UPDATED_TIME = DateTime.Now;
                }
            }

            this.Save();
            return true;

        }

        /// <summary>
        /// get service package by packageCode
        /// </summary>
        /// <param name="dic">strServiceCode: ma goi cuoc</param>
        /// <returns></returns>
        public int GetServicePackageIDByServiceCode(string serviceCode)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["strServiceCode"] = serviceCode;
            SMS_SERVICE_PACKAGE servicePackage = Search(dic).FirstOrDefault();
            if (servicePackage != null) return servicePackage.SERVICE_PACKAGE_ID;
            return -1;
        }


        /// <summary>
        /// Lay danh sach goi cuoc truong duoc ap dung
        /// </summary>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <param name="academicYearBO"></param>
        /// <param name="provinceID"></param>
        /// <returns></returns>
        public List<ServicePackageBO> GetListServicesPackage(int year, int semester, AcademicYear academicYearBO, int provinceID, int schoolID, bool? isExtraPackage = null)
        {
            List<ServicePackageBO> listResult = new List<ServicePackageBO>();
            List<ServicePackageBO> listServicePackage = (from l in this.ListServicesPackage(year, semester, academicYearBO, provinceID, schoolID)
                                                         join d in this.ServicePackageDeclareBusiness.All on l.ServicePackageID equals d.SERVICE_PACKAGE_ID
                                                         where d.YEAR == year && (!isExtraPackage.HasValue
                                                         || (isExtraPackage == true && d.IS_EXTRA_PACKAGE == true)
                                                         || (isExtraPackage == false && (d.IS_EXTRA_PACKAGE == null || d.IS_EXTRA_PACKAGE == false)))
                                                         //group l by new { l.ServicePackageID, l.ServiceCode, l.Description, l.ViettelPrice, l.OtherPrice, l.IsAllowOnlyInternal, l.IsAllowRegisterSideSubscriber } into g
                                                         select new ServicePackageBO
                                                         {
                                                             ServicePackageID = l.ServicePackageID,
                                                             ServiceCode = l.ServiceCode,
                                                             Description = l.Description,
                                                             ViettelPrice = l.ViettelPrice,
                                                             OtherPrice = l.OtherPrice,
                                                             IsForOnlyInternal = d.IS_INTERNAL,
                                                             IsForOnlyExternal = d.IS_EXTERNAL,
                                                             IsWholeYearPackage = d.IS_WHOLE_YEAR_PACKAGE,
                                                             IsExtraPackage = d.IS_EXTRA_PACKAGE
                                                         }).Distinct().OrderBy(p => p.ServicePackageID).ToList();

            //Them so tin nhan cho tung goi cuoc
            ServicePackageBO obj = null;
            ServicePackageBO result = null;
            for (int i = 0; i < listServicePackage.Count; i++)
            {
                obj = listServicePackage[i];
                result = new ServicePackageBO();
                result.ServicePackageID = obj.ServicePackageID;
                result.Description = obj.Description;
                result.ViettelPrice = obj.ViettelPrice;
                result.OtherPrice = obj.OtherPrice;
                result.ServiceCode = obj.ServiceCode;
                result.IsForOnlyInternal = obj.IsForOnlyInternal;
                result.IsForOnlyExternal = obj.IsForOnlyExternal;
                result.IsExtraPackage = obj.IsExtraPackage;
                result.IsWholeYearPackage = obj.IsWholeYearPackage;
                result.MaxSMS = this.ServicePackageDetailBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == obj.ServicePackageID).Sum(p => p.MAX_SMS);
                listResult.Add(result);
            }
            return listResult;
        }

        private List<ServicePackageBO> ListServicesPackage(int year, int semester, AcademicYear academicYearBO, int provinceID = 0, int schoolID = 0)
        {
            DateTime datenow = DateTime.Now;
            List<ServicePackageBO> lstResult = (from sv in this.All
                                                join svdt in ServicePackageDeclareBusiness.All on sv.SERVICE_PACKAGE_ID equals svdt.SERVICE_PACKAGE_ID
                                                join spsd in ServicePackageSchoolDetailBusiness.All on sv.SERVICE_PACKAGE_ID equals spsd.SERVICE_PACKAGE_ID
                                                where sv.IS_ACTIVE == true
                                                && (
                                                       (
                                                           svdt.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL
                                                           && EntityFunctions.TruncateTime(datenow) >= EntityFunctions.TruncateTime(academicYearBO.FirstSemesterStartDate)
                                                           && EntityFunctions.TruncateTime(datenow) <= EntityFunctions.TruncateTime(academicYearBO.SecondSemesterEndDate)
                                                           && svdt.STATUS == true && svdt.YEAR == year && svdt.FROM_DATE <= datenow && svdt.TO_DATE >= datenow
                                                       )
                                                   ||
                                                       (
                                                            (
                                                                (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && EntityFunctions.TruncateTime(datenow) >= EntityFunctions.TruncateTime(academicYearBO.FirstSemesterStartDate) && EntityFunctions.TruncateTime(datenow) <= EntityFunctions.TruncateTime(academicYearBO.FirstSemesterEndDate))
                                                                ||
                                                                (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && EntityFunctions.TruncateTime(datenow) >= EntityFunctions.TruncateTime(academicYearBO.SecondSemesterStartDate) && EntityFunctions.TruncateTime(datenow) <= EntityFunctions.TruncateTime(academicYearBO.SecondSemesterEndDate))
                                                            )
                                                            && svdt.SEMESTER == semester && svdt.STATUS == true && svdt.YEAR == year
                                                            && EntityFunctions.TruncateTime(svdt.FROM_DATE) <= EntityFunctions.TruncateTime(datenow) && EntityFunctions.TruncateTime(svdt.TO_DATE) >= EntityFunctions.TruncateTime(datenow)
                                                        )
                                                    )
                                                   && ((
                                                       provinceID == 0 || (spsd.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE && spsd.UNIT_ID == provinceID)
                                                   )

                                                   || (
                                                       schoolID == 0 || (spsd.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL && spsd.UNIT_ID == schoolID)
                                                   ))
                                                select new ServicePackageBO
                                                {
                                                    ServicePackageID = sv.SERVICE_PACKAGE_ID,
                                                    ServiceCode = sv.SERVICE_CODE,
                                                    Description = sv.DESCRIPTION,
                                                    ViettelPrice = sv.VIETTEL_PRICE,
                                                    OtherPrice = sv.OTHER_PRICE,
                                                    IsForOnlyInternal = svdt.IS_INTERNAL,
                                                    IsForOnlyExternal = svdt.IS_EXTERNAL,
                                                    IsWholeYearPackage = svdt.IS_WHOLE_YEAR_PACKAGE
                                                }).ToList();
            return lstResult;
        }

        /// <summary>
        /// Lay tat ca goi cuoc
        /// </summary>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <param name="academicYearBO"></param>
        /// <returns></returns>
        public List<ServicePackageBO> GetListServicesPackageFull(int year)
        {
            return (from s in this.ServicePackageBusiness.All
                     join sp in this.ServicePackageDetailBusiness.All on s.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                    join spd in this.ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                    where spd.YEAR == year
                    group sp by new
                    {
                        sp.SERVICE_PACKAGE_ID,
                        s.SERVICE_CODE,
                        s.DESCRIPTION,
                        s.VIETTEL_PRICE,
                        s.OTHER_PRICE,
                        spd.IS_WHOLE_YEAR_PACKAGE,
                        s.IS_FREE_PACKAGE
                    } into g
                    select new ServicePackageBO
                    {
                        ServicePackageID = g.Key.SERVICE_PACKAGE_ID,
                        ServiceCode = g.Key.SERVICE_CODE,
                        Description = g.Key.DESCRIPTION,
                        ViettelPrice = g.Key.VIETTEL_PRICE,
                        OtherPrice = g.Key.OTHER_PRICE,
                        IsWholeYearPackage = g.Key.IS_WHOLE_YEAR_PACKAGE,
                        IsFreePackage = g.Key.IS_FREE_PACKAGE,
                        MaxSMS = g.Sum(p => p.MAX_SMS)
                    }).ToList();
        }

        /// <summary>
        ///Viethd31: Lay danh sach goi cuoc mua them truong duoc ap dung
        /// </summary>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <param name="academicYearBO"></param>
        /// <param name="provinceID"></param>
        /// <returns></returns>
        public List<ServicePackageBO> GetListExtraServicesPackage(int year, int semester, AcademicYear academicYearBO, int provinceID, int schoolID)
        {
            List<ServicePackageBO> listResult = new List<ServicePackageBO>();
            List<ServicePackageBO> listServicePackage = (from l in this.ListServicesPackage(year, semester, academicYearBO, provinceID, schoolID)
                                                         join d in this.ServicePackageDeclareBusiness.All on l.ServicePackageID equals d.SERVICE_PACKAGE_ID
                                                         where d.IS_EXTRA_PACKAGE == true
                                                         && d.YEAR == year
                                                         select new ServicePackageBO
                                                         {
                                                             ServicePackageID = l.ServicePackageID,
                                                             ServiceCode = l.ServiceCode,
                                                             Description = l.Description,
                                                             ViettelPrice = l.ViettelPrice,
                                                             OtherPrice = l.OtherPrice,
                                                             IsForOnlyInternal = d.IS_INTERNAL,
                                                             IsForOnlyExternal = d.IS_EXTERNAL,
                                                             IsWholeYearPackage = d.IS_WHOLE_YEAR_PACKAGE
                                                         }).Distinct().OrderBy(p => p.ServicePackageID).ToList();

            //Them so tin nhan cho tung goi cuoc
            ServicePackageBO obj = null;
            ServicePackageBO result = null;
            for (int i = 0; i < listServicePackage.Count; i++)
            {
                obj = listServicePackage[i];
                result = new ServicePackageBO();
                result.ServicePackageID = obj.ServicePackageID;
                result.Description = obj.Description;
                result.ViettelPrice = obj.ViettelPrice;
                result.OtherPrice = obj.OtherPrice;
                result.ServiceCode = obj.ServiceCode;
                result.IsForOnlyInternal = obj.IsForOnlyInternal;
                result.IsForOnlyExternal = obj.IsForOnlyExternal;
                result.IsWholeYearPackage = obj.IsWholeYearPackage;
                result.MaxSMS = this.ServicePackageDetailBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == obj.ServicePackageID).Sum(p => p.MAX_SMS);
                listResult.Add(result);
            }
            return listResult;
        }

        public MoneyMaxSMSBO GetSMSByServicesPackageID(int servicePackageID, int year, int semester, AcademicYear academicYearBO)
        {
            int? MaxSMS = 0;
            MoneyMaxSMSBO maxSMSOBj = new MoneyMaxSMSBO();
            List<SMS_SERVICE_PACKAGE_DETAIL> servicesDetailList = this.ServicePackageDetailBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == servicePackageID).ToList();
            if (servicesDetailList.Count > 0)
            {
                MaxSMS = servicesDetailList.Sum(p => p.MAX_SMS);
            }
            List<ServicePackageBO> servicesPackageList = this.ListServicesPackage(year, semester, academicYearBO).Where(p => p.ServicePackageID == servicePackageID).ToList();
            List<MoneyMaxSMSBO> moneyMaxSMSBOObj = (from l in servicesPackageList
                                                    select new MoneyMaxSMSBO
                                                    {
                                                        MoneyViettel = l.ViettelPrice,
                                                        MoneyOther = l.OtherPrice
                                                    }).ToList();

            maxSMSOBj.MaxSMS = MaxSMS;
            maxSMSOBj.MoneyViettel = moneyMaxSMSBOObj.Select(p => p.MoneyViettel).FirstOrDefault();
            maxSMSOBj.MoneyOther = moneyMaxSMSBOObj.Select(p => p.MoneyOther).FirstOrDefault();
            return maxSMSOBj;
        }

        /// <summary>
        /// Lay goi cuoc dung cho edit so dien thoai nhan hop dong
        /// </summary>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <param name="academicYearBO"></param>
        /// <param name="servicePackageIDEdit"></param>
        /// <param name="provinceID"></param>
        /// <returns></returns>
        public List<ServicePackageBO> GetListServicesPackageEdit(int year, int semester, AcademicYear academicYearBO, int servicePackageIDEdit, int provinceID, int schoolID = 0)
        {
            List<ServicePackageBO> listResult = new List<ServicePackageBO>();
            List<ServicePackageBO> listServicePackage = (from l in this.ListServicesPackageEdit(year, semester, academicYearBO, servicePackageIDEdit, provinceID, schoolID)
                                                         join d in this.ServicePackageDeclareBusiness.All on l.ServicePackageID equals d.SERVICE_PACKAGE_ID

                                                         select new ServicePackageBO
                                                         {
                                                             ServicePackageID = l.ServicePackageID,
                                                             ServiceCode = l.ServiceCode,
                                                             Description = l.Description,
                                                             ViettelPrice = l.ViettelPrice,
                                                             OtherPrice = l.OtherPrice,
                                                             IsForOnlyInternal = d.IS_INTERNAL,
                                                             IsForOnlyExternal = d.IS_EXTERNAL,
                                                             IsWholeYearPackage = d.IS_WHOLE_YEAR_PACKAGE
                                                         }).Distinct().OrderBy(p => p.ServicePackageID).ToList();

            //Them so tin nhan cho tung goi cuoc
            ServicePackageBO obj = null;
            ServicePackageBO result = null;
            for (int i = 0; i < listServicePackage.Count; i++)
            {
                obj = listServicePackage[i];
                result = new ServicePackageBO();
                result.ServicePackageID = obj.ServicePackageID;
                result.Description = obj.Description;
                result.ViettelPrice = obj.ViettelPrice;
                result.OtherPrice = obj.OtherPrice;
                result.ServiceCode = obj.ServiceCode;
                result.IsForOnlyInternal = obj.IsForOnlyInternal;
                result.IsForOnlyExternal = obj.IsForOnlyExternal;
                result.IsWholeYearPackage = obj.IsWholeYearPackage;
                result.MaxSMS = this.ServicePackageDetailBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == obj.ServicePackageID).Sum(p => p.MAX_SMS);
                listResult.Add(result);
            }
            return listResult;
        }

        /// <summary>
        /// Danh sach goi cuoc truong duoc quyen ap dung
        /// Bo sung cau hinh goi cuoc ap dung theo Tinh thanh
        /// </summary>
        /// <modifier date="12/8/2015">AnhVD9</modifier>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <param name="academicYearBO"></param>
        /// <param name="servicePackageID"></param>
        /// <param name="provinceID"></param>
        /// <returns></returns>
        private List<ServicePackageBO> ListServicesPackageEdit(int year, int semester, AcademicYear academicYearBO, int servicePackageID, int provinceID = 0, int schoolID = 0)
        {
            DateTime datenow = DateTime.Now;
            return (from sv in this.ServicePackageBusiness.All
                    join x in this.ServicePackageDeclareBusiness.All on sv.SERVICE_PACKAGE_ID equals x.SERVICE_PACKAGE_ID 
                    join t in this.ServicePackageSchoolDetailBusiness.All on sv.SERVICE_PACKAGE_ID equals t.SERVICE_PACKAGE_ID
                    where sv.IS_ACTIVE == true
                    && (sv.SERVICE_PACKAGE_ID == servicePackageID
                    || (
                        x.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL && (EntityFunctions.TruncateTime(datenow) >= EntityFunctions.TruncateTime(academicYearBO.FirstSemesterStartDate) && EntityFunctions.TruncateTime(datenow) <= EntityFunctions.TruncateTime(academicYearBO.SecondSemesterEndDate))
                        &&
                        x.STATUS == true && x.YEAR == year
                        &&
                        EntityFunctions.TruncateTime(x.FROM_DATE) <= EntityFunctions.TruncateTime(datenow) && EntityFunctions.TruncateTime(x.TO_DATE) >= EntityFunctions.TruncateTime(datenow)
                        )
                    || (
                            (
                                (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && (EntityFunctions.TruncateTime(datenow) >= EntityFunctions.TruncateTime(academicYearBO.FirstSemesterStartDate) && EntityFunctions.TruncateTime(datenow) <= EntityFunctions.TruncateTime(academicYearBO.FirstSemesterEndDate)))
                                ||
                                (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && (EntityFunctions.TruncateTime(datenow) >= EntityFunctions.TruncateTime(academicYearBO.SecondSemesterStartDate) && EntityFunctions.TruncateTime(datenow) <= EntityFunctions.TruncateTime(academicYearBO.SecondSemesterEndDate)))
                            )
                            && x.STATUS == true && x.YEAR == year && x.SEMESTER == semester
                            && EntityFunctions.TruncateTime(x.FROM_DATE) <= EntityFunctions.TruncateTime(datenow)
                            && EntityFunctions.TruncateTime(x.TO_DATE) >= EntityFunctions.TruncateTime(datenow)

                        )
                    ) && (
                        (provinceID == 0 || (t.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE && t.UNIT_ID == provinceID)) || sv.SERVICE_PACKAGE_ID == servicePackageID // Bổ sung gói đã ko còn áp dụng
                    )
                    && (
                        (schoolID == 0 || (t.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL && t.UNIT_ID == schoolID)) || sv.SERVICE_PACKAGE_ID == servicePackageID // Bổ sung gói đã ko còn áp dụng
                    )
                    select new ServicePackageBO
                    {
                        ServicePackageID = sv.SERVICE_PACKAGE_ID,
                        ServiceCode = sv.SERVICE_CODE,
                        Description = sv.DESCRIPTION,
                        ViettelPrice = sv.VIETTEL_PRICE,
                        OtherPrice = sv.OTHER_PRICE,
                        IsForOnlyInternal = x.IS_INTERNAL,
                        IsForOnlyExternal = x.IS_EXTERNAL,
                        IsWholeYearPackage = x.IS_WHOLE_YEAR_PACKAGE
                    }).ToList();
        }

        /// <summary>
        /// Ham su dung cho chuc nang quan ly hop dong, neu chon sua goi cuoc da het han su dung thi van hien cac thong tin dung
        /// </summary>
        /// <param name="servicePackageID"></param>
        /// <param name="academicYearBO"></param>
        /// <returns></returns>
        public MoneyMaxSMSBO GetSMSByServicesPackageIDEdit(int servicePackageID, AcademicYear academicYearBO)
        {
            int? MaxSMS = 0;
            MoneyMaxSMSBO maxSMSOBj = new MoneyMaxSMSBO();
            List<SMS_SERVICE_PACKAGE_DETAIL> servicesDetailList = this.ServicePackageDetailBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == servicePackageID).ToList();
            if (servicesDetailList.Count > 0)
            {
                MaxSMS = servicesDetailList.Sum(p => p.MAX_SMS);
            }
            List<MoneyMaxSMSBO> moneyMaxSMSBOObj = (from l in this.ServicePackageBusiness.All
                                                    where l.SERVICE_PACKAGE_ID == servicePackageID
                                                    select new MoneyMaxSMSBO
                                                    {
                                                        MoneyViettel = l.VIETTEL_PRICE,
                                                        MoneyOther = l.OTHER_PRICE
                                                    }).ToList();

            maxSMSOBj.MaxSMS = MaxSMS;
            maxSMSOBj.MoneyViettel = moneyMaxSMSBOObj.Select(p => p.MoneyViettel).FirstOrDefault();
            maxSMSOBj.MoneyOther = moneyMaxSMSBOObj.Select(p => p.MoneyOther).FirstOrDefault();
            return maxSMSOBj;
        }

        public bool SchooolHasService(int schoolId, int provinceId, string serviceCode)
        {
            return (from sv in this.All
                    join scv in this.ServicePackageSchoolDetailBusiness.All on sv.SERVICE_PACKAGE_ID equals scv.SERVICE_PACKAGE_ID
                    where sv.SERVICE_CODE == serviceCode && ((scv.UNIT_ID == schoolId && scv.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL)
                    || scv.UNIT_ID == provinceId && scv.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE)
                    select sv.SERVICE_PACKAGE_ID).Any();
        }
    
    }
}
