﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Text;
using Newtonsoft.Json;

namespace SMAS.Business.Business
{
    public partial class TeacherNoteBookMonthBusiness
    {
        private string strNoteMonth = "Nhận xét môn học: ";
        private string strNoteCQ = "Biểu hiện: ";
        public IQueryable<TeacherNoteBookMonth> Search(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int monthID = Utils.GetInt(dic, "MonthID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstMonthID = Utils.GetIntList(dic, "lstMonthID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            IQueryable<TeacherNoteBookMonth> iqTeacherNoteBookMonth = TeacherNoteBookMonthBusiness.All;
            if (schoolID > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => p.SchoolID == schoolID);
            }
            if (academicYearID > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => p.AcademicYearID == academicYearID);
            }
            if (classID > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => p.ClassID == classID);
            }
            if (lstClassID.Count > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (pupilID > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => p.PupilID == pupilID);
            }
            if (lstPupilID.Count > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (subjectID > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => p.SubjectID == subjectID);
            }
            if (monthID > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => p.MonthID == monthID);
            }
            if (lstMonthID.Count > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => lstMonthID.Contains(p.MonthID));
            }
            if (lstSubjectID.Count > 0)
            {
                iqTeacherNoteBookMonth = iqTeacherNoteBookMonth.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            return iqTeacherNoteBookMonth;
        }

        public List<TeacherNoteBookMonthBO> GetListTeacherNoteBookMonth(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int monthID = Utils.GetInt(dic, "MonthID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            List<TeacherNoteBookMonthBO> lstTeacherNoteBookMonth = new List<TeacherNoteBookMonthBO>();
            lstTeacherNoteBookMonth = (from t in TeacherNoteBookMonthBusiness.All
                                       join pf in PupilProfileBusiness.All on t.PupilID equals pf.PupilProfileID
                                       join poc in PupilOfClassBusiness.All on t.PupilID equals poc.PupilID
                                       join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                       where t.SchoolID == schoolID
                                       && (t.AcademicYearID == academicYearID || academicYearID == 0)
                                       && (t.PupilID == pupilID || pupilID == 0)
                                       && (t.ClassID == classID || classID == 0)
                                       && (poc.ClassID == classID || classID == 0)
                                       && t.PartitionID == (schoolID % 100)
                                       && t.MonthID == monthID
                                       && (t.SubjectID == subjectID || subjectID == 0)
                                       orderby poc.OrderInClass
                                       select new TeacherNoteBookMonthBO
                                       {
                                           PupilID = t.PupilID,
                                           FullName = pf.FullName,
                                           SubjectName = sc.SubjectName,
                                           SchoolID = t.SchoolID,
                                           AcademicYearID = t.AcademicYearID,
                                           ClassID = poc.ClassID,
                                           CommentSubject = t.CommentSubject,
                                           CommentCQ = t.CommentCQ,
                                           MonthID = t.MonthID,
                                           SubjectID = t.SubjectID
                                       }).ToList();
            return lstTeacherNoteBookMonth;
        }

        public void InsertOrUpdate(List<TeacherNoteBookMonth> lstTeacherNoteBookMonth, 
            IDictionary<string, object> dic, 
            List<TeacherNoteBookMonth> lstTeacherNoteBookMonthDB,
            ref List<ActionAuditDataBO> lstActionAuditData)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int monthID = Utils.GetInt(dic, "MonthID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            string monthName = string.Empty;
            if (monthID != 15 && monthID != 16)
            {
                int tmp = Int32.Parse(monthID.ToString().Substring(4, monthID.ToString().Length - 4));
                monthName = "Tháng " + monthID.ToString().Substring(4, monthID.ToString().Length - 4);
            }
            else
            {
                monthName = "Đánh giá cuối kỳ";
            }

            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
           // List<TeacherNoteBookMonth> lstTeacherNoteBookMonthDB = this.Search(dic).ToList();
            TeacherNoteBookMonth objTeacherNoteBookMonthDB = null;
            List<TeacherNoteBookMonth> lstUpdate = new List<TeacherNoteBookMonth>();
            List<TeacherNoteBookMonth> lstInsert = new List<TeacherNoteBookMonth>();
            TeacherNoteBookMonth objTeacherNoteBookMonth = null;
            int partitionID = (schoolID % 100);

            #region ghi log du lieu
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            ActionAuditDataBO objAc = null;
            List<int> lstPupilID = lstTeacherNoteBookMonth.Select(p => p.PupilID).Distinct().ToList();
            int pupilID = 0;
            bool isChange = false;

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == academicYearID
                                           && poc.SchoolID == schoolID
                                           && poc.ClassID == classID
                                           && lstPupilID.Contains(poc.PupilID)
                                           && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();

            SubjectCat objSC = SubjectCatBusiness.Find(subjectID);
            #endregion

            //lay danh sach mien giam
            List<ExemptedSubject> lstExemp = ExemptedSubjectBusiness.Search(dic).ToList();
            for (int i = 0; i < lstTeacherNoteBookMonth.Count; i++)
            {
                isChange = false;
                objTeacherNoteBookMonth = lstTeacherNoteBookMonth[i];
                if (lstExemp.Any(p => p.PupilID == objTeacherNoteBookMonth.PupilID && p.SubjectID == objTeacherNoteBookMonth.SubjectID))
                {
                    continue;
                }
                pupilID = objTeacherNoteBookMonth.PupilID;
                objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                objTeacherNoteBookMonthDB = lstTeacherNoteBookMonthDB.Where(p => p.PupilID == objTeacherNoteBookMonth.PupilID && p.SubjectID == objTeacherNoteBookMonth.SubjectID).FirstOrDefault();

                objectIDStrtmp.Append(pupilID);
                descriptionStrtmp.Append("Cập nhật đánh giá sổ tay GV");
                paramsStrtmp.Append(pupilID);
                userFuntionsStrtmp.Append("Sổ tay giáo viên");
                userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                userDescriptionsStrtmp.Append("Cập nhật đánh giá HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + monthName + "/Học kỳ " + semesterID + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");
                if (objTeacherNoteBookMonthDB != null)
                {
                    if (objTeacherNoteBookMonthDB.CommentSubject != objTeacherNoteBookMonth.CommentSubject)
                    {
                        newObjectStrtmp.Append(strNoteMonth + objTeacherNoteBookMonth.CommentSubject).Append("; ");
                        oldObjectStrtmp.Append(strNoteMonth + objTeacherNoteBookMonthDB.CommentSubject).Append("; ");
                        objTeacherNoteBookMonthDB.CommentSubject = objTeacherNoteBookMonth.CommentSubject;                    
                        isChange = true;
                    }

                    if (objTeacherNoteBookMonthDB.CommentCQ != objTeacherNoteBookMonth.CommentCQ)
                    {
                        newObjectStrtmp.Append(strNoteCQ + objTeacherNoteBookMonth.CommentCQ).Append("; ");
                        oldObjectStrtmp.Append(strNoteCQ + objTeacherNoteBookMonthDB.CommentCQ).Append("; ");
                        objTeacherNoteBookMonthDB.CommentCQ = objTeacherNoteBookMonth.CommentCQ;                 
                        isChange = true;
                    }

                    if (isChange)
                    {
                        objTeacherNoteBookMonthDB.UpdateTime = DateTime.Now.Date;
                        lstUpdate.Add(objTeacherNoteBookMonthDB);
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(objTeacherNoteBookMonth.CommentSubject))
                    {
                        newObjectStrtmp.Append(strNoteMonth + objTeacherNoteBookMonth.CommentSubject).Append("; ");
                        oldObjectStrtmp.Append(strNoteMonth).Append("; ");
                        isChange = true;
                    }

                    if (!string.IsNullOrEmpty(objTeacherNoteBookMonth.CommentCQ))
                    {
                        newObjectStrtmp.Append(strNoteCQ + objTeacherNoteBookMonth.CommentCQ).Append("; ");
                        oldObjectStrtmp.Append(strNoteCQ).Append("; ");
                        isChange = true;
                    }
                    if (isChange)
                    {
                        objTeacherNoteBookMonth.CreateTime = DateTime.Now.Date;
                        objTeacherNoteBookMonth.PartitionID = partitionID;
                        lstInsert.Add(objTeacherNoteBookMonth);
                    }
                }

                objAc = new ActionAuditDataBO();
                objAc.PupilID = pupilID;
                objAc.ClassID = classID;
                objAc.SubjectID = subjectID;
                objAc.ObjID = objectIDStrtmp;
                objAc.Parameter = paramsStrtmp;
                objAc.OldData = oldObjectStrtmp;
                objAc.NewData = newObjectStrtmp;
                objAc.UserAction = userActionsStrtmp;
                objAc.UserFunction = userFuntionsStrtmp;
                objAc.UserDescription = userDescriptionsStrtmp;
                objAc.Description = descriptionStrtmp;
                objAc.isInsertLog = isChange;
                lstActionAuditData.Add(objAc);


                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }

            if (lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
                this.Save();
            }

            if (lstInsert.Count > 0)
            {
                this.BulkInsert(lstInsert, TeacherNoteBookMonthMaping(), "TeacherNoteBookMonthID");
            }

        }

        // InsertOrUpdateForImportExcel CHUC NANG SO TAY GIAO VIEN
        public void InsertOrUpdateForImport(List<TeacherNoteBookMonth> lstTeacherNoteBookMonth, IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            List<TeacherNoteBookMonth> lstTeacherNoteBookMonthDB = this.Search(dic).ToList();
            TeacherNoteBookMonth objTeacherNoteBookMonthDB = null;
            List<TeacherNoteBookMonth> lstUpdate = new List<TeacherNoteBookMonth>();
            List<TeacherNoteBookMonth> lstInsert = new List<TeacherNoteBookMonth>();
            TeacherNoteBookMonth objTeacherNoteBookMonth = null;
            int partitionID = (schoolID % 100);
            //lay danh sach mien giam
            List<ExemptedSubject> lstExemp = ExemptedSubjectBusiness.Search(dic).ToList();

            for (int i = 0; i < lstTeacherNoteBookMonth.Count; i++)
            {
                objTeacherNoteBookMonth = lstTeacherNoteBookMonth[i];
                if (lstExemp.Any(p => p.PupilID == objTeacherNoteBookMonth.PupilID && p.SubjectID == objTeacherNoteBookMonth.SubjectID))
                {
                    continue;
                }
                objTeacherNoteBookMonthDB = lstTeacherNoteBookMonthDB.Where(p => p.PupilID == objTeacherNoteBookMonth.PupilID
                && p.SubjectID == objTeacherNoteBookMonth.SubjectID && p.MonthID == objTeacherNoteBookMonth.MonthID).FirstOrDefault();
                if (objTeacherNoteBookMonthDB != null)
                {
                    objTeacherNoteBookMonthDB.UpdateTime = DateTime.Now.Date;
                    objTeacherNoteBookMonthDB.CommentSubject = objTeacherNoteBookMonth.CommentSubject;
                    objTeacherNoteBookMonthDB.CommentCQ = objTeacherNoteBookMonth.CommentCQ;
                    lstUpdate.Add(objTeacherNoteBookMonthDB);
                }
                else
                {
                    objTeacherNoteBookMonth.CreateTime = DateTime.Now.Date;
                    objTeacherNoteBookMonth.PartitionID = partitionID;
                    lstInsert.Add(objTeacherNoteBookMonth);
                }
            }
            if (lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
                this.Save();
            }

            if (lstInsert.Count > 0)
            {
                this.BulkInsert(lstInsert, TeacherNoteBookMonthMaping(), "TeacherNoteBookMonthID");
            }
        }

        public void DeleteTeacherNoteBookMonth(IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAuditData, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail)
        {
            List<int> lstpupilID = Utils.GetIntList(dic, "lstPupilID");
            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int monthID = Utils.GetInt(dic, "MonthID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            bool lockComment = Utils.GetBool(dic, "lockComment");

            RESTORE_DATA objRes = Utils.GetObject<RESTORE_DATA>(dic, "RESTORE_DATA");
            string monthName = string.Empty;
            if (monthID != 15 && monthID != 16)
            {
                int tmp = Int32.Parse(monthID.ToString().Substring(4, monthID.ToString().Length - 4));
                monthName = "Tháng " + monthID.ToString().Substring(4, monthID.ToString().Length - 4);
            }
            else
            {
                monthName = "Đánh giá cuối kỳ";
            }
            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
            List<TeacherNoteBookMonth> lstDelete = new List<TeacherNoteBookMonth>();
            TeacherNoteBookMonth objTeacherNoteBookMonth = null;
            lstDelete = this.Search(dic).ToList();
            #region ghi log du lieu
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder restoreDescription = new StringBuilder();
            ActionAuditDataBO objAc = null;
            int pupilID = 0;
            bool isChange = false;            
            SubjectCat objSC = SubjectCatBusiness.Find(subjectID);

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == academicYearID
                                           && poc.SchoolID == schoolID
                                           && poc.ClassID == classID
                                           && lstpupilID.Contains(poc.PupilID)
                                           && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();


            RESTORE_DATA_DETAIL objRestoreDetail;
            #endregion
            for (int i = 0; i < lstDelete.Count; i++)
            {
                isChange = false;
                objTeacherNoteBookMonth = lstDelete[i];

                #region Luu thong tin phuc vu phuc hoi du lieu
                objRestoreDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    IS_VALIDATE = 1,
                    LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(objRes.SCHOOL_ID),
                    ORDER_ID = 1,
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = objRes.SCHOOL_ID,
                    SQL_DELETE = "",
                    SQL_UNDO = JsonConvert.SerializeObject(objTeacherNoteBookMonth),
                    TABLE_NAME = RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_MONTH
                };

                lstRestoreDetail.Add(objRestoreDetail);
                #endregion

                pupilID = objTeacherNoteBookMonth.PupilID;
                objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                objectIDStrtmp.Append(pupilID);
                descriptionStrtmp.Append("Xóa đánh giá sổ tay GV");
                paramsStrtmp.Append(pupilID);
                userFuntionsStrtmp.Append("Sổ tay giáo viên");
                userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                userDescriptionsStrtmp.Append("Xóa đánh giá HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + monthName + "/Học kỳ " + semesterID + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");
                restoreDescription.Append(string.Format(" HS:{0}, mã: {1}, ", objPOC.PupilFullName, objPOC.PupilCode));

                if (!string.IsNullOrEmpty(objTeacherNoteBookMonth.CommentSubject))
                {
                    oldObjectStrtmp.Append(strNoteMonth).Append(objTeacherNoteBookMonth.CommentSubject).Append("; ");
                    objTeacherNoteBookMonth.CommentSubject = "";
                    isChange = true;
                }

                if (!string.IsNullOrEmpty(objTeacherNoteBookMonth.CommentCQ))
                {
                    oldObjectStrtmp.Append(strNoteCQ).Append(objTeacherNoteBookMonth.CommentCQ).Append("; ");
                    objTeacherNoteBookMonth.CommentCQ = "";
                    isChange = true;
                }

                if (isChange)
                {
                    objTeacherNoteBookMonth.UpdateTime = DateTime.Now.Date;
                    this.Update(objTeacherNoteBookMonth);
                }

                objAc = new ActionAuditDataBO();
                objAc.PupilID = pupilID;
                objAc.ClassID = classID;
                objAc.SubjectID = subjectID;
                objAc.ObjID = objectIDStrtmp;
                objAc.Parameter = paramsStrtmp;
                objAc.OldData = oldObjectStrtmp;
                objAc.NewData = newObjectStrtmp;
                objAc.UserAction = userActionsStrtmp;
                objAc.UserFunction = userFuntionsStrtmp;
                objAc.UserDescription = userDescriptionsStrtmp;
                objAc.Description = descriptionStrtmp;
                objAc.isInsertLog = isChange;
                objAc.RestoreMsgDescription = restoreDescription.ToString();
                lstActionAuditData.Add(objAc);


                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
                restoreDescription = new StringBuilder();


            }
            this.Save();
        }

        private IDictionary<string, object> TeacherNoteBookMonthMaping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MonthID", "MONTH_ID");
            columnMap.Add("CommentSubject", "COMMENT_SUBJECT");
            columnMap.Add("CommentCQ", "COMMENT_CQ");
            columnMap.Add("PartitionID", "PARTITION_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }

        #region Function To SMAS_MOBILE
        public void InsertOrUpdateToMobile(TeacherNoteBookMonth objTeacherNoteBookMonth, IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int monthID = Utils.GetInt(dic, "MonthID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            string TitleMark = Utils.GetString(dic, "TitleName");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int FieldType = Utils.GetInt(dic, "FieldType");
            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
            TeacherNoteBookMonth objTeacherNoteBookMonthDB = this.Search(dic).Where(p => p.PupilID == pupilID).FirstOrDefault();
            List<TeacherNoteBookMonth> lstUpdate = new List<TeacherNoteBookMonth>();
            List<TeacherNoteBookMonth> lstInsert = new List<TeacherNoteBookMonth>();
            int partitionID = (schoolID % 100);
            bool isChange = false;

            SubjectCat objSC = SubjectCatBusiness.Find(subjectID);
            #endregion

            //lay danh sach mien giam
            List<ExemptedSubject> lstExemp = ExemptedSubjectBusiness.Search(dic).ToList();

            isChange = false;
            if (!lstExemp.Any(p => p.PupilID == objTeacherNoteBookMonth.PupilID && p.SubjectID == objTeacherNoteBookMonth.SubjectID))
            {
                if (objTeacherNoteBookMonthDB != null)
                {
                    if (objTeacherNoteBookMonthDB.CommentSubject != objTeacherNoteBookMonth.CommentSubject && "NXMH".Equals(TitleMark))
                    {
                        objTeacherNoteBookMonthDB.CommentSubject = objTeacherNoteBookMonth.CommentSubject;
                        isChange = true;
                    }

                    if (objTeacherNoteBookMonthDB.CommentCQ != objTeacherNoteBookMonth.CommentCQ && "NXCQ".Equals(TitleMark))
                    {
                        objTeacherNoteBookMonthDB.CommentCQ = objTeacherNoteBookMonth.CommentCQ;
                        isChange = true;
                    }

                    if (isChange)
                    {
                        objTeacherNoteBookMonthDB.UpdateTime = DateTime.Now.Date;
                        lstUpdate.Add(objTeacherNoteBookMonthDB);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(objTeacherNoteBookMonth.CommentSubject) && "NXMH".Equals(TitleMark))
                    {
                        isChange = true;
                    }

                    if (!string.IsNullOrEmpty(objTeacherNoteBookMonth.CommentCQ) && "NXCQ".Equals(TitleMark))
                    {
                        isChange = true;
                    }
                    if (isChange)
                    {
                        objTeacherNoteBookMonth.CreateTime = DateTime.Now.Date;
                        objTeacherNoteBookMonth.PartitionID = partitionID;
                        lstInsert.Add(objTeacherNoteBookMonth);
                    }
                }
            }

            if (lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
                this.Save();
            }

            if (lstInsert.Count > 0)
            {
                this.BulkInsert(lstInsert, TeacherNoteBookMonthMaping(), "TeacherNoteBookMonthID");
            }
        }
    }
}
