/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class QualificationLevelBusiness
    {
        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="QualificationLevel"></param>
        /// <returns></returns>
        public override QualificationLevel Insert(QualificationLevel QualificationLevel)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(QualificationLevel.Resolution, "QualificationLevel_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(QualificationLevel.Resolution, 50, "QualificationLevel_Label_Resolution");
            // Kiem tra trung ten 
            //this.CheckDuplicate(QualificationLevel.Resolution, GlobalConstants.LIST_SCHEMA, "QualificationLevel", "Resolution", true, null, "QualificationLevel_Label_Resolution");
            //DungVA
            IDictionary<string, object> expDic = null;
            if (QualificationLevel.QualificationLevelID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["QualificationLevelID"] = QualificationLevel.QualificationLevelID;
            }
            bool QualificationLevelDuplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "QualificationLevel",
                   new Dictionary<string, object>()
                {
                    {"Resolution",QualificationLevel.Resolution},
                    {"IsActive",true}
                }, expDic);
            if (QualificationLevelDuplicate)
            {
                List<object> listParam = new List<object>();
                listParam.Add("QualificationLevel_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(QualificationLevel.Description, 400, "QualificationLevel_Label_Description");
            // Them vao CSDL            
            return base.Insert(QualificationLevel);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="QualificationLevel"></param>
        /// <returns></returns>
        public override QualificationLevel Update(QualificationLevel QualificationLevel)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(QualificationLevel.Resolution, "QualificationLevel_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(QualificationLevel.Resolution, 50, "QualificationLevel_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(QualificationLevel.Resolution, GlobalConstants.LIST_SCHEMA, "QualificationLevel", "Resolution", true, QualificationLevel.QualificationLevelID, "QualificationLevel_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(QualificationLevel.Description, 400, "QualificationLevel_Label_Description");
            // Kiem tra ten trinh do van hoa da co trong DB
            //this.CheckAvailable(QualificationLevel.QualificationLevelID, "QualificationLevel_Label_Resolution", false);
            new QualificationLevelBusiness(null).CheckAvailable(QualificationLevel.QualificationLevelID, "QualificationLevel_Label_Resolution", false);
            // Cap nhat vao CSDL  
            return base.Update(QualificationLevel);
        }

        /// <summary>
        /// Xoa trinh do van hoa
        /// </summary>
        /// <param name="QualificationLevelID"></param>
        public void Delete(int QualificationLevelID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(QualificationLevelID, "QualificationLevel_Label_Title", true);
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "QualificationLevel", QualificationLevelID, "QualificationLevel_Label_TitleFailed");
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(QualificationLevelID, true);
        }

        /// <summary>
        /// Tim kiem trinh do van hoa
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<QualificationLevel> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            string Description = Utils.GetString(dicParam, "Description");
            bool? IsActive = Utils.GetIsActive(dicParam, "IsActive");
            IQueryable<QualificationLevel> listQualificationLevel = this.QualificationLevelRepository.All.OrderBy(o => o.Resolution);
            if (!Resolution.Equals(string.Empty))
            {
                listQualificationLevel = listQualificationLevel.Where(ql => ql.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (!Description.Equals(string.Empty))
            {
                listQualificationLevel = listQualificationLevel.Where(ql => ql.Description.ToLower().Contains(Description.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listQualificationLevel = listQualificationLevel.Where(ql => ql.IsActive == true);
            }
            //int count = listQualificationLevel != null ? listQualificationLevel.Count() : 0;
            //if (count == 0)
            //{
            //    return null;
            //}
            return listQualificationLevel;

        }
    }
}