/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Reflection;
namespace SMAS.Business.Business
{
    public partial class CBReportBusiness
    {
        public List<SaleByDateReportBO> GetSaleByDateReport(int provinceId, int districtId, int year, int month, int acaYear)
        {
            string sql = this.GetSaleByDateReportSQLString();
            StringBuilder sb = new StringBuilder(sql);
            sb.Replace("@Year", year.ToString());
            sb.Replace("@Month", month.ToString());
            sb.Replace("@AcaYear", acaYear.ToString());
            sb.Replace("@ProvinceID", provinceId.ToString());
            sb.Replace("@DistrictID", districtId.ToString());

            DateTime startDateOfMonth = new DateTime(year, month, 1);
            string strStartDate = startDateOfMonth.ToString("dd/MM/yyyy");
            DateTime endDateOfMonth = startDateOfMonth.EndOfMonth();
            string strEndDate = endDateOfMonth.ToString("dd/MM/yyyy");
            int daysInMonth = DateTime.DaysInMonth(year, month);

            sb.Replace("@DayInMonth", daysInMonth.ToString());
            sb.Replace("@FirstDateMonth", string.Format("TO_DATE('{0}', 'dd/mm/yyyy')", strStartDate));
            sb.Replace("@EndDate", string.Format("TO_DATE('{0}', 'dd/mm/yyyy')", strEndDate));

            string query = sb.ToString();
            DataTable results = new DataTable();
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();

            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                using (OracleCommand command = new OracleCommand(query, conn))
                {
                    using (OracleDataAdapter dataAdapter = new OracleDataAdapter(command))
                    {
                        dataAdapter.Fill(results);
                    }
                }
            }

            return this.DataTableToList<SaleByDateReportBO>(results);
        }

        private string GetSaleByDateReportSQLString()
        {
            string sql = @"
                            select PROVINCE_NAME AS ProvinceName, DISTRICT_NAME AS DistrictName, FULL_NAME AS FullName, CB_CODE AS CBCode, PHONE AS Phone, EMAIL AS Email,
                            MAX(TBCT) as TBCT,MAX(DTCT) as DTCT, 
                            MAX(TB1) as TB1,MAX(DT1) as DT1,
                            MAX(TB2) as TB2,MAX(DT2) as DT2,
                            MAX(TB3) as TB3,MAX(DT3) as DT3,
                            MAX(TB4) as TB4,MAX(DT4) as DT4,
                            MAX(TB5) as TB5,MAX(DT5) as DT5,
                            MAX(TB6) as TB6,MAX(DT6) as DT6,
                            MAX(TB7) as TB7,MAX(DT7) as DT7,
                            MAX(TBW1) AS TBW1, MAX(DTW1) AS DTW1,
                            MAX(TB8) as TB8,MAX(DT8) as DT8,
                            MAX(TB9) as TB9,MAX(DT9) as DT9,
                            MAX(TB10) as TB10,MAX(DT10) as DT10,
                            MAX(TB11) as TB11,MAX(DT11) as DT11,
                            MAX(TB12) as TB12,MAX(DT12) as DT12,
                            MAX(TB13) as TB13,MAX(DT13) as DT13,
                            MAX(TB14) as TB14,MAX(DT14) as DT14,
                            MAX(TBW2) AS TBW2, MAX(DTW2) AS DTW2,
                            MAX(TB15) as TB15,MAX(DT15) as DT15,
                            MAX(TB16) as TB16,MAX(DT16) as DT16,
                            MAX(TB17) as TB17,MAX(DT17) as DT17,
                            MAX(TB18) as TB18,MAX(DT18) as DT18,
                            MAX(TB19) as TB19,MAX(DT19) as DT19,
                            MAX(TB20) as TB20,MAX(DT20) as DT20,
                            MAX(TB21) as TB21,MAX(DT21) as DT21,
                            MAX(TBW3) AS TBW3, MAX(DTW3) AS DTW3,
                            MAX(TB22) as TB22,MAX(DT22) as DT22,
                            MAX(TB23) as TB23,MAX(DT23) as DT23,
                            MAX(TB24) as TB24,MAX(DT24) as DT24,
                            MAX(TB25) as TB25,MAX(DT25) as DT25,
                            MAX(TB26) as TB26,MAX(DT26) as DT26,
                            MAX(TB27) as TB27,MAX(DT27) as DT27,
                            MAX(TB28) as TB28,MAX(DT28) as DT28,
                            MAX(TBW4) AS TBW4, MAX(DTW4) AS DTW4,
                            MAX(TB29) as TB29,MAX(DT29) as DT29,
                            MAX(TB30) as TB30,MAX(DT30) as DT30,
                            MAX(TB31) as TB31,MAX(DT31) as DT31

                            from 
                            (
                            (
	                            --thue bao
	                            select PROVINCE_NAME,DISTRICT_NAME, FULL_NAME, CB_CODE, PHONE,EMAIL,
	                            --ca thang
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) >=1 and EXTRACT(DAY FROM RecordTime) <= 31
		                            then 1 else 0 end) as TBCT,
	                            0 as DTCT,
	                            --ngay 1
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 1
		                            then 1 else 0 end) as TB1,
	                            0 as DT1,
	                            --ngay 2
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 2
		                            then 1 else 0 end) as TB2,
	                            0 as DT2,
	                            --ngay 3
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 3
		                            then 1 else 0 end) as TB3,
	                            0 as DT3,
	                            --ngay 4
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 4
		                            then 1 else 0 end) as TB4,
	                            0 as DT4,
	                            --ngay 5
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 5
		                            then 1 else 0 end) as TB5,
	                            0 as DT5,
	                            --ngay 6
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 6
		                            then 1 else 0 end) as TB6,
	                            0 as DT6,
	                            --ngay 7
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 7
		                            then 1 else 0 end) as TB7,
	                            0 as DT7,
	                            --Tuan 1
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) >=1 and EXTRACT(DAY FROM RecordTime) <= 7
		                            then 1 else 0 end) as TBW1,
	                            0 as DTW1,
		
	                            --ngay 8
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 8
		                            then 1 else 0 end) as TB8,
	                            0 as DT8,	
	                            --ngay 9
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 9
		                            then 1 else 0 end) as TB9,
	                            0 as DT9,
	                            --ngay 10
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 10
		                            then 1 else 0 end) as TB10,
	                            0 as DT10,
	                            --ngay 11
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 11
		                            then 1 else 0 end) as TB11,
	                            0 as DT11,
	                            --ngay 12
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 12
		                            then 1 else 0 end) as TB12,
	                            0 as DT12,
	                            --ngay 13
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 13
		                            then 1 else 0 end) as TB13,
	                            0 as DT13,
	                            --ngay 14
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 14
		                            then 1 else 0 end) as TB14,
	                            0 as DT14,
	                            --Tuan 2
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) >=8 and EXTRACT(DAY FROM RecordTime) <= 14
		                            then 1 else 0 end) as TBW2,
	                            0 as DTW2,
		
	                            --ngay 15
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 15
		                            then 1 else 0 end) as TB15,
	                            0 as DT15,
	                            --ngay 16
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 16
		                            then 1 else 0 end) as TB16,
	                            0 as DT16,	
	                            --ngay 17
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 17
		                            then 1 else 0 end) as TB17,
	                            0 as DT17,
	                            --ngay 18
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 18
		                            then 1 else 0 end) as TB18,
	                            0 as DT18,
	                            --ngay 19
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 19
		                            then 1 else 0 end) as TB19,
	                            0 as DT19,
	                            --ngay 20
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 20
		                            then 1 else 0 end) as TB20,
	                            0 as DT20,
	                            --ngay 21
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 21
		                            then 1 else 0 end) as TB21,
	                            0 as DT21,
	                            --Tuan 3
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) >=15 and EXTRACT(DAY FROM RecordTime) <= 21
		                            then 1 else 0 end) as TBW3,
	                            0 as DTW3,
		
	                            --ngay 22
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 22
		                            then 1 else 0 end) as TB22,
	                            0 as DT22,
	                            --ngay 23
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 23
		                            then 1 else 0 end) as TB23,
	                            0 as DT23,
	                            --ngay 24
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 24
		                            then 1 else 0 end) as TB24,
	                            0 as DT24,
	                            --ngay 25
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 25
		                            then 1 else 0 end) as TB25,
	                            0 as DT25,
	                            --ngay 26
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 26
		                            then 1 else 0 end) as TB26,
	                            0 as DT26,
	                            --ngay 27
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 27
		                            then 1 else 0 end) as TB27,
	                            0 as DT27,
	                            --ngay 28
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 28
		                            then 1 else 0 end) as TB28,
	                            0 as DT28,
	                            --Tuan 2
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) >=22 and EXTRACT(DAY FROM RecordTime) <= 28
		                            then 1 else 0 end) as TBW4,
	                            0 as DTW4,

	                            --ngay 29
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 29
		                            then 1 else 0 end) as TB29,
	                            0 as DT29,
	                            --ngay 30
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 30
		                            then 1 else 0 end) as TB30,
	                            0 as DT30,
	                            --ngay 31
	                            SUM (case when EXTRACT(YEAR FROM RecordTime) = @Year
		                            and EXTRACT(MONTH FROM RecordTime) = @Month
		                            and EXTRACT(DAY FROM RecordTime) = 31
		                            then 1 else 0 end) as TB31,
	                            0 as DT31
		
	                             from
	                            (

		                            select * from 
		                            (
			                            select AP.PROVINCE_NAME, 
			                            AD.DISTRICT_NAME, 
			                            CBC.FULL_NAME, 
			                            CBC.CB_CODE, 
			                            CBC.PHONE, 
			                            CBC.EMAIL,
			                            CBMA.Assign_Date,
			                            CBMA.End_Date,
			                            SMSSPC.Pupil_ID,
			                            SMSSPCD.Subscriber, 
			                            min(case when SMSSPCD.Payment_Time IS not null then SMSSPCD.Payment_Time else SMSSPCD.CREATED_TIME end) as RecordTime
			                            from CB_COLLABORATOR CBC
				                            left join PROVINCE AP on CBC.PROVINCE_ID = AP.PROVINCE_ID
				                            left join DISTRICT AD on CBC.DISTRICT_ID = AD.DISTRICT_ID
				                            join CB_MANAGING_ASCRIPTION CBMA on CBC.CB_ID = CBMA.CB_ID
				                            join SMS_PARENT_CONTRACT SMSSPC on CBMA.School_ID = SMSSPC.School_ID
				                            join SMS_PARENT_CONTRACT_DETAIL SMSSPCD on SMSSPC.SMS_PARENT_CONTRACT_ID = SMSSPCD.SMS_PARENT_CONTRACT_ID
				                            join SMS_SERVICE_PACKAGE sv on SMSSPCD.Service_Package_ID = sv.Service_Package_ID
			                            where CBMA.Type = 1 and cbc.Status=1
			                            and (SMSSPCD.Payment_Time is not null or (SMSSPCD.Payment_Time is null and SMSSPCD.SUBSCRIPTION_STATUS in(1,3,4)))
			                            and (CBC.PROVINCE_ID = @ProvinceID or @ProvinceID = 0)
			                            and (CBC.DISTRICT_ID = @DistrictID or @DistrictID = 0)
			                            and SMSSPCD.YEAR_ID = @AcaYear
			                            and CBMA.Status = 1
			                            AND (CBMA.End_Date is null OR (CBMA.End_Date >= @FirstDateMonth OR  CBMA.End_Date >= @EndDate))
			                            group by AP.PROVINCE_NAME, AD.DISTRICT_NAME, CBC.FULL_NAME, CBC.CB_CODE, CBC.PHONE,
			                             CBC.EMAIL, CBMA.Assign_Date,CBMA.End_Date, SMSSPC.Pupil_ID, SMSSPCD.Subscriber
		                            )tbl
		                            where CAST(Assign_Date AS DATE) <= CAST(RecordTime AS DATE) 
		                            and (CAST(End_Date AS DATE) >= CAST(RecordTime AS DATE) OR End_Date IS NULL)  
		                            and EXTRACT(YEAR FROM RecordTime)= @Year
		                            AND EXTRACT(MONTH FROM RecordTime)=@Month
		

	                            )tbl2
	                            group by PROVINCE_NAME,DISTRICT_NAME, FULL_NAME, CB_CODE, PHONE,EMAIL
	                            )
	                            UNION
	                            (
			                            --DOANH THU
			                            select AP.PROVINCE_NAME,AD.DISTRICT_NAME, CBC.FULL_NAME, CBC.CB_CODE, CBC.PHONE,CBC.EMAIL,
			                            --ca thang
			                            0 as TBCT,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) >=1 and EXTRACT(DAY FROM ET.CREATED_TIME) <= 31
				                            then ET.TRANS_AMOUNT else 0 end) as DTCT,
			                            --ngay 1
			                            0 as TB1,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 1
				                            then ET.TRANS_AMOUNT else 0 end) as DT1,
			                            --ngay 2
			                            0 as TB2,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 2
				                            then ET.TRANS_AMOUNT else 0 end) as DT2,
			
			                            --ngay 3
			                            0 as TB3,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 3
				                            then ET.TRANS_AMOUNT else 0 end) as DT3,
			                            --ngay 4
			                            0 as TB4,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 4
				                            then ET.TRANS_AMOUNT else 0 end) as DT4,
			                            --ngay 5
			                            0 as TB5,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 5
				                            then ET.TRANS_AMOUNT else 0 end) as DT5,
			                            --ngay 6
			                            0 as TB6,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 6
				                            then ET.TRANS_AMOUNT else 0 end) as DT6,
			                            --ngay 7
			                            0 as TB7,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 7
				                            then ET.TRANS_AMOUNT else 0 end) as DT7,
			                            --Tuan 1
			                            0 as TBW1,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) >=1 and EXTRACT(DAY FROM ET.CREATED_TIME) <= 7
				                            then ET.TRANS_AMOUNT else 0 end) as DTW1,
				
			                            --ngay 8
			                            0 as TB8,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 8
				                            then ET.TRANS_AMOUNT else 0 end) as DT8,
			                            --ngay 9
			                            0 as TB9,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 9
				                            then ET.TRANS_AMOUNT else 0 end) as DT9,
			                            --ngay 10
			                            0 as TB10,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 10
				                            then ET.TRANS_AMOUNT else 0 end) as DT10,
			                            --ngay 11
			                            0 as TB11,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 11
				                            then ET.TRANS_AMOUNT else 0 end) as DT11,
			                            --ngay 12
			                            0 as TB12,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 12
				                            then ET.TRANS_AMOUNT else 0 end) as DT12,
			                            --ngay 13
			                            0 as TB13,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 13
				                            then ET.TRANS_AMOUNT else 0 end) as DT13,
			                            --ngay 14
			                            0 as TB14,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 14
				                            then ET.TRANS_AMOUNT else 0 end) as DT14,
			                            --Tuan 2
			                            0 as TBW2,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) >=8 and EXTRACT(DAY FROM ET.CREATED_TIME) <= 14
				                            then ET.TRANS_AMOUNT else 0 end) as DTW2,
				
			                            --ngay 15
			                            0 as TB15,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 15
				                            then ET.TRANS_AMOUNT else 0 end) as DT15,
			                            --ngay 16
			                            0 as TB16,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 16
				                            then ET.TRANS_AMOUNT else 0 end) as DT16,
			                            --ngay 17
			                            0 as TB17,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 17
				                            then ET.TRANS_AMOUNT else 0 end) as DT17,
			                            --ngay 18
			                            0 as TB18,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 18
				                            then ET.TRANS_AMOUNT else 0 end) as DT18,
			                            --ngay 19
			                            0 as TB19,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 19
				                            then ET.TRANS_AMOUNT else 0 end) as DT19,
			                            --ngay 20
			                            0 as TB20,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 20
				                            then ET.TRANS_AMOUNT else 0 end) as DT20,
			                            --ngay 21
			                            0 as TB21,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 21
				                            then ET.TRANS_AMOUNT else 0 end) as DT21,
			                            --Tuan 3
			                            0 as TBW3,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) >=15 and EXTRACT(DAY FROM ET.CREATED_TIME) <= 21
				                            then ET.TRANS_AMOUNT else 0 end) as DTW3,
				
			                            --ngay 22
			                            0 as TB22,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 22
				                            then ET.TRANS_AMOUNT else 0 end) as DT22,
			                            --ngay 23
			                            0 as TB23,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 23
				                            then ET.TRANS_AMOUNT else 0 end) as DT23,
			                            --ngay 24
			                            0 as TB24,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 24
				                            then ET.TRANS_AMOUNT else 0 end) as DT24,
			                            --ngay 25
			                            0 as TB25,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 25
				                            then ET.TRANS_AMOUNT else 0 end) as DT25,
			                            --ngay 26
			                            0 as TB26,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 26
				                            then ET.TRANS_AMOUNT else 0 end) as DT26,
			                            --ngay 27
			                            0 as TB27,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 27
				                            then ET.TRANS_AMOUNT else 0 end) as DT27,
			                            --ngay 28
			                            0 as TB28,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 28
				                            then ET.TRANS_AMOUNT else 0 end) as DT28,
			                            --Tuan 4
			                            0 as TBW4,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) >=22 and EXTRACT(DAY FROM ET.CREATED_TIME) <= 28
				                            then ET.TRANS_AMOUNT else 0 end) as DTW4,

			                            --ngay 29
			                            0 as TB29,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 29
				                            then ET.TRANS_AMOUNT else 0 end) as DT29,
			                            --ngay 30
			                            0 as TB30,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 30
				                            then ET.TRANS_AMOUNT else 0 end) as DT30,
			                            --ngay 31
			                            0 as TB31,
			                            SUM (case when EXTRACT(YEAR FROM ET.CREATED_TIME) = @Year
				                            and EXTRACT(MONTH FROM ET.CREATED_TIME) = @Month
				                            and EXTRACT(DAY FROM ET.CREATED_TIME) = 31
				                            then ET.TRANS_AMOUNT else 0 end) as DT31
			                            from CB_COLLABORATOR CBC
				                            left join PROVINCE AP on CBC.PROVINCE_ID = AP.PROVINCE_ID
				                            left join DISTRICT AD on CBC.DISTRICT_ID = AD.DISTRICT_ID
				                            join CB_MANAGING_ASCRIPTION CBMA on CBC.CB_ID = CBMA.CB_ID
				                            JOIN EW_EWALLET EW ON ((CBMA.SCHOOL_ID = EW.UNIT_ID AND EW.EWALLET_TYPE = 1 AND CBMA.Type = 1) OR (CBMA.SUPER_VISING_DEPT_ID = EW.UNIT_ID AND EW.EWALLET_TYPE = 2 AND (CBMA.Type = 2 or CBMA.Type = 3)))
				                            JOIN EW_EWALLET_TRANSACTION ET ON EW.EWALLET_ID = ET.EWALLET_ID
			                            WHERE (CBC.PROVINCE_ID = @ProvinceID or @ProvinceID = 0)
				                            and (CBC.DISTRICT_ID = @DistrictID or @DistrictID = 0)
				                            AND ET.TRANS_TYPE_ID = 1
				                            AND ET.STATUS = 1
				                            AND EXTRACT(YEAR FROM ET.CREATED_TIME)=@Year
				                            AND EXTRACT(MONTH FROM ET.CREATED_TIME)=@Month
				                            and CBMA.Status = 1
				                            AND CAST(CBMA.ASSIGN_DATE AS DATE) <= CAST(ET.CREATED_TIME AS DATE) 
				                            and ( CAST(CBMA.END_DATE AS DATE) >= CAST(ET.CREATED_TIME AS DATE) OR CBMA.END_DATE IS NULL)
			                            group by PROVINCE_NAME,DISTRICT_NAME, FULL_NAME, CB_CODE, PHONE,EMAIL
	                            )
                            )tbl3
                            GROUP BY PROVINCE_NAME, DISTRICT_NAME, FULL_NAME, CB_CODE, PHONE, EMAIL
                            order by PROVINCE_NAME,DISTRICT_NAME,FULL_NAME
                ";

            return sql;
        }

        private T ToObject<T>(DataRow row) where T : class, new()
        {
            T obj = new T();

            foreach (var prop in obj.GetType().GetProperties())
            {
                try
                {
                    if (row[prop.Name] == DBNull.Value)
                    {
                        prop.SetValue(obj, null);
                    }
                    if (prop.PropertyType.IsGenericType && prop.PropertyType.Name.Contains("Nullable"))
                    {
                        if (!string.IsNullOrEmpty(row[prop.Name].ToString()))
                            prop.SetValue(obj, Convert.ChangeType(row[prop.Name],
                            Nullable.GetUnderlyingType(prop.PropertyType), null));
                        //else do nothing
                    }
                    else
                        prop.SetValue(obj, Convert.ChangeType(row[prop.Name], prop.PropertyType), null);
                }
                catch
                {
                    continue;
                }
            }
            return obj;
        }
        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        private List<T> DataTableToList<T>(DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    var obj = this.ToObject<T>(row);

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}
