﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class EmployeeBreatherBusiness
    {
        public IQueryable<EmployeeBreather> GetListEmployeeBreather(IDictionary<string,object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int teacherID = Utils.GetInt(dic, "TeacherID");
            IQueryable<EmployeeBreather> iqEmployeeBreather = this.EmployeeBreatherRepository.All;
            if (schoolID > 0)
            {
                iqEmployeeBreather = iqEmployeeBreather.Where(p => p.SchoolID == schoolID);
            }
            if (academicYearID > 0)
            {
                iqEmployeeBreather = iqEmployeeBreather.Where(p => p.AcademicYearID == academicYearID);
            }
            if (teacherID > 0)
            {
                iqEmployeeBreather = iqEmployeeBreather.Where(p => p.TeacherID == teacherID);
            }
            return iqEmployeeBreather;
        }

        public void DeleteBreather(int EmployeeBreatherID, int SchoolID)
        {
            EmployeeBreather employeeBreather = EmployeeBreatherBusiness.Find(EmployeeBreatherID);
            this.CheckAvailable(EmployeeBreatherID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID", false);
            //EmployeeID, SchoolID: not compatible(Employee)
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"EmployeeID",employeeBreather.TeacherID},
                    {"SchoolID",employeeBreather.SchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //SchoolID(FromSchoolID), EmployeeWorkMovementID: not compatible(EmployeeWorkMovement)
            bool EmployeeBreatherCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeBreather",
                  new Dictionary<string, object>()
                {
                    {"EmployeeBreatherID",employeeBreather.EmployeeBreatherID},
                    {"SchoolID",employeeBreather.SchoolID}
                }, null);
            if (!EmployeeBreatherCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == employeeBreather.TeacherID && o.SchoolID == employeeBreather.SchoolID);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                EmployeeHistoryStatus.ToDate = null;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }

            Employee Employee = EmployeeBusiness.Find(employeeBreather.TeacherID);
            // Cap nhat lai trang thai cho can bo la dang lam viec
            Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            EmployeeBusiness.Update(Employee);

            //Cập nhật lại trang thái trong UserAccount
            int teacherID = employeeBreather.TeacherID;
            UserAccount ua = UserAccountBusiness.All.Where(o => o.EmployeeID.Value == teacherID && o.IsActive == true).ToList().FirstOrDefault();
            if (ua != null)
            {
                ua.IsActive = true;
                ua.EmployeeID = teacherID;
                UserAccountBusiness.Update(ua);
                aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).FirstOrDefault();
                member.IsApproved = 1;
                aspnet_MembershipBusiness.Update(member);
            }
            base.Delete(EmployeeBreatherID);
        }

        public EmployeeBreather InsertOrUpdateBreather(EmployeeBreather objEmployeeBreather,IDictionary<string,object> dic)
        {
            bool isChecked = Utils.GetBool(dic, "IsChecked");
            //validate du lieu dau vao
            ValidationMetadata.ValidateObject(objEmployeeBreather);
            int EmployeeID = objEmployeeBreather.TeacherID;
            int schoolID = objEmployeeBreather.SchoolID;

            Employee Employee = EmployeeBusiness.Find(EmployeeID);
            EmployeeBreather objEmployeeBreatherDB = EmployeeBreatherBusiness.All.Where(p => p.TeacherID == EmployeeID).FirstOrDefault();

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID =Entity.TeacherID, SchoolID = Entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_SeveranceMENT, ToDate = Entity.MovedDate.
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == objEmployeeBreather.TeacherID && o.SchoolID == objEmployeeBreather.SchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = isChecked ? GlobalConstants.EMPLOYMENT_STATUS_WORKING : GlobalConstants.EMPLOYMENT_STATUS_BREATHER;
                if(isChecked)
                {
                    EmployeeHistoryStatus.ToDate = null;
                }
                else
                {
                EmployeeHistoryStatus.ToDate = objEmployeeBreather.DateBreather;
                }
                
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }
            DateTime? dateBreather = EmployeeBreatherBusiness.All.Where(o => o.EmployeeBreatherID == objEmployeeBreather.EmployeeBreatherID).Select(o => o.DateBreather).FirstOrDefault();
            if (objEmployeeBreather.DateBreather != dateBreather)
            {
                IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", EmployeeID }, { "IsActive", true } });
                foreach (ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
                {
                    if (p.FromDate >= objEmployeeBreather.DateBreather)
                    {
                        ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                    }
                    else
                    {
                        if (p.ToDate > objEmployeeBreather.DateBreather)
                        {
                            p.ToDate = objEmployeeBreather.DateBreather;
                            ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                        }
                    }
                }
                IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == EmployeeID);
                foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
                {
                    if (p.AssignedDate >= objEmployeeBreather.DateBreather)
                    {
                        HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                    }
                    else
                    {
                        if (p.EndDate > objEmployeeBreather.DateBreather)
                        {
                            p.EndDate = objEmployeeBreather.DateBreather;
                            HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                        }
                    }
                }

            }

            if (objEmployeeBreatherDB != null)//update
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeID",objEmployeeBreather.TeacherID}
                    ,{"SchoolID",objEmployeeBreather.SchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
                if (isChecked)//tro lai lam viec
                {
                    //Utils.ValidateBeforeDate(objEmployeeBreather.DateStartWork, objEmployeeBreather.DateBreather, "Employee_Label_StartWorkBreather", "Employee_Label_DateBreather");
                    if (objEmployeeBreather.DateStartWork < objEmployeeBreather.DateBreather)
                    {
                        throw new BusinessException("Validate_StartDate_BreatherDate");
                    }                    
                    objEmployeeBreatherDB.DateStartWork = objEmployeeBreather.DateStartWork;
                    //cap nhat lai trang thai can bo la dang lam viec
                    Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
                    EmployeeBusiness.Update(Employee);

                    //Cập nhật lại trang thái trong UserAccount
                    int teacherID = objEmployeeBreather.TeacherID;
                    UserAccount ua = UserAccountBusiness.All.Where(o => o.EmployeeID.Value == teacherID && o.IsActive == true).ToList().FirstOrDefault();
                    if (ua != null)
                    {
                        aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).FirstOrDefault();
                        member.IsApproved = 1;
                        aspnet_MembershipBusiness.Update(member);
                    }

                }
                else//khong tro lai lam viec
                {
                    objEmployeeBreatherDB.DateBreather = objEmployeeBreather.DateBreather;
                    objEmployeeBreatherDB.DateStartWork = null;

                    //if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                    //{
                    //    throw new BusinessException("HeadTeacherSubstitution_Label_WorkingStatusErrr");
                    //}
                    // Cap nhat lai trang thai cho can bo la tam nghi
                    Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_BREATHER;
                    EmployeeBusiness.Update(Employee);

                    //Cập nhật lại trang thái trong UserAccount
                    int teacherID = objEmployeeBreather.TeacherID;
                    UserAccount ua = UserAccountBusiness.All.Where(o => o.EmployeeID.Value == teacherID && o.IsActive == true).ToList().FirstOrDefault();
                    if (ua != null)
                    {
                        aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).FirstOrDefault();
                        member.IsApproved = 0;
                        aspnet_MembershipBusiness.Update(member);
                    }
                }
                objEmployeeBreatherDB.VacationReasonID = objEmployeeBreather.VacationReasonID;
                objEmployeeBreatherDB.Note = objEmployeeBreather.Note;
                objEmployeeBreatherDB.IsComeBack = objEmployeeBreather.IsComeBack;
                objEmployeeBreatherDB.UpdateTime = DateTime.Now;
                return base.Update(objEmployeeBreatherDB);
            }
            else//insert
            {
                if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                {
                    throw new BusinessException("HeadTeacherSubstitution_Label_WorkingStatusErrr");
                }
                // Cap nhat lai trang thai cho can bo la tam nghi
                Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_BREATHER;
                EmployeeBusiness.Update(Employee);
                objEmployeeBreather.CreateTime = DateTime.Now;

                //Cập nhật lại trang thái trong UserAccount
                int teacherID = objEmployeeBreather.TeacherID;
                UserAccount ua = UserAccountBusiness.All.Where(o => o.EmployeeID.Value == teacherID && o.IsActive == true).ToList().FirstOrDefault();
                if (ua != null)
                {
                    aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).FirstOrDefault();
                    member.IsApproved = 0;
                    aspnet_MembershipBusiness.Update(member);
                }

                return base.Insert(objEmployeeBreather);

            }
        }
    }
}