﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Lỗi vi phạm quy chế thi
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class ExamViolationTypeBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 100;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion

        #region insert
        /// <summary>
        /// Thêm mới Lỗi vi phạm quy chế thi
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertExamViolationType">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được vào DB thành công</returns>
        public override ExamViolationType Insert(ExamViolationType insertExamViolationType)
        {


            //resolution rong
            Utils.ValidateRequire(insertExamViolationType.Resolution, "ExamViolationType_Label_Resolution");

            Utils.ValidateMaxLength(insertExamViolationType.Resolution, RESOLUTION_MAX_LENGTH, "ExamViolationType_Label_Resolution");
            Utils.ValidateMaxLength(insertExamViolationType.Description, DESCRIPTION_MAX_LENGTH, "ExamViolationType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            //this.CheckDuplicate(insertExamViolationType.Resolution, GlobalConstants.LIST_SCHEMA, "ExamViolationType", "Resolution", true, 0, "ExamViolationType_Label_Resolution");
            //this.CheckDuplicate(insertExamViolationType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "ExamViolationType", "AppliedLevel", true, 0, "ExamViolationType_Label_ShoolID");
           // this.CheckDuplicateCouple(insertExamViolationType.Resolution, insertExamViolationType.SchoolID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "ExamViolationType", "Resolution", "SchoolID", true, 0, "ExamViolationType_Label_Resolution");
            //DungVA
            IDictionary<string, object> expDic = null;
            if (insertExamViolationType.ExamViolationTypeID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["ExamViolationTypeID"] = insertExamViolationType.ExamViolationTypeID;
            }
            bool ExamViolationTypeDuplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ExamViolationType",
                   new Dictionary<string, object>()
                {
                    {"Resolution",insertExamViolationType.Resolution},
                    {"SchoolID",insertExamViolationType.SchoolID},
                    {"AppliedForCandidate",insertExamViolationType.AppliedForCandidate},
                    {"AppliedForInvigilator",insertExamViolationType.AppliedForInvigilator},
                    {"IsActive",true}
                }, expDic);
            if (ExamViolationTypeDuplicate)
            {
                List<object> listParam = new List<object>();
                listParam.Add("ExamViolationType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }
            //kiem tra range
            if (insertExamViolationType.AppliedForCandidate.HasValue && insertExamViolationType.AppliedForCandidate.Value)
            {
                Utils.ValidateRange((int)insertExamViolationType.PenalizedMark, 1, 100, "ExamViolationType_Column_PenalizedMark");
            }
            
            //
            SchoolProfileBusiness.CheckAvailable(insertExamViolationType.SchoolID, "UserInfo_SchoolID");


            insertExamViolationType.IsActive = true;

           return base.Insert(insertExamViolationType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Lỗi vi phạm quy chế thi
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateExamViolationType">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được vào DB thành công</returns>
        public override ExamViolationType Update(ExamViolationType updateExamViolationType)
        {

            //check avai
            new ExamViolationTypeBusiness(null).CheckAvailable(updateExamViolationType.ExamViolationTypeID, "ExamViolationType_Label_ExamViolationTypeID");

            //resolution rong
            Utils.ValidateRequire(updateExamViolationType.Resolution, "ExamViolationType_Label_Resolution");

            Utils.ValidateMaxLength(updateExamViolationType.Resolution, RESOLUTION_MAX_LENGTH, "ExamViolationType_Label_Resolution");
            Utils.ValidateMaxLength(updateExamViolationType.Description, DESCRIPTION_MAX_LENGTH, "ExamViolationType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            // this.CheckDuplicate(updateExamViolationType.Resolution, GlobalConstants.LIST_SCHEMA, "ExamViolationType", "Resolution", true, updateExamViolationType.ExamViolationTypeID, "ExamViolationType_Label_Resolution");

            this.CheckDuplicateCouple(updateExamViolationType.Resolution, updateExamViolationType.SchoolID.ToString(),
                GlobalConstants.LIST_SCHEMA, "ExamViolationType", "Resolution", "SchoolID", true, updateExamViolationType.ExamViolationTypeID, "ExamViolationType_Label_Resolution");

            // this.CheckDuplicate(updateExamViolationType.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "ExamViolationType", "AppliedLevel", true, updateExamViolationType.ExamViolationTypeID, "ExamViolationType_Label_ShoolID");

            //kiem tra range
            if (updateExamViolationType.AppliedForCandidate.HasValue && updateExamViolationType.AppliedForCandidate.Value)
            {
                Utils.ValidateRange((int)updateExamViolationType.PenalizedMark, 1, 100, "ExamViolationType_Column_PenalizedMark");
            }

            //check 
            SchoolProfileBusiness.CheckAvailable(updateExamViolationType.SchoolID, "UserInfo_SchoolID");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu ExamViolationTypeID ->vao csdl lay ExamViolationType tuong ung roi 
            //so sanh voi updateExamViolationType.SchoolID


            //.................



            updateExamViolationType.IsActive = true;

           return base.Update(updateExamViolationType);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Lỗi vi phạm quy chế thi
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="ExamViolationTypeId">ID Lỗi vi phạm quy chế thi</param>
        public  void Delete(int ExamViolationTypeId)
        {
            //check avai
            new ExamViolationTypeBusiness(null).CheckAvailable(ExamViolationTypeId, "Exam_Violation_Type_Label");

            //da su dung hay chua
            this.CheckConstraintsNotDelete(GlobalConstants.LIST_SCHEMA, "ExamViolationType", ExamViolationTypeId, "Exam_Violation_Type_Label");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu ExamViolationTypeID ->vao csdl lay ExamViolationType tuong ung roi 
            //so sanh voi updateExamViolationType.SchoolID


            //.................


            base.Delete(ExamViolationTypeId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Lỗi vi phạm quy chế thi
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<ExamViolationType> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");
            int SchoolID = Utils.GetInt(dic, "SchoolID") ;
            bool? isActive =Utils.GetIsActive(dic,"IsActive");
            bool? IsAppliedFor = Utils.GetIsActive(dic, "IsAppliedFor");
            IQueryable<ExamViolationType> lsExamViolationType = this.ExamViolationTypeRepository.All;
             if (isActive.HasValue)
            {
                lsExamViolationType = lsExamViolationType.Where(em => (em.IsActive == isActive));
            }

        

            if (resolution.Trim().Length != 0)
            {

                lsExamViolationType = lsExamViolationType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (SchoolID != 0)
            {
                lsExamViolationType = lsExamViolationType.Where(em => (em.SchoolID == SchoolID));
            }

            if (description.Trim().Length != 0)
            {

                lsExamViolationType = lsExamViolationType.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

            if (IsAppliedFor.HasValue)
            {
                if (IsAppliedFor.Value)
                {
                    lsExamViolationType = lsExamViolationType.Where(em => em.AppliedForCandidate == true);
                }
                else
                {
                    lsExamViolationType = lsExamViolationType.Where(em => em.AppliedForInvigilator == true);
                }
            }
         
            return lsExamViolationType;
        }

        public IQueryable<ExamViolationType> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0 || dic == null) return null;
            dic.Add("SchoolID", SchoolID);
            return Search(dic);
        }
        #endregion

    }
}