/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.DAL.Repository;
using SMAS.Business.BusinessObject;
using System.Configuration;

namespace SMAS.Business.Business
{
    public partial class VJudgeRecordBusiness
    {
        /// <summary>        /// 
        /// Tim kiem theo truong
        /// </summary>
        /// <author>Quanglm</author>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<VJudgeRecord> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.SearchJudgeRecord(dic);
        }

        #region Search

        /// <summary>
        /// Tim chi tiet
        /// </summary>
        /// <author>Quanglm</author>
        /// <date>30/2/2013</date>
        private IQueryable<VJudgeRecord> SearchJudgeRecord(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            if (SchoolID == 0)
            {
                return null;
            }
            int JudgeRecordID = Utils.GetInt(dic, "JudgeRecordID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester", -1);
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            string Judgement = Utils.GetString(dic, "Judgement");
            string ReTestJudgement = Utils.GetString(dic, "ReTestJudgement");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            int OrderNumber = Utils.GetInt(dic, "OrderNumber");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            int partitionid = UtilsBusiness.GetPartionId(SchoolID);
            //tien hanh tim
            IQueryable<PupilProfile> IqPupilProfile = PupilProfileBusiness.AllNoTracking;
            IQueryable<VJudgeRecord> ListJudgeRecord = from jud in VJudgeRecordRepository.AllNoTracking
                                                       join pp in IqPupilProfile on jud.PupilID equals pp.PupilProfileID
                                                       where pp.IsActive && jud.Last2digitNumberSchool == partitionid
                                                       select jud;
            if (JudgeRecordID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.JudgeRecordID == JudgeRecordID));
            }
            if (PupilID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.AcademicYearID == AcademicYearID));
                //AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                /*var aca = this.AcademicYearBusiness.All
                   .Where(p => p.AcademicYearID == AcademicYearID)
                   .Select(p => new { AcademicYearID = p.AcademicYearID, SchoolID = p.SchoolID, Year = p.Year })
                   .FirstOrDefault();
                int? createAcaYear = aca.Year;
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.CreatedAcademicYear == createAcaYear));
                // Phong khi khong tim kiem theo dk school_id
                if (SchoolID <= 0)
                {
                    ListJudgeRecord = ListJudgeRecord.Where(o => (o.Last2digitNumberSchool == aca.SchoolID % 100));
                }*/
            }
            if (Year > 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.CreatedAcademicYear == Year);
            }
            if (MarkTypeID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (SubjectID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (Semester != -1)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Semester == Semester));
            }
            // Quanglm Sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        ListJudgeRecord = ListJudgeRecord.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }

            if (Judgement != null && !Judgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Judgement.Contains(Judgement)));
            }
            if (ReTestJudgement != null && !ReTestJudgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.ReTestJudgement.Contains(ReTestJudgement)));
            }
            if (CreatedDate.HasValue)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                   o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (OrderNumber != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.OrderNumber == OrderNumber));
            }
            if (MarkTitle != "")
            {
                IQueryable<MarkType> IqMarkType = MarkTypeBusiness.All;
                ListJudgeRecord = ListJudgeRecord.Where(o => IqMarkType.Any(m => m.MarkTypeID == o.MarkTypeID && m.Title == MarkTitle));
            }
            if (Title != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Title == Title));
            }
            if (strMarkType != null && strMarkType != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All;
                ListJudgeRecord = ListJudgeRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID && c.IsActive==true));
            }

            return ListJudgeRecord;
        }

        #endregion Search
        #region Search No View
        public IQueryable<JudgeRecordBO> SearchJudgeRecordNoView(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            if (SchoolID == 0)
            {
                return null;
            }
            int JudgeRecordID = Utils.GetInt(dic, "JudgeRecordID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int MarkTypeID = Utils.GetInt(dic, "MarkTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester", -1);
            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");
            string Judgement = Utils.GetString(dic, "Judgement");
            string ReTestJudgement = Utils.GetString(dic, "ReTestJudgement");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            int OrderNumber = Utils.GetInt(dic, "OrderNumber");
            string MarkTitle = Utils.GetString(dic, "MarkTitle");
            string Title = Utils.GetString(dic, "Title");
            string strMarkType = Utils.GetString(dic, "StrContainMarkTitle");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");

            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            //tien hanh tim
            IQueryable<PupilProfile> IqPupilProfile = PupilProfileBusiness.AllNoTracking;
            IQueryable<JudgeRecordBO> ListJudgeRecord;
            ListJudgeRecord = from jud in VJudgeRecordBusiness.AllNoTracking
                              join pp in IqPupilProfile on jud.PupilID equals pp.PupilProfileID
                              where pp.IsActive && jud.Last2digitNumberSchool == partitionId
                              select new JudgeRecordBO
                              {
                                  Last2digitNumberSchool = jud.Last2digitNumberSchool,
                                  JudgeRecordID = jud.JudgeRecordID,
                                  PupilID = jud.PupilID,
                                  ClassID = jud.ClassID,
                                  SchoolID = jud.SchoolID,
                                  AcademicYearID = jud.AcademicYearID,
                                  CreatedAcademicYear = jud.CreatedAcademicYear,
                                  MarkTypeID = jud.MarkTypeID,
                                  SubjectID = jud.SubjectID,
                                  Semester = jud.Semester,
                                  Title = jud.Title,
                                  Judgement = jud.Judgement,
                                  ReTestJudgement = jud.ReTestJudgement,
                                  CreatedDate = jud.CreatedDate,
                                  ModifiedDate = jud.ModifiedDate,
                                  OrderNumber = jud.OrderNumber,
                              };
            if (JudgeRecordID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.JudgeRecordID == JudgeRecordID));
            }
            if (PupilID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.AcademicYearID == AcademicYearID));
                //AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                /*var aca = this.AcademicYearBusiness.All
                   .Where(p => p.AcademicYearID == AcademicYearID)
                   .Select(p => new { AcademicYearID = p.AcademicYearID, SchoolID = p.SchoolID, Year = p.Year })
                   .FirstOrDefault();
                int? createAcaYear = aca.Year;
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.CreatedAcademicYear == createAcaYear));
                // Phong khi khong tim kiem theo dk school_id
                if (SchoolID <= 0)
                {
                    ListJudgeRecord = ListJudgeRecord.Where(o => (o.Last2digitNumberSchool == aca.SchoolID % 100));
                }*/
            }
            if (Year > 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.CreatedAcademicYear == Year);
            }
            if (MarkTypeID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.MarkTypeID == MarkTypeID));
            }
            if (SubjectID != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.SubjectID == SubjectID));
            }
            if (Semester != -1)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Semester == Semester));
            }
            // Quanglm Sua
            if (PeriodID.HasValue)
            {
                if (PeriodID.Value > 0)
                {
                    if (string.IsNullOrWhiteSpace(strMarkType))
                    {
                        string tStrMarkType = MarkRecordBusiness.GetMarkTitle(PeriodID.Value);
                        ListJudgeRecord = ListJudgeRecord.Where(o => (tStrMarkType.Contains(o.Title + ",")));
                    }
                }
            }

            if (Judgement != null && !Judgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Judgement.Contains(Judgement)));
            }
            if (ReTestJudgement != null && !ReTestJudgement.Trim().Equals(string.Empty))
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.ReTestJudgement.Contains(ReTestJudgement)));
            }
            if (CreatedDate.HasValue)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.CreatedDate.Value.Day == CreatedDate.Value.Day &&
                    o.CreatedDate.Value.Month == CreatedDate.Value.Month && o.CreatedDate.Value.Year == CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                   o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year == ModifiedDate.Value.Year);
            }
            if (OrderNumber != 0)
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.OrderNumber == OrderNumber));
            }
            if (MarkTitle != "")
            {
                IQueryable<MarkType> IqMarkType = MarkTypeBusiness.All;
                ListJudgeRecord = ListJudgeRecord.Where(o => IqMarkType.Any(m => m.MarkTypeID == o.MarkTypeID && m.Title == MarkTitle));
            }
            if (Title != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (o.Title == Title));
            }
            if (strMarkType != null && strMarkType != "")
            {
                ListJudgeRecord = ListJudgeRecord.Where(o => (strMarkType.Contains(o.Title + ",")));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All;
                ListJudgeRecord = ListJudgeRecord.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID));
            }

            return ListJudgeRecord;
        }
        #endregion
    }
}