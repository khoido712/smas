/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
	public partial class PraiseTypeBusiness
	{
		/// <summary>
		/// Insert vao CSDL
		/// </summary>
		/// <param name="PraiseType"></param>
		/// <returns></returns>
		public override PraiseType Insert(PraiseType PraiseType)
		{
			// Kiem tra truong bat buoc
			Utils.ValidateRequire(PraiseType.Resolution, "PraiseType_Label_Resolution");
			// Kiem tra truong Resolution maxlength khong duoc vuot qua 100 ky tu
			Utils.ValidateMaxLength(PraiseType.Resolution, 100, "PraiseType_Label_Resolution");
			// Kiem tra thong tin truong hoc da co trong CSDL
			SchoolProfileBusiness.CheckAvailable(PraiseType.SchoolID, "SchoolProfile_Label_SchoolID", true);
			// Kiem tra trung ten va truong hoc
			string strSchoolID = PraiseType.SchoolID == null ? null : PraiseType.SchoolID.ToString();
			//this.CheckDuplicateCouple(PraiseType.Resolution, strSchoolID, GlobalConstants.LIST_SCHEMA, "PraiseType", "Resolution", "SchoolID", true, null, "PraiseType_Label_Resolution");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = PraiseType.SchoolID;
            SearchInfo["Resolution"] = PraiseType.Resolution;
            SearchInfo["IsActive"] = true;
            

            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["PraiseTypeID"] = PraiseType.PraiseTypeID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "PraiseType", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("PraiseType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            // Them vao CSDL            
			return base.Insert(PraiseType);
		}

		/// <summary>
		/// Cap nhat vao CSDL
		/// </summary>
		/// <param name="PraiseType"></param>
		/// <returns></returns>
		public override PraiseType Update(PraiseType PraiseType)
		{
			// Kiem tra truong bat buoc
			Utils.ValidateRequire(PraiseType.Resolution, "PraiseType_Label_Resolution");
			// Kiem tra truong Resolution maxlength khong duoc vuot qua 100 ky tu
			Utils.ValidateMaxLength(PraiseType.Resolution, 100, "PraiseType_Label_Resolution");
			// Kiem tra trung ten va truong hoc
			// Kiem tra thong tin truong hoc da co trong CSDL
			SchoolProfileBusiness.CheckAvailable(PraiseType.SchoolID, "SchoolProfile_Label_SchoolID", true);
			// Kiem tra trung ten va truong hoc
			string strSchoolID = PraiseType.SchoolID == null ? null : PraiseType.SchoolID.ToString();
			//this.CheckDuplicateCouple(PraiseType.Resolution, strSchoolID, GlobalConstants.LIST_SCHEMA, "PraiseType", "Resolution", "SchoolID", true, null, "PraiseType_Label_Resolution");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = PraiseType.SchoolID;
            SearchInfo["Resolution"] = PraiseType.Resolution;
            SearchInfo["IsActive"] = true;


            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["PraiseTypeID"] = PraiseType.PraiseTypeID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "PraiseType", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("PraiseType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            // Kiem tra ID da ton tai trong he thong
			new PraiseTypeBusiness(null).CheckAvailable(PraiseType.PraiseTypeID, "PraiseType_Label_Title", true);
			// Cap nhat vao CSDL  
			return base.Update(PraiseType);
		}

		/// <summary>
		/// Xoa hinh thuc khen thuong
		/// </summary>
		/// <param name="PraiseTypeID"></param>
		public void Delete(int PraiseTypeID)
		{
			// Kiem tra da ton tai trong  CSDL
			this.CheckAvailable(PraiseTypeID, "PraiseType_Label_Title", true);
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "PraiseType", PraiseTypeID, "PraiseType_Label_FailedDelete");
			// Xoa bang cach thiet lap IsActive = 0;
			this.Delete(PraiseTypeID, true);
		}

		/// <summary>
		/// Tim kiem hinh thuc khen thuong
		/// </summary>
		/// <param name="dicParam"></param>
		/// <returns></returns>
		public IQueryable<PraiseType> Search(IDictionary<string, object> dicParam)
		{
			string Resolution = Utils.GetString(dicParam, "Resolution");
			int SchoolID = Utils.GetInt(dicParam, "SchoolID");
			bool? IsActive = Utils.GetNullableBool(dicParam, "IsActive");
			IQueryable<PraiseType> listPraiseType = this.PraiseTypeRepository.All;
			if (!Resolution.Equals(string.Empty))
			{
				listPraiseType = listPraiseType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
			}
			if (IsActive.HasValue)
			{
				listPraiseType = listPraiseType.Where(x => x.IsActive == true);
			}
			if (SchoolID != 0)
			{
				listPraiseType = listPraiseType.Where(x => x.SchoolID == SchoolID || x.SchoolID == null);
			}
            //int count = listPraiseType != null ? listPraiseType.Count() : 0;
            //if (count == 0)
            //{
            //    return null;
            //}
			return listPraiseType;

		}

		/// <summary>
		/// Tim kiem theo thong tin truong hoc
		/// </summary>
		/// <param name="SchoolID"></param>
		/// <param name="SearchInfo"></param>
		/// <returns></returns>
		public IQueryable<PraiseType> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
		{
            if (SchoolID == 0)
            {
                return null;
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
		}
	}
}