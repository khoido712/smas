﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Loại điểm thi đua
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class EmulationCriteriaBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion
      



        #region insert
        /// <summary>
        ///Thêm mới Loại điểm thi đua
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertEmulationCriteria">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override EmulationCriteria Insert(EmulationCriteria insertEmulationCriteria)
        {


            //resolution rong
            Utils.ValidateRequire(insertEmulationCriteria.Resolution, "EmulationCriteria_Label_Resolution");

            Utils.ValidateMaxLength(insertEmulationCriteria.Resolution, RESOLUTION_MAX_LENGTH, "EmulationCriteria_Label_Resolution");
          

            //kiem tra ton tai - theo cap - can lam lai
            //this.CheckDuplicate(insertEmulationCriteria.Resolution, GlobalConstants.LIST_SCHEMA, "EmulationCriteria", "Resolution", true, 0, "EmulationCriteria_Label_Resolution");
            //this.CheckDuplicate(insertEmulationCriteria.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "EmulationCriteria", "AppliedLevel", true, 0, "EmulationCriteria_Label_ShoolID");
            this.CheckDuplicateCouple(insertEmulationCriteria.Resolution, insertEmulationCriteria.SchoolID.ToString(),
           GlobalConstants.LIST_SCHEMA, "EmulationCriteria", "Resolution", "SchoolID", true, 0, "EmulationCriteria_Label_Resolution");
           
            //kiem tra range
            if (insertEmulationCriteria.Mark == 0 || insertEmulationCriteria.Mark == 100)
            {
                throw new BusinessException("Common_Validate_MarkEmulation");
            }
            Utils.ValidateRange((int)insertEmulationCriteria.Mark, 0, 100, "EmulationCriteria_Column_Mark");

            //
            SchoolProfileBusiness.CheckAvailable(insertEmulationCriteria.SchoolID, "UserInfo_SchoolID");


            insertEmulationCriteria.IsActive = true;

          return  base.Insert(insertEmulationCriteria);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Loại điểm thi đua
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateEmulationCriteria">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được update vào DB thành công</returns>
        public override EmulationCriteria Update(EmulationCriteria updateEmulationCriteria)
        {

            //check avai
            new EmulationCriteriaBusiness(null).CheckAvailable(updateEmulationCriteria.EmulationCriteriaId, "EmulationCriteria_Label_EmulationCriteriaID");

            //resolution rong
            Utils.ValidateRequire(updateEmulationCriteria.Resolution, "EmulationCriteria_Label_Resolution");

            Utils.ValidateMaxLength(updateEmulationCriteria.Resolution, RESOLUTION_MAX_LENGTH, "EmulationCriteria_Label_Resolution");
          

            //kiem tra ton tai - theo cap - can lam lai
            // this.CheckDuplicate(updateEmulationCriteria.Resolution, GlobalConstants.LIST_SCHEMA, "EmulationCriteria", "Resolution", true, updateEmulationCriteria.EmulationCriteriaID, "EmulationCriteria_Label_Resolution");

            this.CheckDuplicateCouple(updateEmulationCriteria.Resolution, updateEmulationCriteria.SchoolID.ToString(),
                GlobalConstants.LIST_SCHEMA, "EmulationCriteria", "Resolution", "SchoolID", true, updateEmulationCriteria.EmulationCriteriaId, "EmulationCriteria_Label_Resolution");

            // this.CheckDuplicate(updateEmulationCriteria.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "EmulationCriteria", "AppliedLevel", true, updateEmulationCriteria.EmulationCriteriaID, "EmulationCriteria_Label_ShoolID");

            //kiem tra range
            if (updateEmulationCriteria.Mark == 0 || updateEmulationCriteria.Mark == 100)
            {
                throw new BusinessException("Common_Validate_MarkEmulation");
            }
            Utils.ValidateRange((int)updateEmulationCriteria.Mark, 0, 100, "EmulationCriteria_Column_Mark");

            //check 
            SchoolProfileBusiness.CheckAvailable(updateEmulationCriteria.SchoolID, "UserInfo_SchoolID");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu EmulationCriteriaID ->vao csdl lay EmulationCriteria tuong ung roi 
            //so sanh voi updateEmulationCriteria.SchoolID


            //.................



            updateEmulationCriteria.IsActive = true;

            return base.Update(updateEmulationCriteria);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Loại điểm thi đua
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="EmulationCriteriaId">ID Loại điểm thi đua</param>
        public void Delete(int EmulationCriteriaId, int SchoolID)
        {
            //check avai
            new EmulationCriteriaBusiness(null).CheckAvailable(EmulationCriteriaId, "EmulationCriteria_Label_EmulationCriteriaID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "EmulationCriteria",EmulationCriteriaId, "EmulationCriteria_Label_EmulationCriteriaID");

           

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu EmulationCriteriaID ->vao csdl lay EmulationCriteria tuong ung roi 
            //so sanh voi updateEmulationCriteria.SchoolID
           
            bool isCompatible = new EmulationCriteriaRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EmulationCriteria", new Dictionary<string, object>()
            {
                {"EmulationCriteriaID", EmulationCriteriaId},
                {"SchoolID", SchoolID}
            }, null);
            if (!isCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            base.Delete(EmulationCriteriaId, true);
            //.................


        }
        #endregion

      

        #region Search
        /// <summary>
        /// Tìm kiếm Loại điểm thi đua
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<EmulationCriteria> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");

            int SchoolID = Utils.GetInt(dic,"SchoolID");
            bool? isActive =Utils.GetIsActive(dic,"IsActive");
            IQueryable<EmulationCriteria> lsEmulationCriteria = this.EmulationCriteriaRepository.All.OrderBy(o => o.Resolution);

         




             if (isActive.HasValue)
            {
                lsEmulationCriteria = lsEmulationCriteria.Where(em => (em.IsActive == isActive));
            }

            if (resolution.Trim().Length != 0)
            {

                lsEmulationCriteria = lsEmulationCriteria.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (SchoolID != 0)
            {
                lsEmulationCriteria = lsEmulationCriteria.Where(em => (em.SchoolID == SchoolID));
            }

         
            return lsEmulationCriteria;
        }


        /// <summary>
        /// Tìm kiếm Loại điểm thi đua theo trường
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="SchoolID">id cua truong can tim kiem</param>
        /// <param name="dic">cac tham so</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<EmulationCriteria> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                if (dic == null)
                {
                    dic = new Dictionary<string, object>();
                }
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }
        #endregion
        
    }
}