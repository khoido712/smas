﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class StatisticSubcommitteeOfTertiaryBusiness
    {

        /// <summary>
        /// Searches the specified search info.
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        ///<author>hath</author>
        /// <date>1/18/2013</date>
        public IQueryable<StatisticSubcommitteeOfTertiary> Search(IDictionary<string, object> SearchInfo)
        {
            //- SchoolID: default = 0; 0 => Tìm kiếm all
            //- AcademicYearID: default = 0; 0 => Tìm kiếm All
            //- SubCommitteeID: default = 0; 0 => Tìm kiếm All
            //- EducationLevelID : default = 0; 0 => Tìm kiếm All
            //- SpecicalType: default = 0; 0 => Tìm kiếm All
            //- Year: default = 0; 0 => Tìm kiếm All, !=0 tìm kiếm theo AcademicYear.Year = Year
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SpecicalType = Utils.GetInt(SearchInfo, "SpecicalType");
            int Year = Utils.GetInt(SearchInfo, "Year");

            IQueryable<StatisticSubcommitteeOfTertiary> lst = StatisticSubcommitteeOfTertiaryRepository.All;
            if (SchoolID > 0)
            {
                lst = lst.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                lst = lst.Where(o => o.AcademicYearID==AcademicYearID);
            }
            if (SubCommitteeID > 0)
            {
                lst = lst.Where(o => o.SubCommitteeID == SubCommitteeID);
            }
            if (EducationLevelID > 0)
            {
                lst = lst.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SpecicalType > 0)
            {
                lst = lst.Where(o => o.SpecicalType == SpecicalType);
            }
            if (Year > 0)
            {
                lst = lst.Where(o => o.AcademicYear.Year == Year);
            }
            return lst;
        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{StatisticSubcommitteeOfTertiary}
        /// </returns>
        ///<author>hath</author>
        /// <date>1/18/2013</date>
        public IQueryable<StatisticSubcommitteeOfTertiary> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// InsertOrUpdate
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="lstStatisticSubCommittee">The LST statistic sub committee.</param>
        ///<author>hath</author>
        /// <date>1/18/2013</date>
        public void InsertOrUpdate(int SchoolID, int AcademicYearID, List<StatisticSubcommitteeOfTertiary> lstStatisticSubCommittee)
        { 
            //SubCommitteeID: FK(SubCommittee)
            

            //Tìm kiếm trong bảng StatisticSubCommitteeOfTertiary theo SchooID và AcademicYearID. Xoá hết các bản ghi tìm được.
            //Cập nhật SchoolID, AcademicYearID vào trong lstStatisticSubCommittee
            //Insert lstStatisticSubCommittee vào bảng StatisticSubCommitteeOfTertiary
            IQueryable<StatisticSubcommitteeOfTertiary> lsStatisticSubcommitteeOfTertiary = StatisticSubcommitteeOfTertiaryRepository.All.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID);
            if (lsStatisticSubcommitteeOfTertiary.Count() > 0)
            {
                List<StatisticSubcommitteeOfTertiary> lstStatisticSubcommitteeOfTertiary = lsStatisticSubcommitteeOfTertiary.ToList();
                base.DeleteAll(lstStatisticSubcommitteeOfTertiary);
            }
            foreach (StatisticSubcommitteeOfTertiary ssot in lstStatisticSubCommittee)
            {
                if (ssot.SubCommitteeID != -1 && ssot.SubCommitteeID == 0)
                {
                    SubCommitteeBusiness.CheckAvailable(ssot.SubCommitteeID, "SubCommittee_Label_SubCommitteeID", false);
                }
                //EducationLevelID: FK(EducationLevel)
                EducationLevelBusiness.CheckAvailable(ssot.EducationLevelID, "EducationLevel_Label_EducationLevelID", false);
                //TotalClass, TotalPupil in (0, 10000)
                if (ssot.TotalClass != null)
                {
                    Utils.ValidateRange(ssot.TotalClass.Value, 0, 1000, "StatisticSubcommitteeOfTertiary_Label_TotalClass");
                }
                if (ssot.TotalPupil != null)
                {
                    Utils.ValidateRange(ssot.TotalPupil.Value, 0, 1000, "StatisticSubcommitteeOfTertiary_Label_TotalPupil");
                }

                ssot.SchoolID = SchoolID;
                ssot.AcademicYearID = AcademicYearID;
                base.Insert(ssot);
            }
        }
    }
}