﻿
using Payment.ServiceLoader.MobileAPI;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;

namespace SMAS.Business.Business
{
    public partial class AlertMessageBusiness
    {
        #region Validate
        public void CheckALertMessageDuplicate(AlertMessage alertMessage)
        {
            /*IDictionary<string, object> expDic = null;
            if (alertMessage.AlertMessageID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["AlertMessageID"] = alertMessage.AlertMessageID;
            }
            bool AlertMessageCompatible = this.repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "AlertMessage",
                   new Dictionary<string, object>()
                {
                    {"Title",alertMessage.Title},
                    {"IsActive",true}
                }, expDic);
            if (AlertMessageCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("AlertMessage_Label_Title");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }*/
            var iqAlert = AlertMessageRepository.AllNoTracking.Where(c => c.IsActive);
            if (alertMessage.UnitID > 0)
            {
                iqAlert = iqAlert.Where(c => c.UnitID == alertMessage.UnitID);
            }
            else
            {
                iqAlert = iqAlert.Where(c => c.UnitID == null);
            }
            if (!String.IsNullOrEmpty(alertMessage.Title))
            {
                iqAlert = iqAlert.Where(c => c.Title.ToUpper() == alertMessage.Title.ToUpper());
            }
            if (alertMessage.AlertMessageID > 0)
            {
                iqAlert = iqAlert.Where(c => c.AlertMessageID != alertMessage.AlertMessageID);
            }
            if (iqAlert.Any())
            {
                List<object> listParam = new List<object>();
                listParam.Add("AlertMessage_Label_Title");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }

        }

        public void CheckAlertMessageIDAvailable(int alertMessageID)
        {
            bool AvailableAlertMessageIDAvailable = this.repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "AlertMessage",
                   new Dictionary<string, object>()
                {
                    {"AlertMessageID",alertMessageID}
                    
                }, null);
            if (!AvailableAlertMessageIDAvailable)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion
        public IQueryable<AlertMessage> Search(IDictionary<string, object> dic)
        {
            string title = Utils.GetString(dic, "Title");
            string fileUrl = Utils.GetString(dic, "FileUrl");
            DateTime? publishedDate = Utils.GetDateTime(dic, "PublishedDate");
            bool isActive = Utils.GetBool(dic, "IsActive");
            int alertMessageID = Utils.GetInt(dic, "AlertMessageID");
            int? UnitID = Utils.GetInt(dic, "UnitID");
            int TypeID = Utils.GetInt(dic, "TypeID");
            bool isAdmin = Utils.GetBool(dic, "isAdmin", false);
            bool ExcludeGlobal = Utils.GetBool(dic, "ExcludeGlobal");
            IQueryable<AlertMessage> lstAlertMessage = this.AlertMessageRepository.All.Where(o => o.IsActive == true);
            if (ExcludeGlobal)
            {
                lstAlertMessage = lstAlertMessage.Where(o => !o.IsGlobal.HasValue || (o.IsGlobal.HasValue && o.IsGlobal.Value == false));
            }

            if (!string.IsNullOrEmpty(title))
            {
                lstAlertMessage = lstAlertMessage.Where(o => o.Title.ToLower().Contains(title.ToLower()));
            }

            if (!string.IsNullOrEmpty(fileUrl))
            {
                lstAlertMessage = lstAlertMessage.Where(o => o.FileUrl == fileUrl);
            }

            if (publishedDate.HasValue)
            {
                lstAlertMessage = lstAlertMessage.Where(o => EntityFunctions.TruncateTime(o.PublishedDate) == EntityFunctions.TruncateTime(publishedDate));
            }

            if (alertMessageID > 0)
            {
                lstAlertMessage = lstAlertMessage.Where(o => o.AlertMessageID == alertMessageID);
            }

            if (UnitID.Value > 0)
            {
                lstAlertMessage = lstAlertMessage.Where(o => o.UnitID == UnitID);
            }

            if (isAdmin)
            {
                lstAlertMessage = lstAlertMessage.Where(o => o.UnitID == null);
            }

            if (TypeID > 0)
            {
                lstAlertMessage = lstAlertMessage.Where(p => p.TypeID == TypeID);
            }
            return lstAlertMessage;
        }

        public void Insert(AlertMessage alertMessage)
        {
            //Check Require, max length
            ValidationMetadata.ValidateObject(alertMessage);

            //Check duplicate
            CheckALertMessageDuplicate(alertMessage);
            //Ngày ban hành > ngày hiện tại
            Utils.ValidatePastDate(alertMessage.PublishedDate, "AlertMessage_Label_PublishedDate");

            alertMessage.IsActive = true;
            base.Insert(alertMessage);
        }
        public void Update(AlertMessage alertMessage)
        {
            //Check duplicate
            CheckALertMessageDuplicate(alertMessage);

            //Check available
            CheckAlertMessageIDAvailable(alertMessage.AlertMessageID);

            //Check require, max length
            ValidationMetadata.ValidateObject(alertMessage);

            //Check not compatible
            bool AlertMessageIDCompatible = this.repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "AlertMessage",
                   new Dictionary<string, object>()
                {
                    {"AlertMessageID",alertMessage.AlertMessageID},
                    {"IsActive",true}
                }, null);
            if (!AlertMessageIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            base.Update(alertMessage);
        }
        public void Delete(int AlertMessageID)
        {
            //Check  Available
            CheckAlertMessageIDAvailable(AlertMessageID);

            //Check not compatible
            bool AlertMessageIDCompatible = this.repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "AlertMessage",
                   new Dictionary<string, object>()
                {
                    {"AlertMessageID",AlertMessageID},
                    {"IsActive",true}
                }, null);
            if (!AlertMessageIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Check Using
            this.CheckConstraints(GlobalConstants.ADM_SCHEMA, "AlertMessage", AlertMessageID, "Bản tin");

            base.Delete(AlertMessageID, true);
        }


        public void PushAlertMessageScheduler()
        {
            var queryAlertMessage = from amr in AlertMessageRepository.All                              
                                    where (amr.IsActive && (amr.UnitID == null || amr.UnitID > 0) && amr.IsPublish 
                                            && amr.PublishedDate.Day == DateTime.Now.Day
                                            && amr.PublishedDate.Month == DateTime.Now.Month
                                            && amr.PublishedDate.Year == DateTime.Now.Year)
                                    select amr;
            foreach (var alertMessage in queryAlertMessage)
            {
                var listDeviceToken = new List<string>();
                if (alertMessage.UnitID == null) // Notification by admin system
                {
                    listDeviceToken = this.GetListDeviceToken(0);
                }
                else
                {
                    var _schoolProfile = SchoolProfileRepository.Find(alertMessage.UnitID);
                    if (_schoolProfile != null)
                    {
                        listDeviceToken = this.GetListDeviceToken(int.Parse(alertMessage.UnitID.ToString()));
                    }
                }
                this.SendNotificationToAPI(listDeviceToken, alertMessage.ContentMessage, alertMessage.Title);
            }
        }

        public List<string> GetListDeviceToken(int SchoolID) 
        {
            var listDeviceToken = new List<string>();
            var queryAuthenticationToken = AuthenticationTokenRepository.All;
            IQueryable<AuthenticationToken> queryAlertMessage = null;
            var listAdminID = new List<int?>();
            if (SchoolID > 0)
            {
                queryAlertMessage = from emp in EmployeeRepository.All // on am.UnitID equals emp.SchoolID
                                    join ua in UserAccountRepository.All on emp.EmployeeID equals ua.EmployeeID
                                    join auth in queryAuthenticationToken on ua.UserAccountID equals auth.UserAccountID
                                    where SchoolID > 0 && emp.SchoolID == SchoolID && auth.DeviceToken != null
                                    select auth;
                var objSchool = SchoolProfileRepository.Find(SchoolID);
                if (objSchool != null)
                    listAdminID.Add(objSchool.AdminID);
            }
            else // Notification by admin system
            {
                queryAlertMessage = from emp in EmployeeRepository.All 
                                    join ua in UserAccountRepository.All on emp.EmployeeID equals ua.EmployeeID
                                    join auth in queryAuthenticationToken on ua.UserAccountID equals auth.UserAccountID
                                    where auth.DeviceToken != null
                                    select auth;
            }
            listDeviceToken = queryAlertMessage.Select(x => x.DeviceToken).ToList();
            listDeviceToken.AddRange(queryAuthenticationToken.Where(x => listAdminID.Contains(x.UserAccountID)).Select(x => x.DeviceToken));
            return listDeviceToken;
        }

        public string SendNotificationToAPI(List<string> deviceToken, string body, string title)
        {
            UserModuleClient client = new UserModuleClient();
            string[] arr = deviceToken.ToArray<string>();
            logger.Info("Start send to API. Token: " + string.Join(",", deviceToken));
            string result = client.SendNotificationFromFirebaseCloud(new Payment.ServiceLoader.MobileAPI.SendNotificationFromFirebaseCloudRequest
            {
                body=body,
                deviceToken= arr,
                title=title,
            });

            logger.Info("End send to API " + result);
            return result;
        }
    }
}
