﻿using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
   public partial class DOETExamSubjectBusiness
    {
       public IQueryable<DOETExamSubject> Search(IDictionary<string,object> dic)
       {
           int examinationsId = Utils.GetInt(dic,"ExaminationsID");
           int examGroupId = Utils.GetInt(dic,"GroupID");
           List<int> listGroupId = Utils.GetIntList(dic,"ListGroupID");
           IQueryable<DOETExamSubject> iquery = DOETExamSubjectBusiness.All;
           if (examinationsId > 0)
           {
               iquery = iquery.Where(p => p.ExaminationsID == examinationsId);
           }
           if (examGroupId > 0)
           {
               iquery = iquery.Where(p => p.ExamGroupID == examGroupId);
           }
           if (listGroupId != null && listGroupId.Count > 0)
           {
               iquery = iquery.Where(p => listGroupId.Contains(p.ExamGroupID));
           }
           return iquery;
       }
       public List<DOETExamSubjectBO> GetListExamSubject(IDictionary<string,object> dic)
       {
           int ExaminationsID = Utils.GetInt(dic, "ExaminationsID");
           int ExamGroupID = Utils.GetInt(dic, "ExamGroupID");
           List<int> listGroupId = Utils.GetIntList(dic, "ListGroupID");
           IQueryable<DOETExamSubjectBO> iQueryExamSubjectBO = (from es in DOETExamSubjectBusiness.All
                               join s in DOETSubjectBusiness.All on es.DOETSubjectID equals s.DOETSubjectID                             
                               select new DOETExamSubjectBO
                               {
                                   ExamSubjectID=es.ExamSubjectID,
                                   DOETSubjectID = es.DOETSubjectID,
                                   DOETSubjectCode = s.DOETSubjectCode,
                                   DOETSubjectName = s.DOETSubjectName,
                                   ExaminationsID = es.ExaminationsID,
                                   ExamGroupID = es.ExamGroupID
                               });
           if (ExaminationsID > 0)
           {
               iQueryExamSubjectBO = iQueryExamSubjectBO.Where(p => p.ExaminationsID == ExaminationsID);
           }
           if (ExamGroupID > 0)
           {
               iQueryExamSubjectBO = iQueryExamSubjectBO.Where(p => p.ExamGroupID == ExamGroupID);
           }
           if (listGroupId != null && listGroupId.Count > 0)
           {
               iQueryExamSubjectBO = iQueryExamSubjectBO.Where(p => listGroupId.Contains(p.ExamGroupID));
           }
           return iQueryExamSubjectBO.ToList();
       }

    }
}
