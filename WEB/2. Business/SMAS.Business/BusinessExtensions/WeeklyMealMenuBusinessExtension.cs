/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class WeeklyMealMenuBusiness
    {
        public IQueryable<WeeklyMealMenu> Search(IDictionary<string, object> dic)
        {
            int schoolId = Utils.GetInt(dic, "SchoolID");
            int academicYearId = Utils.GetInt(dic, "AcademicYearID");
            int classId = Utils.GetInt(dic, "ClassID");
            DateTime? dateFrom = Utils.GetDateTime(dic, "DateFrom");
            DateTime? dateTo = Utils.GetDateTime(dic, "DateTo");

            var query = this.repository.All;

            if (schoolId != 0)
            {
                int partition = schoolId % 100;
                query = query.Where(o => o.SchoolID == schoolId && o.Last2digitNumberSchool == partition);
            }

            if (academicYearId != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearId);
            }

            if (classId != 0)
            {
                query = query.Where(o => o.ClassID == classId);
            }

            if (dateFrom.HasValue)
            {
                query = query.Where(o => o.DateOfMeal >= dateFrom);
            }

            if (dateTo.HasValue)
            {
                query = query.Where(o => o.DateOfMeal <= dateTo);
            }

            return query;
        }
    }
}
