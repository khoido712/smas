﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.VTUtils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SMAS.Business.Business
{
    public partial class LockRatedCommentPupilBusiness
    {
        public IQueryable<LockRatedCommentPupil> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");
            int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstEvaluationID = Utils.GetIntList(dic, "lstEvaluationID");

            IQueryable<LockRatedCommentPupil> iqQuery = LockRatedCommentPupilBusiness.All;
            if (SchoolID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartitionID);
            }
            if (AcademicYearID != 0)
            {
                iqQuery = iqQuery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iqQuery = iqQuery.Where(p => p.ClassID == ClassID);
            }
            if (SubjectID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SubjectID == SubjectID);
            }
            if (EvaluationID != 0)
            {
                if (EvaluationID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
                {
                    iqQuery = iqQuery.Where(p => p.EvaluationID == EvaluationID);
                }
                else
                {
                    iqQuery = iqQuery.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY);
                }
            }
            if (lstSubjectID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            if (lstClassID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstClassID.Contains(p.ClassID));
            }

            if (lstEvaluationID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstEvaluationID.Contains(p.EvaluationID));
            }
            return iqQuery;
        }

        public string SaveLockRatedCommentPupil(int submitType, int schoolId, int educationId, int classId, int academicYearId, Dictionary<int, List<string>> subjectLockTitleDic, List<string> talentLst, List<string> ethicLst, List<int> lstSubjectId)
        {
            string res = "";
            List<int> lstClassId = new List<int>();
            if (submitType == 0)
            {
                lstClassId.Add(classId);
            }
            if (submitType == 1)
            {
                lstClassId = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearId && o.IsActive == true && o.SchoolID == schoolId && o.EducationLevelID == educationId).Select(p => p.ClassProfileID).ToList();
            }
            if (submitType == 2)
            {
                lstClassId = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearId && o.IsActive == true && o.SchoolID == schoolId).Select(p => p.ClassProfileID).ToList();
            }

            try
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;

                //xóa cấu hình
                var lstDelete = this.context.LockRatedCommentPupil.Where(o => o.AcademicYearID == academicYearId && o.SchoolID == schoolId && lstClassId.Contains(o.ClassID));

                //xóa cấu hình năng lực và phẩm chất
                List<LockRatedCommentPupil> lstDeleteNLPC = lstDelete.Where(o => o.EvaluationID == 2 || o.EvaluationID == 3).ToList();
                if (lstDeleteNLPC.Count > 0)
                {
                    LockRatedCommentPupilBusiness.DeleteAll(lstDeleteNLPC);
                }

                //xóa cấu hình môn học
                List<LockRatedCommentPupil> lstDeleteSubject = new List<LockRatedCommentPupil>();
                if (lstSubjectId != null && lstSubjectId.Count >= subjectLockTitleDic.Count)
                {
                    lstDeleteSubject = lstDelete.Where(o => lstSubjectId.Contains(o.SubjectID)).ToList();
                    if (lstDeleteSubject.Count > 0)
                    {
                        LockRatedCommentPupilBusiness.DeleteAll(lstDeleteSubject);
                    }
                }

                //Lưu cấu hình mới
                List<LockRatedCommentPupil> lstLockRatedCommentPupil = new List<LockRatedCommentPupil>();
                int twoLastdigitSchool = UtilsBusiness.GetPartionId(schoolId);
                LockRatedCommentPupil addlock = null;

                foreach (var itemId in lstClassId)
                {
                    //add lock for subject
                    if (subjectLockTitleDic.Count > 0)
                    {

                        foreach (var item in subjectLockTitleDic)
                        {
                            int subjectId = item.Key;
                            IEnumerable<string> lockSubjectDetail = item.Value;

                            addlock = new LockRatedCommentPupil();
                            addlock.LockRatedCommentPupilID = LockRatedCommentPupilBusiness.GetNextSeq<int>();

                            addlock.AcademicYearID = academicYearId;
                            addlock.SchoolID = schoolId;
                            addlock.SubjectID = subjectId;
                            addlock.ClassID = itemId;
                            addlock.LastDigitSchoolID = twoLastdigitSchool;
                            addlock.EvaluationID = 1;

                            if (IsFourOrFive(itemId))
                            {
                                if (!IsToanOrTiengViet(subjectId))
                                {
                                    item.Value.Remove("KTGK1");
                                    item.Value.Remove("KTGK2");
                                }
                            }
                            else
                            {
                                item.Value.Remove("KTGK1");
                                item.Value.Remove("KTGK2");
                            }


                            string configSubject = String.Join(",", item.Value);
                            addlock.LockTitle = configSubject;

                            addlock.CreateTime = DateTime.Now;

                            lstLockRatedCommentPupil.Add(addlock);
                        }
                    }

                    //add lock for talent
                    if (talentLst.Count > 0)
                    {
                        addlock = new LockRatedCommentPupil();
                        addlock.LockRatedCommentPupilID = LockRatedCommentPupilBusiness.GetNextSeq<int>();

                        addlock.AcademicYearID = academicYearId;
                        addlock.SchoolID = schoolId;
                        addlock.ClassID = itemId;
                        addlock.SubjectID = 0;
                        addlock.LastDigitSchoolID = twoLastdigitSchool;
                        addlock.EvaluationID = 2;
                        addlock.LockTitle = String.Join(",", talentLst); ;
                        addlock.CreateTime = DateTime.Now;

                        lstLockRatedCommentPupil.Add(addlock);
                    }

                    //add lock for ethic
                    if (ethicLst.Count > 0)
                    {
                        addlock = new LockRatedCommentPupil();
                        addlock.LockRatedCommentPupilID = LockRatedCommentPupilBusiness.GetNextSeq<int>();

                        addlock.AcademicYearID = academicYearId;
                        addlock.SchoolID = schoolId;
                        addlock.ClassID = itemId;
                        addlock.SubjectID = 0;
                        addlock.LastDigitSchoolID = twoLastdigitSchool;
                        addlock.EvaluationID = 3;
                        addlock.LockTitle = String.Join(",", ethicLst);
                        addlock.CreateTime = DateTime.Now;

                        lstLockRatedCommentPupil.Add(addlock);
                    }
                }

                if (lstLockRatedCommentPupil != null && lstLockRatedCommentPupil.Count > 0)
                {
                    for (int i = 0; i < lstLockRatedCommentPupil.Count; i++)
                    {
                        LockRatedCommentPupilBusiness.Insert(lstLockRatedCommentPupil[i]);
                    }
                }

                LockRatedCommentPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("submitType={0}, schoolId={1},educationId={2},classId={3},academicYearId={4}", submitType, schoolId, educationId, classId, academicYearId);
                LogExtensions.ErrorExt(logger, DateTime.Now,"SaveLockRatedCommentPupil", paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

            return res;
        }

        private bool IsToanOrTiengViet(int subjectId)
        {
            var subject = SubjectCatBusiness.Find(subjectId);
            if (subject != null)
            {
                if (subject.Abbreviation == "Tt" || subject.Abbreviation == "TV")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool IsFourOrFive(int classId)
        {
            var cls = ClassProfileBusiness.All.FirstOrDefault(o => o.ClassProfileID == classId);
            if (cls != null)
            {
                if (cls.EducationLevelID == 4 || cls.EducationLevelID == 5)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public string GetLockTitleVNEN(IDictionary<string,object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");
            List<int> lstEvaluationID = Utils.GetIntList(dic, "lstEvaluationID");
            string titleLock = string.Empty;
            IQueryable<LockRatedCommentPupil> iquery = this.LockRatedCommentPupilBusiness.All;
            if (AcademicYearID > 0)
            {
                iquery = iquery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (SchoolID > 0)
            {
                iquery = iquery.Where(p => p.SchoolID == SchoolID);
            }
            if (ClassID > 0)
            {
                iquery = iquery.Where(p => p.ClassID == ClassID);
            }
            if (SubjectID > 0)
            {
                iquery = iquery.Where(p => p.SubjectID == SubjectID);
            }
            if (EvaluationID > 0)
            {
                iquery = iquery.Where(p => p.EvaluationID == EvaluationID);
            }
            if (lstEvaluationID.Count > 0)
            {
                iquery = iquery.Where(p => lstEvaluationID.Contains(p.EvaluationID));
            }
            List<LockRatedCommentPupil> lstLockRate = iquery.ToList();
            LockRatedCommentPupil objLockRate = null;
            for (int i = 0; i < lstLockRate.Count; i++)
            {
                objLockRate = lstLockRate[i];
                titleLock += objLockRate.LockTitle + ",";
            }
            return titleLock;
        }

        public void SaveDataLockVNEN(FormCollection frm, IDictionary<string, object> dic, LockRatedCommentPupilBO objLockRatedBO)
        {
            
            this.LockRatedCommentPupilBusiness.SetAutoDetectChangesEnabled(false);
            this.ClassProfileBusiness.SetAutoDetectChangesEnabled(false);
            this.SubjectCatBusiness.SetAutoDetectChangesEnabled(false);
            this.ClassSubjectBusiness.SetAutoDetectChangesEnabled(false);
            this.SchoolSubjectBusiness.SetAutoDetectChangesEnabled(false);

            int schoolId = Utils.GetInt(dic, "SchoolID");
            int academicYear = Utils.GetInt(dic, "AcademicYearID");
            int appliedAllClass = Utils.GetInt(dic, "AppliedAllClass");
            int educationID = Utils.GetInt(dic, "EducationID");
            int classID = Utils.GetInt(dic, "ClassID");
            List<int> lstEducationID = Utils.GetIntList(dic, "lstEducationID");
            List<int> ListEvaluation = Utils.GetIntList(dic, "lstEvaluationID");
          
            if (appliedAllClass == 1 || appliedAllClass == 2)
            {
                lstEducationID = lstEducationID.Where(x => x == educationID).ToList();
            }
            
            List<ClassProfile> lstClass = new List<ClassProfile>();
            List<int> lstClassID = new List<int>();

            lstClass = this.ClassProfileBusiness.Search(dic).Where(x => lstEducationID.Contains(x.EducationLevelID))
                                                .Where(x => x.IsVnenClass.HasValue && x.IsVnenClass.Value == true).ToList();

            if (appliedAllClass == 1)
            {
                lstClass = lstClass.Where(x => x.ClassProfileID == classID).ToList();
            }
            lstClassID = lstClass.Select(x => x.ClassProfileID).Distinct().ToList();
            dic["ListClassID"] = lstClassID;
            dic["ClassID"] = 0;
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.Search(dic).Where(x => x.IsSubjectVNEN.HasValue && x.IsSubjectVNEN.Value == true).ToList();
            List<int> lstSubjectId = lstClassSubject.Select(x => x.SubjectID).Distinct().ToList();

           /* List<SubjectCatBO> lstSubject = ListSubjectByEducation(lstEducationID)
                                            .Where(x => lstSubjectId.Contains(x.SubjectCatID))
                                            .Distinct().ToList();*/
            List<int> lstSubjectOfClassSelected = lstClassSubject.Where(x => x.ClassID == classID).Select(x => x.SubjectID).Distinct().ToList();

            #region //Xóa dữ liệu cũ
            lstSubjectOfClassSelected.Add(0);
            dic["lstClassID"] = lstClassID;
            dic["lstSubjectID"] = lstSubjectOfClassSelected;
            List<LockRatedCommentPupil> lstDB = this.Search(dic).ToList();
            if (lstDB.Count > 0)
            {
                foreach (var item in lstDB)
                {
                    LockRatedCommentPupilBusiness.Delete(item.LockRatedCommentPupilID);
                }
            }
            #endregion

            int lastDigit = schoolId % 100;

            List<LockRatedCommentPupil> lstResult = new List<LockRatedCommentPupil>();
            LockRatedCommentPupil objResult = null;

            MonthInYearLockBO objMonthHK = null;
            List<string> lstLock = new List<string>();
            List<ClassProfile> lstCP = new List<ClassProfile>();
            List<ClassSubject> lstSubjectOfClass = new List<ClassSubject>();
            List<int> lstSubjectIdOfClass = new List<int>();
            int subjectId = 0;
            for (int e = 0; e < lstEducationID.Count(); e++)
            {
                lstCP = lstClass.Where(x => x.EducationLevelID == lstEducationID[e]).ToList();
                #region //Danh sách lớp học
                for (int i = 0; i < lstCP.Count(); i++)
                {
                    lstSubjectOfClass = lstClassSubject.Where(x => x.ClassID == lstCP[i].ClassProfileID).ToList();
                    lstSubjectIdOfClass = lstSubjectOfClass.Select(x => x.SubjectID).ToList();
                    #region //Đánh giá môn học
                    for (int j = 0; j < lstSubjectIdOfClass.Count(); j++)
                    {
                        subjectId = lstSubjectIdOfClass[j];
                        objResult = new LockRatedCommentPupil();
                        objResult = CreateObject(lstCP[i].ClassProfileID, schoolId, academicYear, subjectId, lastDigit, 4);
                        // Duyệt HK 1
                        for (int k = 0; k < objLockRatedBO.ListMonthHK1.Count; k++)
                        {
                            objMonthHK = objLockRatedBO.ListMonthHK1[k];
                            if (!"on".Equals(frm[string.Format("checkHK1_{0}_{1}", objMonthHK.MonthName, subjectId)]))
                            {
                                continue;
                            }
                            lstLock.Add((objMonthHK.MonthName + "HK1"));
                        }

                        // Cuối HK 1
                        if ("on".Equals(frm[string.Format("checkHK1Late_{0}", subjectId)]))
                        {
                            lstLock.Add(("CHK1"));
                        }

                        // Duyệt HK 2
                        for (int k = 0; k < objLockRatedBO.ListMonthHK2.Count; k++)
                        {
                            objMonthHK = objLockRatedBO.ListMonthHK2[k];
                            if (!"on".Equals(frm[string.Format("checkHK2_{0}_{1}", objMonthHK.MonthName, subjectId)]))
                            {
                                continue;
                            }
                            lstLock.Add((objMonthHK.MonthName + "HK2"));
                        }

                        // Cuối HK 2
                        if ("on".Equals(frm[string.Format("checkHK2Late_{0}", subjectId)]))
                        {
                            lstLock.Add(("CHK2"));
                        }

                        #region//Điểm Kiểm tra định kỳ
                        // GK 1
                        if ("on".Equals(frm[string.Format("checkDKGK1_{0}", subjectId)]))
                        {
                            lstLock.Add("DKGK1");
                        }
                        // CK 1
                        if ("on".Equals(frm[string.Format("checkDKCK1_{0}", subjectId)]))
                        {
                            lstLock.Add("DKCK1");
                        }
                        // GK 2
                        if ("on".Equals(frm[string.Format("checkDKGK2_{0}", subjectId)]))
                        {
                            lstLock.Add("DKGK2");
                        }
                        // CK 2
                        if ("on".Equals(frm[string.Format("checkDKCK2_{0}", subjectId)]))
                        {
                            lstLock.Add("DKCK2");
                        }
                        #endregion

                        objResult.LockTitle = string.Join(",", lstLock.ToArray());

                        if (!string.IsNullOrEmpty(objResult.LockTitle))
                            lstResult.Add(objResult);

                        lstLock = new List<string>();
                    }
                    #endregion

                    #region // Nhận xét năng lực/phẩm chất
                    objResult = new LockRatedCommentPupil();
                    objResult = CreateObject(lstCP[i].ClassProfileID,
                        schoolId, academicYear, 0, lastDigit, 5);

                    if ("on".Equals(frm["lockCommentKH1"]))
                    {
                        lstLock.Add("NXHK1");
                    }
                    if (("on".Equals(frm["lockCommentKH2"])))
                    {
                        lstLock.Add("NXHK2");
                    }
                    objResult.LockTitle = string.Join(",", lstLock.ToArray());
                    if (!string.IsNullOrEmpty(objResult.LockTitle))
                    {
                        lstResult.Add(objResult);
                        lstLock = new List<string>();
                    }
                    #endregion

                    #region // Đánh giá năng lực/phẩm chất
                    objResult = new LockRatedCommentPupil();
                    objResult = CreateObject(lstCP[i].ClassProfileID,
                        schoolId, academicYear, 0, lastDigit, 6);
                    if (("on".Equals(frm["lockEvaluationKH1"])))
                    {
                        lstLock.Add("DGHK1");
                    }
                    if (("on".Equals(frm["lockEvaluationKH2"])))
                    {
                        lstLock.Add("DGHK2");
                    }
                    if (("on".Equals(frm["lockEvaluationInSummer"])))
                    {
                        lstLock.Add("RLTH");
                    }

                    objResult.LockTitle = string.Join(",", lstLock.ToArray());
                    if (!string.IsNullOrEmpty(objResult.LockTitle))
                    {
                        lstResult.Add(objResult);
                        lstLock = new List<string>();
                    }
                    #endregion

                    #region // Đánh giá chung
                    objResult = new LockRatedCommentPupil();
                    objResult = CreateObject(lstCP[i].ClassProfileID,
                        schoolId, academicYear, 0, lastDigit, 7);

                    if (("on".Equals(frm["lockBonusKH1"])))
                    {
                        lstLock.Add("KTHK1");
                    }
                    if (("on".Equals(frm["lockBonusKH2"])))
                    {
                        lstLock.Add("KTHK2");
                    }
                    if (("on".Equals(frm["lockAdditional"])))
                    {
                        lstLock.Add("DGBS");
                    }

                    objResult.LockTitle = string.Join(",", lstLock.ToArray());
                    if (!string.IsNullOrEmpty(objResult.LockTitle))
                    {
                        lstResult.Add(objResult);
                        lstLock = new List<string>();
                    }
                    #endregion
                }
                #endregion
            }

            this.LockRatedCommentPupilBusiness.SetAutoDetectChangesEnabled(false);
            this.ClassProfileBusiness.SetAutoDetectChangesEnabled(false);
            this.SubjectCatBusiness.SetAutoDetectChangesEnabled(false);
            this.ClassSubjectBusiness.SetAutoDetectChangesEnabled(false);
            this.SchoolSubjectBusiness.SetAutoDetectChangesEnabled(false);
            //Save
            if (lstResult.Count > 0)
            {
                LockRatedCommentPupil obj = null;
                for (int i = 0; i < lstResult.Count(); i++)
                {
                    obj = lstResult[i];
                    this.LockRatedCommentPupilBusiness.Insert(obj);
                }
            }

            this.LockRatedCommentPupilBusiness.Save();
        }

        private LockRatedCommentPupil CreateObject(int classID, int schoolId, int academicYear, int subjectId, int lastDigit, int evaluation)
        {
            LockRatedCommentPupil obj = new LockRatedCommentPupil();
            obj.ClassID = classID;
            obj.SchoolID = schoolId;
            obj.AcademicYearID = academicYear;
            obj.SubjectID = subjectId;
            obj.LastDigitSchoolID = lastDigit;
            obj.EvaluationID = evaluation;
            obj.CreateTime = DateTime.Now;
            return obj;
        }
    }
}
