﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// khoa/ tổ bộ môn
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class SchoolFacultyBusiness
    {
        #region private member variable
        private const int FacultyName_MAX_LENGTH = 100; //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 200; //do dai lon nhat truong mieu ta
        #endregion

        #region insert
        /// <summary>
        /// Thêm mới khoa/ tổ bộ môn
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertSchoolFaculty">Đối tượng Insert</param>
        /// <returns></returns>
        public override SchoolFaculty Insert(SchoolFaculty insertSchoolFaculty)
        {

            //FacultyName rong
            Utils.ValidateRequire(insertSchoolFaculty.FacultyName, "SchoolFaculty_Control_FacultyName");

            Utils.ValidateMaxLength(insertSchoolFaculty.FacultyName, FacultyName_MAX_LENGTH, "SchoolFaculty_Control_FacultyName");
            Utils.ValidateMaxLength(insertSchoolFaculty.Description, DESCRIPTION_MAX_LENGTH, "SchoolFaculty_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
           // this.CheckDuplicate(insertSchoolFaculty.FacultyName, GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty", "FacultyName", true, 0, "SchoolFaculty_Control_FacultyName");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty",
                        new Dictionary<string, object>()
                {
                    {"FacultyName",insertSchoolFaculty.FacultyName}
                    ,{"SchoolID",insertSchoolFaculty.SchoolID}
                    , {"IsActive",true}
                }, null);
                if (Exist)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() {"SchoolFaculty_Control_FacultyName" });
                }
            }
            //
            SchoolProfileBusiness.CheckAvailable(insertSchoolFaculty.SchoolID, "UserInfo_SchoolID");


            insertSchoolFaculty.IsActive = true;
            insertSchoolFaculty.CreatedDate = DateTime.Now;
            return base.Insert(insertSchoolFaculty);
        }
        #endregion

        #region update
        /// <summary>
        /// Sửa khoa/ tổ bộ môn
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateSchoolFaculty">Đối tượng Update</param>
        /// <returns></returns>
        public override SchoolFaculty Update(SchoolFaculty updateSchoolFaculty)
        {

            //check avai
            new SchoolFacultyBusiness(null).CheckAvailable(updateSchoolFaculty.SchoolFacultyID, "SchoolFaculty_Label_SchoolFacultyID");

            //FacultyName rong
            Utils.ValidateRequire(updateSchoolFaculty.FacultyName, "SchoolFaculty_Control_FacultyName");

            Utils.ValidateMaxLength(updateSchoolFaculty.FacultyName, FacultyName_MAX_LENGTH, "SchoolFaculty_Control_FacultyName");
            Utils.ValidateMaxLength(updateSchoolFaculty.Description, DESCRIPTION_MAX_LENGTH, "SchoolFaculty_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
             //this.CheckDuplicate(updateSchoolFaculty.FacultyName, GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty", "FacultyName", true, updateSchoolFaculty.SchoolFacultyID, "SchoolFaculty_Control_FacultyName");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty",
                        new Dictionary<string, object>()
                {
                    {"FacultyName",updateSchoolFaculty.FacultyName}
                    ,{"SchoolID",updateSchoolFaculty.SchoolID}
                    ,{"IsActive",true}
                }, new Dictionary<string,object>()
                {
                {"SchoolFacultyID",updateSchoolFaculty.SchoolFacultyID}    
                });
                if (Exist)
                {
                    throw new BusinessException("Common_Validate_Duplicate",new List<object>() {"SchoolFaculty_Control_FacultyName" });
                }
            }
            //this.CheckDuplicateCouple(updateSchoolFaculty.FacultyName, updateSchoolFaculty.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "SchoolFaculty", "FacultyName", "SchoolID", true, updateSchoolFaculty.SchoolFacultyID, "SchoolFaculty_Control_FacultyName");

            // this.CheckDuplicate(updateSchoolFaculty.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "SchoolFaculty", "AppliedLevel", true, updateSchoolFaculty.SchoolFacultyID, "SchoolFaculty_Label_ShoolID");



            //check 
            SchoolProfileBusiness.CheckAvailable(updateSchoolFaculty.SchoolID, "UserInfo_SchoolID");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu SchoolFacultyID ->vao csdl lay SchoolFaculty tuong ung roi 
            //so sanh voi updateSchoolFaculty.SchoolID


            //.................



            updateSchoolFaculty.IsActive = true;
           
            return base.Update(updateSchoolFaculty);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa khoa/ tổ bộ môn
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="SchoolFacultyId">id mon can xoa</param>
        public void Delete(int SchoolFacultyId, int SchoolID)
        {
            SchoolFaculty schoolFaculty = this.SchoolFacultyBusiness.Find(SchoolFacultyId);
            if (schoolFaculty.SchoolID != SchoolID)
            {
                throw new BusinessException("SchoolFaculty_Label_School_Err");
            }
            // Update lai truong SchoolFacultyID cua cac giao vien da bi huy kich hoat
            // trong to bo mon lai thanh null de co the xoa duoc to bo mon
            var lstEmployee = schoolFaculty.Employees.Where(e => e.IsActive).ToList();
            if (lstEmployee.Count() > 0)
            {
                throw new BusinessException("Tồn tại giáo viên thuộc tổ bộ môn. Hãy xóa giáo viên trong tổ bộ môn trước.");
            }
            // Xoa cac giao vien trong to bo mon co isactive = false
            lstEmployee = schoolFaculty.Employees.Where(e => e.IsActive == false).ToList();
            this.EmployeeBusiness.DeleteAll(lstEmployee);
            //check avai
            this.SchoolFacultyBusiness.CheckAvailable(SchoolFacultyId, "SchoolFaculty_Label_SchoolFacultyID");
            // AnhVD9 20140915
            // Thuc hien xoa CONCURRENT_WORK_ASSIGNMENT
            var lstWorkAssignment = this.ConcurrentWorkAssignmentBusiness.Search(new Dictionary<string, object> { { "SchoolID", SchoolID }, { "FacultyID", SchoolFacultyId }, { "IsActive" , false } }).ToList();
            if (lstWorkAssignment != null && lstWorkAssignment.Count > 0) this.ConcurrentWorkAssignmentBusiness.DeleteAll(lstWorkAssignment);
            // Thuc hien xoa CONCURRENT_WORK_REPLACEMENT
            var lstWorkReplacement = this.ConcurrentWorkReplacementBusiness.Search(new Dictionary<string, object> { { "SchoolID", SchoolID }, { "SchoolFacultyID", SchoolFacultyId }, { "IsActive", false } }).ToList();
            if (lstWorkReplacement != null && lstWorkReplacement.Count > 0) this.ConcurrentWorkReplacementBusiness.DeleteAll(lstWorkReplacement);
            // Thuc hien xoa TEACHING_ASSIGNMENT
            var lstTeachingAssignment = this.TeachingAssignmentBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "FacultyID", SchoolFacultyId }, { "IsActive", false } }).ToList();
            if (lstTeachingAssignment != null && lstTeachingAssignment.Count > 0) this.TeachingAssignmentBusiness.DeleteAll(lstTeachingAssignment);
            
            //// Thuc hien xoa trong bang Teacher_of_faculty - AnhVD 20140915
            //this.TeacherOfFacultyBusiness.DeleteTeacherInFaculty(SchoolFacultyId);
            //this.TeacherOfFacultyBusiness.Save();

            //da su dung hay chua
            //this.CheckConstraints(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty", SchoolFacultyId, "SchoolFaculty_Label_SchoolFacultyID");

            base.Delete(SchoolFacultyId, true);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm khoa/ tổ bộ môn
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<SchoolFaculty> Search(IDictionary<string, object> dic)
        {
            string FacultyName = Utils.GetString(dic, "FacultyName");  
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
           
            IQueryable<SchoolFaculty> lsSchoolFaculty = this.SchoolFacultyRepository.All;


             //isActive = true default
                lsSchoolFaculty = lsSchoolFaculty.Where(em => (em.IsActive == true));
           



            if (FacultyName.Trim().Length != 0)
            {

                lsSchoolFaculty = lsSchoolFaculty.Where(em => (em.FacultyName.ToUpper().Contains(FacultyName.ToUpper())));
            }
           


            if (SchoolID != 0)
            {
                lsSchoolFaculty = lsSchoolFaculty.Where(em => (em.SchoolID == SchoolID));
            }

            if (SchoolFacultyID != 0)
            {
                lsSchoolFaculty = lsSchoolFaculty.Where(em => (em.SchoolFacultyID == SchoolFacultyID));
            }
           
            return lsSchoolFaculty;
        }

        /// <summary>
        /// Tìm kiếm khoa/ tổ bộ môn theo id truong
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="SchoolID">id trường</param>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<SchoolFaculty> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }

        #endregion


        // Thuyen
        public void InsertSchoolFaculty(List<SchoolFaculty> lstInput)
        {
            SchoolFaculty objInput = null;
            foreach (var item in lstInput)
            {
                objInput = new SchoolFaculty()
                {
                    SchoolID = item.SchoolID,
                    FacultyName = item.FacultyName,
                    Description = item.Description,
                    CreatedDate = item.CreatedDate,
                    IsActive = item.IsActive,
                    ModifiedDate = item.ModifiedDate,
                    HeadMasterID = item.HeadMasterID,
                };
                this.Insert(objInput);
            }
            this.Save();
        }

        public void UpdateSchoolFaculty(SchoolFaculty objUpdate, IDictionary<string, object> dic)
        {
            SchoolFaculty objDB = Search(dic).Where(x=>x.SchoolFacultyID == objUpdate.SchoolFacultyID).FirstOrDefault();
            if (objDB != null)
            {
                objDB.FacultyName = objUpdate.FacultyName;
                objDB.Description = objUpdate.Description;
                objDB.HeadMasterID = objUpdate.HeadMasterID;
                objDB.ModifiedDate = objUpdate.ModifiedDate;
                this.Update(objDB);
                this.Save();
            }
        }

    }
}