﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ThreadMarkBusiness
    {
        public int InsertActionList(ThreadMarkBO obj)
        {
            int Count = obj.PupilIds.Count;
            ThreadMark threadMark;
            List<ThreadMark> lstInsert = new List<ThreadMark>();
            threadMark = new ThreadMark();
            threadMark.PupilID = 0;
            threadMark.ClassID = obj.ClassID;
            threadMark.SubjectID = obj.SubjectID;
            threadMark.Semester = (short)obj.Semester;
            threadMark.AcademicYearID = obj.AcademicYearID;
            threadMark.SchoolID = obj.SchoolID;
            threadMark.PeriodID = obj.PeriodID;
            threadMark.CreatedDate = DateTime.Now;
            threadMark.Type = obj.Type;
            threadMark.Status = 1;
            threadMark.NumberScan = 0;
            threadMark.LastDigitSchoolID = UtilsBusiness.GetPartionId(obj.SchoolID);
            lstInsert.Add(threadMark);
            //int pupilID = 0;
            /*for (int i = 0; i < Count; i++)
            {
                threadMark = new ThreadMark();
                pupilID = obj.PupilIds[i];
                threadMark.PupilID = pupilID;
                threadMark.ClassID = obj.ClassID;
                threadMark.SubjectID = obj.SubjectID;
                threadMark.Semester = (short)obj.Semester;
                threadMark.AcademicYearID = obj.AcademicYearID;
                threadMark.SchoolID = obj.SchoolID;
                threadMark.PeriodID = obj.PeriodID;
                threadMark.CreatedDate = DateTime.Now;
                threadMark.Type = obj.Type;
                threadMark.Status = 1;
                threadMark.NumberScan = 0;
                threadMark.LastDigitSchoolID = UtilsBusiness.GetPartionId(obj.SchoolID);
                //this.Insert(threadMark);
                //Them moi vao thread mark se khong insert pupil, subjectId do viec bo tinh TBM. Chi can classId de tinh tong ket diem
                if (lstInsert.Exists(s => s.ClassID == obj.ClassID && s.Semester == obj.Semester && (s.PeriodID == obj.PeriodID && obj.PeriodID > 0)))
                {
                    continue;
                }

                lstInsert.Add(threadMark);
            }*/
            if (lstInsert.Count == 0)
            {
                return 0;
            }

            this.BulkInsertListThreadMark(lstInsert);
            return Count;
        }

        public int BulkInsertActionList(ThreadMarkBO obj)
        {
            int Count = obj.PupilIds.Count;
            ThreadMark threadMark;
            List<ThreadMark> lstInsert = new List<ThreadMark>();
            threadMark = new ThreadMark();
            threadMark.PupilID = 0;
            threadMark.ClassID = obj.ClassID;
            threadMark.SubjectID = obj.SubjectID;
            threadMark.Semester = (short)obj.Semester;
            threadMark.AcademicYearID = obj.AcademicYearID;
            threadMark.SchoolID = obj.SchoolID;
            threadMark.PeriodID = obj.PeriodID;
            threadMark.CreatedDate = DateTime.Now;
            threadMark.Type = obj.Type;
            threadMark.Status = 1;
            threadMark.NumberScan = 0;
            threadMark.LastDigitSchoolID = UtilsBusiness.GetPartionId(obj.SchoolID);
            lstInsert.Add(threadMark);
            //int pupilID = 0;
            /*for (int i = 0; i < Count; i++)
            {
                threadMark = new ThreadMark();
                pupilID = obj.PupilIds[i];
                threadMark.PupilID = pupilID;
                threadMark.ClassID = obj.ClassID;
                threadMark.SubjectID = obj.SubjectID;
                threadMark.Semester = (short)obj.Semester;
                threadMark.AcademicYearID = obj.AcademicYearID;
                threadMark.SchoolID = obj.SchoolID;
                threadMark.PeriodID = obj.PeriodID;
                threadMark.CreatedDate = DateTime.Now;
                threadMark.Type = obj.Type;
                threadMark.Status = 1;
                threadMark.NumberScan = 0;
                threadMark.LastDigitSchoolID = UtilsBusiness.GetPartionId(obj.SchoolID);
                //this.Insert(threadMark);

                //Them moi vao thread mark se khong insert pupil, subjectId do viec bo tinh TBM. Chi can classId de tinh tong ket diem
                if (lstInsert.Exists(s => s.ClassID == obj.ClassID && s.Semester == obj.Semester && (s.PeriodID == obj.PeriodID && obj.PeriodID > 0)))
                {
                    continue;
                }

                lstInsert.Add(threadMark);
            }*/
            if (lstInsert.Count == 0)
            {
                return 0;
            }

            this.BulkInsertListThreadMark(lstInsert);
            return Count;
        }

        public void UpdateThreadMark(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            DateTime fromDate = DateTime.Now.Date.AddMonths(-1);
            DateTime toDate = DateTime.Now.Date.AddMonths(2);
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            IQueryable<ThreadMark> lstThreadMarkDB = (from t in ThreadMarkBusiness.All
                                                      join cs in ClassSubjectBusiness.SearchBySchool(schoolID) on t.ClassID equals cs.ClassID //new { t.ClassID, t.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                      where t.SchoolID == schoolID
                                                      && t.AcademicYearID == academicYearID
                                                      && t.CreatedDate >= fromDate && t.CreatedDate <= toDate
                                                      && t.SubjectID == cs.SubjectID
                                                      && t.LastDigitSchoolID == partitionId
                                                      && cs.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE
                                                      select t);
            foreach (ThreadMark item in lstThreadMarkDB)
            {
                item.Status = 1;
                item.NumberScan = 0;
                this.Update(item);
            }
            this.Save();

        }


        public void BulkInsertListThreadMark(List<ThreadMark> lstThreadMark)
        {
            try
            {

                this.BulkInsert(lstThreadMark, ColumnMappings());
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "BulkInsertListThreadMark", "null", ex);
            }
        }

        public void AddThreadMark(int schoolId, int academicYearId, int classId, int subjectId, short semester, int type, int? PeriodID, int pupilId, List<ThreadMark> lstThreadMark)
        {
            //Them moi vao thread mark se khong insert pupil, subjectId do viec bo tinh TBM. Chi can classId de tinh tong ket diem
            if (lstThreadMark.Exists(s => s.ClassID == classId && s.Semester == semester && (s.PeriodID == PeriodID || !PeriodID.HasValue)))
            {
                return;
            }
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            ThreadMark objThreadMark = new ThreadMark
            {
                AcademicYearID = academicYearId,
                ClassID = classId,
                CreatedDate = DateTime.Now,
                LastDigitSchoolID = partitionId,
                NumberScan = 0,
                PeriodID = PeriodID,
                PupilID = pupilId,
                SchoolID = schoolId,
                Semester = semester,
                Status = 1,
                StatusResult = 0,
                SubjectID = subjectId,
                Type = type
            };
            lstThreadMark.Add(objThreadMark);
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ThreadMarkID", "THREAD_MARK_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("StatusResult", "STATUS_RESULT");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("NumberScan", "NUMBER_SCAN");
            return columnMap;
        }

    }
}
