﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using SMAS.Models.Models.CustomModels;
using System.Data.Objects.SqlClient;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class IndicatorDataBusiness
    {
        public IQueryable<IndicatorData> SearchIndicatorData(IDictionary<string, object> search)
        {
            int indicatorDataID = Utils.GetInt(search, "IndicatorDataID");
            int indicatorID = Utils.GetInt(search, "IndicatorID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int provinceID = Utils.GetInt(search, "ProvinceID");
            int partion = provinceID % SystemParamsInFile.PARTITION_INDICATOR_DATA; // So Tinh
            int districtID = Utils.GetInt(search, "DistrictID");
            int semesterID = Utils.GetInt(search, "SemesterID");
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int year = Utils.GetInt(search, "Year");
            int educationGrade = Utils.GetInt(search, "AppliedLevel");
            List<int> listIndicatorId = Utils.GetIntList(search, "ListIndicatorID");
            List<int> listSchoolId = Utils.GetIntList(search, "ListSchoolID");
            int period = Utils.GetInt(search, "Period");

            IQueryable<IndicatorData> query = IndicatorDataBusiness.All.Where(o => o.LastDigitProvinceID == partion);
            if (indicatorDataID > 0)
            {
                query = query.Where(o => o.IndicatorDataID == indicatorDataID);
            }
            if (indicatorID > 0)
            {
                query = query.Where(o => o.IndicatorID == indicatorID);
            }
            if (schoolID > 0)
            {
                query = query.Where(o => o.SchoolId == schoolID);
            }
            if (provinceID > 0)
            {
                query = query.Where(o => o.ProvinceID == provinceID);
            }
            if (districtID > 0)
            {
                query = query.Where(o => o.DistrictID == districtID);
            }
            if (semesterID > 0)
            {
                query = query.Where(o => o.SemesterID == semesterID);
            }
            if (academicYearID > 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (year > 0)
            {
                query = query.Where(o => o.CreateYear == year);
            }
            if (educationGrade > 0)
            {
                query = query.Where(o => o.EducationGrade == educationGrade);
            }
            if (listIndicatorId != null && listIndicatorId.Count > 0)
            {
                query = query.Where(o => listIndicatorId.Contains(o.IndicatorID));
            }
            if (listSchoolId != null && listSchoolId.Count > 0)
            {
                query = query.Where(o => listSchoolId.Contains(o.SchoolId));
            }
            if (period > 0)
            {
                query = query.Where(o => o.SemesterID == period);
            }

            return query;
        }

        public void InsertData(List<IndicatorData> listData, List<int> listIndicatorIdDelete, int provinceId, int period, int academicYearId)
        {
            // IndicatorData dataObj = listData.FirstOrDefault();
            List<int> listIndicatorId = listData.Select(p => p.IndicatorID).Distinct().ToList();
            int partiton = provinceId % SystemParamsInFile.PARTITION_INDICATOR_DATA;
            //Xóa d? li?u cu c?a d?t báo cáo hi?n t?i theo list indicatorId (xóa d? li?u theo template)
            List<IndicatorData> listDataSource = IndicatorDataBusiness.All
                .Where(p => p.LastDigitProvinceID == partiton
                && p.ProvinceID == provinceId
                && p.SemesterID == period
                && p.AcademicYearID == academicYearId
                && listIndicatorIdDelete.Contains(p.IndicatorID)
                ).ToList();
            if (listDataSource != null && listDataSource.Count > 0)
            {
                IndicatorDataBusiness.DeleteAll(listDataSource);
            }

            //Insert du lieu moi
            if (listData != null && listData.Count > 0)
            {
                for (int i = 0; i < listData.Count; i++)
                {
                    IndicatorDataBusiness.Insert(listData[i]);
                }
            }
            IndicatorDataBusiness.SaveDetect();
        }

        public void InsertDataPreviosPeriod(int academicYearId, int provinceId, int period, int htmlContentCodeId)
        {
            #region Xoa du lieu ky hien tai
            List<IndicatorData> listIndicatorData = this.GetListIndicatorDataByCondition(academicYearId, provinceId, period, htmlContentCodeId);
            if (listIndicatorData != null && listIndicatorData.Count > 0)
            {
                IndicatorDataBusiness.DeleteAll(listIndicatorData);
                IndicatorDataBusiness.Save();
            }
            #endregion

            //Lay du lieu ky truoc
            AcademicYear aca = AcademicYearBusiness.Find(academicYearId);
            int schoolId = aca.SchoolID;
            int prePeriod = GetPreviosPeriod(period);//ky truoc

            if (period == SystemParamsInFile.FIRST_PERIOD)
            {
                //lay Id nam hoc truoc
                int preYear = aca.Year - 1;
                AcademicYear preAca = AcademicYearBusiness.All.Where(p => p.SchoolID == schoolId && p.Year == preYear && p.IsActive == true).FirstOrDefault();
                if (preAca == null)
                {
                    throw new BusinessException("Trường không có năm học trước");
                }
                else
                {
                    //Lay du lieu ky truoc va nam hoc truoc
                    List<IndicatorData> listIndicatorDataPrevios = this.GetListIndicatorDataByCondition(preAca.AcademicYearID, provinceId, prePeriod, htmlContentCodeId);
                    if (listIndicatorDataPrevios != null && listIndicatorDataPrevios.Count > 0)
                    {
                        //Doi ve ky va nam hoc hien tai
                        IndicatorData indiInsertObj = null;
                        IndicatorData indiTmp = null;
                        for (int i = 0; i < listIndicatorDataPrevios.Count; i++)
                        {
                            indiTmp = listIndicatorDataPrevios[i];
                            indiInsertObj = new IndicatorData();
                            indiInsertObj.AcademicYearID = aca.AcademicYearID;
                            indiInsertObj.CellValue = indiTmp.CellValue;
                            indiInsertObj.CreateTime = DateTime.Now;
                            indiInsertObj.CreateYear = aca.Year;
                            indiInsertObj.DistrictID = indiTmp.DistrictID;
                            indiInsertObj.EducationGrade = indiTmp.EducationGrade;
                            indiInsertObj.IndicatorID = indiTmp.IndicatorID;
                            indiInsertObj.LastDigitProvinceID = indiTmp.LastDigitProvinceID;
                            indiInsertObj.ProvinceID = indiTmp.ProvinceID;
                            indiInsertObj.SchoolId = schoolId;
                            indiInsertObj.SemesterID = period;
                            IndicatorDataBusiness.Insert(indiInsertObj);
                        }
                        IndicatorDataBusiness.SaveDetect();
                    }
                }
            }
            else
            {
                List<IndicatorData> listIndicatorDataPrevios = this.GetListIndicatorDataByCondition(aca.AcademicYearID, provinceId, prePeriod, htmlContentCodeId);
                IndicatorData indicatorDataObj = null;
                IndicatorData objTmp = null;
                if (listIndicatorDataPrevios != null && listIndicatorDataPrevios.Count > 0)
                {
                    for (int i = 0; i < listIndicatorDataPrevios.Count; i++)
                    {
                        objTmp = listIndicatorDataPrevios[i];
                        indicatorDataObj = new IndicatorData();
                        indicatorDataObj.IndicatorID = objTmp.IndicatorID;
                        indicatorDataObj.SchoolId = objTmp.SchoolId;
                        indicatorDataObj.CellValue = objTmp.CellValue;
                        indicatorDataObj.ProvinceID = objTmp.ProvinceID;
                        indicatorDataObj.LastDigitProvinceID = objTmp.LastDigitProvinceID;
                        indicatorDataObj.DistrictID = objTmp.DistrictID;
                        indicatorDataObj.SemesterID = period;
                        indicatorDataObj.AcademicYearID = aca.AcademicYearID;
                        indicatorDataObj.EducationGrade = objTmp.EducationGrade;
                        indicatorDataObj.CreateTime = DateTime.Now;
                        indicatorDataObj.CreateYear = aca.Year;
                        IndicatorDataRepository.Insert(indicatorDataObj);
                    }
                    IndicatorDataRepository.SaveDetect();
                }
            }
        }

        private List<IndicatorData> GetListIndicatorDataByCondition(int academicYearId, int provinceId, int period, int htmlContentCodeId)
        {
            int partition = provinceId % SystemParamsInFile.PARTITION_INDICATOR_DATA;
            List<IndicatorData> listIndicatorData = (from ht in HtmlTemplateBusiness.All
                                                     join indi in IndicatorBusiness.All on ht.HtmlTemplateID equals indi.HtmlTemplateID
                                                     join indidata in IndicatorDataBusiness.All.Where(p => p.LastDigitProvinceID == partition && p.ProvinceID == provinceId) on indi.IndicatorID equals indidata.IndicatorID
                                                     where (ht.HtmlContentCodeID == htmlContentCodeId || htmlContentCodeId == 0)
                                                     && indidata.SemesterID == period
                                                     && indidata.AcademicYearID == academicYearId
                                                     select indidata).ToList();
            return listIndicatorData;
        }

        public List<PhysicalFacilitiesBO> GetListIndicatorData(int provinceId, int period, int schoolId, int year, int? levelApplied = 0)
        {
            int partition = provinceId % SystemParamsInFile.PARTITION_INDICATOR_DATA;
            List<PhysicalFacilitiesBO> listIndicatorData = (from indi in IndicatorBusiness.All
                                                            join indidata in IndicatorDataBusiness
                                                            .All.Where(p => p.LastDigitProvinceID == partition && p.ProvinceID == provinceId) on indi.IndicatorID equals indidata.IndicatorID
                                                            where indidata.SemesterID == period
                                                            && indidata.SchoolId == schoolId
                                                            && indidata.CreateYear == year
                                                            && (indi.Formula == null || indi.Formula.Equals(string.Empty))
                                                            && string.IsNullOrEmpty(indi.CellCalPrePeriod)
                                                            select new PhysicalFacilitiesBO
                                                            {
                                                                CellAddress = indi.CellAddress,
                                                                CellValue = indidata.CellValue,
                                                                HtmlTemplateID = indi.HtmlTemplateID
                                                            }).ToList();
            if (listIndicatorData != null && levelApplied != null && levelApplied > 3)
            {
                listIndicatorData = listIndicatorData.Where(x => x.HtmlTemplateID >= 13).ToList();
            }
            else
            {
                listIndicatorData = listIndicatorData.Where(x => x.HtmlTemplateID < 13).ToList();
            }
            List<int> lstSchoolId = new List<int>() { schoolId };
            List<PhysicalFacilitiesBO> listdataByPeriod = this.GetListDataUpDown(provinceId, period, lstSchoolId, year);
            if (listdataByPeriod != null && listdataByPeriod.Count > 0)
            {
                listIndicatorData.AddRange(listdataByPeriod);
            }
            return listIndicatorData;
        }

        public int GetPreviosPeriod(int period)
        {
            if (period == SystemParamsInFile.FIRST_PERIOD)
            {
                return SystemParamsInFile.END_PERIOD;
            }
            else if (period == SystemParamsInFile.MID_PERIOD)
            {
                return SystemParamsInFile.FIRST_PERIOD;
            }
            else if (period == SystemParamsInFile.END_PERIOD)
            {
                return SystemParamsInFile.MID_PERIOD;
            }
            return 0;
        }

        #region Anhvd9 - Tra cuu thong tin CSVC
        /// <summary>
        /// Tra cuu thong tin cac truong thuoc Phong/So
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<FacilitiesDataBO> LookupFacilitiesData(IDictionary<string, object> dic)
        {
            try
            {
                int provinceID = Utils.GetInt(dic, "ProvinceID");
                int districtID = Utils.GetInt(dic, "DistrictID");
                int year = Utils.GetInt(dic, "Year");
                int semester = Utils.GetInt(dic, "DataSemester");
                int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
                int grade = appliedLevel > 0 ? Utils.GradeToBinary(appliedLevel) : (int)0;
                int schoolID = Utils.GetInt(dic, "SchoolID");

                int deptClassRoomStatus = Utils.GetInt(dic, "DeptClassRoom");
                int publicHouseStatus = Utils.GetInt(dic, "PublicHouse");
                int schoolBoardHouse = Utils.GetInt(dic, "SchoolBoardHouse");
                int boardingHouse = Utils.GetInt(dic, "BoardingHouse");

                int libraryStatus = Utils.GetInt(dic, "Library");
                int kitchenStatus = Utils.GetInt(dic, "Kitchen");
                int schoolGate = Utils.GetInt(dic, "SchoolGate");
                int schoolFence = Utils.GetInt(dic, "SchoolFence");
                int waterSource = Utils.GetInt(dic, "WaterSource");
                int restRoomStatus = Utils.GetInt(dic, "RestRoom");
                int shortlivedRoom = Utils.GetInt(dic, "ShortlivedRoom");
                int threeShiftsRoom = Utils.GetInt(dic, "ThreeShiftsRoom");

                int supervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");

                List<FacilitiesDataBO> result = this.context.LOOKUP_INFO_FACILITIES_FN(provinceID, supervisingDeptID, districtID, year, semester, grade, schoolID,
                            deptClassRoomStatus, publicHouseStatus, schoolBoardHouse, boardingHouse, libraryStatus, kitchenStatus,
                            schoolGate, schoolFence, waterSource, restRoomStatus, shortlivedRoom, threeShiftsRoom);

                List<int> lstSchoolID = new List<int>();
                if (deptClassRoomStatus == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfDeptClassroom > 0).Select(o => o.SchoolID).ToList());
                else if (deptClassRoomStatus == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfDeptClassroom == 0).Select(o => o.SchoolID).ToList());

                if (publicHouseStatus == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfPublicHouse > 0).Select(o => o.SchoolID).ToList());
                else if (publicHouseStatus == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfPublicHouse == 0).Select(o => o.SchoolID).ToList());

                if (schoolBoardHouse == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfSchoolBoardHouse > 0).Select(o => o.SchoolID).ToList());
                else if (schoolBoardHouse == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfSchoolBoardHouse == 0).Select(o => o.SchoolID).ToList());

                if (boardingHouse == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfBoardingHouse > 0).Select(o => o.SchoolID).ToList());
                else if (boardingHouse == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfBoardingHouse == 0).Select(o => o.SchoolID).ToList());

                if (kitchenStatus == 0) lstSchoolID.AddRange(result.Where(o => o.OnewayKitchenStatus > 0).Select(o => o.SchoolID).ToList());
                else if (kitchenStatus == 1) lstSchoolID.AddRange(result.Where(o => o.OnewayKitchenStatus == 0).Select(o => o.SchoolID).ToList());

                if (schoolGate == 0) lstSchoolID.AddRange(result.Where(o => o.SchoolGateStatus > 0).Select(o => o.SchoolID).ToList());
                else if (schoolGate == 1) lstSchoolID.AddRange(result.Where(o => o.SchoolGateStatus == 0).Select(o => o.SchoolID).ToList());

                if (restRoomStatus == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfStandardWC > 0 || o.NumOfNotStandardWC > 0).Select(o => o.SchoolID).ToList());
                else if (restRoomStatus == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfStandardWC == 0).Select(o => o.SchoolID).ToList());
                else if (restRoomStatus == 2) lstSchoolID.AddRange(result.Where(o => o.NumOfNotStandardWC == 0).Select(o => o.SchoolID).ToList());

                if (shortlivedRoom == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfShortlivedRoom > 0).Select(o => o.SchoolID).ToList());
                else if (shortlivedRoom == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfShortlivedRoom == 0).Select(o => o.SchoolID).ToList());

                if (threeShiftsRoom == 0) lstSchoolID.AddRange(result.Where(o => o.NumOfThreeShiftsRoom > 0).Select(o => o.SchoolID).ToList());
                else if (threeShiftsRoom == 1) lstSchoolID.AddRange(result.Where(o => o.NumOfThreeShiftsRoom == 0).Select(o => o.SchoolID).ToList());

                if (libraryStatus > 0) lstSchoolID.AddRange(result.Where(o => o.LibraryStatus != libraryStatus).Select(o => o.SchoolID).ToList());
                if (schoolFence > 0) lstSchoolID.AddRange(result.Where(o => o.SchoolFence != schoolFence).Select(o => o.SchoolID).ToList());
                if (waterSource > 0) lstSchoolID.AddRange(result.Where(o => o.WaterSource != waterSource).Select(o => o.SchoolID).ToList());

                return result.Where(o => !lstSchoolID.Contains(o.SchoolID)).ToList();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "LookupFacilitiesData", "null", ex);
                return null;
            }

        }

        /// <summary>
        /// Tong hop so lieu cho Phong/So
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<PhysicalFacilitiesBO> SynthesizeDataforSupervisingDept(IDictionary<string, object> dic, int? levelApplied = 0)
        {
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int year = Utils.GetInt(dic, "Year");
            int period = Utils.GetInt(dic, "Period");
            List<int> lstSchoolID = SchoolProfileBusiness.Search(dic).Select(o => o.SchoolProfileID).ToList();
            dic["ListSchoolID"] = lstSchoolID;
            dic["IsActive"] = true;

            List<PhysicalFacilitiesBO> result = new List<PhysicalFacilitiesBO>();
            try
            {
                List<string> LstSyntheticCode = new List<string>();
                List<string> LstSyntheticAddress = new List<string>();
                for (int i = 192; i <= 198; i++)
                {
                    LstSyntheticCode.Add(string.Format("CSVCTB{0}", i));
                    LstSyntheticAddress.Add(string.Format("B{0}", i));
                }
                var lstResult = (from d in this.SearchIndicatorData(dic)
                                 join idc in this.IndicatorBusiness.All on d.IndicatorID equals idc.IndicatorID
                                 where !string.IsNullOrEmpty(d.CellValue) //&& string.IsNullOrEmpty(idc.Formula)
                                    && lstSchoolID.Contains(d.SchoolId)
                                 select new PhysicalFacilitiesBO()
                                 {
                                     IndicatorID = idc.IndicatorID,
                                     IndicatorCode = idc.IndicatorCode,
                                     CellAddress = idc.CellAddress,
                                     CellValue = d.CellValue,
                                     HtmlTemplateID = idc.HtmlTemplateID
                                 }).ToList();

                if (lstResult != null && levelApplied != null && levelApplied > 3)
                {
                    lstResult = lstResult.Where(x => x.HtmlTemplateID >= 13).ToList();
                }
                else
                {
                    lstResult = lstResult.Where(x => x.HtmlTemplateID < 13).ToList();
                }
                var lstResulttes = lstResult.Where(x => x.IndicatorID == 712).ToList();
                var LstSyntheticData = lstResult.Where(o => LstSyntheticCode.Contains(o.IndicatorCode) && LstSyntheticAddress.Contains(o.CellAddress)).ToList();
                PhysicalFacilitiesBO objFacility = null;
                // Nguon nuoc
                for (int i = 1; i <= 5; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(65 + i), 193);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB192" && o.CellAddress == "B192" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }
                // Nuoc dung hop ve sinh
                for (int i = 0; i <= 1; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(67 - i), 195);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB193" && o.CellAddress == "B193" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }
                // Nguon dien
                for (int i = 0; i <= 1; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(67 - i), 197);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB194" && o.CellAddress == "B194" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }
                // Bep an
                for (int i = 0; i <= 1; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(67 - i), 199);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB195" && o.CellAddress == "B195" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }
                // Cong truong 
                for (int i = 0; i <= 1; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(67 - i), 201);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB196" && o.CellAddress == "B196" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }
                // Hang rao
                for (int i = 1; i <= 3; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(65 + i), 203);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB197" && o.CellAddress == "B197" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }
                // Nguon nuoc
                for (int i = 1; i <= 4; i++)
                {
                    objFacility = new PhysicalFacilitiesBO();
                    objFacility.CellAddress = string.Format("{0}{1}", (char)(65 + i), 205);
                    objFacility.CellValue = LstSyntheticData.Where(o => o.IndicatorCode == "CSVCTB198" && o.CellAddress == "B198" && o.CellValue == i.ToString()).Count().ToString();
                    result.Add(objFacility);
                }

                result.AddRange(lstResult.Except(LstSyntheticData).GroupBy(x => new { x.IndicatorID, x.CellAddress })
                             .Select(x => new PhysicalFacilitiesBO
                             {
                                 IndicatorID = x.Key.IndicatorID,
                                 CellAddress = x.Key.CellAddress,
                                 CellValue = x.Sum(o => Int32.Parse(o.CellValue)).ToString()
                             }).ToList());
                var ListDataUpDown = GetListDataUpDown(provinceID, period, lstSchoolID, year);
                if (ListDataUpDown != null && levelApplied != null && levelApplied > 3)
                {
                    ListDataUpDown = ListDataUpDown.Where(x => x.HtmlTemplateID >= 13).ToList();
                }
                else
                {
                    ListDataUpDown = ListDataUpDown.Where(x => x.HtmlTemplateID < 13).ToList();
                }
                var test = ListDataUpDown.Where(x => x.IndicatorID == 712).ToList();
                result.AddRange(ListDataUpDown);

                return result;
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "SynthesizeDataforSupervisingDept", "null", ex);
                return result;
            }
        }
        public List<PhysicalFacilitiesBO> SynthesizeDataPrimaryLevelforSupervisingDept(IDictionary<string, object> dic)
        {
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int year = Utils.GetInt(dic, "Year");
            int period = Utils.GetInt(dic, "Period");
            List<int> lstSchoolID = SchoolProfileBusiness.Search(dic).Select(o => o.SchoolProfileID).ToList();
            dic["ListSchoolID"] = lstSchoolID;
            dic["IsActive"] = true;

            List<PhysicalFacilitiesBO> result = new List<PhysicalFacilitiesBO>();
            try
            {
                var ListDataUpDownPrimary = GetListDataUpDownForPrimaryLevel(provinceID, period, lstSchoolID, year);
                ListDataUpDownPrimary = ListDataUpDownPrimary.Where(x => x.HtmlTemplateID >= 13).ToList();
                result.AddRange(ListDataUpDownPrimary);
                return result;
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "SynthesizeDataPrimaryLevelforSupervisingDept", "null", ex);
                return result;
            }
        }
        #endregion

        private List<PhysicalFacilitiesBO> GetListDataUpDown(int provinceId, int period, List<int> lstSchoolId, int year)
        {
            //Danh sach indicator va dia chi cac cua cac cell Tang Giam so luong
            List<Indicator> listIndiCatorCaled = IndicatorBusiness.All.Where(p => !string.IsNullOrEmpty(p.CellCalPrePeriod)).ToList();
            List<string> listIndicatorCodeCaled = listIndiCatorCaled.Select(p => p.CellCalPrePeriod).ToList();
            int prePeriod = this.GetPreviosPeriod(period); // id ky truoc
            //Lay danh sach indicator cac cell Tang Giam so luong va du lieu ki hien tai
            List<PhysicalFacilitiesBO> listDataCurrent = this.GetDataByPeriod(provinceId, period, lstSchoolId, year, listIndicatorCodeCaled);

            //Danh sach du lieu cua ky lien truoc
            int yearID = year;
            List<PhysicalFacilitiesBO> listResult = new List<PhysicalFacilitiesBO>();
            PhysicalFacilitiesBO objResult = null;
            if (period == SystemParamsInFile.FIRST_PERIOD)
            {
                yearID = year - 1;
            }
            List<PhysicalFacilitiesBO> listDataPrePeriod = this.GetDataByPeriod(provinceId, prePeriod, lstSchoolId, yearID, listIndicatorCodeCaled);

            // Fix lỗi ATTT
            // if (listDataPrePeriod == null && listDataPrePeriod.Count == 0)
            if (listDataPrePeriod == null || !listDataPrePeriod.Any())
            {
                return listDataCurrent;
            }
            else
            {
                //Xu ly thong tin, lay cot tong so ky hien tai tru tong so ky lien truoc
                int valueCurrent = 0;
                int valuePre = 0;
                Indicator objDataTmp = null;
                for (int i = 0; i < listIndiCatorCaled.Count; i++)
                {
                    objDataTmp = listIndiCatorCaled[i];
                    objResult = new PhysicalFacilitiesBO();
                    valueCurrent = 0;
                    valuePre = 0;
                    //lay du lieu ky hien tai cua ky indicatorId
                    valueCurrent = listDataCurrent.Where(p => !string.IsNullOrEmpty(p.CellValue) && p.IndicatorCode == objDataTmp.CellCalPrePeriod).Sum(o => Int32.Parse(o.CellValue));
                    //lay du lieu cua ky lien truoc
                    valuePre = listDataPrePeriod.Where(p => !string.IsNullOrEmpty(p.CellValue) && p.IndicatorCode == objDataTmp.CellCalPrePeriod).Sum(o => Int32.Parse(o.CellValue));

                    if (valueCurrent > 0 || valuePre > 0)
                    {
                        objResult.CellAddress = objDataTmp.CellAddress;
                        objResult.CellValue = (valueCurrent - valuePre).ToString();
                        objResult.HtmlTemplateID = objDataTmp.HtmlTemplateID;
                        listResult.Add(objResult);
                    }
                }
            }
            return listResult;
        }
        private List<PhysicalFacilitiesBO> GetListDataUpDownForPrimaryLevel(int provinceId, int period, List<int> lstSchoolId, int year)
        {
            //Danh sach indicator va dia chi cac cua cac cell Tang Giam so luong
            int prePeriod = this.GetPreviosPeriod(period); // id ky truoc
            int yearID = year;
            List<PhysicalFacilitiesBO> listDataPeriod = this.GetDataByPeriodForPrimaryLevel(provinceId, period, lstSchoolId, yearID);

            return listDataPeriod;
        }
        public List<PhysicalFacilitiesBO> GetDataByPeriod(int provinceId, int period, List<int> lstSchoolId, int year, List<string> listIndicatorCode = null)
        {
            int partition = provinceId % SystemParamsInFile.PARTITION_INDICATOR_DATA;
            IQueryable<PhysicalFacilitiesBO> listData = (from a in IndicatorDataBusiness.All.Where(p => p.LastDigitProvinceID == partition && p.ProvinceID == provinceId)
                                                         join b in IndicatorBusiness.All on a.IndicatorID equals b.IndicatorID
                                                         join c in HtmlTemplateBusiness.All on b.HtmlTemplateID equals c.HtmlTemplateID
                                                         join d in HtmlContentCodeBusiness.All on c.HtmlContentCodeID equals d.HtmlContentCodeID
                                                         join e in TemplateBusiness.All on d.TemplateID equals e.TemplateID
                                                         where e.TemplateID == SystemParamsInFile.REPORT_TEMPLATE_CSVS_ID
                                                         && a.SemesterID == period
                                                         && lstSchoolId.Contains(a.SchoolId)
                                                         && a.CreateYear == year
                                                         select new PhysicalFacilitiesBO
                                                         {
                                                             CellAddress = b.CellAddress,
                                                             CellValue = a.CellValue,
                                                             IndicatorID = a.IndicatorID,
                                                             IndicatorCode = b.IndicatorCode,
                                                             CellCaled = b.CellCalPrePeriod
                                                         });
            if (listIndicatorCode != null && listIndicatorCode.Count > 0)
            {
                listData = listData.Where(p => listIndicatorCode.Contains(p.IndicatorCode));
            }
            return listData.ToList();
        }
        public List<PhysicalFacilitiesBO> GetDataByPeriodForPrimaryLevel(int provinceId, int period, List<int> lstSchoolId, int year)
        {
            int partition = provinceId % SystemParamsInFile.PARTITION_INDICATOR_DATA;
            IQueryable<PhysicalFacilitiesBO> listData = (from a in IndicatorDataBusiness.All.Where(p => p.LastDigitProvinceID == partition && p.ProvinceID == provinceId)
                                                         join b in IndicatorBusiness.All on a.IndicatorID equals b.IndicatorID
                                                         join c in HtmlTemplateBusiness.All on b.HtmlTemplateID equals c.HtmlTemplateID
                                                         join d in HtmlContentCodeBusiness.All on c.HtmlContentCodeID equals d.HtmlContentCodeID
                                                         join e in TemplateBusiness.All on d.TemplateID equals e.TemplateID
                                                         where e.TemplateID == SystemParamsInFile.REPORT_TEMPLATE_CSVS_MN_ID
                                                         && a.SemesterID == period
                                                         && lstSchoolId.Contains(a.SchoolId)
                                                         && a.CreateYear == year
                                                         select new PhysicalFacilitiesBO
                                                         {
                                                             CellAddress = b.CellAddress,
                                                             CellValue = a.CellValue,
                                                             IndicatorID = a.IndicatorID,
                                                             IndicatorCode = b.IndicatorCode,
                                                             CellCaled = b.CellCalPrePeriod,
                                                             HtmlTemplateID = b.HtmlTemplateID,
                                                             SchoolID = a.SchoolId
                                                         });
            return listData.ToList();
        }
    }
}
