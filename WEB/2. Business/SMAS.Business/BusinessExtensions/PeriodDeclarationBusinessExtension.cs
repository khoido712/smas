﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// định nghĩa giai đoạn/ đợt
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class PeriodDeclarationBusiness
    {
        public int RESOLUTION_MAXLENGTH = 20;

        #region insert
        /// <summary>
        ///Thêm định nghĩa giai đoạn/ đợt
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertPeriodDeclaration">Đối tượng Insert</param>
        /// <returns></returns>
        public void Insert(PeriodDeclaration insertPeriodDeclaration)
        {

            Validate(insertPeriodDeclaration,false);
            
            base.Insert(insertPeriodDeclaration);
            UpdateIndex(insertPeriodDeclaration);

        }
        #endregion

        #region update
        /// <summary>
        /// Sửa định nghĩa giai đoạn/ đợt
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertPeriodDeclaration">Đối tượng Update</param>
        /// <returns></returns>
        public void Update(PeriodDeclaration insertPeriodDeclaration, bool checkChange)
        {
            Validate(insertPeriodDeclaration, checkChange);
            ValidationMetadata.ValidateObject(insertPeriodDeclaration);
            //Kiểm tra sự tồn tại của PeriodDeclarationID
            CheckAvailable(insertPeriodDeclaration.PeriodDeclarationID, "PeriodDeclaration_Label_PeriodDeclarationID");
            //CheckAvailable(insertPeriodDeclaration.PeriodDeclarationID, "PeriodDeclaration_Label_PeriodDeclarationID", false);

            //var MarkRecordDic = this.GetMaxMark(insertPeriodDeclaration.AcademicYearID, insertPeriodDeclaration.SchoolID.Value, (int)insertPeriodDeclaration.Semester, StartM, StartP, StartV, MaxMarkM, MaxMarkP, MaxMarkV);
            //int MaxM = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_M];
            //int MaxP = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_P];
            //int MaxV = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_V];
            //if (insertPeriodDeclaration.InterviewMark < MaxM)
            //{
            //    throw new BusinessException("SemesterDeclaration_Label_MarkError_M");
            //}
            //if (insertPeriodDeclaration.WritingMark < MaxP)
            //{
            //    throw new BusinessException("SemesterDeclaration_Label_MarkError_P");
            //}
            //if (insertPeriodDeclaration.TwiceCoeffiecientMark < MaxV)
            //{
            //    throw new BusinessException("SemesterDeclaration_Label_MarkError_V");
            //}
            base.Update(insertPeriodDeclaration);
            PeriodDeclarationBusiness.Save();
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa định nghĩa giai đoạn/ đợt
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="PeriodDeclarationId">ID giai đoạn</param>
        public void Delete(int PeriodDeclarationId, int AcademicYearID, int SchoolID)
        {
            PeriodDeclaration PeriodDeclaration = PeriodDeclarationBusiness.Find(PeriodDeclarationId);
            if (PeriodDeclaration == null)
            {
                throw new BusinessException("Đợt được chọn không tồn tại");
            }
            
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodDeclarationID"] = PeriodDeclarationId;
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["PeriodID"] = PeriodDeclarationId;
            dic["Semester"] = PeriodDeclaration.Semester;
            if (academicYear != null)
            {
                dic["Year"] = academicYear.Year;
            }
            //check avai
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration",
                        new Dictionary<string, object>()
                {
                    {"PeriodDeclarationID",PeriodDeclarationId}
                    ,{"AcademicYearID",AcademicYearID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object>() { "PeriodDeclaration_Label_PeriodDeclaration" });
                }
            }
            // Kiem tra thuoc nam hoc
            if (PeriodDeclaration.AcademicYearID != AcademicYearID)
            {
                throw new BusinessException("PeriodDeclaration_Label_School_Err");
            }

            /*if (JudgeRecordBusiness.SearchBySchool(SchoolID, dic).Count() > 0)
            {
                throw new BusinessException("Đợt {0} đã có điểm, thầy/cô không thể xóa đợt này", new List<object>() { PeriodDeclaration.Resolution });
            }

            if (MarkRecordBusiness.SearchBySchool(SchoolID, dic).Count() > 0)
            {
                throw new BusinessException("Đợt {0} đã có điểm, thầy/cô không thể xóa đợt này", new List<object>() { PeriodDeclaration.Resolution });
                //throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "PeriodDeclaration_Label_PeriodDeclaration" });
            }*/

            if (VPupilRankingBusiness.SearchBySchool(SchoolID, dic).Count() > 0)
            {
                throw new BusinessException("Đợt {0} đã có điểm, thầy/cô không thể xóa đợt này", new List<object>() { PeriodDeclaration.Resolution });
                //throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "PeriodDeclaration_Label_PeriodDeclaration" });
            }

            if (SummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Count() > 0)
            {
                throw new BusinessException("Đợt {0} đã có điểm, thầy/cô không thể xóa đợt này", new List<object>() { PeriodDeclaration.Resolution });
                //throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "PeriodDeclaration_Label_PeriodDeclaration" });
            }

           
            // Kiem tra thuoc nam hoc
            if (PeriodDeclaration.AcademicYearID != AcademicYearID)
            {
                throw new BusinessException("PeriodDeclaration_Label_School_Err");
            }

            base.Delete(PeriodDeclarationId);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm giai đoạn/ đợt
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<PeriodDeclaration> Search(IDictionary<string, object> dic)
        {

            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int Period = Utils.GetInt(dic, "PeriodID");
            string Resulotion = Utils.GetString(dic, "Resolution");

            IQueryable<PeriodDeclaration> lsPeriodDeclaration = this.PeriodDeclarationRepository.All;

            if (SchoolID != 0)
            {
                lsPeriodDeclaration = lsPeriodDeclaration.Where(em => (em.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsPeriodDeclaration = lsPeriodDeclaration.Where(em => (em.AcademicYearID == AcademicYearID));
            }

            if (Year != 0)
            {
                lsPeriodDeclaration = lsPeriodDeclaration.Where(em => (em.Year == Year));
            }

            if (Semester != 0)
            {
                lsPeriodDeclaration = lsPeriodDeclaration.Where(em => (em.Semester == Semester));
            }
            if (Period != 0)
            {
                lsPeriodDeclaration = lsPeriodDeclaration.Where(em => (em.PeriodDeclarationID == Period));
            }
            if (Resulotion.Trim().Length != 0)
            {
                lsPeriodDeclaration = lsPeriodDeclaration.Where(em => (em.Resolution.ToUpper().Contains(Resulotion.ToUpper())));
            }
            return lsPeriodDeclaration;
        }

        public List<PeriodDeclaration> SearchBySemester(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SemesterID = Utils.GetInt(dic, "Semester");

            if (AcademicYearID == 0)
            {
                return null;
            }
            if (SchoolID == 0)
            {
                return null;
            }
            if (SemesterID == 0)
            {
                return null;
            }
            return Search(dic).ToList();
        }

        public IQueryable<PeriodDeclaration> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }
        #endregion

        #region date time comparation
        /// <summary>
        /// hàm so sánh 1 khoảng thời gian A có dính líu đến khoảng thời gian B hay không
        /// </summary>
        /// <param name="sourceStart">Bắt đầu thời gian A</param>
        /// <param name="sourceEnd">Kết thúc thời gian A</param>
        /// <param name="destinationStart">Bắt đầu thời gian B</param>
        /// <param name="destinationEnd">Kết thúc thời gian B</param>
        /// <returns>true nếu dính líu và false nếu không</returns>
        public bool CompareDateTimeRange(DateTime sourceStart, DateTime sourceEnd, DateTime destinationStart, DateTime destinationEnd)
        {
            //Có các trường hợp dính líu sau:
            //            |--- Date 1 ---|
            //                 | --- Date 2 --- |


            //                | --- Date 1 --- |
            //         | --- Date 2 ---- |


            //         | -------- Date 1 -------- |
            //               | --- Date 2 --- |

            //               | --- Date 1 --- |
            //         | -------- Date 2 -------- |


            //if (sourceStart == sourceEnd || destinationStart == destinationEnd)
            //    return true; // No actual date range

            if (sourceStart == destinationStart || sourceEnd == destinationEnd)
                return true; // If any set is the same time, then by default there must be some overlap. 

            if (sourceStart < destinationStart)
            {
                if (sourceEnd > destinationStart && sourceEnd < destinationEnd)
                    return true; // Condition 1

                if (sourceEnd > destinationEnd)
                    return true; // Condition 3
            }
            else
            {
                if (destinationEnd > sourceStart && destinationEnd < sourceEnd)
                    return true; // Condition 2

                if (destinationEnd > sourceEnd)
                    return true; // Condition 4
            }

            return false;

        }
        #endregion


        #region validate
        private void Validate(PeriodDeclaration insertPeriodDeclaration, bool? checkChange = null)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYear"] = insertPeriodDeclaration.AcademicYearID;
            dic["Semester"] = insertPeriodDeclaration.Semester;
            dic["SchoolID"] = insertPeriodDeclaration.SchoolID;
            //Số con điểm miệng, điểm 15’, điểm 1 tiết phải là số nguyên dương. –
            //Kiểm tra InterviewMark, WritingMark, TwiceCoeffiecientMark
            //InterviewMark, WritingMark, TwiceCoeffiecientMark: Not null
            Utils.ValidateNull(insertPeriodDeclaration.InterviewMark, "PeriodDeclaration_Label_InterviewMark");
            Utils.ValidateNull(insertPeriodDeclaration.WritingMark, "PeriodDeclaration_Label_WritingMark");
            Utils.ValidateNull(insertPeriodDeclaration.TwiceCoeffiecientMark, "PeriodDeclaration_Label_TwiceCoeffiecientMark");
            if (insertPeriodDeclaration.InterviewMark.HasValue && insertPeriodDeclaration.WritingMark.HasValue && insertPeriodDeclaration.TwiceCoeffiecientMark.HasValue)
            {

                if (insertPeriodDeclaration.InterviewMark == 0 && insertPeriodDeclaration.WritingMark == 0 && insertPeriodDeclaration.TwiceCoeffiecientMark == 0)
                {
                    if (insertPeriodDeclaration.ContaintSemesterMark == null || insertPeriodDeclaration.ContaintSemesterMark == false)
                    {
                        throw new BusinessException("Validate_Mark_Period");
                    }
                }
            }
            if (insertPeriodDeclaration.InterviewMark.HasValue)
            {
                Utils.ValidateRange((int)insertPeriodDeclaration.InterviewMark, 0, 20, "PeriodDeclaration_Label_InterviewMark");
            }
            if (insertPeriodDeclaration.WritingMark.HasValue)
            {
                Utils.ValidateRange((int)insertPeriodDeclaration.WritingMark, 0, 20, "PeriodDeclaration_Label_WritingMark");
            }
            if (insertPeriodDeclaration.TwiceCoeffiecientMark.HasValue)
            {
                Utils.ValidateRange((int)insertPeriodDeclaration.TwiceCoeffiecientMark, 0, 20, "PeriodDeclaration_Label_TwiceCoeffiecientMark");
            }
            //Đến ngày phải lớn hơn từ ngày. Kiểm tra FromDate, EndDate
            Utils.ValidateAfterDate(insertPeriodDeclaration.EndDate.Value, insertPeriodDeclaration.FromDate.Value, "PeriodDeclaration_Label_EndDate", "PeriodDeclaration_Label_FromDate");

            //Đã tồn tại đợt trong khoảng thời gian này. Kiểm tra FromDate, EndDate
            List<PeriodDeclaration> lsPeriodDeclaration = PeriodDeclarationRepository.All.Where(o => o.AcademicYearID == insertPeriodDeclaration.AcademicYearID
                                                          && o.SchoolID == insertPeriodDeclaration.SchoolID && o.PeriodDeclarationID != insertPeriodDeclaration.PeriodDeclarationID
                                                          && o.Semester == insertPeriodDeclaration.Semester).ToList();

            foreach (PeriodDeclaration item in lsPeriodDeclaration)
            {
                if (insertPeriodDeclaration.FromDate.HasValue && insertPeriodDeclaration.EndDate.HasValue)
                {
                    if (item.FromDate.HasValue && item.EndDate.HasValue)
                    {
                        if (CompareDateTimeRange(insertPeriodDeclaration.FromDate.Value, insertPeriodDeclaration.EndDate.Value, item.FromDate.Value, item.EndDate.Value))
                        {
                            throw new BusinessException("PeriodDeclaration_Label_DateConflict", new List<object>() { });
                        }
                    }
                }
            }
            //Resolution: Require, MaxLength(20), Duplicate
            Utils.ValidateRequire(insertPeriodDeclaration.Resolution, "PeriodDeclaration_Label_Resolution");
            Utils.ValidateMaxLength(insertPeriodDeclaration.Resolution, 200, "PeriodDeclaration_Label_Resolution");

            //check trung ten
            IDictionary<string, object> dicP = new Dictionary<string, object>()
            {
                {"SchoolID",insertPeriodDeclaration.SchoolID},
                {"AcademicYearID",insertPeriodDeclaration.AcademicYearID},
                {"Semester",insertPeriodDeclaration.Semester}
            };
            List<string> lstPeriodName = PeriodDeclarationBusiness.SearchBySemester(dicP).Where(p => p.PeriodDeclarationID != insertPeriodDeclaration.PeriodDeclarationID)
                                         .Select(p => p.Resolution).ToList();
            bool checkDuplicate = lstPeriodName.Contains(insertPeriodDeclaration.Resolution);
            //bool checkDuplicate = repository.CheckDuplicate(insertPeriodDeclaration.Resolution, GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration", "Resolution", false, insertPeriodDeclaration.PeriodDeclarationID);
            if (checkDuplicate)
            {
                throw new BusinessException("PeriodDeclaration_Label_ResolutionIsCreated");
            }

            // tu Ngày phải >= ngày bắt đầu học kỳ và <= ngày kết thúc học kỳ. Kiểm tra FromDate
            AcademicYear academicYear = AcademicYearRepository.Find(insertPeriodDeclaration.AcademicYearID);
            if (academicYear != null && insertPeriodDeclaration.Semester.HasValue)
            {
                if (insertPeriodDeclaration.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    try
                    {
                        Utils.ValidateAfterDate(insertPeriodDeclaration.FromDate.Value, academicYear.FirstSemesterStartDate.Value, "PeriodDeclaration_Label_FromDate", "AcademicYear_Label_FirstSemesterStartDate");
                        Utils.ValidateAfterDate(academicYear.FirstSemesterEndDate.Value, insertPeriodDeclaration.FromDate.Value, "AcademicYear_Label_FirstSemesterEndDate", "PeriodDeclaration_Label_FromDate");

                    }
                    catch (Exception)
                    {

                        throw new BusinessException("PeriodDeclaration_Label_FromDateHK1Error");
                    }

                    try
                    {
                        //Ngày đến phải >= ngày bắt đầu học kỳ và <= ngày kết thúc học kỳ.Kiểm tra EndDate
                        Utils.ValidateAfterDate(insertPeriodDeclaration.EndDate.Value, academicYear.FirstSemesterStartDate.Value, "PeriodDeclaration_Label_EndDate", "AcademicYear_Label_FirstSemesterStartDate");
                        Utils.ValidateAfterDate(academicYear.FirstSemesterEndDate.Value, insertPeriodDeclaration.EndDate.Value, "AcademicYear_Label_FirstSemesterEndDate", "PeriodDeclaration_Label_EndDate");

                    }
                    catch (Exception)
                    {

                        throw new BusinessException("PeriodDeclaration_Label_EndDateHK1Error");
                    }

                }
                else if (insertPeriodDeclaration.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    try
                    {
                        Utils.ValidateAfterDate(insertPeriodDeclaration.FromDate.Value, academicYear.SecondSemesterStartDate.Value, "PeriodDeclaration_Label_FromDate", "AcademicYear_Label_SecondSemesterStartDate");
                        Utils.ValidateAfterDate(academicYear.SecondSemesterEndDate.Value, insertPeriodDeclaration.FromDate.Value, "AcademicYear_Label_SecondSemesterEndDate", "PeriodDeclaration_Label_FromDate");
                    }
                    catch (Exception)
                    {

                        throw new BusinessException("PeriodDeclaration_Label_FromDateHK2Error");
                    }
                    try
                    {
                        //Ngày đến phải >= ngày bắt đầu học kỳ và <= ngày kết thúc học kỳ.Kiểm tra EndDate
                        Utils.ValidateAfterDate(insertPeriodDeclaration.EndDate.Value, academicYear.SecondSemesterStartDate.Value, "PeriodDeclaration_Label_EndDate", "AcademicYear_Label_SecondSemesterStartDate");
                        Utils.ValidateAfterDate(academicYear.SecondSemesterEndDate.Value, insertPeriodDeclaration.EndDate.Value, "AcademicYear_Label_SecondSemesterEndDate", "PeriodDeclaration_Label_EndDate");

                    }
                    catch (Exception)
                    {

                        throw new BusinessException("PeriodDeclaration_Label_EndDateHK2Error");
                    }

                }

            }
            //Tổng số điểm miệng không được vượt quá số con điểm miệng  khai báo cho trường. Kiểm tra sum(InterviewMark)

            //lay ra so diem trong hoc ky
            SemeterDeclaration Semeter = SemeterDeclarationRepository.All.
                Where(o => (o.SchoolID == insertPeriodDeclaration.SchoolID))
                .Where(o => (o.AcademicYearID == insertPeriodDeclaration.AcademicYearID))
                .Where(o => (o.Semester == insertPeriodDeclaration.Semester))
                .Where(o => DateTime.Compare(o.AppliedDate, DateTime.Now) <= 0).OrderByDescending(o => o.AppliedDate).FirstOrDefault();

            if (Semeter == null)
            {
                throw new BusinessException("PeriodDeclaration_Label_SemesterNull");
            }
            Utils.ValidateNull(Semeter.InterviewMark, "SemeterDeclaration_Label_InterviewMark");
            Utils.ValidateNull(Semeter.WritingMark, "SemeterDeclaration_Label_WritingMark");
            Utils.ValidateNull(Semeter.TwiceCoeffiecientMark, "SemeterDeclaration_Label_TwiceCoeffiecientMark");


            //tim so luong con diem da ton tai trong hoc ky cung voi hoc ky cua insert object
            List<int?> interviewMarkRegisterCountNow = lsPeriodDeclaration.Where(o => (o.Semester == insertPeriodDeclaration.Semester))
                .Select(o => o.InterviewMark).ToList();
            int interviewMarkRegisterSum = 0;
            foreach (var item in interviewMarkRegisterCountNow)
            {
                if (item.HasValue)
                {
                    interviewMarkRegisterSum += (int)item;
                }
            }
            interviewMarkRegisterSum += (insertPeriodDeclaration.InterviewMark.HasValue) ? insertPeriodDeclaration.InterviewMark.Value : 0;
            if (interviewMarkRegisterSum > Semeter.InterviewMark)
            {
                throw new BusinessException("PeriodDeclaration_Label_MaxInterviewMark");
            }


            //Tổng số điểm miệng không được vượt quá số con điểm miệng  khai báo cho trường. Kiểm tra sum(InterviewMark)
            List<int?> WritingMarkCountNow = lsPeriodDeclaration.Where(o => (o.Semester == insertPeriodDeclaration.Semester))
                 .Select(o => o.WritingMark).ToList();
            int WritingMarkCountSum = 0;
            foreach (var item in WritingMarkCountNow)
            {
                if (item.HasValue)
                {
                    WritingMarkCountSum += (int)item;
                }
            }
            WritingMarkCountSum += (insertPeriodDeclaration.WritingMark.HasValue) ? insertPeriodDeclaration.WritingMark.Value : 0;
            if (WritingMarkCountSum > Semeter.WritingMark)
            {
                throw new BusinessException("PeriodDeclaration_Label_MaxWritingMark");
            }


            //Tổng số điểm miệng không được vượt quá số con điểm miệng  khai báo cho trường. Kiểm tra sum(InterviewMark)
            List<int?> TwiceCoeffiecientMarkCountNow = lsPeriodDeclaration.Where(o => (o.Semester == insertPeriodDeclaration.Semester))
                 .Select(o => o.TwiceCoeffiecientMark).ToList();
            int TwiceCoeffiecientMarkCountSum = 0;
            foreach (var item in TwiceCoeffiecientMarkCountNow)
            {
                if (item.HasValue)
                {
                    TwiceCoeffiecientMarkCountSum += (int)item;
                }
            }
            TwiceCoeffiecientMarkCountSum += (insertPeriodDeclaration.TwiceCoeffiecientMark.HasValue) ? insertPeriodDeclaration.TwiceCoeffiecientMark.Value : 0;
            if (TwiceCoeffiecientMarkCountSum > Semeter.TwiceCoeffiecientMark)
            {
                throw new BusinessException("PeriodDeclaration_Label_MaxTwiceCoeffiecientMark");
            }

            //Năm hoc không tồn tại hoặc đã bị xóa – Kiểm tra AcademicYearID
            AcademicYearBusiness.CheckAvailable(insertPeriodDeclaration.AcademicYearID, "AcademicYear_Label_AcademicYearID");
            //Trường học không tồn tai hoặc đã bị xóa – Kiểm tra SchoolID
            SchoolProfileBusiness.CheckAvailable(insertPeriodDeclaration.SchoolID, "UserInfo_SchoolID");

            //Check con diem
            string strMarkType = "";
            if (!checkChange.HasValue || checkChange.Value)
            {
                //Kiem tra so con diem da nhap nhung dot sau
                List<PeriodDeclaration> lstpe = PeriodDeclarationBusiness.SearchBySchool(insertPeriodDeclaration.SchoolID.Value, dic).Where(o => o.FromDate > insertPeriodDeclaration.EndDate).OrderBy(o => o.FromDate).ToList();
                foreach (PeriodDeclaration pd in lstpe)
                {
                    //for (int i = pd.StartIndexOfInterviewMark.Value; i < pd.StartIndexOfInterviewMark + pd.InterviewMark; i++)
                    //{
                    //    strMarkType += "M" + i.ToString();
                    //}
                    //for (int i = pd.StartIndexOfWritingMark.Value; i < pd.StartIndexOfWritingMark + pd.WritingMark; i++)
                    //{
                    //    strMarkType += "P" + i.ToString();
                    //}
                    //for (int i = pd.StartIndexOfTwiceCoeffiecientMark.Value; i < pd.StartIndexOfTwiceCoeffiecientMark + pd.TwiceCoeffiecientMark; i++)
                    //{
                    //    strMarkType += "V" + i.ToString();
                    //}
                    strMarkType += (pd.StrInterviewMark + pd.StrWritingMark + pd.StrTwiceCoeffiecientMark);

                }

                //Neu da nhap diem thi ko cho sua
                //if (MarkRecordBusiness.SearchBySchool(insertPeriodDeclaration.SchoolID.Value, dic).Where(o => strMarkType.Contains(o.Title)).Count() > 0)
                //{
                //    throw new BusinessException("PeriodDeclaration_Label_MarkCount");
                //}


            }

        }
        #endregion

        private IDictionary<string, object> GetMaxMark(int AcademicYearID, int SchoolID, int Semester, int StartM, int StartP, int StartV, int MaxMarkM, int MaxMarkP, int MaxMarkV)
        {
            int MaxM = 0;
            int MaxP = 0;
            int MaxV = 0;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> MarkRecordDic = new Dictionary<string, object>();


            //PeriodDeclaration p = PeriodDeclarationBusiness.Find(PeriodID);

            //So con diem cua dot
            string markM = "";
            string markP = "";
            string markV = "";
            for (int i = StartM; i < MaxMarkM + StartM; i++)
            {
                markM += "M" + i.ToString();
            }
            for (int i = StartP; i < MaxMarkP + StartP; i++)
            {
                markP += "P" + i.ToString();
            }
            for (int i = StartV; i < MaxMarkV + StartV; i++)
            {
                markV += "V" + i.ToString();
            }
            IQueryable<MarkRecord> lsMarkRecord = MarkRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID }
                    ,{"Semester",Semester }
                    ,{"Year", academicYear.Year}
                });

            if (lsMarkRecord.Count() > 0)
            {
                IQueryable<MarkRecord> lsMarkRecordM = lsMarkRecord.Where(o => markM.Contains(o.Title));
                if (lsMarkRecordM.Count() > 0)
                {
                    MaxM = lsMarkRecordM.Select(o => o.OrderNumber).Max();
                }
                IQueryable<MarkRecord> lsMarkRecordP = lsMarkRecord.Where(o => markP.Contains(o.Title));
                if (lsMarkRecordP.Count() > 0)
                {
                    MaxP = lsMarkRecordP.Select(o => o.OrderNumber).Max();
                }
                IQueryable<MarkRecord> lsMarkRecordV = lsMarkRecord.Where(o => markV.Contains(o.Title));
                if (lsMarkRecordV.Count() > 0)
                {
                    MaxV = lsMarkRecordV.Select(o => o.OrderNumber).Max();
                }
            }



            MarkRecordDic[SystemParamsInFile.MARK_TYPE_M] = MaxM - StartM + 1;
            MarkRecordDic[SystemParamsInFile.MARK_TYPE_P] = MaxP - StartP + 1;
            MarkRecordDic[SystemParamsInFile.MARK_TYPE_V] = MaxV - StartV + 1;


            return MarkRecordDic;

        }
        private void UpdateIndex(PeriodDeclaration insertPeriodDeclaration)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYear"] = insertPeriodDeclaration.AcademicYearID;
            dic["Semester"] = insertPeriodDeclaration.Semester;
            dic["SchoolID"] = insertPeriodDeclaration.SchoolID;
            //Cap nhat lai index
            List<PeriodDeclaration> lstpe = PeriodDeclarationBusiness.SearchBySchool(insertPeriodDeclaration.SchoolID.Value, dic).Where(o => o.FromDate > insertPeriodDeclaration.EndDate).OrderBy(o => o.FromDate).ToList();
            IQueryable<PeriodDeclaration> lsPeriD = PeriodDeclarationBusiness.SearchBySchool(insertPeriodDeclaration.SchoolID.Value, new Dictionary<string, object>()
                    {
                       {"AcademicYearID",insertPeriodDeclaration.AcademicYearID},
                        {"Semester",insertPeriodDeclaration.Semester}
                    });
            foreach (PeriodDeclaration pd in lstpe)
            {
                IQueryable<PeriodDeclaration> lsPD = lsPeriD.Where(o => DateTime.Compare(o.FromDate.Value, pd.FromDate.Value) < 0);
                pd.StartIndexOfInterviewMark = (int?)((lsPD.Sum(o => o.InterviewMark).HasValue ? lsPD.Sum(o => o.InterviewMark).Value : 0) + 1);
                pd.StartIndexOfWritingMark = (int?)((lsPD.Sum(o => o.WritingMark).HasValue ? lsPD.Sum(o => o.WritingMark).Value : 0) + 1);
                pd.StartIndexOfTwiceCoeffiecientMark = (int?)((lsPD.Sum(o => o.TwiceCoeffiecientMark).HasValue ? lsPD.Sum(o => o.TwiceCoeffiecientMark).Value : 0) + 1);
                //Cap nhat lai index cho dot
                base.Update(pd);
            }
        }

        /// <summary>
        /// 26/03/2014
        /// Quanglm1
        /// Khoa dot hoac mo khoa dot, cap nhat lai thong tin khoa con diem
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        public void LockPeriod(int SchoolID, int AcademicYearID, int Semester, int PeriodID)
        {
            this.context.PROC_LOCK_PERIOD(SchoolID, AcademicYearID, Semester, PeriodID);
        }
    }
}
