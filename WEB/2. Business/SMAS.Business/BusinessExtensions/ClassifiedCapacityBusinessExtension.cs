﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ClassifiedCapacityBusiness
    {

        #region Lấy mảng băm cho các tham số đầu vào của thống kê xếp loại học lực
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê xếp loại học lực
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKey(ClassifiedCapacityBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// <summary>
        /// Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetClassifiedCapacity(ClassifiedCapacityBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_HOC_LUC;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin thống kê xếp loại học lực
        /// <summary>
        /// Lưu lại thông tin thống kê xếp loại học lực
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertClassifiedCapacity(ClassifiedCapacityBO entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_HOC_LUC;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeXLHL_[Học kỳ]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lưu lại thông tin sổ gọi tên và ghi điểm
        /// <summary>
        /// Lưu lại thông tin sổ gọi tên và ghi điểm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateClassifiedCapacity(ClassifiedCapacityBO entity)
        {
            AcademicYear ac = this.AcademicYearBusiness.Find(entity.AcademicYearID);
            string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_HOC_LUC;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            string SemesterName = "";
            string SchoolName = school.SchoolName.ToUpper();
            string AcademicYear = ac.DisplayTitle;
            string Suppervising = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string Province = (school.District != null ? school.District.DistrictName : "");

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) SemesterName = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) SemesterName = "HỌC KỲ II";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) SemesterName = "CẢ NĂM";

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange rangeFirst = firstSheet.GetRange("A13", "Q13");
            IVTRange range = firstSheet.GetRange("A14", "Q14");
            IVTRange rangeClass = firstSheet.GetRange("A12", "Q12");
            IVTRange rangeLastClass = firstSheet.GetRange("A15", "Q15");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            //Lấy dữ liệu 
            IDictionary<string, object> Dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                //{"Semester", entity.Semester},
                {"PeriodID" , null},
                {"SchoolID", entity.SchoolID}
            };

            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                //{"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking);
            IQueryable<PupilOfClassBO> lstPoc = from p in lstQPoc.AddCriteriaSemester(Aca, entity.Semester)
                                              join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                              join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                                              where (!r.IsVnenClass.HasValue || (r.IsVnenClass.HasValue && r.IsVnenClass.Value == false))
                                              && r.IsActive.Value
                                              select new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  ClassID = p.ClassID,
                                                  EducationLevelID = r.EducationLevelID,
                                                  Genre = q.Genre,
                                                  EthnicID = q.EthnicID
                                              };


            List<PupilRankingBO> lstCultureRank = new List<PupilRankingBO>();
            // Lây list pupilranking
            IQueryable<PupilRankingBO> lstCultureRankAllStatus = from p in this.VPupilRankingBusiness.SearchBySchool(entity.SchoolID, Dic).Where(o => o.PeriodID == null)
                                          join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                          join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                                          where (!r.IsVnenClass.HasValue || (r.IsVnenClass.HasValue && r.IsVnenClass.Value == false))
                                          && r.IsActive.Value
                                          select new PupilRankingBO
                                          {
                                              PupilID = p.PupilID,
                                              ClassID = p.ClassID, 
                                              EducationLevelID = r.EducationLevelID,
                                              Genre = q.Genre, 
                                              EthnicID = q.EthnicID,
                                              Semester = p.Semester,
                                              CapacityLevelID = p.CapacityLevelID
                                          };
            // Lay danh sach hoc sinh thuc te
           
            if (entity.Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstCultureRankAllStatus = lstCultureRankAllStatus.Where(o => o.Semester == entity.Semester);
                lstCultureRank = (from u in lstCultureRankAllStatus
                                  where lstPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select u).ToList();
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstCultureRank = (from u in lstCultureRankAllStatus.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                  where lstPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                  select u).ToList();
            }
            //ID dan toc kinh, nuoc ngoai
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            List<PupilRankingBO> lstCultureRank_Ethnic = lstCultureRank.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            List<PupilRankingBO> lstCultureRank_FemaleEthnic = lstCultureRank.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            // Lấy danh sách khối học
            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            //Lấy danh sách học sinh
            //var listCountPupilInClassBase = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            //{
            //    {"AcademicYearID", entity.AcademicYearID},
            //    {"AppliedLevel", entity.AppliedLevel}                
            //}).GetPupil(Aca, entity.Semester);

            //var listCountPupilInClass = listCountPupilInClassBase.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();

            var listCountCurentPupilByClass = lstPoc.GroupBy(o => new { o.ClassID, o.EducationLevelID, o.Genre, o.EthnicID }).Select(o => new { ClassID = o.Key.ClassID, EducationLevelID = o.Key.EducationLevelID, Genre = o.Key.Genre, EthnicID = o.Key.EthnicID,  TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_Ethnic = listCountCurentPupilByClass.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            var listCountCurentPupilByClass_Female = listCountCurentPupilByClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var listCountCurentPupilByClass_FemaleEthnic = listCountCurentPupilByClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            // Danh sach lop hoc
            IDictionary<string, object> dic_ClassProfile = new Dictionary<string, object> {
                    {"AcademicYearID", entity.AcademicYearID},
                    {"SchoolID", entity.SchoolID},
                    {"AppliedLevel", entity.AppliedLevel},
                    {"IsVNEN", true}   // tutv khong thong ke lop hoc ENVN
                };
            List<ClassProfileTempBO> listAllClass = this.ClassProfileBusiness.SearchBySchool(entity.SchoolID, dic_ClassProfile)
                .Select(o => new ClassProfileTempBO
                {
                    ClassProfileID = o.ClassProfileID,
                    EducationLevelID = o.EducationLevelID,
                    OrderNumber = o.OrderNumber,
                    SchoolID = o.SchoolID,
                    AcademicYearID = o.AcademicYearID,
                    DisplayName = o.DisplayName,
                    EmployeeID = o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0,
                    EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty
                })
                .ToList();
            #region Tạo dữ liệu cho sheet học lực chung
            //Fill dữ liệu chung
            sheet.Name = "TKXLHL";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("H4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            sheet.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC ");
            if (entity.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue("A7", SemesterName + " - NĂM HỌC: " + AcademicYear);
            }
            else
            {
                sheet.SetCellValue("A7",  "NĂM HỌC: " + AcademicYear);
            }

            int startrow = 13;

            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int startrowClass = startrow - 1;

                // List Class Profile
                int educationLevelID = lstEducationLevel[i].EducationLevelID;
                List<ClassProfileTempBO> lstClassProfile = listAllClass.Where(o => o.EducationLevelID == educationLevelID)
                    .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                    .ThenBy(o => o.DisplayName).ToList();
                List<PupilRankingBO> lstCultureRank_Edu = lstCultureRank.Where(o => o.EducationLevelID == educationLevelID).ToList();
                var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass.Where(o => o.EducationLevelID == educationLevelID).ToList();
                for (int j = 0; j < lstClassProfile.Count; j++)
                {
                    int classID = lstClassProfile[j].ClassProfileID;
                    List<PupilRankingBO> listPrByClass = lstCultureRank_Edu.Where(o => o.ClassID == classID).ToList();
                    // Lấy số lượng học sinh có học lực Giỏi
                    int HSG = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT);

                    // lấy số lượng học sinh có học lực khá
                    int HSKH = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD);

                    // lấy số lượng học sinh có học lực trung bình
                    int HSTB = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL);

                    // lấy số lượng học sinh có học lực yếu
                    int HSY = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK);

                    // lấy số lượng học sinh có học lực kém
                    int HSK = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR);

                    //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào
                    var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                    int sumPupilCuurent = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                    // fill dữ liệu từng lớp vào excel
                    if (j == lstClassProfile.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeLastClass, startrow);
                    }
                    else if (j == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(rangeFirst, startrow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(range, startrow);
                    }

                    sheet.SetCellValue(startrow, 1, (i + 1) + "." + (j + 1).ToString());
                    sheet.SetCellValue(startrow, 2, lstClassProfile[j].DisplayName);
                    sheet.SetCellValue(startrow, 3, lstClassProfile[j].EmployeeName);
                    sheet.SetCellValue(startrow, 4, sumPupilCuurent);
                    sheet.SetCellValue(startrow, 5, HSG);
                    sheet.SetCellValue(startrow, 6, "=ROUND(E" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                    sheet.SetCellValue(startrow, 7, HSKH);
                    sheet.SetCellValue(startrow, 8, "=ROUND(G" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                    sheet.SetCellValue(startrow, 9, HSTB);
                    sheet.SetCellValue(startrow, 10, "=ROUND(I" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                    sheet.SetCellValue(startrow, 11, HSY);
                    sheet.SetCellValue(startrow, 12, "=ROUND(K" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                    sheet.SetCellValue(startrow, 13, HSK);
                    sheet.SetCellValue(startrow, 14, "=ROUND(M" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                    sheet.SetCellValue(startrow, 15, "=SUM(E" + startrow.ToString() + ",G" + startrow.ToString() + ",I" + startrow.ToString() + ")");
                    sheet.SetCellValue(startrow, 16, "=ROUND(O" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                    startrow++;
                }

                // fill vào excel từng Khối
                sheet.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                sheet.SetCellValue(startrowClass, 1, i + 1);
                sheet.SetCellValue(startrowClass, 2, lstEducationLevel[i].Resolution);
                if (lstClassProfile != null && lstClassProfile.Count > 0)
                {
                    sheet.SetCellValue(startrowClass, 4, "=SUM(D" + (startrowClass + 1).ToString() + ":D" + (startrow - 1).ToString() + ")");
                    sheet.SetCellValue(startrowClass, 5, "=SUM(E" + (startrowClass + 1).ToString() + ":E" + (startrow - 1).ToString() + ")");
                    sheet.SetCellValue(startrowClass, 6, "=ROUND(E" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    sheet.SetCellValue(startrowClass, 7, "=SUM(G" + (startrowClass + 1).ToString() + ":G" + (startrow - 1).ToString() + ")");
                    sheet.SetCellValue(startrowClass, 8, "=ROUND(G" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    sheet.SetCellValue(startrowClass, 9, "=SUM(I" + (startrowClass + 1).ToString() + ":I" + (startrow - 1).ToString() + ")");
                    sheet.SetCellValue(startrowClass, 10, "=ROUND(I" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    sheet.SetCellValue(startrowClass, 11, "=SUM(K" + (startrowClass + 1).ToString() + ":K" + (startrow - 1).ToString() + ")");
                    sheet.SetCellValue(startrowClass, 12, "=ROUND(K" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    sheet.SetCellValue(startrowClass, 13, "=SUM(M" + (startrowClass + 1).ToString() + ":M" + (startrow - 1).ToString() + ")");
                    sheet.SetCellValue(startrowClass, 14, "=ROUND(M" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    sheet.SetCellValue(startrowClass, 15, "=SUM(E" + startrowClass.ToString() + ",G" + startrowClass.ToString() + ",I" + startrowClass.ToString() + ")");
                    sheet.SetCellValue(startrowClass, 16, "=ROUND(O" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                }
                else
                {
                    sheet.SetCellValue(startrowClass, 4, 0);
                    sheet.SetCellValue(startrowClass, 5, 0);
                    sheet.SetCellValue(startrowClass, 6, 0);
                    sheet.SetCellValue(startrowClass, 7, 0);
                    sheet.SetCellValue(startrowClass, 8, 0);
                    sheet.SetCellValue(startrowClass, 9, 0);
                    sheet.SetCellValue(startrowClass, 10, 0);
                    sheet.SetCellValue(startrowClass, 11, 0);
                    sheet.SetCellValue(startrowClass, 12, 0);
                    sheet.SetCellValue(startrowClass, 13, 0);
                    sheet.SetCellValue(startrowClass, 14, 0);
                    sheet.SetCellValue(startrowClass, 15, 0);
                    sheet.SetCellValue(startrowClass, 16, 0);
                }
                startrow++;
            }

            // fill vào excel dữ liệu row tổng của trường
            sheet.SetCellValue(11, 1, "Toàn trường");
            sheet.SetCellValue(11, 4, "=SUM(D12:D" + (startrow - 2).ToString() + ")/2");
            sheet.SetCellValue(11, 5, "=SUM(E12:E" + (startrow - 2).ToString() + ")/2");
            sheet.SetCellValue(11, 6, "=ROUND(E" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
            sheet.SetCellValue(11, 7, "=SUM(G12:G" + (startrow - 2).ToString() + ")/2");
            sheet.SetCellValue(11, 8, "=ROUND(G" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
            sheet.SetCellValue(11, 9, "=SUM(I12:I" + (startrow - 2).ToString() + ")/2");
            sheet.SetCellValue(11, 10, "=ROUND(I" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
            sheet.SetCellValue(11, 11, "=SUM(K12:K" + (startrow - 2).ToString() + ")/2");
            sheet.SetCellValue(11, 12, "=ROUND(K" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
            sheet.SetCellValue(11, 13, "=SUM(M12:M" + (startrow - 2).ToString() + ")/2");
            sheet.SetCellValue(11, 14, "=ROUND(M" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
            sheet.SetCellValue(11, 15, "=SUM(E11,G11,I11)");
            sheet.SetCellValue(11, 16, "=ROUND(O" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
            sheet.CopyPasteSameRowHeigh(firstSheet.GetRange(startrow + 1, 1, startrow + 1, 12), startrow + 1);
            sheet.GetRange(startrow + 1, 12, startrow + 1, 16).Merge();
            sheet.SetCellValue(startrow + 1, 12, "Người lập báo cáo");
            sheet.GetRange(startrow + 1, 12, startrow + 1, 12).SetFontStyle(true, null, false, null, false, false);
            sheet.PageSize = VTXPageSize.VTxlPaperA4;
            sheet.FitAllColumnsOnOnePage = true;
            #endregion
           

            if (entity.FemaleChecked)
            {
                #region Tạo sheet thống kê học lực học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(firstSheet);
                //Fill dữ liệu chung
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetCellValue("A2", Suppervising);
                sheet_Female.SetCellValue("A3", SchoolName);
                sheet_Female.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                sheet_Female.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC HỌC SINH NỮ");
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    sheet_Female.SetCellValue("A7", "NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet_Female.SetCellValue("A7", SemesterName + " - NĂM HỌC: " + AcademicYear);
                }

                startrow = 13;

                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowClass = startrow - 1;

                    // List Class Profile
                    int educationLevelID = lstEducationLevel[i].EducationLevelID;
                    List<ClassProfileTempBO> lstClassProfile = listAllClass.Where(o => o.EducationLevelID == educationLevelID)
                        .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                        .ThenBy(o => o.DisplayName).ToList();
                    List<PupilRankingBO> lstCultureRank_Edu = lstCultureRank.Where(o => o.EducationLevelID == educationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_Female.Where(o => o.EducationLevelID == educationLevelID).ToList();
                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int classID = lstClassProfile[j].ClassProfileID;
                        List<PupilRankingBO> listPrByClass = lstCultureRank_Edu.Where(o => o.ClassID == classID).ToList();
                        // Lấy số lượng học sinh có học lực Giỏi
                        int HSG = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT);

                        // lấy số lượng học sinh có học lực khá
                        int HSKH = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD);

                        // lấy số lượng học sinh có học lực trung bình
                        int HSTB = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL);

                        // lấy số lượng học sinh có học lực yếu
                        int HSY = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK);

                        // lấy số lượng học sinh có học lực kém
                        int HSK = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR);


                        //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào
                        var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                        int sumPupilCuurent = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;
                        // fill dữ liệu từng lớp vào excel
                        if (j == lstClassProfile.Count - 1)
                        {
                            sheet_Female.CopyPasteSameRowHeigh(rangeLastClass, startrow);
                        }
                        else if (j == 0)
                        {
                            sheet_Female.CopyPasteSameRowHeigh(rangeFirst, startrow);
                        }
                        else
                        {
                            sheet_Female.CopyPasteSameRowHeigh(range, startrow);
                        }

                        sheet_Female.SetCellValue(startrow, 1, (i + 1) + "." + (j + 1).ToString());
                        sheet_Female.SetCellValue(startrow, 2, lstClassProfile[j].DisplayName);
                        sheet_Female.SetCellValue(startrow, 3, lstClassProfile[j].EmployeeName);
                        sheet_Female.SetCellValue(startrow, 4, sumPupilCuurent);
                        sheet_Female.SetCellValue(startrow, 5, HSG);
                        sheet_Female.SetCellValue(startrow, 6, "=ROUND(E" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Female.SetCellValue(startrow, 7, HSKH);
                        sheet_Female.SetCellValue(startrow, 8, "=ROUND(G" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Female.SetCellValue(startrow, 9, HSTB);
                        sheet_Female.SetCellValue(startrow, 10, "=ROUND(I" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Female.SetCellValue(startrow, 11, HSY);
                        sheet_Female.SetCellValue(startrow, 12, "=ROUND(K" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Female.SetCellValue(startrow, 13, HSK);
                        sheet_Female.SetCellValue(startrow, 14, "=ROUND(M" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Female.SetCellValue(startrow, 15, "=SUM(E" + startrow.ToString() + ",G" + startrow.ToString() + ",I" + startrow.ToString() + ")");
                        sheet_Female.SetCellValue(startrow, 16, "=ROUND(O" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        startrow++;
                    }

                    // fill vào excel từng Khối
                    sheet_Female.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                    sheet_Female.SetCellValue(startrowClass, 1, i + 1);
                    sheet_Female.SetCellValue(startrowClass, 2, lstEducationLevel[i].Resolution);
                    if (lstClassProfile != null && lstClassProfile.Count > 0)
                    {
                        sheet_Female.SetCellValue(startrowClass, 4, "=SUM(D" + (startrowClass + 1).ToString() + ":D" + (startrow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 5, "=SUM(E" + (startrowClass + 1).ToString() + ":E" + (startrow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 6, "=ROUND(E" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Female.SetCellValue(startrowClass, 7, "=SUM(G" + (startrowClass + 1).ToString() + ":G" + (startrow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 8, "=ROUND(G" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Female.SetCellValue(startrowClass, 9, "=SUM(I" + (startrowClass + 1).ToString() + ":I" + (startrow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 10, "=ROUND(I" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Female.SetCellValue(startrowClass, 11, "=SUM(K" + (startrowClass + 1).ToString() + ":K" + (startrow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 12, "=ROUND(K" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Female.SetCellValue(startrowClass, 13, "=SUM(M" + (startrowClass + 1).ToString() + ":M" + (startrow - 1).ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 14, "=ROUND(M" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Female.SetCellValue(startrowClass, 15, "=SUM(E" + startrowClass.ToString() + ",G" + startrowClass.ToString() + ",I" + startrowClass.ToString() + ")");
                        sheet_Female.SetCellValue(startrowClass, 16, "=ROUND(O" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    }
                    else
                    {
                        sheet_Female.SetCellValue(startrowClass, 4, 0);
                        sheet_Female.SetCellValue(startrowClass, 5, 0);
                        sheet_Female.SetCellValue(startrowClass, 6, 0);
                        sheet_Female.SetCellValue(startrowClass, 7, 0);
                        sheet_Female.SetCellValue(startrowClass, 8, 0);
                        sheet_Female.SetCellValue(startrowClass, 9, 0);
                        sheet_Female.SetCellValue(startrowClass, 10, 0);
                        sheet_Female.SetCellValue(startrowClass, 11, 0);
                        sheet_Female.SetCellValue(startrowClass, 12, 0);
                        sheet_Female.SetCellValue(startrowClass, 13, 0);
                        sheet_Female.SetCellValue(startrowClass, 14, 0);
                        sheet_Female.SetCellValue(startrowClass, 15, 0);
                        sheet_Female.SetCellValue(startrowClass, 16, 0);
                    }
                    startrow++;
                }

                // fill vào excel dữ liệu row tổng của trường
                sheet_Female.SetCellValue(11, 1, "Toàn trường");
                sheet_Female.SetCellValue(11, 4, "=SUM(D12:D" + (startrow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue(11, 5, "=SUM(E12:E" + (startrow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue(11, 6, "=ROUND(E" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Female.SetCellValue(11, 7, "=SUM(G12:G" + (startrow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue(11, 8, "=ROUND(G" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Female.SetCellValue(11, 9, "=SUM(I12:I" + (startrow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue(11, 10, "=ROUND(I" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Female.SetCellValue(11, 11, "=SUM(K12:K" + (startrow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue(11, 12, "=ROUND(K" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Female.SetCellValue(11, 13, "=SUM(M12:M" + (startrow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue(11, 14, "=ROUND(M" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Female.SetCellValue(11, 15, "=SUM(E11,G11,I11)");
                sheet_Female.SetCellValue(11, 16, "=ROUND(O" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Female.CopyPasteSameRowHeigh(firstSheet.GetRange(startrow + 1, 1, startrow + 1, 12), startrow + 1);
                sheet_Female.GetRange(startrow + 1, 12, startrow + 1, 16).Merge();
                sheet_Female.SetCellValue(startrow + 1, 12, "Người lập báo cáo");
                sheet_Female.FitAllColumnsOnOnePage = true;
                //sheet.Delete();
                #endregion 
            }
            
            if (entity.EthnicChecked)
            {
                #region Tạo sheet Thống kê học lực học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(firstSheet);
                //Fill dữ liệu chung
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.SetCellValue("A2", Suppervising);
                sheet_Ethnic.SetCellValue("A3", SchoolName);
                sheet_Ethnic.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                sheet_Ethnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC HỌC SINH DÂN TỘC");
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    sheet_Ethnic.SetCellValue("A7", "NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet_Ethnic.SetCellValue("A7", SemesterName + " - NĂM HỌC: " + AcademicYear);
                }

                startrow = 13;

                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowClass = startrow - 1;
                    // List Class Profile
                    int educationLevelID = lstEducationLevel[i].EducationLevelID;
                    List<ClassProfileTempBO> lstClassProfile = listAllClass.Where(o => o.EducationLevelID == educationLevelID)
                        .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                        .ThenBy(o => o.DisplayName).ToList();
                    List<PupilRankingBO> lstCultureRank_Edu = lstCultureRank_Ethnic.Where(o => o.EducationLevelID == educationLevelID).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_Ethnic.Where(o => o.EducationLevelID == educationLevelID).ToList();
                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int classID = lstClassProfile[j].ClassProfileID;
                        List<PupilRankingBO> listPrByClass = lstCultureRank_Edu.Where(o => o.ClassID == classID).ToList();
                        // Lấy số lượng học sinh có học lực Giỏi
                        int HSG = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT);

                        // lấy số lượng học sinh có học lực khá
                        int HSKH = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD);

                        // lấy số lượng học sinh có học lực trung bình
                        int HSTB = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL);

                        // lấy số lượng học sinh có học lực yếu
                        int HSY = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK);

                        // lấy số lượng học sinh có học lực kém
                        int HSK = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR);


                        //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào
                        var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                        int sumPupilCuurent = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                        // fill dữ liệu từng lớp vào excel
                        if (j == lstClassProfile.Count - 1)
                        {
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangeLastClass, startrow);
                        }
                        else if (j == 0)
                        {
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangeFirst, startrow);
                        }
                        else
                        {
                            sheet_Ethnic.CopyPasteSameRowHeigh(range, startrow);
                        }

                        sheet_Ethnic.SetCellValue(startrow, 1, (i + 1) + "." + (j + 1).ToString());
                        sheet_Ethnic.SetCellValue(startrow, 2, lstClassProfile[j].DisplayName);
                        sheet_Ethnic.SetCellValue(startrow, 3, lstClassProfile[j].EmployeeName);
                        sheet_Ethnic.SetCellValue(startrow, 4, sumPupilCuurent);
                        sheet_Ethnic.SetCellValue(startrow, 5, HSG);
                        sheet_Ethnic.SetCellValue(startrow, 6, "=ROUND(E" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrow, 7, HSKH);
                        sheet_Ethnic.SetCellValue(startrow, 8, "=ROUND(G" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrow, 9, HSTB);
                        sheet_Ethnic.SetCellValue(startrow, 10, "=ROUND(I" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrow, 11, HSY);
                        sheet_Ethnic.SetCellValue(startrow, 12, "=ROUND(K" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrow, 13, HSK);
                        sheet_Ethnic.SetCellValue(startrow, 14, "=ROUND(M" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrow, 15, "=SUM(E" + startrow.ToString() + ",G" + startrow.ToString() + ",I" + startrow.ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrow, 16, "=ROUND(O" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        startrow++;
                    }

                    // fill vào excel từng Khối
                    sheet_Ethnic.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                    sheet_Ethnic.SetCellValue(startrowClass, 1, i + 1);
                    sheet_Ethnic.SetCellValue(startrowClass, 2, lstEducationLevel[i].Resolution);
                    if (lstClassProfile != null && lstClassProfile.Count > 0)
                    {
                        sheet_Ethnic.SetCellValue(startrowClass, 4, "=SUM(D" + (startrowClass + 1).ToString() + ":D" + (startrow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 5, "=SUM(E" + (startrowClass + 1).ToString() + ":E" + (startrow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 6, "=ROUND(E" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrowClass, 7, "=SUM(G" + (startrowClass + 1).ToString() + ":G" + (startrow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 8, "=ROUND(G" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrowClass, 9, "=SUM(I" + (startrowClass + 1).ToString() + ":I" + (startrow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 10, "=ROUND(I" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrowClass, 11, "=SUM(K" + (startrowClass + 1).ToString() + ":K" + (startrow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 12, "=ROUND(K" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrowClass, 13, "=SUM(M" + (startrowClass + 1).ToString() + ":M" + (startrow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 14, "=ROUND(M" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_Ethnic.SetCellValue(startrowClass, 15, "=SUM(E" + startrowClass.ToString() + ",G" + startrowClass.ToString() + ",I" + startrowClass.ToString() + ")");
                        sheet_Ethnic.SetCellValue(startrowClass, 16, "=ROUND(O" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    }
                    else
                    {
                        sheet_Ethnic.SetCellValue(startrowClass, 4, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 5, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 6, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 7, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 8, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 9, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 10, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 11, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 12, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 13, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 14, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 15, 0);
                        sheet_Ethnic.SetCellValue(startrowClass, 16, 0);
                    }
                    startrow++;
                }

                // fill vào excel dữ liệu row tổng của trường
                sheet_Ethnic.SetCellValue(11, 1, "Toàn trường");
                sheet_Ethnic.SetCellValue(11, 4, "=SUM(D12:D" + (startrow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(11, 5, "=SUM(E12:E" + (startrow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(11, 6, "=ROUND(E" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Ethnic.SetCellValue(11, 7, "=SUM(G12:G" + (startrow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(11, 8, "=ROUND(G" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Ethnic.SetCellValue(11, 9, "=SUM(I12:I" + (startrow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(11, 10, "=ROUND(I" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Ethnic.SetCellValue(11, 11, "=SUM(K12:K" + (startrow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(11, 12, "=ROUND(K" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Ethnic.SetCellValue(11, 13, "=SUM(M12:M" + (startrow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue(11, 14, "=ROUND(M" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Ethnic.SetCellValue(11, 15, "=SUM(E11,G11,I11)");
                sheet_Ethnic.SetCellValue(11, 16, "=ROUND(O" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_Ethnic.CopyPasteSameRowHeigh(firstSheet.GetRange(startrow + 1, 1, startrow + 1, 12), startrow + 1);
                sheet_Ethnic.GetRange(startrow + 1, 12, startrow + 1, 16).Merge();
                sheet_Ethnic.SetCellValue(startrow + 1, 12, "Người lập báo cáo");
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                //sheet.Delete();
                #endregion
            }
             
            if (entity.FemaleEthnicChecked)
            {
                #region Tạo sheet Thống kê học lực học sinh nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(firstSheet);
                //Fill dữ liệu chung
                sheet_FemaleEthnic.Name = "HS_NU_DT";
                sheet_FemaleEthnic.SetCellValue("A2", Suppervising);
                sheet_FemaleEthnic.SetCellValue("A3", SchoolName);
                sheet_FemaleEthnic.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                sheet_FemaleEthnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC HỌC SINH NỮ DÂN TỘC");
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    sheet_FemaleEthnic.SetCellValue("A7", "NĂM HỌC: " + AcademicYear);
                }
                else
                {
                    sheet_FemaleEthnic.SetCellValue("A7", SemesterName + " - NĂM HỌC: " + AcademicYear);
                }

                startrow = 13;

                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowClass = startrow - 1;
                    // List Class Profile
                    int educationLevelID = lstEducationLevel[i].EducationLevelID;
                    List<ClassProfileTempBO> lstClassProfile = listAllClass.Where(o => o.EducationLevelID == educationLevelID)
                        .OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                        .ThenBy(o => o.DisplayName).ToList();
                    List<PupilRankingBO> lstCultureRank_Edu = lstCultureRank_FemaleEthnic.Where(o => o.EducationLevelID == educationLevelID).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_FemaleEthnic.Where(o => o.EducationLevelID == educationLevelID).ToList();
                    for (int j = 0; j < lstClassProfile.Count; j++)
                    {
                        int classID = lstClassProfile[j].ClassProfileID;
                        List<PupilRankingBO> listPrByClass = lstCultureRank_Edu.Where(o => o.ClassID == classID).ToList();
                        // Lấy số lượng học sinh có học lực Giỏi
                        int HSG = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT);

                        // lấy số lượng học sinh có học lực khá
                        int HSKH = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD);

                        // lấy số lượng học sinh có học lực trung bình
                        int HSTB = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL);

                        // lấy số lượng học sinh có học lực yếu
                        int HSY = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK);

                        // lấy số lượng học sinh có học lực kém
                        int HSK = listPrByClass.Count(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR);


                        //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào
                        var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                        int sumPupilCuurent = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                        // fill dữ liệu từng lớp vào excel
                        if (j == lstClassProfile.Count - 1)
                        {
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeLastClass, startrow);
                        }
                        else if (j == 0)
                        {
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeFirst, startrow);
                        }
                        else
                        {
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(range, startrow);
                        }

                        sheet_FemaleEthnic.SetCellValue(startrow, 1, (i + 1) + "." + (j + 1).ToString());
                        sheet_FemaleEthnic.SetCellValue(startrow, 2, lstClassProfile[j].DisplayName);
                        sheet_FemaleEthnic.SetCellValue(startrow, 3, lstClassProfile[j].EmployeeName);
                        sheet_FemaleEthnic.SetCellValue(startrow, 4, sumPupilCuurent);
                        sheet_FemaleEthnic.SetCellValue(startrow, 5, HSG);
                        sheet_FemaleEthnic.SetCellValue(startrow, 6, "=ROUND(E" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrow, 7, HSKH);
                        sheet_FemaleEthnic.SetCellValue(startrow, 8, "=ROUND(G" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrow, 9, HSTB);
                        sheet_FemaleEthnic.SetCellValue(startrow, 10, "=ROUND(I" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrow, 11, HSY);
                        sheet_FemaleEthnic.SetCellValue(startrow, 12, "=ROUND(K" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrow, 13, HSK);
                        sheet_FemaleEthnic.SetCellValue(startrow, 14, "=ROUND(M" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrow, 15, "=SUM(E" + startrow.ToString() + ",G" + startrow.ToString() + ",I" + startrow.ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrow, 16, "=ROUND(O" + startrow + "/IF(D" + startrow + "<=0;1;D" + startrow + ");4)*100");
                        startrow++;
                    }

                    // fill vào excel từng Khối
                    sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                    sheet_FemaleEthnic.SetCellValue(startrowClass, 1, i + 1);
                    sheet_FemaleEthnic.SetCellValue(startrowClass, 2, lstEducationLevel[i].Resolution);
                    if (lstClassProfile != null && lstClassProfile.Count > 0)
                    {
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 4, "=SUM(D" + (startrowClass + 1).ToString() + ":D" + (startrow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 5, "=SUM(E" + (startrowClass + 1).ToString() + ":E" + (startrow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 6, "=ROUND(E" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 7, "=SUM(G" + (startrowClass + 1).ToString() + ":G" + (startrow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 8, "=ROUND(G" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 9, "=SUM(I" + (startrowClass + 1).ToString() + ":I" + (startrow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 10, "=ROUND(I" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 11, "=SUM(K" + (startrowClass + 1).ToString() + ":K" + (startrow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 12, "=ROUND(K" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 13, "=SUM(M" + (startrowClass + 1).ToString() + ":M" + (startrow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 14, "=ROUND(M" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 15, "=SUM(E" + startrowClass.ToString() + ",G" + startrowClass.ToString() + ",I" + startrowClass.ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 16, "=ROUND(O" + startrowClass + "/IF(D" + startrowClass + "<=0;1;D" + startrowClass + ");4)*100");
                    }
                    else
                    {
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 4, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 5, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 6, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 7, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 8, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 9, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 10, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 11, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 12, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 13, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 14, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 15, 0);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 16, 0);
                    }
                    startrow++;
                }

                // fill vào excel dữ liệu row tổng của trường
                sheet_FemaleEthnic.SetCellValue(11, 1, "Toàn trường");
                sheet_FemaleEthnic.SetCellValue(11, 4, "=SUM(D12:D" + (startrow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue(11, 5, "=SUM(E12:E" + (startrow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue(11, 6, "=ROUND(E" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_FemaleEthnic.SetCellValue(11, 7, "=SUM(G12:G" + (startrow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue(11, 8, "=ROUND(G" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_FemaleEthnic.SetCellValue(11, 9, "=SUM(I12:I" + (startrow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue(11, 10, "=ROUND(I" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_FemaleEthnic.SetCellValue(11, 11, "=SUM(K12:K" + (startrow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue(11, 12, "=ROUND(K" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_FemaleEthnic.SetCellValue(11, 13, "=SUM(M12:M" + (startrow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue(11, 14, "=ROUND(M" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_FemaleEthnic.SetCellValue(11, 15, "=SUM(E11,G11,I11)");
                sheet_FemaleEthnic.SetCellValue(11, 16, "=ROUND(O" + 11 + "/IF(D" + 11 + "<=0;1;D" + 11 + ");4)*100");
                sheet_FemaleEthnic.CopyPasteSameRowHeigh(firstSheet.GetRange(startrow + 1, 1, startrow + 1, 12), startrow + 1);
                sheet_FemaleEthnic.GetRange(startrow + 1, 12, startrow + 1, 16).Merge();
                sheet_FemaleEthnic.SetCellValue(startrow + 1, 12, "Người lập báo cáo");
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                //sheet.Delete();
                #endregion
            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }
        #endregion
    }
}
