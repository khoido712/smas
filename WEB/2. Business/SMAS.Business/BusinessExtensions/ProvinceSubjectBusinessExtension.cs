﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  NAMTA
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    public partial class ProvinceSubjectBusiness
    {
        #region Validate
        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="entity">The entity.</param>
        ///<author>Nam ta</author>
        ///<Date>1/11/2013</Date>
        /// <exception cref="BusinessException">ProvinceSubject_Validate_ApplidedType</exception>
        public void Validate(ProvinceSubject entity)
        {
            ValidationMetadata.ValidateObject(entity);

            //AppliedType: require, in (0, 1, 2)
            //IsCommenting: require, in (0, 1, 2)

            if (entity.AppliedType != 0 && entity.AppliedType != 1 && entity.AppliedType!= 2)
            {
                throw new BusinessException("ProvinceSubject_Validate_ApplidedType");
            }
            if (entity.IsCommenting != 0 && entity.IsCommenting != 1 && entity.IsCommenting != 2)
            {
                throw new BusinessException("ProvinceSubject_Validate_IsCommenting");
            }
            if (entity.SectionPerWeekFirstSemester != null )
            {
                if (entity.SectionPerWeekFirstSemester > 10 || entity.SectionPerWeekSecondSemester < 0)
                {
                    throw new BusinessException("ProvinceSubject_Validate_Time");
                }
            }
            if (entity.SectionPerWeekSecondSemester != null)
            {
                if (entity.SectionPerWeekSecondSemester > 10 || entity.SectionPerWeekSecondSemester < 0)
                {
                    throw new BusinessException("ProvinceSubject_Validate_Time");
                }
            }
        } 
        #endregion

        #region Insert
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="list">The list.</param>
        ///<author>Nam ta</author>
        ///<Date>1/11/2013</Date>
        public void Insert(List<ProvinceSubject> list)
        {
            ProvinceSubject entity = list.FirstOrDefault();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearOfProvinceID"] = entity.AcademicYearOfProvinceID;
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["IsRegularEducation"] = entity.IsRegularEducation;
            IQueryable<ProvinceSubject> listProvinceSubject = SearchByProvince(entity.ProvinceID, dic);
            if (listProvinceSubject != null && listProvinceSubject.Count() > 0)
            {
                this.DeleteAll(listProvinceSubject.ToList());
            }

            this.Save();

            foreach (ProvinceSubject ProvinceSubject in list)
            {
                Validate(ProvinceSubject);

                this.Insert(ProvinceSubject);


            }
        } 
        #endregion

        #region Search
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns>
        /// IQueryable{ProvinceSubject}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>1/11/2013</Date>
        public IQueryable<ProvinceSubject> Search(IDictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AcademicYearOfProvinceID = Utils.GetInt(dic, "AcademicYearOfProvinceID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            bool? IsRegularEducation = Utils.GetNullableBool(dic, "IsRegularEducation");
            int AppliedType = Utils.GetInt(dic, "AppliedType", -1);
            int IsCommenting = Utils.GetInt(dic, "IsCommenting", -1);
            int Year = Utils.GetInt(dic, "Year");

            IQueryable<ProvinceSubject> query = this.All;
            query = query.Where(o => o.AcademicYearOfProvince.IsActive == true);

            if (ProvinceID != 0)
            {
                query = query.Where(o => o.ProvinceID == ProvinceID);
            }
            if (SubjectID != 0)
            {
                query = query.Where(o => o.SubjectID == SubjectID);
            }
            if (AcademicYearOfProvinceID != 0)
            {
                query = query.Where(o => o.AcademicYearOfProvinceID == AcademicYearOfProvinceID);
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (IsRegularEducation != null)
            {
                query = query.Where(o => o.IsRegularEducation == IsRegularEducation);
            }
            if (AppliedType != -1)
            {
                query = query.Where(o => o.AppliedType == AppliedType);
            }
            if (IsCommenting != -1)
            {
                query = query.Where(o => o.IsCommenting == IsCommenting);
            }
            if (Year != 0)
            {
                query = query.Where(o => o.AcademicYearOfProvince.Year == Year);
            }
            return query;
        } 
        #endregion

        #region SearchByProvince
        /// <summary>
        /// SearchByProvince
        /// </summary>
        /// <param name="ProvinceID">The province ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{ProvinceSubject}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>1/11/2013</Date>
        public IQueryable<ProvinceSubject> SearchByProvince(int ProvinceID, IDictionary<string, object> SearchInfo)
        {
            if (ProvinceID == 0)
                return null;
            else
            {
                SearchInfo["ProvinceID"] = ProvinceID;
            }
            return this.Search(SearchInfo);
        }  
        #endregion

        public IQueryable<ProvinceSubjectBO> GetListProvinceSubject(IDictionary<string,object> SearchInfo)
        {
            //EducationLevelID, AppliedLevel, AcademicYearOfProvinceID, IsRegularEducation

            int AcademicYearOfProvinceID = Utils.GetInt(SearchInfo, "AcademicYearOfProvinceID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            bool? IsRegularEducation = Utils.GetNullableBool(SearchInfo, "IsRegularEducation");

            IQueryable<SubjectCat> lstSubject = SubjectCatBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", AppliedLevel } });//.OrderBy(o=>o.OrderInSubject).ThenBy(o=>o.DisplayName);
            //var querry = from ls in lstSubject
            //             join ps in ProvinceSubjectRepository.All.Where(p => (p.AcademicYearOfProvinceID == (int)AcademicYearOfProvinceID) 
            //                 && p.EducationLevelID == (int)EducationLevelID && p.IsRegularEducation == IsRegularEducation) 
            //             on ls.SubjectCatID equals ps.SubjectID into sp1
            //             from ps2 in sp1.DefaultIfEmpty()
            //             //join epr in EducationProgramBusiness.All on ls.SubjectCatID equals epr.SubjectCatID into sp2
            //             join epr in EducationProgramBusiness.All.Where(u => (u.AppliedLevel == AppliedLevel && u.EducationLevelID == EducationLevelID && u.IsActive == true 
            //                 && ((IsRegularEducation == true && u.TrainingType != null && u.TrainingType.Resolution == "GDTX") || 
            //                 (IsRegularEducation == false && (u.TrainingType == null || (u.TrainingType != null && u.TrainingType.Resolution != "GDTX")))))) 
            //             on ls.SubjectCatID equals epr.SubjectCatID 
            //             select new ProvinceSubjectBO
            //             {
            //                 ProvinceSubjectID = ps2.ProvinceSubjectID,
            //                 DisplayName = ls.DisplayName,
            //                 Abbreviation = ls.Abbreviation,
            //                 SubjectID = ls.SubjectCatID,
            //                 ProvinceID = ps2.ProvinceID,
            //                 AcademicYearOfProvinceID = ps2.AcademicYearOfProvinceID,
            //                 AppliedLevel = epr.AppliedLevel,
            //                 EducationLevelID = epr.EducationLevelID,
            //                 IsRegularEducation = ps2.IsRegularEducation,
            //                 Coefficient = ps2.Coefficient,
            //                 AppliedType = ps2.AppliedType,
            //                 IsCommenting = ps2.IsCommenting,
            //                 SectionPerWeekFirstSemester = ps2.SectionPerWeekFirstSemester,
            //                 SectionPerWeekSecondSemester = ps2.SectionPerWeekSecondSemester,
            //                 NumberOfLession = epr.NumberOfLesson,
            //                 OrderInSubject = epr.SubjectCat.OrderInSubject,
            //                 IsCoreSubject = ls.IsCoreSubject
            //             };
            int count = lstSubject.Count();
            var querry = from ls in lstSubject
                         join ps in ProvinceSubjectRepository.All.Where(p => (p.AcademicYearOfProvinceID == (int)AcademicYearOfProvinceID) && p.EducationLevelID == (int)EducationLevelID && p.IsRegularEducation == IsRegularEducation) on ls.SubjectCatID equals ps.SubjectID into sp1
                         from ps2 in sp1.DefaultIfEmpty()
                         //join epr in EducationProgramBusiness.All on ls.SubjectCatID equals epr.SubjectCatID into sp2
                         join epr in EducationProgramBusiness.All.Where
                         (u => (u.AppliedLevel == AppliedLevel && u.EducationLevelID == EducationLevelID && u.IsActive == true && 
                             ((IsRegularEducation == true && u.TrainingType != null && u.TrainingType.Resolution == "GDTX") 
                             || (IsRegularEducation == false && (u.TrainingType == null || (u.TrainingType != null && u.TrainingType.Resolution != "GDTX")))))) 
                         on ls.SubjectCatID equals epr.SubjectCatID into sp2
                         from sp in sp2.DefaultIfEmpty()
                         select new ProvinceSubjectBO
                         {
                             ProvinceSubjectID = ps2.ProvinceSubjectID,
                             DisplayName = ls.DisplayName,
                             Abbreviation = ls.Abbreviation,
                             SubjectID = ls.SubjectCatID,
                             ProvinceID = ps2.ProvinceID,
                             AcademicYearOfProvinceID = ps2.AcademicYearOfProvinceID,
                             AppliedLevel = sp.AppliedLevel,
                             EducationLevelID = sp.EducationLevelID,
                             IsRegularEducation = ps2.IsRegularEducation,
                             Coefficient = ps2.Coefficient,
                             AppliedType = ps2.AppliedType,
                             //IsCommenting = ps2.IsCommenting,
                             IsCommenting = ps2!=null? ps2.IsCommenting:ls.IsCommenting,
                             SectionPerWeekFirstSemester = ps2.SectionPerWeekFirstSemester,
                             SectionPerWeekSecondSemester = ps2.SectionPerWeekSecondSemester,
                             NumberOfLession = sp.NumberOfLesson,
                             //OrderInSubject = sp.SubjectCat.OrderInSubject,
                             OrderInSubject = ps2.ProvinceSubjectID != null ? ls.OrderInSubject - count : ls.OrderInSubject, // thêm - Count để đưa những môn đã khai báo lên đầu
                             IsCoreSubject = ls.IsCoreSubject,
                             IsChoice = (sp.EducationProgramID != null||ps2.ProvinceSubjectID!=null) ? true:false,
                             IsEditIsCommenting = ls.IsEditIsCommentting
                         };
            return querry;
            //.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName);
                
        }

    }
}
