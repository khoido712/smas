/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ConductEvaluationDetailBusiness
    {
        private void ValidateEntity(ConductEvaluationDetail entity)
        {
            ValidationMetadata.ValidateObject(entity);

            ////Kiem tra ton tai hocj sinh
            //var pupilProfile = this.PupilProfileBusiness.Find(entity.PupilID);
            //Utils.ValidateNull(pupilProfile, "ConductEvaluationDetail_Label_PupilID");

            //Kiem tra FK truong hoc
            //this.SchoolProfileBusiness.CheckAvailable(entity.SchoolID, "ConductEvaluationDetail_Label_SchoolID");

            //Kiem tra FK lop hoc
            //this.ClassProfileBusiness.CheckAvailable(entity.ClassID, "ConductEvaluationDetail_Label_ClassID");

            //Kiem tra FK academic year
            //var academicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID);
            //Utils.ValidateNull(academicYear, "ConductEvaluationDetail_Label_AcademicYearID");

            //Check SchoolID, AcademicYearID compatible in AcademicYear
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear"
                                            , new Dictionary<string, object>()
                                            {
                                                {"AcademicYearID" , entity.AcademicYearID},
                                                {"SchoolID", entity.SchoolID}
                                            }, null);
            if (!AcademicYearCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra Class co ton tai khong
            //Utils.ValidateNull(entity.ClassID, "ConductEvaluationDetail_Label_ClassID");

            //Kiem tra tuong thich ClassProfile
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ClassProfileID" , entity.ClassID},
                                                {"AcademicYearID", entity.AcademicYearID}
                                            }, null);
            if (!ClassProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible"); 

            //Kiem tratuong thich Pupil Profile
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile"
                                            , new Dictionary<string, object>()
                                            {
                                                {"PupilProfileID" , entity.PupilID},
                                                {"CurrentClassID", entity.ClassID}
                                            }, null);
            if (!PupilProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible"); 

        }

        public void InsertOrUpdateConductEvaluationDetail(int UserID, List<ConductEvaluationDetail> lstConductEvaluationDetail)
        {
            if (lstConductEvaluationDetail == null || lstConductEvaluationDetail.Count == 0)
                throw new BusinessException("Common_Validate_Null");

            int schoolID = lstConductEvaluationDetail[0].SchoolID;
            int academicYearID = lstConductEvaluationDetail[0].AcademicYearID;

            //Check SchoolID, AcademicYearID compatible in AcademicYear
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear"
                                            , new Dictionary<string, object>()
                                            {
                                                {"AcademicYearID" , academicYearID},
                                                {"SchoolID", schoolID }
                                            }, null);
            if (!AcademicYearCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");
            
            List<int> lstClassID = lstConductEvaluationDetail.Select(u => u.ClassID).Distinct().ToList();
            lstClassID.ForEach(u => {
                //Kiem tra tuong thich ClassProfile
                bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile"
                                                , new Dictionary<string, object>()
                                            {
                                                {"ClassProfileID" , u},
                                                {"AcademicYearID", academicYearID}
                                            }, null);
                if (!ClassProfileCompatible)
                    throw new BusinessException("Common_Validate_NotCompatible");
            });

            Dictionary<int, List<PupilOfClass>> dicPOC = new Dictionary<int, List<PupilOfClass>>();
            lstClassID.ForEach(u => {
                dicPOC[u] = PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "AcademicYearID", academicYearID }, { "ClassID", u }, { "Check", "Check" } }).ToList();
            });

            foreach (var entity in lstConductEvaluationDetail)
            {
                ValidationMetadata.ValidateObject(entity);

                PupilOfClass poc = dicPOC[entity.ClassID].SingleOrDefault(u => u.PupilID == entity.PupilID);
                if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("PupilRanking_Validate_ProfileStatus");
            }

            List<int> lstPupilID = lstConductEvaluationDetail.Select(u => u.PupilID).Distinct().ToList();
            List<int> lstSemester = lstConductEvaluationDetail.Where(u=> u.Semester.HasValue).Select(u => u.Semester.Value).Distinct().ToList();
            var lstDeleteItems = this.All.Where(u => lstClassID.Contains(u.ClassID) && lstPupilID.Contains(u.PupilID)).ToList();

            foreach (int semester in lstSemester) 
            {
                var lstDelete = lstDeleteItems.Where(u => u.Semester == semester).ToList();
                this.DeleteAll(lstDelete);
            }

            foreach (var entity in lstConductEvaluationDetail)
                base.Insert(entity);
        }

        private IQueryable<ConductEvaluationDetail> Search(IDictionary<string, object> dic)
        {
            int ConductEvaluationDetailID = Utils.GetInt(dic, "ConductEvaluationDetailID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");


            IQueryable<ConductEvaluationDetail> lsConductEvaluationDetail = this.ConductEvaluationDetailRepository.All;
            if (ClassID != 0)
            {
                lsConductEvaluationDetail = from ce in lsConductEvaluationDetail
                                            join cp in ClassProfileBusiness.All on ce.ClassID equals cp.ClassProfileID
                                            where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                            && ce.ClassID == ClassID
                                            select ce;
            }
            if (SchoolID != 0)
            {
                lsConductEvaluationDetail = lsConductEvaluationDetail.Where(o => o.SchoolID == SchoolID);
            }
            if (ConductEvaluationDetailID != 0)
            {
                lsConductEvaluationDetail = lsConductEvaluationDetail.Where(o => o.ConductEvaluationDetailID == ConductEvaluationDetailID);
            }

            if (PupilID != 0)
            {
                lsConductEvaluationDetail = lsConductEvaluationDetail.Where(o => o.PupilID == PupilID);
            }

            if (AcademicYearID != 0)
            {
                lsConductEvaluationDetail = lsConductEvaluationDetail.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (Semester != 0)
            {
                lsConductEvaluationDetail = lsConductEvaluationDetail.Where(o => o.Semester == Semester);
            }
            return lsConductEvaluationDetail;
        }


        public IQueryable<ConductEvaluationDetail> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }
    }
}