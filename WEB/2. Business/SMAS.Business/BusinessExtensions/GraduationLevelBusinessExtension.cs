/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class GraduationLevelBusiness
    {
        #region Search

        /// <summary>
        /// Tìm kiếm trình độ đào tạo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="FullfilmentStatus"></param>
        /// <param name="IsActive"></param>
        /// <returns>IQueryable<GraduationLevel></returns>
        public IQueryable<GraduationLevel> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            int AppliedLevel = (int)Utils.GetInt(dic, "AppliedLevel");
            int FullfilmentStatus = (int)Utils.GetInt(dic, "FullfilmentStatus");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<GraduationLevel> lsGraduationLevel = GraduationLevelRepository.All;

            if (IsActive.HasValue)
                lsGraduationLevel = lsGraduationLevel.Where(gra => gra.IsActive == IsActive);
            if (AppliedLevel !=0)
                lsGraduationLevel = lsGraduationLevel.Where(gra => gra.AppliedLevel == AppliedLevel);
            if (FullfilmentStatus != 0)
                lsGraduationLevel = lsGraduationLevel.Where(gra => gra.FullfilmentStatus == FullfilmentStatus);
            if (Resolution.Trim().Length != 0)
                lsGraduationLevel = lsGraduationLevel.Where(gra => gra.Resolution.ToLower().Contains(Resolution.ToLower()));



            return lsGraduationLevel;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm trình độ đào tạo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="graduationLevel"> object GraduationLevel</param>
        /// <returns>object GraduationLevel</returns>
        public override GraduationLevel Insert(GraduationLevel graduationLevel)
        {

            //Tên trình độ đào tạo không được để trống 
            Utils.ValidateRequire(graduationLevel.Resolution, "GraduationLevel_Label_Resolution");
            //Độ dài Tên trình độ đào tạo không được vượt quá 50 
            Utils.ValidateMaxLength(graduationLevel.Resolution, 50, "GraduationLevel_Label_Resolution");
            //Cấp học chỉ nhận giá trị từ 1 - 5 - Kiểm tra AppliedLevel
            Utils.ValidateRange((int)graduationLevel.AppliedLevel, 1, 5, "GraduationLevel_Label_Resolution");
            //Chuẩn đào tạo chỉ nhận giá trị từ 1-3 - Kiểm tra FullfilmentStatus
            Utils.ValidateRange((int)graduationLevel.FullfilmentStatus, 1, 3, "GraduationLevel_Label_FullfilmentStatus");
            //Tên trình độ đào tạo đã tồn tại - Kiểm tra Resolution / AppliedLevel với IsActive=TRUE
            new GraduationLevelBusiness(null).CheckDuplicateCouple(graduationLevel.Resolution, graduationLevel.AppliedLevel.ToString(), GlobalConstants.LIST_SCHEMA, "GraduationLevel", "Resolution", "AppliedLevel", true, graduationLevel.GraduationLevelID, "GraduationLevel_Label_Resolution");

            //Insert
            base.Insert(graduationLevel);
            return graduationLevel;
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa trình độ đào tạo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="graduationLevel"> object GraduationLevel</param>
        /// <returns>object GraduationLevel</returns>
        public override GraduationLevel Update(GraduationLevel graduationLevel)
        {
            //Bạn chưa chọn Loại trình độ đào tạo cần sửa hoặc Loại trình độ đào tạo bạn chọn đã bị xóa khỏi hệ thống
            new GraduationLevelBusiness(null).CheckAvailable((int)graduationLevel.GraduationLevelID, "GraduationLevel_Label_GraduationLevelID", true);
            //Tên trình độ đào tạo không được để trống 
            Utils.ValidateRequire(graduationLevel.Resolution, "GraduationLevel_Label_Resolution");
            //Độ dài Tên trình độ đào tạo không được vượt quá 50 
            Utils.ValidateMaxLength(graduationLevel.Resolution, 50, "GraduationLevel_Label_Resolution");
            //Cấp học chỉ nhận giá trị từ 1 - 5 - Kiểm tra AppliedLevel
            Utils.ValidateRange((int)graduationLevel.AppliedLevel, 1, 5, "GraduationLevel_Label_Resolution");
            //Chuẩn đào tạo chỉ nhận giá trị từ 1-3 - Kiểm tra FullfilmentStatus
            Utils.ValidateRange((int)graduationLevel.FullfilmentStatus, 1, 3, "GraduationLevel_Label_FullfilmentStatus");
            //Tên trình độ đào tạo đã tồn tại - Kiểm tra Resolution / AppliedLevel với IsActive=TRUE
            new GraduationLevelBusiness(null).CheckDuplicateCouple(graduationLevel.Resolution, graduationLevel.AppliedLevel.ToString(), GlobalConstants.LIST_SCHEMA, "GraduationLevel", "Resolution", "AppliedLevel", true, graduationLevel.GraduationLevelID, "GraduationLevel_Label_Resolution");

            base.Update(graduationLevel);
            return graduationLevel;
        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa trình độ đào tạo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="GraduationLevelID"> ID GraduationLevel</param>
        /// <returns>ID GraduationLevel</returns>
        public void Delete(int GraduationLevelID)
        {
            //Bạn chưa chọn Loại trình độ đào tạo cần xóa hoặc Loại trình độ đào tạo bạn chọn đã bị xóa khỏi hệ thống
            new GraduationLevelBusiness(null).CheckAvailable((int)GraduationLevelID, "GraduationLevel_Label_GraduationLevelID", true);

            //Không thể xóa Loại trình độ đào tạo đang sử dụng
            new GraduationLevelBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "GraduationLevel", GraduationLevelID, "GraduationLevel_Label_GraduationLevelID");
            base.Delete(GraduationLevelID, true);
        }
        #endregion
    }
}
