﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class HtmlContentCodeBusiness
    {
        public List<HtmlContentCode> GetListHtmlContentCode(string templateCode)
        {
            List<HtmlContentCode> listTitle = (from a in HtmlContentCodeBusiness.All
                                               join b in TemplateBusiness.All on a.TemplateID equals b.TemplateID
                                               where b.TemplateCode == templateCode
                                               orderby a.OrderNumber
                                               select a).ToList();
            return listTitle;
        }
    }
}
