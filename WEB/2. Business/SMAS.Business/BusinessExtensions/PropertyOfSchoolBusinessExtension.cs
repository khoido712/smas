/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common; 

namespace SMAS.Business.Business
{ 
    public partial class PropertyOfSchoolBusiness
    {


        #region Search

        /// <summary>
        /// Tìm kiếm tính chất trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="PropertyOfSchoolID">ID tính chất của trường</param>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<PropertyOfSchool></returns>
        public IQueryable<PropertyOfSchool> Search(IDictionary<string, object> dic)
        { 
            int PropertyOfSchoolID = Utils.GetInt(dic,"PropertyOfSchoolID"); 
            int SchoolID = Utils.GetInt(dic,"SchoolID"); 
            //int SchoolPropertyCatID =Utils.GetShort(dic,"SchoolPropertyCatID");
            bool? IsActive = Utils.GetNullableBool(dic, "IsActive");
            IQueryable<PropertyOfSchool> lsPropertyOfSchool = PropertyOfSchoolRepository.All;
            if (IsActive.HasValue)
                lsPropertyOfSchool = lsPropertyOfSchool.Where(pro => pro.IsActive == IsActive);
            if (SchoolID != 0)
                lsPropertyOfSchool = lsPropertyOfSchool.Where(pro => pro.SchoolID == SchoolID);

            return lsPropertyOfSchool;
        }
        #endregion

        #region SearchBySchool

        /// <summary>
        /// Tìm kiếm trường theo ID
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="PropertyOfSchoolID">ID tính chất của trường</param>
        /// <param name="SchoolID">ID trường</param>
        /// <returns>IQueryable<PropertyOfSchool></returns>
        public IQueryable<PropertyOfSchool> SearchBySchool(IDictionary<string, object> dic,int SchoolID)
        {
            
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;

            }
            return Search(dic);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm tính chất trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="propertyOfSchool">Đối tượng tính chất trường</param>
        /// <returns>Đối tượng tính chất trường</returns>
        public override PropertyOfSchool Insert(PropertyOfSchool propertyOfSchool)
        {
            //Trường có SchoolID không tồn tại trong hệ thống
            SchoolProfileBusiness.CheckAvailable(propertyOfSchool.SchoolID, "PropertyOfSchool_Label_SchoolID", true);


            //Tính chất trường không tồn tại. Kiểm tra SchoolPropertyCatID
            SchoolPropertyCatBusiness.CheckAvailable(propertyOfSchool.SchoolPropertyCatID, "PropertyOfSchool_Label_SchoolPropertyCatID", true);


            //Insert
           return base.Insert(propertyOfSchool);

        }

        #endregion

        #region Update
        /// <summary>
        /// Sửa tính chất trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="propertyOfSchool">Đối tượng tính chất trường</param>
        /// <returns>Đối tượng tính chất trường</returns>
        public override PropertyOfSchool Update(PropertyOfSchool propertyOfSchool)
        {
            //Trường có SchoolID không tồn tại trong hệ thống
            SchoolProfileBusiness.CheckAvailable(propertyOfSchool.SchoolID, "PropertyOfSchool_Label_SchoolID", true);


            //Tính chất trường không tồn tại. Kiểm tra SchoolPropertyCatID
            SchoolPropertyCatBusiness.CheckAvailable(propertyOfSchool.SchoolPropertyCatID, "PropertyOfSchool_Label_SchoolPropertyCatID", true);


            //Bản ghi không tồn tại trong hệ thống
            this.CheckAvailable(propertyOfSchool.PropertyOfSchoolID, "PropertyOfSchool_Label_PropertyOfSchoolID", true);
            //Update
            return base.Update(propertyOfSchool);

        }

        #endregion

        #region Delete

        /// <summary>
        /// Xóa tính chất trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="PropertyOfSchoolID">ID tính chất trường</param>
        public void Delete(int PropertyOfSchoolID, int SchoolID)
        {
            PropertyOfSchool PropertyOfSchool = PropertyOfSchoolRepository.Find(PropertyOfSchoolID);
            if (PropertyOfSchool == null)
            {
                return;
            }
            else
            {
                // Kiem tra truong
                if (PropertyOfSchool.SchoolID != SchoolID)
                {
                    throw new BusinessException("PropertyOfSchool_Label_School_Err");
                }
                base.Delete(PropertyOfSchoolID, true);
            }
        }

        #endregion

        #region DeleteBySchoolID
        /// <summary>
        /// Xóa tính chất trường theo ID trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SchoolID">ID trường</param>
        public void DeleteBySchoolID(int SchoolID)
        {
            this.CheckAvailable(SchoolID,"SchoolProfile_Label_SchoolProfileID",true);

            List<PropertyOfSchool> lsProperty = PropertyOfSchoolRepository.All.Where(o => o.SchoolID == SchoolID && o.IsActive == true).ToList();
            foreach (PropertyOfSchool Property in lsProperty)
            {
                
                Property.IsActive = false;
                PropertyOfSchoolRepository.Update(Property);
                PropertyOfSchoolRepository.Save();
            }
        }
        #endregion

    }
}
