﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class SMSParentContractExtraBusiness
    {
        public void SaveDetail(List<SMS_PARENT_CONTRACT_EXTRA> listDetailInsert, int year)
        {
            if (listDetailInsert != null && listDetailInsert.Count > 0)
            {
                #region Data AnhVD9 20150115
                List<long> contractIds = listDetailInsert.Select(o => o.SMS_PARENT_CONTRACT_ID).ToList();
                List<SMS_PARENT_CONTRACT> lstExistContract = this.SMSParentContractBusiness.All.Where(o => contractIds.Contains(o.SMS_PARENT_CONTRACT_ID)).ToList();

                // 20151020 AnhVD9 Hotfix - Lay lai HS trong danh sach 
                List<int> pupilIds = lstExistContract.Select(o => o.PUPIL_ID).ToList();
                List<PupilProfileBO> LstPupilProfile = PupilProfileBusiness.GetPupilsByYear(pupilIds, year);
                List<ServicePackageBO> LstServicePackage = ServicePackageDetailBusiness.GetListMaxSMSSP(year);
                ServicePackageBO objSP = null;
                List<ContractDetailInClassBO> lstContractInClass = new List<ContractDetailInClassBO>();
                ContractDetailInClassBO objDetailInClass = null;

                //List<SParentAccountBO> lstSParentAccount = new List<SParentAccountBO>();

                PupilProfileBO objPupilProfile = null;
                SMS_PARENT_CONTRACT entity = null;
                #endregion

                SMS_PARENT_CONTRACT_EXTRA objInsert = null;
                for (int i = 0; i < listDetailInsert.Count; i++)
                {
                    objInsert = listDetailInsert[i];
                    entity = lstExistContract.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_ID == objInsert.SMS_PARENT_CONTRACT_ID);
                    objPupilProfile = LstPupilProfile.FirstOrDefault(o => o.PupilProfileID == entity.PUPIL_ID);

                    this.SMSParentContractExtraBusiness.Insert(objInsert);

                    #region Add data for update SMSparentContractDetailInClass
                    // Không cộng tin nhắn với Hợp đồng Nợ cước

                    objDetailInClass = new ContractDetailInClassBO();
                    objDetailInClass.SchoolID = entity.SCHOOL_ID;
                    objDetailInClass.PupilID = entity.PUPIL_ID;
                    objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                    objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                    objDetailInClass.ServicePackageID = objInsert.SERVICE_PACKAGE_ID;
                    objDetailInClass.Semester = objInsert.SUB_SCRIPTION_TIME;
                    objSP = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == objInsert.SERVICE_PACKAGE_ID);
                    if (objSP != null && objSP.IsLimit != true && objSP.MaxSMS.HasValue)
                    {
                        objDetailInClass.MaxSMS = objSP.MaxSMS.HasValue ? objInsert.SUB_SCRIPTION_TIME == 3 ? objSP.MaxSMS.Value * 2 : objSP.MaxSMS : 0;
                    }
                    lstContractInClass.Add(objDetailInClass);

                    #endregion


                }
                this.Save();

                // Update message budget in class
                SMSParentContractInClassDetailBusiness.UpdateMessageBudgetInClass(lstContractInClass, year);
            }
        }
    }
}
