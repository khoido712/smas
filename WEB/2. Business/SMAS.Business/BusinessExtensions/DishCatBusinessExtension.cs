/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class DishCatBusiness
    {
        #region Validate
        public void Validate(DishCat Entity)
        {
            ValidationMetadata.ValidateObject(Entity);
            //check validate
            if (Entity.DishCatID != 0)
            {
                if (!this.Find(Entity.DishCatID).DishName.Equals(Entity.DishName))
                    this.CheckDuplicate(Entity.DishName, GlobalConstants.NUTRITION_SCHEMA, "DishCat", "DishName", true, 0, "DishCat_Label_DishName");
            }
            else
            {
                this.CheckDuplicate(Entity.DishName, GlobalConstants.NUTRITION_SCHEMA, "DishCat", "DishName", true, 0, "DishCat_Label_DishName");
            }
            //NumberOfServing>0
            if (Entity.NumberOfServing <= 0)
            {
                throw new Exception("DishCat_Label_NumberOfServing");
            }



        }
        #endregion

        #region Insert
        public void Insert(DishCat Entity)
        {
            //Validate
            //this.Validate(Entity);
            base.Insert(Entity);

        }
        #endregion

        #region Update
        public void Update(DishCat Entity)
        {
            //Validate
            this.Validate(Entity);
            bool compatible = new DishCatRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DishCat",
                 new Dictionary<string, object>()
                {
                    {"IsActive",true},
                    {"DishCatID",Entity.DishCatID}
                }, null);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            bool checkDishCatNamExist = new EatingGroupRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DishCat",
                 new Dictionary<string, object>()
                {
                    {"DishName",Entity.DishName}
                },
                new Dictionary<string, object>()
                {
                    {"DishCatID",Entity.DishCatID},
                    {"IsActive",false}
                });
            if (checkDishCatNamExist)
            {
                throw new BusinessException("Common_Validate_Duplicate", new List<object> { "DishCat_Label_DishName" });
            }
            base.Update(Entity);

        }
        #endregion

        #region Delete
        public void Delete(int DishCatID, int SchoolID)
        {

            this.CheckConstraints(GlobalConstants.SCHOOL_SCHEMA, "DishCat", DishCatID, "DishCat_Label_DishCatID");

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["DishCatID"] = DishCatID;

            bool compatible = new DishCatRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DishCat", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            DishCat dishCat = this.Find(DishCatID);
            if (dishCat != null)
            {
                dishCat.IsActive = false;
                dishCat.ModifiedDate = DateTime.Now;
                base.Update(dishCat);
            }


        }
        #endregion


        #region
        /// <summary>
        /// Namta
        /// </summary>
        /// Search DishCat
        /// 
        /// <param name="DishCat"></param>
        private IQueryable<DishCat> Search(IDictionary<string, object> dic)
        {
            string DishName = Utils.GetString(dic, "DishName");
            int NumberOfServing = Utils.GetInt(dic, "NumberOfServing");
            string Prepare = Utils.GetString(dic, "Prepare");
            string Note = Utils.GetString(dic, "Note");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int TypeOfDishID = Utils.GetInt(dic, "TypeOfDishID");
            Boolean? IsActive = Utils.GetBool(dic, "IsActive");
            if (IsActive == null)
            {
                IsActive = true;
            }

            IQueryable<DishCat> listMealName = this.All;

            if (DishName != "")
            {
                listMealName = listMealName.Where(o => o.DishName.Contains(DishName.ToLower()));
            }

            if (Prepare != "")
            {
                listMealName = listMealName.Where(o => o.Prepare.Contains(Prepare.ToLower()));
            }

            if (Note != "")
            {
                listMealName = listMealName.Where(o => o.Note.Contains(Note.ToLower()));
            }

            if (SchoolID != 0)
            {
                listMealName = listMealName.Where(o => o.SchoolID == SchoolID);
            }

            if (TypeOfDishID != 0)
            {
                listMealName = listMealName.Where(o => o.TypeOfDishID == TypeOfDishID);
            }

            if (IsActive != null)
            {
                listMealName = listMealName.Where(o => o.IsActive == IsActive);
            }


            return listMealName;

        }
        #endregion

        #region SearchBySchool
        public IQueryable<DishCat> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                dic["SchoolID"] = SchoolID;
                return this.Search(dic);
            }
        }
        #endregion
    }
}