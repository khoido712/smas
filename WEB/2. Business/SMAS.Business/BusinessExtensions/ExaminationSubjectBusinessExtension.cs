/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    /// <summary>
    /// <Author>dungnt</Author>
    /// <DateTime>06/11/2012</DateTime>
    /// </summary>
    public partial class ExaminationSubjectBusiness
    {
        #region UpdateAll -TODO:Sửa lỗi phần update

        /// <summary>
        /// Cập nhật thông tin môn thi và lịch thi tương ứng thuộc mỗi kì thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ExaminationID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ListSubjectID"></param>
        /// <param name="ListStartTime"></param>
        /// <param name="ListDurationInMinute"></param>
        /// <param name="ListHasDetachableHead"></param>
        public void UpdateAll(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, List<int> ListSubjectID
         , List<DateTime> ListStartTime, List<int> ListDurationInMinute, List<bool> ListHasDetachableHead)
        {
            //SchoolID: PK(SchoolProfile)
            {
                bool Exist = SchoolProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                        new Dictionary<string, object>()
                {
                    {"SchoolProfileID",SchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //ExaminationID: PK(Examination)
            {
                bool Exist = ExaminationRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable", new List<object>() { "Candidate_Label_Examination" });
                }
            }

            //ExaminationID, SchoolID: not compatible (Examination)
            {
                bool Compatible = ExaminationRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"SchoolID",SchoolID},
                     {"IsActive",true}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible", new List<object>() { "Candidate_Label_Examination", "ClassProfile_Control_School" });
                }
            }

            //AppliedLevel, ExaminationID: not compatible (Examination)
            {
                bool Compatible = ExaminationRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"AppliedLevel",AppliedLevel}
                    , {"IsActive",true}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible", new List<object>() { "Candidate_Label_Examination", "Employee_Label_EducationGrade" });
                }
            }

            //Nếu EducationLevelID != 0:
            //- EducationLevelID, AppliedLevel(Grade): not compatible (EducationLevel)
            if (EducationLevelID != 0)
            {
                bool Compatible = EducationLevelRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel",
                        new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"Grade",AppliedLevel}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //ListSubjectID, ListStartTime, ListDurationInMinute, ListHasDetachableHead có độ dài không hợp lệ
            int iSubject = ListSubjectID.Count();
            int iStartTime = ListStartTime.Count();
            int iDurationInMinute = ListDurationInMinute.Count();
            int iHasDetachableHead = ListHasDetachableHead.Count();
            if (iSubject != iStartTime || iSubject != iHasDetachableHead || iSubject != iDurationInMinute
                || iStartTime != iDurationInMinute || iStartTime != iHasDetachableHead
                || iDurationInMinute != iHasDetachableHead)
            {
                throw new BusinessException("ExaminationSubject_Label_DataListError");
            }

            //SubjectID không nằm trong danh sách: ClassSubjectBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
            //+ Dictionary[“AcademicYearID”] = AcademicYearID
            //+ Dictionary[“AppliedLevel”] = AppliedLevel
            //+ Dictionary[“EducationLevel”] = cboEducationLevel.Value
            //=> Dữ liệu không hợp lệ
            Examination exam = ExaminationRepository.Find(ExaminationID);

            IQueryable<ClassSubject> lsCS = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"AcademicYearID",exam.AcademicYearID},
                {"AppliedLevel",AppliedLevel},
                {"EducationLevel",EducationLevelID}
            });
            if (lsCS.Count() > 0)
            {
                List<int> lsSubject = lsCS.Select(o => o.SubjectID).ToList();
                if (!ListContaintList(lsSubject, ListSubjectID))
                {
                    throw new BusinessException("ExaminationSubject_Label_DataListError");
                }
            }

            //Ngày thi phải nằm trong kỳ thi:  ex.FromDate <= StartTime <= ex.ToDate
            foreach (var item in ListStartTime)
            {
                if (exam.FromDate.HasValue && exam.ToDate.HasValue)
                {
                    var FromDate = exam.FromDate.Value;
                    var ToDate = exam.ToDate.Value;
                    exam.FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                    exam.ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                    Utils.ValidateDateInsideDateRange(item, exam.FromDate.Value, exam.ToDate.Value, "ExaminationSubject_Label_StartTime");
                }
            }
            //Ngày giờ thi không được trùng nhau: kiểm tra các StartTime có trùng nhau không
            //và có trùng trong bảng ExaminationSubject:
            //Select 1 from ExaminationSubject es where EducationLevelID <> 0 or (  es.SchoolID = es.SchoolID and es. StartTime = StartTime
            //and es.ExaminationID = ExaminationID and es.EducationLevelID != EducationLevelID and es.SubjectID != SubjectID)

            //kiểm tra xem bên trong list ListStartTime, ListDurationInMinute có trùng nhau không
            List<DateRange> lsAllStartDateForAllSubject = new List<DateRange>();
            for (int i = 0; i < ListStartTime.Count(); i++)
            {
                DateTime start = ListStartTime[i];
                DateTime end = start.Add(new TimeSpan(0, ListDurationInMinute[i], 0));
                DateRange dr = new DateRange(start, end);
                lsAllStartDateForAllSubject.Add(dr);
            }
            bool hasOverLap = DateRange.HasOverlap(false, lsAllStartDateForAllSubject);
            if (hasOverLap)
            {
                List<object> Params = new List<object>();
                string result = "";
                foreach (OverlapPairs overlap in DateRange.OverlapingRanges(false, lsAllStartDateForAllSubject))
                {
                    result += DateRange.OverlapPairDisplay(overlap) + "\n";
                }
                Params.Add(result);
                throw new BusinessException("Examination_Label_OverlapSubjectExamTime", Params);
            }
            //

            //DurationInMinute: range(1-240)
            foreach (var item in ListDurationInMinute)
            {
                Utils.ValidateRange(item, 1, 240, "ExaminationSubject_Label_DurationInMinute");
            }

            //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận giá trị < EXAMINATION_STAGE_MARK_COMPLETED (5)
            if (exam.CurrentStage > SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                List<object> Params = new List<object>();
                Params.Add("Examination_Label_CurrentStage");
                Params.Add(SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED.ToString());
                throw new BusinessException("Common_Validate_CompareNumberDecimalNotEqual", Params);
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại Examination_Label_NotCurrentAcademicYear
            if (!AcademicYearBusiness.IsCurrentYear(exam.AcademicYearID))
            {
                throw new BusinessException("Examination_Label_NotCurrentAcademicYear", new List<object>() { });
            }

            // 

            //list thong tin
            IDictionary<int, IDictionary<string, object>> examinationDic = new Dictionary<int, IDictionary<string, object>>();
            for (int i = 0; i < ListSubjectID.Count(); i++)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("StartTime", ListStartTime[i]);
                dic.Add("DurationInMinute", ListDurationInMinute[i]);
                dic.Add("HasDetachableHead", ListHasDetachableHead[i]);
                examinationDic.Add(ListSubjectID[i], dic);
            }

            //Lấy danh sách môn thi cũ ListExaminationSubject
            List<int> ListExaminationSubject = SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"AppliedLevel",AppliedLevel},
                {"ExaminationID",ExaminationID},
                {"EducationLevelID",EducationLevelID}
            }).Select(o => o.ExaminationSubjectID).ToList();

            List<int> ListSubjectInExaminationSubject = SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"AppliedLevel",AppliedLevel},
                {"ExaminationID",ExaminationID},
                {"EducationLevelID",EducationLevelID}
            }).Select(o => o.SubjectID).ToList();




            //xoa
            if (ListExaminationSubject.Count > 0)
            {

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationID"] = exam.ExaminationID;
                dic["AppliedLevel"] = AppliedLevel;
                dic["EducationLevelID"] = EducationLevelID;
                List<ExaminationSubject> lstExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, dic)
                                                                 .Where(o => ListExaminationSubject.Contains(o.ExaminationSubjectID)).ToList();
                if (lstExaminationSubject.Count > 0)
                {
                    List<int> lstExaminationSubjectID = lstExaminationSubject.Select(o => o.ExaminationSubjectID).ToList();


                    //Danh sach thi sinh thi mon thi do
                    //if (exam.CandidateFromMultipleLevel == 1)
                    //{
                    //    dic["EducationLevelID"] = 0;
                    //}
                    IQueryable<Candidate> iqCandidate = CandidateBusiness.SearchBySchool(SchoolID, dic);
                    //Neu danh sach thi sinh cho tung mon khac nhau thi xoa danh sach theo mon
                    if (exam.UsingSeparateList == 1)
                    {
                        iqCandidate = iqCandidate.Where(o => lstExaminationSubjectID.Contains(o.SubjectID.Value));
                    }

                    //Xoa danh sach thi sinh, ket qua phan phong thi
                    List<Candidate> lstCandidateToDelete = iqCandidate.ToList();
                    if (lstCandidateToDelete.Count > 0)
                    {
                        //Xoá thí sinh vi phạm
                        List<int> lstCandidateID = lstCandidateToDelete.Select(o => o.CandidateID).Distinct().ToList();
                        List<ViolatedCandidate> lstViolateCandidate = ViolatedCandidateBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()).Where(o => lstCandidateID.Contains(o.CandidateID)).ToList();
                        if (lstViolateCandidate.Count > 0)
                        {
                            ViolatedCandidateBusiness.DeleteAll(lstViolateCandidate);
                        }

                        //Xoá thí sinh vắng thi
                        List<CandidateAbsence> lstCandidateAbsence = CandidateAbsenceBusiness.All.Where(o => lstCandidateID.Contains(o.CandidateID)).ToList();
                        if (lstCandidateAbsence.Count > 0)
                        {
                            CandidateAbsenceBusiness.DeleteAll(lstCandidateAbsence);
                        }
                        CandidateBusiness.DeleteAll(lstCandidateToDelete);
                    }

                    //Xoa ket qua phan cong giam thi
                    IQueryable<ExaminationRoom> iqExamRoom = ExaminationRoomBusiness.SearchBySchool(SchoolID, dic);
                    if (exam.UsingSeparateList == 1)
                    {
                        iqExamRoom = iqExamRoom.Where(o => lstExaminationSubjectID.Contains(o.ExaminationSubjectID));
                    }
                    List<ExaminationRoom> lstExamRoom = iqExamRoom.ToList();
                    if (lstExamRoom.Count > 0)
                    {
                        List<int> lstRoomID = lstExamRoom.Select(o => o.ExaminationRoomID).ToList();

                        List<InvigilatorAssignment> lstInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                                                .Where(o => lstRoomID.Contains(o.RoomID)).ToList();
                        if (lstInvigilatorAssignment.Count > 0)
                        {
                            InvigilatorAssignmentBusiness.DeleteAll(lstInvigilatorAssignment);
                        }
                        //Xoá giám thị vi phạm
                        List<ViolatedInvigilator> lstViolateInvigilator = ViolatedInvigilatorBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()).Where(o => lstRoomID.Contains(o.RoomID.Value)).ToList();
                        if (lstViolateInvigilator.Count > 0)
                        {
                            ViolatedInvigilatorBusiness.DeleteAll(lstViolateInvigilator);
                        }

                        ExaminationRoomBusiness.DeleteAll(lstExamRoom);
                    }
                    //Xoa ket qua danh phach
                    IQueryable<DetachableHeadBag> iqDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(SchoolID, dic);
                    if (exam.UsingSeparateList == 1)
                    {
                        iqDetachableHeadBag = iqDetachableHeadBag.Where(o => lstExaminationSubjectID.Contains(o.ExaminationSubjectID));
                    }
                    List<DetachableHeadBag> lstDetachableHeadBag = iqDetachableHeadBag.ToList();
                    if (lstDetachableHeadBag.Count > 0)
                    {
                        List<int> lstDetachableHeadBagID = lstDetachableHeadBag.Select(o => o.DetachableHeadBagID).ToList();
                        List<DetachableHeadMapping> lstDetachableHeadMapping = DetachableHeadMappingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                                               .Where(o => lstDetachableHeadBagID.Contains(o.DetachableHeadBagID)).ToList();

                        if (lstDetachableHeadMapping.Count > 0)
                        {
                            DetachableHeadMappingBusiness.DeleteAll(lstDetachableHeadMapping);
                        }
                        DetachableHeadBagBusiness.DeleteAll(lstDetachableHeadBag);
                    }

                    //Xoa danh sach mon thi
                    this.DeleteAll(lstExaminationSubject);
                }
            }




            IDictionary<string, object> search = new Dictionary<string, object>()
            {
            {"EducationLevelID",EducationLevelID},
            {"AcademicYearID",exam.AcademicYearID}
            };

            List<int> ListSC = ClassSubjectBusiness.SearchBySchool(SchoolID, search).Select(o => o.SubjectID).ToList();

            for (int i = 0; i < ListSubjectID.Count(); i++)
            {
                int SubjectID = ListSubjectID[i];
                if (ListSC.Contains(SubjectID))
                {
                    ExaminationSubject es = new ExaminationSubject();
                    es.ExaminationID = ExaminationID;
                    es.SubjectID = SubjectID;
                    es.EducationLevelID = EducationLevelID;
                    es.StartTime = ListStartTime[i];
                    es.DurationInMinute = (int?)ListDurationInMinute[i];
                    es.IsLocked = false;
                    es.HasDetachableHead = ListHasDetachableHead[i];
                    base.Insert(es);
                }
            }

        }

        #endregion UpdateAll

        //Confirm anh Tien
        //Khi huy ki thi can:
        //-	Xóa danh sách thí sinh đã tạo
        //-	Xóa kết quả phân phòng thi 
        //-	Xóa kết quả phân công giám thị đã tạo
        //-	Xóa kết quả đánh phách

        public void CancelExaminationSubject(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, List<int> ListSubjectID)
        {

            //Lay danh sach mon thi
            Examination exam = ExaminationBusiness.Find(ExaminationID);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationID"] = exam.ExaminationID;
            dic["AppliedLevel"] = AppliedLevel;
            dic["EducationLevelID"] = EducationLevelID;
            List<ExaminationSubject> lstExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, dic)
                                                             .Where(o => ListSubjectID.Contains(o.SubjectID)).ToList();
            if (lstExaminationSubject.Count > 0)
            {
                List<int> lstExaminationSubjectID = lstExaminationSubject.Select(o => o.ExaminationSubjectID).ToList();

                //Danh sach thi sinh thi mon thi do
                //if (exam.CandidateFromMultipleLevel == 1)
                //{
                //    dic["EducationLevelID"] = 0;
                //}
                IQueryable<Candidate> iqCandidate = CandidateBusiness.SearchBySchool(SchoolID, dic);
                //Neu danh sach thi sinh cho tung mon khac nhau thi xoa danh sach theo mon
                if (exam.UsingSeparateList == 1)
                {
                    iqCandidate = iqCandidate.Where(o => lstExaminationSubjectID.Contains(o.SubjectID.Value));
                }

                //Xoa danh sach thi sinh, ket qua phan phong thi
                List<Candidate> lstCandidateToDelete = iqCandidate.ToList();
                if (lstCandidateToDelete.Count > 0)
                {
                    //Xoá thí sinh vi phạm
                    List<int> lstCandidateID = lstCandidateToDelete.Select(o => o.CandidateID).Distinct().ToList();
                    List<ViolatedCandidate> lstViolateCandidate = ViolatedCandidateBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()).Where(o => lstCandidateID.Contains(o.CandidateID)).ToList();
                    if (lstViolateCandidate.Count > 0)
                    {
                        ViolatedCandidateBusiness.DeleteAll(lstViolateCandidate);
                    }

                    //Xoá thí sinh vắng thi
                    List<CandidateAbsence> lstCandidateAbsence = CandidateAbsenceBusiness.All.Where(o => lstCandidateID.Contains(o.CandidateID)).ToList();
                    if (lstCandidateAbsence.Count > 0)
                    {
                        CandidateAbsenceBusiness.DeleteAll(lstCandidateAbsence);
                    }
                    CandidateBusiness.DeleteAll(lstCandidateToDelete);
                }

                //Xoa ket qua phan cong giam thi
                IQueryable<ExaminationRoom> iqExamRoom = ExaminationRoomBusiness.SearchBySchool(SchoolID, dic);
                if (exam.UsingSeparateList == 1)
                {
                    iqExamRoom = iqExamRoom.Where(o => lstExaminationSubjectID.Contains(o.ExaminationSubjectID));
                }
                List<ExaminationRoom> lstExamRoom = iqExamRoom.ToList();
                if (lstExamRoom.Count > 0)
                {
                    List<int> lstRoomID = lstExamRoom.Select(o => o.ExaminationRoomID).ToList();

                    List<InvigilatorAssignment> lstInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                                            .Where(o => lstRoomID.Contains(o.RoomID)).ToList();
                    if (lstInvigilatorAssignment.Count > 0)
                    {
                        InvigilatorAssignmentBusiness.DeleteAll(lstInvigilatorAssignment);
                    }

                    //Xoá giám thị vi phạm
                    List<ViolatedInvigilator> lstViolateInvigilator = ViolatedInvigilatorBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()).Where(o => lstRoomID.Contains(o.RoomID.Value)).ToList();
                    if (lstViolateInvigilator.Count > 0)
                    {
                        ViolatedInvigilatorBusiness.DeleteAll(lstViolateInvigilator);
                    }
                    ExaminationRoomBusiness.DeleteAll(lstExamRoom);
                }
                //Xoa ket qua danh phach
                IQueryable<DetachableHeadBag> iqDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(SchoolID, dic);
                if (exam.UsingSeparateList == 1)
                {
                    iqDetachableHeadBag = iqDetachableHeadBag.Where(o => lstExaminationSubjectID.Contains(o.ExaminationSubjectID));
                }
                List<DetachableHeadBag> lstDetachableHeadBag = iqDetachableHeadBag.ToList();
                if (lstDetachableHeadBag.Count > 0)
                {
                    List<int> lstDetachableHeadBagID = lstDetachableHeadBag.Select(o => o.DetachableHeadBagID).ToList();
                    List<DetachableHeadMapping> lstDetachableHeadMapping = DetachableHeadMappingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>())
                                                                           .Where(o => lstDetachableHeadBagID.Contains(o.DetachableHeadBagID)).ToList();

                    if (lstDetachableHeadMapping.Count > 0)
                    {
                        DetachableHeadMappingBusiness.DeleteAll(lstDetachableHeadMapping);
                    }
                    DetachableHeadBagBusiness.DeleteAll(lstDetachableHeadBag);
                }

                //Xoa danh sach mon thi
                this.DeleteAll(lstExaminationSubject);
            }
        }



        #region Search

        /// <summary>
        /// Tìm kiếm thông tin môn thi và lịch thi tương ứng thuộc mỗi kì thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<ExaminationSubject> Search(IDictionary<string, object> SearchInfo)
        {
            int ExaminationSubjectID = Utils.GetInt(SearchInfo, "ExaminationSubjectID");
            int ExaminationID = Utils.GetInt(SearchInfo, "ExaminationID");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");

            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int DurationInMinute = Utils.GetInt(SearchInfo, "DurationInMinute", -1);

            string OrderNumberPrefix = Utils.GetString(SearchInfo, "OrderNumberPrefix");
            string Description = Utils.GetString(SearchInfo, "Description");
            DateTime? StartTime = Utils.GetDateTime(SearchInfo, "StartTime");
            bool? IsLocked = Utils.GetNullableBool(SearchInfo, "IsLocked");
            bool? HasDetachableHead = Utils.GetNullableBool(SearchInfo, "HasDetachableHead");
            IQueryable<ExaminationSubject> lsES = ExaminationSubjectRepository.All;

            if (ExaminationSubjectID != 0)
            {
                lsES = lsES.Where(o => o.ExaminationSubjectID == ExaminationSubjectID);
            }
            if (ExaminationID != 0)
            {
                lsES = lsES.Where(o => o.ExaminationID == ExaminationID);
            }
            if (SubjectID != 0)
            {
                lsES = lsES.Where(o => o.SubjectID == SubjectID);
            }
            if (AcademicYearID != 0)
            {
                lsES = lsES.Where(o => o.Examination.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsES = lsES.Where(o => o.Examination.SchoolID == SchoolID);
            }
            if (EducationLevelID != 0)
            {
                lsES = lsES.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (AppliedLevel != 0)
            {
                lsES = lsES.Where(o => o.Examination.AppliedLevel == AppliedLevel);
            }
            if (DurationInMinute != -1)
            {
                lsES = lsES.Where(o => o.DurationInMinute == DurationInMinute);
            }

            if (StartTime.HasValue)
            {
                lsES = lsES.Where(o => o.StartTime.Value.CompareTo(StartTime.Value) == 0);
            }

            if (IsLocked.HasValue)
            {
                lsES = lsES.Where(o => o.IsLocked == IsLocked);
            }

            if (HasDetachableHead.HasValue)
            {
                lsES = lsES.Where(o => o.HasDetachableHead == HasDetachableHead);
            }

            if (OrderNumberPrefix.Trim().Length > 0)
            {
                lsES = lsES.Where(o => o.OrderNumberPrefix.Trim().ToUpper().Contains(OrderNumberPrefix.ToUpper()));
            }

            if (Description.Trim().Length > 0)
            {
                lsES = lsES.Where(o => o.Examination.Description.Trim().ToUpper().Contains(Description.ToUpper()));
            }

            return lsES;
        }

        /// <summary>
        /// Tìm kiếm thông tin môn thi và lịch thi tương ứng thuộc mỗi kì thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ExaminationSubject> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            return Search(SearchInfo);
        }

        #endregion Search

        #region GetSubjectMarkType

        /// <summary>
        /// Lấy thông tin loại môn thi (tính điểm/nhận xét)
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="ExaminationSubjectID"></param>
        /// <returns></returns>
        public int GetSubjectMarkType(int ExaminationSubjectID, int? AppliedLevel)
        {
            //Lấy bản ghi ExaminationSubject theo ExaminationSubjectID tương ứng.
            ExaminationSubject es = ExaminationSubjectRepository.Find(ExaminationSubjectID);
            if (es == null)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }

            //Lấy bản ghi đầu tiên của SchoolSubjectBusiness.Search(Dictionary) với
            //Dictionnary[“AcademicYearID”] = ExaminationSubject.Examination. AcademicYearID
            //Dictionnary[“SubjectID”] = ExaminationSubject.SubjectID
            //Sau khi migrate dữ liệu sẽ tồn tại môn học thuộc lớp nhưng không thuộc trường => validate theo lớp.
            SchoolSubject cs = SchoolSubjectBusiness.Search(new Dictionary<string, object>()
            {
                {"AcademicYearID",es.Examination.AcademicYearID},
                {"SubjectID",es.SubjectID},
                {"AppliedLevel",AppliedLevel}
            }).FirstOrDefault();
            if (cs == null)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
            return cs.IsCommenting.Value;
        }

        /// <summary>
        /// Quanglm
        /// Lay danh sach kieu mon tuong ung de tang hieu nang
        /// </summary>
        /// <param name="ExaminationSubjectID"></param>
        /// <param name="AppliedLevel"></param>
        /// <returns></returns>
        public Dictionary<int, int?> GetSubjectMarkType(int AcademicYearID, List<int> ListSubjectID, int? AppliedLevel)
        {
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.Search(new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"AppliedLevel",AppliedLevel}
            }).Where(o => ListSubjectID.Contains(o.SubjectID)).ToList();
            Dictionary<int, int?> dicSubjectType = new Dictionary<int, int?>();
            if (listSchoolSubject.Count > 0)
            {
                foreach (SchoolSubject ss in listSchoolSubject)
                {
                    dicSubjectType[ss.SubjectID] = ss.IsCommenting;
                }
            }
            return dicSubjectType;
        }
        #endregion GetSubjectMarkType

        #region List containt List

        /// <summary>
        /// hàm kiểm tra list có tồn tại trong list khác không
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="input"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public bool ListContaintList(List<int> input, List<int> child)
        {
            foreach (var item in child)
            {
                if (!input.Contains(item))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion List containt List
    }
}
