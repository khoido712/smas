﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class RatedCommentPupilHistoryBusiness
    {
        private string STR_SPACE = ";";
        private string SPACE = " ";
        private string STR_DOT = ": ";
        public IQueryable<RatedCommentPupilHistory> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int EvaluationID = Utils.GetInt(dic, "TypeID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            IQueryable<RatedCommentPupilHistory> iqQuery = RatedCommentPupilHistoryBusiness.All;
            if (SchoolID != 0)
            {
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                iqQuery = iqQuery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartionID);
            }
            if (AcademicYearID != 0)
            {
                iqQuery = iqQuery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iqQuery = iqQuery.Where(p => p.ClassID == ClassID);
            }
            if (SubjectID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SubjectID == SubjectID);
            }
            if (SemesterID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SemesterID == SemesterID);
            }
            if (PupilID != 0)
            {
                iqQuery = iqQuery.Where(p => p.PupilID == PupilID);
            }
            if (EvaluationID != 0)
            {
                iqQuery = iqQuery.Where(p => p.EvaluationID == EvaluationID);

                //chiendd1: 29/12/2016. Sua loi khong lay du lieu cu
                if (EvaluationID == GlobalConstants.TAB_CAPACTIES)
                {
                    iqQuery = iqQuery.Where(p => (p.SubjectID > 0 && p.SubjectID < 4));
                }
                else if (EvaluationID == GlobalConstants.TAB_QUALITIES)
                {
                    iqQuery = iqQuery.Where(p => (p.SubjectID > 3 && p.SubjectID < 8));
                }
            }
            if (lstClassID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstSubjectID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            if (lstPupilID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            return iqQuery;
        }
        public void InsertOrUpdate(List<RatedCommentPupilBO> lstRatedCommentPupilBO, IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int ClassID = Utils.GetInt(dic, "ClassID");
                int SubjectID = Utils.GetInt(dic, "SubjectID");
                int SemesterID = Utils.GetInt(dic, "SemesterID");
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                int TypeID = Utils.GetInt(dic, "TypeID");
                bool isImport = Utils.GetBool(dic, "IsImport");
                List<int> lstPupilID = lstRatedCommentPupilBO.Select(p => p.PupilID).Distinct().ToList();
                List<int> lstSubjectID = lstRatedCommentPupilBO.Select(p => p.SubjectID).Distinct().ToList();
                List<int> lstClassID = lstRatedCommentPupilBO.Select(p => p.ClassID).Distinct().ToList();
                List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<RatedCommentPupilHistory> lstRatedCommentPupilDB = new List<RatedCommentPupilHistory>();
                IQueryable<RatedCommentPupilHistory> iqRatedCommentDB = (from r in RatedCommentPupilHistoryBusiness.All
                                                                  where r.SchoolID == SchoolID
                                                                  && r.AcademicYearID == AcademicYearID
                                                                  && r.LastDigitSchoolID == PartionID
                                                                  && r.SemesterID == SemesterID
                                                                  select r);
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP);
                }
                else
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY);
                }
                if (lstPupilID.Count > 0)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => lstPupilID.Contains(p.PupilID));
                }
                if (lstClassID.Count > 0)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => lstClassID.Contains(p.ClassID));
                }
                if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP && lstSubjectID.Count > 0)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => lstSubjectID.Contains(p.SubjectID));
                }
                lstRatedCommentPupilDB = iqRatedCommentDB.ToList();
                List<RatedCommentPupilHistory> lstInsert = new List<RatedCommentPupilHistory>();
                RatedCommentPupilBO objRatedCommentPupilBO = null;
                RatedCommentPupilHistory objDB = null;
                RatedCommentPupilHistory objInsert = null;
                // lay danh sach khoa danh gia
                IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"lstClassID",lstClassID},
                    {"EvaluationID",TypeID}
                };
                string strTitleLog = string.Empty;
                string strLockMarkTitle = string.Empty;
                List<LockRatedCommentPupil> lstLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark).ToList();
                List<LockRatedCommentPupil> lstLockRatedtmp = new List<LockRatedCommentPupil>();
                LockRatedCommentPupil objLockTitle = null;
                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                bool isInsertLog = false;
                string comment = string.Empty;
                string oldComment = string.Empty;
                #region Mon hoc va HDGD
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)//Mon hoc va HDGD
                {
                    for (int i = 0; i < lstRatedCommentPupilBO.Count; i++)
                    {
                        objRatedCommentPupilBO = lstRatedCommentPupilBO[i];
                        objLockTitle = lstLockRated.Where(p => p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                        strLockMarkTitle = objLockTitle != null ? objLockTitle.LockTitle : "";
                        objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                        isInsertLog = false;
                        //doi tuong ghi log
                        objectIDStr.Append(objRatedCommentPupilBO.PupilID);
                        if (isImport)
                        {
                            strTitleLog = "Import nhận xét đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                        }
                        else
                        {
                            strTitleLog = "Cập nhật đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                        }
                        descriptionStr.Append(strTitleLog + "học sinh");
                        paramsStr.Append("PupilID: " + objRatedCommentPupilBO.PupilID + "ClassID: " + objRatedCommentPupilBO.ClassID + "SubjectID: " + objRatedCommentPupilBO.SubjectID + "EvaluationID: " + objRatedCommentPupilBO.EvaluationID);
                        userFuntionsStr.Append("Sổ đánh giá HS");

                        userDescriptionsStr.Append(strTitleLog + (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP ? ("Môn học & HĐGD") : "Năng lực – Phẩm chất"));
                        userDescriptionsStr.Append(" HS " + objRatedCommentPupilBO.FullName).Append(", mã " + objRatedCommentPupilBO.PupilCode);
                        userDescriptionsStr.Append(", Lớp " + objRatedCommentPupilBO.ClassName + (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP ? "/" + objRatedCommentPupilBO.SubjectName : "") + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                        oldObjectStr.Append("Giá trị cũ {");
                        newObjectStr.Append("Giá trị mới {");
                        if (objDB == null)
                        {
                            objInsert = new RatedCommentPupilHistory();
                            objInsert.SchoolID = objRatedCommentPupilBO.SchoolID;
                            objInsert.AcademicYearID = objRatedCommentPupilBO.AcademicYearID;
                            objInsert.ClassID = objRatedCommentPupilBO.ClassID;
                            objInsert.PupilID = objRatedCommentPupilBO.PupilID;
                            objInsert.SemesterID = objRatedCommentPupilBO.SemesterID;
                            objInsert.EvaluationID = objRatedCommentPupilBO.EvaluationID;
                            objInsert.RetestMark = objRatedCommentPupilBO.RetestMark;
                            objInsert.EvaluationRetraning = objRatedCommentPupilBO.EvaluationRetraning;
                            objInsert.FullNameComment = objRatedCommentPupilBO.FullNameComment;
                            objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                            objInsert.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                            objInsert.CreateTime = DateTime.Now;
                            objInsert.UpdateTime = DateTime.Now;
                            //add log
                            #region Mon hoc & HDGD
                            objInsert.SubjectID = objRatedCommentPupilBO.SubjectID;
                            if (objRatedCommentPupilBO.isCommenting == 0)
                            {
                                if (objRatedCommentPupilBO.PeriodicMiddleMark > 0 && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTGK: " + objRatedCommentPupilBO.PeriodicMiddleMark).Append(STR_SPACE);
                                    objInsert.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: " + objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.PeriodicEndingMark > 0 && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTCK: " + objRatedCommentPupilBO.PeriodicEndingMark).Append(STR_SPACE);
                                    objInsert.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: " + objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicMiddleJudgement) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTGK: " + objRatedCommentPupilBO.PeriodicMiddleJudgement).Append(STR_SPACE);
                                    objInsert.PeriodicMiddleJudgement = objRatedCommentPupilBO.PeriodicMiddleJudgement;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: " + objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (!string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicEndingJudgement) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTCK: " + objRatedCommentPupilBO.PeriodicEndingJudgement).Append(STR_SPACE);
                                    objInsert.PeriodicEndingJudgement = objRatedCommentPupilBO.PeriodicEndingJudgement;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: " + objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            if (!string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                            {
                                oldObjectStr.Append("NX: ").Append(STR_SPACE);
                                newObjectStr.Append("NX: " + objRatedCommentPupilBO.Comment).Append(STR_SPACE);
                                objInsert.Comment = objRatedCommentPupilBO.Comment;
                                isInsertLog = true;
                            }
                            #endregion
                            oldObjectStr.Append("}.");
                            newObjectStr.Append("}.");
                            if (isInsertLog)
                            {
                                lstInsert.Add(objInsert);
                            }
                        }
                        else
                        {
                            #region Mon hoc & HDGD
                            if (objRatedCommentPupilBO.isCommenting == 0)
                            {
                                if (objDB.PeriodicMiddleMark != objRatedCommentPupilBO.PeriodicMiddleMark && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleMark).Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTGK: ").Append(objRatedCommentPupilBO.PeriodicMiddleMark).Append(STR_SPACE);
                                    objDB.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                    isInsertLog = true;
                                }
                                if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(objDB.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: ").Append(objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (objDB.PeriodicEndingMark != objRatedCommentPupilBO.PeriodicEndingMark && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingMark).Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTCK: ").Append(objRatedCommentPupilBO.PeriodicEndingMark).Append(STR_SPACE);
                                    objDB.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                    isInsertLog = true;
                                }
                                if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(objDB.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: ").Append(objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            else
                            {
                                if ((((string.IsNullOrEmpty(objDB.PeriodicMiddleJudgement) && !string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicMiddleJudgement))
                                    || ((!string.IsNullOrEmpty(objDB.PeriodicMiddleJudgement) && string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicMiddleJudgement)))
                                    || (!string.IsNullOrEmpty(objDB.PeriodicMiddleJudgement) && !string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicMiddleJudgement))))
                                    && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                                {
                                    if ((objDB.PeriodicMiddleJudgement != objRatedCommentPupilBO.PeriodicMiddleJudgement))
                                    {
                                        oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleJudgement).Append(STR_SPACE);
                                        newObjectStr.Append("Điểm KTGK: ").Append(objRatedCommentPupilBO.PeriodicMiddleJudgement).Append(STR_SPACE);
                                        objDB.PeriodicMiddleJudgement = objRatedCommentPupilBO.PeriodicMiddleJudgement;
                                        isInsertLog = true;
                                    }
                                }
                                if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(objDB.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: ").Append(objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if ((((string.IsNullOrEmpty(objDB.PeriodicEndingJudgement) && !string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicEndingJudgement))
                                       || ((!string.IsNullOrEmpty(objDB.PeriodicEndingJudgement) && string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicEndingJudgement)))
                                       || (!string.IsNullOrEmpty(objDB.PeriodicEndingJudgement) && !string.IsNullOrEmpty(objRatedCommentPupilBO.PeriodicEndingJudgement))))
                                    && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                                {
                                    if (objDB.PeriodicEndingJudgement != objRatedCommentPupilBO.PeriodicEndingJudgement)
                                    {
                                        oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingJudgement).Append(STR_SPACE);
                                        newObjectStr.Append("Điểm KTCK: ").Append(objRatedCommentPupilBO.PeriodicEndingJudgement).Append(STR_SPACE);
                                        objDB.PeriodicEndingJudgement = objRatedCommentPupilBO.PeriodicEndingJudgement;
                                        isInsertLog = true;
                                    }
                                }
                                if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(objDB.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: ").Append(objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            if ((!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                        || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                        || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment)))
                            {
                                if (objDB.Comment != objRatedCommentPupilBO.Comment)
                                {
                                    oldObjectStr.Append("NX: ").Append(objDB.Comment).Append(STR_SPACE);
                                    newObjectStr.Append("NX: ").Append(objRatedCommentPupilBO.Comment).Append(STR_SPACE);
                                    objDB.Comment = objRatedCommentPupilBO != null && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment) ? objRatedCommentPupilBO.Comment.Trim() : "";
                                    isInsertLog = true;
                                }
                            }
                            #endregion
                            oldObjectStr.Append("}. ");
                            newObjectStr.Append("}");

                            if (isInsertLog)
                            {
                                objDB.UpdateTime = DateTime.Now;
                                objDB.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                                RatedCommentPupilHistoryBusiness.Update(objDB);
                            }
                        }

                        if (isInsertLog)
                        {
                            ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                            objActionAudit.ObjID = objectIDStr;
                            objActionAudit.Parameter = paramsStr;
                            objActionAudit.Description = descriptionStr;
                            objActionAudit.UserAction = userActionsStr;
                            objActionAudit.UserFunction = userFuntionsStr;
                            objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr).Append(newObjectStr);
                            lstActionAudit.Add(objActionAudit);
                        }
                        objectIDStr = new StringBuilder();
                        paramsStr = new StringBuilder();
                        descriptionStr = new StringBuilder();
                        userActionsStr = new StringBuilder();
                        userFuntionsStr = new StringBuilder();
                        userDescriptionsStr = new StringBuilder();
                        oldObjectStr = new StringBuilder();
                        newObjectStr = new StringBuilder();
                    }
                }
                #endregion
                #region Nang luc pham chat
                else//Nang luc pham chat
                {
                    int pupilID = 0;
                    RatedCommentPupilBO objtmp = null;
                    for (int i = 0; i < lstPupilID.Count; i++)
                    {
                        pupilID = lstPupilID[i];
                        lstRatedtmp = lstRatedCommentPupilBO.Where(p => p.PupilID == pupilID).ToList();
                        objtmp = lstRatedtmp.FirstOrDefault();
                        lstLockRatedtmp = lstLockRated.Where(p => p.ClassID == objtmp.ClassID).ToList();
                        oldComment = string.Empty;
                        for (int j = 0; j < lstLockRatedtmp.Count; j++)
                        {
                            objLockTitle = lstLockRatedtmp[j];
                            strLockMarkTitle += objLockTitle != null ? objLockTitle.LockTitle + "," : string.Empty;
                        }
                        isInsertLog = false;
                        //doi tuong ghi log
                        objectIDStr.Append(objtmp.PupilID);
                        if (isImport)
                        {
                            strTitleLog = "Import nhận xét đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                        }
                        else
                        {
                            strTitleLog = "Cập nhật đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                        }
                        descriptionStr.Append(strTitleLog + "học sinh");
                        paramsStr.Append("PupilID: " + objtmp.PupilID + "ClassID: " + objtmp.ClassID + "EvaluationID: " + objtmp.EvaluationID);
                        userFuntionsStr.Append("Sổ đánh giá HS");
                        userDescriptionsStr.Append(strTitleLog + "Năng lực – Phẩm chất");
                        userDescriptionsStr.Append(" HS " + objtmp.FullName).Append(", mã " + objtmp.PupilCode);
                        userDescriptionsStr.Append(", Lớp " + objtmp.ClassName + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                        oldObjectStr.Append("Giá trị cũ {");
                        newObjectStr.Append("Giá trị mới {");

                        for (int j = 0; j < lstRatedtmp.Count; j++)
                        {
                            objtmp = lstRatedtmp[j];
                            objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objtmp.PupilID && p.ClassID == objtmp.ClassID && p.SubjectID == objtmp.SubjectID && p.EvaluationID == objtmp.EvaluationID).FirstOrDefault();
                            if (objDB == null)
                            {
                                objInsert = new RatedCommentPupilHistory();
                                objInsert.SchoolID = objtmp.SchoolID;
                                objInsert.AcademicYearID = objtmp.AcademicYearID;
                                objInsert.ClassID = objtmp.ClassID;
                                objInsert.PupilID = objtmp.PupilID;
                                objInsert.SemesterID = objtmp.SemesterID;
                                objInsert.EvaluationID = objtmp.EvaluationID;
                                objInsert.RetestMark = objtmp.RetestMark;
                                objInsert.EvaluationRetraning = objtmp.EvaluationRetraning;
                                objInsert.FullNameComment = objtmp.FullNameComment;
                                objInsert.SubjectID = objtmp.SubjectID;
                                objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                                objInsert.LogChangeID = objtmp.LogChangeID;
                                objInsert.CreateTime = DateTime.Now;
                                objInsert.UpdateTime = DateTime.Now;
                                if (objtmp.EvaluationID == 2)
                                {
                                    if (objtmp.CapacityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT)
                                            .Append(objtmp.CapacityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.CapacityMiddleEvaluation = objtmp.CapacityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objtmp.CapacityEndingEvaluation > 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT)
                                            .Append(objtmp.CapacityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.CapacityEndingEvaluation = objtmp.CapacityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }
                                else
                                {
                                    if (objtmp.QualityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.QualityMiddleEvaluation = objtmp.QualityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objtmp.QualityEndingEvaluation > 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.QualityEndingEvaluation = objtmp.QualityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }

                                if (!string.IsNullOrEmpty(objtmp.Comment))
                                {
                                    comment = objtmp.Comment;
                                    objInsert.Comment = objtmp.Comment;
                                    isInsertLog = true;
                                }
                                if (isInsertLog)
                                {
                                    lstInsert.Add(objInsert);
                                }
                            }
                            else
                            {
                                if (objtmp.EvaluationID == 2)
                                {
                                    if (objDB.CapacityMiddleEvaluation != objtmp.CapacityMiddleEvaluation && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objDB.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objtmp.CapacityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.CapacityMiddleEvaluation = objtmp.CapacityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objDB.CapacityEndingEvaluation != objtmp.CapacityEndingEvaluation && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objDB.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objtmp.CapacityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.CapacityEndingEvaluation = objtmp.CapacityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }
                                else
                                {
                                    if (objDB.QualityMiddleEvaluation != objtmp.QualityMiddleEvaluation && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(SPACE)
                                            .Append(STR_DOT).Append(objDB.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.QualityMiddleEvaluation = objtmp.QualityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objDB.QualityEndingEvaluation != objtmp.QualityEndingEvaluation && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(SPACE)
                                            .Append(STR_DOT).Append(objDB.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.QualityEndingEvaluation = objtmp.QualityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }
                                if ((!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objtmp.Comment))
                                        || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objtmp.Comment))
                                        || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objtmp.Comment)))
                                {
                                    if (objDB.Comment != objtmp.Comment)
                                    {
                                        oldComment = objDB.Comment;
                                        comment = objtmp.Comment.Trim();
                                        objDB.Comment = objtmp.Comment.Trim();
                                        isInsertLog = true;
                                    }
                                }
                                if (isInsertLog)
                                {
                                    objDB.UpdateTime = DateTime.Now;
                                    objDB.LogChangeID = objtmp.LogChangeID;
                                    RatedCommentPupilHistoryBusiness.Update(objDB);
                                }
                            }
                        }
                        //xu lu comment
                        if (oldComment != comment)
                        {
                            oldObjectStr.Append("NX: ").Append(oldComment);
                            newObjectStr.Append("NX: ").Append(comment);
                        }

                        oldObjectStr.Append("}. ");
                        newObjectStr.Append("}");
                        if (isInsertLog)
                        {
                            ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                            objActionAudit.ObjID = objectIDStr;
                            objActionAudit.Parameter = paramsStr;
                            objActionAudit.Description = descriptionStr;
                            objActionAudit.UserAction = userActionsStr;
                            objActionAudit.UserFunction = userFuntionsStr;
                            objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr).Append(newObjectStr);
                            lstActionAudit.Add(objActionAudit);
                        }
                        objectIDStr = new StringBuilder();
                        paramsStr = new StringBuilder();
                        descriptionStr = new StringBuilder();
                        userActionsStr = new StringBuilder();
                        userFuntionsStr = new StringBuilder();
                        userDescriptionsStr = new StringBuilder();
                        oldObjectStr = new StringBuilder();
                        newObjectStr = new StringBuilder();
                    }
                }
                #endregion
                if (lstInsert.Count > 0)
                {
                    for (int i = 0; i < lstInsert.Count; i++)
                    {
                        RatedCommentPupilHistoryBusiness.Insert(lstInsert[i]);
                    }
                }
                RatedCommentPupilBusiness.SaveDetect();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdate", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        public void DeleteCommentPupil(IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int PartionID = UtilsBusiness.GetPartionId(SchoolID);
            int TypeID = Utils.GetInt(dic, "TypeID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            int LogChangeID = Utils.GetInt(dic, "LogChangeID");
            int isCommenting = Utils.GetInt(dic, "isCommenting");
            List<RatedCommentPupilHistory> lstRatedCommentPupilDB = new List<RatedCommentPupilHistory>();
            List<RatedCommentPupilHistory> lstRatedtmp = new List<RatedCommentPupilHistory>();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => lstPupilID.Contains(p.PupilID)).Distinct().ToList();
            PupilOfClassBO objPOC = null;
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            // lay danh sach khoa danh gia
            IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"ClassID",ClassID},
                {"EvaluationID",TypeID}
            };
            string strLockMarkTitle = string.Empty;
            IQueryable<LockRatedCommentPupil> iqLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark);
            LockRatedCommentPupil objLockTitle = null;
            if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
            {
                iqLockRated = iqLockRated.Where(p => p.SubjectID == SubjectID);
                objLockTitle = iqLockRated.FirstOrDefault();
                strLockMarkTitle = objLockTitle != null ? objLockTitle.LockTitle : "";
            }
            else
            {
                List<LockRatedCommentPupil> lstLockRatedtmp = iqLockRated.ToList();
                for (int i = 0; i < lstLockRatedtmp.Count; i++)
                {
                    objLockTitle = lstLockRatedtmp[i];
                    strLockMarkTitle += objLockTitle != null ? objLockTitle.LockTitle + "," : string.Empty;
                }
            }

            // Tạo data ghi log
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            bool isInsertLog = false;

            IQueryable<RatedCommentPupilHistory> iqtmp = (from r in RatedCommentPupilHistoryBusiness.All
                                                          where r.SchoolID == SchoolID
                                                          && r.AcademicYearID == AcademicYearID
                                                          && r.LastDigitSchoolID == PartionID
                                                          && r.ClassID == ClassID
                                                          && r.SemesterID == SemesterID
                                                          && r.EvaluationID == TypeID
                                                          && lstPupilID.Contains(r.PupilID)
                                                          select r);
            if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
            {
                lstRatedCommentPupilDB = iqtmp.Where(p => p.SubjectID == SubjectID).ToList();
            }
            else
            {
                lstRatedCommentPupilDB = iqtmp.ToList();
            }
            RatedCommentPupilHistory objDB = null;
            if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
            {
                for (int i = 0; i < lstRatedCommentPupilDB.Count; i++)
                {
                    isInsertLog = false;
                    objDB = lstRatedCommentPupilDB[i];
                    objDB.LogChangeID = LogChangeID;
                    objPOC = lstPOC.Where(p => p.PupilID == objDB.PupilID).FirstOrDefault();
                    //doi tuong ghi log
                    objectIDStr.Append(objDB.PupilID);
                    descriptionStr.Append("Xóa nhận xét đánh giá học sinh");
                    paramsStr.Append("PupilID: " + objDB.PupilID + "ClassID: " + objDB.ClassID + "SubjectID: " + objDB.SubjectID + "EvaluationID: " + objDB.EvaluationID);
                    userFuntionsStr.Append("Sổ đánh giá HS");
                    userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                    userDescriptionsStr.Append("Xóa nhận xét đánh giá " + ("môn " + objSC.SubjectName));
                    userDescriptionsStr.Append(" HS " + objPOC.PupilFullName).Append(", mã " + objPOC.PupilCode);
                    userDescriptionsStr.Append(", Lớp " + objPOC.ClassName + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                    oldObjectStr.Append("Giá trị cũ {");
                    if (isCommenting == 0)
                    {
                        if ((objDB.PeriodicMiddleMark.HasValue && (objDB.PeriodicMiddleMark > 0 || !strLockMarkTitle.Contains("KTGK" + SemesterID))) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                        {
                            oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleMark.ToString()).Append(STR_SPACE);
                            objDB.PeriodicMiddleMark = null;
                            isInsertLog = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objDB.PeriodicMiddleJudgement) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                        {
                            oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleJudgement).Append(STR_SPACE);
                            objDB.PeriodicMiddleJudgement = null;
                            isInsertLog = true;
                        }
                    }

                    if (objDB.MiddleEvaluation.HasValue && objDB.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                    {
                        oldObjectStr.Append("ĐGGK: ").Append(objDB.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : GlobalConstants.EVALUATION_NOT_COMPLETE).Append(STR_SPACE);
                        objDB.MiddleEvaluation = null;
                        isInsertLog = true;
                    }

                    if (isCommenting == 0)
                    {
                        if ((objDB.PeriodicEndingMark.HasValue && (objDB.PeriodicEndingMark > 0 || !strLockMarkTitle.Contains("KTCK" + SemesterID))) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                        {
                            oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingMark.ToString()).Append(STR_SPACE);
                            objDB.PeriodicEndingMark = null;
                            isInsertLog = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objDB.PeriodicEndingJudgement) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                        {
                            oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingJudgement).Append(STR_SPACE);
                            objDB.PeriodicEndingJudgement = null;
                            isInsertLog = true;
                        }
                    }

                    if (objDB.EndingEvaluation.HasValue && objDB.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                    {
                        oldObjectStr.Append("ĐGCK: ").Append(objDB.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : GlobalConstants.EVALUATION_NOT_COMPLETE).Append(STR_SPACE);
                        objDB.EndingEvaluation = null;
                        isInsertLog = true;
                    }

                    if (!string.IsNullOrEmpty(objDB.Comment))
                    {
                        oldObjectStr.Append("NX: ").Append(objDB.Comment);
                        objDB.Comment = "";
                        isInsertLog = true;
                    }

                    oldObjectStr.Append("}.");
                    if (isInsertLog)
                    {
                        objDB.UpdateTime = null;
                        RatedCommentPupilHistoryBusiness.Update(objDB);
                        ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                        objActionAudit.ObjID = objectIDStr;
                        objActionAudit.Parameter = paramsStr;
                        objActionAudit.Description = descriptionStr;
                        objActionAudit.UserAction = userActionsStr;
                        objActionAudit.UserFunction = userFuntionsStr;
                        objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr);
                        lstActionAudit.Add(objActionAudit);
                    }
                    objectIDStr = new StringBuilder();
                    paramsStr = new StringBuilder();
                    descriptionStr = new StringBuilder();
                    userActionsStr = new StringBuilder();
                    userFuntionsStr = new StringBuilder();
                    userDescriptionsStr = new StringBuilder();
                    oldObjectStr = new StringBuilder();
                }
            }
            else
            {
                int PupilID = 0;
                RatedCommentPupilHistory objtmp = null;
                string oldComment = string.Empty;
                for (int i = 0; i < lstPupilID.Count; i++)
                {
                    PupilID = lstPupilID[i];
                    isInsertLog = false;
                    objPOC = lstPOC.Where(p => p.PupilID == PupilID).FirstOrDefault();
                    lstRatedtmp = lstRatedCommentPupilDB.Where(p => p.PupilID == PupilID).ToList();
                    objtmp = lstRatedtmp.FirstOrDefault();
                    if (objtmp == null)
                    {
                        continue;
                    }
                    objectIDStr.Append(objtmp.PupilID);
                    descriptionStr.Append("Xóa nhận xét đánh giá học sinh");
                    paramsStr.Append("PupilID: " + objtmp.PupilID + "ClassID: " + objtmp.ClassID + "SubjectID: " + objtmp.SubjectID + "EvaluationID: " + objtmp.EvaluationID);
                    userFuntionsStr.Append("Sổ đánh giá HS");
                    userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                    userDescriptionsStr.Append("Xóa nhận xét đánh giá Năng lực – Phẩm chất");
                    userDescriptionsStr.Append(" HS " + objPOC.PupilFullName).Append(", mã " + objPOC.PupilCode);
                    userDescriptionsStr.Append(", Lớp " + objPOC.ClassName + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                    oldObjectStr.Append("Giá trị cũ {");
                    for (int j = 0; j < lstRatedtmp.Count; j++)
                    {
                        objDB = lstRatedtmp[j];
                        if (objDB.EvaluationID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            if (objDB.CapacityMiddleEvaluation.HasValue && objDB.CapacityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                            {
                                oldObjectStr.Append("GK NL").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objDB.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                objDB.CapacityMiddleEvaluation = null;
                                isInsertLog = true;
                            }
                            if (objDB.CapacityEndingEvaluation.HasValue && objDB.CapacityEndingEvaluation > 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                            {
                                oldObjectStr.Append("CK NL").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objDB.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                objDB.CapacityEndingEvaluation = null;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objDB.QualityMiddleEvaluation.HasValue && objDB.QualityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                            {
                                oldObjectStr.Append("GK PC").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5
                                    ? "2" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4")
                                            .Append(STR_DOT).Append(objDB.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                objDB.QualityMiddleEvaluation = null;
                                isInsertLog = true;
                            }
                            if (objDB.QualityEndingEvaluation.HasValue && objDB.QualityEndingEvaluation > 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                            {
                                oldObjectStr.Append("CK PC").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5
                                    ? "2" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4")
                                            .Append(STR_DOT).Append(objDB.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                objDB.QualityEndingEvaluation = null;
                                isInsertLog = true;
                            }
                        }

                        if (!string.IsNullOrEmpty(objDB.Comment))
                        {
                            oldComment = objDB.Comment;
                            objDB.Comment = "";
                            isInsertLog = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(oldComment))
                    {
                        oldObjectStr.Append("NX: ").Append(oldComment);
                    }
                    oldObjectStr.Append("}.");
                    if (isInsertLog)
                    {
                        objDB.UpdateTime = null;
                        RatedCommentPupilHistoryBusiness.Update(objDB);
                        ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                        objActionAudit.ObjID = objectIDStr;
                        objActionAudit.Parameter = paramsStr;
                        objActionAudit.Description = descriptionStr;
                        objActionAudit.UserAction = userActionsStr;
                        objActionAudit.UserFunction = userFuntionsStr;
                        objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr);
                        lstActionAudit.Add(objActionAudit);
                    }
                    objectIDStr = new StringBuilder();
                    paramsStr = new StringBuilder();
                    descriptionStr = new StringBuilder();
                    userActionsStr = new StringBuilder();
                    userFuntionsStr = new StringBuilder();
                    userDescriptionsStr = new StringBuilder();
                    oldObjectStr = new StringBuilder();
                }
            }
            RatedCommentPupilHistoryBusiness.Save();
        }

        public void ImportFromInputMark(List<Object> insertList, List<Object> updateList)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;

                for (int i = 0; i < insertList.Count; i++)
                {
                    RatedCommentPupilHistory entity = (RatedCommentPupilHistory)insertList[i];
                    RatedCommentPupilHistoryBusiness.Insert(entity);
                }
                for (int i = 0; i < updateList.Count; i++)
                {
                    RatedCommentPupilHistory entity = (RatedCommentPupilHistory)updateList[i];
                    RatedCommentPupilHistoryBusiness.Update(entity);
                }

                RatedCommentPupilHistoryBusiness.Save();
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        #region API MOBILE
        public void InsertOrUpdateToMobile(RatedCommentPupilBO objRatedCommentPupilBO, IDictionary<string, object> dic)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int ClassID = Utils.GetInt(dic, "ClassID");
                int SubjectID = Utils.GetInt(dic, "SubjectID");
                int SemesterID = Utils.GetInt(dic, "SemesterID");
                int PupilID = Utils.GetInt(dic, "PupilID");
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                int TypeID = Utils.GetInt(dic, "TypeID");
                string TitleMark = Utils.GetString(dic, "TitleMark");
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<RatedCommentPupilHistory> lstRatedCommentPupilDB = new List<RatedCommentPupilHistory>();
                IQueryable<RatedCommentPupilHistory> iqRatedCommentDB = (from r in RatedCommentPupilHistoryBusiness.All
                                                                  where r.SchoolID == SchoolID
                                                                  && r.AcademicYearID == AcademicYearID
                                                                  && r.LastDigitSchoolID == PartionID
                                                                  && r.SemesterID == SemesterID
                                                                  && r.ClassID == ClassID
                                                                  && r.PupilID == PupilID
                                                                  select r);
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP && p.SubjectID == SubjectID);
                }
                else
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY);
                }
                lstRatedCommentPupilDB = iqRatedCommentDB.ToList();
                List<RatedCommentPupilHistory> lstInsert = new List<RatedCommentPupilHistory>();
                RatedCommentPupilHistory objDB = null;
                RatedCommentPupilHistory objInsert = null;
                // lay danh sach khoa danh gia
                List<int> lstClassID = new List<int>();
                lstClassID.Add(ClassID);
                IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"lstClassID",lstClassID},
                    {"EvaluationID",TypeID}
                };
                string strTitleLog = string.Empty;
                string strLockMarkTitle = string.Empty;
                List<LockRatedCommentPupil> lstLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark).ToList();
                List<LockRatedCommentPupil> lstLockRatedtmp = new List<LockRatedCommentPupil>();
                LockRatedCommentPupil objLockTitle = null;
                bool isInsertLog = false;
                #region Mon hoc va HDGD
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)//Mon hoc va HDGD
                {
                    objLockTitle = lstLockRated.Where(p => p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                    strLockMarkTitle = objLockTitle != null ? objLockTitle.LockTitle : "";
                    objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                    if (objDB == null)
                    {
                        objInsert = new RatedCommentPupilHistory();
                        objInsert.SchoolID = objRatedCommentPupilBO.SchoolID;
                        objInsert.AcademicYearID = objRatedCommentPupilBO.AcademicYearID;
                        objInsert.ClassID = objRatedCommentPupilBO.ClassID;
                        objInsert.PupilID = objRatedCommentPupilBO.PupilID;
                        objInsert.SemesterID = objRatedCommentPupilBO.SemesterID;
                        objInsert.EvaluationID = objRatedCommentPupilBO.EvaluationID;
                        objInsert.RetestMark = objRatedCommentPupilBO.RetestMark;
                        objInsert.EvaluationRetraning = objRatedCommentPupilBO.EvaluationRetraning;
                        objInsert.FullNameComment = objRatedCommentPupilBO.FullNameComment;
                        objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                        objInsert.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                        objInsert.CreateTime = DateTime.Now;
                        objInsert.UpdateTime = DateTime.Now;
                        #region Mon hoc & HDGD
                        objInsert.SubjectID = objRatedCommentPupilBO.SubjectID;
                        if (objRatedCommentPupilBO.isCommenting == 0)
                        {
                            if (objRatedCommentPupilBO.PeriodicMiddleMark > 0 && !strLockMarkTitle.Contains("KTGK" + SemesterID) && "KTGK".Equals(TitleMark))
                            {
                                objInsert.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID) && "DGGK".Equals(TitleMark))
                            {
                                objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.PeriodicEndingMark > 0 && !strLockMarkTitle.Contains("KTCK" + SemesterID) && "KTCK".Equals(TitleMark))
                            {
                                objInsert.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID) && "DGCK".Equals(TitleMark))
                            {
                                objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID) && "DGGK".Equals(TitleMark))
                            {
                                objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID) && "DGCK".Equals(TitleMark))
                            {
                                objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                        {
                            objInsert.Comment = objRatedCommentPupilBO.Comment;
                            isInsertLog = true;
                        }
                        #endregion
                        if (isInsertLog)
                        {
                            lstInsert.Add(objInsert);
                        }
                    }
                    else
                    {
                        #region Mon hoc & HDGD
                        if (objRatedCommentPupilBO.isCommenting == 0)
                        {
                            if (objDB.PeriodicMiddleMark != objRatedCommentPupilBO.PeriodicMiddleMark && "KTGK".Equals(TitleMark) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                            {
                                objDB.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                isInsertLog = true;
                            }
                            if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && "DGGK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                            {
                                objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.PeriodicEndingMark != objRatedCommentPupilBO.PeriodicEndingMark && "KTCK".Equals(TitleMark) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                            {
                                objDB.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                isInsertLog = true;
                            }
                            if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && "DGCK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                            {
                                objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && "DGGK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                            {
                                objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && "DGCK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                            {
                                objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        if ("NX".Equals(TitleMark) && (!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                    || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                    || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment)))
                        {
                            if (objDB.Comment != objRatedCommentPupilBO.Comment)
                            {
                                objDB.Comment = objRatedCommentPupilBO != null && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment) ? objRatedCommentPupilBO.Comment.Trim() : "";
                                isInsertLog = true;
                            }
                        }
                        if (isInsertLog)
                        {
                            objDB.UpdateTime = DateTime.Now;
                            objDB.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                            RatedCommentPupilHistoryBusiness.Update(objDB);
                        }
                        #endregion
                    }
                }
                #endregion
                #region Nang luc pham chat
                else//Nang luc pham chat
                {

                    lstLockRatedtmp = lstLockRated.Where(p => p.ClassID == ClassID).ToList();
                    for (int j = 0; j < lstLockRatedtmp.Count; j++)
                    {
                        objLockTitle = lstLockRatedtmp[j];
                        strLockMarkTitle += objLockTitle != null ? objLockTitle.LockTitle + "," : string.Empty;
                    }
                    isInsertLog = false;
                    objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.ClassID == objRatedCommentPupilBO.ClassID && p.SubjectID == objRatedCommentPupilBO.SubjectID && p.EvaluationID == objRatedCommentPupilBO.EvaluationID).FirstOrDefault();
                    if (objDB == null)
                    {
                        objInsert = new RatedCommentPupilHistory();
                        objInsert.SchoolID = objRatedCommentPupilBO.SchoolID;
                        objInsert.AcademicYearID = objRatedCommentPupilBO.AcademicYearID;
                        objInsert.ClassID = objRatedCommentPupilBO.ClassID;
                        objInsert.PupilID = objRatedCommentPupilBO.PupilID;
                        objInsert.SemesterID = objRatedCommentPupilBO.SemesterID;
                        objInsert.EvaluationID = objRatedCommentPupilBO.EvaluationID;
                        objInsert.RetestMark = objRatedCommentPupilBO.RetestMark;
                        objInsert.EvaluationRetraning = objRatedCommentPupilBO.EvaluationRetraning;
                        objInsert.FullNameComment = objRatedCommentPupilBO.FullNameComment;
                        objInsert.SubjectID = objRatedCommentPupilBO.SubjectID;
                        objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                        objInsert.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                        objInsert.CreateTime = DateTime.Now;
                        objInsert.UpdateTime = DateTime.Now;
                        if (objRatedCommentPupilBO.EvaluationID == 2)
                        {
                            if (objRatedCommentPupilBO.CapacityMiddleEvaluation > 0 && TitleMark.IndexOf("NLGK") == 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                            {
                                objInsert.CapacityMiddleEvaluation = objRatedCommentPupilBO.CapacityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.CapacityEndingEvaluation > 0 && TitleMark.IndexOf("NLCK") == 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                            {
                                objInsert.CapacityEndingEvaluation = objRatedCommentPupilBO.CapacityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objRatedCommentPupilBO.QualityMiddleEvaluation > 0 && TitleMark.IndexOf("PCGK") == 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                            {
                                objInsert.QualityMiddleEvaluation = objRatedCommentPupilBO.QualityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.QualityEndingEvaluation > 0 && TitleMark.IndexOf("PCCK") == 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                            {
                                objInsert.QualityEndingEvaluation = objRatedCommentPupilBO.QualityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }

                        if (!string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                        {
                            objInsert.Comment = objRatedCommentPupilBO.Comment;
                            isInsertLog = true;
                        }
                        if (isInsertLog)
                        {
                            lstInsert.Add(objInsert);
                        }
                    }
                    else
                    {
                        if (objRatedCommentPupilBO.EvaluationID == 2)
                        {
                            if (objDB.CapacityMiddleEvaluation != objRatedCommentPupilBO.CapacityMiddleEvaluation && TitleMark.IndexOf("NLGK") == 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                            {
                                objDB.CapacityMiddleEvaluation = objRatedCommentPupilBO.CapacityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.CapacityEndingEvaluation != objRatedCommentPupilBO.CapacityEndingEvaluation && TitleMark.IndexOf("NLCK") == 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                            {
                                objDB.CapacityEndingEvaluation = objRatedCommentPupilBO.CapacityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objDB.QualityMiddleEvaluation != objRatedCommentPupilBO.QualityMiddleEvaluation && TitleMark.IndexOf("PCGK") == 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                            {
                                objDB.QualityMiddleEvaluation = objRatedCommentPupilBO.QualityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.QualityEndingEvaluation != objRatedCommentPupilBO.QualityEndingEvaluation && TitleMark.IndexOf("PCCK") == 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                            {
                                objDB.QualityEndingEvaluation = objRatedCommentPupilBO.QualityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        if (TitleMark.IndexOf("NX") == 0 && (!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment)))
                        {
                            if (objDB.Comment != objRatedCommentPupilBO.Comment)
                            {
                                objDB.Comment = objRatedCommentPupilBO.Comment.Trim();
                                isInsertLog = true;
                            }
                        }
                        if (isInsertLog)
                        {
                            objDB.UpdateTime = DateTime.Now;
                            objDB.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                            if (TitleMark.IndexOf("NX") == 0)
                            {
                                var lsttmp = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.ClassID == objRatedCommentPupilBO.ClassID && p.EvaluationID == objRatedCommentPupilBO.EvaluationID).ToList();
                                for (int i = 0; i < lsttmp.Count; i++)
                                {
                                    lsttmp[i].Comment = objRatedCommentPupilBO.Comment.Trim();
                                    RatedCommentPupilHistoryBusiness.Update(lsttmp[i]);
                                }
                            }
                            else
                            {
                                RatedCommentPupilHistoryBusiness.Update(objDB);
                            }

                        }
                    }
                }
                #endregion
                if (lstInsert.Count > 0)
                {
                    for (int i = 0; i < lstInsert.Count; i++)
                    {
                        RatedCommentPupilHistoryBusiness.Insert(lstInsert[i]);
                    }
                }
                RatedCommentPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateToMobile", "null", ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        #endregion
    }
}
