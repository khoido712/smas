﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class DOETExamPupilBusiness
    {
        public IQueryable<DOETExamPupil> Search(IDictionary<string, object> search)
        {
            int examinationId = Utils.GetInt(search, "ExaminationsID");
            int groupid = Utils.GetInt(search, "GroupID");
            int unitId = Utils.GetInt(search, "UnitID");
            IQueryable<DOETExamPupil> iquery = DOETExamPupilBusiness.All;
            if (examinationId > 0)
            {
                iquery = iquery.Where(p => p.ExaminationsID == examinationId);
            }
            if (groupid > 0)
            {
                iquery = iquery.Where(p => p.ExamGroupID == groupid);
            }
            return iquery;
        }


        public IQueryable<DOETExamPupilBO> GetListExamPupil(IDictionary<string, object> dic)
        {
            int YearID = Utils.GetInt(dic, "YearID");
            int ExaminationsID = Utils.GetInt(dic, "ExaminationsID");
            int ExamGroupID = Utils.GetInt(dic, "ExamGroupID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            string SchoolName = Utils.GetString(dic, "SchoolName");
            string SchoolCode = Utils.GetString(dic, "SchoolCode");
            string FullName = Utils.GetString(dic, "PupilName");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> listPupilIDRegised = Utils.GetIntList(dic, "listPupilIDRegised");
            int Grade = Utils.GetInt(dic, "Grade");
            IQueryable<DOETExamPupilBO> IQuery = (from exp in DOETExamPupilBusiness.All
                                                  join ex in DOETExaminationsBusiness.All on exp.ExaminationsID equals ex.ExaminationsID
                                                  join pf in PupilProfileBusiness.All on exp.PupilID equals pf.PupilProfileID
                                                  join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                                                  join cp in ClassProfileBusiness.All.AsNoTracking() on poc.ClassID equals cp.ClassProfileID
                                                  join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                  join exs in DOETExamSubjectBusiness.All on exp.ExamGroupID equals exs.ExamGroupID
                                                  join m in DOETExamMarkBusiness.All.Where(p => p.DoetSubjectID == SubjectID) on exp.ExamPupilID equals m.ExamPupilID into l
                                                  from ml in l.DefaultIfEmpty()
                                                  where poc.Year == YearID
                                                  && exp.Year == YearID
                                                  && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                  && exs.DOETSubjectID == SubjectID
                                                  && exs.ExaminationsID == ex.ExaminationsID
                                                  select new DOETExamPupilBO
                                                  {
                                                      ExaminationsID = exp.ExaminationsID,
                                                      PupilID = exp.PupilID,
                                                      PupilCode = pf.PupilCode,
                                                      Name = pf.Name,
                                                      FullName = pf.FullName,
                                                      Genre = pf.Genre,
                                                      BirthDay = pf.BirthDate,
                                                      ClassID = poc.ClassID,
                                                      ClassName = cp.DisplayName,
                                                      SchoolID = poc.SchoolID,
                                                      SchoolName = sp.SchoolName,
                                                      ExamineeNumber = exp.ExamineeNumber,
                                                      ExamRoomName = exp.ExamRoom,
                                                      Location = exp.Location,
                                                      Mark = ml.Mark,
                                                      Year = ex.Year,
                                                      ExamGroupID = exp.ExamGroupID,
                                                      SubjectID = ml.DoetSubjectID,
                                                      SchoolCode = sp.SchoolCode,
                                                      EducationLevelID = exp.EducationLevelID,
                                                      SchedulesExam = exs.SchedulesExam,
                                                      OrderInClass = poc.OrderInClass
                                                  });
            if (ExaminationsID > 0)
            {
                IQuery = IQuery.Where(p => p.ExaminationsID == ExaminationsID);
            }
            if (ExamGroupID > 0)
            {
                IQuery = IQuery.Where(p => p.ExamGroupID == ExamGroupID);
            }

            if (!string.IsNullOrEmpty(SchoolName))
            {
                IQuery = IQuery.Where(p => p.SchoolName.ToUpper().Contains(SchoolName.ToUpper()));
            }
            if (!string.IsNullOrEmpty(SchoolCode))
            {
                IQuery = IQuery.Where(p => p.SchoolCode == SchoolCode);
            }
            if (!string.IsNullOrEmpty(FullName))
            {
                IQuery = IQuery.Where(p => p.FullName.ToUpper().Contains(FullName.ToUpper()));
            }
            if (SchoolID > 0)
            {
                IQuery = IQuery.Where(p => p.SchoolID == SchoolID);
            }
            if (Grade > 0)
            {
                IQuery = IQuery.Where(p => (Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY && p.EducationLevelID >= 1 && p.EducationLevelID <= 5)
                    || (Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY && p.EducationLevelID >= 6 && p.EducationLevelID <= 9)
                    || (Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && p.EducationLevelID >= 10 && p.EducationLevelID <= 12));
            }
            if (listPupilIDRegised != null && listPupilIDRegised.Count > 0)
            {
                IQuery = IQuery.Where(p => listPupilIDRegised.Contains(p.PupilID));
            }
            return IQuery;
        }

        public void UpdateExamPupil(List<DOETExamPupilBO> lstExamPupilBO, IDictionary<string, object> dic)
        {
            int DOETExaminationID = Utils.GetInt(dic, "ExaminationsID");
            int DOETExamGroupID = Utils.GetInt(dic, "ExamGroupID");
            List<int> lstPupilID = lstExamPupilBO.Select(p => p.PupilID).Distinct().ToList();
            List<int> lstSchoolID = lstExamPupilBO.Select(p => p.SchoolID).Distinct().ToList();
            List<DOETExamPupil> lstExamPupilDB = new List<DOETExamPupil>();
            lstExamPupilDB = DOETExamPupilBusiness.All.Where(p => lstPupilID.Contains(p.PupilID) && lstSchoolID.Contains(p.SchoolID) && p.ExaminationsID == DOETExaminationID && p.ExamGroupID == DOETExamGroupID).ToList();
            DOETExamPupilBO objExBO = null;
            DOETExamPupil objDB = null;
            List<DOETExamMarkBO> lstExamMarkBO = new List<DOETExamMarkBO>();
            DOETExamMarkBO objExamMarkBO = null;
            for (int i = 0; i < lstExamPupilDB.Count; i++)
            {
                objDB = lstExamPupilDB[i];
                objExBO = lstExamPupilBO.Where(p => p.PupilID == objDB.PupilID).FirstOrDefault();
                if (objExBO != null)
                {
                    objDB.ExamineeNumber = objExBO.ExamineeNumber;
                    objDB.ExamRoom = objExBO.ExamRoomName;
                    objDB.Location = objExBO.Location;
                    objDB.UpdateTime = DateTime.Now.Date;
                    if (objExBO.Mark.HasValue)
                    {
                        objExamMarkBO = new DOETExamMarkBO();
                        objExamMarkBO.ExamPupilID = objDB.ExamPupilID;
                        objExamMarkBO.Mark = objExBO.Mark.Value;
                        objExamMarkBO.DoetSubjectID = objExBO.ExamSubjectID;
                        lstExamMarkBO.Add(objExamMarkBO);
                    }
                }
                this.Update(objDB);
            }
            this.Save();
            if (lstExamMarkBO.Count > 0)
            {
                DOETExamMarkBusiness.InsertOrUpdateMark(lstExamMarkBO, dic);
            }
        }

        public IQueryable<DOETExamPupilBO> GetListDOETExamSubject(IDictionary<string, object> dic)
        {
            int YearID = Utils.GetInt(dic, "YearID");
            int ExaminationsID = Utils.GetInt(dic, "ExaminationsID");
            int ExamGroupID = Utils.GetInt(dic, "ExamGroupID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            string SchoolName = Utils.GetString(dic, "SchoolName");
            string SchoolCode = Utils.GetString(dic, "SchoolCode");
            string FullName = Utils.GetString(dic, "PupilName");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> listPupilIDRegised = Utils.GetIntList(dic, "listPupilIDRegised");
            int Grade = Utils.GetInt(dic, "Grade");
            int academicYearId = Utils.GetInt(dic, "AcademicYearID");

            IQueryable<DOETExamPupilBO> IQuery = (from exp in DOETExamPupilBusiness.All
                                                  join ex in DOETExaminationsBusiness.All on exp.ExaminationsID equals ex.ExaminationsID
                                                  join pf in PupilProfileBusiness.All on exp.PupilID equals pf.PupilProfileID
                                                  join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                                                  join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                  join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                  join exs in DOETExamSubjectBusiness.All on exp.ExamGroupID equals exs.ExamGroupID
                                                  where poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                  && poc.Year == YearID
                                                  && poc.SchoolID == SchoolID
                                                  && poc.AcademicYearID == academicYearId
                                                  && cp.AcademicYearID == academicYearId
                                                  && cp.IsActive == true
                                                  && exs.ExaminationsID == ex.ExaminationsID
                                                  select new DOETExamPupilBO
                                                  {
                                                      ExamPupilID = exp.ExamPupilID,
                                                      ExaminationsID = exp.ExaminationsID,
                                                      PupilID = exp.PupilID,
                                                      PupilCode = pf.PupilCode,
                                                      FullName = pf.FullName,
                                                      Genre = pf.Genre,
                                                      BirthDay = pf.BirthDate,
                                                      ClassID = poc.ClassID,
                                                      ClassName = cp.DisplayName,
                                                      SchoolID = poc.SchoolID,
                                                      SchoolName = sp.SchoolName,
                                                      ExamineeNumber = exp.ExamineeNumber,
                                                      ExamRoomName = exp.ExamRoom,
                                                      Location = exp.Location,
                                                      Year = ex.Year,
                                                      ExamGroupID = exp.ExamGroupID,
                                                      SchoolCode = sp.SchoolCode,
                                                      EducationLevelID = exp.EducationLevelID,
                                                      SchedulesExam = exs.SchedulesExam,
                                                      SubjectID = exs.DOETSubjectID,
                                                      ExamCouncil = ex.ExamCouncil,
                                                      BirthPlace = pf.BirthPlace,
                                                      Name = pf.Name,
                                                      SyncTime = exp.SyncTime,
                                                      SyncSourceId = exp.SyncSourceId
                                                  });
            if (ExaminationsID > 0)
            {
                IQuery = IQuery.Where(p => p.ExaminationsID == ExaminationsID);
            }
            if (ExamGroupID > 0)
            {
                IQuery = IQuery.Where(p => p.ExamGroupID == ExamGroupID);
            }
            if (YearID > 0)
            {
                IQuery = IQuery.Where(p => p.Year == YearID);
            }
            if (SubjectID > 0)
            {
                IQuery = IQuery.Where(p => p.SubjectID == SubjectID);
            }
            if (!string.IsNullOrEmpty(SchoolName))
            {
                IQuery = IQuery.Where(p => p.SchoolName == SchoolName);
            }
            if (!string.IsNullOrEmpty(SchoolCode))
            {
                IQuery = IQuery.Where(p => p.SchoolCode == SchoolCode);
            }
            if (!string.IsNullOrEmpty(FullName))
            {
                IQuery = IQuery.Where(p => p.FullName == FullName);
            }
            if (SchoolID > 0)
            {
                IQuery = IQuery.Where(p => p.SchoolID == SchoolID);
            }
            if (Grade > 0)
            {
                IQuery = IQuery.Where(p => (Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY && p.EducationLevelID >= 1 && p.EducationLevelID <= 5)
                    || (Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY && p.EducationLevelID >= 6 && p.EducationLevelID <= 9)
                    || (Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && p.EducationLevelID >= 10 && p.EducationLevelID <= 12));
            }
            if (listPupilIDRegised != null && listPupilIDRegised.Count > 0)
            {
                IQuery = IQuery.Where(p => listPupilIDRegised.Contains(p.PupilID));
            }

            //Order
            if (SubjectID > 0)
            {
                //1/ Nếu đã chọn môn: Sắp xếp DS thí sinh theo SBD, Họ tên HS
                IQuery = IQuery.OrderBy(p => p.ExamineeNumber).ThenBy(p => p.Name).ThenBy(p => p.FullName);
            }
            else
            {
                //2/ Nếu chưa chọn môn (chưa có môn học): sắp xếp theo họ tên HS
                IQuery = IQuery.OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassName).OrderBy(p => p.Name).ThenBy(p => p.FullName);
            }
            return IQuery;
        }

        public List<DOETExamMarkBO> GetListMark(List<int> listExamPupilID, List<int> listSubjectId)
        {
            List<DOETExamMarkBO> listResult = (from a in DOETExamMarkBusiness.All
                                               where listExamPupilID.Contains(a.ExamPupilID)
                                               && listSubjectId.Contains(a.DoetSubjectID)
                                               select new DOETExamMarkBO
                                               {
                                                   ExamMarkID = a.ExamMarkID,
                                                   ExamPupilID = a.ExamPupilID,
                                                   Mark = a.Mark,
                                                   DoetSubjectID = a.DoetSubjectID
                                               }).ToList();

            return listResult;
        }
        /// <summary>
        /// Ham lay danh sach thi sinh cua cac hoc sinh cu the da duoc xem nhom ma co mon thi nam trong danh sach mon thi cu the
        /// </summary>
        /// <param name="listPupilID"></param>
        /// <param name="listSubjectID"></param>
        /// <param name="examinationsID"></param>
        /// <returns></returns>
        public IQueryable<DOETExamPupil> GetGroupedPupilWithSameSubject(List<int> listPupilID, List<int> listSubjectID, int examinationsID)
        {
            IQueryable<DOETExamPupil> query = from ep in DOETExamPupilRepository.All.Where(o => o.ExaminationsID == examinationsID)
                                              join eg in DOETExamGroupRepository.All on ep.ExamGroupID equals eg.ExamGroupID
                                              join es in DOETExamSubjectRepository.All on new { eg.ExaminationsID, eg.ExamGroupID } equals new { es.ExaminationsID, es.ExamGroupID }
                                              where (
                                              eg.ExaminationsID == examinationsID
                                              && listPupilID.Contains(ep.PupilID)
                                              && listSubjectID.Contains(es.DOETSubjectID))
                                              select ep;

            return query;
        }

        public void InsertList(List<DOETExamPupil> insertList)
        {
            try
            {
                if (insertList == null || insertList.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < insertList.Count; i++)
                {
                    DOETExamPupilBusiness.Insert(insertList[i]);
                }
                var detachContext = ((IObjectContextAdapter)context).ObjectContext;
                detachContext.SaveChanges(SaveOptions.DetectChangesBeforeSave);

                
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// Xoa theo danh sach ID
        /// </summary>
        /// <param name="inputExamPupilID"></param>
        public void DeleteList(List<int> listExamPupilID)
        {
            try
            {
                //Kiem tra list rong
                if (listExamPupilID.Count == 0)
                {
                    throw new BusinessException("ExamPupil_Validate_Delete_NoChoice");
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                //Kiem tra du lieu duoc xoa hay khong
                //bool checkExist = ExamPupilInUsed(listExamPupilID);
                //if (checkExist)
                //{
                //    throw new BusinessException("ExamPupil_Validate_Delete_NotDelete");
                //}
                //Xoa thong tin diem lien quan den thi sinh
                List<DOETExamMark> listExamMarkId = DOETExamMarkBusiness.All.Where(p => listExamPupilID.Contains(p.ExamPupilID)).ToList();
                if (listExamMarkId != null && listExamMarkId.Count > 0)
                {
                    for (int i = 0; i < listExamMarkId.Count; i++)
                    {
                        DOETExamMarkBusiness.Delete(listExamMarkId[i].ExamMarkID);
                    }
                    DOETExamMarkBusiness.Save();
                }

                //Xoa thong tin thi sinh
                for (int i = 0; i < listExamPupilID.Count; i++)
                {
                    this.DOETExamPupilBusiness.Delete(listExamPupilID[i]);
                }
                DOETExamPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// Kiểm tra thi sinh đã có dữ liệu ràng buộc hay chưa
        /// </summary>
        /// <param name="inputExamPupilID">Danh sach ID thi sinh</param>
        /// <returns>Danh sach cac ID thi sinh da co du lieu rang buoc</returns>
        private bool ExamPupilInUsed(List<int> listExamPupilID)
        {
            List<int> ret = new List<int>();
            IQueryable<DOETExamMark> IqueryMark = DOETExamMarkBusiness.All.Where(p => listExamPupilID.Contains(p.ExamPupilID));
            if (IqueryMark.Count() > 0)
            {
                return true;
            }
            return false;
        }

        #region Xuat excel danh sach thi sinh phong dang ky len so
        public Stream CreateExamPupilRegisSubReport(IDictionary<string, object> dic)
        {
            int UnitId = Utils.GetInt(dic["UnitId"]);
            int Year = Utils.GetInt(dic["Year"]);
            int ExaminationsId = Utils.GetInt(dic["ExaminationsId"]);
            int ExamGroupId = Utils.GetInt(dic["ExamGroupId"]);
            int DOETSubjectId = Utils.GetInt(dic["DOETSubjectId"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_PHONG_DK;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Điền các thông tin tiêu đề báo cáo
            DOETExaminations exam = DOETExaminationsBusiness.Find(ExaminationsId);
            DOETSubject subject = DOETSubjectBusiness.Find(DOETSubjectId);
            oSheet.SetCellValue("A3", exam.ExaminationsName + " - Năm học " + Year.ToString() + "-" + (Year + 1).ToString());
            oSheet.SetCellValue("A4", "Môn " + subject.DOETSubjectName);

            List<DOETExamPupilBO> listResult = (from ep in DOETExamPupilBusiness.All
                                                join pp in PupilProfileBusiness.All on ep.PupilID equals pp.PupilProfileID
                                                join poc in PupilOfClassBusiness.All.Where(o => o.Year == Year && o.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS) on new { ep.SchoolID, ep.PupilID } equals new { poc.SchoolID, poc.PupilID } into des1
                                                from x in des1.DefaultIfEmpty()
                                                join cp in ClassProfileBusiness.All on x.ClassID equals cp.ClassProfileID into des2
                                                from y in des2.DefaultIfEmpty()
                                                join sp in SchoolProfileBusiness.All on ep.SchoolID equals sp.SchoolProfileID
                                                join em in DOETExamMarkBusiness.All.Where(o => o.DoetSubjectID == DOETSubjectId) on ep.ExamPupilID equals em.ExamPupilID into des3
                                                from z in des3.DefaultIfEmpty()
                                                where ep.Year == Year
                                                    && ep.ExaminationsID == ExaminationsId
                                                    && ep.ExamGroupID == ExamGroupId
                                                    && sp.SupervisingDeptID == UnitId
                                                    && y.IsActive == true
                                                select new DOETExamPupilBO
                                                {
                                                    ExamPupilID = ep.ExamPupilID,
                                                    PupilID = ep.PupilID,
                                                    BirthDay = pp.BirthDate,
                                                    ClassName = y != null ? y.DisplayName : String.Empty,
                                                    ExamineeNumber = ep.ExamineeNumber,
                                                    ExamRoomName = ep.ExamRoom,
                                                    Genre = pp.Genre,
                                                    Location = ep.Location,
                                                    Mark = z.Mark,
                                                    PupilCode = pp.PupilCode,
                                                    FullName = pp.FullName,
                                                    Name = pp.Name,
                                                    SchoolName = sp.SchoolName,
                                                    SchoolCode = sp.SchoolCode
                                                }).OrderBy(o => o.ExamineeNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            int startRow = 7;
            int lastRow = startRow + listResult.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 10;
            int curColumn = startColumn;

            for (int i = 0; i < listResult.Count; i++)
            {
                DOETExamPupilBO obj = listResult[i];
                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //Ma hoc sinh
                oSheet.SetCellValue(curRow, curColumn, obj.PupilCode);
                curColumn++;
                //Ho va ten
                oSheet.SetCellValue(curRow, curColumn, obj.FullName);
                curColumn++;
                //Ngay sinh
                oSheet.SetCellValue(curRow, curColumn, obj.BirthDay.ToString("dd/MM/yyyy"));
                curColumn++;
                //Ma turong
                oSheet.SetCellValue(curRow, curColumn, obj.SchoolCode);
                curColumn++;
                //Mon thi
                oSheet.SetCellValue(curRow, curColumn, subject.DOETSubjectName);
                curColumn++;
                //SBD
                oSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                curColumn++;
                //Phong thi
                oSheet.SetCellValue(curRow, curColumn, obj.ExamRoomName);
                curColumn++;
                //Dia diem thi
                oSheet.SetCellValue(curRow, curColumn, obj.Location);
                curColumn++;
                //diem
                string strMark = String.Empty;
                if (obj.Mark.HasValue)
                {
                    if (obj.Mark == 10 || obj.Mark == 0)
                    {
                        strMark = obj.Mark.Value.ToString();

                    }
                    else
                    {
                        strMark = obj.Mark.Value.ToString("0.0");
                    }

                }
                oSheet.SetCellValue(curRow, curColumn, strMark);
                curColumn++;

                curRow++;
                curColumn = startColumn;
            }

            IVTRange globalRange = oSheet.GetRange(6, startColumn, lastRow, lastColumn);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            return oBook.ToStream();

        }

        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamPupilRegisSubReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_PHONG_DK;

            int DOETSubjectId = Utils.GetInt(dic["DOETSubjectId"]);

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = String.Empty;
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            //int subjectID = Utils.GetInt(dic, "SubjectID");
            DOETSubject subject = DOETSubjectBusiness.Find(DOETSubjectId);
            string subjectName = subject != null ? subject.DOETSubjectName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return pr;
        }

        #endregion


        public List<DOETExamPupilBO> GetSyncExamPupilHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel, int examinationId, int examGroupId, int subjectId, bool isNotSync)
        {

            IQueryable<DOETExamPupilBO> IQuery = (from exp in DOETExamPupilBusiness.All.Where(e => e.ExaminationsID == examinationId)
                                                  join ex in DOETExaminationsBusiness.All on exp.ExaminationsID equals ex.ExaminationsID
                                                  join pf in PupilProfileBusiness.All on exp.PupilID equals pf.PupilProfileID
                                                  join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                                                  join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                  join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                  join exs in DOETExamSubjectBusiness.All on exp.ExamGroupID equals exs.ExamGroupID
                                                  join et in EthnicBusiness.All.Where(e=>e.IsActive==true) on pf.EthnicID equals et.EthnicID
                                                  where poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                  && poc.AcademicYearID == academicYear.AcademicYearID
                                                  && poc.SchoolID == academicYear.SchoolID
                                                  && cp.AcademicYearID == academicYear.AcademicYearID
                                                  && cp.IsActive == true
                                                  && pf.IsActive == true
                                                  && exs.ExaminationsID == examinationId
                                                  && exp.ExaminationsID == examinationId
                                                  && ex.ExaminationsID == examinationId
                                                  && sp.SchoolProfileID == school.SchoolProfileID
                                                  select new DOETExamPupilBO
                                                  {
                                                      ExamPupilID = exp.ExamPupilID,
                                                      ExaminationsID = exp.ExaminationsID,
                                                      PupilID = exp.PupilID,
                                                      PupilCode = pf.PupilCode,
                                                      FullName = pf.FullName,
                                                      Genre = pf.Genre,
                                                      BirthDay = pf.BirthDate,
                                                      ClassID = poc.ClassID,
                                                      ClassName = cp.DisplayName,
                                                      SchoolID = poc.SchoolID,
                                                      SchoolName = sp.SchoolName,
                                                      ExamineeNumber = exp.ExamineeNumber,
                                                      ExamRoomName = exp.ExamRoom,
                                                      Location = exp.Location,
                                                      Year = ex.Year,
                                                      ExamGroupID = exp.ExamGroupID,
                                                      SchoolCode = sp.SchoolCode,
                                                      EducationLevelID = exp.EducationLevelID,
                                                      SchedulesExam = exs.SchedulesExam,
                                                      SubjectID = exs.DOETSubjectID,
                                                      ExamCouncil = ex.ExamCouncil,
                                                      BirthPlace = pf.BirthPlace,
                                                      Name = pf.Name,
                                                      SyncTime = exp.SyncTime,
                                                      SyncSourceId = exp.SyncSourceId,
                                                      SyncSourceEthnicId=et.SyncSourceId,
                                                      EthnicId=et.EthnicID,
                                                      ExamSubjectID=exs.ExamSubjectID,
                                                      
                                                  });

            if (examGroupId > 0)
            {
                IQuery = IQuery.Where(p => p.ExamGroupID == examGroupId);
            }

            if (subjectId > 0)
            {
                IQuery = IQuery.Where(p => p.SubjectID == subjectId);
            }

            if (appliedLevel > 0)
            {
                if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                {
                    IQuery = IQuery.Where(p => p.EducationLevelID >= 1 && p.EducationLevelID <= 5);
                }
                else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    IQuery = IQuery.Where(p => p.EducationLevelID >= 6 && p.EducationLevelID <= 9);
                }
                else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    IQuery = IQuery.Where(p => p.EducationLevelID >= 10 && p.EducationLevelID <= 12);
                }
            }
            //Lay cac du lieu chua dong bo sang ben soHCM
            if (isNotSync)
            {
                IQuery = IQuery.Where(p => !p.SyncTime.HasValue);
            }

            IQuery = IQuery.OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassName).OrderBy(p => p.Name).ThenBy(p => p.FullName);

            List<DOETExamPupilBO> listVal = IQuery.ToList();
            return listVal;
        }
    }
}