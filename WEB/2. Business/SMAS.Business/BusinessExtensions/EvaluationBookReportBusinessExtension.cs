﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class EvaluationBookReportBusiness
    {
        public ReportDefinition GetReportDefinition(string reportCode)
        {
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }
        public string GetHashKeyEvaluationBookReport(EvaluationBookReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"SemesterID",entity.SemesterID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        public ProcessedReport GetProcessedReport(EvaluationBookReportBO entity,bool isZip = false)
        {
            string reportCode = isZip ? SystemParamsInFile.REPORT_EVALUATION_BOOK_ZIPFILE : SystemParamsInFile.REPORT_EVALUATION_BOOK;
            string inputParameterHashKey = this.GetHashKeyEvaluationBookReport(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport InsertEvaluationBookReport(EvaluationBookReportBO entity, Stream data,bool isZip = false)
        {
            string reportCode = isZip ? SystemParamsInFile.REPORT_EVALUATION_BOOK_ZIPFILE : SystemParamsInFile.REPORT_EVALUATION_BOOK;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyEvaluationBookReport(entity);
            pr.ReportData = isZip ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string SemesterName = ReportUtils.ConvertSemesterForReportName(entity.SemesterID);
            if (!isZip)
            {
                string ClassName = Utils.StripVNSignAndSpace(ClassProfileBusiness.Find(entity.ClassID).DisplayName);
                outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
                outputNamePattern = outputNamePattern.Replace("[Semester]", SemesterName);
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[EducationLevelName]", "Khoi" + entity.EducationLevelID);
                outputNamePattern = outputNamePattern.Replace("[HK]", SemesterName);
            }
            

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.SemesterID},
                {"EducationLevelID",entity.EducationLevelID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public Stream GetEvaluationBookReport(EvaluationBookReportBO Entity)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_EVALUATION_BOOK);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            #region lay du lieu can thiet
            SchoolProfile objSP = SchoolProfileBusiness.Find(Entity.SchoolID);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID);
            ClassProfiletmp objCP = (from cp in ClassProfileBusiness.All
                                     join e in EmployeeBusiness.All on cp.HeadTeacherID equals e.EmployeeID
                                     into el
                                     from lje in el.DefaultIfEmpty()
                                     where cp.AcademicYearID == Entity.AcademicYearID
                                     && cp.SchoolID == Entity.SchoolID
                                     && cp.ClassProfileID == Entity.ClassID
                                     && cp.IsVnenClass == true
                                     && cp.IsActive==true
                                     select new ClassProfiletmp
                                     {
                                         ClassID = cp.ClassProfileID,
                                         EducationLevelID = cp.EducationLevelID,
                                         DisplayName = cp.DisplayName,
                                         HeadTeacherName = lje.FullName,
                                         SectionKeyID = cp.SeperateKey
                                     }).FirstOrDefault();
            #region hoc sinh cua lop
            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           where poc.AcademicYearID == Entity.AcademicYearID
                                           && poc.SchoolID == Entity.SchoolID
                                           && poc.ClassID == Entity.ClassID
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               Status = poc.Status,
                                               PupilFullName = pf.FullName,
                                               Name = pf.Name,
                                               OrderInClass = poc.OrderInClass,
                                               Birthday = pf.BirthDate,
                                               BirthPlace = pf.BirthPlace,
                                               Genre = pf.Genre,
                                               EthnicName = pf.Ethnic.EthnicName,
                                               FatherFullName = pf.FatherFullName,
                                               FatherJobName = pf.FatherJob,
                                               MotherFullName = pf.MotherFullName,
                                               MotherJobName = pf.MotherJob,
                                               SponserFullName = pf.SponsorFullName,
                                               SponserJobName = pf.SponsorJob,
                                               PolicyTargetID = pf.PolicyTargetID,
                                               PolicyTargetName = pf.PolicyTarget.Resolution,
                                               PermanentResidentalAddress = pf.PermanentResidentalAddress,
                                               TempReidentalAddress = pf.TempResidentalAddress
                                           }).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();
            #endregion
            #region hoc sinh nghi hoc
            List<int> lstSectionKeyID = objCP != null && objCP.SectionKeyID.HasValue ? UtilsBusiness.GetListSectionID(objCP.SectionKeyID.Value) : new List<int>();
            IDictionary<string, object> dicAbsence = new Dictionary<string, object>()
            {
                {"AcademicYearID",Entity.AcademicYearID},
                {"ClassID",Entity.ClassID},
                {"EducationLevelID",Entity.EducationLevelID}
            };
            List<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.SearchBySchool(Entity.SchoolID, dicAbsence).Where(p => lstPupilID.Contains(p.PupilID) && lstSectionKeyID.Contains(p.Section)).ToList(); ;
            #endregion
            #region hoc sinh len lop o lai

            IDictionary<string, object> dicReview = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                {"ClassID",Entity.ClassID},
                {"lstPupilID",lstPupilID}
            };
            List<ReviewBookPupil> lstReviewBookPupil = ReviewBookPupilBusiness.Search(dicReview).ToList();
            #endregion
            #region diem kiem tra cuoi ky VNEN
            List<TeacherNoteBookSemesterBO> lstTeacherNoteBookSemester = (from t in TeacherNoteBookSemesterBusiness.All
                                                                          join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                                                          join cs in ClassSubjectBusiness.All on new { t.ClassID, t.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                                          where t.SchoolID == Entity.SchoolID
                                                                          && t.AcademicYearID == Entity.AcademicYearID
                                                                          && t.ClassID == Entity.ClassID
                                                                          && lstPupilID.Contains(t.PupilID)
                                                                          && t.PartitionID == (Entity.SchoolID % 100)
                                                                          select new TeacherNoteBookSemesterBO
                                                                          {
                                                                              PupilID = t.PupilID,
                                                                              PERIODIC_SCORE_END = t.PERIODIC_SCORE_END,
                                                                              PERIODIC_SCORE_END_JUDGLE = t.PERIODIC_SCORE_END_JUDGLE,
                                                                              PERIODIC_SCORE_MIDDLE = t.PERIODIC_SCORE_MIDDLE,
                                                                              PERIODIC_SCORE_MIDDLE_JUDGE = t.PERIODIC_SCORE_MIDDLE_JUDGE,
                                                                              AVERAGE_MARK = t.AVERAGE_MARK,
                                                                              AVERAGE_MARK_JUDGE = t.AVERAGE_MARK_JUDGE,
                                                                              SubjectID = t.SubjectID,
                                                                              SubjectName = sc.SubjectName,
                                                                              isCommenting = cs.IsCommenting,
                                                                              SemesterID = t.SemesterID
                                                                          }).ToList();
            #endregion
            #region Diem TBM theo hoc ky
            List<SummedUpRecordBO> lstSur = (from sur in SummedUpRecordBusiness.All
                                             join sc in SubjectCatBusiness.All on sur.SubjectID equals sc.SubjectCatID
                                             join cs in ClassSubjectBusiness.All on new { sur.ClassID, sur.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                             where sur.SchoolID == Entity.SchoolID
                                             && sur.AcademicYearID == Entity.AcademicYearID
                                             && lstPupilID.Contains(sur.PupilID)
                                             && sur.ClassID == Entity.ClassID
                                             && sur.Last2digitNumberSchool == (Entity.SchoolID % 100)
                                             select new SummedUpRecordBO
                                             {
                                                 PupilID = sur.PupilID,
                                                 SummedUpMark = sur.SummedUpMark,
                                                 JudgementResult = sur.JudgementResult,
                                                 SubjectID = sur.SubjectID,
                                                 SubjectName = sc.SubjectName,
                                                 IsCommenting = cs.IsCommenting,
                                                 Semester = sur.Semester
                                             }).ToList();
            #endregion
            #region danh sach mon hoc
            IQueryable<ClassSubjectBO> iqCSBO = (from cs in ClassSubjectBusiness.All
                                                          join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                          join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                          where cp.AcademicYearID == Entity.AcademicYearID
                                                          && cp.SchoolID == Entity.SchoolID
                                                          && cs.ClassID == Entity.ClassID
                                                          && cp.IsActive.Value
                                                          select new ClassSubjectBO
                                                          {
                                                              ClassID = cs.ClassID,
                                                              SubjectID = cs.SubjectID,
                                                              SubjectName = sc.DisplayName,
                                                              IsCommenting = cs.IsCommenting,
                                                              AppliedType = cs.AppliedType,
                                                              OrderInSubject = sc.OrderInSubject,
                                                              IsVNEN = cs.IsSubjectVNEN,
                                                              IsForeignLanguage = sc.IsForeignLanguage,
                                                              SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                                                              SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester
                                                          }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName);

            //if (Entity.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            //{
            //    iqCSBO = iqCSBO.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
            //}
            //else if (Entity.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            //{
            //    iqCSBO = iqCSBO.Where(o => o.SectionPerWeekFirstSemester > 0);
            //}
            //else iqCSBO = iqCSBO.Where(o => o.SectionPerWeekSecondSemester > 0);

            List<ClassSubjectBO> lstClassSubject = iqCSBO.ToList();

            List<ClassSubjectBO> lstResultSubject = new List<ClassSubjectBO>();
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN == true && o.IsCommenting != 1));
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN == true && o.IsCommenting == 1));
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN != true && o.IsCommenting != 1));
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN != true && o.IsCommenting == 1));
            #endregion
            #region Danh hieu thi dua
            List<RewardFinal> lstRewardFinal = (from r in RewardFinalBusiness.All
                                                join ay in AcademicYearBusiness.All on r.SchoolID equals ay.SchoolID
                                                where r.SchoolID == Entity.SchoolID
                                                && ay.AcademicYearID == Entity.AcademicYearID
                                                select r).ToList();
            IDictionary<int, string> dicRewardFinal = new Dictionary<int, string>();
            lstRewardFinal.ForEach(p =>
            {
                dicRewardFinal[p.RewardFinalID] = p.RewardMode;
            });

            IDictionary<string, object> dicUpdateReward = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                {"ClassID",Entity.ClassID},
                {"SemesterID",Entity.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND}
            };
            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.Search(dicUpdateReward).Where(p => lstPupilID.Contains(p.PupilID)).ToList();
            #endregion
            #endregion
            this.GetDataToFile(Entity, oBook, objSP, objAcademicYear, objCP, lstPOC, lstPupilAbsence, lstReviewBookPupil, lstResultSubject, lstTeacherNoteBookSemester, lstSur, lstUpdateReward, dicRewardFinal);
            return oBook.ToStream();
        }
        public void GetZipFileReport(EvaluationBookReportBO Entity,IDictionary<string,object> dic,out string newfolder)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            bool IsAdminSchoolRole = Utils.GetBool(dic, "IsAdminSchoolRole");
            bool IsViewAll = Utils.GetBool(dic, "IsViewAll");
            bool IsEmployeeManager = Utils.GetBool(dic, "IsEmployeeManager");
            int UserAccountID = Utils.GetInt(dic, "UserAccountID");
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = AppliedLevel;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            if (IsAdminSchoolRole == false && IsViewAll == false && !IsEmployeeManager)
            {
                dic["UserAccountID"] = UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            //List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(Entity.SchoolID, dic).Where(p => p.IsVnenClass == true).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            List<ClassProfiletmp> lstClass = (from cp in ClassProfileBusiness.SearchBySchool(Entity.SchoolID, dic)
                                              join e in EmployeeBusiness.All on cp.HeadTeacherID equals e.EmployeeID
                                              into el
                                              from lje in el.DefaultIfEmpty()
                                              where cp.IsVnenClass == true
                                              select new ClassProfiletmp
                                              {
                                                  ClassID = cp.ClassProfileID,
                                                  EducationLevelID = cp.EducationLevelID,
                                                  DisplayName = cp.DisplayName,
                                                  HeadTeacherName = lje.FullName,
                                                  SectionKeyID = cp.SeperateKey
                                              }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.DisplayName).ToList();
            List<int> lstClassID = lstClass.Select(p => p.ClassID).Distinct().ToList();
            string semesterName = Entity.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "HKI" : "CN";
            string fileName = "HS_THCS_SoDanhGiaHS_{0}_{1}.xls";
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            
            #region lay du lieu can thiet ngoai vong lap for...
            SchoolProfile objSP = SchoolProfileBusiness.Find(Entity.SchoolID);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID);
            #region hoc sinh cua lop
            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           where poc.AcademicYearID == Entity.AcademicYearID
                                           && poc.SchoolID == Entity.SchoolID
                                           && lstClassID.Contains(poc.ClassID)
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               ClassID = poc.ClassID,
                                               Status = poc.Status,
                                               PupilFullName = pf.FullName,
                                               Name = pf.Name,
                                               OrderInClass = poc.OrderInClass,
                                               Birthday = pf.BirthDate,
                                               BirthPlace = pf.BirthPlace,
                                               Genre = pf.Genre,
                                               EthnicName = pf.Ethnic.EthnicName,
                                               FatherFullName = pf.FatherFullName,
                                               FatherJobName = pf.FatherJob,
                                               MotherFullName = pf.MotherFullName,
                                               MotherJobName = pf.MotherJob,
                                               SponserFullName = pf.SponsorFullName,
                                               SponserJobName = pf.SponsorJob,
                                               PolicyTargetID = pf.PolicyTargetID,
                                               PolicyTargetName = pf.PolicyTarget.Resolution,
                                               PermanentResidentalAddress = pf.PermanentResidentalAddress,
                                               TempReidentalAddress = pf.TempResidentalAddress
                                           }).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();
            #endregion
            #region hoc sinh nghi hoc
            List<int> lstSectionKeyID = new List<int>();
            IDictionary<string, object> dicAbsence = new Dictionary<string, object>()
            {
                {"AcademicYearID",Entity.AcademicYearID},
                {"EducationLevelID",Entity.EducationLevelID}
            };
            List<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.SearchBySchool(Entity.SchoolID, dicAbsence).Where(p =>lstPupilID.Contains(p.PupilID)).ToList(); ;
            #endregion
            #region hoc sinh len lop o lai
            
            IDictionary<string, object> dicReview = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                {"lstPupilID",lstPupilID}
            };
            List<ReviewBookPupil> lstReviewBookPupil = ReviewBookPupilBusiness.Search(dicReview).ToList();
            #endregion
            #region diem kiem tra cuoi ky VNEN
            List<TeacherNoteBookSemesterBO> lstTeacherNoteBookSemester = (from t in TeacherNoteBookSemesterBusiness.All
                                                                          join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                                                          join cs in ClassSubjectBusiness.All on new { t.ClassID, t.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                                          where t.SchoolID == Entity.SchoolID
                                                                          && t.AcademicYearID == Entity.AcademicYearID
                                                                          && lstPupilID.Contains(t.PupilID)
                                                                          && t.PartitionID == (Entity.SchoolID % 100)
                                                                          select new TeacherNoteBookSemesterBO
                                                                          {
                                                                              PupilID = t.PupilID,
                                                                              ClassID = t.ClassID,
                                                                              PERIODIC_SCORE_END = t.PERIODIC_SCORE_END,
                                                                              PERIODIC_SCORE_END_JUDGLE = t.PERIODIC_SCORE_END_JUDGLE,
                                                                              PERIODIC_SCORE_MIDDLE = t.PERIODIC_SCORE_MIDDLE,
                                                                              PERIODIC_SCORE_MIDDLE_JUDGE = t.PERIODIC_SCORE_MIDDLE_JUDGE,
                                                                              AVERAGE_MARK = t.AVERAGE_MARK,
                                                                              AVERAGE_MARK_JUDGE = t.AVERAGE_MARK_JUDGE,
                                                                              SubjectID = t.SubjectID,
                                                                              SubjectName = sc.SubjectName,
                                                                              isCommenting = cs.IsCommenting,
                                                                              SemesterID = t.SemesterID
                                                                          }).ToList();
            #endregion
            #region Diem TBM theo hoc ky
            List<SummedUpRecordBO> lstSur = (from sur in SummedUpRecordBusiness.All
                                             join sc in SubjectCatBusiness.All on sur.SubjectID equals sc.SubjectCatID
                                             join cs in ClassSubjectBusiness.All on new { sur.ClassID, sur.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                             where sur.SchoolID == Entity.SchoolID
                                             && sur.AcademicYearID == Entity.AcademicYearID
                                             && lstPupilID.Contains(sur.PupilID)
                                             && sur.Last2digitNumberSchool == (Entity.SchoolID % 100)
                                             select new SummedUpRecordBO
                                             {
                                                 PupilID = sur.PupilID,
                                                 ClassID = sur.ClassID,
                                                 SummedUpMark = sur.SummedUpMark,
                                                 JudgementResult = sur.JudgementResult,
                                                 SubjectID = sur.SubjectID,
                                                 SubjectName = sc.SubjectName,
                                                 IsCommenting = cs.IsCommenting,
                                                 Semester = sur.Semester
                                             }).ToList();
            #endregion
            #region danh sach mon hoc
            List<ClassSubjectBO> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                    join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                    join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                    where cp.AcademicYearID == Entity.AcademicYearID
                                                    && cp.SchoolID == Entity.SchoolID
                                                    && lstClassID.Contains(cs.ClassID)
                                                    && cp.IsActive.Value
                                                    select new ClassSubjectBO
                                                    {
                                                        ClassID = cs.ClassID,
                                                        SubjectID = cs.SubjectID,
                                                        SubjectName = sc.DisplayName,
                                                        IsCommenting = cs.IsCommenting,
                                                        AppliedType = cs.AppliedType,
                                                        OrderInSubject = sc.OrderInSubject,
                                                        IsVNEN = cs.IsSubjectVNEN,
                                                        IsForeignLanguage = sc.IsForeignLanguage,
                                                        SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                                                        SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester
                                                    }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            List<ClassSubjectBO> lstResultSubject = new List<ClassSubjectBO>();
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN == true && o.IsCommenting != 1));
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN == true && o.IsCommenting == 1));
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN != true && o.IsCommenting != 1));
            lstResultSubject.AddRange(lstClassSubject.Where(o => o.IsVNEN != true && o.IsCommenting == 1));
            #endregion
            #region Danh hieu thi dua
            List<RewardFinal> lstRewardFinal = (from r in RewardFinalBusiness.All
                                                join ay in AcademicYearBusiness.All on r.SchoolID equals ay.SchoolID
                                                where r.SchoolID == Entity.SchoolID
                                                && ay.AcademicYearID == Entity.AcademicYearID
                                                select r).ToList();
            IDictionary<int, string> dicRewardFinal = new Dictionary<int, string>();
            lstRewardFinal.ForEach(p =>
            {
                dicRewardFinal[p.RewardFinalID] = p.RewardMode;
            });

            IDictionary<string, object> dicUpdateReward = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                {"SemesterID",Entity.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND}
            };
            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.Search(dicUpdateReward).Where(p=>lstPupilID.Contains(p.PupilID)).ToList();
            #endregion
            #endregion
            int classID = 0;
            ClassProfiletmp objCP = null;
            List<PupilOfClassBO> lstPOCtmp = null;
            List<PupilAbsence> lstPAtmp = null;
            List<ReviewBookPupil> lstReviewBooktmp = null;
            List<ClassSubjectBO> lstCStmp = null;
            List<TeacherNoteBookSemesterBO> lstTNBtmp = null;
            List<SummedUpRecordBO> lstSURtmp = null;
            List<UpdateReward> lstUpdateRewardtmp = null;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_EVALUATION_BOOK);
            for (int i = 0; i < lstClass.Count; i++)
            {
                objCP = lstClass[i];
                classID = objCP.ClassID;
                lstSectionKeyID = objCP.SectionKeyID.HasValue ? UtilsBusiness.GetListSectionID(objCP.SectionKeyID.Value) : new List<int>();
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                lstPOCtmp = lstPOC.Where(p => p.ClassID == classID).ToList();
                lstPAtmp = lstPupilAbsence.Where(p => p.ClassID == classID && lstSectionKeyID.Contains(p.Section)).ToList();
                lstReviewBooktmp = lstReviewBookPupil.Where(p => p.ClassID == classID).ToList();
                lstCStmp = lstResultSubject.Where(p => p.ClassID == classID).ToList();
                lstTNBtmp = lstTeacherNoteBookSemester.Where(p => p.ClassID == classID).ToList();
                lstSURtmp = lstSur.Where(p => p.ClassID == classID).ToList();
                lstUpdateRewardtmp = lstUpdateReward.Where(p => p.ClassID == classID).ToList();
                this.GetDataToFile(Entity, oBook, objSP, objAcademicYear, objCP, lstPOCtmp, lstPAtmp, lstReviewBooktmp, lstCStmp, lstTNBtmp, lstSURtmp, lstUpdateRewardtmp, dicRewardFinal);
                oBook.CreateZipFile(string.Format(fileName,Utils.StripVNSignAndSpace(objCP.DisplayName),semesterName),folder);
            }
            newfolder = folder;
        }
        private void GetDataToFile(EvaluationBookReportBO Entity,IVTWorkbook oBook,SchoolProfile objSP,AcademicYear objAcademicYear,ClassProfiletmp objCP,List<PupilOfClassBO> lstPOC,
                                    List<PupilAbsence> lstPA, List<ReviewBookPupil> lstReviewBook, List<ClassSubjectBO> lstResultSubject, List<TeacherNoteBookSemesterBO> lstTeacherNoteBookSemester,
                                    List<SummedUpRecordBO> lstSur, List<UpdateReward> lstUpdateReward, IDictionary<int, string> dicRewardFinal)
        {
            #region Fill Sheet Thông tin
            //sheet Bia
            IVTWorksheet sheetBia = oBook.GetSheet("Bia");
            //sheet Trang 1
            IVTWorksheet sheetTrang1 = oBook.GetSheet("Trang 1");
            string schoolName = this.CutSchoolName(objSP.SchoolName);
            string className = this.CutClassName(objCP.DisplayName);
            Dictionary<string, object> dicSheet = new Dictionary<string, object>()
            {
                {"SchoolName",schoolName},
                {"ProvinceName",objSP.Province.ProvinceName},
                {"DistrictName",objSP.District.DistrictName},
                {"DisplayYear","Năm học: " + objAcademicYear.DisplayTitle},
                {"ClassName",className},
                {"CommuneName",objSP.Commune != null ? objSP.Commune.CommuneName : ""},
                {"HeadTeacherName",objCP.HeadTeacherName},
                {"HeadMasterName",objSP.HeadMasterName}
            };
            sheetBia.FillVariableValue(dicSheet);
            sheetTrang1.FillVariableValue(dicSheet);
            #endregion
            #region Fill du lieu
            #region lay danh sach tat ca cac sheet
            IVTWorksheet sheetTrang2 = oBook.GetSheet("Trang 2");
            IVTWorksheet sheetTrang3 = oBook.GetSheet("Trang 3");
            IVTWorksheet sheetTrang4 = oBook.GetSheet("Trang 4");
            IVTWorksheet sheetTrang5 = oBook.GetSheet("Trang 5");
            IVTWorksheet sheetTrang6 = oBook.GetSheet("Trang 6");
            IVTWorksheet sheetTrang7 = oBook.GetSheet("Trang 7");
            IVTWorksheet sheetTrang8 = oBook.GetSheet("Trang 8");
            IVTWorksheet sheetTrang9 = oBook.GetSheet("Trang 9");
            IVTWorksheet sheetTrang10 = oBook.GetSheet("Trang 10");
            IVTWorksheet sheetTrang11 = oBook.GetSheet("Trang 11");
            IVTWorksheet sheetTrang12 = oBook.GetSheet("Trang 12");
            IVTWorksheet sheetTrang13 = oBook.GetSheet("Trang 13");
            IVTWorksheet sheetTrang14 = oBook.GetSheet("Trang 14");
            IVTWorksheet sheetTrang15 = oBook.GetSheet("Trang 15");
            IVTWorksheet sheetTrang16 = oBook.GetSheet("Trang 16");
            IVTWorksheet sheetTrang17 = oBook.GetSheet("Trang 17");
            #endregion
            #region khai bao bien
            ClassSubjectBO objCS = null;
            TeacherNoteBookSemesterBO objTeacherNote = null;
            SummedUpRecordBO objSur = null;
            ReviewBookPupil objReviewBook = null;
            UpdateReward objUpdateReward = null;
            List<int> lstRewardMod = new List<int>();
            List<UpdateReward> lstUpdateRewardTMP = new List<UpdateReward>();
            string strReward = string.Empty;
            int startRow = 4;
            int start6 = 4;
            int start7 = 4;
            int start8 = 4;
            int start9 = 4;
            int start11 = 4;
            int start12 = 4;
            int start13 = 4;
            int start14 = 4;
            int start15 = 4;
            int pupilID = 0;
            PupilOfClassBO objPOC = null;
            int countP = 0;
            int countK = 0;
            int totalAbsence = 0;
            int countUpClass = 0;
            int countUpClassSummer = 0;
            int countNotUpClass = 0;
            int totalUpClass = 0;
            int totalNotUpClass = 0;
           
            string subjectName = string.Empty;
            int SkipPupil = 10;
            bool isStudying = true;
            bool check = true;
            string PolicyTargetName = string.Empty;
            int[] arrPolicyID = {25,26,44,54,56,60,61,62,63,65};//Con liệt sĩ,con TB,con BB,gia đình có công với CM..
            #endregion
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                isStudying = true;
                if (objPOC.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    isStudying = false;
                }
                #region fill sheet 2
                //Ho ten
                sheetTrang2.SetCellValue(startRow, 2, objPOC.PupilFullName);
                if (!isStudying)
                {
                    sheetTrang2.GetRange(startRow, 1, startRow, 8).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                //Ngay sinh
                sheetTrang2.SetCellValue(startRow, 3, objPOC.Birthday.ToString("dd/MM/yyyy"));
                //Noi sinh
                sheetTrang2.SetCellValue(startRow, 4, objPOC.BirthPlace);
                //Gioi tinh
                sheetTrang2.SetCellValue(startRow, 5, (objPOC.Genre == 1 ? "Nam" : "Nữ"));
                //Dan toc
                sheetTrang2.SetCellValue(startRow, 6, objPOC.EthnicName);
                //Doi tuong chinh sach
                PolicyTargetName = string.Empty;
                if (objPOC.PolicyTargetID.HasValue && arrPolicyID.Contains(objPOC.PolicyTargetID.Value))
                {
                    PolicyTargetName = objPOC.PolicyTargetName;
                }
                sheetTrang2.SetCellValue(startRow, 7, PolicyTargetName);
                //Dia chi gia dinh
                sheetTrang2.SetCellValue(startRow, 8, !string.IsNullOrEmpty(objPOC.TempReidentalAddress) ? objPOC.TempReidentalAddress : objPOC.PermanentResidentalAddress);
                #endregion
                #region fill sheet 3
                string strFatherName = string.Empty;
                if (!isStudying)
                {
                    sheetTrang3.GetRange(startRow + 1, 1, startRow + 1, 8).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                if (string.IsNullOrEmpty(objPOC.FatherFullName) && string.IsNullOrEmpty(objPOC.MotherFullName))
                {
                    strFatherName = (!string.IsNullOrEmpty(objPOC.SponserFullName) ? (objPOC.SponserFullName + ((!string.IsNullOrEmpty(objPOC.SponserJobName)) ? " - " : "")) : "") + objPOC.SponserJobName;
                }
                else
                {
                    strFatherName = (!string.IsNullOrEmpty(objPOC.FatherFullName) ? (objPOC.FatherFullName + ((!string.IsNullOrEmpty(objPOC.FatherJobName)) ? " - " : "")) : "") + objPOC.FatherJobName;
                }
                //Ho ten cha-Nghe nghiep
                sheetTrang3.SetCellValue(startRow + 1, 2, strFatherName);
                //Ho ten me-Nge nghiep
                sheetTrang3.SetCellValue(startRow + 1, 3, (!string.IsNullOrEmpty(objPOC.MotherFullName) ? (objPOC.MotherFullName + ((!string.IsNullOrEmpty(objPOC.MotherJobName)) ? " - " : "")) : "") + objPOC.MotherJobName);
                //So ngay nghi hoc trong nam
                countP = lstPA.Where(p => p.PupilID == pupilID && p.IsAccepted == true).Count();
                sheetTrang3.SetCellValue(startRow + 1, 5, countP > 0 ? countP.ToString() : "");
                countK = lstPA.Where(p => p.PupilID == pupilID && p.IsAccepted == false).Count();
                sheetTrang3.SetCellValue(startRow + 1, 6, countK > 0 ? countK.ToString() : "");
                //Ket qua hoc tap,ren luyen

                sheetTrang3.SetCellValue(startRow + 1, 7, lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && (p.RateAndYear.HasValue && p.RateAndYear.Value == 1 || (p.RateAdd.HasValue && p.RateAdd.Value == 1))).Count() > 0 ? "x" : "");
                sheetTrang3.SetCellValue(startRow + 1, 8, lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && (p.RateAndYear.HasValue && p.RateAndYear.Value == 2 && (p.RateAdd.HasValue && p.RateAdd.Value == 2))).Count() > 0 ? "x" : "");
                #endregion
                #region fill sheet 4
                sheetTrang4.SetCellValue(startRow + 1, 1, i + 1);
                sheetTrang4.SetCellValue(startRow + 1, 2, objPOC.PupilFullName);
                List<ClassSubjectBO> lsttmp = lstResultSubject.Where(p => p.SectionPerWeekFirstSemester > 0).ToList();
                if (Entity.Year < 2016)
                {
                    sheetTrang4.SetCellValue(3, 3, "Điểm bài kiểm tra cuối học kỳ I");
                }
                if (!isStudying)
                {
                    sheetTrang4.GetRange(startRow + 1, 1, startRow + 1, lsttmp.Count + 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                int startColumn = 3;
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    objCS = lsttmp[j];
                    if (check)
                    {
                        subjectName = objCS.SubjectName;
                        if (objCS.IsForeignLanguage)
                        {
                            subjectName = "Ngoại ngữ \n\r ("+ objCS.SubjectName +")";
                            sheetTrang4.SetColumnWidth(startColumn, 11);
                        }
                        sheetTrang4.SetCellValue(startRow, startColumn, subjectName);    
                    }
                    
                    if (objCS.IsVNEN.HasValue && objCS.IsVNEN.Value)
                    {
                        objTeacherNote = lstTeacherNoteBookSemester.Where(p => p.PupilID == pupilID && p.SubjectID == objCS.SubjectID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                        if (objTeacherNote != null)
                        {
                            if (Entity.Year < 2016)
                            {
                                sheetTrang4.SetCellValue(startRow + 1, startColumn, objCS.IsCommenting == 0 ? (objTeacherNote.PERIODIC_SCORE_END.HasValue ? (objTeacherNote.PERIODIC_SCORE_END.Value == 10 ? "10" : objTeacherNote.PERIODIC_SCORE_END.Value.ToString("0.0")) : "") : objTeacherNote.PERIODIC_SCORE_END_JUDGLE);    
                            }
                            else
                            {
                                sheetTrang4.SetCellValue(startRow + 1, startColumn, objCS.IsCommenting == 0 ? (objTeacherNote.AVERAGE_MARK.HasValue ? (objTeacherNote.AVERAGE_MARK.Value == 10 ? "10" : objTeacherNote.AVERAGE_MARK.Value.ToString("0.0")) : "") : objTeacherNote.AVERAGE_MARK_JUDGE);
                            }
                            
                        }
                    }
                    else
                    {
                        objSur = lstSur.Where(p => p.PupilID == pupilID && p.SubjectID == objCS.SubjectID && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                        if (objSur != null)
                        {
                            sheetTrang4.SetCellValue(startRow + 1, startColumn, objCS.IsCommenting == 0 ? (objSur.SummedUpMark.HasValue ? (objSur.SummedUpMark.Value == 10 ? "10" : objSur.SummedUpMark.Value.ToString("0.0")) : "") : objSur.JudgementResult);
                        }
                    }
                    startColumn++;
                }
                #endregion
                #region fill sheet 5
                objReviewBook = lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                if (i < SkipPupil)
                {
                    sheetTrang5.SetCellValue(startRow, 2, objPOC.PupilFullName);
                    if (!isStudying)
                    {
                        sheetTrang5.GetRange(startRow, 1, startRow, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }
                    if (objReviewBook != null)
                    {
                        sheetTrang5.SetCellValue(startRow, 3, objReviewBook.CQComment);
                        sheetTrang5.SetCellValue(startRow, 4, objReviewBook.CommentAdd);
                    }
                }
                #endregion
                #region fill sheet 6
                if (i >= SkipPupil && i < SkipPupil + 10)
                {
                    sheetTrang6.SetCellValue(start6, 2, objPOC.PupilFullName);
                    if (!isStudying)
                    {
                        sheetTrang6.GetRange(start6, 1, start6, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }
                    if (objReviewBook != null)
                    {
                        sheetTrang6.SetCellValue(start6, 3, objReviewBook.CQComment);
                        sheetTrang6.SetCellValue(start6, 4, objReviewBook.CommentAdd);
                    }
                    start6++;
                }
                #endregion
                #region fill sheet 7
                if (i >= SkipPupil + 10 && i < SkipPupil + 20)
                {
                    sheetTrang7.SetCellValue(start7, 2, objPOC.PupilFullName);
                    if (!isStudying)
                    {
                        sheetTrang7.GetRange(start7, 1, start7, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }
                    if (objReviewBook != null)
                    {
                        sheetTrang7.SetCellValue(start7, 3, objReviewBook.CQComment);
                        sheetTrang7.SetCellValue(start7, 4, objReviewBook.CommentAdd);
                    }
                    start7++;
                }
                #endregion
                #region fill sheet 8
                if (i >= SkipPupil + 20 && i < SkipPupil + 30)
                {
                    sheetTrang8.SetCellValue(start8, 2, objPOC.PupilFullName);
                    if (!isStudying)
                    {
                        sheetTrang8.GetRange(start8, 1, start8, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }
                    if (objReviewBook != null)
                    {
                        sheetTrang8.SetCellValue(start8, 3, objReviewBook.CQComment);
                        sheetTrang8.SetCellValue(start8, 4, objReviewBook.CommentAdd);
                    }
                    start8++;
                }
                #endregion
                #region fill sheet 9
                if (i >= SkipPupil + 30 && i < SkipPupil + 40)
                {
                    sheetTrang9.SetCellValue(start9, 2, objPOC.PupilFullName);
                    if (!isStudying)
                    {
                        sheetTrang9.GetRange(start9, 1, start9, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }
                    if (objReviewBook != null)
                    {
                        sheetTrang9.SetCellValue(start9, 3, objReviewBook.CQComment);
                        sheetTrang9.SetCellValue(start9, 4, objReviewBook.CommentAdd);
                    }
                    start9++;
                }
                #endregion
                if (Entity.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    #region fill sheet 10
                    sheetTrang10.SetCellValue(startRow + 1, 1, i + 1);
                    sheetTrang10.SetCellValue(startRow + 1, 2, objPOC.PupilFullName);
                    if (Entity.Year < 2016)
                    {
                        sheetTrang10.SetCellValue(3, 3, "Điểm bài kiểm tra cuối năm");
                    }
                    if (!isStudying)
                    {
                        sheetTrang10.GetRange(startRow + 1, 1, startRow + 1, lstResultSubject.Count + 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }
                    startColumn = 3;
                    for (int j = 0; j < lstResultSubject.Count; j++)
                    {
                        objCS = lstResultSubject[j];
                        if (check)
                        {
                            subjectName = objCS.SubjectName;
                            if (objCS.IsForeignLanguage)
                            {
                                subjectName = "Ngoại ngữ \n\r (" + objCS.SubjectName + ")";
                                sheetTrang10.SetColumnWidth(startColumn, 11);
                            }
                            sheetTrang10.SetCellValue(startRow, startColumn, subjectName);
                        }
                        
                        if (objCS.IsVNEN.HasValue && objCS.IsVNEN.Value)
                        {
                            objTeacherNote = lstTeacherNoteBookSemester.Where(p => p.PupilID == pupilID && p.SubjectID == objCS.SubjectID && p.SemesterID == (Entity.Year < 2016 ? SystemParamsInFile.SEMESTER_OF_YEAR_SECOND : SystemParamsInFile.SEMESTER_OF_YEAR_ALL)).FirstOrDefault();
                            if (objTeacherNote != null)
                            {
                                if (Entity.Year < 2016)
                                {
                                    sheetTrang10.SetCellValue(startRow + 1, startColumn, objCS.IsCommenting == 0 ? (objTeacherNote.PERIODIC_SCORE_END.HasValue ? objTeacherNote.PERIODIC_SCORE_END.Value.ToString("0.0") : "") : objTeacherNote.PERIODIC_SCORE_END_JUDGLE);
                                }
                                else
                                {
                                    sheetTrang10.SetCellValue(startRow + 1, startColumn, objCS.IsCommenting == 0 ? (objTeacherNote.AVERAGE_MARK.HasValue ? objTeacherNote.AVERAGE_MARK.Value.ToString("0.0") : "") : objTeacherNote.AVERAGE_MARK_JUDGE);
                                }
                                
                            }
                        }
                        else
                        {
                            objSur = lstSur.Where(p => p.PupilID == pupilID && p.SubjectID == objCS.SubjectID && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                            if (objSur != null)
                            {
                                sheetTrang10.SetCellValue(startRow + 1, startColumn, objCS.IsCommenting == 0 ? (objSur.SummedUpMark.HasValue ? (objSur.SummedUpMark.Value == 10 ? "10" : objSur.SummedUpMark.Value.ToString("0.0")) : "") : objSur.JudgementResult);
                            }
                        }
                        startColumn++;
                    }
                    #endregion
                    #region fill sheet 11
                    objReviewBook = lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                    if (i < SkipPupil)
                    {
                        sheetTrang11.SetCellValue(start11, 2, objPOC.PupilFullName);
                        if (!isStudying)
                        {
                            sheetTrang11.GetRange(start11, 1, start11, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        if (objReviewBook != null)
                        {
                            sheetTrang11.SetCellValue(start11, 3, objReviewBook.CQComment);
                            sheetTrang11.SetCellValue(start11, 4, objReviewBook.CommentAdd);
                        }
                        start11++;
                    }
                    #endregion
                    #region fill sheet 12
                    if (i >= SkipPupil && i < SkipPupil + 10)
                    {
                        sheetTrang12.SetCellValue(start12, 2, objPOC.PupilFullName);
                        if (!isStudying)
                        {
                            sheetTrang12.GetRange(start12, 1, start12, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        if (objReviewBook != null)
                        {
                            sheetTrang12.SetCellValue(start12, 3, objReviewBook.CQComment);
                            sheetTrang12.SetCellValue(start12, 4, objReviewBook.CommentAdd);
                        }
                        start12++;
                    }
                    #endregion
                    #region fill sheet 13
                    if (i >= SkipPupil + 10 && i < SkipPupil + 20)
                    {
                        sheetTrang13.SetCellValue(start13, 2, objPOC.PupilFullName);
                        if (!isStudying)
                        {
                            sheetTrang13.GetRange(start13, 1, start13, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        if (objReviewBook != null)
                        {
                            sheetTrang13.SetCellValue(start13, 3, objReviewBook.CQComment);
                            sheetTrang13.SetCellValue(start13, 4, objReviewBook.CommentAdd);
                        }
                        start13++;
                    }
                    #endregion
                    #region fill sheet 14
                    if (i >= SkipPupil + 20 && i < SkipPupil + 30)
                    {
                        sheetTrang14.SetCellValue(start14, 2, objPOC.PupilFullName);
                        if (!isStudying)
                        {
                            sheetTrang14.GetRange(start14, 1, start14, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        if (objReviewBook != null)
                        {
                            sheetTrang14.SetCellValue(start14, 3, objReviewBook.CQComment);
                            sheetTrang14.SetCellValue(start14, 4, objReviewBook.CommentAdd);
                        }
                        start14++;
                    }
                    #endregion
                    #region fill sheet 15
                    if (i >= SkipPupil + 30 && i < SkipPupil + 40)
                    {
                        sheetTrang15.SetCellValue(start15, 2, objPOC.PupilFullName);
                        if (!isStudying)
                        {
                            sheetTrang15.GetRange(start15, 1, start15, 4).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                        }
                        if (objReviewBook != null)
                        {
                            sheetTrang15.SetCellValue(start15, 3, objReviewBook.CQComment);
                            sheetTrang15.SetCellValue(start15, 4, objReviewBook.CommentAdd);
                        }
                        start15++;
                    }
                    #endregion
                }
                #region fill sheet 16
                sheetTrang16.SetCellValue(startRow + 1, 2, objPOC.PupilFullName);
                if (!isStudying)
                {
                    sheetTrang16.GetRange(startRow + 1, 1, startRow + 1, 10).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                //xep loai
                objReviewBook = lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                if (objReviewBook != null)
                {
                    //nang luc
                    sheetTrang16.SetCellValue(startRow + 1, 3, objReviewBook.CapacityRate.HasValue ? (objReviewBook.CapacityRate.Value == 1 ? "T" : objReviewBook.CapacityRate.Value == 2 ? "Đ" : "C") : "");
                    //pham chat
                    sheetTrang16.SetCellValue(startRow + 1, 4, objReviewBook.QualityRate.HasValue ? (objReviewBook.QualityRate.Value == 1 ? "T" : objReviewBook.QualityRate.Value == 2 ? "Đ" : "C") : "");

                    //xep loai sau khi hoc tap ren luyen trong he
                    //nang luc
                    sheetTrang16.SetCellValue(startRow + 1, 8, objReviewBook.CapacitySummer.HasValue ? (objReviewBook.CapacitySummer.Value == 1 ? "T" : objReviewBook.CapacitySummer.Value == 2 ? "Đ" : "C") : "");
                    //pham chat
                    sheetTrang16.SetCellValue(startRow + 1, 9, objReviewBook.QualitySummer.HasValue ? (objReviewBook.QualitySummer.Value == 1 ? "T" : objReviewBook.QualitySummer.Value == 2 ? "Đ" : "C") : "");
                }
                //so ngay nghi hoc
                totalAbsence = lstPA.Where(p => p.PupilID == pupilID).Count();
                sheetTrang16.SetCellValue(startRow + 1, 5, totalAbsence > 0 ? totalAbsence.ToString() : "");
                //duoc len lop
                countUpClass = lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && (p.RateAndYear.HasValue && p.RateAndYear.Value == 1 || (p.RateAdd.HasValue && p.RateAdd.Value == 1))).Count();
                sheetTrang16.SetCellValue(startRow + 1, 6, countUpClass > 0 ? "x" : "");
                totalUpClass += countUpClass;
                //khong duoc len lop
                countNotUpClass = lstReviewBook.Where(p => p.PupilID == pupilID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && (p.RateAndYear.HasValue && p.RateAndYear.Value == 2 && (p.RateAdd.HasValue && p.RateAdd.Value == 2))).Count();
                sheetTrang16.SetCellValue(startRow + 1, 7, countNotUpClass > 0 ? "x" : "");
                totalNotUpClass += countNotUpClass;
                //xep loai hoc tap sau khi ren luyen trong he

                //Danh hieu thi dua
                lstUpdateRewardTMP = lstUpdateReward.Where(p => p.PupilID == objPOC.PupilID).ToList();
                strReward = string.Empty;
                if (lstUpdateRewardTMP.Count > 0)
                {
                    for (int j = 0; j < lstUpdateRewardTMP.Count; j++)
                    {
                        objUpdateReward = lstUpdateRewardTMP[j];
                        lstRewardMod = objUpdateReward.Rewards.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                        for (int k = 0; k < lstRewardMod.Count; k++)
                        {
                            strReward += dicRewardFinal[lstRewardMod[k]] + ((k < lstRewardMod.Count - 1) ? ", " : "");
                        }
                    }
                }
                sheetTrang16.SetCellValue(startRow + 1, 10, strReward);
                #endregion
                #region fill sheet 17
                #endregion
                
                check = false;
                startRow++;
            }
            //Tong hop chung
            countUpClassSummer = lstReviewBook.Where(p => p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && (p.RateAdd.HasValue && p.RateAdd.Value == 1)).Count();
            dicSheet = new Dictionary<string, object>()
                    {
                        {"TotalPupil",lstPOC.Count},
                        {"CountUpClass",totalUpClass},
                        {"CountUpClassSummer",countUpClassSummer},
                        {"CountNotUpClass",totalNotUpClass},
                        {"HeadTeacherName",objCP.HeadTeacherName}
                    };
            sheetTrang16.FillVariableValue(dicSheet);

            int countSubjectSemesterI = lstResultSubject.Where(p=>p.SectionPerWeekFirstSemester > 0).Count();
            if (countSubjectSemesterI > 10)
            {
                //ke khung sheet 4
                sheetTrang4.GetRange(3, 3, 3, countSubjectSemesterI + 2).Merge();
                sheetTrang4.GetRange(2, 1, 2, countSubjectSemesterI + 2).Merge();
                sheetTrang4.GetRange(3, 3, 3, countSubjectSemesterI + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                if (lstPOC.Count + 4 > 50)
                {
                    sheetTrang4.GetRange(3, 1, lstPOC.Count + 4, countSubjectSemesterI + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                }
                else
                {
                    sheetTrang4.GetRange(3, 1, 54, countSubjectSemesterI + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                }
            }

            if (lstResultSubject.Count > 10)
            {
                if (lstPOC.Count + 4 > 50)
                {
                    sheetTrang10.GetRange(3, 1, lstPOC.Count + 4, lstResultSubject.Count + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                }
                else
                {
                    sheetTrang10.GetRange(3, 1, 54, lstResultSubject.Count + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                }

                //ke khung sheet 10
                sheetTrang10.GetRange(3, 3, 3, lstResultSubject.Count + 2).Merge();
                sheetTrang10.GetRange(2, 1, 2, lstResultSubject.Count + 2).Merge();
                sheetTrang10.GetRange(3, 3, 3, lstResultSubject.Count + 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            }
            #endregion
        }
        private List<int> GetSectionsOfClass(ClassProfile cp)
        {
            List<int> sectionOfClass = new List<int>(5);
            if (cp.Section.HasValue)
            {
                if ((cp.Section.Value & 1) == 1)
                {
                    sectionOfClass.Add(1);
                }
                if ((cp.Section.Value & 2) == 2)
                {
                    sectionOfClass.Add(2);
                }
                if ((cp.Section.Value & 4) == 4)
                {
                    sectionOfClass.Add(3);
                }
            }
            return sectionOfClass;
        }
        private string CutSchoolName(string schoolName)
        {
            string result = string.Empty;
            string[] arrName = schoolName.Split(' ');
            if (arrName[0].ToUpper().Contains("TRƯỜNG"))
            {
                for (int i = 1; i < arrName.Length; i++)
                {
                    result = result + arrName[i] + " ";
                }
            }
            else
            {
                result = schoolName;
            }

            return result.Trim();
        }

        private string CutClassName(string className)
        {
            string result = string.Empty;
            string[] arrName = className.Split(' ');
            if (arrName[0].ToUpper().Contains("LỚP"))
            {
                for (int i = 1; i < arrName.Length; i++)
                {
                    result = result + arrName[i] + " ";
                }
            }
            else
            {
                result = className;
            }

            return result.Trim();
        }
        private class ClassProfiletmp
        {
            public int ClassID { get; set; }
            public int EducationLevelID { get; set; }
            public string DisplayName { get; set; }
            public string HeadTeacherName { get; set; }
            public int? SectionKeyID { get; set; }
        }
    }
}