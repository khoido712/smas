﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class PackageBusiness
    {
        #region Validate
        public void CheckPackageIDAvailable(int PackageID)
        {
            bool AvailablePackageID = this.repository.ExistsRow(GlobalConstants.TRS_SCHEMA, "Package",
                   new Dictionary<string, object>()
                {
                    {"PackageID", PackageID}
                    
                }, null);
            if (!AvailablePackageID)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion
        #region Insert
        public Package Insert(Package Package)
        {

            //Check Require, Max Length
            ValidationMetadata.ValidateObject(Package);

            return base.Insert(Package);

        }
        #endregion
        #region Update
        public Package Update(Package Package)
        {
            //Check Require, Max Length
            ValidationMetadata.ValidateObject(Package);

            return base.Update(Package);
        }
        #endregion
        #region Delete
        public bool Delete(int PackageID)
        {
            try
            {
                //Check available
                CheckPackageIDAvailable(PackageID);

                //Check Using
                this.CheckConstraints(GlobalConstants.TRS_SCHEMA, "Package", PackageID, "Package_Label_PackageID");

                base.Delete(PackageID);
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Thông báo
        /// </summary>
        /// <author>BaLX</author>
        /// <date>7/12/2012</date>
        /// <param name="dic">Tiêu chí tìm kiếm</param>
        /// <returns>Đối tượng Package</returns>

        public IQueryable<Package> Search(IDictionary<string, object> dic)
        {
            string PackageCode = Utils.GetString(dic, "PackageCode");
            string PackageName = Utils.GetString(dic, "PackageName");
            int? UnitPrice = Utils.GetNullableInt(dic, "UnitPrice");
            string Description = Utils.GetString(dic, "Description");
            bool? IsStatus = Utils.GetNullableBool(dic, "IsStatus");
            string createuser = Utils.GetString(dic, "create_user");
            string updateuser = Utils.GetString(dic, "update_user");
            DateTime? createtime = Utils.GetDateTime(dic, "create_time");
            DateTime? updatetime = Utils.GetDateTime(dic, "update_time");

            IQueryable<Package> listPackage = this.PackageRepository.All;
            //if (!PackageCode.Equals(string.Empty))
            //{
            //    listPackage = listPackage.Where(x => x.PackageCode.ToLower().Contains(PackageCode.ToLower()));
            //}
            //if (!PackageName.Equals(string.Empty))
            //{
            //    listPackage = listPackage.Where(x => x.PackageName.ToLower().Contains(PackageName.ToLower()));
            //}
            //if (UnitPrice.HasValue && UnitPrice != -1)
            //{
            //    listPackage = listPackage.Where(x => x.UnitPrice == UnitPrice);
            //}
            //if (!Description.Equals(string.Empty))
            //{
            //    listPackage = listPackage.Where(x => x.Description.ToLower().Contains(Description.ToLower()));
            //}
            //if (IsStatus.HasValue)
            //{
            //    listPackage = listPackage.Where(x => x.IsStatus == IsStatus);
            //}
            if (!createuser.Equals(string.Empty))
            {
                listPackage = listPackage.Where(x => x.CreateUser.ToLower().Contains(createuser.ToLower()));
            }
            if (!updateuser.Equals(string.Empty))
            {
                listPackage = listPackage.Where(x => x.UpdateUser.ToLower().Contains(updateuser.ToLower()));
            }
            if (createtime.HasValue)
            {
                listPackage = listPackage.Where(x => x.CreateTime == createtime);
            }
            if (updatetime.HasValue)
            {
                listPackage = listPackage.Where(x => x.UpdateTime == updatetime);
            }

            return listPackage;
        }
        #endregion
        
    }
}