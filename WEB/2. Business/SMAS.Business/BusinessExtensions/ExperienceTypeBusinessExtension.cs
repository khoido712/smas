﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author dungnt 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Sáng kiến kinh nghiệm
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class ExperienceTypeBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion


        #region insert
        /// <summary>
        /// Thêm mới Sáng kiến kinh nghiệm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertExperienceType">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override ExperienceType Insert(ExperienceType insertExperienceType)
        {


            //resolution rong
            Utils.ValidateRequire(insertExperienceType.Resolution, "ExperienceType_Label_Resolution");
            Utils.ValidateMaxLength(insertExperienceType.Resolution, RESOLUTION_MAX_LENGTH, "ExperienceType_Label_Resolution");
            Utils.ValidateMaxLength(insertExperienceType.Description, DESCRIPTION_MAX_LENGTH, "ExperienceType_Column_Description");

            //kiem tra ton tai - theo cap 
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = insertExperienceType.SchoolID;
            SearchInfo["Resolution"] = insertExperienceType.Resolution;
            SearchInfo["IsActive"] = true;

            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["ExperienceTypeID"] = insertExperienceType.ExperienceTypeID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ExperienceType", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("ExperienceType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            SchoolProfileBusiness.CheckAvailable(insertExperienceType.SchoolID, "UserInfo_SchoolID");
           return base.Insert(insertExperienceType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Sáng kiến kinh nghiệm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateExperienceType"> Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override ExperienceType Update(ExperienceType updateExperienceType)
        {

            //check avai
            new ExperienceTypeBusiness(null).CheckAvailable(updateExperienceType.ExperienceTypeID, "ExperienceType_Label_ExperienceTypeID");
            //resolution rong
            Utils.ValidateRequire(updateExperienceType.Resolution, "ExperienceType_Label_Resolution");
            Utils.ValidateMaxLength(updateExperienceType.Resolution, RESOLUTION_MAX_LENGTH, "ExperienceType_Label_Resolution");
            Utils.ValidateMaxLength(updateExperienceType.Description, DESCRIPTION_MAX_LENGTH, "ExperienceType_Column_Description");
            //kiem tra ton tai - theo cap - can lam lai
             //this.CheckDuplicate(updateExperienceType.Resolution, GlobalConstants.LIST_SCHEMA, "ExperienceType", "Resolution", true, updateExperienceType.ExperienceTypeID, "ExperienceType_Label_Resolution");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = updateExperienceType.SchoolID;
            SearchInfo["Resolution"] = updateExperienceType.Resolution;
            SearchInfo["IsActive"] = true;
            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["ExperienceTypeID"] = updateExperienceType.ExperienceTypeID;
            bool duplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ExperienceType", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("ExperienceType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }        
            //check 
             SchoolProfileBusiness.CheckAvailable(updateExperienceType.SchoolID, "UserInfo_SchoolID");
            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu ExperienceTypeID ->vao csdl lay ExperienceType tuong ung roi 
            //so sanh voi updateExperienceType.SchoolID
            //.................
           return base.Update(updateExperienceType);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Sáng kiến kinh nghiệm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="ExperienceTypeId">ID Sáng kiến kinh nghiệm</param>
        public  void Delete(int ExperienceTypeId)
        {
            //check avai
            new ExperienceTypeBusiness(null).CheckAvailable(ExperienceTypeId, "ExperienceType_Label_ExperienceTypeID");

            //da su dung hay chua
           // this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "ExperienceType",ExperienceTypeId, "ExperienceType_Label_ExperienceTypeID");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu ExperienceTypeID ->vao csdl lay ExperienceType tuong ung roi 
            //so sanh voi updateExperienceType.SchoolID


            //.................

            base.Delete(ExperienceTypeId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Sáng kiến kinh nghiệm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">cac thuoc tinh can tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<ExperienceType> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");
            int SchoolID =Utils.GetInt(dic,"SchoolID");
            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<ExperienceType> lsExperienceType = this.ExperienceTypeRepository.All;

          


             if (isActive.HasValue)
            {
            lsExperienceType = lsExperienceType.Where(em => (em.IsActive == isActive));
            }

            

            if (resolution.Trim().Length != 0)
            {

                lsExperienceType = lsExperienceType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (SchoolID != 0)
            {
                lsExperienceType = lsExperienceType.Where(em => (em.SchoolID == SchoolID));
            }

            if (description.Trim().Length != 0)
            {

                lsExperienceType = lsExperienceType.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

          
            return lsExperienceType;
        }
        #endregion
    }
}