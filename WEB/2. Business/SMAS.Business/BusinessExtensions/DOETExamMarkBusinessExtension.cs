/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Text;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class DOETExamMarkBusiness
    {
        public void InsertOrUpdateMark(List<DOETExamMarkBO> lstExamMark,IDictionary<string,object> dic)
        {
            try
            {
                int subjectID = Utils.GetInt(dic, "SubjectID");
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                DOETExamMarkBO objExamMarkBO = null;
                List<int> lstExamPupilID = lstExamMark.Select(p => p.ExamPupilID).ToList();
                List<DOETExamMark> lstExamMarkDB = DOETExamMarkBusiness.All.Where(p => lstExamPupilID.Contains(p.ExamPupilID) && p.DoetSubjectID == subjectID).ToList();
                List<DOETExamMark> lstInsert = new List<DOETExamMark>();
                List<DOETExamMark> lstUpdate = new List<DOETExamMark>();
                DOETExamMark objDB = null;
                DOETExamMark objInsert = null;
                for (int i = 0; i < lstExamMark.Count; i++)
                {
                    objExamMarkBO = lstExamMark[i];
                    objDB = lstExamMarkDB.Where(p => p.ExamPupilID == objExamMarkBO.ExamPupilID).FirstOrDefault();
                    if (objDB != null)
                    {
                        objDB.Mark = objExamMarkBO.Mark;
                        objDB.UpdateTime = DateTime.Now.Date;
                        lstUpdate.Add(objDB);
                    }
                    else
                    {
                        objInsert = new DOETExamMark();
                        objInsert.ExamPupilID = objExamMarkBO.ExamPupilID;
                        objInsert.DoetSubjectID = objExamMarkBO.DoetSubjectID;
                        objInsert.Mark = objExamMarkBO.Mark;
                        objInsert.CreateTime = DateTime.Now.Date;
                        lstInsert.Add(objInsert);
                    }
                }
                if (lstUpdate.Count > 0)
                {
                    for (int i = 0; i < lstUpdate.Count; i++)
                    {
                        this.Update(lstUpdate[i]);
                    }
                    this.Save();
                }
                if (lstInsert.Count > 0)
                {
                    this.BulkInsert(lstInsert, DOETExamMarkMapping(), "ExamMarkID");    
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateMark", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        private IDictionary<string, object> DOETExamMarkMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();

            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("DoetSubjectID", "DOET_SUBJECT_ID");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }
    }
}