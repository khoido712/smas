﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Nhóm lỗi vi phạm
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class FaultGroupBusiness
    {
        #region insert
        /// <summary>
        /// Thêm mới Nhóm lỗi vi phạm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="Entity">doi tuong can them moi</param>
        /// <returns></returns>
        public override FaultGroup Insert(FaultGroup Entity)
        {
            ValidationMetadata.ValidateObject(Entity);
        
            //check trùng tên
            if (this.FaultGroupRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "FaultGroup", new Dictionary<string, object> {
                {"Resolution", Entity.Resolution},
                {"IsActive", true},
                {"SchoolID", Entity.SchoolID}
            }))
            {
                throw new BusinessException("Common_Validate_Duplicate", "FaultGroup_Label_Resolution");
            }
            Entity.IsActive = true;
            return base.Insert(Entity);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Nhóm lỗi vi phạm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="Entity">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override FaultGroup Update(FaultGroup Entity)
        {
            //check avai
            this.CheckAvailable(Entity.FaultGroupID, "FaultGroup_Label_FaultGroupID");
            ValidationMetadata.ValidateObject(Entity);
            //check trùng tên
            if (this.FaultGroupRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "FaultGroup", new Dictionary<string, object> {
                {"Resolution", Entity.Resolution},
                {"IsActive", true},
                {"SchoolID", Entity.SchoolID}
            }, new Dictionary<string, object> {
                {"FaultGroupID", Entity.FaultGroupID}
            }))
            {
                throw new BusinessException("Common_Validate_Duplicate", "FaultGroup_Label_Resolution");
            }
            FaultGroup fc = this.Find(Entity.FaultGroupID);
            if (fc.SchoolID != Entity.SchoolID)
            {
                throw new Exception("Common_Error_NotCompatible");
            }
            Entity.IsActive = true;
            return base.Update(Entity);
        }
        #endregion

        #region Delete
        /// <summary>
        ///Xóa Nhóm lỗi vi phạm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="FaultGroupID">id doi tuong can xoa</param>
        public void Delete(int SchoolID, int FaultGroupID)
        {
            //check avai
            this.CheckAvailable(FaultGroupID, "FaultGroup_Label_FaultGroupID");
            FaultGroup fc = this.Find(FaultGroupID);
            if (fc.SchoolID != SchoolID)
            {
                throw new Exception("Common_Error_NotCompatible");
            }
            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "FaultGroup", FaultGroupID, "FaultGroup_Label_FaultGroupID");
            base.Delete(FaultGroupID, true);
        }
        #endregion

        #region Search
        /// <summary>
        /// hTìm kiếm Nhóm lỗi vi phạm
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<FaultGroup> Search(IDictionary<string, object> dic = null)
        {
            if (dic == null) dic = new Dictionary<string, object>();
            //hieund - 04/10/2012 : bo check schoolID
            string resolution = Utils.GetString(dic, "Resolution");
            string description = Utils.GetString(dic, "Description");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<FaultGroup> lsFaultGroup = this.FaultGroupRepository.All;
            if (isActive.HasValue)
            {
                lsFaultGroup = lsFaultGroup.Where(em => (em.IsActive == isActive));
            }
            if (resolution.Trim().Length != 0)
            {
                lsFaultGroup = lsFaultGroup.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }
            if (description.Trim().Length != 0)
            {
                lsFaultGroup = lsFaultGroup.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }
            if (SchoolID != 0)
            {
                lsFaultGroup = lsFaultGroup.Where(em => (em.SchoolID == SchoolID));
            }
            return lsFaultGroup;
        }

        //HieuND 04/10/2012 : Comment SearchBySchool
        ///// <summary>
        /////Tìm kiếm Nhóm lỗi vi phạm theo truong
        ///// <author>dungnt</author>
        ///// <date>05/09/2012</date>
        ///// </summary>
        ///// <param name="SchoolID">id truong</param>
        ///// <param name="dic">tham so</param>
        ///// <returns></returns>
        public IQueryable<FaultGroup> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (dic == null) dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        #endregion

    }
}