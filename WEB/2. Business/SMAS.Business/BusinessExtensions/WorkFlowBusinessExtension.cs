﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{

    public partial class WorkFlowBusiness
    {
        #region Search

        public IQueryable<WorkFlow> Search(IDictionary<string, object> dic)
        {

            int workFlowId = Utils.GetInt(dic, "WorkFlowID");
            string workFlowName = Utils.GetString(dic, "WorkFlowName");            
            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<WorkFlow> lsWorkFLow = this.WorkFlowRepository.All;


            if (workFlowId != 0)
            {
                lsWorkFLow = lsWorkFLow.Where(o => o.WorkFlowID == workFlowId);
            }

            if(!String.IsNullOrEmpty(workFlowName))
            {
                lsWorkFLow = lsWorkFLow.Where(em => (em.WorkFlowName.ToUpper().Contains(workFlowName.ToUpper())));
            }
            /*if (isActive.HasValue)
            {
            lsWorkFLow = lsWorkFLow.Where(em => (em.IsActive == isActive));
            }*/
            return lsWorkFLow;
        }
        #endregion

    }
}