﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;
namespace SMAS.Business.Business
{
    public partial class SynthesisCapacityConductBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// minhh - 28/11/2012
        ///Lấy mảng băm cho các tham số đầu vào 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(SynthesisCapacityConduct entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin thống kê học lực hạnh kiểm
        /// <summary>
        /// minhh - 28/11/2012
        /// Lưu lại thông tin 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertSynthesisCapacityConduct(SynthesisCapacityConduct entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy bảng thống kê học lực hạnh kiểm được cập nhật mới nhất
        /// <summary>
        /// <author>minhh</author>
        /// Lấy bảng thống kê xếp hạng hạnh kiểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport GetSynthesisCapacityConduct(SynthesisCapacityConduct entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin thống kê học lực hạnh kiểm
        /// <summary>
        /// <author>minhh</author>
        /// Lưu lại thông tin xếp hạng hoc luc hạnh kiểm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateSynthesisCapacityConduct(SynthesisCapacityConduct entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);

            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);

            string semester = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semester = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semester = "HỌC KỲ II";
            //if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) semester = "CẢ NĂM";

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            int startRow = 11;
            IVTRange range = sheet.GetRange("A49", "U49");
            IVTRange range_Temp = sheet.GetRange("A6", "U10");
            IVTRange rangeBold = sheet.GetRange("A50", "U50");
            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID", null}
            };


            IQueryable<VPupilRanking> lstRankingAllStatus = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dic).Where(o => o.PeriodID == null);
            IQueryable<VPupilRanking> lstPUpilRankingAll;
            if (entity.Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstPUpilRankingAll = lstRankingAllStatus.Where(o => o.Semester == entity.Semester);
            }
            else
            {
                lstPUpilRankingAll = lstRankingAllStatus.Where(u => u.Semester == lstRankingAllStatus.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester));
            }
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel},
                {"FemaleID",entity.FemaleID},
                {"EthnicID",entity.EthnicID}
            };
            IQueryable<PupilOfClassBO> lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                 join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                 join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                                 where (!q.IsVnenClass.HasValue || (q.IsVnenClass.HasValue && q.IsVnenClass.Value == false))
                                                 && q.IsActive.Value
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = p.PupilID,
                                                     EducationLevelID = q.EducationLevelID,
                                                     ClassID = p.ClassID,
                                                     Genre = r.Genre,
                                                     EthnicID = r.EthnicID
                                                 };

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            List<PupilRankingBO> listRanking = (from u in lstPUpilRankingAll
                                               join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                               join r in ClassProfileBusiness.All on u.ClassID equals r.ClassProfileID
                                               where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                               && r.IsActive.Value
                                               select new PupilRankingBO
                                               {
                                                   PupilID = u.PupilID, 
                                                   EducationLevelID = r.EducationLevelID, 
                                                   ClassID = u.ClassID,
                                                   Genre = v.Genre,
                                                   EthnicID = v.EthnicID,
                                                   ConductLevelID = u.ConductLevelID,
                                                   CapacityLevelID = u.CapacityLevelID
                                               }).ToList();
            // Lay danh sach hoc sinh theo khoi
            var listCountPupilByEducation = lstQPoc.GroupBy(o => new { o.EducationLevelID, o.Genre, o.EthnicID}).Select(o => new { EducationLevelID = o.Key.EducationLevelID, Genre = o.Key.Genre, EthnicID = o.Key.EthnicID, TotalPupil = o.Count() }).ToList();

            #region Mẫu 1
            int startmin = startRow;

            string semesterAndAcademic = "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM " + semester + " - NĂM HỌC: " + academicYear.DisplayTitle;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                semesterAndAcademic = "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM "  + " - NĂM HỌC: " + academicYear.DisplayTitle;
            }
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.SetCellValue("O4", provinceAndDate);
            sheet.SetCellValue("C6", semesterAndAcademic);
            //Duyet theo tung khoi
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int countAllPupilOfEducation = 0;
                int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                List<PupilRankingBO> listRanking_Edu = listRanking.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, startRow);

                //STT
                sheet.SetCellValue(startRow, 1, (i + 1));

                //Ten khoi
                sheet.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);

                var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID);
                countAllPupilOfEducation = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                sheet.SetCellValue(startRow, 3, countAllPupilOfEducation);

                #region Fill Du Lieu Hoc Luc
                //Hoc luc gioi
                sheet.SetCellValue(startRow, 4, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
                sheet.SetCellValue(startRow, 5, "=IF(C" + startRow + ">0" + "," + "ROUND(D" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Hoc luc kha
                sheet.SetCellValue(startRow, 6, listRanking_Edu.Where(o =>  o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());
                sheet.SetCellValue(startRow, 7, "=IF(C" + startRow + ">0" + "," + "ROUND(F" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Hoc luc TB
                sheet.SetCellValue(startRow, 8, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
                sheet.SetCellValue(startRow, 9, "=IF(C" + startRow + ">0" + "," + "ROUND(H" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Hoc luc yeu
                sheet.SetCellValue(startRow, 10, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());
                sheet.SetCellValue(startRow, 11, "=IF(C" + startRow + ">0" + "," + "ROUND(J" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Hoc luc kem
                sheet.SetCellValue(startRow, 12, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                sheet.SetCellValue(startRow, 13, "=IF(C" + startRow + ">0" + "," + "ROUND(L" + startRow + "/" + "C" + startRow + "*100,2),0)");
                #endregion

                #region Fill Du lieu Hanh Kiem
                //Dien hanh kiem tot
                List<PupilRankingBO> listRankingOfClass = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClass = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClass = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 14, listRankingOfClass.Count());
                sheet.SetCellValue(startRow, 15, "=IF(C" + startRow + ">0" + "," + "ROUND(N" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Dien hanh kiem kha
                List<PupilRankingBO> listRankingOfClassFair = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 16, listRankingOfClassFair.Count());
                sheet.SetCellValue(startRow, 17, "=IF(C" + startRow + ">0" + "," + "ROUND(P" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Dien hanh kiem TB
                List<PupilRankingBO> listRankingOfClassNormal = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 18, listRankingOfClassNormal.Count());
                sheet.SetCellValue(startRow, 19, "=IF(C" + startRow + ">0" + "," + "ROUND(R" + startRow + "/" + "C" + startRow + "*100,2),0)");

                //Dien hanh kiem Yeu
                List<PupilRankingBO> listRankingOfClassWeek = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 20, listRankingOfClassWeek.Count);
                sheet.SetCellValue(startRow, 21, "=IF(C" + startRow + ">0" + "," + "ROUND(T" + startRow + "/" + "C" + startRow + "*100,2),0)");
                #endregion

                startRow++;
            }


            sheet.CopyPasteSameRowHeigh(rangeBold, startRow);
            sheet.SetCellValue(startRow, 1, "");
            sheet.SetCellValue(startRow, 2, "TS");
            sheet.SetCellValue(startRow, 3, "=SUM(C"+startmin.ToString() +":C"+(startRow-1).ToString()+")");
            sheet.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 5, "=IF(C" + startRow + ">0" + "," + "ROUND(D" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 6, "=SUM(F" + startmin.ToString() + ":F" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 7, "=IF(C" + startRow + ">0" + "," + "ROUND(F" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 8, "=SUM(H" + startmin.ToString() + ":H" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 9, "=IF(C" + startRow + ">0" + "," + "ROUND(H" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 10, "=SUM(J" + startmin.ToString() + ":J" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 11, "=IF(C" + startRow + ">0" + "," + "ROUND(J" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 12, "=SUM(L" + startmin.ToString() + ":L" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 13, "=IF(C" + startRow + ">0" + "," + "ROUND(L" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 14, "=SUM(N" + startmin.ToString() + ":N" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 15, "=IF(C" + startRow + ">0" + "," + "ROUND(N" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 16, "=SUM(P" + startmin.ToString() + ":P" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 17, "=IF(C" + startRow + ">0" + "," + "ROUND(P" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 18, "=SUM(R" + startmin.ToString() + ":R" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 19, "=IF(C" + startRow + ">0" + "," + "ROUND(R" + startRow + "/" + "C" + startRow + "*100,2),0)");
            sheet.SetCellValue(startRow, 20, "=SUM(T" + startmin.ToString() + ":T" + (startRow-1).ToString() + ")");
            sheet.SetCellValue(startRow, 21, "=IF(C" + startRow + ">0" + "," + "ROUND(T" + startRow + "/" + "C" + startRow + "*100,2),0)");
            
            #endregion
            #region Mẫu 2 nữ
            startRow = startRow + 2;
            sheet.CopyPasteSameSize(range_Temp, startRow, 1);
            sheet.SetCellValue(startRow, 2, "Mẫu 2");
            sheet.SetCellValue(startRow, 3, "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM HỌC SINH NỮ " + semester + " - NĂM HỌC: " + academicYear.DisplayTitle);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue(startRow, 3, "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM HỌC SINH NỮ "  + " - NĂM HỌC: " + academicYear.DisplayTitle);
            }
            startRow += 5;
            startmin = startRow;
            //Duyet theo tung khoi
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int countAllPupilOfEducation = 0;
                int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                List<PupilRankingBO> listRanking_Edu = listRanking.Where(o => o.EducationLevelID == EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, startRow);

                //STT
                sheet.SetCellValue(startRow, 1, (i + 1));

                //Ten khoi
                sheet.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);


                var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE);
                countAllPupilOfEducation = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                sheet.SetCellValue(startRow, 3, countAllPupilOfEducation);

                #region Fill Du Lieu Hoc Luc
                //Hoc luc gioi       
                sheet.SetCellValue(startRow, 4, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());

                //Hoc luc kha
                sheet.SetCellValue(startRow, 6, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());

                //Hoc luc TB
                sheet.SetCellValue(startRow, 8, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());

                //Hoc luc yeu
                sheet.SetCellValue(startRow, 10, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());

                //Hoc luc kem
                sheet.SetCellValue(startRow, 12, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                #endregion

                #region Fill Du lieu Hanh Kiem
                //Dien hanh kiem tot
                List<PupilRankingBO> listRankingOfClass = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClass = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClass = listRanking_Edu.Where(o => (o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY)).ToList();
                }
                sheet.SetCellValue(startRow, 14, listRankingOfClass.Count());

                //Dien hanh kiem kha
                List<PupilRankingBO> listRankingOfClassFair = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).ToList();
                }

                sheet.SetCellValue(startRow, 16, listRankingOfClassFair.Count());

                //Dien hanh kiem TB
                List<PupilRankingBO> listRankingOfClassNormal = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 18, listRankingOfClassNormal.Count());

                //Dien hanh kiem Yeu
                List<PupilRankingBO> listRankingOfClassWeek = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).ToList();
                }

                sheet.SetCellValue(startRow, 20, listRankingOfClassWeek.Count);
                #endregion

                startRow++;
            }

            sheet.CopyPasteSameRowHeigh(rangeBold, startRow);
            sheet.SetCellValue(startRow, 1, "");
            sheet.SetCellValue(startRow, 2, "TS");
            sheet.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 6, "=SUM(F" + startmin.ToString() + ":F" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 8, "=SUM(H" + startmin.ToString() + ":H" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 10, "=SUM(J" + startmin.ToString() + ":J" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 12, "=SUM(L" + startmin.ToString() + ":L" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 14, "=SUM(N" + startmin.ToString() + ":N" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 16, "=SUM(P" + startmin.ToString() + ":P" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 18, "=SUM(R" + startmin.ToString() + ":R" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 20, "=SUM(T" + startmin.ToString() + ":T" + (startRow - 1).ToString() + ")");
            #endregion
            #region Mẫu 3 dan tộc
            startRow = startRow + 2;
            sheet.CopyPasteSameSize(range_Temp, startRow, 1);
            sheet.SetCellValue(startRow, 2, "Mẫu 3");
            sheet.SetCellValue(startRow, 3, "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM HỌC SINH DÂN TỘC " + semester + " - NĂM HỌC: " + academicYear.DisplayTitle);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue(startRow, 3, "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM HỌC SINH DÂN TỘC "  + " - NĂM HỌC: " + academicYear.DisplayTitle);
            }
            startRow += 5;
            startmin = startRow;
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            //Duyet theo tung khoi
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int countAllPupilOfEducation = 0;
                int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                List<PupilRankingBO> listRanking_Edu = listRanking.Where(o => o.EducationLevelID == EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, startRow);

                //STT
                sheet.SetCellValue(startRow, 1, (i + 1));

                //Ten khoi
                sheet.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);


                var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople);
                countAllPupilOfEducation = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                sheet.SetCellValue(startRow, 3, countAllPupilOfEducation);

                #region Fill Du Lieu Hoc Luc
                //Hoc luc gioi
                sheet.SetCellValue(startRow, 4, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());
   

                //Hoc luc kha
                sheet.SetCellValue(startRow, 6, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());


                //Hoc luc TB
                sheet.SetCellValue(startRow, 8, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());
          

                //Hoc luc yeu
                sheet.SetCellValue(startRow, 10, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());

                //Hoc luc kem
                sheet.SetCellValue(startRow, 12, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                #endregion

                #region Fill Du lieu Hanh Kiem
                //Dien hanh kiem tot
                List<PupilRankingBO> listRankingOfClass = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClass = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClass = listRanking_Edu.Where(o => (o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY)).ToList();
                }
                sheet.SetCellValue(startRow, 14, listRankingOfClass.Count());

                //Dien hanh kiem kha
                List<PupilRankingBO> listRankingOfClassFair = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o =>  o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).ToList();
                }

                sheet.SetCellValue(startRow, 16, listRankingOfClassFair.Count());

                //Dien hanh kiem TB
                List<PupilRankingBO> listRankingOfClassNormal = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 18, listRankingOfClassNormal.Count());

                //Dien hanh kiem Yeu
                List<PupilRankingBO> listRankingOfClassWeek = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).ToList();
                }

                sheet.SetCellValue(startRow, 20, listRankingOfClassWeek.Count);
                #endregion

                startRow++;
            }

            sheet.CopyPasteSameRowHeigh(rangeBold, startRow);
            sheet.SetCellValue(startRow, 1, "");
            sheet.SetCellValue(startRow, 2, "TS");
            sheet.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 6, "=SUM(F" + startmin.ToString() + ":F" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 8, "=SUM(H" + startmin.ToString() + ":H" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 10, "=SUM(J" + startmin.ToString() + ":J" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 12, "=SUM(L" + startmin.ToString() + ":L" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 14, "=SUM(N" + startmin.ToString() + ":N" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 16, "=SUM(P" + startmin.ToString() + ":P" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 18, "=SUM(R" + startmin.ToString() + ":R" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 20, "=SUM(T" + startmin.ToString() + ":T" + (startRow - 1).ToString() + ")");

            #endregion
            #region Mẫu 4 nữ dân tộc
            startRow = startRow + 2;
            sheet.CopyPasteSameSize(range_Temp, startRow, 1);
            sheet.SetCellValue(startRow, 2, "Mẫu 4");
            sheet.SetCellValue(startRow, 3, "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM HỌC SINH NỮ DÂN TỘC " + semester + " - NĂM HỌC: " + academicYear.DisplayTitle);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                sheet.SetCellValue(startRow, 3, "THỐNG KÊ XẾP LOẠI HỌC LỰC - HẠNH KIỂM HỌC SINH NỮ DÂN TỘC " + " - NĂM HỌC: " + academicYear.DisplayTitle);
            }
            startRow += 5;
            startmin = startRow;

            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            //Duyet theo tung khoi
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int countAllPupilOfEducation = 0;
                int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                List<PupilRankingBO> listRanking_Edu = listRanking.Where(o => o.EducationLevelID == EducationLevelID && o.Genre == GlobalConstants.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
                //Copy row style
                sheet.CopyPasteSameRowHeigh(range, startRow);

                //STT
                sheet.SetCellValue(startRow, 1, (i + 1));

                //Ten khoi
                sheet.SetCellValue(startRow, 2, lstEducationLevel[i].Resolution);


                var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID && o.Genre == GlobalConstants.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople);
                countAllPupilOfEducation = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                sheet.SetCellValue(startRow, 3, countAllPupilOfEducation);

                #region Fill Du Lieu Hoc Luc
                //Hoc luc gioi
                sheet.SetCellValue(startRow, 4, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT).Count());


                //Hoc luc kha
                sheet.SetCellValue(startRow, 6, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_GOOD).Count());


                //Hoc luc TB
                sheet.SetCellValue(startRow, 8, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_NORMAL).Count());


                //Hoc luc yeu
                sheet.SetCellValue(startRow, 10, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_WEAK).Count());

                //Hoc luc kem
                sheet.SetCellValue(startRow, 12, listRanking_Edu.Where(o => o.CapacityLevelID == SystemParamsInFile.CAPACITY_TYPE_POOR).Count());
                #endregion

                #region Fill Du lieu Hanh Kiem
                //Dien hanh kiem tot
                List<PupilRankingBO> listRankingOfClass = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClass = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClass = listRanking_Edu.Where(o => (o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY)).ToList();
                }
                sheet.SetCellValue(startRow, 14, listRankingOfClass.Count());

                //Dien hanh kiem kha
                List<PupilRankingBO> listRankingOfClassFair = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassFair = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY).ToList();
                }

                sheet.SetCellValue(startRow, 16, listRankingOfClassFair.Count());

                //Dien hanh kiem TB
                List<PupilRankingBO> listRankingOfClassNormal = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassNormal = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY).ToList();
                }
                sheet.SetCellValue(startRow, 18, listRankingOfClassNormal.Count());

                //Dien hanh kiem Yeu
                List<PupilRankingBO> listRankingOfClassWeek = new List<PupilRankingBO>();
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY).ToList();
                }
                else
                {
                    listRankingOfClassWeek = listRanking_Edu.Where(o => o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY).ToList();
                }

                sheet.SetCellValue(startRow, 20, listRankingOfClassWeek.Count);
                #endregion

                startRow++;
            }

            sheet.CopyPasteSameRowHeigh(rangeBold, startRow);
            sheet.SetCellValue(startRow, 1, "");
            sheet.SetCellValue(startRow, 2, "TS");
            sheet.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 6, "=SUM(F" + startmin.ToString() + ":F" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 8, "=SUM(H" + startmin.ToString() + ":H" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 10, "=SUM(J" + startmin.ToString() + ":J" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 12, "=SUM(L" + startmin.ToString() + ":L" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 14, "=SUM(N" + startmin.ToString() + ":N" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 16, "=SUM(P" + startmin.ToString() + ":P" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 18, "=SUM(R" + startmin.ToString() + ":R" + (startRow - 1).ToString() + ")");
            sheet.SetCellValue(startRow, 20, "=SUM(T" + startmin.ToString() + ":T" + (startRow - 1).ToString() + ")");
            #endregion
            sheet.DeleteRow(49);
            sheet.DeleteRow(49);
            sheet.FitAllColumnsOnOnePage = true;
            return oBook.ToStream();
        }
        #endregion
    }
}