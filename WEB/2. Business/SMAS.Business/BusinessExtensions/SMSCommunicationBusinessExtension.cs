﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.VTUtils;
using System.Text;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{ 
    public partial class SMSCommunicationBusiness
    {

        #region get list notification inbox
        /// <summary>
        /// get list mail box
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>30/30/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<SMSCommunicationBO> GetListNotificationInbox(IDictionary<string, object> dic)
        {
            bool isGetAll = Utils.GetBool(dic, "isGetAll", false);
            bool isRead = Utils.GetBool(dic, "isRead", false);
            bool isTeacher = Utils.GetBool(dic, "isTeacher", false);
            bool isParrent = Utils.GetBool(dic, "isParrent", false);
            int receiverID = Utils.GetInt(dic, "receiverID");           

            IQueryable<SMSCommunicationBO> lstSMSCommunicationBO = from s in SMSCommunicationBusiness.All
                                                                   join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                   join pp in PupilProfileBusiness.All on s.Sender equals pp.PupilProfileID into pp_join
                                                                   from pp1 in pp_join.DefaultIfEmpty()
                                                                   join e in EmployeeBusiness.All on s.Sender equals e.EmployeeID into e_join
                                                                   from e1 in e_join.DefaultIfEmpty()
                                                                   join er in EmployeeBusiness.All on s.Receiver equals er.EmployeeID into er_join
                                                                   from er1 in er_join.DefaultIfEmpty()
                                                                   join ppr in PupilProfileBusiness.All on s.Receiver equals ppr.PupilProfileID into ppr_join
                                                                   from ppr1 in ppr_join.DefaultIfEmpty()
                                                                   join cg in ContactGroupBusiness.All on s.ContactGroupID equals cg.ContactGroupID into cg_join
                                                                   from cg1 in cg_join.DefaultIfEmpty()                                                                   
                                                                   select new SMSCommunicationBO
                                                                  {
                                                                      SMSCommunicationID = s.SMSCommunicationID,
                                                                      ReceiverID = s.Receiver,
                                                                      SenderID = s.Sender,
                                                                      Content = hs.Content,
                                                                      ShortContent = hs.ShortContent,
                                                                      SenderGroup = s.SenderGroup,                                                                     
                                                                      ParentSenderName = pp1.FullName,
                                                                      TeacherSenderName = e1.FullName,
                                                                      ParentReceiverName = ppr1.FullName,
                                                                      TeacherReceiverName = er1.FullName,
                                                                      CreateDate = s.CreateDate,
                                                                      SchoolID = s.SchoolID,
                                                                      TypeHistory = hs.TypeID,
                                                                      ContentCount = hs.ContentCount,                                                
                                                                      Type = s.Type,
                                                                      ContactGroupName = string.IsNullOrEmpty(cg1.Name) ? string.Empty : cg1.Name,
                                                                      ContactGroupID = s.ContactGroupID,
                                                                      IsRead = s.IsRead,
                                                                      IsAdmin = s.IsAdmin
                                                                  };

            //neu khong fai get all thi get theo trang thai doc hoac chua doc
            if (!isGetAll)
            {
                lstSMSCommunicationBO = lstSMSCommunicationBO.Where(p => p.IsRead == isRead);
            }
            
            if (isTeacher)
            {
                lstSMSCommunicationBO = lstSMSCommunicationBO.Where(p => p.Type == GlobalConstants.SMS_Communicatoin_Parent_To_Teacher
                                        || p.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher);
            }

            if (isParrent)
            {
                lstSMSCommunicationBO = lstSMSCommunicationBO.Where(p => p.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Parent);
            }

            if (receiverID != 0)
            {
                lstSMSCommunicationBO = lstSMSCommunicationBO.Where(p => p.ReceiverID == receiverID);
            }

            return lstSMSCommunicationBO;
        }
        #endregion

        #region get list mailbox groupby sender - pagind enable
        /// <summary>
        /// get list mailbox groupby sender - paging enable
        /// dung groupby lam performance kem -> duyet tu contactGroup lay SMSCommunication
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>06/05/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<SMSCommunicationBO> GetListMailBoxGroupBySender(IDictionary<string, object> dic)
        {
            int teacherID = Utils.GetInt(dic, "teacherID");                      
            int schoolID = Utils.GetInt(dic, "schoolID");
            bool isAdmin = Utils.GetBool(dic, "isAdmin", false);            
            
            if (isAdmin)
            {
                //new la admin thi get toan bo theo contactGroup va truong hop nguoi gui la admin
                IQueryable<SMSCommunicationBO> lstSMSCommunicationBO = from sg in SMSCommunicationGroupBusiness.All
                                                                       join s in SMSCommunicationBusiness.All on sg.SMSCommunicationID equals s.SMSCommunicationID
                                                                       join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                       join pp in PupilProfileBusiness.All on sg.PupilProfileID equals pp.PupilProfileID into pp_join
                                                                       from pp1 in pp_join.DefaultIfEmpty()
                                                                       join e in EmployeeBusiness.All on sg.TeacherID equals e.EmployeeID into e_join
                                                                       from e1 in e_join.DefaultIfEmpty()
                                                                       join cg in ContactGroupBusiness.All.Where(p=>p.SchoolProfileID == schoolID) on sg.ContactGroupID equals cg.ContactGroupID
                                                                       into cg_join
                                                                       from cg1 in cg_join.DefaultIfEmpty()
                                                                       where sg.SchoolID == schoolID && sg.IsAdmin == isAdmin && s.SchoolID == schoolID 
                                                                       && s.IsAdmin == true                                                                      
                                                                       orderby s.CreateDate descending
                                                                       select new SMSCommunicationBO
                                                                       {
                                                                           SMSCommunicationID = s.SMSCommunicationID,
                                                                           ReceiverID = s.Receiver,
                                                                           ShortContent = hs.ShortContent,                                                                         
                                                                           ParentReceiverName = pp1.FullName,
                                                                           TeacherReceiverName = e1.FullName,
                                                                           CreateDate = s.CreateDate,
                                                                           Type = (byte)sg.Type,
                                                                           ContactGroupName = string.IsNullOrEmpty(cg1.Name) ? string.Empty : cg1.Name,
                                                                           ContactGroupID = s.ContactGroupID,
                                                                           IsAdmin = s.IsAdmin
                                                                       };                                                                     
                return lstSMSCommunicationBO;
            }
            else
            {               
                //nguoc lai lay theo tin nhan moi nhat
                IQueryable<long> lstSenderGroup = SMSCommunicationBusiness.All.Where(p => p.Receiver == teacherID).Select(p => p.SenderGroup);
                //lay danh sach giao vien da bi loai khoi nhom lien lac- dung de load thu cu cua giao vien voi nhom do
                IQueryable<TeacherOutGroupSMS> lstTeacherOutGroup = from cg in ContactGroupBusiness.All
                                                                    join ec in EmployeeContactBusiness.All on cg.ContactGroupID equals ec.ContactGroupID
                                                                    join s in SMSCommunicationBusiness.All on ec.ContactGroupID equals s.ContactGroupID
                                                                    join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                    where ec.IsActive == false
                                                                    && (s.Receiver == teacherID || s.Sender == teacherID) && cg.IsDefault == false
                                                                    && s.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher
                                                                    && ec.EmployeeID == teacherID
                                                                    orderby s.SenderGroup descending
                                                                    select new TeacherOutGroupSMS
                                                                    {
                                                                        ContactGroupID = cg.ContactGroupID,
                                                                        SenderGroup = s.SenderGroup,
                                                                        ShortContent = hs.ShortContent
                                                                    };               

                IQueryable<SMSCommunicationBO> lstSMSCommunicationBO = from sg in SMSCommunicationGroupBusiness.All
                                                                       join s in SMSCommunicationBusiness.All on sg.SMSCommunicationID equals s.SMSCommunicationID
                                                                       join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                       join pp in PupilProfileBusiness.All on sg.PupilProfileID equals pp.PupilProfileID into pp_join
                                                                       from pp1 in pp_join.DefaultIfEmpty()
                                                                       join e in EmployeeBusiness.All on sg.TeacherID equals e.EmployeeID into e_join
                                                                       from e1 in e_join.DefaultIfEmpty()
                                                                       join cg in ContactGroupBusiness.All on sg.ContactGroupID equals cg.ContactGroupID into cg_join
                                                                       from cg1 in cg_join.DefaultIfEmpty()
                                                                       join ec in EmployeeContactBusiness.All
                                                                        on new { cg1.ContactGroupID } equals new { ec.ContactGroupID } into ec_join
                                                                       from ec1 in ec_join.DefaultIfEmpty()
                                                                       where
                                                                       (
                                                                            (sg.TeacherID == teacherID && (sg.Type == GlobalConstants.SMS_Communicatoin_Parent_To_Teacher || sg.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Parent))
                                                                            || (ec1.EmployeeID == teacherID && sg.TeacherID == teacherID && sg.ContactGroupID == cg1.ContactGroupID && sg.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher)
                                                                            || (cg1.IsDefault == true && sg.IsAdmin == true && sg.SchoolID == schoolID)
                                                                            || (ec1.EmployeeID == teacherID && s.ContactGroupID == ec1.ContactGroupID
                                                                                 && (lstSenderGroup.Contains(s.SenderGroup) || (s.ContactGroupID.HasValue ? lstTeacherOutGroup.Select(p => p.ContactGroupID).Contains(s.ContactGroupID.Value) : false))
                                                                                 )
                                                                       )
                                                                       orderby s.CreateDate descending
                                                                       select new SMSCommunicationBO
                                                                       {
                                                                           SMSCommunicationID = s.SMSCommunicationID,
                                                                           ReceiverID = s.Receiver,
                                                                           SenderID = s.Sender,
                                                                           ShortContent = s.ContactGroupID.HasValue ? (lstTeacherOutGroup.Select(p=>p.ContactGroupID).Contains(s.ContactGroupID.Value) ? lstTeacherOutGroup.Where(p=>p.ContactGroupID == s.ContactGroupID.Value).FirstOrDefault().ShortContent : hs.ShortContent) : hs.ShortContent,
                                                                           SenderGroup = s.SenderGroup,                                                                        
                                                                           ParentSenderName = pp1.FullName,
                                                                           TeacherSenderName = e1.FullName,
                                                                           ParentReceiverName = pp1.FullName,
                                                                           TeacherReceiverName = e1.FullName,
                                                                           CreateDate = s.CreateDate,
                                                                           Type = s.Type,
                                                                           ContactGroupName = string.IsNullOrEmpty(cg1.Name) ? string.Empty : cg1.Name,
                                                                           ContactGroupID = s.ContactGroupID,
                                                                           IsRead = s.IsRead,
                                                                           IsAdmin = s.IsAdmin
                                                                       };
                return lstSMSCommunicationBO;

            }
        }
        public IQueryable<HistorySMS> ListSMSCommunication(int SchoolID, DateTime fromDate, DateTime toDate, int Receiver, int Type, int classID)
        {
            IQueryable<SMSCommunication> smsC = SMSCommunicationBusiness.All;
            IQueryable<PupilProfile> pp = PupilProfileBusiness.All;
            IQueryable<HistorySMS> lstSMSCommunication = from hs in HistorySMSBusiness.All
                                                         where smsC.Any(p => (p.HistorySMSID == hs.HistorySMSID)
                                                                && (p.CreateDate >= fromDate && p.CreateDate <= toDate)
                                                                && (p.Receiver == Receiver || 0 == Receiver)
                                                                && p.Type == Type
                                                                && pp.Any(s => (s.PupilProfileID == hs.PupilID) && s.CurrentClassID == classID && s.IsActive == true))
                                                         select hs;

            return lstSMSCommunication;
        }
        #endregion

        #region get list detail mailbox by sender group
        /// <summary>
        /// get list detail mail box by sender group
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>13/05/2013</date>
        /// <param name="?"></param>
        /// <returns></returns>
        public List<SMSCommunicationBO> GetListDetailMailBox(IDictionary<string,object> dic)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;
            int receiverID = Utils.GetInt(dic,"receiverID");
            int senderID = Utils.GetInt(dic,"senderID");
            int type = Utils.GetInt(dic, "type");
            int contactGroupID = Utils.GetInt(dic, "contactGroupID");
            int page = Utils.GetInt(dic, "page");
            int pageSize = Utils.GetInt(dic, "pageSize");
            bool isAdmin = Utils.GetBool(dic, "isAdmin", false);
            int schoolID = Utils.GetInt(dic, "schoolID");
            List<SMSCommunicationBO> lstSMSCommunicationBO = new List<SMSCommunicationBO>();

            switch (type)
            {
                case GlobalConstants.SMS_Communicatoin_Parent_To_Teacher:
                    {
                        #region truong hop phhs den giao vien
                        //lay tat ca trao doi giua gv va phhs
                        if (isAdmin)
                        {
                            //admin khong co truong hop nay                            
                        }
                        else
                        {
                            IQueryable<SMSCommunicationBO> lstSMSCommunicationBOTemp = (from s in SMSCommunicationBusiness.All
                                                                                        join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                                        join pp in PupilProfileBusiness.All on s.Sender equals pp.PupilProfileID into pp_join
                                                                                        from pp1 in pp_join.DefaultIfEmpty()
                                                                                        join e in EmployeeBusiness.All on s.Sender equals e.EmployeeID into e_join
                                                                                        from e1 in e_join.DefaultIfEmpty()
                                                                                        join er in EmployeeBusiness.All on s.Receiver equals er.EmployeeID into er_join
                                                                                        from er1 in er_join.DefaultIfEmpty()
                                                                                        join ppr in PupilProfileBusiness.All on s.Receiver equals ppr.PupilProfileID into ppr_join
                                                                                        from ppr1 in ppr_join.DefaultIfEmpty()
                                                                                        where (s.Sender == senderID && s.Receiver == receiverID && s.Type == GlobalConstants.SMS_Communicatoin_Parent_To_Teacher)
                                                                                                 || (s.Sender == receiverID && s.Receiver == senderID && s.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Parent)                                                                                             
                                                                                        select new SMSCommunicationBO
                                                                                        {
                                                                                            SMSCommunicationID = s.SMSCommunicationID,
                                                                                            ReceiverID = s.Receiver,
                                                                                            SenderID = s.Sender,
                                                                                            ShortContent = hs.ShortContent,                                                                                         
                                                                                            ParentSenderName = pp1.FullName,
                                                                                            TeacherSenderName = e1.FullName,
                                                                                            ParentReceiverName = ppr1.FullName,
                                                                                            TeacherReceiverName = er1.FullName,
                                                                                            CreateDate = s.CreateDate,
                                                                                            Type = s.Type,
                                                                                            IsAdmin = s.IsAdmin
                                                                                        });
                            lstSMSCommunicationBO = lstSMSCommunicationBOTemp.OrderBy(p => p.CreateDate).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                        }
                        break;
                        #endregion
                    }
                case GlobalConstants.SMS_Communicatoin_Teacher_To_Parent:
                    {
                        #region truong hop giao vien den phhs
                        if (isAdmin)
                        {
                            //neu la admin thi thay theo dk admin va receiver la phhs
                            // Fix lỗi ATTT
                            // s.IsAdmin == true && s.Receiver == receiverID && s.SchoolID == s.SchoolID
                            IQueryable<SMSCommunicationBO> lstSMSCommunicationBOTemp = (from s in SMSCommunicationBusiness.All
                                                                                        join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID                                                                                                                                                                                                                                                                        
                                                                                        join ppr in PupilProfileBusiness.All on s.Receiver equals ppr.PupilProfileID into ppr_join
                                                                                        from ppr1 in ppr_join.DefaultIfEmpty()
                                                                                        where
                                                                                            s.IsAdmin == true && s.Receiver == receiverID && s.SchoolID == schoolID//s.SchoolID
                                                                                        select new SMSCommunicationBO
                                                                                        {
                                                                                            SMSCommunicationID = s.SMSCommunicationID,
                                                                                            ReceiverID = s.Receiver,
                                                                                            SenderID = s.Sender,
                                                                                            ShortContent = hs.ShortContent,                                                                                                                                                                                                                                                                             
                                                                                            ParentReceiverName = ppr1.FullName,
                                                                                            CreateDate = s.CreateDate,
                                                                                            Type = s.Type,
                                                                                            IsAdmin = s.IsAdmin
                                                                                        });
                            lstSMSCommunicationBO = lstSMSCommunicationBOTemp.OrderBy(p => p.CreateDate).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                        }
                        else
                        {
                            //lay theo phhs - gv
                            IQueryable<SMSCommunicationBO> lstSMSCommunicationBOTemp = (from s in SMSCommunicationBusiness.All
                                                                                        join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                                        join pp in PupilProfileBusiness.All on s.Sender equals pp.PupilProfileID into pp_join
                                                                                        from pp1 in pp_join.DefaultIfEmpty()
                                                                                        join e in EmployeeBusiness.All on s.Sender equals e.EmployeeID into e_join
                                                                                        from e1 in e_join.DefaultIfEmpty()
                                                                                        join er in EmployeeBusiness.All on s.Receiver equals er.EmployeeID into er_join
                                                                                        from er1 in er_join.DefaultIfEmpty()
                                                                                        join ppr in PupilProfileBusiness.All on s.Receiver equals ppr.PupilProfileID into ppr_join
                                                                                        from ppr1 in ppr_join.DefaultIfEmpty()                                                                                                                                                                            
                                                                                        where (s.Sender == senderID && s.Receiver == receiverID && s.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Parent)
                                                                                                 || (s.Sender == receiverID && s.Receiver == senderID && s.Type == GlobalConstants.SMS_Communicatoin_Parent_To_Teacher)                                                                         
                                                                                        select new SMSCommunicationBO
                                                                                        {
                                                                                            SMSCommunicationID = s.SMSCommunicationID,
                                                                                            ReceiverID = s.Receiver,
                                                                                            SenderID = s.Sender,
                                                                                            ShortContent = hs.ShortContent,                                                                                          
                                                                                            ParentSenderName = pp1.FullName,
                                                                                            TeacherSenderName = e1.FullName,
                                                                                            ParentReceiverName = ppr1.FullName,
                                                                                            TeacherReceiverName = er1.FullName,
                                                                                            CreateDate = s.CreateDate,
                                                                                            Type = s.Type,
                                                                                            IsAdmin = s.IsAdmin                                                                                           
                                                                                        });
                            lstSMSCommunicationBO = lstSMSCommunicationBOTemp.OrderBy(p => p.CreateDate).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                        }
                        break;
                        #endregion
                    }
                case GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher:
                    {
                        #region truong hop giao vien - giao vien
                        if (isAdmin)
                        {
                            //lay list senderGroup 
                            IQueryable<SMSCommunicationBO> lstSMSCommunicationBOTemp = from s in SMSCommunicationBusiness.All
                                                                                       where s.IsAdmin == true && s.ContactGroupID == contactGroupID
                                                                                             && s.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher
                                                                                             && s.SchoolID == schoolID
                                                                                       select new SMSCommunicationBO
                                                                                       {
                                                                                           CreateDate = s.CreateDate,
                                                                                           SenderGroup = s.SenderGroup
                                                                                       };
                            List<long> lstSenderGroup = lstSMSCommunicationBOTemp.OrderBy(p => p.CreateDate).Select(p => p.SenderGroup).Distinct().ToList().Skip((page - 1) * pageSize).Take(pageSize).ToList();

                            //for theo senderGroup de lay du lieu
                            lstSMSCommunicationBOTemp = null;
                            SMSCommunicationBO objSMSCommunicationBO = null;
                            long senderGroup = 0;
                            if (lstSenderGroup != null && lstSenderGroup.Count > 0)
                            {
                                for (int i = lstSenderGroup.Count - 1; i >= 0; i--)
                                {
                                    objSMSCommunicationBO = null;
                                    senderGroup = lstSenderGroup[i];
                                    //admin thi lay theo contactGroup
                                    lstSMSCommunicationBOTemp = from s in SMSCommunicationBusiness.All
                                                                join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID                                                                
                                                                join cg in ContactGroupBusiness.All on s.ContactGroupID equals cg.ContactGroupID
                                                                where s.SenderGroup == senderGroup
                                                                select new SMSCommunicationBO
                                                                {
                                                                    SMSCommunicationID = s.SMSCommunicationID,
                                                                    ReceiverID = s.Receiver,
                                                                    SenderID = s.Sender,
                                                                    ShortContent = hs.ShortContent,
                                                                    CreateDate = s.CreateDate,
                                                                    Type = s.Type,
                                                                    ContactGroupID = cg.ContactGroupID,
                                                                    ContactGroupName = cg.Name,
                                                                    IsAdmin = s.IsAdmin
                                                                };

                                    objSMSCommunicationBO = lstSMSCommunicationBOTemp.FirstOrDefault();
                                    if (objSMSCommunicationBO != null)
                                    {
                                        objSMSCommunicationBO.NumToSendGroup = lstSMSCommunicationBOTemp.Count();
                                        lstSMSCommunicationBO.Add(objSMSCommunicationBO);
                                    }
                                }
                                if (lstSMSCommunicationBO != null && lstSMSCommunicationBO.Count > 0)
                                {
                                    lstSMSCommunicationBO = lstSMSCommunicationBO.OrderBy(p => p.CreateDate).ToList();
                                }
                            }
                        }
                        else
                        {
                            //lay list senderGroup 
                            IQueryable<SMSCommunicationBO> lstSMSCommunicationBOTemp = from s in SMSCommunicationBusiness.All
                                                                                       where (s.Receiver == receiverID || s.Sender == receiverID) && s.ContactGroupID == contactGroupID
                                                                                             && s.Type == GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher
                                                                                             && s.SchoolID == schoolID 
                                                                                       select new SMSCommunicationBO
                                                                                       {
                                                                                           CreateDate = s.CreateDate,
                                                                                           SenderGroup = s.SenderGroup
                                                                                       };
                            List<long> lstSenderGroup = lstSMSCommunicationBOTemp.OrderBy(p => p.CreateDate).Select(p => p.SenderGroup).Distinct().ToList().Skip((page - 1) * pageSize).Take(pageSize).ToList();

                            //for theo senderGroup de lay du lieu
                            lstSMSCommunicationBOTemp = null;
                            SMSCommunicationBO objSMSCommunicationBO = null;
                            long senderGroup = 0;
                            if (lstSenderGroup != null && lstSenderGroup.Count > 0)
                            {
                                for (int i = lstSenderGroup.Count - 1; i >= 0; i--)
                                {
                                    objSMSCommunicationBO = null;
                                    senderGroup = lstSenderGroup[i];
                                    //giao vien thi lay theo contactGroup
                                    lstSMSCommunicationBOTemp = from s in SMSCommunicationBusiness.All
                                                                join hs in HistorySMSBusiness.All on s.HistorySMSID equals hs.HistorySMSID
                                                                join es in EmployeeBusiness.All on s.Sender equals es.EmployeeID into es_join
                                                                from es1 in es_join.DefaultIfEmpty()
                                                                join er in EmployeeBusiness.All on s.Receiver equals er.EmployeeID into er_join
                                                                from er1 in er_join.DefaultIfEmpty()
                                                                join cg in ContactGroupBusiness.All on s.ContactGroupID equals cg.ContactGroupID
                                                                where s.SenderGroup == senderGroup
                                                                select new SMSCommunicationBO
                                                                {
                                                                    SMSCommunicationID = s.SMSCommunicationID,
                                                                    ReceiverID = s.Receiver,
                                                                    SenderID = s.Sender,
                                                                    ShortContent = hs.ShortContent,                                                                  
                                                                    TeacherSenderName = es1.FullName,
                                                                    TeacherReceiverName = er1.FullName,
                                                                    CreateDate = s.CreateDate,
                                                                    Type = s.Type,
                                                                    ContactGroupID = cg.ContactGroupID,
                                                                    ContactGroupName = cg.Name,
                                                                    IsAdmin = s.IsAdmin
                                                                };

                                    objSMSCommunicationBO = lstSMSCommunicationBOTemp.FirstOrDefault();
                                    if (objSMSCommunicationBO != null)
                                    {
                                        objSMSCommunicationBO.NumToSendGroup = lstSMSCommunicationBOTemp.Count();
                                        lstSMSCommunicationBO.Add(objSMSCommunicationBO);
                                    }
                                }
                                if (lstSMSCommunicationBO != null && lstSMSCommunicationBO.Count > 0)
                                {
                                    lstSMSCommunicationBO = lstSMSCommunicationBO.OrderBy(p => p.CreateDate).ToList();
                                }
                            }
                        }
                        break;
                        #endregion
                    }
                default:
                    break;
            }
        
            return lstSMSCommunicationBO;
        }
        #endregion

        #region sendSMS
        /// <summary>
        /// SendSMS - return SMSResultBO 
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>06/05/2013</date>
        /// <param name="dic">sender, lstReceiverID (to parent), contactGroupID (to teacher), classID, academicYearID, type, schoolID, content</param>
        /// <returns></returns>
        public ResultBO SendSMS(IDictionary<string, object> dic)
        {
            try
            {               
                string content = Utils.GetString(dic, "content").Trim();
                ResultBO objSMSResultBO = new ResultBO();
                if (Utils.CheckContentSMS(content))
                {
                    objSMSResultBO.isError = true;
                    objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                    return objSMSResultBO;
                }

                int senderID = Utils.GetInt(dic, "senderID");
                List<int> lstReceiverID = Utils.GetIntList(dic, "lstReceiverID");
                int contactGroupID = Utils.GetInt(dic, "contactGroupID");
                int classID = Utils.GetInt(dic, "classID");
                int academicYearID = Utils.GetInt(dic, "academicYearID");                
                int type = Utils.GetInt(dic, "type");
                int? typeHistory = Utils.GetNullableInt(dic, "typeHistory");
                int schoolID = Utils.GetInt(dic, "schoolID");                
                string shortContent = Utils.GetString(dic, "shortContent").Trim();
                bool isPrincipal = Utils.GetBool(dic, "isPrincipal", false);
                bool isAdmin = Utils.GetBool(dic, "isAdmin", false);
                int? year = AcademicYearBusiness.Find(academicYearID).Year;
                long senderGroup = DateTime.Now.Ticks;
                dic["isSubmitChange"] = false;
                DateTime createDate = DateTime.Now;
                dic["dateTime"] = createDate;

                if (!SMSCommunicationBusiness.CheckPermissionToSendSMS(dic))
                {
                    objSMSResultBO.isError = true;
                    objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                    return objSMSResultBO;
                }

                StringBuilder newJsonLog = new StringBuilder();

                int countSMS = 0;
                if (content.Length % GlobalConstants.SMS_COUNT == 0)
                {
                    countSMS = content.Length / GlobalConstants.SMS_COUNT;
                }
                else
                {
                    countSMS = content.Length / GlobalConstants.SMS_COUNT + 1;
                }

                //turn off validate of dbset - turn on when submitchange()
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;

                switch (type)
                {
                    case GlobalConstants.SMS_Communicatoin_Parent_To_Teacher:
                        {
                            #region parent to teacher                            
                            if (classID != 0)
                            {
                                //kiem tra giao vien lam thay neu ton tai giao vien cn lam thay thi gui tin nhan cho giao vien cn lam thay, nguoc lai gui cho gvcn
                                int? receiver = null;
                                DateTime currentDate = createDate.Date;
                                receiver = (from hts in HeadTeacherSubstitutionBusiness.All
                                            where hts.ClassID == classID && hts.AcademicYearID == academicYearID
                                            && hts.AssignedDate <= currentDate && hts.EndDate >= currentDate
                                            select hts.HeadTeacherID).FirstOrDefault();
                                if (receiver == null || (receiver.HasValue && receiver.Value == 0))
                                {
                                    receiver = (from cp in ClassProfileBusiness.All
                                                where cp.ClassProfileID == classID && cp.AcademicYearID == academicYearID && cp.IsActive==true
                                                select cp.HeadTeacherID).FirstOrDefault();
                                }

                                if (receiver == null)
                                {
                                    objSMSResultBO.isError = true;
                                    objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                                    return objSMSResultBO;
                                }                                

                                Employee objEmployee = (from e in EmployeeBusiness.All
                                                        where e.EmployeeID == receiver
                                                        && e.IsActive == true
                                                        select e).FirstOrDefault();
                                if (objEmployee != null)
                                {
                                    //validate so dt
                                    if (!Utils.CheckMobileNumber(objEmployee.Mobile))
                                    {
                                        objSMSResultBO.isError = true;
                                        objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                                        return objSMSResultBO;
                                    }

                                    //insert hintorySMS
                                    HistorySMS objHistorySMS = new HistorySMS();
                                    objHistorySMS.Mobile = objEmployee.Mobile;
                                    objHistorySMS.FullName = objEmployee.FullName;
                                    objHistorySMS.TypeID = GlobalConstants.HISTORYSMS_TO_COMMUNICATION_ID;
                                    objHistorySMS.ReceiveType = GlobalConstants.HISTORY_RECEIVER_TEACHER_ID;
                                    objHistorySMS.CreateDate = createDate;
                                    objHistorySMS.SchoolID = schoolID;
                                    objHistorySMS.Year = year.Value;
                                    objHistorySMS.Content = content;
                                    objHistorySMS.PupilID = senderID;
                                    objHistorySMS.EmployeeID = objEmployee.EmployeeID;
                                    objHistorySMS.ShortContent = shortContent;
                                    objHistorySMS.Sender = senderID.ToString();
                                    objHistorySMS.ContentCount = countSMS;
                                    HistorySMSBusiness.Insert(objHistorySMS);

                                    newJsonLog.Append("HistorySMS ").Append(objHistorySMS.HistorySMSID.ToString());

                                    //insert MT
                                    MT objMT = new MT();
                                    objMT.HistorySMS = objHistorySMS;
                                    objMT.ReceiverNumber = objEmployee.Mobile;
                                    objMT.Content = content;
                                    objMT.Status = GlobalConstants.MT_STATUS_NOT_SEND;
                                    objMT.CreateTime = createDate;
                                    MTBusiness.Insert(objMT);

                                    newJsonLog.Append(" MT ").Append(objMT.MT_ID.ToString()).Append(";");

                                    //insert SMSCommunication
                                    SMSCommunication objSMSCommunication = new SMSCommunication();
                                    objSMSCommunication.HistorySMS = objHistorySMS;
                                    objSMSCommunication.Receiver = objEmployee.EmployeeID;
                                    objSMSCommunication.Sender = senderID;
                                    objSMSCommunication.SenderGroup = senderGroup;
                                    objSMSCommunication.SchoolID = schoolID;
                                    objSMSCommunication.IsAdmin = isAdmin;
                                    objSMSCommunication.Type = GlobalConstants.SMS_Communicatoin_Parent_To_Teacher;
                                    objSMSCommunication.IsRead = false;
                                    objSMSCommunication.CreateDate = createDate;
                                    SMSCommunicationBusiness.Insert(objSMSCommunication);

                                    newJsonLog.Append(" SMSCommunication ").Append(objSMSCommunication.SMSCommunicationID.ToString());

                                    //update lai SMSCommunicationGroup                                       
                                    dic["teacherID"] = objEmployee.EmployeeID;
                                    dic["pupilProfileID"] = senderID;
                                    dic["smsCommunication"] = objSMSCommunication;
                                    SMSCommunicationGroupBusiness.UpdateSMSGroup(dic);

                                    //turn off validate of dbset - turn on when submitchange()
                                    this.context.Configuration.AutoDetectChangesEnabled = true;
                                    this.context.Configuration.ValidateOnSaveEnabled = true;

                                    HistorySMSBusiness.Save();

                                    objSMSResultBO.Result = objEmployee.FullName;
                                    objSMSResultBO.LogResult = newJsonLog.ToString();
                                    return objSMSResultBO;
                                }
                                else
                                {
                                    objSMSResultBO.isError = true;
                                    objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                                    return objSMSResultBO;                                    
                                }
                            }
                            else
                            {
                                objSMSResultBO.isError = true;
                                objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                                return objSMSResultBO;
                            }
                            #endregion
                        }
                    case GlobalConstants.SMS_Communicatoin_Teacher_To_Parent:
                        {
                            #region teacher to parent
                            IQueryable<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(p => p.CurrentClassID == classID && p.CurrentAcademicYearID == academicYearID && p.IsActive == true && p.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING);
                            PupilProfile objPupilProfile = null;
                            int receiverID;
                            int countNumOfSend = 0;
                            MT objMT = null;
                            SMSCommunication objSMSCommunication = null;
                            HistorySMS objHistorySMS = null;
                            List<string> lstMobiles = null;
                            string mobile = string.Empty;
                            for (int i = lstReceiverID.Count - 1; i >= 0; i--)
                            {                                
                                receiverID = lstReceiverID[i];
                                objPupilProfile = lstPupilProfile.Where(a=>a.PupilProfileID == receiverID).FirstOrDefault();                                
                                if (objPupilProfile != null)
                                {
                                    objMT = null;
                                    objHistorySMS = null;
                                    objSMSCommunication = null;
                                    lstMobiles = new List<string>();
                                    //So dien thoai cha
                                    if (!string.IsNullOrEmpty(objPupilProfile.FatherMobile) && objPupilProfile.IsFatherSMS.HasValue && objPupilProfile.IsFatherSMS.Value)
                                    {
                                        lstMobiles.Add(objPupilProfile.FatherMobile);
                                    }
                                    //So dien thoai me
                                    if (!string.IsNullOrEmpty(objPupilProfile.MotherMobile) && objPupilProfile.IsMotherSMS.HasValue && objPupilProfile.IsMotherSMS.Value)
                                    {
                                        lstMobiles.Add(objPupilProfile.MotherMobile);
                                    }
                                    //So dien thoai nguoi do dau
                                    if (!string.IsNullOrEmpty(objPupilProfile.SponsorMobile) && objPupilProfile.IsSponsorSMS.HasValue && objPupilProfile.IsSponsorSMS.Value)
                                    {
                                        lstMobiles.Add(objPupilProfile.SponsorMobile);
                                    }
                                    //Neu khong ton tai so dien thoai
                                    if(lstMobiles.Count == 0)
                                    {
                                        continue;
                                    }                                    
                                    
                                    for (int j = lstMobiles.Count - 1; j >=0; j--)
                                    {
                                        mobile = lstMobiles[j];
                                        if (!Utils.CheckMobileNumber(mobile))
                                        {
                                            continue;
                                        }

                                        //insert hintorySMS
                                        objHistorySMS = new HistorySMS();
                                        objHistorySMS.Mobile = mobile;
                                        objHistorySMS.FullName = objPupilProfile.FullName;
                                        if (typeHistory.HasValue)
                                        {
                                            objHistorySMS.TypeID = typeHistory;
                                            objHistorySMS.ReceiveType = GlobalConstants.HISTORY_RECEIVER_PARENT_ID;
                                        }
                                        else
                                        {
                                            objHistorySMS.TypeID = GlobalConstants.HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMS.ReceiveType = GlobalConstants.HISTORY_RECEIVER_PARENT_ID;
                                        }
                                        objHistorySMS.CreateDate = createDate;
                                        objHistorySMS.SchoolID = schoolID;
                                        objHistorySMS.Year = year.Value;
                                        objHistorySMS.Content = content;
                                        objHistorySMS.PupilID = objPupilProfile.PupilProfileID;
                                        objHistorySMS.EmployeeID = senderID;
                                        objHistorySMS.ShortContent = shortContent;
                                        objHistorySMS.Sender = senderID.ToString();
                                        objHistorySMS.ContentCount = countSMS;
                                        HistorySMSBusiness.Insert(objHistorySMS);

                                        newJsonLog.Append("HistorySMS ").Append(objHistorySMS.HistorySMSID.ToString());
                                        
                                        //insert MT
                                        objMT = new MT();
                                        objMT.HistorySMS = objHistorySMS;
                                        objMT.ReceiverNumber = mobile;
                                        objMT.Content = content;
                                        objMT.Status = GlobalConstants.MT_STATUS_NOT_SEND;
                                        objMT.CreateTime = createDate;
                                        MTBusiness.Insert(objMT);

                                        newJsonLog.Append(" MT ").Append(objMT.MT_ID.ToString()).Append(";");

                                        countNumOfSend++;
                                    }
                                    //Tao hop thu cho tin nhan
                                    if (objHistorySMS != null)
                                    {
                                        //insert SMSCommunication
                                        objSMSCommunication = new SMSCommunication();
                                        objSMSCommunication.HistorySMS = objHistorySMS;
                                        objSMSCommunication.Receiver = objPupilProfile.PupilProfileID;
                                        objSMSCommunication.Sender = senderID;
                                        objSMSCommunication.SenderGroup = senderGroup;
                                        objSMSCommunication.SchoolID = schoolID;
                                        objSMSCommunication.IsAdmin = isAdmin;
                                        objSMSCommunication.Type = GlobalConstants.SMS_Communicatoin_Teacher_To_Parent;
                                        objSMSCommunication.IsRead = false;
                                        objSMSCommunication.CreateDate = createDate;
                                        SMSCommunicationBusiness.Insert(objSMSCommunication);

                                        newJsonLog.Append(" SMSCommunication ").Append(objSMSCommunication.SMSCommunicationID.ToString());

                                        //update lai SMSCommunicationGroup                                       
                                        dic["teacherID"] = senderID;
                                        dic["pupilProfileID"] = objPupilProfile.PupilProfileID;
                                        dic["smsCommunication"] = objSMSCommunication;
                                        SMSCommunicationGroupBusiness.UpdateSMSGroup(dic);
                                    }
                                }                                
                            }
                           
                            if (countNumOfSend == 0)
                            {
                                objSMSResultBO.isError = true;
                                objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                                return objSMSResultBO;
                            }
                            //turn off validate of dbset - turn on when submitchange()
                            this.context.Configuration.AutoDetectChangesEnabled = true;
                            this.context.Configuration.ValidateOnSaveEnabled = true;

                            HistorySMSBusiness.Save();                            
                           
                            objSMSResultBO.LogResult = newJsonLog.ToString();
                            objSMSResultBO.Result = countNumOfSend.ToString();
                            objSMSResultBO.MessageSend = countNumOfSend * countSMS;
                            return objSMSResultBO;
                            #endregion
                        }
                    case GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher:
                        {
                            #region teacher to teacher
                            List<EmployeeContactBO> lstEmployeeContactBO = new List<EmployeeContactBO>();
                            SMAS.Models.Models.SMS_TEACHER_CONTACT_GROUP objContactGroup = ContactGroupBusiness.Find(contactGroupID);
                            //kiem tra neu la defaultGroup thi gui cho toan truong
                            if (objContactGroup.IS_DEFAULT)
                            {
                                //chi co admin voi ban giam hieu moi duoc quyen gui tin nhan toan truong
                                if (isAdmin)
                                {
                                    lstEmployeeContactBO = (from e in EmployeeBusiness.All
                                                            where e.SchoolID == schoolID && e.IsActive == true
                                                                && !string.IsNullOrEmpty(e.Mobile)
                                                            select new EmployeeContactBO
                                                            {
                                                                EmployeeID = e.EmployeeID,
                                                                Mobile = e.Mobile,
                                                                FullName = e.FullName
                                                            }
                                                            ).ToList();
                                }
                                else if (isPrincipal)
                                {
                                    lstEmployeeContactBO = (from e in EmployeeBusiness.All
                                                            where e.SchoolID == schoolID && e.IsActive == true
                                                                && !string.IsNullOrEmpty(e.Mobile) && e.EmployeeID != senderID
                                                            select new EmployeeContactBO
                                                            {
                                                                EmployeeID = e.EmployeeID,
                                                                Mobile = e.Mobile,
                                                                FullName = e.FullName
                                                            }
                                                            ).ToList();
                                }
                            }
                            else
                            {
                                lstEmployeeContactBO = (from e in EmployeeBusiness.All
                                                        join ec in EmployeeContactBusiness.All on e.EmployeeID equals ec.EmployeeID
                                                        where ec.ContactGroupID == contactGroupID
                                                        && !string.IsNullOrEmpty(e.Mobile) && ec.IsActive == true
                                                        && e.IsActive == true && ec.EmployeeID != senderID
                                                        select new EmployeeContactBO
                                                        {
                                                            EmployeeID = e.EmployeeID,
                                                            Mobile = e.Mobile,
                                                            FullName = e.FullName
                                                        }).ToList();
                            }

                            EmployeeContactBO objEmployeeReceiver = null;
                            SMSCommunication objSMSCommunication = null;
                            MT objMT = null;
                            HistorySMS objHistorySMS = null;                            

                            int countNumOfSend = 0;

                            if (lstEmployeeContactBO != null && lstEmployeeContactBO.Count > 0)
                            {
                                this.context.Configuration.AutoDetectChangesEnabled = false;
                                this.context.Configuration.ValidateOnSaveEnabled = false;

                                bool isFirstInsert = true;
                                for (int i = lstEmployeeContactBO.Count - 1; i >= 0; i--)
                                {
                                    objEmployeeReceiver = lstEmployeeContactBO[i];                                    

                                    if (!Utils.CheckMobileNumber(objEmployeeReceiver.Mobile))
                                    {
                                        continue;
                                    }
                                   
                                    //insert historySMS 
                                    objHistorySMS = new HistorySMS();
                                    objHistorySMS.Mobile = objEmployeeReceiver.Mobile;
                                    objHistorySMS.FullName = objEmployeeReceiver.FullName;
                                    if (typeHistory.HasValue)
                                    {
                                        objHistorySMS.TypeID = typeHistory;
                                        objHistorySMS.ReceiveType = GlobalConstants.HISTORY_RECEIVER_TEACHER_ID;
                                    }
                                    else
                                    {
                                        objHistorySMS.TypeID = GlobalConstants.HISTORYSMS_TO_COMMUNICATION_ID;
                                        objHistorySMS.ReceiveType = GlobalConstants.HISTORY_RECEIVER_TEACHER_ID;
                                    }
                                    objHistorySMS.CreateDate = createDate;
                                    objHistorySMS.SchoolID = schoolID;
                                    objHistorySMS.Year = year.Value;
                                    objHistorySMS.Content = content;
                                    objHistorySMS.EmployeeID = senderID;
                                    objHistorySMS.ShortContent = shortContent;
                                    objHistorySMS.Sender = senderID.ToString();
                                    objHistorySMS.ContentCount = countSMS;
                                    HistorySMSBusiness.Insert(objHistorySMS);                                   

                                    newJsonLog.Append("HistorySMS ").Append(objHistorySMS.HistorySMSID.ToString());

                                    //insert SMSCommunication
                                    objSMSCommunication = new SMSCommunication();
                                    objSMSCommunication.HistorySMS = objHistorySMS;
                                    objSMSCommunication.ContactGroupID = contactGroupID;
                                    objSMSCommunication.Receiver = objEmployeeReceiver.EmployeeID;
                                    objSMSCommunication.Sender = senderID;
                                    objSMSCommunication.SenderGroup = senderGroup;
                                    objSMSCommunication.SchoolID = schoolID;
                                    objSMSCommunication.IsAdmin = isAdmin;
                                    objSMSCommunication.Type = GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher;
                                    objSMSCommunication.IsRead = false;
                                    objSMSCommunication.CreateDate = createDate;
                                    SMSCommunicationBusiness.Insert(objSMSCommunication);

                                    newJsonLog.Append(" SMSCommunication ").Append(objSMSCommunication.SMSCommunicationID.ToString());

                                    //insert MT
                                    objMT = new MT();
                                    objMT.HistorySMS = objHistorySMS;
                                    objMT.ReceiverNumber = objEmployeeReceiver.Mobile;
                                    objMT.Content = content;
                                    objMT.Status = GlobalConstants.MT_STATUS_NOT_SEND;
                                    objMT.CreateTime = createDate;
                                    MTBusiness.Insert(objMT);

                                    newJsonLog.Append(" MT ").Append(objMT.MT_ID.ToString()).Append(";");

                                    //update lai SMSCommunicationGroup
                                    if (isFirstInsert)
                                    {
                                        dic["smsCommunication"] = objSMSCommunication;
                                        dic["teacherID"] = senderID;                                        
                                        SMSCommunicationGroupBusiness.UpdateSMSGroup(dic);
                                        isFirstInsert = false;
                                    }

                                    countNumOfSend++;                                    
                                }
                                
                                //turn off validate of dbset - turn on when submitchange()
                                this.context.Configuration.AutoDetectChangesEnabled = true;
                                this.context.Configuration.ValidateOnSaveEnabled = true;
                                HistorySMSBusiness.Save();                                     
                            }
                            else
                            {
                                objSMSResultBO.isError = true;
                                objSMSResultBO.ErrorMsg = "SMS_Group_No_Employee";
                                return objSMSResultBO;
                            }
                            if (countNumOfSend == 0)
                            {
                                objSMSResultBO.isError = true;
                                objSMSResultBO.ErrorMsg = "Home_Notification_Label_Error";
                                return objSMSResultBO;
                            }

                            objSMSResultBO.LogResult = newJsonLog.ToString();
                            objSMSResultBO.Result = countNumOfSend.ToString();
                            objSMSResultBO.MessageSend = countNumOfSend * countSMS;
                            return objSMSResultBO;
                            #endregion
                        }                    
                    default:
                        break;
                }
                
                return new ResultBO();
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                LogExtensions.ErrorExt(logger, DateTime.Now, "null", ex.Message, "null");
                throw;
            }
        }
        #endregion

        #region check permission to send SMS
        /// <summary>
        /// check permission to send sms
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>01/04/2013</date>
        /// <param name="idSender">int trong dictionary - id nguoi gui</param>
        /// <param name="LstIdReceiver">List int trong dictionary - id nguoi nhan</param>
        /// <param name="type">int trong dictionary - loai tin nhan</param>
        /// <param name="classID">int trong dictionary - ma lop</param>
        /// <param name="academicYearID">int trong dictionary - ma nam hoc</param>
        /// <param name="contactGroupID">int trong dictionary - ma nhom lien lac</param>
        /// <returns></returns>
        public bool CheckPermissionToSendSMS(IDictionary<string, object> dic)
        {                
            //doi voi truong hop ban giam hieu thi duoc phep gui tin nhan tat ca cac nhom
            bool Principal = Utils.GetBool(dic, "isPrincipal", false);
            if (Principal) return true; //ban giam hieu
            bool isAdmin = Utils.GetBool(dic, "isAdmin", false);                        
            //doi voi truong hop admin thi duoc phep gui tin nhan cho tat ca cac nhom
            if (isAdmin) return true; //admin truong

            int senderID = Utils.GetInt(dic, "senderID");
            List<int> lstReceiverID = Utils.GetIntList(dic, "lstReceiverID");
            int type = Utils.GetInt(dic, "type");
            int classID = Utils.GetInt(dic, "classID");
            int academicYearID = Utils.GetInt(dic, "academicYearID");
            int contactGroupID = Utils.GetInt(dic, "contactGroupID");
            DateTime? dateTimeNow = Utils.GetDateTime(dic,"dateTime");
           
            switch (type)
            {
                case GlobalConstants.SMS_Communicatoin_Parent_To_Teacher:
                    {
                        dateTimeNow = dateTimeNow.Value.Date;
                        if ((from hts in HeadTeacherSubstitutionBusiness.All
                             where hts.ClassID == classID && hts.AcademicYearID == academicYearID
                             && hts.AssignedDate <= dateTimeNow && hts.EndDate >= dateTimeNow
                             select hts.HeadTeacherID).Count() > 0)
                        {
                            return true;
                        }
                        else if ((from cp in ClassProfileBusiness.All
                                  where cp.ClassProfileID == classID && cp.AcademicYearID == academicYearID && cp.IsActive==true
                                  select cp.HeadTeacherID).Count() > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }                        
                    }
                case GlobalConstants.SMS_Communicatoin_Teacher_To_Parent:
                    {
                        int receiverID;
                        List<ClassProfile> lstClassProfile = ClassProfileBusiness.All.Where(p => p.HeadTeacherID == senderID && p.AcademicYearID == academicYearID
                                                            && p.ClassProfileID == classID).ToList();
                        ClassProfile objClassProfile = null;
                        for (int i = lstReceiverID.Count - 1; i >= 0; i--)
                        {
                            receiverID = lstReceiverID[i];
                            for(int j = lstClassProfile.Count - 1; j>=0; j--)
                            {
                                objClassProfile = lstClassProfile[j];
                                if (objClassProfile.PupilProfiles != null && objClassProfile.PupilProfiles.Where(p => p.PupilProfileID == receiverID).Count() == 0)
                                {
                                    return false;
                                }
                            }
                        }                                                                      

                        List<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(p => p.SubstituedHeadTeacher == senderID
                                                                && p.AcademicYearID == academicYearID && p.ClassID == classID).ToList();
                        HeadTeacherSubstitution objHeadTearcherSubstitution = null;
                        for (int i = lstReceiverID.Count - 1; i >= 0; i--)
                        {
                            receiverID = lstReceiverID[i];
                            for (int j = lstHeadTeacherSubstitution.Count - 1; j >= 0; j--)
                            {
                                objHeadTearcherSubstitution = lstHeadTeacherSubstitution[j];
                                if (objHeadTearcherSubstitution.ClassProfile != null && objHeadTearcherSubstitution.ClassProfile.PupilProfiles != null
                                    && objHeadTearcherSubstitution.ClassProfile.PupilProfiles.Where(p => p.PupilProfileID == receiverID).Count() == 0)
                                {
                                    return false;
                                }
                            }                                
                        }
                        return true;
                    }
                case GlobalConstants.SMS_Communicatoin_Teacher_To_Teacher:
                    {                        
                        if (EmployeeContactBusiness.All.Where(p => p.ContactGroupID == contactGroupID && p.EmployeeID == senderID && p.IsActive == true).Count() > 0)
                        {                           
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                default:
                    break;
            }
            return false;
        }
        #endregion

    }

    #region extend 
    /// <summary>
    /// teacherOutGroupSMS Object - dung de lay list teacher da bi loai khoi nhom lien lac de load tin nhan cu cua giao vien do
    /// </summary>
    /// <auth>HaiVT</auth>
    /// <date>10/06/2013</date>
    public class TeacherOutGroupSMS
    {
        public int ContactGroupID { get; set; }
        public long SenderGroup { get; set; }
        public string ShortContent { get; set; }
    }
    #endregion
}
