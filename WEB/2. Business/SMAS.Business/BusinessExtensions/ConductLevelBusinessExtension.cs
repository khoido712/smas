/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ConductLevelBusiness
    {
        public static int PASS = 1;
        public static int NOT_PASS = 2;

        public IQueryable<ConductLevel> Search(IDictionary<string, object> dic)
        {
            bool? isActive = Utils.GetBool(dic,"IsActive");
            int? appliedLevel = Utils.GetNullableByte(dic, "AppliedLevel");

            var lstConduct = this.ConductLevelRepository.All;

            if (isActive.HasValue) lstConduct = lstConduct.Where(u => u.IsActive == isActive.Value);

            if (appliedLevel.HasValue) lstConduct = lstConduct.Where(u => u.AppliedLevel == appliedLevel.Value);

            return lstConduct;
        }
    }
}
