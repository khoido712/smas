﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Configuration;

namespace SMAS.Business.Business
{
    public partial class ReviewBookPupilBusiness
    {
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        public IQueryable<ReviewBookPupil> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            IQueryable<ReviewBookPupil> iqReviewBookPupil = ReviewBookPupilBusiness.All;
            if (SchoolID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.ClassID == ClassID);
            }
            if (SemesterID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.SemesterID == SemesterID);
            }
            if (lstPupilID.Count > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (lstClassID.Count > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => lstClassID.Contains(p.ClassID));
            }
            return iqReviewBookPupil;
        }

        public IQueryable<ReviewBookPupilBO> GetListReviewPupil(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            bool IsGetStudingAndGraduate = Utils.GetBool(dic, "IsGetStudingAndGraduate", false);
            IQueryable<ReviewBookPupil> iqReviewBookPupil = ReviewBookPupilBusiness.All;
            if (SchoolID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.ClassID == ClassID);
            }
            if (SemesterID > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => p.SemesterID == SemesterID);
            }
            if (lstPupilID.Count > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (lstPupilID.Count > 0)
            {
                iqReviewBookPupil = iqReviewBookPupil.Where(p => lstClassID.Contains(p.ClassID));
            }

            IQueryable<ReviewBookPupilBO> iqueryResult = from a in iqReviewBookPupil
                                                         join b in PupilProfileBusiness.All on a.PupilID equals b.PupilProfileID
                                                         join c in PupilOfClassBusiness.All on b.PupilProfileID equals c.PupilID
                                                         where c.ClassID == a.ClassID
                                                         select new ReviewBookPupilBO
                                                         {
                                                             ReviewBookPupilID = a.ReviewBookPupilID,
                                                             SchoolID = a.SchoolID,
                                                             AcademicYearID = a.AcademicYearID,
                                                             ClassID = a.ClassID,
                                                             PupilID = a.PupilID,
                                                             SemesterID = a.SemesterID,
                                                             CapacityRate = a.CapacityRate,
                                                             CQComment = a.CQComment,
                                                             QualityRate = a.QualityRate,
                                                             CommentAdd = a.CommentAdd,
                                                             RateAndYear = a.RateAndYear,
                                                             RateAdd = a.RateAdd,
                                                             CapacitySummer = a.CapacitySummer,
                                                             QualitySummer = a.QualitySummer,
                                                             PartitionID = a.PartitionID,
                                                             CreateTime = a.CreateTime,
                                                             UpdateTime = a.UpdateTime,
                                                             Genre = b.Genre,
                                                             EthnicID = b.EthnicID,
                                                             PupilStatus = c.Status
                                                         };
            if (IsGetStudingAndGraduate)
            {
                iqueryResult = iqueryResult.Where(p => p.PupilStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || p.PupilStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED);
            }

            return iqueryResult;
        }
        public Stream CreatePupilEvaluationHDGDReport(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int classId = Utils.GetInt(dic["ClassID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);

            //Lay duong dan bao cao
            string reportCode = SystemParamsInFile.REPORT_PUPIL_EVALUATION_HDGD;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string[] arrTemplateName = rp.TemplateName.Split('|');
            string templateName;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                templateName = arrTemplateName[0];
            }
            else
            {
                templateName = arrTemplateName[1];
            }
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templateName;

            //Khoi tao
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Dien thong tin vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            bool isNotShowPupil = academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value;
            string schoolName = school.SchoolName.ToUpper();
            string strAcademicYear = " NĂM HỌC " + academicYear.DisplayTitle;
            string title1 = ("BẢNG TỔNG HỢP ĐÁNH GIÁ HỌC SINH LỚP " + cp.DisplayName).ToUpper();
            string title2 = (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HỌC KỲ I -" + strAcademicYear :
                semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND ? "HỌC KỲ II -" + strAcademicYear : "CUỐI" + strAcademicYear).ToUpper();

            oSheet.SetCellValue("A2", schoolName);
            oSheet.SetCellValue("A3", title1);
            oSheet.SetCellValue("A4", title2);
            //Lay danh sach hoc sinh trong lop
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["ClassID"] = classId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(tmpDic).AddPupilStatus(isNotShowPupil).OrderBy(u => u.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName).ToList();

            //Lay danh sach mon hoc cua lop
            tmpDic = new Dictionary<string, object>();
            tmpDic["ClassID"] = classId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                tmpDic["Semester"] = semester;
            }
            IEnumerable<ClassSubject> lstTmp = ClassSubjectBusiness.SearchBySchool(schoolId, tmpDic).ToList().OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName);

            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            lstClassSubject.AddRange(lstTmp.Where(o => o.IsSubjectVNEN == true && o.IsCommenting != 1));
            lstClassSubject.AddRange(lstTmp.Where(o => o.IsSubjectVNEN == true && o.IsCommenting == 1));
            lstClassSubject.AddRange(lstTmp.Where(o => o.IsSubjectVNEN != true && o.IsCommenting != 1));
            lstClassSubject.AddRange(lstTmp.Where(o => o.IsSubjectVNEN != true && o.IsCommenting == 1));


            //Danh sach cac mon hoc Vnen cua lop
            List<ClassSubject> lstVnenSubject = lstClassSubject.Where(o => o.IsSubjectVNEN == true).ToList();
            List<int> lstVnenSubjectId = lstClassSubject.Where(o => o.IsSubjectVNEN == true).Select(o => o.SubjectID).ToList();
            //Danh sach cac mon khong phai mon Vnen
            List<ClassSubject> lstTT58Subject = lstClassSubject.Where(o => o.IsSubjectVNEN != true).ToList();
            List<int> lstTT58SubjectId = lstClassSubject.Where(o => o.IsSubjectVNEN != true).Select(o => o.SubjectID).ToList();

            //Lay danh gia cac mon cua hoc sinh
            List<TeacherNoteBookSemesterBO> lstNoteBook = (from tnbs in TeacherNoteBookSemesterBusiness.All
                                                           join cs in ClassSubjectBusiness.All.Where(s => s.Last2digitNumberSchool == partition && s.ClassID == classId) on new { tnbs.ClassID, tnbs.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                           where tnbs.SchoolID == schoolId
                                                           && tnbs.AcademicYearID == academicYearId
                                                           && tnbs.ClassID == classId
                                                               //&& tnbs.SemesterID == semester
                                                           && tnbs.PartitionID == partition
                                                           && lstVnenSubjectId.Contains(tnbs.SubjectID)
                                                           select new TeacherNoteBookSemesterBO
                                                           {
                                                               PupilID = tnbs.PupilID,
                                                               SubjectID = tnbs.SubjectID,
                                                               PERIODIC_SCORE_END = tnbs.PERIODIC_SCORE_END,
                                                               PERIODIC_SCORE_END_JUDGLE = tnbs.PERIODIC_SCORE_END_JUDGLE,
                                                               AVERAGE_MARK = tnbs.AVERAGE_MARK,
                                                               AVERAGE_MARK_JUDGE = tnbs.AVERAGE_MARK_JUDGE,
                                                               Rate = tnbs.Rate,
                                                               SemesterID = tnbs.SemesterID,
                                                               isCommenting = cs.IsCommenting
                                                           }).ToList();
            //Lay diem mon hoc theo TT58
            tmpDic = new Dictionary<string, object>();
            tmpDic["ClassID"] = classId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["Semester"] = semester;

            List<SummedUpRecordBO> lstSummedUpRecord;
            lstSummedUpRecord = (from vsur in VSummedUpRecordBusiness.SearchBySchool(schoolId, tmpDic)
                                 join cs in ClassSubjectBusiness.All.Where(s => s.Last2digitNumberSchool == partition && s.ClassID == classId) on new { vsur.ClassID, vsur.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                 where vsur.PeriodID == null
                                 && lstTT58SubjectId.Contains(vsur.SubjectID)
                                 select new SummedUpRecordBO
                                 {
                                     PupilID = vsur.PupilID,
                                     SubjectID = vsur.SubjectID,
                                     SummedUpMark = vsur.SummedUpMark,
                                     JudgementResult = vsur.JudgementResult,
                                     Semester = vsur.Semester,
                                     IsCommenting = cs.IsCommenting
                                 }).ToList();
            //Nang luc pham chat
            IQueryable<ReviewBookPupil> iqReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == schoolId
                && o.AcademicYearID == academicYearId
                && o.ClassID == classId
                && o.PartitionID == partition);
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqReviewBook = iqReviewBook.Where(p => p.SemesterID == semester);
            }
            else
            {
                iqReviewBook = iqReviewBook.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND);
            }
            List<ReviewBookPupil> lstReviewBook = iqReviewBook.ToList();

            //Lay thong tin mien giam
            tmpDic = new Dictionary<string, object>();
            tmpDic["ClassID"] = classId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;

            List<ExemptedSubject> lstExemptedSubjects = ExemptedSubjectBusiness.Search(tmpDic).ToList();

            List<ExemptedSubject> lstExemptedSubjectsSemester1 = lstExemptedSubjects.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
            List<ExemptedSubject> lstExemptedSubjectsSemester2 = lstExemptedSubjects.Where(o => o.SecondSemesterExemptType.HasValue).ToList();


            //Lay thong tin dang ky mon tu chon
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["ClassID"] = classId;

            List<RegisterSubjectSpecialize> lstSubjectSpecializes = RegisterSubjectSpecializeBusiness.Search(tmpDic).ToList();
            List<RegisterSubjectSpecialize> lstSubjectSpecializeSemester1 = lstSubjectSpecializes.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).ToList();
            List<RegisterSubjectSpecialize> lstSubjectSpecializeSemester2 = lstSubjectSpecializes.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).ToList();


            int subjectRow = 6;
            int subjectCol = 4;

            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs = lstClassSubject[i];
                string title;
                if (cs.SubjectCat.IsForeignLanguage)
                {
                    title = "Ngoại ngữ\n(" + cs.SubjectCat.DisplayName + ")";
                }
                else if (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3)
                {
                    title = "Tự chọn\n(" + cs.SubjectCat.DisplayName + ")";
                }
                else
                {
                    title = cs.SubjectCat.DisplayName;
                }

                //if (cs.IsSubjectVNEN == true)
                //{
                //    oSheet.SetCellValue(subjectRow, subjectCol, title);
                //    oSheet.MergeRow(subjectRow, subjectCol, subjectCol + 1);
                //    oSheet.SetCellValue(subjectRow + 1, subjectCol, "Đánh\ngiá");
                //    oSheet.SetCellValue(subjectRow + 1, subjectCol + 1, semester == 1 ? "KT\nCK" : "KT\nCN");
                //    oSheet.SetColumnWidth(subjectCol, 5.8);
                //    oSheet.SetColumnWidth(subjectCol + 1, 3.8);
                //    subjectCol = subjectCol + 2;
                //}
                //else
                //{
                    oSheet.SetCellValue(subjectRow, subjectCol, title);
                    oSheet.MergeColumn(subjectCol, subjectRow, subjectRow + 1);
                    oSheet.SetColumnWidth(subjectCol, 8.3);
                    subjectCol++;
                //}
            }
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                oSheet.SetCellValue(subjectRow, subjectCol, "Kết quả\nhọc tập");
                oSheet.MergeColumn(subjectCol, subjectRow, subjectRow + 1);
                oSheet.SetColumnWidth(subjectCol, 9.5);
                subjectCol++;
            }
            //Add cot nang luc, pham chat
            oSheet.SetCellValue(subjectRow, subjectCol, "Năng\nlực");
            oSheet.MergeColumn(subjectCol, subjectRow, subjectRow + 1);
            oSheet.SetColumnWidth(subjectCol, 5.7);
            subjectCol++;

            oSheet.SetCellValue(subjectRow, subjectCol, "Phẩm\nchất");
            oSheet.MergeColumn(subjectCol, subjectRow, subjectRow + 1);
            oSheet.SetColumnWidth(subjectCol, 5.7);
            subjectCol++;

            if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                oSheet.SetCellValue(subjectRow, subjectCol, "Đánh giá");
                oSheet.MergeColumn(subjectCol, subjectRow, subjectRow + 1);
                oSheet.SetColumnWidth(subjectCol, 8.4);
                subjectCol++;

                oSheet.SetCellValue(subjectRow, subjectCol, "Đánh giá\nbổ sung");
                oSheet.MergeColumn(subjectCol, subjectRow, subjectRow + 1);
                oSheet.SetColumnWidth(subjectCol, 9.5);
                subjectCol++;
            }

            //Format header
            IVTRange headerRange = oSheet.GetRange(subjectRow, 1, subjectRow + 1, subjectCol - 1);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            headerRange.FillColor(System.Drawing.Color.LightGray);

            //fill du lieu
            int startRow = 8;
            int lastRow = startRow + lstPoc.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = subjectCol - 1;
            int curColumn = startColumn;

            DateTime endsemester = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate.Value : academicYear.SecondSemesterEndDate.Value;
            for (int i = 0; i < lstPoc.Count; i++)
            {
                PupilOfClass poc = lstPoc[i];

                //Lay ra cac mon mien giam cua hoc sinh nay
                List<int> lstExemptedSubjectIDSemester1 = lstExemptedSubjectsSemester1.Where(o => o.PupilID == poc.PupilID).Select(o => o.SubjectID).ToList();
                List<int> lstExemptedSubjectIDSemester2 = lstExemptedSubjectsSemester2.Where(o => o.PupilID == poc.PupilID).Select(o => o.SubjectID).ToList();

                //Lay ra cac mon tu chon hoc sinh nay hoc
                List<int> lstSubjectSpecializeIDSemester1 = lstSubjectSpecializeSemester1.Where(o => o.PupilID == poc.PupilID).Select(o => o.SubjectID).ToList();
                List<int> lstSubjectSpecializeIDSemester2 = lstSubjectSpecializeSemester2.Where(o => o.PupilID == poc.PupilID).Select(o => o.SubjectID).ToList();

                bool isShow = poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                isShow = isShow || ((poc.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    && poc.EndDate.HasValue && poc.EndDate.Value > endsemester);

                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //Ma hoc sinh
                oSheet.SetCellValue(curRow, curColumn, poc.PupilProfile.PupilCode);
                curColumn++;
                //Ho ten
                oSheet.SetCellValue(curRow, curColumn, poc.PupilProfile.FullName);
                curColumn++;
                if (isShow)
                {
                    //Fill danh gia cac mon
                    for (int j = 0; j < lstClassSubject.Count; j++)
                    {
                        ClassSubject cs = lstClassSubject[j];

                        //Co phai mon mien giam
                        bool IsExempted;
                        //Co phai mon khong hoc
                        bool IsNotLearning;

                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            IsExempted = lstExemptedSubjectIDSemester1.Contains(cs.SubjectID);
                            IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(cs.SubjectID);

                        }
                        else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            IsExempted = lstExemptedSubjectIDSemester2.Contains(cs.SubjectID);
                            IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester2.Contains(cs.SubjectID);
                        }
                        else
                        {
                            IsExempted = lstExemptedSubjectIDSemester1.Contains(cs.SubjectID) && lstExemptedSubjectIDSemester2.Contains(cs.SubjectID);
                            IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(cs.SubjectID)
                                && !lstSubjectSpecializeIDSemester2.Contains(cs.SubjectID);
                        }

                        string strSpecial = String.Empty;
                        if (IsNotLearning)
                        {
                            strSpecial = "KH";
                        }
                        if (IsExempted)
                        {
                            strSpecial = "MG";
                        }

                        if (cs.IsSubjectVNEN == true)
                        {
                            if (strSpecial == String.Empty)
                            {
                                TeacherNoteBookSemesterBO tnbs;
                                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    tnbs = lstNoteBook.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && o.PupilID == poc.PupilID && o.SubjectID == cs.SubjectID).FirstOrDefault();

                                }
                                else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                {

                                    tnbs = lstNoteBook.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.PupilID == poc.PupilID && o.SubjectID == cs.SubjectID).FirstOrDefault();
                                }
                                else
                                {
                                    tnbs = lstNoteBook.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL && o.PupilID == poc.PupilID && o.SubjectID == cs.SubjectID).FirstOrDefault();
                                }

                                if (tnbs != null)
                                {
                                    //Mon nhan xet
                                    if (cs.IsCommenting == 1)
                                    {
                                        oSheet.SetCellValue(curRow, curColumn, !string.IsNullOrEmpty(tnbs.AVERAGE_MARK_JUDGE) ? tnbs.AVERAGE_MARK_JUDGE : "CN");
                                    }
                                    //Mon tinh diem
                                    else
                                    {
                                        oSheet.SetCellValue(curRow, curColumn, tnbs.AVERAGE_MARK.HasValue ? (tnbs.AVERAGE_MARK != 0 && tnbs.AVERAGE_MARK != 10 ? String.Format("{0:0.0}", tnbs.AVERAGE_MARK) : String.Format("{0:0}", tnbs.AVERAGE_MARK)) : "CN");
                                    }
                                }
                            }
                            else
                            {
                                oSheet.SetCellValue(curRow, curColumn, strSpecial);
                            }

                            curColumn++;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(strSpecial))
                            {
                                SummedUpRecordBO sur = lstSummedUpRecord.Where(o => o.PupilID == poc.PupilID && o.SubjectID == cs.SubjectID).FirstOrDefault();
                                if (sur != null)
                                {
                                    if (cs.IsCommenting == 1)
                                    {
                                        oSheet.SetCellValue(curRow, curColumn, !string.IsNullOrEmpty(sur.JudgementResult) ? sur.JudgementResult : "CN");
                                    }
                                    else
                                    {
                                        oSheet.SetCellValue(curRow, curColumn, sur.SummedUpMark.HasValue ? (sur.SummedUpMark != 0 && sur.SummedUpMark != 10 ? String.Format("{0:0.0}", sur.SummedUpMark) : String.Format("{0:0}", sur.SummedUpMark)) : "CN");
                                    }
                                }
                                else
                                {
                                    oSheet.SetCellValue(curRow, curColumn, "CN");
                                }
                            }
                            else
                            {
                                oSheet.SetCellValue(curRow, curColumn, strSpecial);
                            }

                            curColumn++;
                        }

                    }

                    //Fill nang luc, pham chat, danh gia, gianh gia bo sung
                    ReviewBookPupil rbp = lstReviewBook.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();
                    if (rbp != null)
                    {
                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                        {
                            string strStudyResult = string.Empty;
                            if (rbp.StudyResult.HasValue)
                            {
                                if (rbp.StudyResult.Value == 1)
                                {
                                    strStudyResult = "HTT";
                                }
                                else if (rbp.StudyResult.Value == 2)
                                {
                                    strStudyResult = "HT";
                                }
                                else if (rbp.StudyResult.Value == 3)
                                {
                                    strStudyResult = "CHT";
                                }
                            }
                            oSheet.SetCellValue(curRow, curColumn, strStudyResult);
                            curColumn++;
                        }
                        int? capacity = rbp.CapacityRate;
                        if (rbp.CapacitySummer != null)
                        {
                            capacity = rbp.CapacitySummer;
                        }
                        string strCapacity = "";
                        if (capacity == 1)
                        {
                            strCapacity = "T";
                        }
                        else if (capacity == 2)
                        {
                            strCapacity = "Đ";
                        }
                        else if (capacity == 3)
                        {
                            strCapacity = "C";
                        }
                        oSheet.SetCellValue(curRow, curColumn, strCapacity);
                        curColumn++;

                        int? quality = rbp.QualityRate;
                        if (rbp.QualitySummer != null)
                        {
                            quality = rbp.QualitySummer;
                        }
                        string strQuality = "";
                        if (quality == 1)
                        {
                            strQuality = "T";
                        }
                        else if (quality == 2)
                        {
                            strQuality = "Đ";
                        }
                        else if (quality == 3)
                        {
                            strQuality = "C";
                        }
                        oSheet.SetCellValue(curRow, curColumn, strQuality);
                        curColumn++;

                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                        {
                            string RateEndYear = "";
                            if (rbp.RateAndYear == 1)
                            {
                                RateEndYear = "HT";
                            }
                            else if (rbp.RateAndYear == 2)
                            {
                                RateEndYear = "CHT";
                            }
                            oSheet.SetCellValue(curRow, curColumn, RateEndYear);
                            curColumn++;

                            string RateAdd = "";
                            if (rbp.RateAdd == 1)
                            {
                                RateAdd = "HT";
                            }
                            else if (rbp.RateAdd == 2)
                            {
                                RateAdd = "CHT";
                            }
                            oSheet.SetCellValue(curRow, curColumn, RateAdd);
                            curColumn++;
                        }
                    }
                }
                //Ve khung cho dong
                IVTRange range = oSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                if (poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING && poc.Status != GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    oSheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);

                }

                curRow++;
                curColumn = startColumn;
            }

            if (lstPoc.Count > 0)
            {
                IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);

            }

            oSheet.GetRange(3, 1, 3, lastColumn).Merge();
            oSheet.GetRange(4, 1, 4, lastColumn).Merge();
            oSheet.GetRange(5, 1, 5, lastColumn).Merge();
            oSheet.GetRange(5, 1, 5, lastColumn).SetHAlign(VTHAlign.xlHAlignRight);

            oSheet.Orientation = VTXPageOrientation.VTxlLandscape;
            oSheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }

        public ProcessedReport InsertPupilEvaluationHDGDReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_PUPIL_EVALUATION_HDGD;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            string outputNamePattern = reportDef.OutputNamePattern;

            string className = "";
            ClassProfile cp = ClassProfileBusiness.Find(Utils.GetInt(dic, "ClassID"));
            if (cp != null)
            {
                className = cp.DisplayName;
            }

            string semester;
            if (Utils.GetInt(dic, "Semester") == 1)
            {
                semester = "HK1";
            }
            else
            {
                semester = "CN";
            }

            outputNamePattern = outputNamePattern.Replace("[Class]", className);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return pr;
        }

        public Stream CreatePupilEvaluationNLPCReport(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int classId = Utils.GetInt(dic["ClassID"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);

            //Lay duong dan bao cao
            string reportCode = SystemParamsInFile.REPORT_PUPIL_EVALUATION_NLPC;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;

            //Khoi tao
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Dien thong tin vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            bool isNotShowPupil = academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value;
            string schoolName = school.SchoolName.ToUpper();
            string strAcademicYear = " NĂM HỌC " + academicYear.DisplayTitle;
            string title1 = ("BẢNG TỔNG HỢP NHẬN XÉT ĐANH GIÁ NĂNG LỰC, PHẨM CHẤT - LỚP " + cp.DisplayName).ToUpper();
            string title2 = (semester == 1 ? "HỌC KỲ I -" + strAcademicYear : "HỌC KỲ II -" + strAcademicYear).ToUpper();

            oSheet.SetCellValue("A2", schoolName);
            oSheet.SetCellValue("A3", title1);
            oSheet.SetCellValue("A4", title2);

            //Lay danh sach hoc sinh trong lop
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = cp.EducationLevelID;
            tmpDic["ClassID"] = classId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic).AddPupilStatus(isNotShowPupil).OrderBy(u => u.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName).ToList();

            //Lay thong tin danh gia nang luc pham chat

            List<ReviewBookPupil> lstReviewBook = repository.All
                .Where(o => o.SchoolID == schoolId
                && o.AcademicYearID == academicYearId
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition).ToList();

            //Lay thong tin nhan xet cac mon 
            int monthId = semester == 1 ? 15 : 16;
            List<TeacherNoteBookMonthBO> lstTeacherNoteBookMonth = (from tnbm in TeacherNoteBookMonthBusiness.All
                                                                    join sc in SubjectCatBusiness.All on tnbm.SubjectID equals sc.SubjectCatID
                                                                    where tnbm.SchoolID == schoolId
                                                                    && tnbm.AcademicYearID == academicYearId
                                                                    && tnbm.ClassID == classId
                                                                    && tnbm.MonthID == monthId
                                                                    select new TeacherNoteBookMonthBO
                                                                    {
                                                                        PupilID = tnbm.PupilID,
                                                                        SubjectID = tnbm.SubjectID,
                                                                        SubjectName = sc.DisplayName,
                                                                        CommentCQ = tnbm.CommentCQ
                                                                    }).ToList();

            //fill du lieu
            int startRow = 7;
            int lastRow = startRow + lstPoc.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 7;
            int curColumn = startColumn;

            DateTime endsemester = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate.Value : academicYear.SecondSemesterEndDate.Value;
            for (int i = 0; i < lstPoc.Count; i++)
            {
                PupilOfClass poc = lstPoc[i];
                bool isShow = poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                isShow = isShow || ((poc.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    && poc.EndDate.HasValue && poc.EndDate.Value > endsemester);

                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //Ma hoc sinh
                oSheet.SetCellValue(curRow, curColumn, poc.PupilProfile.PupilCode);
                curColumn++;
                //Ho ten
                oSheet.SetCellValue(curRow, curColumn, poc.PupilProfile.FullName);
                curColumn++;

                if (isShow)
                {
                    ReviewBookPupil rbp = lstReviewBook.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();
                    string cqComment = String.Empty;
                    if (rbp != null)
                    {
                        cqComment = rbp.CQComment;
                    }
                    if (String.IsNullOrEmpty(cqComment))
                    {
                        List<TeacherNoteBookMonthBO> lstTnbOfPupil = lstTeacherNoteBookMonth.Where(o => o.PupilID == poc.PupilID).ToList();
                        string comment = String.Empty;
                        for (int j = 0; j < lstTnbOfPupil.Count; j++)
                        {
                            TeacherNoteBookMonthBO tnbm = lstTnbOfPupil[j];
                            if (!string.IsNullOrEmpty(tnbm.CommentCQ))
                            {
                                comment += tnbm.SubjectName + ": " + tnbm.CommentCQ;
                                if (j < lstTnbOfPupil.Count - 1)
                                {
                                    comment += "\n";
                                }
                            }
                        }
                        cqComment = comment;
                    }

                    //bieu hien noi bat...
                    oSheet.SetCellValue(curRow, curColumn, cqComment);
                    curColumn++;

                    if (rbp != null)
                    {
                        //Nhung noi dung chua hoan thanh...
                        oSheet.SetCellValue(curRow, curColumn, rbp.CommentAdd);
                        curColumn++;

                        string strCapacity = "";
                        if (rbp.CapacityRate == 1)
                        {
                            strCapacity = "T";
                        }
                        else if (rbp.CapacityRate == 2)
                        {
                            strCapacity = "Đ";
                        }
                        else if (rbp.CapacityRate == 3)
                        {
                            strCapacity = "C";
                        }
                        oSheet.SetCellValue(curRow, curColumn, strCapacity);
                        curColumn++;

                        string strQuality = "";
                        if (rbp.QualityRate == 1)
                        {
                            strQuality = "T";
                        }
                        else if (rbp.QualityRate == 2)
                        {
                            strQuality = "Đ";
                        }
                        else if (rbp.QualityRate == 3)
                        {
                            strQuality = "C";
                        }
                        oSheet.SetCellValue(curRow, curColumn, strQuality);
                        curColumn++;
                    }
                }
                //Ve khung cho dong
                IVTRange range = oSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                if (poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING && poc.Status != GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    oSheet.GetRange(curRow, startColumn, curRow, lastColumn).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);

                }

                curRow++;
                curColumn = startColumn;
            }

            if (lstPoc.Count > 0)
            {
                IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.Around);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);

            }

            //Format header
            IVTRange headerRange = oSheet.GetRange(6, 1, 6, 7);
            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            headerRange.FillColor(System.Drawing.Color.LightGray);

            oSheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }

        public ProcessedReport InsertPupilEvaluationNLPCReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_PUPIL_EVALUATION_NLPC;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            string outputNamePattern = reportDef.OutputNamePattern;

            string className = "";
            ClassProfile cp = ClassProfileBusiness.Find(Utils.GetInt(dic, "ClassID"));
            if (cp != null)
            {
                className = cp.DisplayName;
            }

            string semester;
            if (Utils.GetInt(dic, "Semester") == 1)
            {
                semester = "HK1";
            }
            else
            {
                semester = "HK2";
            }

            outputNamePattern = outputNamePattern.Replace("[Class]", className);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();

            return pr;
        }

        public List<PupilRepeaterBO> GetListRepeatPupilOfSchool(int academicYearId, int schoolId)
        {
            int partition=UtilsBusiness.GetPartionId(schoolId);
            var query = from rbp in ReviewBookPupilRepository.All
                        join poc in PupilOfClassBusiness.All on new { rbp.PupilID, rbp.ClassID } equals new { poc.PupilID, poc.ClassID }
                        join cp in ClassProfileRepository.All on rbp.ClassID equals cp.ClassProfileID
                        join pp in PupilProfileRepository.All on rbp.PupilID equals pp.PupilProfileID
                        where rbp.AcademicYearID == academicYearId
                        && rbp.PartitionID == partition
                        && rbp.SchoolID == schoolId
                        && (rbp.RateAndYear == 2 && rbp.RateAdd == 2)
                        && cp.EducationLevel.Grade == 2
                        && poc.AcademicYearID == academicYearId
                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                        && cp.IsActive == true
                        select new PupilRepeaterBO
                        {
                            PupilID = rbp.PupilID,
                            PupilFullName = pp.FullName,
                            PupilShortName = pp.Name,
                            ClassID = rbp.ClassID,
                            ClassName = cp.DisplayName,
                            EducationLevelID = cp.EducationLevelID,
                            Capacity = rbp.CapacityRate,
                            CapacityAdd = rbp.CapacitySummer,
                            Quality = rbp.QualityRate,
                            QualityAdd = rbp.QualitySummer,
                            RateAdd = rbp.RateAdd,
                            RateEndYear = rbp.RateAndYear,
                            OrderClass = cp.OrderNumber,
                            OrderID = poc.OrderInClass
                        };

            return query.ToList();
        }
    }
}
