﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.Threading.Tasks;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ExamDetachableBagBusiness
    {
        public IQueryable<ExamDetachableBag> Search(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examPupilID = Utils.GetLong(search, "ExamPupilID");
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examCandenceBagID = Utils.GetLong(search, "ExamCandenceBagID");
            int lastDigitSchoolID = Utils.GetInt(search, "LastDigitSchoolID");
            string examDetachableBagCode = Utils.GetString(search, "ExamDetachableBagCode");

            IQueryable<ExamDetachableBag> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examPupilID != 0)
            {
                query = query.Where(o => o.ExamPupilID == examPupilID);
            }
            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examCandenceBagID != 0)
            {
                query = query.Where(o => o.ExamCandenceBagID == examCandenceBagID);
            }
            if (lastDigitSchoolID != 0)
            {
                query = query.Where(o => o.LastDigitSchoolID == lastDigitSchoolID);
            }
            if (examDetachableBagCode != string.Empty)
            {
                query = query.Where(o => o.ExamDetachableBagCode == examDetachableBagCode);
            }

            return query;
        }

        public void InsertAllExamDetachableBag(List<ExamCandenceBag> lstExamCandenceBagDelete, List<ExamCandenceBag> lstExamCandenceBagInsert, List<ExamDetachableBag> lstExamExamDetachableBagDelete, List<ExamDetachableBag> lstExamDetachableBagInsert)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstExamCandenceBagDelete != null && lstExamCandenceBagDelete.Count > 0)
                {
                    ExamCandenceBagBusiness.DeleteAll(lstExamCandenceBagDelete);
                }
                if (lstExamCandenceBagInsert != null && lstExamCandenceBagInsert.Count > 0)
                {
                    for (int i = 0; i < lstExamCandenceBagInsert.Count; i++)
                    {
                        ExamCandenceBagBusiness.Insert(lstExamCandenceBagInsert[i]);
                    }
                }

                if ((lstExamCandenceBagDelete != null && lstExamCandenceBagDelete.Count > 0)
                    || (lstExamCandenceBagInsert != null && lstExamCandenceBagInsert.Count > 0))
                {
                    ExamCandenceBagBusiness.Save();
                }
                if (lstExamExamDetachableBagDelete != null && lstExamExamDetachableBagDelete.Count > 0)
                {
                    ExamDetachableBagBusiness.DeleteAll(lstExamExamDetachableBagDelete);
                }

                if (lstExamDetachableBagInsert != null && lstExamDetachableBagInsert.Count > 0)
                {
                    for (int i = 0; i < lstExamDetachableBagInsert.Count; i++)
                    {
                        ExamDetachableBagBusiness.Insert(lstExamDetachableBagInsert[i]);
                    }
                }
                if ((lstExamExamDetachableBagDelete != null && lstExamExamDetachableBagDelete.Count > 0)
                    || (lstExamDetachableBagInsert != null && lstExamDetachableBagInsert.Count > 0))
                {
                    ExamDetachableBagBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertAllExamDetachableBag","null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        public void DeleteAllExamDetachableBag(List<ExamCandenceBag> lstExamCandenceBagDelete, List<ExamDetachableBag> lstExamExamDetachableBagDelete)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstExamExamDetachableBagDelete != null && lstExamExamDetachableBagDelete.Count > 0)
                {
                    ExamDetachableBagBusiness.DeleteAll(lstExamExamDetachableBagDelete);
                    ExamDetachableBagBusiness.Save();
                }

                if (lstExamCandenceBagDelete != null && lstExamCandenceBagDelete.Count > 0)
                {
                    ExamCandenceBagBusiness.DeleteAll(lstExamCandenceBagDelete);
                    ExamCandenceBagBusiness.Save();
                }

            }
            catch (Exception ex)
            {

                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteAllExamDetachableBag", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;

            }
        }

        public void InsertDeleteExamCandenceBag(List<ExamCandenceBag> lstExamCandenceBagDelete, List<ExamCandenceBag> lstExamCandenceBagInsert)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstExamCandenceBagDelete != null && lstExamCandenceBagDelete.Count > 0)
                {
                    ExamCandenceBagBusiness.DeleteAll(lstExamCandenceBagDelete);
                }
                ExamCandenceBagBusiness.Save();

                if (lstExamCandenceBagInsert != null && lstExamCandenceBagInsert.Count > 0)
                {
                    ExamCandenceBagBusiness.BulkInsert(lstExamCandenceBagInsert, this.ExamCandenceBagMapping(), "EXAM_CANDENCE_BAG_ID");
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertDeleteExamCandenceBag", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        public void InsertDeleteExamDetachableBag(List<ExamDetachableBag> lstExamExamDetachableBagDelete, List<ExamDetachableBag> lstExamDetachableBagInsert)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstExamExamDetachableBagDelete != null && lstExamExamDetachableBagDelete.Count > 0)
                {
                    ExamDetachableBagBusiness.DeleteAll(lstExamExamDetachableBagDelete);
                }
                ExamDetachableBagBusiness.Save();

                if (lstExamDetachableBagInsert != null && lstExamDetachableBagInsert.Count > 0)
                {
                    ExamDetachableBagBusiness.BulkInsert(lstExamDetachableBagInsert, this.ExamDetachedBagMapping(), "EXAM_DETACHABLE_BAG_ID");
                }
                //if ((lstExamExamDetachableBagDelete != null && lstExamExamDetachableBagDelete.Count > 0)
                //    || (lstExamDetachableBagInsert != null && lstExamDetachableBagInsert.Count > 0))
                //{
                //    ExamDetachableBagBusiness.Save();
                //}
            }
            catch (Exception ex)
            {
                
                string paramList = "List<ExamDetachableBag> lstExamExamDetachableBagDelete, List<ExamDetachableBag> lstExamDetachableBagInsert";
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertDeleteExamDetachableBag",paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        #region Bao cao danh sach thi sinh theo phong thi
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamDetachableBagReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamDetachableBagReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long subjectID = Utils.GetInt(dic["SubjectID"]);

            //L?y đư?ng d?n báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_SO_PHACH;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Kh?i t?o
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //L?y ra sheet đ?u tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học: " + academicYear.DisplayTitle;
            string examGroupName = examGroup != null ? examGroup.ExamGroupName : String.Empty;
            string subjectName = subject != null ? subject.DisplayName : String.Empty;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);
            firstSheet.SetCellValue("C4", provinceAndDate);
            firstSheet.SetCellValue("A7", "NHÓM THI: " + examGroupName + " - MÔN THI: " + subjectName);

            //Lay danh sach tui phach

            List<ExamCandenceBag> listExamCandenceBag = ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                               .Where(o => o.ExamGroupID == examGroupID)
                                                               .Where(o => o.SubjectID == subjectID)
                                                               .OrderBy(o => o.ExamCandenceBagCode)
                                                               .ToList();

            if (listExamCandenceBag.Count == 0)
            {
                throw new BusinessException("ExamDetachableBag_Report_Validate");
            }

            //Danh sach phach
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            //Tim kiem 
            List<ExamPupilBO> listExamPupil = this.ExamPupilBusiness.Search(dic, academicYearID, partitionID).ToList();


            //Lay danh sach danh phach
            List<ExamDetachableBag> listDetachable = this.ExamDetachableBagBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                                            && o.ExamGroupID == examGroupID
                                                                                            && o.SubjectID == subjectID)
                                                                                            .ToList();
            List<ExamDetachableBagBO> listExamDetachableBag = (from ep in listExamPupil
                                                               join ed in listDetachable
                                                                      on ep.ExamPupilID equals ed.ExamPupilID into des
                                                               from x in des.DefaultIfEmpty()
                                                               select new ExamDetachableBagBO
                                                               {
                                                                   ExamineeNumber = ep.ExamineeNumber,
                                                                   ExamDetachableBagCode = x != null ? x.ExamDetachableBagCode : String.Empty,
                                                                   ExamDetachableBagID = x != null ? x.ExamDetachableBagID : 0,
                                                                   ExamCandenceBagID = x != null ? x.ExamCandenceBagID : 0
                                                               }).ToList();

            //Tao tung sheet cho moi tui phach
            for (int i = 0; i < listExamCandenceBag.Count; i++)
            {
                ExamCandenceBag examCandenceBag = listExamCandenceBag[i];

                //Fill du lieu
                //Danh sach thi sinh cua phong thi
                List<ExamDetachableBagBO> listResult = listExamDetachableBag.Where(o => o.ExamCandenceBagID == examCandenceBag.ExamCandenceBagID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 11;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 4;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "D" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = examCandenceBag.ExamCandenceBagCode;
                curSheet.SetCellValue("A8", "Túi phách: " + examCandenceBag.ExamCandenceBagCode);

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamDetachableBagBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //So phach
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamDetachableBagCode);
                    curColumn++;
                    //Ghi chu
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                if (listResult.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                curSheet.FitAllColumnsOnOnePage = true;
            }

            //Xoa sheet dau tien
            firstSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamDetachableBagReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_SO_PHACH;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //T?o tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            ExamGroup eg = ExamGroupBusiness.Find(examGroupID);
            string examGroupCode = eg != null ? eg.ExamGroupCode : String.Empty;

            int subjectID = Utils.GetInt(dic["SubjectID"]);
            SubjectCat sc = SubjectCatBusiness.Find(subjectID);
            string subjectName = sc != null ? sc.DisplayName : String.Empty;

            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", examGroupCode);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion
        public Stream CreateTemplateDemo(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long subjectID = Utils.GetInt(dic["SubjectID"]);
            int typeExportID = Utils.GetInt(dic["TypeExportID"]);
            bool isExamCandenceBag = Utils.GetBool(dic["isExamCandenceBag"]);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/THI" + "/" + "THI_DanhPhach_ThiHocKy1.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            List<ExamGroup> lstEG = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID).ToList();
            ExamGroup examGroup = null;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName.ToUpper());
            firstSheet.SetCellValue("A3", schoolName.ToUpper());
            firstSheet.SetCellValue("A4", examinationsName.ToUpper());
            firstSheet.SetCellValue("A5", strAcademicYear);
            firstSheet.SetCellValue("C4", provinceAndDate);

            //lay danh sach SBD can danh phach
            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            //Tim kiem 
            List<long> lstExamGroupID = lstEG.Select(p => p.ExamGroupID).Distinct().ToList();
            //Tim kiem 
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["SchoolID"] = schoolID;
            dicSearch["ExaminationsID"] = examinationsID;
            dicSearch["lstExamGroupID"] = lstExamGroupID;
            //Tim kiem 
            List<ExamPupilBO> listExamPupil = this.ExamPupilBusiness.Search(dicSearch, academicYearID, partitionID).ToList();
            //Lay danh sach danh phach
            List<ExamDetachableBag> listDetachable = this.ExamDetachableBagBusiness.All.Where(o => o.ExaminationsID == examinationsID
                                                                                            && lstExamGroupID.Contains(o.ExamGroupID)
                                                                                            && o.SubjectID == subjectID)
                                                                                            .ToList();

            List<ExamPupilBO> list1 = (from ep in listExamPupil
                                       join ed in listDetachable
                                              on ep.ExamPupilID equals ed.ExamPupilID into des
                                       from x in des.DefaultIfEmpty()
                                       select new ExamPupilBO
                                         {
                                             ExamGroupID = ep.ExamGroupID,
                                             ExamineeNumber = ep.ExamineeNumber,
                                             ExamRoomID = ep.ExamRoomID,
                                             ExamDetachableBagCode = x != null ? x.ExamDetachableBagCode : String.Empty,
                                             ExamDetachableBagID = x != null ? x.ExamDetachableBagID : 0
                                         }).OrderBy(p => p.ExamineeNumber).ToList();

            //Lay danh sach tui bai thi cua ky thi
            List<ExamBagBO> listExamBagOfExaminations = ExamBagBusiness.GetExamBagOfExaminations(examinationsID).Where(p=>p.SubjectID == subjectID).ToList();

            IDictionary<string, object> dicEcb = new Dictionary<string, object>()
            {
                {"ExaminationsID",examinationsID},
                {"lstExamGroupID",lstExamGroupID},
                {"SubjectID",subjectID}
            };
            List<ExamPupilBO> lsttmp = null;
            if (typeExportID == 1 || typeExportID == 2)//nhom thi hien tai
            {
                lsttmp = list1.Where(p => p.ExamGroupID == examGroupID).ToList();
                examGroup = lstEG.Where(p => p.ExamGroupID == examGroupID).FirstOrDefault();
                firstSheet.SetCellValue("F2", examGroupID);
                firstSheet.HideColumn(6);
                this.SetValueToFile(firstSheet, lsttmp, listExamBagOfExaminations, examGroup, isExamCandenceBag);
            }
            else//Tat ca mon thi va nhom thi
            {
                for (int i = 0; i < lstEG.Count; i++)
                {
                    examGroup = lstEG[i];
                    lsttmp = list1.Where(p => p.ExamGroupID == examGroup.ExamGroupID).ToList();
                    examGroup = lstEG.Where(p => p.ExamGroupID == examGroup.ExamGroupID).FirstOrDefault();
                    IVTWorksheet sheettmp = oBook.CopySheetToLast(firstSheet, "Z1000");
                    sheettmp.SetCellValue("F2", examGroup.ExamGroupID);
                    sheettmp.HideColumn(6);
                    this.SetValueToFile(sheettmp, lsttmp, listExamBagOfExaminations, examGroup, isExamCandenceBag);
                }
                firstSheet.Delete();
            }
            return oBook.ToStream();
        }
        private void SetValueToFile(IVTWorksheet sheet, List<ExamPupilBO> lstExamPupilBO, List<ExamBagBO> lstExamBag, ExamGroup ExamGroup, bool isChecked)
        {
            sheet.SetCellValue("A7", "NHÓM THI: " + ExamGroup.ExamGroupName);
            int startRow = 10;
            ExamPupilBO objExamPupilBO = null;
            ExamBagBO objEB = null;
            for (int i = 0; i < lstExamPupilBO.Count; i++)
            {
                objExamPupilBO = lstExamPupilBO[i];
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objExamPupilBO.ExamineeNumber);
                if (isChecked)
                {
                    objEB = lstExamBag.Where(p => p.ExamGroupID == objExamPupilBO.ExamGroupID && p.ExamRoomID == objExamPupilBO.ExamRoomID).FirstOrDefault();
                    sheet.SetCellValue(startRow, 3, objEB != null ? objEB.ExamBagCode : "");
                }
                sheet.SetCellValue(startRow, 4, objExamPupilBO.ExamDetachableBagCode);
                startRow++;
            }
            sheet.GetRange(10, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.Name = Utils.StripVNSignAndSpace(ExamGroup.ExamGroupName);
        }
        public IDictionary<string, object> ExamCandenceBagMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamCandenceBagID", "EXAM_CANDENCE_BAG_ID");
            columnMap.Add("ExamCandenceBagCode", "EXAM_CANDENCE_BAG_CODE");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }
        public IDictionary<string, object> ExamDetachedBagMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamDetachableBagID", "EXAM_DETACHABLE_BAG_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExamCandenceBagID", "EXAM_CANDENCE_BAG_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ExamDetachableBagCode", "EXAM_DETACHABLE_BAG_CODE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }
    }
}