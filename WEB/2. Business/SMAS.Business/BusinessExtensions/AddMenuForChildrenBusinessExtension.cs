﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class AddMenuForChildrenBusiness
    {

        #region Search

        /// <summary>      
        /// </summary>
        /// <author>trangdd</author>
        /// <date>20/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        private IQueryable<AddMenuForChildren> Search(IDictionary<string, object> dic)
        {
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            int Genre = Utils.GetInt(dic, "Genre", -1);
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int WeeklyMenuID = Utils.GetInt(dic, "WeeklyMenuID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            IQueryable<AddMenuForChildren> query = AddMenuForChildrenRepository.All;

            if (Genre != -1)
            {
                query = query.Where(pp => (pp.PupilProfile.Genre == Genre));

            }
            if (PupilCode.Trim().Length != 0)
            {
                query = query.Where(pp => pp.PupilProfile.PupilCode.Contains(PupilCode));
            }
            if (FullName.Trim().Length != 0)
            {
                query = query.Where(pp => pp.PupilProfile.FullName.Contains(FullName));
            }
            if (SchoolID != 0)
            {
                query = query.Where(pp => pp.SchoolID == SchoolID);
            }
            if (WeeklyMenuID != 0)
            {
                query = query.Where(pp => pp.WeeklyMenuID == WeeklyMenuID);
            }
            if (ClassID != 0)
            {
                query = query.Where(pp => pp.ClassID == ClassID);
            }
            if (PupilID != 0)
            {
                query = query.Where(pp => pp.PupilID == PupilID);
            }
            if (AcademicYearID != 0)
            {
                query = query.Where(pp => pp.AcademicYearID == AcademicYearID);
            }
            return query;

        }
        #endregion
        #region Search by School
        public IQueryable<AddMenuForChildren> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }
        #endregion
        public void Validate(List<int> ListPupilOfClassID, int SchoolID, int WeeklyMenuID)
        {
            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);

            for (int i = 0; i < ListPupilOfClassID.Count; i++)
            {
                PupilProfile PupilProfile = PupilProfileBusiness.Find(ListPupilOfClassID[i]);

                //SchoolID: PK (SchoolProfile)
                SchoolProfileBusiness.CheckAvailable(SchoolID, "AddMenuForChildren_Label_School", true);
                //PupilOfClassID: PK(PupilOfClass)
                //PupilOfClassBusiness.CheckAvailable(ListPupilOfClassID[i], "AddMenuForChildren_Label_Pupil", true);

                //SchoolID, ClassID(ClassProfileID): not compatible(ClassProfile)
                bool ClassCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"ClassProfileID",PupilProfile.CurrentClassID}
                }, null);
                if (!ClassCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //AcademicYearID, ClassID (ClassProfile): not compatible(ClassProfile)
                bool AcademicYearCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"AcademicYearID",WeeklyMenu.AcademicYearID},
                    {"ClassProfileID",PupilProfile.CurrentClassID}
                }, null);
                if (!AcademicYearCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //EatingGroupID, ClassID: not compatible(EatingGroupClass)
                bool EatingGroupCompatible = new EatingGroupClassRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EatingGroupClass",
                new Dictionary<string, object>()
                {
                    {"EatingGroupID",WeeklyMenu.EatingGroupID},
                    {"ClassID",PupilProfile.CurrentClassID}
                }, null);
                if (!EatingGroupCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //PupilID, WeeklyMenu : duplicate(AddMenuForChildren)
                bool WeeklyMenuduplicate = new AddMenuForChildrenRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "AddMenuForChildren",
                     new Dictionary<string, object>()
                {
                    {"PupilID",ListPupilOfClassID[i]},
                    {"WeeklyMenuID",WeeklyMenuID},                    
                },
                       new Dictionary<string, object>()
                 {
                    {"AddMenuForChildrenID",0},                    
                });
                if (WeeklyMenuduplicate)
                {
                    throw new BusinessException("Common_Validate_NotDuplicate");
                }
                //WeeklyMenu (WeeklyMenuID).MenuType (lấy trong CSDL) chỉ được nhận giá trị MENU_TYPE_ADDITIONAL
                if (WeeklyMenu.MenuType != SystemParamsInFile.MENU_TYPE_ADDITIONAL)
                {
                    throw new BusinessException("Common_Validate_MENU_TYPE_ADDITIONAL");
                }
                //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại                
                if (!AcademicYearBusiness.IsCurrentYear(WeeklyMenu.AcademicYearID))
                {
                    throw new BusinessException("Common_Validate_IsCurrentYear");
                }

            }
        }
        #region Insert All
        public void InsertAll(List<int> ListPupilOfClassID, int SchoolID, int WeeklyMenuID)
        {
            Validate(ListPupilOfClassID, SchoolID, WeeklyMenuID);
            //Insert
            WeeklyMenu WeeklyMenu = WeeklyMenuBusiness.Find(WeeklyMenuID);
            for (int i = 0; i < ListPupilOfClassID.Count; i++)
            {
                PupilProfile PupilProfile = PupilProfileBusiness.Find(ListPupilOfClassID[i]);

                AddMenuForChildren AddMenuForChildren = new AddMenuForChildren();
                AddMenuForChildren.PupilID = ListPupilOfClassID[i];
                AddMenuForChildren.WeeklyMenuID = WeeklyMenuID;
                AddMenuForChildren.ClassID = PupilProfile.CurrentClassID;
                AddMenuForChildren.SchoolID = SchoolID;
                AddMenuForChildren.AcademicYearID = WeeklyMenu.AcademicYearID;
                base.Insert(AddMenuForChildren);
            }
        }
        #endregion

        #region Delete All
        public void DeleteAll(List<int> ListAddMenuForChildrenID, int SchoolID)
        {
            for (int i = 0; i < ListAddMenuForChildrenID.Count(); i++)
            {
                AddMenuForChildren AddMenuForChildren = this.Find(ListAddMenuForChildrenID[i]);
                //SchoolID: PK (SchoolProfile)
                SchoolProfileBusiness.CheckAvailable(SchoolID, "AddMenuForChildren_Label_School", true);
                //PupilOfClassID: PK(PupilOfClass)
                //PupilOfClassBusiness.CheckAvailable(AddMenuForChildren.PupilID, "AddMenuForChildren_Label_Pupil", true);
                //AddMenuForChildrenID: PK(AddMenuForChildren)
                this.CheckAvailable(ListAddMenuForChildrenID[i], "AddMenuForChildren_Label_AddMenuForChildren", true);

                //SchoolID, AddMenuForChildrenID: not compatible(AddMenuForChildren)
                bool SchoolCompatible = new AddMenuForChildrenRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "AddMenuForChildren",
               new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AddMenuForChildrenID",ListAddMenuForChildrenID[i]}
                }, null);
                if (!SchoolCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(AddMenuForChildren.AcademicYearID))
                {
                    throw new BusinessException("Common_Validate_IsCurrentYear");
                }
            }
            for (int i = 0; i < ListAddMenuForChildrenID.Count(); i++)
            {
                base.Delete(ListAddMenuForChildrenID[i]);
            }            

        }
        #endregion
    }
}