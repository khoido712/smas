/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{

        
    public partial class ProvinceBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int PROVINCECODE_MAX_LENGTH = 10; //Độ dài mã tỉnh thành phố
        private const int PROVINCENAME_MAX_LENGTH = 50; //Độ dài tên tỉnh thành phố
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn

        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Tỉnh/Thành phố
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ProvinceName"> Tên tỉnh/thành phố</param>
        /// <param name="ProvinceCode">Mã tỉnh/thành phố</param>
        /// <param name="ShortName">Tên ngắn</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// 
        /// <returns>IQueryable<Province></returns>
        public IQueryable<Province> Search(IDictionary<string,object> dic)
        {

            string ProvinceName = Utils.GetString(dic, "ProvinceName");
            string ProvinceCode = Utils.GetString(dic, "ProvinceCode");
            string ShortName = Utils.GetString(dic, "ShortName");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<Province> lsProvince = ProvinceRepository.All;

            if (IsActive != null)
                lsProvince = lsProvince.Where(pro => pro.IsActive == IsActive);
            if (ProvinceName.Trim().Length != 0)
                lsProvince = lsProvince.Where(pro => pro.ProvinceName.ToLower().Contains(ProvinceName.ToLower()));
            if (ProvinceCode.Trim().Length != 0)
                lsProvince = lsProvince.Where(pro => pro.ProvinceCode.ToLower().Contains(ProvinceCode.ToLower()));
            if (ShortName.Trim().Length != 0)
                lsProvince = lsProvince.Where(pro => pro.ShortName.ToLower().Contains(ShortName.ToLower()));
            if (Description.Trim().Length != 0)
                lsProvince = lsProvince.Where(pro => pro.Description.ToLower().Contains(Description.ToLower()));
            return lsProvince;

        }

        public IQueryable<ProvinceApplyBO> GetListProvince(int promotionprogramId)
        {
            IQueryable<ProvinceApplyBO> query = null;

            var lstProvinceId = (from p in this.PromotionDetailBusiness.All.Where(o => o.PROMOTION_ID == promotionprogramId)
                                 select p.PROVINCE_ID).ToList();

            query = (from p in this.All.Where(o => o.IsActive == true)
                     join q in this.PromotionDetailBusiness.All.Where(o => o.PROMOTION_ID == promotionprogramId) on p.ProvinceID equals q.PROVINCE_ID into des
                     from g in des.DefaultIfEmpty()
                     select new ProvinceApplyBO
                     {
                         ProvinceID = p.ProvinceID,
                         ProvinceCode = p.ProvinceCode,
                         ProvinceName = p.ProvinceName,
                         ServicePackageID = 0,
                         IsApply = g != null ? true : false,
                     });
            return query;
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm Tỉnh/Thành phố
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="province">Đối tượng Tỉnh/Thành phố</param>
        /// <returns>Đối tượng Tỉnh/Thành phố</returns>
        public override Province Insert(Province province)
        {
            //Mã Tỉnh/Thành phố không được để trống 
            Utils.ValidateRequire(province.ProvinceCode, "Province_Label_ProvinceCode");
            //Độ dài mã Tỉnh/Thành phố không được vượt quá 10 
            Utils.ValidateMaxLength(province.ProvinceCode, PROVINCECODE_MAX_LENGTH, "Province_Label_ProvinceCode");
            //Mã Tỉnh/Thành phố đã tồn tại 
            new ProvinceBusiness(null).CheckDuplicate(province.ProvinceCode, GlobalConstants.LIST_SCHEMA, "Province", "ProvinceCode", true, province.ProvinceID, "Province_Label_ProvinceCode");
            //Tên Tỉnh/Thành phố không được để trống 
            Utils.ValidateRequire(province.ProvinceName, "Province_Label_ProvinceName");
            //Độ dài trường tên Tỉnh/Thành phố không được vượt quá 50 
            Utils.ValidateMaxLength(province.ProvinceName, PROVINCENAME_MAX_LENGTH, "Province_Label_ProvinceName");
            //Tên Tỉnh/Thành phố đã tồn tại 
            new ProvinceBusiness(null).CheckDuplicate(province.ProvinceName, GlobalConstants.LIST_SCHEMA, "Province", "ProvinceName", true, province.ProvinceID, "Province_Label_ProvinceName");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(province.Description, DESCRIPTION_MAX_LENGTH, "Description");
            //Độ dài trường tên thu gọn không được vượt quá 10 - Kiểm tra ShortName
            Utils.ValidateMaxLength(province.ShortName, SHORTNAME_MAX_LENGTH, "Province_Label_ShortName");
            //Insert
            return base.Insert(province);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa Tỉnh/Thành phố
        /// </summary>
        /// <param name="province">Đối tượng Tỉnh/Thành phố</param>
        /// <returns>
        /// Đối tượng Tỉnh/Thành phố
        /// </returns>

        public override Province Update(Province province)
        {
            //Bạn chưa chọn Tỉnh/Thành phố cần sửa hoặc Tỉnh/Thành phố bạn chọn đã bị xóa khỏi hệ thống
            new ProvinceBusiness(null).CheckAvailable((int)province.ProvinceID, "Province_Label_ProvinceID", true);
            //Mã Tỉnh/Thành phố không được để trống 
            Utils.ValidateRequire(province.ProvinceCode, "Province_Label_ProvinceCode");
            //Độ dài mã Tỉnh/Thành phố không được vượt quá 10 
            Utils.ValidateMaxLength(province.ProvinceCode, PROVINCECODE_MAX_LENGTH, "Province_Label_ProvinceCode");
            //Mã Tỉnh/Thành phố đã tồn tại 
            new ProvinceBusiness(null).CheckDuplicate(province.ProvinceCode, GlobalConstants.LIST_SCHEMA, "Province", "ProvinceCode", true, province.ProvinceID, "Province_Label_ProvinceCode");
            //Tên Tỉnh/Thành phố không được để trống 
            Utils.ValidateRequire(province.ProvinceName, "Province_Label_ProvinceName");
            //Độ dài trường tên Tỉnh/Thành phố không được vượt quá 50 
            Utils.ValidateMaxLength(province.ProvinceName, PROVINCENAME_MAX_LENGTH, "Province_Label_ProvinceName");
            //Tên Tỉnh/Thành phố đã tồn tại 
            new ProvinceBusiness(null).CheckDuplicate(province.ProvinceName, GlobalConstants.LIST_SCHEMA, "Province", "ProvinceName", true, province.ProvinceID, "Province_Label_ProvinceName");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(province.Description, DESCRIPTION_MAX_LENGTH, "Description");
            //Độ dài trường tên thu gọn không được vượt quá 10 - Kiểm tra ShortName
            Utils.ValidateMaxLength(province.ShortName, SHORTNAME_MAX_LENGTH, "Province_Label_ShortName");


            base.Update(province);
            return province;
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Tỉnh/Thành phố
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ProvinceID">ID Tỉnh thành phố</param>
        public void Delete(int ProvinceID )
        {
            //Bạn chưa chọn Tỉnh/Thành phố cần xóa hoặc Tỉnh/Thành phố bạn chọn đã bị xóa khỏi hệ thống
            new ProvinceBusiness(null).CheckAvailable((int)ProvinceID, "Province_Label_ProvinceID", true);

            //Không thể xóa Tỉnh/Thành phố đang sử dụng
            new ProvinceBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "Province", ProvinceID, "Province_Label_ProvinceDelete");
            //List<District> lsDistrict = DistrictRepository.All.Where(dis=>dis.ProvinceID==ProvinceID&&dis.IsActive==true).ToList();
            //if (lsDistrict.Count() != 0)
            //{
            //    District district = lsDistrict.FirstOrDefault();
            //    IQueryable<Commune> lsCommune = CommuneRepository.All.Where(com => com.DistrictID == district.DistrictID);
            //    Commune commune = lsCommune.FirstOrDefault();
            //    commune.IsActive = false;
            //    CommuneRepository.Update(commune);
            //    CommuneRepository.Save();
            //    district.IsActive = false;
            //    DistrictRepository.Update(district);
            //    DistrictRepository.Save();
            //}
            base.Delete(ProvinceID, true);

        }
        #endregion
    }
}
