﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class EvaluationDevelopmentBusiness
    {
        #region Search


        /// <summary>
        /// Tìm kiếm tiêu chí đánh giá sự phát triển của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<EvaluationDevelopment> Search(IDictionary<string, object> dic)
        {
            string EvaluationDevelopmentCode = Utils.GetString(dic, "EvaluationDevelopmentCode");
            string EvaluationDevelopmentName = Utils.GetString(dic, "EvaluationDevelopmentName");
            string Description = Utils.GetString(dic, "Description");
            int InputType = Utils.GetInt(dic, "InputType");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EvaluationDevelopmentGroupID = Utils.GetInt(dic, "EvaluationDevelopmentGroupID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            //thuyen
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int StatusID = Utils.GetInt(dic, "StatusID");

            List<int> lstEvaluationDevID = Utils.GetIntList(dic, "lstEvaluationDevelopmentID");

            IQueryable<EvaluationDevelopment> Query = EvaluationDevelopmentRepository.All;
            /* if (IsActive.HasValue)
             {
                 Query = Query.Where(evd => evd.IsActive == IsActive);
             }*/

            if (lstEvaluationDevID.Count() > 0)
            {
                Query = Query.Where(x => lstEvaluationDevID.Contains(x.EvaluationDevelopmentID));
            }

            if (StatusID != 0)
            {
                if (StatusID == 1)
                    Query = Query.Where(evd => evd.IsActive == true);
                if (StatusID == 2)
                    Query = Query.Where(evd => evd.IsActive == false);
            }

            if (ProvinceID != 0)
            {
                Query = Query.Where(evd => evd.ProvinceID == ProvinceID);
            }

            if (EvaluationDevelopmentCode.Trim().Length != 0)
            {
                Query = Query.Where(evd => evd.EvaluationDevelopmentCode.ToLower().Contains(EvaluationDevelopmentCode.ToLower()));
            }
            if (EvaluationDevelopmentName.Trim().Length != 0)
            {
                Query = Query.Where(evd => evd.EvaluationDevelopmentName.ToLower().Contains(EvaluationDevelopmentName.ToLower()));
            }
            if (Description.Trim().Length != 0)
            {
                Query = Query.Where(evd => evd.Description.Contains(Description));
            }
            //if (SchoolID != 0)
            //{
            //    Query = Query.Where(evd => evd.SchoolID == SchoolID);
            //}
            if (EducationLevelID != 0)
            {
                Query = Query.Where(evd => evd.EducationLevelID == EducationLevelID);
            }
            if (EvaluationDevelopmentGroupID != 0)
            {
                Query = Query.Where(evd => evd.EvaluationDevelopmentGroupID == EvaluationDevelopmentGroupID);
            }
            return Query;

        }
        #endregion

        public List<EvaluationDevelopment> GetListEvaluationDevelopment(IDictionary<string, object> dic)
        {
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearId = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");

            IQueryable<DeclareEvaluationGroup> lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.All;
            IQueryable<EvaluationDevelopmentGroup> lstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.All.Where(x => x.IsActive == true);
            IQueryable<EvaluationDevelopment> lstEvaluationDevelopment = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true);
            IQueryable<DeclareEvaluationIndex> lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.All;

            if (provinceId != 0)
            {
                var checkProvinceInED = lstEvaluationDevelopment.Where(x => x.ProvinceID == provinceId).FirstOrDefault();
                if (checkProvinceInED != null)
                    lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.ProvinceID == provinceId);
                else
                {
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
                }

                var checkProvinceInEDG = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == provinceId).FirstOrDefault();
                if (checkProvinceInEDG != null)
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == provinceId);
                else
                {
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
                }
            }
            else
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
            }
            if (appliedLevel != 0)
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.AppliedLevel == appliedLevel);
            }

            if (educationLevelID != 0 & educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.EducationLevelID == educationLevelID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.EducationLevelID == educationLevelID);
            }
            else if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                List<int> lstEdu = Utils.GetIntList(dic, "LstEducationLevel");
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstEdu.Contains(x.EducationLevelID));
            }

            if (schoolID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.SchoolID == schoolID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.SchoolID == schoolID);
            }

            if (academicYearId != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.AcademicYearID == academicYearId);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.AcademicYearID == academicYearId);
            }

            IQueryable<ClassProfile> queryClass = ClassProfileBusiness.All.Where(x => x.IsActive == true && lstClassID.Contains(x.ClassProfileID));

            if (lstClassID.Count() > 0)
            {
                List<int> lstDeclareEvaluationIndexID = new List<int>();
                var temptQueryDEI = lstDeclareEvaluationIndex.ToList();
                var temptQueryDEG = lstDeclareEvaluationGroup.ToList();
                for (int i = 0; i < lstClassID.Count(); i++)
                {
                    for (int j = 0; j < temptQueryDEG.Count(); j++)
                    {
                        var tempt = temptQueryDEI.Where(x => x.ClassID.HasValue && x.ClassID == lstClassID[i]
                                                            && x.EvaluationDevelopmentGroID == temptQueryDEG[j].EvaluationGroupID).ToList();
                        if (tempt.Count() == 0)
                        {
                            tempt = temptQueryDEI.Where(x => !x.ClassID.HasValue && x.ClassID == null 
                                                            && x.EvaluationDevelopmentGroID == temptQueryDEG[j].EvaluationGroupID).ToList();
                        }
                        var lstDEIID = tempt.Select(x => x.DeclareEvaluationIndexID).ToList();
                        lstDeclareEvaluationIndexID.AddRange(lstDEIID);
                    }              
                }
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstDeclareEvaluationIndexID.Contains(x.DeclareEvaluationIndexID));
            }

            return (from a in lstDeclareEvaluationGroup
                    join b in lstEvaluationDevelopmentGroup on a.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                    join c in lstEvaluationDevelopment on b.EvaluationDevelopmentGroupID equals c.EvaluationDevelopmentGroupID
                    join d in lstDeclareEvaluationIndex on c.EvaluationDevelopmentID equals d.EvaluationDevelopmentID
                    orderby b.OrderID, c.OrderID
                    select c).ToList();
        }

        public List<EvaluationDevelopment> GetListEvaluationDevelopmentByClass(IDictionary<string, object> dic, int? ClassID)
        {
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearId = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int provinceId = Utils.GetInt(dic, "ProvinceID");

            IQueryable<DeclareEvaluationGroup> lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.All;
            IQueryable<EvaluationDevelopmentGroup> lstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.All.Where(x => x.IsActive == true);
            IQueryable<EvaluationDevelopment> lstEvaluationDevelopment = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true);
            IQueryable<DeclareEvaluationIndex> lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.All;

            DeclareEvaluationGroup objDeclareGroup = lstDeclareEvaluationGroup.FirstOrDefault();
            // true: locked, false: unlocked
            if (ClassID.HasValue)
            {
                var tempQueryIndex = lstDeclareEvaluationIndex.Where(x => x.ClassID.HasValue);
                if (tempQueryIndex.Count() > 0)
                {
                    lstDeclareEvaluationIndex = tempQueryIndex;
                }
            }
            else
            {
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => !x.ClassID.HasValue);
            }

            if (provinceId != 0)
            {
                var checkProvinceInED = lstEvaluationDevelopment.Where(x => x.ProvinceID == provinceId).FirstOrDefault();
                if (checkProvinceInED != null)
                    lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.ProvinceID == provinceId);
                else
                {
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
                }

                var checkProvinceInEDG = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == provinceId).FirstOrDefault();
                if (checkProvinceInEDG != null)
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == provinceId);
                else
                {
                    lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
                }
            }

            if (appliedLevel != 0)
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.AppliedLevel == appliedLevel);
            }

            if (educationLevelID != 0 & educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.EducationLevelID == educationLevelID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.EducationLevelID == educationLevelID);
            }
            else if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                List<int> lstEdu = Utils.GetIntList(dic, "LstEducationLevel");
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstEdu.Contains(x.EducationLevelID));
            }

            if (schoolID != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.SchoolID == schoolID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.SchoolID == schoolID);
            }

            if (academicYearId != 0)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.AcademicYearID == academicYearId);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.AcademicYearID == academicYearId);
            }

            return (from a in lstDeclareEvaluationGroup
                    join b in lstEvaluationDevelopmentGroup on a.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                    join c in lstEvaluationDevelopment on b.EvaluationDevelopmentGroupID equals c.EvaluationDevelopmentGroupID
                    join d in lstDeclareEvaluationIndex on c.EvaluationDevelopmentID equals d.EvaluationDevelopmentID
                    orderby b.OrderID, c.OrderID
                    select c).ToList();
        }

        public void InsertEval(EvaluationDevelopment objInput)
        {
            EvaluationDevelopment objNew = new EvaluationDevelopment()
            {
                EvaluationDevelopmentCode = objInput.EvaluationDevelopmentCode,
                EvaluationDevelopmentName = objInput.EvaluationDevelopmentName,
                Description = objInput.Description,
                IsRequired = objInput.IsRequired,
                InputType = objInput.InputType,
                EducationLevelID = objInput.EducationLevelID,
                EvaluationDevelopmentGroupID = objInput.EvaluationDevelopmentGroupID,
                CreatedDate = objInput.CreatedDate,
                IsActive = objInput.IsActive,
                OrderID = objInput.OrderID,
                ProvinceID = objInput.ProvinceID
            };
            EvaluationDevelopmentBusiness.Insert(objInput);
            EvaluationDevelopmentBusiness.Save();
        }

        public void UpdateEval(EvaluationDevelopment objInput, List<EvaluationDevelopment> lstDB)
        {
            if (lstDB.Count > 0)
            {
                var result = lstDB.Where(x => x.EvaluationDevelopmentID == objInput.EvaluationDevelopmentID).FirstOrDefault();

                if (result != null)
                {
                    result.EvaluationDevelopmentCode = objInput.EvaluationDevelopmentCode;
                    result.EvaluationDevelopmentName = objInput.EvaluationDevelopmentName;
                    result.Description = objInput.Description;
                    result.EducationLevelID = objInput.EducationLevelID;
                    result.EvaluationDevelopmentGroupID = objInput.EvaluationDevelopmentGroupID;
                    result.IsActive = objInput.IsActive;
                    result.ProvinceID = objInput.ProvinceID;
                    result.ModifiedDate = objInput.ModifiedDate;

                    //EvaluationDevelopmentBusiness.Update(result);
                    // EvaluationDevelopmentBusiness.Save();
                    EvaluationDevelopmentRepository.Update(result); // khong su dung override Update cua Business
                    EvaluationDevelopmentRepository.Save();
                }
            }
        }

        public void DeleteEval(int EvaluationDevelopmentID)
        {
            EvaluationDevelopmentRepository.Delete(EvaluationDevelopmentID);
            // EvaluationDevelopmentBusiness.Delete(EvaluationDevelopmentID);
            this.Save();
        }

        public void UpdateOrderID(List<EvaluationDevelopment> lstInput, List<EvaluationDevelopment> lstDB)
        {
            foreach (var item in lstInput)
            {
                var result = lstDB.Where(x => x.EvaluationDevelopmentID == item.EvaluationDevelopmentID).FirstOrDefault();
                if (result != null)
                {
                    result.OrderID = item.OrderID;
                    result.ModifiedDate = item.ModifiedDate;
                    //EvaluationDevelopmentBusiness.Update(result);
                    EvaluationDevelopmentRepository.Update(result);
                }
            }
            //EvaluationDevelopmentBusiness.Save();
            EvaluationDevelopmentRepository.Save();
        }


        #region Delete

        /// <summary>
        /// Xóa tiêu chí đánh giá sự phát triển của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>
        /// <param name="EvaluationDevelopmentID"></param>
        public void Delete(int EvaluationDevelopmentID)
        {
            //Bạn chưa chọn tiêu chí đánh giá sự phát triển của trẻ cần xóa hoặc tiêu chí đánh giá sự phát triển của trẻ bạn chọn đã bị xóa khỏi hệ thống
            new EvaluationDevelopmentBusiness(null).CheckAvailable((int)EvaluationDevelopmentID, "EvaluationDevelopment_Label_EvaluationDevelopment", true);

            //Không thể xóa tiêu chí đánh giá sự phát triển của trẻ đang sử dụng
            new EvaluationDevelopmentBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "EvaluationDevelopment", EvaluationDevelopmentID, "EvaluationDevelopment_Label_EvaluationDevelopment");

            base.Delete(EvaluationDevelopmentID, true);
        }
        #endregion
        #region Insert

        /// <summary>
        /// Thêm mới tiêu chí đánh giá sự phát triển của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>
        /// <param name="ethnic">Đối tượng EvaluationDevelopment</param>
        /// <returns>Đối tượng EvaluationDevelopment</returns>
        public override EvaluationDevelopment Insert(EvaluationDevelopment evaluationDevelopment)
        {

            //EvaluationDevelopmentName: duplicate
            bool EvaluationDevelopmentNameduplicate = new EvaluationDevelopmentRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EvaluationDevelopment",
                 new Dictionary<string, object>()
                {
                    {"EvaluationDevelopmentName",evaluationDevelopment.EvaluationDevelopmentName},  
                     {"IsActive",true}
                },
                   new Dictionary<string, object>()
                 {
                    {"EvaluationDevelopmentID",evaluationDevelopment.EvaluationDevelopmentID},                    
                });
            if (EvaluationDevelopmentNameduplicate)
            {
                throw new BusinessException("EvaluationDevelopment_Validate_NameDuplicate");
            }

            bool EvaluationDevelopmentCodeduplicate = new EvaluationDevelopmentRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EvaluationDevelopment",
                 new Dictionary<string, object>()
                {
                    {"EvaluationDevelopmentCode",evaluationDevelopment.EvaluationDevelopmentCode},  
                    {"EvaluationDevelopmentGroupID", evaluationDevelopment.EvaluationDevelopmentGroupID},
                     {"IsActive",true}
                },
                   new Dictionary<string, object>()
                 {
                    {"EvaluationDevelopmentID",evaluationDevelopment.EvaluationDevelopmentID},                    
                });
            if (EvaluationDevelopmentCodeduplicate)
            {
                throw new BusinessException("EvaluationDevelopment_Validate_CodeDuplicate");
            }
            if (evaluationDevelopment.InputMin >= 1000 || evaluationDevelopment.InputMax >= 1000)
            {
                throw new BusinessException("EvaluationDevelopment_Validate_ValidMinMax");
            }
            //InputMax >= InputMin
            if (evaluationDevelopment.InputMax < evaluationDevelopment.InputMin)
            {
                throw new BusinessException("Common_Validate_InputMax");
            }
            //Insert
            return base.Insert(evaluationDevelopment);

        }
        #endregion
        #region Update
        /// <summary>
        /// Sửa tiêu chí đánh giá sự phát triển của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>
        /// <param name="ethnic">Đối tượng EvaluationDevelopment</param>
        /// <returns>Đối tượng EvaluationDevelopment</returns>
        public override EvaluationDevelopment Update(EvaluationDevelopment evaluationDevelopment)
        {
            //EvaluationDevelopmentName: duplicate
            bool EvaluationDevelopmentNameduplicate = new EvaluationDevelopmentRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EvaluationDevelopment",
                 new Dictionary<string, object>()
                {
                    {"EvaluationDevelopmentName",evaluationDevelopment.EvaluationDevelopmentName},    
                      {"IsActive",true}
                },
                   new Dictionary<string, object>()
                 {
                    {"EvaluationDevelopmentID",evaluationDevelopment.EvaluationDevelopmentID},                    
                });
            if (EvaluationDevelopmentNameduplicate)
            {
                throw new BusinessException("EvaluationDevelopment_Validate_NameDuplicate");
            }
            bool EvaluationDevelopmentCodeduplicate = new EvaluationDevelopmentRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EvaluationDevelopment",
                new Dictionary<string, object>()
                {
                    {"EvaluationDevelopmentCode",evaluationDevelopment.EvaluationDevelopmentCode},  
                    {"EvaluationDevelopmentGroupID", evaluationDevelopment.EvaluationDevelopmentGroupID},
                     {"IsActive",true}
                },
                  new Dictionary<string, object>()
                 {
                    {"EvaluationDevelopmentID",evaluationDevelopment.EvaluationDevelopmentID},                    
                });
            if (EvaluationDevelopmentCodeduplicate)
            {
                throw new BusinessException("EvaluationDevelopment_Validate_CodeDuplicate");
            }
            if (evaluationDevelopment.InputMin >= 1000 || evaluationDevelopment.InputMax >= 1000)
            {
                throw new BusinessException("EvaluationDevelopment_Validate_ValidMinMax");
            }
            //InputMax >= InputMin
            if (evaluationDevelopment.InputMax < evaluationDevelopment.InputMin)
            {
                throw new BusinessException("Common_Validate_InputMax");
            }
            return base.Update(evaluationDevelopment);
        }
        #endregion

        public IQueryable<EvaluationDevelopment> GetListByAppliedLevelAndProvince(int AppliedLevel, int ProvinceID)
        {
            IQueryable<EvaluationDevelopment> query = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true);

            if (AppliedLevel != 0)
                query = query.Where(x => x.EducationLevel.Grade == AppliedLevel);

            if (ProvinceID != 0)
            {
                var check = query.Where(x => x.ProvinceID == ProvinceID).FirstOrDefault();
                if (check != null)
                    query = query.Where(x => x.ProvinceID == ProvinceID);
                else
                    query = query.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
            }

            return query;
        }

        public void InsertOrUpdateMulti(List<EvaluationDevelopment> lstInput, List<EvaluationDevelopment> lstDB,
            List<int> lstCheckEvaluationID, List<int> lstUncheckEvaluationID)
        {

            var lstCheckEvaluationDevDB = lstDB.Where(x => lstCheckEvaluationID.Contains(x.EvaluationDevelopmentID)).ToList();
            for (int i = 0; i < lstInput.Count(); i++)
            {
                var objResult = lstCheckEvaluationDevDB.Where(x => x.EvaluationDevelopmentID == lstInput[i].EvaluationDevelopmentID).FirstOrDefault();
                if (objResult != null)
                {
                    objResult.EvaluationDevelopmentName = lstInput[i].EvaluationDevelopmentName;
                    objResult.EvaluationDevelopmentCode = lstInput[i].EvaluationDevelopmentCode;
                    objResult.OrderID = lstInput[i].OrderID;
                    objResult.ModifiedDate = lstInput[i].ModifiedDate;
                    objResult.Description = lstInput[i].Description;
                    objResult.ProvinceID = lstInput[i].ProvinceID;

                    EvaluationDevelopmentRepository.Update(objResult);
                }
                else
                {
                    lstInput[i].EvaluationDevelopmentID = 0;
                    EvaluationDevelopmentRepository.Insert(lstInput[i]);
                }
            }

            if (lstUncheckEvaluationID.Count() > 0)
            {
                var lstUnCheckEvaluationDevDB = lstDB.Where(x => lstUncheckEvaluationID.Contains(x.EvaluationDevelopmentID)).ToList();
                EvaluationDevelopmentRepository.DeleteAll(lstUnCheckEvaluationDevDB);
            }

            base.Save();
        }

        public void InsertOrUpdateMultiFromExcel(List<EvaluationDevelopment> lstInput, List<EvaluationDevelopment> lstDB)
        {
            int count = (lstDB.Count() + 1);
            for (int i = 0; i < lstInput.Count(); i++)
            {
                var objResult = lstDB.Where(x => x.EvaluationDevelopmentGroupID == lstInput[i].EvaluationDevelopmentGroupID
                                            && x.EvaluationDevelopmentCode == lstInput[i].EvaluationDevelopmentCode).FirstOrDefault();
                if (objResult != null)
                {
                    objResult.EvaluationDevelopmentName = lstInput[i].EvaluationDevelopmentName;
                    objResult.ModifiedDate = lstInput[i].ModifiedDate;
                    objResult.Description = lstInput[i].Description;
                    EvaluationDevelopmentRepository.Update(objResult);
                }
                else
                {
                    lstInput[i].OrderID = count;
                    EvaluationDevelopmentRepository.Insert(lstInput[i]);
                    count++;
                }
            }

            base.Save();
        }

        public void DeleteBySchool(List<DeclareEvaluationIndex> lstDEI, Dictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int evaluationGroupID = Utils.GetInt(dic, "EvaluationGroupID");
            List<int> lstEDIID = lstDEI.Select(x => x.EvaluationDevelopmentID).ToList();

            IQueryable<DeclareEvaluationIndex> iqueryDEI = DeclareEvaluationIndexBusiness.All
                                                                                .Where(X => X.SchoolID == schoolID &&
                                                                                    X.AcademicYearID == academicYearID &&
                                                                                    X.EducationLevelID == educationLevelID);

            IQueryable<EvaluationDevelopment> iqueryEDI = from EDI in EvaluationDevelopmentBusiness.All
                                                                   .Where(x => x.EducationLevelID == educationLevelID &&
                                                                            x.CreatedBySchool == true && x.IsActive == true)
                                                          select EDI;

            if (evaluationGroupID != 0)
            {
                iqueryDEI = iqueryDEI.Where(x => x.EvaluationDevelopmentGroID == evaluationGroupID);
                lstDEI = lstDEI.Where(x => x.EvaluationDevelopmentGroID == evaluationGroupID).ToList();
                iqueryEDI = iqueryEDI.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
            }

            List<DeclareEvaluationIndex> lstDEIDelete = iqueryDEI.ToList();
            List<DeclareEvaluationIndex> objDEI = null;
            foreach (var item in lstDEI)
            {
                objDEI = lstDEIDelete.Where(x => x.EvaluationDevelopmentGroID == item.EvaluationDevelopmentGroID && x.EvaluationDevelopmentID == item.EvaluationDevelopmentID).ToList();
                if (objDEI.Count <= 0)
                    continue;
                DeclareEvaluationIndexBusiness.DeleteAll(objDEI);
            }

            List<EvaluationDevelopment> lstEDIDelete = iqueryEDI.ToList();
            EvaluationDevelopment objEDI = null;
            foreach (var item in lstDEI)
            {
                objEDI = lstEDIDelete.Where(x => x.EvaluationDevelopmentID == item.EvaluationDevelopmentID && x.EvaluationDevelopmentGroupID == item.EvaluationDevelopmentGroID).FirstOrDefault();
                if (objEDI == null || objEDI.CreatedBySchool != true)
                    continue;
                base.Delete(objEDI.EvaluationDevelopmentID);
            }
            DeclareEvaluationIndexBusiness.Save();
            base.Save();
        }

        public void InsertBySchool(List<EvaluationDevelopment> lstEDI, List<DeclareEvaluationIndex> lstDEI, Dictionary<string, object> dic)
        {
            List<EvaluationDevelopment> lstResult = new List<EvaluationDevelopment>();
            try
            {
                int schoolID = Utils.GetInt(dic, "SchoolID");
                int academicYearID = Utils.GetInt(dic, "AcademicYearID");
                int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
                int evaluationGroupID = Utils.GetInt(dic, "EvaluationGroupID");

                IQueryable<EvaluationDevelopment> iqueryEDI = from EDI in EvaluationDevelopmentBusiness.All
                                                                       .Where(x => x.EducationLevelID == educationLevelID &&
                                                                                x.IsActive == true)
                                                              select EDI;

                IQueryable<DeclareEvaluationIndex> iqueryDEI = DeclareEvaluationIndexBusiness.All
                                                                                    .Where(X => X.SchoolID == schoolID &&
                                                                                        X.AcademicYearID == academicYearID &&
                                                                                        X.EducationLevelID == educationLevelID);

                if (evaluationGroupID != 0)
                {
                    iqueryEDI = iqueryEDI.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
                    iqueryDEI = iqueryDEI.Where(x => x.EvaluationDevelopmentGroID == evaluationGroupID);
                    lstDEI = lstDEI.Where(x => x.EvaluationDevelopmentGroID == evaluationGroupID).ToList();
                }
                List<DeclareEvaluationIndex> lstDEIDelete = iqueryDEI.ToList();
                DeclareEvaluationIndexBusiness.DeleteAll(lstDEIDelete.Where(x => x.ClassID == null).ToList());
                // Xóa các chỉ số của Class
                List<int> lstID = lstDEI.Select(x => x.EvaluationDevelopmentID).ToList();

                var lstDEIOfClass = lstDEIDelete.Where(x => !lstID.Contains(x.EvaluationDevelopmentID) &&
                                                    x.SchoolID == schoolID &&
                                                    x.AcademicYearID == academicYearID &&
                                                    x.ClassID != null &&
                                                    x.EducationLevelID == educationLevelID).ToList();
                DeclareEvaluationIndexBusiness.DeleteAll(lstDEIOfClass);

                EvaluationDevelopment objResult = new EvaluationDevelopment();
                List<EvaluationDevelopment> lstEDITemp = new List<EvaluationDevelopment>();
                // Cập nhật bảng Evaluation_Development
                lstEDITemp = lstEDI.Where(x => x.EvaluationDevelopmentID == -1).ToList();
                foreach (var item in lstEDITemp)
                {
                    objResult = new EvaluationDevelopment();
                    objResult.EvaluationDevelopmentCode = item.EvaluationDevelopmentCode;
                    objResult.EvaluationDevelopmentName = item.EvaluationDevelopmentName;
                    objResult.Description = item.Description;
                    objResult.IsRequired = item.IsRequired;
                    objResult.InputType = item.InputType;
                    objResult.EducationLevelID = item.EducationLevelID;
                    objResult.EvaluationDevelopmentGroupID = item.EvaluationDevelopmentGroupID;
                    objResult.CreatedDate = item.CreatedDate;
                    objResult.IsActive = true;
                    objResult.OrderID = item.OrderID;
                    objResult.ProvinceID = item.ProvinceID;
                    objResult.CreatedBySchool = true;
                    lstResult.Add(base.Insert(objResult));
                    base.Save();
                }
                // Nếu có evalutionGroupID thì cập nhật lại orderID
                if (evaluationGroupID != 0)
                {
                    lstEDITemp = lstEDI.Where(x => x.EvaluationDevelopmentID != -1).ToList();
                    foreach (var item in lstEDITemp)
                    {
                        objResult = iqueryEDI.Where(x => x.EvaluationDevelopmentID == item.EvaluationDevelopmentID).FirstOrDefault();
                        if (objResult == null)
                            continue;
                        objResult.OrderID = item.OrderID;
                        base.Update(objResult);
                    }
                }

                // Update Mã và Tên chỉ số
                List<EvaluationDevelopment> lstEDGI = iqueryEDI.ToList();
                lstEDITemp = lstEDI.Where(x => x.EvaluationDevelopmentID != -1).ToList();
                foreach (var item in lstEDITemp)
                {
                    objResult = lstEDGI.Where(x => x.EvaluationDevelopmentGroupID == item.EvaluationDevelopmentGroupID && x.EvaluationDevelopmentID == item.EvaluationDevelopmentID).FirstOrDefault();
                    if (objResult != null && objResult.CreatedBySchool == true)
                    {
                        objResult.EvaluationDevelopmentCode = item.EvaluationDevelopmentCode;
                        objResult.EvaluationDevelopmentName = item.EvaluationDevelopmentName;
                        objResult.ModifiedDate = DateTime.Now;
                        base.Update(objResult);
                    }
                }


                //Cập nhật bảng DeclareEvaluationIndex
                List<DeclareEvaluationIndex> lstResultDEI = new List<DeclareEvaluationIndex>();
                DeclareEvaluationIndex objResultDEI = null;
                foreach (var item in lstResult)
                {
                    objResultDEI = new DeclareEvaluationIndex();
                    objResultDEI.SchoolID = schoolID;
                    objResultDEI.AcademicYearID = academicYearID;
                    objResultDEI.EducationLevelID = item.EducationLevelID;
                    objResultDEI.EvaluationDevelopmentGroID = item.EvaluationDevelopmentGroupID;
                    objResultDEI.EvaluationDevelopmentID = item.EvaluationDevelopmentID;
                    objResultDEI.CreateDate = DateTime.Now;
                    lstResultDEI.Add(objResultDEI);
                }

                DeclareEvaluationIndexBusiness.InsertBySchool(lstResultDEI);
                DeclareEvaluationIndexBusiness.InsertBySchool(lstDEI);
                DeclareEvaluationIndexBusiness.Save();
            }
            catch
            {
                base.DeleteAll(lstResult);
            }
        }

        public void ImportBySchool(List<EvaluationDevelopmentBO> lstData, Dictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int appliedLevelID = Utils.GetInt(dic, "AppliedLevel");

            // Insert           
            List<EvaluationDevelopment> lstInsertResult = new List<EvaluationDevelopment>();
            try
            {
                List<EvaluationDevelopment> lstCheck = (from a in EvaluationDevelopmentBusiness.All
                                                        join b in EvaluationDevelopmentGroupBusiness.All on a.EvaluationDevelopmentGroupID equals b.EvaluationDevelopmentGroupID
                                                        where a.EducationLevelID == educationLevelID && a.IsActive == true &&
                                                              b.AppliedLevel == appliedLevelID && b.IsActive == true &&
                                                              b.EducationLevelID == educationLevelID &&
                                                              b.ProvinceID == provinceID &&
                                                              a.ProvinceID == provinceID
                                                        select a).ToList();
                EvaluationDevelopment objResult = null;
                foreach (var item in lstData)
                {
                    if (item.EvaluationDevIndexID == -1)
                    {
                        objResult = new EvaluationDevelopment();
                        objResult.EvaluationDevelopmentCode = item.EvaluationDevIndexCode;
                        objResult.EvaluationDevelopmentName = item.EvaluationDevIndexName;
                        objResult.Description = item.EvaluationDevIndexName;
                        objResult.IsRequired = true;
                        objResult.InputType = 1;
                        objResult.EducationLevelID = educationLevelID;
                        objResult.EvaluationDevelopmentGroupID = item.EvaluationDevGroupID;
                        objResult.CreatedDate = DateTime.Now;
                        objResult.IsActive = true;
                        objResult.OrderID = item.Order;
                        objResult.ProvinceID = provinceID;
                        objResult.CreatedBySchool = true;
                        objResult = base.Insert(objResult);
                        lstInsertResult.Add(objResult);
                        base.Save();

                        // Thêm vào bảng DeclareEvaluationIndex
                        var objDEI = new DeclareEvaluationIndex();
                        objDEI.SchoolID = schoolID;
                        objDEI.AcademicYearID = academicYearID;
                        objDEI.EducationLevelID = educationLevelID;
                        objDEI.EvaluationDevelopmentGroID = item.EvaluationDevGroupID;
                        objDEI.EvaluationDevelopmentID = objResult.EvaluationDevelopmentID;
                        objDEI.CreateDate = DateTime.Now;
                        DeclareEvaluationIndexBusiness.Insert(objDEI);
                    }
                    else
                    {
                        objResult = lstCheck.Where(x => x.EvaluationDevelopmentID == item.EvaluationDevIndexID).FirstOrDefault();
                        if (objResult != null)
                        {
                            var objUpdate = objResult;
                            objUpdate.EvaluationDevelopmentName = item.EvaluationDevIndexName;
                            objUpdate.Description = item.EvaluationDevIndexName;
                        }
                    }
                }
                base.Save();
                DeclareEvaluationIndexBusiness.Save();
            }
            catch
            {
                base.DeleteAll(lstInsertResult);
            }
        }

    }
}