﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class MarkBO
    {
        public int SubjectID { get; set; }
        public int ClassID { get; set; }
        public int MarkIndex { get; set; }
        public string Title { get; set; }
        public int PupilID { get; set; }
    }

    public partial class ReportMarkInputSituationBusiness
    {

        /// <summary>
        /// CreateReportMarkInputSituation
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="Semester">The semester ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>phuongh1</author>
        /// <date>25/4/2013</date>
        public Stream CreateReportMarkInputSituation(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID)
        {

            EducationLevel Edu = EducationLevelBusiness.Find(EducationLevelID);
            AcademicYear Aca = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
            SemeterDeclaration sd = SemeterDeclarationBusiness.Find(Semester);
            ReportMarkInputSituationBO data = this.GetInputMark(
                SchoolID, AcademicYearID, Semester, EducationLevelID);

            SheetData SheetData = new SheetData("Thống kê tình hình nhập điểm");
            SheetData.Data["AcademicYear"] = Aca.DisplayTitle; ;
            SheetData.Data["SchoolName"] = sp.SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = sp.Province.ProvinceName;
            SheetData.Data["SupervisingDeptName"] = sp.SupervisingDept.SupervisingDeptName.ToUpper();
            SheetData.Data["EducationLevelName"] = Edu.Resolution.ToUpper();
            SheetData.Data["Semester"] = "HỌC KỲ " + Semester;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_TKTINHHINHNHAPDIEM_KHOI10_HKI);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Chỉnh lại Template để động cột
            IVTWorksheet Template = oBook.GetSheet(1);

            Template.FillVariableValue(SheetData.Data);

            int StartRow = 9;
            int StartCol = 1;

            if (data.ListClassName.Count > 0)
            {
                Template.SetCellValue(StartRow, StartCol + 3, data.ListClassName[0]);
                Template.MergeRow(StartRow, StartCol + 3, StartCol + 4);
                int k = 0;
                for (int j = 1; j < data.ListClassName.Count; j++)
                {
                    k += 2;
                    Template.CopyPaste(Template.GetRange("D8", "E15"), StartRow - 1, StartCol + 3 + k);
                    Template.SetCellValue(StartRow, StartCol + 3 + k, data.ListClassName[j]);
                    Template.MergeRow(StartRow, StartCol + 3 + k, StartCol + 4 + k);
                    //Template.GetRange(StartRow + 1, StartCol + 2 + j, StartRow + 1, StartCol + 2 + j).AutoFit();

                }

                Template.MergeRow(StartRow - 1, StartCol + 3, StartCol + 2 + data.ListClassName.Count * 2);
            }


            //Tạo Sheet fill dữ liệu
            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange(1, 1, StartRow + 1, 100), 1, 1);

            IVTRange rangeSTT = Template.GetRange(StartRow + 2, 1, StartRow + 6, 100);
            IVTRange rangeSubject = Template.GetRange(StartRow + 2, 2, StartRow + 6, 100);
            for (int i = 0; i < data.ListSubject.Count; i++)
            {
                InputMarkOfSubjectBO subjectBO = data.ListSubject[i];
                Sheet.CopyPaste(rangeSTT, StartRow + 2 + i * 4, 1);
                Sheet.SetCellValue(StartRow + 2 + i * 4, 1, i + 1);
                Sheet.CopyPaste(rangeSubject, StartRow + 2 + i * 4, 2);
                Sheet.SetCellValue(StartRow + 2 + i * 4, 2, subjectBO.SubjectName);
                //Sheet.FillDataHorizon(subjectBO.ListInterview, StartRow + 2 + i * 4, 3);
                //Sheet.FillDataHorizon(subjectBO.ListWriting, StartRow + 3 + i * 4, 3);
                //Sheet.FillDataHorizon(subjectBO.ListTwiceCoeffiecient, StartRow + 4 + i * 4, 3);
                int k = 0;
                for (int j = 0; j < subjectBO.ListInterview.Count(); j++)
                {

                    Sheet.SetCellValue(StartRow + 2 + i * 4, 4 + k, subjectBO.ListInterview[j]);
                    Sheet.SetCellValue(StartRow + 2 + i * 4, 5 + k, subjectBO.listStrInterview[j]);
                    Sheet.SetCellValue(StartRow + 3 + i * 4, 4 + k, subjectBO.ListWriting[j]);
                    Sheet.SetCellValue(StartRow + 3 + i * 4, 5 + k, subjectBO.ListStrWriting[j]);
                    Sheet.SetCellValue(StartRow + 4 + i * 4, 4 + k, subjectBO.ListTwiceCoeffiecient[j]);
                    Sheet.SetCellValue(StartRow + 4 + i * 4, 5 + k, subjectBO.ListStrTwiceCoeffiecient[j]);
                    Sheet.SetCellValue(StartRow + 5 + i * 4, 4 + k, subjectBO.ListFinal[j]);
                    Sheet.SetCellValue(StartRow + 5 + i * 4, 5 + k, subjectBO.ListStrFinal[j]);
                    k += 2;
                }

            }
            Sheet.GetRange(8, 1, 10, data.ListClassName.Count * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);

            Sheet.SetCellValue(StartRow + data.ListSubject.Count * 4 + 3, 2, "Ghi chú:");
            Sheet.GetRange(StartRow + data.ListSubject.Count * 4 + 3, 2, StartRow + data.ListSubject.Count * 4 + 3, 2).SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);
            Sheet.SetCellValue(StartRow + data.ListSubject.Count * 4 + 4, 2, "- Số con điểm đã nhập: Số con điểm lớn nhất đã nhập cho 1 học sinh tương ứng với loại điểm.");
            Sheet.SetCellValue(StartRow + data.ListSubject.Count * 4 + 5, 2, "- Số học sinh thiếu điểm: là tổng số học sinh có số con điểm đã nhập (với từng loại điểm) nhỏ hơn:");
            Sheet.SetCellValue(StartRow + data.ListSubject.Count * 4 + 6, 2, "+ Số con điểm tối thiểu được thiết lập");
            Sheet.SetCellValue(StartRow + data.ListSubject.Count * 4 + 7, 2, "+ Số con điểm đã nhập (trong trường hợp không thiết lập số con điểm tối thiểu)");

            if (data.ListSubject.Count > 0)
            {
                Sheet.SetCellValue(StartRow + data.ListSubject.Count * 4 + 9, 2 + data.ListSubject[0].ListInterview.Count * 2, "Người lập");
            }

            string sheetName = Edu.Resolution;
            Sheet.Orientation = VTXPageOrientation.VTxlLandscape;
            Sheet.FitAllColumnsOnOnePage = true;

            Sheet.Name = sheetName;
            Sheet.SetFontName("Times New Roman", 0);
            Template.Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// CreateReportPupilNotEnoughMark
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="Semester">The semester ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>phuongh</author>
        /// <date>25/4/2013</date>
        public Stream CreateReportPupilNotEnoughMark(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID, int ClassID)
        {
            EducationLevel Edu = EducationLevelBusiness.Find(EducationLevelID);
            AcademicYear Aca = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
            SemeterDeclaration sd = SemeterDeclarationBusiness.Find(Semester);
            ReportMissingMarkSituationBO data = this.GetMissingMark(
                SchoolID, AcademicYearID, Semester, EducationLevelID, ClassID);
            SheetData SheetData = new SheetData("Danh sách học sinh thiếu điểm");
            SheetData.Data["AcademicYear"] = Aca.DisplayTitle; ;
            SheetData.Data["SchoolName"] = sp.SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = sp.Province.ProvinceName;
            SheetData.Data["SupervisingDeptName"] = sp.SupervisingDept.SupervisingDeptName.ToUpper();
            SheetData.Data["EducationLevelName"] = Edu.Resolution.ToUpper();
            SheetData.Data["Semester"] = "HỌC KỲ " + Semester;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THCS_DSHOCSINHTHIEUDIEM_KHOI6_HKI);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);

            Template.FillVariableValue(SheetData.Data);
            int StartRow = 8;
            int StartCol = 1;
            if (data.ListSubjectName.Count > 0)
            {
                Template.SetCellValue(StartRow, StartCol + 3, data.ListSubjectName[0]);
                IVTRange subjectRange = Template.GetRange("D8", "F10");
                for (int j = 1; j < data.ListSubjectName.Count; j++)
                {
                    Template.CopyPaste(subjectRange, StartRow, StartCol + 3 + j * 3);
                    Template.SetCellValue(StartRow, StartCol + 3 + j * 3, data.ListSubjectName[j]);
                }
            }

            //Tạo Sheet fill dữ liệu
            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange(1, 1, StartRow + 1, 100), 1, 1);

            IVTRange rangeSubject = Template.GetRange(StartRow + 2, 1, StartRow + 2, 100);
            for (int i = 0; i < data.ListPupil.Count; i++)
            {

                MissingMarkOfPupilBO bo = data.ListPupil[i];

                Sheet.CopyPaste(rangeSubject, StartRow + i + 2, 1);
                if (bo.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && bo.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                {
                    // Gach ngang va boi do voi cac hoc sinh co trang thai khac dang hoc va khac da tot nghiep
                    //Sheet.GetRow(StartRow + i + 2).SetFontColour(System.Drawing.Color.Red);
                    Sheet.GetRow(StartRow + i + 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                Sheet.SetCellValue(StartRow + i + 2, 1, i + 1);
                Sheet.SetCellValue(StartRow + i + 2, 2, bo.ClassName);
                Sheet.SetCellValue(StartRow + i + 2, 3, bo.PupilName);
                List<string> listTemp = bo.ListMissingMark;
                Sheet.FillDataHorizon(listTemp, StartRow + i + 2, 4);
            }
            Sheet.GetRange(8, 1, 9, data.ListSubjectName.Count * 3 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            Template.Delete();
            string sheetName = Edu.Resolution;
            Sheet.Name = sheetName;
            return oBook.ToStream();
        }

        /// <author>phuongh1</author>
        /// <date>23/4/2013</date>
        public ReportMarkInputSituationBO GetInputMark(
            int SchoolID, int AcademicYearID,
            int Semester, int EducationLevelID)
        {
            ReportMarkInputSituationBO result = new ReportMarkInputSituationBO();

            //Lấy danh sách lớp theo khối
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID}
            }).OrderBy(x => x.DisplayName).ToList();

            result.ListClassName = listClass.Select(x => x.DisplayName).ToList();

            //Lấy danh sách môn học cho lớp 
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID}
            }).ToList();

            ClassSubject objCS = null;

            //Lấy thông tin học sinh đang học
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["CheckWithClass"] = "CheckWithClass";
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).AddCriteriaSemester(aca, Semester).ToList();

            //Lấy thông tin điểm 
            List<MarkBO> listMark = GetMaxInputMarkOfClass(SchoolID, AcademicYearID, Semester, EducationLevelID);

            //Lấy danh sách môn học theo khối
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID}
            }).OrderBy(x => x.SubjectCat.OrderInSubject).ToList();

            //Tính con điểm theo các môn
            List<InputMarkOfSubjectBO> listMoS = new List<InputMarkOfSubjectBO>();
            result.ListSubject = listMoS;
            int baseNum = 0;
            foreach (SchoolSubject schoolSubject in listSchoolSubject)
            {
                InputMarkOfSubjectBO bo = new InputMarkOfSubjectBO();
                bo.SubjectName = schoolSubject.SubjectCat.DisplayName;
                List<int?> listM = new List<int?>();
                List<string> listStrM = new List<string>();
                List<int?> listP = new List<int?>();
                List<string> listStrP = new List<string>();
                List<int?> listV = new List<int?>();
                List<string> listStrV = new List<string>();
                List<int?> listHK = new List<int?>();
                List<string> listStrHK = new List<string>();

                bo.ListInterview = listM;
                bo.listStrInterview = listStrM;
                bo.ListWriting = listP;
                bo.ListStrWriting = listStrP;
                bo.ListTwiceCoeffiecient = listV;
                bo.ListStrTwiceCoeffiecient = listStrV;
                bo.ListFinal = listHK;
                bo.ListStrFinal = listStrHK;

                listMoS.Add(bo);
                //Đếm số học sinh trong lớp
                List<PupilOfClass> lstPOC = new List<PupilOfClass>();
                foreach (ClassProfile classProfile in listClass)
                {
                    lstPOC = listPupilOfClass.Where(o => o.ClassID == classProfile.ClassProfileID).ToList();
                    int pupilStudying = lstPOC.Count();
                    //Kiểm tra trong bảng 
                    //Kiểm tra lớp có học môn đó trong học kỳ x không
                    if (!listClassSubject.Exists(
                        x => x.ClassID == classProfile.ClassProfileID
                            && x.SubjectID == schoolSubject.SubjectID
                            && (
                            (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                            && x.SectionPerWeekFirstSemester.GetValueOrDefault() > 0)
                            ||
                            (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            && x.SectionPerWeekSecondSemester.GetValueOrDefault() > 0)
                            )
                            ))
                    {
                        listM.Add(null);
                        listStrM.Add(null);
                        listP.Add(null);
                        listStrP.Add(null);
                        listV.Add(null);
                        listStrV.Add(null);
                        listHK.Add(null);
                        listStrHK.Add(null);
                    }
                    else
                    {

                        objCS = listClassSubject.Where(p => p.ClassID == classProfile.ClassProfileID && p.SubjectID == schoolSubject.SubjectID).FirstOrDefault();


                        var markM = listMark.Where(x => x.SubjectID == schoolSubject.SubjectID
                            && x.ClassID == classProfile.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_M)).ToList();
                        int mM = 0;
                        if (markM != null && markM.Count > 0)
                        {
                            mM = markM.Max(o => o.MarkIndex);
                        }
                        baseNum = mM;
                        //lay cau hinh so con diem toi thieu M
                        if (Semester == 1)
                        {
                            if (objCS.MMinFirstSemester.HasValue && objCS.MMinFirstSemester > 0)
                            {
                                baseNum = objCS.MMinFirstSemester.Value;
                            }
                        }
                        else
                        {
                            if (objCS.MMinSecondSemester.HasValue && objCS.MMinSecondSemester > 0)
                            {
                                baseNum = objCS.MMinSecondSemester.Value;
                            }
                        }

                        //Những học sinh nhập không đủ số điểm tối đa M
                        int t_mM = 0;
                        if (baseNum > 0)
                        {
                            t_mM = markM.Where(o => o.MarkIndex < baseNum).Count() + pupilStudying - markM.Count();
                        }
                        string str_mM = t_mM + "/" + pupilStudying;
                        listM.Add(mM);
                        listStrM.Add(str_mM);

                        var markP = listMark.Where(x => x.SubjectID == schoolSubject.SubjectID
                            && x.ClassID == classProfile.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_P)).ToList();
                        int mP = 0;
                        if (markP != null && markP.Count > 0)
                        {
                            mP = markP.Max(o => o.MarkIndex);
                        }
                        baseNum = mP;
                        //lay cau hinh so con diem toi thieu P
                        if (Semester == 1)
                        {
                            if (objCS.PMinFirstSemester.HasValue && objCS.PMinFirstSemester > 0)
                            {
                                baseNum = objCS.PMinFirstSemester.Value;
                            }
                        }
                        else
                        {
                            if (objCS.PMinSecondSemester.HasValue && objCS.PMinSecondSemester > 0)
                            {
                                baseNum = objCS.PMinSecondSemester.Value;
                            }
                        }
                        //Những học sinh nhập không đủ số điểm tối đa P
                        int t_mP = 0;
                        if (baseNum > 0)
                        {
                            t_mP = markP.Where(o => o.MarkIndex < baseNum).Count() + pupilStudying - markP.Count();
                        }
                        string str_mP = t_mP + "/" + pupilStudying;
                        listP.Add(mP);
                        listStrP.Add(str_mP);

                        var markV = listMark.Where(x => x.SubjectID == schoolSubject.SubjectID
                            && x.ClassID == classProfile.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_V)).ToList();
                        int mV = 0;
                        if (markV != null && markV.Count > 0)
                        {
                            mV = markV.Max(o => o.MarkIndex);
                        }

                        baseNum = mV;
                        //lay cau hinh so con diem toi thieu V
                        if (Semester == 1)
                        {
                            if (objCS.VMinFirstSemester.HasValue && objCS.VMinFirstSemester > 0)
                            {
                                baseNum = objCS.VMinFirstSemester.Value;
                            }
                        }
                        else
                        {
                            if (objCS.VMinSecondSemester.HasValue && objCS.VMinSecondSemester > 0)
                            {
                                baseNum = objCS.VMinSecondSemester.Value;
                            }
                        }

                        int t_mV = 0;
                        if (baseNum > 0)
                        {
                            t_mV = markV.Where(o => o.MarkIndex < baseNum).Count() + pupilStudying - markV.Count();
                        }
                        string str_mV = t_mV + "/" + pupilStudying;

                        listV.Add(mV);
                        listStrV.Add(str_mV);

                        var markHK = listMark.Where(x => x.SubjectID == schoolSubject.SubjectID
                            && x.ClassID == classProfile.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_HK)).ToList();
                        int mHK = 0;
                        if (markHK != null && markHK.Count > 0)
                        {
                            mHK = 1;
                        }
                        //Những học sinh nhập không đủ số điểm tối đa

                        int t_mHK = 0;

                        t_mHK = pupilStudying - markHK.Count();

                        string str_mHK = t_mHK + "/" + pupilStudying;
                        listHK.Add(mHK);
                        listStrHK.Add(str_mHK);
                    }
                }
            }
            return result;
        }

        private List<MarkBO> GetMaxInputMarkOfClass(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID)
        {
            List<MarkBO> listMark = new List<MarkBO>();
            // Danh sach hoc sinh hoc trong ky
            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"CheckWithClass", "CheckWithClass"}
            }).AddCriteriaSemester(ay, Semester);
            List<MarkBO> listMark1 = (from m in VMarkRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            })
                                      where listPupil.Any(o => o.PupilID == m.PupilID && o.ClassID == m.ClassID)
                                      join mt in MarkTypeBusiness.All
                                      on m.MarkTypeID equals mt.MarkTypeID
                                      group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                      select new MarkBO
                                      {
                                          SubjectID = g.Key.SubjectID,
                                          ClassID = g.Key.ClassID,
                                          MarkIndex = g.Count(),//g.Max(x => x.OrderNumber),
                                          Title = g.Key.Title,
                                          PupilID = g.Key.PupilID
                                      }).Distinct().ToList();
            List<MarkBO> listMark2 = (from m in VJudgeRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            })
                                      where listPupil.Any(o => o.PupilID == m.PupilID && o.ClassID == m.ClassID)
                                      join mt in MarkTypeBusiness.All
                                      on m.MarkTypeID equals mt.MarkTypeID
                                      group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                      select new MarkBO
                                      {
                                          SubjectID = g.Key.SubjectID,
                                          ClassID = g.Key.ClassID,
                                          MarkIndex = g.Count(),//g.Max(x => x.OrderNumber),
                                          Title = g.Key.Title,
                                          PupilID = g.Key.PupilID
                                      }).Distinct().ToList();

            listMark.AddRange(listMark1);
            listMark.AddRange(listMark2);
            return listMark;
        }

        /// <author>phuongh1</author>
        /// <date>23/4/2013</date>
        public ReportMissingMarkSituationBO GetMissingMark(
            int SchoolID, int AcademicYearID,
            int Semester, int EducationLevelID, int ClassID)
        {
            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
            ReportMissingMarkSituationBO result = new ReportMissingMarkSituationBO();
            //Danh sach hoc sinh mien giam
            IDictionary<string, object> dicEx = new Dictionary<string, object>();
            dicEx["AcademicYearID"] = AcademicYearID;
            dicEx["EducationLevelID"] = EducationLevelID;
            dicEx["ClassID"] = ClassID;
            if (Semester == 1)
            {
                dicEx["HasFirstSemester"] = true;
            }
            if (Semester == 2)
            {
                dicEx["HasSecondSemester"] = true;
            }
            IQueryable<ExemptedSubject> lstes = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicEx);

            // Danh sach hoc sinh hoc trong kỳ
            List<PupilOfClass> listPupilInClass = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID",ClassID}
            }).AddCriteriaSemester(ay, Semester).ToList();
            //Lấy danh sách môn học cho lớp 
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID",ClassID}
            }).ToList();

            //Lấy danh sách môn học theo khối
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID}
            }).OrderBy(x => x.SubjectCat.OrderInSubject).ToList();


            if (ClassID != 0)
            {
                listSchoolSubject = listSchoolSubject.Where(x =>
                    listClassSubject.Exists(y => y.ClassID == ClassID
                        && y.SubjectID == x.SubjectID
                        && (
                        (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && y.SectionPerWeekFirstSemester > 0)
                        || (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && y.SectionPerWeekSecondSemester > 0)))).ToList();
            }
            result.ListSubjectName = listSchoolSubject.Select(x => x.SubjectCat.DisplayName).ToList();

            var temp = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID}
           }
            ).AsEnumerable().Where(poc => listPupilInClass.Any(o => o.PupilID == poc.PupilID && o.ClassID == poc.ClassID))
            .OrderBy(x => x.ClassProfile.OrderNumber.HasValue ? x.ClassProfile.OrderNumber : 0)
            .ThenBy(x => x.ClassProfile.DisplayName).ThenBy(x => x.OrderInClass)
            .ThenBy(x => x.PupilProfile.Name)
            .ThenBy(x => x.PupilProfile.FullName);
            List<PupilOfClass> listPupil = temp.ToList();

            //Lấy thông tin điểm đã nhập của lớp
            List<MarkBO> listMark = GetMaxInputMarkOfClass(SchoolID, AcademicYearID, Semester, EducationLevelID);

            //Danh sach diem hoc sinh
            IQueryable<VMarkRecord> lstMarkTemp = VMarkRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"ClassID", ClassID},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            });
            //Lấy thông tin điểm đã nhập của học sinh
            List<MarkBO> listPupilMark = new List<MarkBO>();
            List<MarkBO> listPupilMark1 = (from m in lstMarkTemp
                                           join mt in MarkTypeBusiness.All
                                      on m.MarkTypeID equals mt.MarkTypeID
                                           group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                           select new MarkBO
                                           {
                                               SubjectID = g.Key.SubjectID,
                                               ClassID = g.Key.ClassID,
                                               MarkIndex = g.Count(),
                                               Title = g.Key.Title,
                                               PupilID = g.Key.PupilID
                                           }).ToList();
            List<MarkBO> listPupilMark2 = (from m in VJudgeRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"ClassID", ClassID},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            })
                                           join mt in MarkTypeBusiness.All
                                      on m.MarkTypeID equals mt.MarkTypeID
                                           group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                           select new MarkBO
                                           {
                                               SubjectID = g.Key.SubjectID,
                                               ClassID = g.Key.ClassID,
                                               MarkIndex = g.Count(),
                                               Title = g.Key.Title,
                                               PupilID = g.Key.PupilID
                                           }).ToList();

            listPupilMark.AddRange(listPupilMark1);
            listPupilMark.AddRange(listPupilMark2);

            //Tính con điểm theo các môn
            List<MissingMarkOfPupilBO> listMoS = new List<MissingMarkOfPupilBO>();

            foreach (PupilOfClass pupilOfClass in listPupil)
            {
                MissingMarkOfPupilBO bo = new MissingMarkOfPupilBO();
                //List<MarkBO> lstMarkPP = listMark.Where(o => o.PupilID == pupilOfClass.PupilID).ToList();
                bo.PupilName = pupilOfClass.PupilProfile.FullName;

                bo.ClassName = pupilOfClass.ClassProfile.DisplayName;
                bo.Status = pupilOfClass.Status;
                List<string> listM = new List<string>();
                bo.ListMissingMark = listM;
                bool missMark = false;


                //listMoS.Add(bo);
                foreach (SchoolSubject schoolSubject in listSchoolSubject)
                {
                    //Danh sach hoc sinh duoc mien giam
                    List<int> lstpp = lstes.Where(o => o.SubjectID == schoolSubject.SubjectID).Select(o => o.PupilID).ToList();

                    //Kiểm tra lớp có học môn đó trong học kỳ x không
                    if (listClassSubject.Exists(
                        x => x.ClassID == pupilOfClass.ClassID
                            && x.SubjectID == schoolSubject.SubjectID
                            && (
                            (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                            && x.SectionPerWeekFirstSemester.GetValueOrDefault() > 0)
                            ||
                            (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            && x.SectionPerWeekSecondSemester.GetValueOrDefault() > 0)
                            )
                            ))
                    {
                        if (!lstpp.Contains(pupilOfClass.PupilID))
                        {
                            //Tính điểm 15 phút thiếu
                            int? missMarkP = 0;
                            //Danh sach so con diem da nhap
                            var lstmarkP = listMark.Where(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_P))
                                ).ToList();

                            if (lstmarkP != null && lstmarkP.Count > 0)
                            {
                                int markP = lstmarkP.Max(o => o.MarkIndex);
                                MarkBO mMarkP = listPupilMark.Find(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_P)
                                && x.PupilID == pupilOfClass.PupilID));
                                //Lay so con diem toi da tru di so con diem da nhap
                                if (mMarkP != null)
                                {
                                    missMarkP = markP - mMarkP.MarkIndex;
                                }
                                else
                                {
                                    missMarkP = markP;
                                }
                            }


                            listM.Add(missMarkP.ToString());
                            if (missMarkP > 0) missMark = true;

                            //Tính điểm 1 tiết thiếu
                            int? missMarkV = 0;
                            //Danh sach so con diem da nhap
                            var lstmarkV = listMark.Where(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_V))).ToList();
                            if (lstmarkV != null && lstmarkV.Count > 0)
                            {
                                int markV = lstmarkV.Max(o => o.MarkIndex);
                                MarkBO mMarkV = listPupilMark.Find(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_V)
                                && x.PupilID == pupilOfClass.PupilID));
                                //Lay so con diem toi da tru di so con diem da nhap
                                if (mMarkV != null)
                                {
                                    missMarkV = markV - mMarkV.MarkIndex;
                                }
                                else
                                {
                                    missMarkV = markV;
                                }
                            }
                            listM.Add(missMarkV.ToString());
                            if (missMarkV > 0) missMark = true;

                            //Tính điểm Hoc ky
                            int? missMarkHK = 0;
                            //Danh sach so con diem da nhap
                            var lstmarkHK = listMark.Where(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_HK))).ToList();
                            if (lstmarkHK != null && lstmarkHK.Count > 0)
                            {
                                MarkBO mMarkHK = listPupilMark.Find(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_HK)
                                && x.PupilID == pupilOfClass.PupilID));
                                //Lay so con diem toi da tru di so con diem da nhap
                                if (mMarkHK == null)
                                {
                                    missMarkHK = 1;
                                }

                            }
                            listM.Add(missMarkHK.ToString());
                            if (missMarkHK > 0) missMark = true;
                        }
                        else
                        {
                            listM.Add("MG");
                            listM.Add("MG");
                            listM.Add("MG");
                        }
                    }
                    else
                    {
                        listM.Add("");
                        listM.Add("");
                        listM.Add("");
                    }

                }
                if (missMark)
                {
                    listMoS.Add(bo);
                }
            }
            result.ListPupil = listMoS;
            return result;
        }

        public ProcessedReport InsertMarkInputSituationReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.HS_THPT_TKTINHHINHNHAPDIEM_KHOI10_HKI;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", educationLevel.ToString());

            int semester = Utils.GetInt(dic["Semester"]);
            string strSemester = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";
            outputNamePattern = outputNamePattern.Replace("[Semester]", strSemester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

    }
}
