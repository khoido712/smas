/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class DisabledTypeBusiness
    {
        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="DisabledType"></param>
        /// <returns></returns>
        public override DisabledType Insert(DisabledType DisabledType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(DisabledType.Resolution, "DisableType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(DisabledType.Resolution, 50, "DisableType_Label_Resolution");
            // Kiem tra trung ten 
            new DisabledTypeBusiness(null).CheckDuplicate(DisabledType.Resolution, GlobalConstants.LIST_SCHEMA, "DisabledType", "Resolution", true, DisabledType.DisabledTypeID, "DisableType_Label_Resolution");
            //this.CheckDuplicate(DisabledType.Resolution, GlobalConstants.LIST_SCHEMA, "DisabledType", "Resolution", true, null, "DisableType_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(DisabledType.Description, 400, "DisableType_Label_Description");
            // Them vao CSDL            
            return base.Insert(DisabledType);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="DisabledType"></param>
        /// <returns></returns>
        public override DisabledType Update(DisabledType DisabledType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(DisabledType.Resolution, "DisableType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(DisabledType.Resolution, 50, "DisableType_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(DisabledType.Resolution, GlobalConstants.LIST_SCHEMA, "DisabledType", "Resolution", true, DisabledType.DisabledTypeID, "DisableType_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(DisabledType.Description, 400, "DisableType_Label_Description");
            // Kiem tra ten loai khuyet tat da co trong DB
            //this.CheckAvailable(DisabledType.DisabledTypeID, "DisableType_Label_Resolution", false);
            new DisabledTypeBusiness(null).CheckAvailable(DisabledType.DisabledTypeID, "DisableType_Label_Resolution", false);
            // Cap nhat vao CSDL  
            return base.Update(DisabledType);
        }

        /// <summary>
        /// Xoa loai khuyet tat
        /// </summary>
        /// <param name="DisabledTypeID"></param>
        public void Delete(int DisabledTypeID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(DisabledTypeID, "DisableType_Label_Title", true);
            this.CheckConstraintsWithIsActive(GlobalConstants.LIST_SCHEMA, "DisabledType", DisabledTypeID, "DisableType_Label_Title");
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(DisabledTypeID, true);
        }

        /// <summary>
        /// Tim kiem loai khuyet tat
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<DisabledType> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            string Description = Utils.GetString(dicParam, "Description");
            bool? IsActive = Utils.GetNullableBool(dicParam, "IsActive");
            IQueryable<DisabledType> listDisabledType = this.DisabledTypeRepository.All.OrderBy(o => o.Resolution);
            if (!Resolution.Equals(string.Empty))
            {
                listDisabledType = listDisabledType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (!Description.Equals(string.Empty))
            {
                listDisabledType = listDisabledType.Where(x => x.Description.ToLower().Contains(Description.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listDisabledType = listDisabledType.Where(x => x.IsActive == true);
            }
            //int count = listDisabledType != null ? listDisabledType.Count() : 0;
            //if (count == 0)
            //{
            //    return null;
            //}
            return listDisabledType;

        }
        
    }
}