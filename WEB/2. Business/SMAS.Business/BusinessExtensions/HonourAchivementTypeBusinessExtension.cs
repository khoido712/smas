/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class HonourAchivementTypeBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int PROVINCECODE_MAX_LENGTH = 10; //Độ dài mã tỉnh thành phố
        private const int PROVINCENAME_MAX_LENGTH = 50; //Độ dài tên tỉnh thành phố
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn
        private const int VALUE_MIN = 1;//Giá trị min
        private const int VALUE_MAX = 3;//Giá trị max
        #endregion


        #region Search

        /// <summary>
        /// Tìm kiếm thông tin danh mục danh hiệu thi đua
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// 
        /// <param name="Resolution">Diễn giải danh hiệu thi đua</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="Type">Loại danh hiệu thi đua</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// 
        /// <returns>IQueryable<HonourAchivementType></returns>
        public IQueryable<HonourAchivementType> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            int Type = (int)Utils.GetInt(dic,"Type");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<HonourAchivementType> lsHonourAchivementType = HonourAchivementTypeRepository.All;
            if (IsActive.HasValue)
            {
                lsHonourAchivementType = lsHonourAchivementType.Where(hon => hon.IsActive == IsActive);
            }
            if (Resolution.Trim().Length != 0)
            {
                lsHonourAchivementType = lsHonourAchivementType.Where(hon => hon.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (Description.Trim().Length != 0)
            {
                lsHonourAchivementType = lsHonourAchivementType.Where(hon => hon.Description.ToLower().Contains(Description.ToLower()));
            }
            if (Type != 0)
            {
                lsHonourAchivementType = lsHonourAchivementType.Where(hon => hon.Type == Type);
            }

            return lsHonourAchivementType;

        }
        #endregion

        #region Delte

        /// <summary>
        /// Xóa thông tin danh mục danh hiệu thi đua
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="HonourAchivementTypeID"> IDHonourAchivementType</param>
        public void Delete(int HonourAchivementTypeID)
        {
            //Bạn chưa chọn loại danh hiệu thi đua cần xóa hoặc loại danh hiệu thi đua bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable((int)HonourAchivementTypeID, "HonourAchivementType_Label_HonourAchivementTypeID", true);

            //Không thể xóa loại danh hiệu thi đua đang sử dụng
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "HonourAchivementType", HonourAchivementTypeID, "HonourAchivementType_Label_HonourAchivementTypeID");
            base.Delete(HonourAchivementTypeID, true);
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm thông tin danh mục danh hiệu thi đua
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="honourAchivementType">Đối tượng danh hiệu thi đua</param>
        /// <returns>Đối tượng danh hiệu thi đua</returns>
        public override HonourAchivementType Insert(HonourAchivementType honourAchivementType)
        {
            //Tên loại danh hiệu thi đua không được để trống 
            Utils.ValidateRequire(honourAchivementType.Resolution, "HonourAchivementType_Label_Resolution");
            //Độ dài trường loại danh hiệu thi đua không được vượt quá 50 
            Utils.ValidateMaxLength(honourAchivementType.Resolution, RESOLUTION_MAX_LENGTH, "HonourAchivementType_Label_Resolution");
            //Đối tượng áp dụng chỉ nhận giá trị 1,2,3 – Kiểm tra Type
            Utils.ValidateRange((int)honourAchivementType.Type, VALUE_MIN, VALUE_MAX, "HonourAchivementType_Label_Type");
            //Tên loại danh hiệu thi đua đã tồn tại 
            this.CheckDuplicate(honourAchivementType.Resolution, GlobalConstants.LIST_SCHEMA, "HonourAchivementType", "Resolution", true, honourAchivementType.HonourAchivementTypeID, "HonourAchivementType_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(honourAchivementType.Description, DESCRIPTION_MAX_LENGTH, "Description");


            //Insert
            return base.Insert(honourAchivementType);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa thông tin danh mục danh hiệu thi đua
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="honourAchivementType">Đối tượng danh hiệu thi đua</param>
        /// <returns>Đối tượng danh hiệu thi đua</returns>
        public override HonourAchivementType Update(HonourAchivementType honourAchivementType)
        {
            //Bạn chưa chọn loại danh hiệu thi đua cần sửa hoặc loại danh hiệu thi đua bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable((int)honourAchivementType.HonourAchivementTypeID, "HonourAchivementType_Label_HonourAchivementTypeID", true);
            //Tên loại danh hiệu thi đua không được để trống 
            Utils.ValidateRequire(honourAchivementType.Resolution, "HonourAchivementType_Label_Resolution");
            //Độ dài trường loại danh hiệu thi đua không được vượt quá 50 
            Utils.ValidateMaxLength(honourAchivementType.Resolution, RESOLUTION_MAX_LENGTH, "HonourAchivementType_Label_Resolution");
            //Đối tượng áp dụng chỉ nhận giá trị 1,2,3 – Kiểm tra Type
            Utils.ValidateRange((int)honourAchivementType.Type, VALUE_MIN, VALUE_MAX, "HonourAchivementType_Label_Type");
            //Tên loại danh hiệu thi đua đã tồn tại 
            this.CheckDuplicate(honourAchivementType.Resolution, GlobalConstants.LIST_SCHEMA, "HonourAchivementType", "Resolution", true, honourAchivementType.HonourAchivementTypeID, "HonourAchivementType_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(honourAchivementType.Description, DESCRIPTION_MAX_LENGTH, "Description");


            return base.Update(honourAchivementType);
        }
        #endregion
    }
}
