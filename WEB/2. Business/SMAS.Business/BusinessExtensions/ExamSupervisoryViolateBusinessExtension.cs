﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{ 
    public partial class ExamSupervisoryViolateBusiness
    {
        public IQueryable<ExamSupervisoryViolateBO> Search(IDictionary<string, object> dic)
        {
            long examinationsID = Utils.GetInt(dic, "ExaminationsID");
            long examGroupID = Utils.GetInt(dic, "ExamGroupID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            long examRoomID = Utils.GetInt(dic, "ExamRoomID");
            string supervisoryName = Utils.GetString(dic, "ExamSupervisoryName");

            IQueryable<ExamSupervisoryViolateBO> query = from esv in ExamSupervisoryViolateRepository.All
                                                         join eg in ExamGroupRepository.All on esv.ExamGroupID equals eg.ExamGroupID
                                                         join es in ExamSupervisoryRepository.All on esv.ExamSupervisoryID equals es.ExamSupervisoryID
                                                         join e in EmployeeRepository.All on es.TeacherID equals e.EmployeeID
                                                         join en in this.EthnicRepository.All on e.EthnicID equals en.EthnicID into des
                                                         from x in des.DefaultIfEmpty()
                                                         join sc in SubjectCatRepository.All on esv.SubjectID equals sc.SubjectCatID
                                                         join er in ExamRoomRepository.All on esv.ExamRoomID equals er.ExamRoomID
                                                         join evt in ExamViolationTypeRepository.All on esv.ExamViolationTypeID equals evt.ExamViolationTypeID
                                                         where e.IsActive==true
                                                         select new ExamSupervisoryViolateBO
                                                         {
                                                             ContentViolated = esv.ContentViolated,
                                                             EmployeeCode = e.EmployeeCode,
                                                             EmployeeName = e.FullName,
                                                             EthnicCode = x != null ? x.EthnicCode : null,
                                                             ExamGroupID = esv.ExamGroupID,
                                                             ExaminationsID = esv.ExaminationsID,
                                                             ExamRoomCode = er.ExamRoomCode,
                                                             ExamRoomID = esv.ExamRoomID,
                                                             ExamSupervisoryViolateID = esv.ExamSupervisoryViolateID,
                                                             ExamViolatetionTypeID = esv.ExamViolationTypeID,
                                                             ExamViolatetionTypeResolusion = evt.Resolution,
                                                             SubjectID = esv.SubjectID,
                                                             SubjectName = sc.SubjectName,
                                                             ExamSupervisoryID = esv.ExamSupervisoryID,
                                                             BirtDate=e.BirthDate,
                                                             Genre=e.Genre,
                                                             ExamGroupName=eg.ExamGroupName,

                                                         };


            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (supervisoryName.Trim().Length > 0)
            {
                query = query.Where(o => (o.EmployeeName.ToUpper().Contains(supervisoryName.ToUpper())));
            }

            return query;
        }

        /// <summary>
        /// Xoa theo danh sach ID
        /// </summary>
        /// <param name="listID"></param>
        public void DeleteList(List<long> listID)
        {
            //Kiem tra list rong
            if (listID.Count == 0)
            {
                throw new BusinessException("ExamSupervisoryViolate_Validate_Delete_NoChoice");
            }

            for (int i = 0; i < listID.Count; i++)
            {
                this.ExamSupervisoryViolateBusiness.Delete(listID[i]);
            }
            this.ExamSupervisoryViolateBusiness.Save();
        }

        public IQueryable<ExamSupervisoryViolate> SearchViolate(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examSupervisoryID = Utils.GetLong(search, "ExamSupervisoryID");

            IQueryable<ExamSupervisoryViolate> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examSupervisoryID != 0)
            {
                query = query.Where(o => o.ExamSupervisoryID == examSupervisoryID);
            }

            return query;
        }

        #region Report
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamSupervisoryViolateReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_GIAM_THI_VI_PHAM;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamSupervisoryViolateReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_GIAM_THI_VI_PHAM;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Điền các thông tin tiêu đề báo cáo
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName = "Kỳ Thi: " + exam.ExaminationsName;
            string strAcademicYear = "Năm Học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("A7", examinationsName);
            oSheet.SetCellValue("A8", strAcademicYear);
            oSheet.SetCellValue("G4", provinceAndDate);


            //Fill du lieu
            List<ExamSupervisoryViolateBO> listResult = this.ExamSupervisoryViolateBusiness.Search(dic).ToList()
                                                        .OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeName.getOrderingName(o.EthnicCode))).ToList();

            int startRow = 11;
            int lastRow = startRow + listResult.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 9;
            int curColumn = startColumn;

            for (int i = 0; i < listResult.Count; i++)
            {
                ExamSupervisoryViolateBO obj = listResult[i];
                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //Ho va ten
                oSheet.SetCellValue(curRow, curColumn, obj.EmployeeName);
                curColumn++;
                //Ngay sinh
                oSheet.SetCellValue(curRow, curColumn, obj.BirtDate);
                curColumn++;
                //Gioi tinh
                oSheet.SetCellValue(curRow, curColumn, obj.GenreName);
                curColumn++;
                //Nhom thi
                oSheet.SetCellValue(curRow, curColumn, obj.ExamGroupName);
                curColumn++;
                //Mon thi
                oSheet.SetCellValue(curRow, curColumn, obj.SubjectName);
                curColumn++;
                //Phong thi
                oSheet.SetCellValue(curRow, curColumn, obj.ExamRoomCode);
                curColumn++;
                //Noi dung vi pham
                oSheet.SetCellValue(curRow, curColumn, obj.ContentViolated);
                curColumn++;
                //Hinh thuc xu ly
                oSheet.SetCellValue(curRow, curColumn, obj.ExamViolatetionTypeResolusion);
                curColumn++;


                //Ve khung cho dong
                IVTRange range = oSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Dotted;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                curRow++;
                curColumn = startColumn;
            }

            IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

            oSheet.Orientation = VTXPageOrientation.VTxlPortrait;
            oSheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamSupervisoryViolateReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_GIAM_THI_VI_PHAM;

            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long subjectID = Utils.GetLong(dic["SubjectID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);

            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);
            ExamRoom examRoom = ExamRoomBusiness.Find(examRoomID);

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[Examinations]", ReportUtils.StripVNSign(exam.ExaminationsName));
            if (examGroup != null)
            {
                outputNamePattern = outputNamePattern + "_" + ReportUtils.StripVNSign(examGroup.ExamGroupName);
            }
            if (subject != null)
            {
                outputNamePattern = outputNamePattern + "_" + ReportUtils.StripVNSign(subject.DisplayName);
            }
            if (examRoom != null)
            {
                outputNamePattern = outputNamePattern + "_" + ReportUtils.StripVNSign(examRoom.ExamRoomCode);
            }

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion 
    }
}