﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Globalization;
using Oracle.DataAccess.Client;

namespace SMAS.Business.Business
{
    public partial class SchoolWeekBusiness
    {
        public void Insert(List<SchoolWeek> lsSchoolWeek)
        {
            foreach (SchoolWeek item in lsSchoolWeek)
            {
                ValidationMetadata.ValidateObject(item);
                if (item.FromDate > item.ToDate)
                {
                    throw new BusinessException("SchoolWeek_Validate_DOW");
                }
                base.Insert(item);
            }
        }

        public void Delete(List<int> lsSchoolWeekID)
        {
            if (lsSchoolWeekID == null || lsSchoolWeekID.Count <= 0) return;
            for (int i = 0; i < lsSchoolWeekID.Count; i++)
            {
                this.Delete(lsSchoolWeekID[i]);
            }
            this.Save();
        }

        /*public List<SchoolWeek> getListSchoolWeek(IDictionary<string, object> SearchInfo, bool autoGenerateWeek)
        {
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            DateTime? startDayOfAcademic = Utils.GetDateTime(SearchInfo, "FirstSemesterStartDate");
            DateTime? endDayOfAcademic = Utils.GetDateTime(SearchInfo, "SecondSemesterEndDate");
            int orderWeek = Utils.GetInt(SearchInfo, "OrderWeek");
            var query = SchoolWeekBusiness.All.Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID && x.FromDate < endDayOfAcademic && x.ToDate > startDayOfAcademic);
            if (orderWeek > 0)
            {
                query = query.Where(x => x.OrderWeek == orderWeek);
            }
            List<SchoolWeek> lstSchoolWeek = query.OrderBy(x => x.OrderWeek).ToList();
            if (autoGenerateWeek && lstSchoolWeek != null && lstSchoolWeek.Count == 0)
            {
                GenerateWeek(SearchInfo);
                lstSchoolWeek = SchoolWeekBusiness.All.Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID && x.FromDate < endDayOfAcademic && x.ToDate > startDayOfAcademic).OrderBy(x => x.OrderWeek).ToList();
            }
            return lstSchoolWeek;
        }*/

        public void AutoGenerateWeek(IDictionary<string, object> SearchInfo)
        {
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            DateTime? startDayOfAcademic = Utils.GetDateTime(SearchInfo, "FirstSemesterStartDate");
            DateTime? endDayOfAcademic = Utils.GetDateTime(SearchInfo, "SecondSemesterEndDate");

            var lstResult = SchoolWeekBusiness.All.Where(x => x.SchoolID == SchoolID
                                                    && x.AcademicYearID == AcademicYearID
                                                    && x.FromDate < endDayOfAcademic
                                                    && x.ToDate > startDayOfAcademic).ToList();
            // khi chưa có dữ liệu
            if (lstResult.Count() == 0)
            {
                GenerateWeek(SearchInfo);
            }
            else if (lstResult.Count() > 0)
            { // khi tạo thêm lớp mới thì tạo thêm schoolweek cho lớp đó
                List<int> lstClassID = Utils.GetIntList(SearchInfo, "lstClassID");
                List<int> lstClassIdInSW = lstResult.Select(x => x.ClassID).Distinct().ToList();

                int? classID = 0;
                foreach (var item in lstClassIdInSW)
                {
                    classID = lstClassID.Where(x => x == item).FirstOrDefault();
                    if (classID.HasValue && classID.Value > 0)
                    {
                        lstClassID.Remove(item);
                    }
                }

                if (lstClassID.Count > 0)
                {
                    SearchInfo["lstClassID"] = lstClassID;
                    GenerateWeek(SearchInfo);
                }
            }
        }

        public List<SchoolWeek> getListSchoolWeekForEdit(IDictionary<string, object> SearchInfo, bool autoGenerateWeek)
        {
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");

            DateTime? startDayOfAcademic = Utils.GetDateTime(SearchInfo, "FirstSemesterStartDate");
            DateTime? endDayOfAcademic = Utils.GetDateTime(SearchInfo, "SecondSemesterEndDate");
            List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.All.Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID).OrderBy(x => x.OrderWeek).ToList();
            if (autoGenerateWeek && lstSchoolWeek.Count == 0)
            {
                GenerateWeek(SearchInfo);
                lstSchoolWeek = SchoolWeekBusiness.All.Where(x => x.SchoolID == SchoolID
                                                        && x.AcademicYearID == AcademicYearID
                                                        && x.ClassID == ClassID)
                                                        .OrderBy(x => x.OrderWeek).ToList();
            }
            return lstSchoolWeek;
        }

        public IQueryable<SchoolWeek> Search(IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int OrderWeek = Utils.GetInt(dic, "OrderWeek");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            string fromDateToDate = Utils.GetString(dic, "fromDateToDate");

            IQueryable<SchoolWeek> iqSchoolWeek = SchoolWeekBusiness.All;

            if (SchoolID > 0)
                iqSchoolWeek = iqSchoolWeek.Where(x => x.SchoolID == SchoolID);
            if (AcademicYearID > 0)
                iqSchoolWeek = iqSchoolWeek.Where(x => x.AcademicYearID == AcademicYearID);
            if (ClassID > 0)
                iqSchoolWeek = iqSchoolWeek.Where(x => x.ClassID == ClassID);
            if (lstClassID.Count > 0)
                iqSchoolWeek = iqSchoolWeek.Where(x => lstClassID.Contains(x.ClassID));
            if (OrderWeek > 0)
                iqSchoolWeek = iqSchoolWeek.Where(x => x.OrderWeek == OrderWeek);

            if (!string.IsNullOrEmpty(fromDateToDate))
            {
                List<string> lstFromTo = fromDateToDate.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => x).ToList();
                DateTime fromDate = Convert.ToDateTime(lstFromTo[0]);
                DateTime toDate = Convert.ToDateTime(lstFromTo[1]);

                iqSchoolWeek = iqSchoolWeek.Where(x => x.FromDate >= fromDate && x.FromDate <= toDate);
            }

            //List<SchoolWeek> lstSchoolWeek = iqSchoolWeek.OrderBy(x => x.OrderWeek).ToList();
            return iqSchoolWeek.OrderBy(s => s.OrderWeek);

        }

        public void GenerateWeek(IDictionary<string, object> schoolInfo)
        {
            try
            {
                SchoolWeekBusiness.SetAutoDetectChangesEnabled(false);
                List<int> lstClassID = Utils.GetIntList(schoolInfo, "lstClassID");
                int AcademicYearID = Utils.GetInt(schoolInfo, "AcademicYearID");
                int SchoolId = Utils.GetInt(schoolInfo, "SchoolID");

                // lay ngay thu 2 cua tuan bat dau nam hoc
                DateTime? startDayOfAcademic = Utils.GetDateTime(schoolInfo, "FirstSemesterStartDate");
                int delta = DayOfWeek.Monday - startDayOfAcademic.Value.DayOfWeek;
                DateTime mondaystartDayOfAcademic = startDayOfAcademic.Value.AddDays(delta);

                //Lay ngay CN cua tuan ket thuc nam hoc
                DateTime? endDayOfAcademic = Utils.GetDateTime(schoolInfo, "SecondSemesterEndDate");
                int delta2 = DayOfWeek.Monday - endDayOfAcademic.Value.DayOfWeek;
                DateTime sundayEndDayOfAcademic = endDayOfAcademic.Value.AddDays(delta2 + 6);

                //Tao tuan tu dong
                TimeSpan diffDay = (sundayEndDayOfAcademic - mondaystartDayOfAcademic);
                SchoolWeek sw;

                int k = 0;
                for (int n = 0; n < lstClassID.Count(); n++)
                {
                    k = 1;
                    for (int i = 0; i <= diffDay.TotalDays; i += 7)
                    {
                        sw = new SchoolWeek();
                        sw.AcademicYearID = AcademicYearID;
                        sw.SchoolID = SchoolId;
                        sw.ClassID = lstClassID[n];
                        sw.FromDate = mondaystartDayOfAcademic.AddDays(i);
                        sw.ToDate = mondaystartDayOfAcademic.AddDays(i + 6).AddHours(23).AddMinutes(59).AddSeconds(55);
                        sw.CreatedTime = DateTime.Now;
                        sw.UpdatedTime = DateTime.Now;
                        sw.OrderWeek = k;
                        k++;
                        this.SchoolWeekBusiness.Insert(sw);
                    }
                }

                this.Save();
            }
            finally
            {
                SchoolWeekBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public bool AppliedEducationAndSchool(IDictionary<string, object> dic,
            List<string> listFromDate,
            List<string> listToDate)
        {
            SchoolWeekBusiness.SetAutoDetectChangesEnabled(false);

            int classIdSelected = Utils.GetInt(dic, "ClassIDSelected");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolId = Utils.GetInt(dic, "SchoolID");
            List<int> listSchoolWeekID = Utils.GetIntList(dic, "listSchoolWeekID");
            var lstClass = this.ClassProfileBusiness.SearchBySchool(schoolId, dic)
                                                    .Where(x => x.IsActive.HasValue && x.IsActive.Value == true)
                                                    .Where(x => x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                        || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                                                        || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                                    .OrderBy(u => u.DisplayName).ToList();
            List<int> lstClassID = lstClass.Select(x => x.ClassProfileID).ToList();

            dic.Add("lstClassID", lstClassID);
            List<SchoolWeek> lstSchoolWeekDB = SchoolWeekBusiness.Search(dic).ToList();
            List<SchoolWeek> lstSchoolWeekOfClassSelected = lstSchoolWeekDB.Where(x => x.ClassID == classIdSelected).ToList();

            List<EvaluaSchoolWeek> lstEval = new List<EvaluaSchoolWeek>();
            EvaluaSchoolWeek objEval = null; ;

            SchoolWeek objSchoolWeek = null;
            for (int k = 0; k < listSchoolWeekID.Count(); k++)
            {
                objSchoolWeek = lstSchoolWeekOfClassSelected.Where(x => x.SchoolWeekID == listSchoolWeekID[k]).FirstOrDefault();
                if (objSchoolWeek != null)
                {
                    objSchoolWeek.FromDate = DateTime.ParseExact(listFromDate[k], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    objSchoolWeek.ToDate = DateTime.ParseExact(listToDate[k], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    objSchoolWeek.UpdatedTime = DateTime.Now;
                    this.SchoolWeekBusiness.Update(objSchoolWeek);

                    objEval = new EvaluaSchoolWeek();
                    objEval.OrderWeek = objSchoolWeek.OrderWeek;
                    objEval.FromDate = objSchoolWeek.FromDate;
                    objEval.ToDate = objSchoolWeek.ToDate;
                    lstEval.Add(objEval);
                }
            }

            for (int i = 0; i < lstClassID.Count(); i++)
            {
                if (lstClassID[i] == classIdSelected)
                    continue;

                for (int p = 0; p < lstEval.Count; p++)
                {
                    objSchoolWeek = lstSchoolWeekDB.Where(x => x.ClassID == lstClassID[i]
                                                && x.OrderWeek == lstEval[p].OrderWeek).FirstOrDefault();
                    if (objSchoolWeek != null)
                    {
                        objSchoolWeek.FromDate = lstEval[p].FromDate;
                        objSchoolWeek.ToDate = lstEval[p].ToDate;
                        objSchoolWeek.UpdatedTime = DateTime.Now;
                        this.SchoolWeekBusiness.Update(objSchoolWeek);
                    }
                    else
                    {
                        objSchoolWeek = new SchoolWeek();
                        objSchoolWeek.AcademicYearID = academicYearID;
                        objSchoolWeek.SchoolID = schoolId;
                        objSchoolWeek.ClassID = lstClassID[i];
                        objSchoolWeek.FromDate = lstEval[p].FromDate;
                        objSchoolWeek.ToDate = lstEval[p].ToDate;
                        objSchoolWeek.CreatedTime = DateTime.Now;
                        objSchoolWeek.UpdatedTime = DateTime.Now;
                        objSchoolWeek.OrderWeek = lstEval[p].OrderWeek;
                        this.SchoolWeekBusiness.Insert(objSchoolWeek);
                    }
                }
            }

            SchoolWeekBusiness.SetAutoDetectChangesEnabled(true);
            this.SchoolWeekBusiness.Save();
            return true;
        }

        private class EvaluaSchoolWeek
        {
            public int OrderWeek { get; set; }
            public DateTime? FromDate { get; set; }
            public DateTime? ToDate { get; set; }
        }
    }
}
