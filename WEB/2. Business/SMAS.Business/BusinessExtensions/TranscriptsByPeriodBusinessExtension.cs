﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class TranscriptsByPeriodBusiness
    {
        private const int Subject_Start_Title_Column_Index = 4;
        private const int Subject_Start_Title_Row_Index = 8;

        private const string Diem_Mieng = "Điểm miệng";
        private const string Diem_15P = "15 phút";
        private const string Diem_1Tiet = "1 tiết";

        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm học sinh theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public string GetHashKeyForTranscriptsByPeriod(TranscriptOfClass entity)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = entity.ClassID;
            dic["SubjectID"] = entity.SubjectID;
            dic["PeriodDeclarationID"] = entity.PeriodDeclarationID;
            dic["SchoolID"] = entity.SchoolID;
            dic["AcademicYearID"] = entity.AcademicYearID;
            return ReportUtils.GetHashKey(dic);
        }

        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <param name="Data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public ProcessedReport InsertTranscriptsByPeriod(TranscriptOfClass entity, Stream Data)
        {
            // Kiem tra comparetiable
            IDictionary<string, object> SearchPeriodSchool = new Dictionary<string, object>();
            SearchPeriodSchool["SchoolID"] = entity.SchoolID;
            SearchPeriodSchool["PeriodDeclarationID"] = entity.PeriodDeclarationID;

            bool PeriodSchoolExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "PeriodDeclaration", SearchPeriodSchool, null);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT);

            if (!PeriodSchoolExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForTranscriptsByPeriod(entity);
            pr.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string className = "";
            string educationlevelName = string.Empty;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            if (entity.ClassID != 0)
                className = ClassProfileBusiness.Find(entity.ClassID).DisplayName;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string periodName = PeriodDeclarationBusiness.Find(entity.PeriodDeclarationID).Resolution;
            string subjectName = entity.SubjectID > 0 ? SubjectCatBusiness.Find(entity.SubjectID).SubjectName: "All";

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);            
            outputNamePattern = outputNamePattern.Replace("[Class]", className);            
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[Period]", periodName);
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            //HS_[SchoolLevel]_BangDiem_[Class]_[Semester]_[Period]_[Subject]
            //HS_[SchoolLevel]_BangDiemTH_[EducationLevel/Class]_[Semester]_[Period]

            // ReportParameter
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Period", entity.PeriodDeclarationID},
                {"ClassID ", entity.ClassID},
                {"SubjectID", entity.SubjectID} ,
                {"SchoolID", entity.SchoolID},
                {"AcademicYearID", entity.AcademicYearID}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// <summary>
        /// Lấy bảng điểm học sinh theo đợtđược cập nhật mới nhất
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public ProcessedReport GetTranscriptsByPeriod(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT;
            string inputParameterHashKey = GetHashKeyForTranscriptsByPeriod(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm tổng hợp học sinh cấp 2,3 theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public string GetHashKeyForSummariseTranscriptsByPeriod(TranscriptOfClass entity)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = entity.AcademicYearID;
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["ClassID"] = entity.ClassID;
            dic["PeriodDeclarationID"] = entity.PeriodDeclarationID;
            dic["SchoolID"] = entity.SchoolID;
            return ReportUtils.GetHashKey(dic);
        }

        /// <summary>
        /// Lưu lại thông tin bảng điểm tổng hợp học sinh cấp 2,3 theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <param name="Data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public ProcessedReport InsertSummariseTranscriptsByPeriod(TranscriptOfClass entity, Stream Data)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();
            ProcessedReport.ReportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = GetHashKeyForSummariseTranscriptsByPeriod(entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT);
            string outputNamePattern = reportDef.OutputNamePattern;
            EducationLevel educationLevel = EducationLevelBusiness.Find(entity.EducationLevelID);
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(educationLevel.Grade);
            string className = entity.ClassID > 0 ? ClassProfileBusiness.Find(entity.ClassID).DisplayName : string.Empty;
            string periodName = PeriodDeclarationBusiness.Find(entity.PeriodDeclarationID).Resolution;
            string educationLevelName = educationLevel.Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelName) + "/" + ReportUtils.StripVNSign(className));
            outputNamePattern = outputNamePattern.Replace("[Class]", className);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[Period]", periodName);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            // ReportParameter
            IDictionary<string, object> dic = new Dictionary<string, object> {                
                {"AcademicYearID", entity.AcademicYearID},                
                {"EducationLevelID", entity.EducationLevelID},                
                {"ClassID ", entity.ClassID},
                {"PeriodDeclarationID", entity.PeriodDeclarationID},
                {"SchoolID", entity.SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            ProcessedReportRepository.Insert(ProcessedReport);
            ProcessedReportRepository.Save();
            return ProcessedReport;
        }

        /// <summary>
        /// Lấy bảng điểm tổng hợp học sinh cấp 2,3 theo đợtđược cập nhật mới nhất
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public ProcessedReport GetSummariseTranscriptsByPeriod(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT;
            string inputParameterHashKey = GetHashKeyForSummariseTranscriptsByPeriod(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public Stream CreateTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass)
        {
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANGDIEMLOPCAP23THEODOT);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            AcademicYear acaYear = AcademicYearBusiness.Find(TranscriptOfClass.AcademicYearID);

            //Set variable value
            Dictionary<string, object> variableDic = new Dictionary<string, object>();
            //variableDic["SchoolLevel"] = ReportUtils.ConvertAppliedLevelForReportName(TranscriptOfClass.AppliedLevel);
            SchoolProfile sp = SchoolProfileBusiness.Find(TranscriptOfClass.SchoolID);
            string schoolName = sp.SchoolName.ToUpper();
            string temp = schoolName.Trim().Substring(0, 6).ToUpper();
            if (!temp.Contains("TRƯỜNG"))
            {
                schoolName = "TRƯỜNG " + schoolName.ToUpper();
            }
            variableDic["SchoolName"] = schoolName;
            string periodName = PeriodDeclarationBusiness.Find(TranscriptOfClass.PeriodDeclarationID).Resolution.ToUpper();
            variableDic["PeriodResolution"] = periodName;
            variableDic["academicYearTitle"] = acaYear.Year + " - " + (acaYear.Year + 1);
            if (TranscriptOfClass.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                variableDic["Semester"] = "1";
            }
            else
            {
                variableDic["Semester"] = "2";
            }


            IDictionary<string, object> DicClass = new Dictionary<string, object>();
            DicClass["EducationLevelID"] = TranscriptOfClass.EducationLevelID;
            DicClass["SchoolID"] = TranscriptOfClass.SchoolID;
            DicClass["IsVNEN"] = true;
            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            if (TranscriptOfClass.SubjectID == 0)
            {
                lstClassSubject = ClassSubjectBusiness.SearchByClass(TranscriptOfClass.ClassID, DicClass).OrderBy(u => u.SubjectCat.OrderInSubject).ToList();
            }
            else
            {
                lstClassSubject = ClassSubjectBusiness.SearchByClass(TranscriptOfClass.ClassID, DicClass).Where(u => u.SubjectID == TranscriptOfClass.SubjectID).OrderBy(u => u.SubjectCat.OrderInSubject).ToList();
            }
            var lstPupilOfClass = PupilOfClassBusiness
                                        .SearchBySchool(TranscriptOfClass.SchoolID, new Dictionary<string, object> { { "ClassID", TranscriptOfClass.ClassID }, { "Check", "Check" } })
                                        .Select(u => new { u.PupilID, u.PupilProfile.PupilCode, u.PupilProfile.FullName, u.PupilProfile.Name, u.Status, u.OrderInClass })
                                        .ToList().OrderBy(u => u.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName))
                                        .ToList();

            List<int> lstPupilOfClassHK = PupilOfClassBusiness.GetPupilInSemesterNoCheckAssignDate(TranscriptOfClass.AcademicYearID, TranscriptOfClass.SchoolID, TranscriptOfClass.EducationLevelID, TranscriptOfClass.Semester, TranscriptOfClass.ClassID, null)
                .Select(u => u.PupilID)
                .ToList();

            if ((TranscriptOfClass.EducationLevelID >= 6 && TranscriptOfClass.EducationLevelID <= 12)
                && (acaYear.IsShowPupil.HasValue && acaYear.IsShowPupil.Value == true))
            {
                List<int> lstPupilRemove = lstPupilOfClass.Where(x => x.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED &&
                    x.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING).Select(x=>x.PupilID).ToList();

                lstPupilOfClass = lstPupilOfClass.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

                if (lstPupilRemove.Count > 0)
                {
                    lstPupilOfClassHK = lstPupilOfClassHK.Where(x => !lstPupilRemove.Contains(x)).ToList();

                }
            }

            IEnumerable<string> titles = MarkRecordBusiness.GetMarkTitle(TranscriptOfClass.PeriodDeclarationID).Split(',').Where(u => !string.IsNullOrEmpty(u));
            var titleGroup = titles.Select(u => u[0]).GroupBy(u => u).Select(v => new { StartCharacter = v.Key, Count = v.Count() });

            IVTWorksheet templateSheet = oBook.GetSheet(1);
            int numberOfTitle = titles.ToList().Count();
            
            IVTRange emptyRangeCol = templateSheet.GetRange(Subject_Start_Title_Row_Index - 2, Subject_Start_Title_Column_Index, Subject_Start_Title_Row_Index - 1,  Subject_Start_Title_Column_Index);
            //IVTRange emptyRangeCol = templateSheet.GetRange(Subject_Start_Title_Row_Index - 2, Subject_Start_Title_Column_Index, Subject_Start_Title_Row_Index + 4, Subject_Start_Title_Column_Index);

            IVTRange redPupilTopBold = templateSheet.GetRange(Subject_Start_Title_Row_Index + 6, 1, Subject_Start_Title_Row_Index + 6, numberOfTitle + 4);
            IVTRange redPupilNormal = templateSheet.GetRange(Subject_Start_Title_Row_Index + 7, 1, Subject_Start_Title_Row_Index + 7, numberOfTitle + 4);
            IVTRange redPupilBottomBold = templateSheet.GetRange(Subject_Start_Title_Row_Index + 8, 1, Subject_Start_Title_Row_Index + 8, numberOfTitle + 4);
          
            IVTRange topRow = templateSheet.GetRange(Subject_Start_Title_Row_Index + 3, 1, Subject_Start_Title_Row_Index + 3, numberOfTitle + 4);
            IVTRange lastRow = templateSheet.GetRange(Subject_Start_Title_Row_Index + 4, 1, Subject_Start_Title_Row_Index + 4, numberOfTitle + 4);

            //Tao sheet cho moi mon hoc

            for (int i = 0; i < lstClassSubject.Count; i++)
                oBook.CopySheetToLast(templateSheet,"D7");

            Dictionary<string, object> commonDic = new Dictionary<string, object>();
            commonDic["ClassID"] = TranscriptOfClass.ClassID;
            commonDic["Semester"] = TranscriptOfClass.Semester;
            commonDic["AcademicYearID"] = TranscriptOfClass.AcademicYearID;
            commonDic["Year"] = acaYear.Year;
            string strMarkTitle = MarkRecordBusiness.GetMarkTitle(TranscriptOfClass.PeriodDeclarationID);

            //  + Danh sách điểm môn tính điểm: MarkRecordBusiness.SearchBySchool(SchoolID, Dictionary)
            List<VMarkRecord> lstMarkRecordOfClass = VMarkRecordBusiness.SearchBySchool(TranscriptOfClass.SchoolID, commonDic).Where(o => strMarkTitle.Contains(o.Title)).ToList();

            //  + Danh sách điểm môn nhận xét: JudgeRecordBusiness.SearchBySchool(SchoolID, Dictionary)
            List<VJudgeRecord> lstJudgeRecordOfClass = VJudgeRecordBusiness.SearchBySchool(TranscriptOfClass.SchoolID, commonDic).Where(o => strMarkTitle.Contains(o.Title)).ToList();

            //  + Danh sách tổng kết điểm: SummedUpRecordBusiness.SearchBySchool(SchoolID, Dictionary)
            Dictionary<string, object> searchSummedUp = new Dictionary<string, object>();
            searchSummedUp["ClassID"] = TranscriptOfClass.ClassID;
            searchSummedUp["Semester"] = TranscriptOfClass.Semester;
            searchSummedUp["PeriodID"] = TranscriptOfClass.PeriodDeclarationID;
            searchSummedUp["AcademicYearID"] = TranscriptOfClass.AcademicYearID;
            List<VSummedUpRecord> lstSummedUpRecordOfClass = VSummedUpRecordBusiness.SearchBySchool(TranscriptOfClass.SchoolID, searchSummedUp).ToList();

            int colIndex = 0;
            int rowIndex = 0;
            int sheetIndex = 2;

            foreach (ClassSubject classSubject in lstClassSubject)
            {
                IVTWorksheet oSheet = oBook.GetSheet(sheetIndex++);
                oSheet.Name = Utils.StripVNSignAndSpace(classSubject.SubjectCat.SubjectName);

                variableDic["SubjectName"] = classSubject.SubjectCat.SubjectName.ToUpper();
                variableDic["ClassName"] = classSubject.ClassProfile.DisplayName.ToUpper();
                oSheet.FillVariableValue(variableDic);

                #region tao header
                //Tao cac cot cua mon hoc
                colIndex = Subject_Start_Title_Column_Index;
                for (int i = 0; i <= titles.Count(); i++)
                    oSheet.CopyPaste(emptyRangeCol, Subject_Start_Title_Row_Index - 2, colIndex++, true);

                //Merge cac cot thuoc diem mieng - diem 15p - diem 1 tiet
                colIndex = Subject_Start_Title_Column_Index;
                int mergeIndex = Subject_Start_Title_Column_Index;
                foreach (var title in titleGroup)
                {
                    var group = titles.Where(u => u.StartsWith(title.StartCharacter.ToString())).OrderBy(u => u);
                    foreach (var titleName in group)
                    {
                        oSheet.SetCellValue(Subject_Start_Title_Row_Index - 1, colIndex++, titleName);
                    }

                    IVTRange range = oSheet.GetRange(Subject_Start_Title_Row_Index - 2, mergeIndex, Subject_Start_Title_Row_Index - 2, mergeIndex + group.Count() - 1);
                    range.Merge();

                    //Title for group
                    oSheet.SetCellValue(Subject_Start_Title_Row_Index - 2, mergeIndex, GetHeaderTitle(title.StartCharacter));

                    mergeIndex += group.Count();
                }
                // Megre va canh giua ten lop va dot
                oSheet.GetRange(4, 1, 4, mergeIndex).Merge();
                // Megre va canh giua nam hoc
                oSheet.GetRange(5, 1, 5, mergeIndex).Merge();

                //trung binh mon
                IVTRange rangeCol = oSheet.GetRange(Subject_Start_Title_Row_Index - 2, colIndex, Subject_Start_Title_Row_Index - 1, colIndex);
                rangeCol.Merge();
                oSheet.SetCellValue(Subject_Start_Title_Row_Index - 2, colIndex, "TBM");
                #endregion

                #region tao cac dong cua hoc sinh

                //Xoa template cua cac hoc sinh gach do
                oSheet.DeleteRow(Subject_Start_Title_Row_Index + 6);
                oSheet.DeleteRow(Subject_Start_Title_Row_Index + 6);
                oSheet.DeleteRow(Subject_Start_Title_Row_Index + 6);

                //Tao cac dong moi
                //IVTRange emptyRangeRow = oSheet.GetRange(Subject_Start_Title_Row_Index, 1, Subject_Start_Title_Row_Index + 4, Subject_Start_Title_Column_Index + titles.Count());
                //rowIndex = Subject_Start_Title_Row_Index;
                //if (lstPupilOfClass.Count > 5)
                //{
                //    for (int i = 0; i < lstPupilOfClass.Count / 5 + 1; i++)
                //    {
                //        oSheet.CopyPaste(emptyRangeRow, rowIndex, 1, true);
                //        rowIndex += 5;
                //    }
                //}
                #endregion

                #region do du lieu vao cac dong

                List<VMarkRecord> lstMarkRecord = lstMarkRecordOfClass.Where(u => u.SubjectID == classSubject.SubjectID).ToList();
                List<VJudgeRecord> lstJudgeRecord = lstJudgeRecordOfClass.Where(u => u.SubjectID == classSubject.SubjectID).ToList();
                List<VSummedUpRecord> lstSummedUpRecord = lstSummedUpRecordOfClass.Where(u => u.SubjectID == classSubject.SubjectID).ToList();

                rowIndex = Subject_Start_Title_Row_Index;
                int index = 1;
                int countPupil = lstPupilOfClass.Count();
                foreach (var poc in lstPupilOfClass)
                {
                    if ((index == 1 && countPupil == 1) || (index % 5 == 0) || (index == countPupil))
                    {
                        oSheet.CopyPasteSameRowHeigh(lastRow, rowIndex);
                    }
                    else
                    {
                        oSheet.CopyPasteSameRowHeigh(topRow, rowIndex);
                    }
                    bool showData = lstPupilOfClassHK.Any(u => u == poc.PupilID);
                    var pupilMarks = lstMarkRecord.Where(u => u.PupilID == poc.PupilID);
                    var pupilJudges = lstJudgeRecord.Where(u => u.PupilID == poc.PupilID);
                    var pMark = lstSummedUpRecord.Where(u => u.PupilID == poc.PupilID).Select(u => u.SummedUpMark).FirstOrDefault();
                    var pJudge = lstSummedUpRecord.Where(u => u.PupilID == poc.PupilID).Select(u => u.JudgementResult).FirstOrDefault();
                    var pupilSummed = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (!string.IsNullOrEmpty(pJudge) ? pJudge : string.Empty) : (pMark.HasValue ? ReportUtils.ConvertMarkForV(pMark.Value) : string.Empty);

                    oSheet.SetCellValue(rowIndex, 1, index);
                    oSheet.SetCellValue(rowIndex, 2, poc.PupilCode);

                    if (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if (index % 5 == 1)
                            oSheet.CopyPaste(redPupilTopBold, rowIndex, 1, true);
                        else if (index % 5 == 0 || index == countPupil)
                            oSheet.CopyPaste(redPupilBottomBold, rowIndex, 1, true);
                        else oSheet.CopyPaste(redPupilNormal, rowIndex, 1, true);
                    }
                    oSheet.SetCellValue(rowIndex, 3, poc.FullName);

                    colIndex = Subject_Start_Title_Column_Index;
                    if (showData)
                    {
                        foreach (var title in titleGroup)
                        {
                            foreach (var titleName in titles.Where(u => u.StartsWith(title.StartCharacter.ToString())).OrderBy(u => u))
                            {
                                if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    VJudgeRecord judgeRecord = pupilJudges.Where(u => u.Title == titleName).FirstOrDefault();
                                    oSheet.SetCellValue(rowIndex, colIndex, judgeRecord != null ? judgeRecord.Judgement : string.Empty);
                                }
                                else
                                {
                                    VMarkRecord markRecord = pupilMarks.Where(u => u.Title == titleName).FirstOrDefault();
                                    if (markRecord != null)
                                    {
                                        if (titleName.Contains("M"))
                                        {
                                            string mark = ReportUtils.ConvertMarkForM(markRecord.Mark);
                                            oSheet.SetCellValue(rowIndex, colIndex, mark);
                                        }
                                        else if (titleName.Contains("P") || titleName.Contains("V") || titleName.Contains("HK"))
                                        {
                                            string mark = ReportUtils.ConvertMarkForV(markRecord.Mark);
                                            oSheet.SetCellValue(rowIndex, colIndex, mark);
                                        }
                                        
                                    }
                                }
                                colIndex++;
                            }
                        }
                    }
                    oSheet.SetCellValue(rowIndex, colIndex++, showData ? pupilSummed : string.Empty);
                    oSheet.FitToPage = true;
                    rowIndex++;
                    index++;
                }
                #endregion
            }
            templateSheet.Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Lưu lại thông tin bảng điểm tổng hợp học sinh cấp 2,3 theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        public Stream CreateSummariseTranscriptsByPeriod(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            int academicYearID = entity.AcademicYearID;
            int educationLevelID = entity.EducationLevelID;
            int classID = entity.ClassID;
            int schoolID = entity.SchoolID;
            int userAccountID = entity.UserAccountID;

            // Lay danh sach lop hoc
            List<ClassProfile> listClass = new List<ClassProfile>();
            if (classID > 0)
            {
                listClass.Add(ClassProfileBusiness.Find(classID));
            }
            else
            {
                listClass = ClassProfileBusiness.getClassByAccountRole(academicYearID, schoolID, educationLevelID, null,
                                                                        userAccountID, entity.IsSuperVisingDept,entity.IsSubSuperVisingDept).ToList();

            }

            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorksheet sheet = null;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);

            //Lay template phan header thong tin chung
            IVTRange topRange = firstSheet.GetRange("A1", "H8");
            //Lay template cho phan thong tin chung cua hoc sinh
            IVTRange pupilRange = firstSheet.GetRange("A9", "B10"); // firstSheet.GetRange("A9", "B15");
            //Lay template cho phan thong tin cua tung mon hoc
            IVTRange subjectRange = firstSheet.GetRange("C9", "C10"); // firstSheet.GetRange("C9", "C15");
            //Lay template cho phan title cua tung mon hoc
            IVTRange subjectTitleRange = firstSheet.GetRange("C9", "C9");
            //Lay template cho cot diem trung binh
            IVTRange averageRange = firstSheet.GetRange("D9", "D10"); // firstSheet.GetRange("D9", "D15");
            //Lay template cho cot chua thong tin tong ket diem
            IVTRange summarizeRange = firstSheet.GetRange("E9", "G10"); // firstSheet.GetRange("E9", "G15");

            // Cell gach do
            IVTRange redRangeTop = firstSheet.GetRange("B18", "B18");
            IVTRange redRangeMid = firstSheet.GetRange("B19", "B19");
            IVTRange redRangeBot = firstSheet.GetRange("B20", "B20");


            // Thong tin cell chua du lieu dau tien cua file export
            int firstRow = 11;
            int firstCol = 1;

            // Truong
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);

            // Dot
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodDeclarationID);

            // Thong tin ve nam hoc
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            if (school == null || period == null || academicYear == null)
            {
                return null;
            }

            //Date report
            DateTime now = DateTime.Now;
            string dateReport = "ngày " + now.Day + " tháng " + now.Month + " năm " + now.Year;

            // Lay thong tin cho phan header
            Dictionary<string, object> dicTitle = new Dictionary<string, object>()
            { 
                {"DeptName", UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper()}, 
                {"SchoolName", school.SchoolName.ToUpper()},
                {"Province",school.District != null ?  school.District.DistrictName : string.Empty},
                {"ReportDate", dateReport}
            };

            int countPupil = 0;
            int currentRow = 0;
            int currentCol = 0;
            string strFromDate = period.FromDate.HasValue ? period.FromDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            string strEndDate = period.EndDate.HasValue ? period.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty;

            dicTitle["PeriodDeclarationInfo"] = period.Resolution + " (" + strFromDate + " - " + strEndDate + ")";
            dicTitle["Semester"] = entity.Semester;
            dicTitle["AcademicYearTitle"] = academicYear.DisplayTitle;
            //Lay danh sach hoc sinh trong lop
            Dictionary<string, object> dicPupilOfClass = new Dictionary<string, object> { 
                                                                            { "EducationLevelID", entity.EducationLevelID } ,
                                                                            { "AcademicYearID", entity.AcademicYearID },
                                                                            { "ClassID", entity.ClassID },
                                                                            { "CheckWithClass", "CheckWithClass"}  
                                                                        };
            var listPupil = PupilOfClassBusiness.SearchBySchool(schoolID, dicPupilOfClass)
                                                .Select(u => new { u.ClassID, u.PupilID, u.PupilProfile.FullName, u.PupilProfile.Name, u.Status, u.OrderInClass })
                                                .ToList().OrderBy(o => o.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName))
                                                .ToList();

            if ((educationLevelID >= 6 && educationLevelID <= 12) && (academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value == true))
            {
                listPupil = listPupil.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED 
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            }

            //Lay danh sach hoc sinh trong lop ma thuoc hien thi du lieu
            Dictionary<string, object> dicPupilOfClassHK = new Dictionary<string, object>
            {
                 { "EducationLevelID", entity.EducationLevelID } ,
                 { "AcademicYearID", entity.AcademicYearID },
                 { "ClassID", entity.ClassID }
            };
            var listPupilOfHK = PupilOfClassBusiness.SearchBySchool(schoolID, dicPupilOfClassHK).AddCriteriaSemester(academicYear, entity.Semester).Select(u => new { u.ClassID, u.PupilID, u.PupilProfile.FullName, u.PupilProfile.Name, u.Status, u.OrderInClass }).OrderBy(o => o.OrderInClass).ToList();
            //Tìm kiếm môn học theo khối, lớp
            IDictionary<string, object> dicSubject = new Dictionary<string, object> {
                                {"AcademicYearID", entity.AcademicYearID},
                                {"EducationLevelID", entity.EducationLevelID},
                                {"ClassID",entity.ClassID}, 
                                {"Semester", entity.Semester}
            };
            List<ClassSubjectBO> lstClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject).Select(o => new ClassSubjectBO { 
            ClassID = o.ClassID, 
            SubjectID = o.SubjectID, 
            DisplayName = o.SubjectCat.DisplayName,
            OrderInSubject = o.SubjectCat.OrderInSubject,
            IsCommenting = o.IsCommenting
            }).ToList();

            //Khởi tạo tìm kiếm trong PupilRanking
            IDictionary<string, object> dicSearchMark = new Dictionary<string, object> { {"AcademicYearID", entity.AcademicYearID}, 
            {"EducationLevelID", entity.EducationLevelID},
            {"ClassID", entity.ClassID},
            {"PeriodID", entity.PeriodDeclarationID}
            };
            List<PupilRankingBO> lstPR = (from p in VPupilRankingBusiness.SearchBySchool(schoolID, dicSearchMark)
                                             join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                             from j1 in g1.DefaultIfEmpty()
                                             join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                             from j2 in g2.DefaultIfEmpty()
                                             select new PupilRankingBO{
               PupilRankingID = p.PupilRankingID, 
               PupilID = p.PupilID,
               ClassID = p.ClassID, 
               CapacityLevel = j1.CapacityLevel1,
               ConductLevelID = p.ConductLevelID, 
               ConductLevelName = j2.Resolution,
               AverageMark = p.AverageMark,
               Rank = p.Rank
            }).ToList();
            //Lay du lieu trong bang SummedUpRecord
            List<SummedUpRecordBO> lstSUR = VSummedUpRecordBusiness.SearchBySchool(schoolID, dicSearchMark).Select(u => new SummedUpRecordBO { 
                SummedUpRecordID = u.SummedUpRecordID, 
                PupilID = u.PupilID, 
                ClassID = u.ClassID, 
                SubjectID  = u.SubjectID, 
                SummedUpMark = u.SummedUpMark,
                JudgementResult = u.JudgementResult
                
            }).ToList();
            //Danh sach hoc sinh mien giam            
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SemesterID",entity.Semester},                
                {"YearID",academicYear.Year}
            };
            if (entity.Semester == 1)
            {
                dic["HasFirstSemester"] = true;
            }
            if (entity.Semester == 2)
            {
                dic["HasSecondSemester"] = true;
            }
            List<ExemptedSubject> lstPupilExempted = ExemptedSubjectBusiness.Search(dic).ToList();
            ExemptedSubject examptedPupilObj = null;
            foreach (ClassProfile c in listClass)
            {
                int classProfileID = c.ClassProfileID;
                var listPupilClass = listPupil.Where(o => o.ClassID == classProfileID).ToList();
                var listPupilOfClassHK = listPupilOfHK.Where(o => o.ClassID == classProfileID).ToList();
                // Ten lop cua phan header
                string className = c.DisplayName.Replace("lớp", string.Empty).Replace("Lớp", string.Empty).Replace("LỚP", string.Empty);
                dicTitle["ClassName"] = c.DisplayName;
               
                // Danh sach mon hoc
                List<ClassSubjectBO> listClassSubject = lstClassSubject.Where(o => o.ClassID == classProfileID).OrderBy(o => o.OrderInSubject).ToList();



                List<PupilRankingBO> listPupilRanking = lstPR.Where(o => o.ClassID == classProfileID).ToList();

                // Thong tin diem cua hoc sinh
                List<SummedUpRecordBO> listSummedUpRecord = lstSUR.Where(o => o.ClassID == classProfileID).ToList();

                #region Tao tung sheet trong file excel
                // Tao template sheet
                IVTWorksheet templateSheet = oBook.CopySheetToLast(firstSheet, "G8");

                // Phan header thong tin chung
                templateSheet.CopyPasteSameSize(topRange, 1, 1);
                templateSheet.FillVariableValue(dicTitle);

                // Copy va tao template cho sheet
                // Phan thong tin hoc sinh
                templateSheet.CopyPasteSameSize(pupilRange, 9, 1);

                // Phan tieu de mon hoc
                templateSheet.CopyPasteSameSize(subjectTitleRange, 9, 3);

                // Chi tiet tung mon
                int countSubject = listClassSubject.Count;
                for (int i = 0; i < countSubject; i++)
                {
                    templateSheet.CopyPasteSameSize(subjectRange, 9, 3 + i);
                    templateSheet.SetCellValue(10, 3 + i, listClassSubject[i].DisplayName);
                }

                // Trung binh mon hoc
                templateSheet.CopyPasteSameSize(averageRange, 9, countSubject + 3);

                // Thong tin ve xep loai hoc sinh
                templateSheet.CopyPasteSameSize(summarizeRange, 9, countSubject + 4);

                // Thuc hien merge cac cell o phan header thong tin chung
                IVTRange rangeCountry = templateSheet.GetRange(2, 6, 2, countSubject + 6);
                rangeCountry.Merge();
                IVTRange rangeCountryMore = templateSheet.GetRange(3, 6, 3, countSubject + 6);
                rangeCountryMore.Merge();
                IVTRange rangReportDate = templateSheet.GetRange(4, 6, 4, countSubject + 6);
                rangReportDate.Merge();
                //Thoongs tin tieu de bao cao gom dot, lop, hoc ky, nam hoc.
                IVTRange rangAverageTitle = templateSheet.GetRange(6, 1, 6, countSubject + 6);
                rangAverageTitle.Merge();
                IVTRange rangClassInfo = templateSheet.GetRange(7, 1, 7, countSubject + 6);
                rangClassInfo.Merge();
                templateSheet.GetRange(9, 3, 9, countSubject + 3).Merge();

                // Lay thong tin cell chua thong tin cua hoc sinh lam template de copy
                IVTRange contentRange = templateSheet.GetRange(firstRow, 1, firstRow + 5, countSubject + 6);
                IVTRange testS = templateSheet.GetRange(10, countSubject + 3, 10, countSubject + 4);
                //Tạo sheet
                sheet = oBook.CopySheetToLast(templateSheet);
                // Xoa templateSheet tam

                sheet.Name =Utils.StripVNSignAndSpace(c.DisplayName);
                templateSheet.Delete();

                countPupil = listPupilClass.Count;
                int index = 0;
                // Thong tin phan thong tin hoc sinh
                //Cell mẫu
                //IVTRange bottomRow = firstSheet.GetRange("A12", "G12");
                IVTRange bottomRow = firstSheet.GetRange(12, 1, 12, countSubject + 6);
                IVTRange lastRow = firstSheet.GetRange(15, 1, 15, countSubject + 6);
                //xoá dòng dư
                int order = 0;
                for (int i = 11; i < 11 + countPupil; i++)
                {
                    currentRow = 11 + order;
                    if ((order == 0 && countPupil == 1) || ((order + 1) % 5 == 0) || (order + 1 == countPupil))
                    {
                        sheet.CopyPasteSameRowHeigh(lastRow, currentRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }

                    order++;
                }
                for (int i = 0; i < countPupil; i++)
                {
                    currentRow = firstRow + index;
                    index++;

                    // Copy phan thong tin hoc sinh va thong tin ve diem
                    //if (i % 5 == 0)
                    //{
                    //    sheet.CopyPasteSameRowHeigh(contentRange, currentRow);
                    //}
                    var pupil = listPupilClass[i];
                    sheet.SetCellValue(currentRow, firstCol, index);
                    sheet.SetCellValue(currentRow, firstCol + 1, pupil.FullName);
                    // Danh sach diem cua hoc sinh nay
                    List<SummedUpRecordBO> listSummedUpByPupil = listSummedUpRecord.Where(o => o.PupilID == pupil.PupilID).ToList();
                    int count = listPupilOfClassHK.Where(o => o.PupilID == pupil.PupilID).Count();
                    if (listSummedUpByPupil == null || count == 0)
                    {
                        listSummedUpByPupil = new List<SummedUpRecordBO>();
                    }
                    currentCol = firstCol + 2;
                    for (int j = 0; j < countSubject; j++)
                    {
                        ClassSubjectBO subject = listClassSubject[j];
                        if (subject != null)
                        {
                            // Lay thong tin diem cua hoc sinh va mon tuong ung
                            SummedUpRecordBO summedUpRecord = listSummedUpByPupil.Find(o => o.SubjectID == subject.SubjectID);
                            if (summedUpRecord != null)
                            {
                                if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                                {
                                    if (summedUpRecord.SummedUpMark != null)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForV(summedUpRecord.SummedUpMark.Value);
                                        sheet.SetCellValue(currentRow, currentCol, summedUpMark);
                                    }
                                }
                                else if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    sheet.SetCellValue(currentRow, currentCol, summedUpRecord.JudgementResult);
                                }
                            }
                            else
                            {
                                //Neu la hoc sinh miem giam o ki 1 thi fill M1, ki 2: M2; ca nam: M
                                examptedPupilObj = lstPupilExempted.Where(p => p.ClassID == classProfileID
                                    && ((p.FirstSemesterExemptType.HasValue && p.FirstSemesterExemptType.Value > 0)
                                       || (p.SecondSemesterExemptType.HasValue && p.SecondSemesterExemptType.Value > 0))
                                       && p.PupilID == pupil.PupilID && p.SubjectID == subject.SubjectID).FirstOrDefault();

                                if (examptedPupilObj != null &&
                                    (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND))
                                {
                                    if ((examptedPupilObj.FirstSemesterExemptType.HasValue && examptedPupilObj.FirstSemesterExemptType.Value > 0)
                                         && (examptedPupilObj.SecondSemesterExemptType.HasValue && examptedPupilObj.SecondSemesterExemptType.Value > 0))
                                    {
                                        sheet.SetCellValue(currentRow, currentCol, "M");
                                    }
                                    else if (examptedPupilObj.FirstSemesterExemptType.HasValue && examptedPupilObj.FirstSemesterExemptType.Value > 0)
                                    {
                                        sheet.SetCellValue(currentRow, currentCol, "M1");
                                    }
                                    else if (examptedPupilObj.SecondSemesterExemptType.HasValue && examptedPupilObj.SecondSemesterExemptType.Value > 0)
                                    {
                                        sheet.SetCellValue(currentRow, currentCol, "M2");
                                    }
                                }
                                else
                                {
                                    if (examptedPupilObj != null
                                        && examptedPupilObj.FirstSemesterExemptType.HasValue && examptedPupilObj.FirstSemesterExemptType.Value > 0
                                        && examptedPupilObj.SecondSemesterExemptType.HasValue && examptedPupilObj.SecondSemesterExemptType.Value > 0)
                                    {
                                        sheet.SetCellValue(currentRow, currentCol, "M");
                                    }
                                }
                            }
                        }
                        currentCol++;
                    }

                    // Lay thong tin diem trung binh mon va tong ket theo hoc sinh nay
                    PupilRankingBO pupilRanking = listPupilRanking.Find(o => o.PupilID == pupil.PupilID);
                    if (pupilRanking != null && count != 0)
                    {
                        string AverageMark = pupilRanking.AverageMark.HasValue ? ReportUtils.ConvertMarkForV(pupilRanking.AverageMark.Value) : "";
                        sheet.SetCellValue(currentRow, currentCol, AverageMark);
                        currentCol++;
                        if (pupilRanking.CapacityLevel != null)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(pupilRanking.CapacityLevel);
                            sheet.SetCellValue(currentRow, currentCol, Resolution);
                        }
                        currentCol++;
                        if(pupilRanking.ConductLevelName != null)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(pupilRanking.ConductLevelName);
                            sheet.SetCellValue(currentRow, currentCol, Resolution);
                        }
                        currentCol++;
                        sheet.SetCellValue(currentRow, currentCol, pupilRanking.Rank);
                        currentCol++;
                    }
                    // Neu la hoc sinh thoi hoc thi gach do
                    if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pupil.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        IVTRange styleRange = null;
                        if ((i + 1) % 5 == 0 || (i + 1) == countPupil || countPupil == 1)
                        {
                            styleRange = redRangeBot;
                        }
                        else
                        {
                            styleRange = redRangeMid;
                        }

                        for (int j = firstCol; j <= countSubject + 6; j++)
                        {
                            sheet.CopyPaste(styleRange, currentRow, j, true);
                        }
                    }
                }
                sheet.FitToPage = true;
                //if (countPupil % 5 != 0 && countPupil > 5)
                //{
                //    int temp = (countPupil / 5) * 5;
                //    for (int i = countPupil + 11; i <= temp + 15; i++)
                //    {
                //        sheet.DeleteRow(i);
                //    }
                //    if (currentRow > 0)
                //    {
                //        sheet.GetRange(currentRow, 1, currentRow, currentCol + 3).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thick, VTBorderIndex.EdgeBottom);
                //    }
                //}
                //else if (countPupil <= 5)
                //{
                //    for (int i = 15; i >= countPupil + 11; i--)
                //    {
                //        sheet.DeleteRow(i);
                //    }
                //    if (currentRow > 0)
                //    {
                //        sheet.GetRange(currentRow, 1, currentRow, currentCol + 3).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thick, VTBorderIndex.EdgeBottom);
                //    }
                //}
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();
            secondSheet.Delete();
            return oBook.ToStream();
        }

        private string GetHeaderTitle(char startCharacter)
        {
            switch (startCharacter)
            {
                case 'M':
                case 'm': return Diem_Mieng;
                case 'P':
                case 'p': return Diem_15P;
                case 'V':
                case 'v': return Diem_1Tiet;
                default: return string.Empty;
            }
        }
    }
}