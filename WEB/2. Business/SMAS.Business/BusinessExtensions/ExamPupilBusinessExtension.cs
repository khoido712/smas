﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using log4net;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using SMAS.Models.Models;
using System.Data.Entity;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using System.Linq.Expressions;
using SMAS.VTUtils.Excel.Export;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;
using SMAS.VTUtils.Utils;
using System.Text.RegularExpressions;

namespace SMAS.Business.Business
{
    public partial class ExamPupilBusiness
    {
        public IQueryable<ExamPupil> Search(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examRoomID = Utils.GetInt(search, "ExamRoomID");
            int educationLevelID = Utils.GetInt(search, "EducationLevelID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            long pupilID = Utils.GetLong(search, "PupilID");
            string examineeNumber = Utils.GetString(search, "ExamineeNumber");
            int partition = UtilsBusiness.GetPartionId(schoolID);
            List<int> lstPupilID = Utils.GetIntList(search, "lstPupilID");
            List<long> lstExamPupil = Utils.GetLongList(search, "lstExamPupil");
            List<int> lstClassID = Utils.GetIntList(search, "lstClassID");
            List<long> lstExaminationsID = Utils.GetLongList(search, "lstExaminationsID");

            IQueryable<PupilOfClass> queryPupilOfClass = PupilOfClassRepository.All.Where(o => o.Status == GlobalConstants.PUPIL_STATUS_STUDYING && o.AcademicYearID == academicYearID);
            if (lstClassID.Count() > 0)
            {
                queryPupilOfClass = queryPupilOfClass.Where(x => lstClassID.Contains(x.ClassID));
            }

            IQueryable<ExamPupil> query = from ep in ExamPupilRepository.All.Where(p => p.LastDigitSchoolID == partition)
                                          join pp in PupilProfileRepository.All.Where(o => o.IsActive == true) on ep.PupilID equals pp.PupilProfileID
                                          join poc in queryPupilOfClass on pp.PupilProfileID equals poc.PupilID
                                          select ep;

            if (lstExaminationsID.Count() > 0)
            {
                query = query.Where(x => lstExaminationsID.Contains(x.ExaminationsID));
            }

            if (lstExamPupil.Count() > 0)
            {
                query = query.Where(o => lstExamPupil.Contains(o.ExamPupilID));
            }

            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (educationLevelID != 0)
            {
                query = query.Where(o => o.EducationLevelID == educationLevelID);
            }
            if (schoolID != 0)
            {
                query = query.Where(o => o.SchoolID == schoolID);
            }
            if (pupilID != 0)
            {
                query = query.Where(o => o.PupilID == pupilID);
            }
            if (examineeNumber != string.Empty)
            {
                query = query.Where(o => o.ExamineeNumber == examineeNumber);
            }
            if (lstPupilID.Count() > 0)
            {
                query = query.Where(o => lstPupilID.Contains(o.PupilID));
            }
            return query;
        }

        #region Phiếu thu bài thi
        public ProcessedReport InsertTicketTakeExamPaperReport(IDictionary<string, object> dic, Stream data, int appliedLevel, string subjecbName)
        {

            string reportCode = "";
            reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI;
            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup examGroup = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string examGroupCode = "";
            if (examGroup != null)
            {
                examGroupCode = examGroup.ExamGroupName;
            }

            outputNamePattern = outputNamePattern.Replace("THPT", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[NhomThi]", ReportUtils.StripVNSign(examGroupCode));
            outputNamePattern = outputNamePattern.Replace("[MonThi]", ReportUtils.StripVNSign(subjecbName));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        public ProcessedReport InsertTicketTakeExamPaperReportZip(IDictionary<string, object> diczip, Stream data, int appliedLevel, string subjecbName)
        {
            int subjectID = Utils.GetInt(diczip, "SubjectID");
            string reportCode = reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI_ZIP;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(diczip);
            pr.ReportData = subjectID == 0 ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup examGroup = ExamGroupBusiness.Find(diczip["ExamGroupID"]);
            string examGroupCode = "";
            if (examGroup != null)
            {
                examGroupCode = examGroup.ExamGroupName;
            }

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            outputNamePattern = outputNamePattern.Replace("THPT", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[NhomThi]", ReportUtils.StripVNSign(examGroupCode));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(diczip, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetTicketTakeExamPaperReport(IDictionary<string, object> dic)
        {
            int subjectID = Utils.GetInt(dic, "SubjectID");
            string reportCode = "";
            if (subjectID == 0)
            {
                reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI_ZIP;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI;
            }
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        #endregion

        #region Search

        /// <summary>
        /// Tim kiem danh sach thi sinh
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="academicYearID">Nam hoc tim kiem</param>
        /// <param name="patitionID">Partition cua SchoolID</param>
        /// <returns></returns>
        public IQueryable<ExamPupilBO> Search(IDictionary<string, object> dic, int academicYearID, int partitionID)
        {
            long ExaminationsID = Utils.GetLong(dic, "ExaminationsID");
            long ExamGroupID = Utils.GetLong(dic, "ExamGroupID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string PupilName = Utils.GetString(dic, "PupilName");
            long ExamRoomID = Utils.GetLong(dic, "ExamRoomID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<long> lstExamGroupID = Utils.GetLongList(dic, "lstExamGroupID");

            IQueryable<ExamPupilBO> query = (from ep in ExamPupilRepository.All.Where(p => p.LastDigitSchoolID == partitionID && p.ExaminationsID == ExaminationsID)
                                             join eg in ExamGroupRepository.All.Where(p => p.ExaminationsID == ExaminationsID) on ep.ExamGroupID equals eg.ExamGroupID
                                             join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                             join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                             join cp in ClassProfileRepository.All.Where(p => p.AcademicYearID == academicYearID) on poc.ClassID equals cp.ClassProfileID
                                             join er in ExamRoomRepository.All.Where(p => p.ExaminationsID == ExaminationsID) on ep.ExamRoomID equals er.ExamRoomID into des
                                             from x in des.DefaultIfEmpty()
                                             join en in EthnicRepository.All on pp.EthnicID equals en.EthnicID into des1
                                             from y in des1.DefaultIfEmpty()
                                             where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                             && ep.LastDigitSchoolID == partitionID
                                             && poc.AcademicYearID == academicYearID
                                             && cp.IsActive == true
                                             select new ExamPupilBO
                                             {
                                                 PupilCode = pp.PupilCode,
                                                 PupilFullName = pp.FullName,
                                                 BirthDate = pp.BirthDate,
                                                 EthnicID = pp.EthnicID,
                                                 EthnicCode = y != null ? y.EthnicCode : null,
                                                 Genre = pp.Genre,
                                                 ClassName = cp.DisplayName,
                                                 ExaminationsID = ep.ExaminationsID,
                                                 ExamGroupID = ep.ExamGroupID,
                                                 ExamGroupCode = eg.ExamGroupCode,
                                                 ExamGroupName = eg.ExamGroupName,
                                                 ExamPupilID = ep.ExamPupilID,
                                                 ClassID = poc.ClassID,
                                                 EducationLevelID = cp.EducationLevelID,
                                                 SchoolID = ep.SchoolID,
                                                 LastDigitSchoolID = ep.LastDigitSchoolID,
                                                 PupilID = ep.PupilID,
                                                 ExamRoomID = ep.ExamRoomID,
                                                 ExamRoomCode = x != null ? x.ExamRoomCode : null,
                                                 ExamineeNumber = ep.ExamineeNumber,
                                                 OrderInClass = poc.OrderInClass,
                                                 Status = poc.Status,
                                                 UpdateTime = ep.UpdateTime,
                                                 IsVNENClass = cp.IsVnenClass,                                                 
                                             });

            if (ExamGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == ExamGroupID);
            }

            if (ExamRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == ExamRoomID);
            }

            if (SchoolID != 0)
            {
                query = query.Where(o => o.SchoolID == SchoolID);
            }
            if (PupilCode.Trim().Length > 0)
            {
                query = query.Where(o => (o.PupilCode.ToUpper().Contains(PupilCode.ToUpper())));
            }
            if (PupilName.Trim().Length > 0)
            {
                query = query.Where(o => (o.PupilFullName.ToUpper().Contains(PupilName.ToUpper())));
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (ClassID != 0)
            {
                query = query.Where(o => o.ClassID == ClassID);
            }
            if (lstExamGroupID.Count > 0)
            {
                query = query.Where(o => lstExamGroupID.Contains(o.ExamGroupID));
            }

            return query;
        }

        /// <summary>
        /// Tim kiem danh sach thi sinh
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="academicYearID">Nam hoc tim kiem</param>
        /// <param name="patitionID">Partition cua SchoolID</param>
        /// <returns></returns>
        public IQueryable<ExamPupil> GetExamPupilOfExaminations(long? examinationsID, int schoolID, int academicYearID, int patitionID)
        {
            IQueryable<ExamPupil> query = (from ep in ExamPupilRepository.All
                                           join eg in ExamGroupRepository.All.Where(p => p.ExaminationsID == examinationsID) on ep.ExamGroupID equals eg.ExamGroupID
                                           join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                           join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                           join cp in ClassProfileRepository.All.Where(p => p.AcademicYearID == academicYearID) on poc.ClassID equals cp.ClassProfileID
                                           where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                           && ep.LastDigitSchoolID == patitionID
                                           && ep.ExaminationsID == examinationsID
                                           && ep.SchoolID == schoolID
                                           && poc.AcademicYearID == academicYearID
                                           && cp.IsActive == true
                                           select ep);
            return query;
        }

        /// <summary>
        /// Lay danh sach thi sinh thi cac mon thi trong ky thi
        /// </summary>
        /// <param name="examinationsID"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="patitionID"></param>
        /// <returns></returns>
        public IQueryable<ExamPupilBO> GetExamPupilWithSubject(long? examinationsID, int schoolID, int academicYearID, int patitionID)
        {
            IQueryable<ExamPupilBO> query = from ep in ExamPupilRepository.All
                                            join eg in ExamGroupRepository.All.Where(p => p.ExaminationsID == examinationsID) on ep.ExamGroupID equals eg.ExamGroupID
                                            join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                            join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                            join cp in ClassProfileRepository.All.Where(p => p.AcademicYearID == academicYearID) on poc.ClassID equals cp.ClassProfileID
                                            join es in ExamSubjectRepository.All.Where(p => p.ExaminationsID == examinationsID) on ep.ExamGroupID equals es.ExamGroupID
                                            join epa in ExamPupilAbsenceRepository.All.Where(p => p.ExaminationsID == examinationsID) on new { ep.ExaminationsID, ep.ExamGroupID, ep.ExamPupilID, es.SubjectID }
                                                 equals new { epa.ExaminationsID, epa.ExamGroupID, epa.ExamPupilID, epa.SubjectID } into des
                                            from x in des.DefaultIfEmpty()
                                            where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                            && ep.LastDigitSchoolID == patitionID
                                            && ep.ExaminationsID == examinationsID
                                            && ep.SchoolID == schoolID
                                            && poc.AcademicYearID == academicYearID
                                            && cp.IsActive == true
                                            select new ExamPupilBO
                                            {
                                                PupilCode = pp.PupilCode,
                                                PupilFullName = pp.FullName,
                                                BirthDate = pp.BirthDate,
                                                EthnicID = pp.EthnicID,
                                                Genre = pp.Genre,
                                                ClassName = cp.DisplayName,
                                                ExaminationsID = ep.ExaminationsID,
                                                ExamGroupID = ep.ExamGroupID,
                                                ExamGroupCode = eg.ExamGroupCode,
                                                ExamPupilID = ep.ExamPupilID,
                                                SubjectID = es.SubjectID,
                                                ClassID = poc.ClassID,
                                                EducationLevelID = cp.EducationLevelID,
                                                SchoolID = ep.SchoolID,
                                                PupilID = ep.PupilID,
                                                ExamRoomID = ep.ExamRoomID,
                                                ExamineeNumber = ep.ExamineeNumber,
                                                OrderInClass = poc.OrderInClass,
                                                ExamPupilAbsenceID = x != null ? x.ExamPupilAbsenceID : 0,
                                                ClassOrder = cp.OrderNumber != null ? cp.OrderNumber.Value : 0
                                            };
            return query;
        }

        public IQueryable<ExamPupil> GetExamPupilOfExamGroup(long? examinationsID, long? examGroupID, int schoolID, int academicYearID, int partitionID)
        {
            IQueryable<ExamPupil> query = (from ep in ExamPupilRepository.All.Where(p => p.LastDigitSchoolID == partitionID && p.ExaminationsID == examinationsID)
                                           join eg in ExamGroupRepository.All.Where(p => p.ExaminationsID == examinationsID) on ep.ExamGroupID equals eg.ExamGroupID
                                           join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                           join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                           join cp in ClassProfileRepository.All.Where(p => p.AcademicYearID == academicYearID) on poc.ClassID equals cp.ClassProfileID
                                           where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                           && ep.ExamGroupID == examGroupID
                                           && ep.SchoolID == schoolID
                                           && poc.AcademicYearID == academicYearID
                                           && cp.IsActive == true
                                           select ep);
            return query;
        }

        public IQueryable<ExamPupil> GetExamPupilOfRoom(long? examinationsID, long? examGroupID, long? examRoomID, int schoolID, int academicYearID, int partitionID)
        {
            IQueryable<ExamPupil> query = (from ep in ExamPupilRepository.All.Where(p => p.LastDigitSchoolID == partitionID && p.ExaminationsID == examinationsID)
                                           join eg in ExamGroupRepository.All.Where(p => p.ExaminationsID == examinationsID) on ep.ExamGroupID equals eg.ExamGroupID
                                           join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                           join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                           join cp in ClassProfileRepository.All.Where(p => p.AcademicYearID == academicYearID) on poc.ClassID equals cp.ClassProfileID
                                           where pp.IsActive && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                           && ep.LastDigitSchoolID == partitionID
                                           && ep.ExaminationsID == examinationsID
                                           && ep.ExamGroupID == examGroupID
                                           && ep.ExamRoomID == examRoomID
                                           && ep.SchoolID == schoolID
                                           && poc.AcademicYearID == academicYearID
                                           select ep);
            return query;
        }

        #endregion

        /// <summary>
        /// Kiểm tra thi sinh đã có dữ liệu ràng buộc hay chưa
        /// </summary>
        /// <param name="inputExamPupilID">Danh sach ID thi sinh</param>
        /// <returns>Danh sach cac ID thi sinh da co du lieu rang buoc</returns>
        public List<long> ExamPupilInUsed(List<long> inputExamPupilID, long examinationsID, int partition)
        {
            List<long> ret = new List<long>();
            IQueryable<ExamPupil> listExamPupil = this.ExamPupilRepository.All.Where(o => o.LastDigitSchoolID == partition
                                                                                        && o.ExaminationsID == examinationsID
                                                                                        && inputExamPupilID.Contains(o.ExamPupilID));
            //Da danh so bao danh
            //ret.AddRange(listExamPupil.Where(o => o.ExamineeNumber != null).Select(o => o.ExamPupilID).ToList<long>());

            //Da xep phong thi
            ret.AddRange(listExamPupil.Where(o => o.ExamRoomID != null).Select(o => o.ExamPupilID).ToList<long>());

            //Da danh phach
            ret.AddRange(this.ExamDetachableBagBusiness.All.Where(o => inputExamPupilID.Contains(o.ExamPupilID) && o.LastDigitSchoolID == partition && o.ExaminationsID == examinationsID).Select(o => o.ExamPupilID).ToList<long>());
            //Da vi phan quy che thi
            ret.AddRange(this.ExamPupilViolateBusiness.All.Where(o => inputExamPupilID.Contains(o.ExamPupilID) && o.ExaminationsID == examinationsID).Select(o => o.ExamPupilID).ToList<long>());
            //Da vang thi
            ret.AddRange(this.ExamPupilAbsenceBusiness.All.Where(o => inputExamPupilID.Contains(o.ExamPupilID) && o.ExaminationsID == examinationsID).Select(o => o.ExamPupilID).ToList<long>());
            //Da vao diem thi
            ret.AddRange((from ep in listExamPupil
                          join eim in ExamInputMarkRepository.All on new { ep.ExaminationsID, ep.ExamGroupID, ep.PupilID } equals new { eim.ExaminationsID, eim.ExamGroupID, eim.PupilID }
                          where eim.LastDigitSchoolID == partition && eim.ExaminationsID == examinationsID
                          select ep.ExamPupilID).Distinct().ToList());

            return ret.ToList();
        }

        /// <summary>
        /// Xoa theo danh sach ID
        /// </summary>
        /// <param name="inputExamPupilID"></param>
        public void DeleteList(List<long> inputExamPupilID)
        {
            try
            {
                //Kiem tra list rong
                if (inputExamPupilID.Count == 0)
                {
                    throw new BusinessException("ExamPupil_Validate_Delete_NoChoice");
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                //Ky thi chua tich chon vao diem thi va chot diem thi
                Examinations exam = this.ExaminationsBusiness.Find(this.ExamPupilBusiness.Find(inputExamPupilID.FirstOrDefault()).ExaminationsID);
                if (exam != null)
                {
                    if (exam.MarkInput.GetValueOrDefault() == true || exam.MarkClosing.GetValueOrDefault() == true)
                    {
                        throw new BusinessException("ExamPupil_Validate_Delete_NotDelete");
                    }
                }
                else
                {
                    throw new BusinessException("ExamPupil_Validate_Delete_NotDelete");
                }
                //Kiem tra du lieu duoc xoa hay khong
                List<long> idInUsed = ExamPupilInUsed(inputExamPupilID, exam.ExaminationsID, UtilsBusiness.GetPartionId(exam.SchoolID));
                if (idInUsed.Count > 0)
                {
                    throw new BusinessException("ExamPupil_Validate_Delete_NotDelete");
                }
                for (int i = 0; i < inputExamPupilID.Count; i++)
                {
                    this.ExamPupilBusiness.Delete(inputExamPupilID[i]);
                }
                this.ExamPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// Insert list su dung Bulk insert
        /// </summary>
        /// <param name="insertList"></param>
        public void InsertList(List<ExamPupil> insertList)
        {
            try
            {
                if (insertList == null || insertList.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < insertList.Count; i++)
                {
                    ExamPupilBusiness.Insert(insertList[i]);
                }
                ExamPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }

        /// <summary>
        /// Update list su dung Bulk update
        /// </summary>
        /// <param name="insertList"></param>
        public void UpdateList(List<ExamPupil> updateList)
        {
            try
            {
                if (updateList == null || updateList.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < updateList.Count; i++)
                {
                    ExamPupilBusiness.Update(updateList[i]);
                }
                ExamPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "UpdateList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void UpdateExamineeCode(List<ExamPupil> lstExamineeCode)
        {
            List<long> lstExamPupilID = lstExamineeCode.Select(p => p.ExamPupilID).Distinct().ToList();
            List<ExamPupil> lstExamPupilDB = ExamPupilBusiness.All.Where(p => lstExamPupilID.Contains(p.ExamPupilID)).ToList();
            ExamPupil objExamPupilDB = null;
            ExamPupil objExamPupilCode = null;
            for (int i = 0; i < lstExamineeCode.Count; i++)
            {
                objExamPupilCode = lstExamineeCode[i];
                objExamPupilDB = lstExamPupilDB.Where(p => p.ExamPupilID == objExamPupilCode.ExamPupilID).FirstOrDefault();
                objExamPupilDB.ExamineeNumber = objExamPupilCode.ExamineeNumber;
                ExamPupilBusiness.Update(objExamPupilDB);
            }
            ExamPupilBusiness.Save();
        }
        /// <summary>
        /// Ham lay column mapping de thuc hien bulk insert
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ExamineeNumber", "EXAMINEE_NUMBER");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");

            return columnMap;
        }

        /// <summary>
        /// Ham lay danh sach thi sinh cua cac hoc sinh cu the da duoc xem nhom ma co mon thi nam trong danh sach mon thi cu the
        /// </summary>
        /// <param name="listPupilID"></param>
        /// <param name="listSubjectID"></param>
        /// <param name="examinationsID"></param>
        /// <returns></returns>
        public IQueryable<ExamPupil> GetGroupedPupilWithSameSubject(List<int> listPupilID, List<int> listSubjectID, long examinationsID, int partition)
        {
            IQueryable<ExamPupil> query = from ep in ExamPupilRepository.All.Where(o => o.LastDigitSchoolID == partition && o.ExaminationsID == examinationsID)
                                          join eg in ExamGroupRepository.All on ep.ExamGroupID equals eg.ExamGroupID
                                          join es in ExamSubjectRepository.All on new { eg.ExaminationsID, eg.ExamGroupID } equals new { es.ExaminationsID, es.ExamGroupID }
                                          where (
                                          eg.ExaminationsID == examinationsID
                                          && listPupilID.Contains(ep.PupilID)
                                          && listSubjectID.Contains(es.SubjectID))
                                          select ep;

            return query;
        }

        public IQueryable<ExamPupilBO> GetListExamineeNumber(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);

            IQueryable<ExamPupilBO> query = (from ep in ExamPupilRepository.All.AsNoTracking().Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.ExaminationsID == examinationsID)
                                             join pp in PupilProfileRepository.All.AsNoTracking().Where(pp => pp.IsActive == true) on ep.PupilID equals pp.PupilProfileID
                                             join poc in PupilOfClassRepository.All.AsNoTracking().Where(c => c.AcademicYearID == academicYearID && c.Status == GlobalConstants.PUPIL_STATUS_STUDYING) on ep.PupilID equals poc.PupilID
                                             join cp in ClassProfileRepository.All.AsNoTracking().Where(o => o.AcademicYearID == academicYearID && o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                                             select new ExamPupilBO
                                             {
                                                 ExamPupilID = ep.ExamPupilID,
                                                 ExaminationsID = ep.ExaminationsID,
                                                 ExamGroupID = ep.ExamGroupID,
                                                 EducationLevelID = cp.EducationLevelID,
                                                 SchoolID = ep.SchoolID,
                                                 PupilID = ep.PupilID,
                                                 PupilCode = pp.PupilCode,
                                                 ExamineeNumber = ep.ExamineeNumber,
                                                 ExamRoomID = ep.ExamRoomID,
                                                 Name = pp.Name,
                                                 PupilFullName = pp.FullName,
                                                 BirthDate = pp.BirthDate,
                                                 Genre = pp.Genre,
                                                 ClassID = cp.ClassProfileID,
                                                 ClassName = cp.DisplayName,
                                                 LastDigitSchoolID = ep.LastDigitSchoolID,
                                                 EthnicCode = pp.Ethnic.EthnicCode,
                                                 CreateTime = ep.CreateTime,
                                                 UpdateTime = ep.UpdateTime,
                                                 Status = poc.Status
                                             });

            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }

            // Loc theo cap hoc hien tai
            if (appliedLevel > 0)
            {
                if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID6 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID9);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID10 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID12);
                }
            }
            /*List<int> listEdu = EducationLevelBusiness.All.Where(o => (o.Grade == appliedLevel) && o.IsActive).Select(o => o.EducationLevelID).ToList();
            if (appliedLevel != 0)
            {
                query = query.Where(o => listEdu.Contains(o.EducationLevelID));
            }*/
            return query;
        }

        private IQueryable<ExamPupilBO> GetListExamineeNumberAfterUpdateRoom(IDictionary<string, object> search)//Ham lay 
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);

            IQueryable<ExamPupilBO> query = (from ep in ExamPupilRepository.All.AsNoTracking().Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.ExaminationsID == examinationsID)
                                             join er in ExamRoomRepository.All.AsNoTracking() on ep.ExamRoomID equals er.ExamRoomID
                                             join pp in PupilProfileRepository.All.AsNoTracking().Where(pp => pp.IsActive == true) on ep.PupilID equals pp.PupilProfileID
                                             join poc in PupilOfClassRepository.All.AsNoTracking().Where(c => c.AcademicYearID == academicYearID && c.Status == GlobalConstants.PUPIL_STATUS_STUDYING) on ep.PupilID equals poc.PupilID
                                             join cp in ClassProfileRepository.All.AsNoTracking().Where(o => o.AcademicYearID == academicYearID && o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                                             select new ExamPupilBO
                                             {
                                                 ExamPupilID = ep.ExamPupilID,
                                                 ExaminationsID = ep.ExaminationsID,
                                                 ExamGroupID = ep.ExamGroupID,
                                                 EducationLevelID = cp.EducationLevelID,
                                                 SchoolID = ep.SchoolID,
                                                 PupilID = ep.PupilID,
                                                 PupilCode = pp.PupilCode,
                                                 ExamineeNumber = ep.ExamineeNumber,
                                                 ExamRoomID = ep.ExamRoomID,
                                                 ExamRoomCode = er.ExamRoomCode,
                                                 Name = pp.Name,
                                                 PupilFullName = pp.FullName,
                                                 BirthDate = pp.BirthDate,
                                                 Genre = pp.Genre,
                                                 ClassID = cp.ClassProfileID,
                                                 ClassName = cp.DisplayName,
                                                 LastDigitSchoolID = ep.LastDigitSchoolID,
                                                 EthnicCode = pp.Ethnic.EthnicCode,
                                                 CreateTime = ep.CreateTime,
                                                 UpdateTime = ep.UpdateTime
                                             });

            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }

            // Loc theo cap hoc hien tai
            if (appliedLevel > 0)
            {
                if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID6 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID9);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID10 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID12);
                }
            }
            /*List<int> listEdu = EducationLevelBusiness.All.Where(o => (o.Grade == appliedLevel) && o.IsActive).Select(o => o.EducationLevelID).ToList();
            if (appliedLevel != 0)
            {
                query = query.Where(o => listEdu.Contains(o.EducationLevelID));
            }*/
            return query;
        }

        public List<ExamPupilBO> GetListExamPupilOrderBy(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);

            IQueryable<ExamPupilBO> query = (from ep in ExamPupilRepository.All.AsNoTracking().Where(o => o.LastDigitSchoolID == lastDigitSchoolID && o.ExaminationsID == examinationsID)
                                             join pp in PupilProfileRepository.All.AsNoTracking().Where(pp => pp.IsActive == true) on ep.PupilID equals pp.PupilProfileID
                                             join poc in PupilOfClassRepository.All.AsNoTracking().Where(c => c.AcademicYearID == academicYearID && c.Status == GlobalConstants.PUPIL_STATUS_STUDYING) on ep.PupilID equals poc.PupilID
                                             join cp in ClassProfileRepository.All.AsNoTracking().Where(o => o.AcademicYearID == academicYearID && o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                                             select new ExamPupilBO
                                             {
                                                 ExamPupilID = ep.ExamPupilID,
                                                 ExaminationsID = ep.ExaminationsID,
                                                 ExamGroupID = ep.ExamGroupID,
                                                 EducationLevelID = cp.EducationLevelID,
                                                 SchoolID = ep.SchoolID,
                                                 PupilID = ep.PupilID,
                                                 PupilCode = pp.PupilCode,
                                                 ExamineeNumber = ep.ExamineeNumber,
                                                 ExamRoomID = ep.ExamRoomID,
                                                 Name = pp.Name,
                                                 PupilFullName = pp.FullName,
                                                 BirthDate = pp.BirthDate,
                                                 Genre = pp.Genre,
                                                 ClassID = cp.ClassProfileID,
                                                 ClassName = cp.DisplayName,
                                                 LastDigitSchoolID = ep.LastDigitSchoolID,
                                                 EthnicCode = pp.Ethnic.EthnicCode,
                                                 CreateTime = ep.CreateTime,
                                                 UpdateTime = ep.UpdateTime
                                             });

            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }

            // Loc theo cap hoc hien tai
            if (appliedLevel > 0)
            {
                if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID6 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID9);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    query = query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID10 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID12);
                }
            }
            return query.ToList();
        }

        private List<ExamPupil> DeclareExamineeCodeName(List<ExamPupil> listSBD, string[] Criteria, string ExamGroupID, string textCriteria, string countCodeName)
        {
            for (int i = 0; i < listSBD.Count; i++)
            {
                string STT = string.Empty;
                string SBD = string.Empty;
                if (Criteria != null && Criteria.Contains("1") && !string.IsNullOrEmpty(ExamGroupID))
                {
                    SBD = SBD + ExamGroupID;
                }
                if (Criteria != null && Criteria.Contains("2") && textCriteria != null && textCriteria != string.Empty)
                {
                    SBD = SBD + textCriteria;
                }
                if (Criteria != null && Criteria.Contains("3") && countCodeName != null && countCodeName != string.Empty)
                {
                    int count = int.Parse(countCodeName) - (i + 1).ToString().Length;
                    for (int j = 0; j < count; j++)
                    {
                        STT = STT + "0";
                    }
                }
                else
                {
                    int count = listSBD.Count.ToString().Length - (i + 1).ToString().Length;
                    for (int j = 0; j < count; j++)
                    {
                        STT = STT + "0";
                    }
                }
                STT = STT + (i + 1).ToString();

                listSBD[i].ExamineeNumber = SBD + STT;
            }

            return listSBD;
        }

        public void UpdateExamineeCodeName(IDictionary<string, object> search, string[] Criteria,
            int checkAll, string textCriteriaII, string textCriteriaIII)
        {
            long ExaminationsID = Utils.GetLong(search, "ExaminationsID");
            long ExamGroupID = Utils.GetLong(search, "ExamGroupID");

            if (checkAll == 0)
            {
                ExamGroup group = ExamGroupBusiness.Find(ExamGroupID);

                List<ExamPupilBO> listExamPupil = ExamPupilBusiness.GetListExamineeNumber(search).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                List<ExamPupil> listSBD = listExamPupil.Select(o => new ExamPupil
                {
                    ExamPupilID = o.ExamPupilID,
                    ExaminationsID = o.ExaminationsID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    EducationLevelID = o.EducationLevelID,
                    SchoolID = o.SchoolID,
                    LastDigitSchoolID = o.LastDigitSchoolID,
                    PupilID = o.PupilID,
                    ExamineeNumber = o.ExamineeNumber,
                    CreateTime = o.CreateTime,
                    UpdateTime = DateTime.Now

                }).Distinct().ToList();

                listSBD = DeclareExamineeCodeName(listSBD, Criteria, group.ExamGroupCode, textCriteriaII, textCriteriaIII);
                this.UpdateList(listSBD);
            }
            else
            {
                List<ExamPupil> listAll = new List<ExamPupil>();
                IDictionary<string, object> searchGroup = new Dictionary<string, object>();
                searchGroup.Add("ExaminationsID", ExaminationsID);
                List<ExamGroup> listExamGroup = ExamGroupBusiness.Search(searchGroup).ToList();

                List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();
                List<ExamPupil> listSBD = new List<ExamPupil>();

                if (checkAll == 1)
                {
                    for (int i = 0; i < listExamGroup.Count; i++)
                    {
                        search["ExamGroupID"] = listExamGroup[i].ExamGroupID;
                        listExamPupil = ExamPupilBusiness.GetListExamineeNumber(search).ToList().OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                        listSBD = listExamPupil.Select(o => new ExamPupil
                        {
                            ExamPupilID = o.ExamPupilID,
                            ExaminationsID = o.ExaminationsID,
                            ExamGroupID = o.ExamGroupID,
                            ExamRoomID = o.ExamRoomID,
                            EducationLevelID = o.EducationLevelID,
                            SchoolID = o.SchoolID,
                            LastDigitSchoolID = o.LastDigitSchoolID,
                            PupilID = o.PupilID,
                            ExamineeNumber = o.ExamineeNumber,
                            CreateTime = o.CreateTime,
                            UpdateTime = DateTime.Now
                        }).Distinct().ToList();

                        listSBD = DeclareExamineeCodeName(listSBD, Criteria, listExamGroup[i].ExamGroupCode, textCriteriaII, textCriteriaIII);

                        listAll.AddRange(listSBD);
                    }

                    // Update toàn bộ list
                    this.UpdateList(listAll);
                }
                else
                {
                    List<ExamPupil> listTotalPupil = new List<ExamPupil>();
                    List<ExamPupilBO> queryExamPupil = null;
                    List<ExamPupilBO> listExamPupilTotal = new List<ExamPupilBO>();
                    ExamPupil objExamPupil = null;

                    for (int i = 0; i < listExamGroup.Count(); i++)
                    {
                        search["ExamGroupID"] = listExamGroup[i].ExamGroupID;
                        queryExamPupil = ExamPupilBusiness.GetListExamineeNumber(search).ToList().OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                        listExamPupilTotal.AddRange(queryExamPupil);
                    }

                    listExamPupilTotal = listExamPupilTotal.OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();

                    for (int i = 0; i < listExamPupilTotal.Count(); i++)
                    {
                        var check = listTotalPupil.Where(x => x.PupilID == listExamPupilTotal[i].PupilID).FirstOrDefault();
                        if (check == null)
                        {
                            objExamPupil = new ExamPupil();
                            objExamPupil.PupilID = listExamPupilTotal[i].PupilID;
                            listTotalPupil.Add(objExamPupil);
                        }
                    }

                    listTotalPupil = DeclareExamineeCodeName(listTotalPupil, Criteria, "", textCriteriaII, textCriteriaIII);

                    for (int i = 0; i < listExamGroup.Count; i++)
                    {
                        search["ExamGroupID"] = listExamGroup[i].ExamGroupID;
                        listExamPupil = ExamPupilBusiness.GetListExamineeNumber(search).ToList().OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                        listSBD = listExamPupil.Select(o => new ExamPupil
                        {
                            ExamPupilID = o.ExamPupilID,
                            ExaminationsID = o.ExaminationsID,
                            ExamGroupID = o.ExamGroupID,
                            ExamRoomID = o.ExamRoomID,
                            EducationLevelID = o.EducationLevelID,
                            SchoolID = o.SchoolID,
                            LastDigitSchoolID = o.LastDigitSchoolID,
                            PupilID = o.PupilID,
                            ExamineeNumber = o.ExamineeNumber,
                            CreateTime = o.CreateTime,
                            UpdateTime = DateTime.Now
                        }).Distinct().ToList();

                        for (int j = 0; j < listSBD.Count; j++)
                        {
                            var check = listTotalPupil.Where(x => x.PupilID == listSBD[j].PupilID).FirstOrDefault();
                            if (check != null)
                            {
                                listSBD[j].ExamineeNumber = check.ExamineeNumber;
                            }
                        }

                        if (listSBD.Count > 0)
                        {
                            listAll.AddRange(listSBD);
                        }
                    }

                    // Update toàn bộ list
                    this.UpdateList(listAll);
                }
            }
        }

        public void UpdateExamineeCodeNameCustom(IDictionary<string, object> search, List<int> lstCheckCriteria, List<int> lstOrder,
            string textCriteriaIII, string textCriteriaIV, int checkAll, int usingEXCode)
        {
            long ExaminationsID = Utils.GetLong(search, "ExaminationsID");
            long ExamGroupID = Utils.GetLong(search, "ExamGroupID");
            int SchoolID = Utils.GetInt(search, "SchoolID");
            int AcademicYearID = Utils.GetInt(search, "AcademicYearID");
            int AppledLevelID = Utils.GetInt(search, "AppledLevelID");
            int STT = 0;

            List<CriteriaOrder> lstCriteriaOrder = new List<CriteriaOrder>();
            for (int c = 0; c < lstCheckCriteria.Count(); c++)
            {
                lstCriteriaOrder.Add(new CriteriaOrder() { CheckCriteria = lstCheckCriteria[c], OrderCritetia = lstOrder[c] });
            }

            lstCriteriaOrder = lstCriteriaOrder.OrderBy(x => x.OrderCritetia).ToList();

            IQueryable<ClassProfile> queryClass = ClassProfileBusiness.All.Where(x => x.AcademicYearID == AcademicYearID && x.IsActive == true
                                                                            && x.SchoolID == SchoolID && x.EducationLevel.Grade == AppledLevelID);
            IQueryable<PupilOfClass> queryPOC = PupilOfClassBusiness.All.Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID);

            List<PupilOfClassBO> lstPupilOfClass = (from poc in queryPOC
                                                    join cp in queryClass on poc.ClassID equals cp.ClassProfileID
                                                    select new PupilOfClassBO()
                                                    {
                                                        EducationLevelID = cp.EducationLevelID,
                                                        PupilID = poc.PupilID
                                                    }).ToList();

            ExamGroup group = ExamGroupBusiness.Find(ExamGroupID);

            if (checkAll == 0)
            {
                List<ExamPupilBO> listExamPupil = ExamPupilBusiness.GetListExamineeNumber(search).ToList()
                                                .OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                List<ExamPupil> listSBD = listExamPupil.Select(o => new ExamPupil
                {
                    ExamPupilID = o.ExamPupilID,
                    ExaminationsID = o.ExaminationsID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    EducationLevelID = o.EducationLevelID,
                    SchoolID = o.SchoolID,
                    LastDigitSchoolID = o.LastDigitSchoolID,
                    PupilID = o.PupilID,
                    ExamineeNumber = o.ExamineeNumber,
                    CreateTime = o.CreateTime,
                    UpdateTime = DateTime.Now
                }).Distinct().ToList();

                lstPupilOfClass = lstPupilOfClass.Where(x => listSBD.Select(o => o.PupilID).ToList().Contains(x.PupilID)).ToList();

                listSBD = DeclareExamineeCodeNameCustom(listSBD, lstPupilOfClass, lstCriteriaOrder,
                    textCriteriaIII, textCriteriaIV, checkAll, usingEXCode, group.ExamGroupCode, listSBD.Count(), ref STT);
                this.UpdateList(listSBD);
            }
            else
            {
                List<ExamPupil> listAll = new List<ExamPupil>();
                IDictionary<string, object> searchGroup = new Dictionary<string, object>();
                searchGroup.Add("ExaminationsID", ExaminationsID);
                List<ExamGroup> listExamGroup = ExamGroupBusiness.Search(searchGroup).ToList();

                List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();
                List<ExamPupilBO> listExamPupilBO = new List<ExamPupilBO>();
                List<ExamPupil> listSBD = new List<ExamPupil>();
                List<PupilOfClassBO> lstPupilOfClassByEG = new List<PupilOfClassBO>();

                // Danh sach hoc sinh trong tat ca nhom thi
                search["ExamGroupID"] = 0;
                listExamPupilBO = ExamPupilBusiness.GetListExamineeNumber(search).ToList().OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();

                // Không dùng chung SBD
                if (checkAll == 1)
                {
                    #region
                    for (int i = 0; i < listExamGroup.Count; i++)
                    {
                        search["ExamGroupID"] = listExamGroup[i].ExamGroupID;
                        listExamPupil = listExamPupilBO.Where(x => x.ExamGroupID == listExamGroup[i].ExamGroupID).ToList();
                        listSBD = listExamPupil.Select(o => new ExamPupil
                        {
                            ExamPupilID = o.ExamPupilID,
                            ExaminationsID = o.ExaminationsID,
                            ExamGroupID = o.ExamGroupID,
                            ExamRoomID = o.ExamRoomID,
                            EducationLevelID = o.EducationLevelID,
                            SchoolID = o.SchoolID,
                            LastDigitSchoolID = o.LastDigitSchoolID,
                            PupilID = o.PupilID,
                            ExamineeNumber = o.ExamineeNumber,
                            CreateTime = o.CreateTime,
                            UpdateTime = DateTime.Now
                        }).Distinct().ToList();

                        lstPupilOfClassByEG = lstPupilOfClass.Where(x => listSBD.Select(o => o.PupilID).ToList().Contains(x.PupilID)).ToList();
                        listSBD = DeclareExamineeCodeNameCustom(listSBD, lstPupilOfClassByEG, lstCriteriaOrder,
                            textCriteriaIII, textCriteriaIV, checkAll, usingEXCode, listExamGroup[i].ExamGroupCode, 0, ref STT);
                        listAll.AddRange(listSBD);
                    }
                    // Update toàn bộ list
                    this.UpdateList(listAll);
                    #endregion

                } // Dùng chung SBD
                else
                {
                    List<PupilOfClassBO> lstPupilOfClassByEdu = new List<PupilOfClassBO>();
                    List<ExamPupil> listTotalPupil = new List<ExamPupil>();
                    List<ExamPupilBO> queryExamPupil = null;
                    List<ExamPupilBO> listExamPupilTotal = new List<ExamPupilBO>();
                    List<ExamPupilBO> listExamPupilByEdu = new List<ExamPupilBO>();
                    List<int> lstPupilIDTotal = new List<int>();
                    ExamPupil objExamPupil = null;

                    List<int> lstPupilID = new List<int>();
                    List<int> lstEducation = lstPupilOfClass.Select(x => x.EducationLevelID).Distinct().OrderBy(x => x).ToList();

                    int valueMax = 0;
                    for (int i = 0; i < listExamGroup.Count(); i++)
                    {
                        queryExamPupil = listExamPupilBO.Where(x => x.ExamGroupID == listExamGroup[i].ExamGroupID)
                                                        .OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                        listExamPupilTotal.AddRange(queryExamPupil);
                        if (queryExamPupil.Count() > valueMax)
                        {
                            valueMax = queryExamPupil.Count();
                        }
                    }

                    listExamPupilTotal = listExamPupilTotal.OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                    lstPupilIDTotal = listExamPupilTotal.Select(x => x.PupilID).ToList();
                    lstPupilOfClass = lstPupilOfClass.Where(x => lstPupilIDTotal.Contains(x.PupilID)).ToList();

                    if (usingEXCode == 1) // Tat ca cac khoi
                    {
                        #region
                        for (int i = 0; i < listExamPupilTotal.Count(); i++)
                        {
                            var check = listTotalPupil.Where(x => x.PupilID == listExamPupilTotal[i].PupilID).FirstOrDefault();
                            if (check == null)
                            {
                                objExamPupil = new ExamPupil();
                                objExamPupil.PupilID = listExamPupilTotal[i].PupilID;
                                listTotalPupil.Add(objExamPupil);
                            }
                        }

                        listTotalPupil = DeclareExamineeCodeNameCustom(listTotalPupil, lstPupilOfClass, lstCriteriaOrder,
                            textCriteriaIII, textCriteriaIV, checkAll, usingEXCode, "", valueMax, ref STT);

                        for (int i = 0; i < listExamGroup.Count; i++)
                        {
                            listExamPupil = listExamPupilBO.Where(x => x.ExamGroupID == listExamGroup[i].ExamGroupID)
                                                            .OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                            listSBD = listExamPupil.Select(o => new ExamPupil
                            {
                                ExamPupilID = o.ExamPupilID,
                                ExaminationsID = o.ExaminationsID,
                                ExamGroupID = o.ExamGroupID,
                                ExamRoomID = o.ExamRoomID,
                                EducationLevelID = o.EducationLevelID,
                                SchoolID = o.SchoolID,
                                LastDigitSchoolID = o.LastDigitSchoolID,
                                PupilID = o.PupilID,
                                ExamineeNumber = o.ExamineeNumber,
                                CreateTime = o.CreateTime,
                                UpdateTime = DateTime.Now
                            }).Distinct().ToList();

                            for (int j = 0; j < listSBD.Count; j++)
                            {
                                var check = listTotalPupil.Where(x => x.PupilID == listSBD[j].PupilID).FirstOrDefault();
                                if (check != null)
                                {
                                    listSBD[j].ExamineeNumber = check.ExamineeNumber;
                                }
                            }

                            if (listSBD.Count > 0)
                            {
                                listAll.AddRange(listSBD);
                            }
                        }
                        #endregion
                    }
                    else if (usingEXCode == 2 || usingEXCode == 3) // Theo khoi, lien tiep - Theo khoi, lap lai
                    {
                        #region
                        for (int e = 0; e < lstEducation.Count(); e++)
                        {
                            lstPupilOfClassByEdu = lstPupilOfClass.Where(x => x.EducationLevelID == lstEducation[e]).ToList();
                            lstPupilID = lstPupilOfClass.Where(x => x.EducationLevelID == lstEducation[e]).Select(x => x.PupilID).ToList();
                            listExamPupilByEdu = listExamPupilTotal.Where(x => lstPupilID.Contains(x.PupilID)).ToList();

                            // Chuyển ExamPupilBO -> ExamPupil
                            for (int i = 0; i < listExamPupilByEdu.Count(); i++)
                            {
                                var check = listTotalPupil.Where(x => x.PupilID == listExamPupilByEdu[i].PupilID).FirstOrDefault();
                                if (check == null)
                                {
                                    objExamPupil = new ExamPupil();
                                    objExamPupil.PupilID = listExamPupilByEdu[i].PupilID;
                                    listTotalPupil.Add(objExamPupil);
                                }
                            }

                            listTotalPupil = DeclareExamineeCodeNameCustom(listTotalPupil, lstPupilOfClassByEdu, lstCriteriaOrder,
                                textCriteriaIII, textCriteriaIV, checkAll, usingEXCode, "", valueMax, ref STT);

                            for (int i = 0; i < listExamGroup.Count; i++)
                            {
                                listExamPupil = listExamPupilBO.Where(x => x.ExamGroupID == listExamGroup[i].ExamGroupID)
                                                                .Where(x => lstPupilID.Contains(x.PupilID))
                                                                .OrderBy(o => Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                                listSBD = listExamPupil.Select(o => new ExamPupil
                                {
                                    ExamPupilID = o.ExamPupilID,
                                    ExaminationsID = o.ExaminationsID,
                                    ExamGroupID = o.ExamGroupID,
                                    ExamRoomID = o.ExamRoomID,
                                    EducationLevelID = o.EducationLevelID,
                                    SchoolID = o.SchoolID,
                                    LastDigitSchoolID = o.LastDigitSchoolID,
                                    PupilID = o.PupilID,
                                    ExamineeNumber = o.ExamineeNumber,
                                    CreateTime = o.CreateTime,
                                    UpdateTime = DateTime.Now
                                }).Distinct().ToList();

                                for (int j = 0; j < listSBD.Count; j++)
                                {
                                    var check = listTotalPupil.Where(x => x.PupilID == listSBD[j].PupilID).FirstOrDefault();
                                    if (check != null)
                                    {
                                        listSBD[j].ExamineeNumber = check.ExamineeNumber;
                                    }
                                }

                                if (listSBD.Count > 0)
                                {
                                    listAll.AddRange(listSBD);
                                }
                            }

                        }
                        #endregion
                    }

                    // Update toàn bộ list
                    this.UpdateList(listAll);
                }
            }
        }

        private List<ExamPupil> DeclareExamineeCodeNameCustom(List<ExamPupil> listSBD, List<PupilOfClassBO> lstPupilOfClass, List<CriteriaOrder> lstCriteriaOrder,
            string textCriteriaIII, string textCriteriaIV, int checkAll, int usingEXCode, string ExamGroupCode, int valueMax, ref int STT)
        {
            int totalCheck = lstCriteriaOrder.Count();
            PupilOfClassBO objPOC = null;

            // Danh sách học sinh trong education hiện tại đang xét
            List<ExamPupil> lstExamPupilPOC = listSBD.Where(x => lstPupilOfClass.Select(o => o.PupilID).Contains(x.PupilID)).ToList();
            // Danh sách học sinh không phải trong education hiện tại đang xét
            List<ExamPupil> lstExamPupilNonePOC = listSBD.Where(x => !lstPupilOfClass.Select(o => o.PupilID).Contains(x.PupilID)).ToList();

            bool isShortNumber = false;
            for (int i = 0; i < lstExamPupilPOC.Count; i++)
            {
                string SBD = string.Empty;
                if (totalCheck > 0 && totalCheck <= 4)
                {
                    for (int k = 0; k < lstCriteriaOrder.Count(); k++)
                    {
                        if (lstCriteriaOrder[k].CheckCriteria == 1) // Them ma nhom thi
                        {
                            SBD = SBD + ExamGroupCode;
                        }
                        else if (lstCriteriaOrder[k].CheckCriteria == 2) // Them khoi hoc
                        {
                            objPOC = lstPupilOfClass.Where(x => x.PupilID == lstExamPupilPOC[i].PupilID).FirstOrDefault();
                            SBD = SBD + (objPOC != null ? objPOC.EducationLevelID.ToString() : "");
                        }
                        else if (lstCriteriaOrder[k].CheckCriteria == 3 && !string.IsNullOrEmpty(textCriteriaIII)) // Them tien to
                        {
                            SBD = SBD + textCriteriaIII;
                        }
                        else if (lstCriteriaOrder[k].CheckCriteria == 4) // chieu dai danh so
                        {
                            if (isShortNumber == false)
                                isShortNumber = true;

                            int count = 0;
                            if (!string.IsNullOrEmpty(textCriteriaIV))
                            {
                                count = int.Parse(textCriteriaIV) - (i + 1 + ((checkAll == 2 && usingEXCode == 2) ? STT : 0)).ToString().Length;
                            }
                            else
                            {
                                count = valueMax.ToString().Length - (i + 1 + ((checkAll == 2 && usingEXCode == 2) ? STT : 0)).ToString().Length;
                            }

                            for (int j = 0; j < count; j++)
                            {
                                SBD = SBD + "0";
                            }

                            SBD = SBD + (i + 1 + ((checkAll == 2 && usingEXCode == 2) ? STT : 0)).ToString();
                        }
                    }
                }

                if (!isShortNumber)
                {
                    int count = 0;
                    if (checkAll == 2)
                    {
                        if (usingEXCode == 2)
                        {
                            count = valueMax.ToString().Length - (i + 1 + STT).ToString().Length;
                        }
                        else
                        {
                            count = lstExamPupilPOC.Count.ToString().Length - (i + 1).ToString().Length;
                        }
                    }
                    else
                    {
                        count = lstExamPupilPOC.Count.ToString().Length - (i + 1).ToString().Length;
                    }
                    for (int j = 0; j < count; j++)
                    {
                        SBD = SBD + "0";
                    }

                    SBD = SBD + (i + 1 + ((checkAll == 2 && usingEXCode == 2) ? STT : 0)).ToString();
                }

                lstExamPupilPOC[i].ExamineeNumber = SBD;
            }

            STT = STT + lstExamPupilPOC.Count();
            lstExamPupilPOC.AddRange(lstExamPupilNonePOC);
            listSBD = lstExamPupilPOC;

            return listSBD;
        }

        public void UpdateExamineeCodeByRoom(IDictionary<string, object> search, string[] Criteria,
            int checkAll, string textCriteriaII, string textCriteriaIII)
        {
            long ExaminationsID = Utils.GetLong(search, "ExaminationsID");
            long ExamGroupID = Utils.GetLong(search, "ExamGroupID");
            int AcademicYearID = Utils.GetInt(search, "AcademicYearID");
            int SchoolID = Utils.GetInt(search, "SchoolID");
            List<ExamPupilBO> listExamPupilBO = this.GetListExamineeNumberAfterUpdateRoom(search).ToList();

            if (checkAll == 0)
            {
                ExamGroup group = ExamGroupBusiness.Find(ExamGroupID);
                listExamPupilBO = listExamPupilBO.OrderBy(p => p.ExamRoomCode).ThenBy(p => p.EducationLevelID).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilFullName.getOrderingName(p.EthnicCode))).ToList();
                //.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName.getOrderingName(o.EthnicCode))).ToList();
                List<ExamPupil> listSBD = listExamPupilBO.Select(o => new ExamPupil
                {
                    ExamPupilID = o.ExamPupilID,
                    ExaminationsID = o.ExaminationsID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    EducationLevelID = o.EducationLevelID,
                    SchoolID = o.SchoolID,
                    LastDigitSchoolID = o.LastDigitSchoolID,
                    PupilID = o.PupilID,
                    ExamineeNumber = o.ExamineeNumber,
                    CreateTime = o.CreateTime,
                    UpdateTime = DateTime.Now

                }).Distinct().ToList();

                listSBD = DeclareExamineeCodeName(listSBD, Criteria, group.ExamGroupCode, textCriteriaII, textCriteriaIII);
                this.UpdateExamineeCode(listSBD);
            }
            if (checkAll == 1)
            {
                List<ExamPupil> listAll = new List<ExamPupil>();
                IDictionary<string, object> searchNews = new Dictionary<string, object>();
                searchNews.Add("ExaminationsID", ExaminationsID);
                List<ExamGroup> listExamGroup = ExamGroupBusiness.Search(searchNews).ToList();
                ExamGroup objExamGroup = null;
                List<ExamPupil> listSBD = null;
                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    objExamGroup = listExamGroup[i];
                    listExamPupilBO = listExamPupilBO.Where(p => p.ExamGroupID == objExamGroup.ExamGroupID).ToList();
                    listExamPupilBO = listExamPupilBO.OrderBy(p => p.ExamRoomCode).ThenBy(p => p.EducationLevelID).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilFullName.getOrderingName(p.EthnicCode))).ToList();
                    listSBD = listExamPupilBO.Select(o => new ExamPupil
                    {
                        ExamPupilID = o.ExamPupilID,
                        ExaminationsID = o.ExaminationsID,
                        ExamGroupID = o.ExamGroupID,
                        ExamRoomID = o.ExamRoomID,
                        EducationLevelID = o.EducationLevelID,
                        SchoolID = o.SchoolID,
                        LastDigitSchoolID = o.LastDigitSchoolID,
                        PupilID = o.PupilID,
                        ExamineeNumber = o.ExamineeNumber,
                        CreateTime = o.CreateTime,
                        UpdateTime = DateTime.Now

                    }).Distinct().ToList();

                    listSBD = DeclareExamineeCodeName(listSBD, Criteria, listExamGroup[i].ExamGroupCode, textCriteriaII, textCriteriaIII);
                    listAll.AddRange(listSBD);
                }

                // Update toàn bộ list
                this.UpdateExamineeCode(listAll);
            }
        }

        public void DeleteCheckCodeName(IDictionary<string, object> search, List<long> listExamPupil)
        {
            if (listExamPupil.Count == 0)
            {
                return;
            }

            // Kiem tra cac dieu kien
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            IQueryable<ExamPupil> list = this.Search(search);

            // Thuc hien xoa SBD
            ExamPupil delete = null;
            List<ExamPupil> listDelete = new List<ExamPupil>();
            for (int i = 0; i < listExamPupil.Count; i++)
            {
                long examPupilID = listExamPupil[i];
                delete = list.Where(o => o.ExamPupilID == examPupilID).FirstOrDefault();
                if (delete != null)
                {
                    delete.ExamineeNumber = string.Empty;
                    delete.UpdateTime = DateTime.Now;
                    listDelete.Add(delete);
                }
            }

            this.UpdateList(listDelete);
            context.Configuration.AutoDetectChangesEnabled = true;
            context.Configuration.ValidateOnSaveEnabled = true;
        }

        #region Bao cao danh sach thi sinh theo phong thi
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamPupilByRoomReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamPupilByRoomReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                 .Where(o => o.ExamGroupID == examGroupID)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            if (examRoomID != 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }
            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["ExaminationsID"] = examinationsID;
                SearchInfo["ExamGroupID"] = examGroupID;

                listExamPupil = ExamPupilBusiness.Search(SearchInfo, academicYearID, partition).ToList();

            }
            //Tao tung sheet cho moi phong thi
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom examRoom = listExamRoom[i];

                //Fill du lieu
                //Danh sach thi sinh cua phong thi
                List<ExamPupilBO> listResult = listExamPupil.Where(o => o.ExamRoomID == examRoom.ExamRoomID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 10;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 7;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "G" + (lastRow).ToString());


                //Fill tieu de
                curSheet.Name = examRoom.ExamRoomCode;
                curSheet.SetCellValue("A7", "Phòng thi: " + examRoom.ExamRoomCode);

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamPupilBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //Ma hoc sinh
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //Ngay sinh
                    curSheet.SetCellValue(curRow, curColumn, obj.BirthDate);
                    curColumn++;
                    //Gioi tinh
                    curSheet.SetCellValue(curRow, curColumn, obj.DisplayGenre);
                    curColumn++;
                    //Lop
                    curSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                if (listResult.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                curSheet.FitAllColumnsOnOnePage = true;
            }

            //Xoa sheet dau tien
            firstSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamPupilByRoomReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Bao cao danh sach thi sinh theo lop
        public ProcessedReport GetExamPupilByClassReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_LOP;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamPupilByClassReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            int classID = Utils.GetInt(dic["ClassID"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_LOP;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = school.Province.ProvinceName;
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "Năm học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);

            //Lay danh sach lop
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["ExaminationsID"] = examinationsID;
            tmpDic["ExamGroupID"] = examGroupID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["EducationLevelID"] = educationLevelID;

            List<ExamPupilBO> lstExamPupil = ExamPupilBusiness.Search(tmpDic, academicYearID, partition).ToList();
            List<int> lstClassID = lstExamPupil.Select(o => o.ClassID).Distinct().ToList();

            //Lay danh sach lop thuoc khoi
            tmpDic = new Dictionary<string, object>();
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["EducationLevelID"] = educationLevelID;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;

            List<ClassProfile> listClassProfile = ClassProfileBusiness.SearchByAcademicYear(academicYearID, tmpDic).ToList();
            listClassProfile = listClassProfile.Where(o => lstClassID.Contains(o.ClassProfileID)).ToList();

            if (classID != 0)
            {
                listClassProfile = listClassProfile.Where(o => o.ClassProfileID == classID).ToList();
            }


            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                tmpDic = new Dictionary<string, object>();
                tmpDic["SchoolID"] = schoolID;
                tmpDic["ExaminationsID"] = examinationsID;
                tmpDic["ExamGroupID"] = examGroupID;

                listExamPupil = ExamPupilBusiness.Search(tmpDic, academicYearID, partition).ToList();

            }
            //Tao tung sheet cho moi lop
            for (int i = 0; i < listClassProfile.Count; i++)
            {
                ClassProfile cp = listClassProfile[i];

                //Fill du lieu
                //Danh sach thi sinh cua phong thi
                List<ExamPupilBO> listResult = listExamPupil.Where(o => o.ClassID == cp.ClassProfileID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 10;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 7;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "G" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = cp.DisplayName;
                curSheet.SetCellValue("A7", "Lớp " + cp.DisplayName);

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamPupilBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //Ngay sinh
                    curSheet.SetCellValue(curRow, curColumn, obj.BirthDate);
                    curColumn++;
                    //Gioi tinh
                    curSheet.SetCellValue(curRow, curColumn, obj.DisplayGenre);
                    curColumn++;
                    //Phong thi
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamRoomCode);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                if (listResult.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                curSheet.Orientation = VTXPageOrientation.VTxlPortrait;
                curSheet.FitAllColumnsOnOnePage = true;
            }

            //Xoa sheet dau tien
            firstSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamPupilByClassReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_LOP;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Bao cao danh sach diem danh

        public ProcessedReport GetExamPupilAttendanceReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEM_DANH;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamPupilAttendanceReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEM_DANH;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet templateSheet = oBook.GetSheet(2);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "NĂM HỌC: " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("A4", examinationsName);
            firstSheet.SetCellValue("A5", strAcademicYear);
            firstSheet.SetCellValue("I4", provinceAndDate);

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                 .Where(o => o.ExamGroupID == examGroupID)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            if (examRoomID != 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }
            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();

            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["ExaminationsID"] = examinationsID;
                SearchInfo["ExamGroupID"] = examGroupID;

                listExamPupil = ExamPupilBusiness.Search(SearchInfo, academicYearID, partition).ToList();

            }

            //Lay danh sach mon thi cua nhom thi
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                dic["ExaminationsID"] = examinationsID;
                dic["ExamGroupID"] = examGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
            }

            //So mon thi
            int subjectNum = listExamSubject.Count;

            //Tao tung sheet cho moi phong thi
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom examRoom = listExamRoom[i];

                //Fill du lieu
                //Danh sach thi sinh cua phong thi
                List<ExamPupilBO> listResult = listExamPupil.Where(o => o.ExamRoomID == examRoom.ExamRoomID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 11;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 7 + subjectNum;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "N" + (lastRow).ToString());

                //Fill tieu de
                curSheet.Name = examRoom.ExamRoomCode;
                curSheet.SetCellValue("A7", "PHÒNG THI: " + examRoom.ExamRoomCode);

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamPupilBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //Ngay sinh
                    curSheet.SetCellValue(curRow, curColumn, obj.BirthDate);
                    curColumn++;
                    //Gioi tinh
                    curSheet.SetCellValue(curRow, curColumn, obj.DisplayGenre);
                    curColumn++;
                    //Lop
                    curSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        style = VTBorderStyle.Dotted;
                        weight = VTBorderWeight.Thin;
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Thin;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                    curRow++;
                    curColumn = startColumn;
                }

                //Fill danh sach mon hoc cua nhom thi
                IVTRange templateSubjectRange = templateSheet.GetRange("A1", "A1");
                IVTRange templateNoteRange = templateSheet.GetRange("B2", "B2");
                IVTRange templateSubjectHeaderRange = templateSheet.GetRange("A2", "A2");

                int subjectColumn = 7;
                int subjectRow = 10;

                for (int j = 0; j < listExamSubject.Count; j++)
                {
                    ExamSubjectBO subject = listExamSubject[j];

                    curSheet.CopyPasteSameSize(templateSubjectRange, subjectRow, subjectColumn);
                    curSheet.SetCellValue(subjectRow, subjectColumn, subject.SubjectName);

                    subjectColumn++;
                }

                //Fill cot ghi chu
                curSheet.CopyPasteSameSize(templateNoteRange, subjectRow - 1, subjectColumn);
                curSheet.MergeColumn(subjectColumn, subjectRow - 1, subjectRow);

                //Cot mon thi
                if (subjectNum > 0)
                {
                    curSheet.SetCellValue("G9", "Môn thi");
                    curSheet.CopyPasteSameSize(templateSubjectHeaderRange, subjectRow - 1, 7);
                    curSheet.MergeRow(subjectRow - 1, 7, subjectColumn - 1);
                }


                if (listResult.Count > 0)
                {
                    IVTRange globalRange = curSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.Around);
                    globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);
                }

                //Ve border cho tieu de
                IVTRange headerRange = curSheet.GetRange(9, startColumn, 10, lastColumn);
                headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                curSheet.Orientation = VTXPageOrientation.VTxlLandscape;
                curSheet.FitAllColumnsOnOnePage = true;
            }


            //Xoa sheet dau tien
            firstSheet.Delete();
            templateSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamPupilAttendanceReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEM_DANH;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            ExamGroup examGroup = ExamGroupBusiness.Find(dic["ExamGroupID"]);
            string examGroupCode = "";
            if (examGroup != null)
            {
                examGroupCode = examGroup.ExamGroupCode;
            }

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[ExamGroup]", ReportUtils.StripVNSign(examGroupCode));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region // Phieu bao thi
        public ProcessedReport GetPaperContestReport(IDictionary<string, object> dic)
        {
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            string reportCode = "";
            if (EducationLevelID == 0)
            {
                reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_THI_ZIP;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_THI;
            }
            string inputParameterHashKey = GetHashKey(dic);
            //return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);

            var objProcess = ProcessedReportRepository.All.Where(x => x.ReportCode == reportCode)
                                                    .Where(x => x.InputParameterHashKey == inputParameterHashKey)
                                                    .OrderByDescending(x => x.ProcessedDate).FirstOrDefault();
            if (objProcess != null)
            {
                return objProcess;
            }
            return null;
        }

        public ProcessedReport InsertPaperContestReport(IDictionary<string, object> dic, Stream data, string reportCode, bool isZip = false)
        {
            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_THI_ZIP ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data));

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + (isZip != true ? reportDef.OutputFormat : "zip");
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        public Stream CreatePaperContestReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int appliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            int classID = Utils.GetInt(dic["ClassID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);

            string reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            #region // Get data
            SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            string schoolName = objSchool.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(objSchool.SchoolProfileID, appliedLevelID).ToUpper();
            string examinationsName = exam.ExaminationsName.ToUpper();

            EducationLevel objEdu = EducationLevelBusiness.Find(educationLevelID);
            List<ClassProfile> lstClass = ClassProfileBusiness.All.Where(x => x.EducationLevelID == objEdu.EducationLevelID
                                                                        && x.SchoolID == schoolID && x.AcademicYearID == academicYearID
                                                                        && x.IsActive == true).ToList();
            if (classID > 0)
            { lstClass = lstClass.Where(x => x.ClassProfileID == classID).ToList(); }
            lstClass = lstClass.OrderBy(x => x.OrderNumber).ThenBy(x => x.DisplayName).ToList();

            List<int> lstClassID = new List<int>();
            lstClassID = lstClass.Select(x => x.ClassProfileID).ToList();

            IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All.Where(x => x.SchoolID == schoolID
                                                            && lstClassID.Contains(x.ClassID)
                                                           && x.AcademicYearID == academicYearID
                                                           && x.Status == GlobalConstants.PUPIL_STATUS_STUDYING);

            List<int> lstPupilID = lstPupilOfClass.Select(x => x.PupilID).ToList();
            //Lay danh sach thi sinh
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = schoolID;
            SearchInfo["AcademicYearID"] = academicYearID;
            SearchInfo["EducationLevelID"] = educationLevelID;
            SearchInfo["ExaminationsID"] = examinationsID;
            SearchInfo["lstPupilID"] = lstPupilID;

            IQueryable<ExamGroup> lstExamGroup = ExamGroupBusiness.Search(SearchInfo);
            if (lstExamGroup.Count() == 0)
            {
                throw new BusinessException("Thầy/cô chưa khai báo nhóm thi");
            }

            IQueryable<ExamSubject> lstExamSubject = ExamSubjectBusiness.Search(SearchInfo);
            if (lstExamSubject.Count() == 0)
            {
                throw new BusinessException("Thầy/cô chưa khai báo môn thi");
            }

            IQueryable<ExamPupil> listExamPupil = this.Search(SearchInfo);
            if (listExamPupil.Count() == 0)
            {
                throw new BusinessException("Thầy/cô chưa khai báo thí sinh");
            }
            lstPupilID = listExamPupil.Select(x => x.PupilID).ToList();
            List<ExamRoom> lstExamRoom = ExamRoomBusiness.Search(SearchInfo).ToList();

            lstPupilOfClass = lstPupilOfClass.Where(x => lstPupilID.Contains(x.PupilID));

            List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(x => lstPupilID.Contains(x.PupilProfileID) && x.IsActive == true).ToList();

            var lstExamPupilBO = (from ep in listExamPupil
                                  join poc in lstPupilOfClass on ep.PupilID equals poc.PupilID
                                  join es in lstExamSubject on ep.ExaminationsID equals es.ExaminationsID
                                  join eg in lstExamGroup on es.ExamGroupID equals eg.ExamGroupID
                                  where
                                  ep.ExaminationsID == eg.ExaminationsID
                                  && ep.ExamGroupID == eg.ExamGroupID
                                  select new ExamPupilBO()
                                  {
                                      ClassID = poc.ClassID,
                                      PupilID = ep.PupilID,
                                      SubjectID = es.SubjectID,
                                      SchedulesExam = es.SchedulesExam,
                                      OrderInClass = poc.OrderInClass,
                                      ExaminationsID = ep.ExaminationsID,
                                      ExamGroupID = eg.ExamGroupID,
                                      ExamineeNumber = ep.ExamineeNumber,
                                      ExamRoomID = ep.ExamRoomID
                                  }).ToList();
            List<int> lstSubjectID = lstExamPupilBO.Select(x => x.SubjectID).ToList();
            List<SubjectCat> lstSubject = SubjectCatBusiness.All.Where(x => lstSubjectID.Contains(x.SubjectCatID)).OrderBy(x => x.OrderInSubject).ToList();
            #endregion

            List<SMAS.VTUtils.Utils.VTDataImageValidation> lstDataImage = new List<VTDataImageValidation>();

            dic.Add("SchoolName", schoolName);
            dic.Add("SupervisingDeptName", supervisingDeptName);
            dic.Add("ExaminationsName", examinationsName);
            dic.Add("HeadMasterName", objSchool.HeadMasterName);
            this.CreatePagerContestZipFile(oBook, dic, lstClass, lstExamPupilBO, lstSubject, lstPupilProfile, lstExamRoom, out lstDataImage);

            return oBook.InsertImage(lstDataImage);
        }

        public void DownloadZipFilePagerContest(IDictionary<string, object> dic, out string folderSaveFile)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int appliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);

            string fileName = "HS_PhieuBaoThi_{0}.xls";// HS_PhieuBaoThi_Khoi9.xls
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);

            string reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);

            #region // Get data
            SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            string schoolName = objSchool.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(objSchool.SchoolProfileID, appliedLevelID).ToUpper();
            string examinationsName = exam.ExaminationsName.ToUpper();

            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(x => x.Grade == appliedLevelID).ToList();
            List<int> lstEducationID = lstEdu.Select(x => x.EducationLevelID).ToList();
            List<ClassProfile> lstClass = ClassProfileBusiness.All.Where(x => lstEducationID.Contains(x.EducationLevelID)
                                                                        && x.SchoolID == schoolID && x.AcademicYearID == academicYearID
                                                                        && x.IsActive == true).ToList();
            lstClass = lstClass.OrderBy(x => x.DisplayName).ToList();

            List<int> lstClassID = new List<int>();
            lstClassID = lstClass.Select(x => x.ClassProfileID).ToList();

            IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All.Where(x => x.SchoolID == schoolID
                                                            && lstClassID.Contains(x.ClassID)
                                                           && x.AcademicYearID == academicYearID
                                                           && x.Status == GlobalConstants.PUPIL_STATUS_STUDYING);

            List<int> lstPupilID = lstPupilOfClass.Select(x => x.PupilID).ToList();
            //Lay danh sach thi sinh
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = schoolID;
            SearchInfo["AcademicYearID"] = academicYearID;
            SearchInfo["EducationLevelID"] = educationLevelID;
            SearchInfo["ExaminationsID"] = examinationsID;
            SearchInfo["lstPupilID"] = lstPupilID;

            IQueryable<ExamGroup> lstExamGroup = ExamGroupBusiness.Search(SearchInfo);
            if (lstExamGroup.Count() == 0)
            {
                folderSaveFile = string.Empty;
                throw new BusinessException("Thầy/cô chưa khai báo nhóm thi");
            }

            IQueryable<ExamSubject> lstExamSubject = ExamSubjectBusiness.Search(SearchInfo);
            if (lstExamSubject.Count() == 0)
            {
                folderSaveFile = string.Empty;
                throw new BusinessException("Thầy/cô chưa khai báo môn thi");
            }

            IQueryable<ExamPupil> listExamPupil = this.Search(SearchInfo);
            if (listExamPupil.Count() == 0)
            {
                folderSaveFile = string.Empty;
                throw new BusinessException("Thầy/cô chưa khai báo thí sinh");
            }
            lstPupilID = listExamPupil.Select(x => x.PupilID).ToList();
            List<ExamRoom> lstExamRoom = ExamRoomBusiness.Search(SearchInfo).ToList();

            lstPupilOfClass = lstPupilOfClass.Where(x => lstPupilID.Contains(x.PupilID));

            List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(x => lstPupilID.Contains(x.PupilProfileID) && x.IsActive == true).ToList();

            var lstExamPupilBO = (from ep in listExamPupil
                                  join poc in lstPupilOfClass on ep.PupilID equals poc.PupilID
                                  join es in lstExamSubject on ep.ExaminationsID equals es.ExaminationsID
                                  join eg in lstExamGroup on es.ExamGroupID equals eg.ExamGroupID
                                  where ep.ExaminationsID == eg.ExaminationsID
                                  && ep.ExamGroupID == eg.ExamGroupID
                                  select new ExamPupilBO()
                                  {
                                      ClassID = poc.ClassID,
                                      PupilID = ep.PupilID,
                                      SubjectID = es.SubjectID,
                                      SchedulesExam = es.SchedulesExam,
                                      OrderInClass = poc.OrderInClass,
                                      ExaminationsID = ep.ExaminationsID,
                                      ExamGroupID = eg.ExamGroupID,
                                      ExamineeNumber = ep.ExamineeNumber,
                                      ExamRoomID = ep.ExamRoomID
                                  }).ToList();

            List<int> lstSubjectID = lstExamPupilBO.Select(x => x.SubjectID).ToList();
            List<SubjectCat> lstSubject = SubjectCatBusiness.All.Where(x => lstSubjectID.Contains(x.SubjectCatID)).OrderBy(x => x.OrderInSubject).ToList();
            #endregion

            #region // Fill data
            dic.Add("SchoolName", schoolName);
            dic.Add("SupervisingDeptName", supervisingDeptName);
            dic.Add("ExaminationsName", examinationsName);
            dic.Add("HeadMasterName", objSchool.HeadMasterName);

            string fileName2 = "";
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = null;
            List<ClassProfile> lstClassByEdu = null;
            List<VTDataImageValidation> lstDataImage = new List<VTDataImageValidation>();
            foreach (var item in lstEdu)
            {
                lstClassByEdu = lstClass.Where(x => x.EducationLevelID == item.EducationLevelID).OrderBy(x => x.OrderNumber).ThenBy(x => x.DisplayName).ToList();
                oBook = VTExport.OpenWorkbook(templatePath);
                this.CreatePagerContestZipFile(oBook, dic, lstClassByEdu, lstExamPupilBO, lstSubject, lstPupilProfile, lstExamRoom, out lstDataImage);
                fileName2 = string.Format(fileName, Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(item.Resolution)));
                oBook.CreateZipFile(fileName2, folder, lstDataImage);
                lstDataImage = new List<VTDataImageValidation>();
            }
            #endregion

            folderSaveFile = folder;
        }

        private string TrimClassName(string className)
        {
            string output = "";
            List<string> lstStr = className.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            if (lstStr[0].ToUpper() == "LỚP" || lstStr[0].ToUpper() == "LOP")
            {
                lstStr.Remove(lstStr[0]);
                output = string.Join(" ", lstStr.ToArray());
                return output;
            }
            return className;
        }

        private void CreatePagerContestZipFile(IVTWorkbook oBook, IDictionary<string, object> dic,
            List<ClassProfile> lstClassByEdu, List<ExamPupilBO> lstExamPupilBO, List<SubjectCat> lstSubject,
            List<PupilProfile> lstPupilProfile, List<ExamRoom> lstExamRoom, out List<VTDataImageValidation> lstDataImage)
        {
            string schoolName = Utils.GetString(dic, "SchoolName");
            string supervisingDeptName = Utils.GetString(dic, "SupervisingDeptName");
            string examinationsName = Utils.GetString(dic, "ExaminationsName");
            string headMasterName = Utils.GetString(dic, "HeadMasterName");

            IVTWorksheet infoSheet = oBook.GetSheet(4);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);

            #region // Fill sheet thong tin chung
            infoSheet.SetCellValue("B2", supervisingDeptName);
            infoSheet.SetCellValue("B3", schoolName);
            infoSheet.SetCellValue("B5", ("HỘI ĐỒNG " + examinationsName.Trim()));
            infoSheet.SetCellValue("B11", headMasterName);
            #endregion

            #region // Fill class
            List<SubjectCat> lstSubjectOfPupil = new List<SubjectCat>();
            ExamPupilBO objExam = null;
            PupilProfile objPupilProfile = null;
            ExamRoom objExamRoom = null;
            string examRoomCode = "";
            int index = 1;
            int startRowFillImage = 5;
            int startRowFillSubject = 14;
            List<VTDataImageValidation> lstDataImagePOC = new List<VTDataImageValidation>();
            VTDataImageValidation objDataImg = null;
            IVTWorksheet sheetFill = null;
            int itemClass = 0;
            IVTRange rangLessThan6 = secondSheet.GetRange("A2", "W29"); // <=6 môn
            IVTRange rangMoreThan6 = firstSheet.GetRange("A2", "W29"); // > 6 môn
            List<int> lstSubjectID = new List<int>();
            List<string> lstExamNumber = new List<string>();
            int pupilID = 0;
            string strPattern = @"[^a-zA-Z0-9-!@#$%^&()_+~.<>{}';.,|]";
            Regex rgx = new Regex(strPattern);

            #region
            foreach (var item in lstClassByEdu)
            {
                var tempExamPupil = lstExamPupilBO.Where(x => x.ClassID == item.ClassProfileID).OrderBy(x => x.OrderInClass).ToList();
                List<int> lstPupilInExam = tempExamPupil.Select(x => x.PupilID).Distinct().ToList();
                if (lstPupilInExam.Count() > 0)
                {
                    sheetFill = oBook.CopySheetToBeforeLast(thirdSheet);
                    for (int i = 0; i < lstPupilInExam.Count(); i++)
                    {
                        pupilID = lstPupilInExam[i];
                        lstSubjectID = tempExamPupil.Where(x => x.PupilID == pupilID).Select(o => o.SubjectID).ToList();
                        lstSubjectOfPupil = lstSubject.Where(x => lstSubjectID.Contains(x.SubjectCatID)).OrderBy(x => x.OrderInSubject).ToList();

                        lstExamNumber = tempExamPupil.Where(x => x.PupilID == pupilID && !string.IsNullOrEmpty(x.ExamineeNumber))
                                                   .Select(o => o.ExamineeNumber).Distinct().ToList();
                        objPupilProfile = lstPupilProfile.Where(x => x.PupilProfileID == pupilID).FirstOrDefault();

                        int temp = 0;
                        if (lstSubjectOfPupil.Count() > 6)
                        {
                            sheetFill.CopyPasteSameSize(rangMoreThan6, (startRowFillSubject - 12), 1);

                            sheetFill.SetCellValue(("I" + (startRowFillImage + 4)), objPupilProfile.FullName);
                            sheetFill.SetCellValue(("S" + (startRowFillImage + 4)), TrimClassName(item.DisplayName));
                            sheetFill.SetCellValue(("I" + (startRowFillImage + 5)), lstExamNumber.Count() > 0 ? string.Join(", ", lstExamNumber.ToArray()) : "");

                            int halfValue = (int)Math.Ceiling(((double)lstSubjectOfPupil.Count() / 2));
                            temp = halfValue;
                            int k = 0;
                            for (int j = 0; j < lstSubjectOfPupil.Count(); j++)
                            {
                                objExam = tempExamPupil.Where(x => x.PupilID == lstPupilInExam[i] && x.SubjectID == lstSubjectOfPupil[j].SubjectCatID).FirstOrDefault();
                                if (objExam.ExamRoomID.HasValue)
                                {
                                    objExamRoom = lstExamRoom.Where(x => x.ExamRoomID == objExam.ExamRoomID.Value && x.ExamGroupID == objExam.ExamGroupID).FirstOrDefault();
                                    examRoomCode = objExamRoom != null ? objExamRoom.ExamRoomCode : "";
                                }
                                else
                                {
                                    examRoomCode = "";
                                }

                                if (halfValue > 0)
                                {
                                    sheetFill.SetCellValue(("B" + (startRowFillSubject + j)), index);

                                    sheetFill.GetRange((startRowFillSubject + j), 3, (startRowFillSubject + j), 5).Merge();
                                    sheetFill.GetRange((startRowFillSubject + j), 3, (startRowFillSubject + j), 5).SetHAlign(VTHAlign.xlHAlignCenter);
                                    sheetFill.SetCellValue(("C" + (startRowFillSubject + j)), lstSubjectOfPupil[j].DisplayName);

                                    sheetFill.GetRange((startRowFillSubject + j), 6, (startRowFillSubject + j), 8).Merge();
                                    sheetFill.GetRange((startRowFillSubject + j), 6, (startRowFillSubject + j), 8).SetHAlign(VTHAlign.xlHAlignCenter);
                                    sheetFill.SetCellValue(("F" + (startRowFillSubject + j)), examRoomCode);

                                    sheetFill.GetRange((startRowFillSubject + j), 9, (startRowFillSubject + j), 12).Merge();
                                    sheetFill.GetRange((startRowFillSubject + j), 9, (startRowFillSubject + j), 12).SetHAlign(VTHAlign.xlHAlignCenter);
                                    sheetFill.SetCellValue(("I" + (startRowFillSubject + j)), objExam.SchedulesExam);
                                }
                                else
                                {
                                    sheetFill.SetCellValue(("M" + (startRowFillSubject + k)), index);

                                    sheetFill.GetRange((startRowFillSubject + k), 14, (startRowFillSubject + k), 16).Merge();
                                    sheetFill.GetRange((startRowFillSubject + k), 14, (startRowFillSubject + k), 16).SetHAlign(VTHAlign.xlHAlignCenter);
                                    sheetFill.SetCellValue(("N" + (startRowFillSubject + k)), lstSubjectOfPupil[j].DisplayName);

                                    sheetFill.GetRange((startRowFillSubject + k), 17, (startRowFillSubject + k), 19).Merge();
                                    sheetFill.GetRange((startRowFillSubject + k), 17, (startRowFillSubject + k), 19).SetHAlign(VTHAlign.xlHAlignCenter);
                                    sheetFill.SetCellValue(("Q" + (startRowFillSubject + k)), examRoomCode);

                                    sheetFill.GetRange((startRowFillSubject + k), 20, (startRowFillSubject + k), 23).Merge();
                                    sheetFill.GetRange((startRowFillSubject + k), 20, (startRowFillSubject + k), 23).SetHAlign(VTHAlign.xlHAlignCenter);
                                    sheetFill.SetCellValue(("T" + (startRowFillSubject + k)), objExam.SchedulesExam);
                                    k++;
                                }
                                index++;
                                halfValue--;
                            }

                            if (lstSubjectOfPupil.Count() % 2 != 0)
                            {
                                sheetFill.GetRange((startRowFillSubject + k), 14, (startRowFillSubject + k), 16).Merge();
                                sheetFill.GetRange((startRowFillSubject + k), 17, (startRowFillSubject + k), 19).Merge();
                                sheetFill.GetRange((startRowFillSubject + k), 20, (startRowFillSubject + k), 23).Merge();
                            }

                            if (lstSubjectOfPupil.Count() <= 12)
                            {
                                sheetFill.SetRowHeight(startRowFillSubject + 6, 0);
                            }
                            else
                            {
                                for (int h = 0; h < temp; h++) // Nếu thi > 12 môn
                                {
                                    sheetFill.SetRowHeight(startRowFillSubject + h, 14);
                                }
                            }
                        }
                        else
                        {
                            sheetFill.CopyPasteSameSize(rangLessThan6, (startRowFillSubject - 12), 1);

                            sheetFill.SetCellValue(("I" + (startRowFillImage + 4)), objPupilProfile.FullName);
                            sheetFill.SetCellValue(("S" + (startRowFillImage + 4)), TrimClassName(item.DisplayName));
                            sheetFill.SetCellValue(("I" + (startRowFillImage + 5)), lstExamNumber.Count() > 0 ? string.Join(", ", lstExamNumber.ToArray()) : "");

                            for (int j = 0; j < lstSubjectOfPupil.Count(); j++)
                            {
                                objExam = tempExamPupil.Where(x => x.PupilID == lstPupilInExam[i] && x.SubjectID == lstSubjectOfPupil[j].SubjectCatID).FirstOrDefault();

                                if (objExam.ExamRoomID.HasValue)
                                {
                                    objExamRoom = lstExamRoom.Where(x => x.ExamRoomID == objExam.ExamRoomID.Value && x.ExamGroupID == objExam.ExamGroupID).FirstOrDefault();
                                    examRoomCode = objExamRoom != null ? objExamRoom.ExamRoomCode : "";
                                }
                                else
                                {
                                    examRoomCode = "";
                                }

                                sheetFill.SetCellValue(("B" + (startRowFillSubject + j)), index);

                                sheetFill.GetRange((startRowFillSubject + j), 3, (startRowFillSubject + j), 8).Merge();
                                sheetFill.GetRange((startRowFillSubject + j), 3, (startRowFillSubject + j), 8).SetHAlign(VTHAlign.xlHAlignCenter);
                                sheetFill.SetCellValue(("C" + (startRowFillSubject + j)), lstSubjectOfPupil[j].DisplayName);

                                sheetFill.GetRange((startRowFillSubject + j), 9, (startRowFillSubject + j), 13).Merge();
                                sheetFill.GetRange((startRowFillSubject + j), 9, (startRowFillSubject + j), 13).SetHAlign(VTHAlign.xlHAlignCenter);
                                sheetFill.SetCellValue(("I" + (startRowFillSubject + j)), examRoomCode);

                                sheetFill.GetRange((startRowFillSubject + j), 14, (startRowFillSubject + j), 23).Merge();
                                sheetFill.GetRange((startRowFillSubject + j), 14, (startRowFillSubject + j), 23).SetHAlign(VTHAlign.xlHAlignCenter);
                                sheetFill.SetCellValue(("N" + (startRowFillSubject + j)), objExam.SchedulesExam);

                                index++;
                            }

                            sheetFill.SetRowHeight(startRowFillSubject + 6, 0);
                        }

                        if (objPupilProfile != null && objPupilProfile.Image != null && objPupilProfile.Image.Length > 0)
                        {
                            objDataImg = new VTDataImageValidation();
                            objDataImg.UpperLeftRow = startRowFillImage;
                            objDataImg.UpperLeftColumn = 2;
                            objDataImg.LowerRightRow = (startRowFillImage + 6);
                            objDataImg.LowerRightColumn = 4;
                            objDataImg.SheetIndex = (1 + itemClass);
                            objDataImg.ByteImage = objPupilProfile.Image;
                            objDataImg.TypeFormat = 3;
                            lstDataImagePOC.Add(objDataImg);
                        }
                        else
                        {
                            sheetFill.GetRange(startRowFillImage, 2, (startRowFillImage + 6), 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                        }

                        sheetFill.GetRange(startRowFillSubject, 2, (startRowFillSubject + (lstSubjectOfPupil.Count() > 6 ? temp : lstSubjectOfPupil.Count()) - 1), 23).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                        sheetFill.GetRange((startRowFillSubject + 12), 17, (startRowFillSubject + 12), 23).Merge();
                        sheetFill.GetRange((startRowFillSubject + 12), 17, (startRowFillSubject + 12), 23).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheetFill.SetCellValue(("Q" + (startRowFillSubject + 12)), "=Thongtinchung!$B$11");

                        if (i > 0 && (i % 2 != 0))
                        {
                            sheetFill.SetBreakPage((startRowFillSubject + 16));
                        }


                        startRowFillSubject += 28;
                        startRowFillImage += 28;
                        index = 1;
                    }
                    string OuputName = rgx.Replace(ReportUtils.StripVNSign(item.DisplayName), "-");
                    OuputName = OuputName.Replace("[", "").Replace("]", "");
                    sheetFill.Name = OuputName;
                    sheetFill.SetFontName("Times New Roman", 11);
                    sheetFill.PageMaginTop = 0.4;
                    sheetFill.FitAllColumnsOnOnePage = true;
                    itemClass++;
                }

                index = 1;
                startRowFillImage = 5;
                startRowFillSubject = 14;
            }
            #endregion

            #endregion

            firstSheet.Delete();
            secondSheet.Delete();
            thirdSheet.Delete();
            lstDataImage = lstDataImagePOC;
        }
        #endregion

        public class CriteriaOrder
        {
            public int CheckCriteria { get; set; }
            public int OrderCritetia { get; set; }
        }
    }
}