/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;

using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    /// <summary>
    /// thông tin tổng kết điểm thi đua theo tuần
    /// <author>quanglm</author>
    /// <date>05/09/2012</date>
    /// </summary>

    public partial class ClassEmulationBusiness
    {
        public int startCol = 4;
        const int rowHeaderStart = 9;
        const int rowHeaderEnd = 11;

        public int startCol_M = 3;
        const int rowHeaderStart_M = 8;
        const int rowHeaderEnd_M = 10;
        #region InsertOrUpdate
        /// <summary>
        /// Thêm mới thông tin tổng kết điểm thi đua theo tuần và xếp loại thi đua của các lớp
        /// </summary>
        /// <param name="AcademicYearID">id năm học</param>
        /// <param name="EducationLevelID">id khối</param>
        /// <param name="UpdatedDate">ngày cập nhật</param>
        /// <param name="Data">Dữ liệu điểm của từng hạng thi đua của từng lớp</param>
        public void InsertOrUpdate(int AcademicYearID, int EducationLevelID, DateTime UpdatedDate, Dictionary<int, Dictionary<int, decimal>> Data)
        {

            //Dữ liệu điểm của từng hạng thi đua của từng lớp:
            //Dictionnary[ClassID] = Dictionnary<int , decimal> { EmulationCriteriaID -> mark}

            //Năm học không tồn tại – check AcademicYearID
            AcademicYearBusiness.CheckAvailable(AcademicYearID, "AcademicYear_Label_AcademicYearID");

            //Khối học không tồn tại – check EducationLevelID / AcademicYearID
            EducationLevelBusiness.CheckAvailable(EducationLevelID, "EducationLevel_Label_EducationLevelID");

            #region Kiem tra du lieu dau vao
            // Tu dien luu tru ID va Doi tuong cua bang EmulationCriteria
            Dictionary<int, EmulationCriteria> DicEmulationCriteria = new Dictionary<int, EmulationCriteria>();
            foreach (int ClassID in Data.Keys)
            {
                Dictionary<int, decimal> DicEmulation = Data[ClassID];
                foreach (int EmulationCriteriaID in DicEmulation.Keys)
                {
                    decimal mark = DicEmulation[EmulationCriteriaID];
                    EmulationCriteriaBusiness.CheckAvailable(EmulationCriteriaID, "EmulationCriteria_Label_AllTitle", true);
                    EmulationCriteria EmulationCriteria = EmulationCriteriaBusiness.Find(EmulationCriteriaID);
                    //Điểm phải là số lớn hơn 0.
                    if (mark < 0)
                    {
                        throw new BusinessException("ClassEmulation_Err_Mark");
                    }
                    //Điểm các loại thi đua không được vượt quá điểm Max của từng loại thi đua (điểm max lấy từ EmulationCriteria)
                    if (mark > EmulationCriteria.Mark)
                    {
                        throw new BusinessException("ClassEmulation_Err_EmulationCriteria");
                    }
                    // Them vao tu dien
                    DicEmulationCriteria[EmulationCriteriaID] = EmulationCriteria;
                }
                //Kiểm tra lớp có tồn tại không, có trong  khối/ một năm học không
                ClassProfile ClassProfile = ClassProfileRepository.Find(ClassID);
                if (ClassProfile.AcademicYearID != AcademicYearID || ClassProfile.EducationLevelID != EducationLevelID)
                {
                    throw new BusinessException("ClassEmulation_Err_Class");
                }

            }

            //Không cho phép nhập điểm cho ngày lớn hơn ngày hiện tại – Kiểm tra UpdatedDate, lấy ngày chủ nhật của tuần tương ứng so sánh với ngày hiện tại
            DateTime EndDate = DateTime.Now.EndOfWeek();
            if (UpdatedDate > EndDate)
            {
                throw new BusinessException("ClassEmulation_Err_UpdateDate");
            }
            #endregion

            #region Xu ly theo nghiep vu
            // Them du lieu vao CSDL
            AcademicYear AcademicYear = AcademicYearBusiness.Find(AcademicYearID);

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (int ClassID in Data.Keys)
                {
                    #region xoa du lieu cu
                    // Xoa du lieu cua trong bang ClassEmulationDetailBusiness
                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["AcademicYearID"] =  AcademicYearID;
                    SearchInfo["EducationLevelID"] =  EducationLevelID;
                    SearchInfo["ClassID"] = ClassID;
                    SearchInfo["UpdatedDate"] =  UpdatedDate;
                    var _fromdate = UpdatedDate.StartOfWeek();
                    var _enddate = _fromdate.EndOfWeek();
                    DateTime FromDate = new DateTime(_fromdate.Year, _fromdate.Month, _fromdate.Day, 0, 0, 0);
                    DateTime endDate = new DateTime(_enddate.Year, _enddate.Month, _enddate.Day, 0, 0, 0);
                    SearchInfo["FromDate"] = FromDate;
                    SearchInfo["EndDate"] = endDate;
                    IQueryable<ClassEmulationDetail> ListClassEmulationDetail = ClassEmulationDetailBusiness.Search(SearchInfo);
                    if (ListClassEmulationDetail != null)
                    {
                        ClassEmulationDetailBusiness.DeleteAll(ListClassEmulationDetail.ToList());
                        ClassEmulationDetailBusiness.Save();
                    }
                    // Xoa du lieu bang ClassEmulation
                    IQueryable<ClassEmulation> ListClassEmulation = this.Search(SearchInfo);
                    if (ListClassEmulation != null)
                    {
                        this.DeleteAll(ListClassEmulation.ToList());
                        this.Save();
                    }
                    #endregion

                    // Them moi du lieu vao bang ClassEmulation
                    ClassEmulation ClassEmulation = new ClassEmulation();
                    ClassEmulation.ClassID = ClassID;
                    ClassEmulation.AcademicYearID = AcademicYearID;
                    ClassEmulation.SchoolID = AcademicYear.SchoolID;
                    ClassEmulation.Year = AcademicYear.Year;
                    ClassEmulation.Month = (int)UpdatedDate.Month;
                    ClassEmulation.FromDate = UpdatedDate.StartOfWeek();
                    ClassEmulation.ToDate = ClassEmulation.FromDate.Value.EndOfWeek();
                    // Tong diem thi dua
                    decimal TotalEmulationMark = 0;
                    Dictionary<int, decimal> DicEmulation = Data[ClassID];
                    foreach (int EmulationCriteriaID in DicEmulation.Keys)
                    {
                        decimal mark = DicEmulation[EmulationCriteriaID];
                        TotalEmulationMark += mark;
                    }
                    ClassEmulation.TotalEmulationMark = TotalEmulationMark;
                    // Tinh tong diem tru
                    decimal TotalPenalizedMark = PupilFaultBusiness.SumTotalPenalizedMarkByClass(ClassID, ClassEmulation.FromDate.Value, ClassEmulation.ToDate.Value);
                    ClassEmulation.TotalPenalizedMark = TotalPenalizedMark;
                    ClassEmulation.TotalMark = TotalEmulationMark - TotalPenalizedMark;
                    this.Insert(ClassEmulation);
                    this.Save();
                    // Them moi vao bang ClassEmulationDetail
                    foreach (int EmulationCriteriaID in DicEmulation.Keys)
                    {
                        decimal mark = DicEmulation[EmulationCriteriaID];
                        ClassEmulationDetail ClassEmulationDetail = new ClassEmulationDetail();
                        ClassEmulationDetail.ClassEmulationID = ClassEmulation.ClassEmulationID;
                        ClassEmulationDetail.ClassID = ClassID;
                        ClassEmulationDetail.EmulationCriteriaId = (int)EmulationCriteriaID;
                        EmulationCriteria EmulationCriteria = DicEmulationCriteria[EmulationCriteriaID];
                        ClassEmulationDetail.PenalizedMark = /*EmulationCriteria.Mark -*/ mark;
                        ClassEmulationDetail.PenalizedDate = UpdatedDate;
                        ClassEmulationDetailBusiness.Insert(ClassEmulationDetail);
                        ClassEmulationDetailBusiness.Save();
                    }
                }
                scope.Complete();
            }

            // Xep rank thi dua cho cac lop
            this.OrderClassEmulationRank(AcademicYearID, EducationLevelID, UpdatedDate);
            #endregion
        }
        #endregion

        #region Tim kiem
        /// <summary>
        /// Tìm kiếm thông tin tổng kết điểm thi đua theo tuần và xếp loại thi đua của các lớp
        /// </summary>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<ClassEmulation> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Year = Utils.GetInt(dic, "Year");
            int Month = Utils.GetInt(dic, "Month");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? EndDate = Utils.GetDateTime(dic, "EndDate");

            DateTime? UpdateDate = Utils.GetDateTime(dic, "UpdatedDate");
            if (!FromDate.HasValue && UpdateDate.HasValue)
            {
                FromDate = UpdateDate.Value.StartOfWeek();
                FromDate = new DateTime(FromDate.Value.Year, FromDate.Value.Month, FromDate.Value.Day, 0, 0, 0);
            }
            if (!EndDate.HasValue && FromDate.HasValue)
            {
                EndDate = FromDate.Value.EndOfWeek();
                EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, 0, 0, 0);
            }
            IQueryable<ClassEmulation> lsClassEmulation = this.ClassEmulationRepository.All;
            lsClassEmulation = lsClassEmulation.Where(em => (em.FromDate == FromDate) && (em.ToDate == EndDate));
            if (ClassID != 0)
            {
                lsClassEmulation = from ce in lsClassEmulation
                                   join cp in ClassProfileBusiness.All on ce.ClassID equals cp.ClassProfileID
                                   where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                   && ce.ClassID == ClassID
                                   select ce;
            }
            // Tim kiem theo truong
            if (SchoolID != 0)
            {
                lsClassEmulation = lsClassEmulation.Where(em => (em.SchoolID == SchoolID));
            }
            // Theo nam hoc
            if (AcademicYearID != 0)
            {
                lsClassEmulation = lsClassEmulation.Where(em => (em.AcademicYearID == AcademicYearID));
            }
            // Theo nam
            if (Year != 0)
            {
                lsClassEmulation = lsClassEmulation.Where(em => (em.Year == Year));
            }
            // Theo thang
            if (Month != 0)
            {
                lsClassEmulation = lsClassEmulation.Where(em => (em.Month == Month));
            }
            // Theo khoi
            if (EducationLevelID != 0)
            {
                lsClassEmulation = lsClassEmulation.Where(em => (em.ClassProfile.EducationLevelID == EducationLevelID));
            }

            
            return lsClassEmulation;

        }
        #endregion

        #region Xep hang cho cac lop dua vao tong diem thi dua
        /// <summary>
        /// Xep hang cho cac lop dua vao tong diem thi dua
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        public void OrderClassEmulationRank(int AcademicYearID, int EducationLevelID, DateTime UpdatedDate)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["EducationLevelID"] =  EducationLevelID;
            SearchInfo["UpdatedDate"] = UpdatedDate;
            IQueryable<ClassEmulation> ListClassEmulation = this.Search(SearchInfo);
            if (ListClassEmulation != null)
            {
                // Sap xep diem theo thu tu giam dan
                ListClassEmulation = ListClassEmulation
                    .OrderByDescending(x => x.TotalMark);



                List<ClassEmulation> ListData = ListClassEmulation.ToList();
                List<decimal?> listMark = ListClassEmulation.Select(o => o.TotalMark).ToList();
                Dictionary<int, int> DicClassRank = new Dictionary<int, int>();
                // Rank bat dau la 1
                //int rank = 1;
                //int m = 0;
                var MarkAndRank = GetMarkAndRank(listMark);
                //<100,3>
                //<50,2>
                foreach (ClassEmulation ClassEmulation in ListData)
                {
                    if (MarkAndRank.ContainsKey(ClassEmulation.TotalMark))
                    {
                        ClassEmulation.Rank = MarkAndRank[ClassEmulation.TotalMark];
                    }
                    this.Update(ClassEmulation);
                }
            }
        }
        #endregion


        private Dictionary<decimal?, int> GetMarkAndRank(List<decimal?> ListMark)
        {
            Dictionary<decimal?, int> dic = new Dictionary<decimal?, int>();
            int dem = 0;
            int n = ListMark.Count;
            int rank = 1;
            for (int i = 0; i < n; i++)
            {
                for (int j = i; j < n; j++)
                {
                    if (ListMark[i] == ListMark[j])
                    {
                        dem++;
                    }
                    for (int z = 0; z < i; z++)
                    {
                        if (ListMark[z] == ListMark[i])
                        {
                            dem = 0; break;
                        }
                    }
                }
                if (dem != 0)
                {
                    dic.Add(ListMark[i], rank);
                    rank = (int)(rank + dem);
                }
                dem = 0;
            }
            return dic;
        }


        public Stream ExportClassEmulationByWeek(int SchoolID, int AcademicYearID, DateTime date, int EducationLevelID, out string FileName)
        {
            int week = this.GetWeekNumber(date);
            int month = date.Month;
            var _start = date.StartOfWeek();
            var _end = _start.EndOfWeek();

            DateTime start = new DateTime(_start.Year, _start.Month, _start.Day, 0, 0, 0);
            DateTime end = new DateTime(_end.Year, _end.Month, _end.Day, 0, 0, 0);
            IDictionary<string, object> dic_PupilFault = new Dictionary<string, object>();
            dic_PupilFault["AcademicYearID"] = AcademicYearID;
            dic_PupilFault["EducationLevelID"] = EducationLevelID;
            dic_PupilFault["FromViolatedDate"] = start;
            dic_PupilFault["ToViolatedDate"] = end;
            dic_PupilFault["FromDate"] = start;
            dic_PupilFault["EndDate"] = end;
            IQueryable<ClassEmulationDetail> lstClassEmulationDetailTemp = ClassEmulationDetailBusiness.SearchBySchool(SchoolID, dic_PupilFault);
            List<ClassEmulationDetail> lstClassEmulationDetail = lstClassEmulationDetailTemp != null ? lstClassEmulationDetailTemp.ToList() : new List<ClassEmulationDetail>();

            IQueryable<ClassEmulation> lstClassEmulationTemp = ClassEmulationBusiness.Search(dic_PupilFault);
            List<ClassEmulation> lstClassEmulation =lstClassEmulationTemp !=null ?  lstClassEmulationTemp.ToList() : new List<ClassEmulation>();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("HS_XepLoaiThiDuaCacLop_Tuan");

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);

            //HS_XepLoaiThiDuaCacLop_[EducationLevel]_[Tuan]_[Thang]
            string outputNamePattern = reportDef.OutputNamePattern;
            string educationLevel = EducationLevelID == 0 ? "TatCa" : EducationLevelRepository.Find(EducationLevelID).Resolution;
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationLevel));
            //outputNamePattern = outputNamePattern.Replace("[Tuan]", "Tuan" + week.ToString());
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            string schoolname = SchoolProfileBusiness.Find(SchoolID).SchoolName;
            sheet.SetCellValue("A2", schoolname.ToUpper());

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            var lstEmulationCriteria = EmulationCriteriaBusiness.SearchBySchool(SchoolID, dic).ToList();
            
            //Change header A5
            sheet.MergeRow(5, 1, 5 + lstEmulationCriteria.Count);
            string A5_format = "Xếp hạng thi đua các lớp {0}";
            sheet.SetCellValue("A5", string.Format(A5_format, educationLevel).ToUpper());

            //Change header A6
            sheet.MergeRow(6, 1, 5 + lstEmulationCriteria.Count);
            string A6_format = "Từ ngày {0} đến ngày {1}";
            sheet.SetCellValue("A6", string.Format(A6_format, start.ToShortDateString(), end.ToShortDateString()));

            int lastcol = 4;
            foreach (var item in lstEmulationCriteria)
            {
                copyCol(firstSheet, sheet, 4, startCol, rowHeaderStart, rowHeaderEnd);
                lastcol++;
            }
            
            copyCol(firstSheet, sheet, 5, startCol, rowHeaderStart, rowHeaderEnd);
            copyCol(firstSheet, sheet, 6, startCol, rowHeaderStart, rowHeaderEnd);
            
            var lstClassProfile = ClassProfileBusiness.SearchBySchool(SchoolID, dic_PupilFault)
                                                        .Select(o => new ClassEmulationReportObject
                                                        {
                                                            ClassProfileID = o.ClassProfileID,
                                                            DisplayName = o.DisplayName
                                                        }).ToList();
            //coppy row
            for (int i = 0; i < lstClassProfile.Count - 1; i++)
                sheet.CopyAndInsertARow(11, 11, false);

            // mert header
            sheet.MergeRow(9, 4, lstEmulationCriteria.Count + 3);
            sheet.SetCellValue("D9", "Điểm thi đua");

            int colIndex = 4;
            foreach (var emuCriteria in lstEmulationCriteria)
                sheet.SetCellValue(10, colIndex++, emuCriteria.Resolution);
            
            foreach (var item in lstClassProfile)
            {
                item.TotalPenalizedMark = PupilFaultBusiness.SumTotalPenalizedMarkByClass(item.ClassProfileID, start, end);
                item.TotalMark = 0 - item.TotalPenalizedMark.Value;

                foreach (var emulationCriteria in lstEmulationCriteria)
                {
                    var emuDetail = lstClassEmulationDetail.Where(u => u.ClassID == item.ClassProfileID && u.EmulationCriteriaId == emulationCriteria.EmulationCriteriaId).SingleOrDefault();
                    if (emuDetail != null && emuDetail.PenalizedMark.HasValue)
                        item.TotalMark  += emuDetail.PenalizedMark.Value;
                    else
                        item.TotalMark += emulationCriteria.Mark;
                }
            }

            int rank = 1;
           
            lstClassProfile = lstClassProfile.OrderByDescending(u => u.TotalMark).ThenBy(u=> u.DisplayName).ToList();
            decimal totalMark = lstClassProfile.FirstOrDefault().TotalMark.Value;
            
            foreach (var ce in lstClassProfile)
            {
                int countequals = 0;
                if (ce.TotalMark < totalMark)
                {
                    countequals = lstClassProfile.Where(o => o.TotalMark == totalMark).Count();
                    totalMark = ce.TotalMark.Value;                  
                    //rank++;
                }
                rank = rank + countequals;
                ce.Rank = rank;
            }

            int startrowfilter = 11;
            int rowIndex = 1;
            lstClassProfile = lstClassProfile.OrderBy(o => o.DisplayName).ToList();
            foreach (var item in lstClassProfile)
            {
                colIndex = 1;
                sheet.SetCellValue(startrowfilter, colIndex++, rowIndex);
                sheet.SetCellValue(startrowfilter, colIndex++, item.DisplayName);
                sheet.SetCellValue(startrowfilter, colIndex++, item.TotalPenalizedMark);
                
                foreach (var emulationCriteria in lstEmulationCriteria)
                {
                    var emuDetail = lstClassEmulationDetail.Where(u => u.ClassID == item.ClassProfileID && u.EmulationCriteriaId == emulationCriteria.EmulationCriteriaId).SingleOrDefault();
                    if (emuDetail != null && emuDetail.PenalizedMark.HasValue)
                        sheet.SetCellValue(startrowfilter, colIndex++, emuDetail.PenalizedMark.Value);
                    else
                        sheet.SetCellValue(startrowfilter, colIndex++, emulationCriteria.Mark);
                }
                sheet.SetCellValue(startrowfilter, colIndex++, item.TotalMark);
                sheet.SetCellValue(startrowfilter, colIndex++, item.Rank);

                startrowfilter++;
                rowIndex++;
            }

            firstSheet.Delete();

            return oBook.ToStream();
        }

        public Stream ExportClassEmulationByMonth(int SchoolID, int AcademicYearID, int EducationLevelID, DateTime date, out string FileName)
        {
            var _start = date.StartOfWeek();
            var _end = _start.EndOfWeek();
            DateTime start = new DateTime(_start.Year, _start.Month, _start.Day, 0, 0, 0);
            DateTime end = new DateTime(_end.Year, _end.Month, _end.Day, 0, 0, 0);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("HS_XepLoaiThiDuaCacLop_Thang");

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);

            //HS_XepLoaiThiDuaCacLop_[EducationLevel]__[Thang]
            string outputNamePattern = reportDef.OutputNamePattern;
            string educationLevel = EducationLevelID == 0 ? "Tất cả" : EducationLevelRepository.Find(EducationLevelID).Resolution;
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationLevel));
            outputNamePattern = outputNamePattern.Replace("[Thang]", "Tháng " + date.Month);
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            string schoolname = SchoolProfileBusiness.Find(SchoolID).SchoolName;

            List<ClassEmulationStartEndDate> lstWeek = GetListWeekByDate(date);
            //Change header 1
            sheet.SetCellValue("A2", schoolname.ToUpper());
            //Change header A4
            sheet.MergeRow(4, 1, 4 + lstWeek.Count);
            string A4_format = "xếp hạng thi đua các lớp {0}";
            sheet.SetCellValue("A4", string.Format(A4_format, educationLevel).ToUpper());
            //Change header A5
            sheet.MergeRow(5, 1, 4 + lstWeek.Count);
            string A5_format = "Tháng {0} năm {1}";
            sheet.SetCellValue("A5", string.Format(A5_format, date.Month, date.Year));

            List<ClassEmulationProfileTempMonth> lstClassProfileTempMonth = new List<ClassEmulationProfileTempMonth>();

            List<ClassEmulation> lstClassEmulation = new List<ClassEmulation>();
            #region Get data
            for (var i = 0; i < lstWeek.Count; i++)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["FromDate"] = new DateTime(lstWeek[i].FromDate.Year, lstWeek[i].FromDate.Month, lstWeek[i].FromDate.Day, 0, 0, 0);
                dic["EndDate"] = new DateTime(lstWeek[i].ToDate.Year, lstWeek[i].ToDate.Month, lstWeek[i].ToDate.Day, 0, 0, 0);
                var lstEB = ClassEmulationBusiness.Search(dic);
                lstClassEmulation = lstEB != null ? ClassEmulationBusiness.Search(dic).ToList() : null;
                if (lstClassEmulation != null && lstClassEmulation.Count > 0)
                {
                    foreach (var ClassEmulation in lstClassEmulation)
                    {
                        if (lstClassProfileTempMonth.Any(o => o.ID == ClassEmulation.ClassID))
                        {
                            ClassEmulationProfileTempMonth ClassProfileTempMonth = lstClassProfileTempMonth.Where(o => o.ID == ClassEmulation.ClassID).FirstOrDefault();
                            if (i == 0)
                            {
                                ClassProfileTempMonth.TotalMark1 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark1;
                            }
                            else if (i == 1)
                            {
                                ClassProfileTempMonth.TotalMark2 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark2;
                            }
                            else if (i == 2)
                            {
                                ClassProfileTempMonth.TotalMark3 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark3;
                            }
                            else if (i == 3)
                            {
                                ClassProfileTempMonth.TotalMark4 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark4;
                            }
                            else if (i == 4)
                            {
                                ClassProfileTempMonth.TotalMark5 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark5;
                            }
                            lstClassProfileTempMonth.Remove(lstClassProfileTempMonth.Where(o => o.ID == ClassEmulation.ClassID).FirstOrDefault());
                            lstClassProfileTempMonth.Add(ClassProfileTempMonth);
                        }
                        else
                        {
                            ClassEmulationProfileTempMonth ClassProfileTempMonth = new ClassEmulationProfileTempMonth();
                            ClassProfileTempMonth.ID = ClassEmulation.ClassID;
                            ClassProfileTempMonth.Name = ClassEmulation.ClassProfile.DisplayName;
                            ClassProfileTempMonth.TotalMark = 0;
                            if (i == 0)
                            {
                                ClassProfileTempMonth.TotalMark1 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark1;
                            }
                            else if (i == 1)
                            {
                                ClassProfileTempMonth.TotalMark2 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark2;
                            }
                            else if (i == 2)
                            {
                                ClassProfileTempMonth.TotalMark3 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark3;
                            }
                            else if (i == 3)
                            {
                                ClassProfileTempMonth.TotalMark4 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark4;
                            }
                            else if (i == 4)
                            {
                                ClassProfileTempMonth.TotalMark5 = ClassEmulation.TotalMark;
                                ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark5;
                            }
                            lstClassProfileTempMonth.Add(ClassProfileTempMonth);
                        }
                    }
                }
            }
            #endregion

            var ListClassProfileTempMonth = lstClassProfileTempMonth.OrderBy(o => o.Name).OrderByDescending(o => o.TotalMark).ToList();
            //cop tu firstSheet sang sheet
            int lastcol = 3;
            if (lstWeek != null && lstWeek.Count > 0)
            {
                foreach (var item in lstWeek)
                {
                    copyCol(firstSheet, sheet, 3, lastcol, rowHeaderStart_M, rowHeaderEnd_M);
                    lastcol++;
                }
            }
            copyCol(firstSheet, sheet, 4, lstWeek.Count + 3, rowHeaderStart_M, rowHeaderEnd_M);
            copyCol(firstSheet, sheet, 5, lstWeek.Count + 4, rowHeaderStart_M, rowHeaderEnd_M);
            //coppy row
            if (ListClassProfileTempMonth != null && ListClassProfileTempMonth.Count > 0)
            {
                for (var c = 0; c < ListClassProfileTempMonth.Count - 1; c++)
                {
                    sheet.CopyAndInsertARow(10, 10, false);
                }
            }
            // mert header
            sheet.MergeRow(8, 3, lastcol - 1);
            sheet.SetCellValue("C8", "Điểm thi đua tuần");
            //do du lieu vao file
            if (ListClassProfileTempMonth != null && ListClassProfileTempMonth.Count > 0)
            {
                int startrowfilter = 10;
                int i = 1;
                char rankColfilter = 'A';
                if (lstWeek != null && lstWeek.Count > 0)
                {
                    int RowResolution = 9;
                    string resource_resolution = "Tuần {0}";
                    for (var w = 0; w < lstWeek.Count; w++)
                    {
                        sheet.SetCellValue(VTVector.dicResverse[w + 3].ToString() + RowResolution, string.Format(resource_resolution, w + 1));
                    }
                }
                foreach (var item in ListClassProfileTempMonth)
                {
                    //du lieu vao cot thu tu
                    sheet.SetCellValue("A" + startrowfilter, i);
                    //du lieu vao cot ten lop
                    sheet.SetCellValue("B" + startrowfilter, item.Name);
                    int c = 3;
                    //du lieu vao cot diem cua moi tuan
                    if (lstWeek != null && lstWeek.Count > 0)
                    {
                        if (item.TotalMark1.HasValue)
                        {
                            sheet.SetCellValue(VTVector.dicResverse[c].ToString() + startrowfilter, item.TotalMark1.Value);
                        }
                        if (item.TotalMark2.HasValue)
                        {
                            sheet.SetCellValue(VTVector.dicResverse[c + 1].ToString() + startrowfilter, item.TotalMark2.Value);
                        }
                        if (item.TotalMark3.HasValue)
                        {
                            sheet.SetCellValue(VTVector.dicResverse[c + 2].ToString() + startrowfilter, item.TotalMark3.Value);
                        }
                        if (item.TotalMark4.HasValue)
                        {
                            sheet.SetCellValue(VTVector.dicResverse[c + 3].ToString() + startrowfilter, item.TotalMark4.Value);
                        }
                        if (lstWeek.Count == 5)
                        {
                            if (item.TotalMark5.HasValue)
                            {
                                sheet.SetCellValue(VTVector.dicResverse[c + 4].ToString() + startrowfilter, item.TotalMark5.Value);
                            }
                        }
                    }
                    //du lieu vao cot tong diem
                    string fomula = "=Sum(C{0}:{1}{0})";
                    fomula = string.Format(fomula, startrowfilter, (char)('B' + lstWeek.Count));
                    sheet.SetFormulaValue(VTVector.dicResverse[c + lstWeek.Count].ToString() + startrowfilter, fomula);
                    startrowfilter++;
                    i++;
                }
                //du lieu vao cot xep hang
                rankColfilter = (char)('A' + lstWeek.Count + 3);
                int rankRowFilter = 10;

                Dictionary<int, int> DicClassRank = new Dictionary<int, int>();
                // Rank bat dau la 1
                int rank = 1;
                //40/40/20/-100
                foreach (var ClassProfile in ListClassProfileTempMonth)
                {
                    int ClassID = ClassProfile.ID;
                    if (!DicClassRank.ContainsKey(ClassID))
                    {
                        var ListCurrentMark = ListClassProfileTempMonth.Where(o => o.TotalMark == ClassProfile.TotalMark);
                        foreach (var item in ListCurrentMark)
                        {
                            DicClassRank.Add(item.ID, rank);
                        }
                        rank += ListCurrentMark.Count();
                    }
                }
                foreach (var item in ListClassProfileTempMonth)
                {
                    sheet.SetCellValue(rankColfilter.ToString() + rankRowFilter, DicClassRank[item.ID]);
                    rankRowFilter++;
                }
            }
            oBook.GetSheet(1).Delete();
            return oBook.ToStream();
        }

        private List<ClassEmulationStartEndDate> GetListWeekByDate(DateTime date)
        {
            List<ClassEmulationStartEndDate> lstStartEndDate = new List<ClassEmulationStartEndDate>();
            var startofweek = date.StartOfWeek();
            ClassEmulationStartEndDate StartEndDateCurrent = new ClassEmulationStartEndDate();
            StartEndDateCurrent.FromDate = startofweek;
            StartEndDateCurrent.ToDate = startofweek.AddDays(6);
            lstStartEndDate.Add(StartEndDateCurrent);
            for (var i = 1; i <= 5; i++)
            {
                if (startofweek.AddDays(-(i * 7)).Month == date.Month)
                {
                    ClassEmulationStartEndDate StartEndDate = new ClassEmulationStartEndDate();
                    StartEndDate.FromDate = startofweek.AddDays(-(i * 7));
                    StartEndDate.ToDate = StartEndDate.FromDate.AddDays(6);
                    lstStartEndDate.Add(StartEndDate);
                }
                if (startofweek.AddDays((i * 7)).Month == date.Month)
                {
                    ClassEmulationStartEndDate StartEndDate1 = new ClassEmulationStartEndDate();
                    StartEndDate1.FromDate = startofweek.AddDays((i * 7));
                    StartEndDate1.ToDate = StartEndDate1.FromDate.AddDays(6);
                    lstStartEndDate.Add(StartEndDate1);
                }
            }
            var results = from dateString in lstStartEndDate
                          orderby (Convert.ToDateTime(dateString.FromDate))
                          select dateString;
            return results.ToList();
        }
        private void copyCol(IVTWorksheet sheetOld, IVTWorksheet sheet, int col, int newPostioncol, int rowHeaderStart, int rowHeaderEnd)
        {
            IVTRange range = sheetOld.GetRange(rowHeaderStart, col, rowHeaderEnd, col);
            sheet.CopyPasteSameRowHeigh(range, rowHeaderStart, newPostioncol);
            startCol++;
        }
        /// <summary>Get the week number of a certain date, provided that
        /// the first day of the week is Monday, the first week of a year
        /// is the one that includes the first Thursday of that year and
        /// the last week of a year is the one that immediately precedes
        /// the first calendar week of the next year.
        /// </summary>
        /// <param name="date">Date of interest.</param>
        /// <returns>The week number.</returns>
        private int GetWeekNumber(DateTime date)
        {
            //Constants
            const int JAN = 1;
            const int DEC = 12;
            const int LASTDAYOFDEC = 31;
            const int FIRSTDAYOFJAN = 1;
            const int THURSDAY = 4;
            bool thursdayFlag = false;

            //Get the day number since the beginning of the year
            int dayOfYear = date.DayOfYear;

            //Get the first and last weekday of the year
            int startWeekDayOfYear = (int)(new DateTime(date.Year, JAN, FIRSTDAYOFJAN)).DayOfWeek;
            int endWeekDayOfYear = (int)(new DateTime(date.Year, DEC, LASTDAYOFDEC)).DayOfWeek;

            //Compensate for using monday as the first day of the week
            if (startWeekDayOfYear == 0)
                startWeekDayOfYear = 7;
            if (endWeekDayOfYear == 0)
                endWeekDayOfYear = 7;

            //Calculate the number of days in the first week
            int daysInFirstWeek = 8 - (startWeekDayOfYear);

            //Year starting and ending on a thursday will have 53 weeks
            if (startWeekDayOfYear == THURSDAY || endWeekDayOfYear == THURSDAY)
                thursdayFlag = true;

            //We begin by calculating the number of FULL weeks between
            //the year start and our date. The number is rounded up so
            //the smallest possible value is 0.
            int fullWeeks = (int)Math.Ceiling((dayOfYear - (daysInFirstWeek)) / 7.0);
            int resultWeekNumber = fullWeeks;

            //If the first week of the year has at least four days, the
            //actual week number for our date can be incremented by one.
            if (daysInFirstWeek >= THURSDAY)
                resultWeekNumber = resultWeekNumber + 1;

            //If the week number is larger than 52 (and the year doesn't
            //start or end on a thursday), the correct week number is 1.
            if (resultWeekNumber > 52 && !thursdayFlag)
                resultWeekNumber = 1;

            //If the week number is still 0, it means that we are trying
            //to evaluate the week number for a week that belongs to the
            //previous year (since it has 3 days or less in this year).
            //We therefore execute this function recursively, using the
            //last day of the previous year.
            if (resultWeekNumber == 0)
                resultWeekNumber = GetWeekNumber(new DateTime(date.Year - 1, DEC, LASTDAYOFDEC));
            return resultWeekNumber;
        }
    }

    /// <summary>
    /// Class dung rieng cho xu ly nghiep vu dac biet cua phan bao cao TUAN-THANG
    /// </summary>
    public class ClassEmulationReportObject
    {
        public int ClassProfileID { get; set; }
        public string DisplayName { get; set; }
        public int? Rank { get; set; }
        public decimal? TotalMark { get; set; }
        public decimal? TotalPenalizedMark { get; set; }
    }
    /// <summary>
    /// Class dung rieng cho xu ly nghiep vu dac biet cua phan bao cao TUAN-THANG
    /// </summary>
    public class ClassEmulationProfileTemp
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? Rank { get; set; }
        public decimal? TotalMark { get; set; }
        public decimal? TotalPenalizedMark { get; set; }
    }
    /// <summary>
    /// Class dung rieng cho xu ly nghiep vu dac biet cua phan bao cao TUAN-THANG
    /// </summary>
    public class ClassEmulationProfileTempMonth
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? Rank { get; set; }
        public decimal? TotalMark { get; set; }
        public decimal? TotalMark1 { get; set; }
        public decimal? TotalMark2 { get; set; }
        public decimal? TotalMark3 { get; set; }
        public decimal? TotalMark4 { get; set; }
        public decimal? TotalMark5 { get; set; }
    }
    /// <summary>
    /// Class dung rieng cho xu ly nghiep vu dac biet cua phan bao cao TUAN-THANG
    /// </summary>
    public class ClassEmulationStartEndDate
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
