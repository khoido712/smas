﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;



namespace SMAS.Business.Business
{ 
    public partial class ApprenticeshipTrainingScheduleBusiness
    {  
		
          #region Validate

        public void Validate(ApprenticeshipTrainingSchedule apprenticeshiptrainingschedule)
        {
            ValidationMetadata.ValidateObject(apprenticeshiptrainingschedule);

            //Tháng phải nằm trong khoảng từ 1 đến 12. Kiểm tra trường Month
            if (apprenticeshiptrainingschedule.Month < 1 || apprenticeshiptrainingschedule.Month > 12)
            {
                throw new BusinessException("Common_Validate_NotMouth");
            }
          //Giá trị buổi học phải nằm trong các giá trị

            if (apprenticeshiptrainingschedule.SectionInDay != 1 && apprenticeshiptrainingschedule.SectionInDay != 2 && apprenticeshiptrainingschedule.SectionInDay != 4)
            {
                throw new BusinessException("Common_Validate_NotSectionInDay");
            }
            //Năm học đã hoàn thành không thể khai báo thời gian học nghề. Kiểm tra AcademicYear nếu đã tồn tại AcademicYear với Year + 1 thì không cho phép tác động đến hệ thống
            /// DB lung tung nên comment lại đã @@
            AcademicYear academicyear = AcademicYearRepository.Find(apprenticeshiptrainingschedule.AcademicYearID);

            List<AcademicYear> academicyearcheck = AcademicYearRepository.All.Where(o => (o.SchoolID == apprenticeshiptrainingschedule.SchoolID )).ToList();
            //foreach (var y in academicyearcheck)
            //{
            //    if (y.Year > academicyear.Year)
            //    {
            //        throw new BusinessException("Common_Validate_EndSchoolYear");
            //    }
            //}

            //Học kỳ phải thuộc 3 giá trị Học kỳ:
            if(apprenticeshiptrainingschedule.Semester != GlobalConstants.SEMESTER_OF_YEAR_FIRST && apprenticeshiptrainingschedule.Semester != GlobalConstants.SEMESTER_OF_YEAR_SECOND && apprenticeshiptrainingschedule!= null)
            {
              throw new BusinessException("Common_Validate_NotSemester");
            }
            //Giá trị thời gian đăng ký không đúng định dạng. Kiểm tra ScheduleDetail bao gồm một chuổi các ký tự 31 chữ số bao gồm 2 ký tự K và C
            
            if(apprenticeshiptrainingschedule.ScheduleDetail.Trim().Length != 31)
            {
              throw new BusinessException("Common_Validate_NotNumberenough");
            }
            
            string scheduledetail = apprenticeshiptrainingschedule.ScheduleDetail;
            for (int i = 0; i < scheduledetail.Length; i++)
            {
                if (scheduledetail.Substring(i, 1) != "K" && scheduledetail.Substring(i, 1) != "C")
            {
               throw new BusinessException("Common_Validate_NotScheduleDetail");
            }
            }
        }
        #endregion

        #region InsertApprenticeshipTrainingSchedule

        public ApprenticeshipTrainingSchedule InsertApprenticeshipTrainingSchedule(ApprenticeshipTrainingSchedule Insert)
        {
            // Validate giữ liệu
            Validate(Insert);
            // xóa bản ghi cũ hơn
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Month"] = Insert.Month;
            SearchInfo["ApprenticeshipClassID"] = Insert.ApprenticeshipClassID;
            SearchInfo["AcademicYearID"] = Insert.AcademicYearID;
            SearchInfo["SchoolID"] = Insert.SchoolID;
            //SearchInfo["SectionInDay"] = Insert.SectionInDay;

            List<ApprenticeshipTrainingSchedule> lstApprenticeshipTrainingSchedule = this.Search(SearchInfo).ToList();
            if (lstApprenticeshipTrainingSchedule.Count > 0)
            {
                if (lstApprenticeshipTrainingSchedule.FirstOrDefault().ApprenticeshipClassID == Insert.ApprenticeshipClassID)
                {
                    this.DeleteApprenticeshipTrainingSchedule(lstApprenticeshipTrainingSchedule);
                }
            }

                    
      
            return base.Insert(Insert);
        }
        
        #endregion

        #region UpdateApprenticeshipTrainingSchedule

        public ApprenticeshipTrainingSchedule UpdateApprenticeshipTrainingSchedule(ApprenticeshipTrainingSchedule Update)
        {
            // Validate giữ liệu
            Validate(Update);

            return base.Update(Update);
        }
        
        #endregion

        #region DeleteApprenticeshipTrainingSchedule

        public void DeleteApprenticeshipTrainingSchedule(List<ApprenticeshipTrainingSchedule> ApprenticeshipTrainingSchedule)
        {
            ApprenticeshipTrainingSchedule ATSDelete = ApprenticeshipTrainingSchedule.FirstOrDefault();
            if (ATSDelete.ApprenticeshipClassID != 0)
            {
                // kiểm tra xem dữ liệu định xóa có tồn tại không
                bool checkDelete = new ApprenticeshipTrainingScheduleRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ApprenticeshipTrainingSchedule",
               new Dictionary<string, object>()
                {
                    {"ApprenticeshipTrainingScheduleID",ATSDelete.ApprenticeshipTrainingScheduleID},
                    {"SchoolID",ATSDelete.SchoolID}
                }, null);
                if (checkDelete)
                {
                    base.Delete(ATSDelete.ApprenticeshipTrainingScheduleID);
                }
            
            }


        }
        
        #endregion

        #region SearchApprenticeshipTrainingSchedule

        private IQueryable<ApprenticeshipTrainingSchedule> Search(IDictionary<string, object> SearchInfo)
        {
            int ApprenticeshipTrainingScheduleID = Utils.GetInt(SearchInfo, "ApprenticeshipTrainingScheduleID");
            int Month = Utils.GetInt(SearchInfo, "Month");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int ApprenticeshipClassID = Utils.GetInt(SearchInfo, "ApprenticeshipClassID");
            DateTime? CreatedDate = Utils.GetDateTime(SearchInfo, "CreatedDate");
            int SectionInDay = Utils.GetInt(SearchInfo, "SectionInDay");
            string ScheduleDetail = Utils.GetString(SearchInfo, "ScheduleDetail");

            IQueryable<ApprenticeshipTrainingSchedule> lsApprenticeshipTrainingSchedule = this.ApprenticeshipTrainingScheduleRepository.All;

            if (ApprenticeshipTrainingScheduleID != 0)
            {
                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.ApprenticeshipTrainingScheduleID == ApprenticeshipTrainingScheduleID));
            }


            if (ApprenticeshipClassID != 0)
            {
                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.ApprenticeshipClassID == ApprenticeshipClassID));
            }

            if (Semester != 0)
            {
                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.Semester == Semester));
            }

            if (Month != 0)
            {
                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.Month == Month));
            }

            if (CreatedDate != null)
            {
                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.CreatedDate == CreatedDate));
            }

            if (ScheduleDetail.Length != 0)
            {

                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.ScheduleDetail == ScheduleDetail));
                 
            }

            if (SectionInDay == 1 || SectionInDay == 2 || SectionInDay == 4)
            {
                lsApprenticeshipTrainingSchedule = lsApprenticeshipTrainingSchedule.Where(o => (o.SectionInDay == SectionInDay));
            }

            return lsApprenticeshipTrainingSchedule;

        }
        
        #endregion

        #region SearchBySchool


        public IQueryable<ApprenticeshipTrainingSchedule> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);

            }

        }


        #endregion

    }
}