﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SendResultBusiness
    {  
		#region Validate
        public void CheckSendResultIDAvailable(int SendResultID)
        {
            bool AvailableSendResultID = this.repository.ExistsRow(GlobalConstants.TRS_SCHEMA, "SendResult",
                   new Dictionary<string, object>()
                {
                    {"SendResultID", SendResultID}
                    
                }, null);
            if (!AvailableSendResultID)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion
        #region Insert
        public SendResult Insert(SendResult sendResult)
        {

            //Check Require, Max Length
            ValidationMetadata.ValidateObject(sendResult);

            return base.Insert(sendResult);

        }
        #endregion
        #region Update
        public SendResult Update(SendResult SendResult)
        {
            //Check Require, Max Length
            ValidationMetadata.ValidateObject(SendResult);

            return base.Update(SendResult);
        }
        #endregion
        #region Delete
        public bool Delete(int SendResultID)
        {
            try
            {
                //Check available
                CheckSendResultIDAvailable(SendResultID);

                //Check Using
                this.CheckConstraints(GlobalConstants.TRS_SCHEMA, "SendResult", SendResultID, "SendResult_Label_SendResultID");

                base.Delete(SendResultID);
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Thông báo
        /// </summary>
        /// <author>BaLX</author>
        /// <date>7/12/2012</date>
        /// <param name="ethnic">Đối tượng SendResult</param>
        /// <returns>Đối tượng SendResult</returns>

        public IQueryable<SendResult> Search(IDictionary<string, object> dic)
        {
            int SendInfoID = Utils.GetInt(dic, "SendInfoID");
            string ServiceCode = Utils.GetString(dic, "ServiceCode");
            string PackageCode = Utils.GetString(dic, "PackageCode");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string ReceiverEmail = Utils.GetString(dic, "ReceiverEmail");
            string ReceiverMobile = Utils.GetString(dic, "ReceiverMobile");
            string Title = Utils.GetString(dic, "Title");

            int? ReceiveType = Utils.GetNullableInt(dic, "ReceiveType");
            int? SchedularType = Utils.GetNullableInt(dic, "SchedularType");
            int? SendType = Utils.GetNullableInt(dic, "SendType");
            int? InfoType = Utils.GetNullableInt(dic, "InfoType");
            int? Retry_num = Utils.GetNullableInt(dic, "Retry_num");

            string create_user = Utils.GetString(dic, "create_user");
            string update_user = Utils.GetString(dic, "update_user");
            DateTime? FromDate = Utils.GetDateTime(dic, "From_Create_time");
            DateTime? ToDate = Utils.GetDateTime(dic, "To_Create_time");

            IQueryable<SendResult> listSendResult = this.SendResultRepository.All;

            //if (SendInfoID != 0)
            //{
            //    listSendResult = listSendResult.Where(x => x.SendInfoID == SendInfoID);
            //}
            //if (!ServiceCode.Equals(string.Empty))
            //{
            //    listSendResult = listSendResult.Where(x => x.ServiceCode.ToLower().Contains(ServiceCode.ToLower()));
            //}
            //if (!PackageCode.Equals(string.Empty))
            //{
            //    listSendResult = listSendResult.Where(x => x.PackageCode.ToLower().Contains(PackageCode.ToLower()));
            //}
            if (SchoolID.HasValue)
            {
                listSendResult = listSendResult.Where(x => x.SchoolID == SchoolID);
            }
            if (!PupilCode.Equals(string.Empty))
            {
                listSendResult = listSendResult.Where(x => x.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            }
            if (!ReceiverEmail.Equals(string.Empty))
            {
                listSendResult = listSendResult.Where(x => x.ReceiverEmail.ToLower().Contains(ReceiverEmail.ToLower()));
            }
            if (!ReceiverMobile.Equals(string.Empty))
            {
                listSendResult = listSendResult.Where(x => x.ReceiverMobile.ToLower().Contains(ReceiverMobile.ToLower()));
            }
            if (!Title.Equals(string.Empty))
            {
                listSendResult = listSendResult.Where(x => x.Title.ToLower().Contains(Title.ToLower()));
            }
            if (InfoType.HasValue)
            {
                listSendResult = listSendResult.Where(x => x.InfoType == InfoType);
            }
            if (ReceiveType.HasValue && ReceiveType != -1)
            {
                listSendResult = listSendResult.Where(x => x.TypeOfReceiver == ReceiveType);
            }
            if (SchedularType.HasValue && SchedularType != -1)
            {
                listSendResult = listSendResult.Where(x => x.SchedularType == SchedularType);
            }
            if (SendType.HasValue && SendType != -1)
            {
                listSendResult = listSendResult.Where(x => x.SendType == SendType);
            }
            //if (Retry_num.HasValue && Retry_num != -1)
            //{
            //    listSendResult = listSendResult.Where(x => x.Retry_num == Retry_num);
            //}
            if (!create_user.Equals(string.Empty))
            {
                listSendResult = listSendResult.Where(x => x.CreateUser.ToLower().Contains(create_user.ToLower()));
            }
            //if (!update_user.Equals(string.Empty))
            //{
            //    listSendResult = listSendResult.Where(x => x.update_user.ToLower().Contains(update_user.ToLower()));
            //}
            if (FromDate.HasValue && ToDate.HasValue)
            {
                listSendResult = listSendResult.Where(x => (x.CreateTime >= FromDate && x.CreateTime <= ToDate));

            }
            if (FromDate.HasValue && !ToDate.HasValue)
            {
                listSendResult = listSendResult.Where(x => (x.CreateTime >= FromDate));

            }
            if (!FromDate.HasValue && ToDate.HasValue)
            {
                listSendResult = listSendResult.Where(x => (x.CreateTime <= ToDate));

            }

            return listSendResult;
        }
        #endregion

    }
}