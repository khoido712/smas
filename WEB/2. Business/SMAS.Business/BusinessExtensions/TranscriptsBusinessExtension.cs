﻿
using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using NPOI.SS.UserModel;

namespace SMAS.Business.Business
{
    public partial class TranscriptsBusiness
    {
        private List<int> AreaPraiseID = new List<int>() { 1, 2, 3 };
        private List<int> AreaPraiseIDTT58 = new List<int>() { 1, 2, 3, 4, 5 };
        private List<int> ListPolicyTargetID = new List<int>() { 25, 37, 38, 42, 49, 50, 51, 52, 26, 43, 44, 45, 56, 54 };
        private int strDotLen = 70;
        private string praiseTitle = " Được giải thưởng trong các kỳ thi từ cấp huyện trở lên:";
        private int YearCst = 2016;
        private string bounesSpecial = "Khen thưởng đặc biệt";
        private string upClass = "Được lên lớp sau khi KT lại môn học hoặc rèn luyện thêm về HK: ";
        private string pupilStayClass = "Ở lại lớp";

        #region Lấy mảng băm
        public string GetHashKey(Transcripts transcripts)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AppliedLevel", transcripts.AppliedLevel},
                {"PupilID ", transcripts.PupilID},
                {"ClassID",transcripts.ClassID},
                {"SchoolID", transcripts.SchoolID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu thông tin học bạ theo mẫu
        public ProcessedReport InsertTranscripts(Transcripts transcripts, Stream data)
        {
            ProcessedReport pr = new ProcessedReport();
            string reportCode = "";
            if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP1;
            }
            if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY || transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
            {
                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23TT58;
            }
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(transcripts);
            pr.ReportData = ReportUtils.Compress(data);
            //ProcessedReportBusiness.Insert(pr);
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AppliedLevel", transcripts.AppliedLevel},
                {"PupilID", transcripts.PupilID},
                {"SchoolID", transcripts.SchoolID}
            };
            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(transcripts.AppliedLevel);
            // Lay ten hoc sinh
            PupilProfile pp = PupilProfileBusiness.Find(transcripts.PupilID);
            string pupilName = pp != null ? Utils.StripVNSign(pp.FullName) : string.Empty;
            outputNamePattern = outputNamePattern
                .Replace("[SchoolLevel]", schoolLevel)
                .Replace("[PupilName]", pupilName);
            if (transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX)
                outputNamePattern = "HS_GDTX_HocBa_" + pupilName;
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportBusiness.Insert(pr);
            ProcessedReportBusiness.Save();
            //ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy học bạ được cập nhật mới nhất
        public ProcessedReport GetTranscripts(Transcripts transcripts, bool isZip = false)
        {
            string reportCode = "";
            if (isZip)
            {
                reportCode = transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX_ZIPFILE : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_ZIPFILE;
            }
            else
            {
                reportCode = transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23TT58;
            }
            string inputParameterHashKey = this.GetHashKeyTranscriptsReport(transcripts);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        public string SplitSchoolName(string schoolName)
        {
            char[] arraySchoolName = schoolName.Trim().ToCharArray();
            string checkSchoolName = "" + arraySchoolName[0] + arraySchoolName[1] + arraySchoolName[2] + arraySchoolName[3] + arraySchoolName[4] + arraySchoolName[5];

            if (checkSchoolName == "TRƯỜNG" || checkSchoolName == "trường" || checkSchoolName == "Trường")
                schoolName = schoolName.Substring(6);

            return schoolName.Trim();
        }

        #region Lưu lại thông tin học bạ theo mẫu
        public Stream CreateTranscripts(Transcripts transcripts)
        {
            List<SMAS.VTUtils.Utils.VTDataImageValidation> lstVTDataImageValidation = new List<SMAS.VTUtils.Utils.VTDataImageValidation>();
            List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
            AcademicYear Aca = AcademicYearBusiness.Find(transcripts.AcademicYearID);
            int modSchoolID = transcripts.SchoolID % 100;
            bool checkCurrentYear = transcripts.currentYear == 1 ? true : false;
            bool checkPaging = transcripts.paging == 1 ? true : false;
            int sheetPosition = checkCurrentYear ? 0 : 2;
            int startPageNumberOfSheet = checkCurrentYear ? 0 : 1;
            //int paging = checkCurrentYear ? 0 : 1;
            //DAnh sach diem nhan xet cua hoc sinh tat ca cac nam
            List<VJudgeRecord> lstjudge = VJudgeRecordBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.Last2digitNumberSchool == modSchoolID).ToList();

            #region Khai báo + lấy thông tin
            string reportCode = "";
            string communeName = "";
            string districtName = "";
            string provinceName = "";

            // Sheet trang bìa
            string grade = ReportUtils.ConvertAppliedLevelToFullReportName(transcripts.AppliedLevel);
            PupilProfile pp = PupilProfileBusiness.Find(transcripts.PupilID);
            string fullName = pp.FullName.ToUpper();
            string storageNumber = !string.IsNullOrEmpty(pp.StorageNumber) ? pp.StorageNumber : "";
            SchoolProfile sp = SchoolProfileBusiness.Find(transcripts.SchoolID);

            string schoolName = SplitSchoolName(sp.SchoolName);

            Commune commune = CommuneBusiness.Find(sp.CommuneID);
            if (commune != null)
            {
                communeName = commune.CommuneName;
            }

            District district = DistrictBusiness.Find(sp.DistrictID);
            if (district != null)
            {
                districtName = district.DistrictName;
            }
            Province province = ProvinceBusiness.Find(sp.ProvinceID);
            if (province != null)
            {
                provinceName = province.ProvinceName;
            }


            // Sheet thông tin chung
            string birthDate = Utils.ConvertDateToString(pp.BirthDate);
            string birthPlace = pp.BirthPlace;
            int genre = pp.Genre;
            string ethnicName = "";
            if (pp.Ethnic != null)
                ethnicName = pp.Ethnic.EthnicName;
            string permanentResidentalAddress = pp.PermanentResidentalAddress;
            string tempResidentalAddress = pp.TempResidentalAddress;
            string fatherFullName = pp.FatherFullName;
            string fatherJob = pp.FatherJob;
            string motherFullName = pp.MotherFullName;
            string motherJob = pp.MotherJob;
            string sponsorFullName = pp.SponsorFullName;
            string sponsorJob = pp.SponsorJob;
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"PupilID", transcripts.PupilID},
                //{"Status", new List<int>(){SystemParamsInFile.PUPIL_STATUS_STUDYING,SystemParamsInFile.PUPIL_STATUS_GRADUATED}}
            };

            //các sheet khối
            List<EducationLevel> listLevel = EducationLevelBusiness.GetByGrade(transcripts.AppliedLevel).ToList();
            // Lay danh sach hoc sinh hoc trong cap hoc
            List<PupilOfClass> lstPupilOfClassNull = PupilOfClassBusiness.Search(dic)
                .Where(o => o.ClassProfile.EducationLevel.Grade == transcripts.AppliedLevel && o.EndDate == null)
                .ToList();
            List<PupilOfClass> lstPupilOfClassInSchool = new List<PupilOfClass>();
            List<PupilOfClass> lstPupilOfClassNotNull = PupilOfClassBusiness.Search(dic)
                .Where(o => o.ClassProfile.EducationLevel.Grade == transcripts.AppliedLevel && o.EndDate != null).ToList();
            //Lay ra thong tin hoc sinh cua truong can xuat hoc ba
            foreach (PupilOfClass poc in lstPupilOfClassNotNull)
            {
                if (poc.SchoolID == transcripts.SchoolID)
                {
                    if (poc.EndDate >= Aca.FirstSemesterEndDate)
                    {
                        lstPupilOfClassInSchool.Add(poc);
                    }
                }
                else
                {
                    if (poc.EndDate <= Aca.SecondSemesterEndDate)
                    {
                        lstPupilOfClassInSchool.Add(poc);
                    }
                }
            }

            //Lay ra khoi hoc lon nhat ma hoc sinh day hoc
            IDictionary<string, object> dicpp = new Dictionary<string, object>();
            dicpp["PupilID"] = transcripts.PupilID;
            int EducationLevelMax = 0;
            //bool EducationLevelCheck = false;
            IQueryable<PupilOfClass> lspoc = PupilOfClassBusiness.SearchBySchool(transcripts.SchoolID, dicpp);
            if (lspoc.Count() > 0)
            {
                EducationLevelMax = lspoc.FirstOrDefault().ClassProfile.EducationLevelID;
            }
            //lspoc = lspoc.Where(o => o.EndDate >= Aca.FirstSemesterEndDate);
            //if (lspoc.Count() > 0)
            //{
            //    EducationLevelCheck = true;
            //}
            //Lọc ra những lớp mà học sinh học ở trường khác
            List<PupilOfClass> listPupilOfClassNull = new List<PupilOfClass>();
            if (EducationLevelMax != 0)
            {
                foreach (PupilOfClass poc in lstPupilOfClassNull)
                {
                    if (poc.SchoolID == transcripts.SchoolID)
                    {
                        listPupilOfClassNull.Add(poc);
                    }
                    else
                    {
                        if (poc.ClassProfile.EducationLevelID < EducationLevelMax)
                        {
                            listPupilOfClassNull.Add(poc);
                        }
                    }
                }
            }

            //Ket hop list
            List<PupilOfClass> listPupilOfClass = listPupilOfClassNull.Union(lstPupilOfClassInSchool).OrderBy(o => o.ClassProfile.EducationLevelID).ThenBy(o => o.AssignedDate).ToList();
            //Sap xep lai list
            List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
            foreach (PupilOfClass poc in listPupilOfClass)
            {
                if (poc.SchoolID != transcripts.SchoolID)
                {
                    lstPupilOfClass.Add(poc);
                }
                else
                {
                    lstPupilOfClass.Add(poc);
                }
            }
            List<int> listClassID = new List<int>();

            //new List<int>();
            List<ClassProfile> listClassProfile = new List<ClassProfile>();
            for (int i = 0; i < listLevel.Count; i++)
            {
                List<PupilOfClass> listClass = lstPupilOfClass.Where(o => o.ClassProfile.EducationLevelID == listLevel[i].EducationLevelID).ToList();

                if (listClass.Count > 0)
                {
                    if (checkCurrentYear)
                    {
                        listClass = listClass.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
                    }

                    if (listClass.Count > 1)
                    {
                        listClass = listClass.Where(o => o.SchoolID == transcripts.SchoolID).ToList();
                    }
                    ClassProfile classProfile = null;
                    if (listClass != null && listClass.Count > 0)
                    {
                        for (int j = 0; j < listClass.Count; j++)
                        {
                            classProfile = new ClassProfile();
                            classProfile = listClass[j].ClassProfile;
                            listClassProfile.Add(classProfile);
                            listClassID.Add(classProfile.ClassProfileID);
                        }
                    }
                }
            }
            IVTWorkbook oBook = null;



            #endregion
            #region Cấp 1
            if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {

                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP1;
                ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;


                //Thao tác với Excel
                oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy Sheet Trang bìa
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Xử lý thông tin trang bìa
                firstSheet.SetCellValue("A20", grade);
                firstSheet.SetCellValue("F26", fullName);
                firstSheet.SetCellValue("F27", schoolName);
                firstSheet.SetCellValue("F28", communeName);
                firstSheet.SetCellValue("F29", districtName);
                firstSheet.SetCellValue("F30", provinceName);

                IVTWorksheet secondSheet = oBook.GetSheet(2);
                //Xử lý trang thông tin chung

                secondSheet.SetCellValue("D10", fullName);
                secondSheet.SetCellValue("D11", birthDate);
                secondSheet.SetCellValue("H11", birthPlace);
                secondSheet.SetCellValue("L10", Utils.Genre(genre));
                secondSheet.SetCellValue("K12", ethnicName);
                secondSheet.SetCellValue("D14", permanentResidentalAddress);
                secondSheet.SetCellValue("D13", tempResidentalAddress);
                secondSheet.SetCellValue("D15", fatherFullName);
                secondSheet.SetCellValue("J15", fatherJob);
                secondSheet.SetCellValue("D16", motherFullName);
                secondSheet.SetCellValue("J16", motherJob);
                secondSheet.SetCellValue("F17", sponsorFullName);
                secondSheet.SetCellValue("K17", sponsorJob);
                for (int ii = 0; ii < lstPupilOfClass.Count; ii++)
                {
                    secondSheet.SetCellValue(28 + ii, 1, lstPupilOfClass[ii].AcademicYear.DisplayTitle);
                    secondSheet.SetCellValue(28 + ii, 3, lstPupilOfClass[ii].ClassProfile.DisplayName);
                    secondSheet.SetCellValue(28 + ii, 4, lstPupilOfClass[ii].SchoolProfile.SchoolName);
                }

                // Lay toan bo thong tin diem cua hoc sinh nay
                var listSchoolAca = listClassProfile.Select(o => new { o.SchoolID, o.AcademicYearID, CreatedYear = o.AcademicYear.Year }).Distinct().ToList();

                List<SummedUpRecordBO> listSummedUpRecord = new List<SummedUpRecordBO>();
                List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
                foreach (var sa in listSchoolAca)
                {
                    listSummedUpRecord.AddRange((from p in VSummedUpRecordBusiness.All.Where(o =>
                                                o.Last2digitNumberSchool == sa.SchoolID % 100 && o.CreatedAcademicYear == sa.CreatedYear
                                                 && o.PupilID == transcripts.PupilID && o.PeriodID == null)
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where q.IsActive == true
                                                 select new SummedUpRecordBO
                                                 {
                                                     SummedUpRecordID = p.SummedUpRecordID,
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     SchoolID = p.SchoolID,
                                                     AcademicYearID = p.AcademicYearID,
                                                     PeriodID = p.PeriodID,
                                                     SubjectID = p.SubjectID,
                                                     JudgementResult = p.JudgementResult,
                                                     SummedUpMark = p.SummedUpMark,
                                                     ReTestMark = p.ReTestMark,
                                                     ReTestJudgement = p.ReTestJudgement,
                                                     Semester = p.Semester
                                                 }).ToList());
                    // Danh sach mon hoc

                    // Lay thong tin tong ket cua hoc sinh
                    listPupilRanking.AddRange((from p in VPupilRankingBusiness.All.Where(o =>
                        o.Last2digitNumberSchool == sa.SchoolID % 100 && o.CreatedAcademicYear == sa.CreatedYear
                        && o.PupilID == transcripts.PupilID && o.PeriodID == null)
                                               join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                               join r in CapacityLevelBusiness.All on p.CapacityLevelID equals r.CapacityLevelID into g1
                                               from j1 in g1.DefaultIfEmpty()
                                               join s in ConductLevelBusiness.All on p.ConductLevelID equals s.ConductLevelID into g2
                                               from j2 in g2.DefaultIfEmpty()
                                               join l in StudyingJudgementBusiness.All on p.StudyingJudgementID equals l.StudyingJudgementID into g3
                                               from j3 in g3.DefaultIfEmpty()
                                               where q.IsActive == true
                                               select new PupilRankingBO
                                               {
                                                   PupilRankingID = p.PupilRankingID,
                                                   PupilID = p.PupilID,
                                                   ClassID = p.ClassID,
                                                   Semester = p.Semester,
                                                   CapacityLevelID = p.CapacityLevelID,
                                                   ConductLevelID = p.ConductLevelID,
                                                   ConductLevelName = j2.Resolution,
                                                   CapacityLevel = j1.CapacityLevel1,
                                                   StudyingJudgementID = p.StudyingJudgementID,
                                                   StudyingJudgementResolution = j3.Resolution
                                               }).ToList());
                }
                // Lay thong tin khen thuong cua hoc sinh
                List<PupilPraise> listPupilPraise = PupilPraiseBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.PupilProfile.IsActive == true && o.IsRecordedInSchoolReport == true).ToList();

                //Xử lý các sheet khối
                IVTWorksheet tempSheet = oBook.GetSheet(3);
                for (int i = 0; i < listClassID.Count; i++)
                {

                    IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet);
                    if (listClassID[i] != 0)
                    {
                        int ClassID = listClassID[i];
                        List<ClassSubject> classSubject = ClassSubjectBusiness.All.Where(o => o.ClassID == ClassID && o.Last2digitNumberSchool == modSchoolID).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
                        //Đặt lại tên sheet
                        ClassProfile classProfile = listClassProfile[i];
                        //Danh sach diem mon nhan xet
                        List<VJudgeRecord> lstjud = lstjudge.Where(o => o.ClassID == ClassID && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == classProfile.SchoolID).ToList();
                        //Danh sach diem mon tinh diem
                        List<SummedUpRecordBO> lstSumUp = listSummedUpRecord.Where(o => o.ClassID == ClassID && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == classProfile.SchoolID).ToList();

                        sheet.Name = classProfile.DisplayName;
                        // Xuat ra thong tin nam hoc va truong
                        sheet.SetCellValue("A2", "Năm học: " + classProfile.AcademicYear.DisplayTitle);
                        sheet.SetCellValue("G2", classProfile.SchoolProfile.SchoolName);

                        // xử lý Sheet                        
                        List<ClassSubject> classSubjectNormal = classSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY).ToList();
                        List<ClassSubject> classSubjectOptional = classSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY || o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK).ToList();
                        List<ClassSubject> classSubjectEnglish2 = classSubject.Where(o => o.IsSecondForeignLanguage == true).ToList();
                        List<ClassSubject> classSubjectApprenticeship = classSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_APPRENTICESHIP).ToList();
                        PupilOfClass classmove = lstPupilOfClass.Where(o => o.ClassID == ClassID).FirstOrDefault();
                        AcademicYear acatemp = AcademicYearBusiness.Find(classmove.AcademicYearID);
                        int Semester = 0;
                        if (classmove != null && classmove.EndDate != null)
                        {
                            if (classmove.EndDate > acatemp.FirstSemesterEndDate)
                            {
                                Semester = 2;
                            }
                            else
                            {
                                Semester = 1;
                            }
                        }

                        List<SummedUpRecordBO> summedUpRecord1 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord2 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord3 = lstSumUp
                            .Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .ToList();
                        if (Semester == 1)
                        {
                            summedUpRecord1 = new List<SummedUpRecordBO>();
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                        }
                        if (Semester == 2)
                        {
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                        }
                        // ClassSubject Normal
                        int subNormalID;
                        int subOptionalID;
                        int subEnglishID;
                        int subApprenticeID;
                        int startRow = 6;
                        int rowCopy = 6;
                        for (int j = 0; j < classSubjectNormal.Count; j++)
                        {
                            if (startRow + j > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + j, true);
                            }
                            sheet.SetCellValue(startRow + j, 1, classSubjectNormal[j].SubjectCat.DisplayName.ToUpper());
                            //Môn tính điểm
                            subNormalID = classSubjectNormal[j].SubjectID;
                            if (classSubjectNormal[j].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                            {
                                SummedUpRecordBO sum1 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum2 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum3 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum1 != null && sum1.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum1.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                    sheet.SetCellValue(startRow + j, 5, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark1));
                                }
                                if (sum2 != null && sum2.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum2.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                    sheet.SetCellValue(startRow + j, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark2));
                                }
                                if (sum3 != null)
                                {
                                    if (sum3.ReTestMark != null)
                                    {
                                        decimal summedUpMark3 = sum3.ReTestMark.Value;
                                        sheet.SetCellValue(startRow + j, 7, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                        sheet.SetCellValue(startRow + j, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark3));
                                    }
                                }
                            }
                            //Môn nhận xét
                            if (classSubjectNormal[j].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                SummedUpRecordBO sum4 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum5 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum6 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                //Diem nhan xet
                                List<VJudgeRecord> listJud = lstjud.Where(o => o.SubjectID == subNormalID && !o.Judgement.Contains(SystemParamsInFile.JUDGE_MARK_CD) && o.Judgement != null && o.Judgement != "").ToList();

                                //Ki 1
                                int jud4 = listJud.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Count();
                                //Ca nam
                                int jud5 = listJud.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Count();
                                //thi lai
                                //int jud6 = listJud.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL).Count();



                                if (sum4 != null)
                                {
                                    string judgementResult1 = sum4.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 3, jud4);
                                    sheet.SetCellValue(startRow + j, 5, judgementResult1);
                                }
                                if (sum5 != null)
                                {
                                    string judgementResult2 = sum5.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 6, jud5);
                                    sheet.SetCellValue(startRow + j, 8, judgementResult2);
                                }
                                if (sum6 != null)
                                {
                                    if (sum6.ReTestJudgement != null)
                                    {
                                        string judgementResult3 = sum6.ReTestJudgement;
                                        sheet.SetCellValue(startRow + j, 7, judgementResult3);
                                        sheet.SetCellValue(startRow + j, 8, judgementResult3);
                                    }
                                }
                            }
                        }
                        startRow += classSubjectNormal.Count;

                        // Môn tiếng anh 2

                        for (int n = 0; n < classSubjectEnglish2.Count; n++)
                        {
                            if (startRow + n > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + n, true);
                            }
                            sheet.SetCellValue(startRow + n, 1, classSubjectEnglish2[n].SubjectCat.DisplayName.ToUpper());
                            subEnglishID = classSubjectEnglish2[n].SubjectID;
                            if (classSubjectEnglish2[n].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                            {
                                SummedUpRecordBO sum13 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum14 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum15 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum13 != null && sum13.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum13.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                    sheet.SetCellValue(startRow + n, 5, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark1));
                                }
                                if (sum14 != null && sum14.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum14.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                    sheet.SetCellValue(startRow + n, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark2));
                                }
                                if (sum15 != null)
                                {
                                    if (sum15.ReTestMark != null)
                                    {
                                        decimal summedUpMark3 = sum15.ReTestMark.Value;
                                        sheet.SetCellValue(startRow + n, 7, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                        sheet.SetCellValue(startRow + n, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark3));
                                    }
                                }
                            }
                            if (classSubjectEnglish2[n].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            {
                                SummedUpRecordBO sum16 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum17 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum18 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum16 != null)
                                {
                                    string judgementResult1 = sum16.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 3, judgementResult1);
                                    sheet.SetCellValue(startRow + n, 5, judgementResult1);
                                }
                                if (sum17 != null)
                                {
                                    string judgementResult2 = sum17.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 6, judgementResult2);
                                    sheet.SetCellValue(startRow + n, 8, judgementResult2);
                                }
                                if (sum18 != null)
                                {
                                    if (sum18.ReTestJudgement != null)
                                    {
                                        string judgementResult3 = sum18.ReTestJudgement;
                                        sheet.SetCellValue(startRow + n, 7, judgementResult3);
                                        sheet.SetCellValue(startRow + n, 8, judgementResult3);
                                    }
                                }
                            }
                        }
                        startRow += classSubjectEnglish2.Count;
                        // Môn nghề

                        for (int m = 0; m < classSubjectApprenticeship.Count; m++)
                        {
                            if (startRow + m > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + m, true);
                            }
                            sheet.SetCellValue(startRow + m, 1, classSubjectApprenticeship[m].SubjectCat.DisplayName.ToUpper());
                            subApprenticeID = classSubjectApprenticeship[m].SubjectID;
                            if (classSubjectEnglish2[m].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum19 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum20 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum21 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum19 != null && sum19.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum19.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                    sheet.SetCellValue(startRow + m, 5, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark1));
                                }
                                if (sum20 != null && sum20.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum20.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                    sheet.SetCellValue(startRow + m, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark2));
                                }
                                if (sum21 != null)
                                {
                                    if (sum21.ReTestMark != null)
                                    {
                                        decimal summedUpMark3 = sum21.ReTestMark.Value;
                                        sheet.SetCellValue(startRow + m, 7, summedUpMark3);
                                        sheet.SetCellValue(startRow + m, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark3));
                                    }
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum22 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum23 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum24 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum22 != null)
                                {
                                    string judgementResult1 = sum22.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 3, judgementResult1);
                                    sheet.SetCellValue(startRow + m, 5, judgementResult1);
                                }
                                if (sum23 != null)
                                {
                                    string judgementResult2 = sum23.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 6, judgementResult2);
                                    sheet.SetCellValue(startRow + m, 8, judgementResult2);
                                }
                                if (sum24 != null)
                                {
                                    if (sum24.ReTestJudgement != null)
                                    {
                                        string judgementResult3 = sum24.ReTestJudgement;
                                        sheet.SetCellValue(startRow + m, 7, judgementResult3);
                                        sheet.SetCellValue(startRow + m, 8, judgementResult3);
                                    }
                                }
                            }
                        }
                        startRow += classSubjectApprenticeship.Count;
                        // Class Subject Optional

                        for (int k = 0; k < classSubjectOptional.Count; k++)
                        {
                            if (k > 0)
                            {
                                sheet.CopyAndInsertARow(startRow, startRow + k, true);
                            }
                            sheet.SetCellValue(startRow + k, 2, classSubjectOptional[k].SubjectCat.DisplayName.ToUpper());
                            //Môn tính điểm
                            subOptionalID = classSubjectOptional[k].SubjectID;
                            if (classSubjectOptional[k].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum7 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum8 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum9 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum7 != null && sum7.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum7.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                    sheet.SetCellValue(startRow + k, 5, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark1));
                                }
                                if (sum8 != null && sum8.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum8.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                    sheet.SetCellValue(startRow + k, 8, getCapacitybyMark(transcripts.AppliedLevel, summedUpMark2));
                                }
                                if (sum9 != null)
                                {
                                    if (sum9.ReTestMark != null)
                                    {
                                        decimal summedUpMark3 = sum9.ReTestMark.Value;
                                        sheet.SetCellValue(startRow + k, 7, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                    }
                                }
                            }
                            else //Môn nhận xét
                            {
                                // Tannd
                                var jud10 = lstjud.Where(p => p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && p.SubjectID == subOptionalID && p.Judgement == "Đ").ToList();
                                var jud11 = lstjud.Where(p => (p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) && p.SubjectID == subOptionalID && p.Judgement == "Đ").ToList();
                                var jud12 = lstjud.Where(p => p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && p.SubjectID == subOptionalID && p.Judgement == "Đ").ToList();
                                SummedUpRecordBO sum10 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum11 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum12 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();

                                sheet.SetCellValue(startRow + k, 3, jud10.Count); // Tong so diem dat trong hoc ky 1
                                if (sum10 != null)
                                {
                                    sheet.SetCellValue(startRow + k, 5, sum10.JudgementResult); // Ket qua danh gia, xep loai
                                }
                                sheet.SetCellValue(startRow + k, 6, jud11.Count); // Tong so diem dat trong ca nam
                                if (sum11 != null)
                                {
                                    sheet.SetCellValue(startRow + k, 8, sum11.JudgementResult); // Ket qua danh gia, xep loai
                                }
                                if (sum12 != null)
                                {
                                    if (sum12.ReTestJudgement != null)
                                    {
                                        string judgementResult3 = sum12.ReTestJudgement;
                                        sheet.SetCellValue(startRow + k, 7, judgementResult3);
                                        sheet.SetCellValue(startRow + k, 8, judgementResult3);
                                    }
                                }
                            }
                        }
                        if (classSubjectOptional.Count > 1)
                        {
                            sheet.MergeColumn('A', startRow, startRow + classSubjectOptional.Count - 1);
                            startRow += classSubjectOptional.Count - 1;
                        }
                        // Thong tin tong ket
                        // Thong tin hoc sinh
                        sheet.FitAllColumnsOnOnePage = true;
                        sheet.SetBreakPage(startRow + 7);
                        sheet.PageMaginLeft = 0.5;
                        sheet.PageMaginRight = 0.2;
                        sheet.PageMaginTop = 0.2;
                        sheet.PageMaginBottom = 0.2;
                        sheet.PageSize = VTXPageSize.VTxlPaperA4;
                        sheet.SetCellValue(startRow + 8, 1, "LỚP: " + classProfile.DisplayName.Replace("lớp", "").Replace("Lớp", "").Replace("LỚP", ""));
                        sheet.SetCellValue(startRow + 8, 6, "HỌ VÀ TÊN HỌC SINH: " + pp.FullName.ToUpper());

                        PupilRankingBO pr3 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                        PupilRankingBO pr4 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).FirstOrDefault();
                        PupilRankingBO pr5 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING).FirstOrDefault();
                        // Hanh kiem
                        if (pr5 != null)
                        {
                            sheet.SetCellValue(startRow + 12, 1, pr5.ConductLevelID != null ? pr5.ConductLevelName : string.Empty);
                            sheet.SetCellValue(startRow + 16, 1, pr5.CapacityLevelID != null ? pr5.CapacityLevel : string.Empty);
                            sheet.SetCellValue(startRow + 20, 1, pr5.CapacityLevelID != null ? pr5.CapacityLevel : string.Empty);
                            sheet.SetCellValue(startRow + 26, 1, pr5.StudyingJudgementID != null ? pr5.StudyingJudgementResolution : string.Empty);
                        }
                        else
                        {
                            if (pr4 != null)
                            {
                                sheet.SetCellValue(startRow + 12, 1, pr4.ConductLevelID != null ? pr4.ConductLevelName : string.Empty);
                                sheet.SetCellValue(startRow + 16, 1, pr4.CapacityLevelID != null ? pr4.CapacityLevel : string.Empty);
                                sheet.SetCellValue(startRow + 20, 1, pr4.CapacityLevelID != null ? pr4.CapacityLevel : string.Empty);
                                sheet.SetCellValue(startRow + 26, 1, pr4.StudyingJudgementID != null ? pr4.StudyingJudgementResolution : string.Empty);
                            }
                            else
                            {
                                if (pr3 != null)
                                {
                                    sheet.SetCellValue(startRow + 12, 1, pr3.ConductLevelID != null ? pr3.ConductLevelName : string.Empty);
                                    sheet.SetCellValue(startRow + 16, 1, pr3.CapacityLevelID != null ? pr3.CapacityLevel : string.Empty);
                                    sheet.SetCellValue(startRow + 20, 1, pr3.CapacityLevelID != null ? pr3.CapacityLevel : string.Empty);
                                    sheet.SetCellValue(startRow + 26, 1, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                                }
                            }
                        }
                        // Khen thuong
                        List<PupilPraise> lstpraise = listPupilPraise.Where(o => o.ClassID == listClassID[i] && o.IsRecordedInSchoolReport == true).ToList();
                        if (lstpraise.Count > 0)
                        {
                            string ppraise = "";
                            foreach (PupilPraise ppri in lstpraise)
                            {
                                ppraise += ppri.Description + " , ";


                            }
                            sheet.SetCellValue(startRow + 23, 1, ppraise);
                        }

                    }

                }

                tempSheet.Delete();
            }
            #endregion

            #region Cấp 2-3 // Theo TT 58
            if (transcripts.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX && (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY || transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY))
            {
                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23TT58;
                ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
                //Thao tác với Excel
                oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy Sheet Trang bìa
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                // Lấy Sheet Thông tin chung
                IVTWorksheet secondSheet = oBook.GetSheet(2);
                // Lấy Sheet lớp học
                IVTWorksheet thirdSheet = oBook.GetSheet(3);
                // Lấy Sheet nhận xét
                IVTWorksheet commentSheet = oBook.GetSheet(4);

                #region // Fill thông tin sheet trang bìa
                firstSheet.SetCellValue("A16", grade);
                firstSheet.SetCellValue("A25", fullName);
                if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY)
                {
                    firstSheet.SetCellValue("A33", string.Format("SỐ: {0}/THCS", !string.IsNullOrEmpty(storageNumber) ? storageNumber : ".........."));
                }
                if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
                {
                    firstSheet.SetCellValue("A33", string.Format("SỐ: {0}/THPT", !string.IsNullOrEmpty(storageNumber) ? storageNumber : ".........."));
                }
                #endregion

                #region // Thông tin chung
                string target = "";
                if (pp.PolicyTargetID.HasValue)
                {
                    if (ListPolicyTargetID.Contains(pp.PolicyTargetID.Value))
                        target = pp.PolicyTarget.Resolution;
                }
                string valueCellA13 = string.Empty;
                valueCellA13 = "Dân tộc:                [EthnicName], con liệt sĩ, con thương binh (bệnh binh, người được hưởng chế độ như thương binh, gia đình có công với cách mạng ): [PolicyTarget]";
                valueCellA13 = valueCellA13.Replace("[EthnicName]", ethnicName);
                valueCellA13 = valueCellA13.Replace("[PolicyTarget]", target);

                secondSheet.SetCellValue("B8", grade);
                secondSheet.SetCellValue("B10", fullName);
                secondSheet.SetCellValue("G10", Utils.Genre(genre));
                secondSheet.SetCellValue("B11", string.Format("{0} tháng {1} năm {2}", pp.BirthDate.Day, pp.BirthDate.Month, pp.BirthDate.Year));
                secondSheet.SetCellValue("B12", birthPlace);
                secondSheet.SetCellValue("A13", valueCellA13);
                secondSheet.SetCellValue("B15", !string.IsNullOrEmpty(tempResidentalAddress) ? tempResidentalAddress : permanentResidentalAddress);
                secondSheet.SetCellValue("B16", fatherFullName);
                secondSheet.SetCellValue("G16", fatherJob);
                secondSheet.SetCellValue("B17", motherFullName);
                secondSheet.SetCellValue("G17", motherJob);
                secondSheet.SetCellValue("C18", sponsorFullName);
                secondSheet.SetCellValue("G18", sponsorJob);
                secondSheet.SetCellValue("D20", districtName + ", ngày " + pp.EnrolmentDate.Day + " tháng " + pp.EnrolmentDate.Month + " năm " + pp.EnrolmentDate.Year);
                secondSheet.SetCellValue("D25", sp.HeadMasterName);

                // Quá trình học tập
                for (int ii = 0; ii < lstPupilOfClass.Count; ii++)
                {
                    secondSheet.SetCellValue(29 + ii, 1, lstPupilOfClass[ii].AcademicYear.Year.ToString() + "-" + (lstPupilOfClass[ii].AcademicYear.Year + 1).ToString());
                    secondSheet.SetCellValue(29 + ii, 2, lstPupilOfClass[ii].ClassProfile.DisplayName);
                    if (lstPupilOfClass[ii].SchoolProfile.Commune != null)
                    {
                        string Name = SplitSchoolName(lstPupilOfClass[ii].SchoolProfile.SchoolName);
                        secondSheet.SetCellValue(29 + ii, 3, Name + " - " + lstPupilOfClass[ii].SchoolProfile.Commune.CommuneName + " - " + lstPupilOfClass[ii].SchoolProfile.District.DistrictName + " - " + lstPupilOfClass[ii].SchoolProfile.Province.ProvinceName);
                    }
                    else
                    {
                        string Name = SplitSchoolName(lstPupilOfClass[ii].SchoolProfile.SchoolName);
                        secondSheet.SetCellValue(29 + ii, 3, Name + " - " + lstPupilOfClass[ii].SchoolProfile.District.DistrictName + " - " + lstPupilOfClass[ii].SchoolProfile.Province.ProvinceName);
                    }
                }
                if (checkPaging && !checkCurrentYear)
                {
                    lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                    {
                        SheetIndex = sheetPosition,
                        IsHeaderOrFooter = true,
                        AlignPageNumber = 2,
                        StartPageNumberOfSheet = startPageNumberOfSheet,
                    });
                }
                #endregion

                #region // GetData
                // Lay toan bo thong tin diem cua hoc sinh nay
                var listSchoolAca = listClassProfile.Select(o => new { o.SchoolID, o.AcademicYearID, CreatedYear = o.AcademicYear.Year }).Distinct().ToList();

                List<SummedUpRecordBO> listSummedUpRecord = new List<SummedUpRecordBO>();
                List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
                foreach (var sa in listSchoolAca)
                {
                    listSummedUpRecord.AddRange((from p in VSummedUpRecordBusiness.All.Where(o =>
                                                o.Last2digitNumberSchool == sa.SchoolID % 100 && o.CreatedAcademicYear == sa.CreatedYear
                                                 && o.PupilID == transcripts.PupilID && o.PeriodID == null)
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where q.IsActive == true
                                                 select new SummedUpRecordBO
                                                 {
                                                     SummedUpRecordID = p.SummedUpRecordID,
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     SchoolID = p.SchoolID,
                                                     AcademicYearID = p.AcademicYearID,
                                                     PeriodID = p.PeriodID,
                                                     SubjectID = p.SubjectID,
                                                     JudgementResult = p.JudgementResult,
                                                     SummedUpMark = p.SummedUpMark,
                                                     ReTestMark = p.ReTestMark,
                                                     ReTestJudgement = p.ReTestJudgement,
                                                     Semester = p.Semester
                                                 }).ToList());
                    // Danh sach mon hoc

                    // Lay thong tin tong ket cua hoc sinh
                    listPupilRanking.AddRange((from p in VPupilRankingBusiness.All.Where(o =>
                        o.Last2digitNumberSchool == sa.SchoolID % 100 && o.CreatedAcademicYear == sa.CreatedYear
                        && o.PupilID == transcripts.PupilID && o.PeriodID == null)
                                               join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                               join r in CapacityLevelBusiness.All on p.CapacityLevelID equals r.CapacityLevelID into g1
                                               from j1 in g1.DefaultIfEmpty()
                                               join s in ConductLevelBusiness.All on p.ConductLevelID equals s.ConductLevelID into g2
                                               from j2 in g2.DefaultIfEmpty()
                                               join l in StudyingJudgementBusiness.All on p.StudyingJudgementID equals l.StudyingJudgementID into g3
                                               from j3 in g3.DefaultIfEmpty()
                                               where q.IsActive == true
                                               select new PupilRankingBO
                                               {
                                                   PupilRankingID = p.PupilRankingID,
                                                   PupilID = p.PupilID,
                                                   ClassID = p.ClassID,
                                                   Semester = p.Semester,
                                                   AverageMark = p.AverageMark,
                                                   CapacityLevelID = p.CapacityLevelID,
                                                   ConductLevelID = p.ConductLevelID,
                                                   ConductLevelName = j2.Resolution,
                                                   CapacityLevel = j1.CapacityLevel1,
                                                   StudyingJudgementID = p.StudyingJudgementID,
                                                   StudyingJudgementResolution = j3.Resolution,
                                                   Comment = p.PupilRankingComment
                                               }).ToList());
                }

                // Danh sach mon hoc
                List<ClassSubject> classSubject = ClassSubjectBusiness.All
                    .Where(o => listClassID.Contains(o.ClassID) && o.Last2digitNumberSchool == modSchoolID).Distinct()
                    .OrderBy(o => o.SubjectCat.OrderInSubject)
                    .ThenBy(o => o.SubjectCat.DisplayName)
                    .ToList();
                //Chiendd: 25.11.2015 Bo sung danh sach giao vien BM
                ClassSubject objClassSubject;
                Dictionary<string, object> dicTeaching = new Dictionary<string, object>();
                IQueryable<TeachingAssignment> iqTeachingAssignmen = TeachingAssignmentBusiness.SearchBySchool(transcripts.SchoolID, dicTeaching);

                Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
                dicEmployee.Add("SchoolID", transcripts.SchoolID);
                //dicEmployee.Add("CurrentEmployeeStatus", GlobalConstants.EMPLOYMENT_STATUS_WORKING);
                IQueryable<Employee> IqEmployee = EmployeeBusiness.Search(dicEmployee);
                List<TeachingAssignmentBO> lstTeachingBo = (from tc in iqTeachingAssignmen
                                                            join e in IqEmployee on tc.TeacherID equals e.EmployeeID
                                                            where listClassID.Contains(tc.ClassID.Value)
                                                            select new TeachingAssignmentBO
                                                            {
                                                                EmployeeCode = e.EmployeeCode,
                                                                Semester = tc.Semester,
                                                                TeacherID = e.EmployeeID,
                                                                TeacherName = e.FullName,
                                                                SubjectID = tc.SubjectID,
                                                                ClassID = tc.ClassID
                                                            }).ToList();

                // Lay thong tin khen thuong, ky luat cua hoc sinh
                List<PupilPraise> listPupilPraise = PupilPraiseBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.PupilProfile.IsActive == true && o.IsRecordedInSchoolReport == true)
                .Where(o => AreaPraiseIDTT58.Contains(o.PraiseLevel))
                .ToList();

                //Danh sach cac buoi nghi
                List<PupilAbsence> pupilAbsenceList = PupilAbsenceBusiness.All.Where(x => x.PupilID == transcripts.PupilID).ToList();
                #endregion

                int currentSemesterId = 1;
                //lay ten giao vien chu nhiem
                // int headTeacherId  = 0; 
                int ClassID = 0;
                ClassProfile classProfile = null;
                double defaultAreaSubjectHeight = 306;
                List<string> lstSpecializedSubject = new List<string>();
                startPageNumberOfSheet++;
                for (int i = 0; i < listClassID.Count; i++)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(thirdSheet);
                    IVTWorksheet fourSheet = oBook.CopySheetToLast(commentSheet);
                    IVTRange rangComment = commentSheet.GetRange("A2", "J39");
                    fourSheet.CopyPasteSameSize(rangComment, 2, 1);

                    if (listClassID[i] != 0)
                    {
                        ClassID = listClassID[i];
                        //Đặt lại tên sheet
                        classProfile = listClassProfile[i];
                        //Danh sach diem mon tinh diem
                        AcademicYear academic = AcademicYearBusiness.Find(classProfile.AcademicYearID);
                        if (academic.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academic.FirstSemesterEndDate)
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        }
                        else
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        }

                        //Fill ten giao vien chu nhiem
                        //headTeacherId = listClassProfile.Where(p => p.ClassProfileID == ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                        // Employee employeeObj = EmployeeBusiness.Find(headTeacherId);
                        // sheet.SetCellValue("B17", employeeObj != null ? employeeObj.FullName : string.Empty);

                        List<SummedUpRecordBO> lstSumUp = listSummedUpRecord.Where(o => o.ClassID == ClassID && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == classProfile.SchoolID).ToList();
                        sheet.Name = classProfile.DisplayName;
                        // Xuat ra thong tin nam hoc va truong
                        sheet.SetCellValue("A2", "Họ và tên: " + pp.FullName);
                        sheet.SetCellValue("F2", "Lớp: " + classProfile.DisplayName);
                        sheet.SetCellValue("J2", "Năm học: " + classProfile.AcademicYear.DisplayTitle);
                        sheet.SetCellValue("A3", string.Format("Ban: {0}", classProfile.SubCommitteeID.HasValue ? classProfile.SubCommittee.Resolution : ""));

                        // xử lý Sheet        
                        List<ClassSubject> lstClassSubject = classSubject.Where(o => o.ClassID == ClassID).ToList();
                        List<ClassSubject> classSubjectNormal = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY).ToList();
                        List<ClassSubject> classSubjectOptional = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY
                            || o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK || o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_PRIORITIZE).ToList();
                        List<ClassSubject> classSubjectEnglish2 = lstClassSubject.Where(o => o.IsSecondForeignLanguage == true).ToList();
                        List<ClassSubject> classSubjectApprenticeship = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_APPRENTICESHIP).ToList();

                        //Neu o lop nay thang hoc sinh nay chuyen lop\

                        PupilOfClass classmove = lstPupilOfClass.Where(o => o.ClassID == ClassID).FirstOrDefault();
                        AcademicYear acatemp = AcademicYearBusiness.Find(classmove.AcademicYearID);
                        int Semester = 0;
                        if (classmove != null && classmove.EndDate != null)
                        {
                            if (classmove.EndDate > acatemp.FirstSemesterEndDate)
                            {
                                Semester = 2;
                            }
                            else
                            {
                                Semester = 1;
                            }
                        }
                        // Kiem tra học sinh
                        List<SummedUpRecordBO> summedUpRecord1 = lstSumUp
                            .Where(o => o.ClassID == ClassID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            .ToList();

                        List<SummedUpRecordBO> summedUpRecord2 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord3 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord4 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .ToList();
                        if (Semester == 1)
                        {
                            summedUpRecord1 = new List<SummedUpRecordBO>();
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        if (Semester == 2)
                        {
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        // ClassSubject Normal
                        int subNormalID;
                        int subOptionalID;
                        int subEnglishID;
                        int subApprenticeID;
                        int startRow = 6;
                        int rowCopy = 6;
                        //int totalSubject = 0; // Chỉ đảm bảo 15 môn, vượt quá 15 môn thì bỏ những môn vượt quá 15
                        #region // Duyệt môn học
                        for (int j = 0; j < classSubjectNormal.Count; j++)
                        {
                            if (startRow > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow, true);
                            }
                            if (classSubjectNormal[j].IsSpecializedSubject.HasValue && classSubjectNormal[j].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectNormal[j].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue((startRow), 1, classSubjectNormal[j].SubjectCat.DisplayName);
                            subNormalID = classSubjectNormal[j].SubjectID;

                            #region
                            //Môn tính điểm
                            if (classSubjectNormal[j].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum1 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum2 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum3 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum4 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum1 != null && sum1.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum1.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum2 != null && sum2.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum2.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum4 != null && sum4.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum4.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum3 != null && sum3.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum3.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else  //Môn nhận xét
                            {
                                SummedUpRecordBO sum5 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum6 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum7 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum8 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum5 != null)
                                {
                                    string judgementResult1 = sum5.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum6 != null)
                                {
                                    string judgementResult2 = sum6.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum8 != null)
                                {
                                    string judgementResult4 = sum8.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum7 != null)
                                {
                                    string judgementResult3 = sum7.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectNormal[j];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                            #endregion
                        }
                        #endregion
                        //startRow += classSubjectNormal.Count;
                        #region // Môn tiếng anh 2
                        for (int n = 0; n < classSubjectEnglish2.Count; n++)
                        {
                            if (startRow > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow, true);
                            }
                            if (classSubjectEnglish2[n].IsSpecializedSubject.HasValue && classSubjectEnglish2[n].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectEnglish2[n].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue(startRow, 1, classSubjectEnglish2[n].SubjectCat.DisplayName);
                            subEnglishID = classSubjectEnglish2[n].SubjectID;

                            if (classSubjectEnglish2[n].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum17 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum18 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum19 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum20 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum17 != null && sum17.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum17.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum18 != null && sum18.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum18.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum20 != null && sum20.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum20.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum19 != null && sum19.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum19.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum21 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum22 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum23 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum24 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum21 != null)
                                {
                                    string judgementResult1 = sum21.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum22 != null)
                                {
                                    string judgementResult2 = sum22.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum24 != null)
                                {
                                    string judgementResult4 = sum24.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum23 != null)
                                {
                                    string judgementResult3 = sum23.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectEnglish2[n];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion
                        // startRow += classSubjectEnglish2.Count;
                        #region // Môn nghề
                        for (int m = 0; m < classSubjectApprenticeship.Count; m++)
                        {
                            if (startRow > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow, true);
                            }
                            if (classSubjectApprenticeship[m].IsSpecializedSubject.HasValue && classSubjectApprenticeship[m].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectApprenticeship[m].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue(startRow, 1, classSubjectApprenticeship[m].SubjectCat.DisplayName);
                            subApprenticeID = classSubjectApprenticeship[m].SubjectID;
                            if (classSubjectApprenticeship[m].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum25 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum26 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum27 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum28 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum25 != null && sum25.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum25.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum26 != null && sum26.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum26.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum28 != null && sum28.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum28.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum27 != null && sum27.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum27.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum29 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum30 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum31 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum32 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum29 != null)
                                {
                                    string judgementResult1 = sum29.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum30 != null)
                                {
                                    string judgementResult2 = sum30.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum32 != null)
                                {
                                    string judgementResult4 = sum32.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum31 != null)
                                {
                                    string judgementResult3 = sum31.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectApprenticeship[m];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion
                        // startRow += classSubjectApprenticeship.Count;
                        #region // Class Subject Optional
                        int startRowDefault = startRow;
                        for (int k = 0; k < classSubjectOptional.Count; k++)
                        {
                            if (k > 0)
                            {
                                sheet.CopyAndInsertARow(startRowDefault, startRow, true);
                            }
                            if (classSubjectOptional[k].IsSpecializedSubject.HasValue && classSubjectOptional[k].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectOptional[k].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue(startRow, 2, classSubjectOptional[k].SubjectCat.DisplayName);
                            subOptionalID = classSubjectOptional[k].SubjectID;
                            //Môn tính điểm
                            if (classSubjectOptional[k].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum9 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum10 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum11 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum12 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum9 != null && sum9.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum9.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum10 != null && sum10.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum10.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum12 != null && sum12.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum12.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum11 != null && sum11.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum11.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else //Môn nhận xét
                            {
                                SummedUpRecordBO sum13 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum14 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum15 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum16 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum13 != null)
                                {
                                    string judgementResult1 = sum13.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum14 != null)
                                {
                                    string judgementResult2 = sum14.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum16 != null)
                                {
                                    string judgementResult4 = sum16.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum15 != null)
                                {
                                    string judgementResult3 = sum15.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectOptional[k];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion

                        if (classSubjectOptional.Count() > 1)
                        {
                            sheet.MergeColumn('A', (startRowDefault + classSubjectOptional.Count() - 1), startRowDefault);
                        }
                        else
                        {
                            // Neu ko he co du lieu thi xoa dong 5
                            if (startRow == rowCopy)
                            {
                                sheet.DeleteRow(startRow);
                            }
                            // Giu nguyen dong tu chon
                            if (classSubjectOptional.Count() == 0)
                            {
                                startRow++;
                            }
                        }

                        if (lstSpecializedSubject.Count() > 0)
                        {
                            string _specializedSubject = string.Join(", ", lstSpecializedSubject.ToArray());
                            sheet.SetCellValue("E3", "Các môn học nâng cao: " + _specializedSubject);
                        }

                        // Chiều cao mỗi dòng trong khu vực môn học                     
                        double rowHeight = (defaultAreaSubjectHeight / (startRow - 6));
                        for (int row = 6; row < startRow; row++)
                        {
                            sheet.SetRowHeight(row, rowHeight);
                        }

                        #region // Thong tin tong ket
                        PupilRankingBO pr1 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                        PupilRankingBO pr2 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                        int checkstudy = 0;
                        PupilRankingBO pr3 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                        if (pr3 != null && pr3.StudyingJudgementID.HasValue)
                        {
                            checkstudy = pr3.StudyingJudgementID.Value;
                        }
                        //Thi lai -> Fill vao hoc luc
                        PupilRankingBO pr4 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).FirstOrDefault();
                        //Ren luyen lai -> Fill vao hanh kiem
                        PupilRankingBO pr5 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).FirstOrDefault();
                        if (Semester == 1)
                        {
                            pr1 = null;
                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        if (Semester == 2)
                        {

                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }

                        //Fill thong tin diem TB cac mon sau khi thi lai
                        if (pr4 != null)
                        {
                            if (pr4.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr4.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }
                        if (pr5 != null)
                        {
                            if (pr5.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr5.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow, 6, pr5.AverageMark);
                        }
                        #endregion

                        SchoolProfile spe = SchoolProfileBusiness.Find(classProfile.SchoolID);

                        //fill ten giao vien vao sheet nhan xet truoc khi copy                        
                        int headTeacherId = classProfile.HeadTeacherID.HasValue ? classProfile.HeadTeacherID.Value : 0;
                        Employee employeeObj = null;
                        if (headTeacherId > 0)
                        {
                            employeeObj = EmployeeBusiness.Find(headTeacherId);
                            fourSheet.SetCellValue("A25", employeeObj != null ? employeeObj.FullName : string.Empty);
                        }

                        #region
                        fourSheet.SetCellValue("F32", districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        fourSheet.SetCellValue("F38", spe.HeadMasterName);

                        int startRowTBM = startRow;
                        int startRowFourSheet = 4;

                        sheet.SetCellValue(startRowTBM + 11, 1, employeeObj != null ? employeeObj.FullName : string.Empty);
                        sheet.SetCellValue(startRowTBM + 11, 6, sp != null ? sp.HeadMasterName : string.Empty);

                        #region //Thong tin truong + tinh thanh + hoc luc/hanh kiem
                        fourSheet.SetCellValue(startRowFourSheet - 1, 2, schoolName);
                        fourSheet.SetCellValue(startRowFourSheet - 1, 5, ("Huyện: " + spe.District.DistrictName));
                        fourSheet.SetCellValue(startRowFourSheet - 1, 8, "Tỉnh (TP): " + spe.Province.ProvinceName);
                        if (pr1 != null)
                        {
                            if (pr1.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr1.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 3, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            fourSheet.SetCellValue(startRowFourSheet + 3, 4, pr1.CapacityLevelID != null ? pr1.CapacityLevel : "");
                            fourSheet.SetCellValue(startRowFourSheet + 3, 3, pr1.ConductLevelID != null ? pr1.ConductLevelName : string.Empty);
                        }
                        if (pr2 != null)
                        {
                            if (pr2.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr2.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 4, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            fourSheet.SetCellValue(startRowFourSheet + 4, 4, pr2.CapacityLevelID != null ? pr2.CapacityLevel : "");
                            fourSheet.SetCellValue(startRowFourSheet + 4, 3, pr2.ConductLevelID != null ? pr2.ConductLevelName : string.Empty);
                        }
                        if (pr3 != null)
                        {
                            if (pr3.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr3.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 5, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            fourSheet.SetCellValue(startRowFourSheet + 5, 4, pr3.CapacityLevelID != null ? pr3.CapacityLevel : "");
                            fourSheet.SetCellValue(startRowFourSheet + 5, 3, pr3.ConductLevelID != null ? pr3.ConductLevelName : string.Empty);
                        }
                        #endregion

                        #region // So ngay nghi
                        List<int> lstSectionKeyID = new List<int>();
                        lstSectionKeyID = classProfile.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(classProfile.SeperateKey.Value) : new List<int>();

                        List<PupilAbsence> totalAbsence = pupilAbsenceList.Where(p => lstSectionKeyID.Contains(p.Section) && p.ClassID == ClassID
                                                                                        && p.AcademicYearID == classProfile.AcademicYearID
                                                                                        && p.SchoolID == classProfile.SchoolID).ToList();
                        fourSheet.SetCellValue(startRowFourSheet + 3, 5, totalAbsence.Count());
                        #endregion

                        #region // Xet len lop hay o lai
                        if (pr3 != null)
                        {
                            // lên lớp
                            if (pr3.StudyingJudgementID.HasValue && (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS))
                            {
                                fourSheet.SetCellValue(startRowFourSheet + 1, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                            }
                            else if (pr3.StudyingJudgementID.HasValue && (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS))
                            { // ở lại lớp
                                fourSheet.SetCellValue(startRowFourSheet + 5, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                            }
                            else
                            {
                                if (pr5 != null)
                                {
                                    if (pr5.StudyingJudgementID != null)
                                    {
                                        if (pr5.StudyingJudgementResolution.Contains(pupilStayClass))
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 5, 8, pr5.StudyingJudgementResolution);
                                        }
                                        else
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 2, 8, pr5.StudyingJudgementID != null ? (upClass + pr5.StudyingJudgementResolution) : upClass + "…………….");
                                        }
                                    }

                                }
                                else if (pr4 != null)
                                {
                                    if (pr4.StudyingJudgementID != null)
                                    {
                                        if (pr4.StudyingJudgementResolution.Contains(pupilStayClass))
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 5, 8, pr4.StudyingJudgementResolution);
                                        }
                                        else
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 2, 8, pr4.StudyingJudgementID != null ? (upClass + pr4.StudyingJudgementResolution) : upClass + "…………….");
                                        }
                                    }
                                }
                            }
                        }
                        if (pr4 != null)
                        {
                            fourSheet.SetCellValue(startRowFourSheet + 3, 7, pr4.CapacityLevelID != null ? pr4.CapacityLevel : "");
                        }
                        if (pr5 != null)
                        {
                            fourSheet.SetCellValue(startRowFourSheet + 3, 6, pr5.ConductLevelID != null ? pr5.ConductLevelName : string.Empty);
                        }
                        #endregion

                        #region // Lay thong tin khen thuong khac tu cap huyen tro len
                        // Khen thuong dac biet -> tat ca cac cap
                        string _strBounes = string.Empty;
                        List<string> lstBounesSpecial = new List<string>();
                        List<string> lstBounesNormal = new List<string>();
                        List<PupilPraise> listPupilPraiseClass = listPupilPraise.Where(o => o.ClassID == listClassID[i]).ToList();
                        if (listPupilPraiseClass != null && listPupilPraiseClass.Count > 0)
                        {
                            string praiseType = string.Empty;
                            for (int k = 0; k < listPupilPraiseClass.Count(); k++)
                            {
                                praiseType = listPupilPraiseClass[k].PraiseType.Resolution;
                                if (bounesSpecial.Contains(praiseType))
                                {
                                    lstBounesSpecial.Add(listPupilPraiseClass[k].Description);
                                }
                                else
                                {
                                    if (AreaPraiseID.Contains(listPupilPraiseClass[k].PraiseLevel))
                                    {
                                        lstBounesNormal.Add(listPupilPraiseClass[k].Description);
                                    }
                                }
                            }

                            if (lstBounesNormal.Count() > 0)
                            {
                                _strBounes = "'- Được giải thưởng trong các kỳ thi từ cấp huyện trở lên: " + string.Join(", ", lstBounesNormal.ToArray());
                                fourSheet.SetCellValue(startRowFourSheet + 7, 1, _strBounes);
                            }

                            if (lstBounesSpecial.Count() > 0)
                            {
                                _strBounes = "'- Khen thưởng đặc biệt khác: " + string.Join(", ", lstBounesSpecial.ToArray());
                                fourSheet.SetCellValue(startRowFourSheet + 9, 1, _strBounes);
                            }
                        }
                        #endregion

                        fourSheet.SetCellValue(startRowFourSheet + 14, 1, (pr2 != null && !string.IsNullOrEmpty(pr2.Comment) ? "Nhận xét: " + pr2.Comment : ""));

                        IVTRange rang = fourSheet.GetRange("A2", "J40");
                        sheet.CopyPasteSameSize(rang, (startRowTBM + 13), 1);
                        sheet.GetRange((startRowTBM + 12), 1, (startRowTBM + 12), 10).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                        #endregion

                        int rowLast = (startRowTBM + 12) + 39;
                        sheet.FitAllColumnsOnOnePage = true;
                        sheet.SetBreakPage((startRowTBM + 14));
                        sheet.PageMaginBottom = 0.5;
                        sheet.PrintArea = "$A$1:$J$" + rowLast;
                        fourSheet.Delete();

                        if (checkPaging)
                        {
                            sheetPosition++;
                            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                            {
                                SheetIndex = sheetPosition,
                                IsHeaderOrFooter = true,
                                AlignPageNumber = 2,
                                StartPageNumberOfSheet = startPageNumberOfSheet,
                            });
                            startPageNumberOfSheet += 2;
                        }
                    }
                    //sheet.PageSize = VTXPageSize.VTxlPaperA4;
                    lstSpecializedSubject = new List<string>();
                }

                if (checkCurrentYear)
                {
                    firstSheet.Delete();
                    secondSheet.Delete();
                }
                commentSheet.Delete();
                thirdSheet.Delete();

                if (pp.Image != null && pp.Image.Length > 0 && !checkCurrentYear)
                {
                    lstVTDataImageValidation.Add(new SMAS.VTUtils.Utils.VTDataImageValidation
                    {
                        SheetIndex = 2,
                        UpperLeftRow = 3,
                        UpperLeftColumn = 1,
                        LowerRightColumn = 1,
                        LowerRightRow = 8,
                        TypeFormat = 3,
                        WidthScale = 12,
                        HeightScale = 22,
                        ByteImage = pp.Image
                    });
                }
                else
                {
                    secondSheet.GetRange(3, 1, 8, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
            }
            #endregion

            #region Cấp 2-3 GDTX
            if (transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX && (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY || transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY))
            {
                reportCode = SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX;
                ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
                //Thao tác với Excel
                oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy Sheet Trang bìa
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                grade = "GDTX CẤP " + grade;
                //Xử lý thông tin trang bìa
                firstSheet.SetCellValue("A16", grade);
                firstSheet.SetCellValue("D24", fullName);
                firstSheet.SetCellValue("D25", schoolName);
                firstSheet.SetCellValue("C26", districtName);
                firstSheet.SetCellValue("H26", provinceName);
                if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY)
                {
                    firstSheet.SetCellValue("A36", (!string.IsNullOrEmpty(storageNumber) ? storageNumber + "/GDTX cấp THCS" : "SỐ:.............../GDTX cấp THCS"));
                }
                else if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
                {
                    firstSheet.SetCellValue("A36", (!string.IsNullOrEmpty(storageNumber) ? storageNumber + "/GDTX cấp THPT" : "SỐ................/GDTX cấp THPT"));
                }
                IVTWorksheet secondSheet = oBook.GetSheet(2);
                IVTWorksheet fourSheet = oBook.GetSheet(4);
                //Xử lý trang thông tin chung

                secondSheet.SetCellValue("C8", grade);
                secondSheet.SetCellValue("C10", fullName);
                secondSheet.SetCellValue("H10", Utils.Genre(genre));
                secondSheet.SetCellValue("C11", birthDate);
                secondSheet.SetCellValue("G11", birthPlace);
                secondSheet.SetCellValue("C12", ethnicName);
                secondSheet.SetCellValue("C14", !string.IsNullOrEmpty(tempResidentalAddress) ? tempResidentalAddress : permanentResidentalAddress);
                secondSheet.SetCellValue("C15", fatherFullName);
                secondSheet.SetCellValue("H15", fatherJob);
                secondSheet.SetCellValue("C16", motherFullName);
                secondSheet.SetCellValue("H16", motherJob);
                secondSheet.SetCellValue("E19", districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                secondSheet.SetCellValue("E24", sp != null ? sp.HeadMasterName : "");
                for (int ii = 0; ii < lstPupilOfClass.Count; ii++)
                {
                    secondSheet.SetCellValue(29 + ii, 1, lstPupilOfClass[ii].ClassProfile.DisplayName);
                    secondSheet.SetCellValue(29 + ii, 2, lstPupilOfClass[ii].AcademicYear.FirstSemesterStartDate.Value.ToString("dd/MM/yyyy"));
                    secondSheet.SetCellValue(29 + ii, 4, lstPupilOfClass[ii].AcademicYear.SecondSemesterEndDate.Value.ToString("dd/MM/yyyy"));
                    secondSheet.SetCellValue(29 + ii, 6, SplitSchoolName(lstPupilOfClass[ii].SchoolProfile.SchoolName));
                    secondSheet.SetCellValue(29 + ii, 10, storageNumber.Replace("SỐ:", ""));
                }

                if (pp.Image == null || pp.Image.Length == 0)
                {
                    secondSheet.GetRange(3, 1, 8, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }

                // Lay toan bo thong tin diem cua hoc sinh nay
                var listSchoolAca = listClassProfile.Select(o => new { o.SchoolID, o.AcademicYearID, CreatedYear = o.AcademicYear.Year }).Distinct().ToList();

                List<SummedUpRecordBO> listSummedUpRecord = new List<SummedUpRecordBO>();
                List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
                foreach (var sa in listSchoolAca)
                {
                    listSummedUpRecord.AddRange((from p in VSummedUpRecordBusiness.All.Where(o =>
                                                o.Last2digitNumberSchool == sa.SchoolID % 100 && o.CreatedAcademicYear == sa.CreatedYear
                                                 && o.PupilID == transcripts.PupilID && o.PeriodID == null)
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where q.IsActive == true
                                                 select new SummedUpRecordBO
                                                 {
                                                     SummedUpRecordID = p.SummedUpRecordID,
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     SchoolID = p.SchoolID,
                                                     AcademicYearID = p.AcademicYearID,
                                                     PeriodID = p.PeriodID,
                                                     SubjectID = p.SubjectID,
                                                     JudgementResult = p.JudgementResult,
                                                     SummedUpMark = p.SummedUpMark,
                                                     ReTestMark = p.ReTestMark,
                                                     ReTestJudgement = p.ReTestJudgement,
                                                     Semester = p.Semester
                                                 }).ToList());
                    // Danh sach mon hoc

                    // Lay thong tin tong ket cua hoc sinh
                    listPupilRanking.AddRange((from p in VPupilRankingBusiness.All.Where(o =>
                        o.Last2digitNumberSchool == sa.SchoolID % 100 && o.CreatedAcademicYear == sa.CreatedYear
                        && o.PupilID == transcripts.PupilID && o.PeriodID == null)
                                               join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                               join r in CapacityLevelBusiness.All on p.CapacityLevelID equals r.CapacityLevelID into g1
                                               from j1 in g1.DefaultIfEmpty()
                                               join s in ConductLevelBusiness.All on p.ConductLevelID equals s.ConductLevelID into g2
                                               from j2 in g2.DefaultIfEmpty()
                                               join l in StudyingJudgementBusiness.All on p.StudyingJudgementID equals l.StudyingJudgementID into g3
                                               from j3 in g3.DefaultIfEmpty()
                                               where q.IsActive == true
                                               select new PupilRankingBO
                                               {
                                                   PupilRankingID = p.PupilRankingID,
                                                   PupilID = p.PupilID,
                                                   ClassID = p.ClassID,
                                                   Semester = p.Semester,
                                                   AverageMark = p.AverageMark,
                                                   CapacityLevelID = p.CapacityLevelID,
                                                   ConductLevelID = p.ConductLevelID,
                                                   ConductLevelName = j2.Resolution,
                                                   CapacityLevel = j1.CapacityLevel1,
                                                   StudyingJudgementID = p.StudyingJudgementID,
                                                   StudyingJudgementResolution = j3.Resolution
                                               }).ToList());
                }

                // Danh sach mon hoc
                List<ClassSubject> classSubject = ClassSubjectBusiness.All
                    .Where(o => listClassID.Contains(o.ClassID) && o.Last2digitNumberSchool == modSchoolID).Distinct()
                    .OrderBy(o => o.SubjectCat.OrderInSubject)
                    .ThenBy(o => o.SubjectCat.DisplayName)
                    .ToList();
                //Chiendd: 25.11.2015 Bo sung danh sach giao vien BM
                ClassSubject objClassSubject;
                Dictionary<string, object> dicTeaching = new Dictionary<string, object>();
                IQueryable<TeachingAssignment> iqTeachingAssignmen = TeachingAssignmentBusiness.SearchBySchool(transcripts.SchoolID, dicTeaching);

                Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
                dicEmployee.Add("SchoolID", transcripts.SchoolID);
                dicEmployee.Add("CurrentEmployeeStatus", GlobalConstants.EMPLOYMENT_STATUS_WORKING);
                IQueryable<Employee> IqEmployee = EmployeeBusiness.Search(dicEmployee);
                List<TeachingAssignmentBO> lstTeachingBo = (from tc in iqTeachingAssignmen
                                                            join e in IqEmployee on tc.TeacherID equals e.EmployeeID
                                                            where listClassID.Contains(tc.ClassID.Value)
                                                            select new TeachingAssignmentBO
                                                            {
                                                                EmployeeCode = e.EmployeeCode,
                                                                Semester = tc.Semester,
                                                                TeacherID = e.EmployeeID,
                                                                TeacherName = e.FullName,
                                                                SubjectID = tc.SubjectID,
                                                                ClassID = tc.ClassID
                                                            }).ToList();

                // Lay thong tin khen thuong, ky luat cua hoc sinh
                List<PupilPraise> listPupilPraise = PupilPraiseBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.PupilProfile.IsActive == true && o.IsRecordedInSchoolReport == true)
                .Where(o => AreaPraiseID.Contains(o.PraiseLevel))
                .ToList();

                //Xử lý các sheet khối
                IVTWorksheet tempSheet = oBook.GetSheet(3);
                IVTRange nomalSubjectRange = tempSheet.GetRange("A8", "J8");
                IVTRange optionSubjectRange = tempSheet.GetRange("A9", "J9");
                int currentSemesterId = 1;

                //lay ten giao vien chu nhiem
                // int headTeacherId  = 0; 
                for (int i = 0; i < listClassID.Count; i++)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet);

                    if (listClassID[i] != 0)
                    {
                        int ClassID = listClassID[i];

                        //Đặt lại tên sheet
                        ClassProfile classProfile = listClassProfile[i];
                        //Danh sach diem mon tinh diem
                        AcademicYear academic = AcademicYearBusiness.Find(classProfile.AcademicYearID);
                        if (academic.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academic.FirstSemesterEndDate)
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        }
                        else
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        }

                        //Fill ten giao vien chu nhiem
                        //headTeacherId = listClassProfile.Where(p => p.ClassProfileID == ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                        // Employee employeeObj = EmployeeBusiness.Find(headTeacherId);
                        // sheet.SetCellValue("B17", employeeObj != null ? employeeObj.FullName : string.Empty);

                        List<SummedUpRecordBO> lstSumUp = listSummedUpRecord.Where(o => o.ClassID == ClassID && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == classProfile.SchoolID).ToList();
                        sheet.Name = classProfile.DisplayName;
                        // Xuat ra thong tin nam hoc va truong
                        sheet.SetCellValue("C2", pp.FullName);
                        sheet.SetCellValue("H2", classProfile.DisplayName);

                        sheet.SetCellValue("C3", schoolName);
                        sheet.SetCellValue("C4", districtName);
                        sheet.SetCellValue("H4", provinceName);

                        // xử lý Sheet        
                        List<ClassSubject> lstClassSubject = classSubject.Where(o => o.ClassID == ClassID).ToList();
                        List<ClassSubject> classSubjectNormal = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY).ToList();
                        List<ClassSubject> classSubjectOptional = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY || o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK).ToList();
                        List<ClassSubject> classSubjectEnglish2 = lstClassSubject.Where(o => o.IsSecondForeignLanguage == true).ToList();
                        List<ClassSubject> classSubjectApprenticeship = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_APPRENTICESHIP).ToList();

                        //Neu o lop nay thang hoc sinh nay chuyen lop\

                        PupilOfClass classmove = lstPupilOfClass.Where(o => o.ClassID == ClassID).FirstOrDefault();
                        AcademicYear acatemp = AcademicYearBusiness.Find(classmove.AcademicYearID);
                        int Semester = 0;
                        if (classmove != null && classmove.EndDate != null)
                        {
                            if (classmove.EndDate > acatemp.FirstSemesterEndDate)
                            {
                                Semester = 2;
                            }
                            else
                            {
                                Semester = 1;
                            }
                        }
                        // Kiem tra học sinh
                        List<SummedUpRecordBO> summedUpRecord1 = lstSumUp
                            .Where(o => o.ClassID == ClassID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            .ToList();

                        List<SummedUpRecordBO> summedUpRecord2 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord3 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord4 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .ToList();
                        if (Semester == 1)
                        {
                            summedUpRecord1 = new List<SummedUpRecordBO>();
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        if (Semester == 2)
                        {
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        // ClassSubject Normal
                        int subNormalID;
                        int subOptionalID;
                        int subEnglishID;
                        int subApprenticeID;
                        int startRow = 8;
                        int rowCopy = 8;
                        int countToHidden = 0;
                        for (int j = 0; j < classSubjectNormal.Count; j++)
                        {

                            countToHidden++;
                            if (startRow + j > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + j, true);
                            }
                            sheet.SetCellValue(startRow + j, 1, classSubjectNormal[j].SubjectCat.DisplayName);
                            //Môn tính điểm
                            subNormalID = classSubjectNormal[j].SubjectID;
                            if (classSubjectNormal[j].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum1 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum2 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum3 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum4 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum1 != null && sum1.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum1.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum2 != null && sum2.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum2.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum4 != null && sum4.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum4.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum3 != null && sum3.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum3.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + j, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else  //Môn nhận xét
                            {
                                SummedUpRecordBO sum5 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum6 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum7 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum8 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum5 != null)
                                {
                                    string judgementResult1 = sum5.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 3, judgementResult1);
                                }
                                if (sum6 != null)
                                {
                                    string judgementResult2 = sum6.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 4, judgementResult2);
                                }
                                if (sum8 != null)
                                {
                                    string judgementResult4 = sum8.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 5, judgementResult4);
                                }
                                if (sum7 != null)
                                {
                                    string judgementResult3 = sum7.ReTestJudgement;
                                    sheet.SetCellValue(startRow + j, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectNormal[j];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + j, 7, sheet, currentSemesterId);
                        }
                        startRow += classSubjectNormal.Count;
                        // Môn tiếng anh 2

                        for (int n = 0; n < classSubjectEnglish2.Count; n++)
                        {
                            countToHidden++;
                            if (startRow + n > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + n, true);
                            }
                            sheet.SetCellValue(startRow + n, 1, classSubjectEnglish2[n].SubjectCat.DisplayName);
                            subEnglishID = classSubjectEnglish2[n].SubjectID;

                            if (classSubjectEnglish2[n].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum17 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum18 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum19 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum20 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum17 != null && sum17.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum17.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum18 != null && sum18.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum18.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum20 != null && sum20.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum20.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum19 != null && sum19.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum19.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + n, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum21 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum22 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum23 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum24 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum21 != null)
                                {
                                    string judgementResult1 = sum21.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 3, judgementResult1);
                                }
                                if (sum22 != null)
                                {
                                    string judgementResult2 = sum22.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 4, judgementResult2);
                                }
                                if (sum24 != null)
                                {
                                    string judgementResult4 = sum24.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 5, judgementResult4);
                                }
                                if (sum23 != null)
                                {
                                    string judgementResult3 = sum23.ReTestJudgement;
                                    sheet.SetCellValue(startRow + n, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectEnglish2[n];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + n, 7, sheet, currentSemesterId);
                        }
                        startRow += classSubjectEnglish2.Count;
                        // Môn nghề

                        for (int m = 0; m < classSubjectApprenticeship.Count; m++)
                        {
                            countToHidden++;
                            if (startRow + m > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + m, true);
                            }
                            sheet.SetCellValue(startRow + m, 1, classSubjectApprenticeship[m].SubjectCat.DisplayName);
                            subApprenticeID = classSubjectApprenticeship[m].SubjectID;
                            if (classSubjectApprenticeship[m].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum25 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum26 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum27 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum28 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum25 != null && sum25.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum25.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum26 != null && sum26.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum26.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum28 != null && sum28.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum28.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum27 != null && sum27.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum27.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + m, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum29 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum30 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum31 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum32 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum29 != null)
                                {
                                    string judgementResult1 = sum29.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 3, judgementResult1);
                                }
                                if (sum30 != null)
                                {
                                    string judgementResult2 = sum30.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 4, judgementResult2);
                                }
                                if (sum32 != null)
                                {
                                    string judgementResult4 = sum32.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 5, judgementResult4);
                                }
                                if (sum31 != null)
                                {
                                    string judgementResult3 = sum31.ReTestJudgement;
                                    sheet.SetCellValue(startRow + m, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectApprenticeship[m];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + m, 7, sheet, currentSemesterId);
                        }

                        startRow += classSubjectApprenticeship.Count;

                        // Class Subject Optional

                        for (int k = 0; k < classSubjectOptional.Count; k++)
                        {
                            var classSubjectN = classSubjectOptional[k];
                            countToHidden++;
                            if (k > 0)
                            {
                                sheet.CopyAndInsertARow(startRow, startRow + k, true);
                            }
                            int row = startRow + k;
                            sheet.SetCellValue(row, 1, classSubjectN.SubjectCat.DisplayName);
                            subOptionalID = classSubjectN.SubjectID;
                            //Môn tính điểm
                            if (classSubjectN.IsCommenting != 1)
                            {
                                SummedUpRecordBO sum9 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum10 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum11 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum12 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum9 != null && sum9.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum9.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum10 != null && sum10.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum10.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum12 != null && sum12.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum12.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum11 != null && sum11.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum11.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + k, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else //Môn nhận xét
                            {
                                SummedUpRecordBO sum13 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum14 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum15 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum16 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum13 != null)
                                {
                                    string judgementResult1 = sum13.JudgementResult;
                                    sheet.SetCellValue(startRow + k, 3, judgementResult1);
                                }
                                if (sum14 != null)
                                {
                                    string judgementResult2 = sum14.JudgementResult;
                                    sheet.SetCellValue(startRow + k, 4, judgementResult2);
                                }
                                if (sum16 != null)
                                {
                                    string judgementResult4 = sum16.JudgementResult;
                                    sheet.SetCellValue(startRow + k, 5, judgementResult4);
                                }
                                if (sum15 != null)
                                {
                                    string judgementResult3 = sum15.ReTestJudgement;
                                    sheet.SetCellValue(startRow + k, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectOptional[k];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + k, 7, sheet, currentSemesterId);
                        }
                        if (classSubjectOptional.Count > 1)
                        {
                            //sheet.MergeColumn('A', startRow, startRow + classSubjectOptional.Count - 1);
                            startRow += classSubjectOptional.Count;
                        }
                        else
                        {
                            // Neu ko he co du lieu thi xoa dong 5
                            if (startRow == rowCopy)
                            {
                                sheet.DeleteRow(startRow);
                            }
                            // Giu nguyen dong tu chon
                            startRow += 1;
                        }

                        // Thong tin tong ket
                        PupilRankingBO pr1 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                        PupilRankingBO pr2 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                        int checkstudy = 0;
                        string hornor = "";
                        PupilRankingBO pr3 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                        if (pr3 != null)
                        {
                            checkstudy = pr3.StudyingJudgementID.HasValue ? pr3.StudyingJudgementID.Value : 0;
                        }

                        //Thi lai -> Fill vao hoc luc
                        PupilRankingBO pr4 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).FirstOrDefault();
                        //Ren luyen lai -> Fill vao hanh kiem
                        PupilRankingBO pr5 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).FirstOrDefault();
                        if (Semester == 1)
                        {
                            pr1 = null;
                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        if (Semester == 2)
                        {

                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        SchoolProfile spe = SchoolProfileBusiness.Find(classProfile.SchoolID);

                        //tutv4

                        //fill ten giao vien va sheet nhan xet truoc khi copy                        
                        int headTeacherId = classProfile.HeadTeacherID.HasValue ? classProfile.HeadTeacherID.Value : 0;
                        Employee employeeObj = null;
                        if (headTeacherId > 0)
                        {
                            employeeObj = EmployeeBusiness.Find(headTeacherId);
                            fourSheet.SetCellValue("G28", employeeObj != null ? employeeObj.FullName : string.Empty);
                        }
                        fourSheet.SetCellValue("F39", districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        fourSheet.SetCellValue("F47", sp != null ? sp.HeadMasterName : "");
                        IVTRange rang = fourSheet.GetRange("A2", "J49");
                        sheet.CopyPasteSameSize(rang, 54, 1);
                        int startRowTBM = startRow;
                        sheet.PrintArea = "$A$1:$J$101";
                        //Fill thong tin diem TB cac mon sau khi thi lai
                        if (pr4 != null)
                        {
                            if (pr4.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr4.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }
                        if (pr5 != null)
                        {
                            if (pr5.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr5.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }

                        startRow = 22;

                        //Thong tin truong + tinh thanh
                        sheet.SetCellValue(startRow + 32, 3, pp.FullName);
                        sheet.SetCellValue(startRow + 33, 3, classProfile.DisplayName);
                        sheet.SetCellValue(startRow + 32, 8, classProfile.AcademicYear.FirstSemesterStartDate.Value.ToString("dd/MM/yyyy"));
                        sheet.SetCellValue(startRow + 33, 8, classProfile.AcademicYear.SecondSemesterEndDate.Value.ToString("dd/MM/yyyy"));
                        if (pr1 != null)
                        {
                            if (pr1.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr1.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 3, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow + 38, 2, pr1.CapacityLevelID != null ? pr1.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 38, 3, pr1.ConductLevelID != null ? pr1.ConductLevelName : string.Empty);
                        }
                        if (pr2 != null)
                        {
                            if (pr2.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr2.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 4, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow + 39, 2, pr2.CapacityLevelID != null ? pr2.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 39, 3, pr2.ConductLevelID != null ? pr2.ConductLevelName : string.Empty);
                        }
                        if (pr3 != null)
                        {
                            if (pr3.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr3.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 5, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow + 40, 2, pr3.CapacityLevelID != null ? pr3.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 40, 3, pr3.ConductLevelID != null ? pr3.ConductLevelName : string.Empty);
                        }

                        //fill ten giao vien vao sheet thong tin lop
                        sheet.MergeRow(startRowTBM + 11, 2, 3);
                        sheet.GetRange(startRowTBM + 11, 2, startRowTBM + 11, 3).SetHAlign(VTHAlign.xlHAlignCenter);

                        // So ngay nghi
                        List<int> lstSectionKeyID = new List<int>();
                        lstSectionKeyID = classProfile.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(classProfile.SeperateKey.Value) : new List<int>();
                        IDictionary<string, object> dicabsen = new Dictionary<string, object>();
                        dicabsen["ClassID"] = ClassID;
                        dicabsen["AcademicYearID"] = classProfile.AcademicYearID;
                        dicabsen["PupilID"] = transcripts.PupilID;
                        dicabsen["SchoolID"] = classProfile.SchoolID;

                        //Danh sach cac buoi nghi
                        List<PupilAbsence> pupilAbsenceList = PupilAbsenceBusiness.SearchBySchool(classProfile.SchoolID, dicabsen).Where(p => lstSectionKeyID.Contains(p.Section)).ToList();
                        int countAbsent_P_HK1 = 0, countAbsent_KP_HK1 = 0, countAbsent_P_HK2 = 0, countAbsent_KP_HK2 = 0, countAbsent_P = 0, countAbsent_KP = 0;
                        if (pupilAbsenceList != null && pupilAbsenceList.Count > 0)
                        {
                            for (int j = 0; j < pupilAbsenceList.Count; j++)
                            {
                                if (pupilAbsenceList[j].AbsentDate >= classProfile.AcademicYear.FirstSemesterStartDate && pupilAbsenceList[j].AbsentDate <= classProfile.AcademicYear.FirstSemesterEndDate)
                                {
                                    if (pupilAbsenceList[j].IsAccepted.HasValue && pupilAbsenceList[j].IsAccepted.Value)
                                        countAbsent_P_HK1++;
                                    else
                                        countAbsent_KP_HK1++;
                                }
                                else
                                {
                                    if (pupilAbsenceList[j].IsAccepted.HasValue && pupilAbsenceList[j].IsAccepted.Value)
                                        countAbsent_P_HK2++;
                                    else
                                        countAbsent_KP_HK2++;
                                }
                            }
                        }
                        countAbsent_P = countAbsent_P_HK1 + countAbsent_P_HK2;
                        countAbsent_KP = countAbsent_KP_HK1 + countAbsent_KP_HK2;
                        sheet.SetCellValue(startRow + 38, 4, countAbsent_P_HK1 == 0 ? "" : countAbsent_P_HK1.ToString());
                        sheet.SetCellValue(startRow + 38, 5, countAbsent_KP_HK1 == 0 ? "" : countAbsent_KP_HK1.ToString());
                        sheet.SetCellValue(startRow + 39, 4, countAbsent_P_HK2 == 0 ? "" : countAbsent_P_HK2.ToString());
                        sheet.SetCellValue(startRow + 39, 5, countAbsent_KP_HK2 == 0 ? "" : countAbsent_KP_HK2.ToString());
                        sheet.SetCellValue(startRow + 40, 4, countAbsent_P == 0 ? "" : countAbsent_P.ToString());
                        sheet.SetCellValue(startRow + 40, 5, countAbsent_KP == 0 ? "" : countAbsent_KP.ToString());
                        //Trong 1 ngày mà học sinh có số buổi nghỉ học >1 thì chỉ tính là 1 và loại nghỉ giống nhau.
                        //Nếu học sinh trong 1 ngày có số buổi nghỉ học >1 mà loại nghỉ là khác nhau thì ưu tiên lấy thông tin nghỉ học của học sinh theo thứ tự sáng, chiều, tối

                        //DateTime dateAbsence;
                        //if (pupilAbsenceList != null && pupilAbsenceList.Count > 0)
                        //{
                        //    List<DateTime> listDateAbsenceDistinct = pupilAbsenceList.Select(p => p.AbsentDate).Distinct().ToList();
                        //    List<PupilAbsence> pupilAbsenceListTmp = new List<PupilAbsence>();

                        //    ///For qua cac ngay co diem danh 
                        //    for (int j = 0; j < listDateAbsenceDistinct.Count; j++)
                        //    {
                        //        dateAbsence = listDateAbsenceDistinct[j];
                        //        // lay loai nghi P,K
                        //        pupilAbsenceListTmp = pupilAbsenceList.Where(p => p.AbsentDate == dateAbsence).ToList();
                        //        if (pupilAbsenceListTmp != null && pupilAbsenceListTmp.Count > 0)
                        //        {
                        //            totalAbsentDay++;
                        //        }
                        //    }
                        //}

                        //totalAbsentDay = PupilAbsenceBusiness.SearchBySchool(classProfile.SchoolID, dicabsen).Count();

                        // Xet len lop hay o lai
                        if (pr3 != null)
                        {
                            if (pr3.StudyingJudgementID.HasValue)
                            {
                                if (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS)
                                {
                                    sheet.SetCellValue(startRow + 36, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                                }
                                else if (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                {
                                    sheet.SetCellValue(startRow + 40, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                                }
                                else
                                {
                                    if (pr5 != null)
                                    {
                                        sheet.SetCellValue(startRow + 40, 8, pr5.StudyingJudgementID != null ? pr5.StudyingJudgementResolution : string.Empty);
                                    }
                                    else if (pr4 != null)
                                    {
                                        if (pr4.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS)
                                        {
                                            sheet.SetCellValue(startRow + 38, 8, pr4.StudyingJudgementID != null ? pr4.StudyingJudgementResolution : string.Empty);
                                        }
                                        else if (pr4.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                        {
                                            sheet.SetCellValue(startRow + 40, 8, pr4.StudyingJudgementID != null ? pr4.StudyingJudgementResolution : string.Empty);
                                        }
                                    }
                                }
                            }
                        }
                        if (pr4 != null)
                        {
                            sheet.SetCellValue(startRow + 40, 6, pr4.CapacityLevelID != null ? pr4.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 40, 7, pr4.ConductLevelID != null ? pr4.ConductLevelName : string.Empty);
                        }
                        if (pr5 != null)
                        {
                            sheet.SetCellValue(startRow + 40, 6, pr5.CapacityLevelID != null ? pr5.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 40, 7, pr5.ConductLevelID != null ? pr5.ConductLevelName : string.Empty);
                        }
                        //ducpt1 20160723 bo sung 
                        //Danh hieu thi dua
                        PupilEmulation ppEmulation = PupilEmulationBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.AcademicYearID == classProfile.AcademicYearID).FirstOrDefault();
                        sheet.SetCellValue(startRow + 42, 4, ppEmulation != null ? ppEmulation.HonourAchivementType.Resolution : string.Empty);
                        //Khen thuong ky luat                        
                        string[] lstPupilPraise = (from pb in PupilPraiseBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.AcademicYearID == classProfile.AcademicYearID)
                                                   join ppT in PraiseTypeBusiness.All on pb.PraiseTypeID equals ppT.PraiseTypeID
                                                   select ppT.Resolution).ToArray();
                        string[] lstPupilDiscipline = (from pd in PupilDisciplineBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.AcademicYearID == classProfile.AcademicYearID)
                                                       join dtb in DisciplineTypeBusiness.All on pd.DisciplineTypeID equals dtb.DisciplineTypeID
                                                       select dtb.Resolution).ToArray();

                        lstPupilPraise = lstPupilPraise.Concat(lstPupilDiscipline).ToArray();
                        sheet.SetCellValue(startRow + 43, 4, lstPupilPraise != null ? string.Join(", ", lstPupilPraise) : string.Empty);
                        // Lay thong tin khen thuong khac tu cap huyen tro len
                        List<PupilPraise> listPupilPraiseClass = listPupilPraise.Where(o => o.ClassID == listClassID[i]).ToList();
                        if (listPupilPraiseClass != null && listPupilPraiseClass.Count > 0)
                        {
                            string strPraise = string.Join(", ", listPupilPraiseClass.Select(o => o.Description));
                            if (strPraise.Length > strDotLen)
                            {
                                string[] arrW = strPraise.Split(' ');
                                string firstPraise = " ";
                                int totalLen = 0;
                                int pos = -1;
                                for (int j = 0; j < arrW.Length; j++)
                                {
                                    string w = arrW[j].Trim();
                                    if (totalLen <= strDotLen && totalLen + w.Length + 1 > strDotLen)
                                    {
                                        sheet.SetCellValue(startRow + 40, 1, praiseTitle + firstPraise);
                                        pos = j;
                                        break;
                                    }
                                    firstPraise += w + " ";
                                    totalLen += w.Length + 1;
                                }
                                if (pos >= 0)
                                {
                                    string secondPraise = "";
                                    for (int j = pos; j < arrW.Length; j++)
                                    {
                                        secondPraise += arrW[j] + " ";
                                    }
                                    sheet.SetCellValue(startRow + 41, 1, secondPraise);
                                }
                            }
                            else
                            {
                                sheet.SetCellValue(startRow + 40, 1, praiseTitle + strPraise);
                            }
                        }
                        sheet.FitAllColumnsOnOnePage = true;

                        if (countToHidden == 16)
                        {
                            sheet.DeleteRow(45);
                            sheet.DeleteRow(46);
                            sheet.SetBreakPage(52);
                            sheet.GetRange(51, 1, 51, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                        }
                        else if (countToHidden == 17)
                        {
                            sheet.DeleteRow(45);
                            sheet.DeleteRow(46);
                            sheet.DeleteRow(47);
                            sheet.DeleteRow(48);
                            sheet.SetBreakPage(50);
                            sheet.GetRange(49, 1, 49, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                        }
                        else if (countToHidden == 18)
                        {
                            sheet.DeleteRow(38);
                            sheet.DeleteRow(39);
                            sheet.DeleteRow(40);
                            sheet.DeleteRow(41);
                            sheet.DeleteRow(42);
                            sheet.DeleteRow(43);
                            sheet.SetBreakPage(48);
                            sheet.GetRange(47, 1, 47, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                        }
                        else
                        {
                            sheet.SetBreakPage(54);
                        }
                    }
                    ////sheet.PageMaginLeft = 0.7;
                    ////sheet.PageMaginRight = 1.2;
                    ////sheet.PageMaginTop = 0.2;
                    //sheet.PageMaginBottom = -2;
                    sheet.PageSize = VTXPageSize.VTxlPaperA4;
                }
                tempSheet.Delete();
                fourSheet.Delete();

                if (pp.Image != null && pp.Image.Length > 0)
                {
                    lstVTDataImageValidation.Add(new SMAS.VTUtils.Utils.VTDataImageValidation
                    {
                        SheetIndex = 2,
                        UpperLeftRow = 4,
                        UpperLeftColumn = 1,
                        LowerRightColumn = 2,
                        LowerRightRow = 8,
                        TypeFormat = 3,
                        WidthScale = 13,
                        HeightScale = 18,
                        ByteImage = pp.Image
                    });
                }
            }
            #endregion

            return oBook.InsertImage(lstVTDataImageValidation, lstDataPageNumber);
        }

        public void DownloadZipFileTranscript(Transcripts transcripts, out string folderSaveFile)
        {
            bool currentYear = transcripts.currentYear == 1 ? true : false;
            bool fillPaging = transcripts.paging == 1 ? true : false;
            #region lay danh sach hoc sinh cua lop           
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iqPocClass = PupilOfClassBusiness.GetPupilInClassTranscripReport(transcripts.ClassID);
            if (transcripts.PupilID > 0)
            {
                iqPocClass = iqPocClass.Where(poc => poc.PupilID == transcripts.PupilID);
            }

            if (currentYear)
            {
                iqPocClass = iqPocClass.Where(x => x.AcademicYearID == transcripts.AcademicYearID);
            }

            lstPOC = iqPocClass.OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            if (lstPOC == null || lstPOC.Count == 0)
            {
                folderSaveFile = string.Empty;
                return;
            }

            List<int> lstPupilID = lstPOC.Select(o => o.PupilID).ToList();
            PupilOfClassBO objPOC = null;
            int pupilID = 0;

            //Chiendd1: 06/07/2017, bo sung them STT hoc sinh de tranh bi trung ten file
            string fileName = "{0}_HS_THCS_GDTX_HocBa_{1}.xls";//HS_THCS_HocBaHocSinh_NguyenThiHongNhung
            if (transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX)
            {
                fileName = "{0}_HS_GDTX_HocBa_{1}.xls";
            }
            else if (transcripts.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                fileName = "{0}_HS_THPT_HocBaHocSinh_{1}.xls";
            }
            else
            {
                fileName = "{0}_HS_THCS_HocBaHocSinh_{1}.xls";
            }
            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            string reportCode = transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23TT58;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            AcademicYear Aca = AcademicYearBusiness.Find(transcripts.AcademicYearID);
            int modSchoolID = transcripts.SchoolID % 100;
            List<VJudgeRecord> lstjudgeAll = VJudgeRecordBusiness.All.Where(o => o.Last2digitNumberSchool == modSchoolID && lstPupilID.Contains(o.PupilID)).ToList();
            SchoolProfile sp = SchoolProfileBusiness.Find(transcripts.SchoolID);
            Commune commune = CommuneBusiness.Find(sp.CommuneID);
            District district = DistrictBusiness.Find(sp.DistrictID);
            Province province = ProvinceBusiness.Find(sp.ProvinceID);
            List<EducationLevel> listLevel = EducationLevelBusiness.GetByGrade(transcripts.AppliedLevel).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>
            {
            };

            // Lay danh sach hoc sinh hoc trong cap hoc
            List<PupilOfClass> lstPupilOfClassNullAll = PupilOfClassBusiness.Search(dic)
                                                        .Where(o => o.EndDate == null && lstPupilID.Contains(o.PupilID))
                                                         .ToList();

            List<PupilOfClass> lstPupilOfClassNotNullAll = PupilOfClassBusiness.Search(dic)
                                                            .Where(o => o.EndDate != null && lstPupilID.Contains(o.PupilID)).ToList();

            if (currentYear)
            {
                lstPupilOfClassNullAll = lstPupilOfClassNullAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
                lstPupilOfClassNotNullAll = lstPupilOfClassNotNullAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
            }

            List<int> lstPartition = lstPupilOfClassNullAll.Select(o => o.SchoolID % 100).Distinct().Union(lstPupilOfClassNotNullAll.Select(o => o.ClassProfile.SchoolID % 100).Distinct()).ToList();
            List<int> lstSchoolID = lstPupilOfClassNullAll.Select(o => o.SchoolID).Distinct().Union(lstPupilOfClassNotNullAll.Select(o => o.ClassProfile.SchoolID).Distinct()).ToList();
            IDictionary<string, object> dicpp = new Dictionary<string, object>();
            //bool EducationLevelCheck = false;
            List<PupilOfClass> lspocAll = PupilOfClassBusiness.SearchBySchool(transcripts.SchoolID, dicpp)
                                                                .Where(o => lstPupilID.Contains(o.PupilID)).ToList();

            List<SummedUpRecordBO> lstSummedUpRecordBOAll = (from p in VSummedUpRecordBusiness.All.Where(o =>
                                                        lstPupilID.Contains(o.PupilID) && o.PeriodID == null
                                                        && lstPartition.Contains(o.Last2digitNumberSchool))
                                                             join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                             where q.IsActive == true
                                                             select new SummedUpRecordBO
                                                             {
                                                                 SummedUpRecordID = p.SummedUpRecordID,
                                                                 PupilID = p.PupilID,
                                                                 ClassID = p.ClassID,
                                                                 SchoolID = p.SchoolID,
                                                                 AcademicYearID = p.AcademicYearID,
                                                                 PeriodID = p.PeriodID,
                                                                 SubjectID = p.SubjectID,
                                                                 JudgementResult = p.JudgementResult,
                                                                 SummedUpMark = p.SummedUpMark,
                                                                 ReTestMark = p.ReTestMark,
                                                                 ReTestJudgement = p.ReTestJudgement,
                                                                 Semester = p.Semester,
                                                                 Last2digitNumberSchool = p.Last2digitNumberSchool,
                                                                 CreatedAcademicYear = p.CreatedAcademicYear
                                                             }).ToList();

            List<PupilRankingBO> lstPupilRankingBOAll = (from p in VPupilRankingBusiness.All.Where(o =>
                                               lstPupilID.Contains(o.PupilID) && o.PeriodID == null
                                               && lstPartition.Contains(o.Last2digitNumberSchool))
                                                             //join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                         join r in CapacityLevelBusiness.All on p.CapacityLevelID equals r.CapacityLevelID into g1
                                                         from j1 in g1.DefaultIfEmpty()
                                                         join s in ConductLevelBusiness.All on p.ConductLevelID equals s.ConductLevelID into g2
                                                         from j2 in g2.DefaultIfEmpty()
                                                         join l in StudyingJudgementBusiness.All on p.StudyingJudgementID equals l.StudyingJudgementID into g3
                                                         from j3 in g3.DefaultIfEmpty()
                                                         select new PupilRankingBO
                                                         {
                                                             PupilRankingID = p.PupilRankingID,
                                                             PupilID = p.PupilID,
                                                             ClassID = p.ClassID,
                                                             SchoolID = p.SchoolID,
                                                             Semester = p.Semester,
                                                             AverageMark = p.AverageMark,
                                                             CapacityLevelID = p.CapacityLevelID,
                                                             ConductLevelID = p.ConductLevelID,
                                                             ConductLevelName = j2.Resolution,
                                                             CapacityLevel = j1.CapacityLevel1,
                                                             StudyingJudgementID = p.StudyingJudgementID,
                                                             StudyingJudgementResolution = j3.Resolution,
                                                             Last2digitNumberSchool = p.Last2digitNumberSchool,
                                                             CreatedAcademicYear = p.CreatedAcademicYear,
                                                             AcademicYearID = p.AcademicYearID,
                                                             Comment = p.PupilRankingComment
                                                         }).ToList();

            List<ClassSubject> classSubjectAll = (from cs in ClassSubjectBusiness.All
                                                  join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                  where lstPartition.Contains(cs.Last2digitNumberSchool)
                                                  && lstSchoolID.Contains(cs.ClassProfile.SchoolID)
                                                  orderby sc.OrderInSubject, sc.DisplayName
                                                  select cs).ToList();

            Dictionary<string, object> dicTeaching = new Dictionary<string, object>();
            List<TeachingAssignment> iqTeachingAssignmen = TeachingAssignmentBusiness.SearchBySchool(transcripts.SchoolID, dicTeaching).ToList();

            Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
            dicEmployee.Add("SchoolID", transcripts.SchoolID);
            //dicEmployee.Add("CurrentEmployeeStatus", GlobalConstants.EMPLOYMENT_STATUS_WORKING);
            List<Employee> IqEmployee = EmployeeBusiness.Search(dicEmployee).ToList();

            List<PupilPraise> listPupilPraiseAll = new List<PupilPraise>();
            if (transcripts.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                listPupilPraiseAll = PupilPraiseBusiness.All.Where(o => lstPupilID.Contains(o.PupilID) && o.PupilProfile.IsActive == true && o.IsRecordedInSchoolReport == true
               && AreaPraiseID.Contains(o.PraiseLevel))
               .ToList();
            }
            else
            {
                listPupilPraiseAll = PupilPraiseBusiness.All.Where(o => lstPupilID.Contains(o.PupilID) && o.PupilProfile.IsActive == true && o.IsRecordedInSchoolReport == true
               && AreaPraiseIDTT58.Contains(o.PraiseLevel))
                .ToList();

            }


            List<PupilAbsence> pupilAbsenceListAll = PupilAbsenceBusiness.All.Where(o => lstPupilID.Contains(o.PupilID)
                && lstPartition.Contains(o.Last2digitNumberSchool)).ToList();

            List<Employee> lstEmployeeAll = EmployeeBusiness.All.Where(o => o.SchoolID != null && lstSchoolID.Contains(o.SchoolID.Value)).ToList();

            List<SchoolProfile> lstSchoolAll = SchoolProfileBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolProfileID)).ToList();

            List<AcademicYear> lstAcaAll = AcademicYearBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolID) && o.IsActive == true).ToList();

            if (currentYear)
            {
                lspocAll = lspocAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
                lstSummedUpRecordBOAll = lstSummedUpRecordBOAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
                listPupilPraiseAll = listPupilPraiseAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
                pupilAbsenceListAll = pupilAbsenceListAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
                lstAcaAll = lstAcaAll.Where(x => x.AcademicYearID == transcripts.AcademicYearID).ToList();
            }

            List<SMAS.VTUtils.Utils.VTDataImageValidation> lstVTDataImageValidation = new List<VTUtils.Utils.VTDataImageValidation>();
            List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];

                lstVTDataImageValidation = new List<VTUtils.Utils.VTDataImageValidation>();
                if (((transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY
                    || transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)) && !currentYear)
                {
                    if (objPOC.Image != null && objPOC.Image.Length > 0)
                    {
                        lstVTDataImageValidation.Add(new SMAS.VTUtils.Utils.VTDataImageValidation
                        {
                            SheetIndex = 2,
                            UpperLeftRow = (transcripts.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX ? 3 : 4),
                            UpperLeftColumn = 1,
                            LowerRightColumn = (transcripts.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX ? 1 : 2),
                            LowerRightRow = 8,
                            TypeFormat = 3,
                            ByteImage = objPOC.Image
                        });
                    }
                }

                pupilID = objPOC.PupilID;
                transcripts.PupilID = pupilID;
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                string fileNameByPupil = "";
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                this.CreateTranscriptsZipFile(oBook, transcripts, Aca, lstjudgeAll, objPOC, sp, commune, district, province, listLevel,
                    lstPupilOfClassNullAll, lstPupilOfClassNotNullAll, lspocAll, classSubjectAll, iqTeachingAssignmen, IqEmployee,
                    lstSummedUpRecordBOAll, lstPupilRankingBOAll, listPupilPraiseAll, pupilAbsenceListAll, lstEmployeeAll, lstSchoolAll, lstAcaAll,
                    currentYear, fillPaging, ref lstDataPageNumber);

                fileNameByPupil = string.Format(fileName, objPOC.OrderInClass.HasValue ? objPOC.OrderInClass.ToString() : objPOC.PupilCode, Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(objPOC.PupilFullName)));
                oBook.CreateZipFile(fileNameByPupil, folder, lstVTDataImageValidation, lstDataPageNumber);
                lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
            }
            folderSaveFile = folder;
            #endregion
        }

        public void CreateTranscriptsZipFile(IVTWorkbook oBook, Transcripts transcripts, AcademicYear Aca, List<VJudgeRecord> lstjudgeAll,
            PupilOfClassBO objPoc, SchoolProfile sp, Commune commune, District district, Province province, List<EducationLevel> listLevel,
            List<PupilOfClass> lstPupilOfClassNullAll, List<PupilOfClass> lstPupilOfClassNotNullAll, List<PupilOfClass> lspocAll,
            List<ClassSubject> classSubjectAll, List<TeachingAssignment> iqTeachingAssignmen, List<Employee> IqEmployee,
            List<SummedUpRecordBO> lstSummedUpRecordBOAll, List<PupilRankingBO> lstPupilRankingBOAll, List<PupilPraise> listPupilPraiseAll,
            List<PupilAbsence> pupilAbsenceListAll, List<Employee> lstEmployeeAll, List<SchoolProfile> lstSchoolAll, List<AcademicYear> lstAcaAll,
            bool checkCurrentYear, bool fillPaging, ref List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber)
        {
            int modSchoolID = transcripts.SchoolID % 100;
            //DAnh sach diem nhan xet cua hoc sinh tat ca cac nam
            List<VJudgeRecord> lstjudge = lstjudgeAll.Where(o => o.PupilID == transcripts.PupilID).ToList();
            #region Khai báo + lấy thông tin
            int sheetPosition = checkCurrentYear ? 0 : 2;
            int startPageNumberOfSheet = checkCurrentYear ? 0 : 1;
            string communeName = "";
            string districtName = "";
            string provinceName = "";
            // Sheet trang bìa
            string grade = ReportUtils.ConvertAppliedLevelToFullReportName(transcripts.AppliedLevel);
            string fullName = objPoc.PupilFullName.ToUpper();
            string storageNumber = !string.IsNullOrEmpty(objPoc.StorageNumber) ? objPoc.StorageNumber : "";
            string schoolName = SplitSchoolName(sp.SchoolName);
            if (commune != null)
            {
                communeName = commune.CommuneName;
            }

            if (district != null)
            {
                districtName = district.DistrictName;
            }
            if (province != null)
            {
                provinceName = province.ProvinceName;
            }
            // Sheet thông tin chung
            string birthDate = Utils.ConvertDateToString(objPoc.Birthday);
            string birthPlace = objPoc.BirthPlace;
            int genre = objPoc.Genre.GetValueOrDefault();
            string ethnicName = "";
            ethnicName = objPoc.EthnicName;
            string permanentResidentalAddress = objPoc.PermanentResidentalAddress;
            string tempResidentalAddress = objPoc.ResidentalAddress;
            string fatherFullName = objPoc.FatherFullName;
            string fatherJob = objPoc.FatherJobName;
            string motherFullName = objPoc.MotherFullName;
            string motherJob = objPoc.MotherJobName;
            string sponsorFullName = objPoc.SponserFullName;
            string sponsorJob = objPoc.SponserJobName;

            // Lay danh sach hoc sinh hoc trong cap hoc
            List<PupilOfClass> lstPupilOfClassNull = lstPupilOfClassNullAll.Where(o => o.PupilID == transcripts.PupilID).ToList();
            List<PupilOfClass> lstPupilOfClassInSchool = new List<PupilOfClass>();
            List<PupilOfClass> lstPupilOfClassNotNull = lstPupilOfClassNotNullAll.Where(o => o.PupilID == transcripts.PupilID).ToList();
            //Lay ra thong tin hoc sinh cua truong can xuat hoc ba
            foreach (PupilOfClass poc in lstPupilOfClassNotNull)
            {
                if (poc.SchoolID == transcripts.SchoolID)
                {
                    if (poc.EndDate >= Aca.FirstSemesterEndDate)
                    {
                        lstPupilOfClassInSchool.Add(poc);
                    }
                }
                else
                {
                    if (poc.EndDate <= Aca.SecondSemesterEndDate)
                    {
                        lstPupilOfClassInSchool.Add(poc);
                    }
                }
            }

            //Lay ra khoi hoc lon nhat ma hoc sinh day hoc
            int EducationLevelMax = 0;
            //bool EducationLevelCheck = false;
            List<PupilOfClass> lspoc = lspocAll.Where(o => o.PupilID == transcripts.PupilID).ToList();
            if (lspoc.Count() > 0)
            {
                EducationLevelMax = lspoc.FirstOrDefault().ClassProfile.EducationLevelID;
            }
            //lspoc = lspoc.Where(o => o.EndDate >= Aca.FirstSemesterEndDate);
            //if (lspoc.Count() > 0)
            //{
            //    EducationLevelCheck = true;
            //}
            //Lọc ra những lớp mà học sinh học ở trường khác
            List<PupilOfClass> listPupilOfClassNull = new List<PupilOfClass>();
            if (EducationLevelMax != 0)
            {
                foreach (PupilOfClass poc in lstPupilOfClassNull)
                {
                    if (poc.SchoolID == transcripts.SchoolID)
                    {
                        listPupilOfClassNull.Add(poc);
                    }
                    else
                    {
                        if (poc.ClassProfile.EducationLevelID < EducationLevelMax)
                        {
                            listPupilOfClassNull.Add(poc);
                        }
                    }
                }
            }

            //Ket hop list
            List<PupilOfClass> listPupilOfClass = listPupilOfClassNull.Union(lstPupilOfClassInSchool).OrderBy(o => o.ClassProfile.EducationLevelID).ThenBy(o => o.AssignedDate).ToList();
            //Sap xep lai list
            List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
            foreach (PupilOfClass poc in listPupilOfClass)
            {
                if (poc.SchoolID != transcripts.SchoolID)
                {
                    lstPupilOfClass.Add(poc);
                }
                else
                {
                    lstPupilOfClass.Add(poc);
                }
            }
            List<int> listClassID = new List<int>();

            //new List<int>();
            List<ClassProfile> listClassProfile = new List<ClassProfile>();
            for (int i = 0; i < listLevel.Count; i++)
            {
                List<PupilOfClass> listClass = lstPupilOfClass.Where(o => o.ClassProfile.EducationLevelID == listLevel[i].EducationLevelID).ToList();

                if (listClass.Count > 0)
                {
                    if (listClass.Count > 1)
                    {
                        listClass = listClass.Where(o => o.SchoolID == transcripts.SchoolID).ToList();
                    }
                    ClassProfile classProfile = null;
                    if (listClass != null && listClass.Count > 0)
                    {
                        for (int j = 0; j < listClass.Count; j++)
                        {
                            classProfile = new ClassProfile();
                            classProfile = listClass[j].ClassProfile;
                            listClassProfile.Add(classProfile);
                            listClassID.Add(classProfile.ClassProfileID);
                        }
                    }
                }
            }
            #endregion

            #region Cấp 2-3
            if (transcripts.TrainingType != SystemParamsInFile.TRAINING_TYPE_GDTX && (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY || transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY))
            {
                //Lấy Sheet Trang bìa
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                // Lấy Sheet Thông tin chung
                IVTWorksheet secondSheet = oBook.GetSheet(2);
                // Lấy Sheet lớp học
                IVTWorksheet thirdSheet = oBook.GetSheet(3);
                // Lấy Sheet nhận xét
                IVTWorksheet CommentSheet = oBook.GetSheet(4);

                #region // Fill thông tin sheet trang bìa
                firstSheet.SetCellValue("A16", grade);
                firstSheet.SetCellValue("A25", fullName);
                if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY)
                {
                    firstSheet.SetCellValue("A33", string.Format("SỐ: {0}/THCS", !string.IsNullOrEmpty(storageNumber) ? storageNumber : ".........."));
                }
                if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
                {
                    firstSheet.SetCellValue("A33", string.Format("SỐ: {0}/THPT", !string.IsNullOrEmpty(storageNumber) ? storageNumber : ".........."));
                }
                #endregion

                #region // Thông tin chung
                string target = "";
                if (objPoc.PolicyTargetID.HasValue)
                {
                    if (ListPolicyTargetID.Contains(objPoc.PolicyTargetID.Value))
                        target = objPoc.PolicyTargetName;
                }
                string valueCellA13 = string.Empty;
                valueCellA13 = "Dân tộc:                [EthnicName], con liệt sĩ, con thương binh (bệnh binh, người được hưởng chế độ như thương binh, gia đình có công với cách mạng ): [PolicyTarget]";
                valueCellA13 = valueCellA13.Replace("[EthnicName]", ethnicName);
                valueCellA13 = valueCellA13.Replace("[PolicyTarget]", target);

                secondSheet.SetCellValue("B8", grade);
                secondSheet.SetCellValue("B10", fullName);
                secondSheet.SetCellValue("G10", Utils.Genre(genre));
                secondSheet.SetCellValue("B11", string.Format("{0} tháng {1} năm {2}", objPoc.Birthday.Day, objPoc.Birthday.Month, objPoc.Birthday.Year));
                secondSheet.SetCellValue("B12", birthPlace);
                secondSheet.SetCellValue("A13", valueCellA13);
                secondSheet.SetCellValue("B15", !string.IsNullOrEmpty(tempResidentalAddress) ? tempResidentalAddress : permanentResidentalAddress);
                secondSheet.SetCellValue("B16", fatherFullName);
                secondSheet.SetCellValue("G16", fatherJob);
                secondSheet.SetCellValue("B17", motherFullName);
                secondSheet.SetCellValue("G17", motherJob);
                secondSheet.SetCellValue("C18", sponsorFullName);
                secondSheet.SetCellValue("G18", sponsorJob);
                secondSheet.SetCellValue("D20", districtName + ", ngày " + objPoc.EnrolmentDate.Day + " tháng " + objPoc.EnrolmentDate.Month + " năm " + objPoc.EnrolmentDate.Year);
                secondSheet.SetCellValue("D25", sp.HeadMasterName);

                // Quá trình học tập
                for (int ii = 0; ii < lstPupilOfClass.Count; ii++)
                {
                    secondSheet.SetCellValue(29 + ii, 1, lstPupilOfClass[ii].AcademicYear.Year.ToString() + "-" + (lstPupilOfClass[ii].AcademicYear.Year + 1).ToString());
                    secondSheet.SetCellValue(29 + ii, 2, lstPupilOfClass[ii].ClassProfile.DisplayName);
                    if (lstPupilOfClass[ii].SchoolProfile.Commune != null)
                    {
                        string Name = SplitSchoolName(lstPupilOfClass[ii].SchoolProfile.SchoolName);
                        secondSheet.SetCellValue(29 + ii, 3, Name + " - " + lstPupilOfClass[ii].SchoolProfile.Commune.CommuneName + " - " + lstPupilOfClass[ii].SchoolProfile.District.DistrictName + " - " + lstPupilOfClass[ii].SchoolProfile.Province.ProvinceName);
                    }
                    else
                    {
                        string Name = SplitSchoolName(lstPupilOfClass[ii].SchoolProfile.SchoolName);
                        secondSheet.SetCellValue(29 + ii, 3, Name + " - " + lstPupilOfClass[ii].SchoolProfile.District.DistrictName + " - " + lstPupilOfClass[ii].SchoolProfile.Province.ProvinceName);
                    }
                }
                if (fillPaging && !checkCurrentYear)
                {
                    lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                    {
                        SheetIndex = sheetPosition,
                        IsHeaderOrFooter = true,
                        AlignPageNumber = 2,
                        StartPageNumberOfSheet = startPageNumberOfSheet,
                    });
                }

                if (objPoc.Image == null || objPoc.Image.Length == 0)
                {
                    secondSheet.GetRange(3, 1, 8, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
                #endregion

                // Lay toan bo thong tin diem cua hoc sinh nay
                var listSchoolAca = listClassProfile.Select(o => new { o.SchoolID, o.AcademicYearID, CreatedYear = o.AcademicYear.Year }).Distinct().ToList();

                List<SummedUpRecordBO> listSummedUpRecord = new List<SummedUpRecordBO>();
                List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
                foreach (var sa in listSchoolAca)
                {
                    listSummedUpRecord.AddRange(lstSummedUpRecordBOAll.Where(o =>
                                                o.SchoolID == sa.SchoolID && o.AcademicYearID == sa.AcademicYearID
                                                && o.PupilID == transcripts.PupilID
                                               ).ToList());
                    // Danh sach mon hoc

                    // Lay thong tin tong ket cua hoc sinh
                    listPupilRanking.AddRange(lstPupilRankingBOAll.Where(o =>
                        o.SchoolID == sa.SchoolID && o.AcademicYearID == sa.AcademicYearID
                        && o.PupilID == transcripts.PupilID
                                              ).ToList());
                }

                // Danh sach mon hoc
                List<ClassSubject> classSubject = classSubjectAll
                    .Where(o => listClassID.Contains(o.ClassID)).ToList();
                //Chiendd: 25.11.2015 Bo sung danh sach giao vien BM
                ClassSubject objClassSubject;

                List<TeachingAssignmentBO> lstTeachingBo = (from tc in iqTeachingAssignmen
                                                            join e in IqEmployee on tc.TeacherID equals e.EmployeeID
                                                            where listClassID.Contains(tc.ClassID.Value)
                                                            select new TeachingAssignmentBO
                                                            {
                                                                EmployeeCode = e.EmployeeCode,
                                                                Semester = tc.Semester,
                                                                TeacherID = e.EmployeeID,
                                                                TeacherName = e.FullName,
                                                                SubjectID = tc.SubjectID,
                                                                ClassID = tc.ClassID
                                                            }).ToList();

                // Lay thong tin khen thuong, ky luat cua hoc sinh
                List<PupilPraise> listPupilPraise = listPupilPraiseAll.Where(o => o.PupilID == transcripts.PupilID).ToList();

                int currentSemesterId = 1;

                List<string> lstSpecializedSubject = new List<string>();
                double defaultAreaSubjectHeight = 305.25;
                startPageNumberOfSheet++;
                #region // Fill class
                for (int i = 0; i < listClassID.Count; i++)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(thirdSheet);
                    IVTWorksheet fourSheet = oBook.CopySheetToLast(CommentSheet);
                    IVTRange rangComment = CommentSheet.GetRange("A2", "J40");
                    fourSheet.CopyPasteSameSize(rangComment, 2, 1);

                    if (listClassID[i] != 0)
                    {
                        int ClassID = listClassID[i];
                        //Đặt lại tên sheet
                        ClassProfile classProfile = listClassProfile[i];
                        //Danh sach diem mon tinh diem
                        AcademicYear academic = lstAcaAll.Where(o => o.AcademicYearID == classProfile.AcademicYearID).FirstOrDefault();
                        if (academic.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academic.FirstSemesterEndDate)
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        }
                        else
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        }

                        List<SummedUpRecordBO> lstSumUp = listSummedUpRecord.Where(o => o.ClassID == ClassID && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == classProfile.SchoolID).ToList();
                        sheet.Name = classProfile.DisplayName;
                        // Xuat ra thong tin nam hoc va truong
                        sheet.SetCellValue("A2", "Họ và tên: " + objPoc.PupilFullName);
                        sheet.SetCellValue("F2", "Lớp: " + classProfile.DisplayName);
                        sheet.SetCellValue("J2", "Năm học: " + classProfile.AcademicYear.DisplayTitle);
                        sheet.SetCellValue("A3", string.Format("Ban: {0}", classProfile.SubCommitteeID.HasValue ? classProfile.SubCommittee.Resolution : ""));

                        // xử lý Sheet        
                        List<ClassSubject> lstClassSubject = classSubject.Where(o => o.ClassID == ClassID).ToList();
                        List<ClassSubject> classSubjectNormal = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY).ToList();
                        List<ClassSubject> classSubjectOptional = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY || o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK).ToList();
                        List<ClassSubject> classSubjectEnglish2 = lstClassSubject.Where(o => o.IsSecondForeignLanguage == true).ToList();
                        List<ClassSubject> classSubjectApprenticeship = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_APPRENTICESHIP).ToList();

                        //Neu o lop nay thang hoc sinh nay chuyen lop\
                        PupilOfClass classmove = lstPupilOfClass.Where(o => o.ClassID == ClassID).FirstOrDefault();
                        AcademicYear acatemp = lstAcaAll.Where(o => o.AcademicYearID == classmove.AcademicYearID).FirstOrDefault();
                        int Semester = 0;
                        if (classmove != null && classmove.EndDate != null)
                        {
                            if (classmove.EndDate > acatemp.FirstSemesterEndDate)
                            {
                                Semester = 2;
                            }
                            else
                            {
                                Semester = 1;
                            }
                        }
                        // Kiem tra học sinh
                        List<SummedUpRecordBO> summedUpRecord1 = lstSumUp
                            .Where(o => o.ClassID == ClassID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            .ToList();

                        List<SummedUpRecordBO> summedUpRecord2 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord3 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord4 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .ToList();
                        if (Semester == 1)
                        {
                            summedUpRecord1 = new List<SummedUpRecordBO>();
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        if (Semester == 2)
                        {
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        // ClassSubject Normal
                        int subNormalID;
                        int subOptionalID;
                        int subEnglishID;
                        int subApprenticeID;
                        int startRow = 6;
                        int rowCopy = 6;
                        #region // Duyệt môn học
                        for (int j = 0; j < classSubjectNormal.Count; j++)
                        {
                            if (startRow > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow, true);
                            }

                            if (classSubjectNormal[j].IsSpecializedSubject.HasValue && classSubjectNormal[j].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectNormal[j].SubjectCat.DisplayName);
                            }

                            sheet.SetCellValue(startRow, 1, classSubjectNormal[j].SubjectCat.DisplayName);
                            //Môn tính điểm
                            subNormalID = classSubjectNormal[j].SubjectID;
                            if (classSubjectNormal[j].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum1 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum2 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum3 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum4 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum1 != null && sum1.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum1.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum2 != null && sum2.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum2.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum4 != null && sum4.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum4.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum3 != null && sum3.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum3.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else  //Môn nhận xét
                            {
                                SummedUpRecordBO sum5 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum6 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum7 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum8 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum5 != null)
                                {
                                    string judgementResult1 = sum5.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum6 != null)
                                {
                                    string judgementResult2 = sum6.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum8 != null)
                                {
                                    string judgementResult4 = sum8.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum7 != null)
                                {
                                    string judgementResult3 = sum7.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectNormal[j];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion

                        #region // Môn tiếng anh 2
                        for (int n = 0; n < classSubjectEnglish2.Count; n++)
                        {
                            if (startRow > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow, true);
                            }
                            if (classSubjectEnglish2[n].IsSpecializedSubject.HasValue && classSubjectEnglish2[n].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectEnglish2[n].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue(startRow, 1, classSubjectEnglish2[n].SubjectCat.DisplayName);
                            subEnglishID = classSubjectEnglish2[n].SubjectID;

                            if (classSubjectEnglish2[n].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum17 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum18 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum19 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum20 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum17 != null && sum17.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum17.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum18 != null && sum18.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum18.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum20 != null && sum20.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum20.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum19 != null && sum19.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum19.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum21 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum22 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum23 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum24 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum21 != null)
                                {
                                    string judgementResult1 = sum21.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum22 != null)
                                {
                                    string judgementResult2 = sum22.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum24 != null)
                                {
                                    string judgementResult4 = sum24.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum23 != null)
                                {
                                    string judgementResult3 = sum23.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectEnglish2[n];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion

                        #region // Môn nghề
                        for (int m = 0; m < classSubjectApprenticeship.Count; m++)
                        {
                            if (startRow > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow, true);
                            }
                            if (classSubjectApprenticeship[m].IsSpecializedSubject.HasValue && classSubjectApprenticeship[m].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectApprenticeship[m].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue(startRow, 1, classSubjectApprenticeship[m].SubjectCat.DisplayName);
                            subApprenticeID = classSubjectApprenticeship[m].SubjectID;
                            if (classSubjectApprenticeship[m].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum25 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum26 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum27 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum28 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum25 != null && sum25.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum25.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum26 != null && sum26.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum26.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum28 != null && sum28.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum28.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum27 != null && sum27.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum27.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum29 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum30 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum31 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum32 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum29 != null)
                                {
                                    string judgementResult1 = sum29.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum30 != null)
                                {
                                    string judgementResult2 = sum30.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum32 != null)
                                {
                                    string judgementResult4 = sum32.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum31 != null)
                                {
                                    string judgementResult3 = sum31.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectApprenticeship[m];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion

                        #region // Class Subject Optional
                        int startRowDefault = startRow;
                        for (int k = 0; k < classSubjectOptional.Count; k++)
                        {
                            if (k > 0)
                            {
                                sheet.CopyAndInsertARow(startRowDefault, startRow, true);
                            }
                            if (classSubjectOptional[k].IsSpecializedSubject.HasValue && classSubjectOptional[k].IsSpecializedSubject.Value == true)
                            {
                                lstSpecializedSubject.Add(classSubjectOptional[k].SubjectCat.DisplayName);
                            }
                            sheet.SetCellValue(startRow, 2, classSubjectOptional[k].SubjectCat.DisplayName);
                            subOptionalID = classSubjectOptional[k].SubjectID;
                            //Môn tính điểm
                            if (classSubjectOptional[k].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum9 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum10 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum11 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum12 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum9 != null && sum9.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum9.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum10 != null && sum10.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum10.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum12 != null && sum12.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum12.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum11 != null && sum11.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum11.ReTestMark.Value;
                                    sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else //Môn nhận xét
                            {
                                SummedUpRecordBO sum13 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum14 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum15 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum16 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum13 != null)
                                {
                                    string judgementResult1 = sum13.JudgementResult;
                                    sheet.SetCellValue(startRow, 3, judgementResult1);
                                }
                                if (sum14 != null)
                                {
                                    string judgementResult2 = sum14.JudgementResult;
                                    sheet.SetCellValue(startRow, 4, judgementResult2);
                                }
                                if (sum16 != null)
                                {
                                    string judgementResult4 = sum16.JudgementResult;
                                    sheet.SetCellValue(startRow, 5, judgementResult4);
                                }
                                if (sum15 != null)
                                {
                                    string judgementResult3 = sum15.ReTestJudgement;
                                    sheet.SetCellValue(startRow, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectOptional[k];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow, 7, sheet, currentSemesterId);
                            startRow++;
                        }
                        #endregion

                        if (classSubjectOptional.Count() > 1)
                        {
                            sheet.MergeColumn('A', (startRowDefault + classSubjectOptional.Count() - 1), startRowDefault);
                        }
                        else
                        {
                            // Neu ko he co du lieu thi xoa dong 5
                            if (startRow == rowCopy)
                            { sheet.DeleteRow(startRow); }
                            // Giu nguyen dong tu chon
                            if (classSubjectOptional.Count() == 0)
                            { startRow++; }
                        }

                        if (lstSpecializedSubject.Count() > 0)
                        {
                            string _specializedSubject = string.Join(", ", lstSpecializedSubject.ToArray());
                            sheet.SetCellValue("E3", "Các môn học nâng cao: " + _specializedSubject);
                        }

                        // Chiều cao mỗi dòng trong khu vực môn học
                        double rowHeight = (defaultAreaSubjectHeight / (startRow - 6));
                        for (int row = 6; row < startRow; row++)
                        {
                            sheet.SetRowHeight(row, rowHeight);
                        }

                        #region// Thong tin tong ket
                        PupilRankingBO pr1 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                        PupilRankingBO pr2 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                        int checkstudy = 0;
                        PupilRankingBO pr3 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                        if (pr3 != null && pr3.StudyingJudgementID.HasValue)
                        {
                            checkstudy = pr3.StudyingJudgementID.Value;
                        }
                        //Thi lai -> Fill vao hoc luc
                        PupilRankingBO pr4 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).FirstOrDefault();
                        //Ren luyen lai -> Fill vao hanh kiem
                        PupilRankingBO pr5 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).FirstOrDefault();
                        if (Semester == 1)
                        {
                            pr1 = null;
                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        if (Semester == 2)
                        {

                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        //Fill thong tin diem TB cac mon sau khi thi lai
                        if (pr4 != null)
                        {
                            if (pr4.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr4.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }
                        if (pr5 != null)
                        {
                            if (pr5.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr5.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }
                        #endregion

                        SchoolProfile spe = lstSchoolAll.Where(o => o.SchoolProfileID == classProfile.SchoolID).FirstOrDefault();

                        #region //fill ten giao vien va sheet nhan xet truoc khi copy
                        int headTeacherId = classProfile.HeadTeacherID.HasValue ? classProfile.HeadTeacherID.Value : 0;
                        Employee employeeObj = null;
                        if (headTeacherId > 0)
                        {
                            employeeObj = lstEmployeeAll.Where(o => o.EmployeeID == headTeacherId).FirstOrDefault();
                            fourSheet.SetCellValue("A25", employeeObj != null ? employeeObj.FullName : string.Empty);
                        }

                        fourSheet.SetCellValue("F32", districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        fourSheet.SetCellValue("F38", spe.HeadMasterName);

                        int startRowTBM = startRow;
                        int startRowFourSheet = 4;

                        sheet.SetCellValue(startRowTBM + 11, 1, employeeObj != null ? employeeObj.FullName : string.Empty);
                        sheet.SetCellValue(startRowTBM + 11, 6, sp != null ? sp.HeadMasterName : string.Empty);
                        #endregion

                        #region //Thong tin truong + tinh thanh + hoc luc/hanh kiem
                        fourSheet.SetCellValue(startRowFourSheet - 1, 2, schoolName);
                        fourSheet.SetCellValue(startRowFourSheet - 1, 5, ("Huyện: " + spe.District.DistrictName));
                        fourSheet.SetCellValue(startRowFourSheet - 1, 8, "Tỉnh (TP): " + spe.Province.ProvinceName);

                        if (pr1 != null)
                        {
                            if (pr1.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr1.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 3, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            fourSheet.SetCellValue(startRowFourSheet + 3, 4, pr1.CapacityLevelID != null ? pr1.CapacityLevel : "");
                            fourSheet.SetCellValue(startRowFourSheet + 3, 3, pr1.ConductLevelID != null ? pr1.ConductLevelName : string.Empty);
                        }
                        if (pr2 != null)
                        {
                            if (pr2.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr2.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 4, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            fourSheet.SetCellValue(startRowFourSheet + 4, 4, pr2.CapacityLevelID != null ? pr2.CapacityLevel : "");
                            fourSheet.SetCellValue(startRowFourSheet + 4, 3, pr2.ConductLevelID != null ? pr2.ConductLevelName : string.Empty);
                        }
                        if (pr3 != null)
                        {
                            if (pr3.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr3.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 5, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            fourSheet.SetCellValue(startRowFourSheet + 5, 4, pr3.CapacityLevelID != null ? pr3.CapacityLevel : "");
                            fourSheet.SetCellValue(startRowFourSheet + 5, 3, pr3.ConductLevelID != null ? pr3.ConductLevelName : string.Empty);
                        }


                        // So ngay nghi
                        List<int> lstSectionKeyID = classProfile.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(classProfile.SeperateKey.Value) : new List<int>();

                        //Danh sach cac buoi nghi
                        List<PupilAbsence> pupilAbsenceList = pupilAbsenceListAll.Where(o => o.ClassID == ClassID
                            && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == transcripts.SchoolID
                            && o.PupilID == transcripts.PupilID).Where(p => lstSectionKeyID.Contains(p.Section)).ToList();
                        fourSheet.SetCellValue(startRowFourSheet + 3, 5, pupilAbsenceList.Count);
                        #endregion

                        #region // Xet len lop hay o lai
                        if (pr3 != null)
                        {
                            // lên lớp
                            if (pr3.StudyingJudgementID.HasValue && (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS))
                            {
                                fourSheet.SetCellValue(startRowFourSheet + 1, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                            }
                            else if (pr3.StudyingJudgementID.HasValue && (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS))
                            { // ở lại lớp
                                fourSheet.SetCellValue(startRowFourSheet + 5, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                            }
                            else
                            {
                                if (pr5 != null)
                                {
                                    if (pr5.StudyingJudgementID != null)
                                    {
                                        if (pr5.StudyingJudgementResolution.Contains(pupilStayClass))
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 5, 8, pr5.StudyingJudgementResolution);
                                        }
                                        else
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 2, 8, pr5.StudyingJudgementID != null ? (upClass + pr5.StudyingJudgementResolution) : upClass + "…………….");
                                        }
                                    }
                                }
                                else if (pr4 != null)
                                {
                                    if (pr4.StudyingJudgementID != null)
                                    {
                                        if (pr4.StudyingJudgementResolution.Contains(pupilStayClass))
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 5, 8, pr4.StudyingJudgementResolution);
                                        }
                                        else
                                        {
                                            fourSheet.SetCellValue(startRowFourSheet + 2, 8, pr4.StudyingJudgementID != null ? (upClass + pr4.StudyingJudgementResolution) : upClass + "…………….");
                                        }
                                    }
                                }
                            }
                        }
                        if (pr4 != null)
                        {
                            fourSheet.SetCellValue(startRowFourSheet + 3, 7, pr4.CapacityLevelID != null ? pr4.CapacityLevel : "");
                        }
                        if (pr5 != null)
                        {
                            fourSheet.SetCellValue(startRowFourSheet + 3, 6, pr5.ConductLevelID != null ? pr5.ConductLevelName : string.Empty);
                        }
                        #endregion

                        #region // Lay thong tin khen thuong khac tu cap huyen tro len
                        string _strBounes = string.Empty;
                        List<string> lstBounesSpecial = new List<string>();
                        List<string> lstBounesNormal = new List<string>();
                        List<PupilPraise> listPupilPraiseClass = listPupilPraise.Where(o => o.ClassID == listClassID[i]).ToList();
                        if (listPupilPraiseClass != null && listPupilPraiseClass.Count > 0)
                        {
                            string praiseType = string.Empty;
                            for (int k = 0; k < listPupilPraiseClass.Count(); k++)
                            {
                                praiseType = listPupilPraiseClass[k].PraiseType.Resolution;
                                if (bounesSpecial.Contains(praiseType))
                                {
                                    lstBounesSpecial.Add(listPupilPraiseClass[k].Description);
                                }
                                else
                                {
                                    if (AreaPraiseID.Contains(listPupilPraiseClass[k].PraiseLevel))
                                    {
                                        lstBounesNormal.Add(listPupilPraiseClass[k].Description);
                                    }
                                }
                            }

                            if (lstBounesNormal.Count() > 0)
                            {
                                _strBounes = "'- Được giải thưởng trong các kỳ thi từ cấp huyện trở lên: " + string.Join(", ", lstBounesNormal.ToArray());
                                fourSheet.SetCellValue(startRowFourSheet + 7, 1, _strBounes);
                            }

                            if (lstBounesSpecial.Count() > 0)
                            {
                                _strBounes = "'- Khen thưởng đặc biệt khác: " + string.Join(", ", lstBounesSpecial.ToArray());
                                fourSheet.SetCellValue(startRowFourSheet + 9, 1, _strBounes);
                            }
                        }
                        #endregion

                        fourSheet.SetCellValue(startRowFourSheet + 14, 1, (pr2 != null && !string.IsNullOrEmpty(pr2.Comment) ? "Nhận xét: " + pr2.Comment : ""));

                        IVTRange rang = fourSheet.GetRange("A2", "J41");
                        sheet.CopyPasteSameSize(rang, (startRowTBM + 13), 1);
                        sheet.GetRange((startRowTBM + 12), 1, (startRowTBM + 12), 10).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);

                        int rowLast = (startRowTBM + 12) + 39;
                        sheet.FitAllColumnsOnOnePage = true;
                        sheet.SetBreakPage((startRowTBM + 14));
                        sheet.PageMaginBottom = 0.5;
                        sheet.PrintArea = "$A$1:$J$" + rowLast;
                        fourSheet.Delete();

                        if (fillPaging)
                        {
                            sheetPosition++;
                            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                            {
                                SheetIndex = sheetPosition,
                                IsHeaderOrFooter = true,
                                AlignPageNumber = 2,
                                StartPageNumberOfSheet = startPageNumberOfSheet,
                            });
                            startPageNumberOfSheet += 2;
                        }
                    }

                    //sheet.PageSize = VTXPageSize.VTxlPaperA4;
                    lstSpecializedSubject = new List<string>();
                }
                #endregion

                if (checkCurrentYear)
                {
                    firstSheet.Delete();
                    secondSheet.Delete();
                }
                CommentSheet.Delete();
                thirdSheet.Delete();
            }
            #endregion

            #region Cấp 2-3 GDTX
            if (transcripts.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX && (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY || transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY))
            {
                //Lấy Sheet Trang bìa
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                grade = "GDTX CẤP " + grade;
                //Xử lý thông tin trang bìa
                firstSheet.SetCellValue("A16", grade);
                firstSheet.SetCellValue("D24", fullName);
                firstSheet.SetCellValue("D25", schoolName);
                firstSheet.SetCellValue("C26", districtName);
                firstSheet.SetCellValue("H26", provinceName);
                if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY)
                {
                    firstSheet.SetCellValue("A36", (!string.IsNullOrEmpty(storageNumber) ? "SỐ: " + storageNumber + "/GDTX cấp THCS" : "SỐ:.............../GDTX cấp THCS"));
                }
                else if (transcripts.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
                {
                    firstSheet.SetCellValue("A36", (!string.IsNullOrEmpty(storageNumber) ? "SỐ: " + storageNumber + "/GDTX cấp THPT" : "SỐ................/GDTX cấp THPT"));
                }
                IVTWorksheet secondSheet = oBook.GetSheet(2);
                //Xử lý trang thông tin chung

                secondSheet.SetCellValue("C8", grade);
                secondSheet.SetCellValue("C10", fullName);
                secondSheet.SetCellValue("H10", Utils.Genre(genre));
                secondSheet.SetCellValue("C11", birthDate);
                secondSheet.SetCellValue("G11", birthPlace);
                secondSheet.SetCellValue("C12", ethnicName);
                secondSheet.SetCellValue("C14", !string.IsNullOrEmpty(tempResidentalAddress) ? tempResidentalAddress : permanentResidentalAddress);
                secondSheet.SetCellValue("C15", fatherFullName);
                secondSheet.SetCellValue("H15", fatherJob);
                secondSheet.SetCellValue("C16", motherFullName);
                secondSheet.SetCellValue("H16", motherJob);
                secondSheet.SetCellValue("E19", districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                secondSheet.SetCellValue("E24", sp != null ? sp.HeadMasterName : "");
                for (int ii = 0; ii < lstPupilOfClass.Count; ii++)
                {
                    secondSheet.SetCellValue(29 + ii, 1, lstPupilOfClass[ii].ClassProfile.DisplayName);
                    secondSheet.SetCellValue(29 + ii, 2, lstPupilOfClass[ii].AcademicYear.FirstSemesterStartDate.Value.ToString("dd/MM/yyyy"));
                    secondSheet.SetCellValue(29 + ii, 4, lstPupilOfClass[ii].AcademicYear.SecondSemesterEndDate.Value.ToString("dd/MM/yyyy"));
                    secondSheet.SetCellValue(29 + ii, 6, SplitSchoolName(lstPupilOfClass[ii].SchoolProfile.SchoolName));
                    secondSheet.SetCellValue(29 + ii, 10, storageNumber);
                }
                if (objPoc.Image == null || objPoc.Image.Length == 0)
                {
                    secondSheet.GetRange(3, 1, 8, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }


                // Lay toan bo thong tin diem cua hoc sinh nay
                var listSchoolAca = listClassProfile.Select(o => new { o.SchoolID, o.AcademicYearID, CreatedYear = o.AcademicYear.Year }).Distinct().ToList();

                List<SummedUpRecordBO> listSummedUpRecord = new List<SummedUpRecordBO>();
                List<PupilRankingBO> listPupilRanking = new List<PupilRankingBO>();
                foreach (var sa in listSchoolAca)
                {
                    listSummedUpRecord.AddRange(lstSummedUpRecordBOAll.Where(o =>
                                                o.SchoolID == sa.SchoolID && o.AcademicYearID == sa.AcademicYearID
                                                && o.PupilID == transcripts.PupilID
                                               ).ToList());
                    // Danh sach mon hoc

                    // Lay thong tin tong ket cua hoc sinh
                    listPupilRanking.AddRange(lstPupilRankingBOAll.Where(o =>
                        o.SchoolID == sa.SchoolID && o.AcademicYearID == sa.AcademicYearID
                        && o.PupilID == transcripts.PupilID
                                              ).ToList());
                }

                // Danh sach mon hoc
                List<ClassSubject> classSubject = classSubjectAll
                    .Where(o => listClassID.Contains(o.ClassID)).ToList();
                //Chiendd: 25.11.2015 Bo sung danh sach giao vien BM
                ClassSubject objClassSubject;


                List<TeachingAssignmentBO> lstTeachingBo = (from tc in iqTeachingAssignmen
                                                            join e in IqEmployee on tc.TeacherID equals e.EmployeeID
                                                            where listClassID.Contains(tc.ClassID.Value)
                                                            select new TeachingAssignmentBO
                                                            {
                                                                EmployeeCode = e.EmployeeCode,
                                                                Semester = tc.Semester,
                                                                TeacherID = e.EmployeeID,
                                                                TeacherName = e.FullName,
                                                                SubjectID = tc.SubjectID,
                                                                ClassID = tc.ClassID
                                                            }).ToList();

                // Lay thong tin khen thuong, ky luat cua hoc sinh
                List<PupilPraise> listPupilPraise = listPupilPraiseAll.Where(o => o.PupilID == transcripts.PupilID).ToList();

                //Xử lý các sheet khối
                IVTWorksheet tempSheet = oBook.GetSheet(3);
                IVTWorksheet fourSheet = oBook.GetSheet(4);
                IVTRange nomalSubjectRange = tempSheet.GetRange("A8", "J8");
                IVTRange optionSubjectRange = tempSheet.GetRange("A9", "J9");
                int currentSemesterId = 1;

                //lay ten giao vien chu nhiem
                // int headTeacherId  = 0; 
                for (int i = 0; i < listClassID.Count; i++)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet);

                    if (listClassID[i] != 0)
                    {
                        int ClassID = listClassID[i];

                        //Đặt lại tên sheet
                        ClassProfile classProfile = listClassProfile[i];
                        //Danh sach diem mon tinh diem
                        AcademicYear academic = lstAcaAll.Where(o => o.AcademicYearID == classProfile.AcademicYearID).FirstOrDefault();
                        if (academic.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academic.FirstSemesterEndDate)
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        }
                        else
                        {
                            currentSemesterId = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        }

                        //Fill ten giao vien chu nhiem
                        //headTeacherId = listClassProfile.Where(p => p.ClassProfileID == ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                        // Employee employeeObj = EmployeeBusiness.Find(headTeacherId);
                        // sheet.SetCellValue("B17", employeeObj != null ? employeeObj.FullName : string.Empty);

                        List<SummedUpRecordBO> lstSumUp = listSummedUpRecord.Where(o => o.ClassID == ClassID && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == classProfile.SchoolID).ToList();
                        sheet.Name = classProfile.DisplayName;
                        // Xuat ra thong tin nam hoc va truong
                        sheet.SetCellValue("C2", fullName);
                        sheet.SetCellValue("H2", classProfile.DisplayName);
                        sheet.SetCellValue("C3", schoolName);
                        sheet.SetCellValue("C4", districtName);
                        sheet.SetCellValue("H4", provinceName);

                        // xử lý Sheet        
                        List<ClassSubject> lstClassSubject = classSubject.Where(o => o.ClassID == ClassID).ToList();
                        List<ClassSubject> classSubjectNormal = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_MANDATORY).ToList();
                        List<ClassSubject> classSubjectOptional = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY || o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK).ToList();
                        List<ClassSubject> classSubjectEnglish2 = lstClassSubject.Where(o => o.IsSecondForeignLanguage == true).ToList();
                        List<ClassSubject> classSubjectApprenticeship = lstClassSubject.Where(o => o.AppliedType == SystemParamsInFile.SUBJECT_TYPE_APPRENTICESHIP).ToList();

                        //Neu o lop nay thang hoc sinh nay chuyen lop\

                        PupilOfClass classmove = lstPupilOfClass.Where(o => o.ClassID == ClassID).FirstOrDefault();
                        AcademicYear acatemp = lstAcaAll.Where(o => o.AcademicYearID == classmove.AcademicYearID).FirstOrDefault();
                        int Semester = 0;
                        if (classmove != null && classmove.EndDate != null)
                        {
                            if (classmove.EndDate > acatemp.FirstSemesterEndDate)
                            {
                                Semester = 2;
                            }
                            else
                            {
                                Semester = 1;
                            }
                        }
                        // Kiem tra học sinh
                        List<SummedUpRecordBO> summedUpRecord1 = lstSumUp
                            .Where(o => o.ClassID == ClassID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                            .ToList();

                        List<SummedUpRecordBO> summedUpRecord2 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord3 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                            .ToList();
                        List<SummedUpRecordBO> summedUpRecord4 = lstSumUp
                            .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            .ToList();
                        if (Semester == 1)
                        {
                            summedUpRecord1 = new List<SummedUpRecordBO>();
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        if (Semester == 2)
                        {
                            summedUpRecord2 = new List<SummedUpRecordBO>();
                            summedUpRecord3 = new List<SummedUpRecordBO>();
                            summedUpRecord4 = new List<SummedUpRecordBO>();
                        }
                        // ClassSubject Normal
                        int subNormalID;
                        int subOptionalID;
                        int subEnglishID;
                        int subApprenticeID;
                        int startRow = 8;
                        int rowCopy = 8;
                        int countToHidden = 0;
                        for (int j = 0; j < classSubjectNormal.Count; j++)
                        {
                            countToHidden++;
                            if (startRow + j > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + j, true);
                            }
                            sheet.SetCellValue(startRow + j, 1, classSubjectNormal[j].SubjectCat.DisplayName);
                            //Môn tính điểm
                            subNormalID = classSubjectNormal[j].SubjectID;
                            if (classSubjectNormal[j].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum1 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum2 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum3 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum4 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum1 != null && sum1.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum1.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum2 != null && sum2.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum2.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum4 != null && sum4.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum4.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + j, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum3 != null && sum3.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum3.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + j, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else  //Môn nhận xét
                            {
                                SummedUpRecordBO sum5 = summedUpRecord1.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum6 = summedUpRecord2.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum7 = summedUpRecord3.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                SummedUpRecordBO sum8 = summedUpRecord4.Where(o => o.SubjectID == subNormalID).FirstOrDefault();
                                if (sum5 != null)
                                {
                                    string judgementResult1 = sum5.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 3, judgementResult1);
                                }
                                if (sum6 != null)
                                {
                                    string judgementResult2 = sum6.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 4, judgementResult2);
                                }
                                if (sum8 != null)
                                {
                                    string judgementResult4 = sum8.JudgementResult;
                                    sheet.SetCellValue(startRow + j, 5, judgementResult4);
                                }
                                if (sum7 != null)
                                {
                                    string judgementResult3 = sum7.ReTestJudgement;
                                    sheet.SetCellValue(startRow + j, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectNormal[j];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + j, 7, sheet, currentSemesterId);
                        }
                        startRow += classSubjectNormal.Count;
                        // Môn tiếng anh 2

                        for (int n = 0; n < classSubjectEnglish2.Count; n++)
                        {
                            countToHidden++;
                            if (startRow + n > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + n, true);
                            }
                            sheet.SetCellValue(startRow + n, 1, classSubjectEnglish2[n].SubjectCat.DisplayName);
                            subEnglishID = classSubjectEnglish2[n].SubjectID;

                            if (classSubjectEnglish2[n].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum17 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum18 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum19 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum20 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum17 != null && sum17.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum17.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum18 != null && sum18.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum18.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum20 != null && sum20.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum20.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + n, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum19 != null && sum19.ReTestMark != null)
                                {
                                    decimal summedUpMark3 = sum19.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + n, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum21 = summedUpRecord1.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum22 = summedUpRecord2.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum23 = summedUpRecord3.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                SummedUpRecordBO sum24 = summedUpRecord4.Where(o => o.SubjectID == subEnglishID).FirstOrDefault();
                                if (sum21 != null)
                                {
                                    string judgementResult1 = sum21.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 3, judgementResult1);
                                }
                                if (sum22 != null)
                                {
                                    string judgementResult2 = sum22.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 4, judgementResult2);
                                }
                                if (sum24 != null)
                                {
                                    string judgementResult4 = sum24.JudgementResult;
                                    sheet.SetCellValue(startRow + n, 5, judgementResult4);
                                }
                                if (sum23 != null)
                                {
                                    string judgementResult3 = sum23.ReTestJudgement;
                                    sheet.SetCellValue(startRow + n, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectEnglish2[n];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + n, 7, sheet, currentSemesterId);
                        }
                        startRow += classSubjectEnglish2.Count;
                        // Môn nghề

                        for (int m = 0; m < classSubjectApprenticeship.Count; m++)
                        {
                            countToHidden++;
                            if (startRow + m > rowCopy)
                            {
                                sheet.CopyAndInsertARow(rowCopy, startRow + m, true);
                            }
                            sheet.SetCellValue(startRow + m, 1, classSubjectApprenticeship[m].SubjectCat.DisplayName);
                            subApprenticeID = classSubjectApprenticeship[m].SubjectID;
                            if (classSubjectApprenticeship[m].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum25 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum26 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum27 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum28 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum25 != null && sum25.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum25.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum26 != null && sum26.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum26.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum28 != null && sum28.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum28.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + m, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum27 != null && sum27.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum27.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + m, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else
                            {
                                SummedUpRecordBO sum29 = summedUpRecord1.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum30 = summedUpRecord2.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum31 = summedUpRecord3.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                SummedUpRecordBO sum32 = summedUpRecord4.Where(o => o.SubjectID == subApprenticeID).FirstOrDefault();
                                if (sum29 != null)
                                {
                                    string judgementResult1 = sum29.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 3, judgementResult1);
                                }
                                if (sum30 != null)
                                {
                                    string judgementResult2 = sum30.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 4, judgementResult2);
                                }
                                if (sum32 != null)
                                {
                                    string judgementResult4 = sum32.JudgementResult;
                                    sheet.SetCellValue(startRow + m, 5, judgementResult4);
                                }
                                if (sum31 != null)
                                {
                                    string judgementResult3 = sum31.ReTestJudgement;
                                    sheet.SetCellValue(startRow + m, 6, judgementResult3);
                                }
                            }
                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectApprenticeship[m];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + m, 7, sheet, currentSemesterId);
                        }

                        startRow += classSubjectApprenticeship.Count;

                        // Class Subject Optional

                        for (int k = 0; k < classSubjectOptional.Count; k++)
                        {
                            countToHidden++;
                            if (k > 0)
                            {
                                sheet.CopyAndInsertARow(startRow, startRow + k, true);
                            }
                            sheet.SetCellValue(startRow + k, 2, classSubjectOptional[k].SubjectCat.DisplayName);
                            subOptionalID = classSubjectOptional[k].SubjectID;
                            //Môn tính điểm
                            if (classSubjectOptional[k].IsCommenting != 1)
                            {
                                SummedUpRecordBO sum9 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum10 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum11 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum12 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum9 != null && sum9.SummedUpMark != null)
                                {
                                    decimal summedUpMark1 = sum9.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 3, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark1, false));
                                }
                                if (sum10 != null && sum10.SummedUpMark != null)
                                {
                                    decimal summedUpMark2 = sum10.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 4, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark2, false));
                                }
                                if (sum12 != null && sum12.SummedUpMark != null)
                                {
                                    decimal summedUpMark4 = sum12.SummedUpMark.Value;
                                    sheet.SetCellValue(startRow + k, 5, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark4, false));
                                }
                                if (sum11 != null && sum11.SummedUpMark != null)
                                {
                                    decimal summedUpMark3 = sum11.ReTestMark.Value;
                                    sheet.SetCellValue(startRow + k, 6, ReportUtils.ConvertMarkForV_SGTGD(summedUpMark3, false));
                                }
                            }
                            else //Môn nhận xét
                            {
                                SummedUpRecordBO sum13 = summedUpRecord1.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum14 = summedUpRecord2.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum15 = summedUpRecord3.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                SummedUpRecordBO sum16 = summedUpRecord4.Where(o => o.SubjectID == subOptionalID).FirstOrDefault();
                                if (sum13 != null)
                                {
                                    string judgementResult1 = sum13.JudgementResult;
                                    sheet.SetCellValue(startRow + k, 3, judgementResult1);
                                }
                                if (sum14 != null)
                                {
                                    string judgementResult2 = sum14.JudgementResult;
                                    sheet.SetCellValue(startRow + k, 4, judgementResult2);
                                }
                                if (sum16 != null)
                                {
                                    string judgementResult4 = sum16.JudgementResult;
                                    sheet.SetCellValue(startRow + k, 5, judgementResult4);
                                }
                                if (sum15 != null)
                                {
                                    string judgementResult3 = sum15.ReTestJudgement;
                                    sheet.SetCellValue(startRow + k, 6, judgementResult3);
                                }
                            }

                            //Dien thong tin giao vien BM
                            objClassSubject = classSubjectOptional[k];
                            SetTeachingAssignment(objClassSubject, lstTeachingBo, startRow + k, 7, sheet, currentSemesterId);
                        }
                        if (classSubjectOptional.Count > 1)
                        {
                            sheet.MergeColumn('A', startRow, startRow + classSubjectOptional.Count - 1);
                            startRow += classSubjectOptional.Count;
                        }
                        else
                        {
                            // Neu ko he co du lieu thi xoa dong 5
                            if (startRow == rowCopy)
                            {
                                sheet.DeleteRow(startRow);
                            }
                            // Giu nguyen dong tu chon
                            startRow += 1;
                        }

                        // Thong tin tong ket
                        PupilRankingBO pr1 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                        PupilRankingBO pr2 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                        int checkstudy = 0;
                        PupilRankingBO pr3 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                        if (pr3 != null && pr3.StudyingJudgementID.HasValue)
                        {
                            checkstudy = pr3.StudyingJudgementID.Value;
                        }
                        //Thi lai -> Fill vao hoc luc
                        PupilRankingBO pr4 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).FirstOrDefault();
                        //Ren luyen lai -> Fill vao hanh kiem
                        PupilRankingBO pr5 = listPupilRanking.Where(o => o.ClassID == listClassID[i] && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && checkstudy == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING).FirstOrDefault();
                        if (Semester == 1)
                        {
                            pr1 = null;
                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        if (Semester == 2)
                        {

                            pr2 = null;
                            pr3 = null;
                            pr4 = null;
                            pr5 = null;
                        }
                        SchoolProfile spe = lstSchoolAll.Where(o => o.SchoolProfileID == classProfile.SchoolID).FirstOrDefault();
                        //tutv4
                        //fill ten giao vien va sheet nhan xet truoc khi copy                        
                        int headTeacherId = classProfile.HeadTeacherID.HasValue ? classProfile.HeadTeacherID.Value : 0;
                        Employee employeeObj = null;
                        if (headTeacherId > 0)
                        {
                            employeeObj = lstEmployeeAll.Where(o => o.EmployeeID == headTeacherId).FirstOrDefault();
                            fourSheet.SetCellValue("G28", employeeObj != null ? employeeObj.FullName : string.Empty);
                        }
                        fourSheet.SetCellValue("F39", districtName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        fourSheet.SetCellValue("F47", sp != null ? sp.HeadMasterName : "");
                        IVTRange rang = fourSheet.GetRange("A2", "J49");
                        sheet.CopyPasteSameSize(rang, 54, 1);
                        sheet.PrintArea = "$A$1:$J$101";
                        int startRowTBM = startRow;

                        //Fill thong tin diem TB cac mon sau khi thi lai
                        if (pr4 != null)
                        {
                            if (pr4.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr4.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }
                        if (pr5 != null)
                        {
                            if (pr5.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr5.AverageMark.Value;
                                sheet.SetCellValue(startRow, 6, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                        }

                        startRow = 22;


                        //Thong tin truong + tinh thanh
                        sheet.SetCellValue(startRow + 32, 3, fullName);
                        sheet.SetCellValue(startRow + 33, 3, classProfile.DisplayName);
                        sheet.SetCellValue(startRow + 32, 8, classProfile.AcademicYear.FirstSemesterStartDate.Value.ToString("dd/MM/yyyy"));
                        sheet.SetCellValue(startRow + 33, 8, classProfile.AcademicYear.SecondSemesterEndDate.Value.ToString("dd/MM/yyyy"));
                        if (pr1 != null)
                        {
                            if (pr1.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr1.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 3, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow + 38, 2, pr1.CapacityLevelID != null ? pr1.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 38, 3, pr1.ConductLevelID != null ? pr1.ConductLevelName : string.Empty);
                        }
                        if (pr2 != null)
                        {
                            if (pr2.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr2.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 4, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow + 39, 2, pr2.CapacityLevelID != null ? pr2.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 39, 3, pr2.ConductLevelID != null ? pr2.ConductLevelName : string.Empty);
                        }
                        if (pr3 != null)
                        {
                            if (pr3.AverageMark.HasValue)
                            {
                                decimal AverageMark = pr3.AverageMark.Value;
                                sheet.SetCellValue(startRowTBM, 5, ReportUtils.ConvertMarkForV_SGTGD(AverageMark, false));
                            }
                            sheet.SetCellValue(startRow + 40, 2, pr3.CapacityLevelID != null ? pr3.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 40, 3, pr3.ConductLevelID != null ? pr3.ConductLevelName : string.Empty);
                        }
                        //fill ten giao vien vao sheet thong tin lop
                        sheet.MergeRow(startRowTBM + 11, 2, 3);
                        sheet.GetRange(startRowTBM + 11, 2, startRowTBM + 11, 3).SetHAlign(VTHAlign.xlHAlignCenter);
                        // So ngay nghi

                        List<int> lstSectionKeyID = classProfile.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(classProfile.SeperateKey.Value) : new List<int>();

                        //Danh sach cac buoi nghi
                        List<PupilAbsence> pupilAbsenceList = pupilAbsenceListAll.Where(o => o.ClassID == ClassID
                            && o.AcademicYearID == classProfile.AcademicYearID && o.SchoolID == transcripts.SchoolID
                            && o.PupilID == transcripts.PupilID).Where(p => lstSectionKeyID.Contains(p.Section)).ToList();
                        int countAbsent_P_HK1 = 0, countAbsent_KP_HK1 = 0, countAbsent_P_HK2 = 0, countAbsent_KP_HK2 = 0, countAbsent_P = 0, countAbsent_KP = 0;
                        if (pupilAbsenceList != null && pupilAbsenceList.Count > 0)
                        {
                            for (int j = 0; j < pupilAbsenceList.Count; j++)
                            {
                                if (pupilAbsenceList[j].AbsentDate >= classProfile.AcademicYear.FirstSemesterStartDate && pupilAbsenceList[j].AbsentDate <= classProfile.AcademicYear.FirstSemesterEndDate)
                                {
                                    if (pupilAbsenceList[j].IsAccepted.HasValue && pupilAbsenceList[j].IsAccepted.Value)
                                        countAbsent_P_HK1++;
                                    else
                                        countAbsent_KP_HK1++;
                                }
                                else
                                {
                                    if (pupilAbsenceList[j].IsAccepted.HasValue && pupilAbsenceList[j].IsAccepted.Value)
                                        countAbsent_P_HK2++;
                                    else
                                        countAbsent_KP_HK2++;
                                }
                            }
                        }
                        countAbsent_P = countAbsent_P_HK1 + countAbsent_P_HK2;
                        countAbsent_KP = countAbsent_KP_HK1 + countAbsent_KP_HK2;
                        sheet.SetCellValue(startRow + 38, 4, countAbsent_P_HK1 == 0 ? "" : countAbsent_P_HK1.ToString());
                        sheet.SetCellValue(startRow + 38, 5, countAbsent_KP_HK1 == 0 ? "" : countAbsent_KP_HK1.ToString());
                        sheet.SetCellValue(startRow + 39, 4, countAbsent_P_HK2 == 0 ? "" : countAbsent_P_HK2.ToString());
                        sheet.SetCellValue(startRow + 39, 5, countAbsent_KP_HK2 == 0 ? "" : countAbsent_KP_HK2.ToString());
                        sheet.SetCellValue(startRow + 40, 4, countAbsent_P == 0 ? "" : countAbsent_P.ToString());
                        sheet.SetCellValue(startRow + 40, 5, countAbsent_KP == 0 ? "" : countAbsent_KP.ToString());

                        //Trong 1 ngày mà học sinh có số buổi nghỉ học >1 thì chỉ tính là 1 và loại nghỉ giống nhau.
                        //Nếu học sinh trong 1 ngày có số buổi nghỉ học >1 mà loại nghỉ là khác nhau thì ưu tiên lấy thông tin nghỉ học của học sinh theo thứ tự sáng, chiều, tối

                        //DateTime dateAbsence;
                        //if (pupilAbsenceList != null && pupilAbsenceList.Count > 0)
                        //{
                        //    List<DateTime> listDateAbsenceDistinct = pupilAbsenceList.Select(p => p.AbsentDate).Distinct().ToList();
                        //    List<PupilAbsence> pupilAbsenceListTmp = new List<PupilAbsence>();

                        //    ///For qua cac ngay co diem danh 
                        //    for (int j = 0; j < listDateAbsenceDistinct.Count; j++)
                        //    {
                        //        dateAbsence = listDateAbsenceDistinct[j];
                        //        // lay loai nghi P,K
                        //        pupilAbsenceListTmp = pupilAbsenceList.Where(p => p.AbsentDate == dateAbsence).ToList();
                        //        if (pupilAbsenceListTmp != null && pupilAbsenceListTmp.Count > 0)
                        //        {
                        //            totalAbsentDay++;
                        //        }
                        //    }
                        //}

                        //totalAbsentDay = PupilAbsenceBusiness.SearchBySchool(classProfile.SchoolID, dicabsen).Count();
                        // Xet len lop hay o lai
                        if (pr3 != null)
                        {
                            if (pr3.StudyingJudgementID.HasValue)
                            {
                                if (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS)
                                {
                                    sheet.SetCellValue(startRow + 36, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                                }
                                else if (pr3.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                {
                                    sheet.SetCellValue(startRow + 40, 8, pr3.StudyingJudgementID != null ? pr3.StudyingJudgementResolution : string.Empty);
                                }
                                else
                                {
                                    if (pr5 != null)
                                    {
                                        sheet.SetCellValue(startRow + 40, 8, pr5.StudyingJudgementID != null ? pr5.StudyingJudgementResolution : string.Empty);
                                    }
                                    else if (pr4 != null)
                                    {
                                        if (pr4.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS)
                                        {
                                            sheet.SetCellValue(startRow + 38, 8, pr4.StudyingJudgementID != null ? pr4.StudyingJudgementResolution : string.Empty);
                                        }
                                        else if (pr4.StudyingJudgementID.Value == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                        {
                                            sheet.SetCellValue(startRow + 40, 8, pr4.StudyingJudgementID != null ? pr4.StudyingJudgementResolution : string.Empty);
                                        }
                                    }
                                }
                            }
                        }
                        if (pr4 != null)
                        {
                            sheet.SetCellValue(startRow + 40, 6, pr4.CapacityLevelID != null ? pr4.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 40, 7, pr4.ConductLevelID != null ? pr4.ConductLevelName : string.Empty);
                        }
                        if (pr5 != null)
                        {
                            sheet.SetCellValue(startRow + 40, 6, pr5.CapacityLevelID != null ? pr5.CapacityLevel : "");
                            sheet.SetCellValue(startRow + 40, 7, pr5.ConductLevelID != null ? pr5.ConductLevelName : string.Empty);
                        }
                        //ducpt1 20160723 bo sung 
                        //Danh hieu thi dua
                        PupilEmulation ppEmulation = PupilEmulationBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.AcademicYearID == classProfile.AcademicYearID).FirstOrDefault();
                        sheet.SetCellValue(startRow + 42, 4, ppEmulation != null ? ppEmulation.HonourAchivementType.Resolution : string.Empty);
                        //Khen thuong ky luat
                        var pupilPraises = PupilPraiseBusiness.SearchBySchool(transcripts.SchoolID, new Dictionary<string, object> { { "AcademicYearID", classProfile.AcademicYearID }, { "PupilID", transcripts.PupilID } });
                        var pupilDisciplines = PupilDisciplineBusiness.SearchBySchool(transcripts.SchoolID, new Dictionary<string, object> { { "AcademicYearID", classProfile.AcademicYearID }, { "PupilID", transcripts.PupilID } });
                        string[] lstPupilPraise = (from pb in PupilPraiseBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.AcademicYearID == classProfile.AcademicYearID)
                                                   join ppT in PraiseTypeBusiness.All on pb.PraiseTypeID equals ppT.PraiseTypeID
                                                   select ppT.Resolution).ToArray();
                        string[] lstPupilDiscipline = (from pd in PupilDisciplineBusiness.All.Where(o => o.PupilID == transcripts.PupilID && o.AcademicYearID == classProfile.AcademicYearID)
                                                       join dtb in DisciplineTypeBusiness.All on pd.DisciplineTypeID equals dtb.DisciplineTypeID
                                                       select dtb.Resolution).ToArray();
                        lstPupilPraise = lstPupilPraise.Concat(lstPupilDiscipline).ToArray();
                        sheet.SetCellValue(startRow + 43, 4, lstPupilPraise != null ? string.Join(", ", lstPupilPraise) : string.Empty);
                        // Lay thong tin khen thuong khac tu cap huyen tro len
                        List<PupilPraise> listPupilPraiseClass = listPupilPraise.Where(o => o.ClassID == listClassID[i]).ToList();
                        if (listPupilPraiseClass != null && listPupilPraiseClass.Count > 0)
                        {
                            string strPraise = string.Join(", ", listPupilPraiseClass.Select(o => o.Description));
                            if (strPraise.Length > strDotLen)
                            {
                                string[] arrW = strPraise.Split(' ');
                                string firstPraise = " ";
                                int totalLen = 0;
                                int pos = -1;
                                for (int j = 0; j < arrW.Length; j++)
                                {
                                    string w = arrW[j].Trim();
                                    if (totalLen <= strDotLen && totalLen + w.Length + 1 > strDotLen)
                                    {
                                        sheet.SetCellValue(startRow + 40, 1, praiseTitle + firstPraise);
                                        pos = j;
                                        break;
                                    }
                                    firstPraise += w + " ";
                                    totalLen += w.Length + 1;
                                }
                                if (pos >= 0)
                                {
                                    string secondPraise = "";
                                    for (int j = pos; j < arrW.Length; j++)
                                    {
                                        secondPraise += arrW[j] + " ";
                                    }
                                    sheet.SetCellValue(startRow + 41, 1, secondPraise);
                                }
                            }
                            else
                            {
                                sheet.SetCellValue(startRow + 40, 1, praiseTitle + " " + strPraise);
                            }
                        }
                        sheet.FitAllColumnsOnOnePage = true;

                        if (countToHidden == 16)
                        {
                            sheet.DeleteRow(45);
                            sheet.DeleteRow(46);
                            sheet.SetBreakPage(52);
                            sheet.GetRange(51, 1, 51, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                        }
                        else if (countToHidden == 17)
                        {
                            sheet.DeleteRow(45);
                            sheet.DeleteRow(46);
                            sheet.DeleteRow(47);
                            sheet.DeleteRow(48);
                            sheet.SetBreakPage(50);
                            sheet.GetRange(49, 1, 49, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                        }
                        else if (countToHidden == 18)
                        {
                            sheet.DeleteRow(38);
                            sheet.DeleteRow(39);
                            sheet.DeleteRow(40);
                            sheet.DeleteRow(41);
                            sheet.DeleteRow(42);
                            sheet.DeleteRow(43);
                            sheet.SetBreakPage(48);
                            sheet.GetRange(47, 1, 47, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                        }
                        else
                        {
                            sheet.SetBreakPage(54);
                        }
                    }
                    sheet.PageSize = VTXPageSize.VTxlPaperA4;
                }
                tempSheet.Delete();
                fourSheet.Delete();
            }
            #endregion
        }

        public ProcessedReport InsertTranscriptsReport(Transcripts entity, Stream data, bool isZip = false)
        {
            string reportCode = isZip ? (entity.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX_ZIPFILE : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_ZIPFILE) : (entity.TrainingType == SystemParamsInFile.TRAINING_TYPE_GDTX ? SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23_GDTX : SystemParamsInFile.REPORT_INHOCBATHEOMAUCAP23TT58);
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyTranscriptsReport(entity);
            pr.ReportData = isZip ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            if (!isZip)
            {
                string PupilName = Utils.StripVNSignAndSpace(PupilProfileBusiness.Find(entity.PupilID).FullName);
                outputNamePattern = outputNamePattern.Replace("[PupilName]", PupilName);
            }
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"PupilID", entity.PupilID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public string GetHashKeyTranscriptsReport(Transcripts entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"PupilID",entity.PupilID}
            };
            return ReportUtils.GetHashKey(dic);
        }

        #endregion
        /// <summary>
        /// Dien thong tin giao vien BM
        /// </summary>
        /// <param name="objClassSubject"></param>
        /// <param name="lstTeachingBo"></param>
        /// <param name="startRow"></param>
        /// <param name="sheet"></param>
        private void SetTeachingAssignment(ClassSubject objClassSubject, List<TeachingAssignmentBO> lstTeachingBo, int startRow, int column, IVTWorksheet sheet, int semesterId)
        {
            //Dien thong tin giao vien BM
            TeachingAssignmentBO objTeachingAssignmentBO;
            //Neu hoc ca 2 hoc ky hoac chi hoc hoc ky II thi lay thong tin giao vien hoac chi hoc ky 2
            if (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                objTeachingAssignmentBO = lstTeachingBo.FirstOrDefault(t => t.ClassID == objClassSubject.ClassID
                                                                        && t.SubjectID == objClassSubject.SubjectID
                                                                        && t.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                sheet.SetCellValue(startRow, column, objTeachingAssignmentBO != null ? objTeachingAssignmentBO.TeacherName : "");
            }
            else if ((objClassSubject.SectionPerWeekFirstSemester > 0 && objClassSubject.SectionPerWeekSecondSemester > 0)
                || (!objClassSubject.SectionPerWeekFirstSemester.HasValue || objClassSubject.SectionPerWeekFirstSemester == 0)
                )
            {
                objTeachingAssignmentBO = lstTeachingBo.FirstOrDefault(t => t.ClassID == objClassSubject.ClassID
                                                                        && t.SubjectID == objClassSubject.SubjectID
                                                                        && t.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                sheet.SetCellValue(startRow, column, objTeachingAssignmentBO != null ? objTeachingAssignmentBO.TeacherName : "");

            }
            //Neu chi hoc hoc ky I thi dien giao vien day hoc ky II
            else if (objClassSubject.SectionPerWeekFirstSemester > 0
                      && (!objClassSubject.SectionPerWeekSecondSemester.HasValue || objClassSubject.SectionPerWeekSecondSemester == 0)
                   )
            {
                objTeachingAssignmentBO = lstTeachingBo.FirstOrDefault(t => t.ClassID == objClassSubject.ClassID
                                                                       && t.SubjectID == objClassSubject.SubjectID
                                                                       && t.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                sheet.SetCellValue(startRow, column, objTeachingAssignmentBO != null ? objTeachingAssignmentBO.TeacherName : "");
            }
            else
            {
                sheet.SetCellValue(startRow, column, "");
            }
        }
        private string getCapacitybyMark(int appliedLevel, decimal summedUpMark)
        {
            if (appliedLevel == 1)
            {
                if (summedUpMark >= 9)
                {
                    return SystemParamsInFile.REPORT_CAPACITYBYMARK_EXCELLENT;
                }
                else if (summedUpMark >= 7 && summedUpMark < 9)
                {
                    return SystemParamsInFile.REPORT_CAPACITYBYMARK_GOOD;
                }
                else if (summedUpMark >= 5 && summedUpMark < 7)
                {
                    return SystemParamsInFile.REPORT_CAPACITYBYMARK_NORMAL;
                }
                else
                {
                    return SystemParamsInFile.REPORT_CAPACITYBYMARK_BAD;
                }
            }
            else
            {
                return null;
            }
        }

        #region QuangNN2 - Bao cao Hoc ba tieu hoc - 06/04/2015

        private string CutSchoolName(string schoolName)
        {
            string result = string.Empty;
            string[] arrName = schoolName.Split(' ');
            if (arrName[0].ToUpper().Contains("TRƯỜNG"))
            {
                for (int i = 1; i < arrName.Length; i++)
                {
                    result = result + arrName[i] + " ";
                }
            }
            else
            {
                result = schoolName;
            }

            return result.Trim();
        }

        private string CutClassName(string className)
        {
            string result = string.Empty;
            string[] arrName = className.Split(' ');
            if (arrName[0].ToUpper().Contains("LỚP"))
            {
                for (int i = 1; i < arrName.Length; i++)
                {
                    result = result + arrName[i] + " ";
                }
            }
            else
            {
                result = className;
            }

            return result.Trim();
        }

        private string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport GetReportPrimaryTranscripts(IDictionary<string, object> dic, bool isZip = false)
        {
            string reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_TT22;
            if (isZip)
            {
                reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_ZIPFILE;
            }
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReportPrimaryTranscripts(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partion = UtilsBusiness.GetPartionId(schoolID);
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");
            long pupilID = Utils.GetInt(dic, "PupilProfileID");
            bool fillPaging = Utils.GetBool(dic, "fillPaging");
            int startPageNumberOfSheet = 1;
            int sheetPosition = 3;

            List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            // Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_TT22;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;

            // Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);
            IVTWorksheet fifthSheet = oBook.GetSheet(5);
            IVTWorksheet sixthSheet = oBook.GetSheet(6);
            IVTWorksheet seventhTT22Sheet = oBook.GetSheet(7);
            IVTWorksheet eightTT30Sheet = oBook.GetSheet(8);
            IVTWorksheet nineTT30Sheet = oBook.GetSheet(9);
            #region Trang bia
            // Lấy dữ liệu Thông tin học sinh
            PupilProfile pupil = PupilProfileBusiness.Find(pupilID);

            // Fill dữ liệu trang bìa
            if (pupil != null)
            {
                firstSheet.SetCellValue("C28", pupil.FullName.ToUpper());
                SchoolProfile schoolPupil = pupil.SchoolProfile;
                if (schoolPupil != null)
                {
                    schoolPupil.SchoolName = CutSchoolName(schoolPupil.SchoolName);
                    firstSheet.SetCellValue("C29", schoolPupil.SchoolName);

                    double heigthSN = SetRowHeigth(schoolPupil.SchoolName, 24, 25, 25);
                    firstSheet.SetRowHeight(26, heigthSN);
                    firstSheet.SetCellValue("C30", schoolPupil.Commune != null ? pupil.SchoolProfile.Commune.CommuneName : string.Empty);
                    firstSheet.SetCellValue("C31", schoolPupil.District != null ? pupil.SchoolProfile.District.DistrictName : string.Empty);
                    firstSheet.SetCellValue("C32", schoolPupil.Province != null ? pupil.SchoolProfile.Province.ProvinceName : string.Empty);
                }

            }

            // Chỉnh chiều cao tự động cho một số ô trong Trang bìa

            #endregion

            #region Trang 1
            // Lấy thông tin Quá trình học tập
            IQueryable<PupilOfSchoolBO> queryPOC = (from poc in PupilOfClassBusiness.All.Where(poc => poc.PupilID == pupilID && poc.SchoolID == schoolID)
                                                    join pf in PupilProfileBusiness.All.Where(p => p.PupilProfileID == pupilID) on poc.PupilID equals pf.PupilProfileID
                                                    join ay in AcademicYearBusiness.All.Where(s => s.SchoolID == schoolID) on poc.AcademicYearID equals ay.AcademicYearID
                                                    join cp in ClassProfileBusiness.All.Where(s => s.SchoolID == schoolID) on poc.ClassID equals cp.ClassProfileID
                                                    join pos in PupilOfSchoolBusiness.All.Where(o => o.PupilID == pupilID && o.SchoolID == schoolID) on pf.PupilProfileID equals pos.PupilID
                                                    where cp.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && cp.EducationLevelID <= educationLevelID
                                                    && ay.IsActive == true
                                                    && cp.IsActive == true
                                                    && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS

                                                    select new PupilOfSchoolBO
                                                    {
                                                        AcademicYearID = poc.AcademicYearID,
                                                        Year = poc.Year ?? 0,
                                                        DisplayAcademicYear = ay.DisplayTitle,
                                                        ClassID = poc.ClassID,
                                                        ClassName = cp.DisplayName,
                                                        SchoolID = poc.SchoolID,
                                                        SchoolName = poc.SchoolProfile.SchoolName,
                                                        EnrolmentDate = pos.EnrolmentDate,
                                                        EnrolmentType = pos.EnrolmentType,
                                                        StorageName = pf.StorageNumber,
                                                        EducationLevelId = cp.EducationLevelID
                                                    }).OrderBy(o => o.Year);


            // Fill dữ liệu Trang 1
            if (pupil != null)
            {
                thirdSheet.SetRowHeight(1, 0);
                thirdSheet.SetCellValue("C3", pupil.FullName.ToUpper());
                thirdSheet.SetCellValue("H3", pupil.Genre == GlobalConstants.GENRE_MALE ? "Nam" : "Nữ");
                thirdSheet.SetCellValue("C4", pupil.BirthDate.ToString("dd/MM/yyyy"));
                thirdSheet.SetCellValue("F4", pupil.Ethnic != null ? pupil.Ethnic.EthnicName : string.Empty);
                thirdSheet.SetCellValue("H4", "Việt Nam");
                thirdSheet.SetCellValue("C5", pupil.BirthPlace);
                thirdSheet.SetCellValue("G5", pupil.HomeTown);
                thirdSheet.SetCellValue("C6", !string.IsNullOrEmpty(pupil.TempResidentalAddress) ? pupil.TempResidentalAddress : pupil.PermanentResidentalAddress);
                thirdSheet.SetCellValue("C7", pupil.FatherFullName);
                thirdSheet.SetCellValue("C8", pupil.MotherFullName);
                thirdSheet.SetCellValue("C9", pupil.SponsorFullName);
                thirdSheet.SetCellValue("E20", school.HeadMasterName);
            }
            if (fillPaging)
            {
                lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                {
                    SheetIndex = sheetPosition,
                    IsHeaderOrFooter = true,
                    AlignPageNumber = 2,
                    StartPageNumberOfSheet = startPageNumberOfSheet
                });
            }
            // Chỉnh chiều cao tự động cho một số ô trong Trang 1

            // Fill địa điểm, ngày
            DateTime current = DateTime.Now;
            string strDate = string.Empty;
            string schoolProvince = pupil.SchoolProfile.District != null ? pupil.SchoolProfile.District.DistrictName : ".........";
            strDate = schoolProvince + ", ngày " + pupil.EnrolmentDate.Day + " tháng " + pupil.EnrolmentDate.Month + " năm " + pupil.EnrolmentDate.Year;
            thirdSheet.SetCellValue("E11", strDate);

            int startRowQTHT = 25;
            IVTRange row = thirdSheet.GetRow(startRowQTHT);

            List<PupilOfSchoolBO> listPOS = queryPOC.ToList();

            PupilOfSchoolBO posTemp = null;
            for (int i = 0; i < listPOS.Count; i++)
            {
                posTemp = listPOS[i];
                posTemp.SchoolName = CutSchoolName(posTemp.SchoolName);
                posTemp.ClassName = CutClassName(posTemp.ClassName);
                if (i > 7)
                {
                    thirdSheet.CopyPasteSameSize(row, startRowQTHT + 1, 1);
                }
                thirdSheet.SetCellValue("A" + (startRowQTHT + 1).ToString(), posTemp.DisplayAcademicYear);
                thirdSheet.SetCellValue("B" + (startRowQTHT + 1).ToString(), posTemp.ClassName);
                thirdSheet.SetCellValue("C" + (startRowQTHT + 1).ToString(), posTemp.SchoolName);
                // Tính chiều cao cho ô tên lớp và tên trường, lấy chiều cao lớn hơn
                double heigthCN = SetRowHeigth(posTemp.ClassName, 6, 24, 24);
                double heigthSN = SetRowHeigth(posTemp.SchoolName, 30, 24, 24);
                thirdSheet.SetRowHeight(startRowQTHT + 1, heigthCN > heigthSN ? heigthCN : heigthSN);

                thirdSheet.SetCellValue("G" + (startRowQTHT + 1).ToString(), posTemp.StorageName);
                thirdSheet.SetCellValue("H" + (startRowQTHT + 1).ToString(), posTemp.EnrolmentDate != null ? posTemp.EnrolmentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                startRowQTHT++;
            }
            // Xóa dòng template mẫu
            thirdSheet.DeleteRow(25);
            thirdSheet.PrintArea = "$A$1:$H$32";
            #endregion

            #region Trang 4 5 6
            #region Lay du lieu chung
            // Lấy danh sách năm và học kì cần báo cáo
            /*List<AcademicYearBO> listAcademicYear = (from ay in AcademicYearBusiness.All
                                                     join poc in PupilOfClassBusiness.All.Where(o => o.PupilID == pupilID && o.Status == GlobalConstants.PUPIL_STATUS_STUDYING) on ay.AcademicYearID equals poc.AcademicYearID
                                                     join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                     where cp.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && cp.EducationLevelID <= educationLevelID
                                                     select new AcademicYearBO
                                                     {
                                                         AcademicYearID = ay.AcademicYearID,
                                                         Year = ay.Year,
                                                         DisplayTitle = ay.DisplayTitle,
                                                         SchoolID = poc.SchoolID,
                                                         SchoolName = ay.SchoolProfile.SchoolName,
                                                         ClassID = poc.ClassID,
                                                         ClassName = cp.DisplayName,
                                                         EducationLevelID = cp.EducationLevelID,
                                                         FirstSemesterStartDate = ay.FirstSemesterStartDate,
                                                         FirstSemesterEndDate = ay.FirstSemesterEndDate,
                                                         SecondSemesterStartDate = ay.SecondSemesterStartDate,
                                                         SecondSemesterEndDate = ay.SecondSemesterEndDate,
                                                     }).OrderBy(o => o.Year).ToList();
                                                     */
            List<int> lstAcademicYearID = listPOS.Select(p => p.AcademicYearID).Distinct().ToList();
            List<int> listClassId = listPOS.Select(p => p.ClassID).Distinct().ToList();

            // Lấy tất cả các thông số về sức khỏe và số ngày nghỉ của học sinh
            List<InfoEvaluationPupil> queryInfoPupil = (from pt in PhysicalTestBusiness.All
                                                        join mn in MonitoringBookBusiness.All on pt.MonitoringBookID equals mn.MonitoringBookID
                                                        where mn.PupilID == pupilID

                                                        orderby mn.MonitoringDate descending
                                                        select new InfoEvaluationPupil
                                                        {
                                                            PupilID = mn.PupilID,
                                                            Height = pt.Height.Value,
                                                            Weight = pt.Weight.Value,
                                                            EvaluationHealth = pt.PhysicalClassification.Value,
                                                            AcademicYear = mn.AcademicYearID,
                                                            SchoolID = mn.SchoolID,
                                                            ClassID = mn.ClassID,
                                                            MonitoringDate = mn.MonitoringDate,
                                                            HealthPeriodID = mn.HealthPeriodID
                                                        }).ToList();

            List<PupilAbsence> queryPupilAbsence = PupilAbsenceBusiness.All.Where(o => o.PupilID == pupilID
                                                                                        && o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1
                                                                                        && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5
                                                                                        && o.Last2digitNumberSchool == partitionId).ToList();

            // Lấy danh sách môn học của lớp theo năm và học kì đang xét
            List<ClassSubjectBO> lstClassSubject = (from p in ClassSubjectBusiness.All.Where(o => o.ClassProfile.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.ClassProfile.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5)
                                                    join s in SubjectCatBusiness.All.Where(o => o.IsActive == true) on p.SubjectID equals s.SubjectCatID
                                                    where p.Last2digitNumberSchool == partitionId
                                                    && listClassId.Contains(p.ClassID)
                                                    select new ClassSubjectBO
                                                    {
                                                        AcademicYearID = p.ClassProfile.AcademicYearID,
                                                        SchoolID = p.ClassProfile.SchoolID,
                                                        EducationLevelID = p.ClassProfile.EducationLevelID,
                                                        SubjectID = p.SubjectID,
                                                        ClassID = p.ClassID,
                                                        DisplayName = s.DisplayName,
                                                        SubjectName = s.SubjectName,
                                                        OrderInSubject = s.OrderInSubject,
                                                        IsCommenting = p.IsCommenting,
                                                        SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester,
                                                        SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester
                                                    }).ToList();

            // Lấy nhận xét và điểm các môn - SummedEvaluation và SummedEvaluationHistory
            List<SummedEvaluation> querySE = SummedEvaluationBusiness.All.Where(o => o.PupilID == pupilID && lstAcademicYearID.Contains(o.AcademicYearID)
                && listClassId.Contains(o.ClassID)
                && o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5).ToList();

            List<SummedEvaluationHistory> querySEH = SummedEvaluationHistoryBusiness.All.Where(o => o.PupilID == pupilID && lstAcademicYearID.Contains(o.AcademicYearID)
                && listClassId.Contains(o.ClassID)
                && o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5).ToList();

            // Lấy nhận xét năng lực và phẩm chất - SummedEndingEvaluation
            List<SummedEndingEvaluation> querySEE = SummedEndingEvaluationBusiness.All.Where(o => o.PupilID == pupilID).ToList();

            // Lấy nhận xét, khen thưởng của học sinh - RewardCommentFinal và RewardFinal
            List<RewardCommentFinal> queryRCF = RewardCommentFinalBusiness.All.Where(o => o.PupilID == pupilID).ToList();
            List<RewardFinal> queryRF = RewardFinalBusiness.All.Where(r => r.SchoolID == schoolID).ToList();

            // Duyệt qua các năm, tạo 2 sheet tương ứng HKI và CaNam

            List<int> listSemester = new List<int>() { GlobalConstants.SEMESTER_OF_YEAR_FIRST, GlobalConstants.SEMESTER_OF_YEAR_SECOND };

            // Biến tạm chứa dữ liệu phần thông tin sức khỏe, ngày nghỉ
            InfoEvaluationPupil tempInfo = null;
            List<PupilAbsence> listPupilAbsence = null;
            List<PupilAbsence> listPupilAbsenceAccept = null;
            List<PupilAbsence> listPupilAbsenceNonAccept = null;
            ClassProfile classAca = ClassProfileBusiness.Find(classID);
            List<int> lstSectionKeyID = classAca.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(classAca.SeperateKey.Value) : new List<int>();

            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.OrderBy(p => p.EvaluationCriteriaID).ToList();
            IDictionary<string, object> dicRated = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"SemesterID",GlobalConstants.SEMESTER_OF_YEAR_SECOND},
                        {"PupilID",pupilID}
                    };
            //lay danh sach mon hoc cua lop
            List<VRatedCommentPupil> lstRatedCommentPupil = RatedCommentPupilBusiness.VSearch(dicRated).Where(x => lstAcademicYearID.Contains(x.AcademicYearID)
                                                                                                            && listClassId.Contains(x.ClassID)).ToList();
            //lay du lieu SummedEndingEvaluation
            List<EvaluationReward> lstER = new List<EvaluationReward>();
            IDictionary<string, object> dicER = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"ClassID",classID},
                        {"SemesterID",GlobalConstants.SEMESTER_OF_YEAR_SECOND},
                        {"PupilID",pupilID}
                    };
            lstER = EvaluationRewardBusiness.Search(dicER).Where(x => lstAcademicYearID.Contains(x.AcademicYearID)
                                                                && listClassId.Contains(x.ClassID) && x.PupilID == pupilID).ToList();

            List<ClassProfile> listClassProfile = ClassProfileBusiness.All.Where(p => listClassId.Contains(p.ClassProfileID) && p.IsActive == true).ToList();
            #endregion
            // Biến tạm chứa thông tin phần Môn học, Nhận xét, Năng lực và Phẩm chất
            ClassSubjectBO subjectTemp = null;
            List<ClassSubjectBO> listClassSJ = null;
            TranscriptsSummedEvaluationBO tempTSE = null;
            List<TranscriptsSummedEvaluationBO> queryTSE = null;
            List<TranscriptsSummedEvaluationBO> listSE = null;
            List<SummedEndingEvaluation> listSEE = null;
            SummedEndingEvaluation tempSEE = null;
            List<RewardCommentFinal> listRCF = null;
            List<RewardFinal> listRF = null;
            RewardCommentFinal tempRCF = null;
            int headTeacherId = 0;
            startPageNumberOfSheet++;
            PupilOfSchoolBO tempAca = null;
            for (int i = 0; i < listPOS.Count; i++)
            {
                // Xét năm tương ứng
                tempAca = listPOS[i];
                int partionAca = UtilsBusiness.GetPartionId(tempAca.SchoolID);
                AcademicYear objAcaHis = AcademicYearBusiness.Find(tempAca.AcademicYearID);

                #region // Kiểm tra lấy dữ liệu từ bảng hiện tại hay bảng lịch sử
                if (UtilsBusiness.IsMoveHistory(objAcaHis))
                {
                    queryTSE = (from se in querySEH
                                where se.AcademicYearID == tempAca.AcademicYearID && se.SchoolID == tempAca.SchoolID && se.LastDigitSchoolID == partionAca
                                && se.ClassID == tempAca.ClassID
                                select new TranscriptsSummedEvaluationBO
                                {
                                    SummedEvaluationID = se.SummedEvaluationID,
                                    PupilID = se.PupilID,
                                    ClassID = se.ClassID,
                                    EducationLevelID = se.EducationLevelID,
                                    AcademicYearID = se.AcademicYearID,
                                    LastDigitSchoolID = se.LastDigitSchoolID,
                                    SchoolID = se.SchoolID,
                                    SemesterID = se.SemesterID,
                                    EvaluationCriteriaID = se.EvaluationCriteriaID,
                                    EvaluationID = se.EvaluationID,
                                    EndingEvaluation = se.EndingEvaluation,
                                    EndingComments = se.EndingComments,
                                    PeriodicEndingMark = se.PeriodicEndingMark
                                }).ToList();
                }
                else
                {
                    queryTSE = (from se in querySE
                                where se.AcademicYearID == tempAca.AcademicYearID && se.SchoolID == tempAca.SchoolID && se.LastDigitSchoolID == partionAca
                                && se.ClassID == tempAca.ClassID
                                select new TranscriptsSummedEvaluationBO
                                {
                                    SummedEvaluationID = se.SummedEvaluationID,
                                    PupilID = se.PupilID,
                                    ClassID = se.ClassID,
                                    EducationLevelID = se.EducationLevelID,
                                    AcademicYearID = se.AcademicYearID,
                                    LastDigitSchoolID = se.LastDigitSchoolID,
                                    SchoolID = se.SchoolID,
                                    SemesterID = se.SemesterID,
                                    EvaluationCriteriaID = se.EvaluationCriteriaID,
                                    EvaluationID = se.EvaluationID,
                                    EndingEvaluation = se.EndingEvaluation,
                                    EndingComments = se.EndingComments,
                                    PeriodicEndingMark = se.PeriodicEndingMark
                                }).ToList();
                }
                #endregion

                #region Fill TT30
                if (tempAca.Year < YearCst)
                {
                    //Fill ten giao vien chu nhiem
                    headTeacherId = listClassProfile.Where(p => p.ClassProfileID == tempAca.ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                    Employee employeeObj = EmployeeBusiness.Find(headTeacherId);
                    // Duyệt học kì
                    for (int j = 0; j < listSemester.Count; j++)
                    {
                        int semesterID = listSemester[j];
                        IVTWorksheet sheetTemp = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? fourthSheet : fifthSheet;
                        IVTWorksheet sheetFill = oBook.CopySheetToBeforeLast(sheetTemp);

                        #region
                        // Lọc dữ liệu theo năm, trường, lớp và học kì
                        tempInfo = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ?
                            queryInfoPupil.Where(o => o.AcademicYear == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID &&
                                                      (o.MonitoringDate == null || (o.MonitoringDate != null && o.MonitoringDate >= objAcaHis.FirstSemesterStartDate && o.MonitoringDate <= objAcaHis.FirstSemesterEndDate))
                                                      && o.HealthPeriodID == 1
                                                      ).FirstOrDefault() :
                            queryInfoPupil.Where(o => o.AcademicYear == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID &&
                                                      (o.MonitoringDate == null || (o.MonitoringDate != null && o.MonitoringDate >= objAcaHis.SecondSemesterStartDate && o.MonitoringDate <= objAcaHis.SecondSemesterEndDate))
                                                      && o.HealthPeriodID == 2
                                                      ).FirstOrDefault();

                        listPupilAbsence = queryPupilAbsence.Where(o => o.AcademicYearID == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && lstSectionKeyID.Contains(o.Section)).ToList();

                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            listPupilAbsence = listPupilAbsence.Where(o => o.AbsentDate >= objAcaHis.FirstSemesterStartDate.Value && o.AbsentDate <= objAcaHis.FirstSemesterEndDate.Value).ToList();
                        }
                        else
                        {
                            listPupilAbsence = listPupilAbsence.Where(o => o.AbsentDate >= objAcaHis.SecondSemesterStartDate.Value && o.AbsentDate <= objAcaHis.SecondSemesterEndDate.Value).ToList();
                        }

                        //listPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(listPupilAbsence);
                        listPupilAbsenceAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).ToList();
                        listPupilAbsenceNonAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).ToList();
                        listSE = queryTSE.Where(o => o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && o.SemesterID == semesterID).ToList();
                        listSEE = querySEE.Where(o => o.AcademicYearID == tempAca.AcademicYearID && (o.SemesterID == semesterID || o.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT) && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID).ToList();

                        // Lọc dữ liệu phần khen thưởng
                        listRCF = queryRCF.Where(o => o.AcademicYearID == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && o.SemesterID == semesterID).ToList();
                        listRF = queryRF.Where(o => o.SchoolID == tempAca.SchoolID).ToList();
                        #endregion

                        #region // Fill dữ liệu phần Sức khỏe và Ngày nghỉ
                        sheetFill.SetCellValue("A2", pupil != null ? "Họ và tên học sinh: " + pupil.FullName : string.Empty);
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            sheetFill.SetCellValue("A3", tempAca != null ? "TỔNG HỢP ĐÁNH GIÁ CUỐI HỌC KÌ I NĂM HỌC " + objAcaHis.DisplayTitle : string.Empty);
                        }
                        else
                        {
                            sheetFill.SetCellValue("A3", tempAca != null ? "TỔNG HỢP ĐÁNH GIÁ CUỐI NĂM HỌC " + objAcaHis.DisplayTitle : string.Empty);
                        }

                        sheetFill.SetCellValue("C4", tempInfo != null ? (tempInfo.Height % 1 == 0 ? tempInfo.Height + " cm" : string.Format("{0:0.0}", tempInfo.Height) + " cm") : string.Empty);
                        sheetFill.SetCellValue("H4", tempInfo != null ? (tempInfo.Weight % 1 == 0 ? tempInfo.Weight + " kg" : string.Format("{0:0.0}", tempInfo.Weight) + " kg") : string.Empty);
                        sheetFill.SetCellValue("O4", tempInfo != null && tempInfo.EvaluationHealth == 1 ? "Loại I (Tốt)" : tempInfo != null && tempInfo.EvaluationHealth == 2 ? "Loại II (Khá)" :
                                                     tempInfo != null && tempInfo.EvaluationHealth == 3 ? "Loại III (Trung bình)" : tempInfo != null && tempInfo.EvaluationHealth == 4 ? "Loại IV (Yếu)" :
                                                     tempInfo != null && tempInfo.EvaluationHealth == 5 ? "Loại V (Rất yếu)" : string.Empty);
                        sheetFill.SetCellValue("C5", listPupilAbsence.Count);
                        sheetFill.SetCellValue("H5", listPupilAbsenceAccept.Count);
                        sheetFill.SetCellValue("O5", listPupilAbsenceNonAccept.Count);
                        #endregion

                        #region// Fill dữ liệu phần Nhận xét môn học
                        listClassSJ = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? lstClassSubject.Where(o => o.SchoolID == tempAca.SchoolID &&
                                                                                                o.EducationLevelID == tempAca.EducationLevelId && o.ClassID == tempAca.ClassID &&
                                                                                                o.SectionPerWeekFirstSemester > 0).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList() :
                                                                                             lstClassSubject.Where(o => o.SchoolID == tempAca.SchoolID &&
                                                                                                o.EducationLevelID == tempAca.EducationLevelId && o.ClassID == tempAca.ClassID &&
                                                                                                o.SectionPerWeekSecondSemester > 0).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
                        int startRowMH = 11;
                        int countWord = 55;
                        int countWordTD = 55;
                        int countWordNX = 65;
                        int index = startRowMH;
                        IVTRange rowMHTD = sheetFill.GetRow(9);
                        IVTRange rowMHNX = sheetFill.GetRow(10);
                        IVTRange rowTemp = null;
                        //int totalSubject = 0;
                        for (int k = 0; k < listClassSJ.Count; k++)
                        {
                            subjectTemp = listClassSJ[k];
                            // Lấy dữ liệu tương ứng với môn
                            tempTSE = listSE.Where(o => o.EvaluationCriteriaID == subjectTemp.SubjectID && o.EvaluationID == GlobalConstants.TAB_EDUCATION).FirstOrDefault();
                            rowTemp = subjectTemp != null && subjectTemp.IsCommenting.HasValue && subjectTemp.IsCommenting.Value == 1 ? rowMHNX : rowMHTD;
                            sheetFill.CopyPasteSameSize(rowTemp, index, 1);

                            sheetFill.SetCellValue(index, 1, subjectTemp.SubjectName);
                            sheetFill.SetCellValue(index, 6, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            if (subjectTemp.IsCommenting.HasValue == false || subjectTemp.IsCommenting == 0)
                            {
                                countWord = countWordNX;
                                sheetFill.SetCellValue(index, 5, tempTSE != null && tempTSE.PeriodicEndingMark.HasValue ? tempTSE.PeriodicEndingMark.Value.ToString() : string.Empty);
                            }
                            else
                            {
                                countWord = countWordTD;
                            }
                            index++;
                        }

                        double defaultArea = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 600 : 660;
                        double newHeight = (defaultArea / (index - 11));
                        for (int h = 11; h < index; h++)
                        {
                            sheetFill.SetRowHeight(h, newHeight);
                        }
                        #endregion

                        #region// Fill dữ liệu trước khi copy
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            int startSheetComment = 2;
                            IVTWorksheet eightSheet = oBook.CopySheetToBeforeLast(eightTT30Sheet);
                            IVTRange rangComment = eightTT30Sheet.GetRange("A2", "P32");
                            eightSheet.CopyPasteSameSize(rangComment, 2, 1);

                            eightSheet.SetCellValue(startSheetComment, 1, tempAca != null ? "Trường: " + this.CutSchoolName(tempAca.SchoolName) : "Trường: ");
                            eightSheet.SetCellValue(startSheetComment, 15, tempAca != null ? "Lớp: " + this.CutClassName(tempAca.ClassName) : "Lớp: ");

                            #region Dien du lieu nang luc
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                eightSheet.SetCellValue(startSheetComment + 2, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Đ") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID1).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 5, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID2).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 6, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID3).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 7, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            #region Dien du lieu pham chat
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                eightSheet.SetCellValue(startSheetComment + 9, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Đ") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID4).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 12, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID5).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 13, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID6).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 14, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID7).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 15, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            // Nhận xét, khen thưởng
                            tempRCF = listRCF.FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 18, 1, tempRCF != null ? tempRCF.OutstandingAchievement : string.Empty);

                            string strRewardMode = string.Empty;
                            foreach (RewardFinal rw in listRF)
                            {
                                if (tempRCF != null && tempRCF.RewardID != null && tempRCF.RewardID.Contains(rw.RewardFinalID.ToString()))
                                {
                                    strRewardMode = strRewardMode + rw.RewardMode + ", ";
                                }
                            }
                            eightSheet.SetCellValue(startSheetComment + 20, 1, strRewardMode.Length > 0 ? strRewardMode.Substring(0, strRewardMode.Length - 2) : string.Empty);

                            // Fill địa điểm, ngày
                            eightSheet.SetCellValue((startSheetComment + 21), 7, schoolProvince + ", ngày " + current.Day + " tháng " + current.Month + " năm " + current.Year);
                            eightSheet.SetCellValue((startSheetComment + 28), 7, employeeObj != null ? employeeObj.FullName : "");

                            IVTRange rang = eightSheet.GetRange("A2", "P32");
                            sheetFill.CopyPasteSameSize(rang, (index + 1), 1);
                            eightSheet.Delete();
                        }
                        else
                        {
                            int startSheetComment = 2;
                            IVTWorksheet nineSheet = oBook.CopySheetToBeforeLast(nineTT30Sheet);
                            IVTRange rangComment = nineTT30Sheet.GetRange("A2", "P34");
                            nineSheet.CopyPasteSameSize(rangComment, 2, 1);

                            nineSheet.SetCellValue(startSheetComment, 1, tempAca != null ? "Trường: " + this.CutSchoolName(tempAca.SchoolName) : "Trường: ");
                            nineSheet.SetCellValue(startSheetComment, 15, tempAca != null ? "Lớp: " + this.CutClassName(tempAca.ClassName) : "Lớp: ");

                            #region Dien du lieu nang luc
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                nineSheet.SetCellValue(startSheetComment + 2, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Đ") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID1).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 5, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID2).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 6, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID3).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 7, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            #region Dien du lieu pham chat
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                nineSheet.SetCellValue(startSheetComment + 9, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Đ") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID4).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 12, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID5).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 13, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID6).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 14, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID7).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 15, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            // Nếu là học bạ cuối năm
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (tempAca.EducationLevelId >= GlobalConstants.EDUCATION_LEVEL_ID1 && tempAca.EducationLevelId <= GlobalConstants.EDUCATION_LEVEL_ID4)
                                {
                                    tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.EVALUATION_RESULT).FirstOrDefault();
                                    nineSheet.SetCellValue(startSheetComment + 22, 1, tempSEE != null && "HT".Equals(tempSEE.EndingEvaluation) ? "Hoàn thành chương trình lớp học." : "Không hoàn thành chương trình lớp học");
                                }
                                else if (tempAca.EducationLevelId >= GlobalConstants.EDUCATION_LEVEL_ID5)
                                {
                                    tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.EVALUATION_RESULT).FirstOrDefault();
                                    nineSheet.SetCellValue(startSheetComment + 22, 1, tempSEE != null && "HT".Equals(tempSEE.EndingEvaluation) ? "Hoàn thành chương trình tiểu học." : "Không hoàn thành chương trình tiểu học");
                                }
                            }

                            // Nhận xét, khen thưởng
                            tempRCF = listRCF.FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 18, 1, tempRCF != null ? tempRCF.OutstandingAchievement : string.Empty);

                            string strRewardMode = string.Empty;
                            foreach (RewardFinal rw in listRF)
                            {
                                if (tempRCF != null && tempRCF.RewardID != null && tempRCF.RewardID.Contains(rw.RewardFinalID.ToString()))
                                {
                                    strRewardMode = strRewardMode + rw.RewardMode + ", ";
                                }
                            }
                            nineSheet.SetCellValue(startSheetComment + 20, 1, strRewardMode.Length > 0 ? strRewardMode.Substring(0, strRewardMode.Length - 2) : string.Empty);

                            // Fill địa điểm, ngày
                            nineSheet.SetCellValue((startSheetComment + 23), 7, schoolProvince + ", ngày " + current.Day + " tháng " + current.Month + " năm " + current.Year);
                            nineSheet.SetCellValue((startSheetComment + 29), 1, school.HeadMasterName);
                            nineSheet.SetCellValue((startSheetComment + 29), 7, employeeObj != null ? employeeObj.FullName : "");

                            IVTRange rang = nineSheet.GetRange("A2", "P34");
                            sheetFill.CopyPasteSameSize(rang, (index + 1), 1);
                            nineSheet.Delete();
                        }
                        #endregion
                        int rowLast = (index + (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 28 : 30));
                        // Xóa 2 dòng template mẫu
                        sheetFill.DeleteRow(9);
                        sheetFill.DeleteRow(9);

                        sheetFill.PageMaginBottom = 0.5;
                        sheetFill.SetBreakPage(index - 1);
                        sheetFill.Name = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "CuoiHKI " + objAcaHis.DisplayTitle : "CuoiNam " + objAcaHis.DisplayTitle;
                        sheetFill.SetFontName("Times New Roman", 0);

                        sheetFill.PrintArea = "$A$1:$P$" + rowLast;
                        if (fillPaging)
                        {
                            sheetPosition++;
                            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                            {
                                SheetIndex = sheetPosition,
                                IsHeaderOrFooter = true,
                                AlignPageNumber = 2,
                                StartPageNumberOfSheet = startPageNumberOfSheet,
                            });
                            startPageNumberOfSheet += 2;
                        }
                    }
                }
                #endregion
                #region Fill TT22
                else
                {
                    IVTWorksheet sheetFill = oBook.CopySheetToBeforeLast(sixthSheet);
                    IVTWorksheet seventhSheet = oBook.CopySheetToBeforeLast(seventhTT22Sheet);
                    IVTRange rangComment = seventhTT22Sheet.GetRange("A2", "P32");
                    seventhSheet.CopyPasteSameSize(rangComment, 2, 1);

                    //fill ho ten
                    sheetFill.SetCellValue("D2", pupil.FullName);
                    sheetFill.SetCellValue("O2", tempAca.ClassName);

                    //fill thong tin y te
                    tempInfo = queryInfoPupil.Where(o => o.AcademicYear == objAcaHis.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID &&
                                                  (o.MonitoringDate == null || (o.MonitoringDate != null && o.MonitoringDate >= objAcaHis.SecondSemesterStartDate && o.MonitoringDate <= objAcaHis.SecondSemesterEndDate))
                                                  && o.HealthPeriodID == 2
                                                  ).FirstOrDefault();

                    listPupilAbsence = queryPupilAbsence.Where(o => o.AcademicYearID == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && lstSectionKeyID.Contains(o.Section)).ToList();
                    listPupilAbsence = listPupilAbsence.Where(o => o.AbsentDate >= objAcaHis.FirstSemesterStartDate.Value && o.AbsentDate <= objAcaHis.SecondSemesterEndDate.Value).ToList();

                    //listPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(listPupilAbsence);
                    listPupilAbsenceAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).ToList();
                    listPupilAbsenceNonAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).ToList();
                    // Fill dữ liệu phần Sức khỏe và Ngày nghỉ
                    sheetFill.SetCellValue("C3", tempInfo != null ? (tempInfo.Height % 1 == 0 ? tempInfo.Height + " cm" : string.Format("{0:0.0}", tempInfo.Height) + " cm") : string.Empty);
                    sheetFill.SetCellValue("H3", tempInfo != null ? (tempInfo.Weight % 1 == 0 ? tempInfo.Weight + " kg" : string.Format("{0:0.0}", tempInfo.Weight) + " kg") : string.Empty);
                    sheetFill.SetCellValue("O3", tempInfo != null && tempInfo.EvaluationHealth == 1 ? "Loại I (Tốt)" : tempInfo != null && tempInfo.EvaluationHealth == 2 ? "Loại II (Khá)" :
                                                 tempInfo != null && tempInfo.EvaluationHealth == 3 ? "Loại III (Trung bình)" : tempInfo != null && tempInfo.EvaluationHealth == 4 ? "Loại IV (Yếu)" :
                                                 tempInfo != null && tempInfo.EvaluationHealth == 5 ? "Loại V (Rất yếu)" : string.Empty);
                    sheetFill.SetCellValue("C4", listPupilAbsence.Count);
                    sheetFill.SetCellValue("H4", listPupilAbsenceAccept.Count);
                    sheetFill.SetCellValue("O4", listPupilAbsenceNonAccept.Count);

                    List<VRatedCommentPupil> lstRatedSubject = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                                                                                    .Where(x => x.AcademicYearID == tempAca.AcademicYearID).ToList();
                    List<VRatedCommentPupil> lstRatedCapQua = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY)
                                                                                   .Where(x => x.AcademicYearID == tempAca.AcademicYearID).ToList();
                    VRatedCommentPupil objRatedComentPupil = null;
                    //danh sach mon hoc
                    listClassSJ = lstClassSubject.Where(o => o.SchoolID == tempAca.SchoolID &&
                    o.EducationLevelID == tempAca.EducationLevelId && o.ClassID == tempAca.ClassID &&
                    o.SectionPerWeekSecondSemester > 0).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
                    int startrowMH = 8;
                    IVTRange rangeCopy = sheetFill.GetRange(startrowMH, 1, startrowMH, 16);
                    int startRowComment = 0;
                    for (int j = 0; j < listClassSJ.Count; j++)
                    {
                        subjectTemp = listClassSJ[j];

                        if (j >= 2)
                        {
                            sheetFill.CopyAndInsertARow(rangeCopy, startrowMH, true);
                        }

                        if (subjectTemp.IsCommenting == 0)
                        {
                        }
                        else
                        {
                            if (startRowComment == 0)
                            {
                                startRowComment = startrowMH;
                            }
                        }

                        sheetFill.SetCellValue(startrowMH, 1, subjectTemp.DisplayName);
                        sheetFill.SetCellValue(startrowMH, 5, "");
                        objRatedComentPupil = lstRatedSubject.Where(p => p.SubjectID == subjectTemp.SubjectID).FirstOrDefault();
                        if (objRatedComentPupil != null)
                        {
                            sheetFill.SetCellValue(startrowMH, 8, objRatedComentPupil.Comment);
                            sheetFill.GetRange(startrowMH, 8, startrowMH, 12).SetHAlign(VTHAlign.xlHAlignLeft);
                            if (subjectTemp.IsCommenting == 0)
                            {
                                sheetFill.SetCellValue(startrowMH, 5, objRatedComentPupil.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.EndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_COMPLETE : objRatedComentPupil.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                sheetFill.SetCellValue(startrowMH, 7, objRatedComentPupil.PeriodicEndingMark);
                            }
                            else
                            {
                                sheetFill.SetCellValue(startrowMH, 5, objRatedComentPupil.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.EndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_COMPLETE : objRatedComentPupil.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                if (j < listClassSJ.Count - 1)
                                {
                                    //sixthSheet.GetRange(startrowMH, 15, startrowMH + 1, 16).Merge();
                                }
                            }
                        }
                        startrowMH++;
                    }

                    double defaultArea = 580;
                    double newHeight = defaultArea / (startrowMH - 8);
                    for (int k = 8; k < startrowMH; k++)
                    {
                        if (newHeight > 150)
                            newHeight = 48;
                        sheetFill.SetRowHeight(k, newHeight);
                    }

                    if (startRowComment > 0)
                    {
                        sheetFill.GetRange(startRowComment, 7, startrowMH - 1, 7).Merge();
                    }

                    #region// fill năng lực phẩm chất, khen trưởng, hoàn thành lớp học trước khi copy
                    #region// năng lực phẩm chất
                    seventhSheet.SetCellValue("A2", objAcaHis != null ? "Trường: " + objAcaHis.SchoolProfile.SchoolName : "Trường: ");
                    seventhSheet.SetCellValue("K2", objAcaHis != null ? "Năm học: " + objAcaHis.DisplayTitle : "Năm học: ");
                    int startRowNL = 7;
                    int startRowPC = 12;
                    EvaluationCriteria objEC = null;
                    for (int j = 0; j < lstEvaluationCriteria.Count; j++)
                    {
                        objEC = lstEvaluationCriteria[j];
                        objRatedComentPupil = lstRatedCapQua.Where(p => p.EvaluationID == objEC.TypeID && p.SubjectID == (objEC.EvaluationCriteriaID)).FirstOrDefault();
                        if (objEC.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            if (objRatedComentPupil != null)
                            {
                                seventhSheet.SetCellValue(startRowNL, 5, objRatedComentPupil.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.CapacityEndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComentPupil.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                seventhSheet.SetCellValue(startRowNL, 7, objRatedComentPupil.Comment);
                            }
                            startRowNL++;
                        }
                        else
                        {
                            if (objRatedComentPupil != null)
                            {
                                seventhSheet.SetCellValue(startRowPC, 5, objRatedComentPupil.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.QualityEndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComentPupil.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                seventhSheet.SetCellValue(startRowPC, 7, objRatedComentPupil.Comment);
                            }
                            startRowPC++;
                        }
                    }
                    #endregion

                    #region// Khen thưởng
                    EvaluationReward objER = null;
                    SummedEndingEvaluation objSEE = querySEE.Where(p => p.AcademicYearID == tempAca.AcademicYearID && p.ClassID == tempAca.ClassID
                                                                        && p.EvaluationID == GlobalConstants.EVALUATION_RESULT).FirstOrDefault();
                    string strSEE = objSEE != null && objSEE.Reward.HasValue ? "Hoàn thành xuất sắc các môn học và rèn luyện" : "";
                    string strER = string.Empty;
                    for (int j = 0; j < lstER.Count; j++)
                    {
                        objER = lstER[j];
                        if (j < lstER.Count - 1)
                        {
                            strER += objER.Content + "; ";
                        }
                        else
                        {
                            strER += objER.Content + ".";
                        }

                    }
                    seventhSheet.SetCellValue("A18", !string.IsNullOrEmpty(strSEE) ? (strSEE + "; " + strER) : strER);
                    #endregion

                    #region//hoàn thành CHLH
                    string strEvaluationResult = string.Empty;
                    if (objSEE != null)
                    {
                        if (!string.IsNullOrEmpty(objSEE.EndingEvaluation))
                        {
                            if (educationLevelID < 5)
                            {
                                if (objSEE.EndingEvaluation == "HT")
                                {
                                    strEvaluationResult = "Hoàn thành chương trình lớp " + educationLevelID + "; Được lên lớp " + (educationLevelID + 1);
                                }
                                else
                                {
                                    strEvaluationResult = "Chưa hoàn thành chương trình lớp " + educationLevelID;
                                    if (objSEE.RateAdd.HasValue)
                                    {
                                        if (objSEE.RateAdd.Value == 1)
                                        {
                                            strEvaluationResult += "; Được lên lớp " + +(educationLevelID + 1);
                                        }
                                        else if (objSEE.RateAdd.Value == 0)
                                        {
                                            strEvaluationResult += "; Ở lại lớp " + educationLevelID;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (objSEE.EndingEvaluation == "HT")
                                {
                                    strEvaluationResult = "Hoàn thành chương trình tiểu học";
                                }
                                else
                                {
                                    strEvaluationResult = "Chưa hoàn thành chương trình tiểu học";
                                }
                            }
                        }
                    }
                    seventhSheet.SetCellValue("A20", strEvaluationResult + (!string.IsNullOrEmpty(strEvaluationResult) ? "." : ""));
                    #endregion

                    string _strDateCurrent = schoolProvince + ", ngày " + current.Day + " tháng " + current.Month + " năm " + current.Year;
                    seventhSheet.SetCellValue("G22", _strDateCurrent);

                    headTeacherId = listClassProfile.Where(p => p.ClassProfileID == tempAca.ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                    Employee employeeObj = EmployeeBusiness.Find(headTeacherId);
                    seventhSheet.SetCellValue("G31", employeeObj != null ? employeeObj.FullName : string.Empty);
                    seventhSheet.SetCellValue("A31", objAcaHis != null ? objAcaHis.SchoolProfile.HeadMasterName : string.Empty);
                    #endregion

                    IVTRange rang = seventhSheet.GetRange("A2", "P32");
                    sheetFill.CopyPasteSameSize(rang, (startrowMH + 1), 1);
                    sheetFill.SetBreakPage((startrowMH + 1));
                    sheetFill.Name = string.Format("NH {0}", objAcaHis.DisplayTitle);
                    sheetFill.SetFontName("Times New Roman", 0);
                    seventhSheet.Delete();

                    int rowLast = (startrowMH + 1) + 29;
                    sheetFill.PrintArea = "$A$1:$P$" + rowLast;
                    if (fillPaging)
                    {
                        sheetPosition++;
                        lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                        {
                            SheetIndex = sheetPosition,
                            IsHeaderOrFooter = true,
                            AlignPageNumber = 2,
                            StartPageNumberOfSheet = startPageNumberOfSheet,
                        });
                        startPageNumberOfSheet += 2;
                    }

                }
                #endregion
            }
            #endregion

            // Xóa sheet mẫu
            fourthSheet.Delete();
            fifthSheet.Delete();
            sixthSheet.Delete();
            seventhTT22Sheet.Delete();
            eightTT30Sheet.Delete();
            nineTT30Sheet.Delete();

            return oBook.FillPageNumber(lstDataPageNumber);
        }

        public void DownloadZipFileTranscriptPrimary(IDictionary<string, object> dic, out string folderSaveFile)
        {
            #region lay danh sach hoc sinh cua lop
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partion = UtilsBusiness.GetPartionId(schoolID);
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");
            long pupilID = Utils.GetInt(dic, "PupilProfileID");
            bool fillPaging = Utils.GetBool(dic, "fillPaging");

            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iqPocClass = PupilOfClassBusiness.GetPupilInClassTranscripReport(classID);
            if (pupilID > 0)
            {
                iqPocClass = iqPocClass.Where(poc => poc.PupilID == pupilID);
            }
            lstPOC = iqPocClass.OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            if (lstPOC == null || lstPOC.Count == 0)
            {
                folderSaveFile = string.Empty;
                return;
            }
            List<int> lstPupilID = lstPOC.Select(o => o.PupilID).ToList();
            PupilOfClassBO objPOC = null;
            string fileName = "{0}_HS_TH_Hocba_{1}.xls";//HS_THCS_HocBaHocSinh_NguyenThiHongNhung            
            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            string reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_TT22;
            // Lấy đường dẫn báo cáo
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            List<AcademicYearBO> queryPOCAll = (from poc in PupilOfClassBusiness.AllNoTracking
                                                join cp in ClassProfileBusiness.AllNoTracking on poc.ClassID equals cp.ClassProfileID
                                                join ac in AcademicYearBusiness.AllNoTracking on cp.AcademicYearID equals ac.AcademicYearID
                                                join sp in SchoolProfileBusiness.AllNoTracking on poc.SchoolID equals sp.SchoolProfileID
                                                join ss in PupilOfSchoolBusiness.AllNoTracking on poc.PupilID equals ss.PupilID
                                                join pf in PupilProfileBusiness.AllNoTracking on poc.PupilID equals pf.PupilProfileID
                                                where lstPupilID.Contains(poc.PupilID) && ac.Year <= academicYear.Year
                                                && sp.IsActive && ac.SchoolID == sp.SchoolProfileID
                                                && ss.SchoolID == sp.SchoolProfileID
                                                && ac.IsActive == true
                                                && cp.IsActive == true
                                                && poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                select new AcademicYearBO
                                                {
                                                    AcademicYearID = poc.AcademicYearID,
                                                    Year = ac.Year,
                                                    DisplayTitle = ac.DisplayTitle,
                                                    ClassID = poc.ClassID,
                                                    ClassName = cp.DisplayName,
                                                    SchoolID = poc.SchoolID,
                                                    EnrolmentDate = ss.EnrolmentDate, //poc.AssignedDate,
                                                    SchoolName = sp.SchoolName,
                                                    PupilID = poc.PupilID,
                                                    FirstSemesterStartDate = ac.FirstSemesterStartDate,
                                                    FirstSemesterEndDate = ac.FirstSemesterEndDate,
                                                    SecondSemesterStartDate = ac.SecondSemesterStartDate,
                                                    SecondSemesterEndDate = ac.SecondSemesterEndDate,
                                                    School = sp,
                                                    EducationLevelID = cp.EducationLevelID,
                                                    StorageName = pf.StorageNumber,
                                                    HeadMasterName = sp.HeadMasterName
                                                }).ToList();
            if (queryPOCAll == null || queryPOCAll.Count == 0)
            {
                folderSaveFile = string.Empty;
                return;
            }
            List<int> lstPartition = queryPOCAll.Select(o => o.SchoolID % 100).Distinct().ToList();
            List<int> lstSchoolID = queryPOCAll.Select(o => o.SchoolID).Distinct().ToList();
            List<int> lstPartition20 = queryPOCAll.Select(o => o.SchoolID % 20).Distinct().ToList();

            // Lấy tất cả các thông số về sức khỏe và số ngày nghỉ của học sinh
            List<InfoEvaluationPupil> queryInfoPupilAll = (from pt in PhysicalTestBusiness.All
                                                           join mn in MonitoringBookBusiness.All on pt.MonitoringBookID equals mn.MonitoringBookID
                                                           where lstPupilID.Contains(mn.PupilID) && lstSchoolID.Contains(mn.SchoolID)
                                                           orderby mn.MonitoringDate descending
                                                           select new InfoEvaluationPupil
                                                           {
                                                               PupilID = mn.PupilID,
                                                               Height = pt.Height.Value,
                                                               Weight = pt.Weight.Value,
                                                               EvaluationHealth = pt.PhysicalClassification.Value,
                                                               AcademicYear = mn.AcademicYearID,
                                                               SchoolID = mn.SchoolID,
                                                               ClassID = mn.ClassID,
                                                               MonitoringDate = mn.MonitoringDate,
                                                               HealthPeriodID = mn.HealthPeriodID
                                                           }).ToList();

            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            List<PupilAbsence> queryPupilAbsenceAll = PupilAbsenceBusiness.All.Where(o => lstPupilID.Contains(o.PupilID)
                                                                                        && o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1
                                                                                        && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5
                                                                                        && lstPartition.Contains(o.Last2digitNumberSchool)).ToList();

            // Lấy danh sách môn học của lớp theo năm và học kì đang xét
            List<ClassSubjectBO> lstClassSubject = (from p in ClassSubjectBusiness.All.Where(o => o.ClassProfile.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.ClassProfile.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5)
                                                    join s in SubjectCatBusiness.All.Where(o => o.IsActive == true) on p.SubjectID equals s.SubjectCatID
                                                    where lstPartition.Contains(p.Last2digitNumberSchool) && lstSchoolID.Contains(p.ClassProfile.SchoolID)
                                                    select new ClassSubjectBO
                                                    {
                                                        AcademicYearID = p.ClassProfile.AcademicYearID,
                                                        SchoolID = p.ClassProfile.SchoolID,
                                                        EducationLevelID = p.ClassProfile.EducationLevelID,
                                                        SubjectID = p.SubjectID,
                                                        ClassID = p.ClassID,
                                                        DisplayName = s.DisplayName,
                                                        SubjectName = s.SubjectName,
                                                        OrderInSubject = s.OrderInSubject,
                                                        IsCommenting = p.IsCommenting,
                                                        SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester,
                                                        SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester

                                                    }).ToList();

            List<SummedEvaluation> querySEAll = SummedEvaluationBusiness.All.Where(o => lstPupilID.Contains(o.PupilID)
                                                                                    && lstPartition.Contains(o.LastDigitSchoolID)
                                                                                    && o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1
                                                                                    && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5).ToList();

            List<SummedEvaluationHistory> querySEHAll = SummedEvaluationHistoryBusiness.All.Where(o => lstPupilID.Contains(o.PupilID) && lstPartition.Contains(o.LastDigitSchoolID) && o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && o.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID5).ToList();

            // Lấy nhận xét năng lực và phẩm chất - SummedEndingEvaluation
            List<SummedEndingEvaluation> querySEEAll = SummedEndingEvaluationBusiness.All.Where(o => lstPupilID.Contains(o.PupilID) && lstPartition.Contains(o.LastDigitSchoolID)).ToList();

            // Lấy nhận xét, khen thưởng của học sinh - RewardCommentFinal và RewardFinal
            List<RewardCommentFinal> queryRCFAll = RewardCommentFinalBusiness.All.Where(o => lstPupilID.Contains(o.PupilID) && lstPartition20.Contains(o.LastDigitSchoolID)).ToList();
            List<RewardFinal> queryRF = RewardFinalBusiness.All.ToList();

            List<Employee> lstEmployeeAll = EmployeeBusiness.All.Where(o => o.SchoolID != null && lstSchoolID.Contains(o.SchoolID.Value)).ToList();

            List<SchoolProfile> lstSchoolAll = SchoolProfileBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolProfileID)).ToList();

            List<AcademicYear> lstAcaAll = AcademicYearBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolID) && o.IsActive == true).ToList();

            List<int> lstClassID = queryPOCAll.Select(p => p.ClassID).Distinct().ToList();
            List<ClassProfile> lstCpAll = ClassProfileBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolID) && lstClassID.Contains(o.ClassProfileID) && o.IsActive == true).ToList();

            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.OrderBy(p => p.EvaluationCriteriaID).ToList();
            IDictionary<string, object> dicRated = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"SemesterID",GlobalConstants.SEMESTER_OF_YEAR_SECOND},
                        {"lstPupilID",lstPupilID}
                    };
            //lay danh sach mon hoc cua lop
            List<VRatedCommentPupil> lstRatedCommentPupil = RatedCommentPupilBusiness.VSearch(dicRated).Where(x => lstClassID.Contains(x.ClassID)).ToList();
            //lay du lieu SummedEndingEvaluation
            List<EvaluationReward> lstER = new List<EvaluationReward>();
            IDictionary<string, object> dicER = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"SemesterID",GlobalConstants.SEMESTER_OF_YEAR_SECOND},
                        {"lstPupilID",lstPupilID}
                    };
            lstER = EvaluationRewardBusiness.Search(dicER).Where(x => lstClassID.Contains(x.ClassID)).ToList();

            List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
            List<SMAS.VTUtils.Utils.VTDataImageValidation> listDataImage = new List<VTUtils.Utils.VTDataImageValidation>();
            string fileNameByPupil = "";
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                dic.Remove("PupilProfileID");
                dic.Add("PupilProfileID", pupilID);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                fileNameByPupil = string.Format(fileName, objPOC.OrderInClass.HasValue ? objPOC.OrderInClass.ToString() : objPOC.PupilCode, Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(objPOC.PupilFullName)));
                this.CreateTranscriptsZipFilePrimary(oBook, dic, objPOC, queryPOCAll, queryInfoPupilAll,
                    queryPupilAbsenceAll, lstClassSubject, querySEAll, querySEHAll, querySEEAll, queryRCFAll, queryRF, lstEmployeeAll, lstSchoolAll, lstAcaAll, lstCpAll,
                    lstEvaluationCriteria, lstRatedCommentPupil, lstER, fillPaging, ref lstDataPageNumber);
                oBook.CreateZipFile(fileNameByPupil, folder, listDataImage, lstDataPageNumber);
                lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
            }
            folderSaveFile = folder;
            #endregion
        }

        private void CreateTranscriptsZipFilePrimary(IVTWorkbook oBook, IDictionary<string, object> dic, PupilOfClassBO objPoc, List<AcademicYearBO> queryPOCAll,
            List<InfoEvaluationPupil> queryInfoPupilAll,
             List<PupilAbsence> queryPupilAbsenceAll, List<ClassSubjectBO> lstClassSubject, List<SummedEvaluation> querySEAll,
            List<SummedEvaluationHistory> querySEHAll, List<SummedEndingEvaluation> querySEEAll, List<RewardCommentFinal> queryRCFAll, List<RewardFinal> queryRF,
            List<Employee> lstEmployeeAll, List<SchoolProfile> lstSchoolAll, List<AcademicYear> lstAcaAll, List<ClassProfile> lstCpAll, List<EvaluationCriteria> lstEvaluationCriteria,
            List<VRatedCommentPupil> lstRatedCommentPupil, List<EvaluationReward> lstER, bool fillPaging, ref List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partion = UtilsBusiness.GetPartionId(schoolID);
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");
            long pupilID = Utils.GetInt(dic, "PupilProfileID");
            int startPageNumberOfSheet = 1;
            int sheetPosition = 3;

            ClassProfile objCP = lstCpAll.Where(o => o.ClassProfileID == classID).FirstOrDefault();
            List<int> lstSectionKeyID = (objCP != null && objCP.SeperateKey.HasValue) ? UtilsBusiness.GetListSectionID(objCP.SeperateKey.Value) : new List<int>();
            lstRatedCommentPupil = lstRatedCommentPupil.Where(s => s.PupilID == pupilID).ToList();
            AcademicYear academicYear = lstAcaAll.Where(o => o.AcademicYearID == academicYearID).FirstOrDefault();
            SchoolProfile school = lstSchoolAll.Where(o => o.SchoolProfileID == schoolID).FirstOrDefault();
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            // Khởi tạo
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);
            IVTWorksheet fifthSheet = oBook.GetSheet(5);
            IVTWorksheet sixthSheet = oBook.GetSheet(6);
            IVTWorksheet seventhTT22Sheet = oBook.GetSheet(7);
            IVTWorksheet eightTT30Sheet = oBook.GetSheet(8);
            IVTWorksheet nineTT30Sheet = oBook.GetSheet(9);

            #region Trang bia
            // Fill dữ liệu trang bìa
            if (objPoc != null)
            {
                firstSheet.SetCellValue("C28", objPoc.PupilFullName.ToUpper());

                if (objPoc.SchoolProfile != null)
                {
                    objPoc.SchoolProfile.SchoolName = CutSchoolName(objPoc.SchoolProfile.SchoolName);
                    firstSheet.SetCellValue("C29", objPoc.SchoolProfile.SchoolName);

                    double heigthSN = SetRowHeigth(objPoc.SchoolProfile.SchoolName, 24, 25, 25);
                    firstSheet.SetRowHeight(26, heigthSN);
                }
                firstSheet.SetCellValue("C30", objPoc.SchoolProfile != null && objPoc.SchoolProfile.Commune != null ? objPoc.SchoolProfile.Commune.CommuneName : string.Empty);
                firstSheet.SetCellValue("C31", objPoc.SchoolProfile != null && objPoc.SchoolProfile.District != null ? objPoc.SchoolProfile.District.DistrictName : string.Empty);
                firstSheet.SetCellValue("C32", objPoc.SchoolProfile != null && objPoc.SchoolProfile.Province != null ? objPoc.SchoolProfile.Province.ProvinceName : string.Empty);
            }

            // Chỉnh chiều cao tự động cho một số ô trong Trang bìa

            #endregion

            #region Trang 1
            // Lấy thông tin Quá trình học tập
            List<AcademicYearBO> queryPOC = queryPOCAll.Where(o => o.PupilID == pupilID).OrderBy(o => o.Year).ToList();

            // Fill dữ liệu Trang 1
            if (objPoc != null)
            {
                thirdSheet.SetCellValue("C3", objPoc.PupilFullName.ToUpper());
                thirdSheet.SetCellValue("H3", objPoc.Genre == GlobalConstants.GENRE_MALE ? "Nam" : "Nữ");
                thirdSheet.SetCellValue("C4", objPoc.Birthday.ToString("dd/MM/yyyy"));
                thirdSheet.SetCellValue("F4", objPoc.EthnicName);
                thirdSheet.SetCellValue("H4", "Việt Nam");
                thirdSheet.SetCellValue("C5", objPoc.BirthPlace);
                thirdSheet.SetCellValue("G5", objPoc.HomeTown);
                thirdSheet.SetCellValue("C6", !string.IsNullOrEmpty(objPoc.TempReidentalAddress) ? objPoc.TempReidentalAddress : objPoc.PermanentResidentalAddress);
                thirdSheet.SetCellValue("C7", objPoc.FatherFullName);
                thirdSheet.SetCellValue("C8", objPoc.MotherFullName);
                thirdSheet.SetCellValue("C9", objPoc.SponserFullName);
                thirdSheet.SetCellValue("E20", school.HeadMasterName);
            }
            if (fillPaging)
            {
                lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                {
                    SheetIndex = sheetPosition,
                    IsHeaderOrFooter = true,
                    AlignPageNumber = 2,
                    StartPageNumberOfSheet = startPageNumberOfSheet
                });
            }
            // Chỉnh chiều cao tự động cho một số ô trong Trang 1

            // Fill địa điểm, ngày
            DateTime current = DateTime.Now;
            string strDate = string.Empty;
            string schoolProvince = school.District != null ? school.District.DistrictName : ".........";
            strDate = schoolProvince + ", ngày " + objPoc.EnrolmentDate.Day + " tháng " + objPoc.EnrolmentDate.Month + " năm " + objPoc.EnrolmentDate.Year;
            thirdSheet.SetCellValue("E11", strDate);

            int startRowQTHT = 25;
            IVTRange row = thirdSheet.GetRow(startRowQTHT);
            AcademicYearBO posTemp = null;
            for (int i = 0; i < queryPOC.Count; i++)
            {
                posTemp = queryPOC[i];
                posTemp.SchoolName = CutSchoolName(posTemp.SchoolName);
                posTemp.ClassName = CutClassName(posTemp.ClassName);
                if (i > 7)
                {
                    thirdSheet.CopyPasteSameSize(row, startRowQTHT + 1, 1);
                }
                thirdSheet.SetCellValue("A" + (startRowQTHT + 1).ToString(), posTemp.DisplayTitle);
                thirdSheet.SetCellValue("B" + (startRowQTHT + 1).ToString(), posTemp.ClassName);
                thirdSheet.SetCellValue("C" + (startRowQTHT + 1).ToString(), posTemp.SchoolName);

                // Tính chiều cao cho ô tên lớp và tên trường, lấy chiều cao lớn hơn
                double heigthCN = SetRowHeigth(posTemp.ClassName, 6, 24, 24);
                double heigthSN = SetRowHeigth(posTemp.SchoolName, 30, 24, 24);
                thirdSheet.SetRowHeight(startRowQTHT + 1, heigthCN > heigthSN ? heigthCN : heigthSN);

                thirdSheet.SetCellValue("G" + (startRowQTHT + 1).ToString(), posTemp.StorageName);
                thirdSheet.SetCellValue("H" + (startRowQTHT + 1).ToString(), posTemp.EnrolmentDate != null ? posTemp.EnrolmentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                startRowQTHT++;
            }

            // Xóa dòng template mẫu
            thirdSheet.DeleteRow(25);
            thirdSheet.PrintArea = "$A$1:$H$32";
            #endregion

            #region Trang 2
            // Lấy danh sách năm và học kì cần báo cáo
            List<AcademicYearBO> listAcademicYear = queryPOC.Where(o => o.PupilID == pupilID).OrderBy(o => o.Year).ToList();
            // Lấy tất cả các thông số về sức khỏe và số ngày nghỉ của học sinh
            List<InfoEvaluationPupil> queryInfoPupil = queryInfoPupilAll.Where(o => o.PupilID == pupilID).ToList();
            List<PupilAbsence> queryPupilAbsence = queryPupilAbsenceAll.Where(o => o.PupilID == pupilID).ToList();
            // Lấy nhận xét và điểm các môn - SummedEvaluation và SummedEvaluationHistory
            List<SummedEvaluation> querySE = querySEAll.Where(o => o.PupilID == pupilID).ToList();
            List<SummedEvaluationHistory> querySEH = querySEHAll.Where(o => o.PupilID == pupilID).ToList();
            // Lấy nhận xét năng lực và phẩm chất - SummedEndingEvaluation
            List<SummedEndingEvaluation> querySEE = querySEEAll.Where(o => o.PupilID == pupilID).ToList();
            // Lấy nhận xét, khen thưởng của học sinh - RewardCommentFinal và RewardFinal
            List<RewardCommentFinal> queryRCF = queryRCFAll.Where(o => o.PupilID == pupilID).ToList();
            // Duyệt qua các năm, tạo 2 sheet tương ứng HKI và CaNam
            AcademicYearBO tempAca = null;
            List<int> listSemester = new List<int>() { GlobalConstants.SEMESTER_OF_YEAR_FIRST, GlobalConstants.SEMESTER_OF_YEAR_SECOND };

            // Biến tạm chứa dữ liệu phần thông tin sức khỏe, ngày nghỉ
            InfoEvaluationPupil tempInfo = null;
            List<PupilAbsence> listPupilAbsence = null;
            List<PupilAbsence> listPupilAbsenceAccept = null;
            List<PupilAbsence> listPupilAbsenceNonAccept = null;
            ClassProfile classAca = null;

            // Biến tạm chứa thông tin phần Môn học, Nhận xét, Năng lực và Phẩm chất
            ClassSubjectBO subjectTemp = null;
            List<ClassSubjectBO> listClassSJ = null;
            TranscriptsSummedEvaluationBO tempTSE = null;
            List<TranscriptsSummedEvaluationBO> queryTSE = null;
            List<TranscriptsSummedEvaluationBO> listSE = null;
            List<SummedEndingEvaluation> listSEE = null;
            SummedEndingEvaluation tempSEE = null;
            List<RewardCommentFinal> listRCF = null;
            List<RewardFinal> listRF = null;
            List<EvaluationReward> lstERByAcademicYear = null;
            RewardCommentFinal tempRCF = null;
            int headTeacherId = 0;
            startPageNumberOfSheet++;
            for (int i = 0; i < listAcademicYear.Count; i++)
            {
                // Xét năm tương ứng
                tempAca = listAcademicYear[i];
                int partionAca = UtilsBusiness.GetPartionId(tempAca.SchoolID);
                AcademicYear objAcaHis = AcademicYearBusiness.Find(tempAca.AcademicYearID);
                classAca = lstCpAll.Where(o => o.ClassProfileID == tempAca.ClassID).FirstOrDefault();
                classAca.DisplayName = CutClassName(classAca.DisplayName);

                #region // Kiểm tra lấy dữ liệu từ bảng hiện tại hay bảng lịch sử
                if (UtilsBusiness.IsMoveHistory(objAcaHis))
                {
                    queryTSE = (from se in querySEH
                                where se.AcademicYearID == tempAca.AcademicYearID && se.SchoolID == tempAca.SchoolID && se.LastDigitSchoolID == partionAca
                                && se.ClassID == tempAca.ClassID
                                select new TranscriptsSummedEvaluationBO
                                {
                                    SummedEvaluationID = se.SummedEvaluationID,
                                    PupilID = se.PupilID,
                                    ClassID = se.ClassID,
                                    EducationLevelID = se.EducationLevelID,
                                    AcademicYearID = se.AcademicYearID,
                                    LastDigitSchoolID = se.LastDigitSchoolID,
                                    SchoolID = se.SchoolID,
                                    SemesterID = se.SemesterID,
                                    EvaluationCriteriaID = se.EvaluationCriteriaID,
                                    EvaluationID = se.EvaluationID,
                                    EndingEvaluation = se.EndingEvaluation,
                                    EndingComments = se.EndingComments,
                                    PeriodicEndingMark = se.PeriodicEndingMark

                                }).ToList();
                }
                else
                {
                    queryTSE = (from se in querySE
                                where se.AcademicYearID == tempAca.AcademicYearID && se.SchoolID == tempAca.SchoolID && se.LastDigitSchoolID == partionAca
                                && se.ClassID == tempAca.ClassID
                                select new TranscriptsSummedEvaluationBO
                                {
                                    SummedEvaluationID = se.SummedEvaluationID,
                                    PupilID = se.PupilID,
                                    ClassID = se.ClassID,
                                    EducationLevelID = se.EducationLevelID,
                                    AcademicYearID = se.AcademicYearID,
                                    LastDigitSchoolID = se.LastDigitSchoolID,
                                    SchoolID = se.SchoolID,
                                    SemesterID = se.SemesterID,
                                    EvaluationCriteriaID = se.EvaluationCriteriaID,
                                    EvaluationID = se.EvaluationID,
                                    EndingEvaluation = se.EndingEvaluation,
                                    EndingComments = se.EndingComments,
                                    PeriodicEndingMark = se.PeriodicEndingMark

                                }).ToList();
                }
                #endregion
                #region fill TT30
                if (tempAca.Year < 2016)
                {
                    //Fill ten giao vien chu nhiem
                    headTeacherId = lstCpAll.Where(p => p.ClassProfileID == tempAca.ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                    Employee employeeObj = lstEmployeeAll.Where(o => o.EmployeeID == headTeacherId).FirstOrDefault();
                    // Duyệt học kì
                    for (int j = 0; j < listSemester.Count; j++)
                    {
                        int semesterID = listSemester[j];
                        IVTWorksheet sheetTemp = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? fourthSheet : fifthSheet;
                        IVTWorksheet sheetFill = oBook.CopySheetToBeforeLast(sheetTemp);

                        // Lọc dữ liệu theo năm, trường, lớp và học kì
                        //sửa lại yêu cầu nếu không có MonitoringDate vẫn lấy ra dữ liệu
                        tempInfo = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ?
                            queryInfoPupil.Where(o => o.AcademicYear == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID &&
                                                      (o.MonitoringDate == null ||
                                                        (o.MonitoringDate != null && o.MonitoringDate >= tempAca.FirstSemesterStartDate && o.MonitoringDate <= tempAca.FirstSemesterEndDate))
                                                        && o.HealthPeriodID == 1
                                                      ).FirstOrDefault() :
                            queryInfoPupil.Where(o => o.AcademicYear == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID &&
                                                      (o.MonitoringDate == null || (o.MonitoringDate != null && o.MonitoringDate >= tempAca.SecondSemesterStartDate && o.MonitoringDate <= tempAca.SecondSemesterEndDate))
                                                      && o.HealthPeriodID == 2
                                                      ).FirstOrDefault();

                        listPupilAbsence = queryPupilAbsence.Where(o => o.AcademicYearID == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && lstSectionKeyID.Contains(o.Section)).ToList();

                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            listPupilAbsence = listPupilAbsence.Where(o => o.AbsentDate >= tempAca.FirstSemesterStartDate.Value && o.AbsentDate <= tempAca.FirstSemesterEndDate.Value).ToList();
                        }
                        else
                        {
                            listPupilAbsence = listPupilAbsence.Where(o => o.AbsentDate >= tempAca.SecondSemesterStartDate.Value && o.AbsentDate <= tempAca.SecondSemesterEndDate.Value).ToList();
                        }

                        listPupilAbsenceAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).ToList();
                        listPupilAbsenceNonAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).ToList();
                        listSE = queryTSE.Where(o => o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && o.SemesterID == semesterID).ToList();
                        listSEE = querySEE.Where(o => o.AcademicYearID == tempAca.AcademicYearID && (o.SemesterID == semesterID || o.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT) && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID).ToList();

                        // Lọc dữ liệu phần khen thưởng
                        listRCF = queryRCF.Where(o => o.AcademicYearID == tempAca.AcademicYearID
                            && o.SchoolID == tempAca.SchoolID
                            && o.ClassID == tempAca.ClassID
                            && o.SemesterID == semesterID
                            && o.PupilID == pupilID)
                            .ToList();
                        listRF = queryRF.Where(o => o.SchoolID == tempAca.SchoolID).ToList();

                        // Fill dữ liệu phần Sức khỏe và Ngày nghỉ
                        sheetFill.SetCellValue("A2", objPoc != null ? "Họ và tên học sinh: " + objPoc.PupilFullName : string.Empty);
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            sheetFill.SetCellValue("A3", tempAca != null ? "TỔNG HỢP ĐÁNH GIÁ CUỐI HỌC KÌ I NĂM HỌC " + tempAca.DisplayTitle : string.Empty);
                        }
                        else
                        {
                            sheetFill.SetCellValue("A3", tempAca != null ? "TỔNG HỢP ĐÁNH GIÁ CUỐI NĂM HỌC " + tempAca.DisplayTitle : string.Empty);
                        }

                        sheetFill.SetCellValue("C4", tempInfo != null ? (tempInfo.Height % 1 == 0 ? tempInfo.Height + " cm" : string.Format("{0:0.0}", tempInfo.Height) + " cm") : string.Empty);
                        sheetFill.SetCellValue("H4", tempInfo != null ? (tempInfo.Weight % 1 == 0 ? tempInfo.Weight + " kg" : string.Format("{0:0.0}", tempInfo.Weight) + " kg") : string.Empty);
                        sheetFill.SetCellValue("O4", tempInfo != null && tempInfo.EvaluationHealth == 1 ? "Loại I (Tốt)" : tempInfo != null && tempInfo.EvaluationHealth == 2 ? "Loại II (Khá)" :
                                                     tempInfo != null && tempInfo.EvaluationHealth == 3 ? "Loại III (Trung bình)" : tempInfo != null && tempInfo.EvaluationHealth == 4 ? "Loại IV (Yếu)" :
                                                     tempInfo != null && tempInfo.EvaluationHealth == 5 ? "Loại V (Rất yếu)" : string.Empty);
                        sheetFill.SetCellValue("C5", listPupilAbsence.Count);
                        sheetFill.SetCellValue("H5", listPupilAbsenceAccept.Count);
                        sheetFill.SetCellValue("O5", listPupilAbsenceNonAccept.Count);


                        // Fill dữ liệu phần Nhận xét môn học
                        listClassSJ = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? lstClassSubject.Where(o => o.ClassID == tempAca.ClassID &&
                                                                                                o.SectionPerWeekFirstSemester > 0).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList() :
                                                                                             lstClassSubject.Where(o => o.ClassID == tempAca.ClassID
                                                                                                 && o.SectionPerWeekSecondSemester > 0).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
                        int startRowMH = 11;
                        int countWord = 55;
                        int countWordTD = 55;
                        int countWordNX = 65;
                        int index = startRowMH;
                        IVTRange rowMHTD = sheetFill.GetRow(9);
                        IVTRange rowMHNX = sheetFill.GetRow(10);
                        IVTRange rowTemp = null;
                        for (int k = 0; k < listClassSJ.Count; k++)
                        {
                            subjectTemp = listClassSJ[k];
                            // Lấy dữ liệu tương ứng với môn
                            tempTSE = listSE.Where(o => o.EvaluationCriteriaID == subjectTemp.SubjectID && o.EvaluationID == GlobalConstants.TAB_EDUCATION).FirstOrDefault();

                            rowTemp = subjectTemp != null && subjectTemp.IsCommenting.HasValue && subjectTemp.IsCommenting.Value == 1 ? rowMHNX : rowMHTD;
                            sheetFill.CopyPasteSameSize(rowTemp, index, 1);

                            sheetFill.SetCellValue(index, 1, subjectTemp.SubjectName);
                            sheetFill.SetCellValue(index, 6, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            if (subjectTemp.IsCommenting.HasValue == false || subjectTemp.IsCommenting == 0)
                            {
                                countWord = countWordNX;
                                sheetFill.SetCellValue(index, 5, tempTSE != null && tempTSE.PeriodicEndingMark.HasValue ? tempTSE.PeriodicEndingMark.Value.ToString() : string.Empty);
                            }
                            else
                            {
                                countWord = countWordTD;
                            }
                            index++;
                        }

                        double defaultArea = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 600 : 660;
                        double newHeight = (defaultArea / (index - 11));
                        for (int h = 11; h < index; h++)
                        {
                            sheetFill.SetRowHeight(h, newHeight);
                        }

                        #region// Fill dữ liệu trước khi copy
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            int startSheetComment = 2;
                            IVTWorksheet eightSheet = oBook.CopySheetToBeforeLast(eightTT30Sheet);
                            IVTRange rangComment = eightTT30Sheet.GetRange("A2", "P32");
                            eightSheet.CopyPasteSameSize(rangComment, 2, 1);

                            eightSheet.SetCellValue(startSheetComment, 1, tempAca != null ? "Trường: " + this.CutSchoolName(tempAca.SchoolName) : "Trường: ");
                            eightSheet.SetCellValue(startSheetComment, 15, tempAca != null ? "Lớp: " + this.CutClassName(tempAca.ClassName) : "Lớp: ");

                            #region Dien du lieu nang luc
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                eightSheet.SetCellValue(startSheetComment + 2, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Ð") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID1).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 5, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID2).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 6, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID3).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 7, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            #region Dien du lieu pham chat
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.PupilID == pupilID).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                eightSheet.SetCellValue(startSheetComment + 9, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Ð") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID4).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 12, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID5).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 13, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID6).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 14, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID7).FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 15, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            #region //Nhan xet, khen thuong
                            tempRCF = listRCF.FirstOrDefault();
                            eightSheet.SetCellValue(startSheetComment + 18, 1, tempRCF != null ? tempRCF.OutstandingAchievement : string.Empty);
                            var listRCFID = new List<int>();
                            string strRewardMode = string.Empty;
                            foreach (RewardFinal rw in listRF)
                            {

                                if (tempRCF != null && tempRCF.RewardID != null && tempRCF.RewardID.Contains(rw.RewardFinalID.ToString()))
                                {
                                    if (listRCFID.Contains(tempRCF.RewardCommentFinalID))
                                    {
                                        strRewardMode = strRewardMode + rw.RewardMode + ", ";
                                        listRCFID.Add(tempRCF.RewardCommentFinalID);
                                    }
                                }
                            }
                            eightSheet.SetCellValue(startSheetComment + 20, 1, strRewardMode.Length > 0 ? strRewardMode.Substring(0, strRewardMode.Length - 2) : string.Empty);
                            #endregion

                            #region Ngay, dia diem
                            eightSheet.SetCellValue((startSheetComment + 21), 7, schoolProvince + ", ngày " + current.Day + " tháng " + current.Month + " năm " + current.Year);
                            eightSheet.SetCellValue((startSheetComment + 28), 7, employeeObj != null ? employeeObj.FullName : "");

                            IVTRange rang = eightSheet.GetRange("A2", "P32");
                            sheetFill.CopyPasteSameSize(rang, (index + 1), 1);
                            eightSheet.Delete();
                            #endregion
                        }
                        else
                        {
                            int startSheetComment = 2;
                            IVTWorksheet nineSheet = oBook.CopySheetToBeforeLast(nineTT30Sheet);
                            IVTRange rangComment = nineTT30Sheet.GetRange("A2", "P34");
                            nineSheet.CopyPasteSameSize(rangComment, 2, 1);

                            nineSheet.SetCellValue(startSheetComment, 1, tempAca != null ? "Trường: " + this.CutSchoolName(tempAca.SchoolName) : "Trường: ");
                            nineSheet.SetCellValue(startSheetComment, 15, tempAca != null ? "Lớp: " + this.CutClassName(tempAca.ClassName) : "Lớp: ");

                            #region Dien du lieu nang luc
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                nineSheet.SetCellValue(startSheetComment + 2, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Ð") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID1).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 5, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID2).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 6, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_CAPACTIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID3).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 7, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            #region Dien du lieu pham chat
                            tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES).FirstOrDefault();
                            if (tempSEE != null)
                            {
                                nineSheet.SetCellValue(startSheetComment + 9, tempSEE.EndingEvaluation != null && tempSEE.EndingEvaluation.Equals("Ð") ? 11 : 15, "X");
                            }

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID4).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 12, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID5).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 13, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID6).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 14, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);

                            tempTSE = listSE.Where(o => o.EvaluationID == GlobalConstants.TAB_QUALITIES && o.EvaluationCriteriaID == GlobalConstants.EVALUATION_CRITERIA_ID7).FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 15, 7, tempTSE != null ? tempTSE.EndingComments : string.Empty);
                            #endregion

                            #region
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (tempAca.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID1 && tempAca.EducationLevelID <= GlobalConstants.EDUCATION_LEVEL_ID4)
                                {
                                    tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.EVALUATION_RESULT).FirstOrDefault();
                                    nineSheet.SetCellValue(startSheetComment + 22, 1, tempSEE != null && "HT".Equals(tempSEE.EndingEvaluation) ? "Hoàn thành chương trình lớp học." : "Không hoàn thành chương trình lớp học");
                                }
                                else if (tempAca.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID5)
                                {
                                    tempSEE = listSEE.Where(o => o.EvaluationID == GlobalConstants.EVALUATION_RESULT).FirstOrDefault();
                                    nineSheet.SetCellValue(startSheetComment + 22, 1, tempSEE != null && "HT".Equals(tempSEE.EndingEvaluation) ? "Hoàn thành chương trình tiểu học." : "Không hoàn thành chương trình tiểu học");
                                }
                            }
                            #endregion

                            #region
                            tempRCF = listRCF.FirstOrDefault();
                            nineSheet.SetCellValue(startSheetComment + 18, 1, tempRCF != null ? tempRCF.OutstandingAchievement : string.Empty);

                            string strRewardMode = string.Empty;
                            foreach (RewardFinal rw in listRF)
                            {
                                if (tempRCF != null && tempRCF.RewardID != null && tempRCF.RewardID.Contains(rw.RewardFinalID.ToString()))
                                {
                                    strRewardMode = strRewardMode + rw.RewardMode + ", ";
                                }
                            }
                            nineSheet.SetCellValue(startSheetComment + 20, 1, strRewardMode.Length > 0 ? strRewardMode.Substring(0, strRewardMode.Length - 2) : string.Empty);
                            #endregion

                            #region
                            nineSheet.SetCellValue((startSheetComment + 23), 7, schoolProvince + ", ngày " + current.Day + " tháng " + current.Month + " năm " + current.Year);
                            nineSheet.SetCellValue((startSheetComment + 29), 1, school.HeadMasterName);
                            nineSheet.SetCellValue((startSheetComment + 29), 7, employeeObj != null ? employeeObj.FullName : "");

                            IVTRange rang = nineSheet.GetRange("A2", "P34");
                            sheetFill.CopyPasteSameSize(rang, (index + 1), 1);
                            nineSheet.Delete();
                            #endregion
                        }
                        #endregion

                        int rowLast = (index + (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 28 : 30));
                        // Xóa 2 dòng template mẫu
                        sheetFill.DeleteRow(9);
                        sheetFill.DeleteRow(9);

                        sheetFill.PageMaginBottom = 0.5;
                        sheetFill.SetBreakPage(index - 1);
                        sheetFill.Name = semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "CuoiHKI " + tempAca.DisplayTitle : "CuoiNam " + tempAca.DisplayTitle;
                        sheetFill.SetFontName("Times New Roman", 0);

                        sheetFill.PrintArea = "$A$1:$P$" + rowLast;
                        if (fillPaging)
                        {
                            sheetPosition++;
                            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                            {
                                SheetIndex = sheetPosition,
                                IsHeaderOrFooter = true,
                                AlignPageNumber = 2,
                                StartPageNumberOfSheet = startPageNumberOfSheet,
                            });
                            startPageNumberOfSheet += 2;
                        }
                    }
                }
                #endregion
                #region fill TT22
                else
                {
                    IVTWorksheet sheetFill = oBook.CopySheetToBeforeLast(sixthSheet);
                    IVTWorksheet seventhSheet = oBook.CopySheetToBeforeLast(seventhTT22Sheet);
                    IVTRange rangComment = seventhTT22Sheet.GetRange("A2", "P32");
                    seventhSheet.CopyPasteSameSize(rangComment, 2, 1);

                    //fill ho ten
                    sheetFill.SetCellValue("D2", objPoc.PupilFullName);
                    sheetFill.SetCellValue("O2", classAca.DisplayName);

                    //fill thong tin y te
                    //sửa lại yêu cầu nếu không có MonitoringDate vẫn lấy ra dữ liệu
                    tempInfo = queryInfoPupil.Where(o => o.AcademicYear == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID &&
                                                  (o.MonitoringDate == null || (o.MonitoringDate != null && o.MonitoringDate >= tempAca.SecondSemesterStartDate && o.MonitoringDate <= tempAca.SecondSemesterEndDate))
                                                  && o.HealthPeriodID == 2
                                                  ).FirstOrDefault();

                    listPupilAbsence = queryPupilAbsence.Where(o => o.AcademicYearID == tempAca.AcademicYearID && o.SchoolID == tempAca.SchoolID && o.ClassID == tempAca.ClassID && lstSectionKeyID.Contains(o.Section)).ToList();
                    listPupilAbsence = listPupilAbsence.Where(o => o.AbsentDate >= tempAca.FirstSemesterStartDate.Value && o.AbsentDate <= tempAca.SecondSemesterEndDate.Value).ToList();

                    //listPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(listPupilAbsence);
                    listPupilAbsenceAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue && o.IsAccepted.Value == true).ToList();
                    listPupilAbsenceNonAccept = listPupilAbsence.Where(o => o.IsAccepted.HasValue == false || o.IsAccepted.Value == false).ToList();
                    // Fill dữ liệu phần Sức khỏe và Ngày nghỉ
                    sheetFill.SetCellValue("C3", tempInfo != null ? (tempInfo.Height % 1 == 0 ? tempInfo.Height + " cm" : string.Format("{0:0.0}", tempInfo.Height) + " cm") : string.Empty);
                    sheetFill.SetCellValue("H3", tempInfo != null ? (tempInfo.Weight % 1 == 0 ? tempInfo.Weight + " kg" : string.Format("{0:0.0}", tempInfo.Weight) + " kg") : string.Empty);
                    sheetFill.SetCellValue("O3", tempInfo != null && tempInfo.EvaluationHealth == 1 ? "Loại I (Tốt)" : tempInfo != null && tempInfo.EvaluationHealth == 2 ? "Loại II (Khá)" :
                                                 tempInfo != null && tempInfo.EvaluationHealth == 3 ? "Loại III (Trung bình)" : tempInfo != null && tempInfo.EvaluationHealth == 4 ? "Loại IV (Yếu)" :
                                                 tempInfo != null && tempInfo.EvaluationHealth == 5 ? "Loại V (Rất yếu)" : string.Empty);
                    sheetFill.SetCellValue("C4", listPupilAbsence.Count);
                    sheetFill.SetCellValue("H4", listPupilAbsenceAccept.Count);
                    sheetFill.SetCellValue("O4", listPupilAbsenceNonAccept.Count);

                    List<VRatedCommentPupil> lstRatedSubject = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                                                                                    .Where(x => x.AcademicYearID == tempAca.AcademicYearID).ToList();
                    List<VRatedCommentPupil> lstRatedCapQua = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY)
                                                                                    .Where(x => x.AcademicYearID == tempAca.AcademicYearID).ToList();
                    VRatedCommentPupil objRatedComentPupil = null;
                    //danh sach mon hoc
                    listClassSJ = lstClassSubject.Where(o => o.SchoolID == tempAca.SchoolID &&
                    o.EducationLevelID == tempAca.EducationLevelID && o.ClassID == tempAca.ClassID &&
                    o.SectionPerWeekSecondSemester > 0).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
                    int startrowMH = 8;
                    IVTRange rangeCopy = sheetFill.GetRange(startrowMH, 1, startrowMH, 16);
                    int startRowComment = 0;
                    int numSubjectExceed = listClassSJ.Count - 12;
                    for (int j = 0; j < listClassSJ.Count; j++)
                    {
                        subjectTemp = listClassSJ[j];

                        if (j >= 2)
                        {
                            sheetFill.CopyAndInsertARow(rangeCopy, startrowMH, true);
                        }
                        if (subjectTemp.IsCommenting == 0)
                        {
                        }
                        else
                        {
                            if (startRowComment == 0)
                            {
                                startRowComment = startrowMH;
                            }
                        }

                        sheetFill.SetCellValue(startrowMH, 1, subjectTemp.DisplayName);
                        sheetFill.SetCellValue(startrowMH, 5, "");
                        objRatedComentPupil = lstRatedSubject.Where(p => p.SubjectID == subjectTemp.SubjectID && p.AcademicYearID == tempAca.AcademicYearID).FirstOrDefault();
                        if (objRatedComentPupil != null)
                        {
                            sheetFill.SetCellValue(startrowMH, 8, objRatedComentPupil.Comment);
                            sheetFill.GetRange(startrowMH, 8, startrowMH, 12).SetHAlign(VTHAlign.xlHAlignLeft);
                            if (subjectTemp.IsCommenting == 0)
                            {
                                sheetFill.SetCellValue(startrowMH, 5, objRatedComentPupil.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.EndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_COMPLETE : objRatedComentPupil.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                sheetFill.SetCellValue(startrowMH, 7, objRatedComentPupil.PeriodicEndingMark);
                            }
                            else
                            {
                                sheetFill.SetCellValue(startrowMH, 5, objRatedComentPupil.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.EndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_COMPLETE : objRatedComentPupil.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                            }
                        }
                        startrowMH++;
                    }


                    double defaultArea = 580;
                    double newHeight = defaultArea / (startrowMH - 8);
                    for (int k = 8; k < startrowMH; k++)
                    {
                        if (newHeight > 150)
                            newHeight = 48;
                        sheetFill.SetRowHeight(k, newHeight);
                    }

                    if (startRowComment > 0)
                    {
                        sheetFill.GetRange(startRowComment, 7, startrowMH - 1, 7).Merge();
                    }

                    #region// fill vang luc, phan chat, khen thuong, hoan thanh lop hoc khi copy
                    #region// nang luc phan chat
                    seventhSheet.SetCellValue("A2", objAcaHis != null ? "Trường: " + objAcaHis.SchoolProfile.SchoolName : "Trường: ");
                    seventhSheet.SetCellValue("K2", objAcaHis != null ? "Năm học: " + objAcaHis.DisplayTitle : "Năm học: ");
                    int startRowNL = 7;
                    int startRowPC = 12;
                    EvaluationCriteria objEC = null;
                    for (int j = 0; j < lstEvaluationCriteria.Count; j++)
                    {
                        objEC = lstEvaluationCriteria[j];
                        objRatedComentPupil = lstRatedCapQua.Where(p => p.EvaluationID == objEC.TypeID && p.SubjectID == (objEC.EvaluationCriteriaID)).FirstOrDefault();
                        if (objEC.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            if (objRatedComentPupil != null)
                            {
                                seventhSheet.SetCellValue(startRowNL, 5, objRatedComentPupil.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.CapacityEndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComentPupil.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                seventhSheet.SetCellValue(startRowNL, 7, objRatedComentPupil.Comment);
                            }
                            startRowNL++;
                        }
                        else
                        {
                            if (objRatedComentPupil != null)
                            {
                                seventhSheet.SetCellValue(startRowPC, 5, objRatedComentPupil.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComentPupil.QualityEndingEvaluation == 2 ?
                                    GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComentPupil.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                seventhSheet.SetCellValue(startRowPC, 7, objRatedComentPupil.Comment);
                            }
                            startRowPC++;
                        }
                    }
                    #endregion

                    #region// Khen thuong
                    lstERByAcademicYear = lstER.Where(x => x.AcademicYearID == tempAca.AcademicYearID && x.PupilID == pupilID).ToList();
                    EvaluationReward objER = null;
                    SummedEndingEvaluation objSEE = querySEE.Where(p => p.AcademicYearID == tempAca.AcademicYearID && p.ClassID == tempAca.ClassID
                                                                        && p.EvaluationID == GlobalConstants.EVALUATION_RESULT).FirstOrDefault();
                    string strSEE = objSEE != null && objSEE.Reward.HasValue ? "Hoàn thành xuất sắc các môn học và rèn luyện" : "";
                    string strER = string.Empty;
                    for (int j = 0; j < lstERByAcademicYear.Count; j++)
                    {
                        objER = lstERByAcademicYear[j];
                        if (j < lstERByAcademicYear.Count - 1)
                        {
                            strER += objER.Content + "; ";
                        }
                        else
                        {
                            strER += objER.Content + ".";
                        }

                    }
                    seventhSheet.SetCellValue("A18", !string.IsNullOrEmpty(strSEE) ? (strSEE + "; " + strER) : strER);
                    #endregion

                    #region//hoàn thành CHLH
                    string strEvaluationResult = string.Empty;
                    if (objSEE != null)
                    {
                        if (!string.IsNullOrEmpty(objSEE.EndingEvaluation))
                        {
                            if (educationLevelID < 5)
                            {
                                if (objSEE.EndingEvaluation == "HT")
                                {
                                    strEvaluationResult = "Hoàn thành chương trình lớp " + educationLevelID + "; Được lên lớp " + (educationLevelID + 1);
                                }
                                else
                                {
                                    strEvaluationResult = "Chưa hoàn thành chương trình lớp " + educationLevelID;
                                    if (objSEE.RateAdd.HasValue)
                                    {
                                        if (objSEE.RateAdd.Value == 1)
                                        {
                                            strEvaluationResult += "; Được lên lớp " + +(educationLevelID + 1);
                                        }
                                        else if (objSEE.RateAdd.Value == 0)
                                        {
                                            strEvaluationResult += "; Ở lại lớp " + educationLevelID;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (objSEE.EndingEvaluation == "HT")
                                {
                                    strEvaluationResult = "Hoàn thành chương trình tiểu học";
                                }
                                else
                                {
                                    strEvaluationResult = "Chưa hoàn thành chương trình tiểu học";
                                }
                            }
                        }
                    }
                    seventhSheet.SetCellValue("A20", strEvaluationResult + (!string.IsNullOrEmpty(strEvaluationResult) ? "." : ""));
                    #endregion

                    string _strDateCurrent = schoolProvince + ", ngày " + current.Day + " tháng " + current.Month + " năm " + current.Year;
                    seventhSheet.SetCellValue("G22", _strDateCurrent);

                    headTeacherId = lstCpAll.Where(p => p.ClassProfileID == tempAca.ClassID && p.HeadTeacherID.HasValue).Select(p => p.HeadTeacherID.Value).FirstOrDefault();
                    Employee employeeObj = EmployeeBusiness.Find(headTeacherId);
                    seventhSheet.SetCellValue("G31", employeeObj != null ? employeeObj.FullName : string.Empty);
                    seventhSheet.SetCellValue("A31", objAcaHis != null ? objAcaHis.SchoolProfile.HeadMasterName : string.Empty);
                    #endregion

                    IVTRange rang = seventhSheet.GetRange("A2", "P32");
                    sheetFill.CopyPasteSameSize(rang, (startrowMH + 1), 1);
                    sheetFill.SetBreakPage((startrowMH + 1));
                    sheetFill.Name = string.Format("NH {0}", tempAca.DisplayTitle);
                    sheetFill.SetFontName("Times New Roman", 0);
                    seventhSheet.Delete();

                    int rowLast = (startrowMH + 1) + 29;
                    sheetFill.PrintArea = "$A$1:$P$" + rowLast;
                    if (fillPaging)
                    {
                        sheetPosition++;
                        lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                        {
                            SheetIndex = sheetPosition,
                            IsHeaderOrFooter = true,
                            AlignPageNumber = 2,
                            StartPageNumberOfSheet = startPageNumberOfSheet,
                        });
                        startPageNumberOfSheet += 2;
                    }
                }
                #endregion
            }
            #endregion

            // Xóa sheet mẫu        
            fourthSheet.Delete();
            fifthSheet.Delete();
            sixthSheet.Delete();
            seventhTT22Sheet.Delete();
            eightTT30Sheet.Delete();
            nineTT30Sheet.Delete();
        }

        public ProcessedReport InsertReportPrimaryTranscripts(IDictionary<string, object> dic, Stream data, bool isZip = false)
        {
            string reportCode = isZip ? SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_ZIPFILE : SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS_TT22;
            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            //pr.ReportData = ReportUtils.Compress(data);
            pr.ReportData = isZip ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);
            // Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            long pupilID = Utils.GetLong(dic, "PupilProfileID");
            PupilProfile pupil = PupilProfileBusiness.Find(pupilID);
            if (!isZip)
            {
                string PupilName = Utils.StripVNSignAndSpace(PupilProfileBusiness.Find(pupilID).FullName);
                outputNamePattern = outputNamePattern.Replace("[FullName]", ReportUtils.StripVNSign(PupilName));
            }

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;

        }

        private double SetRowHeigth(string text, double countWord, double heigthDefault, double heigthOneRow)
        {
            double heigth = 0;
            if (text != null)
            {
                string[] arrText = text.Trim().Replace("\r\n", "#").Split('#');
                double countRow = 0;
                for (int i = 0; i < arrText.Length; i++)
                {
                    double rows = Math.Round(((double)arrText[i].Length / countWord) + 0.5, 0);
                    countRow = countRow + rows;
                }

                heigth = (countRow * heigthOneRow);
                if (heigth < heigthDefault)
                {
                    heigth = heigthDefault;
                }
            }
            else
            {
                heigth = heigthDefault;
            }

            return heigth;
        }

        #endregion
    }
}