/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.SearchForm;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    ///<author>Nam ta</author>
    ///<Date>12/14/2012</Date>
    public partial class PupilAbsenceBusiness
    {
        public const int absentOn = 1;
        public const int absentOff = 2;

        #region validate
        private void validate(PupilAbsence pupilAbsence)
        {
            ValidationMetadata.ValidateObject(pupilAbsence);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilProfileID"] = pupilAbsence.PupilID;
            SearchInfo["CurrentClassID"] = pupilAbsence.ClassID;

            bool compatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", SearchInfo, null);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }
        #endregion

        #region Delete

        public void DeletePupilAbsent(int UserID, int PupilID, int ClassID, DateTime AbsentFromDate, DateTime AbsentToDate, int Section)
        {
            // Kiểm tra trạng thái học sinh vi phạm kỉ luật
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
            if (PupilID != 0)
            {
                PupilOfClass poc = PupilOfClassBusiness.All.Where(o => o.PupilID == PupilID && o.ClassID == ClassID).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
                if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("PupilAbsence_Validate_PupilNotWorking");
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) || UtilsBusiness.HasOverSeePermission(UserID, ClassID))
            {
                //Tìm kiếm trong bảng PupilAbsent theo PupilID, ClassID, AbsentFromDate, AbsentToDate. Kết quả thu được lstPupilAbsent.
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["FromAbsentDate"] = AbsentFromDate;
                SearchInfo["ToAbsentDate"] = AbsentToDate;
                SearchInfo["Section"] = Section;
                List<PupilAbsence> lstPupilAbsent = Search(SearchInfo).ToList();
                //Thực hiện xóa từng  bản ghi trong lstPupilAbsent
                base.DeleteAll(lstPupilAbsent);
                //if (lstPupilAbsent.Count != 0)
                //{
                //    foreach (var item in lstPupilAbsent)
                //    {
                //        base.Delete(item.PupilAbsenceID);
                //    }
                //}
            }
        }
        #endregion

        #region InsertOrUpdate
        /// <summary>
        /// <author>minhh</author>
        /// Thêm mới hoặc sửa thông tin học sinh vắng mặt khi điểm danh
        /// </summary>
        /// <edit>hieund9 - 21/05/2013</edit>
        /// <param name="UserID">Nguoi dung</param>
        /// <param name="PupilID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Section"></param>
        /// <param name="AbsentFromDate"></param>
        /// <param name="AbsentToDate"></param>
        public void InsertOrUpdatePupilAbsence(int UserID, bool IsAdmin, int PupilID, int ClassID, int Section, DateTime AbsentFromDate, DateTime AbsentToDate, List<PupilAbsence> lstPupilAbsence)
        {
            //Comment lai theo loi cua HungND8 - Bo qua doi voi hoc sinh khong phai la dang hoc, khong insert vao DB nua
            //if (PupilID != 0)
            //{
            //    PupilOfClass poc = PupilOfClassBusiness.All.Where(o => o.PupilID == PupilID && o.ClassID == ClassID).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
            //    if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
            //        throw new BusinessException("PupilAbsence_Validate_PupilNotWorking");
            //}

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) || UtilsBusiness.HasOverSeePermission(UserID, ClassID))
            {
                ClassProfile cls = ClassProfileBusiness.Find(ClassID);
                List<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(cls.SchoolID, new Dictionary<string, object> { { "ClassID", ClassID }, { "Check", "Check" } }).ToList();
                //Chiendd:02/07/2015, Bo sung partitionId
                int partitionId = UtilsBusiness.GetPartionId(cls.SchoolID);
                //Lay cac PupilAbsence doi voi cac hoc sinh dang hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ClassID;
                dic["FromAbsentDate"] = AbsentFromDate;
                dic["ToAbsentDate"] = AbsentToDate;
                dic["Section"] = Section;
                List<PupilAbsence> lst = this.SearchBySchool(cls.SchoolID, dic).ToList();
                lst = lst.Where(u => lstPoc.Any(v => v.PupilID == u.PupilID && v.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)).ToList();

                //Kieu loi vi pham
                List<FaultCriteria> lstFaultCriteria = FaultCriteriaBusiness.SearchBySchool(cls.SchoolID, new Dictionary<string, object>()).ToList();
                FaultCriteria faultCriteriaP = lstFaultCriteria.Where(u => u.Absence == "P").FirstOrDefault();
                FaultCriteria faultCriteriaK = lstFaultCriteria.Where(u => u.Absence == "K").FirstOrDefault();

                if (faultCriteriaP == null || !faultCriteriaP.PenalizedMark.HasValue || faultCriteriaK == null || !faultCriteriaK.PenalizedMark.HasValue)
                    throw new BusinessException("PupilAbsence_Label_NotHasFaultCriteria");

                //Lay danh sach cac hoc sinh vi pham cua lop
                IDictionary<string, object> dicFault = new Dictionary<string, object>();
                dicFault["ClassID"] = ClassID;
                dicFault["FromViolatedDate"] = AbsentFromDate;
                dicFault["ToViolatedDate"] = AbsentToDate;
                List<PupilFault> lstPupilFault = PupilFaultBusiness.SearchBySchool(cls.SchoolID, dicFault).ToList();

                //Xoa toan bo ban ghi diem danh cua lop
                this.DeleteAll(lst);

                //Cap nhat lai NumberOfFault trong bang PupilFault
                foreach (PupilAbsence pa in lst)
                {
                    FaultCriteria faultCriteria = pa.IsAccepted.Value ? faultCriteriaP : faultCriteriaK;
                    if (faultCriteria == null) throw new BusinessException("Common_Error_InternalError");

                    PupilFault pFault = lstPupilFault.Where(u => u.PupilID == pa.PupilID && u.ViolatedDate.Date == pa.AbsentDate.Date && u.FaultID == faultCriteria.FaultCriteriaID).FirstOrDefault();
                    if (pFault != null)
                    {
                        pFault.SchoolID = pa.SchoolID;
                        pFault.NumberOfFault -= 1;
                        pFault.TotalPenalizedMark = pFault.NumberOfFault * faultCriteria.PenalizedMark;

                        if (pFault.NumberOfFault > 0)
                            PupilFaultBusiness.UpdatePupilFault(UserID, IsAdmin, pFault);
                    }
                }

                //Chi them moi cac hoc sinh dang hoc
                lstPupilAbsence = lstPupilAbsence.Where(u => lstPoc.Any(v => v.PupilID == u.PupilID && v.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)).ToList();

                //Thực hiện insert vào bảng PupilAbsence
                List<PupilFault> lstInserted = new List<PupilFault>();
                foreach (PupilAbsence pupilAbsence in lstPupilAbsence)
                {
                    this.validate(pupilAbsence);
                    base.Insert(pupilAbsence);

                    FaultCriteria faultCri = pupilAbsence.IsAccepted.HasValue && pupilAbsence.IsAccepted.Value ? faultCriteriaP : faultCriteriaK;
                    if (faultCri == null) throw new BusinessException("Common_Error_InternalError");

                    PupilFault pupilFault = lstPupilFault.Where(u => u.PupilID == pupilAbsence.PupilID
                                                                        && u.ViolatedDate.Date == pupilAbsence.AbsentDate.Date
                                                                        && u.FaultID == faultCri.FaultCriteriaID).FirstOrDefault()
                                            ?? lstInserted.Where(u => u.PupilID == pupilAbsence.PupilID
                                                                    && u.ViolatedDate.Date == pupilAbsence.AbsentDate.Date
                                                                    && u.FaultID == faultCri.FaultCriteriaID).FirstOrDefault();

                    if (pupilFault != null)
                    {
                        pupilFault.NumberOfFault += 1;
                        pupilFault.TotalPenalizedMark = pupilFault.NumberOfFault * faultCri.PenalizedMark;
                        PupilFaultBusiness.UpdatePupilFault(UserID, IsAdmin, pupilFault);
                    }
                    else
                    {
                        PupilFault pf = new PupilFault();
                        pf.PupilID = pupilAbsence.PupilID;
                        pf.ClassID = pupilAbsence.ClassID;
                        pf.SchoolID = pupilAbsence.SchoolID;
                        pf.AcademicYearID = pupilAbsence.AcademicYearID;
                        pf.EducationLevelID = pupilAbsence.EducationLevelID;
                        pf.ViolatedDate = pupilAbsence.AbsentDate;
                        pf.FaultID = faultCri.FaultCriteriaID;
                        pf.NumberOfFault = 1;
                        pf.TotalPenalizedMark = faultCri.PenalizedMark;
                        pf = PupilFaultBusiness.Insert(pf);
                        lstInserted.Add(pf);
                    }
                }

                //Xoa cac PupilFault gio khong con loi nua
                PupilFaultBusiness.DeleteAll(lstPupilFault.Where(u => u.NumberOfFault == 0).ToList());
            }
        }
        #endregion

        #region SearchPupilAbsence
        /// <summary>
        /// Tìm kiếm thông tin học sinh vắng mặt khi điểm danh
        /// <author>minhh</author>
        /// </summary>SaveAMonth
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<PupilAbsence> Search(IDictionary<string, object> dic = null)
        {

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? ClassID = Utils.GetNullableInt(dic, "ClassID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Section = (int)Utils.GetByte(dic, "Section");
            DateTime? AbsentDate = Utils.GetDateTime(dic, "AbsentDate");
            DateTime? FromAbsentDate = Utils.GetDateTime(dic, "FromAbsentDate");
            DateTime? ToAbsentDate = Utils.GetDateTime(dic, "ToAbsentDate");
            int SectionKey = Utils.GetInt(dic, "SectionKey");
            int? appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");

            IQueryable<PupilAbsence> Query = PupilAbsenceBusiness.All;
            if (ClassID.HasValue && ClassID.Value != 0)
            {
                Query = from pa in Query
                        join cp in ClassProfileBusiness.All on pa.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && pa.ClassID == ClassID
                        select pa;
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }

            if (lstPupilID.Count > 0)
            {
                Query = Query.Where(o => lstPupilID.Contains(o.PupilID));
            }
            if (lstClassID.Count > 0)
            {
                Query = Query.Where(o => lstClassID.Contains(o.ClassID));
            }
            if (SchoolID.HasValue)
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolID.Value);
                Query = Query.Where(o => o.SchoolID == SchoolID && o.Last2digitNumberSchool == partitionId);
            }
            if (EducationLevelID > 0)
            {
                Query = Query.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
            }

            if (appliedLevel > 0)
            {
                if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                {
                    Query = Query.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID6);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                {
                    Query = Query.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID5 && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID10);
                }
                else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    Query = Query.Where(o => o.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID9 && o.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID13);
                }
                else
                {
                    Query = Query.Where(o => o.EducationLevelID >= GlobalConstants.EDUCATION_LEVEL_ID13);
                }
            }

            if (Section != 0)
            {
                Query = Query.Where(o => o.Section == Section);
            }

            if (SectionKey > 0)
            {
                Query = Query.Where(p => p.Section == SectionKey);
            }

            if (AbsentDate != null)
            {
                Query = Query.Where(o => o.AbsentDate.Day == AbsentDate.Value.Day &&
                    o.AbsentDate.Month == AbsentDate.Value.Month && o.AbsentDate.Year == AbsentDate.Value.Year);
            }
            if (FromAbsentDate != null && ToAbsentDate != null)
            {
                Query = Query.Where(o => o.AbsentDate >= FromAbsentDate && o.AbsentDate <= ToAbsentDate);
            }
            return Query;

        }
        #endregion

        #region SearchBySchool
        /// <summary>
        /// Tìm kiếm học sihn vắng mặt điểm danh theo trường
        /// <author>minhh</author>
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<PupilAbsence> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0) dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
        #endregion

        #region	CountTotalAbsenceWithPermission
        /// <summary>
        /// Tính tổng số buổi điểm danh có phép
        /// <author>minhh</author>
        /// </summary>
        /// <param name="PupilID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        public int CountTotalAbsenceWithPermission(int PupilID, int AcademicYearID, int Semester)
        {
            IQueryable<PupilAbsence> lstPupilAbsence = PupilAbsenceRepository.All;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear academicYear = AcademicYearRepositoty.Find(AcademicYearID);
            if (Semester == 1)
            {
                SearchInfo["FromDate"] = academicYear.FirstSemesterStartDate;
                SearchInfo["EndDate"] = academicYear.FirstSemesterEndDate;
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["IsAccepted"] = true;
                lstPupilAbsence = SearchBySchool(academicYear.SchoolID, SearchInfo);
            }
            if (Semester == 2)
            {
                SearchInfo["FromDate"] = academicYear.SecondSemesterStartDate;
                SearchInfo["EndDate"] = academicYear.SecondSemesterEndDate;
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["IsAccepted"] = true;
                lstPupilAbsence = SearchBySchool(academicYear.SchoolID, SearchInfo);
            }
            if (Semester == 3 || Semester == 4)
            {
                SearchInfo["FromDate"] = academicYear.FirstSemesterStartDate;
                SearchInfo["EndDate"] = academicYear.SecondSemesterEndDate;
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["IsAccepted"] = true;
                lstPupilAbsence = SearchBySchool(academicYear.SchoolID, SearchInfo);
            }
            return lstPupilAbsence.ToList().Count;
        }
        #endregion

        #region CountTotalAbsenceWithoutPermission
        /// <summary>
        /// Tính tổng số buổi điểm danh không phép
        /// <author>minhh</author>
        /// </summary>
        /// <param name="PupilID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        public int CountTotalAbsenceWithoutPermission(int PupilID, int AcademicYearID, int Semester)
        {

            IQueryable<PupilAbsence> lstPupilAbsence = PupilAbsenceRepository.All;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear academicYear = AcademicYearRepositoty.Find(AcademicYearID);
            if (Semester == 1)
            {
                SearchInfo["FromDate"] = academicYear.FirstSemesterStartDate;
                SearchInfo["EndDate"] = academicYear.FirstSemesterEndDate;
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["IsAccepted"] = false;
                lstPupilAbsence = SearchBySchool(academicYear.SchoolID, SearchInfo);
            }
            if (Semester == 2)
            {
                SearchInfo["FromDate"] = academicYear.SecondSemesterStartDate;
                SearchInfo["EndDate"] = academicYear.SecondSemesterEndDate;
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["IsAccepted"] = false;
                lstPupilAbsence = SearchBySchool(academicYear.SchoolID, SearchInfo);
            }
            if (Semester == 3 || Semester == 4)
            {
                SearchInfo["FromDate"] = academicYear.FirstSemesterStartDate;
                SearchInfo["EndDate"] = academicYear.SecondSemesterEndDate;
                SearchInfo["PupilID"] = PupilID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                SearchInfo["IsAccepted"] = false;
                lstPupilAbsence = SearchBySchool(academicYear.SchoolID, SearchInfo);
            }
            return lstPupilAbsence.ToList().Count;
        }

        #endregion

        public List<PupilAbsenceBO> GetListPupilAbsenceBySection(IDictionary<string, object> dic, AcademicYear objAcademicYear = null)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            DateTime? FromAbsentDate = Utils.GetDateTime(dic, "FromAbsentDate");
            DateTime? ToAbsentDate = Utils.GetDateTime(dic, "ToAbsentDate");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            List<int> lstsectionKeyID = Utils.GetIntList(dic, "lstSectionKeyID");
            DateTime? FStartDate = new DateTime();
            DateTime? FEndDate = new DateTime();
            DateTime? SStartDate = new DateTime();
            DateTime? SEndDate = new DateTime();
            if (semesterID > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && objAcademicYear != null)
            {
                FStartDate = objAcademicYear.FirstSemesterStartDate;
                FEndDate = objAcademicYear.FirstSemesterEndDate;
                SStartDate = objAcademicYear.SecondSemesterStartDate;
                SEndDate = objAcademicYear.SecondSemesterEndDate;
            }
            //Chiendd1: 02/07/2015, Bo sung dieu kien partition
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            IQueryable<PupilAbsenceBO> listAbsencetmp = (from pa in PupilAbsenceBusiness.All
                                                         where pa.SchoolID == schoolID && pa.Last2digitNumberSchool == partitionId
                                                         && pa.AcademicYearID == academicYearID
                                                         && (pa.ClassID == classID || classID == 0)
                                                         && (pa.EducationLevelID == educationLevelID || educationLevelID == 0)
                                                         && (((semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                                                              && pa.AbsentDate >= FromAbsentDate.Value
                                                              && pa.AbsentDate <= ToAbsentDate.Value)
                                                              || (semesterID > SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && ((FStartDate <= pa.AbsentDate && pa.AbsentDate <= FEndDate)
                                                                                     || (SStartDate <= pa.AbsentDate && pa.AbsentDate <= SEndDate))))
                                                         //&& (lstsectionKeyID.Count > 0 && lstsectionKeyID.Contains(pa.Section))
                                                         select new PupilAbsenceBO
                                                         {
                                                             ClassID = pa.ClassID,
                                                             PupilID = pa.PupilID,
                                                             Section = pa.Section,
                                                             IsAccepted = pa.IsAccepted,
                                                             AbsentDate = pa.AbsentDate
                                                         });//.OrderBy(p => p.PupilID).ThenBy(p => p.Section).ToList();
            if (lstsectionKeyID.Count > 0)
            {
                listAbsencetmp = listAbsencetmp.Where(p => lstsectionKeyID.Contains(p.Section));
            }
            List<PupilAbsenceBO> lstPupilAbsence = listAbsencetmp.OrderBy(p => p.PupilID).ThenBy(p => p.Section).ToList();
            //List<PupilAbsenceBO> lsttmp = new List<PupilAbsenceBO>();
            //List<PupilAbsenceBO> lstResult = new List<PupilAbsenceBO>();
            //PupilAbsenceBO objPupilAbsence = new PupilAbsenceBO();

            //for (int i = 0; i < lstPupilAbsence.Count; i++)
            //{
            //    objPupilAbsence = lstPupilAbsence[i];
            //    if (lstResult.Any(p => p.PupilID == objPupilAbsence.PupilID && p.AbsentDate == objPupilAbsence.AbsentDate))
            //    {
            //        continue;
            //    }
            //    lstResult.Add(objPupilAbsence);
            //}
            return lstPupilAbsence;
        }

        public Stream ExportReport(IDictionary<string, object> dicClassProfile, int SchoolID, int AcademicYearID, int Section, int ClassID, DateTime date, bool IsAdmin, out string FileName, int typeReport, int EmployeeID, int AppliedLevelID = 0)
        {
            string reportCode = SystemParamsInFile.HS_BANGDIEMDANH;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            //Tính thời gian của học kỳ
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            DateTime fHKI = objAcademicYear.FirstSemesterStartDate.Value;
            DateTime eHKI = objAcademicYear.FirstSemesterEndDate.Value;
            DateTime fHKII = objAcademicYear.SecondSemesterStartDate.Value;
            DateTime eHKII = objAcademicYear.SecondSemesterEndDate.Value;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet1 = oBook.GetSheet(1);
            IVTWorksheet emptySheet1 = oBook.GetSheet(2);
            IVTWorksheet sheet1 = oBook.CopySheetToLast(firstSheet1);
            ClassProfile objClass = ClassProfileBusiness.Find(ClassID);
            string outputNamePattern = reportDef.OutputNamePattern;
            string typeName = "";
            if (typeReport == 1)
                typeName = objClass.DisplayName;
            else if (typeReport == 2)
                typeName = objClass.EducationLevel.Resolution;
            else if (typeReport == 3)
                typeName = "ToanTruong";

            outputNamePattern = outputNamePattern.Replace("Lop_","_").Replace("[Class]", ReportUtils.StripVNSign(typeName));
            if (typeReport == 4)
                outputNamePattern = outputNamePattern.Replace("_", "");
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            // Lấy danh sách học sinh thuộc lớp và số ngày nghỉ tương ứng
            #region
            //ngày bắt đầu và kết thúc tháng
            DateTime dteStart = SMAS.Business.Common.Utils.StartOfMonth(date);
            DateTime dteEnd = SMAS.Business.Common.Utils.EndOfMonth(dteStart);
            //dem so hoc sinh
            List<int> lstSectionType = new List<int>();
            GetTypeSection(lstSectionType, Section);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["IsActive"] = 1;
            dic["AppliedLevel"] = AppliedLevelID;

            List<PupilOfClassBO> iqueryPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).Where(x => lstSectionType.Contains(x.ClassProfile.Section.Value))
                .Select(o => new PupilOfClassBO
                {
                    ClassID = o.ClassID,
                    PupilID = o.PupilID,
                    PupilCode = o.PupilProfile.PupilCode,
                    Name = o.PupilProfile.Name,
                    PupilFullName = o.PupilProfile.FullName,
                    OrderInClass = o.OrderInClass,
                    ClassOrder = o.ClassProfile.OrderNumber,
                    Status = o.Status,
                    EducationLevelID = o.ClassProfile.EducationLevelID,
                    ClassName = o.ClassProfile.DisplayName
                }).ToList();

            int educationLevelID = ClassProfileBusiness.Find(ClassID).EducationLevelID;
            AcademicYear academic = AcademicYearBusiness.Find(AcademicYearID);
            if (academic != null && academic.IsShowPupil == true) 
            {
                iqueryPOC = iqueryPOC.Where(x => x.Status == 1 || x.Status == 2).ToList();
            }
                        
            // danh sách ID Class
            List<int> lstClassID = new List<int>();
            if (typeReport == 1)
            {
                lstClassID = iqueryPOC.Where(x => x.ClassID == ClassID).ToList()
                                            .OrderBy(o => o.EducationLevelID)
                                            .ThenBy(o => o.ClassOrder)
                                            .ThenBy(o=>o.ClassName)
                                            .Select(x => x.ClassID).Distinct().ToList();
            }
            else if (typeReport == 2)
            {               
                lstClassID = iqueryPOC.Where(x => x.EducationLevelID == educationLevelID).ToList()
                                                    .OrderBy(o => o.EducationLevelID)
                                                    .ThenBy(o => o.ClassOrder)
                                                    .ThenBy(o => o.ClassName)
                                                    .Select(x => x.ClassID).Distinct().ToList();
            }
            else if (typeReport == 3)
            {
                lstClassID = iqueryPOC.ToList()
                            .OrderBy(o => o.EducationLevelID)
                            .ThenBy(o => o.ClassOrder)
                            .ThenBy(o => o.ClassName)
                            .Select(x => x.ClassID).Distinct().ToList();
            }
            else if (typeReport == 4)
            {
                List<int> lstCSA = new List<int>();
                if (AppliedLevelID <= 3)
                {
                    IDictionary<string, object> dicCSA = new Dictionary<string, object>();
                    dicCSA["TeacherID"] = EmployeeID;
                    dicCSA["AcademicYearID"] = AcademicYearID;
                    lstCSA = ClassSupervisorAssignmentBusiness.SearchBySchool(SchoolID, dicCSA)
                        .Where(x => (x.PermissionLevel == 1 || x.PermissionLevel == 3) && x.TeacherID == EmployeeID).Select(x => x.ClassProfile.ClassProfileID).Distinct().ToList();

                    IDictionary<string, object> dicCP = new Dictionary<string, object>();
                    dicCP["AppliedLevel"] = AppliedLevelID;
                    dicCP["AcademicYearID"] = AcademicYearID;
                    dicCP["HeadTeacherID"] = EmployeeID;
                    List<int> lstCP = ClassProfileBusiness.SearchBySchool(SchoolID, dicCP).Where(x => x.IsActive == true && x.HeadTeacherID == EmployeeID).Select(x => x.ClassProfileID).Distinct().ToList();
                    lstCSA.AddRange(lstCP);
                }
                else
                {
                    lstCSA = ClassProfileBusiness.SearchTeacherTeachingBySchool(SchoolID, dicClassProfile).Select(x => x.ClassProfileID).ToList();
                }

                if (lstCSA.Count() > 0)
                {
                    iqueryPOC = iqueryPOC.Where(x => lstCSA.Contains(x.ClassID)).ToList();
                    lstClassID = iqueryPOC.ToList()
                            .OrderBy(o => o.EducationLevelID)
                            .ThenBy(o => o.ClassOrder)
                            .ThenBy(o => o.ClassName)
                            .Select(x => x.ClassID).Distinct().ToList();
                }  
            }

            dic = new Dictionary<string, object>();
            //dic["ClassID"] = ClassID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["Section"] = Section;
            dic["FromAbsentDate"] = dteStart;
            dic["ToAbsentDate"] = dteEnd;
            dic["lstClassID"] = lstClassID;

            if (typeReport == 1)
                dic["ClassID"] = ClassID;

            if (typeReport == 2)
                dic["EducationLevelID"] = educationLevelID;
            List<PupilAbsence> listPupilAbsenceMain = PupilAbsenceBusiness.SearchBySchool(SchoolID, dic).ToList();

            List<PupilOfClassBO> listPupilOfClassAll = iqueryPOC.Where(p => lstClassID.Contains(p.ClassID)).ToList()
                                                        .OrderBy(o => o.OrderInClass)
                                                        .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                                                        .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName)).ToList();

            // Lay thong tin thoi gian bi khoa diem danh
            List<LockTraining> lstlockTraining = new List<LockTraining>();
            LockTraining lockTraining = null;
            if (!IsAdmin)
            {
                IDictionary<string, object> dicLockTraining = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"IsAbsence", true}
                };
                lstlockTraining = LockTrainingBusiness.SearchBySchool(SchoolID, dicLockTraining).Where(p => lstClassID.Contains(p.ClassID)).ToList();
            }
            #endregion

            // Các danh sách và biến sử dụng
            List<VTDataValidation> lstValidate = new List<VTDataValidation>();
            List<PupilAbsence> listPupilAbsence = new List<PupilAbsence>();
            SchoolProfile objSchoolProfile = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            DateTime startYear = academicYear.FirstSemesterStartDate.Value;
            DateTime endYear = academicYear.SecondSemesterEndDate.Value;
            string supervisingName = UtilsBusiness.GetSupervisingDeptName(SchoolID, AppliedLevelID);
            int startRow, colDte, rowDte, colDteEnd, indexPupil, indexRow;
            int index = 0;
            string title;
            string orderDay = "";
            string className = "";

            //fill data
            for (int i = 0; i < lstClassID.Count(); i++)
            {
                index++;
                IVTWorksheet firstSheet = oBook.CopySheetToLast(firstSheet1);
                IVTWorksheet emptySheet = oBook.CopySheetToLast(emptySheet1);
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                IVTRange yellowRow = firstSheet.GetRow(11);
                IVTRange niceRow = sheet.GetRow(9);

                var listPupilOfClass = listPupilOfClassAll.Where(x => x.ClassID == lstClassID[i]);
                listPupilAbsence = listPupilAbsenceMain.Where(x => x.ClassID == lstClassID[i]).ToList();

                #region for
                //Change header
                className = listPupilOfClass.First().ClassName.ToUpper().Replace("LỚP", "").Trim();
                title = "BẢNG ĐIỂM DANH THÁNG ";
                title += date.Month.ToString();
                title += "- NĂM " + date.Year.ToString();
                title += " - ";
                title += " LỚP ";
                title += className;
                title += " - ";
                if (Section == 1)
                    title += " BUỔI SÁNG ";
                else if (Section == 3)
                    title += " BUỔI TỐI ";
                else
                    title += " BUỔI CHIỀU ";

                sheet.SetCellValue("A3", objSchoolProfile.SchoolName.ToUpper());
                sheet.SetCellValue("A2", supervisingName.ToUpper());
                sheet.SetCellValue("A6", title);

                sheet.DeleteRow(10);
                sheet.DeleteRow(10);

                //ngay thu
                colDte = 5;
                rowDte = 8;
                colDteEnd = 35;
                orderDay = "";
                startRow = 9;

                IVTRange RangeendWeek = firstSheet.GetRange("I10", "J10");
                IVTRange RangeendWeekSat = firstSheet.GetRange("I10", "I10");
                IVTRange RangeendWeekSun = firstSheet.GetRange("J10", "J10");
                for (DateTime dte = dteStart; dte <= dteEnd; dte = dte.AddDays(1))
                {
                    DateTime thu = Convert.ToDateTime(dte);
                    string orderDayA = thu.DayOfWeek.ToString().Trim();
                    switch (orderDayA)
                    {
                        case "Sunday": orderDay = "CN"; break;
                        case "Monday": orderDay = "T2"; break;
                        case "Tuesday": orderDay = "T3"; break;
                        case "Wednesday": orderDay = "T4"; break;
                        case "Thursday": orderDay = "T5"; break;
                        case "Friday": orderDay = "T6"; break;
                        case "Saturday": orderDay = "T7"; break;
                    }
                    sheet.SetCellValue(rowDte, colDte, orderDay);
                    if (orderDayA == "Saturday")
                    {
                        sheet.CopyPasteSameSize(RangeendWeek, startRow, colDte);
                        if (dte.Day == 31)
                        {
                            sheet.CopyPasteSameSize(RangeendWeekSat, startRow, colDte);
                        }
                    }
                    if (orderDayA == "Sunday" && dte.Day == 1)
                    {
                        sheet.CopyPasteSameSize(RangeendWeekSun, startRow, colDte);
                    }
                    colDte++;
                }
                //an nhung ngay khong co trong thangs
                for (int col = colDte; col <= colDteEnd; col++)
                {
                    sheet.HideColumn(col);
                }

                int cntPupil = listPupilOfClass.Count();
                indexPupil = 0;
                indexRow = 10;
               

                bool isLock = (lockTraining != null);
                foreach (var PupilOfClass in listPupilOfClass)
                {
                    startRow++;
                    if (PupilOfClass.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.CopyAndInsertARow(niceRow, startRow);
                        sheet.GetRange(startRow, 5, startRow, 38).IsLock = false;
                    }
                    else
                    {
                        sheet.CopyAndInsertARow(yellowRow, startRow);
                        sheet.GetRange(startRow, 5, startRow, 38).IsLock = true;
                    }

                    indexPupil++;

                    //cot stt
                    sheet.SetCellValue(startRow, 1, indexPupil.ToString());
                    sheet.SetCellValue(startRow, 2, PupilOfClass.PupilFullName);
                    sheet.SetCellValue(startRow, 3, PupilOfClass.PupilCode);
                    sheet.SetCellValue(startRow, 4, PupilOfClass.PupilID);

                    // cac cot cua cac ngay trong thang
                    int colstartDte = 5;
                    for (DateTime dte = dteStart; dte <= dteEnd; dte = dte.AddDays(1))
                    {
                        //lay thong tin diem danh theo ngay va ma hoc sinh
                        if (dte < startYear || dte > endYear || dte > DateTime.Now)
                        {
                            sheet.GetRange(startRow, colstartDte, startRow, colstartDte).IsLock = true;
                        }
                        PupilAbsence PupilAbsenceItem = listPupilAbsence.Where(o => o.PupilID == PupilOfClass.PupilID && o.AbsentDate == dte).FirstOrDefault();

                        if (PupilAbsenceItem != null && PupilAbsenceItem.IsAccepted != null)
                        {
                            if (PupilAbsenceItem.IsAccepted.Value)
                                sheet.SetCellValue(startRow, colstartDte, "P");
                            else
                                sheet.SetCellValue(startRow, colstartDte, "K");
                        }
                        // Neu bi khoa diem danh thi lock cell chi ap dung voi tk giao vien
                        if (!IsAdmin && isLock && (!lockTraining.FromDate.HasValue || lockTraining.FromDate.Value <= dte)
                            && lockTraining.ToDate >= dte || dte < fHKI || (dte > eHKI && dte < fHKII) || dte > eHKII)
                        {
                            sheet.GetRange(startRow, colstartDte, startRow, colstartDte).IsLock = true;
                        }
                        //string fomular count K
                        string f_countK = "=COUNTIF(D" + indexRow + ":AI" + indexRow + ",'K')";
                        string f_countP = "=COUNTIF(D" + indexRow + ":AI" + indexRow + ",'P')";

                        sheet.SetFormulaValue(startRow, 36, f_countK.Replace(",", ";").Replace("\'", "\""));
                        sheet.SetFormulaValue(startRow, 37, f_countP.Replace(",", ";").Replace("\'", "\""));
                        colstartDte++;
                    }
                    indexRow++;
                }

                sheet.SetCellValue("AN7", lstClassID[i]);
                sheet.HideColumn(40);
                sheet.SetCellValue("E4", "K");
                sheet.SetCellValue("E5", "P");
                sheet.SetColumnWidth(4, 0);
                sheet.ProtectSheet("123456a@");
                sheet.DeleteRow(9);
                sheet.Name =Utils.StripVNSignAndSpace(className);
                #endregion
                emptySheet.Delete();
                firstSheet.Delete();
                lstValidate.Add(new VTDataValidation
                {
                    Contrains = new string[] { "P", "K" },
                    FromColumn = 5,
                    FromRow = 9,
                    SheetIndex = index,
                    ToColumn = 35,
                    ToRow = startRow - 1,
                    Type = VTValidationType.LIST
                });
            }
            emptySheet1.Delete();
            firstSheet1.Delete();
            sheet1.Delete();

            return oBook.ToStreamValidationData(lstValidate);
        }

        public void SaveAWeek(int UserID, bool IsAdmin, int SchoolID, int ClassID,
            int Section, DateTime Month, List<PupilAbsence> ListData, DateTime WeekStartDate, DateTime WeekEndDate, bool CheckAllDay, int AppliedLevel)
        {

            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            //Kiểm tra quyền giáo viên: chỉ có giáo viên có quyền chủ nhiệm hoặc giáo vụ mới được phép điểm danh
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID)
                && !UtilsBusiness.HasOverseeingTeacherPermission(UserID, ClassID))
            {
                throw new BusinessException("PupilAbsent_Error_NoPermission", classProfile.DisplayName);
            }

            //Kiểm tra lớp học có tương ứng trường học

            if (classProfile.SchoolID != SchoolID)
            {
                throw new Exception("PupilAbsent_Error_NotCompatible_SchoolID_ClassID");
            }

            //Kiểm tra lớp học có học buổi học không
            //if (classProfile.Section / (Int32.Parse(Math.Pow(2, Section - 1).ToString())) % 2 != 1)
            //{
            //    throw new BusinessException("PupilAbsent_Error_SectionNotInClass",
            //        classProfile.DisplayName, SMASConvert.ConvertSection(Section));
            //}


            //Năm học không phải năm hiện tại
            AcademicYear academicYear = classProfile.AcademicYear;
            if (!AcademicYearBusiness.IsCurrentYear(academicYear.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            //Kiểm tra học sinh có trong lớp không
            List<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"ClassID", ClassID}
            }).ToList();
            if (ListData.Exists(x => !listPupil.Exists(y => y.PupilID == x.PupilID)))
            {
                throw new BusinessException("PupilAbsent_Error_PupilNotInClass", classProfile.DisplayName);
            }

            PupilOfClass poc = listPupil.Find(x => ListData.Exists(y => y.PupilID == x.PupilID)
                && x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);

            //Kiểm tra thời gian có vượt quá ngày hiện tại không
            if (ListData.Exists(x => x.AbsentDate.Date > DateTime.Now.Date))
            {
                throw new BusinessException("PupilAbsent_Error_OverCurrentDate");
            }

            //Kiểm tra thời gian có nằm trong tháng cần điểm danh không
            //if (ListData.Exists(x => x.AbsentDate.StartOfMonth() != Month.StartOfMonth()))
            //{
            //    throw new BusinessException("PupilAbsent_Error_NotInSelectedMonth");
            //}

            //Kiểm tra thời gian có nằm trong năm học không
            if (ListData.Exists(x => x.AbsentDate.Date > academicYear.SecondSemesterEndDate.GetValueOrDefault().Date
                || x.AbsentDate.Date < academicYear.FirstSemesterStartDate.GetValueOrDefault().Date))
            {
                throw new BusinessException("PupilAbsent_Error_NotInSelectedAcademicYear");
            }

            //Kiểm tra có trùng bản ghi không
            if (ListData.Exists(x => ListData.FindAll(
                y => y.PupilID == x.PupilID && y.AbsentDate == x.AbsentDate).Count > 1))
            {
                throw new BusinessException("PupilAbsent_Error_Duplicate");
            }

            //Lấy thông tin điểm danh trong tháng
            IQueryable<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"ClassID", ClassID},
                {"FromAbsentDate", WeekStartDate},
                {"ToAbsentDate", WeekEndDate}
            });
            IQueryable<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"ClassID", ClassID},
                {"Status", SystemParamsInFile.PUPIL_STATUS_STUDYING}
            });
            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;

            List<int> lstSection = new List<int>();
            if (classProfile != null)
            {
                lstSection = GetSection(lstSection, classProfile.Section.Value);
            }

            //Lấy danh sách cần xoá
            IQueryable<PupilAbsence> listToDelete = null;
            if (CheckAllDay && (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE))
            {
                listToDelete = listPupilAbsence.Where(x => lstSection.Contains(x.Section));
            }
            else
            {
                listToDelete = listPupilAbsence.Where(x => x.Section == Section);
            }
            //Lấy danh sách cần insert chỉ chứa những học sinh đang học
            List<PupilAbsence> lstData = (from p in ListData
                                          join q in lstPOC on p.PupilID equals q.PupilID
                                          where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                          select p).ToList();
            //Lấy danh sách cần xoá điểm danh cũ.
            List<PupilAbsence> lstToDelete = (from p in listToDelete
                                              join q in lstPOC on p.PupilID equals q.PupilID
                                              where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                              select p).ToList();
            //Lấy danh sách còn lại
            List<PupilAbsence> lstPupilAbsen = (from p in listPupilAbsence
                                                join q in lstPOC on p.PupilID equals q.PupilID
                                                where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                select p).ToList();
            List<PupilAbsence> listRemain = new List<PupilAbsence>();
            if (CheckAllDay && (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE))
            {
                listRemain = lstPupilAbsen.Where(x => !lstSection.Contains(x.Section)).ToList();
            }
            else
            {
                listRemain = lstPupilAbsen.Where(x => x.Section != Section).ToList();
            }

            // Lay thong tin thoi gian bi khoa diem danh neu tk khong phai la admin
            if (!IsAdmin)
            {
                LockTraining lockTraining = LockTrainingBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                {
                    {"IsAbsence", true}
                }).FirstOrDefault();
                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                    // Neu co thong tin khoa diem danh thi bo cac phan tu trong CSDL thuoc khoang khoa khong xoa
                    // Va bo cac phan tu trong list du lieu dau vao
                    lstToDelete.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                    lstData.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                }
            }

            //Tạo dữ liệu cần insert
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            foreach (PupilAbsence pa in lstData)
            {
                pa.AcademicYearID = academicYear.AcademicYearID;
                pa.ClassID = ClassID;
                pa.EducationLevelID = classProfile.EducationLevelID;
                pa.SchoolID = SchoolID;
                pa.Section = Section;
                pa.Last2digitNumberSchool = partitionId;
            }

            //Lấy list sau cập nhật
            listRemain.AddRange(lstData);

            #region kiem tra thong tin vi pham de cap nhat CSDL
            List<PupilAbsenceBO> lstPACheckAll = new List<PupilAbsenceBO>();
            PupilAbsenceBO objPA = new PupilAbsenceBO();
            if (CheckAllDay && (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE))
            {

                foreach (var itemSection in lstSection)
                {
                    foreach (var item in lstData)
                    {
                        objPA = new PupilAbsenceBO();
                        objPA.AcademicYearID = item.AcademicYearID;
                        objPA.SchoolID = item.SchoolID;
                        objPA.EducationLevelID = item.EducationLevelID;
                        objPA.ClassID = item.ClassID;
                        objPA.AbsentDate = item.AbsentDate;
                        objPA.PupilID = item.PupilID;
                        objPA.IsAccepted = item.IsAccepted;
                        objPA.Last2digitNumberSchool = item.Last2digitNumberSchool;
                        objPA.Section = itemSection;
                        lstPACheckAll.Add(objPA);
                    }
                }
                UpdatePupilAbsence(lstPACheckAll, lstToDelete, lstSection);
            }
            else
            {
                foreach (var item in lstData)
                {
                    objPA = new PupilAbsenceBO();
                    objPA.AcademicYearID = item.AcademicYearID;
                    objPA.SchoolID = item.SchoolID;
                    objPA.EducationLevelID = item.EducationLevelID;
                    objPA.ClassID = item.ClassID;
                    objPA.AbsentDate = item.AbsentDate;
                    objPA.PupilID = item.PupilID;
                    objPA.IsAccepted = item.IsAccepted;
                    objPA.Last2digitNumberSchool = item.Last2digitNumberSchool;
                    objPA.Section = Section;
                    lstPACheckAll.Add(objPA);
                }
                UpdatePupilAbsence(lstPACheckAll, lstToDelete, lstSection);
            }

            if (lstToDelete.Count > 0)
            {
                //Xoá dữ liệu cũ
                PupilAbsenceRepository.DeleteAll(lstToDelete);
            }
            #endregion


            //Lấy FaultCriteria cho nghỉ có phép và không phép
            var lstFault = FaultCriteriaBusiness.SearchBySchool(SchoolID).ToList();
            FaultCriteria P_Fault = lstFault.Find(x => x.Absence != null && x.Absence.Trim() == SystemParamsInFile.PUPIL_ABSENCE_P);
            FaultCriteria K_Fault = lstFault.Find(x => x.Absence != null && x.Absence.Trim() == SystemParamsInFile.PUPIL_ABSENCE_K);

            List<int> listFaultID = new List<int>();
            if (P_Fault != null) listFaultID.Add(P_Fault.FaultCriteriaID);
            if (K_Fault != null) listFaultID.Add(K_Fault.FaultCriteriaID);

            List<PupilFault> lstPupilFaultDelete = new List<PupilFault>();
            if (listFaultID.Count > 0)
            {
                //Lấy danh sách thông tin vi phạm cần xoá
                IQueryable<PupilFault> listPupilFault = PupilFaultBusiness.SearchBySchool(SchoolID, new PupilFaultSearchForm()
                {
                    ClassID = ClassID,
                    F_ViolatedDate = WeekStartDate,
                    T_ViolatedDate = WeekEndDate,
                    FaultID = listFaultID
                });
                lstPupilFaultDelete = (from p in listPupilFault
                                       join q in lstPOC on p.PupilID equals q.PupilID
                                       where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                       select p).ToList();

                // Neu co khoa vi pham thi khong xoa trong thoi gian do
                // Lay thong tin thoi gian bi khoa diem danh neu tk khong phai la admin
                if (!IsAdmin)
                {
                    LockTraining lockTraining = LockTrainingBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                    {
                        {"IsViolation", true}
                    }).FirstOrDefault();
                    if (lockTraining != null)
                    {
                        DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                        // Neu co thong tin khoa diem danh thi bo cac phan tu trong CSDL thuoc khoang khoa khong xoa
                        // Va bo cac phan tu trong list du lieu dau vao
                        lstPupilFaultDelete.RemoveAll(o => o.ViolatedDate >= fromLockDate && o.ViolatedDate < lockTraining.ToDate.AddDays(1));
                        // Bo cac phan tu insert trong khoang thoi gian bi khoa
                        listRemain.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                    }
                }
            }

            //Tổ hợp lại dữ lại cần insert vào PupilFault
            List<PupilFault> listFault = (from pa in listRemain
                                          group pa by new { pa.ClassID, pa.PupilID, pa.IsAccepted, pa.AbsentDate.Date } into g
                                          select new PupilFault
                                          {
                                              PupilID = g.Key.PupilID,
                                              AcademicYearID = academicYear.AcademicYearID,
                                              ClassID = ClassID,
                                              SchoolID = SchoolID,
                                              EducationLevelID = classProfile.EducationLevelID,
                                              FaultCriteria = g.Key.IsAccepted.GetValueOrDefault() ? P_Fault : K_Fault,
                                              NumberOfFault = g.Count(),
                                              ViolatedDate = g.Key.Date
                                          }).Where(x => x.FaultCriteria != null).ToList();
            listFault.ForEach(x => x.TotalPenalizedMark =
                x.NumberOfFault.GetValueOrDefault() * x.FaultCriteria.PenalizedMark.GetValueOrDefault());
            listFault.ForEach(x => x.FaultID = x.FaultCriteria.FaultCriteriaID);

            #region kiem tra thong tin vi pham de cap nhat CSDL
            int sizeInsert = listFault.Count;
            for (int i = sizeInsert - 1; i >= 0; i--)
            {
                PupilFault pf = listFault[i];
                PupilFault pfDB = lstPupilFaultDelete.Where(o => o.PupilID == pf.PupilID && o.ViolatedDate == pf.ViolatedDate && o.FaultID == pf.FaultID).FirstOrDefault();
                if (pfDB != null)
                {
                    // Cap nhat lai thong tin
                    pfDB.TotalPenalizedMark = pf.TotalPenalizedMark;
                    pfDB.NumberOfFault = pf.NumberOfFault;
                    PupilFaultBusiness.Update(pfDB);

                    listFault.RemoveAt(i);
                    lstPupilFaultDelete.Remove(pfDB);
                }
                else
                {
                    //Insert dữ liệu
                    PupilFaultBusiness.Insert(pf);
                }
            }

            if (lstPupilFaultDelete.Count > 0)
            {
                //Xoá dữ liệu cũ
                PupilFaultBusiness.DeleteAll(lstPupilFaultDelete);
            }
            #endregion

            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        #region Update IsSMS is TRUE
        /// <summary>
        /// Anhvd
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilIds"></param>
        /// <returns></returns>
        public bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds)
        {
            IQueryable<PupilAbsence> lstAbsence = this.Search(dic);
            for (int i = 0; i < pupilIds.Count; i++)
            {
                int pupilId = pupilIds[i];
                IQueryable<PupilAbsence> lstAbsPupil = lstAbsence.Where(o => o.PupilID == pupilId);
                foreach (var item in lstAbsPupil)
                {
                    item.IsSMS = true;
                    this.Update(item);
                }
            }
            this.Save();
            return true;
        }
        #endregion

        #region Thống kê học sinh nghỉ học

        public ProcessedReport GetReportPupilAbsence(PupilAbsenceBO entity)
        {
            string reportCode = SystemParamsInFile.HS_THCS_TKHSNghiHoc;
            string inputParameterHashKey = GetHashKeyAbsence(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        private string GetHashKeyAbsence(PupilAbsenceBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevelID",entity.AppliedLevelID},
                {"FromDate",entity.FromDate},
                {"ToDate",entity.ToDate},
            };
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertPupilAbsenceReport(PupilAbsenceBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.HS_THCS_TKHSNghiHoc;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyAbsence(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string AppliedLevelName = "";
            string EducationOrClass = "";
            if (entity.AppliedLevelID > 0)
            {
                if (entity.AppliedLevelID == 1)
                {
                    AppliedLevelName = "TH";
                }
                if (entity.AppliedLevelID == 2)
                {
                    AppliedLevelName = "THCS";
                }
                if (entity.AppliedLevelID == 3)
                {
                    AppliedLevelName = "THPT";
                }
            }

            if (!string.IsNullOrEmpty(entity.ClassName))
            {
                EducationOrClass = "Lop" + entity.ClassName;
            }
            else
            {
                EducationOrClass = entity.EducationName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            //outputNamePattern = outputNamePattern.Replace("[EducationOrClass]", EducationOrClass);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationOrClass",EducationOrClass}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        #endregion

        #region Báo cáo Học sinh nghỉ học
        //Lấy file báo cáo học sinh nghỉ học
        public ProcessedReport GetReportPupilAway(PupilAbsenceBO entity)
        {
            string reportCode = SystemParamsInFile.HS_BaoCaoHSNghiHoc;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        //Insert file báo cáo xuống DB
        public ProcessedReport InsertPupilAwayReport(PupilAbsenceBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.HS_BaoCaoHSNghiHoc;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string AppliedLevelName = "";
            string EducationOrClass = "";
            if (entity.AppliedLevelID > 0)
            {
                if (entity.AppliedLevelID == 1)
                {
                    AppliedLevelName = "TH";
                }
                if (entity.AppliedLevelID == 2)
                {
                    AppliedLevelName = "THCS";
                }
                if (entity.AppliedLevelID == 3)
                {
                    AppliedLevelName = "THPT";
                }
            }

            if (!string.IsNullOrEmpty(entity.ClassName))
            {
                EducationOrClass = "Lop" + entity.ClassName;
            }
            else
            {
                EducationOrClass = entity.EducationName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationOrClass]", EducationOrClass);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationOrClass",EducationOrClass}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        //Tạo mảng băm cho các tham số đầu vào cho báo cáo
        private string GetHashKey(PupilAbsenceBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevelID",entity.AppliedLevelID},
                {"ClassID",entity.ClassID},
                {"FromDate",entity.FromDate},
                {"ToDate",entity.ToDate},
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region // Báo báo trẻ nghỉ học
        //Lấy file báo cáo trẻ nghỉ học
        public ProcessedReport GetReportChildrenAway(PupilAbsenceBO entity)
        {
            string reportCode = SystemParamsInFile.Tre_BaoCaoTreNghiHoc;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        //Insert file báo cáo xuống DB
        public ProcessedReport InsertChildrenAwayReport(PupilAbsenceBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.Tre_BaoCaoTreNghiHoc;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string EducationOrClass = "";

            if (!string.IsNullOrEmpty(entity.ClassName))
            {
                EducationOrClass = "Lop" + entity.ClassName;
            }
            else
            {
                EducationOrClass = entity.EducationName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[ClassName]", EducationOrClass);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationOrClass",EducationOrClass}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }     
        #endregion

        #region Thống kê Trẻ nghỉ học theo buổi
        public ProcessedReport GetReportChildrenAbsence(PupilAbsenceBO entity)
        {
            string reportCode = SystemParamsInFile.Tre_TKTreNghiHoc;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertChildrenAbsenceReport(PupilAbsenceBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.Tre_TKTreNghiHoc;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string EducationOrClass = "";
            
            if (!string.IsNullOrEmpty(entity.ClassName))
            {
                EducationOrClass = "Lop" + entity.ClassName;
            }
            else
            {
                EducationOrClass = entity.EducationName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[ClassName]", EducationOrClass);
            //outputNamePattern = outputNamePattern.Replace("[EducationOrClass]", EducationOrClass);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationOrClass",EducationOrClass}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Thống kê Trẻ nghỉ học theo ngày
        public ProcessedReport GetReportChildrenAbsenceByDay(PupilAbsenceBO entity)
        {
            string reportCode = SystemParamsInFile.Tre_TKTreNghiHocTheoNgay;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertChildrenAbsenceReportByDay(PupilAbsenceBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.Tre_TKTreNghiHocTheoNgay;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string EducationOrClass = "";

            if (!string.IsNullOrEmpty(entity.ClassName))
            {
                EducationOrClass = "Lop" + entity.ClassName;
            }
            else
            {
                EducationOrClass = entity.EducationName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[ClassName]", EducationOrClass);
            //outputNamePattern = outputNamePattern.Replace("[EducationOrClass]", EducationOrClass);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationOrClass",EducationOrClass}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        //Hàm lấy buổi nghỉ đầu tiên trong ngày của học sinh
        public List<PupilAbsence> GetPupilAbsenceByDate(List<PupilAbsence> listAllPupilAbsence)
        {
            List<PupilAbsence> res = new List<PupilAbsence>();
            List<PupilAbsence> tempList = listAllPupilAbsence.OrderBy(o => o.Section).ToList();

            for (int i = 0; i < tempList.Count; i++)
            {
                PupilAbsence pa = tempList[i];
                if (res.Any(o => o.PupilID == pa.PupilID && o.AbsentDate == pa.AbsentDate))
                {
                    continue;
                }
                res.Add(pa);
            }

            return res;
        }

        public void SaveAMonth(int UserID, bool IsAdmin, int SchoolID, int ClassID,
            int Section, DateTime Month, List<PupilAbsence> ListData, bool CheckAllDay, int AppliedLevel, List<DateTime> lstOutDate = null)
        {

            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            //Kiểm tra quyền giáo viên: chỉ có giáo viên có quyền chủ nhiệm hoặc giáo vụ mới được phép điểm danh
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID)
                && !UtilsBusiness.HasOverseeingTeacherPermission(UserID, ClassID))
            {
                throw new BusinessException("PupilAbsent_Error_NoPermission", classProfile.DisplayName);
            }

            //Kiểm tra lớp học có tương ứng trường học

            if (classProfile.SchoolID != SchoolID)
            {
                throw new Exception("PupilAbsent_Error_NotCompatible_SchoolID_ClassID");
            }

            ////Kiểm tra lớp học có học buổi học không
            //if (classProfile.Section / (Int32.Parse(Math.Pow(2, Section - 1).ToString())) % 2 != 1)
            //{
            //    throw new BusinessException("PupilAbsent_Error_SectionNotInClass",
            //        classProfile.DisplayName, SMASConvert.ConvertSection(Section));
            //}


            //Năm học không phải năm hiện tại
            AcademicYear academicYear = classProfile.AcademicYear;
            if (!AcademicYearBusiness.IsCurrentYear(academicYear.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            //Kiểm tra học sinh có trong lớp không
            List<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"ClassID", ClassID}
            }).ToList();
            if (ListData.Exists(x => !listPupil.Exists(y => y.PupilID == x.PupilID)))
            {
                throw new BusinessException("PupilAbsent_Error_PupilNotInClass", classProfile.DisplayName);
            }

            PupilOfClass poc = listPupil.Find(x => ListData.Exists(y => y.PupilID == x.PupilID)
                && x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);

            //Kiểm tra thời gian có vượt quá ngày hiện tại không
            if (ListData.Exists(x => x.AbsentDate.Date > DateTime.Now.Date))
            {
                throw new BusinessException("PupilAbsent_Error_OverCurrentDate");
            }

            //Kiểm tra thời gian có nằm trong tháng cần điểm danh không
            //if (ListData.Exists(x => x.AbsentDate.StartOfMonth() != Month.StartOfMonth()))
            //{
            //    throw new BusinessException("PupilAbsent_Error_NotInSelectedMonth");
            //}

            //Kiểm tra thời gian có nằm trong năm học không
            if (ListData.Exists(x => x.AbsentDate.Date > academicYear.SecondSemesterEndDate.GetValueOrDefault().Date
                || x.AbsentDate.Date < academicYear.FirstSemesterStartDate.GetValueOrDefault().Date))
            {
                throw new BusinessException("PupilAbsent_Error_NotInSelectedAcademicYear");
            }

            //Kiểm tra có trùng bản ghi không
            if (ListData.Exists(x => ListData.FindAll(
                y => y.PupilID == x.PupilID && y.AbsentDate == x.AbsentDate).Count > 1))
            {
                throw new BusinessException("PupilAbsent_Error_Duplicate");
            }

            //Lấy thông tin điểm danh trong tháng
            IQueryable<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"ClassID", ClassID},
                {"FromAbsentDate", lstOutDate != null && lstOutDate.Count > 0 ? lstOutDate[0] : Month.StartOfMonth()},
                {"ToAbsentDate", lstOutDate != null && lstOutDate.Count > 0 ? lstOutDate[lstOutDate.Count - 1] : Month.EndOfMonth()}
            });
            IQueryable<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"ClassID", ClassID},
                {"Status", SystemParamsInFile.PUPIL_STATUS_STUDYING}
            });
            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            List<int> lstSection = new List<int>();
            if (classProfile != null)
            {
                lstSection = GetSection(lstSection, classProfile.Section.Value);
            }

            //Lấy danh sách cần xoá
            IQueryable<PupilAbsence> listToDelete = null;
            if (CheckAllDay && (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE))
            {
                listToDelete = listPupilAbsence.Where(x => lstSection.Contains(x.Section));
            }
            else
            {
                listToDelete = listPupilAbsence.Where(x => x.Section == Section);
            }
            //Lấy danh sách cần insert chỉ chứa những học sinh đang học
            List<PupilAbsence> lstData = (from p in ListData
                                          join q in lstPOC on p.PupilID equals q.PupilID
                                          where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                          select p).ToList();
            //Lấy danh sách cần xoá điểm danh cũ.
            List<PupilAbsence> lstToDelete = (from p in listToDelete
                                              join q in lstPOC on p.PupilID equals q.PupilID
                                              where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                              select p).ToList();
            //Lấy danh sách còn lại
            List<PupilAbsence> lstPupilAbsen = (from p in listPupilAbsence
                                                join q in lstPOC on p.PupilID equals q.PupilID
                                                where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                select p).ToList();
            List<PupilAbsence> listRemain = new List<PupilAbsence>();
            if (CheckAllDay && (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE))
            {
                listRemain = lstPupilAbsen.Where(x => !lstSection.Contains(x.Section)).ToList();
            }
            else
            {
                listRemain = lstPupilAbsen.Where(x => x.Section != Section).ToList();
            }

            // Lay thong tin thoi gian bi khoa diem danh neu tk khong phai la admin
            if (!IsAdmin)
            {
                LockTraining lockTraining = LockTrainingBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                {
                    {"IsAbsence", true}
                }).FirstOrDefault();
                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                    // Neu co thong tin khoa diem danh thi bo cac phan tu trong CSDL thuoc khoang khoa khong xoa
                    // Va bo cac phan tu trong list du lieu dau vao
                    lstToDelete.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                    lstData.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                }
            }

            //Tạo dữ liệu cần insert
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            foreach (PupilAbsence pa in lstData)
            {
                pa.AcademicYearID = academicYear.AcademicYearID;
                pa.ClassID = ClassID;
                pa.EducationLevelID = classProfile.EducationLevelID;
                pa.SchoolID = SchoolID;
                pa.Section = Section;
                pa.Last2digitNumberSchool = partitionId;
            }

            //Lấy list sau cập nhật
            listRemain.AddRange(lstData);

            #region kiem tra thong tin vi pham de cap nhat CSDL
            List<PupilAbsenceBO> lstPACheckAll = new List<PupilAbsenceBO>();
            PupilAbsenceBO objPA = new PupilAbsenceBO();
            if (CheckAllDay && (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE))
            {

                foreach (var itemSection in lstSection)
                {
                    foreach (var item in lstData)
                    {
                        objPA = new PupilAbsenceBO();
                        objPA.AcademicYearID = item.AcademicYearID;
                        objPA.SchoolID = item.SchoolID;
                        objPA.EducationLevelID = item.EducationLevelID;
                        objPA.ClassID = item.ClassID;
                        objPA.AbsentDate = item.AbsentDate;
                        objPA.PupilID = item.PupilID;
                        objPA.IsAccepted = item.IsAccepted;
                        objPA.Last2digitNumberSchool = item.Last2digitNumberSchool;
                        objPA.Section = itemSection;
                        lstPACheckAll.Add(objPA);
                    }
                }
                UpdatePupilAbsence(lstPACheckAll, lstToDelete, lstSection);
            }
            else
            {
                foreach (var item in lstData)
                {
                    objPA = new PupilAbsenceBO();
                    objPA.AcademicYearID = item.AcademicYearID;
                    objPA.SchoolID = item.SchoolID;
                    objPA.EducationLevelID = item.EducationLevelID;
                    objPA.ClassID = item.ClassID;
                    objPA.AbsentDate = item.AbsentDate;
                    objPA.PupilID = item.PupilID;
                    objPA.IsAccepted = item.IsAccepted;
                    objPA.Last2digitNumberSchool = item.Last2digitNumberSchool;
                    objPA.Section = Section;
                    lstPACheckAll.Add(objPA);
                }
                UpdatePupilAbsence(lstPACheckAll, lstToDelete, lstSection);
            }

            if (lstToDelete.Count > 0)
            {
                //Xoá dữ liệu cũ
                PupilAbsenceRepository.DeleteAll(lstToDelete);
            }
            #endregion

            //Lấy FaultCriteria cho nghỉ có phép và không phép
            var lstFault = FaultCriteriaBusiness.SearchBySchool(SchoolID).ToList();
            FaultCriteria P_Fault = lstFault.Find(x => x.Absence != null && x.Absence.Trim() == SystemParamsInFile.PUPIL_ABSENCE_P);
            FaultCriteria K_Fault = lstFault.Find(x => x.Absence != null && x.Absence.Trim() == SystemParamsInFile.PUPIL_ABSENCE_K);

            List<int> listFaultID = new List<int>();
            if (P_Fault != null) listFaultID.Add(P_Fault.FaultCriteriaID);
            if (K_Fault != null) listFaultID.Add(K_Fault.FaultCriteriaID);

            List<PupilFault> lstPupilFaultDelete = new List<PupilFault>();
            if (listFaultID.Count > 0)
            {
                //Lấy danh sách thông tin vi phạm cần xoá
                IQueryable<PupilFault> listPupilFault = PupilFaultBusiness.SearchBySchool(SchoolID, new PupilFaultSearchForm()
                {
                    ClassID = ClassID,
                    F_ViolatedDate = Month.StartOfMonth(),
                    T_ViolatedDate = Month.EndOfMonth(),
                    FaultID = listFaultID
                });
                lstPupilFaultDelete = (from p in listPupilFault
                                       join q in lstPOC on p.PupilID equals q.PupilID
                                       where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                       select p).ToList();

                // Neu co khoa vi pham thi khong xoa trong thoi gian do
                // Lay thong tin thoi gian bi khoa diem danh neu tk khong phai la admin
                if (!IsAdmin)
                {
                    LockTraining lockTraining = LockTrainingBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                    {
                        {"IsViolation", true}
                    }).FirstOrDefault();
                    if (lockTraining != null)
                    {
                        DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                        // Neu co thong tin khoa diem danh thi bo cac phan tu trong CSDL thuoc khoang khoa khong xoa
                        // Va bo cac phan tu trong list du lieu dau vao
                        lstPupilFaultDelete.RemoveAll(o => o.ViolatedDate >= fromLockDate && o.ViolatedDate < lockTraining.ToDate.AddDays(1));
                        // Bo cac phan tu insert trong khoang thoi gian bi khoa
                        listRemain.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                    }
                }
            }

            //Tổ hợp lại dữ lại cần insert vào PupilFault
            List<PupilFault> listFault = (from pa in listRemain
                                          group pa by new { pa.ClassID, pa.PupilID, pa.IsAccepted, pa.AbsentDate.Date } into g
                                          select new PupilFault
                                          {
                                              PupilID = g.Key.PupilID,
                                              AcademicYearID = academicYear.AcademicYearID,
                                              ClassID = ClassID,
                                              SchoolID = SchoolID,
                                              EducationLevelID = classProfile.EducationLevelID,
                                              FaultCriteria = g.Key.IsAccepted.GetValueOrDefault() ? P_Fault : K_Fault,
                                              NumberOfFault = g.Count(),
                                              ViolatedDate = g.Key.Date
                                          }).Where(x => x.FaultCriteria != null).ToList();
            listFault.ForEach(x => x.TotalPenalizedMark =
                x.NumberOfFault.GetValueOrDefault() * x.FaultCriteria.PenalizedMark.GetValueOrDefault());
            listFault.ForEach(x => x.FaultID = x.FaultCriteria.FaultCriteriaID);

            #region kiem tra thong tin vi pham de cap nhat CSDL
            int sizeInsert = listFault.Count;
            for (int i = sizeInsert - 1; i >= 0; i--)
            {
                PupilFault pf = listFault[i];
                PupilFault pfDB = lstPupilFaultDelete.Where(o => o.PupilID == pf.PupilID && o.ViolatedDate == pf.ViolatedDate && o.FaultID == pf.FaultID).FirstOrDefault();
                if (pfDB != null)
                {
                    // Cap nhat lai thong tin
                    pfDB.TotalPenalizedMark = pf.TotalPenalizedMark;
                    pfDB.NumberOfFault = pf.NumberOfFault;
                    PupilFaultBusiness.Update(pfDB);

                    listFault.RemoveAt(i);
                    lstPupilFaultDelete.Remove(pfDB);
                }
                else
                {
                    //Insert dữ liệu
                    PupilFaultBusiness.Insert(pf);
                }
            }

            if (lstPupilFaultDelete.Count > 0)
            {
                //Xoá dữ liệu cũ
                PupilFaultBusiness.DeleteAll(lstPupilFaultDelete);
            }
            #endregion

            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        public void SaveAMonthManyClass(int UserID, bool IsAdmin, int SchoolID, int AcademicYearID, int AppliedLevel,
            int Section, DateTime Month, List<PupilAbsence> ListDataMain, List<DateTime> lstOutDate = null)
        {
            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            List<int> lstClassID = ListDataMain.Select(x => x.ClassID).Distinct().ToList();
            List<PupilAbsence> ListData = new List<PupilAbsence>();
            Dictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["AcademicYearID"] = AcademicYearID;
            dicClass["AppliedLevel"] = AppliedLevel;
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(SchoolID, dicClass).ToList();
            ClassProfile classProfile = new ClassProfile();
            // Danh sách học sinh của lớp
            //List<PupilOfClass> listPupilAll = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>{ {"AcademicYearID", AcademicYearID},
            //                                                                                    {"AppliedLevel", AppliedLevel} }).ToList();
            List<PupilOfClass> listPupil = new List<PupilOfClass>();

            //Lấy FaultCriteria cho nghỉ có phép và không phép
            var lstFault = FaultCriteriaBusiness.SearchBySchool(SchoolID).ToList();
            List<int> listFaultID = null;
            //Lấy FaultCriteria cho nghỉ có phép và không phép
            FaultCriteria P_Fault = lstFault.Find(x => x.Absence != null && x.Absence.Trim() == SystemParamsInFile.PUPIL_ABSENCE_P);
            FaultCriteria K_Fault = lstFault.Find(x => x.Absence != null && x.Absence.Trim() == SystemParamsInFile.PUPIL_ABSENCE_K);
            listFaultID = new List<int>();
            if (P_Fault != null) listFaultID.Add(P_Fault.FaultCriteriaID);
            if (K_Fault != null) listFaultID.Add(K_Fault.FaultCriteriaID);
            IQueryable<PupilFault> listPupilFaultAll = null;
            if (listFaultID.Count > 0)
            {
                //Lấy danh sách thông tin vi phạm cần xoá
                listPupilFaultAll = PupilFaultBusiness.SearchBySchool(SchoolID, new PupilFaultSearchForm()
                {
                    //ClassID = objClassID,
                    F_ViolatedDate = Month.StartOfMonth(),
                    T_ViolatedDate = Month.EndOfMonth(),
                    FaultID = listFaultID,
                    AcademicYearID = AcademicYearID
                });
            }

            // Danh sách nghỉ học
            List<PupilAbsence> listPupilAbsenceAll = PupilAbsenceBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
                {
                    {"FromAbsentDate", lstOutDate != null && lstOutDate.Count > 0 ? lstOutDate[0] : Month.StartOfMonth()},
                    {"ToAbsentDate", lstOutDate != null && lstOutDate.Count > 0 ? lstOutDate[lstOutDate.Count - 1] : Month.EndOfMonth()},
                    {"AcademicYearID", AcademicYearID}
                }).Where(p=>lstClassID.Contains(p.ClassID)).ToList();
            List<PupilOfClass> lstPOCAll = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
                {
                    {"AcademicYearID", AcademicYearID},
                    {"AppliedLevel", AppliedLevel}
                }).Where(p => lstClassID.Contains(p.ClassID)).ToList();

            // Lay thong tin thoi gian bi khoa diem danh
            List<LockTraining> lstlockTraining = new List<LockTraining>();
            LockTraining lockTraining = null;
            if (!IsAdmin)
            {
                IDictionary<string, object> dicLockTraining = new Dictionary<string, object>()
                {
                    {"AcademicYearID", AcademicYearID}
                };
                lstlockTraining = LockTrainingBusiness.SearchBySchool(SchoolID, dicLockTraining).Where(p => lstClassID.Contains(p.ClassID)).ToList();
            }

            foreach (var objClassID in lstClassID)
            {
                ListData = ListDataMain.Where(x => x.ClassID == objClassID).ToList();
                #region
                classProfile = lstClass.Where(x => x.ClassProfileID == objClassID).FirstOrDefault();
                //Kiểm tra quyền giáo viên: chỉ có giáo viên có quyền chủ nhiệm hoặc giáo vụ mới được phép điểm danh
                if (!UtilsBusiness.HasHeadTeacherPermission(UserID, objClassID)
                    && !UtilsBusiness.HasOverseeingTeacherPermission(UserID, objClassID))
                {
                    throw new BusinessException("PupilAbsent_Error_NoPermission", classProfile.DisplayName);
                }

                //Kiểm tra lớp học có tương ứng trường học

                if (classProfile.SchoolID != SchoolID)
                {
                    throw new Exception("PupilAbsent_Error_NotCompatible_SchoolID_ClassID");
                }

                //Năm học không phải năm hiện tại
                AcademicYear academicYear = classProfile.AcademicYear;
                if (!AcademicYearBusiness.IsCurrentYear(academicYear.AcademicYearID))
                {
                    throw new BusinessException("Common_Validate_NotCurrentYear");
                }

                //Kiểm tra học sinh có trong lớp không
                listPupil = lstPOCAll.Where(x => x.ClassID == objClassID).ToList();
                //Kiểm tra thời gian có vượt quá ngày hiện tại không
                if (ListData.Exists(x => x.AbsentDate.Date > DateTime.Now.Date))
                {
                    throw new BusinessException("PupilAbsent_Error_OverCurrentDate");
                }

                //Kiểm tra thời gian có nằm trong năm học không
                if (ListData.Exists(x => x.AbsentDate.Date > academicYear.SecondSemesterEndDate.GetValueOrDefault().Date
                    || x.AbsentDate.Date < academicYear.FirstSemesterStartDate.GetValueOrDefault().Date))
                {
                    throw new BusinessException("PupilAbsent_Error_NotInSelectedAcademicYear");
                }

                //Kiểm tra có trùng bản ghi không
                if (ListData.Exists(x => ListData.FindAll(
                    y => y.PupilID == x.PupilID && y.AbsentDate == x.AbsentDate).Count > 1))
                {
                    throw new BusinessException("PupilAbsent_Error_Duplicate");
                }

                //Lấy thông tin điểm danh trong tháng
                List<PupilAbsence> listPupilAbsence = listPupilAbsenceAll.Where(x => x.ClassID == objClassID).ToList();
                List<PupilOfClass> lstPOC = lstPOCAll.Where(x => x.ClassID == objClassID).ToList();
                //Lấy danh sách cần xoá
                List<PupilAbsence> listToDelete = listPupilAbsence.Where(x => x.Section == Section).ToList();
                //Lấy danh sách cần insert chỉ chứa những học sinh đang học

                List<PupilAbsence> lstData = (from p in ListData
                                              join q in lstPOC on p.PupilID equals q.PupilID
                                              where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                              select p).ToList();
                //Lấy danh sách cần xoá điểm danh cũ.
                List<PupilAbsence> lstToDelete = (from p in listToDelete
                                                  join q in lstPOC on p.PupilID equals q.PupilID
                                                  where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                  select p).ToList();
                //Lấy danh sách còn lại
                List<PupilAbsence> lstPupilAbsen = (from p in listPupilAbsence
                                                    join q in lstPOC on p.PupilID equals q.PupilID
                                                    where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                    select p).ToList();
                List<PupilAbsence> listRemain = lstPupilAbsen.Where(x => x.Section != Section).ToList();

                // Lay thong tin thoi gian bi khoa diem danh neu tk khong phai la admin
                if (!IsAdmin)
                {
                    lockTraining = lstlockTraining.Where(x => x.ClassID == objClassID && (x.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE || x.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION)).FirstOrDefault();
                    if (lockTraining != null)
                    {
                        DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                        // Neu co thong tin khoa diem danh thi bo cac phan tu trong CSDL thuoc khoang khoa khong xoa
                        // Va bo cac phan tu trong list du lieu dau vao
                        lstToDelete.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                        lstData.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                    }
                }



                //Tạo dữ liệu cần insert
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                foreach (PupilAbsence pa in lstData)
                {
                    pa.AcademicYearID = academicYear.AcademicYearID;
                    pa.ClassID = objClassID;
                    pa.EducationLevelID = classProfile.EducationLevelID;
                    pa.SchoolID = SchoolID;
                    pa.Section = Section;
                    pa.Last2digitNumberSchool = partitionId;
                }

                //Lấy list sau cập nhật
                listRemain.AddRange(lstData);

                #region kiem tra thong tin vi pham de cap nhat CSDL
                int sizeInsert = lstData.Count;
                int sizeDelete = lstToDelete.Count;
                for (int i = sizeInsert - 1; i >= 0; i--)
                {
                    PupilAbsence pa = lstData[i];
                    PupilAbsence paDB = lstToDelete.Where(o => o.PupilID == pa.PupilID && o.AbsentDate == pa.AbsentDate).FirstOrDefault();
                    if (paDB != null)
                    {
                        if (pa.IsAccepted != paDB.IsAccepted)
                        {
                            // Cap nhat lai kieu nghi
                            paDB.IsAccepted = pa.IsAccepted;
                            this.Update(paDB);
                        }
                        lstData.RemoveAt(i);
                        lstToDelete.Remove(paDB);
                    }
                    else
                    {
                        //Insert dữ liệu
                        this.Insert(pa);
                    }
                }

                if (lstToDelete.Count > 0)
                {
                    //Xoá dữ liệu cũ
                    PupilAbsenceRepository.DeleteAll(lstToDelete);
                }
                #endregion

                List<PupilFault> lstPupilFaultDelete = new List<PupilFault>();
                if (listFaultID.Count > 0)
                {
                    //Lấy danh sách thông tin vi phạm cần xoá                   
                    List<PupilFault> listPupilFault = listPupilFaultAll.Where(x => x.ClassID == objClassID).ToList();
                    lstPupilFaultDelete = (from p in listPupilFault
                                           join q in lstPOC on p.PupilID equals q.PupilID
                                           where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                           select p).ToList();

                    // Neu co khoa vi pham thi khong xoa trong thoi gian do
                    // Lay thong tin thoi gian bi khoa diem danh neu tk khong phai la admin
                    if (!IsAdmin)
                    {
                        //    LockTraining lockTraining = LockTrainingBusiness.SearchByClass(objClassID, new Dictionary<string, object>()
                        //{
                        //    {"IsViolation", true}
                        //}).FirstOrDefault();
                        lockTraining = lstlockTraining.Where(o => o.ClassID == objClassID && (o.TrainingType == GlobalConstants.LOCK_TRAINING_VIOLATION || o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION)).FirstOrDefault();
                        if (lockTraining != null)
                        {
                            DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                            // Neu co thong tin khoa diem danh thi bo cac phan tu trong CSDL thuoc khoang khoa khong xoa
                            // Va bo cac phan tu trong list du lieu dau vao
                            lstPupilFaultDelete.RemoveAll(o => o.ViolatedDate >= fromLockDate && o.ViolatedDate < lockTraining.ToDate.AddDays(1));
                            // Bo cac phan tu insert trong khoang thoi gian bi khoa
                            listRemain.RemoveAll(o => o.AbsentDate >= fromLockDate && o.AbsentDate < lockTraining.ToDate.AddDays(1));
                        }
                    }
                }

                //Tổ hợp lại dữ lại cần insert vào PupilFault
                List<PupilFault> listFault = (from pa in listRemain
                                              group pa by new { pa.ClassID, pa.PupilID, pa.IsAccepted, pa.AbsentDate.Date } into g
                                              select new PupilFault
                                              {
                                                  PupilID = g.Key.PupilID,
                                                  AcademicYearID = academicYear.AcademicYearID,
                                                  ClassID = objClassID,
                                                  SchoolID = SchoolID,
                                                  EducationLevelID = classProfile.EducationLevelID,
                                                  FaultCriteria = g.Key.IsAccepted.GetValueOrDefault() ? P_Fault : K_Fault,
                                                  NumberOfFault = g.Count(),
                                                  ViolatedDate = g.Key.Date
                                              }).Where(x => x.FaultCriteria != null).ToList();
                listFault.ForEach(x => x.TotalPenalizedMark =
                    x.NumberOfFault.GetValueOrDefault() * x.FaultCriteria.PenalizedMark.GetValueOrDefault());
                listFault.ForEach(x => x.FaultID = x.FaultCriteria.FaultCriteriaID);

                #region kiem tra thong tin vi pham de cap nhat CSDL
                sizeInsert = listFault.Count;
                sizeDelete = lstPupilFaultDelete.Count;
                for (int i = sizeInsert - 1; i >= 0; i--)
                {
                    PupilFault pf = listFault[i];
                    PupilFault pfDB = lstPupilFaultDelete.Where(o => o.PupilID == pf.PupilID && o.ViolatedDate == pf.ViolatedDate && o.FaultID == pf.FaultID).FirstOrDefault();
                    if (pfDB != null)
                    {
                        // Cap nhat lai thong tin
                        pfDB.TotalPenalizedMark = pf.TotalPenalizedMark;
                        pfDB.NumberOfFault = pf.NumberOfFault;
                        PupilFaultBusiness.Update(pfDB);

                        listFault.RemoveAt(i);
                        lstPupilFaultDelete.Remove(pfDB);
                    }
                    else
                    {
                        //Insert dữ liệu
                        PupilFaultBusiness.Insert(pf);
                    }
                }

                if (lstPupilFaultDelete.Count > 0)
                {
                    //Xoá dữ liệu cũ
                    PupilFaultBusiness.DeleteAll(lstPupilFaultDelete);
                }
                #endregion
                #endregion
            }
            PupilAbsenceBusiness.Save();
            PupilFaultBusiness.Save();
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        public List<int> GetTypeSection(List<int> a, int b)
        {
            if (b == 1)
            {
                a.Add(1);
                a.Add(4);
                a.Add(5);
            }
            else if (b == 2)
            {
                a.Add(2);
                a.Add(4);
                a.Add(6);
            }
            else if (b == 3)
            {
                a.Add(3);
                a.Add(5);
                a.Add(6);
            }
            a.Add(7);
            return a;
        }

        public List<int> GetSection(List<int> a, int b)
        {

            switch (b)
            {
                case 7:
                    a.Add(1);
                    a.Add(2);
                    a.Add(3);
                    break;
                case 6:
                    a.Add(2);
                    a.Add(3);
                    break;
                case 5:
                    a.Add(1);
                    a.Add(3);
                    break;
                case 4:
                    a.Add(1);
                    a.Add(2);
                    break;
                case 3:
                    a.Add(3);
                    break;
                case 2:
                    a.Add(2);
                    break;
                case 1:
                    a.Add(1);
                    break;
            }

            return a;
        }

        public void UpdatePupilAbsence(List<PupilAbsenceBO> lstData, List<PupilAbsence> lstToDelete, List<int> lstSection)
        {
            int sizeInsert = lstData.Count;
            for (int i = sizeInsert - 1; i >= 0; i--)
            {
                PupilAbsenceBO pa = lstData[i];
                PupilAbsence paDB = new PupilAbsence();
                if (lstSection.Count > 1)
                    paDB = lstToDelete.Where(o => o.PupilID == pa.PupilID && o.AbsentDate == pa.AbsentDate && o.Section == pa.Section).FirstOrDefault();
                else
                    paDB = lstToDelete.Where(o => o.PupilID == pa.PupilID && o.AbsentDate == pa.AbsentDate).FirstOrDefault();
                if (paDB != null)
                {
                    if (pa.IsAccepted != paDB.IsAccepted)
                    {
                        // Cap nhat lai kieu nghi
                        paDB.IsAccepted = pa.IsAccepted;
                        this.Update(paDB);
                    }
                    lstData.RemoveAt(i);
                    lstToDelete.Remove(paDB);
                }
                else
                {
                    //Insert dữ liệu
                    PupilAbsence objPA = new PupilAbsence();
                    objPA.AcademicYearID = pa.AcademicYearID;
                    objPA.SchoolID = pa.SchoolID;
                    objPA.EducationLevelID = pa.EducationLevelID;
                    objPA.ClassID = pa.ClassID;
                    objPA.AbsentDate = pa.AbsentDate.HasValue ? pa.AbsentDate.Value : new DateTime();
                    objPA.PupilID = pa.PupilID;
                    objPA.IsAccepted = pa.IsAccepted;
                    objPA.Last2digitNumberSchool = pa.Last2digitNumberSchool;
                    objPA.Section = pa.Section;
                    this.Insert(objPA);
                }
            }
        }
    }
}
