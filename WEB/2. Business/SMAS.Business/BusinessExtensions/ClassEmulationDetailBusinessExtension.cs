﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ClassEmulationDetailBusiness
    {

        #region Tim kiem chi tiet thi dua theo tuan
        /// <summary>
        /// Tim kiem chi tiet thi dua theo tuan
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<ClassEmulationDetail> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Month = Utils.GetInt(SearchInfo, "Month");
            int ClassEmulationID = Utils.GetInt(SearchInfo, "ClassEmulationID");
            DateTime? FromDate = Utils.GetDateTime(SearchInfo, "FromDate");
            DateTime? EndDate = Utils.GetDateTime(SearchInfo, "EndDate");
            IQueryable<ClassEmulationDetail> ListClassEmulationDetail = ClassEmulationDetailRepository.All;
            // Tim kiem theo dieu kien truyen vao
            if (ClassID != 0)
            {
                ListClassEmulationDetail = from ced in ListClassEmulationDetail
                                           join cp in ClassProfileBusiness.All on ced.ClassID equals cp.ClassProfileID
                                           where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                           && ced.ClassID == ClassID
                                           select ced;
            }
            // Theo truong
            if (SchoolID != 0)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.ClassProfile.SchoolID == SchoolID);
            }
            // Theo nam hoc
            if (AcademicYearID != 0)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.ClassProfile.AcademicYearID == AcademicYearID);
            }

            // Theo ket qua thi dua theo lop
            if (ClassEmulationID != 0)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.ClassEmulationID == ClassEmulationID);
            }
            // Theo khoi
            if (EducationLevelID != 0)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.ClassProfile.EducationLevelID == EducationLevelID);
            }
            // Theo nam
            if (Year != 0)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.ClassProfile.AcademicYear.Year == Year);
            }
            // Theo ngay bat dau va ngay ket thuc
            if (FromDate.HasValue)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.PenalizedDate >= FromDate.Value);
            }
            if (EndDate.HasValue)
            {
                ListClassEmulationDetail = ListClassEmulationDetail.Where(x => x.PenalizedDate <= EndDate.Value);
            }

            return ListClassEmulationDetail;
        }

        public IQueryable<ClassEmulationDetail> SearchBySchool(int SchoolID = 0, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0) return null;
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
        #endregion

        #region Xoa du lieu dua vao bang diem thi dua cua lop theo tuan
        /// <summary>
        /// Xoa du lieu dua vao bang diem thi dua cua lop theo tuan
        /// </summary>
        /// <param name="ClassEmulationID"></param>
        public void DeleteByClassEmulation(int ClassEmulationID)
        {
            IQueryable<ClassEmulationDetail> ListClassEmulationDetail = this.SearchByClassEmulation(ClassEmulationID);
            if (ListClassEmulationDetail != null)
            {
                this.DeleteAll(ListClassEmulationDetail.ToList());
            }
        }
        #endregion

        #region Tim kiem du lieu dua vao bang diem thi dua cua lop theo tuan
        public IQueryable<ClassEmulationDetail> SearchByClassEmulation(int ClassEmulationID)
        {
            if (ClassEmulationID == 0)
            {
                return null;
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo.Add("ClassEmulationID", ClassEmulationID);
            return this.Search(SearchInfo);
        }
        #endregion
    }
}