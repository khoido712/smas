﻿
/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{

    public partial class CommuneBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int COMMUNECODE_MAX_LENGTH = 10; //Độ dài mã XÃ PHƯỜNG
        private const int COMMUNENAME_MAX_LENGTH = 50; //Độ dài tên xã phường
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn
        private const int VALUE_MIN = 1;//Giá trị min
        private const int VALUE_MAX = 3;//Giá trị max
        #endregion


        #region Search


        /// <summary>
        /// Tìm kiếm Xã/Phường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="CommuneName">Tên xã phường</param>
        /// <param name="CommuneCode">Mã xã phường</param>
        /// <param name="ProvinceID">ID tỉnh/ thành phố</param>
        /// <param name="DistrictID">ID quận/huyện</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến Kích hoạt</param>
        /// <returns>IQueryable<Commune></returns>
        public IQueryable<Commune> Search(IDictionary<string, object> dic)
        {
            string CommuneName = Utils.GetString(dic, "CommuneName");
            string CommuneCode = Utils.GetString(dic, "CommuneCode");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int CommuneID = Utils.GetInt(dic, "CommuneID");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<Commune> lsCommune = CommuneRepository.All;


            if (IsActive.HasValue)
                lsCommune = lsCommune.Where(pro => pro.IsActive == IsActive && pro.District.IsActive == true);
            if (CommuneName.Trim().Length != 0)
                lsCommune = lsCommune.Where(pro => pro.CommuneName.Contains(CommuneName.ToLower()) && pro.District.IsActive == true);
            if (CommuneCode.Trim().Length != 0)
                lsCommune = lsCommune.Where(pro => pro.CommuneCode.Contains(CommuneCode.ToLower()) && pro.District.IsActive == true);
            if (Description.Trim().Length != 0)
                lsCommune = lsCommune.Where(pro => pro.Description.Contains(Description.ToLower()) && pro.District.IsActive == true);
            if (ProvinceID != 0)
                lsCommune = lsCommune.Where(pro => pro.District.ProvinceID == ProvinceID && pro.District.IsActive == true);
            if (DistrictID != 0)
                lsCommune = lsCommune.Where(pro => pro.DistrictID == DistrictID);
            if (CommuneID != 0)
            {
                lsCommune = lsCommune.Where(pro => pro.CommuneID == CommuneID);
            }



            return lsCommune;

        }
        #endregion

        #region Insert


        /// <summary>
        /// Thêm Xã/Phường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="commune">object commune.</param>
        /// <returns>object commune</returns>
        public override Commune Insert(Commune commune)
        {
            //Mã Xã/Phường không được để trống 
            Utils.ValidateRequire(commune.CommuneCode, "Commune_Label_CommuneCode");
            //Độ dài mã Xã/Phường không được vượt quá 10 
            Utils.ValidateMaxLength(commune.CommuneCode, COMMUNECODE_MAX_LENGTH, "Commune_Label_CommuneCode");
            //Mã Xã/Phường đã tồn tại 
            new CommuneBusiness(null).CheckDuplicate(commune.CommuneCode, GlobalConstants.LIST_SCHEMA, "Commune", "CommuneCode", true, commune.CommuneID, "Commune_Label_CommuneCode");
            //Tên Xã/Phường không được để trống 
            Utils.ValidateRequire(commune.CommuneName, "Commune_Label_CommuneName");
            //Độ dài trường tên Xã/Phường không được vượt quá 50 
            Utils.ValidateMaxLength(commune.CommuneName, COMMUNENAME_MAX_LENGTH, "Commune_Label_CommuneName");
            //Tên Xã/Phường đã tồn tại 
            new CommuneBusiness(null).CheckDuplicate(commune.CommuneName, GlobalConstants.LIST_SCHEMA, "Commune", "CommuneName", true, commune.CommuneID, "Commune_Label_CommuneName");
            //Không tồn tại Quận huyện hoặc đã bị xóa - Kiểm tra DistrictID trong bảng Province với IsActive =1            
            new DistrictBusiness(null).CheckAvailable(commune.DistrictID, "Commune_DistrictID", true);
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(commune.Description, DESCRIPTION_MAX_LENGTH, "Description");

            //Insert
            base.Insert(commune);
            return commune;

        }
        #endregion

        #region Update

        /// <summary>
        /// Sửa Xã/Phường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="commune">object commune.</param>
        /// <returns>object commune</returns>
        public override Commune Update(Commune commune)
        {
            //Bạn chưa chọn Xã/Phường cần sửa hoặc Xã/Phường bạn chọn đã bị xóa khỏi hệ thống
            new CommuneBusiness(null).CheckAvailable((int)commune.CommuneID, "Commune_Label_CommuneID", true);
            //Mã Xã/Phường không được để trống 
            Utils.ValidateRequire(commune.CommuneCode, "Commune_Label_CommuneCode");
            //Độ dài mã Xã/Phường không được vượt quá 10 
            Utils.ValidateMaxLength(commune.CommuneCode, COMMUNECODE_MAX_LENGTH, "Commune_Label_CommuneCode");
            //Mã Xã/Phường đã tồn tại 
            new CommuneBusiness(null).CheckDuplicate(commune.CommuneCode, GlobalConstants.LIST_SCHEMA, "Commune", "CommuneCode", true, commune.CommuneID, "Commune_Label_CommuneCode");
            //Tên Xã/Phường không được để trống 
            Utils.ValidateRequire(commune.CommuneName, "Commune_Label_CommuneName");
            //Độ dài trường tên Xã/Phường không được vượt quá 50 
            Utils.ValidateMaxLength(commune.CommuneName, COMMUNENAME_MAX_LENGTH, "Commune_Label_CommuneName");
            //Tên Xã/Phường đã tồn tại 
            new CommuneBusiness(null).CheckDuplicate(commune.CommuneName, GlobalConstants.LIST_SCHEMA, "Commune", "CommuneName", true, commune.CommuneID, "Commune_Label_CommuneName");
            //Không tồn tại Quận huyện hoặc đã bị xóa - Kiểm tra DistrictID trong bảng Province với IsActive =1
            new DistrictBusiness(null).CheckAvailable(commune.DistrictID, "Commune_DistrictID", true);
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(commune.Description, DESCRIPTION_MAX_LENGTH, "Description");


            base.Update(commune);
            return commune;
        }
        #endregion

        #region Delete

        /// <summary>
        /// Sửa Xã/Phường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="CommuneID">ID Xã/Phường</param>
        public void Delete(int CommuneID)
        {
            //Bạn chưa chọn Xã/Phường cần xóa hoặc Xã/Phường bạn chọn đã bị xóa khỏi hệ thống
            new CommuneBusiness(null).CheckAvailable(CommuneID, "Commune_Label_CommuneID", true);
            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "Commune", CommuneID, "Commune_Label_CommuneIDFailed");
            ////Không thể xóa Xã/Phường đang sử dụng
            //new CommuneBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "Commune", CommuneID, "Commune_Label_CommuneID");
            base.Delete(CommuneID, true);
        }
        #endregion
    }
}