﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author namdv3
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DailyDishCostBusiness
    {
        private const int COST_ENTITY = 9;
        private void Validation(DailyDishCost Entity, DateTime? oldEffectDate)
        {
            if (Entity.Cost.ToString().Length > COST_ENTITY)
                throw new BusinessException("DailyDishCost_Validate_Cost_MaxLength");
            if (Entity.Cost <= 0)
                throw new BusinessException("DailyDishCost_Validate_Cost_IsInt");
            DateTime? max_InspectedDate = null;
            //if (DishInspectionRepository.All.Count() > 0)
                //max_InspectedDate = DishInspectionRepository.All.Where(o=>o.SchoolID==Entity.SchoolID).Select(o => o.InspectedDate).FirstOrDefault();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = Entity.SchoolID;
            var listDish = this.SearchBySchool(Entity.SchoolID, SearchInfo).Where(o => o.EffectDate == oldEffectDate).Where(o => o.SchoolID == Entity.SchoolID).FirstOrDefault();
            if (listDish!=null)
            {
                if (Entity.EffectDate <= max_InspectedDate)
                    throw new BusinessException("DailyDishCost_Validate_EffectDate");
            }
            // Thêm cùng tháng khai báo tiền ăn thì thông báo và không cho phép insert
            IQueryable<DailyDishCost> DailyDishCost = this.DailyDishCostBusiness.All;
            if (Entity.EffectDate != null && (!oldEffectDate.HasValue || oldEffectDate.Value.ToShortDateString() != Entity.EffectDate.ToShortDateString()))
            {
                DailyDishCost = DailyDishCost.Where(p => p.EffectDate == Entity.EffectDate && p.IsActive == true);
                if (DailyDishCost.Count() > 0)
                {
                    int month = Entity.EffectDate.Month;
                    throw new BusinessException("DailyDishCost_Validate_CheckEffectDate", month);
                }
            }

        }
        public void Insert(DailyDishCost Entity)
        {
            Validation(Entity, null);
            base.Insert(Entity);

        }
        public void Update(DailyDishCost Entity, DateTime oldEffectDate)
        {
            Validation(Entity, oldEffectDate);
            base.Update(Entity);
        }
        private void ValidationDel(int DailyDishCostID, int SchoolID)
        {
            if (!DailyDishCostRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "DailyDishCost", new Dictionary<string, object> { { "DailyDishCostID", DailyDishCostID } }))
                throw new BusinessException("DailyDishCost_Validate_NotPK");

            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "DailyDishCost", DailyDishCostID, "DailyDishCost_Validate_DailyDishCostID");

            bool DailyDishCostCompatible = new DailyDishCostRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "DailyDishCost",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"DailyDishCostID",DailyDishCostID}
                }, null);
            if (!DailyDishCostCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }
        public void Delete(int DailyDishCostID, int SchoolID)
        {
            ValidationDel(DailyDishCostID, SchoolID);
            var DailyDishCost = this.All.Where(o => o.DailyDishCostID == DailyDishCostID).FirstOrDefault();
            if (DailyDishCost != null)
            {
                DailyDishCost.IsActive = false;
                DailyDishCost.ModifiedDate = DateTime.Now;
                base.Update(DailyDishCost);
            }
        }


        private IQueryable<DailyDishCost> Search(IDictionary<string, object> SearchInfo)
        {
            var query = this.All;
            if (query.Count() > 0)
            {
                string Note = Utils.GetString(SearchInfo, "Note");
                int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
                DateTime? EffectDate = Utils.GetDateTime(SearchInfo, "EffectDate");
                int Cost = Utils.GetInt(SearchInfo, "Cost");
                bool? IsActive = Utils.GetIsActive(SearchInfo, "IsActive");
                if (!string.IsNullOrWhiteSpace(Note))
                    query = query.Where(o => o.Note == Note || o.Note.Contains(Note));
                if (EffectDate.HasValue)
                    query = query.Where(o => o.EffectDate == EffectDate.Value);
                if (SchoolID > 0)
                    query = query.Where(o => o.SchoolID == SchoolID);
                if (Cost > 0)
                    query = query.Where(o => o.Cost == Cost);
                if (IsActive.HasValue)
                    query = query.Where(o => o.IsActive == IsActive.Value);
                return query;
            }
            else return null;
        }

        public IQueryable<DailyDishCost> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
        public int MaxDailyDishCost(DateTime date, int SchoolID)
        {
            //var max_EffectDate = this.All.Where(o => o.SchoolID == SchoolID).Select(o => o.EffectDate).Max();
            var query = this.All.Where(o => o.EffectDate< date && o.SchoolID == SchoolID).OrderByDescending(o=>o.EffectDate).FirstOrDefault();
            if (query != null)
            {
                return query.Cost;
            }
            return 0;
        }
    }
}