﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamSupervisoryBO
    {
        public long ExamSupervisoryID { get; set; }
        public long ExaminationsID { get; set; }
        public long? ExamGroupID { get; set; }
        public string ExamGroupName { get; set; }
        public int AcademicYearID { get; set; }
        public int? SchoolID { get; set; }
        public int? AppliedLevel { get; set; }
        public int TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string EmployeeCode { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
        public bool? Genre { get; set; }
        public int FacultyID { get; set; }
        public string FacultyName { get; set; }
        public string SchoolFacultyName { get; set; }
        public string Telephone { get; set; }
        public string EthnicCode { get; set; }
        public bool IsDelete { get; set; }
    }
}
