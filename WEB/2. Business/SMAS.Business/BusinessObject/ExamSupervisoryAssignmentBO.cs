﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamSupervisoryAssignmentBO
    {
        public long ExamSupervisoryAssignmentID { get; set; }
        public long ExamSupervisoryID { get; set; }
        public int TeacherID { get; set; }
        public int? SchoolFacultyID { get; set; }
        public long ExamRoomID { get; set; }
        public int SubjectID { get; set; }
        public int AcademicYearID { get; set; }
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public DateTime? CreateDate { get; set; }

        public string EmployeeCode { get; set; }
        public string EmployeeFullName { get; set; }
        public DateTime? EmployeeBirthDate { get; set; }
        public string EthnicCode { get; set; }
        public string EmployeePhoneNumber { get; set; }
        public string SchoolFacultyName { get; set; }
        public string ExamRoomCode { get; set; }
        public string SubjectName { get; set; }
        public int? OrderInSubject { get; set; }
        public string SchedulesExam { get; set; }
        public string ExamGroupCode { get; set; }
        public string ExamGroupName { get; set; }
    }
}
