﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PupilOfSchoolBO
    {
        public int AcademicYearID { get; set; }
        public int Year { get; set; }
        public string DisplayAcademicYear { get; set; }
        public int PupilOfSchoolID { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public int PupilID { get; set; }
        public int PupilOfClassID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public DateTime? EnrolmentDate { get; set; }
        public int? EnrolmentType { get; set; }
        public int? SynChronizeID { get; set; }
        public string StorageName { get; set; }
        public int EducationLevelId { get; set; }
    }
}
