﻿using System;

namespace SMAS.Business.BusinessObject
{
    public class BakMarkRecordBO
    {
        
        public decimal? OldMark { get; set; }
        public string M_MarkRecord { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }
        public bool? IsOldData { get; set; }
        public int? SynchronizeID { get; set; }
        public int? PeriodID { get; set; }
        public int? LogChange { get; set; }
        public int? Year { get; set; }
        public long MarkRecordID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public int? M_OldID { get; set; }
        public int MarkTypeID { get; set; }
        public string Title { get; set; }
        public decimal Mark { get; set; }
        public int OrderNumber { get; set; }
        public DateTime? MarkedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? Semester { get; set; }
    }
    public class KeyDelMarkBO
    {
        public int? PeriodID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public int MarkTypeID { get; set; }
        public int? Semester { get; set; }
    }

    

    public class BakJudgeRecordBO
    {
        public long JudgeRecordID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public int MarkTypeID { get; set; }
        public Nullable<int> Semester { get; set; }
        public string Judgement { get; set; }
        public string ReTestJudgement { get; set; }
        public int OrderNumber { get; set; }
        public Nullable<System.DateTime> MarkedDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Title { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<bool> IsOldData { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public string OldJudgement { get; set; }
        public string MJudgement { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> PeriodID { get; set; }
        public Nullable<int> LogChange { get; set; }
    }

    public class BakSummedUpBO
    {
        public long SummedUpRecordID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public int IsCommenting { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> PeriodID { get; set; }
        public Nullable<decimal> SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> SummedUpDate { get; set; }
        public Nullable<decimal> ReTestMark { get; set; }
        public string ReTestJudgement { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<bool> IsOldData { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
        public Nullable<int> Year { get; set; }
    }

    public class ResKeyDelSummedUpBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public int IsCommenting { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> PeriodID { get; set; }
    }
}
