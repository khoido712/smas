﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
namespace SMAS.Business.BusinessObject
{
    public class TeacherNoteBookSemesterBO:TeacherNoteBookSemester
    {
        public int? isCommenting { get; set; }
        public int? AppliedType { get; set; }
        public string SubjectName { get; set; }
    }
}
