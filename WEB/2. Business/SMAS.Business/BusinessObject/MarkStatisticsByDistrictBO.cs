﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>03/12/2012 2:43:14 PM</date>
    /// </summary>
   public class MarkStatisticsByDistrictBO
    {
        #region Member Variables
        public int DistrictID { get; set; }
        public string DistrictName  { get; set; }
        public int TotalSchool { get; set; }
        public IDictionary<string,int> MarkInfo { get; set; }
        public int TotalTest  { get; set; }
        public int TotalBelowAverage { get; set; }
        public double PercentBelowAverage { get; set; }
        public int TotalAboveAverage { get; set; }
        public double PercentAboveAverage { get; set; }

        #endregion

      public MarkStatisticsByDistrictBO()
        {
        if(this.MarkInfo == null)
        {
            this.MarkInfo = new Dictionary<string, int>();
        }
        }

    }
}
