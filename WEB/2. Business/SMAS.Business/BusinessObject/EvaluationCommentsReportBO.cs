﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    
    public class EvaluationCommentsReportBO
    {
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public int PupilID { get; set; }
        public int OrderInClass { get; set; }
        public int Status { get; set; }
        /// <summary>
        /// Mon hoc hoac tieu chi danh gia
        /// </summary>
        public int EvaluationCriteriaID { get; set; }
        /// <summary>
        /// Luu thong tin mat danh gia cua hoc sinh vao IDictionary. key=1:Education, key=2:Cappacities,key=3:Qualities
        /// </summary>
        public IDictionary<int, EvaluationComments> DicEvaluation { get; set; }

        /// <summary>
        /// Nang luc pham chat cuoi ky theo tieu chi danh gia cua GVCN
        /// </summary>
        public IDictionary<int, SummedEvaluation> CapacitesOrQualitiesSemester { get; set; }
        /// <summary>
        /// Luu nhan xet danh gia cuoi ky cua hoc sinh vao Dictionary key=1:Education, key=2:Cappacities,key=3:Qualities
        /// </summary>
        public IDictionary<int, SummedEvaluation> DicSummedEvaluation { get; set; }
        public IDictionary<int, SummedEndingEvaluation> DicSummedEndingEvaluation { get; set; }
        public bool Error { get; set; }
        public string MessageError { get; set; }
        public Dictionary<string, object> arrComment { get; set; }
        public int IsCommenting { get; set; }
        //Su dung khi import so GVCN thang
        public int EvaluationId { get; set; }


    }


}
