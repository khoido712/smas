﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SchoolProfileBO
    {
        public int SchoolProfileID { get; set; }
        public int AcademicYearID { get; set; }
        public int? DistrictID { get; set; }
        public int? ProvinceID { get; set; }
        public string SchoolCode { get; set; }
        public string SchoolName { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int EducationGrade { get; set; }
    }

    public class ResponseData
    {
        public int TotalPage { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public int page { get; set; }
        public string version { get; set; }
        public List<SchoolProfileAPI> Data { get; set; }
    }

    public class SchoolProfileAPI
    {
        public string Address { get; set; }
        public string District { get; set; }
        public string DistrictID { get; set; }
        public string Email { get; set; }
        public string Lever { get; set; }
        public string ManagementUnit { get; set; }
        public string NameHeadMaster { get; set; }
        public string NameSchool { get; set; }
        public string Phone { get; set; }
        public string Province { get; set; }
        public string ProvinceAlias { get; set; }
        public string ProvinceID { get; set; }
        public string Quality { get; set; }
        public string SchoolID { get; set; }
        public string SchoolOfType { get; set; }
        public string Status { get; set; }
        public int TotalClass { get; set; }
        public int TotalStudent { get; set; }
        public int TotalTeacher { get; set; }
        public string Type { get; set; }
        public string Ward { get; set; }
    }
}
