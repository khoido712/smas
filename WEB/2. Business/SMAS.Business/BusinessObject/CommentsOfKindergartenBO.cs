﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class CommentsOfKindergartenBO
    {
        public int CommnetsOfKindergatenId { get; set; }
        public int SchoolId { get; set; }
        public int AcademicYearId { get; set; }
        public int AccountId { get; set; }
        public string CommnetCode { get; set; }
        public string Commnet { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
     }
}
