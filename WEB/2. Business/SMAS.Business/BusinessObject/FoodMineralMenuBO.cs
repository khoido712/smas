﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class FoodMineralMenuBO
    {
        public int MineralID { get; set; }
        public decimal Value { get; set; }
        public decimal ValueFrom { get; set; }
        public decimal ValueTo { get; set; }
        public string MineralName { get; set; }
    }
}
