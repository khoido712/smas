﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EvaluationCriteiaBO
    {
        public int EvaluationCriteriaID { get; set; }
        public string CriteriaName { get; set; }
        public int TypeID { get; set; }
    }
}
