﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSHistoryBO
    {
        public long? SenderGroup { get; set; }
        public bool IsAdmin { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string SMSContent { get; set; }
        public int SMSCNT { get; set; }
        public string TypeName { get; set; }
        public int TypeID { get; set; }
        public int ReceiverID { get; set; }
        public int ClassID { get; set; }
        public int SenderID { get; set; }
        public string SenderName { get; set; }
        public bool Status { get; set; }
        public bool? IsOnSchedule { get; set; }
        public Nullable<System.DateTime> SendTime { get; set; }
        public bool IsCustomType { get; set; }
        public string Subscriber { get; set; }
    }
}
