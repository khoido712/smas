﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ChatMessageBO
    {
        public Guid MessageID { get; set; }
        public int ChatGroupID { get; set; }

        public int UserAccountSendID { get; set; }

        public string ContentMessage { get; set; }

        public DateTime SendDate { get; set; }
    }
}
