﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MSendSMSResult
    {
        public MSendSMSResult(bool success, int successCount, string message)
        {
            this.Success = success;
            this.SuccessCount = successCount;
            this.Message = message;
        }

        public bool Success { get; set; }

        public int SuccessCount { get; set; }

        public string Message { get; set; }
    }
}
