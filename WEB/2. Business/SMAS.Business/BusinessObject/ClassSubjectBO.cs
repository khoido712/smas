﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ClassSubjectBO
    {        
        public int SubjectID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string Abbreviation { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string DisplayName { get; set; }
        public Nullable<int> OrderInSubject { get; set; }
        public int? IsCommenting { get; set; }
        public int? AppliedType { get; set; }
        public decimal? SectionPerWeekFirstSemester { get; set; }
        public decimal? SectionPerWeekSecondSemester { get; set; }
        public long? ClassSubjectID { get; set; }
        public int? OrderInClass { get; set; }
        public bool? Is_Specialize_Subject { get; set; }
        public bool? IsVNEN { get; set; }
        public bool IsForeignLanguage { get; set; }

        public int? FirstSemesterCoefficient { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectIDCrease { get; set; }
        public int? M_Min_Fist_Semester { get; set; }
        public int? P_Min_Fist_Semester { get; set; }
        public int? V_Min_Fist_Semester { get; set; }
        public int? M_Min_Second_Semester { get; set; }
        public int? P_Min_Second_Semester { get; set; }
        public int? V_Min_Second_Semester { get; set; }

        public int SemesterID { get; set; }
    }
}
