﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class BrandNameUsingBO
    {
        public int BrandnameRegistrationID { get; set; }
        public string Brandname { get; set; }
        public int ProviderID { get; set; }
        public string ProviderName { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public int Status { get; set; }


    }
}
