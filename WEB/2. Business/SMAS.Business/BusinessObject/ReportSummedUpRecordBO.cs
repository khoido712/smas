﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportSummedUpRecordBO
    {
        public int EducationLevelID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public int NumberOfPupil { get; set; }
    }
}
