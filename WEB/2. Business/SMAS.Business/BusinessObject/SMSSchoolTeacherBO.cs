﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSSchoolTeacherBO
    {

        public string Content { get; set; }
        public List<string> ListContent { get; set; }
        public int SchoolId { get; set; }
        public int AcademicYearID { get; set; }
        public SchoolProfile School { get; set; }
        public AcademicYear AcademicYearBO { get; set; }

        public int Type { get; set; }

        public int Semester { get; set; }

        public int SenderID { get; set; }

        public List<int> ListReceiverId { get; set; }

        public int ContactGroupId { get; set; }

        public int ClassId { get; set; }

        public string ShortContent { get; set; }

        public bool IsPrincipal { get; set; }
        public bool IsAdmin { get; set; }
        public int? TypeHistory { get; set; }
        public int EducationLevelId { get; set; }
        public bool SendAllSchool { get; set; }
        public int LevelId { get; set; }
        public List<string> ListShortContent { get; set; }
        public int UserType { get; set; }

        public long SenderGroup { get; set; }

        public string SchoolUserName { get; set; }
        public bool IsSignMsg { get; set; }
        public int CountSMS { get; set; }
        public bool IsSendSMSCommentOfPupil { get; set; }
        public PromotionProgramBO Promotion { get; set; }
    }
}
