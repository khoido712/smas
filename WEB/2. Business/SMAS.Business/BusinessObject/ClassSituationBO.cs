﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ClassSituationBO
    {
        public int ClassID { get; set; }
        public int InputedNum { get; set; }
        public int MissPupilNum { get; set; }
        public int TotalPupil { get; set; }
        public string StrMissDivTotal
        {
            get
            {
                return MissPupilNum + "/" + TotalPupil;
            }
        }
        public ClassSituationBO(int ClassID)
        {
            this.ClassID = ClassID;
        }
    }
}
