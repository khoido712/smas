﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DailyInfo
    {
        public int MealID { get; set; }
        public string DishName { get; set; }
    }
}
