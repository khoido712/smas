﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
   public class SummedUpRecordMarkBO
    {
       public SummedUpRecord SummedUpRecord{get;set;}
       public SummedUpRecordBO SummedUpRecordBO { get; set; }
       public string Mark { get; set; }
    }
    public class SummedUpRecordMarkHistoryBO
    {
        public SummedUpRecordHistory SummedUpRecordHistory { get; set; }
        public SummedUpRecordBO SummedUpRecordBO { get; set; }
        public string Mark { get; set; }
    }
}
