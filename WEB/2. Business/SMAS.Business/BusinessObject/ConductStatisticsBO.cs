﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class ConductStatisticsBO : ConductStatistic
    {
        public string SchoolName { get; set; }
        public int? TotalGood { get; set; }
        public int? TotalFair { get; set; }
        public int? TotalNormal { get; set; }
        public int? TotalWeak { get; set; }
        public decimal? PercentGood { get; set; }
        public decimal? PercentFair { get; set; }
        public decimal? PercentNormal { get; set; }
        public decimal? PercentWeak { get; set; }
        public int? TotalPupil { get; set; }
        public int? TotalAboveAverage { get; set; }
        public decimal? PercentAboveAverage { get; set; }
        public string DistrictName { get; set; }
        public int? DistrictID { get; set; }
        public int? GenreType { get; set; }
        public int? EthnicType { get; set; }
        public int OrderCretia { get; set; }
    }
}
