﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExaminationBO
    {
        public int ExaminationsID { get; set; }
        public string ExaminationsName { get; set; }
        public int UnitID { get; set; }
        public int Year { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public System.DateTime DeadLine { get; set; }
        public string Note { get; set; }
        public string ExamCouncil { get; set; }
        public int AppliedType { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<System.DateTime> SyncTime { get; set; }
        public string SyncSourceId { get; set; }
        public MapSource<int> ListMapSource { get; set; }
    }
}
