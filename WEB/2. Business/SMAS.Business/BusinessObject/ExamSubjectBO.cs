﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamSubjectBO : IEquatable<ExamSubjectBO>
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public long ExamSubjectID { get; set; }
        public long ExaminationsID { get; set; }
        public string ExaminationsName { get; set; }
        public long ExamGroupID { get; set; }
        public string ExamGroupCode { get; set; }
        public string ExamGroupName { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string Abbreviation { get; set; }
        public string SchedulesExam { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int? OrderInSubject { get; set; }
        public int IsCommenting { get; set; }
        public string ScheduleContent { get; set; }

        public bool Equals(ExamSubjectBO other)
        {
            if (SubjectID == other.SubjectID)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return SubjectID.GetHashCode();
        }
    }
}
