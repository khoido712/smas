﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class AlertMessageBO
    {
        public int AlertMessageID { get; set; }
        public string Title { get; set; }
        public string FileUrl { get; set; }
        public System.DateTime PublishedDate { get; set; }
        public string ContentMessage { get; set; }
        public bool IsPublish { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public short TypeID { get; set; }
        public int CreatedUserID { get; set; }
        public int EmployyeID { get; set; }
        public bool isRead { get; set; }
        public string CreateUserName { get; set; }
        public string SendName { get; set; }
        public int? UnitID { get; set; }
        public string ShortFileUrl
        {
            get
            {
                try
                {
                    string[] arrName = FileUrl.Split('.');
                    string extension = arrName[arrName.Length - 1];
                    string res = FileUrl.Remove(FileUrl.LastIndexOf('_'));
                    res = res + "." + extension;
                    return res;
                }
                catch
                {
                    return String.Empty;
                }
            }
        }
    }
}
