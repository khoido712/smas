﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ViolatedInvigilatorBO
    {
        public int Index { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public bool Genre { get; set; }
        public string GenreTitle { get; set; }
        public string EducationLevelResolution { get; set; }
        public string SubjectName { get; set; }
        public string RoomTitle { get; set; }
        public string DisciplinedForm { get; set; }
        public string ViolationDetail { get; set; }

        public int InvigilatorID { get; set; }
        public int EducationLevelID { get; set; }
        public int ExaminationSubjectID { get; set; }
        public int ExaminationRoomID { get; set; }
    }
}
