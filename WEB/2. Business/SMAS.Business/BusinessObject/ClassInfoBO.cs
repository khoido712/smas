﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class ClassInfoBO
    {
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string ClassName { get; set; }
        public int? ClassOrderNumber { get; set; }
        public string TeacherName { get; set; }
        public int EducationLevelID { get; set; }
        public int NumOfPupil { get; set; }
        public int ActualNumOfPupil { get; set; }
        public bool? IsClassVNEN { get; set; }
    }
    public class PupilCountBO
    {
        public int ClassID { get; set; }
        public int PupilCount { get; set; }
    }
}
