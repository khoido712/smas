﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class PhysicalTestBO
    {
        public int? PhysicalTestID { get; set; }     
        public int MonitoringBookID { get; set; }     
        public DateTime? Date { get; set; }        
        public decimal? Height { get; set; }        
        public decimal? Weight { get; set; }        
        public decimal? Breast { get; set; }
        public int? PhysicalClassification { get; set; }              
        public int? Nutrition { get; set; }                        
        public DateTime? CreateDate { get; set; }
        public int PupilProFileID { get; set; }
        public PupilProfile PupilProFile { get; set; }
        public int ClassProFileID { get; set; }
        public ClassProfile ClassProFile { get; set; }
        public DateTime? ModifineDate { get; set; }
        public bool IsActive { get; set; }
        public int LogID { get; set; }
    }

    public class ValuePhysicalMonitoringBook
    {
        public int PupilProFileID { get; set; }
        public int MonitoringBookID { get; set; }     
    }

    public class ValueNutrition
    {
        public int PupilProFileID { get; set; }
        public int Nutrition { get; set; }
    }
}
