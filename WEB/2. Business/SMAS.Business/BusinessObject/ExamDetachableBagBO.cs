﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamDetachableBagBO
    {
        public string ExamineeNumber { get; set; }
        public string ExamDetachableBagCode { get; set; }
        public long ExamDetachableBagID { get; set; }
        public long ExamCandenceBagID { get; set; }
        public string ExamCandenceBagCode { get; set; }
        public long ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
    }
}
