﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
  public  class MineralOfDailyMenuBO
    {

        public decimal ProteinAnimal { get; set; }
        public decimal ProteinPlant { get; set; }
        public decimal FatAnimal { get; set; }
        public decimal FatPlant { get; set; }
        public decimal Sugar { get; set; }
        public List<FoodMineralMenuBO> lstFoodMineral { get; set; }
        public decimal Calo { get; set; }
        public decimal ProteinAnimalMin { get; set; }
        public decimal ProteinAnimalMax { get; set; }
        public decimal ProteinPlantMin { get; set; }
        public decimal ProteinPlantMax { get; set; }
        public decimal FatAnimalMin { get; set; }
        public decimal FatAnimalMax { get; set; }
        public decimal FatPlantMin { get; set; }
        public decimal FatPlantMax { get; set; }
        public decimal SugarMin { get; set; }
        public decimal SugarMax { get; set; }
        public decimal CaloMin { get; set; }
        public decimal CaloMax { get; set; }
        public int CountOfLstFoodMineral { get; set; }
    }
}
