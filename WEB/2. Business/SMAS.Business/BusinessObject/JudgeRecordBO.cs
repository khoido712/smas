using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class JudgeRecordBO
    {
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public long? JudgeRecordID { get; set; }
        public int? PupilID { get; set; }
        public int? ClassID { get; set; }
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public int? SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int? MarkTypeID { get; set; }
        public Nullable<int> Semester { get; set; }
        public string Judgement { get; set; }
        public string ReTestJudgement { get; set; }
        public int? OrderNumber { get; set; }
        public int? Status { get; set; }
        public string Title { get; set; }
        public string MarkTitle { get; set; }
        public bool EnableMark { get; set; }
        public bool DisplayMark { get; set; }
        public bool NotStudyingPupil { get; set; }
        public int? OrderInClass { get; set; }
        public bool StatusDisplay { get; set; }
        public int Coefficient { get; set; } 
        public string[] InterviewMark { get; set; }
        public string[] WritingMark { get; set; }
        public string[] TwiceCoeffiecientMark { get; set; }
        public string[] LogChangeM { get; set; }
        public string[] LogChangeP { get; set; }
        public string[] LogChangeV { get; set; }
        public string LogChangeHK { get; set; }
        public string SemesterMark { get; set; }
        public string TBMYearMark { get; set; }
        public int? EducationLevelID { get; set; }
        public int? AppliedLevel { get; set; }
        public bool? IsLegalBot { get; set; }
        public string ErrorDescription { get; set; }
        public bool? IsExempted { get; set; }
        public bool? HasSumUpSemester1 { get; set; }
        public DateTime? MarkedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<bool> IsOldData { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public string OldJudgement { get; set; }
        public string MJudgement { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }
    }
}
