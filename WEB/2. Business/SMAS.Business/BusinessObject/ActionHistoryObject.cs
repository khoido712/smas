﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ActionHistoryObject
    {
        public int ActionAuditID { get; set; }
        public DateTime ActionDate { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string FunctionName { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public string IP { get; set; }
    }
}
