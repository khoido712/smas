﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class RestoreDataDetailBO
    {
        public long RESTORE_DATA_DETAIL_ID { get; set; }
        public System.Guid RESTORE_DATA_ID { get; set; }
        public int SCHOOL_ID { get; set; }
        public int ACADEMIC_YEAR_ID { get; set; }
        public int RESTORE_DATA_TYPE_ID { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public Nullable<System.DateTime> END_DATE { get; set; }
        public int LAST_2DIGIT_NUMBER_SCHOOL { get; set; }
        public string SQL_UNDO { get; set; }
        public string SQL_DELETE { get; set; }
        public string TABLE_NAME { get; set; }
        public int ORDER_ID { get; set; }
        public int IS_VALIDATE { get; set; }


        /// <summary>
        /// Dung de kiem tra ma da ton tai trong he thong
        /// phuc hoi user: userName, phuc hoi truong, schoolCode, hs: pupilcode
        /// </summary>
        public string VALIDATE_CODE { get; set; }
        public string USER_ID { get; set; }
        /// <summary>
        /// ID tuong ung voi validate_code
        /// </summary>
        public int KEY_VALUE { get; set; }

        //public string SQL_RESTORE { get; set; }

        /// <summary>
        /// Code thay the khi da ton tai
        /// </summary>
        public string REPLACE_CODE { get; set; }

        /// <summary>
        /// Luu thong tin de phuc hoi hoc sinh
        /// </summary>
        public PupilOfClass PupilOfClass { get; set; }

        public MarkRecord MarkRecord { get; set; }

        public JudgeRecord JudgeRecord { get; set; }

        public SummedUpRecord SummedUpRecord { get; set; }

        public TeacherNoteBookMonth TeacherNoteBookMonth { get; set; }
        public TeacherNoteBookSemester TeacherNoteBookSemester { get; set; }

        public RatedCommentPupil RatedCommentPupil { get; set; }


    }
}
