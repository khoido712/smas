﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ManagementHealthInsuranceBO
    {
        public int managementHealthInsuranceID { get; set; }
        public int schoolID { get; set; }
        public int academicYearID { get; set; }
        public int classID { get; set; }
        public int pupilID { get; set; }
        public string cardNumber { get; set; }
        public string address { get; set; }
        public string registerAddress { get; set; }
        public string cardCode { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public DateTime dateRange { get; set; }

        public string placeRange { get; set; }
        public DateTime? modifiedDate { get; set; }
        public DateTime createDate 
        {
            get { return DateTime.Now; }

        }

    }
}
