﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DetailContractReportBO
    {
        public int Order { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }

        public string CustomerName { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }

        public string CustomerUserName { get; set; }

        public DateTime CreatedDate { get; set; }
        public string RoamingMode { get; set; }

        public string MainRoamingCode { get; set; }

        public string ExtraRoamingCode { get; set; }

        public int? UnitID { get; set; }
    }
}
