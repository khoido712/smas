using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilRankingBO
    {
        public DateTime? AssignedDate { get; set; }
        public long? PupilRankingID { get; set; }
        /// <summary>
        /// Thứ tự môn học
        /// </summary>
        public int? OrderInSubject { get; set; }
        /// <summary>
        /// xếp hạng
        /// </summary>
        public int? Rank { get; set; }

        /// <summary>
        /// ID hoc sinh
        /// </summary>
        public int? PupilID { get; set; }

        /// <summary>
        /// Ma hoc sinh
        /// </summary>
        public string PupilCode { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// Ten hoc sinh
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// gioi tinh
        /// </summary>
        public int? Genre { get; set; }

        /// <summary>
        /// Gioi tinh hien thi (Nam/Nu)
        /// </summary>
        public string GenreDetail { get; set; }
        /// <summary>
        /// Trang thai cua hoc sinh
        /// </summary>
        public int? ProfileStatus { get; set; }

        /// <summary>
        /// Ngay sinh cua hoc sinh
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Khoi hoc
        /// </summary>
        public int? EducationLevelID { get; set; }

        /// <summary>
        /// Dang ky VNEN
        /// </summary>
        public bool? IsVNEN { get; set; }

        /// <summary>
        /// Truong hoc
        /// </summary>
        public int? SchoolID { get; set; }

        /// <summary>
        /// Nam hoc
        /// </summary>
        public int? AcademicYearID { get; set; }

        /// <summary>
        /// Hoc ky
        /// </summary>
        public int? Semester { get; set; }

        /// <summary>
        ///Lớp học
        /// </summary>
        public int? ClassID { get; set; }
        /// <summary>
        ///Mã Môn hoc
        /// </summary>
        public int? SubjectID { get; set; }
        /// <summary>
        /// Tên môn học
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// Hoc luc cua hoc sinh cap 1
        /// </summary>
        public int? CapacityType { get; set; }

        /// <summary>
        /// Nhan xet va danh gia
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// So ngay nghi co phep
        /// </summary>
        public int? TotalAbsentDaysWithPermission { get; set; }

        /// <summary>
        /// So ngay nghi khong co phep
        /// </summary>
        public int? TotalAbsentDaysWithoutPermission { get; set; }

        /// <summary>
        /// Tong so ngay nghi
        /// </summary>
        public int? ToTalAbsentDay { get; set; }
        /// <summary>
        /// Diem cong hanh kiem
        /// </summary>
        public decimal? sumConductMark { get; set; }

        /// <summary>
        /// Diem tru hanh kiem
        /// </summary>
        public decimal? sumConductMinusMark { get; set; }

        /// <summary>
        /// Diem tru vi pham
        /// </summary>
        public decimal? sumTotalPenalizedMark { get; set; }
              
        /// <summary>
        ///Điểm trung bình từng môn
        /// </summary>
        public decimal? SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        /// <summary>
        /// Tong diem
        /// </summary>
        public decimal? sumMark { get; set; }

        public int? Task1Evaluation { get; set; }
        public int? Task2Evaluation { get; set; }
        public int? Task3Evaluation { get; set; }
        public int? Task4Evaluation { get; set; }
        public int? Task5Evaluation { get; set; }

        /// <summary>
        /// Hoc luc cua hoc sinh (id)
        /// </summary>
        public int? CapacityLevelID { get; set; }

        /// <summary>
        /// Hoc luc cua hoc sinh (name)
        /// </summary>
        public string CapacityLevel { get; set; }

        /// <summary>
        /// hanh kiem cua hoc sinh (id)
        /// </summary>
        public int? ConductLevelID { get; set; }

        public int? NumberOfFault { get; set; }

        /// <summary>
        /// Tong diem
        /// </summary>
        public decimal? TotalMark { get; set; }
        public int? PeriodID { get; set; }

        /// <summary>
        /// hien thi hay disable
        /// </summary>
        public bool? Enable { get; set; }

        public int? HonourAchivementTypeID { get; set; }

        public string HonourAchivementTypeResolution { get; set; }

        /// <summary>
        /// danh hieu hoc sinh
        /// </summary>
        public int? PupilEmulationID { get; set; }
        public string PupilEmulationResolution { get; set; }
        /// <summary>
        ///hoc sinh thuoc dien
        /// </summary>
        public int? StudyingJudgementID { get; set; }

        public string StudyingJudgementResolution { get; set; }

        /// <summary>
        /// Thu tu trong lop
        /// </summary>
        public int? OrderInClass { get; set; }

        public string ConductLevelName { get; set; }

        public DateTime? EndDate { get; set; }

        public int? Year { get; set; }

        public bool IsAccepted { get; set; }

        /// <summary>
        ///Điểm trung bình các môn
        /// </summary>
        public decimal? AverageMark { get; set; }        

        /// <summary>
        /// Tên lớp học
        /// </summary>
        public string ClassName { get; set; }


        /// <summary>
        /// Tên lớp học
        /// </summary>
        public int? ClassOrderNumber { get; set; }

        /// <summary>
        /// Hạnh kiểm
        /// </summary>
        public string ConductLevel { get; set; }

        /// <summary>
        /// Tên môn học
        /// </summary>
        public string SubjectNameRetest { get; set; }

        public int? IsCommenting { get; set; }
        public string Mobile { get; set; }
        /// <summary>
        /// Ngày đăng ký thi lại
        /// </summary>
        public DateTime? RegisteredDate { get; set; }
        public string ColorDisplay { get; set; }
        public int Age { get; set; }

        public bool ShowMark { get; set; }

        /// <summary>
        /// Trang thai cua hoc sinh trong lop
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Hinh thuc hoc
        /// </summary>
        public int? LearningType { get; set; }

        /// <summary>
        /// Thuoc dien XLHK hay khong
        /// true : Không thuộc diện xếp loại hạnh kiểm
        /// false : Thuộc diện xếp loại hạnh kiểm
        /// </summary>
        public bool IsKXLHK { get; set; }

        public bool IsShow { get; set; }
        public bool IsCategory { get; set; }
        public int? EthnicID { get; set; }
        public string EthnicName { get; set; }
        //Hoc sinh khuyet tat
        public bool? IsDisabled { get; set; }
        public int? SchoolTypeID { get; set; }
        public int? SubCommiteeID { get; set; }

        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }
        public string StatusName { get; set; }
        public int? SectionKeyID { get; set; }
        public bool? IsBackTrain { get; set; }
    }
}
