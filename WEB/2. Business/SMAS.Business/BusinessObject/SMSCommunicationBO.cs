﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SMSCommunicationBO
    {
        public IEnumerable<string> ListSubcriber { get; set; }

        public long SMSParentContractID { get; set; }

        public long? HistoryID { get; set; }

        public long SMSCommunicationID { get; set; }

        public int ReceiverID { get; set; }

        public int SenderID { get; set; }

        public long SenderGroup { get; set; }

        public string SenderGroupStringType { get; set; }

        public string Content { get; set; }

        public int? ContentCount { get; set; }

        public string ShortContent { get; set; }                      

        public int? ParentSender { get; set; }

        public int? ParentReceiver { get; set; }

        public int? TeacherSender { get; set; }

        public int? TeacherReceiver { get; set; }

        public string ParentReceiverName { get; set; }

        public string ParentSenderName { get; set; }

        public string TeacherReceiverName { get; set; }

        public string TeacherSenderName { get; set; }

        public DateTime CreateDate { get; set; }

        public int? Type { get; set; }

        public int? TypeHistory { get; set; }

        public string ContactGroupName { get; set; }

        public int? ContactGroupID { get; set; }

        public bool IsRead { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsDefaultGroup { get; set; }

        public bool IsSMS { get; set; }

        public int NumUnReadGroup { get; set; }

        public int NumToSendGroup { get; set; }

        public int SchoolID { get; set; }

        public int PupilID { get; set; }

        public string ClassName { get; set; }


        public int OrderNumber { get; set; }

        public int OrderInClass { get; set; }

        public string PupilFileName { get; set; }

        public string Subscriber { get; set; }

        public string ContractorName { get; set; }

        public string PhoneMainContract { get; set; }

        public string SMSType { get; set; }

        public string UserName { get; set; }

        //For Report        
        public int TotalActive { get; set; }

        public int TotalManual { get; set; }

        public int TotalParent { get; set; }

        public int TotalReceived { get; set; }

        public string Status { get; set; } // trang thai 1: gui thanh cong, 0: Gui khong thanh cong, null: chua gui

        public int HeadTeacherSenderID { get; set; }

        public int ClassID { get; set; }

        public int NumberOrderClass { get; set; }

        public int EducaitonLevelID { get; set; }

        public string EmployeeShortName { get; set; }
        public int SmsCount { get; set; }

        public bool? IsOnSchedule { get; set; }

        public DateTime SendTime { get; set; }
    }
}
