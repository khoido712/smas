﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RestoreUserBO
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string ReplaceUser { get; set; }
        public string SqlUndo { get; set; }
    }
}
