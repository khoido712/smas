﻿using System ;
using System.Collections.Generic ;
using System.Linq ;
using System.Text ;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class InputStatistics
    {
        public int? DistrictID {get;set;}
        public string DistrictName { get; set; }
        public int SchoolID {get;set;}
        public string SchoolName { get; set; }
        public int EducationLevelID {get;set;}
        public string EducationLevelName { get; set; }
        public int GradeID { get; set; }
        public string ListGrade { get; set; }
        public int NumberOfPupil {get;set;}
        public int NumberOfTeacher {get;set;}
        public int NumberOfSchool {get;set;}
        public int NumberOfSchoolHasData {get;set;}
        public int NumberOfClass {get;set;}
        public int NumberOfClassHasSubject {get;set;}
        public int NumberOfClassHasAssignForFirstSemester {get;set;}
        public int NumberOfClassHasAssignForSecondSemester {get;set;}
        public int NumberOfClassHasMarkForFirstSemester {get;set;}
        public int NumberOfClassHasMarkForSecondSemester {get;set;}
        public int NumberOfClassHasConductForFirstSemester {get;set;}
        public int NumberOfClassHasConductForSecondSemester {get;set;}
        public int NumberOfClassHasSumMarkForFirstSemester {get;set;}
        public int NumberOfClassHasSumMarkForSecondSemester {get;set;}
        public int NumberOfUser {get;set;}
        public int NumberOfLoggedUser {get;set;}
        public int NumberOfPupilHasMobile {get;set;}
        public int NumberOfPupilHasMotherMobile {get;set;}
        public int NumberOfPupilHasFatherMobile {get;set;}

    }

    public class ReportInputMonitorBO
    {
        /// <summary>
        /// id truong
        /// </summary>
        public int SchoolID { get; set; }

        /// <summary>
        /// ten truong
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// id quan/huyen
        /// </summary>
        public int DistrictID { get; set; }

        /// <summary>
        /// quan/huyen
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// id cap hoc
        /// </summary>
        public int GradeID { get; set; }

        /// <summary>
        /// ten cap hoc
        /// </summary>
        public string GradeName { get; set; }

        /// <summary>
        /// so luong truong
        /// </summary>
        public int TotalSchool { get; set; }

        /// <summary>
        /// so luong truong da nhap lieu
        /// </summary>
        public int TotalSchoolInput { get; set; }

        /// <summary>
        /// so hoc sinh
        /// </summary>
        public int TotalPupil { get; set; }

        /// <summary>
        /// so giao vien
        /// </summary>
        public int TotalTeacher { get; set; }

        /// <summary>
        /// so lop hoc
        /// </summary>
        public int TotalClass { get; set; }

        /// <summary>
        /// so lop khai bao mon hoc
        /// </summary>
        public int TotalClassSubject { get; set; }    

        /// <summary>
        /// so lop da phan cong giang day HK1
        /// </summary>
        public int TotalClassAssignment1 { get; set; }

        /// <summary>
        /// so lop da phan cong giang day HK2
        /// </summary>
        public int TotalClassAssignment2 { get; set; }        

        /// <summary>
        /// so lop da nhap diem HK1
        /// </summary>
        public int TotalClassMark1 { get; set; }

        /// <summary>
        /// so lop da nhap diem HK2
        /// </summary>
        public int TotalClassMark2 { get; set; }

        /// <summary>
        /// so lop da nhap hanh kiem HK1
        /// </summary>
        public int TotalClassConduct1 { get; set; }

        /// <summary>
        /// so lop da nhap hanh kiem HK2
        /// </summary>
        public int TotalClassConduct2 { get; set; }
        
        /// <summary>
        /// so lop da tong ket HK1
        /// </summary>
        public int TotalClassSummedUp1 { get; set; }

        /// <summary>
        /// so lop da tong ket HK2
        /// </summary>
        public int TotalClassSummedUp2 { get; set; }

        /// <summary>
        /// so user da cap
        /// </summary>
        public int TotalUser { get; set; }

        /// <summary>
        /// so user da cap dang nhap he thong
        /// </summary>
        public int TotalUserLogin { get; set; }

        /// <summary>
        /// so hoc sinh da nhap sdt ca nhan
        /// </summary>
        public int TotalPupilMobile { get; set; }

        /// <summary>
        /// so hoc sinh da nhap sdt phu huynh
        /// </summary>
        public int TotalFamilyMobile { get; set; }

        /// <summary>
        /// so hoc sinh da nhap sdt phu huynh cha
        /// </summary>
        public int TotalFamilyMobileF { get; set; }

        /// <summary>
        /// so hoc sinh da nhap sdt phu huynh me
        /// </summary>
        public int TotalFamilyMobileM { get; set; }
    }
}
