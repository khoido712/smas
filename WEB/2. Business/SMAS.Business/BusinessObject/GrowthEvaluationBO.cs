﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class GrowthEvaluationBO
    {
        public int GrowthEvaluationID { get; set; }

        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int PupilID { get; set; }

        public int DeclareEvaluationGroupID { get; set; }
        public int DeclareEvaluationIndexID { get; set; }
        public int? EvaluationType { get; set; }
        public string EvaluationTypeStr { get; set; }
        public int? LogID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public int EvaluationDevGroupID { get; set; }
        public int EvaluationDevID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string PupilName { get; set; }
        public string PupilCode { get; set; }

        public string Error { get; set; }
        public List<ListError> ListErrorMessage { get; set; }
    }

    public class ListError
    {
       // public string Error { get; set; }
        public string PupilCode { get; set; }
        public string PupilName { get; set; }
        public string Description { get; set; }
        public string SheetName { get; set; }
    }
}
