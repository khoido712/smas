﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportSituationOfViolationBO
    {
        public int AcademicYearID{get;set;}
        public int SchoolID { get; set; }
        public int EducationLevelID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int AppliedLevel { get; set; }
    }
}
