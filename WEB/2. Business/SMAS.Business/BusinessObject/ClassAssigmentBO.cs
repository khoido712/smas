﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ClassAssigmentBO
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int ClassAssigmentID { get; set; }
        public string TeacherName { get; set; }
        public string AssignmentWork { get; set; }
        public string EducationLevelName { get; set; }
        public int EducationLevelID { get; set; }
        public int? OrderClass { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ToDate { get; set; }
        public Nullable<bool> IsTeacher { get; set; }
        public bool IsHeadTeacher { get; set; }
        public int TeacherID {get;set;}
    }
}
