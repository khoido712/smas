﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RestoreSchoolProfileBO
    {
        public int SchoolProfileId { get; set; }
        public string SchoolCode { get; set; }
        /// <summary>
        /// Code thay the khi phuc hoi(neu co)
        /// </summary>
        public string ReplaceSchoolCode { get; set; }

        /// <summary>
        /// Cau lenh phuc hoi
        /// </summary>
        public string SqlUndo { get; set; }
    }
}
