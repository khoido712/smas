using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessExtensions
{
   public class HonourAchivementBO
    {
        public int HonourAchivementID { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int HonourAchivementTypeID { get; set; }
        public int? EmployeeID { get; set; }
        public DateTime? AchivedDate { get; set; }
        public string Description { get; set; }
        public string FacultyName { get; set; }
        public string Resolution { get; set; }
        public string EmployeeCode { get; set; }
        public string FullName { get; set; }
        public int? FacultyID { get; set; }
    }
}
