﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class CaptchaBO
    {
         //static string[] fonts = { "Arial", "Verdana", "Times New Roman", "Tahoma" };
        //const string chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";

        /// <summary>
        /// Giá trị của captcha
        /// </summary>
        public string Value
        {
            get;
            set;
        }

        /// <summary>
        /// Captcha dạng ảnh. Sử dụng ảnh này để hiển thị trên trang web.
        /// </summary>
        public Image Image
        {
            get
            {
                Image img = this.createImageFromString(this.Value, this.settings);
                return img;
            }
        }

        private Settings settings;

        /// <summary>
        /// Tạo captcha có giá trị là một chuỗi ngẫu nhiên.
        /// </summary>
        public CaptchaBO()
        {
            this.settings = new Settings();
            this.Value = this.getRandomString(this.settings);
        }

        /// <summary>
        /// Tạo captcha với giá trị được chỉ ra
        /// </summary>
        /// <param name="value">Giá trị của captchar sẽ được tạo ra.</param>
        public CaptchaBO(string value)
        {
            this.settings = new Settings();
            this.Value = value;
        }

        /// <summary>
        /// Tạo captcha với giá trị được chỉ ra
        /// </summary>
        /// <param name="value">Giá trị của captchar sẽ được tạo ra.</param>
        public CaptchaBO(Settings settings)
        {
            this.settings = settings;
            this.Value = this.getRandomString(settings);
        }

        /// <summary>
        /// Tạo captcha với giá trị được chỉ ra
        /// </summary>
        /// <param name="value">Giá trị của captchar sẽ được tạo ra.</param>
        public CaptchaBO(string value, Settings settings)
        {
            this.settings = settings;
            this.Value = value;
        }
        
        /// <summary>
        /// Tao mot xau ngau nhien bao gom chu so, chu in hoa, chu in thuong
        /// </summary>
        /// <returns></returns>
        private string getRandomString(Settings settings)
        {
            Random rand = new Random();
            int LENGTH = rand.Next(6,6);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < LENGTH; i++)
            {
                // Lấy kí tự ngẫu nhiên từ mảng chars
                string str = settings.chars[rand.Next(settings.chars.Length)].ToString();
                builder.Append(str);
            }
            return builder.ToString();
        }

        private Image createImageFromString(string text, Settings settings)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new Exception("Invalid text. Text must not be null or whitespace.");
            }
            Random rand = new Random(126);
            Image img = new Bitmap(settings.Width, settings.Height);
            // chuỗi để lấy các kí tự sẽ sử dụng cho captcha
            using (Graphics g = Graphics.FromImage(img))
            {
                // Tạo nền cho ảnh dạng sóng
                HatchBrush brush = new HatchBrush(HatchStyle.LightDownwardDiagonal, Color.Wheat, Color.Silver);
                g.FillRegion(brush, g.Clip);
                // Lưu chuỗi captcha trong quá trình tạo
                //StringBuilder strCaptcha = new StringBuilder();
                for (int i = 0; i < text.Length; i++)
                {
                    // Lấy kí tự từ text
                    string str = text[i].ToString();
                    // Tạo font với tên font ngẫu nhiên chọn từ mảng fonts
                    Font font = new Font(settings.fonts[rand.Next(settings.fonts.Length)], 14, FontStyle.Italic | FontStyle.Bold);
                    // Lấy kích thước của kí tự
                    SizeF size = g.MeasureString(str, font);
                    // Vẽ kí tự đó ra ảnh tại vị trí tăng dần theo i, vị trí top ngẫu nhiên
                    PointF upperleftCorner = new PointF(i * 20 + 3, rand.Next(0, settings.Height/6));
                    g.DrawString(str, font, Brushes.Blue, upperleftCorner);
                    font.Dispose();
                }
            }
            return img;
        }

        /// <summary>
        /// Captcha settings
        /// </summary>
        public class Settings
        {
            /// <summary>
            /// Do cao cua captcha tao ra.
            /// Mac dinh: 24px
            /// </summary>
            public int Height = 24;
            /// <summary>
            /// Do rong cua captcha tao ra
            /// Mac dinh: 130px
            /// </summary>
            public int Width = 130;
            /// <summary>
            /// Tap hop cac fonts su dung.
            /// Mac dinh bao gom: { "Arial", "Verdana", "Times New Roman", "Tahoma" }
            /// </summary>
            public string[] fonts = { "Arial", "Verdana", "Times New Roman", "Tahoma" };
            /// <summary>
            /// Tap hop cac ky tu co the co trong captcha.
            /// Mac dinh bao gom: "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz"
            /// </summary>
            public string chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
        }
    }
}
