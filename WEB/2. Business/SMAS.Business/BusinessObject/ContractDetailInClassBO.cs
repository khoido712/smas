﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ContractDetailInClassBO
    {
        public int SMSParentContractID { get; set; }

        public long SMSParentContractDetailID { get; set; }

        public int SchoolID { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int Semester { get; set; }

        public int AcademicYearID { get; set; }

        public int? ServicePackageID { get; set; }

        public int? MaxSMS { get; set; }
    }
}
