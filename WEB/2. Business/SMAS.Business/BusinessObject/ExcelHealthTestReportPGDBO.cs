﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class ExcelHealthTestReportPGDBO
    {

       public int Year { get; set; }
       public int HealthPeriod { get; set; }
       public int AppliedLevel { get; set; }
       public int SupervisingDeptID { get; set; }
       public int DistrictID { get; set; }
       public int AcademicYearID { get; set; }

    }
}
