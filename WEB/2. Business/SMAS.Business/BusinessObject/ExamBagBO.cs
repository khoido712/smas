﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamBagBO
    {
        public long ExamBagID { get; set; }
        public string ExamBagCode { get; set; }
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
        public int SubjectID { get; set; }
    }
}
