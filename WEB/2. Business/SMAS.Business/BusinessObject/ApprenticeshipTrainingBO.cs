﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ApprenticeshipTrainingBO
    {
        public string ClassName { get; set; }
        public int? ApprenticeshipClassID { get; set; }
        public string ApprenticeshipClassName { get; set; }
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Genre { get; set; }
        public string BirthPlace { get; set; }
        public string ApprenticeshipSubjectName { get; set; }
        public decimal? SummedUpMark { get; set; }
        public string Category { get; set; }
    }
}
