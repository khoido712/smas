﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DetailDeferredPaymentReportBO
    {
        public int Order { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }

        public string SchoolName { get; set; }

        public string SchoolUserName { get; set; }
        public string ServicePackage { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? MaturityDate { get; set; }

        public int? NumOfParentUnpaid { get; set; }

        public decimal? SumMoneyUnpaid { get; set; }
        public decimal? SumMoneyPaidOnTime { get; set; }

        public decimal? SumMoneyPaidOverdue { get; set; }

        public int? NumOfParentOverdue { get; set; }

        public int? NumOfInSMSOverdueSubscriber { get; set; }

        public int? NumOfExSMSOverdueSubscriber { get; set; }

        public decimal? SumMoneyUsedforSMS { get; set; }
    }
}
