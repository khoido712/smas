﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PointReportBO
    {
        public int EducationLevelID { get; set; }
        public int ClassID { get; set; }
        public int Semester { get; set; }
        public int PeriodDeclarationID { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int PaperSize { get; set; }
        public int AppliedLevel { get; set; }
        public int UserAccountID { get; set; }
        public bool IsSubSuperVisingDept { get; set; }
        public bool IsSuperVisingDept { get; set; }
        public int ReportPatternID { get; set; }
    }

    public class LevelOfPupilInExamBO
    {
        public long ExaminationID { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public decimal? LevelInExamination { get; set; }
    }

    public class SetHeightBO
    {       
        public int Row { get; set; }
        public double Height { get; set; }
    }
}
