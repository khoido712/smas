﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MovementAcceptanceBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public DateTime? MovedDate { get; set; }
        public string DisplayName { get; set; }
        public int? OrderNumber { get; set; }
        public int? MovedFromSchoolID { get; set; }
        public string SchoolName { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Description { get; set; }
        public int Genre { get; set; }
        public int EducationLeveID { get; set; }
    }
}
