﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilRewardReportBO
    {
        public int? EducationLevelID { get; set; }
        public int? ClassID { get; set; }
        public int? Semester { get; set; }
        public int? AcademicYearID { get; set; }
        public int? SchoolID { get; set; }
        public int? AppliedLevel { get; set; }
        public int UserAccountID { get; set; }
        public bool IsSubSuperVisingDept { get;set;}
        public bool IsSuperVisingDept { get; set; }
        public bool chkChecked { get; set; }
    }
}
