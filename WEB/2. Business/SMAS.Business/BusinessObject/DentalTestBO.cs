﻿using System;

namespace SMAS.Business.BusinessObject
{
    public class DentalTestBO
    {
        public int? MonitoringBookID { get; set; }
        public DateTime? MonitoringBookDate { get; set; }
        public int? DentalTestID { get; set; }
        public int? HealthPeriodID { get; set; }
        public string UnitName { get; set; }
        public Nullable<int> NumTeethExtracted { get; set; }
        public string DescriptionTeethExtracted { get; set; }
        public int? NumDentalFilling { get; set; }
        public string DescriptionDentalFilling { get; set; }
        public Nullable<int> NumPreventiveDentalFilling { get; set; }
        public string DescriptionPreventiveDentalFilling { get; set; }
        public bool? IsScraptTeeth { get; set; }
        public bool? IsTreatment { get; set; }
        public string Other { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public bool? IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool? EnableAction { get; set; }
        public int PupilProFileID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifineDate { get; set; }
        public int LogID { get; set; }
        public string NumDentalFillingOne { get; set; }
        public string NumDentalFillingTwo { get; set; }
        public string NumDentalFillingThree { get; set; }
        public string NumDentalFillingFour { get; set; }
        public bool IsScraptTeethCustom { get; set; }
        public bool IsTreatmentCustom { get; set; }
        public int? NumDentalFillingCustom { get; set; }
        public int ClassID { get; set; }

    }
}
