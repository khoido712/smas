﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportEmployeeProfileBO
    {
        public DateTime ReportDate { get; set; }
        public int SchoolID { get; set; }
        public int FacultyID { get; set; }
        public int? Compare { get; set; } // 1: >=; 2: <=;3:=
        public int? SeniorYear { get; set; }
        public int AppliedLevel { get; set; }
    }
}
