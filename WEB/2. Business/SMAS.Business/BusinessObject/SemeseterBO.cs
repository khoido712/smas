﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SemeseterBO
    {
        public string semesterName { get; set; }
        public int semesterId { get; set; }
    }
}
