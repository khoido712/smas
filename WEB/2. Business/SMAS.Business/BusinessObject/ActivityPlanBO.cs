using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ActivityPlanBO
    {
        public int ActivityPlanID { get; set; }
        public System.DateTime PlannedDate { get; set; }
        public string ActivityPlanName { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public System.TimeSpan FromTime { get; set; }
        public System.TimeSpan ToTime { get; set; }
        public string CommentOfTeacher { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int? ClassID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? AppliedLevel { get; set; }
        public int ActivityID { get; set; }
        public string ClassName { get; set; }
    }
}
