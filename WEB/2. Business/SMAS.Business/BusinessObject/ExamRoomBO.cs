﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamRoomBO
    {
        public long ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
        public string ExamGroupName { get; set; }
    }
}
