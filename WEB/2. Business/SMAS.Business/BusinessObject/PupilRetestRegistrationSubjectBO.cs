using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class PupilRetestRegistrationSubjectBO
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string PupilName { get; set; }
        public string ClassName { get; set; }
        public int ClassID { get; set; }
        public string JudgementResult { get; set; }
        public decimal? SummedUpMark { get; set; }
        public int IsCommenting { get; set; }
        public int PupilRetestRegistrationID { get; set; }
        public int? Semester { get; set; }
        public string ReTestJudgement { get; set; }
        public decimal? ReTestMark { get; set; }
        public DateTime? ModifiedDatePRR { get; set; }
        public long SummedUpRecordID { get; set; }
        public string Comment { get; set; }
        public int AcademicYearID { get; set; }
        public int? PeriodID { get; set; }
        public int SchoolID { get; set; }
        public int SubjectID { get; set; }
        public int Year { get; set; }
        public bool? isAbsentContest { get; set; }
        public string Reason { get; set; }
        // Quanglm add
        public DateTime? SummedUpDate { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public int? ClassOrderNumber { get; set; }
        public int EducationLevelID { get; set; }
    }
}
