﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public partial class ConductStatisticsOfProvinceGroupByLevel23BO
    {
        public int? EducationLevelID { get; set; } 
        public string EducationLevelName { get; set; }
        public int? TotalGood { get; set; }
        public int? TotalFair { get; set; }
        public int? TotalNormal { get; set; }
        public int? TotalWeak { get; set; }
        public int? TotalPupil { get; set; }
        public int? TotalAboveAverage { get; set; }
        public double? PercentGood { get; set; }
        public double? PercentFair { get; set; }
        public double? PercentNormal { get; set; }
        public double? PercentWeak { get; set; }
        public double? PercentAboveAverage { get; set; }
    }
}
