﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class Transcripts
    {
        public int AppliedLevel { get; set; }
        public int PupilID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int TrainingType { get; set; }
        public int currentYear { get; set; }
        public int paging { get; set; }
    }
}
