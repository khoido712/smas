﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSTypeBO
    {
        public int TypeID { get; set; }

        public string TypeCode { get; set; }

        public string Name { get; set; }

        public bool IsCustom { get; set; }

        public bool IsForVNEN { get; set; }

        public int PeriodType { get; set; }

        public int OrderID { get; set; }
    }
}
