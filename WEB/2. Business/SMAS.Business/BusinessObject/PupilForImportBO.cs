using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// HieuND : edit class nay co the anh huong den ham group trong chuc nang ImportSemesterTestMark
    /// </summary>
    public class PupilForImportBO
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int? ClassOrderNumber { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int? SubjectID { get; set; }
        public int? MarkTypeID { get; set; }
        public int? Year { get; set; }
        public int? Semester { get; set; }
        public int? PeriodID { get; set; }
        public string Title { get; set; }
        
        public long? MarkRecordID { get; set; }
        public long? JudgeRecordID { get; set; }

        public decimal? Mark { get; set; }
        public int? OrderNumber { get; set; }
        public DateTime? MarkedDate { get; set; }

        public string Judgement { get; set; }
        public string ReTestJudgement { get; set; }

        public int? IsCommenting { get; set; }

        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Hoc sinh duoc mien giam mon hoc nay
        /// </summary>
        public bool IsExempted { get; set; }
    }
}
