﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportPupilFaultBO
    {
        public int? PupilID { get; set; }
        public decimal? NewField { get; set; }
        public DateTime? ViolateDate { get; set; }
        public int ClassID { get; set; }
        public int FaultID { get; set; }
		public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
    }
}
