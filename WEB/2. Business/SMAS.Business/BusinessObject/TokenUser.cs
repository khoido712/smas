﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Business.BusinessObject
{
    public class TokenUser
    {
        public string code { get; set; }
        public string fullName { get; set; }
        public string email { get; set; }
    }
}