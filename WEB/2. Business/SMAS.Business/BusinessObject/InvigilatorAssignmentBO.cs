﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class InvigilatorAssignmentBO
    {
        public int InvigilatorAssignmentID { get; set; }
        public string SubjectNameRoom { get; set; }
    }
}
