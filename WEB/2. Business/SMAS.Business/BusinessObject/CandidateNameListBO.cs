﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class CandidateNameListBO : IComparable<CandidateNameListBO>, IEqualityComparer<CandidateNameListBO>
    {
        public Candidate Candidate { get; set; }
        
        public int PupilProfileID { get; set; }
        
        public string FullName { get; set; }
        
        public string Name { get; set; }
        
        public DateTime Birthdate { get; set; }
        
        public string EthnicCode { get; set; }

        public int ClassID { get; set; }

        public string ClassName { get; set; }

        public int EducationLevel { get; set; }

        public int? OrderOfClassInEducationLevel { get; set; }

        public int CompareTo(CandidateNameListBO other)
        {
            return CompareInName(this, other);
        }

        public bool Equals(CandidateNameListBO c1, CandidateNameListBO c2)
        {
            return c1.PupilProfileID == c2.PupilProfileID;
        }

        public bool Equals(CandidateNameListBO other)
        {
            if (other == null)
            {
                return false;
            }
            return this.PupilProfileID.Equals(other.PupilProfileID);
        }

        public int GetHashCode(CandidateNameListBO obj)
        {
            return obj.PupilProfileID.GetHashCode();
        }

        /// <summary>
        /// Sap xep uu tien theo Class
        /// </summary>
        /// <param name="candidate1"></param>
        /// <param name="candidate1"></param>
        /// <returns></returns>
        public static int CompareInClass(CandidateNameListBO candidate1, CandidateNameListBO candidate2)
        {
            int compareInEducationLevel = candidate2.EducationLevel - candidate1.EducationLevel;
            if (compareInEducationLevel != 0) return compareInEducationLevel;
            int compareInClassOrderInEductionLevel;
            if (!candidate1.OrderOfClassInEducationLevel.HasValue)
            {
                compareInClassOrderInEductionLevel = !candidate2.OrderOfClassInEducationLevel.HasValue ? 0 : 1;
            }
            else
            {
                compareInClassOrderInEductionLevel = !candidate2.OrderOfClassInEducationLevel.HasValue 
                    ? -1 : candidate2.OrderOfClassInEducationLevel.Value - candidate1.OrderOfClassInEducationLevel.Value;
            }
            if (compareInClassOrderInEductionLevel != 0) return compareInClassOrderInEductionLevel;
            int compareInClassName = string.Compare(candidate1.ClassName, candidate2.ClassName);
            if (compareInClassName != 0) return compareInClassName;
            return CompareInName(candidate1, candidate2);
        }
        /// <summary>
        /// Sap xep uu tien theo ten
        /// </summary>
        /// <param name="candidate1"></param>
        /// <param name="candidate2"></param>
        /// <returns></returns>
        public static int CompareInName(CandidateNameListBO candidate1, CandidateNameListBO candidate2)
        {
            string candidate1NameForOrder = SMAS.Business.Common.Utils.SortABC(candidate1.FullName.getOrderingName(candidate1.EthnicCode));
            string candidate2NameForOrder = SMAS.Business.Common.Utils.SortABC(candidate2.FullName.getOrderingName(candidate2.EthnicCode));
            int compareInName = string.Compare(candidate1NameForOrder, candidate2NameForOrder);
            if (compareInName != 0) return compareInName;
            int compareInBirthdate = DateTime.Compare(candidate1.Birthdate, candidate1.Birthdate);
            return compareInBirthdate;
        }
    }
}
