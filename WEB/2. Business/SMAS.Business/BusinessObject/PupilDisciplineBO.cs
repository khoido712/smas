using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class PupilDisciplineBO
    {
        public int PupilDisciplineID { get; set; }
        public int PupilID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> AcademicYearID { get; set; }
        public Nullable<int> DisciplineTypeID { get; set; }
        public bool IsRecordedInSchoolReport { get; set; }
        public int DisciplineLevel { get; set; }
        public Nullable<System.DateTime> DisciplinedDate { get; set; }
        public string Description { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public decimal? ConductMinusMark { get; set; }
    }
}
