﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class TranscriptOfClass
    {
        public int AcademicYearID { get; set; }
        public int AppliedLevel { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int Semester { get; set; }
        public int SubjectID { get; set; }
        public int EducationLevelID { get; set; }
        public int PeriodDeclarationID { get; set; }
        public bool IsCommenting { get; set; }
        public int TeacherID { get; set; }
        public int UserAccountID { get; set; }
        public bool isAdmin { get; set; }
        public int FemaleID { get; set; }
        public int EthnicID { get; set; }
        public int FemaleEthnicID { get; set; }
        public int StatisticLevelReportID { get; set; } 
        public bool IsSuperVisingDept { get; set; }
        public bool IsSubSuperVisingDept { get; set; }
    }
}
