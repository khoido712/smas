﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TopupBO
    {
        
        public long TransactionID { get; set; }

        
        public DateTime TransactionDateTime { get; set; }

        
        public decimal TransactionAmount { get; set; }

        
        public decimal BalanceAfterTransaction { get; set; }

        
        public int status { get; set; }

        /// <summary>
        /// Loại giao dịch
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        /// Mã Loại giao dịch
        /// </summary>
        
        public int TransTypeID { get; set; }

        /// <summary>
        /// Dịch vụ
        /// </summary>
        
        public string TransTypeDes { get; set; }


        /// <summary>
        /// Tài khoản trước giao dịch
        /// </summary>
        
        public decimal BalanceBefore { get; set; }

        /// <summary>
        /// Tài khoản sau giao dịch
        /// </summary>
        
        public decimal BalanceAfter { get; set; }

        /// <summary>
        /// Số thẻ
        /// </summary>
        
        public string CardSerial { get; set; }

        /// <summary>
        /// Loại giao dịch
        /// </summary>
        
        public byte? ServiceType { get; set; }

        /// <summary>
        /// Số lượng SMS
        /// </summary>
        
        public int? Quantity { get; set; }

        /// <summary>
        /// Mã thanh toán qua Bankplus
        /// </summary>
        
        public string BPTransCode { get; set; }
        
        public string Description { get; set; }
    }
}
