﻿using SMS_SCHOOL.Utility.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSParentContractBO
    {

        public int Index { get; set; }
        public string PupilIDContractID { get; set; }

        public short? RegisType { get; set; }

        public string FullName { get; set; }

        public long SMSParentContractID { get; set; }

        public long SMSParentContractDetailID { get; set; }

        public int PupilFileID { get; set; }

        public int PupilOfClassID { get; set; }
        public string PupilCode { get; set; }

        public string PupilName { get; set; }

        public string StayingAddress { get; set; }

        public DateTime? BrithdayPupil { get; set; }

        public int ClassID { get; set; }

        public int AcademicYearID { get; set; }

        public int LevelID { get; set; }

        public string ClassName { get; set; }

        public string PhoneMainContract { get; set; }

        public string RelationShip { get; set; }

        public string ContractCode { get; set; }

        public string ContractName { get; set; }

        public int StatusContract { get; set; }

        public short? StatusSubscriperContract { get; set; }

        public string PhoneNumberRecever { get; set; }

        public int? SubscriptionTime { get; set; }//Thời hạn HKI,HKII

        public short? PaymentType { get; set; }

        public int SubscriptionStatus { get; set; }

        public bool? Isrenewal { get; set; }// gia han

        public DateTime? BirthdayMainContract { get; set; }

        public string IdentityContractName { get; set; }

        public DateTime? DateRegisterIdentify { get; set; }

        public string RegisterIndetifyPlace { get; set; }

        public decimal? Money { get; set; }

        public int? ServicePackageID { get; set; }

        public string ServiceCode { get; set; }
        public bool? IsWholeYearPackage { get; set; }
        public bool? IsLimitPackage { get; set; }
        public int Year { get; set; }

        public string FatherFullName { get; set; }

        public string MotherFullName { get; set; }

        public string SponsorFullName { get; set; }

        public string MobileFather { get; set; }

        public string MobileMother { get; set; }

        public string MobileSponsor { get; set; }

        public string SchoolCode { get; set; }

        public int? OrderID { get; set; }

        public int? TotalSMS { get; set; }

        public int? TotalSent { get; set; }

        public int? RegistrationType { get; set; }
        public int SchoolID { get; set; }

        public int MaxSMS { get; set; }
        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }

        public decimal? ViettelPrice { get; set; }

        public decimal? OtherPrice { get; set; }

        public string Subcriber { get; set; }

        private List<SMSParentContractDetailBO> _SMSParentContractDetailList;
        public List<SMSParentContractDetailBO> SMSParentContractDetailList
        {
            get
            {
                return _SMSParentContractDetailList;
            }
            set
            {
                if (value == null)
                {
                    this._SMSParentContractDetailList = new List<SMSParentContractDetailBO>();
                }
                else
                {
                    this._SMSParentContractDetailList = value;
                }
            }
        }

        public List<int?> ListSemester { get; set; }
    }

    public class SMSParentContractDefaultBO
    {
        public int SMSParentContractID { get; set; }

        public string ServiceCode { get; set; }

        public string ContractCode { get; set; }

        public int SchoolID { get; set; }

        public string Phone { get; set; }

        public string ContractorName { get; set; }

        public DateTime Birthday { get; set; }

        public string ContractorRelationship { get; set; }

        public int PupilID { get; set; }

        public short? Status { get; set; }

        public DateTime? CreatedTime { get; set; }

        public DateTime? UpdatedTime { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? IsMigrate { get; set; }

        public string UpdateNote { get; set; }
    }

    public class SMSParentContractGroupBO
    {
        private List<ContractDetailList> _contractDetailList;

        public int PupilID { get; set; }
        public int PupilOfClassID { get; set; }
        public string PupilCode { get; set; }
        public int? StatusPupil { get; set; }
        public string PupilName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public string ReceiverName { get; set; }
        public string Subscriber { get; set; }
        public int SubciptionTime { get; set; }
        public int? RegistrationType { get; set; }
        public string MotherMobile { get; set; }
        public short? StatusContract { get; set; }
        public string RelationshipContract { get; set; }
        public DateTime CreateTime { get; set; }
        public bool? IsLimit { get; set; }
        /// <summary>
        /// Thuoc tinh dung cho import excel
        /// </summary>
        public bool Error { get; set; }

        /// <summary>
        /// thuoc tinh chi dung duy nhat cho import excel
        /// </summary>
        public long SMSParentContractID { get; set; }

        public string MessageError { get; set; }
        public List<ContractDetailList> contractDetailList
        {
            get
            {
                return _contractDetailList;
            }
            set
            {

                if (value == null)
                {
                    this._contractDetailList = new List<ContractDetailList>();
                }
                else
                {
                    this._contractDetailList = value;
                }
            }
        }
    }

    public class ContractDetailList
    {
        private List<int?> _subscriptionTime;
        public string RelationShip { get; set; }
        public string PhoneReceiver { get; set; }
        public string RelationshipFather { get; set; }
        public string PhoneFather { get; set; }
        public string RelationshipMother { get; set; }
        public string PhoneMother { get; set; }
        public string RelationshipOther { get; set; }
        public string PhoneOther { get; set; }

        public int? ServicesPakageID { get; set; }
        public string ServiceCode { get; set; }
        public bool? IsWholeYearPackage { get; set; }
        public bool? IsLimitPackage { get; set; }
        public int SubscriptionStatus { get; set; }
        public long SMSParentContractDetailID { get; set; }
        public long SMSParentContractID { get; set; }
        public string ReceiverName { get; set; }
        public decimal Money { get; set; }
        public bool ErrorDetail { get; set; }
        public string MessageDetail { get; set; }
        /// <summary>
        /// chi dung cho xuat excel, khi xuat excel khong dung list subscriptionTime.Neu dang ky ca 2 hoc ki thi bang 3
        /// </summary>
        public string subscriptionTimeExcel { get; set; }

        public int? SecondSemester { get; set; }
        public int? FirstSemester { get; set; }
        public int? AllSemester { get; set; }

        public string InputSemester { get; set; }
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// ResgistrationType SMSParentContractDetail
        /// </summary>
        public int? RegistrationType { get; set; }
        public int? RegistrationTypeFirstSemester { get; set; }
        public int? RegistrationTypeSecondSemester { get; set; }
        public int? RegistrationTypeAllSemester { get; set; }
        public List<int?> subscriptionTime
        {
            get
            {
                return _subscriptionTime;
            }
            set
            {
                if (value == null)
                {
                    this._subscriptionTime = new List<int?>();
                }
                else
                {
                    this._subscriptionTime = value;
                }
            }
        }

        public List<string> ListExtraServicePackageCode { get; set; }
    }

    public class ContractSimplifyBO
    {
        public int PupilID { get; set; }
        public string Subscriber { get; set; }
    }


}
