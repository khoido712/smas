﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class OAuthUserBO
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        /// <summary>
        /// Dinh dang: dd/mm/yyyy
        /// </summary>
        public string BirthDate { get; set; }

        /// <summary>
        /// 1: Nam, 2:nu
        /// </summary>
        public string Gender { get; set; }

        public string Province { get; set; }

        public string District { get; set; }

        /// <summary>
        /// 1: tai khoan quan tri phong /so
        /// 2: quan tri truong
        /// 3: tai khoan nhan vien phong/so
        /// 4: tai khoan giao vien
        /// </summary>
        public string JobPosition { get; set; }

        /// <summary>
        /// Don vi
        /// </summary>
        public string UnitName { get; set; }

        public string MSourceDB { get; set; }
    }
}
