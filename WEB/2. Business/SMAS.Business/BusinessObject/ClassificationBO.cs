﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ClassificationBO
    {
        public int? MaxLevel { get; set; }
        public int? MinLevel { get; set; }
        public int MaxClassificationLevelID { get; set; }
        public int MinClassificationLevelID { get; set; }
        public int MaleClassificationCriterialID { get; set; }
        public int FemaleClassificationCriterialID { get; set; }
        public int Genre { get; set; }
        public int Month { get; set; }
        public decimal? Weight { get; set; }
    }
}
