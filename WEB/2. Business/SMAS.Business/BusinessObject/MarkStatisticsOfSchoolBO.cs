﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>05/12/2012 2:40:35 PM</date>
    /// </summary>
   public class MarkStatisticsOfSchoolBO
    {
        #region Member Variables
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public IDictionary<string, int> MarkInfo { get; set; }
        public int TotalTest { get; set; }
        public int TotalBelowAverage { get; set; }
        public double PercentBelowAverage { get; set; }
        public int TotalAboveAverage { get; set; }
        public double PercentAboveAverage { get; set; }
        #endregion

        public MarkStatisticsOfSchoolBO()
        {
        if(this.MarkInfo == null)
        {
            this.MarkInfo = new Dictionary<string, int>();
        }
        }

    }
}
