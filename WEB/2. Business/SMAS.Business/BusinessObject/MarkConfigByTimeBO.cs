﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MarkConfigByTimeBO
    {
        public int MarkConfigByTimeID { get; set; }
        public string TimeName { get; set; }
        public int SemesterID { get; set; }
        public string SemesterName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Year { get; set; }
    }
}
