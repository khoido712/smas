﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ProvinceApplyBO
    {
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceCode { get; set; }
        public int? ServicePackageID { get; set; }
        public bool? IsApply { get; set; }
    }
}
