using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;


namespace SMAS.Business.BusinessObject
{
    public class SpineTestBO
    {
        public int? MonitoringBookID { get; set; }
        public DateTime? MonitoringBookDate { get; set; }

        public int? SpineTestID { get; set; }
        public bool? IsDeformityOfTheSpine { get; set; }
        public string ExaminationStraight { get; set; }
        public string ExaminationItalic { get; set; }
        public string Other { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int? HealthPeriodID { get; set; }
        public string UnitName { get; set; }
        public PupilProfile PupilProFile { get; set; }
        public int PupilProFileID { get; set; }
        public DateTime? ModifineDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int LogID { get; set; }
        public bool IsDeformity { get; set; }
        public bool IsActive { get; set; }
        public int ClassID { get; set; }

    }
}
