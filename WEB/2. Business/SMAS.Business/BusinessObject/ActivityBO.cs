﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ActivityBO
    {
        public int ActivityID { get; set; }   
        public string ActivityName { get; set; }
        public string PeriodOfTime { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public int ActivityPlanID { get; set; }
        public int? DayOfWeek { get; set; }




        
        public int Section { get; set; }
        
        public int ActivityTypeID { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<System.DateTime> ActivityDate { get; set; }

        

    }
}
