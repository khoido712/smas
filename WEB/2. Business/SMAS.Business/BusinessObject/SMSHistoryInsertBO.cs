﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSHistoryInsertBO
    {
        public long HistoryID { get; set; }
        public string Mobile { get; set; }
        public int TypeID { get; set; }
        public int ReceiverID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int SchoolID { get; set; }
        public int Year { get; set; }
        public string Content { get; set; }
        public bool? Status { get; set; }
        public string SchoolUserName { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public int? FlagID { get; set; }
        public int SMS_CNT { get; set; }
        public System.DateTime? SEND_TIME { get; set; }
        public string ShortContent { get; set; }
        public int? ReceiveType { get; set; }
        public long? SMSParentContractDetailID { get; set; }
        public bool IsSMS { get; set; }
        public int? SenderUnitID { get; set; }
        public bool? IsAuto { get; set; }
        public long? SenderGroup { get; set; }
        public int PartitionSchoolID { get; set; }

        public byte? ContentType { get; set; }

        public bool? IsOnSchedule { get; set; }

        public Guid? TimerConfigID { get; set; }
        public bool IsAdmin { get; set; }
        public int SenderID { get; set; }

        public bool IsCustomType { get; set; }
    }
}
