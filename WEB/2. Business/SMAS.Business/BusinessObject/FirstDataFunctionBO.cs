﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class FirstDataFunctionBO
    {
        public int FunctionId { get; set; }
        public string FunctionName { get; set; }
        public string Url { get; set; }
        public string StatusName { get; set; }
        public string Contents { get; set; }
        public string AppliedLevel { get; set; }
        public string ProductVersion { get; set; }
        public int TabFunction { get; set; }
    }
}
