﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RatedCommentEQReportBO
    {
        public int Genre { get; set; }
        public int? EthnicID { get; set; }
        public bool IsCombinedClass { get; set; }
        public bool? IsDisabled { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationID { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public int EducationLevelID { get; set; }
        public int? PeriodicEndingMark { get; set; }
        public int? EndingEvaluation { get; set; }
        public int? CapacityEndingEvaluation { get; set; }
        public int? QualityEndingEvaluation { get; set; }
        public int SubjectID { get; set; }
    }
}
