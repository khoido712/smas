﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReportRequestBO
    {
        public int ReportRequestID { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public int SemesterID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationGrade { get; set; }
        public int Year { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool Status { get; set; }
    }
}
