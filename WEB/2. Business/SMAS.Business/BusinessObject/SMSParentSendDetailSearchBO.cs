﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSParentSendDetailSearchBO
    {
        public int SchoolId { get; set; }
        public int Semester { get; set; }
        public int Year { get; set; }
        public List<long> lstSMSParentContractDetailID { get; set; }
        public List<int> lstSemesterID { get; set; }
    }
}
