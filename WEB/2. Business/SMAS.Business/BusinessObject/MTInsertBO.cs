﻿using System;

namespace SMAS.Business.BusinessObject
{
    public class MTInsertBO
    {
        public long REQUEST_ID { get; set; }
        public string USER_ID { get; set; }
        public string COMMAND_CODE { get; set; }
        public DateTime? TIME_SEND_REQUEST { get; set; }
        public DateTime? TIME_SENT { get; set; }
        public string SERVICE_ID { get; set; }
        public string CONTENT { get; set; }
        public string CONTENT_TYPE { get; set; }
        public int STATUS { get; set; }
        public int RETRY_NUM { get; set; }
        public string ISDN_TAIL { get; set; }
        public int? MO_HIS_ID { get; set; }
        public int? SUB_ID { get; set; }
        public int? MT_TYPE { get; set; }
        public string CP_CODE { get; set; }
        public int? BLOCK_ID { get; set; }
        public long? HISTORY_ID { get; set; }
        public DateTime? SEND_TIME { get; set; }

        public int HistoryIndex { get; set; }
        public int UnitId { get; set; }

        public Guid? TimerConfigID { get; set; }

    }
}
