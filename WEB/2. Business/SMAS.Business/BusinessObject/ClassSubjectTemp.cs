﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ClassSubjectTemp
    {
        public int SubjectID { get; set; }
        public int? ClassID { get; set; }
        public string Abbreviation { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string DisplayName { get; set; }
        public Nullable<int> OrderInSubject { get; set; }
        public int? IsCommenting { get; set; }
        public int? SectionPerWeekFirstSemester { get; set; }
        public int? SectionPerWeekSecondSemester { get; set; }
    }
}
