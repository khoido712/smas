﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamMarkSemesterBO
    {
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public string MarkContent { get; set; }
    }
}
