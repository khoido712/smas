using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class ApprenticeshipTrainingAbsenceBO
    {

        public System.Int32 ApprenticeshipTrainingAbsenceID { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Int32 ClassID { get; set; }
        public System.Int32 ApprenticeshipClassID { get; set; }
        public System.Int32 AcademicYearID { get; set; }
        public System.Nullable<System.Int32> EducationLevelID { get; set; }
        public System.Int32 PupilID { get; set; }
        public System.String PupilCode { get; set; }
        public System.Nullable<System.Int32> SectionInDay { get; set; }
        public System.DateTime AbsentDate { get; set; }
        public System.Boolean IsAccepted { get; set; }
        public System.String FullName { get; set; }
        public System.String ScheduleDetail { get; set; }
        public System.Int32 SumP { get; set; }
        public System.Int32 SumK { get; set; }
    }
}
