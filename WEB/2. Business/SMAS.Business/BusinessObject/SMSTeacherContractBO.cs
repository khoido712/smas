﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    [Serializable]
    public class SMSTeacherContractBO
    {
        public string ContractCode { get; set; }
        public int PackageID { get; set; }
        public string PackageCode { get; set; }
        public string SchoolCode { get; set; }
        public string SupervisingDeptCode { get; set; }
        public int Status { get; set; }
        public int MaxSMS { get; set; }
    }
}
