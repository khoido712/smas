﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DishDetailBO
    {
        public int? FoodID;
        public int? DishID;
        public decimal? WeightPerRation;
    }
}
