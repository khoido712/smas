﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class Conversation
    {
        public int TotalRecord { get; set; }

        public List<ConversationDetail> ListConversation { get; set; }
    }

    public class ConversationDetail
    {
        public int SenderId { get; set; }

        public string SenderName { get; set; }

        public string SendDate { get; set; }


        public string Content { get; set; }
    }
}
