﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
   public class ClassBO
    {
       public int ClassID { set; get; }
       public string ClassName { set; get; }
       public int EducationGrade { get; set; }
       public int EducationLevelID { get; set; }
       public int? OrderID { get; set; }
       public string HeadTeacherName { get; set; }
       public int? HeadTeacherID { get; set; }
       public int Grade { get; set; }
       public string EducationName { get; set; }
       public bool? IsVNEN { get; set; }
    }
}
