﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PhysicalFacilitiesBO
    {
        public string CellAddress { get; set; }
        public string CellValue { get; set; }
        public int IndicatorID { get; set; }
        public string IndicatorCode { get; set; }
        public string CellCaled { get; set; }
        public string CellReportLCU { get; set; }
            
        //Thong tin ghi log khi xuat lai file execel
        public int SchoolID { get;set;}
        public int Year { get; set; }
        public int Period { get; set; }
        //Check HtmlTemplate khi xuat excel cap Mam non
        public int HtmlTemplateID { get; set; }
    }
}
