﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExamSupervisoryBO
    {
        public int ExamSupervisoryID { get; set; }
        public int ExaminationsID { get; set; }
        public int ExamGroupID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int TeacherID { get; set; }
        public int SupervisoryType { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<System.DateTime> SyncTime { get; set; }
        public string SyncSourceId { get; set; }

        public string EmployeeCode { get; set; }
        public string FullName { get; set; }

        public int? EthnicId { get; set; }

        public string EthnicName { get; set; }
        public string Mobile { get; set; }

        public DateTime? BirthDay { get; set; }

        /// <summary>
        /// Trinh do dao tao
        /// </summary>
        public int? TrainingLevelID { get; set; }

        public string TrainingLevelName { get; set; }

        public string ExamEmployeeStatus { get; set; }

        public string GenreName { get; set; }
        public bool Genre { get; set; }

        public string SupervisoryName { get; set; }




    }
}
