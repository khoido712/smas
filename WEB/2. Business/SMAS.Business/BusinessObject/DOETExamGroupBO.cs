﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExamGroupBO
    {
        public int ExamGroupID { get; set; }
        public string ExamGroupCode { get; set; }
        public string ExamGroupName { get; set; }
        public int ExaminationsID { get; set; }
        public int AppliedLevel { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<System.DateTime> SyncTime { get; set; }
        public string SyncSourceId { get; set; }
        public MapSource<int> ListMapSource { get; set; }
    }
}
