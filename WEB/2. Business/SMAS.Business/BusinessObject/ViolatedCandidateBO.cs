﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ViolatedCandidateBO 
    {
        public int Index { get; set; }
        public string NameListCode { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Genre { get; set; }
        public string GenreTitle { get; set; }
        public string EducationLevelResolution { get; set; }
        public string SubjectName { get; set; }
        public string RoomTitle { get; set; }
        public string ExamViolationTypeResolution { get; set; }
        public string ViolationDetail { get; set; }

        public int CandidateID {get;set;}
        public int EducationLevelID {get;set;}
        public int ExaminationID {get;set;}
        public int ExamViolationTypeID {get;set;}
        public int PupilID {get;set;}
        public int RoomID {get;set;}
        public int SubjectID {get;set;}
    }
}