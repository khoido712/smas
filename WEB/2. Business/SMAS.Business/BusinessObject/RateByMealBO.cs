﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class RateByMealBO
    {

        public int MealID { get; set; }
        public decimal Rate { get; set; }
        public decimal EnergyDemandFrom { get; set; }
        public decimal EnergyDemandTo { get; set; }
        public string MealName { get; set; }
    }
}
