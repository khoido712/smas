using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class MarkRecordBO
    {
        //Doi tuong cua bang MarkRecord
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public Nullable<System.Int32> SubjectID { get; set; }
        public string SubjectName { get; set; }
        public Nullable<int> MarkTypeID { get; set; }
        public Nullable<decimal> Mark { get; set; }
        public Nullable<decimal> SummedUpMark { get; set; }
        public Nullable<long> MarkRecordID { get; set; }
        public Nullable<int> PupilID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> AcademicYearID { get; set; }
        public Nullable<int> Semester { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> ReTestMark { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public int Status { get; set; }
        public int Coefficient { get; set; }
        public Nullable<System.DateTime> MarkedDate { get; set; }
        public Nullable<DateTime> BrithDate { get; set; }
        public Nullable<bool> Disable { get; set; }
        public Nullable<int> OrderInClass { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }
        //Doi tuong cua bang MarkRecordHistory
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<int> M_IsByC { get; set; }
        public Nullable<bool> IsOldData { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public Nullable<decimal> OldMark { get; set; }
        public string M_MarkRecord { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }
        public int? AppliedType { get; set; }
    }
}