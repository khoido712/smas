using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class PupilRetestToCategoryBO
    {
        public Nullable<int> Semester { get; set; }
        public Nullable<int> ClassID { get; set; }
        public string ClassName { get; set; }
        public int? SubjectID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public int? PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public DateTime? BirthDate { get; set; }
        public Nullable<int> Genre { get; set; }
        public int? CapacityLevelID { get; set; }
        public int? ConductLevelID { get; set; }
        public int? StudyingJudgementID { get; set; }
        public List<string> ListMark { get; set; }
        public List<SummedUpRecord> ListSummedUpRecord { get; set; }
    }
}
