using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportEmployeeSalaryBO
    {
        public int EmployeeID { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool Genre { get; set; }
        public string FacultyName { get; set; }
        public string EmployeeScale { get; set; }
        public int? SubLevel { get; set; }
        public decimal? Coefficent { get; set; }
        public decimal? SalaryAmount { get; set; }
        public DateTime? AppliedDate { get; set; }
        public string SalaryResolution { get; set; }
        public int? DurationInYear { get; set; }
    }
}
