﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.Common;

namespace SMAS.Business.BusinessObject
{
    public class Tab1ViewModelBO
    {
        public int PupilID { get; set; }
        public int Status { get; set; }
        public DateTime? EndDate { get; set; }
        public string PupilName { get; set; }
        public List<PupilSubjectEvaluationModelBO> lstSubjectEvaluation { get; set; }
        //Nang luc
        public int? Capacity { get; set; }
        //Pham chat
        public int? Quality { get; set; }
        //Danh gia cuoi nam
        public int? RateEndYear { get; set; }
        //Danh gia bo sung
        public int? RateAdd { get; set; }
        public int? StudyResult { get; set; }
        public string StrStudyResult
        {
            get
            {
                string str = string.Empty;
                if (StudyResult == GlobalConstants.STUDY_RESULT_HTT)
                {
                    str = "HTT";
                }
                else if (StudyResult == GlobalConstants.STUDY_RESULT_HT)
                {
                    str = "HT";
                }
                else if (StudyResult == GlobalConstants.STUDY_RESULT_CHT)
                {
                    str = "CHT";
                }
                return str;
            }
        }
        public string StrCapacity
        {
            get
            {
                string str = String.Empty;
                if (Capacity == GlobalConstants.T)
                {
                    str = "T";
                }
                else if (Capacity == GlobalConstants.D)
                {
                    str = "Đ";
                }
                else if (Capacity == GlobalConstants.C)
                {
                    str = "C";
                }
                return str;

            }
        }
        public string StrQuality
        {
            get
            {
                string str = String.Empty;
                if (Quality == GlobalConstants.T)
                {
                    str = "T";
                }
                else if (Quality == GlobalConstants.D)
                {
                    str = "Đ";
                }
                else if (Quality == GlobalConstants.C)
                {
                    str = "C";
                }
                return str;
            }
        }
        public string StrRateEndYear
        {
            get
            {
                string str = String.Empty;
                if (RateEndYear == GlobalConstants.HT)
                {
                    str = "HT";
                }
                else if (RateEndYear == GlobalConstants.CHT)
                {
                    str = "CHT";
                }
                return str;
            }
        }
        public string StrRateAdd
        {
            get
            {
                string str = String.Empty;
                if (RateAdd == GlobalConstants.HT)
                {
                    str = "HT";
                }
                else if (RateAdd == GlobalConstants.CHT)
                {
                    str = "CHT";
                }
                else if (RateAdd == GlobalConstants.CDG)
                {
                    str = "CĐG";
                }
                return str;
            }
        }
        public string InputRateAdd { get; set; }
        public bool IsShowData { get; set; }
        public string CQComment { get; set; }
        public string CommentAdd { get; set; }
    }
    public class PupilSubjectEvaluationModelBO
    {
        public int? SubjectID { get; set; }
        public string SubjectName { get; set; }
        public decimal? KTDK_CK { get; set; }
        public string KTDK_CK_JUDGE { get; set; }
        public int? Rate { get; set; }
        public int? IsCommenting { get; set; }
        public bool IsVnenSubject { get; set; }
        public decimal? SummedUpMark { get; set; }
        public decimal? AVERAGE_MARK { get; set; }
        public string AVERAGE_JUDGE { get; set; }
        public string JudgementResult { get; set; }
        public bool IsExempted { get; set; }
        public bool IsNotLearning { get; set; }
        public string StrDisplay
        {
            get
            {
                string str = String.Empty;
                if (IsNotLearning)
                {
                    return "KH";
                }
                if (IsExempted)
                {
                    return "MG";
                }

                if (IsVnenSubject)
                {
                    if (IsCommenting == 1)
                    {
                        if (string.IsNullOrEmpty(AVERAGE_JUDGE))
                        {
                            str = "CN";
                        }
                        else
                        {
                            str = AVERAGE_JUDGE;
                        }
                    }
                    else
                    {
                        string strKTDK_CK = String.Empty;
                        if (AVERAGE_MARK.HasValue)
                        {
                            strKTDK_CK = AVERAGE_MARK.Value != 0 && AVERAGE_MARK.Value != 10 ? String.Format("{0:0.0}", AVERAGE_MARK.Value) : String.Format("{0:0}", AVERAGE_MARK.Value);
                        }
                        else
                        {
                            strKTDK_CK = "CN";
                        }
                        str = strKTDK_CK;
                    }
                }
                else
                {
                    string strMark = String.Empty;
                    if (SummedUpMark != null)
                    {
                        strMark = SummedUpMark != 0 && SummedUpMark != 10 ? String.Format("{0:0.0}", SummedUpMark) : String.Format("{0:0}", SummedUpMark);
                    }
                    else
                    {
                        strMark = "CN";
                    }

                    string strJudge = string.Empty;
                    if (string.IsNullOrEmpty(JudgementResult))
                    {
                        strJudge = "CN";
                    }
                    else
                    {
                        strJudge = JudgementResult;
                    }

                    if (IsCommenting == 1)
                    {
                        str = strJudge;

                    }
                    else
                    {
                        str = strMark;
                    }
                }
                return str;
            }
        }
    }
}
