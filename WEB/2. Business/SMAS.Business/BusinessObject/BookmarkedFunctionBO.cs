﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class BookmarkedFunctionBO
    {
        public long BookmarkedFunctionID { get; set; }
        public int SchoolID { get; set; }
        public Nullable<int> AcademicYearID { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> LevelID { get; set; }
        public string FunctionName { get; set; }
        public string IconLink { get; set; }
        public Nullable<int> Order { get; set; }
        public int MenuID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Url { get; set; }
    }
}
