﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSParentContractExtraBO
    {
        public long SMS_PARENT_CONTRACT_EXTRA_ID { get; set; }
        public long SMS_PARENT_CONTRACT_ID { get; set; }
        public long SMS_PARENT_CONTRACT_DETAIL_ID { get; set; }
        public int SERVICE_PACKAGE_ID { get; set; }
        public int SUB_SCRIPTION_TIME { get; set; }
        public long AMOUNT { get; set; }
        public int PAYMENT_TYPE { get; set; }
        public bool IS_ACTIVE { get; set; }
        public System.DateTime CREATED_TIME { get; set; }
        public Nullable<System.DateTime> UPDATED_TIME { get; set; }
        public string  SERVICE_CODE { get; set; }
    }
}
