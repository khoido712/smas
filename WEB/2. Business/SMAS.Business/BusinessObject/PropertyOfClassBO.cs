﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PropertyOfClassBO
    {
        public int PropertyOfClassID { get; set; }

        public int ClassID { get; set; }

        public int ClassPropertyCatID { get; set; }
        public int? AppliedLevel { get; set; }
        public string ClassPropertyCatName { get; set; }

        public string ClassPropertyCatCode { get; set; }

        public bool IsActive { get; set; }	
    }
}
