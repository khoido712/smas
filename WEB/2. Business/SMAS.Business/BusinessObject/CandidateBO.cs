using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>11/12/2012 10:42:36 AM</date>
    /// </summary>
    public class CandidateBO : IComparable<CandidateBO>
    {
        #region Member Variables
        public System.Int32 ExaminationID { get; set; }

        public int EducationLevel { get; set; }

        public int ExaminationSubjectID { get; set; }

        public int? ClassID { get; set; }


        public string PupilCode { get; set; }

        public string FullName { get; set; }

        public string PupilName { get; set; }

        public string ClassName { get; set; }

        public string Genre { get; set; }

        public int OrgGenre { get; set; }

        public int AcademicYearID { get; set; }

        public int SchoolID { get; set; }

        public int AppliedLevel { get; set; }

        public int SupervisingDeptID { get; set; }

        public string EthnicCode { get; set; }

        #endregion

        #region search candidate mark
        public System.Int32 CandidateID { get; set; }

        public System.Nullable<System.Int32> RoomID { get; set; }

        public System.Int32 PupilID { get; set; }

        public System.Nullable<System.Int32> OrderNumber { get; set; }

        public System.String Judgement { get; set; }

        public System.Nullable<System.Int32> NamedListNumber { get; set; }

        public System.Nullable<System.Boolean> HasReason { get; set; }

        public System.String ReasonDetail { get; set; }

        public DateTime? BirthDate { get; set; }

        public System.String BirthPlace { get; set; }

        public string NamedListCode { get; set; }

        public string RoomTitle { get; set; }

        public string ExamMark { get; set; }

        public string RealMark { get; set; }

        public string Description { get; set; }

        public System.Int32 ProvinceID { get; set; }

        #endregion

        // Thuc hien giao dien IComparable
        public int CompareTo(CandidateBO other)
        {
            return CandidateBO.Compare(this, other);
        }

        public static int Compare(CandidateBO candidate1, CandidateBO candidate2)
        {
            int compareInNamedListNumber = Comparer<int?>.Default.Compare(candidate1.NamedListNumber, candidate2.NamedListNumber);
            if (compareInNamedListNumber != 0) return compareInNamedListNumber;
            int compareInNamedListCode = string.Compare(candidate1.NamedListCode, candidate2.NamedListCode);
            if (compareInNamedListCode != 0) return compareInNamedListCode;
            string thisNameForOrder = SMAS.Business.Common.Utils.SortABC(candidate1.FullName.getOrderingName(candidate1.EthnicCode));
            string otherNameForOrder = SMAS.Business.Common.Utils.SortABC(candidate2.FullName.getOrderingName(candidate2.EthnicCode));
            int compareInName = string.Compare(thisNameForOrder, otherNameForOrder, true);
            if (compareInName != 0) return compareInName;
            int compareInBirthDate;
            if (!candidate1.BirthDate.HasValue)
            {
                compareInBirthDate = !candidate2.BirthDate.HasValue ? 0 : -1;
            }
            else
            {
                compareInBirthDate = !candidate2.BirthDate.HasValue ? 1 : DateTime.Compare(candidate1.BirthDate.Value, candidate2.BirthDate.Value);
            }
            return compareInBirthDate;
        }
    }
}
