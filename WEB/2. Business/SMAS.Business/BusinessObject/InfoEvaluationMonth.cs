﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class InfoEvaluationMonth
    {
        /// <summary>
        /// luu thang nhan xet
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// luu danh gia ve giao duc
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// luu danh gia ve nang luc
        /// </summary>
        public string Fitness { get; set; }

        /// <summary>
        /// luu danh gia ve pham chat
        /// </summary>
        public string Quality { get; set; }
    }
}
