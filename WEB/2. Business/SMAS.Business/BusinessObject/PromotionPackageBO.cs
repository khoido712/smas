﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PromotionPackageBO
    {
        public int PromotionPackageID { get; set; }

        public int PromotionPackageDetailID;

        public bool? IsLimited { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string PromotionPackageName;

        public string PromotionPackageDetailName { get; set; }

        public string Description;

        public string DetailDescription { get; set; }
    }
}
