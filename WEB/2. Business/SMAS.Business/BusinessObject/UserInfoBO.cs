﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class UserInfoBO
    {
        public int UserAccountId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }

        /// <summary>
        /// Id cua nguoi dung. quan tri la id don vi
        /// </summary>
        public int Id { get; set; }
    }
}
