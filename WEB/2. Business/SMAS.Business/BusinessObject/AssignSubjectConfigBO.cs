﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class AssignSubjectConfigBO
    {
        public int AssignSubjectConfigID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public string AssignSubjectName { get; set; }
        public int EducationLevelID { get; set; }
        public string EducationApplied { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
