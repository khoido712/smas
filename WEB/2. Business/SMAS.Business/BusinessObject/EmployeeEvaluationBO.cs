﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EmployeeEvaluationBO
    {
        public int EmployeeID { get; set; }
        public int EvaluationFieldID { get; set; }
        public string EvaluationFieldName { get; set; }
        public string EvaluationLevelName { get; set; }
        public int FieldType { get; set; }
        public string EvaluationText { get; set; }
        public int SchoolID { get; set; }
    }
}
