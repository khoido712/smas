using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class StatisticOfTertiaryBO
    {
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public int? TrainingType { get; set; }
        public string TrainingTypeResolution { get; set; }
        public int? DistrictID { get; set; }
        public string DistrictName { get; set; }
        public int? Status { get; set; }
        public DateTime? SentDate { get; set; }
        public int? StatisticOfTertiaryID { get; set; }
        public int Year { get; set; }
        public string StatusResolution { get; set; }
        public bool? EnableDetail { get; set; }
    }
}
