using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class FoodCatBO
    {
        public FoodCat FoodCat { get; set; }
        public decimal? PLG { get; set; }
        public decimal Weight { get; set; }
        public decimal Min { get; set; }
    }

    public class DailyMenuFoodBO
    {
        public DailyMenuFood DailyMenuFood { get; set; }
        public int? DiscardedRate { get; set; }
        public int NumberOfChildren { get; set; }
        public FoodCat FoodCat { get; set; }
        public decimal? WeightPerRation { get; set; }
    }
}
