﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class HistoryReceiverBO
    {
        /// <summary>
        /// ID nguoi nhan
        /// </summary>
        public int ReceiverID { get; set; }
        /// <summary>
        /// Ten nguoi nhan
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// Ten don vi nguoi nhan
        /// </summary>
        public string ReceiverUnitName { get; set; }
        public int ReceiverUnitID { get; set; }
        /// <summary>
        /// So dt nguoi nhan
        /// </summary>
        public string Mobile { get; set; }
    }
}
