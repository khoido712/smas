﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EwalletResultBO
    {
        /// <summary>
        /// so hieu vi dien tu
        /// </summary>
        public int EwalletID { get; set; }


        /// <summary>
        /// loai giao dich
        /// </summary>
        public byte TransTypeID { get; set; }

        /// <summary>
        /// ma so tai khoan khuyen mai
        /// </summary>
        public int PromotionAccountID { get; set; }
        
        /// <summary>
        /// kiem tra du tien de gui
        /// </summary>
        public bool OK { get; set; }
        /// <summary>
        /// true neu vi dien tu khong du de gui tin nhan
        /// </summary>
        public bool OutOfBalance { get; set; }

        /// <summary>
        /// so tien tai khoan chinh khau tru
        /// </summary>
        public decimal SubMainBalance { get; set; }

        /// <summary>
        /// so tin noi mang khuyen mai khau tru
        /// </summary>
        public int SubPromOfInSMS { get; set; }

        /// <summary>
        /// so tin ngoai mang khuyen mai khau tru
        /// </summary>
        public int SubPromOfExSMS { get; set; }

        /// <summary>
        /// so tien tai khoan noi mang khau tru
        /// </summary>
        public decimal SubInternalBalance { get; set; }

        /// <summary>
        /// So tien tai khoan khuyen mai khau tru
        /// </summary>
        public decimal SubPromBalance { get; set; }

        public bool isError { get; set; }
        public string ErrorMsg { get; set; }

        /// <summary>
        /// so tin nhan tru o tai khoan chinh
        /// </summary>
        public int TotalSMSInMainBalance { get; set; }

        public EwalletResultBO() 
        {
            OK = true;
            SubMainBalance = 0;
            SubInternalBalance = 0;
            SubPromBalance = 0;
            TotalSMSInMainBalance = 0;
            SubPromOfInSMS = 0;
            SubPromOfExSMS = 0;
            OutOfBalance = false;
            isError = false;
        }
    }
}
