﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PromotionProgramBO
    {
        //thuonglv3 05102016
        #region Promotion Program
        public long PromotionID { get; set; }

        public string PromotionName { get; set; }

        public int FromOfInternalSMS { get; set; }

        public int FromOfExternalSMS { get; set; }

        public string strFromOfInternalSMS 
        {
            get
            {
                string str = string.Empty;
                if (IsFreeSMS.HasValue && IsFreeSMS.Value)
                {
                    str = "Miễn phí";
                }
                else
                {
                    str = FromOfInternalSMS.ToString();
                }
                return str;
            }
        }

        public string strFromOfExternalSMS
        {
            get
            {
                string str = string.Empty;
                if (IsFreeExSMS.HasValue && IsFreeExSMS.Value)
                {
                    str = "Miễn phí";
                }
                else
                {
                    str = FromOfExternalSMS.ToString();
                }
                return str;
            }
        }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Note { get; set; }

        public bool? IsActive { get; set; }
        public bool? IsFreeSMS { get; set; }
        public bool? IsFreeExSMS { get; set; }

        public DateTime? CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }

        #endregion

        #region Promotion Program Detail
        public long PromotionDetailID { get; set; }

        public int ProvinceID { get; set; }

        public DateTime? CreateTimeDetail { get; set; }

        public DateTime? UpdateTimeDetail { get; set; }
        #endregion
    }
}
