﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Business.BusinessObject
{
    public class ENTTestBO
    {
        public int? MonitoringBookID { get; set; }
        public int? ENTTestID { get; set; }
        public int? HealthPeriodID { get; set; }
        public DateTime? MonitoringBookDate { get; set; }
        public string UnitName { get; set; }
        public bool? IsDeaf { get; set; }
        public bool? IsSick { get; set; }
        public int? RCapacityHearing { get; set; }
        public int? LCapacityHearing { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int PupilProFileID { get; set; }
        public bool IsActive { get; set; }
        public int LogID { get; set; }
        public int ClassID { get; set; }
    }
}
