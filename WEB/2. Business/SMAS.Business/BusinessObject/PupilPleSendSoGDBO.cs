﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilPleSendSoGDBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int? PupilLeavingOffID { get; set; }
        public int? LeavingReasonID { get; set; }
        public string LeavingReasonName { get; set; }
        public DateTime? LeavingDate { get; set; }
        public int EducationLevelID { get; set; }
        public int? SchoolMovementID { get; set; }
        public DateTime? MovedDate { get; set; }
        public int? EnrolmentType { get; set; }
        public int Genre { get; set; }
        public DateTime? EnrolmentDate { get; set; }
    }
}
