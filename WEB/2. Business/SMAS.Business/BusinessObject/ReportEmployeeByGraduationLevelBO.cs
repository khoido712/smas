﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportEmployeeByGraduationLevelBO
    {
        public int SchoolID { get; set; }
        public int AppliedLevel { get; set; }
        public DateTime ReportDate { get; set; }
        public int FacultyID { get; set; }
        public int? GraduationLevelID { get; set; }
        public int? SpecialityCatID { get; set; }
    }
}
