﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ConsumptionReportBO
    {
        public string Content { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedTime { get; set; }
    }
}
