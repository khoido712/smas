using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class TeachingAssignmentBO
    {
        public long? TeachingAssignmentID { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> ClassID { get; set; }
        public string ClassName { get; set; }
        public int? ClassOrderNumber { get; set; }
        public int? SubjectID { get; set; }
        public string SubjcetAbbreviation { get; set; }
        public string SubjectName { get; set; }
        public Nullable<int> FacultyID { get; set; }
        public string FacultyName { get; set; }
        public int? TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string EmployeeCode { get; set; }
        public string EthnicCode { get; set; }
        public int? EducationLevelID { get; set; }
        public bool IsActive { get; set; }
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public bool OnlyOneSemester { get; set; }
        public int AppliedLevel { get; set; }
        public bool ExistInOtherSemester { get; set; }
        public decimal? SectionPerWeek { get; set; }
        public decimal? SectionPerWeekFirst { get; set; }
        public decimal? SectionPerWeekSecond { get; set; }
        //Thêm vào để sắp xếp
        public string Name { get; set; }
        public int? EmploymentStatus { get; set; }
        public int? OrderInSubject { get; set; }
        public bool TeachSemester1 { get; set; }
        public bool TeachSemester2 { get; set; }

    }
}
