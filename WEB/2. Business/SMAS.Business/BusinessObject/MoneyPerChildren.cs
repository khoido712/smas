﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class MoneyPerChildren
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Genre { get; set; }
        public int CurrentClassID { get; set; }
        public string CurrentClassName { get; set; }
        public string Date { get; set; }
        public int NumberDayOfMonth { get; set; }
        public decimal DailyDishCost { get; set; }
        public decimal MoneyPerMonth { get; set; }
    }
}
