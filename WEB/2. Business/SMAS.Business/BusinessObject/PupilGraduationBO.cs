using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilGraduationBO
    {
        public string ClassName { get; set; }

        public string PupilCode { get; set; }

        public string FullName { get; set; }

        public DateTime BirthDate { get; set; }

        public string BirthPlace { get; set; }

        public int Genre { get; set; }

        public string TempResidentalAddress { get; set; }

        public string PermanentResidentalAddress { get; set; }

        public string CapacityLevel { get; set; }

        public string ConductLevel { get; set; }

        public string ExemptionResolution { get; set; }

        public string Description { get; set; }

        public long? PupilRankingID { get; set; }

        public int? CapacityLevelID { get; set; }

        public int? ConductLevelID { get; set; }

        public int? PupilGraduationID { get; set; }

        public int PupilID { get; set; }

        public int? EthnicID { get; set; }
        public string EthnicName { get; set; }

        public int ClassID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public int? Year { get; set; }

        public int? GraduationLevel { get; set; }

        public int? GraduationGrade { get; set; }

        public string IssuedNumber { get; set; }

        public string RecordNumber { get; set; }

        public DateTime? IssuedDate { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? IsReceived { get; set; }

        public string PriorityReason { get; set; }

        public int? AbsentDays { get; set; }

        public int? GraduationStatus { get; set; }

        public string Notification { get; set; }

        public decimal? AverageMark { get; set; }

        public int? ApprenticeshipRank { get; set; }

        public decimal? ApprenticeshipPoints { get; set; }

        public decimal? PraisePoints { get; set; }

        public decimal? CertificatePoints { get; set; }

        public decimal? MeritPoints { get; set; }

        public string StatusString { get; set; }

        public int? OrderInClass { get; set; }

        public string Name { get; set; }

        public bool chkDisabled { get; set; }

        public int? PupilLearningType { get; set; }

        public decimal? LanguageCertificatePoint { get; set; }

        public decimal? ItCertificatePoint { get; set; }

        public int? SeperateKey { get; set; }
        public int? GraduationApprovalStatus { get; set; }
        public string GraduationApprovalStatusName { get; set; }
        public int? GraduationApprovalUserApprove { get; set; }
        public string GraduationApprovalUserApproveName { get; set; }
        public bool EnableCheckBoxPriority { get; set; }

        public IEnumerable<PupilAbsence> lstAbsence { get; set; }
    }
}
