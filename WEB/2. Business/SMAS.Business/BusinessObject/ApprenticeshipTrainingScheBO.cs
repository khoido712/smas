﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ApprenticeshipTrainingScheBO
    {
        public int day { get; set; }
        public int month { get; set; }
        public int order { get; set; }
    }
}
