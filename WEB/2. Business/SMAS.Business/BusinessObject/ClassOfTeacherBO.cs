﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// list class of teacher
    /// <auth>HaiVT</auth>
    /// <date>31/05/2013</date>
    /// </summary>
    public class ClassOfTeacherBO
    {
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public string Resolution { get; set; }
        public string DisplayName { get; set; }
        public Nullable<int> HeadTeacherID { get; set; }
        public Nullable<int> SubstituedHeadTeacher { get; set; }
        public string HeadTeacherName { get; set; }
        public string SubstituedHeadTeacherName { get; set; }
        public Nullable<int> ClassIDOfSupervisior { get; set; }
        public int CountOfPupil { get; set; }
        public int CountOfMale { get; set; }
        public int CountOfFemale { get; set; }
        public int? AppliedLevel { get; set; }
        public bool? IsVNEN { get; set; }
        public int? SectionID { get; set; }
    }    
}
