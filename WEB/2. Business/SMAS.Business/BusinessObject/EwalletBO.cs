﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EwalletBO
    {
        /// <summary>
        /// Số hiệu ví điện tử
        /// </summary>
        
        public int EWalletID { get; set; }

        /// <summary>
        /// Tên đăng nhập
        /// </summary>
        
        public string Username;

        /// <summary>
        /// Số dư tài khoản
        /// </summary>
        
        public int Balance { get; set; }

        /// <summary>
        /// Số tin nhắn khuyến mãi/tháng. Hiện giờ chưa có thì cho nó bằng 0.
        /// </summary>
        
        public int NumberOfPromotiveSMS { get; set; }

        /// <summary>
        /// trang thai khong hay khong xoa cua vi dien tu
        /// </summary>
        
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// thoi gian bi khoa
        /// </summary>
        
        public TimeSpan RemainOfTimeToUnlock { get; set; }

        #region AnhVD9 - Bổ sung thông tin tài khoản khuyến mãi
        /// <summary>
        /// Số hiệu tài khoản khuyến mãi
        /// </summary>
        
        public int PromotionAccountID { get; set; }

        /// <summary>
        /// Số dư tài khoản khuyến mãi
        /// </summary>
        
        public decimal PromotionBalance { get; set; }

        /// <summary>
        /// Số dư tài khoản nội mạng khuyến mãi
        /// </summary>
        
        public decimal InternalBalance { get; set; }

        /// <summary>
        /// Số tin nội mạng khuyến mãi
        /// </summary>
        
        public int InternalSMS;

        /// <summary>
        /// Số tin ngoại mạng khuyến mãi
        /// </summary>
        
        public int ExternalSMS;


        /// <summary>
        /// Số hiệu tài khoản
        /// </summary>
        
        public int UnitID { get; set; }

        public int EwalletType { get; set; }
        #endregion End AnhVD9 - 20141027
    }
}
