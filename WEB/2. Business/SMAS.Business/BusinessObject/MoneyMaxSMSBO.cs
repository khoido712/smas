﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MoneyMaxSMSBO
    {
        public decimal? MoneyViettel { get; set; }
        public decimal? MoneyOther { get; set; }
        public int? MaxSMS { get; set; }
        public int ServicesPackageID { get; set; }
    }
}
