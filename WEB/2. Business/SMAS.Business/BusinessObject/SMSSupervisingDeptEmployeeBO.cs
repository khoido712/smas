﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSSupervisingDeptEmployeeBO
    {
        public string Content { get; set; }

        public List<int> ListReceiverId { get; set; }

        public string ShortContent { get; set; }

        public bool IsSignMsg { get; set; }
        public int CountSMS { get; set; }

        public PromotionProgramBO Promotion { get; set; }

        public int SuperVisingDeptId { get; set; }
        public string SuperUserName { get; set; }

        public SupervisingDept SuperVisingDepts { get; set; }

        public bool isAdmin { get; set; }

        public int SenderId { get; set; }

        public int SelectedUnitID { get; set; }

        public int ProvinceID { get; set; }

        public int EmployeeID { get; set; }
    }
}
