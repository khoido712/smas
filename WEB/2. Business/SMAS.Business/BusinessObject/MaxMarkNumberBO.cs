﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MaxMarkNumberBO
    {
        public int SubjectID { get; set; }
        public int ClassID { get; set; }
        public int MarkIndex { get; set; }
        public string Title { get; set; }
        public int PupilID { get; set; }
    }
}
