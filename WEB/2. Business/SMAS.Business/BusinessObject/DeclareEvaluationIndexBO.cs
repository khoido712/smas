﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DeclareEvaluationIndexBO
    {
        public int DeclareEvaluationIndexID { get; set; }
        public int CurrentSchoolID { get; set; }
        public int CurrentAcademicYearID { get; set; }
        public int EvaluationGroupID { get; set; }
        public string EvaluationTypeName { get; set; }
        public int EvaluationDevGroupID { get; set; }
        public int EvaluationDevIndexID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModfiedDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string EvaluationDevCode { get; set; }
        public int? OrderID { get; set; }
        public int? OrderIDEvaluationGroup { get; set; }
        public int AppliedLevel { get; set; }
        public int? ClassID { get; set; }
        public int EducationLevelID_DEG { get; set; }
        public int EducationLevelID_EDG { get; set; }
        public int EducationLevelID_DEI { get; set; }

        public string DescriptionIndex { get; set; }
        public int DeclareEvaluationGroupID { get; set; }
        public int EvaluationDevelopmentID { get; set; }


        // 18/04/2018
        public long DeclareEducationID { get; set; }
        public int SchoolID { get; set; }
        public string EvaluationDevelopmentGroupName { get; set; }
        public string EvaluationDevelopmentIndexName { get; set; }
        public int? EvaluationDevelopmentOrderID { get; set; }
        public string EvaluationDevelomentIndexCode { get; set; }
        public int DeclareEducatioGroupID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationID { get; set; }
        public int EvaluationIndexID { get; set; }
        public int? EvaluationGroupOrderID { get; set; }
        public int? EvaluationGroupOfDEI { get; set; }
        public bool? CreatedBySchool { get; set; }
        public bool isCheck
        {
            get
            {
                if (EvaluationGroupOfDEI == null)
                    return false;
                return true;
            }
        }
        public bool? IsLocked { get; set; }
        public int? IsNew { get; set; }
    }
}
