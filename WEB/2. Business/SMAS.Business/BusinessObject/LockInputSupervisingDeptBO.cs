﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    class LockInputSupervisingDeptBO
    {
		//BO
        public int LockInputSuperVisingDeptID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int AppliedLevelID { get; set; }
        public string LockTitle { get; set; }
        public int UserLockID { get; set; }
        public int UserUnlockID{get;set;}
        public DateTime ModifiedDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
