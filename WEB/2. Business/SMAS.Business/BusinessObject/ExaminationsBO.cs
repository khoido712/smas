﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExaminationsBO
    {
        public long ExaminationsID { get; set; }
        public string ExaminationsName { get; set; }
        public int? Semester { get; set; }
        public bool? DataInheritance { get; set; }
        public long? InheritanceExam { get; set; }
        public string InheritanceData { get; set; }
        public int? MarkInputType { get; set; }
        public bool? MarkInput { get; set; }
        public bool? MarkClosing { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsDelete { get; set; }
        public int AcademicYearID {get;set;}
        public int AppliedLevel {get;set;}
        public int SemesterID{get;set;}
        public int SchoolID{get;set;}
        public DateTime? UpdateTime { get; set; }
    }
}
