﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EvaluationRewardBO
    {
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public string Content { get; set; }
        public DateTime CreateTime { get; set; }
        public int LastDigitSchoolID { get; set; }
        public int PupilID { get; set; }
        public int? RewardID { get; set; }
        public int SchoolID { get; set; }
        public int SemesterID { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}
