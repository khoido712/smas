﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReportSummedEndingEvaluationBO
    {
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public DateTime CreateTime { get; set; }
        public string EndingEvaluation { get; set; }
        public int EvaluationID { get; set; }
        public string EvaluationReTraining { get; set; }
        public int LastDigitSchoolID { get; set; }
        public long PupilID { get; set; }
        public int SchoolID { get; set; }
        public int SemesterID { get; set; }
        public long SummedEndingEvaluationID { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int? RateAdd { get; set; }
    }
}
