﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DetailCostOfSalesReport
    {
        public int Order { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public int TotalCustommer { get; set; }
        public long ChannelTypeID { get; set; }
        public string ChannelTypeName { get; set; }

        public string CollaboratorCode { get; set; }
        public string CollaboratorName { get; set; }

        public string CustomerName { get; set; }
        public string CustomerUserName { get; set; }

        public string ServiceCode { get; set; }

        public decimal InterRatio { get; set; }
        public decimal ExterRatio { get; set; }

        public decimal NumOfInternalSubscriber { get; set; }
        public decimal NumOfExternalSubscriber { get; set; }

        public decimal TotalOfInternalRevenue { get; set; }
        public decimal TotalOfExternalRevenue { get; set; }
    }
    public class RevenueSharingRatio
    {
        public RevenueSharingRatio(string serviceCode, decimal interRatio, decimal exterRatio)
        {
            this.ServiceCode = serviceCode;
            this.InterRatio = interRatio;
            this.ExterRatio = exterRatio;
        }
        public string ServiceCode { get; set; }
        public decimal InterRatio { get; set; }
        public decimal ExterRatio { get; set; }
    }
}
