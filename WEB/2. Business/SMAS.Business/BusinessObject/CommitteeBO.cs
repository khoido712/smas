﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class CommitteeBO
    {
        public int? CommitteeID { get; set; }
        public string Resolution { get; set; }
        public int? EducationLevelID { get; set; }
        public int? PupilTotal { get; set; }
        public string strEducationLevel { get; set; }
    }
}
