using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilFaultBO
    {
        public int PupilFaultID { get; set; }
        public int PupilID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public int FaultID { get; set; }
        public int AcademicYearID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<System.DateTime> ViolatedDate { get; set; }
        public Nullable<int> NumberOfFault { get; set; }
        public Nullable<decimal> TotalPenalizedMark { get; set; }
        //public Nullable<System.DateTime> FromViolatedDate { get; set; }
        //public Nullable<System.DateTime> ToViolatedDate { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public int FaultGroupID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string DisplayName { get; set; }
        public int ProfileStatus { get; set; }
        public string Resolution { get; set; }
        public string Description { get; set; }
    }
}
