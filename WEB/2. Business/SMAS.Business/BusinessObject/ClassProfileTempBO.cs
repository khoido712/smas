﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class ClassProfileTempBO
    {
        public int ClassProfileID { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int? EmployeeID { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectID { get; set; }
        public string DisplayName { get; set; }
        public string EmployeeName { get; set; }
        public int? Section { get; set; }
        public int? OrderNumber { get; set; }
        public int? SectionKey { get; set; }//buoi hoc sinh
        public bool? isVNEN { get; set; }
        public int? HeadTeacherID { get; set; }
    }
}
