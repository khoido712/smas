﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PriceSendSMSBO
    {
        public decimal PriceSendSMS { get; set; }
        public int InternalSendSMS { get; set; }
        public int ExternalSendSMS { get; set; }
        /// <summary>
        /// Tong so tin nhan da nhan
        /// </summary>
        public int TotalReceivedSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan noi mang
        /// </summary>
        public int TotalInternalSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan ngoai mang da nhan
        /// </summary>
        public int TotalExternalSMS { get; set; }
    }
}
