using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ParticularPupilBO
    {
        public int ParticularPupilID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public string FamilyCharacteristic { get; set; }
        public string PsychologyCharacteristic { get; set; }
        public Nullable<System.DateTime> RecordedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
    }
}
