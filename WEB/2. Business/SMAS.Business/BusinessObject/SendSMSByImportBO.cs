﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SendSMSByImportBO
    {
        public int PupilFileID { get; set; }
        public int ClassID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string Content { get; set; }

        public string ShortContent { get; set; }
    }
}
