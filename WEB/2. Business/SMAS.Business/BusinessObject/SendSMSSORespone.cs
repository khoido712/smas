﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SendSMSSORespone
    {
        public bool Result { get; set; }

        public int ValidateCode { get; set; }
    }
}
