﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ActionAuditDataBO
    {
        public long PupilID { get; set; }
        public int ClassID { get; set; }
        public int EvaluationID { get; set; }
        public int SubjectID { get; set; }
        public StringBuilder OldData { get; set; }
        public StringBuilder NewData { get; set; }
        public StringBuilder ObjID { get; set; }
        public StringBuilder Parameter { get; set; }
        public StringBuilder UserFunction { get; set; }
        public StringBuilder UserAction { get; set; }
        public StringBuilder UserDescription { get; set; }
        public StringBuilder Description { get; set; }
        public bool isInsertLog { get; set; }

        public string RestoreMsgDescription { get; set; }
    }
}
