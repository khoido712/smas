﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EmployeeColumnTemplateBO
    {
        public int ColDesId { get; set; }
        public string ColExcel { get; set; }
        public string ExcelName { get; set; }
    }
}
