﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class StatisticReportBO
    {
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int SemesterID { get; set; }
        public int EducationLevelID { get; set; }
        public int FemaleType { get; set; }
        public int EthnicTypeID { get; set; }
        public int AppliedLevelID { get; set; }
        public int SubCommitteeID { get; set; }
    }
}
