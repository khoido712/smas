﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class LevelBO
    {
        /// <summary>
        /// Cấp trường
        /// </summary>
        public int EducationGrade { get; set; } //Cấp 

        /// <summary>
        /// Khối
        /// </summary>
        public int EducationLevelID { get; set; } // Khối

        /// <summary>
        /// Tên Khối
        /// </summary>
        public string EducationLevelName { get; set; } //Tên khối
    }
}
