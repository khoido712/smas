﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ServicePackageBO
    {
        #region Service package
        public int ServicePackageID { get; set; }

        public string ServiceCode { get; set; }

        public string Description { get; set; }

        public DateTime? Createdtime { get; set; }

        public decimal? ViettelPrice { get; set; }

        public decimal? OtherPrice { get; set; }

        public decimal PriceForCustomer { get; set; }

        public bool? IsActive { get; set; }

        //public bool? Indefinitely { get; set; }

        public bool? IsLimit { get; set; }

        //public bool? IsAllowPostPaid { get; set; }

        //public bool? IsLimitProvince { get; set; }
        //public bool? IsLimitDistrict { get; set; }
        //public bool? IsLimitSchool { get; set; }
        //public bool? IsAllowOnlyInternal { get; set; }

        // Anhvd9 20160218
        //public bool? IsAllowRegisterSideSubscriber { get; set; }

        //public string ProvinceApply { get; set; }
        //public string DistrictApply { get; set; }
        //public string SchoolApply { get; set; }

        public bool? IsFreePackage { get; set; }
        public decimal? InternalSharingRatio { get; set; }
        public decimal? ExternalSharingRatio { get; set; }

        #endregion

        #region Service Package Detail
        public int ServicePackageDetailID { get; set; }

        public int? MaxSMS { get; set; }

        public int? Type { get; set; }
        #endregion

        #region Service Package Declare Detail
        public int? ServicePackageDeclareDetailID { get; set; }

        public int? Year { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int? Semester { get; set; }

        public int? EffectiveDays { get; set; }

        public DateTime UpdatedTime { get; set; }

        public bool? Status { get; set; }

        public bool? IsForOnlyInternal { get; set; }

        public bool? IsExtraPackage { get; set; }

        public bool? IsForOnlyExternal { get; set; }
        public bool? IsWholeYearPackage { get; set; }
        public int? ApplyType { get; set; }

        public string StrApplyType
        {
            get
            {
                string str = string.Empty;
                if (ApplyType == 1)
                {
                    str = "Theo trường";
                }
                else if (ApplyType == 2)
                {
                    str = "Theo tỉnh";
                }

                return str;

            }
        }
        #endregion

        public List<int> ListAppliedProvince { get; set; }

        public List<int> ListAppliedSchool { get; set; }

    }
}
