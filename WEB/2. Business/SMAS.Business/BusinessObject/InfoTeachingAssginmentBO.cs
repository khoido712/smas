﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class InfoTeachingAssginmentBO
    {
        /// <summary>
        /// ma giao vien
        /// </summary>
        public int? TeacherID { get; set; }

        /// <summary>
        /// ma code nhan vien
        /// </summary>
        public string EmployeeCode { get; set; }

        /// <summary>
        /// ho ten
        /// </summary>
        public string FulName { get; set; }

        /// <summary>
        /// to bo mon
        /// </summary>
        public string FaultyName { get; set; }
      
        /// <summary>
        /// phan cong HKI
        /// </summary>
        public string AssignSemesterI { get; set; }

        /// <summary>
        /// Phan cong HKII
        /// </summary>
        public string AssignSemesterII { get; set; }
    }
}
