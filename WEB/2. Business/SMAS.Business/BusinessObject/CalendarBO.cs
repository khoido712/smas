﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class CalendarBO
    {
        public int CalendarID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public int  DayOfWeek { get; set; }
        public Nullable<int> Section { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> SubjectOrder { get; set; }
        public Nullable<int> NumberOfPeriod { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string ClassName { get; set; }
        public string SubjectName { get; set; }
        public string Color { get; set; }
        public int? AssignmentSubjectID { get; set; }
        public string strCalendar { get; set; }
    }
}
