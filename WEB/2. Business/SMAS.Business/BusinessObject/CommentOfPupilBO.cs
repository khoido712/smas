﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class CommentOfPupilBO
    {
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public int SemesterID { get; set; }
        public int MonthID { get; set; }
        public System.DateTime DateTimeComment { get; set; }
        public string Comment { get; set; }
        public int LastDigitSchoolID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public int LogChangeID { get; set; }
        public int? OrderInSubject { get; set; }
    }
}
