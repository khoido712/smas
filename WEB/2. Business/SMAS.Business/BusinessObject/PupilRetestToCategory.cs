﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class PupilRetestToCategory
    {
        public int PupilID{get;set;}
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public int Semester { get; set; }
        public List<SummedUpRecord> lstSummedUpRecord {get;set;}
        public List<SummedUpRecordBO> lstSummedUpRecordBO { get; set; }
        public List<VSummedUpRecord> VlstSummedUpRecord { get; set; }
        public List<string> lstMark { get; set; }
        public PupilRanking pr {get;set;}
        public PupilRankingBO Vpr { get; set; }
        public List<PupilRetestRegistration> lstPupiRetestRegistration { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public string CapacityLevelName { get; set; }
        public string ConductLevelName { get; set; }
    }
    public class PupilRetestHistoryToCategory
    {
        public int PupilID { get; set;  }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public int Semester { get; set; }
        public List<SummedUpRecordHistory> lstSummedUpRecordHistory { get; set; }
        public List<string> lstMark { get; set; }
        public PupilRankingHistory pr { get; set; }
        public PupilRankingBO Vpr { get; set; }
        public List<PupilRetestRegistration> lstPupiRetestRegistration { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public string CapacityLevelName { get; set; }
        public string ConductLevelName { get; set; }
    }
}
