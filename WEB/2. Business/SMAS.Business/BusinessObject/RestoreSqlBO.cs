﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RestoreSqlBO
    {
        public bool Error { get; set; }
        public int OrderId { get; set; }
        public string Sql { get; set; }

        public PupilOfClass PupilOfass { get; set; }

        public MarkRecord MarkRecord { get; set; }

        public JudgeRecord JudgeRecord { get; set; }

        public SummedUpRecord SummedUpRecord { get; set; }

        public TeacherNoteBookMonth TeacherNoteBookMonth { get; set; }

        public TeacherNoteBookSemester TeacherNoteBookSemester { get; set; }

        public RatedCommentPupil RatedCommentPupil { get; set; }

    }
}
