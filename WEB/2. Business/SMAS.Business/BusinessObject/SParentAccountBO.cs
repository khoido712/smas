﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SParentAccountBO
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsExist { get; set; }

        public int PupilID { get; set; }

        public string FullName { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsRenew { get; set; }

        public bool? IsCancel { get; set; }

        public bool? IsSuspension { get; set; }

        public bool? IsEnable { get; set; }
    }
}
