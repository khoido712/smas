using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DevelopmentByClassBO
    {
        public int DevelopmentOfChildrenID { get; set; }
        public int Semester { get; set; }
        public int Year { get; set; }
        public int PupilID { get; set; }
        public int EvaluationDevelopmentID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int EvaluationDevelopmentGroupID { get; set; }
        public string EvaluationDevelopmentGroupName_ { get; set; }
        public int AppliedLevel { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string EvaluationDevelopmentCode { get; set; }
        public string EvaluationDevelopmentName { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public int InputType { get; set; }
        public Nullable<decimal> InputMin { get; set; }
        public Nullable<decimal> InputMax { get; set; }
        public int EducationLevelID { get; set; }
        public DateTime BirthDate { get; set; }

    }
}
