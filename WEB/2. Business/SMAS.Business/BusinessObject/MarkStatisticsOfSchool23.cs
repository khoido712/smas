﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class MarkStatisticsOfSchool23
    {
        public string SchoolName { get; set; }
        public int SchoolID { get; set; }
        public DateTime? SentDate { get; set; }
        public int TotalSchool { get; set; }
        public int M00 { get; set; }
        public int M03 { get; set; }
        public int M05 { get; set; }
        public int M08 { get; set; }
        public int M10 { get; set; }
        public int M13 { get; set; }
        public int M15 { get; set; }
        public int M18 { get; set; }
        public int M20 { get; set; }
        public int M23 { get; set; }
        public int M25 { get; set; }
        public int M28 { get; set; }
        public int M30 { get; set; }
        public int M33 { get; set; }
        public int M35 { get; set; }
        public int M38 { get; set; }
        public int M40 { get; set; }
        public int M43 { get; set; }
        public int M45 { get; set; }
        public int M48 { get; set; }
        public int M50 { get; set; }

        public int M53 { get; set; }
        public int M55 { get; set; }
        public int M58 { get; set; }
        public int M60 { get; set; }
        public int M63 { get; set; }
        public int M65 { get; set; }
        public int M68 { get; set; }
        public int M70 { get; set; }
        public int M73 { get; set; }
        public int M75 { get; set; }
        public int M78 { get; set; }
        public int M80 { get; set; }
        public int M83 { get; set; }
        public int M85 { get; set; }
        public int M88 { get; set; }
        public int M90 { get; set; }
        public int M93 { get; set; }
        public int M95 { get; set; }
        public int M98 { get; set; }
        public int M100 { get; set; }
        public int TotalTest { get; set; }
        public int TotalBelowAverage { get; set; }
        public decimal PercentBelowAverage { get; set; }
        public int TotalAboveAverage { get; set; }
        public decimal PercentAboveAverage { get; set; }
    }
}
