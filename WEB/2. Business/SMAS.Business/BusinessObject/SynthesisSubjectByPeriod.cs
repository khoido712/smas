﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SynthesisSubjectByPeriod
    {
        public int AcademicYearID { get; set; }
        public int PeriodID { get; set; }
        public int SchoolID { get; set; }
        public int AppliedLevel { get; set; }
        public int SemesterID { get; set; }
        public int StatisticLevelReportID { get; set; }
        public int FemaleID { get; set; }
        public int EthnicID { get; set; }
        public int FemaleEthnicID { get; set; }
    }
}
