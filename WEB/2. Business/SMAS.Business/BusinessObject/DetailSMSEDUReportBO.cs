﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DetailSMSEDUReportBO
    {
        public int Order { get; set; }

        public int ProvinceID { get; set; }

        public int DistrictID { get; set; }

        public int SchoolID { get; set; }

        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }

        public string SchoolName { get; set; }

        public string SchoolUserName { get; set; }

        public int? NumOfInternalSubscriber { get; set; }

        public int? NumOfExternalSubscriber { get; set; }
        public int? NumOfSubscriberDeactive { get; set; }
        public int? NumOfInternalSubscriberDeactive { get; set; }
        public int? NumOfExternalSubscriberDeactive { get; set; }
        public int? NumOfSubscriberActive { get; set; }
        public decimal? SumMoneyTopUp { get; set; }

        public decimal? SumMoneyConsumer { get; set; }

        public decimal? SumAllPackage { get; set; }
        public int? SumSMSTeacherInternal { get; set; }

        public int? SumSMSTeacherExternal { get; set; }

        public int? SumSMSParentExternal { get; set; }
        public int? SumSMSParentInternal { get; set; }

        public int? SumTeacherReceiveSMS { get; set; }

        public int? SumParentReceiveSMS { get; set; }
    }
}
