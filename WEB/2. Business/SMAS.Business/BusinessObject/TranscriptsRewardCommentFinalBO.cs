﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TranscriptsRewardCommentFinalBO
    {
        public int RewardCommentFinalID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SemesterID { get; set; }
        public string OutstandingAchievement { get; set; }
        public string RewardID { get; set; }
        public string RewardMode { get; set; }
        public int LastDigitSchoolID { get; set; }
    }
}
