﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class CapacityStatisticRequestBO
    {
        public int SupervisingDeptID { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public int YearID { get; set; }
        public int SemesterID { get; set; }
        public int AppliedLevelID { get; set; }
        public int EducationLevelID { get; set; }
        public bool IsSupervisingDept { get; set; }
        public bool IsFemale { get; set; }
        public bool IsEthnic { get; set; }
        public bool IsEthnicFemale { get; set; }
        public int reportType { get; set; } 
    }
}
