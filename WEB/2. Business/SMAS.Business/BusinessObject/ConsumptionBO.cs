﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ConsumptionBO
    {
        public string NDChiPhi { get; set; }
        public decimal DonGia { get; set; }
        public decimal SoLuong { get; set; }
        public decimal SoTien { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
