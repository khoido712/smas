﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MessageOfConversation
    {
        public int TotalRecord { get; set; }

        public List<MessageOfConversationDetail> ListMessage { get; set; }
    }

    public class MessageOfConversationDetail
    {
        public long MessageId { get; set; }

        public int SenderId { get; set; }

        public string SenderName { get; set; }

        public string SendDate { get; set; }


        public string Content { get; set; }
    }
}
