﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class EmployeeContactBO
    {
        public int EmployeeID { get; set; }
        public string Mobile { get; set; }
        public string FullName { get; set; }
    }
}
