﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SchoolBO
    {
        public int SchoolID { get; set; }

        public string SchoolCode { get; set; }

        public string SchoolName { get; set; }

        public int SupervisingDeptID { get; set; }
        public string SupervisingDeptName { get; set; }

        public string HeadMasterPhone { get; set; }

        public int SMSTeacherActiveType { get; set; }

        public int SMSParentActiveType { get; set; }

        public int AdminID { get; set; }

        public string HeadMasterName { get; set; }

        public int ProvinceID { get; set; }

        public int DistrictID { get; set; }

        public string ProvinceName { get; set; }

        public string DistrictName { get; set; }

        public bool VocationType { get; set; }

        // 20160216 Anhvd9 - 
        public string UserName { get; set; }

        public int EducationGrade { get; set; }

        public int AreaID { get; set; }

        public int CommuneID { get; set; }

        public string Address { get; set; }

        public DateTime EstablishedDate { get; set; }

        public string Telephone { get; set; }

        public string Email { get; set; }

        public byte ProductVersion { get; set; }

        public bool IsNewSchoolModel { get; set; }

        public bool isNumberVT { get; set; }
        public bool? isFreeSMS { get; set; }
        public bool? isFreeExSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan noi mang da nhan trong thang
        /// </summary>
        public int TotalInternalSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan ngoai mang da nhan trong thang
        /// </summary>
        public int TotalExternalSMS { get; set; }

        public bool HasBeenApplied { get; set; }
        public int TotalSMSPromotion { get; set; }
        public string strTotalSMSPromotion
        {
            get
            {
                string str = string.Empty;
                if (isNumberVT)
                {
                    if (isFreeSMS.HasValue && isFreeSMS.Value)
                    {
                        str = "Miễn phí";
                    }
                    else
                    {
                        str = TotalSMSPromotion.ToString();
                    }
                }
                else
                {
                    if (isFreeExSMS.HasValue && isFreeExSMS.Value)
                    {
                        str = "Miễn phí";
                    }
                    else
                    {
                        str = TotalSMSPromotion.ToString();
                    }
                }
                return str;
            }
        }
    }
}
