﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportPreliminaryBO
    {
        public int AcademicYearID { get; set; }
        public int SchoolID{get;set;}
        public int Semester{get;set;}
        public int AppliedLevel { get; set; }
    }
}
