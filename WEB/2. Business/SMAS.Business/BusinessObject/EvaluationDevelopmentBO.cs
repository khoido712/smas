﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EvaluationDevelopmentBO
    {
        public string EvaluationDevGroupName { get; set; }
        public string EvaluationDevIndexCode { get; set; }
        public string EvaluationDevIndexName { get; set; }
        public int EvaluationDevGroupID { get; set; }
        public int EvaluationDevIndexID { get; set; }
        public int? Order { get; set; }
    }
}
