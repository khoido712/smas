﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SendSMSToParentBO
    {
        public int STT { get; set; }
        public int PupilFileID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public string PupilCode { get; set; }
        public int NumOfSubscriber { get; set; }

        public string Content { get; set; }

        public string ShortContent { get; set; }
        public bool NoNewMessage { get; set; }

        // Icon báo đã gửi 
        public bool IsSent { get; set; }
        public string IconTitle { get; set; }

        public string SMSLength { get; set; }

        public int NumSMS { get; set; }

        public string RemainSMSTxt { get; set; }

        public int RemainSMSCount { get; set; }

        public bool IsDisableNoSMS { get; set; }

        public string SMSTemplate { get; set; }
        // check box
        public bool? isCheck { get; set; }
        public bool? IsImport { get; set; }

        public int EducationLevelID { get; set; }

        public bool bImage { get; set; }

        public bool isNewMessage { get; set; }

        /// <summary>
        /// Hết tin nhắn dùng thử
        /// </summary>
        public bool ExpireTrial { get; set; }

        public List<string> LstPhoneReceiver { get; set; }
    }
}
