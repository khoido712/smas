﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TranscriptsVNENReportBO
    {
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int PupilID { get; set; }
        public bool fillPaging { get; set; }
    }
}
