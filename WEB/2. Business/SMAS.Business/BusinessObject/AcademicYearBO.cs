﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class AcademicYearBO
    {
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }

        public int SemesterID { get; set; }
        public string DisplayTitle { get; set; }
        public int Year { get; set; }
        public int EducationLevelID { get; set; }
        public DateTime? FirstSemesterStartDate { get; set; }
        public DateTime? FirstSemesterEndDate { get; set; }
        public DateTime? SecondSemesterStartDate { get; set; }
        public DateTime? SecondSemesterEndDate { get; set; }
        public SMAS.Models.Models.SchoolProfile School { get; set; }
        public int PupilID { get; set; }
        public string StorageName { get; set; }

        //ngay vao truong
        public DateTime? EnrolmentDate { get; set; }
        public string SchoolName { get; set; }
        public int EducationGradeId { get; set; }
        public string HeadMasterName { get; set; }

        public int? SuperVisingID { get; set; }
        public int? DistrictID { get; set; }
        public int EducationGrade { get; set; }
    }
}
