﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class OverallTestBO
    {
        public int? OverallTestID { get; set; }
        public int MonitoringBookID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int? HealthPeriodID { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public string HistoryYourSelf { get; set; }
        public int? OverallInternal { get; set; }
        public string DescriptionOverallInternal { get; set; }
        public int? OverallForeign { get; set; }
        public string DescriptionOverallForeign { get; set; }
        public int? SkinDiseases { get; set; }
        public string DescriptionSkinDiseases { get; set; }
        public bool? IsHeartDiseases { get; set; }
        public bool? IsBirthDefect { get; set; }
        public string Other { get; set; }
        public int? EvaluationHealth { get; set; }
        public string RequireOfDoctor { get; set; }
        public string Doctor { get; set; }
        public bool? IsDredging { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public bool? IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string UnitName { get; set; }
        public bool EnableDeleteOrEdit { get; set; }
        public int PupilProFileID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifineDate { get; set; }
        public int LogID { get; set; }
    }
}
