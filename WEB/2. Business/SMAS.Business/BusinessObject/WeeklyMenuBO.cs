﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class WeeklyMenuBO
    {
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public int? WeeklyMenuID { get; set; }
    }
}
