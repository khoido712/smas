﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamInputMarkAssignedBO
    {
        //PK
        public long ExamInputMarkAssignedID { get; set; }

        //FK
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public int SubjectID { get; set; }
        public int TeacherID { get; set; }
        public int? SchoolFacultyID { get; set; }

        public string SubjectName { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string SchoolFacultyName { get; set; }
        public string EthnicCode { get; set; }
        public string ExamRoomID { get; set; }
    }
}
