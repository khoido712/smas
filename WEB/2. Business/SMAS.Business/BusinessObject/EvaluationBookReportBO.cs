﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EvaluationBookReportBO
    {
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int SemesterID { get; set; }
        public int EducationLevelID { get; set; }
        public int Year { get; set; }
        public int AppliedLevel { get; set; }
    }
}
