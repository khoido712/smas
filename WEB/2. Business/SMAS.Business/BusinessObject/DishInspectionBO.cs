using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class DishInspectionBO
    {
        public int DishInspectionID { get; set; }
        public DateTime InspectedDate { get; set; }
        public int MenuType { get; set; }
        public int DailyCostOfChildren { get; set; }
        public int Expense { get; set; }
        public int EatingGroupID { get; set; }
        public int SchoolIDInspection { get; set; }
        public int DailyMenuID { get; set; }
        public bool IsActiveInspection { get; set; }
        public int NumberOfChildren { get; set; }
        public int OtherServiceID { get; set; }
        public string OtherServiceName { get; set; }
        public string Note { get; set; }
        public int PriceService { get; set; }
        public int SchoolIDService { get; set; }
        public DateTime EffectDate { get; set; }
        public bool IsActiveService { get; set; }
        public int Price { get; set; }
        // Phan thong tin thuc pham va nhom an
        public string EatingGroupName { get; set; }
        public int FoodID { get; set; }
        public string FoodName { get; set; }
        public decimal Weight { get; set; }
        public int PricePerOnce { get; set; }
        public int FoodGroupID { get; set; }
    }
}
