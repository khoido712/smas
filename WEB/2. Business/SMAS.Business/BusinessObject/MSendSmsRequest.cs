﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MSendSmsRequest
    {
        public int SchoolID { get; set; }

        public string SchoolName { get; set; }

        public int Grade { get; set; }

        public int AcademicYearID { get; set; }

        public int EmployeeID { get; set; }

        public int Role { get; set; }

        public int EducationlevelID { get; set; }

        public List<string> Contents { get; set; }

        public List<string> ShortContents { get; set; }

        public List<int> PupilIDs { get; set; }

        public int TypeID { get; set; }

        public int ClassID { get; set; }
    }
}
