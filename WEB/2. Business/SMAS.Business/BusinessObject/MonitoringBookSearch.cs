using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class MonitoringBookSearch
    {        
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> AcademicYearID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<int> MonitoringBookID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public Nullable<DateTime> BrithDate { get; set; }
        public int Sex { get; set; }     
        public string ClassName { get; set; }
        public int? HealthPeriodID { get; set; }
        public System.Nullable<System.DateTime> MonitoringDate { get; set; }				

        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public bool IsAction { get; set; }
        public string PupilShortName { get; set; }


    }
}
