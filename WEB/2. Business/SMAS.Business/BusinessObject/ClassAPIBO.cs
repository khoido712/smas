﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ClassAPIBO
    {
        public string SchoolID { get; set; }//Ma truong
        public string LopID { get; set; }//Ma lop cua so
        public string ClientLopID { get; set; }//Ma lop cua truong
        public string TenLop { get; set; }
        public int Khoi { get; set; }
        public string PGDID { get; set; }//Ma PGD
        public bool LopHoc2Buoi { get; set; }
    }
    public class PupilAPIBO
    {
        public string HocSinhID { get; set; }//Ma HS cua so
        public string SchoolID { get; set; }//Ma truong cua so
        public string ClientHocSinhID { get; set; }//Ma HS cua truong
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool GioiTinh { get; set; }
        public string NgaySinh { get; set; }
        public string NoiSinh { get; set; }
        public string DanTocID { get; set; }//Ma dan toc
        public string TonGiaID { get; set; }//Ma ton giao
        public string KhuyetTatID { get; set; }//Ma khuyet tat
        public string SDT { get; set; }
        public string SDTCha { get; set; }
        public string SDTMe { get; set; }
        public string TenCha { get; set; }
        public string TenMe { get; set; }
        public string CMND { get; set; }
        public bool HocSinhChuaDuocCapMaSo { get; set; }//HS chua duoc cap ma cua so
    }
    public class PupilByClassAPIBO
    {
        public string HocSinhID { get; set; }//ma cua so
        public string LopID { get; set; }//Ma lop cua so
        public string LyDoBienDong { get; set; }
        public string NgayBienDong { get; set; }
    }
    public class PupilBD_APIBO
    {
        public string HocSinhID { get; set; }
        public string LopID { get; set; }
        public int LoaiBienDongID { get; set; }
        public string LyDoBienDong { get; set; }
        public string NgayBienDong { get; set; }
    }
}
