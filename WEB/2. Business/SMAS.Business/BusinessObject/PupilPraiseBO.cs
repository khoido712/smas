using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilPraiseBO
    {
        public int PupilPraiseID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int PraiseTypeID { get; set; }
        public System.DateTime PraiseDate { get; set; }
        public int PraiseLevel { get; set; }
        public Nullable<bool> IsRecordedInSchoolReport { get; set; }
        public Nullable<int> Semester { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public decimal? ConductMark { get; set; }
    }
}
