﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SupervisingDeptBO
    {
        public int SupervisingDeptID { get; set; }
        public string SupervisingDeptCode { get; set; }
        public string SupervisingDeptName { get; set; }
        public int ParentID { get; set; }
        public short HierachyLevel { get; set; }
        public int AreaID { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public string Traversal_Path { get; set; }
        public string Address { get; set; }
        public DateTime EstablishedDate { get; set; }
        public string SupervisingDeptUserName { get; set; }
        public int AdminID { get; set; }
        public bool IsActiveSMAS { get; set; }
        public int SMSActiveType { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
