﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
   public class ClassProfileBO
    {
       public ClassProfile classProfile {get;set;}
       public int Type {get;set;}
       public string strTitle { get; set; }
    }
}
