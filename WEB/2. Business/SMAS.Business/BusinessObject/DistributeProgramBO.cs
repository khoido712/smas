﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DistributeProgramBO
    {
        public long DistributeProgramID { get; set; }
        public int? DistributeProgramOrder { get; set; }
        public string LessonName { get; set; }
        public string Note { get; set; }
        public int SubjectID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolWeekID { get; set; }
        public string WeekName { get; set; }
        public DateTime? FromDate {get;set;}
        public DateTime? ToDate { get; set; }
        public int OrderWeek { get; set; }

        public int AdcademicYearID { get; set; }
        public int schoolID { get; set; }

        public int ClassID { get; set; }
        public int? AssignSubjectID { get; set; }
    }
}
