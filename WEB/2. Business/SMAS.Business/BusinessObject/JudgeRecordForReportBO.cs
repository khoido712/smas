using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>11/12/2012 4:03:06 PM</date>
    /// </summary>
   public class JudgeRecordForReportBO
    {
        #region Member Variables
        public int? JudgeRecordID { get; set; }

        public int? ClassID { get; set; }

        public int? SchoolID { get; set; }

        public int? AcademicYearID { get; set; }

        public int? AppliedLevel { get; set; }

        public int? SubjectID { get; set; }

        public int? MarkTypeID { get; set; }

        public System.Int32 Year { get; set; }

        public System.Nullable<System.Int32> Semester { get; set; }

        public System.Nullable<System.Int32> EducationLevelID { get; set; }

        public System.String Judgement { get; set; }

        public System.String ReTestJudgement { get; set; }

        public System.Nullable<System.DateTime> CreatedDate { get; set; }

        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        public int? PupilID { get; set; }

        public int? Status { get; set; }

        public string PupilCode { get; set; }

        public string FullName { get; set; }

        public string Name { get; set; }

        public DateTime BirthDate { get; set; }

        public bool H11 { get; set; }

        public bool H12 { get; set; }

        public bool H13 { get; set; }

        public bool H14 { get; set; }

        public bool H15 { get; set; }

        public bool H21 { get; set; }

        public bool H22 { get; set; }

        public bool H23 { get; set; }

        public bool H24 { get; set; }

        public bool H25 { get; set; }

        public int? OrderInClass { get; set; }

        public string MarkSemester1 { get; set; }

        public string MarkSemester2 { get; set; }

        public bool ExemptedSuject { get; set; }

        public int SupervisingDeptID { get; set; }

        public bool IsShowMark { get; set; }
        #endregion


        #region Properties
        #endregion

        #region Functions
        #endregion


    }
}
