﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ClassificationCriteriaDetailBO
    {
        public int ClassificationCriteriaDetailID { get; set; }

        public int ClassificationCriteriaID { get; set; }

        public int Month { get; set; }

        public Nullable<decimal> IndexValue { get; set; }

        public string IndexValueText { get; set; }

        public int ClassificationLevelHealthID { get; set; }	
    }
}
