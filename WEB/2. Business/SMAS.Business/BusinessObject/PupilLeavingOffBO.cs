using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilLeavingOffBO
    {
        public int PupilLeavingOffID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int LeavingReasonID { get; set; }
        public int Semester { get; set; }
        public System.DateTime LeavingDate { get; set; }

        public string Description { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string DisplayName { get; set; }
        public string DisplayTile { get; set; }
        public string LeavingReason { get; set; }

        public bool IsCombinedClass { get; set; }
        public bool? IsDisabled { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }
    		

    }
}
