﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReportEvalutionComments
    {
        public int AppliedLevel { get; set; }       
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }      
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public int EducationLevelID { get; set; }
    }
}
