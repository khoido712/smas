﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class YearBO
    {
        public int YearID { get; set; }
        public string DisplayYear { get; set; }
    }
}
