﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReportSummedEvaluationBO
    {
        public long SummedEvaluationID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public int SchoolID { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public int EvaluationID { get; set; }
        public Nullable<int> PeriodicEndingMark { get; set; }
        public string EndingEvaluation { get; set; }
        public string EndingComments { get; set; }
        public Nullable<int> RetestMark { get; set; }
        public string EvaluationReTraining { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
    }
}
