﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class InfoEvaluationPupil
    {
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int AcademicYear { get; set; }
        public int PupilID { get; set; }
        public string FullName { get; set; }

        /// <summary>
        /// Trang thai hoc sinh (dang hoc, chuyen truong...)
        /// </summary>
        public int ProfileStatus { get; set; }

        /// <summary>
        /// chieu cao
        /// </summary>
        public decimal? Height { get; set; }
        /// <summary>
        /// can nang
        /// </summary>
        public decimal? Weight { get; set; }

        /// <summary>
        /// trinh trang suc khoe
        /// </summary>
        public int? EvaluationHealth { get; set; }

        public DateTime? MonitoringDate { get; set; }

        /// <summary>
        /// so ngay nghi
        /// </summary>
        public int NumberAbsence{ get; set; }

        /// <summary>
        /// so ngay nghi co phep
        /// </summary>
        public int HasAccepted { get; set; }

        /// <summary>
        /// so ngay ngi khong phep
        /// </summary>
        public int NoAccepted { get; set; }

        /// <summary>
        /// danh sach nhan xet theo thang cua hoc sinh
        /// </summary>
        public List<InfoEvaluationMonth> lstEvalutionMonth = null;

        /// <summary>
        /// ket qua danh gia cua hoc sinh theo tung hoc ky cua GVBM
        /// </summary>
        public List<ResultEvaluationSubject> lstResultEvalution = null;

        /// <summary>
        /// ket qua danh gia cua hoc sinh theo tung hoc ky cua GBCN
        /// </summary>
        public List<ResultEvaluationHeadTeacher> lstResultHeadTeacher = null;

        /// <summary>
        /// ket qua khen thuong cua hoc sinh
        /// </summary>
        public List<ResultRewardEvaluation> lstResultRewardEvaluation = null;
        public int? HealthPeriodID { get; set; }
    }
}
