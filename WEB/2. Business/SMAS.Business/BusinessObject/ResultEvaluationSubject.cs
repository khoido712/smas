﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ResultEvaluationSubject
    {
        /// <summary>
        /// thong tin hoc ki 
        /// </summary>
        public int SemesterSubject { get; set; }

        /// <summary>
        /// Diem danh gia
        /// </summary>
        public int? PeriodicEndingMark { get; set; }

        /// <summary>
        /// kiem chua neu HT : true, CHT: flase
        /// </summary>
        public string CheckResult { get; set; }

        public ResultEvaluationSubject()
        {
            SemesterSubject = 0;
            PeriodicEndingMark = 0;
            CheckResult = string.Empty;
        }
    }
}
