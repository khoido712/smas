using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>07/01/2013 10:07:32 AM</date>
    /// </summary>
  public class PupilRepeatedBO
    {
        #region Member Variables
        public int PupilID { get; set; }
        public int PupilRepeatedID { get; set; }
        public int OldClassID { get; set; }
        public string OldClassName { get; set; }

        public int RepeatedClassID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }
        public int Year { get; set; }
        public int SchoolID { get; set; }

        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public DateTime? BirthDate { get; set; }
        public int Genre { get; set; }
        public string GenreName { get; set; }
        public string Name { get; set; }
        public int? OrderInClass { get; set; }
        public SMAS.Models.Models.PupilProfile PupilProfile { get; set; }
        public SMAS.Models.Models.PupilOfClass PupilOfClass { get; set; }
        public SMAS.Models.Models.PupilRepeated PupilRepeated { get; set; }

        #endregion

    }
}
