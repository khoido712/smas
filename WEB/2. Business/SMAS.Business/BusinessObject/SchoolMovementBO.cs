using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SchoolMovementBO
    {
        public int SchoolMovementID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int Semester { get; set; }
        public Nullable<int> MovedToClassID { get; set; }
        public Nullable<int> MovedToSchoolID { get; set; }
        public string MovedToClassName { get; set; }
        public string MovedToSchoolName { get; set; }
        public System.DateTime MovedDate { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int? EthnicID { get; set; }
        public int Genre { get; set; }
    }
}
