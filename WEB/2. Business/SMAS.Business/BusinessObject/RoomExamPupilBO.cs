﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RoomExamPupilBO
    {
        public long? ExamRoomID { get; set; }
        public int ExamPupilCount { get; set; }
    }
}
