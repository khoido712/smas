﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DeclareEvaluationGroupBO
    {
        public int DeclareEvaluationGroupID { get; set; }
        public int CurrentSchoolID { get; set; }
        public int CurrentAcademicYearID { get; set; }
        public int CurrentClassID { get; set; }
        public int EvaluationGroupID { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModfiedDate { get; set; }
        public int? OrderID { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public int EducationLevelID { get; set; }
        public string EvaluationDevelopmentGroupName { get; set; }
    }
}
