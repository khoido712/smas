﻿namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// MasterBook
    /// </summary>
    /// <author>
    /// dungnt77
    /// </author>
    /// <remarks>
    /// 20/11/2012   3:56 PM
    /// </remarks>
   public class MasterBook
    {
        #region Member Variables

       public int EducationLevelID { get; set; }

       public int ClassID { get; set; }

       public int Semester { get; set; }

       public int AcademicYearID { get; set; }

       public int SchoolID { get; set; }

       public int AppliedLevel { get; set; }
       
        public string FormatFile { get; set; }
        public int ViewBooklet { get; set; }
        public bool chkAllPupil { get; set; }
        public int PupilNumber { get; set; }
        public bool isGVBM { get; set; }
        public bool isBorderMark { get; set; }
        public bool chkAllSubject { get; set; }
        public string arrSubjectID { get; set; }
        public int InterviewMark { get; set; }
        public int WritingMark { get; set; }
        public int TwiceCoeffiecientMark { get; set; }
        public bool rdo1 { get; set; }
        public bool rdo2 { get; set; }
        public bool chkPRAddress { get; set; }
        public bool chkTRAddress { get; set; }
        public bool chkVillageID { get; set; }
        public bool chkCommuneID { get; set; }
        public bool chkDistrictID { get; set; }
        public bool chkProvinceID { get; set; }
        public bool fillPageNumber { get; set; } 
        #endregion Member Variables

        #region Constructors

        #endregion Constructors 

        #region Properties

        #endregion Properties

        #region Functions

        #endregion Functions
    }
}