﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EvaluationCommentsBO
    {
        public long EvaluationCommentsID { get; set; }
        public int SchoolID { get; set; }
        public int EvaluationID { get; set; }
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public string SubjectName { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public int PupilID { get; set; }
        public int OrderInClass { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public string Comment4 { get; set; }
        public string Comment5 { get; set; }
        public int? PiriodicEndingMark { get; set; }
        public string EvaluationEnding { get; set; }
        public int? RetestMark { get; set; }
        public string EvaluationReTraining { get; set; }
        public int Status { get; set; }
        public string CommentEnding { get; set; }
        public string PupilCode { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public long SummedEvaluationID { get; set; }
        public long SummedEndingEvaluationID { get; set; }
        public string NoteInsert { get; set; }
        public string NoteUpdate { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public DateTime? MaxCreateTime { get; set; }
        public DateTime? MaxUpdateTime { get; set; }
        public int? OrderInSubject { get; set; }
        /// <summary>
        /// chuoi nhan xet theo thang
        /// </summary>
        public string EvaluationMonth { get; set; }
    }
}
