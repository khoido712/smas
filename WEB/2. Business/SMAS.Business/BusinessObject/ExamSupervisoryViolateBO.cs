﻿using SMAS.Business.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamSupervisoryViolateBO
    {
        //PK
        public long ExamSupervisoryViolateID { get; set; }

        //FK
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long ExamSupervisoryID { get; set; }
        public int SubjectID { get; set; }
        public long ExamRoomID { get; set; }
        public int? ExamViolatetionTypeID { get; set; }

        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? BirtDate { get; set; }
        public bool Genre { get; set; }
        public string EthnicCode { get; set; }
        public string SubjectName { get; set; }
        public string ExamRoomCode { get; set; }
        public string ContentViolated { get; set; }
        public string ExamViolatetionTypeResolusion { get; set; }
        public string ExamGroupName { get; set; }
        public string GenreName
        {
            get
            {
                return Genre == true ? GlobalConstants.GENRE_MALE_TITLE :GlobalConstants.GENRE_FEMALE_TITLE;
            }
        }
    }
}
