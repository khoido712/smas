﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PupilRepeaterBO
    {
        public int PupilID { get; set; }
        public string PupilFullName { get; set; }
        public string PupilShortName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }

        /// <summary>
        /// Hoc luc cap 2,3. Cap 1 la XLGD
        /// </summary>
        public int CapacityID { get; set; }

        /// <summary>
        /// hanh kiem
        /// </summary>
        public int ConductID { get; set; }

        public int Semester { get; set; }

        /// <summary>
        /// Khoi hoc
        /// </summary>
        public int EducationLevelID { get; set; }

        // order theo hoc sinh
        public int? OrderID { get; set; }

        public int? OrderClass { get; set; }

        public string PupilCode { get; set; }
        public int? Capacity { get; set; }
        public int? CapacityAdd { get; set; }
        public int? Quality { get; set; }
        public int? QualityAdd { get; set; }
        public int? RateEndYear { get; set; }
        public int? RateAdd { get; set; }
        public bool? IsVnenClass { get; set; }
        public int? IsCommenting { get; set; }
    }
}
