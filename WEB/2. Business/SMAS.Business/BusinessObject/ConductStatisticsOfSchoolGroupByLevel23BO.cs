﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public partial class ConductStatisticsOfSchoolGroupByLevel23BO
    {
        public int? EducationLevelID { get; set; }
        public string EducationLevelName { get; set; }
        public int? TotalGood { get; set; }
        public int? TotalFair { get; set; }
        public int? TotalNormal { get; set; }
        public int? TotalWeak { get; set; }
        public int? TotalPupil { get; set; }
        public int? TotalAboveAverage { get; set; }
        public decimal? PercentGood { get; set; }
        public decimal? PercentFair { get; set; }
        public decimal? PercentNormal { get; set; }
        public decimal? PercentWeak { get; set; }
        public decimal? PercentAboveAverage { get; set; }
    }
}
