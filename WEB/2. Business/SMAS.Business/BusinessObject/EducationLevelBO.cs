﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EducationLevelBO
    {
        public int EducationLevelID { get; set; }
        public string Resolution { get; set; }
        public bool? IsSpecializedSubject { get; set; }
        public int ClassProfileID { get; set; }
        public string DisplayName { get; set; }
    }
}
