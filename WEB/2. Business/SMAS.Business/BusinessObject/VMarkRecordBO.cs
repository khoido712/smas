﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class VMarkRecordBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectID { get; set; }
        public Nullable<int> Semester { get; set; }
        public decimal Mark { get; set; }
        public int? AppliedType { get; set; }
        public int Genre { get; set; }
        public int? EthnicID { get; set; }
        public bool? SubjectVNEN { get; set; }
    }
}
