﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class Result
    {
        public Result(string code, string message)
        {
            this.ErrorCode = code;
            this.Message = message;
        }

        public Result()
        {
        }

        public string ErrorCode { get; set; }

        public string Message { get; set; }


        public string CustomerInfo { get; set; }
    }
}
