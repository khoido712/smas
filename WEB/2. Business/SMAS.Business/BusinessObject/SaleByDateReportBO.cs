﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SaleByDateReportBO
    {
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string FullName { get; set; }
        public string CBCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public decimal? TBCT { get; set; } public decimal? DTCT { get; set; }
        public decimal? TB1 { get; set; } public decimal? DT1 { get; set; }
        public decimal? TB2 { get; set; } public decimal? DT2 { get; set; }
        public decimal? TB3 { get; set; } public decimal? DT3 { get; set; }
        public decimal? TB4 { get; set; } public decimal? DT4 { get; set; }
        public decimal? TB5 { get; set; } public decimal? DT5 { get; set; }
        public decimal? TB6 { get; set; } public decimal? DT6 { get; set; }
        public decimal? TB7 { get; set; } public decimal? DT7 { get; set; }
        public decimal? TBW1 { get; set; } public decimal? DTW1 { get; set; }
        public decimal? TB8 { get; set; } public decimal? DT8 { get; set; }
        public decimal? TB9 { get; set; } public decimal? DT9 { get; set; }
        public decimal? TB10 { get; set; } public decimal? DT10 { get; set; }
        public decimal? TB11 { get; set; } public decimal? DT11 { get; set; }
        public decimal? TB12 { get; set; } public decimal? DT12 { get; set; }
        public decimal? TB13 { get; set; } public decimal? DT13 { get; set; }
        public decimal? TB14 { get; set; } public decimal? DT14 { get; set; }
        public decimal? TBW2 { get; set; } public decimal? DTW2 { get; set; }
        public decimal? TB15 { get; set; } public decimal? DT15 { get; set; }
        public decimal? TB16 { get; set; } public decimal? DT16 { get; set; }
        public decimal? TB17 { get; set; } public decimal? DT17 { get; set; }
        public decimal? TB18 { get; set; } public decimal? DT18 { get; set; }
        public decimal? TB19 { get; set; } public decimal? DT19 { get; set; }
        public decimal? TB20 { get; set; } public decimal? DT20 { get; set; }
        public decimal? TB21 { get; set; } public decimal? DT21 { get; set; }
        public decimal? TBW3 { get; set; } public decimal? DTW3 { get; set; }
        public decimal? TB22 { get; set; } public decimal? DT22 { get; set; }
        public decimal? TB23 { get; set; } public decimal? DT23 { get; set; }
        public decimal? TB24 { get; set; } public decimal? DT24 { get; set; }
        public decimal? TB25 { get; set; } public decimal? DT25 { get; set; }
        public decimal? TB26 { get; set; } public decimal? DT26 { get; set; }
        public decimal? TB27 { get; set; } public decimal? DT27 { get; set; }
        public decimal? TB28 { get; set; } public decimal? DT28 { get; set; }
        public decimal? TBW4 { get; set; } public decimal? DTW4 { get; set; }
        public decimal? TB29 { get; set; } public decimal? DT29 { get; set; }
        public decimal? TB30 { get; set; } public decimal? DT30 { get; set; }
        public decimal? TB31 { get; set; } public decimal? DT31 { get; set; }
    }
}
