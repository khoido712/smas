﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ImportFromOtherSystemBO
    {
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public bool IsComment { get; set; }
        public int PupilID { get; set; }
        public int Semester { get; set; }
        public object M1 { get; set; }
        public object M2 { get; set; }
        public object M3 { get; set; }
        public object M4 { get; set; }
        public object M5 { get; set; }
        public object P1 { get; set; }
        public object P2 { get; set; }
        public object P3 { get; set; }
        public object P4 { get; set; }
        public object P5 { get; set; }
        public object V1 { get; set; }
        public object V2 { get; set; }
        public object V3 { get; set; }
        public object V4 { get; set; }
        public object V5 { get; set; }
        public object V6 { get; set; }
        public object HK { get; set; }
        public object TBK1 { get; set; }
        public object TBK2 { get; set; }
        private List<object> lstMVnedu;
        private List<object> lstPVnedu;
        private List<object> lstVVnedu;

        public List<object> lstM
        {
            get
            {
                if (lstMVnedu != null)
                {
                    return lstMVnedu;
                }
                else
                {
                    return new List<object> { M1, M2, M3, M4, M5 };
                }
            }
            set
            {
                lstMVnedu = value;
            }
        }

        public List<object> lstP
        {
            get
            {
                if (lstPVnedu != null)
                {
                    return lstPVnedu;
                }
                else
                {
                    return new List<object> { P1, P2, P3, P4, P5 }; 
                }
            }
            set
            {
                lstPVnedu = value;
            }
        }

        public List<object> lstV
        {
            get
            {
                if (lstVVnedu != null)
                {
                    return lstVVnedu;
                }
                else
                {
                    return new List<object> { V1, V2, V3, V4, V5, V6 }; 
                }
            }
            set
            {
                lstVVnedu = value;
            }
        }
        public object TB
        {
            get
            {
                if (Semester == 1)
                {
                    return TBK1;
                }
                else
                {
                    return TBK2;
                }
            }
        }

        public object TBCN { get; set; }
    }
}
