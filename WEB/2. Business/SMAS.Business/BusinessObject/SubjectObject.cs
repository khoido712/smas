﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SubjectObject
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public Nullable<int> OrderInSubject { get; set; }
    }
}
