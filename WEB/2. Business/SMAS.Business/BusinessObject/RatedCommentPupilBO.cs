﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RatedCommentPupilBO
    {
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationID { get; set; }
        public int? isCommenting { get; set; }
        public Nullable<int> PeriodicMiddleMark { get; set; }
        public Nullable<int> MiddleEvaluation { get; set; }
        public string MiddleEvaluationName { get; set; }
        public Nullable<int> PeriodicEndingMark { get; set; }
        public Nullable<int> EndingEvaluation { get; set; }
        public string EndingEvaluationName { get; set; }
        public string PeriodicMiddleJudgement { get; set; }
        public string PeriodicEndingJudgement { get; set; }
        public Nullable<int> CapacityMiddleEvaluation { get; set; }
        public string CapacityMiddleEvaluationName { get; set; }
        public Nullable<int> CapacityEndingEvaluation { get; set; }
        public string CapacityEndingEvaluationName { get; set; }
        public Nullable<int> QualityMiddleEvaluation { get; set; }
        public string QualityMiddleEvaluationName { get; set; }
        public Nullable<int> QualityEndingEvaluation { get; set; }
        public string QualityEndingEvaluationName { get; set; }
        public string Comment { get; set; }
        public Nullable<int> RetestMark { get; set; }
        public Nullable<int> EvaluationRetraning { get; set; }
        public int LastDigitSchoolID { get; set; }
        public string FullNameComment { get; set; }
        public int LogChangeID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public int? EthnicID { get; set; }
        public int Genre { get; set; }
        public int EducationLevelID { get; set; }
        public string SMSContent { get; set; }
    }
}
