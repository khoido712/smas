﻿using SMAS.Business.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamPupilBO
    {
        public long ExamPupilID { get; set; }
        public long ExaminationsID { get; set; }
        public string ExaminationName { get; set; }
        public string SchedulesExam { get; set; }
        public long ExamGroupID { get; set; }
        public string ExamGroupCode { get; set; }
        public string ExamGroupName { get; set; }
        public string ExamineeCode { get; set; }
        public int SchoolID { get; set; }
        public int EducationLevelID { get; set; }
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string PupilFullName { get; set; }
        //public byte[] Image { get; set; }
        public DateTime BirthDate { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }
        public string EthnicCode { get; set; }
        public int EducationLevel { get; set; }
        public int ClassID { get; set; }
        public bool? IsVNENClass { get; set; }
        public string ClassName { get; set; }
        public long? ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
        public string ExamineeNumber { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public long ExamPupilAbsenceID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string ExamDetachableBagCode { get; set; }
        public int? OrderInClass { get; set; }
        public int Status { get; set; }
        public int? ClassOrder { get; set; }
        public string ExamBagCode { get; set; }
        public long ExamCandenceBagID { get; set; }
        public string ExamCandenceBagCode { get; set; }
        public long ExamDetachableBagID { get; set; }
        public string DisplayGenre
        {
            get
            {
                return Genre.GetValueOrDefault() == GlobalConstants.GENRE_MALE ? GlobalConstants.GENRE_MALE_TITLE :
                                (this.Genre.GetValueOrDefault() == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE :
                                (this.Genre.GetValueOrDefault() == 2 ? "Không xác định" : String.Empty));
            }
        }

        public List<ListErrorExamBO> ListErrorMessage { get; set; }
    }

    public class ListErrorExamBO
    {
        public string PupilCode { get; set; }
        public string PupilName { get; set; }
        public string Description { get; set; }
        //public string SheetName { get; set; }
    }
}
