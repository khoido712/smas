﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EmployeeSupervisingDeptBO
    {
        /// <summary>
        /// ID Nhân viên, giáo viên
        /// </summary>
        
        public int EmployeeID { get; set; }

        /// <summary>
        /// Tên nhân viên
        /// </summary>
        
        public string EmployeeName { get; set; }

        
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// Tên phòng ban
        /// </summary>
        
        public string SupervisingDeptName { get; set; }

        /// <summary>
        /// id phong ban la don vi cha gan nhat
        /// </summary>
        
        public int ParentSupervisingDeptID { get; set; }

        /// <summary>
        /// ten don vi phong ban cha gan nhat
        /// </summary>
        
        public string ParentSupervisingDeptName { get; set; }

        /// <summary>
        /// Mã cán bộ
        /// </summary>
        
        public string EmployeeCode { get; set; }

        //Số ĐT
        
        public string EmployeeMobile { get; set; }
    }
}
