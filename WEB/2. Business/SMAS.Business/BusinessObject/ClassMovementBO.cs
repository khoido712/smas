﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ClassMovementBO
    {
        public int ClassMovementID { get; set; }
        public int PupilID { get; set; }
        public int FromClassID { get; set; }
        public int ToClassID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public Nullable<System.DateTime> MovedDate { get; set; }
        public string Reason { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string oldClass { get; set; }
        public string newClass { get; set; }
        public DateTime? BirthDate { get; set; }
        public int Genre { get; set; }
        public int ClassOrder { get; set; }
        public string ClassName { get; set; }
        public int PupilOrder { get; set; }
        public string Name { get; set; }
        public int? EthnicID { get; set; }
    }
}
