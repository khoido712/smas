﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class DailyMenuDetailDishInspectionBO
    {
        public int DailyMenuDetailID { get; set; }
        public int MealCatID { get; set; }
        public string MealName { get; set; }
        public int? DishID { get; set; }
        public int DailyMenuID { get; set; }
        public string DishName { get; set; }
        public string Dish_Name { get; set; }
        public MealCat MealCat { get; set; } 
    }
}
