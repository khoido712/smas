﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SubjectByClassBO
    {
        public int SubjectID { get; set; }

        public List<int> listClassID { get; set; }
    }
}
