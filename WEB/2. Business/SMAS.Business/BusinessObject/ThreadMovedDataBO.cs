﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ThreadMovedDataBO
    {
        public int SchoolProfileID { get; set; }
        public int ProvinceID { get; set; }         
        public string ProvinceName { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }        
        public string SchoolName { get; set; }
        public string SchoolCode { get; set; }
        public int EducationGrade { get; set; }      
        public string SupervisingDeptName { get; set; }        
        public string Resolution { get; set; }
        public string UserName { get; set; }
        public int AcademicYearId { get; set; }
        public int Year { get; set; }
    }
}
