﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilAbsenceBO
    {
        public int PupilID { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public DateTime? AbsentDate { get; set; }
        public Nullable<bool> IsAccepted { get; set; }
        public int Section { get; set; }
        public int Last2digitNumberSchool { get; set; }
        /// <summary>
        /// sang
        /// </summary>
        public string Section_M { get; set; }
        /// <summary>
        /// chieu
        /// </summary>
        public string Section_A { get; set; }
        /// <summary>
        /// toi
        /// </summary>
        public string Section_N { get; set; }

        public string SectionName { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int AppliedLevelID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EducationLevelID { get; set; }
        public string EducationName { get; set; }
        public int? OrderNumber { get; set; }
        public int? OrderInClass { get; set; }
        public int Status { get; set; }
        public int? ClassSection { get; set; }
    }
}
