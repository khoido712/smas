﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class VSummedUpRecordBO
    {
        public long SummedUpRecordID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubjectID { get; set; }
        public int? IsCommenting { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> PeriodID { get; set; }
        public Nullable<decimal> SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public int? AppliedType { get; set; }
        public decimal? ReTestMark { get; set; }
        public int Genre { get; set; }
        public int? EthnicID { get; set; }
        public bool? subjectVNEN { get; set; }
    }
}
