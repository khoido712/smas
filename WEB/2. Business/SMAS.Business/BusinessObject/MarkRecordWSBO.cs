﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class MarkRecordWSBO
    {
        public int? PupilID { get; set; }
        public decimal? Mark { get; set; }
        public int? SubjectID { get; set; }
        public int? MarkTypeID { get; set; }        
        public int? SubjectOrderNumber { get; set; }
        public int? MarkOrderNumber { get; set; }
        public string Title { get; set; }
        public string SubjectName { get; set; }
        public DateTime? MarkedDate { get; set; }

        public DateTime? MarkredDatePrimary { get; set; }
    }
}
