﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
   public class MonthEvaluationCommentsBO
    {
        public int MonthID { get; set; }
        public string MonthName { get; set; }
        public bool Selected { get; set; }
        public int Year { get; set; }
    }
}
