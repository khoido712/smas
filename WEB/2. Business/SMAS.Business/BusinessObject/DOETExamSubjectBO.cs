﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExamSubjectBO
    {
        public int ExamSubjectID { get; set; }
        public int ExaminationsID { get; set; }
        public int ExamGroupID { get; set; }
        public int DOETSubjectID { get; set; }
        public string DOETSubjectCode { get; set; }
        public string SchedulesExam { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string DOETSubjectName { get; set; }
    }
}
