﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PupilTransferBO
    {
        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public int? Semester { get; set; }

        public int? StudyingJudgementID { get; set; }

        public string EndingEvaluation { get; set; }

        public int? RateAdd { get; set; }
    }
}
