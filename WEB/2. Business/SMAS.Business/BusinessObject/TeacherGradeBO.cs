﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TeacherGradeBO
    {
        public DateTime? CreatedDate { get; set; }
        public string Description { get; set; }
        public string GradeResolution { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SynchronizeID { get; set; }
        public int TeacherGradeID { get; set; }
        public int Order { get; set; }
    }
}
