﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETSubjectBO
    {
        public int DOETSubjectID { get; set; }
        public string DOETSubjectName { get; set; }
        public string DOETSubjectCode { get; set; }
        public string AppliedLevel { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<System.DateTime> SyncTime { get; set; }
        public string SyncSourceId { get; set; }
        public MapSource<int> ListMapSource { get; set; }
    }
}
