﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class EmployeeBO
    {
        [DisplayName("Ngày sinh")]
        public DateTime? BirthDate { get; set; }
        public int? ContractID { get; set; }
        
        public string ContractName { get; set; }
        [DisplayName("Hình thức hợp đồng")]
        public int? ContractTypeID { get; set; }
        public int ContactGroupID { get; set; }
        [DisplayName("Hình thức hợp đồng")]
        public string ContractTypeName { get; set; }
         [DisplayName("Chức danh")]
        public string Description { get; set; }
        [DisplayName("Địa chỉ Email")]
        public string Email { get; set; }
        [DisplayName("Mã cán bộ")]
        public string EmployeeCode { get; set; }
        public int EmployeeID { get; set; }
        public int EmployeeStatus { get; set; }
        [DisplayName("Tình trạng CB")]
        public string EmployeeStatusName { get; set; }
        public int EmployeeType { get; set; }
        public int SubjectID { get; set; }
        [DisplayName("Họ và tên")]
        public string FullName { get; set; }
        [DisplayName("Giới tính")]
        public bool Genre { get; set; }
        [DisplayName("Giới tính")]
        public string GenreName { get; set; }
        public int? GraduationLevelID { get; set; }
        public string GraduationLevelName { get; set; }
         [DisplayName("Trình độ tin học")]
        public int? ITQualificationLevelID { get; set; }
         [DisplayName("Trình độ tin học")]
        public string ITQualificationLevelName { get; set; }
        [DisplayName("Ngày vào biên chế")]
        public DateTime? JoinedDate { get; set; }
        [DisplayName("Số ĐTDĐ")]
        public string Mobile { get; set; }
        public string Name { get; set; }
        [DisplayName("Trình độ văn hóa")]
        public int? QualificationLevelID { get; set; }
        [DisplayName("Trình độ văn hóa")]
        public string QualificationLevelName { get; set; }
        [DisplayName("Hình thức đào tạo chính")]
        public int? QualificationTypeID { get; set; }
        //Add 
        [DisplayName("Trình độ CMNV cao nhất")]
        public int? TrainingLevelID { get; set; }
        public int? StaffPositionID { get; set; }
        public int? ParentID { get; set; }
        public int? EducationGrade { get; set; }
        [DisplayName("Nơi kết nạp Đoàn")]
        public string YouthLeagueJoinedPlace { get; set; }
        [DisplayName("Nơi kết nạp Đảng")]
        public string CommunistPartyJoinedPlace { get; set; }
        [DisplayName("Ngày vào CĐ")]
        public DateTime? SyndicateDate { get; set; }
         [DisplayName("Bí danh")]
        public string Alias { get; set; }
        [DisplayName("Đã tập huấn KN sống")]
        public bool SoftSkillTrained { get; set; }
        [DisplayName("Đã tập huấn KN sống")]
        public string SoftSkillTrainedName { get; set; }
        [DisplayName("Cơ quan tuyển dụng")]
        public string EmployedBy { get; set; }
        [DisplayName("Nghề nghiệp khi được tuyển dụng")]
        public string FirstJob { get; set; }
        [DisplayName("S.tiết thực dạy trên tuần")]
        public int? ThourPerWeek { get; set; }
        [DisplayName("S.tiết thực k.nhiệm trên tuần")]
        public int? CoHourPerWeek { get; set; }
        [DisplayName("Dạy HSKT học hòa nhập")]
        public bool DisablePupilTeaching { get; set; }
        [DisplayName("Dạy HSKT học hòa nhập")]
        public string DisablePupilTeachingName { get; set; }
        [DisplayName("Ghi chú khác")]
        public string Note { get; set; }
        [DisplayName("Thuê trọ ngoài")]
        public bool? MotelRoomOutsite { get; set; }
        [DisplayName("Thuê trọ ngoài")]
        public string MotelRoomOutsiteName { get; set; }
        [DisplayName("Đang ở nhà công vụ giáo vụ")]
        public bool? TeacherDuties { get; set; }
        [DisplayName("Đang ở nhà công vụ giáo vụ")]
        public string TeacherDutiesName { get; set; }
        [DisplayName("Có nhu cầu ở nhà công vụ giáo viên")]
        public bool? NeedTeacherDuties { get; set; }
        [DisplayName("Có nhu cầu ở nhà công vụ giáo viên")]
        public string NeedTeacherDutiesName { get; set; }
        //End Add
        [DisplayName("Hình thức đào tạo chính")]
        public string QualificationTypeName { get; set; }
        [DisplayName("Bồi dưỡng thường xuyên")]
        public bool? RegularRefresher { get; set; }
        [DisplayName("Bồi dưỡng thường xuyên")]
        public string RegularRefresherName { get; set; }
        [DisplayName("Tổ bộ môn")]
        public int? SchoolFacultyID { get; set; }
        [DisplayName("Tổ bộ môn")]
        public string SchoolFacultyName { get; set; }
        public int? SchoolID { get; set; }
        [DisplayName("Chuyên ngành đào tạo chính")]
        public int? SpecialityCatID { get; set; }
        [DisplayName("Chuyên ngành đào tạo chính")]
        public string SpecialityCatName { get; set; }
        [DisplayName("Ngày tuyển dụng")]
        public DateTime? StartingDate { get; set; }
        public int? SupervisingDeptID { get; set; }
        public string SupervisingDeptName { get; set; }
        [DisplayName("Số điện thoại")]
        public string Telephone { get; set; }
        [DisplayName("Vị trí việc làm")]
        public int? WorkGroupTypeID { get; set; }
        [DisplayName("Vị trí việc làm")]
        public string WorkGroupTypeName { get; set; }
        [DisplayName("Chức vụ/Công việc")]
        public int? WorkTypeID { get; set; }
        [DisplayName("Chức vụ/Công việc")]
        public string WorkTypeName { get; set; }
        [DisplayName("Mức phụ cấp thu hút nghề (%)")]
        public int? VocationalAllowance { get; set; }
        [DisplayName("Mức phụ cấp thâm niên (%)")]
        public int? SeniorityAllowance { get; set; }
        [DisplayName("Mức phụ cấp ưu đãi nghề (%)")]
        public int? PreferentialAllowance { get; set; }
        [DisplayName("Danh hiệu phong tặng")]
        public string AppellationAward { get; set; }
        public int? ConcurentWork { get; set; }
        [DisplayName("Nghiệm vụ kiêm nhiệm")]
        public string ConcurentWorkTypeResolution { get; set; }
        [DisplayName("Dạy 1 buổi/ngày")]
        public int? SectionPerDay1 { get; set; }
        [DisplayName("Dạy 1 buổi/ngày")]
        public string SectionPerDay1Name { get; set; }
        [DisplayName("Dạy 2 buổi/ngày")]
        public int? SectionPerDay2 { get; set; }
        [DisplayName("Dạy 2 buổi/ngày")]
        public string SectionPerDay2Name { get; set; }
        [DisplayName("Ngạch/Hạng")]
        public string EmployeeScaleName { get; set; }
        [DisplayName("Mã ngạch")]
        public string EmployeeScaleCode { get; set; }
        [DisplayName("Hệ số lượng")]
        public decimal? Coefficient { get; set; }
        [DisplayName("Hệ số lượng")]
        public string CoefficientName { get; set; }
        [DisplayName("Ngày hưởng")]
        public DateTime? AppliedDate { get; set; }
        [DisplayName("Vượt khung (%)")]
        public int? SalaryAmount { get; set; }
        [DisplayName("Bậc lương")]
        public string SubLevel { get; set; }
        [DisplayName("Trình độ CN đào tạo chính")]
        public string MainGraduationLevel { get; set; }
        [DisplayName("Trình độ CN đào tạo chính")]
        public int? MainGraduationLevelID { get; set; }
        [DisplayName("Chuyên ngành đào tạo khác")]
        public int? OtherSpecialityCatID { get; set; }
        [DisplayName("Chuyên ngành đào tạo khác")]
        public string OtherSpecialityCatName { get; set; }
        [DisplayName("Hình thức đào tạo khác")]
        public int? OtherQualificationTypeID { get; set; }
        [DisplayName("Hình thức đào tạo khác")]
        public string OtherQualificationTypeName { get; set; }
        [DisplayName("Trình độ CN đào tạo khác")]
        public int? OtherGraduationLevelID { get; set; }
        [DisplayName("Trình độ CN đào tạo khác")]
        public string OtherGraduationLevelName { get; set; }
        [DisplayName("Môn dạy chính")]
        public string PrimarilyAssignedSubject { get; set; }
        [DisplayName("Tình trạng hôn nhân")]
        public int? MariageStatusID { get; set; }
        [DisplayName("Tình trạng hôn nhân")]
        public string MariageStatus { get; set; }
        [DisplayName("Ngoại ngữ chính")]
        public int? MainForeignLanguageId { get; set; }
        [DisplayName("Ngoại ngữ chính")]
        public string MainForeignLanguageName { get; set; }
         [DisplayName("KQ chuẩn nghề nghiệp")]
        public string EvaluationField1 { get; set; }
         [DisplayName("Đánh giá viên chức")]
        public string EvaluationField2 { get; set; }
         [DisplayName("KQ b.dưỡng t.xuyên")]
        public string EvaluationField3 { get; set; }
         [DisplayName("Giáo viên dạy giỏi")]
        public string EvaluationField4 { get; set; }
        //Dung cho tra cuu cap Phong so
        public int? DistrictID { get; set; }
        public int? ProvinceID { get; set; }
        public bool IsActive { get; set; }
        [DisplayName("Số CMND/TCC")]
        public string IdentifyNumber { get; set; }
        [DisplayName("Ngày cấp CMND/TCC")]
        public DateTime? IdentityIssuedDate { get; set; }
        [DisplayName("Nơi cấp CMND/TCC")]
        public string IdentityIssuedPlace { get; set; }
        [DisplayName("Quê quán")]
        public string HomeTown { get; set; }
        [DisplayName("Địa chỉ thường trú")]
        public string PermanentResidentalAddress { get; set; }
        [DisplayName("Là đoàn viên")]
        public bool? IsYouthLeageMember {get; set;}
        [DisplayName("Là đoàn viên")]
        public string IsYouthLeageMemberName { get; set; }
        [DisplayName("Ngày KN Đoàn")]
        public DateTime? YouthLeagueJoinedDate {get; set;}
        [DisplayName("Là Đảng viên")]
        public bool? IsCommunistPartyMember {get; set;}
        [DisplayName("Là Đảng viên")]
        public string IsCommunistPartyMemberName { get; set; }
         [DisplayName("Ngày KN Đảng")]
        public DateTime? CommunistPartyJoinedDate {get; set;}
        [DisplayName("Họ tên bố")]
        public string FatherFullName {get; set;}
        [DisplayName("Nghề nghiệp")]
        public string FatherJob { get; set; }
        [DisplayName("Nơi làm việc")]
        public string FatherWorkingPlace { get; set; }
        [DisplayName("Năm sinh")]
        public DateTime? FatherBirthDate {get; set;}
        [DisplayName("Năm sinh")]
        public string StringFatherBirthDate { get; set; }
        [DisplayName("Họ tên mẹ")]
        public string MotherFullName {get; set;}
         [DisplayName("Nghề nghiệp")]
        public string MotherJob { get; set; }
        [DisplayName("Nơi làm việc")]
        public string MotherWorkingPlace { get; set; }
        [DisplayName("Năm sinh")]
        public DateTime? MotherBirthDate {get; set;}
        [DisplayName("Năm sinh")]
        public string StringMotherBirthDate { get; set; }
        [DisplayName("Họ tên vợ/chồng")]
        public string SpouseFullName {get; set;}
        [DisplayName("Nghề nghiệp")]
        public string SpouseJob { get; set; }
         [DisplayName("Nơi làm việc")]
        public string SpouseWorkingPlace { get; set; }
        [DisplayName("Năm sinh")]
        public DateTime? SpouseBirthDate {get; set;}
        [DisplayName("Năm sinh")]
        public String StringSpouseBirthDate { get; set; }
        [DisplayName("Trình độ CMNV cao nhất")]
        public string TrainingLevelResolution {get; set;}
        public string QualificationLevelResolution {get; set;}
        public string QualificationResolution { get; set; }
        [DisplayName("Trình độ NN chính")]
        public string ForeignLanguageGradeResolution {get; set;}
        public string ForeignResolution { get; set; }
        public string ITQualificationLevelResolution {get; set;}
        public string ITResolution { get; set; }
        [DisplayName("Trình độ lý luận CT")]
        public string PoliticalGradeResolution {get; set;}
        public string SpecialityCatResolution {get; set;}
        public string ContractTypeResolution {get; set;}
        public string PoliticalGradeName { get; set; }
        public string WorkTypeResolution {get; set;}
        [DisplayName("Tên trường")]
        public string SchoolName {get; set;}
        [DisplayName("Mã trường chuẩn")]
        public string SyncCode { get; set; }
        [DisplayName("Tôn giáo")]
        public string ReligionName { get; set; }
        [DisplayName("Dân tộc")]
        public string EthnicName { get; set; }
        public string EthnicCode { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? EmploymentStatus { get; set; }
        [DisplayName("Ngày vào trường")]
        public DateTime IntoSchoolDate { get; set; }
        [DisplayName("Cấp dạy chính")]
        public int? AppliedLevel { get; set; }
        [DisplayName("Cấp dạy chính")]
        public string AppliedLevelName { get; set; }
        [DisplayName("Sức khỏe")]
        public string HealthStatus { get; set; }
         [DisplayName("Trình độ NN chính")]
        public string ForeignLanguageGradeName { get; set; }
        [DisplayName("Trình độ QL nhà nước")]
        public string StateManagementGradeName { get; set; }
        [DisplayName("Trình độ QL giáo dục")    ]
        public string EducationalManagementGradeName { get; set; }
        [DisplayName("Chuyên trách Đoàn/Đội")]
        public bool? DedicatedForYoungLeague { get; set; }
        [DisplayName("Chuyên trách Đoàn/Đội")]
        public string DedicatedForYoungLeagueName { get; set; }
        [DisplayName("Là công đoàn viên")]
        public bool? IsSyndicate { get; set; }
        [DisplayName("Là công đoàn viên")]
        public string IsSyndicateName { get; set; }
        [DisplayName("Thành phần gia đình")]
        public int? FamilyTypeID { get; set; }
        [DisplayName("Thành phần gia đình")]
        public string FamilyTypeName { get; set; }
        /// <summary>
        /// So so bao hiem
        /// </summary>
        /// 
        [DisplayName("Số sổ BHXH")]
        public string InsuranceNumber { get; set; }
        [DisplayName("Tỉnh/Tp")]
        public string ProvinceName { get; set; }
        [DisplayName("Quận/Huyện")]
        public string DistrictName { get; set; }
        [DisplayName("Xã Phường")]
        public string CommuneName { get; set; }
    }
}