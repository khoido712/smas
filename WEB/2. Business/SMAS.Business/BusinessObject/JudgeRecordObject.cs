﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class JudgeRecordObject
    {
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public int? JudgeRecordID { get; set; }
        public int PupilID { get; set; }
        public int? ClassID { get; set; }
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public int? SubjectID { get; set; }
        public int? MarkTypeID { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<int> PeriodID { get; set; }
        public string Judgement { get; set; }
        public string ReTestJudgement { get; set; }
        public int OrderNumber { get; set; }
        public int? Status { get; set; }
        public string Title { get; set; }
        public string MarkTitle { get; set; }
        public bool EnableMark { get; set; }
        public bool DisplayMark { get; set; }
        public bool NotStudyingPupil { get; set; }
        public int? OrderInClass { get; set; }
        public string PupilName { get; set; }
        public int Year { get; set; }


        public string[] InterviewMark { get; set; }
        public string[] WritingMark { get; set; }
        public string[] TwiceCoeffiecientMark { get; set; }
        public string SemesterMark { get; set; }
        public string TBMYearMark { get; set; }
        public int? EducationLevelID { get; set; }
        public int? AppliedLevel { get; set; }

        public bool? IsLegalBot { get; set; }
        public string ErrorDescription { get; set; }

        public DateTime? DateClassMoving { get; set; }
        public DateTime? DatePupilLeavingOff { get; set; }
        public DateTime? DateSchoolMovement { get; set; }
        public DateTime? MarkedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
