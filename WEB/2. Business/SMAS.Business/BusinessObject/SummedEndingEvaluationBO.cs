﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SummedEndingEvaluationBO
    {
        public long SummedEndingEvaluationID { get; set; }
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationID { get; set; }
        public int SchoolID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public int ClassID { get; set; }
        public int AcademicYearID { get; set; }
        public string EndingEvaluation { get; set; }
        public string EvaluationReTraining { get; set; }

        public bool IsCombinedClass { get; set; }
        public int? SchoolTypeID { get; set; }
        public bool? IsDisabled { get; set; }
        public int? Genre {get;set;}
        public int? EthnicID { get; set; }
        public int EducationLevelID { get; set; }
        public int? RateAdd { get; set; }
        public int? RewardID { get; set; }
    }
}
