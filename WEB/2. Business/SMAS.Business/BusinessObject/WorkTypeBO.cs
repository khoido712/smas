﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class WorkTypeBO
    {
        public int WorkTypeID { get; set; }
        public string Resolution { get; set; }
    }
}
