﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SParentAccountResponse
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsExist { get; set; }
    }
}
