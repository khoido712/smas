﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class CapacityConductReport
    {
        public int Year { get; set; }
        public int EducationLevelID { get; set; }
        public int Semester { get; set; }
        public int TrainingTypeID { get; set; }
        public int SubcommitteeID { get; set; }
        public int ProvinceID { get; set; }
        public int? DistrictID { get; set; }
        public int SupervisingDeptID { get; set; }

        public int SubjectID { get; set; }


    }
}
