﻿using SMAS.Business.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamPupilAbsenceBO
    {
        public long ExamPupilAbsenceID { get; set; }
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public long SubjectID { get; set; }
        public long? ExamRoomID { get; set; }
        public long ExamPupilID { get; set; }
        public string ContentReasonAbsence { get; set; }
        public bool? ReasonAbsence { get; set; }
        public string ExamineeNumber { get; set; }
        public DateTime BirtDate { get; set; }
        public int Genre { get; set; }
        public string ClassName { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string EthnicCode { get; set; }
        public string SubjectName { get; set; }
        public string ExamRoomCode { get; set; }
        public string SBD { get; set; }
        public string GenreName
        {
            get
            {
                return Genre == GlobalConstants.GENRE_MALE ? GlobalConstants.GENRE_MALE_TITLE :
                                (this.Genre == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE :
                                (this.Genre == 2 ? "Không xác định" : String.Empty));
            }
        }

        public string DisplayReasonAbsence
        {
            get
            {
                return ReasonAbsence == true ? "Có" : "Không";
            }
        }
    }
}
