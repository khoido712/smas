using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class PupilOfClassBO
    {
        public int PupilOfClassID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public DateTime? LeavingDate { get; set; }
        public Nullable<int> OrderInClass { get; set; }
        public string Description { get; set; }
        public Nullable<bool> NoConductEstimation { get; set; }
        public int Status { get; set; }
        public Nullable<int> FirstSemesterExemptType { get; set; }
        public Nullable<int> SecondSemesterExemptType { get; set; }
        public Models.Models.PupilProfile PupilProfile { get; set; }
        public bool? isShow { get; set; }
        public string Name { get; set; }
        public string PupilFullName { get; set; }
        public string PupilCode { get; set; }
        public DateTime? EndDate { get; set; }
        public string ClassName { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }
        public string EthnicCode { get; set; }
        public string EthnicName { get; set; }
        public string StorageNumber { get; set; }
        public int? EnrolmentType { get; set; }
        public string SchoolName { get; set; }
        public string Mobile { get; set; }
        public string EducationLevelName { get; set; }

        public DateTime Birthday { get; set; }
        public string BirthPlace { get; set; }
        public int? OrderPupil { get; set; }
        public int? AppliedType { get; set; }
        public bool? IsSpecialize { get; set; }
        public int SubjectID { get; set; }
        public bool? IsVNEN { get; set; }
        public int? ExemptedSubjectID { get; set; }
        public string FatherFullName { get; set; }
        public string FatherJobName { get; set; }
        public string FatherMobile { get; set; }
        public string FatherEmail { get; set; }
        public string MotherFullName { get; set; }
        public string MotherJobName { get; set; }
        public string MotherMobile { get; set; }
        public string MotherEmail { get; set; }
        public string SponserFullName { get; set; }
        public string SponserJobName { get; set; }
        public string SponserMobile { get; set; }
        public string SponserEmail { get; set; }
        public string ResidentalAddress { get; set; }
        public int? PolicyTargetID { get; set; }
        public string PolicyTargetName { get; set; }
        public string PriorityTypeName { get; set; } 
        public string PermanentResidentalAddress { get; set; }//Dia chi thuong tru
        public string TempReidentalAddress { get; set; }//Dia chi tam tru
        public string AcademicDisplay { get; set; }
        public int CurrentClassId { get; set; }
        public int ProfileStatus { get; set; }
        public int YearTitle { get; set; }
        public int? SectionID { get; set; }
        /// <summary>
        /// Dang ky HD
        /// </summary>
        public bool? IsRegisterContract { get; set; }
        public int? ClassOrder { get; set; }
        public SchoolProfile SchoolProfile { get; set; }
        public string HomeTown { get; set; }
        public bool IsActive { get; set; }
        public string HeadTeacherName { get; set; }
        public string Combine_Class_Code { get; set; }
        public bool IsCombineClass { get; set; }
        public DateTime EnrolmentDate { get; set; }
        public byte[] Image { get; set; }
       
        
    }
}
