using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class CandidateMark
    {
        public int CandidateID { get; set; }
        public DateTime? BirthDate { get; set; }
        public int PupilID { get; set; }
        public int? ClassID { get; set; }
        public string ClassName { get; set; }
        public int? SubjectID { get; set; }
        public decimal? Mark { get; set; }
        public string Judgement { get; set; }
        public string ExamMark { get; set; }
        public string RealMark { get; set; }
        public int? PenaltyMarkPercent { get; set; }
        public string DetachableHeadNumber { get; set; }
        public int DetachableHeadBagID { get; set; }
        public string ErrorReason { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string PupilName { get; set; }
        public string PupilCode { get; set; }
        public string NamedListCode { get; set; }
        public int? NamedListNumber { get; set; }
        public int? RoomID { get; set; }
        public bool IsCheck { get; set; }
        public bool IsDisable { get; set; }
        public int? IsCommenting { get; set; }
        public string RoomName { get; set; }
        public bool? HasReason { get; set; }
        /// <summary>
        /// Trang thai cua hoc sinh trong lop dang hoc
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// Ma dan toc cua hoc sinh
        /// </summary>
        public string EthnicCode { get; set; }
    }
}
