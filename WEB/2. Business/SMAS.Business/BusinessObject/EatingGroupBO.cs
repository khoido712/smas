using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class EatingGroupBO
    {
        public int? EatingGroupID { get; set; }
        public string EatingGroupName { get; set; }
        public int? SchoolID { get; set; }
        public int? MonthFrom { get; set; }
        public int? MonthTo { get; set; }
        public int? AcademicYearID { get; set; }
        public bool? IsActive { get; set; }
        public int? ClassID { get; set; }
        public string ClassName { get; set; }
		public int? EatingGroupClassID { get; set; }
    }
}
