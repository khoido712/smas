﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MonitoringBookBO
    {
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public DateTime CreateDate { get; set; }    
        public int EducationLevelID { get; set; }      
        public int? HealthPeriodID { get; set; }
        public bool? IsSMS { get; set; }
        public int? M_OldID { get; set; }
        public int? M_ProvinceID { get; set; }
        public int MonitoringBookID { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public int? MonitoringType { get; set; }
        public string MSourcedb { get; set; }    
        public int PupilID { get; set; }    
        public int SchoolID { get; set; }
        public string Solution { get; set; }
        public string Symptoms { get; set; }
        public string UnitName { get; set; }

        public int Genre { get; set; }
        public DateTime BirthDate { get; set; }

        public List<ListErrorBO> ListErrorMessage { get; set; }
    }

    public class ListErrorBO
    {      
        public string PupilCode { get; set; }
        public string PupilName { get; set; }
        public string Description { get; set; }
        public string SheetName { get; set; }
    }
}
