﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SMSParentContractClassBO
    {
        public int SMS_PARENT_CONTRACT_CLASS_ID { get; set; }
        public int CLASS_ID { get; set; }
        public int PUPIL_ID { get; set; }
        public int ACADEMIC_YEAR_ID { get; set; }
        public int SCHOOL_ID { get; set; }
        public int YEAR { get; set; }
        public int SEMESTER { get; set; }
        public int TOTAL_SMS { get; set; }
        public int SENT_TOTAL { get; set; }
        public int COUNT_PHONE { get; set; }
        public int COUNT_PHONE_STATUS { get; set; }
        public int STATUS { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public Nullable<System.DateTime> UPDATED_DATE { get; set; }
        public string UPDATED_NOTE { get; set; }
    }
}
