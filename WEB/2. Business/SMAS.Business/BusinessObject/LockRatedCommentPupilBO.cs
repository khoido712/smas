﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class LockRatedCommentPupilBO
    {
        public List<MonthInYearLockBO> ListMonthHK1 { get; set; }
        public List<MonthInYearLockBO> ListMonthHK2 { get; set; }
    }

    public class MonthInYearLockBO
    {
        public string MonthName { get; set; }
        public int Semeter { get; set; }
    }
}
