﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamPupilInfoBO
    {
        public long PupilID { get; set; }
        public long ExamPupilID { get; set; }
        public string PupilCode { get; set; }
        public string ExamineeCode { get; set; }
        public string ClassName { get; set; }
        public long ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
    }
}
