﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RegisterSMSFreeBO
    {
        public long RegisterSMSFreeID { get; set; }
        public int SchoolID { get; set; }
        public int YearID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public Nullable<int> StatusRegister { get; set; }
        public int Last2DigitNumberSchool { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string PupilCode { get; set; }
        public string Fullname { get; set; }
        public Nullable<bool> Gender { get; set; }
        public Nullable<int> Year { get; set; }
        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public string YearName { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public string Name { get; set; }
    }
}
