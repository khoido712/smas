﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TimerConfigBO
    {
        public System.Guid TimerConfigID { get; set; }
        public int SchoolID { get; set; }
        public int PartitionSchoolID { get; set; }
        public int AppliedLevel { get; set; }
        public int Type { get; set; }
        public int TargetID { get; set; }
        public string TimerConfigName { get; set; }
        public string Content { get; set; }
        public System.DateTime SendTime { get; set; }
        public int Status { get; set; }
        public bool SendAllClass { get; set; }
        public bool SendUnicode { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public bool IsImport { get; set; }
    }
}
