﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExamMarkBO
    {
        public int ExamMarkID { get; set; }
        public int ExamPupilID { get; set; }
        public decimal Mark { get; set; }
        public int SubjectID { get; set; }
        public int DoetSubjectID { get; set; }
    }
}
