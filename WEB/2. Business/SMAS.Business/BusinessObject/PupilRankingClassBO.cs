﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilRankingClassBO
    {
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int Semester { get; set; }
        public int? PeriodID { get; set; }
        public int PupilID { get; set; }
        public int? CapacityLevelID { get; set; }
        public int? ConductLevelID { get; set; }

        public List<PupilRankingSubjectBO> ListSubject { get; set; }
    }

    public class PupilRankingSubjectBO
    {
        public int SubjectID { get; set; }
        public int IsCommenting { get; set; }
        public bool IsExempted { get; set; }
        public decimal? RankingMark { get; set; }
        public string RankingJudgement { get; set; }
        public int? AppliedType { get; set; }
        public decimal? SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public decimal? ReTestMark { get; set; }
        public string ReTestJudgement { get; set; }
    }
}
