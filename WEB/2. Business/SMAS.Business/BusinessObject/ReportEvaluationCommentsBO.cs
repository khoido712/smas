﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReportEvaluationCommentsBO
    {
        public long EvaluationCommentsID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public int SchoolID { get; set; }
        public int TypeOfTeacher { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public int EvaluationID { get; set; }
        public string CommentsM1M6 { get; set; }
        public string CommentsM2M7 { get; set; }
        public string CommentsM3M8 { get; set; }
        public string CommentsM4M9 { get; set; }
        public string CommentsM5M10 { get; set; }
        public string NoteInsert { get; set; }
        public string NoteUpdate { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
    }
}
