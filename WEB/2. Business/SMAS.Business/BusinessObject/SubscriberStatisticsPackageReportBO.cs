﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SubscriberStatisticsPackageReportBO
    {
        public int Order { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }

        public string SchoolName { get; set; }

        public string SchoolUserName { get; set; }
        public string ServicePackage { get; set; }

        public int? NumOfSubscriberRegOnlySem1 { get; set; }

        public int? NumOfSubscriberRegOnlySem2 { get; set; }

        public int? NumOfSubscriberRegOnlySem3 { get; set; }
    }
}
