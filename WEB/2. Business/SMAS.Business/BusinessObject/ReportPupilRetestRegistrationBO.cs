﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportPupilRetestRegistrationBO
    {
        public string ClassName { get; set; }
        public string PupilName { get; set; }
        public string Name { get; set; }
        public string PupilCode { get; set; }
        public DateTime BirthDate { get; set; }
        public decimal? SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public int Genre { get; set; }
        public int EducationLevelID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int status { get; set; }
        public int PupilOrder { get; set; }
        public int CLassOrder { get; set; }
    }
}
