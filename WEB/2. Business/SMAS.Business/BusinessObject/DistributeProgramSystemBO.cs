﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DistributeProgramSystemBO
    {
        public int DistributeProgramSystemID { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectID { get; set; }
        public string AssignSubjectName { get; set; }
        public string LessonName { get; set; }
        public int SchoolWeekID { get; set; }
        public int? DistributeProgramOrder { get; set; }
    }
}
