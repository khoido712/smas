﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    
    public class TopupResponseBO
    {
        /// <summary>
        /// id EWalletTransaction
        /// </summary>
        
        public long TransactionID { get; set; }
        /// <summary>
        /// trang thai
        /// </summary>
        
        public bool Success { get; set; }

        /// <summary>
        /// thoi gian bi khoa con lai
        /// </summary>
        
        public DateTime LastLockedOutDate { get; set; }

        /// <summary>
        /// so luong
        /// </summary>
        
        public decimal TransactionAmount { get; set; }

        /// <summary>
        /// thoi gian giao dich
        /// </summary>
        
        public DateTime TransactionTime { get; set; }

        /// <summary>
        /// so du tai khoan
        /// </summary>
        
        public decimal BalanceAfterTransaction { get; set; }

        /// <summary>
        /// ma loi
        /// </summary>
        
        public string ErrorCode { get; set; }

        /// <summary>
        /// thong bao loi
        /// </summary>

        public string ErrorMessage
        {
            get
            {
                string message = string.Empty;
                switch (this.ErrorCode)
                {

                    case "01":
                        message = "Thẻ cào không hợp lệ hoặc đã được sử dụng. Thầy/cô vui lòng kiểm tra lại.";
                        break;
                    case "02":
                        message = "Thẻ cào không hợp lệ hoặc đã được sử dụng. Thầy/cô vui lòng kiểm tra lại.";
                        break;
                    case "03":
                        message = "Thẻ cào không hợp lệ hoặc đã được sử dụng. Thầy/cô vui lòng kiểm tra lại.";
                        break;
                    case "04":
                        message = "Thẻ cào không hợp lệ hoặc đã được sử dụng. Thầy/cô vui lòng kiểm tra lại.";
                        break;
                    case "05":
                        message = "Đầu thẻ không được hỗ trợ. Hệ thống chỉ chấp nhận thẻ Viettel.";
                        break;
                    case "06":
                        message = "Thẻ cào chưa được kích hoạt. Thầy/cô vui lòng gọi đến số 18008000 nhánh 1 để được hỗ trợ.";
                        break;
                    case "07":
                        message = "Thẻ cào đã quá hạn sử dụng. Thầy/cô vui lòng kiểm tra lại.";
                        break;
                    case "08":
                        message = "Thông tin thẻ cào không hợp lệ. Thầy/cô vui lòng gọi đến số 18008000 nhánh 1 để được hỗ trợ.";
                        break;
                    case "54":
                        message = "Thẻ cào đang được xử lý.";
                        break;
                    case "200":
                        message = "Nạp thẻ bị khóa do nhập sai thông tin quá số lần quy định.";
                        break;
                    default:
                        message = "Không thể thực hiện giao dịch. Vui lòng thử lại sau.";
                        break;
                }

                return message;
            }
        }

        /// <summary>
        /// thoi gian con lai khi bi khoa
        /// </summary>
        
        public int RemainTimeToUnLock { get; set; }

        /// <summary>
        /// Mã Ewallet
        /// </summary>
        
        public int EwalletID { get; set; }

        /// <summary>
        /// Số tiền thay đổi
        /// </summary>
        
        public decimal TransAmount { get; set; }

        /// <summary>
        /// id Topup
        /// </summary>
        
        public Guid TopupID { get; set; }

        /// <summary>
        /// khoa doi tac
        /// </summary>
        
        public bool IsLockPartner { get; set; }
    }

    
    public class LstTopupResponse
    {
        private List<TopupResponseBO> lstResponse;

        /// <summary>
        /// ma loi tra ve
        /// </summary>
        
        public int ValidationCode { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>

        public List<TopupResponseBO> LstResponse
        {
            get
            {
                return lstResponse;
            }
            set
            {
                if (value == null)
                {
                    this.lstResponse = new List<TopupResponseBO>();
                }
                else
                {
                    this.lstResponse = value;
                }
            }
        }
    }
}
