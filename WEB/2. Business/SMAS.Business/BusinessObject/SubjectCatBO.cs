﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SubjectCatBO
    {
        public int SubjectCatID { get; set; }
        public string SubjectName { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public Nullable<int> OrderInSubject { get; set; }
        public int? IsCommenting { get; set; }
        public int? SubjectIdInCrease { get; set; }
        public int ClassID { get; set; }
        public string Abbreviation { get; set; }
        public int? AssignSubjectID { get; set; }
        public string strSubjectAndAssignCatID { get; set; }
        public bool? IsSubjectVNEN { get; set; }
        public int EducationLevel { get; set; }

        public int? AppliedLevel { get; set; }
        //public virtual ApprenticeshipGroup ApprenticeshipGroup { get; set; }
        public int? ApprenticeshipGroupID { get; set; }
        //public virtual ICollection<ApprenticeshipSubject> ApprenticeshipSubjects { get; set; }
        //public virtual ICollection<Calendar> Calendars { get; set; }
        //public virtual ICollection<CapacityStatistic> CapacityStatistics { get; set; }
        //public virtual ICollection<ClassSubject> ClassSubjects { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public DateTime? CreatedDate { get; set; }

        //public virtual ICollection<EducationProgram> EducationPrograms { get; set; }
        //public virtual ICollection<Employee> Employees { get; set; }
        //public virtual ICollection<ExaminationSubject> ExaminationSubjects { get; set; }
        //public virtual ICollection<ExemptedSubject> ExemptedSubjects { get; set; }
        public bool HasPractice { get; set; }
        public bool IsActive { get; set; }
        public bool IsApprenticeshipSubject { get; set; }

        public bool IsCoreSubject { get; set; }
        public bool IsEditIsCommentting { get; set; }
        public bool IsExemptible { get; set; }
        public bool IsForeignLanguage { get; set; }
        //public virtual ICollection<JudgeRecordHistory> JudgeRecordHistories { get; set; }
        //public virtual ICollection<JudgeRecord> JudgeRecords { get; set; }
        //public virtual ICollection<LockedMarkDetail> LockedMarkDetails { get; set; }
        //public virtual ICollection<MarkRecordHistory> MarkRecordHistories { get; set; }
        //public virtual ICollection<MarkRecord> MarkRecords { get; set; }
        //public virtual ICollection<MarkStatistic> MarkStatistics { get; set; }
        public bool MiddleSemesterTest { get; set; }
        public DateTime? ModifiedDate { get; set; }

        //public virtual ICollection<ProvinceSubject> ProvinceSubjects { get; set; }
        //public virtual ICollection<PupilRetestRegistration> PupilRetestRegistrations { get; set; }
        public bool ReadAndWriteMiddleTest { get; set; }
        //public virtual ICollection<SchoolSubject> SchoolSubjects { get; set; }
        //public virtual ICollection<StatisticSubjectOfTertiary> StatisticSubjectOfTertiaries { get; set; }


        //public virtual ICollection<SummedUpRecordHistory> SummedUpRecordHistories { get; set; }
        //public virtual ICollection<SummedUpRecord> SummedUpRecords { get; set; }
        public int? SynchronizeID { get; set; }
        //public virtual ICollection<TeachingAssignment> TeachingAssignments { get; set; }
        //public virtual ICollection<TeachingRegistration> TeachingRegistrations { get; set; }
        //public virtual ICollection<TeachingTargetRegistration> TeachingTargetRegistrations { get; set; }
    }
}
