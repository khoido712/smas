﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SumUpRecordPrimaryBO
    {
        public long? SummedUpRecordID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? SchoolID { get; set; }
        public int? PeriodID { get; set; }
        public int AcademicYearID { get; set; }
        public int? SubjectID { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public string SubjectName { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }

        public Nullable<int> Semester { get; set; }
        public Nullable<decimal> SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public Nullable<decimal> ReTestMark { get; set; }
        public string ReTestJudgement { get; set; }
        public Nullable<int> OrderInClass { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<decimal> AverageMark { get; set; }
        public Nullable<int> CapacityLevelID { get; set; }
        public Nullable<int> ConductLevelID { get; set; }
        public Nullable<int> Rank { get; set; }
        public string PupilFullName { get; set; }
        public string PupilCode { get; set; }

        public string Comment { get; set; }
        public int? IsCommenting { get; set; }
        public int? Year { get; set; }
        public DateTime? AssignedDate { get; set; }

        public Nullable<System.DateTime> SummedUpDate { get; set; }

        public bool? IsLegalBot { get; set; }

        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<bool> IsOldData { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public int CreatedAcademicYear { get; set; }

        public Nullable<decimal> SummedUpMark1 { get; set; }
        public Nullable<decimal> SummedUpMark2 { get; set; }
        public Nullable<decimal> SummedUpMark3 { get; set; }
        public Nullable<decimal> ReTestMark1 { get; set; }
        public Nullable<decimal> ReTestMark2 { get; set; }

    }

    public class SummedUpRecordPrimaryJudgementBO
    {
        public string JudgementResult1 { get; set; }
        public string JudgementResult2 { get; set; }
        public string JudgementResult3 { get; set; }
        public int? ClassID { get; set; }
        public int? SubjectID { get; set; }
        public int? AcademicYearID { get; set; }
        public int? PupilID { get; set; }
    }

}
