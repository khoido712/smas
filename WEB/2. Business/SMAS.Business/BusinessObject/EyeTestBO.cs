﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class EyeTestBO
    {
        public int? EyeTestID { get; set; }
        public int MonitoringBookID { get; set; }
        public int? REye { get; set; }
        public int? LEye { get; set; }
        public Nullable<decimal> RMyopic { get; set; }
        public Nullable<decimal> LMyopic { get; set; }
        public Nullable<decimal> RFarsightedness { get; set; }
        public Nullable<decimal> LFarsightedness { get; set; }
        public Nullable<decimal> RAstigmatism { get; set; }
        public Nullable<decimal> LAstigmatism { get; set; }
        public string Other { get; set; }
        public bool? IsTreatment { get; set; }               
        public Nullable<bool> IsMyopic { get; set; }
        public Nullable<bool> IsFarsightedness { get; set; }
        public Nullable<bool> IsAstigmatism { get; set; }
        public DateTime Date { get; set; }
        public DateTime? CreateDate { get; set; }

        public int PupilProFileID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int LogID { get; set; }
        public bool IsActive { get; set; }

        public int? RIsEye { get; set; }
        public int? LIsEye { get; set; }
        public int ClassID { get; set; }
    }
}
