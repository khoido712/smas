﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EvaluationRewardEQReportBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public bool IsCombinedClass { get; set; }
        public bool? IsDisabled { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }
    }
}
