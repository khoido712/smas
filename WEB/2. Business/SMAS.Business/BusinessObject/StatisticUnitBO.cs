﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class StatisticUnitBO
    {
        public int SchoolID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectID { get; set; }
        public decimal? Mark { get; set; }
        public int? Semester { get; set; }
        public int? Genre { get; set; }
        public int? EthnicId { get; set; }
    }
}
