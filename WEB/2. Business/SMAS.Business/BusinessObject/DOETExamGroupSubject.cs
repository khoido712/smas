﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExamGroupSubject
    {
        public int GroupID { get;set;}
        public string GroupName { get; set; }
        public string ArrSubjectName { get; set; }
    }
}
