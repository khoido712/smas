using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ProvinceSubjectBO
    {
        public int? ProvinceSubjectID { get; set; }
        public int? SubjectID { get; set; }
        public int? ProvinceID { get; set; }
        public int? AcademicYearOfProvinceID { get; set; }
        public int? AppliedLevel { get; set; }
        public int? EducationLevelID { get; set; }
        public Nullable<bool> IsRegularEducation { get; set; }
        public Nullable<int> Coefficient { get; set; }
        public int? AppliedType { get; set; }
        public int? IsCommenting { get; set; }
        public decimal? SectionPerWeekFirstSemester { get; set; }
        public decimal? SectionPerWeekSecondSemester { get; set; }
        public string DisplayName { get; set; }
        public string Abbreviation { get; set; }
        public decimal? NumberOfLession { get; set; }
        public int? OrderInSubject { get; set; }
        public bool? IsCoreSubject { get; set; }
        public bool? IsChoice { get; set; }
        public bool? IsEditIsCommenting { get; set; }
    }
}
