﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class JudgeRecordWSBO
    {
        public int? PupilID { get; set; }
        public string Judgement { get; set; }
        public int? SubjectID { get; set; }
        public int? MarkTypeID { get; set; }        
        public int? SubjectOrderNumber { get; set; }
        public int? MarkOrderNumber { get; set; }
        public string Title { get; set; }
        public string SubjectName { get; set; }
        public DateTime? MarkedDate { get; set; }
        public int EducationLevelID { get; set; }
    }
}
