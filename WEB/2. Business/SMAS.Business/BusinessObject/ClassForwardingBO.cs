using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>04/01/2013 2:38:41 PM</date>
    /// </summary>
    public class ClassForwardingBO
    {
        #region Member Variables
        public int OldClassID { get; set; }
        public string OldClassName { get; set; }
        public int TotalPupil { get; set; }
        public int TotalMove { get; set; }
        public int NewClassID { get; set; }
        public int? TotalMoved { get; set; }
        #endregion

     

    }
}
