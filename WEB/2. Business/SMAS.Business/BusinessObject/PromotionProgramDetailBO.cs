﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PromotionProgramDetailBO
    {
        public int PromotionProgramDetailID { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public int? ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string Account { get; set; }
    }

    public class PromotionProgramDetailBO2
    {
        public int PromotionProgramDetailID { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public int? ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string Account { get; set; }
        public bool HasBeenApplied { get; set; }
    }
}
