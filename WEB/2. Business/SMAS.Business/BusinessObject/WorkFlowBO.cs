﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class WorkFlowBO
    {
        public int WorkFlowID { get; set; }
        public string WorkFlowName { get; set; }
        public int ParentID { get; set; }
        public string Url { get; set; }
        public string AppliedLevelID { get; set; }
        public string ProductVersion { get; set; }
        public string ViewName { get; set; }
        public int? OrderId { get; set; }
    }
}
