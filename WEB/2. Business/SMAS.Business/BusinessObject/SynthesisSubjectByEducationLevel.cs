﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SynthesisSubjectByEducationLevel
    {
        public int Semester { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int AppliedLevel { get; set; }
        public int StatisticLevelReportID { get; set; }
        public bool FemaleChecked { get; set; }
        public bool EthnicChecked { get; set; }
        public bool FemaleEthnicChecked { get; set; }
    }
}
