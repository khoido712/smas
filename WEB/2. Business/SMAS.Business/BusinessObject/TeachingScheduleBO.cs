﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TeachingScheduleBO
    {
        public long TeachingScheduleID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public System.DateTime ScheduleDate { get; set; }
        public string SubjectName { get; set; }
        public string ClassName { get; set; }
        public int DayOfWeek { get; set; }
        public int SectionID { get; set; }
        public int TeacherID { get; set; }
        public int SubjectOrder { get; set; }
        public int TeachingScheduleOrder { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string TeachingUtensil { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<bool> InRoom { get; set; }
        public Nullable<bool> IsSelfMade { get; set; }
        public int EducationLevelID { get; set; }
        public string LessionName { get; set; }
        public int? AssignSubjectID { get; set; }
        public int Status { get; set; }
    }
}
