﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PromotionBO
    {
        /// <summary>
        /// Số hiệu tài khoản khuyến mãi
        /// </summary>
        public int PromotionAccountID { get; set; }

        /// <summary>
        /// Số tin nội mạng
        /// </summary>
        public int InternalSMS;

        /// <summary>
        /// Số tin ngoại mạng
        /// </summary>
        public int ExternalSMS;

        /// <summary>
        /// Số dư tài khoản khuyến mãi
        /// </summary>
        public decimal PromotionBalance { get; set; }

        /// <summary>
        /// Số dư tài khoản khuyến mãi nội mạng
        /// </summary>
        public decimal InternalBalance { get; set; }
    }
}
