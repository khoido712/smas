﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PupilTransferedBO
    {
        public PupilProfile PupilProfile { get; set; }
        public PupilOfClass PupilOfClassInPreviousAcademicYear { get; set; }
        public PupilOfClass PupilOfClassInCurrentAcademicYear { get; set; }
    }
}
