﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ExaminationSubjectBO
    {
        public int ExaminationSubjectID { get; set; }
        public int SubjectID { get; set; }
        public string DisplayName { get; set; }
        public int? OrderInSubject { get; set; }
    }
}
