﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class OrderInsertBO
    {
        public string PupilCode { get; set; }
        public int ClassID { get; set; }
        public int OrderNumber { get; set; }
    }
}
