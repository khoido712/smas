﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SelectPupilBO
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Genre { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }        
    }
}
