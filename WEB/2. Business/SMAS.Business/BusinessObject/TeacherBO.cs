﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TeacherBO
    {
        public int TeacherID { get; set; }

        public string TeacherCode { get; set; }

        public string FullName { get; set; }

        /// <summary>
        /// ten viet tat dung de search
        /// </summary>
        public string Name { get; set; }

        public string Mobile { get; set; }
        public bool Genre { get; set; }

        public string GenreName { get; set; }

        public int TotalSMS { get; set; }

        public int TotalRecords { get; set; }

        public int EmployeeStatus { get; set; }

        public string TeachingSubject { get; set; }

        public string ShortName { get; set; }

        public bool IsActive { get; set; }

        public string TeacherSubject { get; set; }

        public string Status { get; set; } // trang thai 1: gui thanh cong, 0: Gui khong thanh cong, null: chua gui

        public int ClassID { get; set; }//lop hoc theo giao vien

        public int SchoolFacultyID { get; set; }
        public string SchoolFacultyName { get; set; }

        public int WorkGroupTypeID { get; set; }

        public string WorkGroupTypeName { get; set; }
        public int WorkTypeID { get; set; }
        public string WorkTypeName { get; set; }
        /// <summary>
        /// Doan vien
        /// </summary>
        public bool IsYouthLeageMember { get; set; }
        /// <summary>
        /// Dang vien
        /// </summary>
        public bool IsCommunistPartyMember { get; set; }
        /// <summary>
        /// Cong doan vien
        /// </summary>
        public bool IsSyndicate { get; set; }

        public int PhoneNetwork { get; set; }
        public string AccountName { get; set; }
        public bool IsHeadTeacher { get; set; }
        public bool IsSubjectTecher { get; set; }
        public bool isNumberVT { get; set; }
        public bool? isFreeSMS { get; set; }
        public bool? isFreeExSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan noi mang da nhan trong thang
        /// </summary>
        public int TotalInternalSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan ngoai mang da nhan trong thang
        /// </summary>
        public int TotalExternalSMS { get; set; }

        public int TotalSMSPromotion { get; set; }
        public string strTotalSMSPromotion
        {
            get
            {
                string str = string.Empty;
                if (!string.IsNullOrEmpty(Mobile))
                {
                    if (isNumberVT)
                    {
                        if (isFreeSMS.HasValue && isFreeSMS.Value)
                        {
                            str = "Miễn phí";
                        }
                        else
                        {
                            str = TotalSMSPromotion.ToString();
                        }
                    }
                    else
                    {
                        if (isFreeExSMS.HasValue && isFreeExSMS.Value)
                        {
                            str = "Miễn phí";
                        }
                        else
                        {
                            str = TotalSMSPromotion.ToString();
                        }
                    }
                }
                return str;
            }
        }
    }
}
