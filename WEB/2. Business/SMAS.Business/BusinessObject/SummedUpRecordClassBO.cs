﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SummedUpRecordClassBO
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int? Status { get; set; }
        public int? ClassOrderNumber { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? NumberOfPupil { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int Semester { get; set; }
        public int EducationLevelID { get; set; }
        public int PeriodID { get; set; }
        public int SummedUpRecordClassID { get; set; }
        public bool? isVNEN { get; set; }
    }
}
