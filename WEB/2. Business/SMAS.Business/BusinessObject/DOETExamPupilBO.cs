﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class DOETExamPupilBO
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string GenreName { get; set; }
        public int Genre { get; set; }
        public DateTime BirthDay { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string SchoolCode { get; set; }
        public string ExamineeNumber { get; set; }
        public int ExamRoomID { get; set; }
        public string ExamRoomName { get; set; }
        public string Location { get; set; }
        public decimal? Mark { get; set; }
        public bool isErr { get; set; }
        public string ErrorMessage { get; set; }
        public int ExaminationsID { get; set; }
        public int Year { get; set; }
        public int ExamGroupID { get; set; }
        public int ExamSubjectID { get; set; }
        public int EducationLevelID { get; set; }
        public string SchedulesExam { get; set; }
        public string ExamSubjectName { get; set; }
        public int ExamPupilID { get; set; }
        public string ExamCouncil { get; set; }
        public string BirthPlace { get; set; }
        public string DisplayMark { get; set; }
        public int? OrderInClass { get; set; }
        public int? SubjectID { get; set; }

        public Nullable<System.DateTime> SyncTime { get; set; }
        public string SyncSourceId { get; set; }

        public int EthnicId { get; set; }
        public string SyncSourceEthnicId { get; set; }
        public string ExamPupilStatus { get; set; }


    }
}
