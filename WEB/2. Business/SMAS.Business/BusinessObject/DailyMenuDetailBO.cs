﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DailyMenuDetailBO
    {
        public int DailyMenuDetailID { get; set; }
        public int?[] MealIDs { get; set; }
        public int?[] DishIDS { get; set; }
        public int DailyMenuID { get; set; }
        public string[] DishNames { get; set; }
    }
}
