﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SkillTitleBO
    {
        public int SkillTitleID { get; set; }
        public string Description { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public string SkillTitle { get; set; }
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int? ClassID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? AppliedLevel { get; set; }
        public string ClassName { get; set; }
    }
}
