﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MarkInputSituationBO
    {
        public int TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string TeacherFullName { get; set; }
        public List<ClassSituationBO> lstMarkM { get; set; }
        public List<ClassSituationBO> lstMarkP { get; set; }
        public List<ClassSituationBO> lstMarkV { get; set; }
        public List<ClassSituationBO> lstMarkHK { get; set; }
    }
}
