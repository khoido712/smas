﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ColumnDescriptionBO
    {
        public int ColumnDescriptionId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string Resolution { get; set; }
        public int ColumnOrder { get; set; }
        public bool RequiredColumn { get; set; }
        
        public int ColumnSize { get; set; }

        public string CellName { get; set; }
        public int ColumnIndex { get; set; }

        public string ColumnType { get; set; }
        public string Description { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string FieldName { get; set; }
        public int? ReportType { get; set; }
        public int? OrderID { get; set; }

    }
}
