﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamPupilViolateMarkBO
    {
        public long ExamPupilID { get; set; }
        public long ExamViolationTypeID { get; set; }
        public int? penalizedMark { get; set; }
    }
}
