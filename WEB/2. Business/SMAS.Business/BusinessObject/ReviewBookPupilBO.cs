﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReviewBookPupilBO
    {
        public int ReviewBookPupilID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public int SemesterID { get; set; }
        public Nullable<int> CapacityRate { get; set; }
        public string CQComment { get; set; }
        public Nullable<int> QualityRate { get; set; }
        public string CommentAdd { get; set; }
        public Nullable<int> RateAndYear { get; set; }
        public Nullable<int> RateAdd { get; set; }
        public Nullable<int> CapacitySummer { get; set; }
        public Nullable<int> QualitySummer { get; set; }
        public int PartitionID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public int Genre { get; set; }
        public int? EthnicID { get; set; }
        public int PupilStatus { get; set; }
    }
}
