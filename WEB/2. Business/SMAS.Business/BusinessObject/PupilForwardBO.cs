﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PupilForwardBO
    {
        public int PupilID { get; set; }  
        public int OldClassID { get; set; }
        public string OldClassName { get; set; }

        public int ForwardClassID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }
        public int Year { get; set; }
        public int SchoolID { get; set; }

        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public DateTime? BirthDate { get; set; }
        public int Genre { get; set; }
        public string GenreName { get; set; }
        public string Name { get; set; }
        public int? OrderInClass { get; set; }
        public int? OrderNumber { get; set; }      
    }
}
