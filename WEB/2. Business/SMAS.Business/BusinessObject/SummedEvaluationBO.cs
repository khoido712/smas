﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SummedEvaluationBO
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }

        public int ClassID { get; set; }
        public bool IsCombinedClass { get; set; }
        public int? SchoolTypeID { get; set; }
        public bool? IsDisabled { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationID { get; set; }
        public int EvaluationCriteriaID { get; set; }
        public string EvaluationName { get; set; }
        public string EndingComment { get; set; }//Nhan xet cuoi ky
        public Nullable<int> PeriodicEndingMark { get; set; }
        public string EndingEvaluation { get; set; }//danh gia
        public int? Mark { get; set; }
        public int SubjectID { get; set; }
        public long SummedEvaluationID { get; set; }
        public int? Genre {get;set;}
        public int? EthnicID {get;set;}
        public int? RateAdd { get; set; }


        List<SubjectBO> lstSubject { get; set; }
    }

    public class SubjectBO
    {
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public int IsCommenting { get; set; }
        public string EqualComment { get; set; }
        public string Capacity { get; set; } // Năng lực
        public string Quality { get; set; } // Phẩm chất
    }
}
