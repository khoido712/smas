﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RewardCommentFinalBO
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int EducationLevelID { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string RewardID { get; set; }
        public string EndingComment { get; set; } // Nhan xet
        public int? PeriodicEndingMark { get; set; } // Diem KTDK
        public string EndingEvaluation { get; set; } // Danh gia mon hoc
        public string MessageError { get; set; }
        public bool pass { get; set; }
        public string ShortName { get; set; }
        public int? OrderInClass { get; set; }
        public int? OrderClass { get; set; }
        public int PupilStatus { get; set; }

        public bool IsCombinedClass { get; set; }
        public int? SchoolTypeID { get; set; }
        public bool? IsDisabled { get; set; }
        public int? Genre { get; set; }
        public int? EthnicID { get; set; }

        public string RewardMode { get; set; }
    }
}
