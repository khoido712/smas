﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class KyThi
    {
        public int KiThiID { get; set; }
        public string TenKiThi { get; set; }

        public string MoTa { get; set; }

        public DateTime HanChotDangKy { get; set; }

        public bool DangKyGiamThiTHCS { get; set; }

        public bool DangKyGiamThiTHPT { get; set; }

        public bool DangKyGiamKhaoTHCS { get; set; }

        public bool DangKyGiamKhaoTHPT { get; set; }

        public bool DangKyPhucVuHoiDongTHCS { get; set; }

        public bool DangKyPhucVuHoiDongTHPT { get; set; }

        public bool TrangThai { get; set; }

        public int ManagingUnitID { get; set; }

        public int NienHocID { get; set; }

        public int DaKhoa { get; set; }

        public int KiThiCap { get; set; }

        public string slChild { get; set; }

    }

    public class RootObjectExam
    {
        public List<KyThi> DanhSachKyThi { get; set; }
    }

    public class MonThi
    {
        public int MonThiID { get; set; }
        public string TenMonThi { get; set; }
        public string MoTa { get; set; }

        public string TienTo { get; set; }
    }

    public class RootKiThiMon
    {
        public List<MonThi> KiThiMon { get; set; }
    }

    public class RootEthnic
    {
        public List<EthnicResponse> Table { get; set; }
    }

    public class EthnicResponse
    {
        public int DanTocID { get; set; }
        public string TenDanToc { get; set; }
    }
    public class DanhSachThiSinh
    {
        public int STT { get; set; }
        public string MaThiSinh { get; set; }

        public string SchoolID { get; set; }
        public int HocSinhLopID { get; set; }
        public int KiThiID { get; set; }
        public int MonThiID { get; set; }
        public string TenDuAn { get; set; }
        public int Khoi { get; set; }
        public string TenLop { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool Phai { get; set; }
        public string NgaySinh { get; set; }
        public string NoiSinh { get; set; }
        public int DanTocID { get; set; }
        public string TenDanToc { get; set; }
        public string SBD { get; set; }
        public bool DaDuyet { get; set; }
        public decimal? Diem { get; set; }
        public string XepGiai { get; set; }
    }
    public class RootThiSinh
    {
        public List<DanhSachThiSinh> Table { get; set; }
    }



    public class ThiSinhSMAS
    {
        public string MaThiSinh { get; set; }
        public int HocSinhLopID { get; set; }

        public int Khoi { get; set; }

        public string TenLop { get; set; }

        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool Phai { get; set; }
        public string NgaySinh { get; set; }
        public string NoiSinh { get; set; }

        public int DanTocID { get; set; }
        public string TenDuAn { get; set; }
    }


    public class CanBo
    {
        public string MaCanBo { get; set; }
        public int NguoiDungID { get; set; }

        public string TenNguoiDung { get; set; }

        public string NgaySinh { get; set; }

        public string GioiTinh { get; set; }
        public int DanTocID { get; set; }
        public string SDT { get; set; }
        public string DSChuyenMon { get; set; }
        public string DSKhoi { get; set; }

        public int TrinhDoDaoTaoID { get; set; }
    }


    public class RootCanBo
    {
        public List<CanBo> Table { get; set; }
    }

    public class MapSource<T>
    {
        public string SourceName { get; set; }
        public T SourceId { get; set; }
    }

    public class SupervisingDeptExamBO
    {
        public int SupervisingDeptID { get; set; }
        public string SupervisingDeptCode { get; set; }
        public string SupervisingDeptName { get; set; }

        public int SourceUnitId { get; set; }
    }

    public class ResponseExceptionMessage
    {
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionType { get; set; }
        public string StackTrace { get; set; }
    }


    public class ThiSinhDTO
    {
        public string SchoolID { get; set; }
        public int kiThiID { get; set; }
        public int monThiID { get; set; }
        public string jsThiSinh { get; set; }
    }

    public class NopCanBoDTO
    {
        //int kythiId, string matruong, int loaicanbo, string jsCanBo
        public int KythiId { get; set; }
        public string MaTruong { get; set; }
        public int LoaiCanBo { get; set; }

        public string JsCanBo { get; set; }
    }
}
