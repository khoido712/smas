﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportMarkRecordSearchBO
    {
        public int EducationLevelID { get; set; }
        public int Semester { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int AppliedLevel { get; set; }
        public int SubjectID { get; set; }
        public bool IsCheckedFemale { get; set; }
        public bool IsCheckedEthnic { get; set; }
        public bool IsCheckedFemaleEthnic { get; set; }
    }
}
