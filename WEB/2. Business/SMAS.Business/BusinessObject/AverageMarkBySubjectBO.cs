﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
   public class AverageMarkBySubjectBO
    {

        public int Semester { get; set; }
        public int IsCommenting { get; set; }
        public int SubjectID { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int AppliedLevel { get; set; }
        public int StatisticLevelReportID { get; set; }
        public int EmployeeID { get; set; }
        public int MarkType { get; set; }
        public string MarkColumn { get; set; }
        public bool FemaleChecked { get; set; }
        public bool EthnicChecked { get; set; }
        public bool FemaleEthnicChecked { get; set; }
        public bool IsPeriod { get; set; }
        public int? PeriodID { get; set; }
        public List<EducationLevel> EducationLevels { get; set; }
    }
}
