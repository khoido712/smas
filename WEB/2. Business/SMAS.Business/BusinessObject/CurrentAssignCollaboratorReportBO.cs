﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class CurrentAssignCollaboratorReportBO
    {
        public int Order { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string CommuneName { get; set; }
        public int? AdminID { get; set; }
        public string ChannelTypeName { get; set; }
        public int TypeID { get; set; }
        public string CollaboratorCode { get; set; }
        public string CollaboratorName { get; set; }

        public string CustomerName { get; set; }
        public string CustomerUserName { get; set; }

        public string CurrentAssignStatus { get; set; }
        public DateTime? AssignedDate { get; set; }

        public DateTime? EndAssignedDate { get; set; }
    }
}
