﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class SchoolSubjectBO
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int SchoolID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? OrderInSubject { get; set; }
        /// <summary>
        /// Ky hieu mon hoc
        /// </summary>
        public string Abbreviation { get; set; }
    }
}
