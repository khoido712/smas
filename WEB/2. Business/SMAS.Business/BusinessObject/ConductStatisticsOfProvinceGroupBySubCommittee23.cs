﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ConductStatisticsOfProvinceGroupBySubCommittee23
    {
        public int? EducationLevel { get; set; }
        public string EducationLevelName { get; set; }
        public int? SubCommittee { get; set; }
        public string SubCommitteeTitle { get; set; }
        public int? TotalExcellent { get; set; }
        public int? TotalGood { get; set; }
        public int? TotalNormal { get; set; }
        public int? TotalWeak { get; set; }
        public int? TotalPoor { get; set; }
        public int? TotalPass { get; set; }
        public int? TotalFail { get; set; }
        public double? PercentExcellent { get; set; }
        public double? PercentGood { get; set; }
        public double? PercentNormal { get; set; }
        public double? PercentWeak { get; set; }
        public double? PercentPoor { get; set; }
        public double? PercentPass { get; set; }
        public double? PercentFail { get; set; }
        public int? TotalPupil { get; set; }
        public int? TotalAboveAverage { get; set; }
        public double? PercentAboveAverage { get; set; }

    }
}
