﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilRetestRegistrationBO
    {
        public int? EducationLevelID { get; set; }
        public int? Semester { get; set; }
        public int? AcademicYearID { get; set; }
        public int? SchoolID { get; set; }
        public int? AppliedLevel { get; set; }
        public int? SubjectID { get; set; }

        public string GenreName { get; set; }
        public int? IsCommenting { get; set; }
        public int? ClassID { get; set; }
        public string ClassName { get; set; }
        public int? ClassOrderNumber { get; set; }
        public string FullName { get; set; }
        public string PupilCode { get; set; }
        public DateTime? BirthDate { get; set; }
        public string JudgementResult { get; set; }
        public decimal? SummedUpMark { get; set; }
        public int? OrderInClass { get; set; }
        public Nullable<System.DateTime> RegisteredDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsAbsentContest { get; set; }
        public string Reason { get; set; }   
        public int PupilID { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public int PupilRetestRegistrationID { get; set; }
        
    }
}
