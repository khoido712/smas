﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SummarySMSBO
    {
        public int? ActivitiDate { get; set; }
        public int? DayMark { get; set; }
        public int? Exchange { get; set; }
        public int? GrowthMonth { get; set; }
        public int? HealthDate { get; set; }
        public int? MonthMark { get; set; }
        public int? Parent { get; set; }
        public int? PeriodicHealth { get; set; }
        public int? SemesterResult { get; set; }
        public int? Teacher { get; set; }
        public int? TrainingDate { get; set; }
        public int? WeekMark { get; set; }
        public int? System { get; set; }
    }
}
