﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ReportGeneralPupilMarkBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int isCommenting { get; set; }
        public string Judgement { get; set; }
        public string Mark { get; set; }
    }
}
