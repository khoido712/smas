﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportNumberOfPupilByTeacherBO
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string FacultyName { get; set; }
        public int NumberPupil { get; set; }
    }
}
