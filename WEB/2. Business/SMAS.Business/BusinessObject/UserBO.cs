﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class UserBO
    {
        public int EmployeeID { get; set; }

        public string UserName { get; set; }
    }
}
