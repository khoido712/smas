﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
   public class PushNotificationBO
    {
       public int ContactID { get; set; }
       public string Content { get; set; }
       public string ShortContent { get; set; }
       public int IsPupil { get; set; }
    }
}
