using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class StatisticsForUnitBO
    {
        public int AppliedLevel { get; set; }
        public int SchoolID { get; set; }
        public int Semester { get; set; }
        public int EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public int SubCommitteeID { get; set; }
        public int Year { get; set; }
        public int FemaleType { get; set; }
        public int EthnicType { get; set; }
        public int FemaleEthnicType { get; set; }
    }
}
