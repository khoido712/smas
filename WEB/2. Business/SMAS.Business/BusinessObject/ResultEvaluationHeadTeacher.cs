﻿using SMAS.Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ResultEvaluationHeadTeacher
    {
        /// <summary>
        /// Luu loai danh gia
        /// </summary>
        public int evaluationID { get; set; }

        /// <summary>
        /// luu ma mon hoc 
        /// </summary>
        public int subject { get; set; }
        
        /// <summary>
        /// luu hoc ki 
        /// </summary>
        public int semester { get; set; }

        /// <summary>
        /// luu ket qua danh gia "HT hoac "CHT"
        /// </summary>
        public string checkResult { get; set; }

        /// <summary>
        /// Diem danh gia cuoi ky
        /// </summary>
        public int? EndingMark { get; set; }
        /// <summary>
        /// Diem thi lai
        /// </summary>
        public int? RetestMark { get; set; }
        /// <summary>
        /// Nhan xet cuoi ky
        /// </summary>
        public string EndingEvaluation { get; set; }
        
        /// <summary>
        /// Danh gia bo sung: 0: khong co danh gia, 1: HT, 2:CHT
        /// </summary>
        public int RateAdd { get; set; }
        public ResultEvaluationHeadTeacher()
        {
            this.evaluationID = 0;
            this.subject = 0;
            this.semester = 0;
            this.checkResult = "";
            this.EndingMark = null;
            this.RetestMark = null;
            this.EndingEvaluation = "";
            this.RateAdd = 0;

        }
    }

    public class SubjectReportHeadTeacher
    {
        public const string CAPACITIES_COL = "NANGLUC";
        public const string QUALITIES_COL = "PHAMCHAT";
        public const string FELICITATION_COL = "KHENTHUONG";
        public const string FINNISH_PRIMARY_EDUCATION = "HOANTHANHCHUONGTRINHLOPHOC";
        public const int END_COLUMN = 47;
        /// <summary>
        /// Ten mon hoc khong dau
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// Thu tu mon hoc trong file excel
        /// </summary>
        public int OderId { get; set; }
        /// <summary>
        /// Kieu mon: 0 tinh diem, 1: nhan xet
        /// </summary>
        public int IsCommenting { get; set; }

        /// <summary>
        /// Cot bat dau cua mon hoc
        /// </summary>
        public int StartColumn { get; set; }
        /// <summary>
        /// So cot cua 1 mon hoc trong file excel
        /// </summary>
        public int ColumnNumber { get; set; }

        /// <summary>
        /// Tao danh sach mon hoc theo thu tu trong bao cao
        /// </summary>
        /// <param name="subjectFL"></param>
        /// <returns></returns>
        public static List<SubjectReportHeadTeacher> CreateSubject(string subjectFL)
        {
            List<SubjectReportHeadTeacher> lstSubject = new List<SubjectReportHeadTeacher>();
            SubjectReportHeadTeacher objSubject = null;
            #region dien mon hoc
            //mon TV
            objSubject = new SubjectReportHeadTeacher { ShortName = "TIENGVIET", ColumnNumber = 4, IsCommenting = 0, OderId = 1, StartColumn = 3 };
            lstSubject.Add(objSubject);
            //Mon Toan
            objSubject = new SubjectReportHeadTeacher { ShortName = "TOAN", ColumnNumber = 4, IsCommenting = 0, OderId = 2, StartColumn = 7 };
            lstSubject.Add(objSubject);
            //Tu nhien xa hoi hoac Khoa hoc
            objSubject = new SubjectReportHeadTeacher { ShortName = "TUNHIENVAXAHOI", ColumnNumber = 4, IsCommenting = 0, OderId = 3, StartColumn = 11 };
            lstSubject.Add(objSubject);

            objSubject = new SubjectReportHeadTeacher { ShortName = "KHOAHOC", ColumnNumber = 4, IsCommenting = 0, OderId = 3, StartColumn = 11 };
            lstSubject.Add(objSubject);
            //LSDL
            objSubject = new SubjectReportHeadTeacher { ShortName = "LICHSUVADIALI", ColumnNumber = 4, IsCommenting = 0, OderId = 4, StartColumn = 15 };
            lstSubject.Add(objSubject);
            //LSDL
            objSubject = new SubjectReportHeadTeacher { ShortName = "LICHSUVADIALY", ColumnNumber = 4, IsCommenting = 0, OderId = 4, StartColumn = 15 };
            lstSubject.Add(objSubject);
            //ngoai ngu
            objSubject = new SubjectReportHeadTeacher { ShortName = subjectFL, ColumnNumber = 4, IsCommenting = 0, OderId = 5, StartColumn = 19 };
            lstSubject.Add(objSubject);
            //Tin hoc
            objSubject = new SubjectReportHeadTeacher { ShortName = "TINHOC", ColumnNumber = 4, IsCommenting = 0, OderId = 6, StartColumn = 23 };
            lstSubject.Add(objSubject);
            //Tien dan toc
            objSubject = new SubjectReportHeadTeacher { ShortName = "TIENGDANTOC", ColumnNumber = 4, IsCommenting = 0, OderId = 7, StartColumn = 27 };
            lstSubject.Add(objSubject);

            //dao duc
            objSubject = new SubjectReportHeadTeacher { ShortName = "DAODUC", ColumnNumber = 2, IsCommenting = 1, OderId = 8, StartColumn = 31 };
            lstSubject.Add(objSubject);

            //AMNHAC
            objSubject = new SubjectReportHeadTeacher { ShortName = "AMNHAC", ColumnNumber = 2, IsCommenting = 1, OderId = 9, StartColumn = 33 };
            lstSubject.Add(objSubject);

            //MYTHUAT
            objSubject = new SubjectReportHeadTeacher { ShortName = "MYTHUAT", ColumnNumber = 2, IsCommenting = 1, OderId = 10, StartColumn = 35 };
            lstSubject.Add(objSubject);

            //KYTHUAT
            objSubject = new SubjectReportHeadTeacher { ShortName = "KYTHUAT", ColumnNumber = 2, IsCommenting = 1, OderId = 11, StartColumn = 37 };
            lstSubject.Add(objSubject);

            //THUCONG
            objSubject = new SubjectReportHeadTeacher{ShortName = "THUCONG",ColumnNumber = 2,IsCommenting = 1,OderId = 11,StartColumn = 37};
            lstSubject.Add(objSubject);

            //THEDUC
            objSubject = new SubjectReportHeadTeacher{ShortName = "THEDUC",ColumnNumber = 2,IsCommenting = 1,OderId = 12,StartColumn = 39};
            lstSubject.Add(objSubject);

            //NANG LUC
            objSubject = new SubjectReportHeadTeacher{ShortName = CAPACITIES_COL,ColumnNumber = 2,IsCommenting = 1,OderId = 13,StartColumn = 41};
            lstSubject.Add(objSubject);

            //Pham chat
            objSubject = new SubjectReportHeadTeacher{ShortName = QUALITIES_COL,ColumnNumber = 2,IsCommenting = 1,OderId = 14,StartColumn = 43};
            lstSubject.Add(objSubject);

            //Khen thuong
            objSubject = new SubjectReportHeadTeacher{ShortName = FELICITATION_COL,ColumnNumber = 1,IsCommenting = 1,OderId = 15,StartColumn = 45};
            lstSubject.Add(objSubject);

            //Hoan thanh chuong trinh lop hoc
            objSubject = new SubjectReportHeadTeacher{ShortName = FINNISH_PRIMARY_EDUCATION,ColumnNumber = 2,IsCommenting = 1,OderId = 16,StartColumn = 46};
            lstSubject.Add(objSubject);
            #endregion
            /*IDictionary<string, int> lstSubject1 = new Dictionary<string, int>
            {
                {"TIENGVIET", 1},
                {"TOAN", 2},
                {"TUNHIENVAXAHOI", 3}, 
                {"KHOAHOC", 3},
                {"LICHSUVADIALI", 4},
                {"LICHSUVADIALY", 4},
                {subjectFL, 5},               
                {"TINHOC", 6},
                {"TIENGDANTOC", 7},
                {"DAODUC", 8}, 
                {"AMNHAC", 9},
                {"MYTHUAT", 10},
                {"KYTHUAT", 11}, 
                {"THUCONG", 11}, 
                {"THEDUC", 12},
                {EvaluationCommentActionAuditBO.CAPACITIES_COL, 13}, 
                {EvaluationCommentActionAuditBO.QUALITIES_COL, 14},
                {EvaluationCommentActionAuditBO.FELICITATION_COL, 15}
            };
            return lstSubject1;*/
            return lstSubject;
        }
    }

}
