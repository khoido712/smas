﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SMAS.Business.BusinessObject
{
   public class MineralOfFoodBO
    {
        public int FoodID { get; set; }
        public decimal ProteinAnimal { get; set; }
        public decimal ProteinPlant { get; set; }
        public decimal FatAnimal { get; set; }
        public decimal FatPlant { get; set; }
        public decimal Sugar { get; set; }
        public decimal Calo { get; set; }
        public List<FoodMineralBO> lstFoodMineral { get; set; }
    }
}
