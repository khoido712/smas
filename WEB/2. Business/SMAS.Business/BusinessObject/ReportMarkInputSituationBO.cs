﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ReportMarkInputSituationBO
    {
        public List<string> ListClassName { get; set; }
        public List<InputMarkOfSubjectBO> ListSubject { get; set; }
    }

    public class ReportMissingMarkSituationBO
    {
        public List<string> ListSubjectName { get; set; }
        public List<MissingMarkOfPupilBO> ListPupil { get; set; }
    }

    public class MissingMarkOfPupilBO 
    {
        public string PupilName { get; set; }
        public string ClassName { get; set; }
        public int Status {get;set;}
        public List<string> ListMissingMark { get; set; }
    }

    public class InputMarkOfSubjectBO
    {
        public string SubjectName { get; set; }
        public List<int?> ListInterview { get; set; }
        public List<string> listStrInterview { get; set; }
        public List<int?> ListWriting { get; set; }
        public List<string> ListStrWriting { get; set; }
        public List<int?> ListTwiceCoeffiecient { get; set; }
        public List<string> ListStrTwiceCoeffiecient { get; set; }
        public List<int?> ListFinal { get; set; }
        public List<string> ListStrFinal { get; set; }

    }
}
