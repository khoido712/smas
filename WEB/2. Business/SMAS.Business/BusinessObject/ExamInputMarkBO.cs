﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ExamInputMarkBO
    {
        public long ExamInputMarkID { get; set; }
        public long ExaminationsID { get; set; }
        public long ExamGroupID { get; set; }
        public int SubjectID { get; set; }
        public long ExamPupilID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EducationLevelID { get; set; }
        public Nullable<decimal> ExamMark { get; set; }
        public Nullable<decimal> ActualMark { get; set; }
        public string ExamJudgeMark { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }

        public long? ExamRoomID { get; set; }
        public string PupilCode { get; set; }
        public string PupilName { get; set; }
        public DateTime BirthDay { get; set; }
        public int? EthnicID { get; set; }
        public int Genre { get; set; }
        public string ClassName { get; set; }
        public string ExamRoomCode { get; set; }
        public string ExamineeNumber { get; set; }
        public string SubjectName { get; set; }
       
        public long ExamDetachableBagID { get; set; }
        public string ExamDetachableBagCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string EthnicCode { get; set; }
        public string NoteReport { get; set; }
        public bool Error { get; set; }
        public string MesageError { get; set; }

        public List<ExamPupilViolateBO> listViolate;
        public string Note
        {
            get
            {
                string note = String.Empty;
                if (listViolate != null && listViolate.Count > 0)
                {
                for (int i = 0; i < listViolate.Count; i++)
                {
                    note = note + listViolate[i].Note;
                    if (i < listViolate.Count - 1)
                    {
                        note = note + ", ";
                    }
                }
                }
                return note;
            }
        }

        public int isAbsence { get; set; }
        public int penalizedMark { get; set; }

        public string ExamMarkInput { get; set; }
        public string ActualMarkInput { get; set; }

        public long ExamCandenceBagID { get; set; }
        public string ExamCandenceBagCode { get; set; }
    }
}
