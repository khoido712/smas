﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class WorkFlowUserBO
    {
        public int WorkFlowID { get; set; }
        public string WorkFlowName { get; set; }
        public int UserID { get; set; }
        public int OrderID { get; set; }
        public bool IsPopUp { get; set; }
        public bool IsAutoAlert { get; set; }
        public string ViewName { get; set; }
        public int DefaultTab { get; set; }
        public string AppliedLevel { get; set; }
        public string ProductVersion { get; set; }
        public bool AvailableAdmin { get; set; }
        public bool AvailableTeacher { get; set; }
        public bool AvailableSup { get; set; }
    }
}
