﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class MarkBySubjectBO
    {
        public int EducationLevelID { get; set; }
        public int TotalCount { get; set; }
        public int ClassID { get; set; }
        public int Loai { get; set; }
        public Dictionary<string, object> dicMark { get; set; }
    }
}
