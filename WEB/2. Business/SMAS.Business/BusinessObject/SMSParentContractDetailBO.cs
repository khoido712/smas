﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS_SCHOOL.Utility.BusinessObject
{
    public class SMSParentContractDetailBO
    {
        public int PupilID { get; set; }
        public long SMSParentContractDetailID { get; set; }
        
        public long SMSParentContractID { get; set; }
        
        public string Subscriber { get; set; }
        
        public short SubscriberType { get; set; }
        
        public int SubscriptionTime { get; set; }
        
        public int SubscriptionStatus { get; set; }
        
        public int Year { get; set; }
        
        public decimal Money { get; set; }
        
        public short? PaymentType { get; set; }
        
        public bool IsActive { get; set; }
        
        public DateTime CreatedTime { get; set; }
        
        public DateTime? UpdatedTime { get; set; }
        
        public bool? IsRenewal { get; set; }

        public int MainServicePackageID { get; set; }
        public int? ServicePackageID { get; set; }

        public string ServiceCode { get; set; }
        
        public string UpdateNote { get; set; }

        public string Relationship { get; set; }
        public string ReceiverName { get; set; }
        public int MaxSMS { get; set; }
        public decimal? ViettelPrice { get; set; }
        public decimal? OtherPrice { get; set; }
        public bool? IsLimit { get; set; }
        public int ClassID { get; set; }
    }
}
