﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class GeneralContractReportBO
    {
        public int Order { get; set; }

        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int Status { get; set; }

        public int DistrictId { get; set; }
        public string DistrictName { get; set; }


        public Guid? CreatedUserId { get; set; }
        public string CreatedUserName { get; set; }

        public int NumOfContractHasSales { get; set; }

        public int NumOfContractNotSales { get; set; }

        public int NumOfNewContract { get; set; }
        public int NumOfCancelContract { get; set; }

        public int NumOfChangedContract { get; set; }
    }
}
