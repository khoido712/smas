﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class CollectionCommentsBO
    {
        public long CollectionCommentsID { get; set; }
        public string CommentCode { get; set; }
        public string Comment { get; set; }
    }
}
