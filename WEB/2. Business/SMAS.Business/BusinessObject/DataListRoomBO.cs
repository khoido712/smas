﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class DataListRoomBO
    {
        public int ExaminationRoomID { get; set; }
        public string RoomDetail { get; set; }
    }
}
