﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class TeacherNoteBookMonthBO
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public int MonthID { get; set; }
        public string MonthName { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public string CommentSubject { get; set; }
        public string CommentCQ { get; set; }
        public int? OrderInSubject { get; set; }
        public int Semester { get; set; }
    }
}
