using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    /// <summary>
    /// 
    /// <author>DungNT77</author>
    /// <date>13/12/2012 3:25:48 PM</date>
    /// </summary>
  public  class SearchCandidateMarkBO
    {
        #region Member Variables
        public System.Int32 CandidateID { get; set; }

        public System.Nullable<System.Int32> ExaminationID { get; set; }

        public System.Nullable<System.Int32> SubjectID { get; set; }

        public System.Nullable<System.Int32> RoomID { get; set; }

        public System.Nullable<System.Int32> ClassID { get; set; }

        public System.Int32 PupilID { get; set; }

        public System.Nullable<System.Int32> OrderNumber { get; set; }

        public System.Nullable<System.Int32> EducationLevelID { get; set; }

        public System.String Judgement { get; set; }

        public System.Nullable<System.Int32> NamedListNumber { get; set; }

        public System.Nullable<System.Boolean> HasReason { get; set; }

        public System.String ReasonDetail { get; set; }


        public string PupilCode { get; set; }

        public string FullName { get; set; }

        public string ClassName { get; set; }

        public DateTime? BirthDate { get; set; }

        public System.String BirthPlace { get; set; }

        public string GenreName { get; set; }

        public string NamedListCode { get; set; }

        public string RoomTitle { get; set; }

        public string ExamMark { get; set; }

        public string RealMark { get; set; }

        public string Description { get; set; }

        public System.Int32 SupervisingDept { get; set; }

        public System.Int32 ProvinceID { get; set; }

        public System.Int32 AcademicYearID { get; set; }
        #endregion


    }
}
