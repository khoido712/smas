﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class EmisRequestBO
    {
        public int SchoolID { get; set; }
        public EmisRequest EmisRequest { get; set; }
        public int? TrainingTypeID { get; set; }
        public int? DistrictID { get; set; }
        public int? ProvinceID { get; set; }
        public int AcademicYearID { get; set; }
    }
}
