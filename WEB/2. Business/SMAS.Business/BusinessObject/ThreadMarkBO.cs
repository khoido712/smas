﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ThreadMarkBO
    {
        public int SchoolID;
        public int ClassID;
        public int SubjectID; 
        public int AcademicYearID;
        public int Semester;
        public int? PeriodID;
        public List<int> PupilIds;
        public int Type;
        public ThreadMarkBO()
        {
            PupilIds = new List<int>();
        }
        public ThreadMarkBO(int schoolID, int classID, int subjectID, int academicYearID, int semester, int periodID, List<int> pupilIDs, int type)
        {
            this.SchoolID = schoolID;
            this.ClassID = classID;
            this.AcademicYearID = academicYearID;
            this.Semester = semester;
            this.PeriodID = periodID;
            this.PupilIds = pupilIDs;
            this.Type = type;
        }
    }
}
