﻿
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class MarkStatisticBO
    {
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int SuperVisingDeptID { get; set; }
        public string SupervisingDeptName { get; set; }
        public int Last2DigitNumberProvince { get; set; }
        public int EducationGrade { get; set; }
        public int? SchoolType { get; set; }
        public int? TrainingType { get; set; }
        public string SchoolCode { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public int AcademicYearID { get; set; }
        public int Year { get; set; }
        public int AppliedLevel { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public int Semester { get; set; }
        public int SubjectID { get; set; }
        public string ReportCode { get; set; }
        public int MarkType { get; set; }
        public int CriteriaReportID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int? OrderInClass { get; set; }
        public Nullable<System.DateTime> ProcessedDate { get; set; }
        public Nullable<int> MarkLevel00 { get; set; }
        public Nullable<int> MarkLevel01 { get; set; }
        public Nullable<int> MarkLevel02 { get; set; }
        public Nullable<int> MarkLevel03 { get; set; }
        public Nullable<int> MarkLevel04 { get; set; }
        public Nullable<int> MarkLevel05 { get; set; }
        public Nullable<int> MarkLevel06 { get; set; }
        public Nullable<int> MarkLevel07 { get; set; }
        public Nullable<int> MarkLevel08 { get; set; }
        public Nullable<int> MarkLevel09 { get; set; }
        public Nullable<int> MarkLevel10 { get; set; }
        public Nullable<int> MarkLevel11 { get; set; }
        public Nullable<int> MarkLevel12 { get; set; }
        public Nullable<int> MarkLevel13 { get; set; }
        public Nullable<int> MarkLevel14 { get; set; }
        public Nullable<int> MarkLevel15 { get; set; }
        public Nullable<int> MarkLevel16 { get; set; }
        public Nullable<int> MarkLevel17 { get; set; }
        public Nullable<int> MarkLevel18 { get; set; }
        public Nullable<int> MarkLevel19 { get; set; }
        public Nullable<int> MarkLevel20 { get; set; }
        public Nullable<int> MarkLevel21 { get; set; }
        public Nullable<int> MarkLevel22 { get; set; }
        public Nullable<int> MarkLevel23 { get; set; }
        public Nullable<int> MarkLevel24 { get; set; }
        public Nullable<int> MarkLevel25 { get; set; }
        public Nullable<int> MarkLevel26 { get; set; }
        public Nullable<int> MarkLevel27 { get; set; }
        public Nullable<int> MarkLevel28 { get; set; }
        public Nullable<int> MarkLevel29 { get; set; }
        public Nullable<int> MarkLevel30 { get; set; }
        public Nullable<int> MarkLevel31 { get; set; }
        public Nullable<int> MarkLevel32 { get; set; }
        public Nullable<int> MarkLevel33 { get; set; }
        public Nullable<int> MarkLevel34 { get; set; }
        public Nullable<int> MarkLevel35 { get; set; }
        public Nullable<int> MarkLevel36 { get; set; }
        public Nullable<int> MarkLevel37 { get; set; }
        public Nullable<int> MarkLevel38 { get; set; }
        public Nullable<int> MarkLevel39 { get; set; }
        public Nullable<int> MarkLevel40 { get; set; }
        public string MarkLevel41 { get; set; }
        public string MarkLevel42 { get; set; }
        public Nullable<int> MarkTotal { get; set; }
        public Nullable<int> BelowAverage { get; set; }
        public Nullable<int> OnAverage { get; set; }
        public Nullable<int> PupilTotal { get; set; }
        public string Critea { get; set; }
        public int OrderCritea { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
        public virtual SubjectCat SubjectCat { get; set; }
    }
}
