﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class EmployeeSMSBO
    {
        public int EmployeeID { get; set; }

        public string FullName { get; set; }

        public string Mobile { get; set; }

        public int SupervisingDeptID { get; set; }

        public string EmployeeCode { get; set; }
        public string SuperVisingDeptName { get; set; }
        public bool isNumberVT { get; set; }
        public bool? isFreeSMS { get; set; }
        public bool? isFreeExSMS { get; set; }
        /// <summary>
        /// Tong so tin nhan noi mang da nhan trong thang
        /// </summary>
        public int TotalInternalSMS { get; set; }

        /// <summary>
        /// Tong so tin nhan ngoai mang da nhan trong thang
        /// </summary>
        public int TotalExternalSMS { get; set; }

        public int TotalSMSPromotion { get; set; }
        public string strTotalSMSPromotion
        {
            get
            {
                string str = string.Empty;
                if (!string.IsNullOrEmpty(Mobile))
                {
                    if (isNumberVT)
                    {
                        if (isFreeSMS.HasValue && isFreeSMS.Value)
                        {
                            str = "Miễn phí";
                        }
                        else
                        {
                            str = TotalSMSPromotion.ToString();
                        }
                    }
                    else
                    {
                        if (isFreeExSMS.HasValue && isFreeExSMS.Value)
                        {
                            str = "Miễn phí";
                        }
                        else
                        {
                            str = TotalSMSPromotion.ToString();
                        }
                    }
                }
                return str;
            }
        }
    }
}
