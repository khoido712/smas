﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class ResultBO
    {
        public int Value { get; set; }

        /// <summary>
        /// so du tai khoan sau khi gui tin nhan
        /// </summary>
        public string Balance { get; set; }

        /// <summary>
        /// Số tin nhắn khuyến mãi còn lại sau khi gửi tin nhắn
        /// </summary>
        public int NumOfPromotionInternalSMS { get; set; }
        /// <summary>
        /// so tien giao dich
        /// </summary>
        public decimal TransAmount { get; set; }

        /// <summary>
        /// true neu vi dien tu khong du de gui tin nhan
        /// </summary>
        public bool OutOfBalance { get; set; }

        public int NumSendSuccess { get; set; }

        public int TotalSMSSentInMainBalance { get; set; }

        public bool isError { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMsg { get; set; }

        public string JsonResult { get; set; }

        /// <summary>
        /// true neu vi dien tu khong du de gui tin nhan
        /// </summary>
        public List<ClassBO> LstClassNotSent { get; set; }
        public string Result { get; set; }
        public string LogResult { get; set; }
        public int? MessageSend { get; set; }
    }
}
