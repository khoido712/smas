﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class PupilContractBO
    {
        public int ClassID { get; set; }
        /// <summary>
        /// id hoc sinh
        /// </summary>
        public int PupilID { get; set; }

        /// <summary>
        /// Hoc ky
        /// </summary>
        public int SubscriptionTime { get; set; }

        /// <summary>
        /// id hoc sinh
        /// </summary>
        public int SmsParentContractID { get; set; }

        /// <summary>
        /// SDT
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Nam
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Id contract detail
        /// </summary>
        public long SMSParentContactDetailID { get; set; }

        /// <summary>
        /// Id of service package
        /// </summary>
        public int? ServicePackageID{ get; set; }


        /// <summary>
        /// Number of sent sms 
        /// </summary>
        public int SentSMS { get; set; }


        /// <summary>
        /// Total sms in contract detail 
        /// </summary>
        public int TotalSMS { get; set; }
    }
}
