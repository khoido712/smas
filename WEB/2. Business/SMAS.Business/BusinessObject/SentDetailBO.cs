﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class SentDetailBO
    {
        /// <summary>
        /// so luong hoc sinh
        /// </summary>
        public int TotalPupil { get; set; }

        /// <summary>
        /// so luong thue bao
        /// </summary>
        public int TotalSubscriber { get; set; }
    }
}
