using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.BusinessObject
{
    public class PupilProfileBO
    {
        public int PupilProfileID { get; set; }
        public int CurrentClassID { get; set; }
        public string CurrentClassName { get; set; }
        public int CurrentSchoolID { get; set; }
        public int CurrentAcademicYearID { get; set; }
        public Nullable<int> AreaID { get; set; }
        public string AreaName { get; set; }
        public int ProvinceID { get; set; }
        public int Grade { get; set; }
        public string ProvinceName { get; set; }
        public Nullable<int> DistrictID { get; set; }
        public Nullable<int> CommuneID { get; set; }
        public Nullable<int> EthnicID { get; set; }
        public Nullable<int> OtherEthnicID { get; set; }
        public Nullable<int> ReligionID { get; set; }
        public Nullable<int> PolicyTargetID { get; set; }
        public Nullable<int> SupportingPolicy { get; set; }
        public Nullable<bool> IsReceiveRiceSubsidy { get; set; }
        public Nullable<bool> IsResettlementTarget { get; set; }
        public Nullable<int> FamilyTypeID { get; set; }
        public Nullable<int> PriorityTypeID { get; set; }
        public Nullable<int> PreviousGraduationLevelID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int Genre { get; set; }
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string HomeTown { get; set; }
        public int? HomePlace { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string IdentifyNumber { get; set; }
        public string TempResidentalAddress { get; set; }
        public string PermanentResidentalAddress { get; set; }
        public Boolean? IsDisabled { get; set; }
        public Nullable<int> BloodType { get; set; }
        public decimal? EnrolmentMark { get; set; }
        public int ForeignLanguageTraining { get; set; }
        public Boolean? IsYoungPioneerMember { get; set; }
        public DateTime? YoungPioneerJoinedDate { get; set; }
        public string YoungPioneerJoinedPlace { get; set; }
        public Boolean? IsYouthLeageMember { get; set; }
        public DateTime? YouthLeagueJoinedDate { get; set; }
        public string YouthLeagueJoinedPlace { get; set; }
        public Boolean? IsCommunistPartyMember { get; set; }
        public DateTime? CommunistPartyJoinedDate { get; set; }
        public string CommunistPartyJoinedPlace { get; set; }
        public string FatherFullName { get; set; }
        public DateTime? FatherBirthDate { get; set; }
        public string FatherJob { get; set; }
        public string MotherFullName { get; set; }
        public string FatherMobile { get; set; }
        public DateTime? MotherBirthDate { get; set; }
        public string MotherJob { get; set; }
        public string MotherMobile { get; set; }
        public string SponsorFullName { get; set; }
        public DateTime? SponsorBirthDate { get; set; }
        public string SponsorJob { get; set; }
        public int ProfileStatus { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int EducationLevelID { get; set; }
        public int HeadTeacherID { get; set; }

        public Nullable<Decimal> HeightAtBirth { get; set; }
        public Nullable<Decimal> WeightAtBirth { get; set; }
        public string FatherEmail { get; set; }
        public string MotherEmail { get; set; }
        public string SponsorEmail { get; set; }
        public Nullable<Int32> ChildOrder { get; set; }
        public Nullable<Int32> NumberOfChild { get; set; }
        public Nullable<Int32> EatingHabit { get; set; }
        public Nullable<Int32> ReactionTrainingHabit { get; set; }
        public Nullable<Int32> AttitudeInStrangeScene { get; set; }
        public string OtherHabit { get; set; }
        public string FavoriteToy { get; set; }
        public string FavouriteTypeOfDish { get; set; }
        public string HateTypeOfDish { get; set; }
        public Nullable<Int32> EvaluationConfigID { get; set; }
        public string Name { get; set; }		
        
        public Nullable<bool> IsResidentIn { get; set; }
        public Nullable<int> DisabledTypeID { get; set; }
        public string DisabledSeverity { get; set; }
        public System.DateTime EnrolmentDate { get; set; }
        public string SponsorMobile { get; set; }
        public System.DateTime CreatedDate { get; set; }

        public Nullable<int> EnrolmentType { get; set; }
        public Nullable<int> OrderInClass { get; set; }
        public PupilOfClass PupilOfClass { get; set; }
        public string ClassName { get; set; }
        public string EthnicName { get; set; }
        public string OtherEthnicName { get; set; }
        public string ReligionName { get; set; }
        public string FamilyName { get; set; }
        public string PolicyTargetName { get; set; }
        public string PriorityTypeName { get; set; }
        public string SchoolName { get; set; }
        public string SyncCodeSchool { get; set; }
        public string DistrictName { get; set; }
        public string CommuneName { get; set; }
        public string DisabledTypeName { get; set; }
        public Nullable<int> PolicyRegimeID { get; set; }
        public string PolicyRegimeName { get; set; }

        public byte[] Image { get; set; }

        public bool IsSupportForLearning { get; set; }
        public string EducationLevel { get; set; }
        public DateTime? AssignedDate { get; set; }
        public int? SupervisingDeptID { get; set; }
        public int? ParentID { get; set; }
        public int? Year { get; set; }

        public bool? IsFatherSMS { get; set; }
        public bool? IsMotherSMS { get; set; }
        public bool? IsSponsorSMS { get; set; }
        public bool? IsRegisterContract { get; set; }

        public int? PupilLearningType { get; set; }
        public string LanguageCertificate { get; set; }
        public string ItCertificate { get; set; }

        public int? LanguageCertificateID { get; set; }
        public int? ItCertificateID { get; set; }

        public int? SubCommitteeID { get; set; }
        public string SubCommitteeName { get; set; }

        public string EthnicCode { get; set; }
        public string OtherEthnicCode { get; set; }
        public string ReligionCode { get; set; }

        public string CommnuneCode { get; set; }
        public string ProvinceCode { get; set; }
        public string DistrictCode { get; set; }

        public string PolicyTargetCode { get; set; }

        public int? VillageID { get; set; }
        public string VillageName { get; set; }
        public string StorageNumber { get; set; }
        public string VillageCode { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ClassOrderNumber { get; set; }
        public string CombineClassCode { get; set; }
        public int? ClassType { get; set; }
        public bool? IsSwimming { get; set; }
        public bool? IsUsedMoetProgram { get; set; }
        public bool? IsMinorityFather { get; set; }
        public bool? IsMinorityMother { get; set; }

        public List<string> PhoneReceiver { get; set; }
        public int NumOfSMSSubscriber { get; set; }
        public string RemainSMS { get; set; }
        public int TotalSend { get; set; }
        public int MaxSMS { get; set; }
        public int FreePackageNum { get; set; }
        public int PupilOfClassID { get; set; }
        public bool? MinorityFather { get; set; }
        public bool? MinorityMother { get; set; }
        public bool? UsedMoetProgram { get; set; }
        public string ReasonOffLearn { get; set; }
        public string Country { get; set; }
        public string TuitionFree { get; set; }
        public string TuitionReduction { get; set; }
        public bool? _IsSupportForLearning { get; set; }
        public int? ApprenticeshipGroupID { get; set; }
        /// <summary>
        /// H?t tin nh?n d�ng th?
        /// </summary>
        public bool ExpireTrial { get; set; }

        public static PupilProfileBO createFromPupilProfile(PupilProfile pupilProfile)
        {
            PupilProfileBO pp = new PupilProfileBO()
            {
                PupilProfileID = pupilProfile.PupilProfileID,
                CurrentClassID = pupilProfile.CurrentClassID,
                //CurrentClassName = pupilProfile.ClassProfile.DisplayName,
                CurrentSchoolID = pupilProfile.CurrentSchoolID,
                CurrentAcademicYearID = pupilProfile.CurrentAcademicYearID,
                AreaID = pupilProfile.AreaID,
                //AreaName = pupilProfile.Area.AreaName,
                ProvinceID = pupilProfile.ProvinceID,
                //ProvinceName = pupilProfile.Province.ProvinceName,
                DistrictID = pupilProfile.DistrictID,
                CommuneID = pupilProfile.CommuneID,
                EthnicID = pupilProfile.EthnicID,
                OtherEthnicID = pupilProfile.OtherEthnicID,
                ReligionID = pupilProfile.ReligionID,
                PolicyTargetID = pupilProfile.PolicyTargetID,
                FamilyTypeID = pupilProfile.FamilyTypeID,
                PriorityTypeID = pupilProfile.PriorityTypeID,
                PreviousGraduationLevelID = pupilProfile.PreviousGraduationLevelID,
                PupilCode = pupilProfile.PupilCode,
                FullName = pupilProfile.FullName,
                Genre = pupilProfile.Genre,
                BirthDate = pupilProfile.BirthDate,
                BirthPlace = pupilProfile.BirthPlace,
                HomeTown = pupilProfile.HomeTown,
                Telephone = pupilProfile.Telephone,
                Mobile = pupilProfile.Mobile,
                Email = pupilProfile.Email,
                TempResidentalAddress = pupilProfile.TempResidentalAddress,
                PermanentResidentalAddress = pupilProfile.PermanentResidentalAddress,
                IsDisabled = pupilProfile.IsDisabled,
                BloodType = pupilProfile.BloodType,
                EnrolmentMark = pupilProfile.EnrolmentMark,
                ForeignLanguageTraining = pupilProfile.ForeignLanguageTraining,
                IsYoungPioneerMember = pupilProfile.IsYoungPioneerMember,
                YoungPioneerJoinedDate = pupilProfile.YoungPioneerJoinedDate,
                YoungPioneerJoinedPlace = pupilProfile.YoungPioneerJoinedPlace,
                IsYouthLeageMember = pupilProfile.IsYouthLeageMember,
                YouthLeagueJoinedDate = pupilProfile.YouthLeagueJoinedDate,
                YouthLeagueJoinedPlace = pupilProfile.YouthLeagueJoinedPlace,
                IsCommunistPartyMember = pupilProfile.IsCommunistPartyMember,
                CommunistPartyJoinedDate = pupilProfile.CommunistPartyJoinedDate,
                CommunistPartyJoinedPlace = pupilProfile.CommunistPartyJoinedPlace,
                FatherFullName = pupilProfile.FatherFullName,
                MinorityFather = pupilProfile.MinorityFather,
                MinorityMother = pupilProfile.MinorityMother,
                UsedMoetProgram = pupilProfile.UsedMoetProgram
            };
            return pp;
        }
    }

    public class AutoGenericCode{
        public int MaxOrderNumber { get; set; }
        public int NumberLength { get; set; }
        public string OriginalPupilCode { get; set; }
        public int EducationLevelID { get; set; }
        public int Index { get; set; }
    }
}
