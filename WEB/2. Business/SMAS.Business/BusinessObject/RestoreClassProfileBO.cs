﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class RestoreClassProfileBO
    {
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string ReplaceClassName { get; set; }
        public string SqlUndo { get; set; }
    }
}
