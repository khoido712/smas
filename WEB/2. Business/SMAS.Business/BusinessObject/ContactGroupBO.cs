﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.BusinessObject
{
    public class ContactGroupBO
    {
        public int ContactGroupId { get; set; }

        public string ContactGroupName { get; set; }

        public List<int> ListTeacherId { get; set; }
    }
}
