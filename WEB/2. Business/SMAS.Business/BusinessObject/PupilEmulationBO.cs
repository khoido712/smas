﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.BusinessObject
{
    public class PupilEmulationBO
    {
        public int PupilEmulationID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int? Semester { get; set; }
        public int? HonourAchivementTypeID { get; set; }
        public string HonourAchivementTypeResolution { get; set; }
    }
}
