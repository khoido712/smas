﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.SearchForm
{
    public class PupilFaultSearchForm
    {

        public PupilFaultSearchForm()
        {
    
        }

        /// <summary>
        /// Tìm kiếm 'in' với column PupilFaultID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PupilFaultID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PupilFaultID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PupilFaultID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PupilFaultID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PupilFaultID</para>
        /// </summary>
        public object PupilFaultID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column PupilID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PupilID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PupilID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PupilID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PupilID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PupilID</para>
        /// </summary>
        public object PupilID { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column PupilID(PupilProfile).PupilCode
        /// </summary>
        public string PupilCode { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column PupilID(PupilProfile).PupilCode
        /// </summary>
        public string E_PupilCode { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column PupilID(PupilProfile).FullName
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column PupilID(PupilProfile).FullName
        /// </summary>
        public string E_FullName { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ClassID</para>
        /// </summary>
        public object ClassID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column FaultID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.FaultID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.FaultID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.FaultID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.FaultID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với FaultID</para>
        /// </summary>
        public object FaultID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column AcademicYearID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.AcademicYearID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.AcademicYearID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.AcademicYearID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.AcademicYearID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với AcademicYearID</para>
        /// </summary>
        public object AcademicYearID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column AcademicYearID(AcademicYear).Year. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.AcademicYear.Year)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.AcademicYear.Year in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.AcademicYear.Year in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.AcademicYear.Year = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Year</para>
        /// </summary>
        public object Year { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column EducationLevelID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.EducationLevelID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.EducationLevelID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.EducationLevelID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.EducationLevelID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với EducationLevelID</para>
        /// </summary>
        public object EducationLevelID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column EducationLevelID(EducationLevel).Grade. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.EducationLevel.Grade)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.EducationLevel.Grade in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.EducationLevel.Grade in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.EducationLevel.Grade = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Grade</para>
        /// </summary>
        public object Grade { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column ViolatedDate
        /// </summary>
        public DateTime? ViolatedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column ViolatedDate => Tạo câu lệnh SQL x.ViolatedDate >= F_ViolatedDate
        /// </summary>
        public DateTime? F_ViolatedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column ViolatedDate => Tạo câu lệnh SQL x.ViolatedDate >= T_ViolatedDate
        /// </summary>
        public DateTime? T_ViolatedDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column NumberOfFault. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.NumberOfFault)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.NumberOfFault in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.NumberOfFault in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.NumberOfFault = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với NumberOfFault</para>
        /// </summary>
        public object NumberOfFault { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column Description
        /// </summary>
        public string E_Description { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column SchoolID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.SchoolID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.SchoolID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.SchoolID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.SchoolID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với SchoolID</para>
        /// </summary>
        public object SchoolID { get; set; }

        public string Note { get; set; }




        public IQueryable<PupilFault> Search(IQueryable<PupilFault> Query)
        {

            #region PupilFaultID
            if (PupilFaultID != null)
            {
                if (PupilFaultID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PupilFaultID).Any(y => y == x.PupilFaultID));
                }
                else
                {
                    List<int?> l_PupilFaultID = PupilFaultID.ConvertToListInt();
                    if (l_PupilFaultID.Count == 1)
                    {
                        if (l_PupilFaultID[0] != -1)
                        {
                            int? l2_PupilFaultID = l_PupilFaultID[0];
                            Query = Query.Where(x => x.PupilFaultID == l2_PupilFaultID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PupilFaultID.Contains(x.PupilFaultID));
                    }
                }
            }
            #endregion


            #region PupilID
            if (PupilID != null)
            {
                if (PupilID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PupilID).Any(y => y == x.PupilID));
                }
                else
                {
                    List<int?> l_PupilID = PupilID.ConvertToListInt();
                    if (l_PupilID.Count == 1)
                    {
                        if (l_PupilID[0] != -1)
                        {
                            int? l2_PupilID = l_PupilID[0];
                            Query = Query.Where(x => x.PupilID == l2_PupilID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PupilID.Contains(x.PupilID));
                    }
                }
            }
            #endregion


            #region PupilCode
            if (!string.IsNullOrWhiteSpace(PupilCode)) Query = Query.Where(x => x.PupilProfile.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            #endregion

            #region E_PupilCode
            if (!string.IsNullOrWhiteSpace(E_PupilCode)) Query = Query.Where(x => x.PupilProfile.PupilCode.ToUpper() == E_PupilCode.ToUpper());
            #endregion


            #region FullName
            if (!string.IsNullOrWhiteSpace(FullName)) Query = Query.Where(x => x.PupilProfile.FullName.ToUpper().Contains(FullName.ToUpper()));
            #endregion

            #region E_FullName
            if (!string.IsNullOrWhiteSpace(E_FullName)) Query = Query.Where(x => x.PupilProfile.FullName.ToUpper() == E_FullName.ToUpper());
            #endregion


            #region ClassID
            if (ClassID != null)
            {
                if (ClassID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ClassID).Any(y => y == x.ClassID));
                }
                else
                {
                    List<int?> l_ClassID = ClassID.ConvertToListInt();
                    if (l_ClassID.Count == 1)
                    {
                        if (l_ClassID[0] != -1)
                        {
                            int? l2_ClassID = l_ClassID[0];
                            Query = Query.Where(x => x.ClassID == l2_ClassID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ClassID.Contains(x.ClassID));
                    }
                }
            }
            #endregion


            #region FaultID
            if (FaultID != null)
            {
                if (FaultID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)FaultID).Any(y => y == x.FaultID));
                }
                else
                {
                    List<int?> l_FaultID = FaultID.ConvertToListInt();
                    if (l_FaultID.Count == 1)
                    {
                        if (l_FaultID[0] != -1)
                        {
                            int? l2_FaultID = l_FaultID[0];
                            Query = Query.Where(x => x.FaultID == l2_FaultID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_FaultID.Contains(x.FaultID));
                    }
                }
            }
            #endregion


            #region AcademicYearID
            if (AcademicYearID != null)
            {
                if (AcademicYearID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)AcademicYearID).Any(y => y == x.AcademicYearID));
                }
                else
                {
                    List<int?> l_AcademicYearID = AcademicYearID.ConvertToListInt();
                    if (l_AcademicYearID.Count == 1)
                    {
                        if (l_AcademicYearID[0] != -1)
                        {
                            int? l2_AcademicYearID = l_AcademicYearID[0];
                            Query = Query.Where(x => x.AcademicYearID == l2_AcademicYearID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_AcademicYearID.Contains(x.AcademicYearID));
                    }
                }
            }
            #endregion


            #region Year
            if (Year != null)
            {
                if (Year is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Year).Any(y => y == x.AcademicYear.Year));
                }
                else
                {
                    List<int?> l_Year = Year.ConvertToListInt();
                    if (l_Year.Count == 1)
                    {
                        if (l_Year[0] != -1)
                        {
                            int? l2_Year = l_Year[0];
                            Query = Query.Where(x => x.AcademicYear.Year == l2_Year);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Year.Contains(x.AcademicYear.Year));
                    }
                }
            }
            #endregion


            #region EducationLevelID
            if (EducationLevelID != null)
            {
                if (EducationLevelID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)EducationLevelID).Any(y => y == x.EducationLevelID));
                }
                else
                {
                    List<int?> l_EducationLevelID = EducationLevelID.ConvertToListInt();
                    if (l_EducationLevelID.Count == 1)
                    {
                        if (l_EducationLevelID[0] != -1)
                        {
                            int? l2_EducationLevelID = l_EducationLevelID[0];
                            Query = Query.Where(x => x.EducationLevelID == l2_EducationLevelID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_EducationLevelID.Contains(x.EducationLevelID));
                    }
                }
            }
            #endregion


            #region Grade
            if (Grade != null)
            {
                if (Grade is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Grade).Any(y => y == x.EducationLevel.Grade));
                }
                else
                {
                    List<int?> l_Grade = Grade.ConvertToListInt();
                    if (l_Grade.Count == 1)
                    {
                        if (l_Grade[0] != -1)
                        {
                            int? l2_Grade = l_Grade[0];
                            Query = Query.Where(x => x.EducationLevel.Grade == l2_Grade);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Grade.Contains(x.EducationLevel.Grade));
                    }
                }
            }
            #endregion


            #region ViolatedDate
            if (ViolatedDate != null)
            {
                DateTime l_ViolatedDate = ViolatedDate.Value.Date;
                Query = Query.Where(x => x.ViolatedDate == l_ViolatedDate);
            }
            #endregion

            #region F_ViolatedDate
            if (F_ViolatedDate != null)
            {
                DateTime l_F_ViolatedDate = F_ViolatedDate.Value.Date;
                Query = Query.Where(x => x.ViolatedDate >= l_F_ViolatedDate);
            }
            #endregion

            #region T_ViolatedDate
            if (T_ViolatedDate != null)
            {
                DateTime l_T_ViolatedDate = T_ViolatedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.ViolatedDate < l_T_ViolatedDate);
            }
            #endregion


            #region NumberOfFault
            if (NumberOfFault != null)
            {
                if (NumberOfFault is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)NumberOfFault).Any(y => y == x.NumberOfFault));
                }
                else
                {
                    List<int?> l_NumberOfFault = NumberOfFault.ConvertToListInt();
                    if (l_NumberOfFault.Count == 1)
                    {
                        if (l_NumberOfFault[0] != -1)
                        {
                            int? l2_NumberOfFault = l_NumberOfFault[0];
                            Query = Query.Where(x => x.NumberOfFault == l2_NumberOfFault);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_NumberOfFault.Contains(x.NumberOfFault));
                    }
                }
            }
            #endregion


            #region Description
            if (!string.IsNullOrWhiteSpace(Description)) Query = Query.Where(x => x.Description.ToUpper().Contains(Description.ToUpper()));
            #endregion

            #region E_Description
            if (!string.IsNullOrWhiteSpace(E_Description)) Query = Query.Where(x => x.Description.ToUpper() == E_Description.ToUpper());
            #endregion


            #region SchoolID
            if (SchoolID != null)
            {
                if (SchoolID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)SchoolID).Any(y => y == x.SchoolID));
                }
                else
                {
                    List<int?> l_SchoolID = SchoolID.ConvertToListInt();
                    if (l_SchoolID.Count == 1)
                    {
                        if (l_SchoolID[0] != -1)
                        {
                            int? l2_SchoolID = l_SchoolID[0];
                            Query = Query.Where(x => x.SchoolID == l2_SchoolID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_SchoolID.Contains(x.SchoolID));
                    }
                }
            }
            #endregion




            return Query;
        }
    }
}
