﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.SearchForm
{
    public class PupilOfClassSearchForm
    {

        /// <summary>
        /// Tìm kiếm 'in' với column PupilOfClassID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PupilOfClassID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PupilOfClassID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PupilOfClassID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PupilOfClassID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PupilOfClassID</para>
        /// </summary>
        public object PupilOfClassID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column PupilID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PupilID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PupilID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PupilID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PupilID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PupilID</para>
        /// </summary>
        public object PupilID { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column PupilID(PupilProfile).PupilCode
        /// </summary>
        public string PupilCode { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column PupilID(PupilProfile).PupilCode
        /// </summary>
        public string E_PupilCode { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column PupilID(PupilProfile).FullName
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column PupilID(PupilProfile).FullName
        /// </summary>
        public string E_FullName { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ClassID</para>
        /// </summary>
        public object ClassID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassID(ClassProfile).SubCommitteeID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassProfile.SubCommitteeID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.SubCommitteeID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.SubCommitteeID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassProfile.SubCommitteeID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với SubCommitteeID</para>
        /// </summary>
        public object SubCommitteeID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassID(ClassProfile).HeadTeacherID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassProfile.HeadTeacherID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.HeadTeacherID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.HeadTeacherID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassProfile.HeadTeacherID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với HeadTeacherID</para>
        /// </summary>
        public object HeadTeacherID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassID(ClassProfile).EducationLevelID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassProfile.EducationLevelID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevelID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevelID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassProfile.EducationLevelID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với EducationLevelID</para>
        /// </summary>
        public object EducationLevelID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassID(ClassProfile).EducationLevelID(EducationLevel).Grade. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassProfile.EducationLevel.Grade)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevel.Grade in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevel.Grade in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassProfile.EducationLevel.Grade = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Grade</para>
        /// </summary>
        public object Grade { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column SchoolID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.SchoolID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.SchoolID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.SchoolID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.SchoolID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với SchoolID</para>
        /// </summary>
        public object SchoolID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column AcademicYearID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.AcademicYearID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.AcademicYearID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.AcademicYearID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.AcademicYearID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với AcademicYearID</para>
        /// </summary>
        public object AcademicYearID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column Year. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.Year)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.Year in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.Year in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.Year = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Year</para>
        /// </summary>
        public object Year { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column AssignedDate
        /// </summary>
        public DateTime? AssignedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column AssignedDate => Tạo câu lệnh SQL x.AssignedDate >= F_AssignedDate
        /// </summary>
        public DateTime? F_AssignedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column AssignedDate => Tạo câu lệnh SQL x.AssignedDate >= T_AssignedDate
        /// </summary>
        public DateTime? T_AssignedDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column OrderInClass. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.OrderInClass)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.OrderInClass in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.OrderInClass in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.OrderInClass = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với OrderInClass</para>
        /// </summary>
        public object OrderInClass { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column NoConductEstimation
        /// </summary>
        public bool? NoConductEstimation { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column Status. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.Status)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.Status in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.Status in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.Status = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Status</para>
        /// </summary>
        public object Status { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column EndDate
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column EndDate => Tạo câu lệnh SQL x.EndDate >= F_EndDate
        /// </summary>
        public DateTime? F_EndDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column EndDate => Tạo câu lệnh SQL x.EndDate >= T_EndDate
        /// </summary>
        public DateTime? T_EndDate { get; set; }




        public IQueryable<PupilOfClass> Search(IQueryable<PupilOfClass> Query)
        {

            #region PupilOfClassID
            if (PupilOfClassID != null)
            {
                if (PupilOfClassID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PupilOfClassID).Any(y => y == x.PupilOfClassID));
                }
                else
                {
                    List<int?> l_PupilOfClassID = PupilOfClassID.ConvertToListInt();
                    if (l_PupilOfClassID.Count == 1)
                    {
                        if (l_PupilOfClassID[0] != -1)
                        {
                            int? l2_PupilOfClassID = l_PupilOfClassID[0];
                            Query = Query.Where(x => x.PupilOfClassID == l2_PupilOfClassID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PupilOfClassID.Contains(x.PupilOfClassID));
                    }
                }
            }
            #endregion


            #region PupilID
            if (PupilID != null)
            {
                if (PupilID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PupilID).Any(y => y == x.PupilID));
                }
                else
                {
                    List<int?> l_PupilID = PupilID.ConvertToListInt();
                    if (l_PupilID.Count == 1)
                    {
                        if (l_PupilID[0] != -1)
                        {
                            int? l2_PupilID = l_PupilID[0];
                            Query = Query.Where(x => x.PupilID == l2_PupilID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PupilID.Contains(x.PupilID));
                    }
                }
            }
            #endregion


            #region PupilCode
            if (!string.IsNullOrWhiteSpace(PupilCode)) Query = Query.Where(x => x.PupilProfile.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            #endregion

            #region E_PupilCode
            if (!string.IsNullOrWhiteSpace(E_PupilCode)) Query = Query.Where(x => x.PupilProfile.PupilCode.ToUpper() == E_PupilCode.ToUpper());
            #endregion


            #region FullName
            if (!string.IsNullOrWhiteSpace(FullName)) Query = Query.Where(x => x.PupilProfile.FullName.ToUpper().Contains(FullName.ToUpper()));
            #endregion

            #region E_FullName
            if (!string.IsNullOrWhiteSpace(E_FullName)) Query = Query.Where(x => x.PupilProfile.FullName.ToUpper() == E_FullName.ToUpper());
            #endregion


            #region ClassID
            if (ClassID != null)
            {
                if (ClassID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ClassID).Any(y => y == x.ClassID));
                }
                else
                {
                    List<int?> l_ClassID = ClassID.ConvertToListInt();
                    if (l_ClassID.Count == 1)
                    {
                        if (l_ClassID[0] != -1)
                        {
                            int? l2_ClassID = l_ClassID[0];
                            Query = Query.Where(x => x.ClassID == l2_ClassID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ClassID.Contains(x.ClassID));
                    }
                }
            }
            #endregion


            #region SubCommitteeID
            if (SubCommitteeID != null)
            {
                if (SubCommitteeID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)SubCommitteeID).Any(y => y == x.ClassProfile.SubCommitteeID));
                }
                else
                {
                    List<int?> l_SubCommitteeID = SubCommitteeID.ConvertToListInt();
                    if (l_SubCommitteeID.Count == 1)
                    {
                        if (l_SubCommitteeID[0] != -1)
                        {
                            int? l2_SubCommitteeID = l_SubCommitteeID[0];
                            Query = Query.Where(x => x.ClassProfile.SubCommitteeID == l2_SubCommitteeID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_SubCommitteeID.Contains(x.ClassProfile.SubCommitteeID));
                    }
                }
            }
            #endregion


            #region HeadTeacherID
            if (HeadTeacherID != null)
            {
                if (HeadTeacherID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)HeadTeacherID).Any(y => y == x.ClassProfile.HeadTeacherID));
                }
                else
                {
                    List<int?> l_HeadTeacherID = HeadTeacherID.ConvertToListInt();
                    if (l_HeadTeacherID.Count == 1)
                    {
                        if (l_HeadTeacherID[0] != -1)
                        {
                            int? l2_HeadTeacherID = l_HeadTeacherID[0];
                            Query = Query.Where(x => x.ClassProfile.HeadTeacherID == l2_HeadTeacherID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_HeadTeacherID.Contains(x.ClassProfile.HeadTeacherID));
                    }
                }
            }
            #endregion


            #region EducationLevelID
            if (EducationLevelID != null)
            {
                if (EducationLevelID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)EducationLevelID).Any(y => y == x.ClassProfile.EducationLevelID));
                }
                else
                {
                    List<int?> l_EducationLevelID = EducationLevelID.ConvertToListInt();
                    if (l_EducationLevelID.Count == 1)
                    {
                        if (l_EducationLevelID[0] != -1)
                        {
                            int? l2_EducationLevelID = l_EducationLevelID[0];
                            Query = Query.Where(x => x.ClassProfile.EducationLevelID == l2_EducationLevelID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_EducationLevelID.Contains(x.ClassProfile.EducationLevelID));
                    }
                }
            }
            #endregion


            #region Grade
            if (Grade != null)
            {
                if (Grade is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Grade).Any(y => y == x.ClassProfile.EducationLevel.Grade));
                }
                else
                {
                    List<int?> l_Grade = Grade.ConvertToListInt();
                    if (l_Grade.Count == 1)
                    {
                        if (l_Grade[0] != -1)
                        {
                            int? l2_Grade = l_Grade[0];
                            Query = Query.Where(x => x.ClassProfile.EducationLevel.Grade == l2_Grade);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Grade.Contains(x.ClassProfile.EducationLevel.Grade));
                    }
                }
            }
            #endregion


            #region SchoolID
            if (SchoolID != null)
            {
                if (SchoolID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)SchoolID).Any(y => y == x.SchoolID));
                }
                else
                {
                    List<int?> l_SchoolID = SchoolID.ConvertToListInt();
                    if (l_SchoolID.Count == 1)
                    {
                        if (l_SchoolID[0] != -1)
                        {
                            int? l2_SchoolID = l_SchoolID[0];
                            Query = Query.Where(x => x.SchoolID == l2_SchoolID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_SchoolID.Contains(x.SchoolID));
                    }
                }
            }
            #endregion


            #region AcademicYearID
            if (AcademicYearID != null)
            {
                if (AcademicYearID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)AcademicYearID).Any(y => y == x.AcademicYearID));
                }
                else
                {
                    List<int?> l_AcademicYearID = AcademicYearID.ConvertToListInt();
                    if (l_AcademicYearID.Count == 1)
                    {
                        if (l_AcademicYearID[0] != -1)
                        {
                            int? l2_AcademicYearID = l_AcademicYearID[0];
                            Query = Query.Where(x => x.AcademicYearID == l2_AcademicYearID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_AcademicYearID.Contains(x.AcademicYearID));
                    }
                }
            }
            #endregion


            #region Year
            if (Year != null)
            {
                if (Year is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Year).Any(y => y == x.Year));
                }
                else
                {
                    List<int?> l_Year = Year.ConvertToListInt();
                    if (l_Year.Count == 1)
                    {
                        if (l_Year[0] != -1)
                        {
                            int? l2_Year = l_Year[0];
                            Query = Query.Where(x => x.Year == l2_Year);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Year.Contains(x.Year));
                    }
                }
            }
            #endregion


            #region AssignedDate
            if (AssignedDate != null)
            {
                DateTime l_AssignedDate = AssignedDate.Value.Date;
                Query = Query.Where(x => x.AssignedDate == l_AssignedDate);
            }
            #endregion

            #region F_AssignedDate
            if (F_AssignedDate != null)
            {
                DateTime l_F_AssignedDate = F_AssignedDate.Value.Date;
                Query = Query.Where(x => x.AssignedDate >= l_F_AssignedDate);
            }
            #endregion

            #region T_AssignedDate
            if (T_AssignedDate != null)
            {
                DateTime l_T_AssignedDate = T_AssignedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.AssignedDate < l_T_AssignedDate);
            }
            #endregion


            #region OrderInClass
            if (OrderInClass != null)
            {
                if (OrderInClass is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)OrderInClass).Any(y => y == x.OrderInClass));
                }
                else
                {
                    List<int?> l_OrderInClass = OrderInClass.ConvertToListInt();
                    if (l_OrderInClass.Count == 1)
                    {
                        if (l_OrderInClass[0] != -1)
                        {
                            int? l2_OrderInClass = l_OrderInClass[0];
                            Query = Query.Where(x => x.OrderInClass == l2_OrderInClass);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_OrderInClass.Contains(x.OrderInClass));
                    }
                }
            }
            #endregion


            #region NoConductEstimation
            if (NoConductEstimation != null) Query = Query.Where(x => x.NoConductEstimation == NoConductEstimation);
            #endregion


            #region Status
            if (Status != null)
            {
                if (Status is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Status).Any(y => y == x.Status));
                }
                else
                {
                    List<int?> l_Status = Status.ConvertToListInt();
                    if (l_Status.Count == 1)
                    {
                        if (l_Status[0] != -1)
                        {
                            int? l2_Status = l_Status[0];
                            Query = Query.Where(x => x.Status == l2_Status);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Status.Contains(x.Status));
                    }
                }
            }
            #endregion


            #region EndDate
            if (EndDate != null)
            {
                DateTime l_EndDate = EndDate.Value.Date;
                Query = Query.Where(x => x.EndDate == l_EndDate);
            }
            #endregion

            #region F_EndDate
            if (F_EndDate != null)
            {
                DateTime l_F_EndDate = F_EndDate.Value.Date;
                Query = Query.Where(x => x.EndDate >= l_F_EndDate);
            }
            #endregion

            #region T_EndDate
            if (T_EndDate != null)
            {
                DateTime l_T_EndDate = T_EndDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.EndDate < l_T_EndDate);
            }
            #endregion




            return Query;
        }
    }
}
